DROP TABLE IF EXISTS `account_folder_observations`;

CREATE TABLE `account_folder_observations` (
  `account_folder_observation_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) NOT NULL,
  `location_name` varchar(255) DEFAULT NULL,
  `observation_date_time` datetime DEFAULT NULL,
  `huddle_account_folder_id` INT(11) NULL,
  `is_private` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  KEY `account_folder_observation_id` (`account_folder_observation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `account_folder_observations`   
  ADD PRIMARY KEY (`account_folder_observation_id`);
  