CREATE TABLE `observation_notice_log`(  
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `account_folder_observation_id` INT(11),
  `observation_date_time` DATETIME,
  `user_id` INT(11) NULL,
  `created_on` DATETIME,
  PRIMARY KEY (`id`)
);
