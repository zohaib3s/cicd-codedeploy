
CREATE TABLE `frameworks`(  
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `parent_id` INT(11),
  `framework_code` VARCHAR(50),
  `framework_title` TEXT,
  `framework_desc` TEXT,
  `created_on` DATETIME,
  `created_by` INT(11),
  `last_edit_on` DATETIME,
  `last_edit_by` INT(11),
  PRIMARY KEY (`id`)
);
