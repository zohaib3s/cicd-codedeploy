
DROP TABLE IF EXISTS `account_folder_observation_users`;

CREATE TABLE `account_folder_observation_users` (
  `account_folder_observation_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_folder_observation_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL COMMENT '300 == Observee, 310 == Observer',
  `notify_at` int(11) DEFAULT NULL,
  `notify_at_unit` int(2) DEFAULT NULL COMMENT '1 = min, 2 = Hrs, 3 = Days',
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`account_folder_observation_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;