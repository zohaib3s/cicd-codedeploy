-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2013 at 02:34 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sibme_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_account_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `custom_url` varchar(255) DEFAULT NULL,
  `image_logo` varchar(255) DEFAULT NULL,
  `header_background_color` varchar(255) DEFAULT NULL,
  `text_link_color` varchar(255) DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `storage` int(11) DEFAULT NULL,
  `storage_used` int(11) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `last_4digits` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `subdomain` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `braintree_customer_id` text,
  `braintree_subscription_id` text,
  `nav_bg_color` varchar(255) DEFAULT NULL,
  `usernav_bg_color` varchar(255) DEFAULT NULL,
  `in_trial` tinyint(1) DEFAULT NULL,
  `has_credit_card` tinyint(1) DEFAULT NULL,
  `suspended_at` datetime DEFAULT NULL,
  `is_suspended` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_accounts_on_user_id` (`user_id`),
  KEY `index_accounts_on_plan_id` (`plan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `parent_account_id`, `user_id`, `company_name`, `custom_url`, `image_logo`, `header_background_color`, `text_link_color`, `plan_id`, `storage`, `storage_used`, `card_type`, `last_4digits`, `created_at`, `updated_at`, `subdomain`, `is_active`, `braintree_customer_id`, `braintree_subscription_id`, `nav_bg_color`, `usernav_bg_color`, `in_trial`, `has_credit_card`, `suspended_at`, `is_suspended`) VALUES
(1, 0, NULL, 'Sibme', NULL, '', '336699', NULL, 4, NULL, NULL, NULL, NULL, '2013-08-20 00:00:00', '2013-09-10 10:58:31', NULL, 1, NULL, NULL, '668dab', NULL, 0, 0, NULL, 0),
(2, 0, NULL, 'Sibme QA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-08-20 00:00:00', '2013-08-20 00:00:00', NULL, 1, NULL, NULL, NULL, NULL, 1, 0, NULL, 0),
(4, 0, NULL, '3S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-09-01 04:03:22', '2013-09-01 04:03:22', NULL, 1, NULL, NULL, NULL, NULL, 1, 0, NULL, 0),
(7, 1, NULL, 'Test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-09-02 08:09:15', '0000-00-00 00:00:00', NULL, 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0),
(8, 0, NULL, '3S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-09-02 12:34:03', '2013-09-02 12:34:03', NULL, 1, NULL, NULL, NULL, NULL, 1, 0, NULL, 0),
(10, 0, NULL, 'Home', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2013-09-04 06:30:17', '2013-09-04 06:30:17', NULL, 1, NULL, NULL, NULL, NULL, 1, 0, NULL, 0),
(11, 0, NULL, 'Home', NULL, NULL, '2a4d44', NULL, NULL, NULL, NULL, NULL, NULL, '2013-09-04 06:34:19', '2013-09-04 13:35:02', NULL, 1, NULL, NULL, '8bab67', NULL, 1, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `account_folderdocument_attachments`
--

CREATE TABLE IF NOT EXISTS `account_folderdocument_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_folder_document_id` int(11) NOT NULL,
  `attach_id` int(11) NOT NULL COMMENT 'account_folder_attach_document_id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `account_folderdocument_attachments`
--

INSERT INTO `account_folderdocument_attachments` (`id`, `account_folder_document_id`, `attach_id`) VALUES
(1, 15, 45),
(2, 15, 40),
(3, 19, 0),
(4, 20, 45),
(5, 22, 0),
(6, 23, 0),
(8, 28, 0),
(9, 32, 0),
(10, 33, 0),
(11, 34, 0);

-- --------------------------------------------------------

--
-- Table structure for table `account_folders`
--

CREATE TABLE IF NOT EXISTS `account_folders` (
  `account_folder_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `folder_type` tinyint(4) NOT NULL COMMENT '1=huddle, 2=video library, 3=my files, etc.',
  `parent_folder_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `desc` varchar(4000) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`account_folder_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `account_folders`
--

INSERT INTO `account_folders` (`account_folder_id`, `account_id`, `folder_type`, `parent_folder_id`, `name`, `desc`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`, `active`) VALUES
(5, 1, 1, NULL, 'Khurram''s Coaching classes', 'Khurram''s Coaching classes', 1, '2013-08-23 19:14:10', 1, '2013-08-23 07:14:10', 0),
(6, 1, 1, NULL, 'Mu Huddlesss', 'Huddle Descing', 1, '2013-08-24 20:31:04', 1, '2013-08-26 06:32:21', 1),
(7, 1, 1, NULL, 'Mu Huddle', 'Huddle Desc', 1, '2013-08-24 20:31:44', 1, '2013-09-10 09:08:28', 1),
(8, 1, 1, NULL, 'Mu Huddle', 'Huddle Desc', 1, '2013-08-24 20:32:45', 1, '2013-08-24 08:32:45', 0),
(17, 1, 2, NULL, 'My Test Doc', 'Testing Library', 1, '2013-08-28 15:40:28', 1, '2013-08-28 15:40:28', 1),
(18, 1, 2, NULL, 'Test Videos..', 'Description Is here ok.', 1, '2013-08-28 16:29:37', 1, '2013-09-02 06:00:36', 1),
(19, 1, 2, NULL, 'Testing Video', 'Hello World', 1, '2013-09-01 20:32:42', 1, '2013-09-01 20:32:42', 0),
(20, 8, 1, NULL, 'My Huddle', 'Testing.', 823, '2013-09-02 12:34:46', 823, '2013-09-02 12:34:46', 1),
(21, 11, 1, NULL, 'Khurram''s Coaching classes', '', 826, '2013-09-04 06:52:23', 826, '2013-09-04 08:08:31', 0),
(22, 11, 1, NULL, 'Khurram''s Coaching classes', 'Ok''s Im testing it....', 826, '2013-09-04 08:19:49', 826, '2013-09-04 09:28:36', 1),
(23, 11, 2, NULL, 'Testing', 'Test', 826, '2013-09-04 09:44:42', 826, '2013-09-04 09:44:42', 1),
(24, 1, 1, NULL, 'New Huddle added by hamid12', 'Mrs. Smith demonstrates how to execute a Socratic seminar12', 1, '2013-09-10 09:09:17', 1, '2013-09-10 11:23:32', 1),
(25, 1, 1, NULL, 'Test with Hamid', 'TEST WITH HAMID', 1, '2013-09-10 10:13:09', 1, '2013-09-10 10:13:09', 1),
(26, 1, 1, NULL, 'Test with Hamid', 'TEST WITH HAMID', 1, '2013-09-10 10:14:02', 1, '2013-09-11 04:31:45', 1),
(27, 1, 2, NULL, 'Mere Saath Chalte Chalte', 'Mere Saath Chalte Chalte', 1, '2013-09-10 10:34:33', 1, '2013-09-10 10:34:33', 1),
(28, 1, 2, NULL, 'Mere Saath Chalte Chalte', 'Mere Saath Chalte Chalte', 1, '2013-09-10 10:35:23', 1, '2013-09-10 10:35:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `account_folders_meta_data`
--

CREATE TABLE IF NOT EXISTS `account_folders_meta_data` (
  `account_folder_meta_data_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) NOT NULL,
  `meta_data_name` varchar(64) NOT NULL COMMENT 'tag, topic, subject, etc',
  `meta_data_value` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`account_folder_meta_data_id`),
  UNIQUE KEY `account_folders_meta_data_id_UNIQUE` (`account_folder_meta_data_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `account_folders_meta_data`
--

INSERT INTO `account_folders_meta_data` (`account_folder_meta_data_id`, `account_folder_id`, `meta_data_name`, `meta_data_value`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`) VALUES
(3, 5, 'message', 'Hello people!!!', 1, '2013-08-23 19:14:10', 1, '2013-08-23 07:14:10'),
(4, 6, 'message', 'Hello World!!! &nbsp; dffdsfdsfdsf', 1, '2013-08-24 20:31:04', 1, '2013-08-26 06:32:21'),
(5, 7, 'message', 'Hello World!!!', 1, '2013-08-24 20:31:44', 1, '2013-09-10 09:08:28'),
(6, 8, 'message', 'Hello World!!!', 1, '2013-08-24 20:32:45', 1, '2013-08-24 08:32:45'),
(7, 10, 'tag', 'kkk', 1, '2013-08-28 05:56:35', 1, '2013-08-28 05:56:35'),
(8, 10, 'tag', 'kk', 1, '2013-08-28 05:56:36', 1, '2013-08-28 05:56:36'),
(9, 11, 'tag', 'kkk', 1, '2013-08-28 06:11:33', 1, '2013-08-28 06:11:33'),
(10, 11, 'tag', 'k', 1, '2013-08-28 06:11:33', 1, '2013-08-28 06:11:33'),
(11, 12, 'tag', 'kk', 1, '2013-08-28 06:12:29', 1, '2013-08-28 06:12:29'),
(12, 12, 'tag', 'dd', 1, '2013-08-28 06:12:29', 1, '2013-08-28 06:12:29'),
(13, 13, 'tag', 'Test', 1, '2013-08-28 15:28:36', 1, '2013-08-28 03:28:36'),
(14, 13, 'tag', 'kk', 1, '2013-08-28 15:28:36', 1, '2013-08-28 03:28:36'),
(15, 14, 'tag', 'testing', 1, '2013-08-28 15:31:24', 1, '2013-08-28 03:31:24'),
(16, 15, 'tag', 'll', 1, '2013-08-28 15:33:50', 1, '2013-08-28 03:33:50'),
(17, 16, 'tag', 'kkk', 1, '2013-08-28 15:37:41', 1, '2013-08-28 03:37:41'),
(18, 17, 'tag', 'kk', 1, '2013-08-28 15:40:28', 1, '2013-08-28 03:40:28'),
(19, 17, 'tag', 'asdd', 1, '2013-08-28 15:40:28', 1, '2013-08-28 03:40:28'),
(22, 19, 'tag', 'Hello World Testing 123', 1, '2013-09-01 20:32:42', 1, '2013-09-01 08:32:42'),
(23, 19, 'tag', 'Test', 1, '2013-09-01 20:32:42', 1, '2013-09-01 08:32:42'),
(30, 18, 'tag', 'Test', 1, '2013-09-02 06:00:36', 1, '2013-09-02 06:00:36'),
(31, 18, 'tag', 'Khurram', 1, '2013-09-02 06:00:36', 1, '2013-09-02 06:00:36'),
(32, 18, 'tag', 'Edited', 1, '2013-09-02 06:00:36', 1, '2013-09-02 06:00:36'),
(33, 20, 'message', 'Hello World', 823, '2013-09-02 12:34:46', 823, '2013-09-02 12:34:46'),
(34, 21, 'message', 'Hello People.. welcome to sibme!', 826, '2013-09-04 06:52:23', 826, '2013-09-04 08:08:31'),
(35, 22, 'message', 'Hello World! welcome to sibme 3 users', 826, '2013-09-04 08:19:49', 826, '2013-09-04 09:28:36'),
(36, 23, 'tag', 'Hello', 826, '2013-09-04 09:44:42', 826, '2013-09-04 09:44:42'),
(37, 24, 'message', 'Mrs. Smith demonstrates how to execute a Socratic seminar', 1, '2013-09-10 09:09:17', 1, '2013-09-10 11:23:32'),
(38, 25, 'message', '', 1, '2013-09-10 10:13:09', 1, '2013-09-10 10:13:09'),
(39, 26, 'message', '', 1, '2013-09-10 10:14:02', 1, '2013-09-11 04:31:45'),
(40, 27, 'tag', 'Mere Saath Chalte Chalte', 1, '2013-09-10 10:34:33', 1, '2013-09-10 10:34:33'),
(41, 28, 'tag', 'Mere Saath Chalte Chalte', 1, '2013-09-10 10:35:23', 1, '2013-09-10 10:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `account_folder_documents`
--

CREATE TABLE IF NOT EXISTS `account_folder_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `is_viewed` bit(1) NOT NULL DEFAULT b'0',
  `title` varchar(255) DEFAULT NULL,
  `desc` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `account_folder_documents`
--

INSERT INTO `account_folder_documents` (`id`, `account_folder_id`, `document_id`, `is_viewed`, `title`, `desc`) VALUES
(5, 7, 27, '1', 'My Video', 'sadasd'),
(9, 7, 32, '1', 'Testing', '123'),
(10, 12, 33, '1', 'kk', 'asdsdd'),
(11, 17, 38, '1', 'My Test Doc', 'Testing Library'),
(12, 18, 39, '1', 'Test Video', 'Description ....'),
(13, 7, 40, '1', 'K Video io', 'IO Upload.'),
(14, 7, 41, '1', 'Testing', 'Tester'),
(15, 7, 43, '1', 'My 100', '100 desc'),
(16, 7, 44, '1', 'Another 100', '100 here.'),
(17, 7, 45, '1', 'K Test 2', 'testing'),
(18, 7, 46, '1', '100 3', '100 3 attempt'),
(19, 7, 63, '1', 'Specialty products-variations export-convex glass.csv', ''),
(20, 7, 64, '1', 'Specialty products-variations export-convex glass.csv', ''),
(21, 7, 65, '1', 'Hello Short', 'Short'),
(22, 7, 66, '1', '957g6tQmRQG3KlO8FO6B_ioncoachselected', ''),
(23, 7, 67, '1', 'ioncoachselected', ''),
(24, 19, 68, '1', 'Testing Video', 'Hello World'),
(25, 19, 69, '1', '100', ''),
(26, 18, 70, '1', 'InLine Frames Price Export from BOM', ''),
(28, 20, 72, '1', 'Specialty products-variations export-convex glass', ''),
(29, 22, 74, '1', 'My Video', 'Testing.'),
(30, 23, 75, '1', 'Testing', 'Test'),
(31, 22, 76, '1', 'Video 2', ''),
(32, 25, 79, '1', '_Ishq Sufiyana Song from The Dirty Picture_ Ft. Emraan Hashmi, Vidya Balan - YouTube_2 - Copy', ''),
(33, 25, 80, '1', 'test', ''),
(34, 26, 81, '1', '_Ishq Sufiyana Song from The Dirty Picture_ Ft. Emraan Hashmi, Vidya Balan - YouTube_2 - Copy', '');

-- --------------------------------------------------------

--
-- Table structure for table `account_folder_groups`
--

CREATE TABLE IF NOT EXISTS `account_folder_groups` (
  `account_folder_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`account_folder_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `account_folder_groups`
--

INSERT INTO `account_folder_groups` (`account_folder_group_id`, `account_folder_id`, `group_id`, `role_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`) VALUES
(1, 5, 1, 210, 1, '2013-08-23 19:14:10', 1, '2013-08-23 07:14:10'),
(4, 8, 1, 210, 1, '2013-08-24 20:32:45', 1, '2013-08-24 08:32:45'),
(5, 6, 1, 210, 1, '2013-08-26 06:32:21', 1, '2013-08-26 06:32:21'),
(7, 7, 1, 210, 1, '2013-09-10 09:08:28', 1, '2013-09-10 09:08:28'),
(17, 25, 1, 210, 1, '2013-09-10 10:13:10', 1, '2013-09-10 10:13:10'),
(19, 24, 1, 210, 1, '2013-09-10 11:23:32', 1, '2013-09-10 11:23:32'),
(20, 26, 1, 210, 1, '2013-09-11 04:31:46', 1, '2013-09-11 04:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `account_folder_subjects`
--

CREATE TABLE IF NOT EXISTS `account_folder_subjects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `account_folder_subjects`
--

INSERT INTO `account_folder_subjects` (`id`, `account_folder_id`, `subject_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`) VALUES
(1, 10, 3, 1, '2013-08-28 05:56:36', 1, '2013-08-28 05:56:36'),
(2, 10, 2, 1, '2013-08-28 05:56:36', 1, '2013-08-28 05:56:36'),
(3, 11, 3, 1, '2013-08-28 06:11:33', 1, '2013-08-28 06:11:33'),
(4, 12, 3, 1, '2013-08-28 06:12:29', 1, '2013-08-28 06:12:29'),
(5, 13, 3, 1, '2013-08-28 15:28:36', 1, '2013-08-28 03:28:36'),
(6, 14, 3, 1, '2013-08-28 15:31:24', 1, '2013-08-28 03:31:24'),
(7, 15, 3, 1, '2013-08-28 15:33:50', 1, '2013-08-28 03:33:50'),
(8, 16, 3, 1, '2013-08-28 15:37:41', 1, '2013-08-28 03:37:41'),
(9, 17, 3, 1, '2013-08-28 15:40:28', 1, '2013-08-28 03:40:28'),
(21, 18, 2, 1, '2013-09-02 06:00:36', 1, '2013-09-02 06:00:36'),
(11, 19, 4, 1, '2013-09-01 20:32:42', 1, '2013-09-01 08:32:42'),
(12, 19, 3, 1, '2013-09-01 20:32:42', 1, '2013-09-01 08:32:42'),
(20, 18, 3, 1, '2013-09-02 06:00:36', 1, '2013-09-02 06:00:36'),
(19, 18, 4, 1, '2013-09-02 06:00:36', 1, '2013-09-02 06:00:36'),
(22, 23, 5, 826, '2013-09-04 09:44:42', 826, '2013-09-04 09:44:42'),
(23, 27, 6, 1, '2013-09-10 10:34:33', 1, '2013-09-10 10:34:33'),
(24, 28, 6, 1, '2013-09-10 10:35:23', 1, '2013-09-10 10:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `account_folder_topics`
--

CREATE TABLE IF NOT EXISTS `account_folder_topics` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `account_folder_topics`
--

INSERT INTO `account_folder_topics` (`id`, `account_folder_id`, `topic_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`) VALUES
(1, NULL, 1, NULL, NULL, NULL, NULL),
(2, 11, 1, 1, '2013-08-28 06:11:33', 1, '2013-08-28 06:11:33'),
(3, 12, 1, 1, '2013-08-28 06:12:29', 1, '2013-08-28 06:12:29'),
(4, 13, 1, 1, '2013-08-28 15:28:36', 1, '2013-08-28 03:28:36'),
(5, 14, 1, 1, '2013-08-28 15:31:24', 1, '2013-08-28 03:31:24'),
(6, 15, 1, 1, '2013-08-28 15:33:50', 1, '2013-08-28 03:33:50'),
(7, 16, 1, 1, '2013-08-28 15:37:41', 1, '2013-08-28 03:37:41'),
(8, 17, 1, 1, '2013-08-28 15:40:28', 1, '2013-08-28 03:40:28'),
(17, 18, 2, 1, '2013-09-02 06:00:36', 1, '2013-09-02 06:00:36'),
(11, 19, 2, 1, '2013-09-01 20:32:42', 1, '2013-09-01 08:32:42'),
(16, 18, 1, 1, '2013-09-02 06:00:36', 1, '2013-09-02 06:00:36'),
(18, 23, 3, 826, '2013-09-04 09:44:42', 826, '2013-09-04 09:44:42'),
(19, 27, 4, 1, '2013-09-10 10:34:33', 1, '2013-09-10 10:34:33'),
(20, 28, 4, 1, '2013-09-10 10:35:23', 1, '2013-09-10 10:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `account_folder_users`
--

CREATE TABLE IF NOT EXISTS `account_folder_users` (
  `account_folder_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`account_folder_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=161 ;

--
-- Dumping data for table `account_folder_users`
--

INSERT INTO `account_folder_users` (`account_folder_user_id`, `account_folder_id`, `user_id`, `role_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`) VALUES
(7, 5, 3, 200, 1, '2013-08-23 19:14:10', 1, '2013-08-23 07:14:10'),
(8, 5, 814, 210, 1, '2013-08-23 19:14:10', 1, '2013-08-23 07:14:10'),
(13, 8, 3, 200, 1, '2013-08-24 20:32:45', 1, '2013-08-24 08:32:45'),
(14, 8, 814, 210, 1, '2013-08-24 20:32:45', 1, '2013-08-24 08:32:45'),
(17, 6, 3, 200, 1, '2013-08-26 06:32:21', 1, '2013-08-26 06:32:21'),
(18, 6, 814, 210, 1, '2013-08-26 06:32:21', 1, '2013-08-26 06:32:21'),
(20, 6, 820, 200, 1, '2013-08-30 07:44:22', 1, '2013-08-30 07:44:22'),
(21, 20, 823, 200, 823, '2013-09-02 12:34:46', 823, '2013-09-02 12:34:46'),
(25, 21, 830, 200, 826, '2013-09-04 08:08:31', 826, '2013-09-04 08:08:31'),
(26, 21, 831, 210, 826, '2013-09-04 08:08:31', 826, '2013-09-04 08:08:31'),
(27, 22, 826, 200, 826, '2013-09-04 08:19:49', 826, '2013-09-04 08:19:49'),
(32, 22, 832, 200, 826, '2013-09-04 09:28:36', 826, '2013-09-04 09:28:36'),
(33, 22, 833, 210, 826, '2013-09-04 09:28:36', 826, '2013-09-04 09:28:36'),
(37, 7, 815, 200, 1, '2013-09-10 09:08:28', 1, '2013-09-10 09:08:28'),
(38, 7, 3, 200, 1, '2013-09-10 09:08:28', 1, '2013-09-10 09:08:28'),
(39, 7, 820, 200, 1, '2013-09-10 09:08:28', 1, '2013-09-10 09:08:28'),
(40, 7, 814, 210, 1, '2013-09-10 09:08:28', 1, '2013-09-10 09:08:28'),
(41, 24, 1, 200, 1, '2013-09-10 09:09:17', 1, '2013-09-10 09:09:17'),
(123, 25, 1, 200, 1, '2013-09-10 10:13:09', 1, '2013-09-10 10:13:09'),
(124, 25, 819, 200, 1, '2013-09-10 10:13:09', 1, '2013-09-10 10:13:09'),
(125, 25, 818, 200, 1, '2013-09-10 10:13:09', 1, '2013-09-10 10:13:09'),
(126, 25, 815, 200, 1, '2013-09-10 10:13:09', 1, '2013-09-10 10:13:09'),
(127, 25, 3, 200, 1, '2013-09-10 10:13:09', 1, '2013-09-10 10:13:09'),
(128, 25, 834, 200, 1, '2013-09-10 10:13:09', 1, '2013-09-10 10:13:09'),
(129, 25, 820, 200, 1, '2013-09-10 10:13:09', 1, '2013-09-10 10:13:09'),
(130, 25, 817, 200, 1, '2013-09-10 10:13:09', 1, '2013-09-10 10:13:09'),
(131, 25, 816, 210, 1, '2013-09-10 10:13:10', 1, '2013-09-10 10:13:10'),
(132, 25, 814, 210, 1, '2013-09-10 10:13:10', 1, '2013-09-10 10:13:10'),
(133, 26, 1, 200, 1, '2013-09-10 10:14:02', 1, '2013-09-10 10:14:02'),
(143, 24, 819, 200, 1, '2013-09-10 11:23:32', 1, '2013-09-10 11:23:32'),
(144, 24, 818, 200, 1, '2013-09-10 11:23:32', 1, '2013-09-10 11:23:32'),
(145, 24, 815, 200, 1, '2013-09-10 11:23:32', 1, '2013-09-10 11:23:32'),
(146, 24, 3, 200, 1, '2013-09-10 11:23:32', 1, '2013-09-10 11:23:32'),
(147, 24, 834, 200, 1, '2013-09-10 11:23:32', 1, '2013-09-10 11:23:32'),
(148, 24, 820, 200, 1, '2013-09-10 11:23:32', 1, '2013-09-10 11:23:32'),
(149, 24, 817, 200, 1, '2013-09-10 11:23:32', 1, '2013-09-10 11:23:32'),
(150, 24, 816, 210, 1, '2013-09-10 11:23:32', 1, '2013-09-10 11:23:32'),
(151, 24, 814, 210, 1, '2013-09-10 11:23:32', 1, '2013-09-10 11:23:32'),
(152, 26, 818, 200, 1, '2013-09-11 04:31:45', 1, '2013-09-11 04:31:45'),
(153, 26, 819, 200, 1, '2013-09-11 04:31:45', 1, '2013-09-11 04:31:45'),
(154, 26, 3, 200, 1, '2013-09-11 04:31:45', 1, '2013-09-11 04:31:45'),
(155, 26, 815, 200, 1, '2013-09-11 04:31:45', 1, '2013-09-11 04:31:45'),
(156, 26, 834, 200, 1, '2013-09-11 04:31:45', 1, '2013-09-11 04:31:45'),
(157, 26, 820, 200, 1, '2013-09-11 04:31:45', 1, '2013-09-11 04:31:45'),
(158, 26, 817, 200, 1, '2013-09-11 04:31:45', 1, '2013-09-11 04:31:45'),
(159, 26, 816, 210, 1, '2013-09-11 04:31:46', 1, '2013-09-11 04:31:46'),
(160, 26, 814, 210, 1, '2013-09-11 04:31:46', 1, '2013-09-11 04:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `account_meta_data`
--

CREATE TABLE IF NOT EXISTS `account_meta_data` (
  `account_meta_data_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `meta_data_name` varchar(64) NOT NULL COMMENT 'tag, topic, subject, etc',
  `meta_data_value` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`account_meta_data_id`),
  UNIQUE KEY `account_meta_data_id_UNIQUE` (`account_meta_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `audit_emails`
--

CREATE TABLE IF NOT EXISTS `audit_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_from` text,
  `email_to` text,
  `email_cc` text,
  `email_bcc` text,
  `email_subject` varchar(4000) DEFAULT NULL,
  `email_body` text,
  `is_html` bit(1) NOT NULL,
  `error_msg` text,
  `sent_date` datetime DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=103 ;

--
-- Dumping data for table `audit_emails`
--

INSERT INTO `audit_emails` (`id`, `email_from`, `email_to`, `email_cc`, `email_bcc`, `email_subject`, `email_body`, `is_html`, `error_msg`, `sent_date`, `account_id`) VALUES
(1, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/users/activation/c4ca4238a0b923820dcc509a6f75849b/d0n2g70hf40vtxtdx58f/96ea64f3a1aa2fd00c72faacf0cb8ac9">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 01:24:20', 1),
(2, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/users/activation/c4ca4238a0b923820dcc509a6f75849b/nd4cdbrhmqw5c1jzltme/da8ce53cf0240070ce6c69c48cd588ee">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 01:25:05', 1),
(3, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/users/activation/c4ca4238a0b923820dcc509a6f75849b/vxbhv3cqgfq54o19hrxp/82489c9737cc245530c7a6ebef3753ec">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 01:28:00', 1),
(4, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/ksnnt73h4bqbrwf047el/7c590f01490190db0ed02a5070e20f01">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 04:35:42', 1),
(5, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/625gia9s4foa7wqwclww/35cf8659cfcb13224cbd47863a34fc58">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 04:48:27', 1),
(6, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/mtc16p4d4l15vsd62w2m/beb22fb694d513edcf5533cf006dfeae">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 05:45:24', 1),
(7, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/bmk7wk4dj8f84kalsl8x/9e3cfc48eccf81a0d57663e129aef3cb">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 05:53:16', 1),
(8, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/nva9ts6mb3irrsq25dn7/28267ab848bcf807b2ed53c3a8f8fc8a">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 06:54:34', 1),
(9, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/uybtic176xwcwiogi1lv/7a53928fa4dd31e82c6ef826f341daec">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 06:58:15', 1),
(10, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/l5v41yky0xy3b47lnugp/1905aedab9bf2477edc068a355bba31a">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 14:28:53', 1),
(11, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/4zo6jrij8555shg8ok35/1141938ba2c2b13f5505d7c424ebae5f">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 14:45:31', 1),
(12, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/sfraeu0n0bcaxg4c7z8r/1aa48fc4880bb0c9b8a3bf979d3b917e">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 14:52:46', 1),
(13, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/c57zohpea6wxnw1vsoas/dc5689792e08eb2e219dce49e64c885b">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 14:55:41', 1),
(14, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/xrt5mxywobro01ceze7z/846c260d715e5b854ffad5f70a516c88">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 15:07:15', 1),
(15, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/4xsjjd0yghikskk5werh/d58072be2820e8682c0a27c0518e805e">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 16:51:51', 1),
(16, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Welcome to Sibme!', '<div id=":6r" style="overflow: hidden;">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href="#" target="_blank">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class="il">Sibme</span> as much as you would like.  We know it''s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href="mailto:info@sibme.com" target="_blank">info@<span class="il">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class="il">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href="http://localhost/sibme/app//Users/login" target="_blank">http://localhost/sibme/app/<span class="il">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> ksaleem3    </p>\n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-08-23 16:52:33', NULL),
(17, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/6x9eadwlb129tx15ej72/6e7b33fdea3adc80ebd648fffb665bb8">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 16:54:57', 1),
(18, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Welcome to Sibme!', '<div id=":6r" style="overflow: hidden;">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href="#" target="_blank">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class="il">Sibme</span> as much as you would like.  We know it''s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href="mailto:info@sibme.com" target="_blank">info@<span class="il">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class="il">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href="http://localhost/sibme/app//Users/login" target="_blank">http://localhost/sibme/app/<span class="il">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> ksaleem3    </p>\n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-08-23 16:55:33', 1),
(19, 'do-not-reply@sibme.com', 'info@3ssolutions.net', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/k41o1qur8lmaagiqphpc/a8ecbabae151abacba7dbde04f761c37">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 16:58:23', 1),
(20, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/4q5cw0st7y9t20fyg0px/32b30a250abd6331e03a2a1f16466346">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 17:01:33', 1),
(21, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saeems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/9643hnkinr7qpwda0s7l/b6edc1cd1f36e45daf6d7824d7bb2283">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 17:58:41', 1),
(22, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/fl7xx8lk0y7dcqugbrw1/670e8a43b246801ca1eaca97b3e19189">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 18:26:31', 1),
(23, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/f5eqwhsjc4s5vfne9i5m/81e74d678581a3bb7a720b019f4f1a93">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 18:29:55', 1),
(24, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Welcome to Sibme!', '<div id=":6r" style="overflow: hidden;">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href="#" target="_blank">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class="il">Sibme</span> as much as you would like.  We know it''s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href="mailto:info@sibme.com" target="_blank">info@<span class="il">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class="il">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href="http://localhost/sibme/app//Users/login" target="_blank">http://localhost/sibme/app/<span class="il">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> ksaleem3    </p>\n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-08-23 18:30:31', 1),
(25, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/qwamqlvxfjzf47f2w23r/e0cf1f47118daebc5b16269099ad7347">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 18:31:11', 1),
(26, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/os5fvbptg6al7xt1vzga/96b9bff013acedfb1d140579e2fbeb63">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 18:33:31', 1),
(27, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Welcome to Sibme!', '<div id=":6r" style="overflow: hidden;">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href="#" target="_blank">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class="il">Sibme</span> as much as you would like.  We know it''s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href="mailto:info@sibme.com" target="_blank">info@<span class="il">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class="il">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href="http://localhost/sibme/app//Users/login" target="_blank">http://localhost/sibme/app/<span class="il">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> ksaleem3    </p>\n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-08-23 18:34:03', 1),
(28, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/xib9bnmv9i27nw7avaaz/71ad16ad2c4d81f348082ff6c4b20768">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-23 18:34:47', 1);
INSERT INTO `audit_emails` (`id`, `email_from`, `email_to`, `email_cc`, `email_bcc`, `email_subject`, `email_body`, `is_html`, `error_msg`, `sent_date`, `account_id`) VALUES
(29, 'do-not-reply@sibme.com', NULL, NULL, NULL, '  created a new Sibme Coaching Huddle: ', '<div id=":6r" style="overflow: hidden;">\n    <p><pre class="cake-error"><a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e58840f7-trace'').style.display = (document.getElementById(''cakeErr52195e58840f7-trace'').style.display == ''none'' ? '''' : ''none'');"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>5</b>]<div id="cakeErr52195e58840f7-trace" class="cake-stack-trace" style="display: none;"><a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e58840f7-code'').style.display = (document.getElementById(''cakeErr52195e58840f7-code'').style.display == ''none'' ? '''' : ''none'')">Code</a> <a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e58840f7-context'').style.display = (document.getElementById(''cakeErr52195e58840f7-context'').style.display == ''none'' ? '''' : ''none'')">Context</a><pre id="cakeErr52195e58840f7-code" class="cake-code-dump" style="display: none;"><code><span style="color: #000000"><span style="color: #0000BB">?&gt;</span></span></code>\n<code><span style="color: #000000"><span style="color: #0000BB"></span><span style="color: #007700">&lt;</span><span style="color: #0000BB">div&nbsp;id</span><span style="color: #007700">=</span><span style="color: #DD0000">":6r"&nbsp;</span><span style="color: #0000BB">style</span><span style="color: #007700">=</span><span style="color: #DD0000">"overflow:&nbsp;hidden;"</span><span style="color: #007700">&gt;</span></span></code>\n<span class="code-highlight"><code><span style="color: #000000">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;<span style="color: #0000BB">&lt;?php&nbsp;</span><span style="color: #007700">echo&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''first_name''</span><span style="color: #007700">]&nbsp;.&nbsp;</span><span style="color: #DD0000">"&nbsp;"&nbsp;</span><span style="color: #007700">.&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''last_name''</span><span style="color: #007700">]&nbsp;</span><span style="color: #0000BB">?&gt;</span>&nbsp;invited&nbsp;you&nbsp;to&nbsp;a&nbsp;Coaching&nbsp;Huddle::&lt;/p&gt;</span></code></span></pre><pre id="cakeErr52195e58840f7-context" class="cake-context" style="display: none;">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class="stack-trace">include - APP/View/Elements/emails/new_huddle.ctp, line 5\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre><pre class="cake-error"><a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e5884a8d-trace'').style.display = (document.getElementById(''cakeErr52195e5884a8d-trace'').style.display == ''none'' ? '''' : ''none'');"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>5</b>]<div id="cakeErr52195e5884a8d-trace" class="cake-stack-trace" style="display: none;"><a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e5884a8d-code'').style.display = (document.getElementById(''cakeErr52195e5884a8d-code'').style.display == ''none'' ? '''' : ''none'')">Code</a> <a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e5884a8d-context'').style.display = (document.getElementById(''cakeErr52195e5884a8d-context'').style.display == ''none'' ? '''' : ''none'')">Context</a><pre id="cakeErr52195e5884a8d-code" class="cake-code-dump" style="display: none;"><code><span style="color: #000000"><span style="color: #0000BB">?&gt;</span></span></code>\n<code><span style="color: #000000"><span style="color: #0000BB"></span><span style="color: #007700">&lt;</span><span style="color: #0000BB">div&nbsp;id</span><span style="color: #007700">=</span><span style="color: #DD0000">":6r"&nbsp;</span><span style="color: #0000BB">style</span><span style="color: #007700">=</span><span style="color: #DD0000">"overflow:&nbsp;hidden;"</span><span style="color: #007700">&gt;</span></span></code>\n<span class="code-highlight"><code><span style="color: #000000">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;<span style="color: #0000BB">&lt;?php&nbsp;</span><span style="color: #007700">echo&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''first_name''</span><span style="color: #007700">]&nbsp;.&nbsp;</span><span style="color: #DD0000">"&nbsp;"&nbsp;</span><span style="color: #007700">.&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''last_name''</span><span style="color: #007700">]&nbsp;</span><span style="color: #0000BB">?&gt;</span>&nbsp;invited&nbsp;you&nbsp;to&nbsp;a&nbsp;Coaching&nbsp;Huddle::&lt;/p&gt;</span></code></span></pre><pre id="cakeErr52195e5884a8d-context" class="cake-context" style="display: none;">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class="stack-trace">include - APP/View/Elements/emails/new_huddle.ctp, line 5\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre>  invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style="color:#3d9bc2"><pre class="cake-error"><a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e5885397-trace'').style.display = (document.getElementById(''cakeErr52195e5885397-trace'').style.display == ''none'' ? '''' : ''none'');"><b>Notice</b> (8)</a>: Undefined index: AccountFolder [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>7</b>]<div id="cakeErr52195e5885397-trace" class="cake-stack-trace" style="display: none;"><a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e5885397-code'').style.display = (document.getElementById(''cakeErr52195e5885397-code'').style.display == ''none'' ? '''' : ''none'')">Code</a> <a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e5885397-context'').style.display = (document.getElementById(''cakeErr52195e5885397-context'').style.display == ''none'' ? '''' : ''none'')">Context</a><pre id="cakeErr52195e5885397-code" class="cake-code-dump" style="display: none;"><code><span style="color: #000000">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;<span style="color: #0000BB">&lt;?php&nbsp;</span><span style="color: #007700">echo&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''first_name''</span><span style="color: #007700">]&nbsp;.&nbsp;</span><span style="color: #DD0000">"&nbsp;"&nbsp;</span><span style="color: #007700">.&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''last_name''</span><span style="color: #007700">]&nbsp;</span><span style="color: #0000BB">?&gt;</span>&nbsp;invited&nbsp;you&nbsp;to&nbsp;a&nbsp;Coaching&nbsp;Huddle::&lt;/p&gt;</span></code>\n<code><span style="color: #000000"><span style="color: #0000BB">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #007700">&lt;</span><span style="color: #0000BB">br</span><span style="color: #007700">/&gt;</span></span></code>\n<span class="code-highlight"><code><span style="color: #000000">&nbsp;&nbsp;&nbsp;&nbsp;&lt;h2&nbsp;style="color:#3d9bc2"&gt;<span style="color: #0000BB">&lt;?php&nbsp;</span><span style="color: #007700">echo&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''AccountFolder''</span><span style="color: #007700">][</span><span style="color: #DD0000">''name''</span><span style="color: #007700">];&nbsp;</span><span style="color: #0000BB">?&gt;</span>&lt;/h2&gt;</span></code></span></pre><pre id="cakeErr52195e5885397-context" class="cake-context" style="display: none;">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class="stack-trace">include - APP/View/Elements/emails/new_huddle.ctp, line 7\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre></h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href="http://localhost/sibme/app/">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read <pre class="cake-error"><a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e588607b-trace'').style.display = (document.getElementById(''cakeErr52195e588607b-trace'').style.display == ''none'' ? '''' : ''none'');"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>12</b>]<div id="cakeErr52195e588607b-trace" class="cake-stack-trace" style="display: none;"><a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e588607b-code'').style.display = (document.getElementById(''cakeErr52195e588607b-code'').style.display == ''none'' ? '''' : ''none'')">Code</a> <a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e588607b-context'').style.display = (document.getElementById(''cakeErr52195e588607b-context'').style.display == ''none'' ? '''' : ''none'')">Context</a><pre id="cakeErr52195e588607b-code" class="cake-code-dump" style="display: none;"><code><span style="color: #000000"><span style="color: #0000BB">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #007700">&lt;</span><span style="color: #0000BB">br</span><span style="color: #007700">/&gt;</span></span></code>\n<code><span style="color: #000000"><span style="color: #0000BB"></span></span></code>\n<span class="code-highlight"><code><span style="color: #000000">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;Read&nbsp;<span style="color: #0000BB">&lt;?php&nbsp;</span><span style="color: #007700">echo&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''first_name''</span><span style="color: #007700">]&nbsp;.&nbsp;</span><span style="color: #DD0000">"&nbsp;"&nbsp;</span><span style="color: #007700">.&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''last_name''</span><span style="color: #007700">]&nbsp;</span><span style="color: #0000BB">?&gt;</span>''s&nbsp;message&nbsp;below:&lt;/p&gt;</span></code></span></pre><pre id="cakeErr52195e588607b-context" class="cake-context" style="display: none;">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class="stack-trace">include - APP/View/Elements/emails/new_huddle.ctp, line 12\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre><pre class="cake-error"><a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e588694a-trace'').style.display = (document.getElementById(''cakeErr52195e588694a-trace'').style.display == ''none'' ? '''' : ''none'');"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>12</b>]<div id="cakeErr52195e588694a-trace" class="cake-stack-trace" style="display: none;"><a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e588694a-code'').style.display = (document.getElementById(''cakeErr52195e588694a-code'').style.display == ''none'' ? '''' : ''none'')">Code</a> <a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e588694a-context'').style.display = (document.getElementById(''cakeErr52195e588694a-context'').style.display == ''none'' ? '''' : ''none'')">Context</a><pre id="cakeErr52195e588694a-code" class="cake-code-dump" style="display: none;"><code><span style="color: #000000"><span style="color: #0000BB">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #007700">&lt;</span><span style="color: #0000BB">br</span><span style="color: #007700">/&gt;</span></span></code>\n<code><span style="color: #000000"><span style="color: #0000BB"></span></span></code>\n<span class="code-highlight"><code><span style="color: #000000">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;Read&nbsp;<span style="color: #0000BB">&lt;?php&nbsp;</span><span style="color: #007700">echo&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''first_name''</span><span style="color: #007700">]&nbsp;.&nbsp;</span><span style="color: #DD0000">"&nbsp;"&nbsp;</span><span style="color: #007700">.&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''last_name''</span><span style="color: #007700">]&nbsp;</span><span style="color: #0000BB">?&gt;</span>''s&nbsp;message&nbsp;below:&lt;/p&gt;</span></code></span></pre><pre id="cakeErr52195e588694a-context" class="cake-context" style="display: none;">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class="stack-trace">include - APP/View/Elements/emails/new_huddle.ctp, line 12\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre> ''s message below:</p>\n    <p><pre class="cake-error"><a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e5887219-trace'').style.display = (document.getElementById(''cakeErr52195e5887219-trace'').style.display == ''none'' ? '''' : ''none'');"><b>Notice</b> (8)</a>: Undefined index: account_folders_meta_data [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>13</b>]<div id="cakeErr52195e5887219-trace" class="cake-stack-trace" style="display: none;"><a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e5887219-code'').style.display = (document.getElementById(''cakeErr52195e5887219-code'').style.display == ''none'' ? '''' : ''none'')">Code</a> <a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e5887219-context'').style.display = (document.getElementById(''cakeErr52195e5887219-context'').style.display == ''none'' ? '''' : ''none'')">Context</a><pre id="cakeErr52195e5887219-code" class="cake-code-dump" style="display: none;"><code><span style="color: #000000"><span style="color: #0000BB"></span></span></code>\n<code><span style="color: #000000">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;Read&nbsp;<span style="color: #0000BB">&lt;?php&nbsp;</span><span style="color: #007700">echo&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''first_name''</span><span style="color: #007700">]&nbsp;.&nbsp;</span><span style="color: #DD0000">"&nbsp;"&nbsp;</span><span style="color: #007700">.&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''last_name''</span><span style="color: #007700">]&nbsp;</span><span style="color: #0000BB">?&gt;</span>''s&nbsp;message&nbsp;below:&lt;/p&gt;</span></code>\n<span class="code-highlight"><code><span style="color: #000000">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;<span style="color: #0000BB">&lt;?php&nbsp;</span><span style="color: #007700">echo&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''account_folders_meta_data''</span><span style="color: #007700">][</span><span style="color: #DD0000">''message''</span><span style="color: #007700">];&nbsp;</span><span style="color: #0000BB">?&gt;</span>&lt;/p&gt;</span></code></span></pre><pre id="cakeErr52195e5887219-context" class="cake-context" style="display: none;">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class="stack-trace">include - APP/View/Elements/emails/new_huddle.ctp, line 13\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong><pre class="cake-error"><a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e5887cf8-trace'').style.display = (document.getElementById(''cakeErr52195e5887cf8-trace'').style.display == ''none'' ? '''' : ''none'');"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>16</b>]<div id="cakeErr52195e5887cf8-trace" class="cake-stack-trace" style="display: none;"><a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e5887cf8-code'').style.display = (document.getElementById(''cakeErr52195e5887cf8-code'').style.display == ''none'' ? '''' : ''none'')">Code</a> <a href="javascript:void(0);" onclick="document.getElementById(''cakeErr52195e5887cf8-context'').style.display = (document.getElementById(''cakeErr52195e5887cf8-context'').style.display == ''none'' ? '''' : ''none'')">Context</a><pre id="cakeErr52195e5887cf8-code" class="cake-code-dump" style="display: none;"><code><span style="color: #000000"><span style="color: #0000BB">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #007700">&lt;</span><span style="color: #0000BB">p</span><span style="color: #007700">&gt;</span><span style="color: #0000BB">Still&nbsp;have&nbsp;questions</span><span style="color: #007700">?&lt;/</span><span style="color: #0000BB">p</span><span style="color: #007700">&gt;</span></span></code>\n<code><span style="color: #000000"><span style="color: #0000BB"></span></span></code>\n<span class="code-highlight"><code><span style="color: #000000">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;Contact&nbsp;&lt;strong&gt;<span style="color: #0000BB">&lt;?php&nbsp;</span><span style="color: #007700">echo&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''first_name''</span><span style="color: #007700">]&nbsp;.&nbsp;</span><span style="color: #DD0000">"&nbsp;"&nbsp;</span><span style="color: #007700">.&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''last_name''</span><span style="color: #007700">]&nbsp;</span><span style="color: #0000BB">?&gt;</span>&lt;/strong&gt;&nbsp;at&nbsp;&lt;a&nbsp;href="mailto:<span style="color: #0000BB">&lt;?php&nbsp;</span><span style="color: #007700">echo&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''email''</span><span style="color: #007700">];&nbsp;</span><span style="color: #0000BB">?&gt;</span>"&gt;<span style="color: #0000BB">&lt;?php&nbsp;</span><span style="color: #007700">echo&nbsp;</span><span style="color: #0000BB">$data</span><span style="color: #007700">[</span><span style="color: #DD0000">''User''</span><span style="color: #007700">][</span><span style="color: #DD0000">''email''</span><span style="color: #007700">];&nbsp;</span><span style="color: #0000BB">?&gt;</span>&lt;/a&gt;&lt;/p&gt;</span></code></span></pre><pre id="cakeErr52195e5887cf8-context" class="cake-context" style="display: none;">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039', '1', NULL, '2013-08-24 20:31:04', NULL);
INSERT INTO `audit_emails` (`id`, `email_from`, `email_to`, `email_cc`, `email_bcc`, `email_subject`, `email_body`, `is_html`, `error_msg`, `sent_date`, `account_id`) VALUES
(30, 'do-not-reply@sibme.com', 'khurri.saleem@gmail.com', NULL, NULL, 'Khurram Saleem created a new Sibme Coaching Huddle: Mu Huddle', '<div id=":6r" style="overflow: hidden;">\n    <p>Khurram Saleem invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style="color:#3d9bc2">Mu Huddle</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href="http://localhost/sibme/app/">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram Saleem''s message below:</p>\n    <p>Hello World!!!</p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram Saleem</strong> at <a href="mailto:khurri.saleem@gmail.com">khurri.saleem@gmail.com</a></p>\n    \n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-08-24 20:32:45', 1),
(31, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:41:20', 1),
(32, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:41:24', 1),
(33, 'do-not-reply@sibme.com', 'khurri.saleem@gmail.com', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:41:27', 1),
(34, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:41:30', 1),
(35, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:41:57', 1),
(36, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:42:00', 1),
(37, 'do-not-reply@sibme.com', 'khurri.saleem@gmail.com', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:42:03', 1),
(38, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:42:06', 1),
(39, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:42:50', 1),
(40, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:42:53', 1),
(41, 'do-not-reply@sibme.com', 'khurri.saleem@gmail.com', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:42:56', 1),
(42, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:43:00', 1),
(43, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:43:24', 1),
(44, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:43:27', 1),
(45, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:44:27', 1),
(46, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Huddle Discussions', 'Hello World', '1', NULL, '2013-08-29 04:44:31', 1),
(47, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Huddle Discussions', 'Hello', '1', NULL, '2013-08-29 05:20:11', 1),
(48, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Huddle Discussions', 'My Reply to This.', '1', NULL, '2013-08-29 05:24:05', 1),
(49, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Huddle Discussions', 'My Reply to This.', '1', NULL, '2013-08-29 05:24:09', 1),
(50, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Huddle Discussions', 'Here you go', '1', NULL, '2013-08-29 05:41:49', 1),
(51, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Huddle Discussions', 'Testing..', '1', NULL, '2013-08-29 05:48:59', 1),
(52, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Huddle Discussions', 'My Testing 2', '1', NULL, '2013-08-29 05:50:43', 1),
(53, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Huddle Discussions', 'My Testing 2', '1', NULL, '2013-08-29 05:50:48', 1),
(54, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Huddle Discussions', 'My Testing 2', '1', NULL, '2013-08-29 05:51:58', 1),
(55, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Huddle Discussions', 'My Testing 2', '1', NULL, '2013-08-29 05:52:02', 1),
(56, 'do-not-reply@sibme.com', 'khurri.saleem@gmail.com', NULL, NULL, 'Huddle Discussions', 'My Testing 2', '1', NULL, '2013-08-29 05:52:27', 1),
(57, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Huddle Discussions', 'My Testing 2', '1', NULL, '2013-08-29 05:52:31', 1),
(58, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'You''ve been invited to join ''Khurram Sals'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/uacmswmgz6g9w7oh1da7/71ad16ad2c4d81f348082ff6c4b20768">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-29 20:37:17', 1),
(59, 'do-not-reply@sibme.com', 'khurri10@hotmail.com', NULL, NULL, 'You''ve been invited to join ''k ts'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/s7r1oajebdzu3wq74sme/43fa7f58b7eac7ac872209342e62e8f1">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-29 20:44:19', 1),
(60, 'do-not-reply@sibme.com', 'test@test.com', NULL, NULL, 'You''ve been invited to join ''test s'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/0e0c6bjo55pblydkm0ux/31839b036f63806cba3f47b93af8ccb5">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-30 07:09:04', 1),
(61, 'do-not-reply@sibme.com', 'k@k.com', NULL, NULL, 'You''ve been invited to join ''k@k.com s'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/il06rv1nnq2e6wtng970/f0adc8838f4bdedde4ec2cfad0515589">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-30 07:19:52', 1),
(62, 'do-not-reply@sibme.com', 'k@kk.com', NULL, NULL, 'You''ve been invited to join ''k@k.com s'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/gobn7bxfhr64dotdh9m5/3b5dca501ee1e6d8cd7b905f4e1bf723">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-30 07:34:46', 1),
(63, 'do-not-reply@sibme.com', 't@t.com', NULL, NULL, 'You''ve been invited to join ''t@t.com s'' Sibme Account!', '<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/14vz5a4n2e1viez3irlf/e2a2dcc36a08a345332c751b2f2e476c">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-08-30 07:44:19', 1),
(64, 'do-not-reply@sibme.com', 'khurri.saleem@gmail.com', NULL, NULL, 'Welcome to Sibme!', '<div id=":6r" style="overflow: hidden;">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href="#" target="_blank">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class="il">Sibme</span> as much as you would like.  We know it''s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href="mailto:info@sibme.com" target="_blank">info@<span class="il">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class="il">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href="http://localhost/sibme/app//Users/login" target="_blank">http://localhost/sibme/app/<span class="il">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> khurri    </p>\n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-09-01 03:53:39', 3),
(65, 'do-not-reply@sibme.com', 'khurri.saleem@gmail.com', NULL, NULL, 'Welcome to Sibme!', '<div id=":6r" style="overflow: hidden;">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href="#" target="_blank">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class="il">Sibme</span> as much as you would like.  We know it''s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href="mailto:info@sibme.com" target="_blank">info@<span class="il">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class="il">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href="http://localhost/sibme/app//Users/login" target="_blank">http://localhost/sibme/app/<span class="il">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> khurri    </p>\n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-09-01 04:03:22', 4),
(66, 'do-not-reply@sibme.com', 'khurri.saleem@gmail.com', NULL, NULL, 'Welcome to Sibme!', '<div id=":6r" style="overflow: hidden;">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href="#" target="_blank">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class="il">Sibme</span> as much as you would like.  We know it''s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href="mailto:info@sibme.com" target="_blank">info@<span class="il">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class="il">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href="http://localhost/sibme/app//Users/login" target="_blank">http://localhost/sibme/app/<span class="il">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> khurri3    </p>\n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-09-02 12:34:03', 8),
(67, 'do-not-reply@sibme.com', 'khurri.saleem@gmail.com', NULL, NULL, 'Khurram Saleem created a new Sibme Huddle: My Huddle', '<div id=":6r" style="overflow: hidden;">\n    <p>Khurram Saleem invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style="color:#3d9bc2">My Huddle</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href="http://localhost/sibme/app/">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram Saleem''s message below:</p>\n    <p>Hello World</p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram Saleem</strong> at <a href="mailto:khurri.saleem@gmail.com">khurri.saleem@gmail.com</a></p>\n    \n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-09-02 12:34:46', 8),
(68, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Welcome to Sibme!', '<div id=":6r" style="overflow: hidden;">\n    <p>Hi Mahwash Khurram, thanks for signing up for your free 60 day trial!</p>\n\n    <br/>\n\n    <a href="http://localhost/sibme/app//accounts/accountSettings/1/3">Subscribe Now</a><br/>\n\n    <p>Please feel free to use <span class="il">Sibme</span> as much as you would like.  We know it''s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href="mailto:info@sibme.com" target="_blank">info@<span class="il">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class="il">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href="http://localhost/sibme/app//Users/login" target="_blank">http://localhost/sibme/app/<span class="il">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> mishi    </p>\n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-09-04 06:30:17', 10),
(69, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Welcome to Sibme!', '<div id=":6r" style="overflow: hidden;">\n    <p>Hi Mahwash Khurram, thanks for signing up for your free 60 day trial!</p>\n\n    <br/>\n\n    <a href="http://localhost.sibme.com/accounts/accountSettings/1/3">Subscribe Now</a><br/>\n\n    <p>Please feel free to use <span class="il">Sibme</span> as much as you would like.  We know it''s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href="mailto:info@sibme.com" target="_blank">info@<span class="il">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class="il">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href="http://localhost.sibme.com/Users/login" target="_blank">http://localhost.sibme.com/<span class="il">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> mishi    </p>\n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-09-04 06:34:19', 11),
(70, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Mahwash Khurram created a new Sibme Huddle: Khurram''s Coaching classes', '<div id=":6r" style="overflow: hidden;">\n    <p>Mahwash Khurram invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style="color:#3d9bc2">Khurram''s Coaching classes</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href="http://localhost.sibme.com/app/">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Mahwash Khurram''s message below:</p>\n    <p>Hello People</p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Mahwash Khurram</strong> at <a href="mailto:khurram@makeityourweb.com">khurram@makeityourweb.com</a></p>\n    \n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-09-04 06:52:23', 11),
(71, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram Saleems'' Sibme Account!', '<h4><strong>Mahwash Khurram</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/az6nvq490ndvwj7pis1k/fa3a3c407f82377f55c19c5d403335c7">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Mahwash Khurram</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Mahwash Khurram</strong> at khurram@makeityourweb.com</p>\n', '1', NULL, '2013-09-04 07:34:58', 11),
(72, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram JJs'' Sibme Account!', '<h4><strong>Mahwash Khurram</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/il4uedqviay7z4xx2s80/c2626d850c80ea07e7511bbae4c76f4b">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Mahwash Khurram</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Mahwash Khurram</strong> at khurram@makeityourweb.com</p>\n', '1', NULL, '2013-09-04 07:42:04', 11),
(73, 'do-not-reply@sibme.com', 'info@3ssolutions.net', NULL, NULL, 'You''ve been invited to join ''Khurram 3Ss'' Sibme Account!', '<h4><strong>Mahwash Khurram</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/1ki7lljstb5j8fd2g5dn/ce78d1da254c0843eb23951ae077ff5f">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Mahwash Khurram</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Mahwash Khurram</strong> at khurram@makeityourweb.com</p>\n', '1', NULL, '2013-09-04 07:42:36', 11),
(74, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram JJs'' Sibme Account!', '<h4><strong>Mahwash Khurram</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/8e82ab7243b7c66d768f1b8ce1c967eb/8e82ab7243b7c66d768f1b8ce1c967eb">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Mahwash Khurram</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Mahwash Khurram</strong> at khurram@makeityourweb.com</p>\n', '1', NULL, '2013-09-04 07:59:03', 11),
(75, 'do-not-reply@sibme.com', 'info@3ssolutions.net', NULL, NULL, 'You''ve been invited to join ''Khurram 3Ss'' Sibme Account!', '<h4><strong>Mahwash Khurram</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/e0ec453e28e061cc58ac43f91dc2f3f0/e0ec453e28e061cc58ac43f91dc2f3f0">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Mahwash Khurram</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Mahwash Khurram</strong> at khurram@makeityourweb.com</p>\n', '1', NULL, '2013-09-04 07:59:22', 11),
(76, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'You''ve been invited to join ''Khurram JJs'' Sibme Account!', '<h4><strong>Mahwash Khurram</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/7250eb93b3c18cc9daa29cf58af7a004/7250eb93b3c18cc9daa29cf58af7a004">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Mahwash Khurram</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Mahwash Khurram</strong> at khurram@makeityourweb.com</p>\n', '1', NULL, '2013-09-04 08:19:10', 11),
(77, 'do-not-reply@sibme.com', 'info@3ssolutions.net', NULL, NULL, 'You''ve been invited to join ''Khurram 3Ss'' Sibme Account!', '<h4><strong>Mahwash Khurram</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://localhost.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/013a006f03dbc5392effeb8f18fda755/013a006f03dbc5392effeb8f18fda755">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Mahwash Khurram</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Mahwash Khurram</strong> at khurram@makeityourweb.com</p>\n', '1', NULL, '2013-09-04 08:19:27', 11),
(78, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com', NULL, NULL, 'Mahwash Khurram created a new Sibme Huddle: Khurram''s Coaching classes', '<div id=":6r" style="overflow: hidden;">\n    <p>Mahwash Khurram invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style="color:#3d9bc2">Khurram''s Coaching classes</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href="http://localhost.sibme.com/app/">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Mahwash Khurram''s message below:</p>\n    <p>Hello World! welcome to sibme 3 users</p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Mahwash Khurram</strong> at <a href="mailto:khurram@makeityourweb.com">khurram@makeityourweb.com</a></p>\n    \n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-09-04 08:19:49', 11),
(79, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Welcome to Sibme!', '<div id=":6r" style="overflow: hidden;">\n    <p>Hi Khurram JJ, thanks for signing up for your free 60 day trial!</p>\n\n    <br/>\n\n    <a href="http://localhost.sibme.com/app/accounts/accountSettings/1/3">Subscribe Now</a><br/>\n\n    <p>Please feel free to use <span class="il">Sibme</span> as much as you would like.  We know it''s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href="mailto:info@sibme.com" target="_blank">info@<span class="il">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class="il">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href="http://localhost.sibme.com/app/Users/login" target="_blank">http://localhost.sibme.com/app/<span class="il">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> kjj    </p>\n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-09-04 08:41:13', 11),
(80, 'do-not-reply@sibme.com', 'khurram@jjtestsite.us', NULL, NULL, 'Huddle Discussions', 'Testing 123', '1', NULL, '2013-09-04 09:29:20', 11),
(81, 'do-not-reply@sibme.com', 'hamidbscs8@gmail.com', NULL, NULL, 'You''ve been invited to join ''Muha s'' Sibme Account!', '<h4><strong>Khurrams Saleems</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://qa.sibme.com/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/301ad0e3bd5cb1627a2044908a42fdc2/301ad0e3bd5cb1627a2044908a42fdc2">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurrams Saleems</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurrams Saleems</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-09-10 03:34:17', 1),
(82, 'do-not-reply@sibme.com', 'khurri.saleem@gmail.com', NULL, NULL, 'Khurrams Saleems created a new Sibme Huddle: New Huddle added by hamid', '<div id=":6r" style="overflow: hidden;">\n    <p>Khurrams Saleems invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style="color:#3d9bc2">New Huddle added by hamid</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href="http://qa.sibme.com/app/">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurrams Saleems''s message below:</p>\n    <p>Mrs. Smith demonstrates how to execute a Socratic seminar</p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurrams Saleems</strong> at <a href="mailto:khurri.saleem@gmail.com">khurri.saleem@gmail.com</a></p>\n    \n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-09-10 09:09:17', 1),
(83, 'do-not-reply@sibme.com', 'khurri.saleem@gmail.com', NULL, NULL, 'Khurrams Saleems created a new Sibme Huddle: Test with Hamid', '<div id=":6r" style="overflow: hidden;">\n    <p>Khurrams Saleems invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style="color:#3d9bc2">Test with Hamid</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href="http://qa.sibme.com/app/">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurrams Saleems''s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurrams Saleems</strong> at <a href="mailto:khurri.saleem@gmail.com">khurri.saleem@gmail.com</a></p>\n    \n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-09-10 10:13:10', 1),
(84, 'do-not-reply@sibme.com', 'khurri.saleem@gmail.com', NULL, NULL, 'Khurrams Saleems created a new Sibme Huddle: Test with Hamid', '<div id=":6r" style="overflow: hidden;">\n    <p>Khurrams Saleems invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style="color:#3d9bc2">Test with Hamid</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href="http://qa.sibme.com/app/">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurrams Saleems''s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurrams Saleems</strong> at <a href="mailto:khurri.saleem@gmail.com">khurri.saleem@gmail.com</a></p>\n    \n    <div class="yj6qo"></div>\n    <div class="adL">\n    </div>\n\n</div>', '1', NULL, '2013-09-10 10:14:03', 1),
(85, 'do-not-reply@sibme.com', 'tanvair@gmail.com', NULL, NULL, 'You''ve been invited to join ''Muhammad hamids'' Sibme Account!', '<h4><strong>Khurrams Saleems</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://qa.sibme.com/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/4d5b995358e7798bc7e9d9db83c612a5/4d5b995358e7798bc7e9d9db83c612a5">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurrams Saleems</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurrams Saleems</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-09-10 10:23:03', 1),
(86, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com.pk', NULL, NULL, 'Huddle Discussions', 'test', '1', NULL, '2013-09-10 10:45:27', 1),
(87, 'do-not-reply@sibme.com', 'hamidbscs8@gmail.com', NULL, NULL, 'Huddle Discussions', 'test', '1', NULL, '2013-09-10 10:45:31', 1),
(88, 'do-not-reply@sibme.com', 'khurram@jjtestsite.pk', NULL, NULL, 'Huddle Discussions', 'test', '1', NULL, '2013-09-10 10:45:35', 1),
(89, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com.pk', NULL, NULL, 'Huddle Discussions', 'test', '1', NULL, '2013-09-10 10:45:39', 1),
(90, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com.pk', NULL, NULL, 'Huddle Discussions', 'test', '1', NULL, '2013-09-10 10:47:11', 1),
(91, 'do-not-reply@sibme.com', 'hamidbscs8@gmail.com', NULL, NULL, 'Huddle Discussions', 'test', '1', NULL, '2013-09-10 10:47:15', 1),
(92, 'do-not-reply@sibme.com', 'khurram@jjtestsite.pk', NULL, NULL, 'Huddle Discussions', 'test', '1', NULL, '2013-09-10 10:47:19', 1),
(93, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com.pk', NULL, NULL, 'Huddle Discussions', 'test', '1', NULL, '2013-09-10 10:47:23', 1),
(94, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com.pk', NULL, NULL, 'Huddle Discussions', 'fadgasdgadg', '1', NULL, '2013-09-10 10:48:14', 1),
(95, 'do-not-reply@sibme.com', 'hamidbscs8@gmail.com', NULL, NULL, 'Huddle Discussions', 'fadgasdgadg', '1', NULL, '2013-09-10 10:48:18', 1),
(96, 'do-not-reply@sibme.com', 'khurram@jjtestsite.pk', NULL, NULL, 'Huddle Discussions', 'fadgasdgadg', '1', NULL, '2013-09-10 10:48:22', 1),
(97, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com.pk', NULL, NULL, 'Huddle Discussions', 'fadgasdgadg', '1', NULL, '2013-09-10 10:48:26', 1),
(98, 'do-not-reply@sibme.com', 'haroon@gmail.com', NULL, NULL, 'You''ve been invited to join ''Haroon Rasheeds'' Sibme Account!', '<h4><strong>Khurrams Saleems</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://qa.sibme.com/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/ab88b15733f543179858600245108dd8/ab88b15733f543179858600245108dd8">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurrams Saleems</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurrams Saleems</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-09-11 05:15:54', 1),
(99, 'do-not-reply@sibme.com', 'Ayaan@gmail.com', NULL, NULL, 'You''ve been invited to join ''Ayaan s'' Sibme Account!', '<h4><strong>Khurrams Saleems</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://qa.sibme.com/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/b0b183c207f46f0cca7dc63b2604f5cc/b0b183c207f46f0cca7dc63b2604f5cc">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurrams Saleems</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurrams Saleems</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-09-11 05:17:56', 1),
(100, 'do-not-reply@sibme.com', 'tanvair12@gmail.com', NULL, NULL, 'You''ve been invited to join ''admin Userss'' Sibme Account!', '<h4><strong>Khurrams Saleems</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://qa.sibme.com/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/f9028faec74be6ec9b852b0a542e2f39/f9028faec74be6ec9b852b0a542e2f39">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurrams Saleems</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurrams Saleems</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-09-11 05:18:46', 1),
(101, 'do-not-reply@sibme.com', 'findUser@gmail.com', NULL, NULL, 'You''ve been invited to join ''hamid s'' Sibme Account!', '<h4><strong>Khurrams Saleems</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style="color:#3d9bc2;">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href="http://qa.sibme.com/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/8f7d807e1f53eff5f9efbe5cb81090fb/8f7d807e1f53eff5f9efbe5cb81090fb">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurrams Saleems</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurrams Saleems</strong> at khurri.saleem@gmail.com</p>\n', '1', NULL, '2013-09-11 05:19:41', 1),
(102, 'do-not-reply@sibme.com', 'khurram@makeityourweb.com.pk', NULL, NULL, 'Huddle Discussions', 'asdASDASD', '1', NULL, '2013-09-11 07:18:42', 1);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_comment_id` int(11) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `ref_type` tinyint(4) NOT NULL COMMENT '1=huddle(folder),2=document,3=comment',
  `ref_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `time` int(11) DEFAULT NULL,
  `restrict_to_users` bit(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_comments_on_commentable_id` (`ref_id`),
  KEY `index_comments_on_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `parent_comment_id`, `title`, `comment`, `ref_type`, `ref_id`, `user_id`, `time`, `restrict_to_users`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`) VALUES
(2, NULL, NULL, 'Testing', 2, 27, 1, 2, '0', 1, '2013-08-25 14:53:02', 1, '2013-08-25 14:53:02'),
(3, NULL, NULL, 'Testing', 2, 65, 1, 0, '0', 1, '2013-09-02 08:25:42', 1, '2013-09-02 08:25:42'),
(4, NULL, 'asdasd', 'asdasdd', 1, 20, 823, NULL, '0', 823, '2013-09-02 12:55:06', 823, '2013-09-02 12:55:06'),
(5, NULL, 'My Test Discussion', 'Testing 123', 1, 22, 826, NULL, '0', 826, '2013-09-04 09:29:18', 826, '2013-09-04 09:29:18'),
(6, NULL, 'test', 'test', 1, 25, 1, NULL, '0', 1, '2013-09-10 10:45:27', 1, '2013-09-10 10:45:27'),
(7, NULL, 'test', 'test', 1, 25, 1, NULL, '0', 1, '2013-09-10 10:47:11', 1, '2013-09-10 10:47:11'),
(8, NULL, 'dfg', 'fadgasdgadg', 1, 26, 1, NULL, '0', 1, '2013-09-10 10:48:14', 1, '2013-09-10 10:48:14'),
(9, NULL, '', 'asdASDASD', 1, 8, 1, NULL, '0', 1, '2013-09-11 07:18:42', 1, '2013-09-11 07:18:42'),
(10, NULL, '', 'dsafsdfasdfdsfasfd', 1, 8, 1, NULL, '0', 1, '2013-09-11 07:27:42', 1, '2013-09-11 07:27:42'),
(11, NULL, '', 'dsafsdfasdfdsfasfd', 1, 8, 1, NULL, '0', 1, '2013-09-11 07:28:20', 1, '2013-09-11 07:28:20'),
(12, NULL, '', 'dsfasdfasdfsdfasfdafasdf', 1, 8, 1, NULL, '0', 1, '2013-09-11 07:32:42', 1, '2013-09-11 07:32:42');

-- --------------------------------------------------------

--
-- Table structure for table `comment_attachments`
--

CREATE TABLE IF NOT EXISTS `comment_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `comment_attachments`
--

INSERT INTO `comment_attachments` (`id`, `comment_id`, `document_id`) VALUES
(1, 15, 48),
(2, 17, 49),
(3, 18, 50),
(4, 19, 51),
(5, 20, 52),
(6, 21, 53),
(7, 22, 54),
(8, 23, 55),
(9, 27, 56),
(10, 29, 57),
(11, 30, 58),
(12, 31, 59),
(13, 32, 60),
(14, 33, 61),
(15, 34, 62),
(16, 5, 73);

-- --------------------------------------------------------

--
-- Table structure for table `comment_users`
--

CREATE TABLE IF NOT EXISTS `comment_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) NOT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `comment_user_type` tinyint(4) DEFAULT NULL COMMENT '1=notification,2=recipient',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=108 ;

--
-- Dumping data for table `comment_users`
--

INSERT INTO `comment_users` (`id`, `comment_id`, `user_id`, `comment_user_type`) VALUES
(83, 26, '814', 1),
(84, 27, '3', 1),
(85, 27, '814', 1),
(86, 29, '814', 1),
(87, 31, '814', 1),
(88, 32, '3', 1),
(89, 32, '814', 1),
(90, 33, '3', 1),
(91, 33, '814', 1),
(92, 34, '3', 1),
(93, 34, '814', 1),
(94, 5, '832', 1),
(95, 6, '3', 1),
(96, 6, '834', 1),
(97, 6, '814', 1),
(98, 6, '3', 1),
(99, 7, '3', 1),
(100, 7, '834', 1),
(101, 7, '814', 1),
(102, 7, '3', 1),
(103, 8, '3', 1),
(104, 8, '834', 1),
(105, 8, '814', 1),
(106, 8, '3', 1),
(107, 9, '3', 1);

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE IF NOT EXISTS `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `doc_type` tinyint(3) NOT NULL COMMENT '1=video, 2=document',
  `url` varchar(4000) DEFAULT NULL,
  `original_file_name` varchar(1024) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  `content_type` varchar(128) DEFAULT NULL,
  `published` int(11) NOT NULL DEFAULT '0',
  `job_id` int(11) DEFAULT NULL,
  `zencoder_output_id` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL DEFAULT b'0',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=83 ;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `account_id`, `doc_type`, `url`, `original_file_name`, `file_size`, `view_count`, `content_type`, `published`, `job_id`, `zencoder_output_id`, `active`, `created_date`, `created_by`, `last_edit_date`, `last_edit_by`) VALUES
(27, 1, 1, '1/7/2013/08/25/kupload.mov', 'kupload.mov', 623275, 48, NULL, 1, NULL, '56706763', '1', '2013-08-25 08:53:24', 1, '2013-08-25 08:53:24', 1),
(28, 1, 2, '1/7/2013/08/25/logo-1.jpg', 'logo-1.jpg', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-25 04:39:00', 1, '2013-08-25 04:39:00', 1),
(29, 1, 2, '1/7/2013/08/25/logo-coach.png', 'logo-coach.png', 22952, 0, NULL, 1, NULL, NULL, '1', '2013-08-25 04:40:39', 1, '2013-08-25 04:40:39', 1),
(32, 1, 2, '1/7/2013/08/26/sibmeqasites.xml', 'sibmeqa_sites.xml', 785, 0, NULL, 1, NULL, NULL, '1', '2013-08-26 05:17:41', 1, '2013-08-26 05:17:41', 1),
(33, 1, 1, '1/12/2013/08/28/kupload.mov', 'kupload.mov', 623275, 0, NULL, 1, NULL, '56989570', '1', '2013-08-28 06:12:29', 1, '2013-08-28 06:12:29', 1),
(34, 1, 1, NULL, 'kupload.mov', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-28 03:28:36', 1, '2013-08-28 03:28:36', 1),
(35, 1, 1, 'tempupload/1/2013/08/28/yRDWuPCaQ2eKBbPCrwqX_kupload.mov', 'kupload.mov', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-28 03:31:24', 1, '2013-08-28 03:31:24', 1),
(36, 1, 1, 'tempupload/1/2013/08/28/7MtAGuRnQK1dtPN1ItAB_kupload.mov', 'kupload.mov', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-28 03:33:50', 1, '2013-08-28 03:33:50', 1),
(37, 1, 1, 'tempupload/1/2013/08/28/LCZZVJpyRAalTa5SVgM3_kupload.mov', 'kupload.mov', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-28 03:37:41', 1, '2013-08-28 03:37:41', 1),
(38, 1, 1, '1/17/2013/08/28/HDzqUThT3eWLLsuz2W87_kupload.mov', 'kupload.mov', 623275, 6, NULL, 1, NULL, '57036379', '1', '2013-08-28 03:40:28', 1, '2013-08-28 03:40:28', 1),
(39, 1, 1, '1//2013/09/02/A7RaBAvmQIadzvVIDTJE_Movieon42213at420AMcopy.mov', 'Movieon42213at420AMcopy.mov', 623275, 20, NULL, 1, NULL, '57420306', '1', '2013-08-28 04:29:37', 1, '2013-09-02 06:00:40', 1),
(40, 1, 1, '1/7/2013/08/28/Wi5UVDKjSf2bRHC0Kfpz_kupload.mov', 'kupload.mov', 623275, 4, NULL, 1, NULL, '57047611', '1', '2013-08-28 05:35:29', 1, '2013-08-28 05:35:29', 1),
(41, 1, 1, '1/7/2013/08/28/Wx3ei1oDQMqzz9A4GXDS_kupload.mov', 'kupload.mov', 623275, 0, NULL, 1, NULL, '57048801', '1', '2013-08-28 05:50:03', 1, '2013-08-28 05:50:03', 1),
(42, 1, 2, '1/7/2013/08/28/tempupload/1/2013/08/28/P7kAyinQVirjlJRAYs1w_100.txt', 'tempupload/1/2013/08/28/P7kAyinQVirjlJRAYs1w_100.txt', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-28 06:00:42', 1, '2013-08-28 06:00:42', 1),
(43, 1, 2, '1/7/2013/08/28/nrs2olaIQpCb2KYXzcCy_100.txt', 'tempupload/1/2013/08/28/nrs2olaIQpCb2KYXzcCy_100.txt', 53536, 0, NULL, 1, NULL, NULL, '1', '2013-08-28 06:05:37', 1, '2013-08-28 06:05:37', 1),
(44, 1, 1, '1/7/2013/08/28/bOH76HyOSiq3TShf6NFE_100.txt', 'bOH76HyOSiq3TShf6NFE_100.txt', 53536, 1, NULL, 1, NULL, '57050432', '1', '2013-08-28 06:09:33', 1, '2013-08-28 06:09:33', 1),
(45, 1, 1, '1/7/2013/08/28/qw7lwbLlTwKHDCDnfBNc_kupload.mov', 'kupload.mov', 623275, 3, NULL, 1, NULL, '57050566', '1', '2013-08-28 06:11:19', 1, '2013-08-28 06:11:19', 1),
(46, 1, 2, '1/7/2013/08/28/St1t0KGWQyO0UCtbTPbg_100.txt', '100.txt', 53536, 0, NULL, 1, NULL, NULL, '1', '2013-08-28 06:11:54', 1, '2013-08-28 06:11:54', 1),
(47, 1, 1, '1/7/2013/08/29/account-.png', 'account-.png', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 04:23:40', 1, '2013-08-29 04:23:40', 1),
(48, 1, 1, '1/7/2013/08/29/astrosites.xml', 'astrosites.xml', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 04:28:43', 1, '2013-08-29 04:28:43', 1),
(49, 1, 1, '1/7/2013/08/29/image002.jpg', 'image002.jpg', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 04:31:59', 1, '2013-08-29 04:31:59', 1),
(50, 1, 1, '1/7/2013/08/29/frf-utah-dns.png', 'frf-utah-dns.png', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 04:41:20', 1, '2013-08-29 04:41:20', 1),
(51, 1, 1, '1/7/2013/08/29/frf-utah-dns.png', 'frf-utah-dns.png', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 04:41:57', 1, '2013-08-29 04:41:57', 1),
(52, 1, 1, '1/7/2013/08/29/frf-utah-dns.png', 'frf-utah-dns.png', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 04:42:50', 1, '2013-08-29 04:42:50', 1),
(53, 1, 1, '1/7/2013/08/29/frf-utah-dns.png', 'frf-utah-dns.png', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 04:43:24', 1, '2013-08-29 04:43:24', 1),
(54, 1, 1, '1/7/2013/08/29/frf-utah-dns.png', 'frf-utah-dns.png', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 04:44:27', 1, '2013-08-29 04:44:27', 1),
(55, 1, 1, '1/7/2013/08/29/frf-utah-dns.png', 'frf-utah-dns.png', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 04:44:50', 1, '2013-08-29 04:44:50', 1),
(56, 1, 1, '1/7/2013/08/29/image002.jpg', 'image002.jpg', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 05:24:05', 1, '2013-08-29 05:24:05', 1),
(57, 1, 1, '1/7/2013/08/29/image002.jpg', 'image002.jpg', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 05:41:49', 1, '2013-08-29 05:41:49', 1),
(58, 1, 1, '1/7/2013/08/29/image003.jpg', 'image003.jpg', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 05:45:13', 1, '2013-08-29 05:45:13', 1),
(59, 1, 1, '1/7/2013/08/29/frf-utah-dns.png', 'frf-utah-dns.png', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 05:48:59', 1, '2013-08-29 05:48:59', 1),
(60, 1, 1, '1/7/2013/08/29/donate-image.png', 'donate-image.png', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 05:50:43', 1, '2013-08-29 05:50:43', 1),
(61, 1, 1, '1/7/2013/08/29/donate-image.png', 'donate-image.png', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 05:51:58', 1, '2013-08-29 05:51:58', 1),
(62, 1, 1, '1/7/2013/08/29/donate-image.png', 'donate-image.png', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 05:52:27', 1, '2013-08-29 05:52:27', 1),
(63, 1, 2, '1/7/2013/08/29/Oe5vXbVWTEqyctByyBgY_Specialty products-variations export-convex glass.csv', 'Specialty products-variations export-convex glass.csv', 6023, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 09:04:40', 1, '2013-08-29 09:04:40', 1),
(64, 1, 2, '1/7/2013/08/29/kQEYsophT8mTGylBgNQn_Specialty products-variations export-convex glass.csv', 'Specialty products-variations export-convex glass.csv', 6023, 0, NULL, 1, NULL, NULL, '1', '2013-08-29 09:04:58', 1, '2013-08-29 09:04:58', 1),
(65, 1, 1, '1/7/2013/08/29/0AGUmuNQRH1iTkmodVCe_kupload.mov', 'kupload.mov', 623275, 8, NULL, 1, NULL, '57153391', '1', '2013-08-29 09:05:26', 1, '2013-08-29 09:05:26', 1),
(66, 1, 2, '1/7/2013/08/30/957g6tQmRQG3KlO8FO6B_ioncoachselected.jpg', 'ioncoachselected.jpg', 30005, 0, NULL, 1, NULL, NULL, '1', '2013-08-30 07:00:50', 1, '2013-08-30 07:00:50', 1),
(67, 1, 2, '1/7/2013/08/30/nMlkUOnTEWGlulJq4QRc_ioncoachselected.jpg', 'ioncoachselected.jpg', 30005, 0, NULL, 1, NULL, NULL, '1', '2013-08-30 07:02:21', 1, '2013-08-30 07:02:21', 1),
(68, 1, 1, '1/19/2013/09/01/6Lywf3x9SK7jKYIXCODu_kupload.mov', 'kupload.mov', 623275, 19, NULL, 1, NULL, '57397463', '1', '2013-09-01 08:32:42', 1, '2013-09-01 08:32:42', 1),
(69, 1, 2, '1/19/2013/09/02/FeiBbet6SiirP1HEjhJF_100.txt', '100.txt', 53536, 0, NULL, 1, NULL, NULL, '1', '2013-09-02 12:03:53', 1, '2013-09-02 12:03:53', 1),
(70, 1, 2, '1/18/2013/09/02/4zqLFIBBR0axKU2ChVpB_InLine Frames Price Export from BOM.TXT', 'InLine Frames Price Export from BOM.TXT', 36392, 0, NULL, 1, NULL, NULL, '1', '2013-09-02 05:41:30', 1, '2013-09-02 05:41:30', 1),
(72, 8, 2, '8/20/2013/09/02/uTqiDnqPR8Cnxqm8SNcA_Specialty products-variations export-convex glass.csv', 'Specialty products-variations export-convex glass.csv', 6023, 0, NULL, 1, NULL, NULL, '1', '2013-09-02 12:36:24', 823, '2013-09-02 12:36:24', 823),
(73, 11, 1, '11/22/2013/09/04/authorizationletter.docx', 'authorizationletter.docx', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-09-04 09:29:20', 826, '2013-09-04 09:29:20', 826),
(74, 11, 1, '11/22/2013/09/04/v4F5f9CcSqGHx2P6kmof_kupload.mov', 'kupload.mov', 623275, 1, NULL, 1, NULL, '57612221', '1', '2013-09-04 09:38:48', 826, '2013-09-04 09:38:48', 826),
(75, 11, 1, '11/23/2013/09/04/RysN5CRQsuyNsAlCPzoD_kupload.mov', 'kupload.mov', 623275, 6, NULL, 1, NULL, '57612718', '1', '2013-09-04 09:44:42', 826, '2013-09-04 09:44:42', 826),
(76, 11, 1, '11/22/2013/09/04/15K8ovX2S7S7gDzaOgfH_kupload.mov', 'kupload.mov', 623275, 1, NULL, 1, NULL, '57613348', '1', '2013-09-04 09:51:41', 826, '2013-09-04 09:51:41', 826),
(77, 1, 1, 'tempupload/1/2013/09/10/C8XS5bXSTie8OCNpclxD_Akshay kumar song Mere Saath Chalte Chalte -indian songs.flv - YouTube.mp4', 'Akshay kumar song Mere Saath Chalte Chalte -indian songs.flv - YouTube.mp4', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-09-10 10:34:33', 1, '2013-09-10 10:34:33', 1),
(78, 1, 1, 'tempupload/1/2013/09/10/C8XS5bXSTie8OCNpclxD_Akshay kumar song Mere Saath Chalte Chalte -indian songs.flv - YouTube.mp4', 'Akshay kumar song Mere Saath Chalte Chalte -indian songs.flv - YouTube.mp4', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-09-10 10:35:23', 1, '2013-09-10 10:35:23', 1),
(79, 1, 2, '1/25/2013/09/10/46SXvyCKRg2zTiOpq3v4__Ishq Sufiyana Song from The Dirty Picture_ Ft. Emraan Hashmi, Vidya Balan - YouTube_2 - Copy.MP4', '_Ishq Sufiyana Song from The Dirty Picture_ Ft. Emraan Hashmi, Vidya Balan - YouTube_2 - Copy.MP4', 81094844, 0, NULL, 1, NULL, NULL, '1', '2013-09-10 10:39:01', 1, '2013-09-10 10:39:01', 1),
(80, 1, 2, '1/25/2013/09/10/VmMvcQTjSa2R2KzBDBGQ_test.jpg', 'test.jpg', 31038, 0, NULL, 1, NULL, NULL, '1', '2013-09-10 10:40:56', 1, '2013-09-10 10:40:56', 1),
(81, 1, 2, '1/26/2013/09/11/JxFM0d63Tgec8mMhlJeL__Ishq Sufiyana Song from The Dirty Picture_ Ft. Emraan Hashmi, Vidya Balan - YouTube_2 - Copy.MP4', '_Ishq Sufiyana Song from The Dirty Picture_ Ft. Emraan Hashmi, Vidya Balan - YouTube_2 - Copy.MP4', 81094844, 0, NULL, 1, NULL, NULL, '1', '2013-09-11 05:28:36', 1, '2013-09-11 05:28:36', 1),
(82, 1, 1, 'tempupload/1/2013/09/11/6vUvknOETHiKDKdkmhow__Ooh La La Tu Hai Meri Fantasy_ Song_ The Dirty Picture Feat. Vidya Balan - YouTube.MP4', '_Ooh La La Tu Hai Meri Fantasy_ Song_ The Dirty Picture Feat. Vidya Balan - YouTube.MP4', NULL, 0, NULL, 1, NULL, NULL, '1', '2013-09-11 05:48:13', 1, '2013-09-11 05:48:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `document_meta_data`
--

CREATE TABLE IF NOT EXISTS `document_meta_data` (
  `document_meta_data_id` int(11) NOT NULL,
  `meta_data_name` varchar(64) NOT NULL COMMENT 'tag, topic, subject, etc',
  `meta_data_value` varchar(255) NOT NULL,
  PRIMARY KEY (`document_meta_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_groups_on_account_id` (`account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `account_id`, `name`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`) VALUES
(1, 1, 'Math Group', 1, '2013-12-12 00:00:00', 1, '2013-12-12 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE IF NOT EXISTS `plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_type` varchar(255) DEFAULT NULL,
  `storage` bigint(20) DEFAULT NULL,
  `validity` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `users` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `per_year` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `plan_type`, `storage`, `validity`, `created_at`, `updated_at`, `users`, `price`, `per_year`) VALUES
(1, 'Department', 5, '60 Days', '2012-10-08 11:57:52', '2013-02-28 12:37:38', 10, 54, 0),
(2, 'School', 20, '89 Days', '2012-10-08 11:58:28', '2013-02-28 12:39:23', 40, 119, 0),
(3, 'Large School', 50, '895 Days', '2012-10-22 11:57:23', '2013-02-28 12:41:20', 150, 239, 0),
(4, 'Institution', 100, NULL, '2012-11-08 11:57:45', '2013-02-28 12:42:40', 500, 399, 0),
(5, 'Department', 5, NULL, '2013-03-01 10:41:29', '2013-03-01 10:41:29', 10, 588, 1),
(6, 'School', 20, NULL, '2013-03-01 10:41:29', '2013-03-01 10:41:29', 40, 1308, 1),
(7, 'Large School', 50, NULL, '2013-03-01 10:41:29', '2013-03-01 10:41:29', 150, 2628, 1),
(8, 'Institution', 100, NULL, '2013-03-01 10:41:29', '2013-03-01 10:41:29', 500, 4368, 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `role_id` int(11) NOT NULL,
  `role_type` tinyint(4) NOT NULL COMMENT '1=account, 2=huddle',
  `name` varchar(64) NOT NULL,
  `desc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `role_type`, `name`, `desc`) VALUES
(100, 1, 'Account Owner', NULL),
(110, 1, 'Super User', NULL),
(120, 1, 'User', NULL),
(200, 2, 'Admin', NULL),
(210, 2, 'User', NULL),
(220, 2, 'Viewer', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`, `account_id`) VALUES
(1, 'kk', NULL, '2013-08-28 03:55:18', NULL, '2013-08-28 03:55:18', NULL),
(2, 'Test', 1, '2013-08-28 04:22:06', 1, '2013-08-28 04:22:06', 1),
(3, 'Math', 1, '2013-08-28 05:21:55', 1, '2013-08-28 05:21:55', 1),
(4, 'Chemistry', 1, '2013-08-28 05:29:41', 1, '2013-08-28 05:29:41', 1),
(5, 'Math', 826, '2013-09-04 09:44:32', 826, '2013-09-04 09:44:32', 11),
(6, 'Music', 1, '2013-09-10 10:34:16', 1, '2013-09-10 10:34:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`id`, `name`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`, `account_id`) VALUES
(1, 'First Topic', 1, '2013-08-28 05:33:01', 1, '2013-08-28 05:33:01', 1),
(2, 'New Topic', 1, '2013-09-01 20:32:11', 1, '2013-09-01 08:32:11', 1),
(3, 'New Topic', 826, '2013-09-04 09:44:38', 826, '2013-09-04 09:44:38', 11),
(4, 'Songs', 1, '2013-09-10 10:34:28', 1, '2013-09-10 10:34:28', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) DEFAULT NULL,
  `last_sign_in_ip` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `time_zone` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `authentication_token` varchar(255) DEFAULT NULL,
  `email_notification` tinyint(1) DEFAULT '1',
  `is_active` tinyint(1) DEFAULT '1',
  `type` enum('Invite_Sent','Active') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=843 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `reset_password_token`, `reset_password_sent_at`, `remember_created_at`, `sign_in_count`, `current_sign_in_at`, `last_sign_in_at`, `current_sign_in_ip`, `last_sign_in_ip`, `first_name`, `last_name`, `title`, `phone`, `time_zone`, `image`, `file_size`, `authentication_token`, `email_notification`, `is_active`, `type`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`) VALUES
(1, 'ksaleem', 'khurri.saleem1@gmail.com', 'e5d0a553689a762c58f72d61fb00fc0ccbf4fb7d', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Khurrams', 'Saleems', 'CEO', '123-123-1234', 'Central Time (US & Canada)', 'logo_coach.png', 22952, 'c4ca4238a0b923820dcc509a6f75849b', 1, 1, 'Active', 0, NULL, 1, '2013-09-11 07:10:08'),
(3, 'ksaleem2', 'khurram@makeityourweb.com.pk', '5662a293b6f5a24f9d4b2ae75a635cdc111eac71', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Khurram', 'Miyw', 'Mr', '123-123-1234', NULL, NULL, NULL, NULL, 1, 1, 'Active', 0, NULL, 0, NULL),
(814, 'ksaleem3', 'khurram@jjtestsite.pk', '5662a293b6f5a24f9d4b2ae75a635cdc111eac71', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Khurram', 'Saleem', NULL, NULL, NULL, 'srch_img.jpg', 41281, 'os5fvbptg6al7xt1vzga', 1, 1, 'Active', 1, '2013-08-23 06:34:03', 1, '2013-08-23 06:33:31'),
(815, '', 'khurram@makeityourweb.com.ca', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Khurram', 'Sal', NULL, NULL, NULL, NULL, NULL, 'uacmswmgz6g9w7oh1da7', 1, 0, 'Invite_Sent', 1, '2013-08-29 08:37:17', 1, '2013-08-29 08:37:17'),
(816, '', 'khurri10@hotmail.com', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'k', 't', NULL, NULL, NULL, NULL, NULL, 's7r1oajebdzu3wq74sme', 1, 0, 'Invite_Sent', 1, '2013-08-29 08:44:19', 1, '2013-08-29 08:44:19'),
(817, '', 'test@test.com', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'test', '', NULL, NULL, NULL, NULL, NULL, '0e0c6bjo55pblydkm0ux', 1, 0, 'Invite_Sent', 1, '2013-08-30 07:09:04', 1, '2013-08-30 07:09:04'),
(818, '', 'k@k.com', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'k@k.com', '', NULL, NULL, NULL, NULL, NULL, 'il06rv1nnq2e6wtng970', 1, 0, 'Invite_Sent', 1, '2013-08-30 07:19:52', 1, '2013-08-30 07:19:52'),
(819, '', 'k@kk.com', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'k@k.com', '', NULL, NULL, NULL, NULL, NULL, 'gobn7bxfhr64dotdh9m5', 1, 0, 'Invite_Sent', 1, '2013-08-30 07:34:46', 1, '2013-08-30 07:34:46'),
(820, '', 't@t.com', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 't@t.com', '', NULL, NULL, NULL, NULL, NULL, '14vz5a4n2e1viez3irlf', 1, 0, 'Invite_Sent', 1, '2013-08-30 07:44:19', 1, '2013-08-30 07:44:19'),
(822, 'khurri', 'khurri.saleem@gmail.com', 'e5d0a553689a762c58f72d61fb00fc0ccbf4fb7d', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Khurram', 'Saleem', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Active', 0, '2013-09-01 04:03:22', 0, NULL),
(823, 'khurri3', 'khurri.saleem@gmail.com', '5662a293b6f5a24f9d4b2ae75a635cdc111eac71', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Khurram', 'Saleem', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 'Active', 0, '2013-09-02 12:34:03', 0, NULL),
(825, 'mishi2', 'khurram@makeityourweb.co', '5662a293b6f5a24f9d4b2ae75a635cdc111eac71', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Mahwash', 'Khurram', NULL, NULL, NULL, NULL, NULL, 'd554f7bb7be44a7267068a7df88ddd20', 1, 1, 'Active', 0, '2013-09-04 06:30:17', 0, NULL),
(826, 'mishi', 'khurram@makeityourweb.com', '5662a293b6f5a24f9d4b2ae75a635cdc111eac71', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Mahwash', 'Khurram', NULL, NULL, NULL, NULL, NULL, '795c7a7a5ec6b460ec00c5841019b9e9', 1, 1, 'Active', 0, '2013-09-04 06:34:19', 0, NULL),
(832, 'kjj', 'khurram@jjtestsite.us', '5662a293b6f5a24f9d4b2ae75a635cdc111eac71', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Khurram', 'JJ', NULL, NULL, NULL, '169484_10151074057426722_2142794718_o.jpg', 508179, '7250eb93b3c18cc9daa29cf58af7a004', 1, 1, 'Active', 826, '2013-09-04 08:41:13', 826, '2013-09-04 08:19:10'),
(833, '', 'info@3ssolutions.net', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Khurram', '3S', NULL, NULL, NULL, NULL, NULL, '013a006f03dbc5392effeb8f18fda755', 1, 0, 'Invite_Sent', 826, '2013-09-04 08:19:27', 826, '2013-09-04 08:19:27'),
(834, 'hamidbscs8', 'hamidbscs8@gmail.com', 'e5d0a553689a762c58f72d61fb00fc0ccbf4fb7d', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Muhammad', 'hamid', '', NULL, 'American Samoa', 'test.jpg', 31038, '301ad0e3bd5cb1627a2044908a42fdc2', 1, 1, 'Active', 1, '2013-09-10 05:31:47', 834, '2013-09-10 07:28:26'),
(835, '', 'tanvair@gmail.com', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Muhammad', 'hamid', NULL, NULL, NULL, NULL, NULL, '4d5b995358e7798bc7e9d9db83c612a5', 1, 0, 'Invite_Sent', 1, '2013-09-10 10:23:02', 1, '2013-09-10 10:23:02'),
(836, '', 'haroon@gmail.com', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Haroon', 'Rasheed', NULL, NULL, NULL, NULL, NULL, 'ab88b15733f543179858600245108dd8', 1, 0, 'Invite_Sent', 1, '2013-09-11 05:15:54', 1, '2013-09-11 05:15:54'),
(837, '', 'Ayaan@gmail.com', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Ayaan', '', NULL, NULL, NULL, NULL, NULL, 'b0b183c207f46f0cca7dc63b2604f5cc', 1, 0, 'Invite_Sent', 1, '2013-09-11 05:17:56', 1, '2013-09-11 05:17:56'),
(838, '', 'tanvair12@gmail.com', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'admin', 'Users', NULL, NULL, NULL, NULL, NULL, 'f9028faec74be6ec9b852b0a542e2f39', 1, 0, 'Invite_Sent', 1, '2013-09-11 05:18:46', 1, '2013-09-11 05:18:46'),
(839, '', 'findUser@gmail.com', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'hamid', '', NULL, NULL, NULL, NULL, NULL, '8f7d807e1f53eff5f9efbe5cb81090fb', 1, 0, 'Invite_Sent', 1, '2013-09-11 05:19:41', 1, '2013-09-11 05:19:41'),
(840, '', 'kahsif@gmail.com', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Muhammad', 'hamid', NULL, NULL, NULL, NULL, NULL, 'fa83a11a198d5a7f0bf77a1987bcd006', 1, 0, 'Invite_Sent', 1, '2013-09-11 06:04:23', 1, '2013-09-11 06:04:23'),
(841, '', 'hamid@gmail.com', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Kahsif', '', NULL, NULL, NULL, NULL, NULL, '02a32ad2669e6fe298e607fe7cc0e1a0', 1, 0, 'Invite_Sent', 1, '2013-09-11 06:06:16', 1, '2013-09-11 06:06:16'),
(842, '', 'hwattoo@3ssolutions.net', '', NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 'Muhammad', 'hamid', NULL, NULL, NULL, NULL, NULL, 'fc3cf452d3da8402bebb765225ce8c0e', 1, 0, 'Invite_Sent', 1, '2013-09-11 06:12:32', 1, '2013-09-11 06:12:32');

-- --------------------------------------------------------

--
-- Table structure for table `users_accounts`
--

CREATE TABLE IF NOT EXISTS `users_accounts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_default` bit(1) NOT NULL DEFAULT b'0',
  `permission_maintain_folders` bit(1) NOT NULL,
  `permission_access_video_library` bit(1) NOT NULL,
  `permission_video_library_upload` bit(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `account_id` (`account_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

--
-- Dumping data for table `users_accounts`
--

INSERT INTO `users_accounts` (`id`, `account_id`, `user_id`, `role_id`, `is_default`, `permission_maintain_folders`, `permission_access_video_library`, `permission_video_library_upload`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`) VALUES
(1, 1, 1, 100, '1', '1', '1', '1', 1, '2013-09-20 00:00:00', 1, '2013-09-04 00:43:04'),
(2, 2, 1, 110, '1', '1', '1', '1', 1, '2013-09-20 00:00:00', 1, '2013-09-20 00:00:00'),
(3, 1, 3, 110, '1', '1', '1', '1', 1, '2013-09-20 00:00:00', 1, '2013-09-04 00:43:04'),
(35, 1, 814, 120, '1', '0', '0', '0', 1, '2013-08-23 18:34:47', 1, '2013-08-23 06:34:47'),
(36, 1, 815, 110, '1', '0', '0', '0', 1, '2013-08-29 20:37:17', 1, '2013-08-29 08:37:17'),
(37, 1, 816, 120, '1', '0', '0', '0', 1, '2013-08-29 20:44:19', 1, '2013-08-29 08:44:19'),
(38, 1, 817, 110, '1', '0', '0', '0', 1, '2013-08-30 07:09:04', 1, '2013-08-30 07:09:04'),
(39, 1, 818, 110, '1', '0', '0', '0', 1, '2013-08-30 07:19:52', 1, '2013-08-30 07:19:52'),
(40, 1, 819, 110, '1', '0', '0', '0', 1, '2013-08-30 07:34:46', 1, '2013-08-30 07:34:46'),
(41, 1, 820, 110, '1', '0', '0', '0', 1, '2013-08-30 07:44:19', 1, '2013-08-30 07:44:19'),
(43, 4, 822, 100, '1', '1', '1', '1', 822, '2013-09-01 04:03:22', 822, '2013-09-01 04:03:22'),
(45, 7, 1, 100, '0', '0', '0', '0', 0, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00'),
(47, 10, 825, 100, '1', '1', '1', '1', 825, '2013-09-04 06:30:17', 825, '2013-09-04 06:30:17'),
(48, 11, 826, 100, '1', '1', '1', '1', 826, '2013-09-04 06:34:19', 826, '2013-09-04 06:34:19'),
(54, 11, 832, 110, '1', '0', '0', '0', 826, '2013-09-04 08:19:10', 826, '2013-09-04 08:19:10'),
(55, 11, 833, 120, '1', '0', '0', '0', 826, '2013-09-04 08:19:27', 826, '2013-09-04 08:19:27'),
(56, 1, 834, 110, '1', '0', '0', '0', 1, '2013-09-10 03:34:17', 1, '2013-09-10 03:34:17'),
(57, 1, 835, 110, '1', '0', '0', '0', 1, '2013-09-10 10:23:03', 1, '2013-09-10 10:23:03'),
(58, 1, 836, 120, '1', '0', '0', '0', 1, '2013-09-11 05:15:54', 1, '2013-09-11 05:15:54'),
(59, 1, 837, 120, '1', '0', '0', '0', 1, '2013-09-11 05:17:56', 1, '2013-09-11 05:17:56'),
(60, 1, 838, 120, '1', '0', '0', '0', 1, '2013-09-11 05:18:46', 1, '2013-09-11 05:18:46'),
(61, 1, 839, 110, '1', '0', '0', '0', 1, '2013-09-11 05:19:41', 1, '2013-09-11 05:19:41'),
(62, 1, 840, 120, '1', '0', '0', '0', 1, '2013-09-11 06:04:23', 1, '2013-09-11 06:04:23'),
(63, 1, 841, 110, '1', '0', '0', '0', 1, '2013-09-11 06:06:16', 1, '2013-09-11 06:06:16'),
(64, 1, 842, 110, '1', '0', '0', '0', 1, '2013-09-11 06:12:32', 1, '2013-09-11 06:12:32');

-- --------------------------------------------------------

--
-- Table structure for table `user_activity_logs`
--

CREATE TABLE IF NOT EXISTS `user_activity_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `desc` text NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `account_folder_id` int(11) DEFAULT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `user_activity_logs`
--

INSERT INTO `user_activity_logs` (`id`, `account_id`, `user_id`, `desc`, `url`, `account_folder_id`, `date_added`) VALUES
(3, 1, 1, '''Mrs. Smith demonstrates how to execute a Socratic seminar12''', '/app/Huddles/view/24', 24, '2013-09-12 00:00:00'),
(4, 1, 1, '''TEST WITH HAMID''', '/app/Huddles/view/', 25, '2013-09-03 00:00:00'),
(5, 1, 1, '''TEST WITH HAMID''', '/app/Huddles/view/26', 26, '2013-09-25 00:00:00'),
(6, 1, 1, '''Muhammad''', '/appHuddles', NULL, '2013-09-24 00:00:00'),
(7, 1, 1, '''Mere Saath Chalte Chalte''', '/appVideoLibrary/view/27', 27, '2013-09-26 00:00:00'),
(8, 1, 1, '''Mere Saath Chalte Chalte''', '/appVideoLibrary/view/28', 28, '2013-09-02 00:00:00'),
(9, 1, 1, '''Mrs. Smith demonstrates how to execute a Socratic seminar12''', '/app/Huddles/view/24', 24, '2013-09-01 00:00:00'),
(10, 1, 1, 'TEST WITH HAMID', '/app/Huddles/view/26', 26, '2013-09-08 00:00:00'),
(11, 1, 1, 'Haroon', '/appHuddles', NULL, '0000-00-00 00:00:00'),
(12, 1, 1, '''hamid  is invited.''', '/appHuddles', NULL, '0000-00-00 00:00:00'),
(13, 1, 1, 'dsafsdfasdfdsfasfd', '/appapp/Huddles/view/11', 11, '0000-00-00 00:00:00'),
(14, 1, 1, 'dsfasdfasdfsdfasfdafasdf', '/appapp/Huddles/view/12', 12, '2013-09-11 07:32:42');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE IF NOT EXISTS `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_groups_on_group_id` (`group_id`),
  KEY `index_user_groups_on_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `group_id`, `user_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`) VALUES
(1, 1, 1, 1, '2013-12-12 00:00:00', 1, '2013-12-12 00:00:00'),
(2, 1, 3, 1, '2013-12-12 00:00:00', 1, '2013-12-12 00:00:00');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts`
--
ALTER TABLE `accounts`
  ADD CONSTRAINT `fk_accounts_plans` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`);

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `fk_comments_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
