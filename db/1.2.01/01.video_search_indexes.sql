ALTER TABLE `account_folder_subjects` ADD INDEX `idx_account_folder_id` (`account_folder_id`)
ALTER TABLE `account_folder_subjects` ADD INDEX `idx_subject_id` (`subject_id`)


ALTER TABLE `account_folder_topics` ADD INDEX `idx_account_folder_id` (`account_folder_id`)
ALTER TABLE `account_folder_topics` ADD INDEX `idx_topic_id` (`topic_id`)