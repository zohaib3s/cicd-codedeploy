# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.32-0ubuntu0.13.04.1)
# Database: sibme_new
# Generation Time: 2013-09-18 06:07:57 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table account_folder_documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folder_documents`;

CREATE TABLE `account_folder_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `is_viewed` bit(1) NOT NULL DEFAULT b'0',
  `title` varchar(255) DEFAULT NULL,
  `desc` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `account_folder_documents` WRITE;
/*!40000 ALTER TABLE `account_folder_documents` DISABLE KEYS */;

INSERT INTO `account_folder_documents` (`id`, `account_folder_id`, `document_id`, `is_viewed`, `title`, `desc`)
VALUES
	(37,29,85,b'1','My Test','Testing'),
	(38,31,86,b'1','Test video','X'),
	(39,33,87,b'1','Test video','This should work hopefully.'),
	(41,32,89,b'1','hsdhfks djksd',''),
	(42,32,90,b'1','Bug Fixes',''),
	(43,32,91,b'1','Dallas Exhibit Brochure',''),
	(44,29,93,b'1','Yyy','Yyy'),
	(45,29,94,b'1','Tedt','Tedt'),
	(46,29,95,b'1','Lk','Lk'),
	(47,29,96,b'1','A video','A video'),
	(48,29,97,b'1','Testing 123','Testing 123'),
	(49,34,98,b'1','This video is a test',''),
	(50,34,99,b'1','Bug Fixes',''),
	(51,34,100,b'1','GettingReal',''),
	(52,32,101,b'1','Testing testing ','Testing testing '),
	(53,34,102,b'1','Dallas Exhibit Brochure',''),
	(54,35,103,b'1','Testing','This is a description'),
	(55,34,104,b'1','Test','Test'),
	(56,32,105,b'1','Testing','Testing'),
	(57,32,106,b'1','Testing','Testing'),
	(58,29,107,b'1','Ok','Ok'),
	(59,29,108,b'1','K video','K video'),
	(60,27,109,b'1','Hjjjjj','Hjjjjj'),
	(61,27,110,b'1','Hjjjjj','Hjjjjj'),
	(62,29,111,b'1','Jjjjk','Jjjjk'),
	(63,26,112,b'1','Nt','Nt'),
	(64,29,113,b'1','Demo with dave','Demo with dave'),
	(65,27,114,b'1','Demo to Dave ','Demo to Dave '),
	(66,27,115,b'1','2nd','2nd'),
	(67,27,116,b'1','1st upload','1st upload'),
	(68,27,117,b'1','Kk','Kk'),
	(69,27,118,b'1','Trst','Trst'),
	(70,26,119,b'1','Rut','Rut'),
	(71,26,120,b'1','Dhgu','Dhgu'),
	(72,26,121,b'1','Dud','Dud'),
	(73,26,122,b'1','Gkg','Gkg'),
	(74,26,123,b'1','Fh','Fh'),
	(75,26,124,b'1','Fh','Fh'),
	(76,26,125,b'1','Gj','Gj'),
	(77,26,126,b'1','He','He'),
	(78,26,127,b'1','Tu','Tu'),
	(79,26,128,b'1','Fu','Fu'),
	(80,27,129,b'1','Tt ','Tt '),
	(81,29,130,b'1','Title','Title'),
	(82,29,131,b'1','Tedt','Tedt'),
	(83,29,132,b'1','Testing','Testing'),
	(84,29,133,b'1','Ok','Ok'),
	(85,29,134,b'1','Testing','Testing'),
	(86,29,135,b'1','uploaded hamid video',''),
	(87,29,136,b'1','kupload',''),
	(88,29,137,b'1','Test','Test'),
	(89,29,138,b'1','Lovely','Lovely'),
	(90,26,139,b'1','1A','1A'),
	(93,37,142,b'1','kupload.mov',''),
	(94,37,143,b'1','169484_10151074057426722_2142794718_o',''),
	(95,37,144,b'1','kupload',''),
	(96,38,146,b'1','Testing Lib','Test'),
	(97,36,147,b'1','Room','Room'),
	(98,39,148,b'1','video1','video'),
	(99,26,149,b'1','Gjff','Gjff'),
	(100,26,150,b'1','Gjff','Gjff'),
	(101,26,151,b'1','Gjff','Gjff'),
	(102,26,152,b'1','Ancds','Ancds'),
	(103,26,153,b'1','Ancds','Ancds'),
	(104,26,154,b'1','Ancds','Ancds'),
	(105,26,155,b'1','Ancds','Ancds'),
	(106,26,156,b'1','Ancds','Ancds'),
	(107,26,157,b'1','Ancds','Ancds'),
	(108,26,158,b'1','Gjjrr','Gjjrr'),
	(109,26,159,b'1','Gjjrr','Gjjrr'),
	(110,27,160,b'1','Ghg','Ghg'),
	(111,27,161,b'1','Ghg','Ghg'),
	(112,27,162,b'1','Ghg','Ghg'),
	(113,27,163,b'1','Ghg','Ghg'),
	(114,27,164,b'1','Ghg','Ghg'),
	(115,26,165,b'1','Dhf','Dhf'),
	(116,26,166,b'1','Dhf','Dhf'),
	(117,26,167,b'1','Dhf','Dhf'),
	(118,26,168,b'1','Dhf','Dhf'),
	(119,26,169,b'1','Dhf','Dhf'),
	(120,26,170,b'1','Dhf','Dhf'),
	(121,26,171,b'1','Df','Df'),
	(122,26,172,b'1','Fhh','Fhh'),
	(123,26,173,b'1','Chx','Chx'),
	(124,26,174,b'1','Xhg','Xhg'),
	(125,26,175,b'1','Xhg','Xhg'),
	(126,26,176,b'1','Fuf','Fuf'),
	(127,26,177,b'1','Dfg','Dfg'),
	(128,26,178,b'1','Llh','Llh'),
	(129,26,179,b'1','Llh','Llh'),
	(130,26,180,b'1','Llh','Llh'),
	(131,26,181,b'1','Dhf','Dhf'),
	(132,26,182,b'1','Dhf','Dhf'),
	(133,26,183,b'1','Dhf','Dhf'),
	(134,26,184,b'1','Dhf','Dhf'),
	(135,26,185,b'1','Fgf','Fgf'),
	(136,26,186,b'1','Hgf','Hgf'),
	(137,26,187,b'1','Hgf','Hgf'),
	(138,26,188,b'1','Fuf','Fuf'),
	(139,26,189,b'1','Lvh','Lvh'),
	(140,26,190,b'1','Lvh','Lvh'),
	(141,34,191,b'1','Test','Test'),
	(142,32,192,b'1','Hello','Hello'),
	(143,32,193,b'1','Hello','Hello'),
	(144,32,194,b'1','2','2'),
	(145,32,195,b'1','Ok','Ok'),
	(146,26,196,b'1','Hr','Hr'),
	(147,26,197,b'1','Hrh','Hrh'),
	(148,26,198,b'1','Ghj','Ghj'),
	(149,26,199,b'1','Ghj','Ghj'),
	(150,29,200,b'1','Chgf','Chgf'),
	(151,26,201,b'1','Lbc','Lbc'),
	(152,26,202,b'1','Lbc','Lbc'),
	(153,26,203,b'1','Lbc','Lbc'),
	(154,26,204,b'1','Kgb','Kgb'),
	(155,26,205,b'1','Lvc','Lvc'),
	(156,26,206,b'1','Lvc','Lvc'),
	(157,26,207,b'1','Lvc','Lvc'),
	(158,26,208,b'1','Lvc','Lvc'),
	(159,34,209,b'1','Xhcgy','Xhcgy'),
	(160,34,210,b'1','Xhcgy','Xhcgy'),
	(161,34,211,b'1','Xhcgy','Xhcgy'),
	(162,34,212,b'1','Xhcgy','Xhcgy'),
	(163,34,213,b'1','Xhcgy','Xhcgy'),
	(164,34,214,b'1','Fhd','Fhd'),
	(165,34,215,b'1','Dhdg','Dhdg'),
	(166,34,216,b'1','Dhdg','Dhdg'),
	(167,34,217,b'1','Ghdv','Ghdv'),
	(168,34,218,b'1','Djk','Djk'),
	(169,34,219,b'1','Djk','Djk'),
	(170,34,220,b'1','Tuk','Tuk'),
	(171,34,222,b'1','Tuk','Tuk'),
	(172,34,221,b'1','Tuk','Tuk'),
	(173,34,223,b'1','Hxf','Hxf'),
	(174,34,224,b'1','Thi','Thi'),
	(175,34,225,b'1','Thi','Thi'),
	(176,32,226,b'1','Hkjg','Hkjg'),
	(177,34,227,b'1','Thi','Thi'),
	(178,34,228,b'1','Thi','Thi'),
	(179,32,229,b'1','Chl','Chl'),
	(180,32,230,b'1','Joh','Joh'),
	(181,32,231,b'1','Dhj','Dhj'),
	(182,32,232,b'1','Jht','Jht'),
	(183,32,233,b'1','Fhk','Fhk'),
	(184,34,234,b'1','Gjg','Gjg'),
	(185,34,235,b'1','Fhi','Fhi'),
	(186,34,236,b'1','Fhi','Fhi'),
	(187,34,237,b'1','Hmg','Hmg'),
	(188,34,238,b'1','Fg','Fg'),
	(189,34,239,b'1','Fg','Fg'),
	(190,34,240,b'1','Gjl','Gjl'),
	(191,34,241,b'1','Dhd','Dhd'),
	(192,34,242,b'1','Fhg','Fhg'),
	(193,34,243,b'1','Chc','Chc'),
	(194,34,244,b'1','Gheyh','Gheyh'),
	(195,34,245,b'1','Hkfp','Hkfp'),
	(196,34,246,b'1','Ryh','Ryh'),
	(197,34,247,b'1','Fhi','Fhi'),
	(198,44,248,b'1','Test Video','testing'),
	(199,43,249,b'1','My test','My test'),
	(200,43,250,b'1','Hello 2','Hello 2'),
	(201,45,251,b'1','Fjjh','Fjjh'),
	(202,43,252,b'1','Testing','Testing'),
	(203,46,253,b'1','Testing','Testing'),
	(204,47,254,b'1','test','test'),
	(205,48,255,b'1','Lecture No 1','Lecture No 2 Discriptions'),
	(206,49,256,b'1','Lecture #3','Lecture No 3 '),
	(207,50,257,b'1','test','test'),
	(208,51,258,b'1','test','test'),
	(209,52,261,b'1','gg','kkk'),
	(210,53,262,b'1','test','dhfgh'),
	(211,32,263,b'1','Video test','Video test');

/*!40000 ALTER TABLE `account_folder_documents` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_folder_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folder_groups`;

CREATE TABLE `account_folder_groups` (
  `account_folder_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`account_folder_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `account_folder_groups` WRITE;
/*!40000 ALTER TABLE `account_folder_groups` DISABLE KEYS */;

INSERT INTO `account_folder_groups` (`account_folder_group_id`, `account_folder_id`, `group_id`, `role_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(9,41,8,210,851,'2013-09-15 19:27:32',851,'2013-09-15 19:27:32'),
	(10,40,8,210,851,'2013-09-15 19:30:16',851,'2013-09-15 07:30:16'),
	(11,40,7,210,851,'2013-09-15 19:30:16',851,'2013-09-15 07:30:16'),
	(12,40,6,210,851,'2013-09-15 19:30:16',851,'2013-09-15 07:30:16'),
	(13,32,3,220,823,'2013-09-17 11:38:56',823,'2013-09-17 11:38:56');

/*!40000 ALTER TABLE `account_folder_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_folder_subjects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folder_subjects`;

CREATE TABLE `account_folder_subjects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `account_folder_subjects` WRITE;
/*!40000 ALTER TABLE `account_folder_subjects` DISABLE KEYS */;

INSERT INTO `account_folder_subjects` (`id`, `account_folder_id`, `subject_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(25,31,8,1,'2013-09-13 11:47:45',1,'2013-09-13 11:47:45'),
	(22,33,9,823,'2013-09-04 12:31:55',823,'2013-09-04 12:31:55'),
	(23,33,10,823,'2013-09-04 12:31:55',823,'2013-09-04 12:31:55'),
	(24,35,10,823,'2013-09-09 14:45:24',823,'2013-09-09 02:45:24'),
	(27,38,11,836,'2013-09-13 14:14:04',836,'2013-09-13 02:14:04'),
	(28,39,12,851,'2013-09-15 19:20:02',851,'2013-09-15 07:20:02'),
	(29,44,13,854,'2013-09-16 08:14:52',854,'2013-09-16 08:14:52'),
	(30,47,13,854,'2013-09-16 10:20:27',854,'2013-09-16 10:20:27'),
	(31,48,14,854,'2013-09-16 10:42:11',854,'2013-09-16 10:42:11'),
	(32,49,13,854,'2013-09-16 10:48:18',854,'2013-09-16 10:48:18'),
	(33,50,14,854,'2013-09-16 10:59:12',854,'2013-09-16 10:59:12'),
	(34,51,14,854,'2013-09-16 11:02:33',854,'2013-09-16 11:02:33'),
	(35,52,13,854,'2013-09-16 11:06:41',854,'2013-09-16 11:06:41'),
	(36,53,14,854,'2013-09-16 11:09:38',854,'2013-09-16 11:09:38'),
	(37,53,13,854,'2013-09-16 11:09:38',854,'2013-09-16 11:09:38');

/*!40000 ALTER TABLE `account_folder_subjects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_folder_topics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folder_topics`;

CREATE TABLE `account_folder_topics` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `account_folder_topics` WRITE;
/*!40000 ALTER TABLE `account_folder_topics` DISABLE KEYS */;

INSERT INTO `account_folder_topics` (`id`, `account_folder_id`, `topic_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(23,31,5,1,'2013-09-13 11:47:46',1,'2013-09-13 11:47:46'),
	(21,33,6,823,'2013-09-04 12:31:55',823,'2013-09-04 12:31:55'),
	(22,35,6,823,'2013-09-09 14:45:24',823,'2013-09-09 02:45:24'),
	(25,38,7,836,'2013-09-13 14:14:04',836,'2013-09-13 02:14:04'),
	(26,39,8,851,'2013-09-15 19:20:02',851,'2013-09-15 07:20:02'),
	(27,44,9,854,'2013-09-16 08:14:52',854,'2013-09-16 08:14:52'),
	(28,47,9,854,'2013-09-16 10:20:27',854,'2013-09-16 10:20:27'),
	(29,48,9,854,'2013-09-16 10:42:11',854,'2013-09-16 10:42:11'),
	(30,49,9,854,'2013-09-16 10:48:18',854,'2013-09-16 10:48:18'),
	(31,50,9,854,'2013-09-16 10:59:12',854,'2013-09-16 10:59:12'),
	(32,51,9,854,'2013-09-16 11:02:33',854,'2013-09-16 11:02:33'),
	(33,52,9,854,'2013-09-16 11:06:41',854,'2013-09-16 11:06:41'),
	(34,53,9,854,'2013-09-16 11:09:38',854,'2013-09-16 11:09:38');

/*!40000 ALTER TABLE `account_folder_topics` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_folder_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folder_users`;

CREATE TABLE `account_folder_users` (
  `account_folder_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`account_folder_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `account_folder_users` WRITE;
/*!40000 ALTER TABLE `account_folder_users` DISABLE KEYS */;

INSERT INTO `account_folder_users` (`account_folder_user_id`, `account_folder_id`, `user_id`, `role_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(26,27,1,200,1,'2013-09-03 14:21:27',1,'2013-09-03 02:21:27'),
	(27,28,822,200,822,'2013-09-03 14:22:32',822,'2013-09-03 02:22:32'),
	(28,29,1,200,1,'2013-09-03 14:29:04',1,'2013-09-03 02:29:04'),
	(29,30,822,200,822,'2013-09-03 14:32:51',822,'2013-09-03 02:32:51'),
	(34,29,826,200,1,'2013-09-04 15:16:58',1,'2013-09-04 03:16:58'),
	(36,27,826,200,1,'2013-09-04 15:57:38',1,'2013-09-04 03:57:38'),
	(37,27,829,210,1,'2013-09-04 15:57:38',1,'2013-09-04 03:57:38'),
	(42,37,836,200,836,'2013-09-13 13:19:01',836,'2013-09-13 13:19:01'),
	(44,37,837,210,836,'2013-09-13 13:27:39',836,'2013-09-13 01:27:39'),
	(45,40,851,200,851,'2013-09-15 19:27:02',851,'2013-09-15 19:27:02'),
	(50,41,851,200,851,'2013-09-15 19:27:32',851,'2013-09-15 19:27:32'),
	(51,40,853,200,851,'2013-09-15 19:30:16',851,'2013-09-15 07:30:16'),
	(52,40,852,210,851,'2013-09-15 19:30:16',851,'2013-09-15 07:30:16'),
	(56,43,855,200,855,'2013-09-16 06:46:23',855,'2013-09-16 06:46:23'),
	(57,43,854,210,855,'2013-09-16 06:46:23',855,'2013-09-16 06:46:23'),
	(59,45,855,200,855,'2013-09-16 08:45:22',855,'2013-09-16 08:45:22'),
	(60,46,854,200,854,'2013-09-16 09:13:03',854,'2013-09-16 09:13:03'),
	(67,55,854,200,854,'2013-09-17 14:00:08',854,'2013-09-17 14:00:08'),
	(68,55,855,200,854,'2013-09-17 14:00:08',854,'2013-09-17 14:00:08'),
	(74,55,834,210,854,'2013-09-17 02:05:57',0,'0000-00-00 00:00:00'),
	(75,45,834,210,854,'2013-09-17 02:05:57',0,'0000-00-00 00:00:00'),
	(76,55,857,220,854,'2013-09-17 02:06:42',0,'0000-00-00 00:00:00'),
	(77,45,857,220,854,'2013-09-17 02:06:42',0,'0000-00-00 00:00:00'),
	(84,36,824,210,823,'2013-09-17 02:08:18',0,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `account_folder_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_folderdocument_attachments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folderdocument_attachments`;

CREATE TABLE `account_folderdocument_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_folder_document_id` int(11) NOT NULL,
  `attach_id` int(11) NOT NULL COMMENT 'account_folder_attach_document_id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `account_folderdocument_attachments` WRITE;
/*!40000 ALTER TABLE `account_folderdocument_attachments` DISABLE KEYS */;

INSERT INTO `account_folderdocument_attachments` (`id`, `account_folder_document_id`, `attach_id`)
VALUES
	(11,43,89),
	(14,53,98),
	(15,42,89),
	(16,42,101),
	(17,87,131),
	(18,94,142),
	(19,95,0);

/*!40000 ALTER TABLE `account_folderdocument_attachments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_folders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folders`;

CREATE TABLE `account_folders` (
  `account_folder_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `folder_type` tinyint(4) NOT NULL COMMENT '1=huddle, 2=video library, 3=my files, etc.',
  `parent_folder_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `desc` varchar(4000) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`account_folder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `account_folders` WRITE;
/*!40000 ALTER TABLE `account_folders` DISABLE KEYS */;

INSERT INTO `account_folders` (`account_folder_id`, `account_id`, `folder_type`, `parent_folder_id`, `name`, `desc`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`, `active`)
VALUES
	(26,1,1,NULL,'Khurram\'s Coaching classes','',1,'2013-09-02 22:44:33',1,'2013-09-03 11:34:22',1),
	(27,1,1,NULL,'test','',1,'2013-09-03 14:21:27',1,'2013-09-04 15:57:38',1),
	(28,9,1,NULL,'Test Huddle 1','test',822,'2013-09-03 14:22:32',822,'2013-09-03 02:22:32',1),
	(29,1,1,NULL,'Testing 2','',1,'2013-09-03 14:29:04',1,'2013-09-03 02:29:04',1),
	(30,9,1,NULL,'Test Huddle 2','',822,'2013-09-03 14:32:50',822,'2013-09-03 02:32:50',0),
	(31,1,2,NULL,'Test video','X',1,'2013-09-03 23:04:22',1,'2013-09-13 11:47:45',1),
	(32,10,1,NULL,'Test huddle','',823,'2013-09-04 12:21:41',823,'2013-09-17 11:38:56',1),
	(33,10,2,NULL,'Test video','This should work hopefully.  Does it.',823,'2013-09-04 12:29:19',823,'2013-09-04 12:31:55',0),
	(34,10,1,NULL,'Chem huddle','',823,'2013-09-05 10:11:05',823,'2013-09-05 10:11:05',1),
	(35,10,2,NULL,'Testing','This is a description',823,'2013-09-09 14:45:23',823,'2013-09-09 14:45:23',1),
	(36,10,1,NULL,'English huddle','',823,'2013-09-09 15:31:57',823,'2013-09-09 15:31:57',1),
	(37,12,1,NULL,'Khurram\'s Coaching classes','',836,'2013-09-13 13:19:01',836,'2013-09-13 13:27:39',1),
	(38,12,2,NULL,'Testing Lib','Test',836,'2013-09-13 14:09:45',836,'2013-09-13 14:14:04',1),
	(39,13,2,NULL,'video1','video',851,'2013-09-15 19:20:02',851,'2013-09-15 19:20:02',0),
	(40,13,1,NULL,'Coach Huddle','coaching huddle',851,'2013-09-15 19:27:02',851,'2013-09-15 19:30:16',1),
	(41,13,1,NULL,'huddle 2','huddle',851,'2013-09-15 19:27:32',851,'2013-09-15 19:27:32',0),
	(43,14,1,NULL,'MIYW Huddle','Here is my description.',855,'2013-09-16 06:46:23',855,'2013-09-16 06:46:23',0),
	(44,14,2,NULL,'Test Video','testing',854,'2013-09-16 08:14:52',854,'2013-09-16 08:14:52',1),
	(45,14,1,NULL,'Testing for Me','',855,'2013-09-16 08:45:22',855,'2013-09-16 08:45:22',1),
	(46,14,1,NULL,'ksaleem Huddle Only','',854,'2013-09-16 09:13:03',854,'2013-09-16 09:13:03',0),
	(47,14,2,NULL,'test','test',854,'2013-09-16 10:20:27',854,'2013-09-16 10:20:27',1),
	(48,14,2,NULL,'Lecture No 1','Lecture No 2 Discriptions',854,'2013-09-16 10:42:11',854,'2013-09-16 10:42:11',1),
	(49,14,2,NULL,'Lecture #3','Lecture No 3 ',854,'2013-09-16 10:48:18',854,'2013-09-16 10:48:18',1),
	(50,14,2,NULL,'test','test',854,'2013-09-16 10:59:12',854,'2013-09-16 10:59:12',1),
	(51,14,2,NULL,'test','test',854,'2013-09-16 11:02:33',854,'2013-09-16 11:02:33',1),
	(52,14,2,NULL,'gg','kkk',854,'2013-09-16 11:06:41',854,'2013-09-16 11:06:41',1),
	(53,14,2,NULL,'test','dhfgh',854,'2013-09-16 11:09:38',854,'2013-09-16 11:09:38',1),
	(54,10,1,NULL,'New Huddle','',830,'2013-09-17 12:02:29',824,'2013-09-17 12:09:29',1),
	(55,14,1,NULL,'English Department Meeting - 1/9/13','English Department Meeting - 1/9/13',854,'2013-09-17 14:00:08',854,'2013-09-17 14:00:08',1);

/*!40000 ALTER TABLE `account_folders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_folders_meta_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folders_meta_data`;

CREATE TABLE `account_folders_meta_data` (
  `account_folder_meta_data_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) NOT NULL,
  `meta_data_name` varchar(64) NOT NULL COMMENT 'tag, topic, subject, etc',
  `meta_data_value` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`account_folder_meta_data_id`),
  UNIQUE KEY `account_folders_meta_data_id_UNIQUE` (`account_folder_meta_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `account_folders_meta_data` WRITE;
/*!40000 ALTER TABLE `account_folders_meta_data` DISABLE KEYS */;

INSERT INTO `account_folders_meta_data` (`account_folder_meta_data_id`, `account_folder_id`, `meta_data_name`, `meta_data_value`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(34,26,'message','Testing with Khurram\'s only.',1,'2013-09-02 22:44:33',1,'2013-09-03 11:34:22'),
	(35,27,'message','',1,'2013-09-03 14:21:27',1,'2013-09-04 15:57:38'),
	(36,28,'message','',822,'2013-09-03 14:22:32',822,'2013-09-03 02:22:32'),
	(37,29,'message','',1,'2013-09-03 14:29:04',1,'2013-09-03 02:29:04'),
	(38,30,'message','',822,'2013-09-03 14:32:51',822,'2013-09-03 02:32:51'),
	(43,32,'message','',823,'2013-09-04 12:21:41',823,'2013-09-17 11:38:56'),
	(52,33,'tag','English',823,'2013-09-04 12:31:55',823,'2013-09-04 12:31:55'),
	(53,33,'tag','Best practices',823,'2013-09-04 12:31:55',823,'2013-09-04 12:31:55'),
	(54,33,'tag','Math',823,'2013-09-04 12:31:55',823,'2013-09-04 12:31:55'),
	(55,34,'message','',823,'2013-09-05 10:11:05',823,'2013-09-05 10:11:05'),
	(56,35,'tag','Chemistry',823,'2013-09-09 14:45:23',823,'2013-09-09 02:45:23'),
	(57,35,'tag','Math',823,'2013-09-09 14:45:23',823,'2013-09-09 02:45:23'),
	(58,36,'message','',823,'2013-09-09 15:31:57',823,'2013-09-09 15:31:57'),
	(59,31,'tag','tag',1,'2013-09-13 11:47:45',1,'2013-09-13 11:47:45'),
	(60,37,'message','Test',836,'2013-09-13 13:19:01',836,'2013-09-13 13:27:39'),
	(62,38,'tag','Math',836,'2013-09-13 14:14:04',836,'2013-09-13 02:14:04'),
	(63,39,'tag','tag1',851,'2013-09-15 19:20:02',851,'2013-09-15 07:20:02'),
	(64,39,'tag','tag2',851,'2013-09-15 19:20:02',851,'2013-09-15 07:20:02'),
	(65,40,'message','<blockquote>fhhs mszzV</blockquote>',851,'2013-09-15 19:27:02',851,'2013-09-15 19:30:16'),
	(66,41,'message','',851,'2013-09-15 19:27:32',851,'2013-09-15 19:27:32'),
	(67,42,'message','Hello Here is MIYW huddle.',855,'2013-09-16 06:44:13',855,'2013-09-16 06:44:13'),
	(68,43,'message','Hello Here is MIYW huddle.',855,'2013-09-16 06:46:23',855,'2013-09-16 06:46:23'),
	(69,44,'tag','Test',854,'2013-09-16 08:14:52',854,'2013-09-16 08:14:52'),
	(70,45,'message','',855,'2013-09-16 08:45:22',855,'2013-09-16 08:45:22'),
	(71,46,'message','',854,'2013-09-16 09:13:03',854,'2013-09-16 09:13:03'),
	(72,47,'tag','test',854,'2013-09-16 10:20:27',854,'2013-09-16 10:20:27'),
	(73,48,'tag','test',854,'2013-09-16 10:42:11',854,'2013-09-16 10:42:11'),
	(74,49,'tag','lecture',854,'2013-09-16 10:48:18',854,'2013-09-16 10:48:18'),
	(75,50,'tag','test',854,'2013-09-16 10:59:12',854,'2013-09-16 10:59:12'),
	(76,51,'tag','test',854,'2013-09-16 11:02:33',854,'2013-09-16 11:02:33'),
	(77,52,'tag','kk',854,'2013-09-16 11:06:41',854,'2013-09-16 11:06:41'),
	(78,53,'tag','ddf',854,'2013-09-16 11:09:38',854,'2013-09-16 11:09:38'),
	(79,54,'message','',830,'2013-09-17 12:02:29',824,'2013-09-17 12:09:29'),
	(80,55,'message','',854,'2013-09-17 14:00:08',854,'2013-09-17 14:00:08');

/*!40000 ALTER TABLE `account_folders_meta_data` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_meta_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_meta_data`;

CREATE TABLE `account_meta_data` (
  `account_meta_data_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `meta_data_name` varchar(64) NOT NULL COMMENT 'tag, topic, subject, etc',
  `meta_data_value` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`account_meta_data_id`),
  UNIQUE KEY `account_meta_data_id_UNIQUE` (`account_meta_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table accounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accounts`;

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_account_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `custom_url` varchar(255) DEFAULT NULL,
  `image_logo` varchar(255) DEFAULT NULL,
  `header_background_color` varchar(255) DEFAULT NULL,
  `text_link_color` varchar(255) DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `storage` int(11) DEFAULT NULL,
  `storage_used` int(11) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `last_4digits` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `subdomain` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `braintree_customer_id` text,
  `braintree_subscription_id` text,
  `nav_bg_color` varchar(255) DEFAULT NULL,
  `usernav_bg_color` varchar(255) DEFAULT NULL,
  `in_trial` tinyint(1) DEFAULT NULL,
  `has_credit_card` tinyint(1) DEFAULT NULL,
  `suspended_at` datetime DEFAULT NULL,
  `is_suspended` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_accounts_on_user_id` (`user_id`),
  KEY `index_accounts_on_plan_id` (`plan_id`),
  CONSTRAINT `fk_accounts_plans` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;

INSERT INTO `accounts` (`id`, `parent_account_id`, `user_id`, `company_name`, `custom_url`, `image_logo`, `header_background_color`, `text_link_color`, `plan_id`, `storage`, `storage_used`, `card_type`, `last_4digits`, `created_at`, `updated_at`, `subdomain`, `is_active`, `braintree_customer_id`, `braintree_subscription_id`, `nav_bg_color`, `usernav_bg_color`, `in_trial`, `has_credit_card`, `suspended_at`, `is_suspended`)
VALUES
	(1,0,NULL,'Sibme',NULL,'','336699',NULL,NULL,NULL,623275,NULL,NULL,'2013-08-20 00:00:00','2013-09-13 12:33:10',NULL,1,NULL,NULL,'668dab',NULL,1,0,NULL,0),
	(2,0,NULL,'Sibme QA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-20 00:00:00','2013-08-20 00:00:00',NULL,1,NULL,NULL,NULL,NULL,1,0,NULL,0),
	(9,0,NULL,'Test',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-09-03 14:18:26','2013-09-03 14:18:26',NULL,1,NULL,NULL,NULL,NULL,1,0,NULL,0),
	(10,0,NULL,'HIDDD',NULL,'','336699',NULL,3,NULL,NULL,NULL,NULL,'2013-09-04 11:57:53','2013-09-11 09:09:33',NULL,1,NULL,NULL,'668dab',NULL,0,0,NULL,0),
	(11,0,NULL,'MySchool',NULL,'','336699',NULL,NULL,NULL,NULL,NULL,NULL,'2013-09-11 21:45:35','2013-09-15 18:37:21',NULL,1,NULL,NULL,'668dab',NULL,1,0,NULL,0),
	(12,0,NULL,'3S',NULL,NULL,NULL,NULL,NULL,NULL,623275,NULL,NULL,'2013-09-13 13:00:49','2013-09-13 13:00:49',NULL,1,NULL,NULL,NULL,NULL,1,0,NULL,0),
	(13,0,NULL,'School3',NULL,NULL,NULL,NULL,NULL,NULL,45992992,NULL,NULL,'2013-09-15 19:08:45','2013-09-15 19:08:45',NULL,1,NULL,NULL,NULL,NULL,1,0,NULL,0),
	(14,0,NULL,'3S',NULL,NULL,NULL,NULL,NULL,NULL,623275,NULL,NULL,'2013-09-16 06:24:53','2013-09-16 06:24:53',NULL,1,NULL,NULL,NULL,NULL,1,0,NULL,0);

/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table audit_emails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `audit_emails`;

CREATE TABLE `audit_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_from` text,
  `email_to` text,
  `email_cc` text,
  `email_bcc` text,
  `email_subject` varchar(4000) DEFAULT NULL,
  `email_body` text,
  `is_html` bit(1) NOT NULL,
  `error_msg` text,
  `sent_date` datetime DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `audit_emails` WRITE;
/*!40000 ALTER TABLE `audit_emails` DISABLE KEYS */;

INSERT INTO `audit_emails` (`id`, `email_from`, `email_to`, `email_cc`, `email_bcc`, `email_subject`, `email_body`, `is_html`, `error_msg`, `sent_date`, `account_id`)
VALUES
	(1,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/users/activation/c4ca4238a0b923820dcc509a6f75849b/d0n2g70hf40vtxtdx58f/96ea64f3a1aa2fd00c72faacf0cb8ac9\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 01:24:20',1),
	(2,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/users/activation/c4ca4238a0b923820dcc509a6f75849b/nd4cdbrhmqw5c1jzltme/da8ce53cf0240070ce6c69c48cd588ee\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 01:25:05',1),
	(3,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/users/activation/c4ca4238a0b923820dcc509a6f75849b/vxbhv3cqgfq54o19hrxp/82489c9737cc245530c7a6ebef3753ec\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 01:28:00',1),
	(4,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/ksnnt73h4bqbrwf047el/7c590f01490190db0ed02a5070e20f01\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 04:35:42',1),
	(5,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/625gia9s4foa7wqwclww/35cf8659cfcb13224cbd47863a34fc58\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 04:48:27',1),
	(6,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/mtc16p4d4l15vsd62w2m/beb22fb694d513edcf5533cf006dfeae\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 05:45:24',1),
	(7,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/bmk7wk4dj8f84kalsl8x/9e3cfc48eccf81a0d57663e129aef3cb\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 05:53:16',1),
	(8,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/nva9ts6mb3irrsq25dn7/28267ab848bcf807b2ed53c3a8f8fc8a\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 06:54:34',1),
	(9,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/uybtic176xwcwiogi1lv/7a53928fa4dd31e82c6ef826f341daec\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 06:58:15',1),
	(10,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/l5v41yky0xy3b47lnugp/1905aedab9bf2477edc068a355bba31a\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 14:28:53',1),
	(11,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/4zo6jrij8555shg8ok35/1141938ba2c2b13f5505d7c424ebae5f\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 14:45:31',1),
	(12,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/sfraeu0n0bcaxg4c7z8r/1aa48fc4880bb0c9b8a3bf979d3b917e\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 14:52:46',1),
	(13,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/c57zohpea6wxnw1vsoas/dc5689792e08eb2e219dce49e64c885b\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 14:55:41',1),
	(14,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/xrt5mxywobro01ceze7z/846c260d715e5b854ffad5f70a516c88\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 15:07:15',1),
	(15,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/4xsjjd0yghikskk5werh/d58072be2820e8682c0a27c0518e805e\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 16:51:51',1),
	(16,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href=\"#\" target=\"_blank\">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://localhost/sibme/app//Users/login\" target=\"_blank\">http://localhost/sibme/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> ksaleem3    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-08-23 16:52:33',NULL),
	(17,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/6x9eadwlb129tx15ej72/6e7b33fdea3adc80ebd648fffb665bb8\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 16:54:57',1),
	(18,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href=\"#\" target=\"_blank\">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://localhost/sibme/app//Users/login\" target=\"_blank\">http://localhost/sibme/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> ksaleem3    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-08-23 16:55:33',1),
	(19,'do-not-reply@sibme.com','info@3ssolutions.net',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/k41o1qur8lmaagiqphpc/a8ecbabae151abacba7dbde04f761c37\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 16:58:23',1),
	(20,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/4q5cw0st7y9t20fyg0px/32b30a250abd6331e03a2a1f16466346\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 17:01:33',1),
	(21,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saeems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/9643hnkinr7qpwda0s7l/b6edc1cd1f36e45daf6d7824d7bb2283\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 17:58:41',1),
	(22,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/fl7xx8lk0y7dcqugbrw1/670e8a43b246801ca1eaca97b3e19189\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 18:26:31',1),
	(23,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/f5eqwhsjc4s5vfne9i5m/81e74d678581a3bb7a720b019f4f1a93\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 18:29:55',1),
	(24,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href=\"#\" target=\"_blank\">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://localhost/sibme/app//Users/login\" target=\"_blank\">http://localhost/sibme/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> ksaleem3    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-08-23 18:30:31',1),
	(25,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/qwamqlvxfjzf47f2w23r/e0cf1f47118daebc5b16269099ad7347\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 18:31:11',1),
	(26,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/os5fvbptg6al7xt1vzga/96b9bff013acedfb1d140579e2fbeb63\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 18:33:31',1),
	(27,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href=\"#\" target=\"_blank\">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://localhost/sibme/app//Users/login\" target=\"_blank\">http://localhost/sibme/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> ksaleem3    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-08-23 18:34:03',1),
	(28,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/xib9bnmv9i27nw7avaaz/71ad16ad2c4d81f348082ff6c4b20768\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 18:34:47',1),
	(29,'do-not-reply@sibme.com',NULL,NULL,NULL,'  created a new Sibme Coaching Huddle: ','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p><pre class=\"cake-error\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e58840f7-trace\').style.display = (document.getElementById(\'cakeErr52195e58840f7-trace\').style.display == \'none\' ? \'\' : \'none\');\"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>5</b>]<div id=\"cakeErr52195e58840f7-trace\" class=\"cake-stack-trace\" style=\"display: none;\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e58840f7-code\').style.display = (document.getElementById(\'cakeErr52195e58840f7-code\').style.display == \'none\' ? \'\' : \'none\')\">Code</a> <a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e58840f7-context\').style.display = (document.getElementById(\'cakeErr52195e58840f7-context\').style.display == \'none\' ? \'\' : \'none\')\">Context</a><pre id=\"cakeErr52195e58840f7-code\" class=\"cake-code-dump\" style=\"display: none;\"><code><span style=\"color: #000000\"><span style=\"color: #0000BB\">?&gt;</span></span></code>\n<code><span style=\"color: #000000\"><span style=\"color: #0000BB\"></span><span style=\"color: #007700\">&lt;</span><span style=\"color: #0000BB\">div&nbsp;id</span><span style=\"color: #007700\">=</span><span style=\"color: #DD0000\">\":6r\"&nbsp;</span><span style=\"color: #0000BB\">style</span><span style=\"color: #007700\">=</span><span style=\"color: #DD0000\">\"overflow:&nbsp;hidden;\"</span><span style=\"color: #007700\">&gt;</span></span></code>\n<span class=\"code-highlight\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'first_name\'</span><span style=\"color: #007700\">]&nbsp;.&nbsp;</span><span style=\"color: #DD0000\">\"&nbsp;\"&nbsp;</span><span style=\"color: #007700\">.&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'last_name\'</span><span style=\"color: #007700\">]&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>&nbsp;invited&nbsp;you&nbsp;to&nbsp;a&nbsp;Coaching&nbsp;Huddle::&lt;/p&gt;</span></code></span></pre><pre id=\"cakeErr52195e58840f7-context\" class=\"cake-context\" style=\"display: none;\">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class=\"stack-trace\">include - APP/View/Elements/emails/new_huddle.ctp, line 5\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre><pre class=\"cake-error\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5884a8d-trace\').style.display = (document.getElementById(\'cakeErr52195e5884a8d-trace\').style.display == \'none\' ? \'\' : \'none\');\"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>5</b>]<div id=\"cakeErr52195e5884a8d-trace\" class=\"cake-stack-trace\" style=\"display: none;\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5884a8d-code\').style.display = (document.getElementById(\'cakeErr52195e5884a8d-code\').style.display == \'none\' ? \'\' : \'none\')\">Code</a> <a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5884a8d-context\').style.display = (document.getElementById(\'cakeErr52195e5884a8d-context\').style.display == \'none\' ? \'\' : \'none\')\">Context</a><pre id=\"cakeErr52195e5884a8d-code\" class=\"cake-code-dump\" style=\"display: none;\"><code><span style=\"color: #000000\"><span style=\"color: #0000BB\">?&gt;</span></span></code>\n<code><span style=\"color: #000000\"><span style=\"color: #0000BB\"></span><span style=\"color: #007700\">&lt;</span><span style=\"color: #0000BB\">div&nbsp;id</span><span style=\"color: #007700\">=</span><span style=\"color: #DD0000\">\":6r\"&nbsp;</span><span style=\"color: #0000BB\">style</span><span style=\"color: #007700\">=</span><span style=\"color: #DD0000\">\"overflow:&nbsp;hidden;\"</span><span style=\"color: #007700\">&gt;</span></span></code>\n<span class=\"code-highlight\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'first_name\'</span><span style=\"color: #007700\">]&nbsp;.&nbsp;</span><span style=\"color: #DD0000\">\"&nbsp;\"&nbsp;</span><span style=\"color: #007700\">.&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'last_name\'</span><span style=\"color: #007700\">]&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>&nbsp;invited&nbsp;you&nbsp;to&nbsp;a&nbsp;Coaching&nbsp;Huddle::&lt;/p&gt;</span></code></span></pre><pre id=\"cakeErr52195e5884a8d-context\" class=\"cake-context\" style=\"display: none;\">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class=\"stack-trace\">include - APP/View/Elements/emails/new_huddle.ctp, line 5\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre>  invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\"><pre class=\"cake-error\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5885397-trace\').style.display = (document.getElementById(\'cakeErr52195e5885397-trace\').style.display == \'none\' ? \'\' : \'none\');\"><b>Notice</b> (8)</a>: Undefined index: AccountFolder [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>7</b>]<div id=\"cakeErr52195e5885397-trace\" class=\"cake-stack-trace\" style=\"display: none;\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5885397-code\').style.display = (document.getElementById(\'cakeErr52195e5885397-code\').style.display == \'none\' ? \'\' : \'none\')\">Code</a> <a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5885397-context\').style.display = (document.getElementById(\'cakeErr52195e5885397-context\').style.display == \'none\' ? \'\' : \'none\')\">Context</a><pre id=\"cakeErr52195e5885397-code\" class=\"cake-code-dump\" style=\"display: none;\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'first_name\'</span><span style=\"color: #007700\">]&nbsp;.&nbsp;</span><span style=\"color: #DD0000\">\"&nbsp;\"&nbsp;</span><span style=\"color: #007700\">.&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'last_name\'</span><span style=\"color: #007700\">]&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>&nbsp;invited&nbsp;you&nbsp;to&nbsp;a&nbsp;Coaching&nbsp;Huddle::&lt;/p&gt;</span></code>\n<code><span style=\"color: #000000\"><span style=\"color: #0000BB\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"color: #007700\">&lt;</span><span style=\"color: #0000BB\">br</span><span style=\"color: #007700\">/&gt;</span></span></code>\n<span class=\"code-highlight\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;h2&nbsp;style=\"color:#3d9bc2\"&gt;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'AccountFolder\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'name\'</span><span style=\"color: #007700\">];&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>&lt;/h2&gt;</span></code></span></pre><pre id=\"cakeErr52195e5885397-context\" class=\"cake-context\" style=\"display: none;\">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class=\"stack-trace\">include - APP/View/Elements/emails/new_huddle.ctp, line 7\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre></h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://localhost/sibme/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read <pre class=\"cake-error\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e588607b-trace\').style.display = (document.getElementById(\'cakeErr52195e588607b-trace\').style.display == \'none\' ? \'\' : \'none\');\"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>12</b>]<div id=\"cakeErr52195e588607b-trace\" class=\"cake-stack-trace\" style=\"display: none;\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e588607b-code\').style.display = (document.getElementById(\'cakeErr52195e588607b-code\').style.display == \'none\' ? \'\' : \'none\')\">Code</a> <a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e588607b-context\').style.display = (document.getElementById(\'cakeErr52195e588607b-context\').style.display == \'none\' ? \'\' : \'none\')\">Context</a><pre id=\"cakeErr52195e588607b-code\" class=\"cake-code-dump\" style=\"display: none;\"><code><span style=\"color: #000000\"><span style=\"color: #0000BB\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"color: #007700\">&lt;</span><span style=\"color: #0000BB\">br</span><span style=\"color: #007700\">/&gt;</span></span></code>\n<code><span style=\"color: #000000\"><span style=\"color: #0000BB\"></span></span></code>\n<span class=\"code-highlight\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;Read&nbsp;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'first_name\'</span><span style=\"color: #007700\">]&nbsp;.&nbsp;</span><span style=\"color: #DD0000\">\"&nbsp;\"&nbsp;</span><span style=\"color: #007700\">.&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'last_name\'</span><span style=\"color: #007700\">]&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>\'s&nbsp;message&nbsp;below:&lt;/p&gt;</span></code></span></pre><pre id=\"cakeErr52195e588607b-context\" class=\"cake-context\" style=\"display: none;\">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class=\"stack-trace\">include - APP/View/Elements/emails/new_huddle.ctp, line 12\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre><pre class=\"cake-error\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e588694a-trace\').style.display = (document.getElementById(\'cakeErr52195e588694a-trace\').style.display == \'none\' ? \'\' : \'none\');\"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>12</b>]<div id=\"cakeErr52195e588694a-trace\" class=\"cake-stack-trace\" style=\"display: none;\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e588694a-code\').style.display = (document.getElementById(\'cakeErr52195e588694a-code\').style.display == \'none\' ? \'\' : \'none\')\">Code</a> <a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e588694a-context\').style.display = (document.getElementById(\'cakeErr52195e588694a-context\').style.display == \'none\' ? \'\' : \'none\')\">Context</a><pre id=\"cakeErr52195e588694a-code\" class=\"cake-code-dump\" style=\"display: none;\"><code><span style=\"color: #000000\"><span style=\"color: #0000BB\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"color: #007700\">&lt;</span><span style=\"color: #0000BB\">br</span><span style=\"color: #007700\">/&gt;</span></span></code>\n<code><span style=\"color: #000000\"><span style=\"color: #0000BB\"></span></span></code>\n<span class=\"code-highlight\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;Read&nbsp;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'first_name\'</span><span style=\"color: #007700\">]&nbsp;.&nbsp;</span><span style=\"color: #DD0000\">\"&nbsp;\"&nbsp;</span><span style=\"color: #007700\">.&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'last_name\'</span><span style=\"color: #007700\">]&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>\'s&nbsp;message&nbsp;below:&lt;/p&gt;</span></code></span></pre><pre id=\"cakeErr52195e588694a-context\" class=\"cake-context\" style=\"display: none;\">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class=\"stack-trace\">include - APP/View/Elements/emails/new_huddle.ctp, line 12\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre> \'s message below:</p>\n    <p><pre class=\"cake-error\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5887219-trace\').style.display = (document.getElementById(\'cakeErr52195e5887219-trace\').style.display == \'none\' ? \'\' : \'none\');\"><b>Notice</b> (8)</a>: Undefined index: account_folders_meta_data [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>13</b>]<div id=\"cakeErr52195e5887219-trace\" class=\"cake-stack-trace\" style=\"display: none;\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5887219-code\').style.display = (document.getElementById(\'cakeErr52195e5887219-code\').style.display == \'none\' ? \'\' : \'none\')\">Code</a> <a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5887219-context\').style.display = (document.getElementById(\'cakeErr52195e5887219-context\').style.display == \'none\' ? \'\' : \'none\')\">Context</a><pre id=\"cakeErr52195e5887219-code\" class=\"cake-code-dump\" style=\"display: none;\"><code><span style=\"color: #000000\"><span style=\"color: #0000BB\"></span></span></code>\n<code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;Read&nbsp;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'first_name\'</span><span style=\"color: #007700\">]&nbsp;.&nbsp;</span><span style=\"color: #DD0000\">\"&nbsp;\"&nbsp;</span><span style=\"color: #007700\">.&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'last_name\'</span><span style=\"color: #007700\">]&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>\'s&nbsp;message&nbsp;below:&lt;/p&gt;</span></code>\n<span class=\"code-highlight\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'account_folders_meta_data\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'message\'</span><span style=\"color: #007700\">];&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>&lt;/p&gt;</span></code></span></pre><pre id=\"cakeErr52195e5887219-context\" class=\"cake-context\" style=\"display: none;\">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class=\"stack-trace\">include - APP/View/Elements/emails/new_huddle.ctp, line 13\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong><pre class=\"cake-error\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5887cf8-trace\').style.display = (document.getElementById(\'cakeErr52195e5887cf8-trace\').style.display == \'none\' ? \'\' : \'none\');\"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>16</b>]<div id=\"cakeErr52195e5887cf8-trace\" class=\"cake-stack-trace\" style=\"display: none;\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5887cf8-code\').style.display = (document.getElementById(\'cakeErr52195e5887cf8-code\').style.display == \'none\' ? \'\' : \'none\')\">Code</a> <a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5887cf8-context\').style.display = (document.getElementById(\'cakeErr52195e5887cf8-context\').style.display == \'none\' ? \'\' : \'none\')\">Context</a><pre id=\"cakeErr52195e5887cf8-code\" class=\"cake-code-dump\" style=\"display: none;\"><code><span style=\"color: #000000\"><span style=\"color: #0000BB\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"color: #007700\">&lt;</span><span style=\"color: #0000BB\">p</span><span style=\"color: #007700\">&gt;</span><span style=\"color: #0000BB\">Still&nbsp;have&nbsp;questions</span><span style=\"color: #007700\">?&lt;/</span><span style=\"color: #0000BB\">p</span><span style=\"color: #007700\">&gt;</span></span></code>\n<code><span style=\"color: #000000\"><span style=\"color: #0000BB\"></span></span></code>\n<span class=\"code-highlight\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;Contact&nbsp;&lt;strong&gt;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'first_name\'</span><span style=\"color: #007700\">]&nbsp;.&nbsp;</span><span style=\"color: #DD0000\">\"&nbsp;\"&nbsp;</span><span style=\"color: #007700\">.&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'last_name\'</span><span style=\"color: #007700\">]&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>&lt;/strong&gt;&nbsp;at&nbsp;&lt;a&nbsp;href=\"mailto:<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'email\'</span><span style=\"color: #007700\">];&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>\"&gt;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'email\'</span><span style=\"color: #007700\">];&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>&lt;/a&gt;&lt;/p&gt;</span></code></span></pre><pre id=\"cakeErr52195e5887cf8-context\" class=\"cake-context\" style=\"display: none;\">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039',b'1',NULL,'2013-08-24 20:31:04',NULL),
	(30,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Khurram Saleem created a new Sibme Coaching Huddle: Mu Huddle','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Khurram Saleem invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">Mu Huddle</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://localhost/sibme/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram Saleem\'s message below:</p>\n    <p>Hello World!!!</p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram Saleem</strong> at <a href=\"mailto:khurri.saleem@gmail.com\">khurri.saleem@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-08-24 20:32:45',1),
	(31,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:41:20',1),
	(32,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:41:24',1),
	(33,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:41:27',1),
	(34,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:41:30',1),
	(35,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:41:57',1),
	(36,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:42:00',1),
	(37,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:42:03',1),
	(38,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:42:06',1),
	(39,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:42:50',1),
	(40,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:42:53',1),
	(41,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:42:56',1),
	(42,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:43:00',1),
	(43,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:43:24',1),
	(44,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:43:27',1),
	(45,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:44:27',1),
	(46,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:44:31',1),
	(47,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Hello',b'1',NULL,'2013-08-29 05:20:11',1),
	(48,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','My Reply to This.',b'1',NULL,'2013-08-29 05:24:05',1),
	(49,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','My Reply to This.',b'1',NULL,'2013-08-29 05:24:09',1),
	(50,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Here you go',b'1',NULL,'2013-08-29 05:41:49',1),
	(51,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Testing..',b'1',NULL,'2013-08-29 05:48:59',1),
	(52,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','My Testing 2',b'1',NULL,'2013-08-29 05:50:43',1),
	(53,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','My Testing 2',b'1',NULL,'2013-08-29 05:50:48',1),
	(54,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','My Testing 2',b'1',NULL,'2013-08-29 05:51:58',1),
	(55,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','My Testing 2',b'1',NULL,'2013-08-29 05:52:02',1),
	(56,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Huddle Discussions','My Testing 2',b'1',NULL,'2013-08-29 05:52:27',1),
	(57,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','My Testing 2',b'1',NULL,'2013-08-29 05:52:31',1),
	(58,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Huddle Discussions','Testing 123',b'1',NULL,'2013-08-29 07:43:15',1),
	(59,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Testing 123',b'1',NULL,'2013-08-29 07:43:16',1),
	(60,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Testing 123',b'1',NULL,'2013-08-29 07:43:24',1),
	(61,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Khurram Saleem created a new Sibme Coaching Huddle: Khurram\'s Coaching classes','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Khurram Saleem invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">Khurram\'s Coaching classes</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://localhost/sibme/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram Saleem\'s message below:</p>\n    <p>Hello Guys!!</p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram Saleem</strong> at <a href=\"mailto:khurri.saleem@gmail.com\">khurri.saleem@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-08-29 07:47:55',1),
	(62,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','test',b'1',NULL,'2013-08-29 09:01:26',1),
	(63,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','test',b'1',NULL,'2013-08-29 09:01:28',1),
	(64,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href=\"#\" target=\"_blank\">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://localhost/sibme/app//Users/login\" target=\"_blank\">http://localhost/sibme/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> khurri    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-02 08:51:09',3),
	(65,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href=\"#\" target=\"_blank\">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://localhost/sibme/app//Users/login\" target=\"_blank\">http://localhost/sibme/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> khurri2    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-02 08:54:52',4),
	(66,'do-not-reply@sibme.com','mdonaho@gmail.com',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Mark Donaho, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href=\"#\" target=\"_blank\">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://localhost/sibme/app//Users/login\" target=\"_blank\">http://localhost/sibme/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> mdonaho    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-02 11:21:12',5),
	(67,'do-not-reply@sibme.com','dmwakefield@gmail.com',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi David Wakefield, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href=\"#\" target=\"_blank\">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://localhost/sibme/app//Users/login\" target=\"_blank\">http://localhost/sibme/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> dmwakefield81    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-02 12:20:50',6),
	(68,'do-not-reply@sibme.com','dmwakefield@gmail.com',NULL,NULL,'David Wakefield created a new Sibme Huddle: Test Huddle','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>David Wakefield invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">Test Huddle</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://localhost/sibme/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read David Wakefield\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>David Wakefield</strong> at <a href=\"mailto:dmwakefield@gmail.com\">dmwakefield@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-02 12:38:43',6),
	(69,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://qa.sibme.com/app//Users/login\" target=\"_blank\">http://qa.sibme.com/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> khurri3    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-02 12:44:15',7),
	(70,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Khurram Saleem created a new Sibme Huddle: My Huddle','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Khurram Saleem invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">My Huddle</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram Saleem\'s message below:</p>\n    <p>Hello Word!!!</p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram Saleem</strong> at <a href=\"mailto:khurri.saleem@gmail.com\">khurri.saleem@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-02 12:44:55',7),
	(71,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Huddle Discussions','Test',b'1',NULL,'2013-09-02 12:46:33',7),
	(72,'do-not-reply@sibme.com','mdonaho@gmail.com',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Mark Donaho, thanks for signing up for your free 60 day trial!</p>\n\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://qa.sibme.com/app//Users/login\" target=\"_blank\">http://qa.sibme.com/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> mdonaho5    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-02 12:50:09',8),
	(73,'do-not-reply@sibme.com','mdonaho@gmail.com',NULL,NULL,'Mark Donaho created a new Sibme Huddle: Test Huddle 1','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Mark Donaho invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">Test Huddle 1</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Mark Donaho\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Mark Donaho</strong> at <a href=\"mailto:mdonaho@gmail.com\">mdonaho@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-02 12:50:27',8),
	(74,'do-not-reply@sibme.com','davew@sibme.com',NULL,NULL,'You\'ve been invited to join \'Larry Karrys\' Sibme Account!','<h4><strong>David Wakefield</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/1679091c5a880faf6fb5e6087eb1b2dc/u25e12rmxlcoorin9efj/4558dbb6f6f8bb2e16d03b85bde76e2c\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>David Wakefield</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>David Wakefield</strong> at dmwakefield@gmail.com</p>\n',b'1',NULL,'2013-09-02 13:11:52',6),
	(75,'do-not-reply@sibme.com','davew@sibme.com',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Larry Karry, thanks for signing up for your free 60 day trial!</p>\n\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://qa.sibme.com/app//Users/login\" target=\"_blank\">http://qa.sibme.com/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> lkarry    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-02 13:18:29',6),
	(76,'do-not-reply@sibme.com','davew@sibme.com',NULL,NULL,'Huddle Discussions','Hey, your lesson really sucked today. &nbsp;No offense.',b'1',NULL,'2013-09-02 13:25:38',6),
	(77,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Khurram Saleem created a new Sibme Huddle: Khurram\'s Coaching classes','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Khurram Saleem invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">Khurram\'s Coaching classes</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram Saleem\'s message below:</p>\n    <p>Testing with Khurram only.</p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram Saleem</strong> at <a href=\"mailto:khurri.saleem@gmail.com\">khurri.saleem@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-02 22:44:33',1),
	(78,'do-not-reply@sibme.com','mdonaho@gmail.com',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Mark Donaho, thanks for signing up for your free 60 day trial!</p>\n\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://qa.sibme.com/app//Users/login\" target=\"_blank\">http://qa.sibme.com/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> mdonaho    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-03 14:18:26',9),
	(79,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Khurram Saleem created a new Sibme Huddle: test','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Khurram Saleem invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">test</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram Saleem\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram Saleem</strong> at <a href=\"mailto:khurri.saleem@gmail.com\">khurri.saleem@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-03 14:21:27',1),
	(80,'do-not-reply@sibme.com','mdonaho@gmail.com',NULL,NULL,'Mark Donaho created a new Sibme Huddle: Test Huddle 1','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Mark Donaho invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">Test Huddle 1</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Mark Donaho\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Mark Donaho</strong> at <a href=\"mailto:mdonaho@gmail.com\">mdonaho@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-03 14:22:32',9),
	(81,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Khurram Saleem created a new Sibme Huddle: Testing 2','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Khurram Saleem invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">Testing 2</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram Saleem\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram Saleem</strong> at <a href=\"mailto:khurri.saleem@gmail.com\">khurri.saleem@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-03 14:29:04',1),
	(82,'do-not-reply@sibme.com','mdonaho@gmail.com',NULL,NULL,'Mark Donaho created a new Sibme Huddle: Test Huddle 2','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Mark Donaho invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">Test Huddle 2</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Mark Donaho\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Mark Donaho</strong> at <a href=\"mailto:mdonaho@gmail.com\">mdonaho@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-03 14:32:51',9),
	(83,'do-not-reply@sibme.com','dmwakefield@gmail.com',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi David Wakefield, thanks for signing up for your free 60 day trial!</p>\n\n    <br/>\n\n    <a href=\"http://qa.sibme.com/app/accounts/accountSettings/1/3\">Subscribe Now</a><br/>\n\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://qa.sibme.com/app/Users/login\" target=\"_blank\">http://qa.sibme.com/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> dmwakefiel    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-04 11:57:53',10),
	(84,'do-not-reply@sibme.com','davew@sibme.com',NULL,NULL,'You\'ve been invited to join \'Larry Karrys\' Sibme Account!','<h4><strong>David Wakefield</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/d3d9446802a44259755d38e6d163e820/677e09724f0e2df9b6c000b75b5da10d/677e09724f0e2df9b6c000b75b5da10d\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>David Wakefield</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>David Wakefield</strong> at dmwakefield@gmail.com</p>\n',b'1',NULL,'2013-09-04 12:02:53',10),
	(85,'do-not-reply@sibme.com','dwakefield81@yahoo.com',NULL,NULL,'You\'ve been invited to join \'Gary Johnsons\' Sibme Account!','<h4><strong>Larry Karry</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/d3d9446802a44259755d38e6d163e820/d554f7bb7be44a7267068a7df88ddd20/d554f7bb7be44a7267068a7df88ddd20\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Larry Karry</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Larry Karry</strong> at davew@sibme.com</p>\n',b'1',NULL,'2013-09-04 12:08:37',10),
	(86,'do-not-reply@sibme.com','dmwakefield@gmail.com',NULL,NULL,'David Wakefield created a new Sibme Huddle: Test huddle','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>David Wakefield invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">Test huddle</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read David Wakefield\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>David Wakefield</strong> at <a href=\"mailto:dmwakefield@gmail.com\">dmwakefield@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-04 12:21:41',10),
	(87,'do-not-reply@sibme.com','davew@sibme.com',NULL,NULL,'Huddle Discussions','Hi there.',b'1',NULL,'2013-09-04 12:25:58',10),
	(88,'do-not-reply@sibme.com','davew@sibme.com',NULL,NULL,'Huddle Discussions','',b'1',NULL,'2013-09-04 12:27:04',10),
	(89,'do-not-reply@sibme.com','info@3ssolutions.net',NULL,NULL,'You\'ve been invited to join \'Khurram 3Ss\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/795c7a7a5ec6b460ec00c5841019b9e9/795c7a7a5ec6b460ec00c5841019b9e9\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-09-04 15:16:58',1),
	(90,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/fa3a3c407f82377f55c19c5d403335c7/fa3a3c407f82377f55c19c5d403335c7\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-09-04 15:47:38',1),
	(91,'do-not-reply@sibme.com','khurri10@hotmail.com',NULL,NULL,'You\'ve been invited to join \'khurram 3s\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/c2626d850c80ea07e7511bbae4c76f4b/c2626d850c80ea07e7511bbae4c76f4b\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-09-04 15:52:43',1),
	(92,'do-not-reply@sibme.com','k@k.com',NULL,NULL,'You\'ve been invited to join \'k@k.com s\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/ce78d1da254c0843eb23951ae077ff5f/ce78d1da254c0843eb23951ae077ff5f\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-09-04 15:57:30',1),
	(93,'do-not-reply@sibme.com','tfaimprovement@yahoo.com',NULL,NULL,'You\'ve been invited to join \'lindsey Smiths\' Sibme Account!','<h4><strong>David Wakefield</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/d3d9446802a44259755d38e6d163e820/8e82ab7243b7c66d768f1b8ce1c967eb/8e82ab7243b7c66d768f1b8ce1c967eb\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>David Wakefield</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>David Wakefield</strong> at dmwakefield@gmail.com</p>\n',b'1',NULL,'2013-09-05 10:01:08',10),
	(94,'do-not-reply@sibme.com','dmwakefield@gmail.com',NULL,NULL,'David Wakefield created a new Sibme Huddle: Chem huddle','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>David Wakefield invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">Chem huddle</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read David Wakefield\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>David Wakefield</strong> at <a href=\"mailto:dmwakefield@gmail.com\">dmwakefield@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-05 10:11:05',10),
	(95,'do-not-reply@sibme.com','davew@sibme.com',NULL,NULL,'Huddle Discussions','Awesome',b'1',NULL,'2013-09-05 10:14:29',10),
	(96,'do-not-reply@sibme.com','tfaimprovement@yahoo.com',NULL,NULL,'Huddle Discussions','Hi',b'1',NULL,'2013-09-09 14:17:00',10),
	(97,'do-not-reply@sibme.com','dmwakefield@gmail.com',NULL,NULL,'David Wakefield created a new Sibme Huddle: English huddle','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>David Wakefield invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">English huddle</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read David Wakefield\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>David Wakefield</strong> at <a href=\"mailto:dmwakefield@gmail.com\">dmwakefield@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-09 15:31:57',10),
	(98,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Supers\' Sibme Account!','<h4><strong>Sibme</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/e0ec453e28e061cc58ac43f91dc2f3f0/e0ec453e28e061cc58ac43f91dc2f3f0\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p>Hi Khurram</p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-09-11 10:09:11',1),
	(99,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'You\'ve been invited to join Sibme\'s Account!','<h4><strong>Sibme</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/7250eb93b3c18cc9daa29cf58af7a004/7250eb93b3c18cc9daa29cf58af7a004\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-09-11 13:41:45',1),
	(100,'do-not-reply@sibme.com','ram@rgprasad.com',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Ram Prasad, thanks for signing up for your free 60 day trial!</p>\n\n    <br/>\n\n    <a href=\"http://qa.sibme.com/app/accounts/accountSettings/1/3\">Subscribe Now</a><br/>\n\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://qa.sibme.com/app/Users/login\" target=\"_blank\">http://qa.sibme.com/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> rprasad    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-11 21:45:35',11),
	(101,'hamidbscs8@gmail.com','khurri.saleem@gmail.com',NULL,NULL,'Technology Support','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat,',b'0',NULL,'2013-09-13 12:00:04',NULL),
	(102,'hamidbscs8@gmail.com','khurri.saleem@gmail.com',NULL,NULL,'General Questions','Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat,',b'0',NULL,'2013-09-13 12:04:01',NULL),
	(103,'do-not-reply@sibme.com','hamidbscs8@gmail.com',NULL,NULL,'You\'ve been invited to join Sibme\'s Account!','<h4><strong>Sibme</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/301ad0e3bd5cb1627a2044908a42fdc2/301ad0e3bd5cb1627a2044908a42fdc2\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-09-13 12:08:18',1),
	(104,'Khurram Saleem<khurri.saleem@gmail.com>','Muhamad hamid<hamidbscs8@gmail.com>',NULL,NULL,'Khurram Saleem Message','<ol><li><b><i>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat,</i></b></li></ol>',b'0',NULL,'2013-09-13 12:13:17',1),
	(105,'do-not-reply@sibme.com','hwattoo@3ssolutions.net',NULL,NULL,'You\'ve been invited to join Sibme\'s Account!','<h4><strong>Sibme</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/4d5b995358e7798bc7e9d9db83c612a5/4d5b995358e7798bc7e9d9db83c612a5\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-09-13 12:24:02',1),
	(106,'k@k.com','khurri.saleem@gmail.com',NULL,NULL,'Technology Support','Hello world',b'0',NULL,'2013-09-13 12:54:38',NULL),
	(107,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br/>\n\n    <a href=\"http://qa.sibme.com/app/accounts/accountSettings/1/3\">Subscribe Now</a><br/>\n\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://qa.sibme.com/app/Users/login\" target=\"_blank\">http://qa.sibme.com/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> ksaleem    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-13 13:00:49',12),
	(108,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Khurram Saleem created a new Sibme Huddle: Khurram\'s Coaching classes','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Khurram Saleem invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">Khurram\'s Coaching classes</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram Saleem\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram Saleem</strong> at <a href=\"mailto:khurri.saleem@gmail.com\">khurri.saleem@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-13 13:19:01',12),
	(109,'do-not-reply@sibme.com','dmwakefield@gmail.com',NULL,NULL,'You\'ve been invited to join 3S\'s Account!','<h4><strong>3S</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/c20ad4d76fe97759aa27a0c99bff6710/8cyukqitulco1kol2yqq/632cee946db83e7a52ce5e8d6f0fed35\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-09-13 13:24:48',12),
	(110,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join 3S\'s Account!','<h4><strong>3S</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/c20ad4d76fe97759aa27a0c99bff6710/b0b183c207f46f0cca7dc63b2604f5cc/b0b183c207f46f0cca7dc63b2604f5cc\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-09-13 13:26:52',12),
	(111,'Khurram Saleem<khurri.saleem@gmail.com>','David Wakefield<dmwakefield@gmail.com>',NULL,NULL,'Khurram Saleem Message','Testing...',b'0',NULL,'2013-09-13 14:21:00',12),
	(112,'d@mloolm.com','khurri.saleem@gmail.com',NULL,NULL,'Technology Support','testing QA page',b'0',NULL,'2013-09-15 18:40:08',NULL),
	(113,'do-not-reply@sibme.com','super.user1@hotm.com',NULL,NULL,'You\'ve been invited to join MySchool\'s Account!','<h4><strong>MySchool</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/f9028faec74be6ec9b852b0a542e2f39/f9028faec74be6ec9b852b0a542e2f39\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ram@rgprasad.com</p>\n',b'1',NULL,'2013-09-15 18:44:02',11),
	(114,'do-not-reply@sibme.com','user2@gm.com',NULL,NULL,'You\'ve been invited to join MySchool\'s Account!','<h4><strong>MySchool</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/8f7d807e1f53eff5f9efbe5cb81090fb/8f7d807e1f53eff5f9efbe5cb81090fb\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ram@rgprasad.com</p>\n',b'1',NULL,'2013-09-15 18:44:46',11),
	(115,'do-not-reply@sibme.com','user3@gm.com',NULL,NULL,'You\'ve been invited to join MySchool\'s Account!','<h4><strong>MySchool</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/fa83a11a198d5a7f0bf77a1987bcd006/fa83a11a198d5a7f0bf77a1987bcd006\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ram@rgprasad.com</p>\n',b'1',NULL,'2013-09-15 18:44:46',11),
	(116,'do-not-reply@sibme.com','one.user@ml.com',NULL,NULL,'You\'ve been invited to join MySchool\'s Account!','<h4><strong>MySchool</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/02a32ad2669e6fe298e607fe7cc0e1a0/02a32ad2669e6fe298e607fe7cc0e1a0\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ram@rgprasad.com</p>\n',b'1',NULL,'2013-09-15 18:45:46',11),
	(117,'do-not-reply@sibme.com','one@lm.org',NULL,NULL,'You\'ve been invited to join MySchool\'s Account!','<h4><strong>MySchool</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/fc3cf452d3da8402bebb765225ce8c0e/fc3cf452d3da8402bebb765225ce8c0e\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ram@rgprasad.com</p>\n',b'1',NULL,'2013-09-15 18:46:23',11),
	(118,'do-not-reply@sibme.com','two@lm.edu',NULL,NULL,'You\'ve been invited to join MySchool\'s Account!','<h4><strong>MySchool</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/3d8e28caf901313a554cebc7d32e67e5/3d8e28caf901313a554cebc7d32e67e5\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ram@rgprasad.com</p>\n',b'1',NULL,'2013-09-15 18:46:24',11),
	(119,'do-not-reply@sibme.com','one@me.c',NULL,NULL,'You\'ve been invited to join MySchool\'s Account!','<h4><strong>MySchool</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/e97ee2054defb209c35fe4dc94599061/e97ee2054defb209c35fe4dc94599061\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ram@rgprasad.com</p>\n',b'1',NULL,'2013-09-15 18:48:17',11),
	(120,'do-not-reply@sibme.com','one@mm.com',NULL,NULL,'You\'ve been invited to join MySchool\'s Account!','<h4><strong>MySchool</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/b86e8d03fe992d1b0e19656875ee557c/b86e8d03fe992d1b0e19656875ee557c\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ram@rgprasad.com</p>\n',b'1',NULL,'2013-09-15 18:49:34',11),
	(121,'do-not-reply@sibme.com','plain.user1@mm.com',NULL,NULL,'You\'ve been invited to join MySchool\'s Account!','<h4><strong>MySchool</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/84f7e69969dea92a925508f7c1f9579a/84f7e69969dea92a925508f7c1f9579a\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ram@rgprasad.com</p>\n',b'1',NULL,'2013-09-15 18:50:23',11),
	(122,'do-not-reply@sibme.com','two@mll.com',NULL,NULL,'You\'ve been invited to join MySchool\'s Account!','<h4><strong>MySchool</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/f4552671f8909587cf485ea990207f3b/f4552671f8909587cf485ea990207f3b\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ram@rgprasad.com</p>\n',b'1',NULL,'2013-09-15 18:50:23',11),
	(123,'do-not-reply@sibme.com','onuser@mm.com',NULL,NULL,'You\'ve been invited to join MySchool\'s Account!','<h4><strong>MySchool</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/362e80d4df43b03ae6d3f8540cd63626/362e80d4df43b03ae6d3f8540cd63626\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ram@rgprasad.com</p>\n',b'1',NULL,'2013-09-15 18:52:02',11),
	(124,'do-not-reply@sibme.com','rgprasad5@gmail.com',NULL,NULL,'You\'ve been invited to join MySchool\'s Account!','<h4><strong>MySchool</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/fe8c15fed5f808006ce95eddb7366e35/fe8c15fed5f808006ce95eddb7366e35\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ram@rgprasad.com</p>\n',b'1',NULL,'2013-09-15 18:52:34',11),
	(125,'do-not-reply@sibme.com','joh.doe@gmail.com',NULL,NULL,'You\'ve been invited to join MySchool\'s Account!','<h4><strong>MySchool</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/6512bd43d9caa6e02c990b0a82652dca/1efa39bcaec6f3900149160693694536/1efa39bcaec6f3900149160693694536\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ram@rgprasad.com</p>\n',b'1',NULL,'2013-09-15 18:53:08',11),
	(126,'do-not-reply@sibme.com','ramgprasad5@gmail.com',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Ram Prasad, thanks for signing up for your free 60 day trial!</p>\n\n    <br/>\n\n    <a href=\"http://qa.sibme.com/app/accounts/accountSettings/1/3\">Subscribe Now</a><br/>\n\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://qa.sibme.com/app/Users/login\" target=\"_blank\">http://qa.sibme.com/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> rprasad3    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-15 19:08:45',13),
	(127,'do-not-reply@sibme.com','us.cm@cm.com',NULL,NULL,'You\'ve been invited to join School3\'s Account!','<h4><strong>School3</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/c51ce410c124a10e0db5e4b97fc2af39/22ac3c5a5bf0b520d281c122d1490650/22ac3c5a5bf0b520d281c122d1490650\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ramgprasad5@gmail.com</p>\n',b'1',NULL,'2013-09-15 19:10:12',13),
	(128,'do-not-reply@sibme.com','su@me.comd',NULL,NULL,'You\'ve been invited to join School3\'s Account!','<h4><strong>School3</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/c51ce410c124a10e0db5e4b97fc2af39/aff1621254f7c1be92f64550478c56e6/aff1621254f7c1be92f64550478c56e6\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Ram Prasad</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Ram Prasad</strong> at ramgprasad5@gmail.com</p>\n',b'1',NULL,'2013-09-15 19:10:44',13),
	(129,'do-not-reply@sibme.com','ramgprasad5@gmail.com',NULL,NULL,'Ram Prasad created a new Sibme Huddle: Coach Huddle','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Ram Prasad invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">Coach Huddle</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Ram Prasad\'s message below:</p>\n    <p><blockquote>fhhs mszzV</blockquote></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Ram Prasad</strong> at <a href=\"mailto:ramgprasad5@gmail.com\">ramgprasad5@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-15 19:27:02',13),
	(130,'do-not-reply@sibme.com','ramgprasad5@gmail.com',NULL,NULL,'Ram Prasad created a new Sibme Huddle: huddle 2','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Ram Prasad invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">huddle 2</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Ram Prasad\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Ram Prasad</strong> at <a href=\"mailto:ramgprasad5@gmail.com\">ramgprasad5@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-15 19:27:32',13),
	(131,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br/>\n\n    <a href=\"http://qa.sibme.com/app/accounts/accountSettings/1/3\">Subscribe Now</a><br/>\n\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://qa.sibme.com/app/Users/login\" target=\"_blank\">http://qa.sibme.com/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> ksaleem    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-16 06:24:53',14),
	(132,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'You\'ve been invited to join 3S\'s Account!','<h4><strong>3S</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/aab3238922bcc25a6f606eb525ffdc56/addfa9b7e234254d26e9c7f2af1005cb/addfa9b7e234254d26e9c7f2af1005cb\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-09-16 06:26:06',14),
	(133,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join 3S\'s Account!','<h4><strong>3S</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/aab3238922bcc25a6f606eb525ffdc56/8c235f89a8143a28a1d6067e959dd858/8c235f89a8143a28a1d6067e959dd858\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram MIYW</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram MIYW</strong> at khurram@makeityourweb.com</p>\n',b'1',NULL,'2013-09-16 06:44:13',14),
	(134,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Khurram MIYW created a new Sibme Huddle: MIYW Huddle','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Khurram MIYW invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">MIYW Huddle</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram MIYW\'s message below:</p>\n    <p>Hello Here is MIYW huddle.</p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram MIYW</strong> at <a href=\"mailto:khurram@makeityourweb.com\">khurram@makeityourweb.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-16 06:44:14',14),
	(135,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join 3S\'s Account!','<h4><strong>3S</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/aab3238922bcc25a6f606eb525ffdc56/847cc55b7032108eee6dd897f3bca8a5/847cc55b7032108eee6dd897f3bca8a5\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram MIYW</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram MIYW</strong> at khurram@makeityourweb.com</p>\n',b'1',NULL,'2013-09-16 06:46:23',14),
	(136,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Khurram MIYW created a new Sibme Huddle: MIYW Huddle','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Khurram MIYW invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">MIYW Huddle</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram MIYW\'s message below:</p>\n    <p>Hello Here is MIYW huddle.</p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram MIYW</strong> at <a href=\"mailto:khurram@makeityourweb.com\">khurram@makeityourweb.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-16 06:46:23',14),
	(137,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Khurram MIYW created a new Sibme Huddle: Testing for Me','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Khurram MIYW invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">Testing for Me</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram MIYW\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram MIYW</strong> at <a href=\"mailto:khurram@makeityourweb.com\">khurram@makeityourweb.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-16 08:45:22',14),
	(138,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Khurram Saleem created a new Sibme Huddle: ksaleem Huddle Only','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Khurram Saleem invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">ksaleem Huddle Only</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram Saleem\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram Saleem</strong> at <a href=\"mailto:khurri.saleem@gmail.com\">khurri.saleem@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-16 09:13:03',14),
	(139,'hamidbscs8@gmail.com','khurri.saleem@gmail.com',NULL,NULL,NULL,'sdfsadfsadf',b'0',NULL,'2013-09-17 07:51:08',NULL),
	(140,'k@k.com','khurri.saleem@gmail.com',NULL,NULL,'Technology Support','asdssd',b'0',NULL,'2013-09-17 08:22:24',NULL),
	(141,'k@k.com','khurri.saleem@gmail.com',NULL,NULL,'Technology Support','asdds',b'0',NULL,'2013-09-17 08:24:02',NULL),
	(142,'hamidbscs8@gmail.com','khurri.saleem@gmail.com',NULL,NULL,'General Questions','test',b'0',NULL,'2013-09-17 08:37:36',NULL),
	(143,'do-not-reply@sibme.com','davew@sibme.com',NULL,NULL,'Huddle Discussions','Hi there',b'1',NULL,'2013-09-17 11:41:33',10),
	(144,'do-not-reply@sibme.com','dwakefield81@yahoo.com',NULL,NULL,'Huddle Discussions','Hi there',b'1',NULL,'2013-09-17 11:41:34',10),
	(145,'David Wakefield<dmwakefield@gmail.com>','Larry Karry<davew@sibme.com>',NULL,NULL,'David Wakefield Message','Hey what\'s up',b'0',NULL,'2013-09-17 11:43:40',10),
	(146,'David Wakefield<dmwakefield@gmail.com>','Larry Karry<davew@sibme.com>',NULL,NULL,'David Wakefield Message','Hey what\'s up',b'0',NULL,'2013-09-17 11:43:46',10),
	(147,'do-not-reply@sibme.com','tfaimprovement@yahoo.com',NULL,NULL,'lindsey Smith created a new Sibme Huddle: New Huddle','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>lindsey Smith invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">New Huddle</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read lindsey Smith\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>lindsey Smith</strong> at <a href=\"mailto:tfaimprovement@yahoo.com\">tfaimprovement@yahoo.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-17 12:02:29',10),
	(148,'do-not-reply@sibme.com','hamidbscs8@gmail.com',NULL,NULL,'You\'ve been invited to join 3S\'s Account!','<h4><strong>3S</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://qa.sibme.com/app/Users/activation/aab3238922bcc25a6f606eb525ffdc56/c6sgnjbvoxl6yntjg8tq/301ad0e3bd5cb1627a2044908a42fdc2\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram JJ</strong>âs message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram JJ</strong> at khurram@jjtestsite.us</p>\n',b'1',NULL,'2013-09-17 13:23:57',14),
	(149,'do-not-reply@sibme.com','davew@sibme.com',NULL,NULL,'Huddle Discussions','Hello jisjd fijs d',b'1',NULL,'2013-09-17 13:50:01',10),
	(150,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Khurram Saleem created a new Sibme Huddle: English Department Meeting - 1/9/13','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Khurram Saleem invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">English Department Meeting - 1/9/13</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://qa.sibme.com/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram Saleem\'s message below:</p>\n    <p></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram Saleem</strong> at <a href=\"mailto:khurri.saleem@gmail.com\">khurri.saleem@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-09-17 14:00:08',14),
	(151,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','English Department Meeting - 1/9/13<br>',b'1',NULL,'2013-09-17 14:00:33',14),
	(152,'do-not-reply@sibme.com','hamidbscs8@gmail.com',NULL,NULL,'Huddle Discussions','English Department Meeting - 1/9/13<br>',b'1',NULL,'2013-09-17 14:00:33',14),
	(153,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','English Department Meeting - 1/9/13<br>',b'1',NULL,'2013-09-17 14:00:33',14);

/*!40000 ALTER TABLE `audit_emails` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comment_attachments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comment_attachments`;

CREATE TABLE `comment_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `comment_attachments` WRITE;
/*!40000 ALTER TABLE `comment_attachments` DISABLE KEYS */;

INSERT INTO `comment_attachments` (`id`, `comment_id`, `document_id`)
VALUES
	(19,30,92),
	(20,52,145),
	(21,61,265);

/*!40000 ALTER TABLE `comment_attachments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comment_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comment_users`;

CREATE TABLE `comment_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) NOT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `comment_user_type` tinyint(4) DEFAULT NULL COMMENT '1=notification,2=recipient',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `comment_users` WRITE;
/*!40000 ALTER TABLE `comment_users` DISABLE KEYS */;

INSERT INTO `comment_users` (`id`, `comment_id`, `user_id`, `comment_user_type`)
VALUES
	(101,25,'824',1),
	(102,26,'824',1),
	(103,30,'824',1),
	(104,34,'830',1),
	(105,64,'824',1),
	(106,64,'825',1),
	(107,67,'824',1),
	(108,70,'855',1),
	(109,70,'834',1),
	(110,70,'857',1);

/*!40000 ALTER TABLE `comment_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_comment_id` int(11) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `ref_type` tinyint(4) NOT NULL COMMENT '1=huddle(folder),2=document,3=comment',
  `ref_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `time` int(11) DEFAULT NULL,
  `restrict_to_users` bit(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  `active` enum('1','0') NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_comments_on_commentable_id` (`ref_id`),
  KEY `index_comments_on_user_id` (`user_id`),
  CONSTRAINT `fk_comments_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;

INSERT INTO `comments` (`id`, `parent_comment_id`, `title`, `comment`, `ref_type`, `ref_id`, `user_id`, `time`, `restrict_to_users`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`, `active`)
VALUES
	(27,NULL,'','this needs to be improved big time.',1,25,823,NULL,b'0',823,'2013-09-04 12:27:37',823,'2013-09-04 12:27:37','1'),
	(28,NULL,NULL,'Does it work?',2,89,823,98,b'0',823,'2013-09-04 12:52:54',823,'2013-09-04 12:52:54','1'),
	(29,NULL,NULL,'testing',2,89,823,122,b'0',823,'2013-09-05 09:57:49',823,'2013-09-05 09:57:49','1'),
	(31,NULL,NULL,'stop',2,89,823,148,b'0',823,'2013-09-06 11:19:28',823,'2013-09-06 11:19:28','1'),
	(32,NULL,NULL,'sdfsdf',2,89,823,81,b'0',823,'2013-09-08 19:02:20',823,'2013-09-08 19:02:20','1'),
	(33,NULL,NULL,'sdfsafs',2,89,823,179,b'0',823,'2013-09-08 19:02:37',823,'2013-09-08 19:02:37','1'),
	(34,NULL,'Test discussion','Hi',1,34,823,NULL,b'0',823,'2013-09-09 14:17:00',823,'2013-09-09 14:17:00',''),
	(35,NULL,NULL,'hi',2,98,823,70,b'0',823,'2013-09-09 14:42:55',823,'2013-09-09 14:42:55','1'),
	(36,NULL,NULL,'hey',2,98,823,146,b'0',823,'2013-09-11 20:57:28',823,'2013-09-11 20:57:28','1'),
	(39,NULL,NULL,'Testing',2,104,823,4,b'0',823,'2013-09-12 19:58:50',823,'2013-09-12 19:58:50','1'),
	(44,NULL,NULL,'tested',2,NULL,835,6,b'0',835,'2013-09-13 12:37:17',835,'2013-09-13 12:37:17','1'),
	(45,NULL,NULL,'tested',2,113,835,0,b'0',835,'2013-09-13 12:37:23',835,'2013-09-13 12:37:23','1'),
	(53,NULL,NULL,'Hhhhhjj',2,89,823,NULL,b'0',823,'2013-09-14 07:41:38',823,'2013-09-14 07:41:38','1'),
	(54,NULL,'Discussion1','<span><span>let&nbsp;us discudad</span>dad<br></span>dadada<br>',1,40,851,NULL,b'0',851,'2013-09-15 19:28:44',851,'2013-09-15 19:28:44','1'),
	(55,NULL,'Discussion2','discus3',1,40,851,NULL,b'0',851,'2013-09-15 19:29:15',851,'2013-09-15 19:29:15','1'),
	(56,NULL,'','a second comment',1,54,851,NULL,b'0',851,'2013-09-15 19:30:43',851,'2013-09-15 19:30:43','1'),
	(57,NULL,NULL,'Testing',2,251,855,0,b'0',855,'2013-09-16 09:13:58',855,'2013-09-16 09:13:58','1'),
	(58,NULL,NULL,'hi',2,233,823,0,b'0',823,'2013-09-16 14:54:18',823,'2013-09-16 14:54:18','1'),
	(59,NULL,'Wendesday PLC','Irritated with this process.',1,34,823,NULL,b'0',823,'2013-09-17 11:35:24',823,'2013-09-17 11:35:24','1'),
	(60,NULL,'','Hi there.',1,59,823,NULL,b'0',823,'2013-09-17 11:36:19',823,'2013-09-17 11:36:19','1'),
	(61,NULL,'Please','I don\'',1,32,823,NULL,b'0',823,'2013-09-17 11:40:25',823,'2013-09-17 11:40:25','1'),
	(62,NULL,'','',1,61,823,NULL,b'0',823,'2013-09-17 11:41:18',823,'2013-09-17 11:41:18','1'),
	(63,NULL,'','Hi there',1,61,823,NULL,b'0',823,'2013-09-17 11:41:25',823,'2013-09-17 11:41:25','1'),
	(64,NULL,'','Hi there',1,61,823,NULL,b'0',823,'2013-09-17 11:41:33',823,'2013-09-17 11:41:33','1'),
	(65,NULL,'Test Discussion','Yeah',1,36,823,NULL,b'0',823,'2013-09-17 13:49:23',823,'2013-09-17 13:49:23','1'),
	(66,NULL,'','Hello jisjd fijs d',1,65,823,NULL,b'0',823,'2013-09-17 13:49:47',823,'2013-09-17 13:49:47','1'),
	(67,NULL,'','Hello jisjd fijs d',1,65,823,NULL,b'0',823,'2013-09-17 13:50:01',823,'2013-09-17 13:50:01','1'),
	(68,NULL,'','Hello jisjd fijs d',1,65,823,NULL,b'0',823,'2013-09-17 13:50:38',823,'2013-09-17 13:50:38','1'),
	(69,NULL,'','Hello jisjd fijs d',1,65,823,NULL,b'0',823,'2013-09-17 13:51:09',823,'2013-09-17 13:51:09','1'),
	(70,NULL,'English Department Meeting - 1/9/13 discussions','English Department Meeting - 1/9/13<br>',1,55,854,NULL,b'0',854,'2013-09-17 14:00:33',854,'2013-09-17 14:00:33','1'),
	(71,NULL,'','Test Reply Comment<br>',1,70,854,NULL,b'0',854,'2013-09-17 14:01:03',854,'2013-09-17 14:01:03','1');

/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table document_meta_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `document_meta_data`;

CREATE TABLE `document_meta_data` (
  `document_meta_data_id` int(11) NOT NULL,
  `meta_data_name` varchar(64) NOT NULL COMMENT 'tag, topic, subject, etc',
  `meta_data_value` varchar(255) NOT NULL,
  PRIMARY KEY (`document_meta_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `doc_type` tinyint(3) NOT NULL COMMENT '1=video, 2=document',
  `url` varchar(4000) DEFAULT NULL,
  `original_file_name` varchar(1024) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  `content_type` varchar(128) DEFAULT NULL,
  `published` int(11) NOT NULL DEFAULT '0',
  `job_id` int(11) DEFAULT NULL,
  `zencoder_output_id` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL DEFAULT b'0',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `zencoder_output` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;

INSERT INTO `documents` (`id`, `account_id`, `doc_type`, `url`, `original_file_name`, `file_size`, `view_count`, `content_type`, `published`, `job_id`, `zencoder_output_id`, `active`, `created_date`, `created_by`, `last_edit_date`, `last_edit_by`, `zencoder_output`)
VALUES
	(85,1,1,'1/29/2013/09/03/vXRQqKKQTH2OLX3Mo30j_kupload.mov','kupload.mov',623275,10,NULL,1,NULL,'57543160',b'1','2013-09-03 02:41:26',1,'2013-09-03 02:41:26',1,NULL),
	(86,1,1,'1//2013/09/13/ZI67VOqQ8CaGC28ofay4_Movieon42213at420AMcopycopycopy.mov','Movieon42213at420AMcopycopycopy.mov',623275,12,NULL,1,NULL,'58503594',b'1','2013-09-03 11:04:22',1,'2013-09-13 11:47:47',1,NULL),
	(87,10,1,'10/33/2013/09/04/wilQm77SAWTZvABvSAQb_IMG_0507.mov','IMG_0507.mov',97157612,8,NULL,1,NULL,'57627118',b'1','2013-09-04 12:29:19',823,'2013-09-04 12:29:19',823,NULL),
	(89,10,1,'10/32/2013/09/04/MabGxu7ASpOBSnwgP4gz_IMG_0507.mov','IMG_0507.mov',97157612,18,NULL,1,NULL,'57628905',b'1','2013-09-04 12:47:13',823,'2013-09-04 12:47:13',823,NULL),
	(90,10,2,'10/32/2013/09/04/gdkTyn12R9eHNZYcaT8i_Bug Fixes.docx','Bug Fixes.docx',108216,0,NULL,1,NULL,NULL,b'1','2013-09-04 12:54:30',823,'2013-09-04 12:54:30',823,NULL),
	(91,10,2,'10/32/2013/09/04/bACCxuNqRlG6f39WJX9b_Dallas Exhibit Brochure.pdf','Dallas Exhibit Brochure.pdf',1252782,0,NULL,1,NULL,NULL,b'1','2013-09-04 12:54:50',823,'2013-09-04 12:54:50',823,NULL),
	(92,10,1,'10/32/2013/09/05/Bug-Fixes.docx','Bug-Fixes.docx',NULL,0,NULL,1,NULL,NULL,b'1','2013-09-05 10:14:29',823,'2013-09-05 10:14:29',823,NULL),
	(93,1,1,'1/29/2013/09/09/appsibme09092013101454comp.MOV','appsibme09092013101454comp.MOV',24575,0,NULL,1,NULL,'58080451',b'1','2013-09-09 12:28:34',1,'2013-09-09 12:28:34',1,NULL),
	(94,1,1,'1/29/2013/09/09/appsibme09092013101454comp.MOV','appsibme09092013101454comp.MOV',24575,0,NULL,1,NULL,'58081739',b'1','2013-09-09 12:39:52',1,'2013-09-09 12:39:52',1,NULL),
	(95,1,1,'1/29/2013/09/09/appsibme09092013101454comp.MOV','appsibme09092013101454comp.MOV',24575,0,NULL,1,NULL,'58081882',b'1','2013-09-09 12:40:54',1,'2013-09-09 12:40:54',1,NULL),
	(96,1,1,'1/29/2013/09/09/appsibme09092013112107comp.MOV','appsibme09092013112107comp.MOV',202143,0,NULL,1,NULL,'58087709',b'1','2013-09-09 01:42:38',1,'2013-09-09 01:42:38',1,NULL),
	(97,1,1,'1/29/2013/09/09/appsibme09092013114752comp.MOV','appsibme09092013114752comp.MOV',1719157,0,NULL,1,NULL,'58088622',b'1','2013-09-09 01:51:39',1,'2013-09-09 01:51:39',1,NULL),
	(98,10,1,'10/34/2013/09/09/UFufXLRaSFarxjng7dVF_IMG_0507.mov','IMG_0507.mov',97157612,11,NULL,1,NULL,'58092736',b'1','2013-09-09 02:33:08',823,'2013-09-09 02:33:08',823,NULL),
	(99,10,2,'10/34/2013/09/09/3OB2RLbqQf6w7UlowTLB_Bug Fixes.docx','Bug Fixes.docx',108216,0,NULL,1,NULL,NULL,b'1','2013-09-09 02:36:22',823,'2013-09-09 02:36:22',823,NULL),
	(100,10,2,'10/34/2013/09/09/8RaVkmVRNScnNGkOr5tG_GettingReal.pdf','GettingReal.pdf',497909,0,NULL,1,NULL,NULL,b'1','2013-09-09 02:37:37',823,'2013-09-09 02:37:37',823,NULL),
	(101,10,1,'10/32/2013/09/09/appsibme09092013023613comp.MOV','appsibme09092013023613comp.MOV',1615461,1,NULL,1,NULL,'58093426',b'1','2013-09-09 02:41:07',823,'2013-09-09 02:41:07',823,NULL),
	(102,10,2,'10/34/2013/09/09/uePmmwK8RZmEgMuntAeE_Dallas Exhibit Brochure.pdf','Dallas Exhibit Brochure.pdf',1252782,0,NULL,1,NULL,NULL,b'1','2013-09-09 02:42:19',823,'2013-09-09 02:42:19',823,NULL),
	(103,10,1,'10/35/2013/09/09/8kTlwZjBRhOCDQP2mjQY_IMG_0507.mov','IMG_0507.mov',97157612,14,NULL,1,NULL,'58093817',b'1','2013-09-09 02:45:24',823,'2013-09-09 02:45:24',823,NULL),
	(104,10,1,'10/34/2013/09/09/appsibme09092013024538comp.MOV','appsibme09092013024538comp.MOV',805313,6,NULL,1,NULL,'58094098',b'1','2013-09-09 02:47:56',823,'2013-09-09 02:47:56',823,NULL),
	(105,10,1,'10/32/2013/09/09/appsibme09092013024538comp.MOV','appsibme09092013024538comp.MOV',805313,0,NULL,1,NULL,'58094402',b'1','2013-09-09 02:51:39',823,'2013-09-09 02:51:39',823,NULL),
	(106,10,1,'10/32/2013/09/09/appsibme09092013023613comp.MOV','appsibme09092013023613comp.MOV',1615461,0,NULL,1,NULL,'58099453',b'1','2013-09-09 03:42:55',823,'2013-09-09 03:42:55',823,NULL),
	(107,1,1,'1/29/2013/09/09/appsibme09092013114752comp.MOV','appsibme09092013114752comp.MOV',1719157,0,NULL,1,NULL,'58101997',b'1','2013-09-09 04:11:06',1,'2013-09-09 04:11:06',1,NULL),
	(108,1,1,'1/29/2013/09/09/trimEo1ymAcomp.MOV','trimEo1ymAcomp.MOV',23258005,3,NULL,1,NULL,'58102381',b'1','2013-09-09 04:15:25',1,'2013-09-09 04:15:25',1,NULL),
	(109,1,1,'1/27/2013/09/09/appsibme09092013042155comp.MOV','appsibme09092013042155comp.MOV',301591,0,NULL,1,NULL,'58103762',b'1','2013-09-09 04:27:27',1,'2013-09-09 04:27:27',1,NULL),
	(110,1,1,'1/27/2013/09/09/appsibme09092013042155comp.MOV','appsibme09092013042155comp.MOV',0,0,NULL,1,NULL,'58103764',b'1','2013-09-09 04:27:28',1,'2013-09-09 04:27:28',1,NULL),
	(111,1,1,'1/29/2013/09/09/appsibme09092013033122comp.MOV','appsibme09092013033122comp.MOV',670570,0,NULL,1,NULL,'58103958',b'1','2013-09-09 04:28:51',1,'2013-09-09 04:28:51',1,NULL),
	(112,1,1,'1/26/2013/09/09/appsibme09092013033404comp.MOV','appsibme09092013033404comp.MOV',328041,0,NULL,1,NULL,'58104068',b'1','2013-09-09 04:29:44',1,'2013-09-09 04:29:44',1,NULL),
	(113,1,1,'1/29/2013/09/09/appsibme09092013114752comp.MOV','appsibme09092013114752comp.MOV',1719157,5,NULL,1,NULL,'58106339',b'1','2013-09-09 04:46:38',1,'2013-09-09 04:46:38',1,NULL),
	(114,1,1,'1/27/2013/09/09/appsibme09092013114752comp.MOV','appsibme09092013114752comp.MOV',1719157,0,NULL,1,NULL,'58106811',b'1','2013-09-09 04:50:04',1,'2013-09-09 04:50:04',1,NULL),
	(115,1,1,'1/27/2013/09/09/appsibme09092013114752comp.MOV','appsibme09092013114752comp.MOV',1719157,0,NULL,1,NULL,'58108257',b'1','2013-09-09 05:07:15',1,'2013-09-09 05:07:15',1,NULL),
	(116,1,1,'1/27/2013/09/09/trimzjTGhJcomp.MOV','trimzjTGhJcomp.MOV',23258005,0,NULL,1,NULL,'58108888',b'1','2013-09-09 05:15:15',1,'2013-09-09 05:15:15',1,NULL),
	(117,1,1,'1/27/2013/09/10/appsibme10092013121109comp.MOV','appsibme10092013121109comp.MOV',736663,0,NULL,1,NULL,'58137838',b'1','2013-09-10 02:13:24',1,'2013-09-10 02:13:24',1,NULL),
	(118,1,1,'1/27/2013/09/10/appsibme10092013121552comp.MOV','appsibme10092013121552comp.MOV',523600,0,NULL,1,NULL,'58138035',b'1','2013-09-10 02:18:10',1,'2013-09-10 02:18:10',1,NULL),
	(119,1,1,'1/26/2013/09/10/appsibme10092013121552comp.MOV','appsibme10092013121552comp.MOV',523600,3,NULL,1,NULL,'58140202',b'1','2013-09-10 02:48:52',1,'2013-09-10 02:48:52',1,NULL),
	(120,1,1,'1/26/2013/09/10/appsibme10092013010928comp.MOV','appsibme10092013010928comp.MOV',1555442,0,NULL,1,NULL,'58141137',b'1','2013-09-10 03:11:49',1,'2013-09-10 03:11:49',1,NULL),
	(121,1,1,'1/26/2013/09/10/appsibme10092013010928comp.MOV','appsibme10092013010928comp.MOV',1555442,0,NULL,1,NULL,'58141187',b'1','2013-09-10 03:12:54',1,'2013-09-10 03:12:54',1,NULL),
	(122,1,1,'1/26/2013/09/10/appsibme10092013010928comp.MOV','appsibme10092013010928comp.MOV',1555442,0,NULL,1,NULL,'58141303',b'1','2013-09-10 03:16:12',1,'2013-09-10 03:16:12',1,NULL),
	(123,1,1,'1/26/2013/09/10/appsibme10092013012549comp.MOV','appsibme10092013012549comp.MOV',1880203,0,NULL,1,NULL,'58141765',b'1','2013-09-10 03:28:42',1,'2013-09-10 03:28:42',1,NULL),
	(124,1,1,'1/26/2013/09/10/appsibme10092013012935comp.MOV','appsibme10092013012935comp.MOV',1612419,0,NULL,1,NULL,'58141895',b'1','2013-09-10 03:32:19',1,'2013-09-10 03:32:19',1,NULL),
	(125,1,1,'1/26/2013/09/10/appsibme10092013013913comp.MOV','appsibme10092013013913comp.MOV',230708,0,NULL,1,NULL,'58142181',b'1','2013-09-10 03:41:28',1,'2013-09-10 03:41:28',1,NULL),
	(126,1,1,'1/26/2013/09/10/appsibme10092013013913comp.MOV','appsibme10092013013913comp.MOV',230708,0,NULL,1,NULL,'58142255',b'1','2013-09-10 03:44:03',1,'2013-09-10 03:44:03',1,NULL),
	(127,1,1,'1/26/2013/09/10/appsibme10092013023721comp.MOV','appsibme10092013023721comp.MOV',869882,0,NULL,1,NULL,'58144399',b'1','2013-09-10 04:40:10',1,'2013-09-10 04:40:10',1,NULL),
	(128,1,1,'1/26/2013/09/10/appsibme10092013024858comp.MOV','appsibme10092013024858comp.MOV',1010538,0,NULL,1,NULL,'58144787',b'1','2013-09-10 04:51:25',1,'2013-09-10 04:51:25',1,NULL),
	(129,1,1,'1/27/2013/09/10/appsibme10092013110302comp.MOV','appsibme10092013110302comp.MOV',223299,0,NULL,1,NULL,'58182926',b'1','2013-09-10 01:05:18',1,'2013-09-10 01:05:18',1,NULL),
	(130,1,1,'1/29/2013/09/10/trimEgP8Axcomp.MOV','trimEgP8Axcomp.MOV',927127,0,NULL,1,NULL,'58205177',b'1','2013-09-10 04:34:33',1,'2013-09-10 04:34:33',1,NULL),
	(131,1,1,'1/29/2013/09/11/appsibme11092013052227comp.MOV','appsibme11092013052227comp.MOV',429496,2,NULL,1,NULL,'58251766',b'1','2013-09-11 07:24:38',1,'2013-09-11 07:24:38',1,NULL),
	(132,1,1,'1/29/2013/09/11/appsibme11092013053510comp.MOV','appsibme11092013053510comp.MOV',276332,1,NULL,1,NULL,'58252508',b'1','2013-09-11 07:37:23',1,'2013-09-11 07:37:23',1,NULL),
	(133,1,1,'1/29/2013/09/11/appsibme11092013053510comp.MOV','appsibme11092013053510comp.MOV',276332,0,NULL,1,NULL,'58252671',b'1','2013-09-11 07:39:45',1,'2013-09-11 07:39:45',1,NULL),
	(134,1,1,'1/29/2013/09/11/appsibme11092013053827comp.MOV','appsibme11092013053827comp.MOV',1537561,0,NULL,1,NULL,'58252775',b'1','2013-09-11 07:41:06',1,'2013-09-11 07:41:06',1,NULL),
	(135,1,1,'1/29/2013/09/13/ac51DZyIRIS9J5bFFaFL_kupload.mov','kupload.mov',623275,1,NULL,1,NULL,'58507405',b'1','2013-09-13 12:20:38',1,'2013-09-13 12:20:38',1,NULL),
	(136,1,2,'1/29/2013/09/13/qlZiJwMKRi24T3LAINJ8_kupload.mov','kupload.mov',623275,0,NULL,1,NULL,NULL,b'1','2013-09-13 12:22:10',1,'2013-09-13 12:22:10',1,NULL),
	(137,1,1,'1/29/2013/09/13/appsibme13092013082433comp.MOV','appsibme13092013082433comp.MOV',4287161,0,NULL,1,NULL,'58509399',b'1','2013-09-13 12:39:07',1,'2013-09-13 12:39:07',1,NULL),
	(138,1,1,'1/29/2013/09/13/appsibme13092013082433comp.MOV','appsibme13092013082433comp.MOV',4287161,0,NULL,1,NULL,'58509703',b'1','2013-09-13 12:42:10',1,'2013-09-13 12:42:10',1,NULL),
	(139,1,1,'1/26/2013/09/13/appsibme13092013082433comp.MOV','appsibme13092013082433comp.MOV',4287161,0,NULL,1,NULL,'58510040',b'1','2013-09-13 12:45:01',1,'2013-09-13 12:45:01',1,NULL),
	(142,12,1,'12/37/2013/09/13/5sUC8vBgSbKNtem1T3XC_kupload.mov','kupload.mov',623275,2,NULL,1,NULL,'58517051',b'1','2013-09-13 01:53:11',836,'2013-09-13 01:53:11',836,NULL),
	(143,12,2,'12/37/2013/09/13/qPbNyKVSPi41iS9Ujatx_169484_10151074057426722_2142794718_o.jpg','169484_10151074057426722_2142794718_o.jpg',508179,0,NULL,1,NULL,NULL,b'1','2013-09-13 01:53:45',836,'2013-09-13 01:53:45',836,NULL),
	(144,12,2,'12/37/2013/09/13/SK7Es1wkRai9ARB1pYKf_kupload.mov','kupload.mov',623275,0,NULL,1,NULL,NULL,b'1','2013-09-13 01:54:15',836,'2013-09-13 01:54:15',836,NULL),
	(145,12,1,'12/37/2013/09/13/100.txt','100.txt',NULL,0,NULL,1,NULL,NULL,b'1','2013-09-13 02:03:33',836,'2013-09-13 02:03:33',836,NULL),
	(146,12,1,'12/38/2013/09/13/H81Pm2MBQGitXBbtt4sR_kupload.mov','kupload.mov',623275,2,NULL,1,NULL,'58519020',b'1','2013-09-13 02:09:45',836,'2013-09-13 02:09:45',836,NULL),
	(147,10,1,'10/36/2013/09/14/appsibme14092013073436comp.MOV','appsibme14092013073436comp.MOV',1696745,1,NULL,1,NULL,'58583787',b'1','2013-09-14 07:37:41',823,'2013-09-14 07:37:41',823,NULL),
	(148,13,1,'13/39/2013/09/15/ShxJ6fOKTEix3PqsEGM1_Malu Riya videos.wmv','Malu Riya videos.wmv',45992992,4,NULL,1,NULL,'58724325',b'1','2013-09-15 07:20:02',851,'2013-09-15 07:20:02',851,NULL),
	(149,1,1,'1/26/2013/09/16/appsibme16092013030935comp.MOV','appsibme16092013030935comp.MOV',351506,0,NULL,1,NULL,'58752250',b'1','2013-09-16 05:12:33',1,'2013-09-16 05:12:33',1,NULL),
	(150,1,1,'1/26/2013/09/16/appsibme16092013030935comp.MOV','appsibme16092013030935comp.MOV',0,0,NULL,1,NULL,'58752252',b'1','2013-09-16 05:12:33',1,'2013-09-16 05:12:33',1,NULL),
	(151,1,1,'1/26/2013/09/16/appsibme16092013030935comp.MOV','appsibme16092013030935comp.MOV',351506,0,NULL,1,NULL,'58752253',b'1','2013-09-16 05:12:34',1,'2013-09-16 05:12:34',1,NULL),
	(152,1,1,'1/26/2013/09/16/appsibme16092013031606comp.MOV','appsibme16092013031606comp.MOV',257891,0,NULL,1,NULL,'58752548',b'1','2013-09-16 05:18:53',1,'2013-09-16 05:18:53',1,NULL),
	(153,1,1,'1/26/2013/09/16/appsibme16092013031606comp.MOV','appsibme16092013031606comp.MOV',0,0,NULL,1,NULL,'58752549',b'1','2013-09-16 05:18:53',1,'2013-09-16 05:18:53',1,NULL),
	(154,1,1,'1/26/2013/09/16/appsibme16092013031606comp.MOV','appsibme16092013031606comp.MOV',0,0,NULL,1,NULL,'58752550',b'1','2013-09-16 05:18:53',1,'2013-09-16 05:18:53',1,NULL),
	(155,1,1,'1/26/2013/09/16/appsibme16092013031606comp.MOV','appsibme16092013031606comp.MOV',0,0,NULL,1,NULL,'58752551',b'1','2013-09-16 05:18:54',1,'2013-09-16 05:18:54',1,NULL),
	(156,1,1,'1/26/2013/09/16/appsibme16092013031606comp.MOV','appsibme16092013031606comp.MOV',257891,0,NULL,1,NULL,'58752560',b'1','2013-09-16 05:19:04',1,'2013-09-16 05:19:04',1,NULL),
	(157,1,1,'1/26/2013/09/16/appsibme16092013031606comp.MOV','appsibme16092013031606comp.MOV',257891,0,NULL,1,NULL,'58752562',b'1','2013-09-16 05:19:07',1,'2013-09-16 05:19:07',1,NULL),
	(158,1,1,'1/26/2013/09/16/appsibme16092013032403comp.MOV','appsibme16092013032403comp.MOV',370748,0,NULL,1,NULL,'58752942',b'1','2013-09-16 05:26:41',1,'2013-09-16 05:26:41',1,NULL),
	(159,1,1,'1/26/2013/09/16/appsibme16092013032403comp.MOV','appsibme16092013032403comp.MOV',370748,0,NULL,1,NULL,'58752944',b'1','2013-09-16 05:26:44',1,'2013-09-16 05:26:44',1,NULL),
	(160,1,1,'1/27/2013/09/16/appsibme16092013032655comp.MOV','appsibme16092013032655comp.MOV',189866,0,NULL,1,NULL,'58753095',b'1','2013-09-16 05:29:32',1,'2013-09-16 05:29:32',1,NULL),
	(161,1,1,'1/27/2013/09/16/appsibme16092013032655comp.MOV','appsibme16092013032655comp.MOV',189866,0,NULL,1,NULL,'58753099',b'1','2013-09-16 05:29:36',1,'2013-09-16 05:29:36',1,NULL),
	(162,1,1,'1/27/2013/09/16/appsibme16092013032655comp.MOV','appsibme16092013032655comp.MOV',189866,0,NULL,1,NULL,'58753100',b'1','2013-09-16 05:29:37',1,'2013-09-16 05:29:37',1,NULL),
	(163,1,1,'1/27/2013/09/16/appsibme16092013032655comp.MOV','appsibme16092013032655comp.MOV',0,0,NULL,1,NULL,'58753101',b'1','2013-09-16 05:29:37',1,'2013-09-16 05:29:37',1,NULL),
	(164,1,1,'1/27/2013/09/16/appsibme16092013032655comp.MOV','appsibme16092013032655comp.MOV',0,0,NULL,1,NULL,'58753102',b'1','2013-09-16 05:29:37',1,'2013-09-16 05:29:37',1,NULL),
	(165,1,1,'1/26/2013/09/16/appsibme16092013032750comp.MOV','appsibme16092013032750comp.MOV',169335,0,NULL,1,NULL,'58753135',b'1','2013-09-16 05:30:18',1,'2013-09-16 05:30:18',1,NULL),
	(166,1,1,'1/26/2013/09/16/appsibme16092013032750comp.MOV','appsibme16092013032750comp.MOV',169335,0,NULL,1,NULL,'58753141',b'1','2013-09-16 05:30:22',1,'2013-09-16 05:30:22',1,NULL),
	(167,1,1,'1/26/2013/09/16/appsibme16092013032750comp.MOV','appsibme16092013032750comp.MOV',169335,0,NULL,1,NULL,'58753142',b'1','2013-09-16 05:30:23',1,'2013-09-16 05:30:23',1,NULL),
	(168,1,1,'1/26/2013/09/16/appsibme16092013032750comp.MOV','appsibme16092013032750comp.MOV',169335,0,NULL,1,NULL,'58753145',b'1','2013-09-16 05:30:25',1,'2013-09-16 05:30:25',1,NULL),
	(169,1,1,'1/26/2013/09/16/appsibme16092013032750comp.MOV','appsibme16092013032750comp.MOV',169335,0,NULL,1,NULL,'58753146',b'1','2013-09-16 05:30:26',1,'2013-09-16 05:30:26',1,NULL),
	(170,1,1,'1/26/2013/09/16/appsibme16092013032750comp.MOV','appsibme16092013032750comp.MOV',169335,0,NULL,1,NULL,'58753147',b'1','2013-09-16 05:30:27',1,'2013-09-16 05:30:27',1,NULL),
	(171,1,1,'1/26/2013/09/16/appsibme16092013033132comp.MOV','appsibme16092013033132comp.MOV',327221,0,NULL,1,NULL,'58753305',b'1','2013-09-16 05:34:00',1,'2013-09-16 05:34:00',1,NULL),
	(172,1,1,'1/26/2013/09/16/appsibme16092013033204comp.MOV','appsibme16092013033204comp.MOV',596316,0,NULL,1,NULL,'58753343',b'1','2013-09-16 05:34:38',1,'2013-09-16 05:34:38',1,NULL),
	(173,1,1,'1/26/2013/09/16/appsibme16092013033233comp.MOV','appsibme16092013033233comp.MOV',530868,0,NULL,1,NULL,'58753358',b'1','2013-09-16 05:35:05',1,'2013-09-16 05:35:05',1,NULL),
	(174,1,1,'1/26/2013/09/16/appsibme16092013033303comp.MOV','appsibme16092013033303comp.MOV',486293,0,NULL,1,NULL,'58753380',b'1','2013-09-16 05:35:38',1,'2013-09-16 05:35:38',1,NULL),
	(175,1,1,'1/26/2013/09/16/appsibme16092013033303comp.MOV','appsibme16092013033303comp.MOV',486293,0,NULL,1,NULL,'58753381',b'1','2013-09-16 05:35:39',1,'2013-09-16 05:35:39',1,NULL),
	(176,1,1,'1/26/2013/09/16/appsibme16092013033507comp.MOV','appsibme16092013033507comp.MOV',302134,0,NULL,1,NULL,'58753494',b'1','2013-09-16 05:37:40',1,'2013-09-16 05:37:40',1,NULL),
	(177,1,1,'1/26/2013/09/16/appsibme16092013034013comp.MOV','appsibme16092013034013comp.MOV',541713,0,NULL,1,NULL,'58753727',b'1','2013-09-16 05:42:49',1,'2013-09-16 05:42:49',1,NULL),
	(178,1,1,'1/26/2013/09/16/appsibme16092013034124comp.MOV','appsibme16092013034124comp.MOV',436660,0,NULL,1,NULL,'58753778',b'1','2013-09-16 05:43:57',1,'2013-09-16 05:43:57',1,NULL),
	(179,1,1,'1/26/2013/09/16/appsibme16092013034124comp.MOV','appsibme16092013034124comp.MOV',436660,0,NULL,1,NULL,'58753781',b'1','2013-09-16 05:43:57',1,'2013-09-16 05:43:57',1,NULL),
	(180,1,1,'1/26/2013/09/16/appsibme16092013034124comp.MOV','appsibme16092013034124comp.MOV',436660,0,NULL,1,NULL,'58753790',b'1','2013-09-16 05:44:05',1,'2013-09-16 05:44:05',1,NULL),
	(181,1,1,'1/26/2013/09/16/appsibme16092013034210comp.MOV','appsibme16092013034210comp.MOV',462639,0,NULL,1,NULL,'58753825',b'1','2013-09-16 05:44:43',1,'2013-09-16 05:44:43',1,NULL),
	(182,1,1,'1/26/2013/09/16/appsibme16092013034210comp.MOV','appsibme16092013034210comp.MOV',462639,0,NULL,1,NULL,'58753826',b'1','2013-09-16 05:44:43',1,'2013-09-16 05:44:43',1,NULL),
	(183,1,1,'1/26/2013/09/16/appsibme16092013034210comp.MOV','appsibme16092013034210comp.MOV',462639,0,NULL,1,NULL,'58753827',b'1','2013-09-16 05:44:44',1,'2013-09-16 05:44:44',1,NULL),
	(184,1,1,'1/26/2013/09/16/appsibme16092013034210comp.MOV','appsibme16092013034210comp.MOV',0,0,NULL,1,NULL,'58753828',b'1','2013-09-16 05:44:44',1,'2013-09-16 05:44:44',1,NULL),
	(185,1,1,'1/26/2013/09/16/appsibme16092013034406comp.MOV','appsibme16092013034406comp.MOV',592659,0,NULL,1,NULL,'58753919',b'1','2013-09-16 05:46:46',1,'2013-09-16 05:46:46',1,NULL),
	(186,1,1,'1/26/2013/09/16/appsibme16092013034427comp.MOV','appsibme16092013034427comp.MOV',489192,0,NULL,1,NULL,'58753935',b'1','2013-09-16 05:47:06',1,'2013-09-16 05:47:06',1,NULL),
	(187,1,1,'1/26/2013/09/16/appsibme16092013034427comp.MOV','appsibme16092013034427comp.MOV',0,0,NULL,1,NULL,'58753936',b'1','2013-09-16 05:47:06',1,'2013-09-16 05:47:06',1,NULL),
	(188,1,1,'1/26/2013/09/16/appsibme16092013034550comp.MOV','appsibme16092013034550comp.MOV',188643,0,NULL,1,NULL,'58754000',b'1','2013-09-16 05:48:20',1,'2013-09-16 05:48:20',1,NULL),
	(189,1,1,'1/26/2013/09/16/appsibme16092013034621comp.MOV','appsibme16092013034621comp.MOV',462182,0,NULL,1,NULL,'58754021',b'1','2013-09-16 05:48:58',1,'2013-09-16 05:48:58',1,NULL),
	(190,1,1,'1/26/2013/09/16/appsibme16092013034621comp.MOV','appsibme16092013034621comp.MOV',462182,0,NULL,1,NULL,'58754025',b'1','2013-09-16 05:49:04',1,'2013-09-16 05:49:04',1,NULL),
	(191,10,1,'10/34/2013/09/16/appsibme16092013035021comp.MOV','appsibme16092013035021comp.MOV',208999,0,NULL,1,NULL,'58754222',b'1','2013-09-16 05:53:07',823,'2013-09-16 05:53:07',823,NULL),
	(192,10,1,'10/32/2013/09/16/appsibme16092013035050comp.MOV','appsibme16092013035050comp.MOV',306942,0,NULL,1,NULL,'58754250',b'1','2013-09-16 05:53:44',823,'2013-09-16 05:53:44',823,NULL),
	(193,10,1,'10/32/2013/09/16/appsibme16092013035050comp.MOV','appsibme16092013035050comp.MOV',306942,0,NULL,1,NULL,'58754260',b'1','2013-09-16 05:53:57',823,'2013-09-16 05:53:57',823,NULL),
	(194,10,1,'10/32/2013/09/16/appsibme16092013035050comp.MOV','appsibme16092013035050comp.MOV',306942,0,NULL,1,NULL,'58754367',b'1','2013-09-16 05:55:46',823,'2013-09-16 05:55:46',823,NULL),
	(195,10,1,'10/32/2013/09/16/appsibme16092013035202comp.MOV','appsibme16092013035202comp.MOV',646953,0,NULL,1,NULL,'58754406',b'1','2013-09-16 05:56:30',823,'2013-09-16 05:56:30',823,NULL),
	(196,1,1,'1/26/2013/09/16/appsibme16092013040452comp.MOV','appsibme16092013040452comp.MOV',786319,0,NULL,1,NULL,'58755082',b'1','2013-09-16 06:11:17',1,'2013-09-16 06:11:17',1,NULL),
	(197,1,1,'1/26/2013/09/16/appsibme16092013041216comp.MOV','appsibme16092013041216comp.MOV',402846,0,NULL,1,NULL,'58755254',b'1','2013-09-16 06:14:54',1,'2013-09-16 06:14:54',1,NULL),
	(198,1,1,'1/26/2013/09/16/appsibme16092013041238comp.MOV','appsibme16092013041238comp.MOV',449323,0,NULL,1,NULL,'58755280',b'1','2013-09-16 06:15:12',1,'2013-09-16 06:15:12',1,NULL),
	(199,1,1,'1/26/2013/09/16/appsibme16092013041238comp.MOV','appsibme16092013041238comp.MOV',449323,0,NULL,1,NULL,'58755285',b'1','2013-09-16 06:15:15',1,'2013-09-16 06:15:15',1,NULL),
	(200,1,1,'1/29/2013/09/16/appsibme16092013041319comp.MOV','appsibme16092013041319comp.MOV',446525,0,NULL,1,NULL,'58755314',b'1','2013-09-16 06:15:54',1,'2013-09-16 06:15:54',1,NULL),
	(201,1,1,'1/26/2013/09/16/appsibme16092013041338comp.MOV','appsibme16092013041338comp.MOV',680881,0,NULL,1,NULL,'58755336',b'1','2013-09-16 06:16:23',1,'2013-09-16 06:16:23',1,NULL),
	(202,1,1,'1/26/2013/09/16/appsibme16092013041338comp.MOV','appsibme16092013041338comp.MOV',680881,0,NULL,1,NULL,'58755343',b'1','2013-09-16 06:16:27',1,'2013-09-16 06:16:27',1,NULL),
	(203,1,1,'1/26/2013/09/16/appsibme16092013041338comp.MOV','appsibme16092013041338comp.MOV',680881,0,NULL,1,NULL,'58755345',b'1','2013-09-16 06:16:29',1,'2013-09-16 06:16:29',1,NULL),
	(204,1,1,'1/26/2013/09/16/appsibme16092013041426comp.MOV','appsibme16092013041426comp.MOV',544411,0,NULL,1,NULL,'58755371',b'1','2013-09-16 06:17:02',1,'2013-09-16 06:17:02',1,NULL),
	(205,1,1,'1/26/2013/09/16/appsibme16092013041446comp.MOV','appsibme16092013041446comp.MOV',573001,0,NULL,1,NULL,'58755392',b'1','2013-09-16 06:17:27',1,'2013-09-16 06:17:27',1,NULL),
	(206,1,1,'1/26/2013/09/16/appsibme16092013041446comp.MOV','appsibme16092013041446comp.MOV',573001,0,NULL,1,NULL,'58755394',b'1','2013-09-16 06:17:30',1,'2013-09-16 06:17:30',1,NULL),
	(207,1,1,'1/26/2013/09/16/appsibme16092013041446comp.MOV','appsibme16092013041446comp.MOV',573001,0,NULL,1,NULL,'58755395',b'1','2013-09-16 06:17:30',1,'2013-09-16 06:17:30',1,NULL),
	(208,1,1,'1/26/2013/09/16/appsibme16092013041446comp.MOV','appsibme16092013041446comp.MOV',573001,0,NULL,1,NULL,'58755397',b'1','2013-09-16 06:17:31',1,'2013-09-16 06:17:31',1,NULL),
	(209,10,1,'10/34/2013/09/16/appsibme16092013041631comp.MOV','appsibme16092013041631comp.MOV',90364,0,NULL,1,NULL,'58755461',b'1','2013-09-16 06:19:09',823,'2013-09-16 06:19:09',823,NULL),
	(210,10,1,'10/34/2013/09/16/appsibme16092013041631comp.MOV','appsibme16092013041631comp.MOV',614652,0,NULL,1,NULL,'58755462',b'1','2013-09-16 06:19:09',823,'2013-09-16 06:19:09',823,NULL),
	(211,10,1,'10/34/2013/09/16/appsibme16092013041631comp.MOV','appsibme16092013041631comp.MOV',0,0,NULL,1,NULL,'58755463',b'1','2013-09-16 06:19:09',823,'2013-09-16 06:19:09',823,NULL),
	(212,10,1,'10/34/2013/09/16/appsibme16092013041631comp.MOV','appsibme16092013041631comp.MOV',90364,1,NULL,1,NULL,'58755465',b'1','2013-09-16 06:19:12',823,'2013-09-16 06:19:12',823,NULL),
	(213,10,1,'10/34/2013/09/16/appsibme16092013041631comp.MOV','appsibme16092013041631comp.MOV',614652,0,NULL,1,NULL,'58755470',b'1','2013-09-16 06:19:18',823,'2013-09-16 06:19:18',823,NULL),
	(214,10,1,'10/34/2013/09/16/appsibme16092013042154comp.MOV','appsibme16092013042154comp.MOV',535613,0,NULL,1,NULL,'58755735',b'1','2013-09-16 06:24:32',823,'2013-09-16 06:24:32',823,NULL),
	(215,10,1,'10/34/2013/09/16/appsibme16092013042219comp.MOV','appsibme16092013042219comp.MOV',362369,0,NULL,1,NULL,'58755750',b'1','2013-09-16 06:24:55',823,'2013-09-16 06:24:55',823,NULL),
	(216,10,1,'10/34/2013/09/16/appsibme16092013042219comp.MOV','appsibme16092013042219comp.MOV',362369,0,NULL,1,NULL,'58755753',b'1','2013-09-16 06:24:58',823,'2013-09-16 06:24:58',823,NULL),
	(217,10,1,'10/34/2013/09/16/appsibme16092013043104comp.MOV','appsibme16092013043104comp.MOV',267724,0,NULL,1,NULL,'58756275',b'1','2013-09-16 06:35:33',823,'2013-09-16 06:35:33',823,NULL),
	(218,10,1,'10/34/2013/09/16/appsibme16092013043344comp.MOV','appsibme16092013043344comp.MOV',534240,0,NULL,1,NULL,'58756317',b'1','2013-09-16 06:36:36',823,'2013-09-16 06:36:36',823,NULL),
	(219,10,1,'10/34/2013/09/16/appsibme16092013043344comp.MOV','appsibme16092013043344comp.MOV',534240,0,NULL,1,NULL,'58756322',b'1','2013-09-16 06:36:38',823,'2013-09-16 06:36:38',823,NULL),
	(220,10,1,'10/34/2013/09/16/appsibme16092013043448comp.MOV','appsibme16092013043448comp.MOV',259289,0,NULL,1,NULL,'58756368',b'1','2013-09-16 06:37:48',823,'2013-09-16 06:37:48',823,NULL),
	(221,10,1,'10/34/2013/09/16/appsibme16092013043448comp.MOV','appsibme16092013043448comp.MOV',259289,0,NULL,1,NULL,'58756369',b'1','2013-09-16 06:37:48',823,'2013-09-16 06:37:48',823,NULL),
	(222,10,1,'10/34/2013/09/16/appsibme16092013043448comp.MOV','appsibme16092013043448comp.MOV',783577,0,NULL,1,NULL,'58756370',b'1','2013-09-16 06:37:48',823,'2013-09-16 06:37:48',823,NULL),
	(223,10,1,'10/34/2013/09/16/appsibme16092013043949comp.MOV','appsibme16092013043949comp.MOV',412667,0,NULL,1,NULL,'58756603',b'1','2013-09-16 06:42:30',823,'2013-09-16 06:42:30',823,NULL),
	(224,10,1,'10/34/2013/09/16/appsibme16092013044020comp.MOV','appsibme16092013044020comp.MOV',387264,0,NULL,1,NULL,'58756625',b'1','2013-09-16 06:43:00',823,'2013-09-16 06:43:00',823,NULL),
	(225,10,1,'10/34/2013/09/16/appsibme16092013044020comp.MOV','appsibme16092013044020comp.MOV',387264,0,NULL,1,NULL,'58756632',b'1','2013-09-16 06:43:02',823,'2013-09-16 06:43:02',823,NULL),
	(226,10,1,'10/32/2013/09/16/appsibme16092013044507comp.MOV','appsibme16092013044507comp.MOV',464065,0,NULL,1,NULL,'58756849',b'1','2013-09-16 06:47:44',823,'2013-09-16 06:47:44',823,NULL),
	(227,10,1,'10/34/2013/09/16/appsibme16092013044020comp.MOV','appsibme16092013044020comp.MOV',387264,0,NULL,1,NULL,'58756857',b'1','2013-09-16 06:47:55',823,'2013-09-16 06:47:55',823,NULL),
	(228,10,1,'10/34/2013/09/16/appsibme16092013044020comp.MOV','appsibme16092013044020comp.MOV',387264,0,NULL,1,NULL,'58756858',b'1','2013-09-16 06:47:56',823,'2013-09-16 06:47:56',823,NULL),
	(229,10,1,'10/32/2013/09/16/appsibme16092013044624comp.MOV','appsibme16092013044624comp.MOV',275310,0,NULL,1,NULL,'58756939',b'1','2013-09-16 06:49:46',823,'2013-09-16 06:49:46',823,NULL),
	(230,10,1,'10/32/2013/09/16/appsibme16092013044821comp.MOV','appsibme16092013044821comp.MOV',381963,0,NULL,1,NULL,'58757009',b'1','2013-09-16 06:51:23',823,'2013-09-16 06:51:23',823,NULL),
	(231,10,1,'10/32/2013/09/16/appsibme16092013045344comp.MOV','appsibme16092013045344comp.MOV',1063869,0,NULL,1,NULL,'58757278',b'1','2013-09-16 06:56:49',823,'2013-09-16 06:56:49',823,NULL),
	(232,10,1,'10/32/2013/09/16/appsibme16092013045419comp.MOV','appsibme16092013045419comp.MOV',526912,0,NULL,1,NULL,'58757281',b'1','2013-09-16 06:56:55',823,'2013-09-16 06:56:55',823,NULL),
	(233,10,1,'10/32/2013/09/16/appsibme16092013045453comp.MOV','appsibme16092013045453comp.MOV',403926,1,NULL,1,NULL,'58757302',b'1','2013-09-16 06:57:28',823,'2013-09-16 06:57:28',823,NULL),
	(234,10,1,'10/34/2013/09/16/appsibme16092013045931comp.MOV','appsibme16092013045931comp.MOV',753897,0,NULL,1,NULL,'58757543',b'1','2013-09-16 07:02:17',823,'2013-09-16 07:02:17',823,NULL),
	(235,10,1,'10/34/2013/09/16/appsibme16092013045952comp.MOV','appsibme16092013045952comp.MOV',282436,0,NULL,1,NULL,'58757547',b'1','2013-09-16 07:02:21',823,'2013-09-16 07:02:21',823,NULL),
	(236,10,1,'10/34/2013/09/16/appsibme16092013045952comp.MOV','appsibme16092013045952comp.MOV',0,0,NULL,1,NULL,'58757548',b'1','2013-09-16 07:02:21',823,'2013-09-16 07:02:21',823,NULL),
	(237,10,1,'10/34/2013/09/16/appsibme16092013050527comp.MOV','appsibme16092013050527comp.MOV',537948,0,NULL,1,NULL,'58757841',b'1','2013-09-16 07:08:06',823,'2013-09-16 07:08:06',823,NULL),
	(238,10,1,'10/34/2013/09/16/appsibme16092013050550comp.MOV','appsibme16092013050550comp.MOV',198832,0,NULL,1,NULL,'58757867',b'1','2013-09-16 07:08:26',823,'2013-09-16 07:08:26',823,NULL),
	(239,10,1,'10/34/2013/09/16/appsibme16092013050550comp.MOV','appsibme16092013050550comp.MOV',198832,0,NULL,1,NULL,'58757873',b'1','2013-09-16 07:08:29',823,'2013-09-16 07:08:29',823,NULL),
	(240,10,1,'10/34/2013/09/16/appsibme16092013050844comp.MOV','appsibme16092013050844comp.MOV',512881,0,NULL,1,NULL,'58758044',b'1','2013-09-16 07:11:25',823,'2013-09-16 07:11:25',823,NULL),
	(241,10,1,'10/34/2013/09/16/appsibme16092013052712comp.MOV','appsibme16092013052712comp.MOV',541678,0,NULL,1,NULL,'58758960',b'1','2013-09-16 07:30:17',823,'2013-09-16 07:30:17',823,NULL),
	(242,10,1,'10/34/2013/09/16/appsibme16092013052828comp.MOV','appsibme16092013052828comp.MOV',311810,0,NULL,1,NULL,'58759050',b'1','2013-09-16 07:31:56',823,'2013-09-16 07:31:56',823,NULL),
	(243,10,1,'10/34/2013/09/16/appsibme16092013052955comp.MOV','appsibme16092013052955comp.MOV',327520,0,NULL,1,NULL,'58759084',b'1','2013-09-16 07:32:28',823,'2013-09-16 07:32:28',823,NULL),
	(244,10,1,'10/34/2013/09/16/appsibme16092013055712comp.MOV','appsibme16092013055712comp.MOV',435601,0,NULL,1,NULL,'58760904',b'1','2013-09-16 08:06:57',823,'2013-09-16 08:06:57',823,NULL),
	(245,10,1,'10/34/2013/09/16/appsibme16092013055554comp.MOV','appsibme16092013055554comp.MOV',372498,0,NULL,1,NULL,'58760906',b'1','2013-09-16 08:07:00',823,'2013-09-16 08:07:00',823,NULL),
	(246,10,1,'10/34/2013/09/16/appsibme16092013055347comp.MOV','appsibme16092013055347comp.MOV',521656,0,NULL,1,NULL,'58760908',b'1','2013-09-16 08:07:02',823,'2013-09-16 08:07:02',823,NULL),
	(247,10,1,'10/34/2013/09/16/appsibme16092013060342comp.MOV','appsibme16092013060342comp.MOV',3378561,0,NULL,1,NULL,'58761015',b'1','2013-09-16 08:08:41',823,'2013-09-16 08:08:41',823,NULL),
	(248,14,1,'14/44/2013/09/16/7b4WMeQGQQuYtw9HRlIT_kupload.mov','kupload.mov',623275,6,NULL,1,NULL,'58761370',b'1','2013-09-16 08:14:52',854,'2013-09-16 08:14:52',854,NULL),
	(249,14,1,'14/43/2013/09/16/appsibme16092013061752comp.MOV','appsibme16092013061752comp.MOV',1773712,0,NULL,1,NULL,'58761868',b'1','2013-09-16 08:22:33',854,'2013-09-16 08:22:33',854,NULL),
	(250,14,1,'14/43/2013/09/16/appsibme16092013062059comp.MOV','appsibme16092013062059comp.MOV',602799,1,NULL,1,NULL,'58762016',b'1','2013-09-16 08:24:59',854,'2013-09-16 08:24:59',854,NULL),
	(251,14,1,'14/45/2013/09/16/appsibme16092013064558comp.MOV','appsibme16092013064558comp.MOV',492781,2,NULL,1,NULL,'58763904',b'1','2013-09-16 08:49:53',854,'2013-09-16 08:49:53',854,NULL),
	(252,14,1,'14/43/2013/09/16/appsibme16092013055712comp.MOV','appsibme16092013055712comp.MOV',435601,0,NULL,1,NULL,'58771741',b'1','2013-09-16 10:12:06',854,'2013-09-16 10:12:06',854,NULL),
	(253,14,1,'14/46/2013/09/16/appsibme16092013081726comp.MOV','appsibme16092013081726comp.MOV',950703,0,NULL,1,NULL,'58772562',b'1','2013-09-16 10:20:18',854,'2013-09-16 10:20:18',854,NULL),
	(254,14,1,'14/47/2013/09/16/5CZVJq6SWqdF4SsPSc2Q_kupload.mov','kupload.mov',623275,0,NULL,1,NULL,'58772589',b'1','2013-09-16 10:20:27',854,'2013-09-16 10:20:27',854,NULL),
	(255,14,1,'14/48/2013/09/16/CUzmEfATp6busmTDZ1UT_kupload.mov','kupload.mov',623275,0,NULL,1,NULL,'58774983',b'1','2013-09-16 10:42:11',854,'2013-09-16 10:42:11',854,NULL),
	(256,14,1,'14/49/2013/09/16/e01vs6TxSVaX73sNvcLl_kupload.mov','kupload.mov',623275,1,NULL,1,NULL,'58775714',b'1','2013-09-16 10:48:18',854,'2013-09-16 10:48:18',854,NULL),
	(257,14,1,'14/50/2013/09/16/tfRQAZcRmqFJwo7iGmgy_kupload.mov','kupload.mov',623275,0,NULL,1,NULL,'58776822',b'1','2013-09-16 10:59:12',854,'2013-09-16 10:59:12',854,NULL),
	(258,14,1,'14/51/2013/09/16/IO16JoxSQNq7WwuBKM7s_kupload.mov','kupload.mov',623275,0,NULL,1,NULL,'58777291',b'1','2013-09-16 11:02:33',854,'2013-09-16 11:02:33',854,NULL),
	(259,0,0,NULL,NULL,NULL,0,NULL,0,NULL,NULL,b'1','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0,'kk'),
	(260,0,0,NULL,NULL,NULL,0,NULL,0,NULL,NULL,b'1','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0,'kk'),
	(261,14,1,'14/52/2013/09/16/RyNszoRMaP1gdkx0Yb1w_kupload.mov','kupload.mov',623275,1,NULL,1,NULL,'58777755',b'1','2013-09-16 11:06:41',854,'2013-09-16 11:06:41',854,NULL),
	(262,14,1,'14/53/2013/09/16/TVgStP7gQOqWBXMHBBVV_kupload.mov','kupload.mov',623275,0,NULL,1,NULL,'58778078',b'1','2013-09-16 11:09:38',854,'2013-09-16 11:09:38',854,NULL),
	(263,10,1,'10/32/2013/09/16/trim16zGm1comp.MOV','trim16zGm1comp.MOV',5035414,0,NULL,1,NULL,'58801129',b'1','2013-09-16 02:39:38',823,'2013-09-16 02:39:38',823,NULL),
	(264,0,0,NULL,NULL,NULL,0,NULL,0,NULL,NULL,b'1','0000-00-00 00:00:00',0,'0000-00-00 00:00:00',0,'kk'),
	(265,10,1,'10/32/2013/09/17/Bug-Fixes.docx','Bug-Fixes.docx',NULL,0,NULL,1,NULL,NULL,b'1','2013-09-17 11:40:25',823,'2013-09-17 11:40:25',823,NULL);

/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_groups_on_account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `account_id`, `name`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(1,1,'Math Group',1,'2013-12-12 00:00:00',1,'2013-09-13 12:14:34'),
	(2,1,'Test new edited',0,'0000-00-00 00:00:00',1,'2013-09-13 12:14:55'),
	(3,10,'Test group',0,'0000-00-00 00:00:00',0,'0000-00-00 00:00:00'),
	(6,13,'Teaching Group',0,'0000-00-00 00:00:00',851,'2013-09-15 07:10:56'),
	(7,13,'group3',0,'0000-00-00 00:00:00',0,'0000-00-00 00:00:00'),
	(8,13,'combi',0,'0000-00-00 00:00:00',0,'0000-00-00 00:00:00'),
	(9,14,'Dev Group',0,'0000-00-00 00:00:00',0,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plans
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plans`;

CREATE TABLE `plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_type` varchar(255) DEFAULT NULL,
  `storage` bigint(20) DEFAULT NULL,
  `validity` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `users` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `per_year` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `plans` WRITE;
/*!40000 ALTER TABLE `plans` DISABLE KEYS */;

INSERT INTO `plans` (`id`, `plan_type`, `storage`, `validity`, `created_at`, `updated_at`, `users`, `price`, `per_year`)
VALUES
	(1,'Department',5,'60 Days','2012-10-08 11:57:52','2013-02-28 12:37:38',10,54,0),
	(2,'School',20,'89 Days','2012-10-08 11:58:28','2013-02-28 12:39:23',40,119,0),
	(3,'Large School',50,'895 Days','2012-10-22 11:57:23','2013-02-28 12:41:20',150,239,0),
	(4,'Institution',100,NULL,'2012-11-08 11:57:45','2013-02-28 12:42:40',500,399,0),
	(5,'Department',5,NULL,'2013-03-01 10:41:29','2013-03-01 10:41:29',10,588,1),
	(6,'School',20,NULL,'2013-03-01 10:41:29','2013-03-01 10:41:29',40,1308,1),
	(7,'Large School',50,NULL,'2013-03-01 10:41:29','2013-03-01 10:41:29',150,2628,1),
	(8,'Institution',100,NULL,'2013-03-01 10:41:29','2013-03-01 10:41:29',500,4368,1);

/*!40000 ALTER TABLE `plans` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_type` tinyint(4) NOT NULL COMMENT '1=account, 2=huddle',
  `name` varchar(64) NOT NULL,
  `desc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`role_id`, `role_type`, `name`, `desc`)
VALUES
	(100,1,'Account Owner',NULL),
	(110,1,'Super User',NULL),
	(120,1,'User',NULL),
	(200,2,'Admin',NULL),
	(210,2,'User',NULL),
	(220,2,'Viewer',NULL);

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table subjects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subjects`;

CREATE TABLE `subjects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;

INSERT INTO `subjects` (`id`, `name`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`, `account_id`)
VALUES
	(8,'Test',1,'2013-09-03 23:04:10',1,'2013-09-03 11:04:10',1),
	(9,'English',823,'2013-09-04 12:29:02',823,'2013-09-04 12:29:02',10),
	(10,'Math',823,'2013-09-04 12:31:47',823,'2013-09-04 12:31:47',10),
	(11,'Subject 1',836,'2013-09-13 14:08:14',836,'2013-09-13 02:08:14',12),
	(12,'subj',851,'2013-09-15 19:19:34',851,'2013-09-15 07:19:34',13),
	(13,'My Subject',854,'2013-09-16 08:14:42',854,'2013-09-16 08:14:42',14),
	(14,'Math',854,'2013-09-16 10:41:53',854,'2013-09-16 10:41:53',14);

/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table topics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `topics`;

CREATE TABLE `topics` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `topics` WRITE;
/*!40000 ALTER TABLE `topics` DISABLE KEYS */;

INSERT INTO `topics` (`id`, `name`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`, `account_id`)
VALUES
	(5,'Test',1,'2013-09-03 23:04:17',1,'2013-09-03 11:04:17',1),
	(6,'Introduction to new material',823,'2013-09-04 12:29:13',823,'2013-09-04 12:29:13',10),
	(7,'New Topic',836,'2013-09-13 14:09:41',836,'2013-09-13 02:09:41',12),
	(8,'test video',851,'2013-09-15 19:19:22',851,'2013-09-15 07:19:22',13),
	(9,'New Topic',854,'2013-09-16 08:14:49',854,'2013-09-16 08:14:49',14);

/*!40000 ALTER TABLE `topics` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_activity_logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_activity_logs`;

CREATE TABLE `user_activity_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `desc` text,
  `url` varchar(255) DEFAULT NULL,
  `account_folder_id` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `user_activity_logs` WRITE;
/*!40000 ALTER TABLE `user_activity_logs` DISABLE KEYS */;

INSERT INTO `user_activity_logs` (`id`, `account_id`, `user_id`, `desc`, `url`, `account_folder_id`, `date_added`, `type`)
VALUES
	(61,14,854,'A new Huddle : \"ksaleem Huddle Only\" is created by Khurram Saleem','/app/Huddles/view/46',46,'2013-09-16 09:13:04',NULL),
	(62,14,855,'A new comment: Testing is added in :\"Testing for Me\"Khurram MIYW','/app/huddles/view/45/1/251',45,'2013-09-16 09:13:59',NULL),
	(63,14,854,'A new Video Library : \"test\" was created by Khurram Saleem','/app/videoLibrary/view/47',47,'2013-09-16 10:20:27',NULL),
	(64,14,854,'A new Video Library : \"Lecture No 1\" was created by Khurram Saleem','/app/videoLibrary/view/48',48,'2013-09-16 10:42:11',NULL),
	(65,14,854,'A new Video Library : \"Lecture #3\" was created by Khurram Saleem','/app/videoLibrary/view/49',49,'2013-09-16 10:48:18',NULL),
	(66,14,854,'A new Video Library : \"test\" was created by Khurram Saleem','/app/videoLibrary/view/50',50,'2013-09-16 10:59:12',NULL),
	(67,14,854,'A new Video Library : \"test\" was created by Khurram Saleem','/app/videoLibrary/view/51',51,'2013-09-16 11:02:33',NULL),
	(68,14,854,'A new Video Library : \"gg\" was created by Khurram Saleem','/app/videoLibrary/view/52',52,'2013-09-16 11:06:41',NULL),
	(69,14,854,'A new Video Library : \"test\" was created by Khurram Saleem','/app/videoLibrary/view/53',53,'2013-09-16 11:09:38',NULL),
	(70,10,823,'A new comment: hi is added in :\"Test huddle\"David Wakefield','/app/huddles/view/32/1/233',32,'2013-09-16 14:54:18',NULL),
	(71,10,823,'A new Discussion:\"Wendesday PLC\" is created under :\"Chem huddle\" by David Wakefield','/app/Huddles/view/34/3',34,'2013-09-17 11:35:24',NULL),
	(72,10,823,'A new Discussion:\"\" is created under :\"Chem huddle\" by David Wakefield','/app/Huddles/view/34/3',34,'2013-09-17 11:36:19',NULL),
	(73,10,823,'A new Discussion:\"Please\" is created under :\"Test huddle\" by David Wakefield','/app/Huddles/view/32/3',32,'2013-09-17 11:40:25',NULL),
	(74,10,823,'A new Discussion:\"\" is created under :\"Test huddle\" by David Wakefield','/app/Huddles/view/32/3',32,'2013-09-17 11:41:18',NULL),
	(75,10,823,'A new Discussion:\"\" is created under :\"Test huddle\" by David Wakefield','/app/Huddles/view/32/3',32,'2013-09-17 11:41:25',NULL),
	(76,10,823,'A new Discussion:\"\" is created under :\"Test huddle\" by David Wakefield','/app/Huddles/view/32/3',32,'2013-09-17 11:41:33',NULL),
	(77,10,830,'A new user:Larry Karry  is invited to: \"New Huddle\" by lindsey Smith','/app/huddles/view/54/4',54,'2013-09-17 12:02:29',NULL),
	(78,10,830,'A new Huddle : \"New Huddle\" is created by lindsey Smith','/app/Huddles/view/54',54,'2013-09-17 12:02:29',NULL),
	(79,14,857,'A new user:Muhammad hamid  is invited to: \"3S\" by Khurram JJ','/app/users/administrators_groups/14/834',NULL,'2013-09-17 13:23:57',NULL),
	(80,10,823,'A new Discussion:\"Test Discussion\" is created under :\"English huddle\" by David Wakefield','/app/Huddles/view/36/3',36,'2013-09-17 13:49:23',NULL),
	(81,10,823,'A new Discussion:\"\" is created under :\"English huddle\" by David Wakefield','/app/Huddles/view/36/3',36,'2013-09-17 13:49:47',NULL),
	(82,10,823,'A new Discussion:\"\" is created under :\"English huddle\" by David Wakefield','/app/Huddles/view/36/3',36,'2013-09-17 13:50:01',NULL),
	(83,10,823,'A new Discussion:\"\" is created under :\"English huddle\" by David Wakefield','/app/Huddles/view/36/3',36,'2013-09-17 13:50:38',NULL),
	(84,10,823,'A new Discussion:\"\" is created under :\"English huddle\" by David Wakefield','/app/Huddles/view/36/3',36,'2013-09-17 13:51:09',NULL),
	(85,14,854,'A new user:Khurram MIYW  is invited to: \"English Department Meeting - 1/9/13\" by Khurram Saleem','/app/huddles/view/55/4',55,'2013-09-17 14:00:08',NULL),
	(86,14,854,'A new user:Muhamad hamid  is invited to: \"English Department Meeting - 1/9/13\" by Khurram Saleem','/app/huddles/view/55/4',55,'2013-09-17 14:00:08',NULL),
	(87,14,854,'A new user:Khurram JJ  is invited to: \"English Department Meeting - 1/9/13\" by Khurram Saleem','/app/huddles/view/55/4',55,'2013-09-17 14:00:08',NULL),
	(88,14,854,'A new Huddle : \"English Department Meeting - 1/9/13\" is created by Khurram Saleem','/app/Huddles/view/55',55,'2013-09-17 14:00:08',NULL),
	(89,14,854,'A new Discussion:\"English Department Meeting - 1/9/13 discussions\" is created under :\"English Department Meeting - 1/9/13\" by Khurram Saleem','/app/Huddles/view/55/3',55,'2013-09-17 14:00:33',NULL);

/*!40000 ALTER TABLE `user_activity_logs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_groups`;

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_groups_on_group_id` (`group_id`),
  KEY `index_user_groups_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;

INSERT INTO `user_groups` (`id`, `group_id`, `user_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(5,3,825,0,'2013-09-11 10:16:42',0,'0000-00-00 00:00:00'),
	(6,3,830,0,'2013-09-11 10:16:42',0,'0000-00-00 00:00:00'),
	(12,1,832,0,'0000-00-00 00:00:00',0,'2013-09-13 12:14:34'),
	(13,1,831,0,'0000-00-00 00:00:00',0,'2013-09-13 12:14:34'),
	(14,2,831,0,'0000-00-00 00:00:00',0,'2013-09-13 12:14:55'),
	(22,6,853,0,'0000-00-00 00:00:00',0,'2013-09-15 07:10:56'),
	(23,7,852,0,'2013-09-15 07:11:14',0,'0000-00-00 00:00:00'),
	(24,8,853,0,'2013-09-15 07:11:29',0,'0000-00-00 00:00:00'),
	(26,9,855,0,'2013-09-16 08:51:37',0,'0000-00-00 00:00:00'),
	(27,9,857,0,'2013-09-16 08:51:37',0,'0000-00-00 00:00:00');

/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) DEFAULT NULL,
  `last_sign_in_ip` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `time_zone` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `authentication_token` varchar(255) DEFAULT NULL,
  `email_notification` tinyint(1) DEFAULT '1',
  `is_active` tinyint(1) DEFAULT '1',
  `type` enum('Invite_Sent','Active') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `email`, `password`, `reset_password_token`, `reset_password_sent_at`, `remember_created_at`, `sign_in_count`, `current_sign_in_at`, `last_sign_in_at`, `current_sign_in_ip`, `last_sign_in_ip`, `first_name`, `last_name`, `title`, `phone`, `time_zone`, `image`, `file_size`, `authentication_token`, `email_notification`, `is_active`, `type`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(822,'mdonaho','mdonaho@gmail.com','d560004e15c2c0922e19c50b3da635807b4002b4',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'Mark','Donaho',NULL,NULL,NULL,NULL,NULL,'afda332245e2af431fb7b672a68b659d',1,1,'Active',0,'2013-09-03 14:18:26',0,NULL),
	(823,'dmwakefield','dmwakefield@gmail.com','9ddcd320030babdf4c9836bed0acf29a4dd0f101',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'David','Wakefield','CEO',NULL,'Central Time (US & Canada)','IMG_0273.JPG',926456,'632cee946db83e7a52ce5e8d6f0fed35',1,1,'Active',0,'2013-09-04 11:57:53',823,'2013-09-05 10:02:32'),
	(824,'lkarry','davew@sibme.com','9ddcd320030babdf4c9836bed0acf29a4dd0f101',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'Larry','Karry','CEO',NULL,'American Samoa','IMG_0273.JPG',926456,'677e09724f0e2df9b6c000b75b5da10d',1,1,'Active',823,'2013-09-04 12:06:28',824,'2013-09-17 01:11:35'),
	(825,'gjohnson','dwakefield81@yahoo.com','9ddcd320030babdf4c9836bed0acf29a4dd0f101',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'Gary','Johnson',NULL,NULL,NULL,NULL,NULL,'d554f7bb7be44a7267068a7df88ddd20',1,1,'Active',824,'2013-09-04 12:10:12',824,'2013-09-04 12:08:37'),
	(830,'lsmith','tfaimprovement@yahoo.com','9ddcd320030babdf4c9836bed0acf29a4dd0f101',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'lindsey','Smith',NULL,NULL,NULL,NULL,NULL,'8e82ab7243b7c66d768f1b8ce1c967eb',1,1,'Active',823,'2013-09-05 10:05:38',823,'2013-09-05 10:01:08'),
	(833,'rprasad','ram@rgprasad.com','14f4c249c04d3e9f44e7aa1fc98846005fb229c5',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'Ram','Prasad','',NULL,'Central Time (US & Canada)',NULL,NULL,'013a006f03dbc5392effeb8f18fda755',1,1,'Active',0,'2013-09-11 21:45:35',833,'2013-09-15 06:38:29'),
	(834,'hamidbscs8','hamidbscs8@gmail.com','b2936d8927d9fe5d4cb8fec381216877542a15ec',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'Muhamad','hamid','',NULL,'American Samoa','538285_322227341185803_2084468540_n.jpg',18822,'301ad0e3bd5cb1627a2044908a42fdc2',1,1,'Active',1,'2013-09-13 12:12:32',834,'2013-09-17 05:03:07'),
	(835,'hwattoo','hwattoo@3ssolutions.net','e5d0a553689a762c58f72d61fb00fc0ccbf4fb7d',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'Mr','Hw','',NULL,'American Samoa','threadless.gif',3774,'4d5b995358e7798bc7e9d9db83c612a5',1,1,'Active',1,'2013-09-13 12:24:55',835,'2013-09-13 12:28:34'),
	(838,'','super.user1@hotm.com','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'Super','User1',NULL,NULL,NULL,NULL,NULL,'f9028faec74be6ec9b852b0a542e2f39',1,0,'Invite_Sent',833,'2013-09-15 06:44:01',833,'2013-09-15 06:44:01'),
	(839,'','user2@gm.com','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'User2','',NULL,NULL,NULL,NULL,NULL,'8f7d807e1f53eff5f9efbe5cb81090fb',1,0,'Invite_Sent',833,'2013-09-15 06:44:46',833,'2013-09-15 06:44:46'),
	(840,'','user3@gm.com','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'User3','',NULL,NULL,NULL,NULL,NULL,'fa83a11a198d5a7f0bf77a1987bcd006',1,0,'Invite_Sent',833,'2013-09-15 06:44:46',833,'2013-09-15 06:44:46'),
	(841,'','one.user@ml.com','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'One','user',NULL,NULL,NULL,NULL,NULL,'02a32ad2669e6fe298e607fe7cc0e1a0',1,0,'Invite_Sent',833,'2013-09-15 06:45:46',833,'2013-09-15 06:45:46'),
	(842,'','one@lm.org','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'myuser','one',NULL,NULL,NULL,NULL,NULL,'fc3cf452d3da8402bebb765225ce8c0e',1,0,'Invite_Sent',833,'2013-09-15 06:46:23',833,'2013-09-15 06:46:23'),
	(843,'','two@lm.edu','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'myuser.two','',NULL,NULL,NULL,NULL,NULL,'3d8e28caf901313a554cebc7d32e67e5',1,0,'Invite_Sent',833,'2013-09-15 06:46:23',833,'2013-09-15 06:46:23'),
	(844,'','one@me.c','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'one','',NULL,NULL,NULL,NULL,NULL,'e97ee2054defb209c35fe4dc94599061',1,0,'Invite_Sent',833,'2013-09-15 06:48:17',833,'2013-09-15 06:48:17'),
	(845,'','one@mm.com','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'only','one',NULL,NULL,NULL,NULL,NULL,'b86e8d03fe992d1b0e19656875ee557c',1,0,'Invite_Sent',833,'2013-09-15 06:49:34',833,'2013-09-15 06:49:34'),
	(846,'','plain.user1@mm.com','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'plain','User1',NULL,NULL,NULL,NULL,NULL,'84f7e69969dea92a925508f7c1f9579a',1,0,'Invite_Sent',833,'2013-09-15 06:50:23',833,'2013-09-15 06:50:23'),
	(847,'','two@mll.com','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'pl','UserTwo',NULL,NULL,NULL,NULL,NULL,'f4552671f8909587cf485ea990207f3b',1,0,'Invite_Sent',833,'2013-09-15 06:50:23',833,'2013-09-15 06:50:23'),
	(848,'','onuser@mm.com','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'one','user',NULL,NULL,NULL,NULL,NULL,'362e80d4df43b03ae6d3f8540cd63626',1,0,'Invite_Sent',833,'2013-09-15 06:52:02',833,'2013-09-15 06:52:02'),
	(849,'rgpr','rgprasad5@gmail.com','49642191ee784e2ac868529cc0e4d2696809718e',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'User1','my last',NULL,NULL,NULL,NULL,NULL,'fe8c15fed5f808006ce95eddb7366e35',1,1,'Active',833,'2013-09-15 06:54:07',833,'2013-09-15 06:52:34'),
	(850,'','joh.doe@gmail.com','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'John','Doe',NULL,NULL,NULL,NULL,NULL,'1efa39bcaec6f3900149160693694536',1,0,'Invite_Sent',833,'2013-09-15 06:53:08',833,'2013-09-15 06:53:08'),
	(851,'rprasad3','ramgprasad5@gmail.com','742f12841ebeb3cab80361e2ec7124a22cd9c6d3',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'Ram','Prasad',NULL,NULL,NULL,NULL,NULL,'92fb0c6d1758261f10d052e6e2c1123c',1,1,'Active',0,'2013-09-15 19:08:45',0,NULL),
	(852,'','us.cm@cm.com','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'User1','',NULL,NULL,NULL,NULL,NULL,'22ac3c5a5bf0b520d281c122d1490650',1,0,'Invite_Sent',851,'2013-09-15 07:10:12',851,'2013-09-15 07:10:12'),
	(853,'','su@me.comd','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'su','',NULL,NULL,NULL,NULL,NULL,'aff1621254f7c1be92f64550478c56e6',1,0,'Invite_Sent',851,'2013-09-15 07:10:44',851,'2013-09-15 07:10:44'),
	(854,'ksaleem','khurri.saleem@gmail.com','5662a293b6f5a24f9d4b2ae75a635cdc111eac71',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'Khurram','Saleem',NULL,NULL,NULL,NULL,NULL,'f7e9050c92a851b0016442ab604b0488',1,1,'Active',0,'2013-09-16 06:24:53',0,NULL),
	(855,'miyw','khurram@makeityourweb.com','5662a293b6f5a24f9d4b2ae75a635cdc111eac71',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'Khurram','MIYW',NULL,NULL,NULL,'169484_10151074057426722_2142794718_o.jpg',508179,'addfa9b7e234254d26e9c7f2af1005cb',1,1,'Active',854,'2013-09-16 06:31:13',854,'2013-09-16 06:26:06'),
	(857,'jjtest','khurram@jjtestsite.us','5662a293b6f5a24f9d4b2ae75a635cdc111eac71',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'Khurram','JJ','',NULL,'American Samoa','logo_1.jpg',30404,'847cc55b7032108eee6dd897f3bca8a5',1,1,'Active',855,'2013-09-16 06:51:20',855,'2013-09-17 01:28:18');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_accounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_accounts`;

CREATE TABLE `users_accounts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_default` bit(1) NOT NULL DEFAULT b'0',
  `permission_maintain_folders` int(11) NOT NULL DEFAULT '0',
  `permission_access_video_library` int(11) NOT NULL DEFAULT '0',
  `permission_video_library_upload` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  `permission_administrator_user_new_role` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `account_id` (`account_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users_accounts` WRITE;
/*!40000 ALTER TABLE `users_accounts` DISABLE KEYS */;

INSERT INTO `users_accounts` (`id`, `account_id`, `user_id`, `role_id`, `is_default`, `permission_maintain_folders`, `permission_access_video_library`, `permission_video_library_upload`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`, `permission_administrator_user_new_role`)
VALUES
	(43,9,822,100,b'1',1,1,1,822,'2013-09-03 14:18:26',822,'2013-09-03 14:18:26',0),
	(44,10,823,100,b'1',1,1,1,823,'2013-09-04 11:57:53',823,'2013-09-04 11:57:53',0),
	(45,10,824,110,b'1',0,0,0,823,'2013-09-04 12:02:53',823,'2013-09-04 12:02:53',0),
	(46,10,825,120,b'1',0,0,0,824,'2013-09-04 12:08:37',823,'2013-09-17 02:25:03',1),
	(47,1,826,110,b'1',0,0,0,1,'2013-09-04 15:16:57',1,'2013-09-04 03:16:57',0),
	(48,1,827,110,b'1',0,0,0,1,'2013-09-04 15:47:38',1,'2013-09-04 03:47:38',0),
	(49,1,828,110,b'1',0,0,0,1,'2013-09-04 15:52:43',1,'2013-09-04 03:52:43',0),
	(50,1,829,120,b'1',0,0,0,1,'2013-09-04 15:57:30',1,'2013-09-04 03:57:30',0),
	(51,10,830,120,b'1',0,0,0,823,'2013-09-05 10:01:08',823,'2013-09-17 02:55:20',0),
	(52,1,831,110,b'1',0,0,0,1,'2013-09-11 10:09:11',1,'2013-09-11 10:09:11',0),
	(53,1,832,110,b'1',0,0,0,1,'2013-09-11 13:41:45',1,'2013-09-11 01:41:45',0),
	(54,11,833,100,b'1',1,1,1,833,'2013-09-11 21:45:35',833,'2013-09-11 21:45:35',0),
	(55,1,834,110,b'1',0,0,0,1,'2013-09-13 12:08:18',1,'2013-09-17 02:06:18',0),
	(56,1,835,110,b'1',0,0,0,1,'2013-09-13 12:24:02',1,'2013-09-13 12:24:02',0),
	(58,12,823,110,b'1',0,0,0,836,'2013-09-13 13:24:48',836,'2013-09-13 01:24:48',0),
	(59,12,837,120,b'1',0,0,0,836,'2013-09-13 13:26:52',836,'2013-09-13 01:26:52',0),
	(60,11,838,110,b'1',0,0,0,833,'2013-09-15 18:44:01',833,'2013-09-15 06:44:01',0),
	(61,11,839,110,b'1',0,0,0,833,'2013-09-15 18:44:46',833,'2013-09-15 06:44:46',0),
	(62,11,840,110,b'1',0,0,0,833,'2013-09-15 18:44:46',833,'2013-09-15 06:44:46',0),
	(63,11,841,110,b'1',0,0,0,833,'2013-09-15 18:45:46',833,'2013-09-15 06:45:46',0),
	(64,11,842,110,b'1',0,0,0,833,'2013-09-15 18:46:23',833,'2013-09-15 06:46:23',0),
	(65,11,843,110,b'1',0,0,0,833,'2013-09-15 18:46:24',833,'2013-09-15 06:46:24',0),
	(66,11,844,110,b'1',0,0,0,833,'2013-09-15 18:48:17',833,'2013-09-15 06:48:17',0),
	(67,11,845,110,b'1',0,0,0,833,'2013-09-15 18:49:34',833,'2013-09-15 06:49:34',0),
	(68,11,846,120,b'1',0,0,0,833,'2013-09-15 18:50:23',833,'2013-09-15 06:50:23',0),
	(69,11,847,120,b'1',0,0,0,833,'2013-09-15 18:50:23',833,'2013-09-15 06:50:23',0),
	(70,11,848,120,b'1',0,0,0,833,'2013-09-15 18:52:02',833,'2013-09-15 06:52:02',0),
	(71,11,849,120,b'1',0,0,0,833,'2013-09-15 18:52:34',833,'2013-09-15 06:52:34',0),
	(72,11,850,120,b'1',0,0,0,833,'2013-09-15 18:53:08',833,'2013-09-15 06:53:08',0),
	(73,13,851,100,b'1',1,1,1,851,'2013-09-15 19:08:45',851,'2013-09-15 19:08:45',0),
	(74,13,852,120,b'1',0,0,0,851,'2013-09-15 19:10:12',851,'2013-09-15 07:10:12',0),
	(75,13,853,110,b'1',0,0,0,851,'2013-09-15 19:10:44',851,'2013-09-15 07:10:44',0),
	(76,14,854,100,b'1',1,1,1,854,'2013-09-16 06:24:53',854,'2013-09-16 06:24:53',0),
	(77,14,855,110,b'1',0,0,0,854,'2013-09-16 06:26:06',854,'2013-09-16 06:26:06',0),
	(78,14,856,120,b'1',0,0,0,855,'2013-09-16 06:44:13',855,'2013-09-16 06:44:13',0),
	(79,14,857,120,b'1',0,0,1,855,'2013-09-16 06:46:23',854,'2013-09-17 02:05:30',1),
	(80,14,834,110,b'1',0,0,1,857,'2013-09-17 13:23:57',854,'2013-09-17 02:06:18',1);

/*!40000 ALTER TABLE `users_accounts` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
