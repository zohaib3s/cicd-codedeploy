-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2013 at 07:21 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sibme_new`
--

-- --------------------------------------------------------

--
-- Table structure for table `audit_errors`
--

CREATE TABLE IF NOT EXISTS `audit_errors` (
  `error_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `error_detail` text NOT NULL,
  `path` varchar(512) NOT NULL,
  `form_fields` text NOT NULL,
  PRIMARY KEY (`error_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `audit_errors`
--

INSERT INTO `audit_errors` (`error_id`, `created_date`, `created_by`, `error_detail`, `path`, `form_fields`) VALUES
(1, '2013-10-07 11:37:53', -1, '''<a href="#" onclick="traceToggle(event, ''trace-args-0'')">Dispatcher->dispatch(CakeRequest, CakeResponse)</a> <div id="trace-args-0" class="cake-code-dump" style="display: block;"><pre>object(CakeRequest) {\n	params =&gt; array(\n		[maximum depth reached]\n	)\n	data =&gt; array([maximum depth reached])\n	query =&gt; array([maximum depth reached])\n	url =&gt; &#039;testlkfldsf&#039;\n	base =&gt; &#039;/app&#039;\n	webroot =&gt; &#039;/app/&#039;\n	here =&gt; &#039;/app/testlkfldsf&#039;\n	[protected] _detectors =&gt; array(\n		[maximum depth reached]\n	)\n	[protected] _input =&gt; &#039;&#039;\n}\nobject(CakeResponse) {\n	[protected] _statusCodes =&gt; array(\n		[maximum depth reached]\n	)\n	[protected] _mimeTypes =&gt; array(\n		[maximum depth reached]\n	)\n	[protected] _protocol =&gt; &#039;HTTP/1.1&#039;\n	[protected] _status =&gt; (int) 200\n	[protected] _contentType =&gt; &#039;text/html&#039;\n	[protected] _headers =&gt; array([maximum depth reached])\n	[protected] _body =&gt; null\n	[protected] _file =&gt; null\n	[protected] _fileRange =&gt; null\n	[protected] _charset =&gt; &#039;UTF-8&#039;\n	[protected] _cacheDirectives =&gt; array([maximum depth reached])\n	[protected] _cookies =&gt; array([maximum depth reached])\n}</pre></div>''', 'localhost.sibme.com', '<strong> Error </strong>Controller class TestlkfldsfController could not be found.<br/>'),
(2, '2013-10-07 11:39:37', -1, '''<a href="#" onclick="traceToggle(event, ''trace-args-0'')">Dispatcher->dispatch(CakeRequest, CakeResponse)</a> <div id="trace-args-0" class="cake-code-dump" style="display: block;"><pre>object(CakeRequest) {\n	params =&gt; array(\n		[maximum depth reached]\n	)\n	data =&gt; array([maximum depth reached])\n	query =&gt; array([maximum depth reached])\n	url =&gt; &#039;testlkfldsf&#039;\n	base =&gt; &#039;/app&#039;\n	webroot =&gt; &#039;/app/&#039;\n	here =&gt; &#039;/app/testlkfldsf&#039;\n	[protected] _detectors =&gt; array(\n		[maximum depth reached]\n	)\n	[protected] _input =&gt; &#039;&#039;\n}\nobject(CakeResponse) {\n	[protected] _statusCodes =&gt; array(\n		[maximum depth reached]\n	)\n	[protected] _mimeTypes =&gt; array(\n		[maximum depth reached]\n	)\n	[protected] _protocol =&gt; &#039;HTTP/1.1&#039;\n	[protected] _status =&gt; (int) 200\n	[protected] _contentType =&gt; &#039;text/html&#039;\n	[protected] _headers =&gt; array([maximum depth reached])\n	[protected] _body =&gt; null\n	[protected] _file =&gt; null\n	[protected] _fileRange =&gt; null\n	[protected] _charset =&gt; &#039;UTF-8&#039;\n	[protected] _cacheDirectives =&gt; array([maximum depth reached])\n	[protected] _cookies =&gt; array([maximum depth reached])\n}</pre></div>''', 'localhost.sibme.com', '<strong> Error </strong>Controller class TestlkfldsfController could not be found.<br/>'),
(3, '2013-10-07 11:49:32', -1, '''<a href="#" onclick="traceToggle(event, ''file-excerpt-%s'')">APP\\webroot\\index.php line 110</a><div id="file-excerpt-%s" class="cake-code-dump" style="display:block;"><pre><code><span style="color: #000000"><span style="color: #0000BB">$Dispatcher</span><span style="color: #007700">-&gt;</span><span style="color: #0000BB">dispatch</span><span style="color: #007700">(</span></span></code>\n<code><span style="color: #000000"><span style="color: #0000BB">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #007700">new&nbsp;</span><span style="color: #0000BB">CakeRequest</span><span style="color: #007700">(),</span></span></code>\n<code><span style="color: #000000"><span style="color: #0000BB">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style="color: #007700">new&nbsp;</span><span style="color: #0000BB">CakeResponse</span><span style="color: #007700">()</span></span></code>\n<span class="code-highlight"><code><span style="color: #000000"><span style="color: #0000BB"></span><span style="color: #007700">);</span></span></code></span>\n<code><span style="color: #000000"><span style="color: #0000BB"></span></span></code></pre></div> <a href="#" onclick="traceToggle(event, ''trace-args-0'')">Dispatcher->dispatch(CakeRequest, CakeResponse)</a> <div id="trace-args-0" class="cake-code-dump" style="display: block;"><pre>object(CakeRequest) {\n	params =&gt; array(\n		[maximum depth reached]\n	)\n	data =&gt; array([maximum depth reached])\n	query =&gt; array([maximum depth reached])\n	url =&gt; &#039;testlkfldsf&#039;\n	base =&gt; &#039;/app&#039;\n	webroot =&gt; &#039;/app/&#039;\n	here =&gt; &#039;/app/testlkfldsf&#039;\n	[protected] _detectors =&gt; array(\n		[maximum depth reached]\n	)\n	[protected] _input =&gt; &#039;&#039;\n}\nobject(CakeResponse) {\n	[protected] _statusCodes =&gt; array(\n		[maximum depth reached]\n	)\n	[protected] _mimeTypes =&gt; array(\n		[maximum depth reached]\n	)\n	[protected] _protocol =&gt; &#039;HTTP/1.1&#039;\n	[protected] _status =&gt; (int) 200\n	[protected] _contentType =&gt; &#039;text/html&#039;\n	[protected] _headers =&gt; array([maximum depth reached])\n	[protected] _body =&gt; null\n	[protected] _file =&gt; null\n	[protected] _fileRange =&gt; null\n	[protected] _charset =&gt; &#039;UTF-8&#039;\n	[protected] _cacheDirectives =&gt; array([maximum depth reached])\n	[protected] _cookies =&gt; array([maximum depth reached])\n}</pre></div>''', 'localhost.sibme.com', '<strong> Error </strong>Controller class TestlkfldsfController could not be found.<br/>');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
