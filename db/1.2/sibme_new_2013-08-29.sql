# ************************************************************
# Sequel Pro SQL dump
# Version 3408
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.1.65)
# Database: sibme_new
# Generation Time: 2013-08-29 11:32:48 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table account_folder_documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folder_documents`;

CREATE TABLE `account_folder_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  `is_viewed` bit(1) NOT NULL DEFAULT b'0',
  `title` varchar(255) DEFAULT NULL,
  `desc` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `account_folder_documents` WRITE;
/*!40000 ALTER TABLE `account_folder_documents` DISABLE KEYS */;

INSERT INTO `account_folder_documents` (`id`, `account_folder_id`, `document_id`, `is_viewed`, `title`, `desc`)
VALUES
	(5,7,27,b'1','My Video','sadasd'),
	(9,7,32,b'1','Testing','123'),
	(10,12,33,b'1','kk','asdsdd'),
	(11,17,38,b'1','My Test Doc','Testing Library'),
	(12,18,39,b'1','Test Video','Description ....'),
	(13,7,40,b'1','K Video io','IO Upload.'),
	(14,7,41,b'1','Testing','Tester'),
	(15,7,43,b'1','My 100','100 desc'),
	(16,7,44,b'1','Another 100','100 here.'),
	(17,7,45,b'1','K Test 2','testing'),
	(18,7,46,b'1','100 3','100 3 attempt');

/*!40000 ALTER TABLE `account_folder_documents` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_folder_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folder_groups`;

CREATE TABLE `account_folder_groups` (
  `account_folder_group_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`account_folder_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `account_folder_groups` WRITE;
/*!40000 ALTER TABLE `account_folder_groups` DISABLE KEYS */;

INSERT INTO `account_folder_groups` (`account_folder_group_id`, `account_folder_id`, `group_id`, `role_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(1,5,1,210,1,'2013-08-23 19:14:10',1,'2013-08-23 07:14:10'),
	(3,7,1,210,1,'2013-08-24 20:31:44',1,'2013-08-24 08:31:44'),
	(4,8,1,210,1,'2013-08-24 20:32:45',1,'2013-08-24 08:32:45'),
	(5,6,1,210,1,'2013-08-26 06:32:21',1,'2013-08-26 06:32:21');

/*!40000 ALTER TABLE `account_folder_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_folder_subjects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folder_subjects`;

CREATE TABLE `account_folder_subjects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) DEFAULT NULL,
  `subject_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `account_folder_subjects` WRITE;
/*!40000 ALTER TABLE `account_folder_subjects` DISABLE KEYS */;

INSERT INTO `account_folder_subjects` (`id`, `account_folder_id`, `subject_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(1,10,3,1,'2013-08-28 05:56:36',1,'2013-08-28 05:56:36'),
	(2,10,2,1,'2013-08-28 05:56:36',1,'2013-08-28 05:56:36'),
	(3,11,3,1,'2013-08-28 06:11:33',1,'2013-08-28 06:11:33'),
	(4,12,3,1,'2013-08-28 06:12:29',1,'2013-08-28 06:12:29'),
	(5,13,3,1,'2013-08-28 15:28:36',1,'2013-08-28 03:28:36'),
	(6,14,3,1,'2013-08-28 15:31:24',1,'2013-08-28 03:31:24'),
	(7,15,3,1,'2013-08-28 15:33:50',1,'2013-08-28 03:33:50'),
	(8,16,3,1,'2013-08-28 15:37:41',1,'2013-08-28 03:37:41'),
	(9,17,3,1,'2013-08-28 15:40:28',1,'2013-08-28 03:40:28'),
	(10,18,3,1,'2013-08-28 16:29:37',1,'2013-08-28 04:29:37');

/*!40000 ALTER TABLE `account_folder_subjects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_folder_topics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folder_topics`;

CREATE TABLE `account_folder_topics` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `account_folder_topics` WRITE;
/*!40000 ALTER TABLE `account_folder_topics` DISABLE KEYS */;

INSERT INTO `account_folder_topics` (`id`, `account_folder_id`, `topic_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(1,NULL,1,NULL,NULL,NULL,NULL),
	(2,11,1,1,'2013-08-28 06:11:33',1,'2013-08-28 06:11:33'),
	(3,12,1,1,'2013-08-28 06:12:29',1,'2013-08-28 06:12:29'),
	(4,13,1,1,'2013-08-28 15:28:36',1,'2013-08-28 03:28:36'),
	(5,14,1,1,'2013-08-28 15:31:24',1,'2013-08-28 03:31:24'),
	(6,15,1,1,'2013-08-28 15:33:50',1,'2013-08-28 03:33:50'),
	(7,16,1,1,'2013-08-28 15:37:41',1,'2013-08-28 03:37:41'),
	(8,17,1,1,'2013-08-28 15:40:28',1,'2013-08-28 03:40:28'),
	(9,18,1,1,'2013-08-28 16:29:37',1,'2013-08-28 04:29:37');

/*!40000 ALTER TABLE `account_folder_topics` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_folder_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folder_users`;

CREATE TABLE `account_folder_users` (
  `account_folder_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`account_folder_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `account_folder_users` WRITE;
/*!40000 ALTER TABLE `account_folder_users` DISABLE KEYS */;

INSERT INTO `account_folder_users` (`account_folder_user_id`, `account_folder_id`, `user_id`, `role_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(7,5,3,200,1,'2013-08-23 19:14:10',1,'2013-08-23 07:14:10'),
	(8,5,814,210,1,'2013-08-23 19:14:10',1,'2013-08-23 07:14:10'),
	(11,7,3,200,1,'2013-08-24 20:31:44',1,'2013-08-24 08:31:44'),
	(12,7,814,210,1,'2013-08-24 20:31:44',1,'2013-08-24 08:31:44'),
	(13,8,3,200,1,'2013-08-24 20:32:45',1,'2013-08-24 08:32:45'),
	(14,8,814,210,1,'2013-08-24 20:32:45',1,'2013-08-24 08:32:45'),
	(17,6,3,200,1,'2013-08-26 06:32:21',1,'2013-08-26 06:32:21'),
	(18,6,814,210,1,'2013-08-26 06:32:21',1,'2013-08-26 06:32:21');

/*!40000 ALTER TABLE `account_folder_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_folderdocument_attachments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folderdocument_attachments`;

CREATE TABLE `account_folderdocument_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_folder_document_id` int(11) NOT NULL,
  `attach_id` int(11) NOT NULL COMMENT 'account_folder_attach_document_id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `account_folderdocument_attachments` WRITE;
/*!40000 ALTER TABLE `account_folderdocument_attachments` DISABLE KEYS */;

INSERT INTO `account_folderdocument_attachments` (`id`, `account_folder_document_id`, `attach_id`)
VALUES
	(1,15,45),
	(2,15,40);

/*!40000 ALTER TABLE `account_folderdocument_attachments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_folders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folders`;

CREATE TABLE `account_folders` (
  `account_folder_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `folder_type` tinyint(4) NOT NULL COMMENT '1=huddle, 2=video library, 3=my files, etc.',
  `parent_folder_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `desc` varchar(4000) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`account_folder_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `account_folders` WRITE;
/*!40000 ALTER TABLE `account_folders` DISABLE KEYS */;

INSERT INTO `account_folders` (`account_folder_id`, `account_id`, `folder_type`, `parent_folder_id`, `name`, `desc`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`, `active`)
VALUES
	(5,1,1,NULL,'Khurram\'s Coaching classes','Khurram\'s Coaching classes',1,'2013-08-23 19:14:10',1,'2013-08-23 07:14:10',0),
	(6,1,1,NULL,'Mu Huddlesss','Huddle Descing',1,'2013-08-24 20:31:04',1,'2013-08-26 06:32:21',1),
	(7,1,1,NULL,'Mu Huddle','Huddle Desc',1,'2013-08-24 20:31:44',1,'2013-08-24 08:31:44',1),
	(8,1,1,NULL,'Mu Huddle','Huddle Desc',1,'2013-08-24 20:32:45',1,'2013-08-24 08:32:45',0),
	(17,1,2,NULL,'My Test Doc','Testing Library',1,'2013-08-28 15:40:28',1,'2013-08-28 15:40:28',1),
	(18,1,2,NULL,'Test Video','Description ....',1,'2013-08-28 16:29:37',1,'2013-08-28 16:29:37',1);

/*!40000 ALTER TABLE `account_folders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_folders_meta_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_folders_meta_data`;

CREATE TABLE `account_folders_meta_data` (
  `account_folder_meta_data_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) NOT NULL,
  `meta_data_name` varchar(64) NOT NULL COMMENT 'tag, topic, subject, etc',
  `meta_data_value` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`account_folder_meta_data_id`),
  UNIQUE KEY `account_folders_meta_data_id_UNIQUE` (`account_folder_meta_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `account_folders_meta_data` WRITE;
/*!40000 ALTER TABLE `account_folders_meta_data` DISABLE KEYS */;

INSERT INTO `account_folders_meta_data` (`account_folder_meta_data_id`, `account_folder_id`, `meta_data_name`, `meta_data_value`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(3,5,'message','Hello people!!!',1,'2013-08-23 19:14:10',1,'2013-08-23 07:14:10'),
	(4,6,'message','Hello World!!! &nbsp; dffdsfdsfdsf',1,'2013-08-24 20:31:04',1,'2013-08-26 06:32:21'),
	(5,7,'message','Hello World!!!',1,'2013-08-24 20:31:44',1,'2013-08-24 08:31:44'),
	(6,8,'message','Hello World!!!',1,'2013-08-24 20:32:45',1,'2013-08-24 08:32:45'),
	(7,10,'tag','kkk',1,'2013-08-28 05:56:35',1,'2013-08-28 05:56:35'),
	(8,10,'tag','kk',1,'2013-08-28 05:56:36',1,'2013-08-28 05:56:36'),
	(9,11,'tag','kkk',1,'2013-08-28 06:11:33',1,'2013-08-28 06:11:33'),
	(10,11,'tag','k',1,'2013-08-28 06:11:33',1,'2013-08-28 06:11:33'),
	(11,12,'tag','kk',1,'2013-08-28 06:12:29',1,'2013-08-28 06:12:29'),
	(12,12,'tag','dd',1,'2013-08-28 06:12:29',1,'2013-08-28 06:12:29'),
	(13,13,'tag','Test',1,'2013-08-28 15:28:36',1,'2013-08-28 03:28:36'),
	(14,13,'tag','kk',1,'2013-08-28 15:28:36',1,'2013-08-28 03:28:36'),
	(15,14,'tag','testing',1,'2013-08-28 15:31:24',1,'2013-08-28 03:31:24'),
	(16,15,'tag','ll',1,'2013-08-28 15:33:50',1,'2013-08-28 03:33:50'),
	(17,16,'tag','kkk',1,'2013-08-28 15:37:41',1,'2013-08-28 03:37:41'),
	(18,17,'tag','kk',1,'2013-08-28 15:40:28',1,'2013-08-28 03:40:28'),
	(19,17,'tag','asdd',1,'2013-08-28 15:40:28',1,'2013-08-28 03:40:28'),
	(20,18,'tag','Test',1,'2013-08-28 16:29:37',1,'2013-08-28 04:29:37'),
	(21,18,'tag','Khurram',1,'2013-08-28 16:29:37',1,'2013-08-28 04:29:37');

/*!40000 ALTER TABLE `account_folders_meta_data` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table account_meta_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `account_meta_data`;

CREATE TABLE `account_meta_data` (
  `account_meta_data_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `meta_data_name` varchar(64) NOT NULL COMMENT 'tag, topic, subject, etc',
  `meta_data_value` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`account_meta_data_id`),
  UNIQUE KEY `account_meta_data_id_UNIQUE` (`account_meta_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table accounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `accounts`;

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_account_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `custom_url` varchar(255) DEFAULT NULL,
  `image_logo` varchar(255) DEFAULT NULL,
  `header_background_color` varchar(255) DEFAULT NULL,
  `text_link_color` varchar(255) DEFAULT NULL,
  `plan_id` int(11) DEFAULT NULL,
  `storage` int(11) DEFAULT NULL,
  `storage_used` int(11) DEFAULT NULL,
  `card_type` varchar(255) DEFAULT NULL,
  `last_4digits` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `subdomain` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `braintree_customer_id` text,
  `braintree_subscription_id` text,
  `nav_bg_color` varchar(255) DEFAULT NULL,
  `usernav_bg_color` varchar(255) DEFAULT NULL,
  `in_trial` tinyint(1) DEFAULT NULL,
  `has_credit_card` tinyint(1) DEFAULT NULL,
  `suspended_at` datetime DEFAULT NULL,
  `is_suspended` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `index_accounts_on_user_id` (`user_id`),
  KEY `index_accounts_on_plan_id` (`plan_id`),
  CONSTRAINT `fk_accounts_plans` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;

INSERT INTO `accounts` (`id`, `parent_account_id`, `user_id`, `company_name`, `custom_url`, `image_logo`, `header_background_color`, `text_link_color`, `plan_id`, `storage`, `storage_used`, `card_type`, `last_4digits`, `created_at`, `updated_at`, `subdomain`, `is_active`, `braintree_customer_id`, `braintree_subscription_id`, `nav_bg_color`, `usernav_bg_color`, `in_trial`, `has_credit_card`, `suspended_at`, `is_suspended`)
VALUES
	(1,0,NULL,'Sibme',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-20 00:00:00','2013-08-20 00:00:00',NULL,1,NULL,NULL,NULL,NULL,1,0,NULL,0),
	(2,0,NULL,'Sibme QA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2013-08-20 00:00:00','2013-08-20 00:00:00',NULL,1,NULL,NULL,NULL,NULL,1,0,NULL,0);

/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table audit_emails
# ------------------------------------------------------------

DROP TABLE IF EXISTS `audit_emails`;

CREATE TABLE `audit_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email_from` text,
  `email_to` text,
  `email_cc` text,
  `email_bcc` text,
  `email_subject` varchar(4000) DEFAULT NULL,
  `email_body` text,
  `is_html` bit(1) NOT NULL,
  `error_msg` text,
  `sent_date` datetime DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `audit_emails` WRITE;
/*!40000 ALTER TABLE `audit_emails` DISABLE KEYS */;

INSERT INTO `audit_emails` (`id`, `email_from`, `email_to`, `email_cc`, `email_bcc`, `email_subject`, `email_body`, `is_html`, `error_msg`, `sent_date`, `account_id`)
VALUES
	(1,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/users/activation/c4ca4238a0b923820dcc509a6f75849b/d0n2g70hf40vtxtdx58f/96ea64f3a1aa2fd00c72faacf0cb8ac9\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 01:24:20',1),
	(2,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/users/activation/c4ca4238a0b923820dcc509a6f75849b/nd4cdbrhmqw5c1jzltme/da8ce53cf0240070ce6c69c48cd588ee\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 01:25:05',1),
	(3,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/users/activation/c4ca4238a0b923820dcc509a6f75849b/vxbhv3cqgfq54o19hrxp/82489c9737cc245530c7a6ebef3753ec\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 01:28:00',1),
	(4,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/ksnnt73h4bqbrwf047el/7c590f01490190db0ed02a5070e20f01\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 04:35:42',1),
	(5,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/625gia9s4foa7wqwclww/35cf8659cfcb13224cbd47863a34fc58\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 04:48:27',1),
	(6,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/mtc16p4d4l15vsd62w2m/beb22fb694d513edcf5533cf006dfeae\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 05:45:24',1),
	(7,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/bmk7wk4dj8f84kalsl8x/9e3cfc48eccf81a0d57663e129aef3cb\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 05:53:16',1),
	(8,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/nva9ts6mb3irrsq25dn7/28267ab848bcf807b2ed53c3a8f8fc8a\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 06:54:34',1),
	(9,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/uybtic176xwcwiogi1lv/7a53928fa4dd31e82c6ef826f341daec\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 06:58:15',1),
	(10,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/l5v41yky0xy3b47lnugp/1905aedab9bf2477edc068a355bba31a\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 14:28:53',1),
	(11,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/4zo6jrij8555shg8ok35/1141938ba2c2b13f5505d7c424ebae5f\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 14:45:31',1),
	(12,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/sfraeu0n0bcaxg4c7z8r/1aa48fc4880bb0c9b8a3bf979d3b917e\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 14:52:46',1),
	(13,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/c57zohpea6wxnw1vsoas/dc5689792e08eb2e219dce49e64c885b\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 14:55:41',1),
	(14,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/xrt5mxywobro01ceze7z/846c260d715e5b854ffad5f70a516c88\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 15:07:15',1),
	(15,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/4xsjjd0yghikskk5werh/d58072be2820e8682c0a27c0518e805e\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 16:51:51',1),
	(16,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href=\"#\" target=\"_blank\">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://localhost/sibme/app//Users/login\" target=\"_blank\">http://localhost/sibme/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> ksaleem3    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-08-23 16:52:33',NULL),
	(17,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/6x9eadwlb129tx15ej72/6e7b33fdea3adc80ebd648fffb665bb8\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 16:54:57',1),
	(18,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href=\"#\" target=\"_blank\">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://localhost/sibme/app//Users/login\" target=\"_blank\">http://localhost/sibme/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> ksaleem3    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-08-23 16:55:33',1),
	(19,'do-not-reply@sibme.com','info@3ssolutions.net',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/k41o1qur8lmaagiqphpc/a8ecbabae151abacba7dbde04f761c37\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 16:58:23',1),
	(20,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/4q5cw0st7y9t20fyg0px/32b30a250abd6331e03a2a1f16466346\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 17:01:33',1),
	(21,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saeems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/9643hnkinr7qpwda0s7l/b6edc1cd1f36e45daf6d7824d7bb2283\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 17:58:41',1),
	(22,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/fl7xx8lk0y7dcqugbrw1/670e8a43b246801ca1eaca97b3e19189\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 18:26:31',1),
	(23,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/f5eqwhsjc4s5vfne9i5m/81e74d678581a3bb7a720b019f4f1a93\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 18:29:55',1),
	(24,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href=\"#\" target=\"_blank\">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://localhost/sibme/app//Users/login\" target=\"_blank\">http://localhost/sibme/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> ksaleem3    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-08-23 18:30:31',1),
	(25,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/qwamqlvxfjzf47f2w23r/e0cf1f47118daebc5b16269099ad7347\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 18:31:11',1),
	(26,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/os5fvbptg6al7xt1vzga/96b9bff013acedfb1d140579e2fbeb63\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 18:33:31',1),
	(27,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Welcome to Sibme!','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Hi Khurram Saleem, thanks for signing up for your free 60 day trial!</p>\n\n    <br>\n    <a href=\"#\" target=\"_blank\">Subscribe Now</a>\n    <br>\n    <p>Please feel free to use <span class=\"il\">Sibme</span> as much as you would like.  We know it\'s not perfect, so please let us know how we can improve.</p>\n    <br>\n    <p>Got questions?  Contact us at <a href=\"mailto:info@sibme.com\" target=\"_blank\">info@<span class=\"il\">sibme</span>.com</a></p>\n    <br>\n    <p>Thanks again for choosing <span class=\"il\">Sibme</span>!</p>\n    <br>\n    <p>\n        <strong>Sign into your account</strong><br>\n        <a href=\"http://localhost/sibme/app//Users/login\" target=\"_blank\">http://localhost/sibme/app/<span class=\"il\">sibme</span>/Users/<wbr>login</a>\n    </p>\n\n    <p>\n        <strong>Username:</strong> ksaleem3    </p>\n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-08-23 18:34:03',1),
	(28,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'You\'ve been invited to join \'Khurram Saleems\' Sibme Account!','<h4><strong>Khurram Saleem</strong> invited you to become a user in their Sibme Account:</h4>\n<h2 style=\"color:#3d9bc2;\">Sibme</h2>\n<p>Sibme is a tool we use to gather and deliver feedback to improve teaching and learning.\n    Huddles and Recruiting Huddles help us bring together through video collaboration.</p>\n\n<a href=\"http://localhost/sibme/app/Users/activation/c4ca4238a0b923820dcc509a6f75849b/xib9bnmv9i27nw7avaaz/71ad16ad2c4d81f348082ff6c4b20768\">Accept this invitation to get started</a>\n<br />\n<p>Read <strong>Khurram Saleem</strong>â€™s message below:</p>\n\n<p></p>\n\n<p>Still have questions?</p>\n<p>Contact <strong>Khurram Saleem</strong> at khurri.saleem@gmail.com</p>\n',b'1',NULL,'2013-08-23 18:34:47',1),
	(29,'do-not-reply@sibme.com',NULL,NULL,NULL,'  created a new Sibme Coaching Huddle: ','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p><pre class=\"cake-error\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e58840f7-trace\').style.display = (document.getElementById(\'cakeErr52195e58840f7-trace\').style.display == \'none\' ? \'\' : \'none\');\"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>5</b>]<div id=\"cakeErr52195e58840f7-trace\" class=\"cake-stack-trace\" style=\"display: none;\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e58840f7-code\').style.display = (document.getElementById(\'cakeErr52195e58840f7-code\').style.display == \'none\' ? \'\' : \'none\')\">Code</a> <a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e58840f7-context\').style.display = (document.getElementById(\'cakeErr52195e58840f7-context\').style.display == \'none\' ? \'\' : \'none\')\">Context</a><pre id=\"cakeErr52195e58840f7-code\" class=\"cake-code-dump\" style=\"display: none;\"><code><span style=\"color: #000000\"><span style=\"color: #0000BB\">?&gt;</span></span></code>\n<code><span style=\"color: #000000\"><span style=\"color: #0000BB\"></span><span style=\"color: #007700\">&lt;</span><span style=\"color: #0000BB\">div&nbsp;id</span><span style=\"color: #007700\">=</span><span style=\"color: #DD0000\">\":6r\"&nbsp;</span><span style=\"color: #0000BB\">style</span><span style=\"color: #007700\">=</span><span style=\"color: #DD0000\">\"overflow:&nbsp;hidden;\"</span><span style=\"color: #007700\">&gt;</span></span></code>\n<span class=\"code-highlight\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'first_name\'</span><span style=\"color: #007700\">]&nbsp;.&nbsp;</span><span style=\"color: #DD0000\">\"&nbsp;\"&nbsp;</span><span style=\"color: #007700\">.&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'last_name\'</span><span style=\"color: #007700\">]&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>&nbsp;invited&nbsp;you&nbsp;to&nbsp;a&nbsp;Coaching&nbsp;Huddle::&lt;/p&gt;</span></code></span></pre><pre id=\"cakeErr52195e58840f7-context\" class=\"cake-context\" style=\"display: none;\">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class=\"stack-trace\">include - APP/View/Elements/emails/new_huddle.ctp, line 5\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre><pre class=\"cake-error\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5884a8d-trace\').style.display = (document.getElementById(\'cakeErr52195e5884a8d-trace\').style.display == \'none\' ? \'\' : \'none\');\"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>5</b>]<div id=\"cakeErr52195e5884a8d-trace\" class=\"cake-stack-trace\" style=\"display: none;\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5884a8d-code\').style.display = (document.getElementById(\'cakeErr52195e5884a8d-code\').style.display == \'none\' ? \'\' : \'none\')\">Code</a> <a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5884a8d-context\').style.display = (document.getElementById(\'cakeErr52195e5884a8d-context\').style.display == \'none\' ? \'\' : \'none\')\">Context</a><pre id=\"cakeErr52195e5884a8d-code\" class=\"cake-code-dump\" style=\"display: none;\"><code><span style=\"color: #000000\"><span style=\"color: #0000BB\">?&gt;</span></span></code>\n<code><span style=\"color: #000000\"><span style=\"color: #0000BB\"></span><span style=\"color: #007700\">&lt;</span><span style=\"color: #0000BB\">div&nbsp;id</span><span style=\"color: #007700\">=</span><span style=\"color: #DD0000\">\":6r\"&nbsp;</span><span style=\"color: #0000BB\">style</span><span style=\"color: #007700\">=</span><span style=\"color: #DD0000\">\"overflow:&nbsp;hidden;\"</span><span style=\"color: #007700\">&gt;</span></span></code>\n<span class=\"code-highlight\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'first_name\'</span><span style=\"color: #007700\">]&nbsp;.&nbsp;</span><span style=\"color: #DD0000\">\"&nbsp;\"&nbsp;</span><span style=\"color: #007700\">.&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'last_name\'</span><span style=\"color: #007700\">]&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>&nbsp;invited&nbsp;you&nbsp;to&nbsp;a&nbsp;Coaching&nbsp;Huddle::&lt;/p&gt;</span></code></span></pre><pre id=\"cakeErr52195e5884a8d-context\" class=\"cake-context\" style=\"display: none;\">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class=\"stack-trace\">include - APP/View/Elements/emails/new_huddle.ctp, line 5\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre>  invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\"><pre class=\"cake-error\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5885397-trace\').style.display = (document.getElementById(\'cakeErr52195e5885397-trace\').style.display == \'none\' ? \'\' : \'none\');\"><b>Notice</b> (8)</a>: Undefined index: AccountFolder [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>7</b>]<div id=\"cakeErr52195e5885397-trace\" class=\"cake-stack-trace\" style=\"display: none;\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5885397-code\').style.display = (document.getElementById(\'cakeErr52195e5885397-code\').style.display == \'none\' ? \'\' : \'none\')\">Code</a> <a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5885397-context\').style.display = (document.getElementById(\'cakeErr52195e5885397-context\').style.display == \'none\' ? \'\' : \'none\')\">Context</a><pre id=\"cakeErr52195e5885397-code\" class=\"cake-code-dump\" style=\"display: none;\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'first_name\'</span><span style=\"color: #007700\">]&nbsp;.&nbsp;</span><span style=\"color: #DD0000\">\"&nbsp;\"&nbsp;</span><span style=\"color: #007700\">.&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'last_name\'</span><span style=\"color: #007700\">]&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>&nbsp;invited&nbsp;you&nbsp;to&nbsp;a&nbsp;Coaching&nbsp;Huddle::&lt;/p&gt;</span></code>\n<code><span style=\"color: #000000\"><span style=\"color: #0000BB\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"color: #007700\">&lt;</span><span style=\"color: #0000BB\">br</span><span style=\"color: #007700\">/&gt;</span></span></code>\n<span class=\"code-highlight\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;h2&nbsp;style=\"color:#3d9bc2\"&gt;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'AccountFolder\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'name\'</span><span style=\"color: #007700\">];&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>&lt;/h2&gt;</span></code></span></pre><pre id=\"cakeErr52195e5885397-context\" class=\"cake-context\" style=\"display: none;\">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class=\"stack-trace\">include - APP/View/Elements/emails/new_huddle.ctp, line 7\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre></h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://localhost/sibme/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read <pre class=\"cake-error\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e588607b-trace\').style.display = (document.getElementById(\'cakeErr52195e588607b-trace\').style.display == \'none\' ? \'\' : \'none\');\"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>12</b>]<div id=\"cakeErr52195e588607b-trace\" class=\"cake-stack-trace\" style=\"display: none;\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e588607b-code\').style.display = (document.getElementById(\'cakeErr52195e588607b-code\').style.display == \'none\' ? \'\' : \'none\')\">Code</a> <a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e588607b-context\').style.display = (document.getElementById(\'cakeErr52195e588607b-context\').style.display == \'none\' ? \'\' : \'none\')\">Context</a><pre id=\"cakeErr52195e588607b-code\" class=\"cake-code-dump\" style=\"display: none;\"><code><span style=\"color: #000000\"><span style=\"color: #0000BB\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"color: #007700\">&lt;</span><span style=\"color: #0000BB\">br</span><span style=\"color: #007700\">/&gt;</span></span></code>\n<code><span style=\"color: #000000\"><span style=\"color: #0000BB\"></span></span></code>\n<span class=\"code-highlight\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;Read&nbsp;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'first_name\'</span><span style=\"color: #007700\">]&nbsp;.&nbsp;</span><span style=\"color: #DD0000\">\"&nbsp;\"&nbsp;</span><span style=\"color: #007700\">.&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'last_name\'</span><span style=\"color: #007700\">]&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>\'s&nbsp;message&nbsp;below:&lt;/p&gt;</span></code></span></pre><pre id=\"cakeErr52195e588607b-context\" class=\"cake-context\" style=\"display: none;\">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class=\"stack-trace\">include - APP/View/Elements/emails/new_huddle.ctp, line 12\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre><pre class=\"cake-error\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e588694a-trace\').style.display = (document.getElementById(\'cakeErr52195e588694a-trace\').style.display == \'none\' ? \'\' : \'none\');\"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>12</b>]<div id=\"cakeErr52195e588694a-trace\" class=\"cake-stack-trace\" style=\"display: none;\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e588694a-code\').style.display = (document.getElementById(\'cakeErr52195e588694a-code\').style.display == \'none\' ? \'\' : \'none\')\">Code</a> <a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e588694a-context\').style.display = (document.getElementById(\'cakeErr52195e588694a-context\').style.display == \'none\' ? \'\' : \'none\')\">Context</a><pre id=\"cakeErr52195e588694a-code\" class=\"cake-code-dump\" style=\"display: none;\"><code><span style=\"color: #000000\"><span style=\"color: #0000BB\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"color: #007700\">&lt;</span><span style=\"color: #0000BB\">br</span><span style=\"color: #007700\">/&gt;</span></span></code>\n<code><span style=\"color: #000000\"><span style=\"color: #0000BB\"></span></span></code>\n<span class=\"code-highlight\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;Read&nbsp;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'first_name\'</span><span style=\"color: #007700\">]&nbsp;.&nbsp;</span><span style=\"color: #DD0000\">\"&nbsp;\"&nbsp;</span><span style=\"color: #007700\">.&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'last_name\'</span><span style=\"color: #007700\">]&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>\'s&nbsp;message&nbsp;below:&lt;/p&gt;</span></code></span></pre><pre id=\"cakeErr52195e588694a-context\" class=\"cake-context\" style=\"display: none;\">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class=\"stack-trace\">include - APP/View/Elements/emails/new_huddle.ctp, line 12\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre> \'s message below:</p>\n    <p><pre class=\"cake-error\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5887219-trace\').style.display = (document.getElementById(\'cakeErr52195e5887219-trace\').style.display == \'none\' ? \'\' : \'none\');\"><b>Notice</b> (8)</a>: Undefined index: account_folders_meta_data [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>13</b>]<div id=\"cakeErr52195e5887219-trace\" class=\"cake-stack-trace\" style=\"display: none;\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5887219-code\').style.display = (document.getElementById(\'cakeErr52195e5887219-code\').style.display == \'none\' ? \'\' : \'none\')\">Code</a> <a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5887219-context\').style.display = (document.getElementById(\'cakeErr52195e5887219-context\').style.display == \'none\' ? \'\' : \'none\')\">Context</a><pre id=\"cakeErr52195e5887219-code\" class=\"cake-code-dump\" style=\"display: none;\"><code><span style=\"color: #000000\"><span style=\"color: #0000BB\"></span></span></code>\n<code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;Read&nbsp;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'first_name\'</span><span style=\"color: #007700\">]&nbsp;.&nbsp;</span><span style=\"color: #DD0000\">\"&nbsp;\"&nbsp;</span><span style=\"color: #007700\">.&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'last_name\'</span><span style=\"color: #007700\">]&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>\'s&nbsp;message&nbsp;below:&lt;/p&gt;</span></code>\n<span class=\"code-highlight\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'account_folders_meta_data\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'message\'</span><span style=\"color: #007700\">];&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>&lt;/p&gt;</span></code></span></pre><pre id=\"cakeErr52195e5887219-context\" class=\"cake-context\" style=\"display: none;\">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-24 20:31:04&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-24 08:31:04&#039;\n		),\n		&#039;User&#039; =&gt; array(\n			&#039;username&#039; =&gt; &#039;ksaleem&#039;,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;email&#039; =&gt; &#039;khurri.saleem@gmail.com&#039;\n		),\n		&#039;account_folders_meta_data&#039; =&gt; array(\n			&#039;message&#039; =&gt; &#039;Hello World!!!&#039;\n		)\n	)\n)\n$sibme_base_url = &#039;http://localhost/sibme/app/&#039;</pre><pre class=\"stack-trace\">include - APP/View/Elements/emails/new_huddle.ctp, line 13\nView::_evaluate() - CORE/Cake/View/View.php, line 945\nView::_render() - CORE/Cake/View/View.php, line 907\nView::_renderElement() - CORE/Cake/View/View.php, line 1208\nView::element() - CORE/Cake/View/View.php, line 414\nAppController::newHuddleEmail() - APP/Controller/AppController.php, line 170\nHuddlesController::add() - APP/Controller/HuddlesController.php, line 171\nReflectionMethod::invokeArgs() - [internal], line ??\nController::invokeAction() - CORE/Cake/Controller/Controller.php, line 490\nDispatcher::_invoke() - CORE/Cake/Routing/Dispatcher.php, line 187\nDispatcher::dispatch() - CORE/Cake/Routing/Dispatcher.php, line 162\n[main] - APP/webroot/index.php, line 110</pre></div></pre></p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong><pre class=\"cake-error\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5887cf8-trace\').style.display = (document.getElementById(\'cakeErr52195e5887cf8-trace\').style.display == \'none\' ? \'\' : \'none\');\"><b>Notice</b> (8)</a>: Undefined index: User [<b>APP/View/Elements/emails/new_huddle.ctp</b>, line <b>16</b>]<div id=\"cakeErr52195e5887cf8-trace\" class=\"cake-stack-trace\" style=\"display: none;\"><a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5887cf8-code\').style.display = (document.getElementById(\'cakeErr52195e5887cf8-code\').style.display == \'none\' ? \'\' : \'none\')\">Code</a> <a href=\"javascript:void(0);\" onclick=\"document.getElementById(\'cakeErr52195e5887cf8-context\').style.display = (document.getElementById(\'cakeErr52195e5887cf8-context\').style.display == \'none\' ? \'\' : \'none\')\">Context</a><pre id=\"cakeErr52195e5887cf8-code\" class=\"cake-code-dump\" style=\"display: none;\"><code><span style=\"color: #000000\"><span style=\"color: #0000BB\">&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"color: #007700\">&lt;</span><span style=\"color: #0000BB\">p</span><span style=\"color: #007700\">&gt;</span><span style=\"color: #0000BB\">Still&nbsp;have&nbsp;questions</span><span style=\"color: #007700\">?&lt;/</span><span style=\"color: #0000BB\">p</span><span style=\"color: #007700\">&gt;</span></span></code>\n<code><span style=\"color: #000000\"><span style=\"color: #0000BB\"></span></span></code>\n<span class=\"code-highlight\"><code><span style=\"color: #000000\">&nbsp;&nbsp;&nbsp;&nbsp;&lt;p&gt;Contact&nbsp;&lt;strong&gt;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'first_name\'</span><span style=\"color: #007700\">]&nbsp;.&nbsp;</span><span style=\"color: #DD0000\">\"&nbsp;\"&nbsp;</span><span style=\"color: #007700\">.&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'last_name\'</span><span style=\"color: #007700\">]&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>&lt;/strong&gt;&nbsp;at&nbsp;&lt;a&nbsp;href=\"mailto:<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'email\'</span><span style=\"color: #007700\">];&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>\"&gt;<span style=\"color: #0000BB\">&lt;?php&nbsp;</span><span style=\"color: #007700\">echo&nbsp;</span><span style=\"color: #0000BB\">$data</span><span style=\"color: #007700\">[</span><span style=\"color: #DD0000\">\'User\'</span><span style=\"color: #007700\">][</span><span style=\"color: #DD0000\">\'email\'</span><span style=\"color: #007700\">];&nbsp;</span><span style=\"color: #0000BB\">?&gt;</span>&lt;/a&gt;&lt;/p&gt;</span></code></span></pre><pre id=\"cakeErr52195e5887cf8-context\" class=\"cake-context\" style=\"display: none;\">$viewFile = &#039;/Library/WebServer/Documents/sibme/app/app/View/Elements/emails/new_huddle.ctp&#039;\n$dataForView = array(\n	&#039;user_id&#039; =&gt; &#039;1&#039;,\n	&#039;users_groups&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;Group&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;super_users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;users&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;users_accounts&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;roles&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	),\n	&#039;title_for_layout&#039; =&gt; &#039;Sibme&#039;,\n	&#039;data&#039; =&gt; array(\n		(int) 0 =&gt; array(\n			&#039;AccountFolder&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;User&#039; =&gt; array(\n				[maximum depth reached]\n			),\n			&#039;account_folders_meta_data&#039; =&gt; array(\n				[maximum depth reached]\n			)\n		)\n	)\n)\n$user_id = &#039;1&#039;\n$users_groups = array(\n	(int) 0 =&gt; array(\n		&#039;Group&#039; =&gt; array(\n			&#039;id&#039; =&gt; &#039;1&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;name&#039; =&gt; &#039;Math Group&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-12-12 00:00:00&#039;\n		)\n	)\n)\n$super_users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;3&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem2&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@makeityourweb.com&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Miyw&#039;,\n			&#039;title&#039; =&gt; &#039;Mr&#039;,\n			&#039;phone&#039; =&gt; &#039;123-123-1234&#039;,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; null,\n			&#039;file_size&#039; =&gt; null,\n			&#039;authentication_token&#039; =&gt; null,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;0&#039;,\n			&#039;created_date&#039; =&gt; null,\n			&#039;last_edit_by&#039; =&gt; &#039;0&#039;,\n			&#039;last_edit_date&#039; =&gt; null\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;3&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;Super User&#039;\n		)\n	)\n)\n$users = array(\n	(int) 0 =&gt; array(\n		&#039;User&#039; =&gt; array(\n			&#039;password&#039; =&gt; &#039;*****&#039;,\n			&#039;id&#039; =&gt; &#039;814&#039;,\n			&#039;username&#039; =&gt; &#039;ksaleem3&#039;,\n			&#039;email&#039; =&gt; &#039;khurram@jjtestsite.us&#039;,\n			&#039;reset_password_token&#039; =&gt; null,\n			&#039;reset_password_sent_at&#039; =&gt; null,\n			&#039;remember_created_at&#039; =&gt; null,\n			&#039;sign_in_count&#039; =&gt; &#039;0&#039;,\n			&#039;current_sign_in_at&#039; =&gt; null,\n			&#039;last_sign_in_at&#039; =&gt; null,\n			&#039;current_sign_in_ip&#039; =&gt; null,\n			&#039;last_sign_in_ip&#039; =&gt; null,\n			&#039;first_name&#039; =&gt; &#039;Khurram&#039;,\n			&#039;last_name&#039; =&gt; &#039;Saleem&#039;,\n			&#039;title&#039; =&gt; null,\n			&#039;phone&#039; =&gt; null,\n			&#039;time_zone&#039; =&gt; null,\n			&#039;image&#039; =&gt; &#039;srch_img.jpg&#039;,\n			&#039;file_size&#039; =&gt; &#039;41281&#039;,\n			&#039;authentication_token&#039; =&gt; &#039;os5fvbptg6al7xt1vzga&#039;,\n			&#039;email_notification&#039; =&gt; true,\n			&#039;is_active&#039; =&gt; true,\n			&#039;type&#039; =&gt; &#039;Active&#039;,\n			&#039;created_by&#039; =&gt; &#039;1&#039;,\n			&#039;created_date&#039; =&gt; &#039;2013-08-23 06:34:03&#039;,\n			&#039;last_edit_by&#039; =&gt; &#039;1&#039;,\n			&#039;last_edit_date&#039; =&gt; &#039;2013-08-23 06:33:31&#039;\n		),\n		&#039;accounts&#039; =&gt; array(\n			&#039;company_name&#039; =&gt; &#039;Sibme&#039;\n		),\n		&#039;users_accounts&#039; =&gt; array(\n			&#039;user_id&#039; =&gt; &#039;814&#039;\n		),\n		&#039;roles&#039; =&gt; array(\n			&#039;name&#039; =&gt; &#039;User&#039;\n		)\n	)\n)\n$title_for_layout = &#039;Sibme&#039;\n$data = array(\n	(int) 0 =&gt; array(\n		&#039;AccountFolder&#039; =&gt; array(\n			&#039;account_folder_id&#039; =&gt; &#039;6&#039;,\n			&#039;account_id&#039; =&gt; &#039;1&#039;,\n			&#039;folder_type&#039; =&gt; &#039;1&#039;,\n			&#039;parent_folder_id&#039; =&gt; null,\n			&#039;name&#039; =&gt; &#039;Mu Huddle&#039;,\n			&#039;desc&#039; =&gt; &#039;Huddle Desc&#039;,\n			&#039;created_by&#039; =&gt; &#039',b'1',NULL,'2013-08-24 20:31:04',NULL),
	(30,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Khurram Saleem created a new Sibme Coaching Huddle: Mu Huddle','<div id=\":6r\" style=\"overflow: hidden;\">\n    <p>Khurram Saleem invited you to a Coaching Huddle::</p>\n    <br/>\n    <h2 style=\"color:#3d9bc2\">Mu Huddle</h2>\n    <p>Sibme is an awesome tool we use to gather and deliver feedback to improve teaching and learning.</p><br/>\n    <a href=\"http://localhost/sibme/app/\">Click here to access coaching huddle </a>\n    <br/>\n\n    <p>Read Khurram Saleem\'s message below:</p>\n    <p>Hello World!!!</p>\n    <p>Still have questions?</p>\n\n    <p>Contact <strong>Khurram Saleem</strong> at <a href=\"mailto:khurri.saleem@gmail.com\">khurri.saleem@gmail.com</a></p>\n    \n    <div class=\"yj6qo\"></div>\n    <div class=\"adL\">\n    </div>\n\n</div>',b'1',NULL,'2013-08-24 20:32:45',1),
	(31,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:41:20',1),
	(32,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:41:24',1),
	(33,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:41:27',1),
	(34,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:41:30',1),
	(35,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:41:57',1),
	(36,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:42:00',1),
	(37,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:42:03',1),
	(38,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:42:06',1),
	(39,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:42:50',1),
	(40,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:42:53',1),
	(41,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:42:56',1),
	(42,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:43:00',1),
	(43,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:43:24',1),
	(44,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:43:27',1),
	(45,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:44:27',1),
	(46,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Hello World',b'1',NULL,'2013-08-29 04:44:31',1),
	(47,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Hello',b'1',NULL,'2013-08-29 05:20:11',1),
	(48,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','My Reply to This.',b'1',NULL,'2013-08-29 05:24:05',1),
	(49,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','My Reply to This.',b'1',NULL,'2013-08-29 05:24:09',1),
	(50,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Here you go',b'1',NULL,'2013-08-29 05:41:49',1),
	(51,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','Testing..',b'1',NULL,'2013-08-29 05:48:59',1),
	(52,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','My Testing 2',b'1',NULL,'2013-08-29 05:50:43',1),
	(53,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','My Testing 2',b'1',NULL,'2013-08-29 05:50:48',1),
	(54,'do-not-reply@sibme.com','khurram@makeityourweb.com',NULL,NULL,'Huddle Discussions','My Testing 2',b'1',NULL,'2013-08-29 05:51:58',1),
	(55,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','My Testing 2',b'1',NULL,'2013-08-29 05:52:02',1),
	(56,'do-not-reply@sibme.com','khurri.saleem@gmail.com',NULL,NULL,'Huddle Discussions','My Testing 2',b'1',NULL,'2013-08-29 05:52:27',1),
	(57,'do-not-reply@sibme.com','khurram@jjtestsite.us',NULL,NULL,'Huddle Discussions','My Testing 2',b'1',NULL,'2013-08-29 05:52:31',1);

/*!40000 ALTER TABLE `audit_emails` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comment_attachments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comment_attachments`;

CREATE TABLE `comment_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `comment_attachments` WRITE;
/*!40000 ALTER TABLE `comment_attachments` DISABLE KEYS */;

INSERT INTO `comment_attachments` (`id`, `comment_id`, `document_id`)
VALUES
	(1,15,48),
	(2,17,49),
	(3,18,50),
	(4,19,51),
	(5,20,52),
	(6,21,53),
	(7,22,54),
	(8,23,55),
	(9,27,56),
	(10,29,57),
	(11,30,58),
	(12,31,59),
	(13,32,60),
	(14,33,61),
	(15,34,62);

/*!40000 ALTER TABLE `comment_attachments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comment_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comment_users`;

CREATE TABLE `comment_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_id` int(11) NOT NULL,
  `user_id` varchar(45) DEFAULT NULL,
  `comment_user_type` tinyint(4) DEFAULT NULL COMMENT '1=notification,2=recipient',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `comment_users` WRITE;
/*!40000 ALTER TABLE `comment_users` DISABLE KEYS */;

INSERT INTO `comment_users` (`id`, `comment_id`, `user_id`, `comment_user_type`)
VALUES
	(83,26,'814',1),
	(84,27,'3',1),
	(85,27,'814',1),
	(86,29,'814',1),
	(87,31,'814',1),
	(88,32,'3',1),
	(89,32,'814',1),
	(90,33,'3',1),
	(91,33,'814',1),
	(92,34,'3',1),
	(93,34,'814',1);

/*!40000 ALTER TABLE `comment_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comments`;

CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_comment_id` int(11) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `ref_type` tinyint(4) NOT NULL COMMENT '1=huddle(folder),2=document,3=comment',
  `ref_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `time` int(11) DEFAULT NULL,
  `restrict_to_users` bit(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_comments_on_commentable_id` (`ref_id`),
  KEY `index_comments_on_user_id` (`user_id`),
  CONSTRAINT `fk_comments_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;

INSERT INTO `comments` (`id`, `parent_comment_id`, `title`, `comment`, `ref_type`, `ref_id`, `user_id`, `time`, `restrict_to_users`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(2,NULL,NULL,'Testing',2,27,1,2,b'0',1,'2013-08-25 14:53:02',1,'2013-08-25 14:53:02');

/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table document_meta_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `document_meta_data`;

CREATE TABLE `document_meta_data` (
  `document_meta_data_id` int(11) NOT NULL,
  `meta_data_name` varchar(64) NOT NULL COMMENT 'tag, topic, subject, etc',
  `meta_data_value` varchar(255) NOT NULL,
  PRIMARY KEY (`document_meta_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table documents
# ------------------------------------------------------------

DROP TABLE IF EXISTS `documents`;

CREATE TABLE `documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `doc_type` tinyint(3) NOT NULL COMMENT '1=video, 2=document',
  `url` varchar(4000) DEFAULT NULL,
  `original_file_name` varchar(1024) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `view_count` int(11) NOT NULL DEFAULT '0',
  `content_type` varchar(128) DEFAULT NULL,
  `published` int(11) NOT NULL DEFAULT '0',
  `job_id` int(11) DEFAULT NULL,
  `zencoder_output_id` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL DEFAULT b'0',
  `created_date` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;

INSERT INTO `documents` (`id`, `account_id`, `doc_type`, `url`, `original_file_name`, `file_size`, `view_count`, `content_type`, `published`, `job_id`, `zencoder_output_id`, `active`, `created_date`, `created_by`, `last_edit_date`, `last_edit_by`)
VALUES
	(27,1,1,'1/7/2013/08/25/kupload.mov','kupload.mov',623275,48,NULL,1,NULL,'56706763',b'1','2013-08-25 08:53:24',1,'2013-08-25 08:53:24',1),
	(28,1,2,'1/7/2013/08/25/logo-1.jpg','logo-1.jpg',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-25 04:39:00',1,'2013-08-25 04:39:00',1),
	(29,1,2,'1/7/2013/08/25/logo-coach.png','logo-coach.png',22952,0,NULL,1,NULL,NULL,b'1','2013-08-25 04:40:39',1,'2013-08-25 04:40:39',1),
	(32,1,2,'1/7/2013/08/26/sibmeqasites.xml','sibmeqa_sites.xml',785,0,NULL,1,NULL,NULL,b'1','2013-08-26 05:17:41',1,'2013-08-26 05:17:41',1),
	(33,1,1,'1/12/2013/08/28/kupload.mov','kupload.mov',623275,0,NULL,1,NULL,'56989570',b'1','2013-08-28 06:12:29',1,'2013-08-28 06:12:29',1),
	(34,1,1,NULL,'kupload.mov',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-28 03:28:36',1,'2013-08-28 03:28:36',1),
	(35,1,1,'tempupload/1/2013/08/28/yRDWuPCaQ2eKBbPCrwqX_kupload.mov','kupload.mov',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-28 03:31:24',1,'2013-08-28 03:31:24',1),
	(36,1,1,'tempupload/1/2013/08/28/7MtAGuRnQK1dtPN1ItAB_kupload.mov','kupload.mov',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-28 03:33:50',1,'2013-08-28 03:33:50',1),
	(37,1,1,'tempupload/1/2013/08/28/LCZZVJpyRAalTa5SVgM3_kupload.mov','kupload.mov',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-28 03:37:41',1,'2013-08-28 03:37:41',1),
	(38,1,1,'1/17/2013/08/28/HDzqUThT3eWLLsuz2W87_kupload.mov','kupload.mov',623275,0,NULL,1,NULL,'57036379',b'1','2013-08-28 03:40:28',1,'2013-08-28 03:40:28',1),
	(39,1,1,'1/18/2013/08/28/bZ8rYETFSPqeFPNSgs9v_kupload.mov','kupload.mov',623275,0,NULL,1,NULL,'57041641',b'1','2013-08-28 04:29:37',1,'2013-08-28 04:29:37',1),
	(40,1,1,'1/7/2013/08/28/Wi5UVDKjSf2bRHC0Kfpz_kupload.mov','kupload.mov',623275,4,NULL,1,NULL,'57047611',b'1','2013-08-28 05:35:29',1,'2013-08-28 05:35:29',1),
	(41,1,1,'1/7/2013/08/28/Wx3ei1oDQMqzz9A4GXDS_kupload.mov','kupload.mov',623275,0,NULL,1,NULL,'57048801',b'1','2013-08-28 05:50:03',1,'2013-08-28 05:50:03',1),
	(42,1,2,'1/7/2013/08/28/tempupload/1/2013/08/28/P7kAyinQVirjlJRAYs1w_100.txt','tempupload/1/2013/08/28/P7kAyinQVirjlJRAYs1w_100.txt',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-28 06:00:42',1,'2013-08-28 06:00:42',1),
	(43,1,2,'1/7/2013/08/28/nrs2olaIQpCb2KYXzcCy_100.txt','tempupload/1/2013/08/28/nrs2olaIQpCb2KYXzcCy_100.txt',53536,0,NULL,1,NULL,NULL,b'1','2013-08-28 06:05:37',1,'2013-08-28 06:05:37',1),
	(44,1,1,'1/7/2013/08/28/bOH76HyOSiq3TShf6NFE_100.txt','bOH76HyOSiq3TShf6NFE_100.txt',53536,0,NULL,1,NULL,'57050432',b'1','2013-08-28 06:09:33',1,'2013-08-28 06:09:33',1),
	(45,1,1,'1/7/2013/08/28/qw7lwbLlTwKHDCDnfBNc_kupload.mov','kupload.mov',623275,1,NULL,1,NULL,'57050566',b'1','2013-08-28 06:11:19',1,'2013-08-28 06:11:19',1),
	(46,1,2,'1/7/2013/08/28/St1t0KGWQyO0UCtbTPbg_100.txt','100.txt',53536,0,NULL,1,NULL,NULL,b'1','2013-08-28 06:11:54',1,'2013-08-28 06:11:54',1),
	(47,1,1,'1/7/2013/08/29/account-.png','account-.png',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 04:23:40',1,'2013-08-29 04:23:40',1),
	(48,1,1,'1/7/2013/08/29/astrosites.xml','astrosites.xml',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 04:28:43',1,'2013-08-29 04:28:43',1),
	(49,1,1,'1/7/2013/08/29/image002.jpg','image002.jpg',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 04:31:59',1,'2013-08-29 04:31:59',1),
	(50,1,1,'1/7/2013/08/29/frf-utah-dns.png','frf-utah-dns.png',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 04:41:20',1,'2013-08-29 04:41:20',1),
	(51,1,1,'1/7/2013/08/29/frf-utah-dns.png','frf-utah-dns.png',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 04:41:57',1,'2013-08-29 04:41:57',1),
	(52,1,1,'1/7/2013/08/29/frf-utah-dns.png','frf-utah-dns.png',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 04:42:50',1,'2013-08-29 04:42:50',1),
	(53,1,1,'1/7/2013/08/29/frf-utah-dns.png','frf-utah-dns.png',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 04:43:24',1,'2013-08-29 04:43:24',1),
	(54,1,1,'1/7/2013/08/29/frf-utah-dns.png','frf-utah-dns.png',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 04:44:27',1,'2013-08-29 04:44:27',1),
	(55,1,1,'1/7/2013/08/29/frf-utah-dns.png','frf-utah-dns.png',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 04:44:50',1,'2013-08-29 04:44:50',1),
	(56,1,1,'1/7/2013/08/29/image002.jpg','image002.jpg',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 05:24:05',1,'2013-08-29 05:24:05',1),
	(57,1,1,'1/7/2013/08/29/image002.jpg','image002.jpg',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 05:41:49',1,'2013-08-29 05:41:49',1),
	(58,1,1,'1/7/2013/08/29/image003.jpg','image003.jpg',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 05:45:13',1,'2013-08-29 05:45:13',1),
	(59,1,1,'1/7/2013/08/29/frf-utah-dns.png','frf-utah-dns.png',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 05:48:59',1,'2013-08-29 05:48:59',1),
	(60,1,1,'1/7/2013/08/29/donate-image.png','donate-image.png',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 05:50:43',1,'2013-08-29 05:50:43',1),
	(61,1,1,'1/7/2013/08/29/donate-image.png','donate-image.png',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 05:51:58',1,'2013-08-29 05:51:58',1),
	(62,1,1,'1/7/2013/08/29/donate-image.png','donate-image.png',NULL,0,NULL,1,NULL,NULL,b'1','2013-08-29 05:52:27',1,'2013-08-29 05:52:27',1);

/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_groups_on_account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;

INSERT INTO `groups` (`id`, `account_id`, `name`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(1,1,'Math Group',1,'2013-12-12 00:00:00',1,'2013-12-12 00:00:00');

/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table plans
# ------------------------------------------------------------

DROP TABLE IF EXISTS `plans`;

CREATE TABLE `plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_type` varchar(255) DEFAULT NULL,
  `storage` bigint(20) DEFAULT NULL,
  `validity` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `users` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `per_year` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `plans` WRITE;
/*!40000 ALTER TABLE `plans` DISABLE KEYS */;

INSERT INTO `plans` (`id`, `plan_type`, `storage`, `validity`, `created_at`, `updated_at`, `users`, `price`, `per_year`)
VALUES
	(1,'Department',5,'60 Days','2012-10-08 11:57:52','2013-02-28 12:37:38',10,54,0),
	(2,'School',20,'89 Days','2012-10-08 11:58:28','2013-02-28 12:39:23',40,119,0),
	(3,'Large School',50,'895 Days','2012-10-22 11:57:23','2013-02-28 12:41:20',150,239,0),
	(4,'Institution',100,NULL,'2012-11-08 11:57:45','2013-02-28 12:42:40',500,399,0),
	(5,'Department',5,NULL,'2013-03-01 10:41:29','2013-03-01 10:41:29',10,588,1),
	(6,'School',20,NULL,'2013-03-01 10:41:29','2013-03-01 10:41:29',40,1308,1),
	(7,'Large School',50,NULL,'2013-03-01 10:41:29','2013-03-01 10:41:29',150,2628,1),
	(8,'Institution',100,NULL,'2013-03-01 10:41:29','2013-03-01 10:41:29',500,4368,1);

/*!40000 ALTER TABLE `plans` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_type` tinyint(4) NOT NULL COMMENT '1=account, 2=huddle',
  `name` varchar(64) NOT NULL,
  `desc` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;

INSERT INTO `roles` (`role_id`, `role_type`, `name`, `desc`)
VALUES
	(100,1,'Account Owner',NULL),
	(110,1,'Super User',NULL),
	(120,1,'User',NULL),
	(200,2,'Admin',NULL),
	(210,2,'User',NULL),
	(220,2,'Viewer',NULL);

/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table subjects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `subjects`;

CREATE TABLE `subjects` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `subjects` WRITE;
/*!40000 ALTER TABLE `subjects` DISABLE KEYS */;

INSERT INTO `subjects` (`id`, `name`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`, `account_id`)
VALUES
	(1,'kk',NULL,'2013-08-28 03:55:18',NULL,'2013-08-28 03:55:18',NULL),
	(2,'Test',1,'2013-08-28 04:22:06',1,'2013-08-28 04:22:06',1),
	(3,'Math',1,'2013-08-28 05:21:55',1,'2013-08-28 05:21:55',1),
	(4,'Chemistry',1,'2013-08-28 05:29:41',1,'2013-08-28 05:29:41',1);

/*!40000 ALTER TABLE `subjects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table topics
# ------------------------------------------------------------

DROP TABLE IF EXISTS `topics`;

CREATE TABLE `topics` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  `account_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `topics` WRITE;
/*!40000 ALTER TABLE `topics` DISABLE KEYS */;

INSERT INTO `topics` (`id`, `name`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`, `account_id`)
VALUES
	(1,'First Topic',1,'2013-08-28 05:33:01',1,'2013-08-28 05:33:01',1);

/*!40000 ALTER TABLE `topics` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_activity_logs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_activity_logs`;

CREATE TABLE `user_activity_logs` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `desc` text NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `account_folder_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table user_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_groups`;

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `index_user_groups_on_group_id` (`group_id`),
  KEY `index_user_groups_on_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;

INSERT INTO `user_groups` (`id`, `group_id`, `user_id`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(1,1,1,1,'2013-12-12 00:00:00',1,'2013-12-12 00:00:00'),
	(2,1,3,1,'2013-12-12 00:00:00',1,'2013-12-12 00:00:00');

/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) DEFAULT NULL,
  `last_sign_in_ip` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `time_zone` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `file_size` int(11) DEFAULT NULL,
  `authentication_token` varchar(255) DEFAULT NULL,
  `email_notification` tinyint(1) DEFAULT '1',
  `is_active` tinyint(1) DEFAULT '1',
  `type` enum('Invite_Sent','Active') NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `username`, `email`, `password`, `reset_password_token`, `reset_password_sent_at`, `remember_created_at`, `sign_in_count`, `current_sign_in_at`, `last_sign_in_at`, `current_sign_in_ip`, `last_sign_in_ip`, `first_name`, `last_name`, `title`, `phone`, `time_zone`, `image`, `file_size`, `authentication_token`, `email_notification`, `is_active`, `type`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(1,'ksaleem','khurri.saleem@gmail.com','5662a293b6f5a24f9d4b2ae75a635cdc111eac71',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'Khurram','Saleem','Mr','123-123-1234',NULL,NULL,NULL,NULL,1,1,'Active',0,NULL,0,NULL),
	(3,'ksaleem2','khurri.saleem@gmail.com','5662a293b6f5a24f9d4b2ae75a635cdc111eac71',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'Khurram','Miyw','Mr','123-123-1234',NULL,NULL,NULL,NULL,1,1,'Active',0,NULL,0,NULL),
	(814,'ksaleem3','khurram@jjtestsite.us','5662a293b6f5a24f9d4b2ae75a635cdc111eac71',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,'Khurram','Saleem',NULL,NULL,NULL,'srch_img.jpg',41281,'os5fvbptg6al7xt1vzga',1,1,'Active',1,'2013-08-23 06:34:03',1,'2013-08-23 06:33:31');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users_accounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users_accounts`;

CREATE TABLE `users_accounts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_default` bit(1) NOT NULL DEFAULT b'0',
  `permission_maintain_folders` bit(1) NOT NULL,
  `permission_access_video_library` bit(1) NOT NULL,
  `permission_video_library_upload` bit(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `account_id` (`account_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users_accounts` WRITE;
/*!40000 ALTER TABLE `users_accounts` DISABLE KEYS */;

INSERT INTO `users_accounts` (`id`, `account_id`, `user_id`, `role_id`, `is_default`, `permission_maintain_folders`, `permission_access_video_library`, `permission_video_library_upload`, `created_by`, `created_date`, `last_edit_by`, `last_edit_date`)
VALUES
	(1,1,1,100,b'1',b'1',b'1',b'1',1,'2013-09-20 00:00:00',1,'2013-09-20 00:00:00'),
	(2,2,1,110,b'1',b'1',b'1',b'1',1,'2013-09-20 00:00:00',1,'2013-09-20 00:00:00'),
	(3,1,3,110,b'1',b'1',b'1',b'1',1,'2013-09-20 00:00:00',1,'2013-09-20 00:00:00'),
	(35,1,814,120,b'1',b'0',b'0',b'0',1,'2013-08-23 18:34:47',1,'2013-08-23 06:34:47');

/*!40000 ALTER TABLE `users_accounts` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
