CREATE TABLE `account_folders_meta_data` (
  `account_folder_meta_data_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) NOT NULL,
  `meta_data_name` varchar(64) NOT NULL COMMENT 'tag, topic, subject, etc',
  `meta_data_value` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_edit_by` int(11) NOT NULL,
  `last_edit_date` datetime NOT NULL,
  PRIMARY KEY (`account_folder_meta_data_id`),
  UNIQUE KEY `account_folders_meta_data_id_UNIQUE` (`account_folder_meta_data_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;