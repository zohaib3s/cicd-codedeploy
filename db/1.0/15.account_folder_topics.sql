CREATE TABLE `account_folder_topics` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_folder_id` int(11) DEFAULT NULL,
  `topic_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_edit_by` int(11) DEFAULT NULL,
  `last_edit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;