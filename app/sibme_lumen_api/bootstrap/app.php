<?php

require_once __DIR__.'/../vendor/autoload.php';

try {
    (new Dotenv\Dotenv(__DIR__.'/../'))->load();
} catch (Dotenv\Exception\InvalidPathException $e) {
    //
}

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
    realpath(__DIR__.'/../')
);

$app->withFacades(true, [
    'Maatwebsite\Excel\Facades\Excel' => 'Excel',
    '\Illuminate\Contracts\Mail\Mailer' => 'mailer',
    'Illuminate\Support\Facades\Mail' => 'Mail'
]);

$app->configure('view');
$app->withEloquent();

date_default_timezone_set('America/Chicago');

// Setup configuration parameters.
//
//config([
//    "filesystems" => [
//        'default' => 'local',
//        'disks' => [
//            'local' => [
//                'driver' => 'local',
//                'root' => storage_path('app'),
//            ],
//        ],
//    ],
//]);

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    App\Console\Kernel::class
);

$app->singleton('filesystem', function ($app) {
    return $app->loadComponent('filesystems', 'Illuminate\Filesystem\FilesystemServiceProvider', 'filesystem');
});
/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

// $app->middleware([
//    App\Http\Middleware\ExampleMiddleware::class
// ]);

// $app->routeMiddleware([
//     'auth' => App\Http\Middleware\Authenticate::class,
// ]);

//$app->routeMiddleware([
//    // ...
//    'cors' => \Barryvdh\Cors\HandleCors::class,
//]);


$app->middleware([
    'authToken' => App\Http\Middleware\CorsMiddleware::class,
    'libraryAccountCheck' => App\Http\Middleware\LibraryAccountMiddleware::class,
]);

// $app->middleware([
//     Clockwork\Support\Lumen\ClockworkMiddleware::class
// ]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

$app->register(App\Providers\AppServiceProvider::class);
$app->register(App\Providers\ConfigMailerServiceProvider::class);
// $app->register(App\Providers\AuthServiceProvider::class);
// $app->register(App\Providers\EventServiceProvider::class);

$app->register(Illuminate\Mail\MailServiceProvider::class);
$app->register(\Barryvdh\DomPDF\ServiceProvider::class);
$app->register(Maatwebsite\Excel\ExcelServiceProvider::class);
// $app->register(Clockwork\Support\Lumen\ClockworkServiceProvider::class);
$app->register(tibonilab\Pdf\PdfServiceProvider::class);
$app->register(Illuminate\Redis\RedisServiceProvider::class);
$app->register(BaoPham\DynamoDb\DynamoDbServiceProvider::class);
$app->register(Aacotroneo\Saml2\Saml2ServiceProvider::class);
/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->router->group([
    'namespace' => 'App\Http\Controllers',
], function ($router) {
    require __DIR__.'/../routes/web.php';
});

/*
|------------------------
| Custom Configured Files
|------------------------
*/
$app->configure('general');
$app->configure('core');
$app->configure('mail');
$app->configure('s3');
$app->configure('excel');
$app->configure('apikeys');
$app->configure('hubspot');
$app->configure('dynamodb');
//SSO
$app->configure('mytestidp1_idp_settings');
$app->configure('saml2_settings');

return $app;
