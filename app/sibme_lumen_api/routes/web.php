<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

//Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
//  var_dump($query->sql); // Dumps sql
//  var_dump($query->bindings); //Dumps data passed to query
//  var_dump($query->time); //Dumps time sql took to be processed
//});

$router->get('/', function () use ($router) {
    return $router->app->version();
});

//$router->get('/data', ['middleware' => 'cors',  function () use ($router) {
//   // $router->post('/account_overview','ApiController@account_overview');
//}]);
//update_general_account_settings
//
//$router->group(['middleware' => 'CorsMiddleware'], function () use ($router) {
//       $router->get('/test123','ApiController@test');
//
//        $router->post('/account_overview','ApiController@account_overview');
//
//});

//$router->group(['middleware' => ['authToken']], function () use ($router) {
//$router->get('/test123','ApiController@test');
//$router->post('/account_overview','ApiController@account_overview');
//
//});

$router->post('/analytics_new', 'ApiController@analytics_new', ['middleware' => 'authToken']);
// $router->get('/update_user_type', 'ApiController@update_user_type', ['middleware' => 'authToken']);
$router->post('/migrate_livestream_analytics', 'ApiController@migrate_livestream_analytics', ['middleware' => 'authToken']);

$router->post('/account_overview', 'ApiController@account_overview', ['middleware' => 'authToken', function () {

}]);

$router->post('/user_summary', 'ApiController@accountUsersAjax', ['middleware' => 'authToken', function () {

}]);
$router->post('/get_user_summary', 'ApiController@getUserSummary', ['middleware' => 'authToken', function () {

}]);

$router->post('/account_overview_by_user', 'ApiController@account_overview_by_user', ['middleware' => 'authToken', function () {

}]);
$router->post('/get_analytics_stats', 'ApiController@get_analytics_stats', ['middleware' => 'authToken', function () {

}]);
$router->post('/get_banners', 'ApiController@get_banners', ['middleware' => 'authToken', function () {

}]);
$router->post('/hid_banner', 'ApiController@hid_banner', ['middleware' => 'authToken', function () {

}]);

$router->post('/performance_level_assessment_array', 'ApiController@performance_level_assessment_array', ['middleware' => 'authToken']);

$router->post('/get_child_accounts', 'ApiController@get_child_accounts', ['middleware' => 'authToken', function () {

}]);
// $router->post('/frequency_of_tagged_standard_2', 'ApiController@frequency_of_tagged_standard_2', ['middleware' => 'authToken', function () {

// }]);
$router->post('/frequency_of_tagged_standard_2', 'ApiController@frequency_of_tagged_standard_user', ['middleware' => 'authToken', function () {

}]);
$router->post('/export_user_summery', 'ApiController@export_user_summery', ['middleware' => 'authToken', function () {

}]);
$router->post('/get_all_users', 'ApiController@get_all_users', ['middleware' => 'authToken', function () {

}]);

$router->post('/create_huddle', 'ApiController@create_huddle', ['middleware' => 'authToken', function () {

}]);

$router->get('/get_frameworks_list/{id}', 'ApiController@get_frameworks_list', ['middleware' => 'authToken', function () {

}]);

/*
Following are 2 apis with same function because we needed to apply some permissions for edit page which were not needed in view huddle page.
*/
$router->post('/edit_huddle_start', 'ApiController@edit_huddle_start', ['middleware' => 'authToken', function () {}]);
$router->post('/view_huddle', 'ApiController@edit_huddle_start', ['middleware' => 'authToken', function () {}]);
$router->post('/get_account_id', 'ApiController@get_account_id', ['middleware' => 'authToken', function () {}]);
$router->post('/get_assessees_list', 'ApiController@get_assessees_list', ['middleware' => 'authToken', function () {}]);

$router->post('/delete_assessee', 'ApiController@delete_assessee', ['middleware' => 'authToken', function () {

}]);

$router->post('/edit_huddle', 'ApiController@edit_huddle', ['middleware' => 'authToken', function () {

}]);

$router->post('/get_group_details', 'ApiController@get_group_details', ['middleware' => 'authToken', function () {

}]);

$router->post('/check_huddle_create_persmission', 'ApiController@check_huddle_create_persmission', ['middleware' => 'authToken', function () {

}]);

$router->get('/getVideoComments', 'VideoController@get_video_comments_with_replies', ['middleware' => 'authToken', function () {

}]);
$router->post('/getSingleComment', 'VideoController@getSingleComment', ['middleware' => 'authToken', function () {

}]);
$router->get('/getSubtitlesFromDb/{id}', 'VideoController@getSubtitlesFromDb', ['middleware' => 'authToken', function () {

}]);
$router->post('/updateSubtitles', 'VideoController@updateSubtitles', ['middleware' => 'authToken', function () {

}]);
$router->post('/getSingleDiscussion', 'HuddleController@getSingleDiscussion', ['middleware' => 'authToken', function () {

}]);

$router->post('/view_page', 'VideoController@view_page', ['middleware' => 'authToken', function () {

}]);

$router->post('/workspace_video_page', 'VideoController@workspace_video_page', ['middleware' => 'authToken', function () {

}]);

$router->post('/download_document', 'VideoController@downloadDocument', ['middleware' => 'authToken']);

$router->post('/download_discussion_document', 'DiscussionController@downloadDiscussionDocument', ['middleware' => 'authToken']);

$router->post('/delete_video_document', 'VideoController@deleteVideoDocument', ['middleware' => 'authToken']);

$router->post('/get_video_documents', 'VideoController@getVideoDocuments', ['middleware' => 'authToken']);

$router->post('/print_pdf_comments', 'VideoController@print_pdf_comments', ['middleware' => 'authToken']);

$router->post('/print_excel_comments', 'VideoController@print_excel_comments', ['middleware' => 'authToken']);

$router->post('/print_pdf_transcriptions', 'VideoController@print_pdf_transcriptions', ['middleware' => 'authToken']);
$router->post('/print_excel_transcriptions', 'VideoController@print_excel_transcriptions', ['middleware' => 'authToken']);

$router->post('/sent_feedback_email', 'VideoController@sent_feedback_email', ['middleware' => 'authToken']);

$router->post('/mobile_video', 'VideoController@mobile_video', ['middleware' => 'authToken']);

$router->post('/publish_observation', 'VideoController@publish_observation', ['middleware' => 'authToken']);

$router->get('/test_waqas', 'ExampleController@testWaqas', ['middleware' => 'authToken']);

$router->post('/add_comment', 'VideoController@addComment', ['middleware' => 'authToken', function () {

}]);

$router->post('/get_document_comments', 'VideoController@get_document_comments', ['middleware' => 'authToken', function () {

}]);

$router->post('/add_document_comment', 'VideoController@addDocumentComment', ['middleware' => 'authToken', function () {

}]);

$router->get('/getSecureAmazonCloudFrontUrl', 'VideoController@getSecureAmazonCloudFrontUrl', ['middleware' => 'authToken', function () {

}]);

$router->post('/get_framework_settings', 'VideoController@get_framework_settings', ['middleware' => 'authToken', function () {

}]);

$router->post('/get_comments_count', 'VideoController@get_comments_count', ['middleware' => 'authToken', function () {

}]);

$router->post('/performace_level_update', 'VideoController@performace_level_update', ['middleware' => 'authToken', function () {

}]);

$router->post('/add_multiple_standard_ratings', 'VideoController@add_multiple_standard_ratings', ['middleware' => 'authToken', function () {

}]);

$router->post('/load_perfomance_level_comments', 'VideoController@load_perfomance_level_comments', ['middleware' => 'authToken', function () {

}]);

$router->post('/edit_comment', 'VideoController@editComment', ['middleware' => ['authToken', 'libraryAccountCheck'], function () {

}]);

$router->post('/feedback_publish', 'VideoController@feedback_publish', ['middleware' => 'authToken', function () {

}]);

$router->post('/delete_comment', 'VideoController@deleteVideoComments', ['middleware' => 'authToken', function () {

}]);

$router->post('/upload_document', 'VideoController@uploadDocuments', ['middleware' => ['authToken', 'libraryAccountCheck'], function () {

}]);

$router->post('/add_reply', 'VideoController@addReply', ['middleware' => 'authToken', function () {

}]);

$router->post('/add_document_reply', 'VideoController@addDocumentReply', ['middleware' => 'authToken', function () {

}]);

$router->post('/delete_huddle_video', 'VideoController@deleteHuddleVideo', ['middleware' => ['authToken', 'libraryAccountCheck'], function () {

}]);

$router->post('/pause_setting_save', 'VideoController@type_pause', ['middleware' => 'authToken', function () {

}]);

$router->get('/bread_crumb_output', 'VideoController@bread_crumb_output', ['middleware' => 'authToken', function () {

}]);

$router->post('/copyMultipleArtificats', 'VideoController@copyMultipleArtificats', ['middleware' => ['authToken', 'libraryAccountCheck'], function () {

}]);

$router->post('/copy', 'VideoController@copy', ['middleware' => ['authToken', 'libraryAccountCheck'], function () {

}]);

$router->post('/copy_discussion_attachments', 'VideoController@copy_discussion_attachments', ['middleware' => 'authToken', function () {

}]);



$router->post('/copytoaccounts', 'VideoController@copytoaccounts', ['middleware' => 'authToken', function () {

}]);

$router->post('/get_copy_huddle', 'VideoController@get_copy_huddle', ['middleware' => 'authToken', function () {

}]);

$router->post('/video_huddle', 'HuddleController@video_huddle', ['middleware' => 'authToken', function () {

    }]);
    $router->post('/add_temp_video', 'HuddleController@addTempVideo', ['middleware' => 'authToken', function () {

    }]);

$router->post('/video_detail', 'VideoController@video_detail', ['middleware' => 'authToken', function () {

}]);

$router->post('/show_publish_button_or_not', 'VideoController@show_publish_button_or_not', ['middleware' => 'authToken', function () {

}]);

$router->post('/coaching_tracker_note', 'VideoController@coaching_tracker_note', ['middleware' => 'authToken', function () {

}]);

$router->post('/assessment_note', 'VideoController@assessment_note', ['middleware' => 'authToken', function () {

}]);

$router->post('/assessment_detail', 'VideoController@assessment_detail', ['middleware' => 'authToken', function () {

}]);

$router->post('/get_coaching_huddle_name', 'ApiController@get_coaching_huddle_name', ['middleware' => 'authToken', function () {

}]);

$router->get('/test313', 'VideoController@test', ['middleware' => 'authToken', function () {

}]);

$router->post('/view_page_observation', 'VideoController@view_page_observation', ['middleware' => 'authToken', function () {

}]);
$router->post('/workspace_view_page_observation', 'VideoController@workspace_view_page_observation', ['middleware' => 'authToken', function () {

}]);

$router->post('/publish_scripted_observation', 'VideoController@publish_scripted_observation', ['middleware' => 'authToken', function () {

}]);

$router->post('/start_scripted_notes', 'VideoController@start_scripted_notes', ['middleware' => 'authToken', function () {

}]);

$router->post('/get_framework_settings_by_id', 'VideoController@get_framework_settings_by_id', ['middleware' => 'authToken', function () {

}]);

$router->post('/select_video_framework', 'VideoController@select_video_framework', ['middleware' => 'authToken', function () {

}]);
$router->post('/get_security', 'VideoController@get_security', ['middleware' => 'authToken', function () {

}]);


$router->post('/update_scripted_observation_duration', 'VideoController@update_scripted_observation_duration', ['middleware' => 'authToken', function () {

}]);

$router->post('/autoscroll_switch', 'VideoController@autoscroll_switch', ['middleware' => 'authToken']);

$router->post('/update_view_count', 'VideoController@update_view_count', ['middleware' => 'authToken']);

$router->post('/get_counts_of_comment_tags/{account_id}/{user_id}', 'VideoController@get_counts_of_comment_tags', ['middleware' => 'authToken', function () {

}]);

$router->post('/get_resource_url_for_mobile', 'VideoController@get_resource_url_for_mobile', ['middleware' => 'authToken']);
$router->post('/copy_comments_trimming', 'VideoController@copy_comments_trimming', ['middleware' => ['authToken', 'libraryAccountCheck']]);

$router->post('/script_to_transfer_data_from_qa_to_prod/{id}', 'VideoController@script_to_transfer_data_from_qa_to_prod', ['middleware' => 'authToken']);


$router->post('/cake_multiple_upload_documents_mobile_emails_to_queues', 'VideoController@cake_multiple_upload_documents_mobile_emails_to_queues', ['middleware' => 'authToken']);
$router->post('/cake_video_app_success_background_queue_emails_to_queues', 'HuddleController@cake_video_app_success_background_queue_emails_to_queues', ['middleware' => 'authToken']);



$router->post('/update_document_comment_custom_marker', 'VideoController@update_document_comment_custom_marker', ['middleware' => 'authToken']);
$router->post('/update_document_comment_status', 'VideoController@update_document_comment_status', ['middleware' => 'authToken']);

$router->get('/download_pdf_annotations', 'VideoController@downloadPdfAnnotations', ['middleware' => 'authToken']);

$router->post('/download_pdf_annotations', 'VideoController@downloadPdfAnnotations', ['middleware' => 'authToken']);


//$router->post('/account_overview','ApiController@account_overview');
$router->get('/test123', 'ApiController@test');

$router->get('/test_example', 'ExampleController@test');
$router->get('/test_email', 'ExampleController@test_email');
$router->get('/test_queue', 'ExampleController@getQueueStats');

//$router->post('/account_overview','ApiController@account_overview');
//$router->post('/user_summary','ApiController@accountUsersAjax');
//$router->post('/account_overview','ApiController@account_overview');

$router->post('/frequency_of_tagged_standard', 'ApiController@frequency_of_tagged_standard');
$router->post('/filters_dropdown/{id}', 'ApiController@filters_dropdown');
$router->post('/getFramework/{account_id}/{framework_id}', 'ApiController@getFramework');
$router->post('/userDetail/{user_id}/{account_id}/{start_date}/{end_date}/{type}/{workspace_huddle_library}', 'ApiController@userDetail');
$router->get('/analytics_details_filter', 'ApiController@analytics_details_filter');
$router->get('/get_coachee_views/{account_id}/{user_id}', 'ApiController@get_coachee_views');
//$router->get('/test123','ApiController@test');

/**
 * Huddle Controller Routes
 */
$router->post('/get_huddles', 'HuddleController@get_huddles', ['middleware' => 'authToken', function () {},
]);
$router->post('/get_huddles_test', 'HuddleController@get_huddles_test', ['middleware' => 'authToken', function () {},
]);
$router->post('/treeview_detail', 'HuddleController@treeview_detail', ['middleware' => 'authToken', function () {},
]);
$router->post('/move_huddle_folder', 'HuddleController@move_huddle_folder', ['middleware' => 'authToken', function () {},
]);
$router->post('/create_folder', 'HuddleController@create_folder', ['middleware' => 'authToken', function () {},
]);
$router->post('/edit_folder', 'HuddleController@edit_folder', ['middleware' => 'authToken', function () {}
]);
$router->post('/delete_folder', 'HuddleController@delete_folder', ['middleware' => 'authToken', function () {}
]);
$router->post('/delete_huddle', 'HuddleController@delete_huddle', ['middleware' => 'authToken', function () {}
]);
$router->post('/get_bread_crumb', 'HuddleController@get_bread_crumb', ['middleware' => 'authToken', function () {}
]);
$router->post('/document_viewed_mobile', 'HuddleController@document_viewed_mobile', ['middleware' => 'authToken', function () {}
]);

$router->post('/import_user', 'UsersController@import_users_csv', ['middleware' => 'authToken', function () {},
]);
$router->post('/add_users', 'UsersController@addUsers', ['middleware' => 'authToken', function () {},
]);
$router->post('/get_groups', 'UsersController@getGroups', ['middleware' => 'authToken', function () {},
]);
$router->post('/add_group', 'UsersController@addGrups', ['middleware' => 'authToken', function () {},
]);
$router->post('/delete_group/{group_id}/{account_id}', 'UsersController@deleteGroups', ['middleware' => 'authToken', function () {},
]);
$router->post('/delete_group_users/{user_group_id}', 'UsersController@deleteGroupUsers', ['middleware' => 'authToken', function () {},
]);
$router->post('/edit_groups/{group_id}', 'UsersController@editGroups', ['middleware' => 'authToken', function () {},
]);
$router->post('/edit_user_group/{group_id}', 'UsersController@editUserGroups', ['middleware' => 'authToken', function () {},
]);
$router->post('/get_members/{account_id}', 'UsersController@getGoupMembers', ['middleware' => 'authToken', function () {},
]);
$router->post('/rename_group', 'UsersController@renameGroup', ['middleware' => 'authToken', function () {},
]);
$router->get('/inactive/{account_id}/{user_id}/{role_id}', 'UsersController@inactive', ['middleware' => 'authToken', function () {},
]);
$router->post('/active/{account_id}/{user_id}', 'UsersController@active', ['middleware' => 'authToken', function () {},
]);
$router->get('/get_participants/{account_folder_id}', 'UsersController@getParticipants', ['middleware' => 'authToken', function () {},
]);
$router->post('/check_coaching_huddle_scenario', 'UsersController@check_coaching_huddle_scenario', ['middleware' => 'authToken', function () {},
]);
$router->post('/change_password', 'UsersController@change_password', ['middleware' => 'authToken', function () {},
]);
$router->post('/get_new_password_change_page_translations', 'UsersController@get_new_password_change_page_translations', ['middleware' => 'authToken', function () {},
]);
$router->post('/get_user_settings', 'UsersController@getUserSettings', ['middleware' => 'authToken', function () {},]);
$router->post('/get_user_profile', 'UsersController@getUserProfile', ['middleware' => 'authToken', function () {},]);
$router->post('/update_user_settings', 'UsersController@updateUserSetting', ['middleware' => 'authToken', function () {},
]);
$router->post('/upload_user_settings_image', 'UsersController@uploadImage', ['middleware' => 'authToken', function () {},
]);
$router->post('/turn_off_email_subscriptions', 'UsersController@turn_off_email_subscriptions', ['middleware' => 'authToken', function () {},
]);




// $router->get('/get_huddles_get','HuddleController@get_huddles');
// $router->get('/get_huddles_test_get','HuddleController@get_huddles_test');
// $router->get('/treeview_detail_get','HuddleController@treeview_detail');

$router->post('/get_coaching_huddle_name', 'ApiController@get_coaching_huddle_name', ['middleware' => 'authToken', function () {

}]);

$router->post('/trial_signup', 'TrialSignUpController@trial_signup', ['middleware' => 'authToken', function () {

}
]);

$router->post('/checkbox_options', 'TrialSignUpController@checkbox_options', ['middleware' => 'authToken', function () {

}
]);

$router->post('/email_existance', 'TrialSignUpController@email_existance', ['middleware' => 'authToken', function () {

}
]);

$router->get('/resend_activation_code/{account_id}/{user_id}', 'TrialSignUpController@resend_activation_code', ['middleware' => 'authToken', function () {

}
]);

$router->post('/export_analytics_standards_to_excel', 'ExportAnalyticsController@ExportStandardsToExcel', ['middleware' => 'authToken']);
$router->get('/export_analytics_standards_to_excel_async', 'ExportAnalyticsController@ExportStandardsToExcelAsync', ['middleware' => 'authToken']);
$router->get('/async_create_churnzero_attribute', 'ApiController@async_create_churnzero_attribute', ['middleware' => 'authToken']);
$router->post('/is_export_analytics_download_ready', 'ExportAnalyticsController@is_download_ready', ['middleware' => 'authToken']);
$router->get('/get_cirqlive_data/{id}', 'CirQliveController@getCirqLiveData', ['middleware' => 'authToken']);
$router->get('/get_cirq_page_data', 'CirQliveController@getCirqLTIPage', ['middleware' => 'authToken']);
$router->post('/get_upcoming_conferences', 'CirQliveController@get_upcoming_conferences', ['middleware' => 'authToken']);

//Huddles page routes
$router->post('/get_huddles_actvities', 'HuddleController@get_huddles_actvities', ['middleware' => 'authToken']);
$router->post('/get_artifects', 'HuddleController@get_artifects', ['middleware' => 'authToken']);

$router->post('/get_assessee_data', 'HuddleController@get_assessee_data', ['middleware' => 'authToken']);
$router->post('/get_participants', 'HuddleController@get_participants', ['middleware' => 'authToken']);
$router->post('/edit_title', 'HuddleController@edit_title', ['middleware' => ['authToken', 'libraryAccountCheck']]);
$router->post('/trim_video', 'HuddleController@trim_video', ['middleware' => 'authToken']);
$router->post('/get_all_discussions', 'HuddleController@get_all_discussions', ['middleware' => 'authToken']);
$router->post('/delete_discussion', 'HuddleController@delete_discussion', ['middleware' => 'authToken']);
$router->post('/add_discussion', 'HuddleController@add_discussion', ['middleware' => 'authToken']);
$router->post('/edit_discussion', 'HuddleController@edit_discussion', ['middleware' => 'authToken']);
$router->post('/uploadVideos', 'HuddleController@uploadVideos', ['middleware' => 'authToken']);
$router->post('/associateVideoDocuments', 'HuddleController@associateVideoDocuments', ['middleware' => 'authToken']);
$router->post('/get_huddle_videos', 'HuddleController@getHuddleVideos', ['middleware' => 'authToken']);
$router->post('/get_workspace_artifects', 'WorkSpaceController@get_workspace_artifects', ['middleware' => 'authToken']);
// $router->post('/get_workspace_videos_for_popup', 'WorkSpaceController@get_workspace_videos_for_popup', ['middleware' => 'authToken']);
$router->post('/get_user_activities', 'WorkSpaceController@getUserActivities', ['middleware' => 'authToken']);
$router->post('/exportEmail', 'DiscussionController@exportEmail', ['middleware' => 'authToken']);
$router->post('/check_videos_count', 'HuddleController@check_videos_count', ['middleware' => 'authToken']);
$router->post('/get_single_video', 'VideoController@get_single_video', ['middleware' => 'authToken']);
$router->post('/publish_assessment_huddle', 'ApiController@publish_assessment_huddle', ['middleware' => 'authToken']);
$router->post('/publish_feedback_and_grades', 'ApiController@publish_feedback_and_grades', ['middleware' => 'authToken']);
$router->post('/submit_assessment_huddle', 'ApiController@submit_assessment_huddle', ['middleware' => 'authToken']);
$router->post('/delete_discussion_attachment', 'DiscussionController@deleteDiscussionAttachment', ['middleware' => 'authToken']);
$router->post('/rename_discussion_attachment', 'DiscussionController@renameDiscussionAttachment', ['middleware' => 'authToken']);
$router->post('/discussion_replies_mark_read', 'DiscussionController@discussion_replies_mark_read', ['middleware' => 'authToken']);

/* =======Discussion module routes */

Route::group(['prefix' => 'discussion'], function() {

    Route::post('discussion-detail', 'DiscussionController@detail', ['middleware' => 'authToken']);
    Route::post('discussion-download', 'DiscussionController@export_download', ['middleware' => 'authToken']);
    Route::post('discussion-download-all', 'DiscussionController@all_export_download', ['middleware' => 'authToken']);
});

$router->get('/test','TrialSignUpController@test', ['middleware' => 'authToken', function () {}
]);

$router->get('/add_to_contact_list_for_cake/{id1}/{id2}','TrialSignUpController@add_to_contact_list_for_cake', ['middleware' => 'authToken', function () {}
]);

$router->get('/remove_from_contact_list_for_cake/{id1}/{id2}','TrialSignUpController@remove_from_contact_list_for_cake', ['middleware' => 'authToken', function () {}
]);

$router->get('/get_language_from_lumen','Controller@getPreferredLanguage', ['middleware' => 'authToken', function () {}
]);

$router->post('/get_language_based_content','ApiController@getLanguageBasedContent');

$router->post('/get_video_duration', 'HuddleController@get_video_duration', ['middleware' => 'authToken']);


//Dashboard page routes
$router->post('/get_dashboard_user_activities', 'DashboardController@getDashboardUserActivities', ['middleware' => 'authToken']);
$router->post('/get_dashboard_user_activitiesp', 'DashboardController@getDashboardUserActivitiesB', ['middleware' => 'authToken']);


$router->post('/update_getting_started_model_show', 'DashboardController@update_getting_started_model_show', ['middleware' => 'authToken']);
$router->post('/get_counts_of_comment_tags', 'DashboardController@get_counts_of_comment_tags', ['middleware' => 'authToken']);
$router->post('/churnzero_attribute_list', 'DashboardController@churnzero_attribute_list', ['middleware' => 'authToken']);
$router->post('/removeRoleAnomalies', 'HuddleController@removeRoleAnomalies', ['middleware' => 'authToken']);
$router->post('/get_dashboard_analytics', 'DashboardController@getDashboardAnalytics', ['middleware' => 'authToken']);


/* =======People module routes */

Route::group(['prefix' => 'users'], function () use ($router) {
    $router->post('/administrators_groups', 'UsersController@administrators_groups', ['middleware' => 'authToken', function () {},
    ]);
    $router->post('/deleteAccount', 'UsersController@deleteAccount', ['middleware' => 'authToken', function () {},
    ]);
    $router->post('/change_password_accounts', 'UsersController@change_password_accounts', ['middleware' => 'authToken', function () {},
    ]);
    $router->post('/resendInvitation', 'UsersController@resendInvitation', ['middleware' => 'authToken', function () {},
    ]);
    $router->post('/associate_video_huddle_user', 'UsersController@associate_video_huddle_user', ['middleware' => 'authToken', function () {},
    ]);
    $router->post('/change_permission', 'UsersController@change_permission', ['middleware' => 'authToken', function () {},
    ]);
    $router->post('/associate_huddle_user', 'UsersController@associate_huddle_user', ['middleware' => 'authToken', function () {},
    ]);
    $router->post('/assign_user', 'UsersController@assign_user', ['middleware' => 'authToken', function () {},
    ]);
});

//Tracker Page Routes
$router->post('/get_assessment_tracker_huddles', 'TrackerController@get_assessment_tracker_huddles', ['middleware' => 'authToken']);
$router->post('/get_assessment_tracker_huddle_assessees', 'TrackerController@get_assessment_tracker_huddle_assessees', ['middleware' => 'authToken']);
$router->post('/isHuddleAllowed', 'TrackerController@isHuddleAllowed', ['middleware' => 'authToken']);
$router->post('/coaching_tracker_video_detail', 'TrackerController@coaching_tracker_video_detail', ['middleware' => 'authToken']);
$router->post('/coaching_tracker_note_tracker_controller', 'TrackerController@coaching_tracker_note', ['middleware' => 'authToken']);
$router->post('/remove_tracking', 'TrackerController@remove_tracking', ['middleware' => 'authToken']);
$router->post('/coaching_tracker', 'DashboardController@coach_tracker', ['middleware' => 'authToken']);
$router->post('/export_coaching_tracker', 'DashboardController@export_coaching_tracker', ['middleware' => 'authToken']);
$router->post('/video_standard_tags_with_performance_rating/{account_id}/{document_id}', 'TrackerController@video_standard_tags_with_performance_rating', ['middleware' => 'authToken']);
$router->post('/send_push_notification', 'HuddleController@send_push_notification', ['middleware' => 'authToken']);
$router->post('/update_comment_time', 'VideoController@update_comment_time', ['middleware' => 'authToken']);
$router->post('/getWowzaStreamDetails', 'HuddleController@getWowzaStreamDetails', ['middleware' => 'authToken']);
$router->post('/getStreamViewerCount', 'HuddleController@getStreamViewerCount', ['middleware' => 'authToken']);
$router->post('/wowzaChangeStreamName', 'HuddleController@wowzaChangeStreamName', ['middleware' => 'authToken']);

// HMH Licensing Routes
$router->post('/create_order', 'LicensingController@create_order', ['middleware' => 'authToken']);
$router->post('/get_orders', 'LicensingController@get_orders', ['middleware' => 'authToken']);
$router->post('/send_invitations', 'LicensingController@send_invitations', ['middleware' => 'authToken']);
$router->post('/resend_invitation', 'LicensingController@resend_invitation', ['middleware' => 'authToken']);
$router->post('/get_video_duration/{id}', 'HuddleController@getVideoDuration', ['middleware' => 'authToken', function () {

}]);
$router->post('/getCommentCounts', 'ApiController@getCommentCounts', ['middleware' => 'authToken', function () {}]);
$router->post('/getSecureStreamUrl', 'VideoController@getSecureStreamUrl', ['middleware' => 'authToken']);

$router->post('/push_notification_sns', 'VideoController@push_notification_sns', ['middleware' => 'authToken', function () {

}]);

$router->post('/oneSignal_addDevice', 'VideoController@oneSignal_addDevice', ['middleware' => 'authToken', function () {

}]);



// Account Controller Routes
$router->post('/get_general_account_settings', 'AccountsController@get_general_account_settings', ['middleware' => 'authToken']);

$router->post('/update_general_account_settings', 'AccountsController@update_general_account_settings', ['middleware' => 'authToken']);

$router->post('/account_users_storage_stats', 'AccountsController@account_users_storage_stats', ['middleware' => 'authToken']);

$router->post('/change_account_owner', 'AccountsController@changeAccountOwner', ['middleware' => 'authToken']);
$router->post('/colors_and_logo', 'AccountsController@colorsAndLogo', ['middleware' => 'authToken']);
$router->post('/get_unarchive_huddles', 'AccountsController@get_unarchive_huddles', ['middleware' => 'authToken', function () {},
]);
$router->post('/get_colors_logos_api', 'AccountsController@get_colors_logos_api', ['middleware' => 'authToken', function () {},
]);
$router->post('/reset_colors_logos', 'AccountsController@reset_colors_logos', ['middleware' => 'authToken', function () {},
]);
$router->post('/global_export', 'AccountsController@global_export', ['middleware' => 'authToken', function () {},
]);

$router->post('/archive', 'AccountsController@archive', ['middleware' => 'authToken', function () {},
]);

$router->post('/unarchive', 'AccountsController@unarchive', ['middleware' => 'authToken', function () {},
]);

$router->post('/update_getting_started_archiving', 'AccountsController@update_getting_started_archiving', ['middleware' => 'authToken', function () {},
]);

$router->post('/send_onesignal_notifications', 'VideoController@send_onesignal_notifications', ['middleware' => 'authToken', function () {

}]);
$router->get('/submitTranscribeJob/', 'HuddleController@submitTranscribeJob', ['middleware' => 'authToken']);

$router->post('/brain_tree_testing', 'UsersController@brain_tree_testing', ['middleware' => 'authToken', function () {

}]);
$router->get('/submitTranscribeJob/', 'HuddleController@submitTranscribeJob', ['middleware' => 'authToken']);


//*** PaymentController Routes Start

$router->post('/fetch_account_plan_details_angular', 'PaymentController@fetch_account_plan_details_angular', ['middleware' => 'authToken', function () {

}]);

$router->post('/purchase_new_plan', 'PaymentController@purchase_new_plan', ['middleware' => 'authToken', function () {

}]);

$router->post('/update_customer_info', 'PaymentController@update_customer_info', ['middleware' => 'authToken', function () {

}]);

$router->post('/credit_card_failure_email', 'PaymentController@credit_card_failure_email', ['middleware' => 'authToken', function () {

}]);

$router->post('/cron_for_checking_card_failures', 'PaymentController@cron_for_checking_card_failures', ['middleware' => 'authToken', function () {

}]);

//*** PaymentController Routes End

$router->post('/analytics_filters/save', 'AnalyticsFilterController@save', ['middleware' => 'authToken']);
$router->post('/analytics_filters/get_all', 'AnalyticsFilterController@get_all', ['middleware' => 'authToken']);
$router->post('/analytics_filters/delete', 'AnalyticsFilterController@delete', ['middleware' => 'authToken']);
$router->post('/submitMediaConvertJob', 'HuddleController@submitMediaConvertJob', ['middleware' => 'authToken', function () {}]);
$router->post('/send_push_from_lumen', 'VideoController@send_push_from_lumen', ['middleware' => 'authToken']);
$router->post('/receive_notification', 'ZencoderController@receive_notification', ['middleware' => 'authToken']);
$router->post('/receive_notification_coconut', 'ZencoderController@receive_notification_coconut', ['middleware' => 'authToken']);
$router->post('/getLibraryData', 'VideoLibraryController@getLibraryData', ['middleware' => 'authToken', function () {}]);
$router->post('/add_categories', 'VideoLibraryController@add_categories', ['middleware' => ['authToken', 'libraryAccountCheck'], function () {}]);
$router->post('/get_video_categories', 'VideoLibraryController@get_video_categories', ['middleware' => 'authToken']);
$router->post('/get_categories', 'VideoLibraryController@get_categories', ['middleware' => ['authToken', 'libraryAccountCheck']]);
$router->post('/change_video_categories', 'VideoLibraryController@change_video_categories', ['middleware' => ['authToken', 'libraryAccountCheck']]);
$router->post('/change_video_description', 'VideoLibraryController@change_video_description', ['middleware' =>['authToken', 'libraryAccountCheck']]);
$router->post('/uploadLibraryVideo', 'VideoLibraryController@uploadLibraryVideo', ['middleware' => ['authToken', 'libraryAccountCheck']]);
$router->post('/download_videos', 'VideoController@downloadVideos', ['middleware' => 'authToken']);

$router->post('/add_url', 'VideoController@addURL', ['middleware' => 'authToken']);
$router->post('/edit_url', 'VideoController@editURL', ['middleware' => 'authToken']);
$router->post('/live_streaming_allowed', 'VideoController@live_streaming_allowed', ['middleware' => 'authToken']);

/* =======Goals module routes start======== */

Route::group(['prefix' => 'goals'], function() use ($router) {
    $router->post('addEditGoal', 'GoalsController@addEditGoal', ['middleware' => 'authToken']);
    $router->get('getGoalsSettings/{account_id}', 'GoalsController@getGoalsSettings', ['middleware' => 'authToken']);
    $router->post('getGoals', 'GoalsController@getGoals', ['middleware' => 'authToken']);
    $router->post('goalsExportData', 'GoalsController@goalsExportData', ['middleware' => 'authToken']);
    $router->post('getGoalStatusExported', 'GoalsController@getGoalStatusExported', ['middleware' => 'authToken']);
    $router->post('sortActionItem', 'GoalsController@sortActionItem', ['middleware' => 'authToken']);
    $router->post('getGoalDetails', 'GoalsController@getGoalDetails', ['middleware' => 'authToken']);
    $router->post('updateGoalsSettings', 'GoalsController@updateGoalsSettings', ['middleware' => 'authToken']);
    $router->post('addGoalOwners', 'GoalsController@addGoalOwners', ['middleware' => 'authToken']);
    $router->post('addGoalCollaborators', 'GoalsController@addGoalCollaborators', ['middleware' => 'authToken']);
    $router->post('addEditGoalItem', 'GoalsController@addEditGoalItem', ['middleware' => 'authToken']);
    $router->post('goalItemReview', 'GoalsController@goalItemReview', ['middleware' => 'authToken']);
    $router->post('addEvidence', 'GoalsController@addEvidence', ['middleware' => 'authToken']);
    $router->post('removeEvidence', 'GoalsController@removeEvidence', ['middleware' => 'authToken']);
    $router->post('deleteActionItem', 'GoalsController@deleteActionItem', ['middleware' => 'authToken']);
    $router->post('deleteCategory', 'GoalsController@deleteCategory', ['middleware' => 'authToken']);
    $router->post('addEditCategories', 'GoalsController@addEditCategories', ['middleware' => 'authToken']);
    $router->post('deleteGoal', 'GoalsController@deleteGoal', ['middleware' => 'authToken']);
    $router->post('getGoalStatus', 'GoalsController@getGoalStatus', ['middleware' => 'authToken']);
    $router->post('getHuddlesList', 'GoalsController@getHuddlesList', ['middleware' => 'authToken']);
    $router->post('getHuddleDocuments', 'GoalsController@getHuddleDocuments', ['middleware' => 'authToken']);
    $router->post('addGoalItemStandards', 'GoalsController@addGoalItemStandards', ['middleware' => 'authToken']);
    $router->post('getEvidenceStandards', 'GoalsController@getEvidenceStandards', ['middleware' => 'authToken']);
    $router->post('getAverageRating', 'GoalsController@getAverageRating', ['middleware' => 'authToken']);
    $router->get('goalCompletedItems/{goal_id}/{user_id}', 'GoalsController@goalCompletedItem', ['middleware' => 'authToken']);
    $router->post('getGoalActivities', 'DashboardController@getGoalActivities', ['middleware' => 'authToken']);
    $router->post('/print_excel_goal_comments', 'GoalsController@print_excel_goal_comments', ['middleware' => 'authToken']);
    $router->post('/print_pdf_goal_comments', 'GoalsController@print_pdf_goal_comments', ['middleware' => 'authToken']);
    $router->post('/exportActionItems', 'GoalsController@exportActionItems', ['middleware' => 'authToken']);
    $router->post('/archive_unarchive_goal', 'GoalsController@archive_unarchive_goal', ['middleware' => 'authToken']);
    $router->post('/getActionItemRF', 'GoalsController@getActionItemRF', ['middleware' => 'authToken']);
});

/* =======Goals module routes end======== */

/* ======= Custom Fields Routes Start ======== */
$router->post('/save_custom_fields', 'CustomFieldsController@save_custom_fields', ['middleware' => 'authToken']);
$router->post('/get_custom_fields', 'CustomFieldsController@get_custom_fields', ['middleware' => 'authToken']);
$router->post('/delete_custom_field', 'CustomFieldsController@delete_custom_field', ['middleware' => 'authToken']);
$router->post('/save_user_custom_fields', 'CustomFieldsController@save_user_custom_fields', ['middleware' => 'authToken']);
$router->post('/get_user_custom_fields', 'CustomFieldsController@get_user_custom_fields', ['middleware' => 'authToken']);
// $router->post('/save_assessment_custom_fields', 'CustomFieldsController@save_assessment_custom_fields', ['middleware' => 'authToken']);
$router->post('/get_assessment_custom_fields', 'CustomFieldsController@get_assessment_custom_fields', ['middleware' => 'authToken']);
$router->post('/save_user_assessment_custom_fields', 'CustomFieldsController@save_user_assessment_custom_fields', ['middleware' => 'authToken']);
$router->post('/get_user_assessment_custom_fields', 'CustomFieldsController@get_user_assessment_custom_fields', ['middleware' => 'authToken']);

$router->post('/export_assessees_excel', 'ApiController@export_assessees_excel', ['middleware' => 'authToken']);
$router->post('/export_assessees_csv', 'ApiController@export_assessees_csv', ['middleware' => 'authToken']);
$router->post('/export_assessees_pdf', 'ApiController@export_assessees_pdf', ['middleware' => 'authToken']);

/* ======= Custom Fields Routes End ======== */
/* =======Rubrics module routes start======== */

Route::group(['prefix' => 'rubrics'], function() use ($router) {
    $router->post('get_rubrics_list', 'RubricsController@get_rubrics_list', ['middleware' => 'authToken']);
    $router->post('save_frame_work_first_level', 'RubricsController@save_frame_work_first_level', ['middleware' => 'authToken']);
    $router->post('publish_framework', 'RubricsController@publish_framework', ['middleware' => 'authToken']);
    $router->post('unpublish_framework', 'RubricsController@unpublish_framework', ['middleware' => 'authToken']);
    $router->post('delete_framework', 'RubricsController@delete_framework', ['middleware' => 'authToken']);
    $router->post('create_performance_levels', 'RubricsController@create_performance_levels', ['middleware' => 'authToken']);
    $router->post('save_rubric_standards', 'RubricsController@save_rubric_standards', ['middleware' => 'authToken']);
    $router->post('get_framework_standards_with_pl_levels', 'RubricsController@get_framework_standards_with_pl_levels', ['middleware' => 'authToken']);
    $router->post('save_unique_description', 'RubricsController@save_unique_description', ['middleware' => 'authToken']);
    $router->post('get_framework_settings', 'RubricsController@get_framework_settings', ['middleware' => 'authToken']);
    $router->post('get_performance_levels_for_framework', 'RubricsController@get_performance_levels_for_framework', ['middleware' => 'authToken']);
    $router->post('get_standards_with_unique_performance_levels', 'RubricsController@get_standards_with_unique_performance_levels', ['middleware' => 'authToken']);
    $router->post('script_for_adding_position_for_standards', 'RubricsController@script_for_adding_position_for_standards', ['middleware' => 'authToken']);
    $router->post('duplicate_performance_levels', 'RubricsController@duplicate_performance_levels', ['middleware' => 'authToken']);
    $router->post('delete_all_performance_levels', 'RubricsController@delete_all_performance_levels', ['middleware' => 'authToken']);
});

/* =======Goals module routes end======== */
//SSO ROUTES
$router->post('/get/sso-link', 'UsersController@getSSOLink', ['middleware' => 'authToken']);
$router->get('/saml2/mytestidp1/logout', array(
    'as' => 'saml2_logout',
    'uses' => 'SSOController@logout',
));

$router->get('/saml2/mytestidp1/login', array(
    'as' => 'saml2_login',
    'uses' => 'SSOController@customlogin',
));

$router->get('/saml2/mytestidp1/metadata', array(
    'as' => 'saml2_metadata',
    'uses' => 'SSOController@metadata',
));

$router->post('/acs/{account_id}', array(
    'as' => 'saml2_acs',
    'uses' => 'SSOController@assertionConsumerService',
));

$router->get('/saml2/mytestidp1/sls', array(
    'as' => 'saml2_sls',
    'uses' => 'SSOController@sls',
));

$router->get('/account/default', array(
    'as' => 'setDefaultAccountForAllUsers',
    'uses' => 'AccountsController@setDefaultAccountForAllUsers',
));
/* =======Rubrics module routes end======== */



// Deactivate Users Function

$router->post('/deactivate_sibme_users', 'UsersController@deactivate_sibme_users', ['middleware' => 'authToken']);
$router->post('/deactivate_hmh_users', 'UsersController@deactivate_hmh_users', ['middleware' => 'authToken']);
/* =======Goals module routes end======== */

/* =======Screen Sharing module routes start======== */
$router->post('screenShareEvent', 'LiveStreamController@screenShareEvent', ['middleware' => 'authToken']);
$router->post('checkScreenSharing', 'LiveStreamController@checkScreenSharing', ['middleware' => 'authToken']);
$router->get('getClippingData/{document_id}', 'LiveStreamController@getClippingData', ['middleware' => 'authToken']);
$router->post('updateLiveStreamStatus', 'LiveStreamController@updateLiveStreamStatus', ['middleware' => 'authToken']);

$router->get('/checkSubmittedAssessemnts', 'AccountsController@check_is_submitted_assessments', ['middleware' => 'authToken']);
/* =======Screen Sharing module routes end======== */
$router->get('/checkSubmittedAssessemnts', 'AccountsController@check_is_submitted_assessments', ['middleware' => 'authToken']);

$router->post('submit_clip_merging_job', 'LiveStreamController@submit_clip_merging_job', ['middleware' => 'authToken']);


/* =======Reporting Screens Start======== */
$router->post('coaching_report', 'ReportingController@coaching_report', ['middleware' => 'authToken']);
$router->post('add_report', 'ReportingController@add_report', ['middleware' => 'authToken']);
$router->post('delete_report', 'ReportingController@delete_report', ['middleware' => 'authToken']);
$router->post('get_reports', 'ReportingController@get_reports', ['middleware' => 'authToken']);
$router->post('update_reports', 'ReportingController@update_reports', ['middleware' => 'authToken']);
$router->post('coaching_report_csv', 'ReportingController@coaching_report_csv', ['middleware' => 'authToken']);
$router->post('coaching_report_excel', 'ReportingController@coaching_report_excel', ['middleware' => 'authToken']);
$router->post('coaching_report_pdf', 'ReportingController@coaching_report_pdf', ['middleware' => 'authToken']);
$router->post('huddle_report', 'ReportingController@huddle_report', ['middleware' => 'authToken']);
$router->post('huddle_report_csv', 'ReportingController@huddle_report_csv', ['middleware' => 'authToken']);
$router->post('huddle_report_excel', 'ReportingController@huddle_report_excel', ['middleware' => 'authToken']);
$router->post('huddle_report_pdf', 'ReportingController@huddle_report_pdf', ['middleware' => 'authToken']);
$router->post('assessment_report', 'ReportingController@assessment_report', ['middleware' => 'authToken']);
$router->post('assessment_report_csv', 'ReportingController@assessment_report_csv', ['middleware' => 'authToken']);
$router->post('assessment_report_excel', 'ReportingController@assessment_report_excel', ['middleware' => 'authToken']);
$router->post('assessment_report_pdf', 'ReportingController@assessment_report_pdf', ['middleware' => 'authToken']);
$router->post('user_summary_report', 'ReportingController@user_summary_report', ['middleware' => 'authToken']);
$router->post('user_summary_report_csv', 'ReportingController@user_summary_report_csv', ['middleware' => 'authToken']);
$router->post('user_summary_report_excel', 'ReportingController@user_summary_report_excel', ['middleware' => 'authToken']);
$router->post('user_summary_report_pdf', 'ReportingController@user_summary_report_pdf', ['middleware' => 'authToken']);
$router->post('detail_view', 'ReportingController@detail_view', ['middleware' => 'authToken']);
$router->post('detail_view_csv', 'ReportingController@detail_view_csv', ['middleware' => 'authToken']);
$router->post('detail_view_excel', 'ReportingController@detail_view_excel', ['middleware' => 'authToken']);
$router->post('detail_view_pdf', 'ReportingController@detail_view_pdf', ['middleware' => 'authToken']);
$router->get('huddle_report_pdf', 'ReportingController@huddle_report_pdf', ['middleware' => 'authToken']);
$router->get('huddle_report_csv', 'ReportingController@huddle_report_csv', ['middleware' => 'authToken']);
$router->get('huddle_report_excel', 'ReportingController@huddle_report_excel', ['middleware' => 'authToken']);

$router->get('coaching_report_pdf', 'ReportingController@coaching_report_pdf', ['middleware' => 'authToken']);
$router->get('coaching_report_csv', 'ReportingController@coaching_report_csv', ['middleware' => 'authToken']);
$router->get('coaching_report_excel', 'ReportingController@coaching_report_excel', ['middleware' => 'authToken']);

$router->get('assessment_report_pdf', 'ReportingController@assessment_report_pdf', ['middleware' => 'authToken']);
$router->get('assessment_report_csv', 'ReportingController@assessment_report_csv', ['middleware' => 'authToken']);
$router->get('assessment_report_excel', 'ReportingController@assessment_report_excel', ['middleware' => 'authToken']);

$router->get('user_summary_report_pdf', 'ReportingController@user_summary_report_pdf', ['middleware' => 'authToken']);
$router->get('user_summary_report_csv', 'ReportingController@user_summary_report_csv', ['middleware' => 'authToken']);
$router->get('user_summary_report_excel', 'ReportingController@user_summary_report_excel', ['middleware' => 'authToken']);
$router->post('assessment_report_assessee', 'ReportingController@assessment_report_assessee', ['middleware' => 'authToken']);
$router->post('huddle_report_users', 'ReportingController@huddle_report_users', ['middleware' => 'authToken']);

//assessment_report
/* =======Reporting Screens End======== */

$router->post('update_auto_delete', 'AccountsController@update_auto_delete', ['middleware' => 'authToken']);

/* =======Artifacts Folder module routes start======== */
Route::group(['prefix' => 'folders'], function() use ($router) {
    $router->post('create', 'FoldersController@create', ['middleware' => 'authToken']);
    $router->post('rename', 'FoldersController@rename', ['middleware' => 'authToken']);
    $router->post('delete', 'FoldersController@delete', ['middleware' => 'authToken']);
    $router->post('treeview_data', 'FoldersController@treeview_data', ['middleware' => 'authToken']);
    $router->post('move_document_folder', 'FoldersController@move_document_folder', ['middleware' => 'authToken']);
    $router->post('search_huddle_folders', 'FoldersController@search_huddle_folders', ['middleware' => 'authToken']);
    $router->post('share_folder', 'FoldersController@share_folder', ['middleware' => 'authToken']);

});
/* =======Artifacts Folder module routes end======== */

/* =======Resource controller + thumbnails start======== */
$router->post('/generate_resources_thumbnail_cake', 'ResourceController@thumbnailsForMobile');
/* =======Resource controller + thumbnails End======== */


$router->post('/get_video_information', 'VideoController@get_video_information', ['middleware' => 'authToken', function () {

}]);
$router->post('/set_user_online_status', 'VideoController@set_user_online_status', ['middleware' => 'authToken', function () {

}]);
$router->post('/get_user_online_status', 'VideoController@get_user_online_status', ['middleware' => 'authToken', function () {

}]);

$router->post('duplicate_document_ids_tracker', 'VideoController@duplicateDocumentsIDsTracker', ['middleware' => 'authToken']);
$router->post('add_websocket_logs', 'LiveStreamController@add_websocket_logs', ['middleware' => 'authToken']);
$router->post('elementalStatusUpdates', 'LiveStreamController@elementalStatusUpdates', ['middleware' => 'authToken']);
$router->post('manuallySubmitAudioForTranscription', 'Controller@manuallySubmitAudioForTranscription', ['middleware' => 'authToken']);
$router->get('getTranscodingJobDetails/{document_id}', 'LiveStreamController@getTranscodingJobDetails', ['middleware' => 'authToken']);

