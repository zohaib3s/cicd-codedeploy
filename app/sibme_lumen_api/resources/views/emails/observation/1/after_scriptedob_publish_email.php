<?php
$sibme_base_url = config('s3.sibme_base_url');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><?php echo $site_title; ?> - Scripted Observation</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style>
            img { max-width:100%;}
        </style>

    </head>
    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style=" font-family:Arial, Helvetica, sans-serif;">

            <tr>

                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">

                        <tr>
                            <td style="text-align:center; padding:30px 0;">
                                <img src="<?php echo $sibme_base_url ?>img/logo-dark.png" width="120" alt="<?php echo $site_title; ?> ">
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; border-bottom:1px solid #ddd;">

                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; font-size:16px; padding:30px 0; line-height:26px; font-family:Arial, Helvetica, sans-serif;">



                                <b><?php echo $creator['first_name'] . " " . $creator['last_name'] ?></b> <br />
                                published a scripted observation<br />
                                <b><?php echo $huddle_name ?></b>
                            </td>

                        </tr>

                        <tr>
                            <td style="text-align:center; padding:10px 0 30px 0;">
                                <a href="<?php echo $sibme_base_url . 'huddles/observation_details_1/' . $huddle_id . '/' . $video_id; ?>" title="Click to view observation"> <img src="<?php echo $sibme_base_url ?>img/sibme_observation.png" width="330" height="50" alt="Click to view observation " /></a>
                            </td>
                        </tr>



                        <tr>
                            <td style="font-size: 14px; text-align: left; color: #212121; padding-top:25px; font-family:Arial, Helvetica, sans-serif; text-align:center;">
                                <a href="#" style="color:#616161; text-decoration:underline;">Unsubscribe Now</a> <br /><br />
                                <a href="<?php echo $sibme_base_url ?>" style="color:#616161; text-decoration:none;">Powered by <?php echo $site_title; ?></a>
                            </td>
                        </tr>




                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>