<?php
if($export_type == "pdf") {
    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PDF</title>
</head>
<body>
    <style type="text/css">
        @page {
            size: a4 landscape;margin:15px;
        }

        .bold { font-weight:600;}
        .pdfmain { margin:0 auto; font-family:Arial, Helvetica, sans-serif; font-size:8px;     word-break: break-word; text-align:center; border:1px solid #ddd;border-bottom:0px solid #ddd;}
        .bx1 { float:left; padding:5px; width:7%;}
        .bx2 { float:left; padding:5px; width:8%;}
        .bx3 { float:left; padding:5px;width:7%;}
        .bx4 { float:left; padding:5px;width:12%;}
        .bx5 { float:left; padding:5px;width:5%;}
        .bx6 { float:left; padding:5px; width:9%;}
        .bx7 { float:left; padding:5px; width:8%;}
        .bx8 { float:left; padding:5px; width:8%;}
        .bx9 { float:left; padding:5px;width:7%;}
        .bx10 { float:left; padding:5px;width:9%;}
        .bx11 { float:left; padding:5px;width:7%;}
        .bxx2 {}
        .clear { clear:both;}
        .bxx2 {    width: 680px;float: left;    border-left: 1px solid #ddd;}
        .repeated {border-bottom: 1px solid #ddd; width: 680px;}
        .bxx2 .repeated:last-child { border-bottom:0;}
        .pdfrow { border-bottom:1px solid #ddd;}
        .rep_cell{float:left;     width: 80px;
            padding: 6px;}
        .inner{display:inline-block;width:80px;
            padding: 8px 6px 0 6px; }
    </style>
    <?php
}
$data = $tracker_data["account_coaches"];
$html = "";
$coach = $data[0];
$coachees = $coach["Coachees"];
$headers = [];
$new = '

<div class="pdfmain">
    
    <div class="pdfrow bold">
       <div class="bx1">Coach Name</div>
       <div class="bx2">Coachee Name</div>
       <div class="bx3">Date Range</div>
       <div class="bx4">Coachee Email</div>
       <div class="rep_cell">Video Date</div>
       <div class="rep_cell">Feedback Timing</div>
       <div class="rep_cell">Coach Comments</div>
       <div class="rep_cell">Coachee Comments</div>
       <div class="rep_cell">Attachments</div>
       <div class="rep_cell">Session Summary</div>
       <div class="rep_cell">Average PL</div>
       <div class="clear"></div>
    </div>
    
    ';
foreach ($coachees as $coachee)
{
    $new .= '<div class="pdfrow">
       <div class="bx1">'.$coach['first_name']." ".$coach['last_name'].'</div>
       <div class="bx2">'.$coachee['first_name']." ".$coachee['last_name'].'</div>
       <div class="bx3">'.$start_date." ".$end_date.'</div>
       <div class="bx4">'.$coachee['email'].'</div>
       
       <div class="bxx2">
           ';


    $html .= "<tr>
                <td>".$coach['first_name']." ".$coach['last_name']."</td>
                <td>".$coachee['first_name']." ".$coachee['last_name']."</td>
                <td>".$start_date." ".$end_date."</td>
                <td>".$coachee['email']."</td>";
    foreach ($coachee["Videos"] as $key => $video)
    {
        $key = $key + 1;
        // Assuming that Our Export Excel Library supports only for maximum 36 videos.
        //if($key>36) break;

        $header_key = "video_".$key;
        if(!in_array($header_key,$headers))
        {
            $headers[$header_key] = ["Video $key Date","Video $key Feedback Timing","Video $key Coach Comments #","Video $key Coachee Comments #","Video $key Attachments #","Video $key Session Summary","Video $key Average PL"];
        }
        $html .=
            "
                        <td>".date("M d",strtotime($video['created_date']))."</td>
                        <td>".($video["color_status"] == "ontime_feedback" ? "On time" : "None")."</td>
                        <td>".($video["coach_comment_count"])."</td>
                        <td>".($video["coachee_comment_count"])."</td>
                        <td>".($video["attachment_count"])."</td>
                        <td>".$video["session_summary"]."</td>
                        <td>".$video["average_performance_rating"]."</td>
                        ";
        $new .= '<div class="repeated">
                <div class="inner">'.date("M d",strtotime($video['created_date'])).'</div>
                <div class="inner">'.($video["color_status"] == "ontime_feedback" ? "On time" : "None").'</div>
                <div class="inner">'.($video["coach_comment_count"]).'</div>
                <div class="inner">'.($video["coachee_comment_count"]).'</div>
                <div class="inner">'.($video["attachment_count"]).'</div>
                <div class="inner">'.substr($video["session_summary"],0,20).'</div>
                <div class="inner">'.$video["average_performance_rating"].'</div>
                <div class="clear"></div>
           </div>';
    }
    $html .= "</tr>";
    $new .='
       </div>
     
     <div class="clear"></div>
    </div>';
}
$header_html = "";
foreach ($headers as $header)
{
    foreach ($header as $head)
    {
        $header_html .= "<th>$head</th>";
    }
}
$final_html = "<table><thead><tr><th>Coach Name</th><th>Coachee Name</th><th>Date Range</th><th>Coachee Email</th>".$header_html."</tr></thead><tbody>".$html."</tbody></table>";
$new .='  
</div>
</body>
</html>
';
if($export_type == "pdf")
{
    echo $new;
}
else
{
    echo $final_html;
}
?>