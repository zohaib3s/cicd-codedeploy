<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Goals</title>
    <style type="text/css">
  #table{
    width:100%;
  }
  #table td{
    width: auto;
    overflow: hidden;
    word-wrap: break-word;
  }

    li {

    }

    .break-word {
    word-break: break-all;
    overflow-wrap: break-word;
    word-wrap: break-word;
    }
 
    @media print{  
   @page {size:landscape;}
   html{
    -ms-transform: rotate(90deg);/* IE 9 */
    -webkit-transform: rotate(90deg);/* Chrome, Safari, Opera */
    transform: rotate(90deg);
   }

   table,h1,h2 {position:relative;left:4.5cm}
  }

    </style>
  </head>
    <body>
           <div class="header-container" style="border-bottom: 2px solid #707070; padding-bottom:15px; margin-bottom:20px;">
            <div class="logo-area" style="float:left; width:30%; padding-top:15px;">
               <a target="_blank" href="<?php echo $header_details['logo_url'];?>"><img style="width: 130px;" src="<?php echo config('s3.sibme_base_url')."/img/pdf_logo.png"; ?>"></a>
            </div>

            <div class="detail-area" style="width:70%; float:right; padding-top:15px;">
                    <div class="left-box" style="float:left; width:46%; border-right:2px solid #707070; font-size: 10px;line-height: 22px;">
                        <div><b>Account Name: </b><?php echo !empty($header_details['account_name'])?$header_details['account_name']:'N/A' ?></div>
                        <div><b>Huddle Name: </b><?php echo !empty($header_details['huddle_name'])?$header_details['huddle_name']:'N/A' ?></div>         
                    </div>            
                    <div class="right-box" style="float:right; width:48%; font-size: 10px; line-height: 22px;">
                        <div><b>Exported By: </b><?php echo !empty($header_details['exported_by'])?$header_details['exported_by']:'N/A' ?></div>                        
                        <div><b>Huddle Participants: </b><?php echo !empty($header_details['huddle_participants'])?$header_details['huddle_participants']:'N/A'?> </div>
                    </div>
                    <div class="clr" style="clear:both;"></div>
            </div> 
                <div class="clr" style="clear:both;"></div>   
        </div> 
        <table border="1" width="100%">
        <?php 
		if (isset($assessees) && !empty($assessees)) { 
			foreach ($assessees as $assessee) {
				$assessee = array_values($assessee);

        ?>
        <tr>
            <td>
                <ul>
            <?php
                    foreach ($headings as $key => $heading) {
            ?>
            <li class="break-word"><?php echo $heading; ?>: <?php echo $assessee[$key] ?></li>
            <?php 
                    }
            ?>
                </ul>
            </td>
        </tr>
        <?php
			}
        }
        ?>
        </table>
    </body>
</html>