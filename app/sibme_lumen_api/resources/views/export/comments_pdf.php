<html>
<style>
    body{ font-family:Arial, Helvetica, sans-serif;}
    .container { max-width:1100px; margin:0 auto;}
    .logo-area { float:left; width:30%; padding-top:15px;}
    .detail-area { width:70%; float:right; padding-top:15px;}
    .left-box { float:left; width:44%; border-right:2px solid #707070;}
    .right-box { float:right; width:44%;}
    .clr { clear:both;}
    .header-container{border-bottom: 2px solid #707070; padding-bottom:15px; margin-bottom:20px;}
</style>
    <body style="font-family:Arial, Helvetica, sans-serif;"> 
    <div class="container" style="max-width:1100px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;">
    <?php if(count($header_details) > 0):?>
    <div class="header-container" style="border-bottom: 2px solid #707070; padding-bottom:15px; margin-bottom:20px;">
            <div class="logo-area" style="float:left; width:30%; padding-top:15px;">
               <a target="_blank" href="<?php echo $header_details['logo_url'];?>"><img style="width: 130px;" src="<?php echo config('s3.sibme_base_url')."/img/pdf_logo.png"; ?>"></a>
            </div>

            <div class="detail-area" style="width:70%; float:right; padding-top:15px;">
                    <div class="left-box" style="float:left; width:46%; border-right:2px solid #707070; font-size: 10px;line-height: 22px;">
                        <div><b>Account Name: </b><?php echo !empty($header_details['account_name'])?$header_details['account_name']:'N/A' ?></div>
                        <?php if($header_details['folder_type'] == 1):?>
                        <div><b>Huddle Name: </b><?php echo !empty($header_details['huddle_name'])?$header_details['huddle_name']:'N/A' ?></div>
                        <?php endif;?>
                        <div><b>Video Name: </b><?php echo !empty($header_details['video_title'])?$header_details['video_title']:'N/A'?></div>
                        <div><b>Upload Date: </b><?php echo !empty($header_details['uploaded_date'])?date('m/d/Y h:iA',strtotime($header_details['uploaded_date'])):'N/A' ?></div>            
                    </div>            
                    <div class="right-box" style="float:right; width:48%; font-size: 10px; line-height: 22px;">
                        <div style="word-break: break-all;"><b>URL: </b> <?php echo !empty($header_details['video_url'])? '<a href="'.$header_details['video_url'].'">'.substr($header_details['video_url'],0,30).'...'.'</a>': 'N/A';?></div>
                        <div><b>Exported By: </b><?php echo !empty($header_details['exported_by'])?$header_details['exported_by']:'N/A' ?></div>                        
                        <div><b>Huddle Participants: </b> <?php echo !empty($header_details['huddle_participants'])?$header_details['huddle_participants']:'N/A'?></div>
                        <div><b>Framework: </b> <?php echo !empty($header_details['framework_title'])?$header_details['framework_title']:'' ?></div> 
                    </div>
                    <div class="clr" style="clear:both;"></div>
            </div> 
                <div class="clr" style="clear:both;"></div>   
        </div> 
    <?php endif;?>
        <ul class="comments" id="normal_comment">       
            <?php

             if (isset($videoComments) && $videoComments != '') { ?>
                <?php
                $timeS = '';
                foreach ($videoComments as $vidComments){
                    ?>
                    <?php
                    $commentDate = $vidComments['created_date'];
                    ?>

                    <li class="comment thread" <?php if(!empty($vidComments['parent_comment_id'])){?>style="margin-left:50px;list-style-type: square;"<?php } ?>>
                        <?php echo ($vidComments['time'] == 0 ? "" : formatSeconds($vidComments['time'])) . ($vidComments['end_time'] == 0 ? "" : " - ".formatSeconds($vidComments['end_time'])); ?>
                        <strong><?php echo $vidComments['first_name'] . " " . $vidComments['last_name']; ?></strong>&nbsp;<i><?= $translations['posted_on_export'] ?> &nbsp;<?php echo date('m/d/Y', strtotime($commentDate)); ?></i><br/>                    
                        <p style="bottom: 0in;">
                            <?php echo nl2br($vidComments['comment']); ?>
                        </p>
                        <!--Tags Start-->
                        <?php  if (!empty($vidComments['tags'])) { ?>                
                            <p style="word-wrap: break-word;bottom: 0in;">
                                <?php
                                $tags = '';
                                $get_value='';
                                foreach ($vidComments['tags'] as $tag) {                            
                                    if ($tag['ref_type'] == '0') {
                                       $tags=$tag['tag_code'] . '-' . $tag['tag_title'];
                                    } else {
                                        $tags=empty($tag['tag_title']) ? $tag['tag_title'] : $tag['tag_title'];
                                    }
                                    $get_value.="#".$tags.", ";
                                }
                                echo trim($get_value, ", ");                                                
                                ?>
                            </p>
                        <?php } ?>
                        <!--Tags End-->
                        <!--Attachments Start-->
                        <?php  if (isset($vidComments['attachments'])) { ?>
                            <p style="word-wrap: break-word;bottom: 0;">
                                <?php echo $vidComments['attachments']; ?>
                            </p>
                        <?php } ?>
                        <!--Attachments End-->
                        <!--Replies Start-->
                        <?php if(isset($vidComments['replies'])) { ?>
                        <ul class="comments" id="normal_comment"> 
                            <?php    foreach($vidComments['replies'] as $reply){?>
                                <?php $commentDate = $reply['created_date']; ?>
                                <li class="comment thread">
                                     <?php echo ($reply['time'] == 0 ? "" : formatSeconds($reply['time'])) . ($reply['end_time'] == 0 ? "" :" - ". formatSeconds($reply['end_time'])) ; ?>
                                    <strong><?php echo $reply['first_name'] . " " . $reply['last_name']; ?></strong>&nbsp;<i>Posted on: &nbsp;<?php echo date('m/d/Y', strtotime($commentDate)); ?></i><br/>                    
                                    <p style="bottom: 0in;">
                                        <?php echo nl2br($reply['comment']); ?>
                                    </p>
                                    <!--Repiles of Reply Start-->
                                    <?php if(isset($reply['replyofreply'])) { ?>
                                    <ul class="comments" id="normal_comment"> 
                                        <?php    foreach($reply['replyofreply'] as $replyofreply){?>
                                            <?php $commentDate = $replyofreply['created_date']; ?>
                                            <li class="comment thread">
                                                 <?php echo ($replyofreply['time'] == 0 ? "" : formatSeconds($replyofreply['time']))  .($replyofreply['end_time'] == 0 ? "" :" - ". formatSeconds($replyofreply['end_time'])); ?>
                                                <strong><?php echo $replyofreply['first_name'] . " " . $replyofreply['last_name']; ?></strong>&nbsp;<i><?= $translations['posted_on_export'] ?> &nbsp;<?php echo date('m/d/Y', strtotime($commentDate)); ?></i><br/>                    
                                                <p style="bottom: 0in;">
                                                    <?php echo nl2br($replyofreply['comment']); ?>
                                                </p>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                    <?php } ?>
                                    <!--Repiles of Reply End-->
                                </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                        <!--Replies End-->

                    </li>
                    <?php } ?>
                 <?php } ?>
        </ul>
    </div>        
    </body>
</html>