<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Export PDF</title>
    <style type="text/css">
  #table{
    width:100%;
  }
  #table td{
    width: auto;
    overflow: hidden;
    word-wrap: break-word;
  }

    li {

    }

    .break-word {
    word-break: break-all;
    overflow-wrap: break-word;
    word-wrap: break-word;
    }
 
    @media print{  
   @page {size:landscape;}
   html{
    -ms-transform: rotate(90deg);/* IE 9 */
    -webkit-transform: rotate(90deg);/* Chrome, Safari, Opera */
    transform: rotate(90deg);
   }

   table,h1,h2 {position:relative;left:4.5cm}
  }

    </style>
  </head>
    <body>
        <table border="1" width="100%">
        <?php 
		if (isset($detail_views) && !empty($detail_views)) { 
			foreach ($detail_views as $detail_view) {
				
        ?>
        <tr>
            <td>
                <ul>
            <?php
                    foreach ($headings as $key => $heading) {
            ?>
            <li class="break-word"><?php echo $heading; ?>: <?php echo $detail_view[$key] ?></li>
            <?php 
                    }
            ?>
                </ul>
            </td>
        </tr>
        <?php
			}
        }
        ?>
        </table>
    </body>
</html>