<html>
<body style="background-color: #fff;font-family: arial;">
<style type="text/css">

    p {
        font-size:12px;
        margin:0;
    }

    br {
        display: none !important;
    }
    .attachments
    {
        background:#ddd;
        padding:2px;
    }
    .thumb_img {
        border-radius: 50px;
        width: 50px;
        height: 50px;

    }
</style>
<?php
$domain = "https://s3.amazonaws.com/sibme.com/static/users";
$discussion = (object)$discussion;
$default_avatar = config('s3.sibme_base_url')."img/home/photo-default.png";
$discussion_creator_avatar = !empty($discussion->image) ? $domain."/".$discussion->created_by."/".$discussion->image : $default_avatar;
?>
<table border="0" cellpadding="0" cellspacing="0"  width="600px" >
    <tr width="600px">
        <td width="600px"><h3><?php echo $discussion->title?></h3></td></tr>
    <tr  width="600px">
        <td width="600px"><img class="thumb_img" src="<?php echo $discussion_creator_avatar?>"/>&nbsp;&nbsp;&nbsp;&nbsp;<b><?php echo $discussion->first_name." ". $discussion->last_name?> </b></td></tr>

    <tr width="600px">
        <td width="600px"><?php echo ($discussion->comment)?>
        </td>
    </tr>
    
    <tr width="600px">
        <td width="600px"> 
        <span class="attachments">
            <?php foreach ($discussionAttachments as $attachment){?> <?php echo $attachment["original_file_name"]?>  <?php } ?>
        </span>
        </td>
    </tr>
    
    <tr><td style="border-top:1px solid black;"><br><br></td></tr>
    

    <?php foreach($discussionReplies as $discussion) { ?>
    <?php
        $discussion_reply_avatar = !empty($discussion->image) ? $domain."/".$discussion->created_by."/".$discussion->image : $default_avatar;
    ?>
        <tr width="600px">
            <td style="margin-left:50px;"><b>
                <img class="thumb_img" src="<?php echo $discussion_reply_avatar?>"/>&nbsp;&nbsp;&nbsp;&nbsp; <?php echo $discussion->first_name ." ". $discussion->last_name?></b> 
            </td>
        </tr>
        <tr width="600px">
            <td style="margin:0;margin-left:50px"><?php echo str_replace("</p>","</span>",str_replace("<p>","<span>",$discussion->comment)) ?></td>
        </tr>
        <!-- <tr><td style="border-bottom:1px solid black;"></td></tr> -->

        <?php if(count($discussion->attachments)){ ?> <tr>
            <td style="margin-left:50px;">
                <span class="attachments"><?php foreach ($discussion->attachments as $attachment){?>
                        <?php echo $attachment["original_file_name"]?> <?php } ?>
           </span> </td>
            </tr><?php } ?>
        <?php foreach($discussion->replies as $reply) { ?>
            <?php
                $replier_avatar = !empty($reply->image) ? $domain."/".$reply->created_by."/".$reply->image : $default_avatar;
            ?>

            <tr width="600px">
                <td style="margin-left:120px;"><b>
                    <img class="thumb_img" src="<?php echo $replier_avatar?>"/>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $reply->first_name ." ". $reply->last_name?></b> </td>
            </tr>

            <tr width="600px">
                <td style="margin:0;margin-left:120px;">
                    <?php echo str_replace("</p>","</span>",str_replace("<p>","<span>",$reply->comment)) ?></td></tr>
            <?php if(count($reply->attachments)){ ?> <tr>
                <td style="margin-left:120px;" class="attachments">
                  <span class="attachments">  <?php foreach ($reply->attachments as $attachment){?>
                          <?php echo $attachment["original_file_name"]?>
                      <?php } ?></span>

                </td>
                </tr><?php } ?>

            <!-- <tr><td style="border-bottom:1px solid black;"></td></tr> -->
            <tr><td style="margin-bottom:0px"></td>
            </tr>
        <?php } ?>

    <?php } ?>
</table>

</body>
</html>
