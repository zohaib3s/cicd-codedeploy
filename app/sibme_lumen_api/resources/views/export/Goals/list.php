<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Goals</title>
    <style type="text/css">
  #table{
    width:100%;
  }
  #table td{
    width: auto;
    overflow: hidden;
    word-wrap: break-word;
  }

    li {

    }

    .break-word {
    word-break: break-all;
    overflow-wrap: break-word;
    word-wrap: break-word;
    }
 
    @media print{  
   @page {size:landscape;}
   html{
    -ms-transform: rotate(90deg);/* IE 9 */
    -webkit-transform: rotate(90deg);/* Chrome, Safari, Opera */
    transform: rotate(90deg);
   }

   table,h1,h2 {position:relative;left:4.5cm}
  }

    </style>
  </head>
    <body>
      
    <div class="container" style="max-width:1100px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;">
        <div class="header-container" style="border-bottom: 2px solid #707070; padding-bottom:15px; margin-bottom:20px;">
            <div class="logo-area" style="float:left; width:30%; padding-top:15px;">
                <img style="width: 130px;" src="<?php echo config('s3.sibme_base_url')."/img/pdf_logo.png"; ?>">
            </div>

            <div class="detail-area" style="width:70%; float:right; padding-top:15px;">
                    <div class="left-box" style="float:left; width:100%; font-size: 10px;line-height: 22px; text-align: right;">
                        <div><b>Account Name: </b><?php echo !empty($header_data['account_name'])?$header_data['account_name']:'N/A' ?></div>
                        <div><b>Report Name: </b><?php echo !empty($header_data['report_name'])?$header_data['report_name']:'N/A' ?></div>
                        <div><b>Exported By: </b><?php echo !empty($header_data['exported_by'])?$header_data['exported_by']:'N/A' ?></div>
                        <div><b>Exported Date: </b><?php echo !empty($header_data['export_date'])?date('m/d/Y h:iA',strtotime($header_data['export_date'])):'N/A' ?></div>            
                    </div>           
                    
                    <div class="clr" style="clear:both;"></div>
            </div> 
                <div class="clr" style="clear:both;"></div>   
        </div>
        <table border="0" width="100%" style="border-collapse: collapse;">
        <?php 
        foreach ($goals as $goal) {
          if($goal){
            $goal = array_values($goal);
          }
           
        ?>
        <tr>
            <td>
                <ul>
            <?php
                    foreach ($headings as $key => $heading) {
            ?>
            <li class="break-word"><?php echo $heading; ?>: <?php echo $goal[$key]; ?></li>
            <?php 
                    }
            ?>
                </ul>
            </td>
        </tr>
        <?php
        }
        ?>
        </table>
        </div>
    </body>
</html>