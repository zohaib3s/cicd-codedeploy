<html>
    <body>
    <div class="container" style="max-width:1100px; margin:0 auto; font-family:Arial, Helvetica, sans-serif;">
    <?php if(count($header_details) > 0):?>
    <div class="header-container" style="border-bottom: 2px solid #707070; padding-bottom:15px; margin-bottom:20px;">
            <div class="logo-area" style="float:left; width:30%; padding-top:15px;">
               <a target="_blank" href="<?php echo $header_details['logo_url'];?>"><img style="width: 130px;" src="<?php echo config('s3.sibme_base_url')."/img/pdf_logo.png"; ?>"></a>
            </div>
            <div class="detail-area" style="width:70%; float:right; padding-top:15px;">
                    <div class="left-box" style="float:left; width:46%; border-right:2px solid #707070; font-size: 10px;line-height: 22px;">
                        <div><b>Account Name: </b><?php echo !empty($header_details['account_name'])?$header_details['account_name']:'N/A' ?></div>
                        <?php if($header_details['folder_type'] == 1):?>
                        <div><b>Huddle Name: </b><?php echo !empty($header_details['huddle_name'])?$header_details['huddle_name']:'N/A' ?></div>
                        <?php endif;?>
                        <div><b>Video Name: </b><?php echo !empty($header_details['video_title'])?$header_details['video_title']:'N/A'?></div>
                        <div><b>Upload Date: </b><?php echo !empty($header_details['uploaded_date'])?date('m/d/Y h:iA',strtotime($header_details['uploaded_date'])):'N/A' ?></div>            
                    </div>            
                    <div class="right-box" style="float:right; width:48%; font-size: 10px; line-height: 22px;">
                        <div style="word-break: break-all;"><b>URL: </b> <?php echo !empty($header_details['video_url'])? '<a href="'.$header_details['video_url'].'">'.substr($header_details['video_url'],0,30).'...'.'</a>': 'N/A';?></div>
                        <div><b>Exported By: </b><?php echo !empty($header_details['exported_by'])?$header_details['exported_by']:'N/A' ?></div>                        
                        <div><b>Huddle Participants: </b> <?php echo !empty($header_details['huddle_participants'])?$header_details['huddle_participants']:'N/A'?></div>
                        <div><b>Framework: </b> <?php echo !empty($header_details['framework_title'])?$header_details['framework_title']:'' ?></div> 
                    </div>
                    <div class="clr" style="clear:both;"></div>
            </div> 
                <div class="clr" style="clear:both;"></div>   
        </div> 
    <?php endif;?>
        <ul class="comments" id="normal_comment">   
            <?php
             if (isset($subtitles) && $subtitles != '') { ?>
                <?php
                foreach ($subtitles as $subtitle){
                    ?>
                  
              <li class="comment thread" style="margin-left:50px;list-style-type: square;">
                  <strong><?php echo $subtitle ['time_range'];?></strong>&nbsp;<i><br/>                    
                        <p style="bottom: 0in;">
                            <?php echo nl2br($subtitle['subtitles']); ?>
                        </p>
                       
                        <?php } ?>
                    </li>
                    <?php } ?>
        </ul>
    </div>
    </body>
</html>