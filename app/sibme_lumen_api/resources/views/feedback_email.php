<?php
$sibme_base_url = config('s3.sibme_base_url');

$profile_image = isset($data['image']) ? $data['image'] : '';

if ($profile_image != '') {
    //$profile_image = $this->webroot . "img/users/$profile_image";
     $profile_image = "https://s3.amazonaws.com/sibme.com/static/users/".$data['user_id']."/".$data['image'];
} else {
    //$profile_image = $this->webroot . "img/profile.jpg";
    $profile_image = config('s3.sibme_base_url')."img/home/photo-default.png";
}
?>
<table>
    <tbody>
        <tr>
            <td style="width: 100px;">
                <p style="margin-bottom: 123px;">
                    <img src="<?php echo 'http://staging.sibme.com/'. $profile_image; ?>" alt="img" height="75px" width="75px"/>
                </p>
            </td>  
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td style="padding-top: 0px; padding-bottom: 10px;">
                            <?php if ($htype == 2) : ?>
                            <strong><?php echo $data['first_name'] . " " . $data['last_name']; ?> published comments to the Coaching Huddle: <?php echo $data['huddle_name']; ?></strong>
                            <?php elseif($htype == 3) : ?>
                             <strong><?php echo $data['first_name'] . " " . $data['last_name']; ?>  published feedback for your review on the following video: <?php echo $data['video_title']; ?></strong>
                            <?php else : ?>
                                <strong><?php echo $data['first_name'] . " " . $data['last_name']; ?> added a video to the Collaboration Huddle: <?php echo $data['huddle_name']; ?></strong>
                            <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px;"> 
                                <?php if ($htype == 2) : ?>
                                <p><strong>Click the link below to view the comments.</strong></p>
                                <?php else: ?>
                                <p><strong>Click the link below to view the feedback.</strong></p>
                                <?php endif; ?>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px;">
                                <a href="<?php echo $sibme_base_url . $data['video_link']; ?>">
                                    <?php if ($htype == 2) : ?>
                                    <span>Click to view the comments.</span>
                                    <?php else: ?>
                                    <span>Click to view the feedback.</span>
                                    <?php endif; ?>
                                    
                                </a>
                            </td>
                        </tr>
                        <?php if($htype != 3) : ?>
                        <tr>
                            <td>
                                <p style="margin-left: 15px;">
                                    People participating in Huddle </p>
                                <p style="margin-left: 15px;">
                                    Coach: <?php
                                    if (isset($data['coach_details']) && count($data['coach_details']) > 0) {
                                        $user_counter = 0;
                                        foreach ($data['coach_details'] as $row) {
                                            if ($user_counter > 0)
                                                echo ", ";
                                            echo $participated_user = $row['first_name'] . " " . $row['last_name'];
                                            $user_counter +=1;
                                        }
                                    }
                                    ?>
                                </p>
                                <p style="margin-left: 15px;">
                                    Coachee: <?php echo $data['huddle_name']; ?> 
                                </p>
                            </td>
                        </tr>  
                        <?php endif; ?>
                      
                        <tr>
                            <td>
                                Delivered by Sibme <a href='www.sibme.com' target="_blank">www.sibme.com</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>  
        </tr>
        <tr>
            <td>&#160;</td>
            <td><a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $user_id;?>/7"> Unsubscribe Now  </a></td>
        </tr>
    </tbody>
</table>