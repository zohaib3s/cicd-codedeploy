
<?php
$sibme_base_url = config('s3.sibme_base_url');
$users = $user_current_account;

$profile_image = isset($users['User']['image']) ? $users['User']['image'] : '';

if ($profile_image != '') {
    //$profile_image = $this->webroot . "img/users/$profile_image";
    $profile_image = "https://s3.amazonaws.com/sibme.com/static/users/" . $users['User']['id'] . "/" . $users['User']['image'];
} else {
    //$profile_image = $this->webroot . "img/profile.jpg";
    $profile_image = config('s3.sibme_base_url') . "img/home/photo-default.png";
}


if ($data['huddle_type'] == '1') {
    ?>
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title><?php echo $site_title; ?> - Collaboration Huddle Invitation</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            <style>
                img { max-width:100%;}
            </style>
        </head>
        <body style="margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, 'sans-serif';">
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                            <tr>
                                <td style="text-align:center; padding:20px 0;">
                                    <img src="<?php echo $sibme_base_url; ?>img/logo-dark.png" width="180" height="87"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; border-bottom:1px solid #ddd;">
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; font-size:16px; padding:20px 0;">
                                    <b style="font-size:20px;"><?php echo $data['first_name'] . " " . $data['last_name'] ?></b><br />
                                    invited you to participate in a collaboration Huddle. <br />
                                    <b><?php echo $data['name']; ?></b>
                                </td>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; padding:20px 0;">
                                    <a href="<?php echo $sibme_base_url; ?>/Huddles/view/<?php echo $data['account_folder_id']; ?>"> <img src="<?php echo $sibme_base_url; ?>img/view_partic2.png" /></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; padding:20px 0;">
                                    If you have questions about this huddle contact<br /> <?php echo $users['User']['first_name'] . " " . $users['User']['last_name'] ?> at <a href="mailto:<?php echo $users['User']['email']; ?>"><?php echo $users['User']['email']; ?></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 15px; text-align: center; color: #212121; padding-top:25px;">
                                    Delivered by Sibme <a href='www.sibme.com' target="_blank">www.sibme.com</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 15px; text-align: center; color: #212121; padding:20px 0;">
                                    <a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $user_id; ?>/4">Unsubscribe Now</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
    </html>
<?php } elseif ($data['huddle_type'] == '3') {
    ?>
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title><?php echo $site_title; ?> - Assessment Huddle Invitation</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            <style>
                img { max-width:100%;}
            </style>
        </head>
        <body style="margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, 'sans-serif';">
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                            <tr>
                                <td style="text-align:center; padding:20px 0;">
                                    <img src="<?php echo $sibme_base_url; ?>img/logo-dark.png" width="180" height="87"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; border-bottom:1px solid #ddd;">
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; font-size:16px; padding:20px 0;">
                                    <b style="font-size:20px;"><?php echo $data['first_name'] . " " . $data['last_name'] ?></b><br />
                                    invited you to participate in an assessment huddle. <br />
                                    <b><?php echo $data['name']; ?></b><br/>
                                    Due: <?php echo $data['submission_time']; ?>
                                </td>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; padding:20px 0;">
                                    <a href="<?php echo $sibme_base_url; ?>/Huddles/view/<?php echo $data['account_folder_id']; ?>">  <img src="<?php echo $sibme_base_url; ?>img/view_partic2.png" /></a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; padding:20px 0;">
                                    If you have questions about this huddle contact<br /> <?php echo $users['User']['first_name'] . " " . $users['User']['last_name'] ?> at <a href="mailto:<?php echo $users['User']['email']; ?>"><?php echo $users['User']['email']; ?></a>
                                </td>
                            </tr>

                            <tr>
                                <td style="font-size: 15px; text-align: center; color: #212121; padding:20px 0;">
                                    <a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $user_id; ?>/4">Unsubscribe Now</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 15px; text-align: center; color: #212121; padding-top:25px;">
                                    <a href="www.sibme.com" target="_blank" style="color:#616161; text-decoration:none;">Powered by Sibme</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
    </html>
<?php } else { ?>
    <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title><?php echo $site_title; ?> - Coaching Huddle Invitation</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            <style>
                img { max-width:100%;}
            </style>
        </head>
        <body style="margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, 'sans-serif';">
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                            <tr>
                                <td style="text-align:center; padding:20px 0;">
                                    <img src="<?php echo $sibme_base_url; ?>img/logo-dark.png" width="180" height="87"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; border-bottom:1px solid #ddd;">
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; font-size:16px; padding:20px 0;">
                                    <b style="font-size:20px;"><?php echo $data['first_name'] . " " . $data['last_name'] ?></b><br />
                                    invited you to participate in a coaching Huddle. <br />
                                    <b><?php echo $data['name']; ?></b>
                                </td>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; padding:20px 0;">
                                    <a href="<?php echo $sibme_base_url; ?>/Huddles/view/<?php echo $data['account_folder_id']; ?>"> <img src="<?php echo $sibme_base_url; ?>img/view_partic2.png" /> </a>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:center; padding:20px 0;">
                                    If you have questions about this huddle contact<br /> <?php echo $users['User']['first_name'] . " " . $users['User']['last_name'] ?> at <a href="mailto:<?php echo $users['User']['email']; ?>"><?php echo $users['User']['email']; ?></a>
                                </td>
                            </tr>

                            <tr>
                                <td style="font-size: 15px; text-align: center; color: #212121; padding:20px 0;">
                                    <a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $user_id; ?>/4">Unsubscribe Now</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size: 15px; text-align: center; color: #212121; padding-top:25px;">
                                    <a href="www.sibme.com" target="_blank" style="color:#616161; text-decoration:none;">Powered by Sibme</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
    </html>

<?php } ?>
