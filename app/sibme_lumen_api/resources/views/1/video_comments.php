<?php
$sibme_base_url = config('s3.sibme_base_url');

$profile_image = isset($data['image']) ? $data['image'] : '';

if ($profile_image != '') {
    //$profile_image = $this->webroot . "img/users/$profile_image";
    $profile_image = "https://s3.amazonaws.com/sibme.com/static/users/" . $data['user_id'] . "/" . $data['image'];
} else {
    //$profile_image = $this->webroot . "img/profile.jpg";
    $profile_image = config('s3.sibme_base_url') . "img/home/photo-default.png";
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <style>
        img { max-width:100%;}
    </style>

    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style=" font-family:Arial, Helvetica, sans-serif;">

            <tr>

                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">

                        <tr>
                            <td style="text-align:center; padding:30px 0;">
                                <img src="<?php echo $sibme_base_url ?>img/logo-dark.png" alt="<?php echo $site_title ?>" width="120"/>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; border-bottom:1px solid #ddd;">

                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; font-size:16px; padding:30px 0; line-height:26px; font-family:Arial, Helvetica, sans-serif;">



                                <b><?php echo $data['first_name'] . " " . $data['last_name']; ?></b> <br />
                                added a comment in <br />
                                <b><?php echo $data['huddle_name'] ?></b>

                            </td>

                        </tr>

                        <tr>
                            <td style="text-align:center; padding:10px 0 30px 0;">
                                <a href="<?php echo $sibme_base_url . $data['video_link']; ?>" title="Click to view Comment"> <img src="<?php echo $sibme_base_url ?>img/sibme-comment-sdded.png" width="330" height="50" alt="Click to view Comment " /></a>
                            </td>
                        </tr>



                        <tr>
                            <td style="font-size: 14px; text-align: left; color: #212121; padding-top:25px; font-family:Arial, Helvetica, sans-serif; text-align:center;">
                                <a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $user_id; ?>/6" style="color:#616161; text-decoration:underline;">Unsubscribe Now</a> <br /><br />
                                <a href="www.sibme.com" target="_blank" style="color:#616161; text-decoration:none;">Powered by Sibme</a>
                            </td>
                        </tr>




                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>