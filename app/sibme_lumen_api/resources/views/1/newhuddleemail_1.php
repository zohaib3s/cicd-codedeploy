
<?php
$sibme_base_url = config('s3.sibme_base_url');
$users = $user_current_account;

$profile_image = isset($users['User']['image']) ? $users['User']['image'] : '';

if ($profile_image != '') {
    //$profile_image = $this->webroot . "img/users/$profile_image";
    $profile_image = "https://s3.amazonaws.com/sibme.com/static/users/".$users['User']['id']."/".$users['User']['image'];
} else {
    //$profile_image = $this->webroot . "img/profile.jpg";
    $profile_image = config('s3.sibme_base_url')."img/home/photo-default.png";
}


if($data['huddle_type']=='1'){
?>
<html>
    <body>
        <table width="100%">
            <tbody>
                <tr>
                    <td style="width: 100px;">
                        <p style="margin-bottom: 270px;">
                              <img src="<?php echo 'http://staging.sibme.com/' . $profile_image; ?>" alt="img" height="75px" width="75px"/>
                        </p>
                    </td>  
                    <td>
                        <table  width="100%" cellspacing="5">
                            <tbody>
                                <tr>
                                    <td><?php echo $data['first_name'] . " " . $data['last_name'] ?> invited you to a Collaboration Huddle.</td>
                                </tr>
                                <tr>
                                    <td style="padding-bottom: 10px;">
                                        <h2 style="color:#3d9bc2"><?php echo $data['name']; ?></h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Sibme is an awesome video coaching and collaboration platform we use to improve teaching and learning.</td>
                                </tr>                                
                                <tr>
                                    <td>
                                        <h2><a href="<?php echo $sibme_base_url; ?>/Huddles/view/<?php echo $data['account_folder_id']; ?>">Click here to get started!</a></h2> 
                                    </td>
                                </tr>
                                <?php 
                                    if (isset($message) && !empty($message)) :
                                ?>
                                <tr>
                                    <td>Read <?php echo $data['first_name'] . " " . $data['last_name'] ?>'s message below:</td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo $message; ?>
                                    </td>
                                </tr>
                                <?php 
                                    endif;
                                ?>
                                <tr>
                                    <td>
                                        Still  have questions? 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Contact <?php echo $users['User']['first_name'] . " " . $users['User']['last_name'] ?> at <a href="mailto:<?php echo $users['User']['email']; ?>"><?php echo $users['User']['email']; ?></a>
                                    </td>
                                </tr>
                                <?php if (isset($data['participated_user']) && count($data['participated_user']) > 0): ?>
                                    <tr>
                                        <td>
                                            People participating in Huddle include:
                                            <?php
                                            if (isset($data['participated_user']) && count($data['participated_user']) > 0) {
                                                $user_counter = 0;
                                                foreach ($data['participated_user'] as $row) {
                                                    if ($user_counter > 0)
                                                        echo ", ";
                                                    echo $participated_user = $row['first_name'] . " " . $row['last_name'];
                                                    $user_counter +=1;
                                                }
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endif; ?>

                                <tr>
                                    <td>
                                        Delivered by Sibme <a href='www.sibme.com' target="_blank">www.sibme.com</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>  
                </tr>
                <tr>
                    <td>&#160;</td>
                    <td>
                        <a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $user_id;?>/4"> Unsubscribe Now  </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
<?php }elseif($data['huddle_type']=='3'){
?>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <title>Sibme</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <style>
         img { max-width:100%;}
      </style>
   </head>
   <body style="margin: 0; padding: 0;">
      <table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family: Segoe, 'Segoe UI', 'DejaVu Sans', 'Trebuchet MS', Verdana, 'sans-serif';">
         <tr>
            <td>
               <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">
                  <tr>
                     <td style="text-align:center; padding:20px 0;">
                        <img src="<?php echo $sibme_base_url; ?>img/logo-dark.png" width="180" height="87"/>
                     </td>
                  </tr>
                  <tr>
                     <td style="text-align:center; border-bottom:1px solid #ddd;">
                     </td>
                  </tr>
                  <tr>
                     <td style="text-align:center; font-size:16px; padding:20px 0;">
                        <b style="font-size:20px;"><?php echo $data['first_name'] . " " . $data['last_name'] ?></b><br />
                        invited you to participate in an assessment huddle. <br />
                        <b><?php echo $data['name']; ?></b>
                     </td>
                     </td>
                  </tr>
                  <tr>
                     <td style="text-align:center; padding:20px 0;">
                        <img src="<?php echo $sibme_base_url; ?>img/view_partic2.png" />
                     </td>
                  </tr>
                  <tr>
                     <td style="text-align:center; padding:20px 0;">
                        If you have questions about this huddle contact<br /> <?php echo $users['User']['first_name'] . " " . $users['User']['last_name'] ?> at <a href="mailto:<?php echo $users['User']['email']; ?>"><?php echo $users['User']['email']; ?></a>
                     </td>
                  </tr>
                  <tr>
                     <td style="font-size: 15px; text-align: center; color: #212121; padding-top:25px;">
                        Delivered by Sibme <a href='www.sibme.com' target="_blank">www.sibme.com</a>
                     </td>
                  </tr>
                  <tr>
                     <td style="font-size: 15px; text-align: center; color: #212121; padding:20px 0;">
                        <a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $user_id;?>/4">Unsubscribe Now</a>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </body>
</html>
<?php }else{?>

<html>
    <body>
        <table  width="100%">
            <tbody>
                <tr>
                    <td style="width: 100px;">
                        <p style="margin-bottom: 270px;">
                               <img src="<?php echo 'http://staging.sibme.com/' . $profile_image; ?>" alt="img" height="75px" width="75px"/>
                        </p>
                    </td>  
                    <td>
                        <table  width="100%" cellspacing="5">
                            <tbody>
                                
                                <tr>
                                    <td><?php echo $data['first_name'] . " " . $data['last_name'] ?> invited you to participate in a Coaching Huddle.</td>
                                </tr>
                                <tr>
                                    <td style="padding-bottom: 10px;">
                                        <h2 style="color:#3d9bc2"><?php echo $data['name']; ?></h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Sibme is an awesome video coaching and collaboration platform we use to improve teaching and learning.</td>
                                </tr>                                
                                <tr>
                                    <td>
                                        <h2><a href="<?php echo $sibme_base_url; ?>/Huddles/view/<?php echo $data['account_folder_id']; ?>">Click here to get started!</a></h2> 
                                    </td>
                                </tr>
                                <?php 
                                    if (isset($message) && !empty($message)) :
                                ?>
                                <tr>
                                    <td>Read <?php echo $data['first_name'] . " " . $data['last_name'] ?>'s message below:</td>
                                </tr>
                                <tr>
                                    <td>
                                        <?php echo $message; ?>
                                    </td>
                                </tr>
                                <?php 
                                    endif;
                                ?>
                                <tr>
                                    <td>
                                        Still  have questions? 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Contact <?php echo $users['User']['first_name'] . " " . $users['User']['last_name'] ?> at <a href="mailto:<?php echo $users['User']['email']; ?>"><?php echo $users['User']['email']; ?></a>
                                    </td>
                                </tr>
                                 
                                <?php if (isset($data['participated_user']) && count($data['participated_user']) > 0): ?>
                                    <tr>
                                        <td>
                                            People participating in Coaching Huddle
                                           <?php
                                            if (isset($data['participated_user']) && count($data['participated_user']) > 0) {
                                                $user_counter = 0;
                                                $is_ment="";
                                                $is_cochee="";
                                                foreach ($data['participated_user'] as $row) {
                                                    if($row['is_mentee']=='1'){
                                                     $is_ment = $row['first_name'] . " " . $row['last_name'].",";
                                                    }
                                                    elseif($row['is_coach']=='1'){
                                                        $is_cochee = $row['first_name'] . " " . $row['last_name'].",";
                                                    }
                                                    $user_counter +=1;
                                                }                                                
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Coach: &#160;  <?php echo rtrim($is_cochee,',');?> 
                                            
                                        </td>
                                        
                                    </tr>
                                     <tr>
                                        <td>
                                            Coachee: &#160; <?php echo rtrim($is_ment,',');?>
                                            
                                        </td>
                                        
                                    </tr>
                                <?php endif; ?>
                                    
                                   

                                <tr>
                                    <td>
                                        Delivered by Sibme <a href='www.sibme.com' target="_blank">www.sibme.com</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>  
                </tr>
                <tr>
                    <td>&#160;</td>
                    <td>
                        <a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $user_id;?>/4"> Unsubscribe Now  </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
<?php } ?>
