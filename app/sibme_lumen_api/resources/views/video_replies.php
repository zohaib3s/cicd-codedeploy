<?php
$sibme_base_url = config('s3.sibme_base_url');

$profile_image = isset($data['image']) ? $data['image'] : '';

if ($profile_image != '') {
    //$profile_image = $this->webroot . "img/users/$profile_image";
    $profile_image = "https://s3.amazonaws.com/sibme.com/static/users/".$data['user_id']."/".$data['image'];
} else {
    //$profile_image = $this->webroot . "img/profile.jpg";
    $profile_image = config('s3.sibme_base_url')."img/home/photo-default.png";
}
?>
<table>
    <tbody>
        <tr>
            <td style="width: 100px;">
                <p style="margin-bottom: 123px;">
                    <img src="<?php echo 'http://staging.sibme.com/'. $profile_image; ?>" alt="img" height="75px" width="75px"/>
                </p>
            </td>  
            <td>
                <table>
                    <tbody>
                        <tr>
                            <td style="padding-top: 0px; padding-bottom: 10px;">
                                <strong><?php echo $data['first_name'] . " " . $data['last_name']; ?> posted a reply to your video comment</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px;"> 
                                <p>Click the link below to view the reply.</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-bottom: 10px;">
                                <a href="<?php echo $sibme_base_url . $data['video_link']; ?>">
                                    <span>Click here to view the comment.</span>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <p style="margin-left: 15px;">
                                    Other people participating in Huddle include: 
                                    <?php
                                    if (isset($data['participating_users']) && count($data['participating_users']) > 0) {
                                        $user_counter = 0;
                                        foreach ($data['participating_users'] as $row) {
                                            if ($user_counter > 0)
                                                echo ", ";
                                            echo $participated_user = $row['first_name'] . " " . $row['last_name'];
                                            $user_counter +=1;
                                        }
                                    }
                                    ?>
                                </p>
                            </td>
                        </tr>  
                      
                        <tr>
                            <td>
                                Delivered by Sibme <a href='www.sibme.com' target="_blank">www.sibme.com</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>  
        </tr>
        <tr>
            <td>&#160;</td>
            <td><a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $user_id;?>/6"> Unsubscribe Now  </a></td>
        </tr>
    </tbody>
</table>