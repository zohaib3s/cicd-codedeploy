<?php $sibme_base_url = config('s3.sibme_base_url'); ?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <style>
        img { max-width:100%;}
    </style>
    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style=" font-family:Arial, Helvetica, sans-serif;">

            <tr>

                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">

                        <tr>
                            <td style="text-align:center; padding:30px 0;">
                                <img src="<?php echo $sibme_base_url; ?>img/logohmh.png" alt="<?php echo $site_title ?>" width="320" />
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; border-bottom:1px solid #ddd;">

                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; font-size:16px; padding:30px 0; line-height:26px; font-family:Arial, Helvetica, sans-serif;">
                                <?php if ($htype == 2) : ?>
                                    <strong><?php echo $data['first_name'] . " " . $data['last_name']; ?></strong><br/>published comments in <br/><strong><?php echo $data['huddle_name']; ?></strong>
                                <?php elseif ($htype == 3) : ?>
                                    <strong><?php echo $data['first_name'] . " " . $data['last_name']; ?></strong><br/>published comments in <br/><strong><?php echo $data['huddle_name']; ?></strong>
                                <?php else : ?>
                                    <strong><?php echo $data['first_name'] . " " . $data['last_name']; ?></strong><br/>published comments in <br/><strong><?php echo $data['huddle_name']; ?></strong>
                                <?php endif; ?>
                            </td>

                        </tr>

                        <tr>
                            <td style="text-align:center; padding:10px 0 30px 0;">
                                <a href="<?php echo $sibme_base_url . $data['video_link']; ?>" title="Click to view Comment"> <img src="<?php echo $sibme_base_url; ?>img/hmh-commentadded.png" width="330" height="50" alt="Click to view Comment " /></a>
                            </td>
                        </tr>



                        <tr>
                            <td style="font-size: 14px; text-align: left; color: #212121; padding-top:25px; font-family:Arial, Helvetica, sans-serif; text-align:center;">
                                <a href="<?php echo $sibme_base_url; ?>subscription/unsubscirbe_now/<?php echo $user_id; ?>/7" style="color:#616161; text-decoration:underline;">Unsubscribe Now</a> <br /><br />
                                <a href="www.sibme.com" style="color:#616161; text-decoration:none;">Powered by Sibme</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>