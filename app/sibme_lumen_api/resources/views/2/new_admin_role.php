<?php
$sibme_base_url = config('s3.sibme_base_url');
$users = $user_current_account;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title> HMH - Account Invitation</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style>
            img { max-width:100%;}
        </style>

    </head>
    <body style="margin: 0; padding: 0;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" style=" font-family:Arial, Helvetica, sans-serif;">

            <tr>

                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;">

                        <tr>
                            <td style="text-align:center; padding:30px 0;">
                                <img src="<?php echo $sibme_base_url ?>img/logohmh.png"  width="320" height="59"/>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; border-bottom:1px solid #ddd;">

                            </td>
                        </tr>

                        <tr>
                            <td style="text-align:center; font-size:16px; padding:30px 0; line-height:26px; font-family:Arial, Helvetica, sans-serif;">
                                <b><?php echo $users['User']['first_name'] . ' ' . $users['User']['last_name'] ?></b><br />
                                invited you into their HMH account<br />
                                <b><?php echo $users['accounts']['company_name']; ?></b>

                            </td>

                        </tr>

                        <tr>
                            <td style="text-align:center; padding:10px 0 30px 0;">
                                <a href="<?php echo $sibme_base_url . 'Users/activation/' . md5($data['account_id']) . "/" . $data['authentication_token'] . "/" . md5($data['user_id']) . "/" . $account_folder_id ?>" title="Click here to log in"> <img src="<?php echo $sibme_base_url ?>img/loginbthmhn.png" width="330" height="50" alt="Click here to log in" /></a>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size: 14px; text-align: left; color: #212121; padding-top:25px; font-family:Arial, Helvetica, sans-serif; text-align:center;">
                                <a href="#" style="color:#616161; text-decoration:underline;">Unsubscribe Now</a> <br /><br />
                                <a href="www.sibme.com" target="_blank" style="color:#616161; text-decoration:none;">Powered by Sibme</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>