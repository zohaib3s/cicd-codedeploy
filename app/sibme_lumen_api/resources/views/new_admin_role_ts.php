<?php
$sibme_base_url = config('s3.sibme_base_url');
$users = $user_current_account;
?>
<h4>
    <strong>You've been invited to join <?php echo $users['company']; ?>'s Sibme account</strong>
</h4>
<br/>
<?php echo $users['full_name']; ?> invited you to become <?php echo $data['role_title'] ?> in <?php echo $users['company']; ?>'s Sibme Account:
<br/>
<h2 style="color:#3d9bc2;"><?php echo $users['company'] ?></h2>
<p>
    <strong>Sibme is an awesome video coaching and collaboration platform we use to improve teaching and learning.</strong>
</p>
<br/>
<a href="<?php echo $sibme_base_url . 'Users/activation/' . md5($data['account_id']) . "/" . $data['authentication_token'] . "/" . md5($data['user_id']) . "/" . $account_folder_id ?>">Accept this invitation to get started</a>
<br />
<br/>
<p>Read <strong><?php echo $users['full_name']; ?></strong>'s  message below:</p>

<p><?php echo isset($data['message']) ? $data['message'] : ''; ?></p>

<p>Still have questions?</p>

<p>Contact <strong><?php echo $users['full_name']; ?></strong> at <?php echo $users['email']; ?></p>
<p>
    Delivered by Sibme <a href='www.sibme.com' target="_blank">www.sibme.com</a>
</p>
