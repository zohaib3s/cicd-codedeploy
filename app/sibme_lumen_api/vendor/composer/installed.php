<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-master',
    'version' => 'dev-master',
    'aliases' => 
    array (
    ),
    'reference' => '3f29551dfb0daee44c9102fbff78e1a223a58096',
    'name' => 'laravel/lumen',
  ),
  'versions' => 
  array (
    'aacotroneo/laravel-saml2' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6ed350b37aec3470655335a33e24496e15bde9b7',
    ),
    'alchemy/binary-driver' => 
    array (
      'pretty_version' => 'v2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6ccde0e19e81e54da77b08da1a176d43e089f3a3',
    ),
    'aws/aws-sdk-php' => 
    array (
      'pretty_version' => '3.155.3',
      'version' => '3.155.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'da40d5d22acbe542f37c44c78ee520005a6c81a2',
    ),
    'baopham/dynamodb' => 
    array (
      'pretty_version' => '6.0.0',
      'version' => '6.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3c85a7b4e552274ad144688f9d4cbab46fba1f41',
    ),
    'barryvdh/laravel-dompdf' => 
    array (
      'pretty_version' => 'v0.8.7',
      'version' => '0.8.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '30310e0a675462bf2aa9d448c8dcbf57fbcc517d',
    ),
    'braintree/braintree_php' => 
    array (
      'pretty_version' => '4.7.0',
      'version' => '4.7.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '044f6e20c3831a7c19dfb6d1730e3e509470a115',
    ),
    'codedge/laravel-fpdf' => 
    array (
      'pretty_version' => '1.5.2',
      'version' => '1.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b33e4f32c0a2c4e7c17631179dc726ccd1f78b39',
    ),
    'cordoval/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'davedevelopment/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'doctrine/cache' => 
    array (
      'pretty_version' => '1.10.2',
      'version' => '1.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '13e3381b25847283a91948d04640543941309727',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '1.4.3',
      'version' => '1.4.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '4650c8b30c753a76bf44fb2ed00117d6f367490c',
    ),
    'doctrine/instantiator' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f350df0268e904597e3bd9c4685c53e0e333feea',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
    ),
    'dompdf/dompdf' => 
    array (
      'pretty_version' => 'v0.8.6',
      'version' => '0.8.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'db91d81866c69a42dad1d2926f61515a1e3f42c5',
    ),
    'dragonmantank/cron-expression' => 
    array (
      'pretty_version' => 'v2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '72b6fbf76adb3cf5bc0db68559b33d41219aba27',
    ),
    'drewm/mailchimp-api' => 
    array (
      'pretty_version' => 'v2.5.4',
      'version' => '2.5.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c6cdfab4ca6ddbc3b260913470bd0a4a5cb84c7a',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '2.1.22',
      'version' => '2.1.22.0',
      'aliases' => 
      array (
      ),
      'reference' => '68e418ec08fbfc6f58f6fd2eea70ca8efc8cc7d5',
    ),
    'erusev/parsedown' => 
    array (
      'pretty_version' => '1.7.4',
      'version' => '1.7.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb17b6477dfff935958ba01325f2e8a2bfa6dab3',
    ),
    'evenement/evenement' => 
    array (
      'pretty_version' => 'v2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6ba9a777870ab49f417e703229d53931ed40fd7a',
    ),
    'firebase/php-jwt' => 
    array (
      'pretty_version' => 'v5.2.0',
      'version' => '5.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'feb0e820b8436873675fd3aca04f3728eb2185cb',
    ),
    'fzaninotto/faker' => 
    array (
      'pretty_version' => 'v1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fc10d778e4b84d5bd315dad194661e091d307c6f',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '6.5.5',
      'version' => '6.5.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '9d4290de1cfd701f38099ef7e183b64b4b7b0c5e',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => 'v1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a59da6cf61d80060647ff4d3eb2c03a2bc694646',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '1.6.1',
      'version' => '1.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '239400de7a173fe9901b9ac7c06497751f00727a',
    ),
    'hamcrest/hamcrest-php' => 
    array (
      'pretty_version' => 'v2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '8c3d0a3f6af734494ad8f6fbbee0ba92422859f3',
    ),
    'illuminate/auth' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '0d4aa427a01ecfcd9eeda1301a7602d6e39a1cdb',
    ),
    'illuminate/broadcasting' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '346c4f5552c0247e0e9f8bf2e637feb13520c061',
    ),
    'illuminate/bus' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '5dba5798de392a3da8add4541df10661d5c6a0f4',
    ),
    'illuminate/cache' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '615759a96b212365aece1abc64cc3a98a7d730b0',
    ),
    'illuminate/config' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '61f4eb8145a1473577a9876471c92fa4de4718a7',
    ),
    'illuminate/console' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'de6b95229de443585f97953da4b02721465a2b2b',
    ),
    'illuminate/container' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '779b56b37396b888414622d5667d3bcc205964ab',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '66b653fd430bf06f59cef1112197d009bd02da84',
    ),
    'illuminate/database' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '33073fe81e705b3d5d5ef992e6b065b3a89b07f6',
    ),
    'illuminate/encryption' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bd5ec7a711863fea409211bc6f04da8d1a05515e',
    ),
    'illuminate/events' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c702e65fe37458fece6ae87ce7906aaa614383d6',
    ),
    'illuminate/filesystem' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b25940b428c9fd284feaad20e176ace8d780973b',
    ),
    'illuminate/hashing' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ee3f00a4f12e562f7d2217b0889e3ba44c587a6d',
    ),
    'illuminate/http' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '552a7d2851c84adc1a4536c56ffc25d77a3b5f34',
    ),
    'illuminate/log' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e00aa10a73127f676939ddb5b615302025c2838',
    ),
    'illuminate/mail' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'affc84b390b8d482da2a05046834772c124b74b4',
    ),
    'illuminate/pagination' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b8ff0633e8c20f16c1100192f71be0b13568f021',
    ),
    'illuminate/pipeline' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '18bf3d2c621939f87e285b5af8c464a549c4df05',
    ),
    'illuminate/queue' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd304bf168765b773a0bb44556a09d1f475d9c12e',
    ),
    'illuminate/redis' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e92b47801ae7dbfb782cced6b812518043daa653',
    ),
    'illuminate/routing' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '4ef7a867415c953a137f5e9084c40ab6554a2c58',
    ),
    'illuminate/session' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '183f1c064bb47a94d72c98aacc24f3189e03c6e7',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e2fce24254b8f60a2f92a3ab485799b372625a06',
    ),
    'illuminate/translation' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '6e4d8211d6717a1d55c11d4e06b3185c9f8a8d2a',
    ),
    'illuminate/validation' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f7da089264cf8cea97a1591ea9622dae1e500ec2',
    ),
    'illuminate/view' => 
    array (
      'pretty_version' => 'v5.6.39',
      'version' => '5.6.39.0',
      'aliases' => 
      array (
      ),
      'reference' => '60fd8f12340417a4312e5d90961510333d4f1d46',
    ),
    'intercom/intercom-php' => 
    array (
      'pretty_version' => 'v3.2.0',
      'version' => '3.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ac2ef62ac6deb6141be05036c7bf05ce034f3170',
    ),
    'itsgoingd/clockwork' => 
    array (
      'pretty_version' => 'v3.1.4',
      'version' => '3.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '529b6f9e2ce487b900213cf1d0694d0aebd5977f',
    ),
    'jeremeamia/superclosure' => 
    array (
      'pretty_version' => '2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5707d5821b30b9a07acfb4d76949784aaa0e9ce9',
    ),
    'kitbrennan90/aws-transcribe-to-webvtt' => 
    array (
      'pretty_version' => 'v1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2c6a4c4f7100c6457498937144f94efebf1e6533',
    ),
    'kodova/hamcrest-php' => 
    array (
      'replaced' => 
      array (
        0 => '*',
      ),
    ),
    'kylekatarnls/update-helper' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '429be50660ed8a196e0798e5939760f168ec8ce9',
    ),
    'laravel/lumen' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => '3f29551dfb0daee44c9102fbff78e1a223a58096',
    ),
    'laravel/lumen-framework' => 
    array (
      'pretty_version' => 'v5.6.4',
      'version' => '5.6.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '377918ff2b73e02fbc091f75471a365f17bf0704',
    ),
    'laravelista/lumen-vendor-publish' => 
    array (
      'pretty_version' => '5.6.0',
      'version' => '5.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4060fe9818f83bebb9458312cb4c15f692db4e97',
    ),
    'league/flysystem' => 
    array (
      'pretty_version' => '1.0.70',
      'version' => '1.0.70.0',
      'aliases' => 
      array (
      ),
      'reference' => '585824702f534f8d3cf7fab7225e8466cc4b7493',
    ),
    'maatwebsite/excel' => 
    array (
      'pretty_version' => '2.1.30',
      'version' => '2.1.30.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f5540c4ba3ac50cebd98b09ca42e61f926ef299f',
    ),
    'maennchen/zipstream-php' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c4c5803cc1f93df3d2448478ef79394a5981cc58',
    ),
    'markbaker/complex' => 
    array (
      'pretty_version' => '1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c3131244e29c08d44fefb49e0dd35021e9e39dd2',
    ),
    'markbaker/matrix' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '182d44c3b2e3b063468f7481ae3ef71c69dc1409',
    ),
    'mockery/mockery' => 
    array (
      'pretty_version' => '1.3.3',
      'version' => '1.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '60fa2f67f6e4d3634bb4a45ff3171fa52215800d',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '1.25.5',
      'version' => '1.25.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '1817faadd1846cd08be9a49e905dc68823bc38c0',
    ),
    'mtdowling/jmespath.php' => 
    array (
      'pretty_version' => '2.6.0',
      'version' => '2.6.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '42dae2cbd13154083ca6d70099692fef8ca84bfb',
    ),
    'myclabs/deep-copy' => 
    array (
      'pretty_version' => '1.10.1',
      'version' => '1.10.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '969b211f9a51aa1f6c01d1d2aef56d3bd91598e5',
      'replaced' => 
      array (
        0 => '1.10.1',
      ),
    ),
    'myclabs/php-enum' => 
    array (
      'pretty_version' => '1.7.6',
      'version' => '1.7.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '5f36467c7a87e20fbdc51e524fd8f9d1de80187c',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '1.25.3',
      'version' => '1.25.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ad6afecd38ce2d7f7bd1b5d47ffd8e93ebbd3ed8',
    ),
    'neutron/temporary-filesystem' => 
    array (
      'pretty_version' => '2.4',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3c55497da8d7762fb4dcabc91d54a5de510e3c99',
    ),
    'nikic/fast-route' => 
    array (
      'pretty_version' => 'v1.3.0',
      'version' => '1.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '181d480e08d9476e61381e04a71b34dc0432e812',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.10.2',
      'version' => '4.10.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '658f1be311a230e0907f5dfe0213742aff0596de',
    ),
    'onelogin/php-saml' => 
    array (
      'pretty_version' => '3.4.1',
      'version' => '3.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fbf3486704ac9835b68184023ab54862c95f213',
    ),
    'opencoconut/coconut' => 
    array (
      'pretty_version' => '2.8.0',
      'version' => '2.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cd0d9d6c1a1c19bb5303d87757407ee25f924fec',
    ),
    'paragonie/random_compat' => 
    array (
      'pretty_version' => 'v9.99.99',
      'version' => '9.99.99.0',
      'aliases' => 
      array (
      ),
      'reference' => '84b4dfb120c6f9b4ff7b3685f9b8f1aa365a0c95',
    ),
    'pclzip/pclzip' => 
    array (
      'pretty_version' => '2.8.2',
      'version' => '2.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '19dd1de9d3f5fc4d7d70175b4c344dee329f45fd',
    ),
    'phar-io/manifest' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7761fcacf03b4d4f16e7ccb606d4879ca431fcf4',
    ),
    'phar-io/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '45a2ec53a73c70ce41d55cedef9063630abaf1b6',
    ),
    'phenx/php-font-lib' => 
    array (
      'pretty_version' => '0.5.2',
      'version' => '0.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ca6ad461f032145fff5971b5985e5af9e7fa88d8',
    ),
    'phenx/php-svg-lib' => 
    array (
      'pretty_version' => 'v0.3.3',
      'version' => '0.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '5fa61b65e612ce1ae15f69b3d223cb14ecc60e32',
    ),
    'php-ffmpeg/php-ffmpeg' => 
    array (
      'pretty_version' => 'v0.13',
      'version' => '0.13.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c11b79ab5b0174aa1a56c54c67491169e78a4c17',
    ),
    'phpdocumentor/reflection-common' => 
    array (
      'pretty_version' => '2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1d01c49d4ed62f25aa84a747ad35d5a16924662b',
    ),
    'phpdocumentor/reflection-docblock' => 
    array (
      'pretty_version' => '5.2.2',
      'version' => '5.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '069a785b2141f5bcf49f3e353548dc1cce6df556',
    ),
    'phpdocumentor/type-resolver' => 
    array (
      'pretty_version' => '1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6a467b8989322d92aa1c8bf2bebcc6e5c2ba55c0',
    ),
    'phpmailer/phpmailer' => 
    array (
      'pretty_version' => 'v6.1.7',
      'version' => '6.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '2c2370ba3df7034f9eb7b8f387c97b52b2ba5ad0',
    ),
    'phpoffice/common' => 
    array (
      'pretty_version' => '0.2.9',
      'version' => '0.2.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'edb5d32b1e3400a35a5c91e2539ed6f6ce925e4d',
    ),
    'phpoffice/phpexcel' => 
    array (
      'pretty_version' => '1.8.2',
      'version' => '1.8.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1441011fb7ecdd8cc689878f54f8b58a6805f870',
    ),
    'phpoffice/phpspreadsheet' => 
    array (
      'pretty_version' => '1.14.1',
      'version' => '1.14.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2383aad5689778470491581442aab38cec41bf1d',
    ),
    'phpoffice/phpword' => 
    array (
      'pretty_version' => '0.16.0',
      'version' => '0.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7b7d4e4936014544aa706f4c03d3ac925d74beb9',
    ),
    'phpspec/prophecy' => 
    array (
      'pretty_version' => '1.12.0',
      'version' => '1.12.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '765cd5d5d237525f8bbadaec5dc161c83a369119',
    ),
    'phpunit/php-code-coverage' => 
    array (
      'pretty_version' => '6.1.4',
      'version' => '6.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '807e6013b00af69b6c5d9ceb4282d0393dbb9d8d',
    ),
    'phpunit/php-file-iterator' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '050bedf145a257b1ff02746c31894800e5122946',
    ),
    'phpunit/php-text-template' => 
    array (
      'pretty_version' => '1.2.1',
      'version' => '1.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '31f8b717e51d9a2afca6c9f046f5d69fc27c8686',
    ),
    'phpunit/php-timer' => 
    array (
      'pretty_version' => '2.1.2',
      'version' => '2.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '1038454804406b0b5f5f520358e78c1c2f71501e',
    ),
    'phpunit/php-token-stream' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '995192df77f63a59e47f025390d2d1fdf8f425ff',
    ),
    'phpunit/phpunit' => 
    array (
      'pretty_version' => '7.5.20',
      'version' => '7.5.20.0',
      'aliases' => 
      array (
      ),
      'reference' => '9467db479d1b0487c99733bb1e7944d32deded2c',
    ),
    'predis/predis' => 
    array (
      'pretty_version' => 'v1.1.6',
      'version' => '1.1.6.0',
      'aliases' => 
      array (
      ),
      'reference' => '9930e933c67446962997b05201c69c2319bf26de',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
        1 => '1.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'robrichards/xmlseclibs' => 
    array (
      'pretty_version' => '3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f8f19e58f26cdb42c54b214ff8a820760292f8df',
    ),
    'sabberworm/php-css-parser' => 
    array (
      'pretty_version' => '8.3.1',
      'version' => '8.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd217848e1396ef962fb1997cf3e2421acba7f796',
    ),
    'sebastian/code-unit-reverse-lookup' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4419fcdb5eabb9caa61a27c7a1db532a6b55dd18',
    ),
    'sebastian/comparator' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '5de4fc177adf9bce8df98d8d141a7559d7ccf6da',
    ),
    'sebastian/diff' => 
    array (
      'pretty_version' => '3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '720fcc7e9b5cf384ea68d9d930d480907a0c1a29',
    ),
    'sebastian/environment' => 
    array (
      'pretty_version' => '4.2.3',
      'version' => '4.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '464c90d7bdf5ad4e8a6aea15c091fec0603d4368',
    ),
    'sebastian/exporter' => 
    array (
      'pretty_version' => '3.1.2',
      'version' => '3.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '68609e1261d215ea5b21b7987539cbfbe156ec3e',
    ),
    'sebastian/global-state' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e8ba02eed7bbbb9e59e43dedd3dddeff4a56b0c4',
    ),
    'sebastian/object-enumerator' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '7cfd9e65d11ffb5af41198476395774d4c8a84c5',
    ),
    'sebastian/object-reflector' => 
    array (
      'pretty_version' => '1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '773f97c67f28de00d397be301821b06708fca0be',
    ),
    'sebastian/recursion-context' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b0cd723502bac3b006cbf3dbf7a1e3fcefe4fa8',
    ),
    'sebastian/resource-operations' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d7a795d35b889bf80a0cc04e08d77cedfa917a9',
    ),
    'sebastian/version' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '99732be0ddb3361e16ad77b68ba41efc8e979019',
    ),
    'swiftmailer/swiftmailer' => 
    array (
      'pretty_version' => 'v6.2.3',
      'version' => '6.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '149cfdf118b169f7840bbe3ef0d4bc795d1780c9',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v4.4.14',
      'version' => '4.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '90933b39c7b312fc3ceaa1ddeac7eb48cb953124',
    ),
    'symfony/css-selector' => 
    array (
      'pretty_version' => 'v4.4.14',
      'version' => '4.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bf17dc9f6ce144e41f786c32435feea4d8e11dcc',
    ),
    'symfony/debug' => 
    array (
      'pretty_version' => 'v4.4.14',
      'version' => '4.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '726b85e69342e767d60e3853b98559a68ff74183',
    ),
    'symfony/error-handler' => 
    array (
      'pretty_version' => 'v4.4.14',
      'version' => '4.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '5a6feca7a384015a09e14265c35ee0bd6f54b2ed',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v4.4.14',
      'version' => '4.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e17bb5e0663dc725f7cdcafc932132735b4725cd',
    ),
    'symfony/event-dispatcher-contracts' => 
    array (
      'pretty_version' => 'v1.1.9',
      'version' => '1.1.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '84e23fdcd2517bf37aecbd16967e83f0caee25a7',
    ),
    'symfony/event-dispatcher-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1',
      ),
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v4.4.14',
      'version' => '4.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '0d386979828c15d37ff936bf9bae2ecbfa36d7dc',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v4.4.14',
      'version' => '4.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '5ef0f6c609c1a36f723880dfe78301199bc96868',
    ),
    'symfony/http-client-contracts' => 
    array (
      'pretty_version' => 'v1.1.10',
      'version' => '1.1.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '7e86f903f9720d0caa7688f5c29a2de2d77cbb89',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v4.4.14',
      'version' => '4.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ff509ca7a73641bdbd7b56169a9004e64a58451d',
    ),
    'symfony/http-kernel' => 
    array (
      'pretty_version' => 'v4.4.14',
      'version' => '4.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e3eac6daeb0c65965a6201bd2de9564a802fe0a9',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v4.4.14',
      'version' => '4.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '42df2507eb8e6cd9795f51c99dd52bab543a918f',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1c302646f6efc070cd46856e600e5e0684d6b454',
    ),
    'symfony/polyfill-iconv' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '6c2f78eb8f5ab8eaea98f6d414a5915f2e0fce36',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '5dcab1bc7146cf8c1beaa4502a3d9be344334251',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '37078a8dd4a2a1e9ab0231af7c6cb671b2ed5a7e',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a6977d63bf9a0ad4c65cd352709e230876f9904a',
    ),
    'symfony/polyfill-php56' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '13df84e91cd168f247c2f2ec82cc0fa24901c011',
    ),
    'symfony/polyfill-php70' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '0dd93f2c578bdc9c72697eaa5f1dd25644e618d3',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '639447d008615574653fb3bc60d1986d7172eaae',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fffa1a52a023e782cdcc221d781fe1ec8f87fcca',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd87d5766cbf48d72388a9f6b85f280c8ad51f981',
    ),
    'symfony/polyfill-util' => 
    array (
      'pretty_version' => 'v1.18.1',
      'version' => '1.18.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '46b910c71e9828f8ec2aa7a0314de1130d9b295a',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v4.4.14',
      'version' => '4.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '9b887acc522935f77555ae8813495958c7771ba7',
    ),
    'symfony/routing' => 
    array (
      'pretty_version' => 'v4.4.14',
      'version' => '4.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '8db77d97152f55f0df5158cc0a877ad8e16099ac',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v1.1.9',
      'version' => '1.1.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b776d18b303a39f56c63747bcb977ad4b27aca26',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v4.4.14',
      'version' => '4.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '0b8c4bb49b05b11d2b9dd1732f26049b08d96884',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v1.1.10',
      'version' => '1.1.10.0',
      'aliases' => 
      array (
      ),
      'reference' => '84180a25fad31e23bebd26ca09d89464f082cacc',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v4.4.14',
      'version' => '4.4.14.0',
      'aliases' => 
      array (
      ),
      'reference' => '0dc22bdf9d1197467bb04d505355180b6f20bcca',
    ),
    'theseer/tokenizer' => 
    array (
      'pretty_version' => '1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '75a63c33a8577608444246075ea0af0d052e452a',
    ),
    'tibonilab/pdf-lumen-bundle' => 
    array (
      'pretty_version' => '2.0.3',
      'version' => '2.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '370023eadc1111b47508ab72b406fd3d8ae041bc',
    ),
    'tijsverkoyen/css-to-inline-styles' => 
    array (
      'pretty_version' => '2.2.3',
      'version' => '2.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b43b05cf43c1b6d849478965062b6ef73e223bb5',
    ),
    'vlucas/phpdotenv' => 
    array (
      'pretty_version' => 'v2.6.6',
      'version' => '2.6.6.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e1d57f62db3db00d9139078cbedf262280701479',
    ),
    'webmozart/assert' => 
    array (
      'pretty_version' => '1.9.1',
      'version' => '1.9.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bafc69caeb4d49c39fd0779086c03a3738cbb389',
    ),
    'wowza/wse-rest-library-php' => 
    array (
      'pretty_version' => 'dev-master',
      'version' => 'dev-master',
      'aliases' => 
      array (
      ),
      'reference' => 'fcc9e69b5c566bfc607aa6c63fb8515319be03c7',
    ),
    'zencoder/zencoder-php' => 
    array (
      'pretty_version' => 'v2.2.4',
      'version' => '2.2.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f1617a7f89a7422dd8682c276dd644764c4200f8',
    ),
    'zendframework/zend-escaper' => 
    array (
      'pretty_version' => '2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '3801caa21b0ca6aca57fa1c42b08d35c395ebd5f',
    ),
  ),
);
