<?php
/**
 * Created by PhpStorm.
 * User: Hassan Raza
 * Date: 12/09/2018
 * Time: 2:00 PM
 */

namespace App\Api\TrialSignUp;

use App\Api\Response\Response;
use App\Api\TrialSignUp\TrialSignUpHandler;
use GuzzleHttp\Client;

class TrialSignUp
{
    protected $tsignup;
    protected $day1_all;
    protected $client;
    protected $from;

    public function __construct(TrialSignUpHandler $tsignup)
    {
        $this->tsignup=$tsignup;
        $this->day1_all = config('apikeys.day1_all');
        $this->client = new \GuzzleHttp\Client(['http_errors' => false]);
        $this->from = config('mail.from.address');
    }
    
    public function save_accounts($data, $site_id)
    {
        return $this->tsignup->store_accounts($data, $site_id);
    }

    public function save_users($data, $salt, $verification_code, $site_id)
    {
        // Password encryption
        $string = $data['password'];
        $data['password'] = sha1($salt . $string);
        //Slit fullname
        $split_name = $this->split_name($data['full_name']);
        $data['first_name'] = $split_name[0];
        $data['last_name'] = $split_name[1];
        return $this->tsignup->store_user($data, $verification_code, $site_id);
    }

    public function update_auth_userid($user_id, $site_id)
    {
        return $this->tsignup->update_auth_userid($user_id, $site_id);
    }

    public function save_users_accounts($user_id, $account_id, $site_id)
    {
        return $this->tsignup->store_user_accounts($user_id, $account_id, $site_id);
    }

    public function save_account_tags($user_id, $account_id, $site_id)
    {
        $account_id = $account_id;
        $user_id = $user_id;
        $comments = ['Question', 'Idea', 'Bright spot'];
        for ($i=0; $i < count($comments); $i++) { 
            $ids[] = $this->tsignup->store_tags($user_id, $account_id, $comments[$i], $site_id);
        }
        return $ids;
    }

    public function get_user_account_data($account_id, $user_id, $site_id)
    {
        return $this->tsignup->update_resend_code_info($account_id, $user_id, $site_id);
    }

    public function copyHuddleForSample($huddleids, $account_id, $user_id, $site_id)
    {
        $this->tsignup->copyHuddleForSample($huddleids, $account_id, $user_id, $site_id);
    }

    public function transformers($data, $sibme_account_id, $sibme_account_owner_user_id)
    {
        $start_date = strtotime(date('j M Y').' 00:00:00 UTC') * 1000;
        $end_date = strtotime(date('j M Y', strtotime("+30 days")).' 00:00:00 UTC') * 1000;
        $split_name = $this->split_name($data['full_name']);
        $fname = $split_name[0];
        $lname = $split_name[1] ? $split_name[1] : '';
        $data['company'] = htmlentities($data['company']);
        $payload = '{
          "properties": [
            {
              "property": "email",
              "value": "'.$data['email'].'"
            },
            {
              "property": "firstname",
              "value": "'.$fname.'"
            },
            {
              "property": "company",
              "value": "'.$data['company'].'"
            },';

            if (isset($lname)) {
                $payload .= '{
                  "property": "lastname",
                  "value": "'.$lname.'"
                },';
            }

            if (isset($data['jobtitle'])) {
                $payload .= '{
                  "property": "jobtitle",
                  "value": "'.$data['jobtitle'].'"
                },';
            }

            if (isset($data['phone'])) {
                $payload .= '{
                  "property": "phone",
                  "value": "'.$data['phone'].'"
                },';
            }

            if (isset($data['how_do_you_intend_to_use_'])) {
                $payload .= '{
                  "property": "how_do_you_intend_to_use_",
                  "value": "'.trim($data['how_do_you_intend_to_use_']).'"
                },';
            }

            $payload .='{
              "property": "lead_source",
              "value": "Online Trial"
            },
            {
              "property": "trial_start_date",
              "value": "'.$start_date.'"
            },
            {
              "property": "trial_end_date",
              "value": "'.$end_date.'"
            },
            {
              "property": "sibme_account_id",
              "value": "'.$sibme_account_id.'"
            },
            {
              "property": "sibme_account_owner_user_id",
              "value": "'.$sibme_account_owner_user_id.'"
            },
            {
              "property": "custom_lifecycle_stage",
              "value": "Lead"
            }
          ]
        }';

        return $payload;
    }

    public function split_name($name) 
    {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim( preg_replace('#'.$last_name.'#', '', $name ) );
        return array($first_name, $last_name);
    }

    public function send_welcome_email_template($data, $account_id, $site_id)
    {
        $use_template = $this->day1_all;
        $req_url = "https://api.sendgrid.com/v3/templates/$use_template";
        $resp = $this->client->get($req_url, 
            ['headers' => 
                ['authorization' => 'Basic ZGF2ZXdAc2libWUuY29tOmM5ODhZWXdzIUA='],
                ['content-type' => 'application/json']
            ],
            ['debug' => true]
        );
        $resp = json_decode($resp->getBody()->getContents(),true);
        if(!empty($resp) && isset($resp['versions']) && isset($resp['versions'][0]) && isset($resp['versions'][0]['html_content'])){
          $status = \App\Services\Emails\TrialSignUp::sendEmail($site_id, $data, $account_id, $resp['versions'][0]['html_content'], $this->from);
          return $status;
        }else{
          return ['error'=>1, 
          'message'=>'Sorry your email could not be sent right now, please try again later.', 
          'exception'=>'SendGrid Api is not responding'];
        }
       
    }

    public function activation_email($data, $account_id, $site_id){
        $status = \App\Services\Emails\TrialSignUp::activationEmail($data, $account_id, $site_id);
        return $status;
    }

    public function generic_options($lang='en')
    {
       if($lang=='es'){
         $roles = [
                    "Mentor Instruccional",
                    "Administrador de Colegio",
                    "Administrador de Distrito",
                    "Profesor de Enseñanza Media",
                    "Administrador de Enseñanza Media",
                    "Consultor",
                    "Entrenador Corporativo",
                    "Profesor entre Kínder a IVº Medio",
                    "Estudiante"
                ];
                $k12_options = [
                    "Quiero probar Sibme en mi colegio o en mi equipo",
                    "Mi colegio se inscribió en Sibme y yo quiero acceso",
                    "Me pidieron crear una cuenta Sibme",
                    "Otro"
                ];
       }else{
         $roles = [
            "Instructional Coach",
            "School Administrator",
            "District Administrator",
            "Higher-Ed Faculty",
            "Higher-Ed Administration",
            "Consultant",
            "Corporate Trainer",
            "K-12 Teacher",
            "Student"
        ];
        $k12_options = [
            "I want to try Sibme for my school or team",
            "My school signed up for Sibme and I want access",
            "I was asked to create a Sibme account",
            "Other"
        ];
       }
       
        $data = [];
        foreach ($roles as $key => $value) {
           $data[$key]['role_id'] = $key;
           $data[$key]['role_name'] = $value;
           if ($key==7 || $key==8) {
               $data[$key]['why_signup_options'] = $k12_options;
           }else{
                $data[$key]['why_signup_options'] = [];
           }
        }
        return $data;
    }

    public function check_email($data, $site_id)
    {
        return $this->tsignup->check_email($data, $site_id);
    }

    public function upGradePlantoUnlimited($account_id){
        return $this->tsignup->upGradePlantoUnlimited($account_id);
    }
}