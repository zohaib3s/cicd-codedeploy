<?php
/**
 * Created by PhpStorm.
 * User: Hassan Raza
 * Date: 12/09/2018
 * Time: 2:00 PM
 */

namespace App\Api\TrialSignUp;

use Illuminate\Support\Facades\DB;

class TrialSignUpHandler
{
    public $per_page;
    public $role_account_owner = '100';
    public $role_super_user = '110';
    public $role_user = '120';
    public $role_viewer = '125';
    public $role_admin = '115';
    public $role_huddle_admin = '200';
    public $role_huddle_user = '210';
    public $role_huddle_viewer = '220';

    public function __construct()
    {
        $this->per_page = 100;
    }

    public function store_accounts($data, $site_id)
    {
        try
        {
            $data = array(
                'company_name' => $data['company'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'is_active' => '1',
                'in_trial' => '1',
                'has_credit_card' => '0',
                'is_suspended' => '0',
                'site_id' => $site_id ,
                'enable_live_rec' => '1'
            );
            $account_id = DB::table('accounts')->insertGetId($data);
            return $account_id;
        }
        catch(Exception $ex)
        {
            throw $ex;
        }
    }
    public function store_user($data, $verification_code, $site_id){
        try
        {
            $data = array(
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'email' => $data['email'],
                'created_date' => date('Y-m-d H:i:s'),
                'username' => $data['email'],
                'password' => $data['password'],
                'current_role' => $data['current_role'],
                'why_signup' => @$data['why_signup'],
                'colleague_name' => @$data['colleague_name'],
                'colleague_email' => @$data['colleague_email'],
                'how_do_you_intend_to_use_' => trim($data['how_do_you_intend_to_use_']),
                'type' => 'Pending_Activation',
                'is_active' => TRUE,
                'verification_code' => $verification_code,
                'site_id' => $site_id
            );
            $user_id = DB::table('users')->insertGetId($data);
            return $user_id;
        }
        catch(Exception $ex)
        {
            throw $ex;
        }
    }
    public function update_auth_userid($userid, $site_id){
        try
        {
            $data = ['authentication_token' => md5($userid)];
            $res = DB::table('users')
                ->where('id', $userid)->where('site_id', $site_id)
                ->update($data);
            return $res;
        }catch(Exception $ex)
        {
            throw $ex;
        }
    }

    public function upGradePlantoUnlimited($account_id){
        try
        {
            $data = ['in_trial' => 0, 'deactive_plan' => 1];
            $res = DB::table('accounts')
                ->where('id', $account_id)
                ->where('site_id', 2)
                ->update($data);
            return $res;
        }catch(Exception $ex)
        {
            throw $ex;
        }
    }

    public function store_user_accounts($user_id, $account_id, $site_id){
        try
        {
            $data = array(
                'user_id' => $user_id,
                'account_id' => $account_id,
                'role_id' => 100,
                'is_default' => true,
                'permission_maintain_folders' => true,
                'permission_access_video_library' => true,
                'permission_video_library_upload' => true,
                'permission_administrator_user_new_role' => true,
                'parmission_access_my_workspace' => true,
                'folders_check' => true,
                'manage_collab_huddles' => true,
                'manage_coach_huddles' => true,
                'manage_evaluation_huddles' => true,
                'live_recording' => true,
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $user_id,
                'last_edit_date' => date('Y-m-d H:i:s'),
                'last_edit_by' => $user_id,
                'site_id' => $site_id
            );
            $last_insert_id = DB::table('users_accounts')->insertGetId($data);
            $get_owner_id = $this->convert_array(DB::table('users_accounts')
                    ->where('id','=',$last_insert_id)
                    ->get());
            return $get_owner_id['user_id'];
        }
        catch(Exception $ex)
        {
            throw $ex;
        }
        
    }

    public function store_tags($user_id, $account_id, $tag_title, $site_id){
        try
        {
            $save_tags = array(
                "account_id" => $account_id,
                "tag_type" => '1',
                "tag_title" => $tag_title,
                "created_by" => $user_id,
                "created_date" => date('Y-m-d H:i:s'),
                "last_edit_by" => $user_id,
                "last_edit_date" => date('Y-m-d H:i:s'),
                "site_id" => $site_id
            );
            $tag_id = DB::table('account_tags')->insertGetId($save_tags);
            return $tag_id;
        }
        catch(Exception $ex)
        {
            throw $ex;
        }
    }

    public function store_accounts_folder_metadata($user_id, $account_id){
        try
        {
            $data = array(
                'account_folder_id' => $account_id,
                'meta_data_name' => 'enable_tags',
                'meta_data_value' => '1',
                'created_by' => $user_id,
                'created_date' => date('Y-m-d H:i:s'),
            );
            $fmeta_data_id = DB::table('account_folders_meta_data')->insertGetId($data);
            return $fmeta_data_id;
        }
        catch(Exception $ex)
        {
            throw $ex;
        }
    }

    
    public function copyHuddleForSample($sourceHuddleIds, $destinationAccountId, $destinationUserId, $site_id) {

        $sampleHuddles = explode(',', $sourceHuddleIds);
        $sourceAccountId = 0;
        $sourceUserId = 0;

        //if (count($huddle_user) === 0) {
        foreach ($sampleHuddles as $sourceHuddleId) {

            $huddle = $this->convert_array(DB::table('account_folders')
                    ->where('account_folder_id','=',$sourceHuddleId)
                    ->get());
            $account = $this->convert_array(DB::table('users_accounts')
                    ->where('account_id','=',$destinationAccountId)
                    ->where('user_id', '=', $destinationUserId)
                    ->get());
            if (empty($huddle) || empty($account))
                return;

            $sourceAccountId = $huddle['account_id'];
            $sourceUserId = $huddle['created_by'];

            if ($sourceAccountId == $destinationAccountId || $sourceUserId == $destinationUserId)
                return;

            $foldername = $huddle['name'];
            $desc = $huddle['desc'];
            $insert_id = $this->store_account_folder($destinationUserId, $destinationAccountId, $foldername, $desc, $site_id);

            if ($insert_id) {
                $destinationHuddleId = $insert_id;
                $huddle_type = $this->convert_array(DB::table('account_folders_meta_data')
                    ->where('account_folder_id','=',$sourceHuddleId)
                    ->where('meta_data_name','=','folder_type')
                    ->where('meta_data_value','=',2)
                    ->get()->toArray());
                if (count($huddle_type) > 0 && $huddle_type['meta_data_name'] == 'folder_type' && $huddle_type['meta_data_value'] == 2) {
                    $account_fold_meta_data = array(
                        'account_folder_id' => $destinationHuddleId,
                        'meta_data_name' => $huddle_type['meta_data_name'],
                        'meta_data_value' => $huddle_type['meta_data_value'],
                        'created_date' => date('Y-m-d H:i:s'),
                        'created_by' => $destinationUserId,
                        'last_edit_date' => date('Y-m-d H:i:s'),
                        'last_edit_by' => $destinationUserId,
                        'site_id' => $site_id
                    );
                    $fmeta_id = $this->store_generic('account_folders_meta_data', $account_fold_meta_data);
                    $account_fold_tag_meta_data = array(
                        'account_folder_id' => $destinationHuddleId,
                        'meta_data_name' => 'chk_tags',
                        'meta_data_value' => 1,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $destinationUserId,
                        'last_edit_by' => $destinationUserId,
                        'site_id'      => $site_id
                    );
                    $fmeta_id = $this->store_generic('account_folders_meta_data', $account_fold_tag_meta_data);
                   
                } else {
                    //$this->AccountFolderMetaData->save($account_fold_meta_data);
                    $account_fold_tag_meta_data = array(
                        'account_folder_id' => $destinationHuddleId,
                        'meta_data_name' => 'chk_tags',
                        'meta_data_value' => 0,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $destinationUserId,
                        'last_edit_by' => $destinationUserId,
                        'site_id'      => $site_id
                    );
                    $fmeta_id = $this->store_generic('account_folders_meta_data', $account_fold_tag_meta_data);
                }
                $account_folders_meta_data = array(
                    'account_folder_id' => $destinationHuddleId,
                    'meta_data_name' => 'message',
                    'meta_data_value' => '',
                    'created_date' => date('Y-m-d H:i:s'),
                    'created_by' => $destinationUserId,
                    'last_edit_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $destinationUserId,
                    'site_id'      => $site_id
                );
                $fmeta_id = $this->store_generic('account_folders_meta_data', $account_folders_meta_data);
                if (count($huddle_type) > 0 && $huddle_type['meta_data_name'] == 'folder_type' && $huddle_type['meta_data_value'] == 2) {
                  
                     $superUserData = array(
                        'account_folder_id' => $destinationHuddleId,
                        'user_id' => '2423',
                        'role_id' => '210',
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $destinationUserId,
                        'last_edit_by' => $destinationUserId,
                        'is_coach' => 0,
                        'site_id' => $site_id,
                        'is_mentee' => 1
                    );
                    
                    $f_id = $this->store_generic('account_folder_users', $superUserData);
                    
                    $superUserData = array(
                        'account_folder_id' => $destinationHuddleId,
                        'user_id' => $destinationUserId,
                        'role_id' => $this->role_huddle_admin,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $destinationUserId,
                        'last_edit_by' => $destinationUserId,
                        'is_coach' => 1,
                        'site_id' => $site_id,
                        'is_mentee' => 0
                    );
                } else {
                    
                    $superUserData = array(
                        'account_folder_id' => $destinationHuddleId,
                        'user_id' => '2423',
                        'role_id' => '210',
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $destinationUserId,
                        'last_edit_by' => $destinationUserId,
                        'is_coach' => 0,
                        'site_id' => $site_id,
                        'is_mentee' => 0
                    );
                    
                    $f_id = $this->store_generic('account_folder_users', $superUserData);
                    
                    $superUserData = array(
                        'account_folder_id' => $destinationHuddleId,
                        'user_id' => '2427',
                        'role_id' => '220',
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $destinationUserId,
                        'last_edit_by' => $destinationUserId,
                        'is_coach' => 0,
                        'site_id' => $site_id,
                        'is_mentee' => 0
                    );
                    
                    $f_id = $this->store_generic('account_folder_users', $superUserData);
                    
                    
                    
                    $superUserData = array(
                        'account_folder_id' => $destinationHuddleId,
                        'user_id' => $destinationUserId,
                        'role_id' => $this->role_huddle_admin,
                        'created_date' => date("Y-m-d H:i:s"),
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'created_by' => $destinationUserId,
                        'last_edit_by' => $destinationUserId,
                        'site_id' => $site_id
                    );
                }
                $f_id = $this->store_generic('account_folder_users', $superUserData);

                $InvitedUserAccount['UserAccount'] = $this->convert_array(DB::table('users_accounts')
                    ->where('account_id','=',$destinationAccountId)
                    ->where('user_id', '=', $destinationUserId)
                    ->get());
                $role = $InvitedUserAccount['UserAccount']['role_id'];

                $this->copyAccountFolderDocument($sourceHuddleId, $sourceUserId, $destinationAccountId, $destinationHuddleId, $destinationUserId, $site_id);
                $this->copyAccountFolderDiscussion($sourceHuddleId, $destinationAccountId, $destinationHuddleId, $destinationUserId, $sourceUserId, $site_id);
                if ($role == 100) {
                    $this->copyUsersToSampleAccount($huddle['account_id'], $destinationAccountId, $site_id);
                    $this->copyGroupForSampleAccount($huddle['account_id'], $destinationAccountId, $sourceUserId, $destinationUserId, $site_id);
                }
                $this->copyUsersToSampleHuddle($sourceHuddleId, $destinationHuddleId, $site_id);
            }
        }
        //}
        if ($sourceAccountId > 0 && $sourceUserId > 0) {
            $InvitedUserAccount['UserAccount'] = $this->convert_array(DB::table('users_accounts')
                    ->where('user_id','=',$destinationUserId)
                    ->where('account_id','=',$destinationAccountId)
                    ->get());

            $role = $InvitedUserAccount['UserAccount']['role_id'];

            $this->copyMyFilesForSampleAccount($sourceAccountId, $sourceUserId, $destinationAccountId, $destinationUserId, $site_id);
            if ($role == 100) {
                $this->copyVideoLibraryForSampleAccount($sourceAccountId, $sourceUserId, $destinationAccountId, $destinationUserId, $site_id);
            }
        } else {
            $sampleHuddleId = config('apikeys.sample_huddle');
            $sampleHuddles = explode(',', $sourceHuddleIds);
            $huddle = $this->convert_array(DB::table('account_folders')
                    ->where('account_folder_id','=',$destinationUserId)
                    ->get());
            $sourceAccountId = $huddle['account_id'];
            $sourceUserId = $huddle['created_by'];
            $this->copyMyFilesForSampleAccount($sourceAccountId, $sourceUserId, $destinationAccountId, $destinationUserId, $site_id);
        }
    }

    protected function copyVideoLibraryForSampleAccount($sourceAccountId, $sourceUserId, $destinationAccountId, $destinationUserId, $site_id) {

        $result = DB::table('account_folder_documents AS afd')
                    ->select('afd.*', 'af.*', 'doc.*')
                    ->join('account_folders AS af', 'afd.account_folder_id', '=', 'af.account_folder_id')
                    ->join('documents AS doc', 'afd.document_id', '=', 'doc.id')
                    ->where('af.account_id','=',$destinationAccountId)
                    ->where('af.created_by','=',$destinationUserId)
                    ->where('af.folder_type','=',2)
                    ->where('af.active','=',1)
                    ->where('af.is_sample','=',1)
                    ->orderBy('af.created_date', 'desc')
                    ->get();
        $result = collect($result)->map(function($x){ return (array) $x; })->toArray();

        if (!empty($result)) {
            $ids = array();
            foreach ($result as $item) {
                DB::table('account_folders')
                ->where('active', 0)
                ->update(['account_folder_id' => $item['account_folder_id']]);
            }
            
        }

        $result = DB::table('account_folder_documents AS afd')
                    ->select('afd.*', 'af.*', 'doc.*')
                    ->join('account_folders AS af', 'afd.account_folder_id', '=', 'af.account_folder_id')
                    ->join('documents AS doc', 'afd.document_id', '=', 'doc.id')
                    ->where('af.account_id','=',$sourceAccountId)
                    ->where('af.created_by','=',$sourceUserId)
                    ->where('af.folder_type','=',2)
                    ->where('af.active','=',1)
                    ->where('af.is_sample','=',0)
                    ->where('doc.doc_type','=',1)
                    ->orderBy('af.created_date', 'desc')
                    ->get();
        $result = collect($result)->map(function($x){ return (array) $x; })->toArray();
        if (empty($result))
            return;

        foreach ($result as $item) {
            $account_folder_data = array(
                'account_id' => $destinationAccountId,
                'created_by' => $destinationUserId,
                'last_edit_by' => $destinationUserId,
                'site_id' => $site_id,
                'is_sample' => 1
                    ) + $item;
            unset($account_folder_data['account_folder_id']);
            $afdata = $this->get_filter_columns('account_folders', $account_folder_data);
            $afid = $this->store_generic('account_folders',$afdata);

            //$afddata = $this->get_filter_columns('account_folder_documents', $account_folder_data);
            //$afdid = $this->store_generic('account_folder_documents',$afddata);

            $fdocdata = $this->get_filter_columns('documents', $account_folder_data);
            $fdocdata['post_rubric_per_video'] = 1;
            unset($fdocdata['id']);
            $docid = $this->store_generic('documents',$fdocdata);

            $det_doc_id = $item['id'];
            $det_doc_zencoder_output_id = $item['zencoder_output_id'];

            $check_data = $this->convert_array(DB::table('document_files')
                    ->where('document_id','=',$det_doc_id)
                    ->where('default_web','=',true)
                    ->get());

            if (isset($check_data)) {

                $data = array(
                    'document_id' => $docid,
                    'url' => @$check_data['url'],
                    'resolution' => 1080,
                    'duration' => @$check_data['duration'],
                    'file_size' => @$check_data['file_size'],
                    'default_web' => 1,
                    'transcoding_job_id' => $det_doc_zencoder_output_id,
                    'site_id' => $site_id,
                    'transcoding_status' => -1
                );

                $dfid = $docfid = $this->store_generic('document_files',$data);
            }
            $fafdd = $this->get_filter_columns('account_folder_documents', $account_folder_data);
            $fafdd['account_folder_id'] = $afid;
            $fafdd['document_id'] = $docid;
            $fafdd['site_id'] = $site_id;
            unset($fafdd['id']);
            $afdid = $this->store_generic('account_folder_documents',$fafdd);

            // copy subjects
            $subjects = DB::table('account_folder_subjects')
                    ->where('account_folder_id','=',$item['account_folder_id'])
                    ->get();
            $subjects = collect($subjects)->map(function($x){ return (array) $x; })->toArray();
                foreach ($subjects as $subject) {
                    $sub = $this->convert_array(DB::table('subjects')
                        ->where('id','=',$subject['subject_id'])
                        ->get());

                    $sub_data = array(
                        'name' => @$sub['name'],
                        'account_id' => $destinationAccountId,
                        'created_by' => $destinationUserId,
                        'created_date' => date('Y-m-d H:i:s'),
                        'last_edit_by' => $destinationUserId,
                        'site_id' => $site_id,
                        'last_edit_date' => date('Y-m-d H:i:s')
                    );
                    $subid = $this->store_generic('subjects',$sub_data);
                    $subject_data = array(
                        'account_folder_id' => $afid,
                        'subject_id' => $subid,
                        'created_by' => $destinationUserId,
                        'created_date' => date('Y-m-d H:i:s'),
                        'last_edit_by' => $destinationUserId,
                        'site_id' => $site_id,
                        'last_edit_date' => date('Y-m-d H:i:s')
                    );
                    $afsid = $this->store_generic('account_folder_subjects', $subject_data);
                }
            // copy topics account_folder_subjects
            $topics = DB::table('account_folder_subjects')
                    ->where('account_folder_id','=',$item['account_folder_id'])
                    ->get();
             $topics = collect($topics)->map(function($x){ return (array) $x; })->toArray();
            foreach ($topics as $topic) {
                    $top = $this->convert_array(DB::table('subjects')
                    ->where('id','=',$topic['subject_id'])
                    ->get());

                $top_data = array(
                    'name' => $top['name'],
                    'account_id' => $destinationAccountId,
                    'created_by' => $destinationUserId,
                    'created_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $destinationUserId,
                    'site_id' => $site_id,
                    'last_edit_date' => date('Y-m-d H:i:s')
                );

                $tid = $this->store_generic('topics',$top_data);
                $topic_data = array(
                    'account_folder_id' => $afid,
                    'topic_id' => $tid,
                    'created_by' => $destinationUserId,
                    'created_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $destinationUserId,
                    'last_edit_date' => date('Y-m-d H:i:s')
                );
                
                $aftid = $this->store_generic('account_folder_topics',$topic_data);
            }
            // copy video tags
            $tags = DB::table('account_folders_meta_data as acmd')
                    ->select('acmd.*', 'af.*')
                    ->join('account_folders AS af', 'acmd.account_folder_id', '=', 'af.account_folder_id')
                    ->where('af.account_id','=',$sourceAccountId)
                    ->where('af.account_folder_id','=',$item['account_folder_id'])
                    ->where('af.folder_type','=',2)
                    ->where('af.active','=',1)
                    ->get();
            $tags = collect($tags)->map(function($x){ return (array) $x; })->toArray();
            if (!empty($tags) && isset($aftid)) {
                foreach ($tags as $tag) {
                    $tag_data = array(
                        'account_folder_id' => $aftid,
                        'meta_data_name' => $tag['meta_data_name'],
                        'meta_data_value' => $tag['meta_data_value'],
                        'created_by' => $destinationUserId,
                        'created_date' => date('Y-m-d H:i:s'),
                        'last_edit_by' => $destinationUserId,
                        'site_id' => $site_id,
                        'last_edit_date' => date('Y-m-d H:i:s')
                    );
                    $this->store_generic('account_folders_meta_data',$tag_data);
                }
            }
            // copy documents
            $this->copyDocumentForSampleVideoLibrary(
                    $item['account_folder_id'], $aftid, $destinationAccountId, $destinationUserId, $site_id);
        }
    }

    protected function copyDocumentForSampleVideoLibrary($account_folder_id, $new_account_folder_id, $destinationAccountId, $destinationUserId, $site_id) {
        $documents = DB::table('documents as doc')
                    ->select('doc.*', 'afd.*')
                    ->join('account_folder_documents AS afd', 'afd.document_id', '=', 'doc.id')
                    ->where('afd.account_folder_id','=',$account_folder_id)
                    ->where('doc.doc_type','=',2)
                    ->where('doc.published','=',1)
                    ->toSql();
            $documents = collect($documents)->map(function($x){ return (array) $x; })->toArray();

        if (empty($documents))
            return;

        foreach ($documents as $document) {
            $doc_data = array(
                'account_id' => $destinationAccountId,
                'created_by' => $destinationUserId,
                'created_date' => date('Y-m-d H:i:s'),
                'last_edit_by' => $destinationUserId,
                'last_edut_date' => date('Y-m-d H:i:s'),
                'post_rubric_per_video' => '1',
                'site_id' => $site_id
                    ) + $document;
            unset($doc_data['id']);
            $fdocdata = $this->get_filter_columns('documents', $doc_data);
            unset($fdocdata['id']);
            $docid = $this->store_generic('documents',$fdocdata);

            $fafddata = $this->get_filter_columns('account_folder_documents', $doc_data);
            $fafddata['account_folder_id'] = $new_account_folder_id;
            $fafddata['document_id'] = $docid;
            $fafddata['site_id'] = $site_id;
            unset($fafddata['id']);
            $afdid = $this->store_generic('account_folder_documents',$fafddata);
        }
    }

    protected function copyMyFilesForSampleAccount($sourceAccountId, $sourceUserId, $destinationAccountId, $destinationUserId, $site_id) {
        $InvitedUserAccount = $this->convert_array(DB::table('users_accounts')
                    ->where('user_id','=',$destinationUserId)
                    ->where('account_id','=',$destinationAccountId)
                    ->get());

        $role = $InvitedUserAccount['role_id'];

        $result = DB::table('account_folder_documents AS afd')
                    ->select('afd.*', 'af.*', 'doc.*')
                    ->join('account_folders AS af', 'afd.account_folder_id', '=', 'af.account_folder_id')
                    ->join('documents AS doc', 'afd.document_id', '=', 'doc.id')
                    ->where('af.account_id','=',$destinationAccountId)
                    ->where('af.created_by','=',$destinationUserId)
                    ->where('af.folder_type','=',3)
                    ->where('af.active','=',1)
                    ->where('af.is_sample','=',1)
                    ->get();
        $result = collect($result)->map(function($x){ return (array) $x; })->toArray();

        if (!empty($result)) {
            foreach ($result as $item) {
                DB::table('account_folders')->where('active', 0)->update(['account_folder_id' => $item['account_folder_id']]);
            }
        }

        $result = DB::table('account_folder_documents AS afd')
                    ->select('afd.*', 'af.*', 'doc.*')
                    ->join('account_folders AS af', 'afd.account_folder_id', '=', 'af.account_folder_id')
                    ->join('documents AS doc', 'afd.document_id', '=', 'doc.id')
                    ->where('af.account_id','=',$sourceAccountId)
                    ->where('af.created_by','=',$sourceUserId)
                    ->where('af.folder_type','=',3)
                    ->where('af.active','=',1)
                    ->where('af.is_sample','=',0)
                    ->get();
        
        $result = collect($result)->map(function($x){ return (array) $x; })->toArray();
        if (empty($result))
            return;

        foreach ($result as $item) {
            $account_folder_data = array(
                'account_id' => $destinationAccountId,
                'created_by' => $destinationUserId,
                'last_edit_by' => $destinationUserId,
                'site_id' => $site_id,
                'is_sample' => 1
                    ) + $item;
            $fafddata = $this->get_filter_columns('account_folders', $account_folder_data);
            unset($fafddata['account_folder_id']);
            $afid = $this->store_generic('account_folders', $fafddata);
            //$this->store_generic('account_folder_documents', $item);

            $det_doc_id = $item['id'];
            $det_doc_zencoder_output_id = $item['zencoder_output_id'];
            if ($role == '120' && ($item['title'] == 'Sibme Quick Start Guide 2015-16  Super User and Account Owner' || $item['original_file_name'] == 'Sibme - User Management-HD.mp4' || $item['original_file_name'] == 'Sibme - Creating Huddles-HD.mp4')) {
                continue;
            } else {
                $document_data = array(
                    'account_id' => $destinationAccountId,
                    'created_by' => $destinationUserId,
                    'created_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $destinationUserId,
                    'last_edit_date' => date('Y-m-d H:i:s'),
                    'post_rubric_per_video' => '1',
                    'site_id' => $site_id
                        ) + $item;
                $fddata = $this->get_filter_columns('documents', $document_data);
                unset($fddata['id']);
                $did = $this->store_generic('documents', $fddata);
            }
            $check_data = $this->convert_array(DB::table('document_files')
                    ->where('document_id','=',$det_doc_id)
                    ->where('default_web','=',true)
                    ->get());

            if (isset($check_data)) {

                $data = array(
                    'document_id' => $did,
                    'url' => @$check_data['url'],
                    'resolution' => 1080,
                    'duration' => @$check_data['duration'],
                    'file_size' => @$check_data['file_size'],
                    'default_web' => 1,
                    'transcoding_job_id' => $det_doc_zencoder_output_id,
                    'transcoding_status' => 3,
                    'site_id' => $site_id,
                    'created_at'=>date('Y-m-d H:i:s')
                );
                $dc = $this->store_generic('document_files', $data);
            }

            $account_folder_document_data = array(
                'account_folder_id' => $afid,
                'document_id' => $did,
                'site_id' => $site_id
                    ) + $item;
            $fafddata = $this->get_filter_columns('account_folder_documents', $account_folder_document_data);
            unset($fafddata['id']);
            $dcid = $this->store_generic('account_folder_documents',$fafddata);

            if ($document_data['doc_type'] == 1) {

                $this->copyVideoComment($item['id'], $did, $destinationUserId, $sourceUserId, 2, $site_id);
            }
        }
    }

    public function copyUsersToSampleHuddle($sourceHuddleId, $destinationHuddleId, $site_id) {
        if ($sourceHuddleId == $destinationHuddleId)
            return;

        $accountFolderUsers['AccountFolderUser'] = $this->convert_array(DB::table('account_folder_users')
                    ->where('account_folder_id','=',$sourceHuddleId)
                    ->get());

        if (empty($accountFolderUsers['AccountFolderUser']))
            return;

        foreach ($accountFolderUsers as $accountFolderUser) {
            $accountFolderUser['AccountFolderUser'] = $accountFolderUser;
            // ignore admin
            if ($accountFolderUser['AccountFolderUser']['role_id'] == 200)
                continue;
            $userInHuddle = $this->convert_array(DB::table('account_folder_users')
                    ->where('account_folder_id','=',$destinationHuddleId)
                    ->where('user_id','=',$accountFolderUser['AccountFolderUser']['user_id'])
                    ->get());

            if (empty($userInHuddle)) {
                $accountFolderUserData = array(
                    'account_folder_id' => $destinationHuddleId,
                    'site_id' => $site_id
                        ) + $accountFolderUser['AccountFolderUser'];

                unset($accountFolderUserData['account_folder_user_id']);
                
                $this->store_generic('account_folder_users', $accountFolderUserData);
            }
        }
    }

    public function copyGroupForSampleAccount($sourceAccountId, $destinationAccountId, $sourceUserId, $destinationUserId, $site_id) {
        // ignore if source and destination account is one
        if ($sourceAccountId == $destinationAccountId)
            return;
        // delete old sample group
        $oldGroups = DB::table('groups')
                    ->where('account_id','=',$destinationAccountId)
                    ->where('is_sample','=',1)
                    ->get();
        $oldGroups = collect($oldGroups)->map(function($x){return (array) $x;})->toArray();
        if (count($oldGroups) > 0 && !empty($oldGroups)) {
            foreach ($oldGroups as $key=>$group) {
                $oldGroupIds[] = $group['id'];
            }
             $this->delete_bulk($oldGroupIds);
             DB::table('groups')
             ->where('account_id','=',$destinationAccountId)
             ->where('is_sample','=',1)->delete();
        }
        // copy sample group to destination account
        $groups['Group'] = $this->convert_array(DB::table('groups')
                    ->where('account_id','=',$sourceAccountId)
                    ->get());
        if (empty($groups['Group']))
            return;

        foreach ($groups as $group) {
            $group['Group'] = $group;
            $group_data = array(
                'account_id' => $destinationAccountId,
                'created_by' => $destinationUserId,
                'created_date' => date('Y-m-d H:i:s'),
                'last_edit_by' => $destinationUserId,
                'last_edit_date' => date('Y-m-d H:i:s'),
                'name' => $group['Group']['name'],
                'is_sample' => 1,
                'site_id' => $site_id
            );
            $gid = $this->store_generic('groups',$group_data);

            $user_groups['UserGroup'] = $this->convert_array(DB::table('user_groups')
                    ->where('group_id','=',$group['Group']['id'])
                    ->get());

            if (empty($user_groups['UserGroup']))
                continue;

            $group_id = $gid;
            foreach ($user_groups as $user_group) {
                $user_group['UserGroup'] = $user_group;
                if ($user_group['UserGroup']['user_id'] == $sourceUserId) {
                    $group_user_id = $destinationUserId;
                } else {
                    $group_user_id = $user_group['UserGroup']['user_id'];
                }
                $user_group_data = array(
                    'group_id' => $group_id,
                    'user_id' => $group_user_id,
                    'created_by' => $destinationUserId,
                    'created_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $destinationUserId,
                    'last_edit_date' => date('Y-m-d H:i:s'),
                    'site_id' => $site_id
                );
                $this->store_generic('user_groups',$user_group_data);
            }
        }
    }

    protected function copyUsersToSampleAccount($sourceAccountId, $destinationAccountId, $site_id) {
        if ($sourceAccountId == $destinationAccountId)
            return;

        $userAccounts = DB::table('users_accounts')
                    ->where('account_id','=',$sourceAccountId)
                    ->get();
        $userAccounts = collect($userAccounts)->map(function($x){return (array) $x;})->toArray();
        if (empty($userAccounts))
            return;
        foreach ($userAccounts as $userAccount) {
            //ignore account owner
            if ($userAccount['role_id'] == 100)
                continue;
            $userId = $userAccount['user_id'];
            $userInAccount = $this->convert_array(DB::table('users_accounts')
                    ->where('account_id','=',$destinationAccountId)
                    ->where('user_id','=',$userId)
                    ->get());

            if (empty($userInAccount)) {
                $userAccountData = array(
                    'account_id' => $destinationAccountId,
                    'user_id' => $userId,
                    'site_id' => $site_id
                        ) + $userAccount;
                $userAccountData = $this->get_filter_columns('users_accounts', $userAccountData);
                unset($userAccountData['id']);
                $this->store_generic('users_accounts', $userAccountData);
            }
        }
    }

    public function copyAccountFolderDiscussion($sourceHuddleId, $destinationAccountId, $destinationHuddleId, $destinationUserId, $sourceUserId, $site_id) {
        $comments['Comment'] = $this->convert_array(DB::table('comments')
                    ->where('ref_type','=',1)
                    ->where('ref_id','=',$sourceHuddleId)
                    ->where('active','=',1)
                    ->get());

        if (count($comments['Comment']) == 0)
            return;

        $comment_maps = array();
        foreach ($comments as $comment) {
            $comment['Comment'] = $comment;
            $comment_user_id = $comment['Comment']['user_id'] == $sourceUserId ? $destinationUserId : $comment['Comment']['user_id'];
            $comment_data = array(
                'parent_comment_id' => isset($comment_maps[$comment['Comment']['parent_comment_id']]) ?
                        $comment_maps[$comment['Comment']['parent_comment_id']] : null,
                'ref_type' => 1,
                'ref_id' => $destinationHuddleId,
                'user_id' => $comment_user_id,
                'active' => 1,
                'created_by' => $comment_user_id,
                'last_edit_by' => $comment_user_id,
                'site_id' => $site_id
                    ) + $comment['Comment'];
            unset($comment_data['id']);
            $commentid = $this->store_generic('comments',$comment_data);
            $comment_maps[$comment['Comment']['id']] = $commentid;

            $attachs['Document'] = $this->convert_array(DB::table('documents as doc')
                    ->select('ca.comment_id', 'doc.*')
                    ->join('comment_attachments as ca', 'ca.document_id', '=', 'doc.id')
                    ->where('ca.comment_id','=',$comment['Comment']['id'])
                    ->get());

            if (count($attachs['Document']) == 0)
                continue;
            foreach ($attachs as $att) {
                $att['Document'] = $att;
                $document_data = array(
                    'account_id' => $destinationAccountId,
                    'doc_type' => 2,
                    'created_by' => $comment_user_id,
                    'last_edit_by' => $comment_user_id,
                    'post_rubric_per_video' => '1',
                    'site_id' => $site_id
                        ) + $att['Document'];
                unset($document_data['id']);
                $did = $this->store_generic('documents',$document_data);
                $att['CommentAttachment'] = $att;
                $att_data = array(
                    'comment_id' => $comment_maps[$att['CommentAttachment']['comment_id']],
                    'document_id' => $did,
                    'site_id' => $site_id
                );
                $this->store_generic('comment_attachments',$att_data);
            }
        }
    }

    public function copyAccountFolderDocument($sourceHuddleId, $sourceUserId, $destinationAccountId, $destinationHuddleId, $destinationUserId, $site_id) {
        // copy document
        $documents = DB::table('account_folder_documents')
                    ->where('account_folder_id','=',$sourceHuddleId)
                    ->get();
        $documents = collect($documents)->map(function($x){ return (array) $x; })->toArray();
        if (empty($documents))
            return;

        $doc_maps = $folder_doc_maps = array();
        foreach ($documents as $document) {
            $doc = $this->convert_array(DB::table('documents')
                    ->where('id','=',$document['document_id'])
                    ->get());
            
            if (!$doc)
                continue;

            $det_doc_id = $doc['id'];
            $det_doc_zencoder_output_id = $doc['zencoder_output_id'];

            $doc_data = array(
                'account_id' => $destinationAccountId,
                'created_by' => $destinationUserId,
                'last_edit_by' => $destinationUserId,
                'post_rubric_per_video' => '1',
                'site_id' => $site_id
                    ) + $doc;
            unset($doc_data['id']);
            $doc_id = $this->store_generic('documents', $doc_data);

            $check_data = $this->convert_array(DB::table('document_files')
                    ->where('document_id','=',$det_doc_id)
                    ->where('default_web','=',true)
                    ->get());

            if (!empty($check_data)) {

                $data = array(
                    'document_id' => $doc_id,
                    'url' => $check_data['url'],
                    'resolution' => 1080,
                    'duration' => $check_data['duration'],
                    'file_size' => $check_data['file_size'],
                    'default_web' => 1,
                    'transcoding_job_id' => $det_doc_zencoder_output_id,
                    'transcoding_status' => 3,
                    'site_id' => $site_id,
                    'created_at'=>date('Y-m-d H:i:s')
                );

               $this->store_generic('document_files', $data);
            }


            $doc_maps[$doc['id']] = $doc_id;

            if ($doc['doc_type'] == 1) {
                $this->copyVideoComment($doc['id'], $doc_id, $destinationUserId, $sourceUserId, 2, $site_id);
            }

            $doc = $document;
            $account_folder_document_data = array(
                'account_folder_id' => $destinationHuddleId,
                'document_id' => $doc_id
                    ) + $doc;
            unset($account_folder_document_data['id']);
            $afd = $this->store_generic('account_folder_documents',$account_folder_document_data);
            $folder_doc_maps[$doc['id']] = $afd;
        }
        $attachments = $this->custom_query($sourceHuddleId);
        $attachments = collect($attachments)->map(function($x){ return (array) $x; })->toArray();
        if (count($attachments) > 0 && !empty($attachments)) {
            foreach ($attachments as $att) {
                $ab['account_folderdocument_attachments'] = $att;
                if (isset($folder_doc_maps[$ab['account_folderdocument_attachments']['account_folder_document_id']]) && isset($doc_maps[$ab['account_folderdocument_attachments']['attach_id']])) {
                    $this->store_generic('account_folderdocument_attachments', array(
                    'account_folder_document_id' => @$folder_doc_maps[$ab['account_folderdocument_attachments']['account_folder_document_id']],
                    'attach_id' => @$doc_maps[$ab['account_folderdocument_attachments']['attach_id']]
                ));
                }
            }
        }
    }

    public function copyVideoComment($sourceVideoId, $destinationVideoId, $destinationUserId, $sourceUserId, $refType = 2, $site_id) {

        $comments = DB::table('comments')
                    ->where('ref_type','=',$refType)
                    ->where('ref_id','=',$sourceVideoId)
                    ->where('active','=',1)
                    ->get();
        $comments = collect($comments)->map(function($x){ return (array) $x;})->toArray();
        if (count($comments) == 0)
            return;
        
        foreach ($comments as $comment) {
            $get_tags = DB::table('account_comment_tags as act')
                    ->select('act.*')
                    ->where('act.ref_type','=',1)
                    ->where('act.comment_id','=',$comment['id'])
                    ->get();
            $get_tags = collect($get_tags)->map(function($x){ return (array) $x;})->toArray();
            $acc = $this->convert_array(DB::table('users_accounts')
                    ->where('user_id','=',$destinationUserId)
                    ->get());

            $comment_user_id = $comment['user_id'] == $sourceUserId ? $destinationUserId : $comment['user_id'];

            $comment_data = array(
                'parent_comment_id' => ($refType == 3) ? $destinationVideoId : null,
                'ref_type' => $refType,
                'ref_id' => $destinationVideoId,
                'user_id' => $comment_user_id,
                'created_by' => $comment_user_id,
                'last_edit_by' => $comment_user_id,
                'comment' => $comment['comment'],
                'time' => $comment['time'],
                'created_date' => $comment['created_date'],
                'last_edit_date' => $comment['last_edit_date'],
                'site_id' => $site_id
            );
            unset($comment_data['id']);
            $commentid = $this->store_generic('comments',$comment_data);
            if ($commentid) {
                if (!empty($get_tags)) {
                    foreach ($get_tags as $tag) {
                        $commentTag = $this->convert_array(DB::table('account_tags')
                            ->where('account_id','=',$acc['account_id'])
                            ->where('tag_title','=',$tag['tag_title'])
                            ->get());

                        $comment_tag_data = array(
                            'comment_id' => $commentid,
                            'ref_id' => $destinationVideoId,
                            'ref_type' => 1,
                            'tag_title' => $tag['tag_title'],
                            'account_tag_id' => isset($commentTag['account_tag_id']) ? $commentTag['account_tag_id'] : 0,
                            'created_by' => $comment_user_id,
                            'site_id' => $site_id
                        );
                        $this->store_generic('account_comment_tags',$comment_tag_data);
                    }
                }
            }

            $this->copyVideoComment($comment['id'], $commentid, $destinationUserId, $sourceUserId, 3, $site_id);
        }
    }

    public function store_account_folder($destinationUserId, $destinationAccountId, $foldername, $desc, $site_id){
        try
        {
           $account_folder_data = array(
                'account_id' => (int) $destinationAccountId,
                'folder_type' => 1,
                'name' => $foldername,
                'desc' => iconv('UTF-8', 'ASCII//TRANSLIT', $desc), 
                'created_by' => $destinationUserId,
                'created_date' => date('Y-m-d H:i:s'),
                'active' => 1,
                'site_id' => $site_id,
                'is_sample' => 1
            );
            $last_insert_id = DB::table('account_folders')->insertGetId($account_folder_data);
            return $last_insert_id;
        }
        catch(Exception $ex)
        {
            throw $ex;
        }
    }

    public function store_generic($tablename, $data){
        try
        {
            $id = DB::table($tablename)->insertGetId($data);
            return $id;
        }
        catch(Exception $ex)
        {
            throw $ex;
        }
    }

    public function convert_array($data){
        $data = collect($data)->map(function($x){ 
                return (array) $x; 
            })->toArray();
        return $data ? $data[0] : $data;
    }

    public function custom_query($huddleid){
        $sql = "select * from account_folderdocument_attachments where attach_id in (select document_id from account_folder_documents where account_folder_id = $huddleid)";
        $data = DB::select($sql);
        return $data;
    }

    public function delete_bulk($ids){
        $mids = implode(',', $ids);
        $sql = "DELETE FROM user_groups WHERE group_id IN ($mids)";
        $data = DB::select($sql);
        return $data;
    }

    public function get_filter_columns($table_name, $columns_array){
        $data = DB::getSchemaBuilder()->getColumnListing($table_name);
        $flipped = array_flip($data);
        $result=array_intersect_key($columns_array,$flipped);
        return $result;
    }

    public function check_email($data, $site_id){
         $resp = DB::table('users')
            ->where('email','=',$data['email'])
            ->where('site_id','=',$site_id)
            ->get();
        $resp = collect($resp)->map(function($x){ 
                return (array) $x; 
            })->toArray();
        return $resp;

    }

    public function update_resend_code_info($account_id, $user_id, $site_id){
        $account_details = $this->convert_array(DB::table('accounts')
                ->where('id','=',$account_id)
                ->where('site_id','=',$site_id)
                ->get());

        $user_details = $this->convert_array(DB::table('users')
                ->where('id','=',$user_id)
                ->where('site_id','=',$site_id)
                ->get());
        
        $verification_code = uniqid();        
        $res = DB::table('users')
                ->where('id', $user_id)
                ->where('site_id','=',$site_id)
                ->update(['verification_code' => $verification_code]);
        
        $data = array(
            'email' => $user_details['email'],
            'full_name' => $user_details['first_name'] . ' ' . $user_details['last_name'],
            'company' => $account_details['company_name'],
            'user_id' => $user_id,
            'verification_code' => $verification_code
        );

        return $data;
    }
}