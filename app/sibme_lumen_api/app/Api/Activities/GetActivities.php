<?php

/**
 * Created by PhpStorm.
 * User: Hassan Raza
 * Date: 14/02/2019
 * Time: 9:00 PM
 */

namespace App\Api\Activities;

use App\Http\Controllers\HuddleController;
use App\Http\Controllers\VideoController;
use App\Models\Goal;
use App\Models\User;
use App\Models\UserActivityLog;
use App\Models\AccountFolder;
use GuzzleHttp\Client;
use App\Services\TranslationsManager;
use App\Services\HelperFunctions;

class GetActivities {

    protected $base;
    public static $translation_data;

    public function __construct() {
        $this->base = config('s3.sibme_base_url');
        $lang = app("Illuminate\Http\Request")->header('current-lang');
        $site_id = app("Illuminate\Http\Request")->header('site_id');
        $user_id = app("Illuminate\Http\Request")->get('user_id');
        self::$translation_data = TranslationsManager::get_page_lang_based_content('Dashboard/home',$site_id,$user_id,$lang);
     
    }

    public static function get_ctivities($activityLogs, $user_id, $site_id, $account_id, $role_id,$lang = '',$goal_evidence = 0) {
        if(!empty($lang))
        {
           $lang = $lang;
        }
        else
        {
           $lang = app("Illuminate\Http\Request")->header('current-lang');
        }
        $live_recording_check = '0';
        $site_id = app("Illuminate\Http\Request")->header('site_id');
        $user_id = app("Illuminate\Http\Request")->get('user_id');        
        self::$translation_data = TranslationsManager::get_page_lang_based_content('Dashboard/home',$site_id,$user_id,$lang);
        

        $recent_activity_des = '';
        $accountFoldereIds = array();
        $accountFolderUsers = \App\Models\AccountFolderUser::get($user_id, $site_id);
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds[] = $row['account_folder_id'];
            }
            $accountFoldereIds = implode(",", $accountFoldereIds);
        } else {
            $accountFoldereIds = '0';
        }

        $accountFolderGroups = \App\Models\UserGroup::get_user_group($user_id, $site_id);
        $accountFolderGroupsIds = array();
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_id'];
            }
            $accountFolderGroupsIds = implode(",", $accountFolderGroupsIds);
        } else {
            $accountFolderGroupsIds = '0';
        }

        $myHuddles = \App\Models\AccountFolder::getAllHuddlesDashboard($account_id, 'name', FALSE, $accountFoldereIds, $accountFolderGroupsIds);
      
        if (!empty($activityLogs)) {
            $allactivities = [];
            $goals_name = HelperFunctions::getGoalsAltName($account_id, $site_id);
            foreach ($activityLogs as $row):
                if(!is_array($row))
                {
                    $row = (array)$row;
                }
                $live_recording_check = '0';
                $type = $row['type'];
                $ref_id = $row['ref_id'];
                $activityLogs_users = '';
                $activityDescription = $row['desc'];
                $activityType = $row['type'];
                $activityUrl = $row['url'];
                $activityDate = $row['date_added'];
                $id = '';
                

                if ($row['user_id'] == $user_id) {
                    $activityLogs_users = self::$translation_data['workspace_you'];
                   
                } else {
                    $activityLogs_users = $row['first_name'] . " " . $row['last_name'];
                }
               
               if(isset($row['account_folder_id']) && !empty($row['account_folder_id']))
               {
                   $huddle_data = \App\Models\AccountFolder::where(array(
                         'account_folder_id' => $row['account_folder_id'],
                         'site_id' => $site_id
                     ))->first();
                   if($huddle_data)
                   {
                       $huddle_data = $huddle_data->toArray();
                   }
                   else
                   {
                       $huddle_data = array();
                   }
                   $huddle_party = \App\Models\AccountFolderUser::where(array(
                         'user_id' => $user_id,
                         'account_folder_id' => $row['account_folder_id'],
                         'site_id' => $site_id
                     ))->get()->toArray();
                   if(isset($huddle_data['folder_type']) && $huddle_data['folder_type'] == '1' && empty($huddle_party) && $goal_evidence == 0 )
                   {
                       continue;
                   }
                   
               }
                   

                $comment = '';
                $comment2 = '';
                $bool = false;
                $huddle_name = \App\Models\AccountFolder::getHuddleName($row['account_folder_id'], $site_id);
                if (isset($type) && $type == '1') {
                    
                $huddle_data = \App\Models\AccountFolder::where(array(
                         'account_folder_id' => $row['account_folder_id'],
                         'site_id' => $site_id
                     ))->first();
                
               
                
                   if($huddle_data)
                   {
                       $huddle_data = $huddle_data->toArray();
                       if($huddle_data['is_published'] == '0' && \App\Models\AccountFolderMetaData::check_if_eval_huddle($row['account_folder_id'], $site_id))
                       {
                           continue;
                       }
                   }
                   
                   
                $huddle_invitation_afterwards = \App\Models\UserActivityLog::where(array(
                         'account_folder_id' => $row['account_folder_id'],
                         'site_id' => $site_id,
                         'type' => '18',
                         'ref_id' => $user_id
                     ))->first();
                
                if($huddle_invitation_afterwards)
                {
                    continue;
                }
                    
                    $comment2 = self::$translation_data['workspace_create_huddle'];
                    if ($row['user_id'] == $user_id) {
                        $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $row['account_folder_id'], 0, 1);
                    } elseif ($row['user_id'] != $user_id && \App\Models\AccountFolderUser::user_present_huddle($user_id, $row['account_folder_id'])) {
                        $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $row['account_folder_id'], 0, 2);
                    } else {
                        continue;
                        // $recent_activity_des = self::Custom->log_activity($activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate,$row['account_folder_id'],0,2);
                    }
                } elseif (isset($type) && $type == '18') {


                    $user_id_per = $user_id;
                    $role_id_per = $role_id;
                    if (self::_check_evaluator_permissions_invite($row['account_folder_id'], $user_id_per, $row['ref_id'], $site_id) == false) {
                        continue;
                    }
                    if ($type == '18' && $row['account_folder_id'] != '') {
                        $comment2 = self::$translation_data['workspace_user_invited'];
                        $username = self::findByid($row['ref_id'], $site_id);
                        $activityDescription = $username['first_name'] . "  " . $username['last_name'];
                        
                        if($row['ref_id'] == $user_id)
                        {
                            $lang = app("Illuminate\Http\Request")->header('current-lang');
                            if($lang == 'es')
                            {
                                $activityDescription= 't�';
                            }
                            else
                            {
                                $activityDescription= 'you';
                            }
                        }
                        
                        $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, '', 0, 1, $row['account_folder_id']);
                    } else {
                        $comment2 = self::$translation_data['workspace_user_invited'];
                        $username = self::findByid($row['ref_id'], $site_id);
                        $activityDescription = $username['first_name'] . "  " . $username['last_name'];
                        $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, '', $activityDescription, $activityUrl, $activityDate);
                    }
                } elseif (isset($type) && $type == '2') {
                    if ($row['user_id'] != $user_id && !\App\Models\AccountFolderUser::user_present_huddle_count($user_id, $row['account_folder_id'])) {
                        continue;
                    }
                    $comment2 = self::$translation_data['workspace_upload_video'];
                    $video_name = \App\Models\AccountFolderDocument::get_document_name($ref_id, $site_id);
                    $video_name = isset($video_name) ? $video_name : self::$translation_data['workspace_untitled_video'];
                    if (strlen($video_name) > 50) {
                        $video_name = substr($video_name, 0, 50) . '...';
                    }
                   
                   
                    $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $video_name, $activityDescription, $activityUrl, $activityDate, $huddle_name, $ref_id, 1, $row['account_folder_id']);
                   
                } elseif (isset($type) && $type == '27') {
                    if ($row['user_id'] != $user_id && !\App\Models\AccountFolderUser::user_present_huddle_count($user_id, $row['account_folder_id'])) {
                        continue;
                    }
                    $evaluators_ids = self::get_evaluator_ids($row['account_folder_id'], $site_id);
                    $htype = HelperFunctions::get_huddle_type($row['account_folder_id']);
                    if ($htype == '3' && VideoController::check_if_evaluated_participant($row['account_folder_id'], $user_id) && $row['user_id'] != $user_id && !in_array($row['user_id'],$evaluators_ids) ) {
                        continue;
                    }
                    $comment2 = self::$translation_data['workspace_upload_video'];
                    $video_name = \App\Models\AccountFolderDocument::get_document_name($ref_id, $site_id);
                    $video_name = isset($video_name) ? $video_name : self::$translation_data['workspace_untitled_video'];
                    if (strlen($video_name) > 50) {
                        $video_name = substr($video_name, 0, 50) . '...';
                    }
                    $shared_huddle_id = explode("/",$row["url"]);
                    $shared_huddle_id = $shared_huddle_id[3];
                    $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $video_name, $activityDescription, $activityUrl, $activityDate, $huddle_name, $ref_id, 1, $shared_huddle_id);
                }elseif (isset($type) && $type == '22') {
                    if ($row['user_id'] != $user_id && !\App\Models\AccountFolderUser::user_present_huddle_count($user_id, $row['account_folder_id'])) {
                        continue;
                    }
                    $evaluators_ids = self::get_evaluator_ids($row['account_folder_id'], $site_id);
                    $htype = HelperFunctions::get_huddle_type($row['account_folder_id']);
                    if ($htype == '3' && VideoController::check_if_evaluated_participant($row['account_folder_id'], $user_id) && $row['user_id'] != $user_id &&(is_array($evaluators_ids) && !in_array($row['user_id'],$evaluators_ids)) ) {
                        continue;
                    }
                    
                    if($htype == '3' && !VideoController::check_if_evaluated_participant($row['account_folder_id'], $user_id) && (is_array($evaluators_ids) && !in_array($row['user_id'],$evaluators_ids)))
                    {
                        continue;
                    }
                    
                    $comment2 = self::$translation_data['workspace_upload_video'];
                    $video_name = \App\Models\AccountFolderDocument::get_document_name($ref_id, $site_id);
                    $video_name = isset($video_name) ? $video_name : self::$translation_data['workspace_untitled_video'];
                    if (strlen($video_name) > 50) {
                        $video_name = substr($video_name, 0, 50) . '...';
                    }
                    $get_video_details = \App\Models\Document::get_video_details($row['ref_id'], $site_id);
                    if($get_video_details['doc_type'] == '2')
                    {
                        $activityUrl = $get_video_details['stack_url'];
                    }
                    $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $video_name, $activityDescription, $activityUrl, $activityDate, $huddle_name, $ref_id, 1, $row['account_folder_id'],$row['user_id'],$user_id);
                } elseif (isset($type) && $type == '3') {
                    if ($row['user_id'] != $user_id && !\App\Models\AccountFolderUser::user_present_huddle_count($user_id, $row['account_folder_id'])) {
                        continue;
                    }
                    $huddle_data = \App\Models\AccountFolder::where(array(
                        'account_folder_id' => $row['account_folder_id'],
                        'site_id' => $site_id
                    ))->first();
                    $evaluators_ids = self::get_evaluator_ids($row['account_folder_id'], $site_id);
                    $htype = HelperFunctions::get_huddle_type($row['account_folder_id']);
                    if ($htype == '3' && VideoController::check_if_evaluated_participant($row['account_folder_id'], $user_id) && $row['user_id'] != $user_id && !in_array($row['user_id'],$evaluators_ids) ) {
                        continue;
                    }
                    if($htype == '3' && !VideoController::check_if_evaluated_participant($row['account_folder_id'], $user_id) && !in_array($row['user_id'],$evaluators_ids))
                    {
                        continue;
                    } 
                   $video_id = $row['ref_id'];
                    $get_video_details = \App\Models\Document::get_video_details($video_id, $site_id);
                    $user_id_per = $user_id;
                    $role_id_per = $role_id;
                    if (self::_check_evaluator_permissions($row['account_folder_id'], $get_video_details['created_by'], $user_id_per, $role_id_per, $site_id) == false) {
                        continue;
                    }
                    if ($row['user_id'] == $user_id) {
                        $comment2 = self::$translation_data['workspace_you'];
                    } else {
                        $comment2 = $row['first_name'] . " " . $row['last_name'];
                    }
                    $document_name = \App\Models\AccountFolderDocument::get_document_name($ref_id, $site_id);
                    if (strlen($document_name) > 50) {
                        $document_name = substr($document_name, 0, 50) . '...';
                    }
                    $activityUrl = $get_video_details["stack_url"];
                    $huddle_id = $row['account_folder_id'];
                    // if($huddle_data['folder_type'] ==1){

                    // }elseif($huddle_data['folder_type'] ==2){

                    // }elseif($huddle_data['folder_type'] ==3){

                    // }
                    //$activityUrl ="/home/video_huddles/huddle/details/$huddle_id/artifacts/grid";
                    $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $document_name, $activityDescription, $activityUrl, $activityDate, $huddle_name, 0, 1, $row['account_folder_id'],$row['user_id'],$user_id);
                } elseif (isset($type) && $type == '4') {
                    $activityUrl = "/home/library_video/home/".$row['account_folder_id']."/".$row['ref_id'];
                    $comment2 = self::$translation_data['workspace_video_library'];
                    $video_title = $huddle_name;
                    $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate);
                } elseif (isset($type) && $type == '5') {
                    if ($row['user_id'] != $user_id && !\App\Models\AccountFolderUser::user_present_huddle_count($user_id, $row['account_folder_id'])) {
                        continue;
                    }
                    $video_comments = \App\Models\Comment::get($ref_id, $site_id);
                    if (!empty($video_comments)) {
                        $get_video_details = \App\Models\Document::get_video_details($video_comments['ref_id'], $site_id);
                        $user_id_per = $user_id;
                        $role_id_per = $role_id;
                        if (self::_check_evaluator_permissions($row['account_folder_id'], $get_video_details['created_by'], $user_id_per, $role_id_per, $site_id) == false) {
                            continue;
                        }
                        $video_title = \App\Models\AccountFolderDocument::get_document_name(isset($video_comments['ref_id']) ? $video_comments['ref_id'] : '', $site_id);
                        $video_id = $video_comments['ref_id'];
                        if ($video_comments['ref_type'] == '6') {
                            $bool = true;
                        }

                        if (isset($video_comments) && $video_comments['active'] == '1') {
                            $comment = isset($video_comments['comment']) ? $video_comments['comment'] : '';
                            if (strlen($comment) > 20) {
                                $comment = substr($comment, 0, 20) . '...';
                            }
                            $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $huddle_name, $comment, $activityUrl, $activityDate, $video_title, $video_id, 1, $row['account_folder_id']);
                        }
                    }
                } elseif (isset($type) && $type == '6') {
                    if ($row['user_id'] != $user_id && !\App\Models\AccountFolderUser::user_present_huddle_count($user_id, $row['account_folder_id'])) {
                        continue;
                    }
                    $orignal_comment = \App\Models\Comment::get_parent_discussion($ref_id, $site_id);
                    if (count($orignal_comment) > 0) {

                        if ($orignal_comment['active'] == '1') {

                            $commnet_title = $orignal_comment['title'];
                            $comment = $activityDescription;
                            $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $commnet_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, 0, 1, $row['account_folder_id']);
                        }
                    } else {
                        $recent_activity_des = '';
                    }
                } elseif (isset($type) && $type == '8') {
                    if ($row['user_id'] != $user_id && !\App\Models\AccountFolderUser::user_present_huddle_count($user_id, $row['account_folder_id'])) {
                        continue;
                    }
                    $child_comments = \App\Models\Comment::get_parent_discussion($row['ref_id'], $site_id);

                    if (!empty($child_comments) && $child_comments['parent_comment_id'] != '') {
                        $topic_name = \App\Models\Comment::get_parent_discussion($child_comments['parent_comment_id'], $site_id);

                        if (isset($topic_name['active']) && $topic_name['active'] == '1') {

                            $commnet_title = $topic_name['title'];
                            $comment = $activityDescription;
                            $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $commnet_title, $activityDescription, $activityUrl, $activityDate, $huddle_name);
                        }
                    } else {
                        $recent_activity_des = '';
                    }
                } elseif (isset($type) && $type == '7') {

                    if ($type == '7' && $row['account_folder_id'] != '') {
                        $comment2 = self::$translation_data['workspace_user_invited'];
                        $username = self::findByid($row['ref_id'], $site_id);

                        $activityDescription = $username['first_name'] . "  " . $username['last_name'];
                        $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate);
                    } else {
                        $comment2 = self::$translation_data['workspace_user_invited'];
                        $username = self::findByid($row['ref_id'], $site_id);
                        $activityDescription = $username['first_name'] . "  " . $username['last_name'];
                        $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, '', $activityDescription, $activityUrl, $activityDate);
                    }
                } elseif (isset($type) && $type == '17') {
                    if ($huddle_name != '') {
                        continue;
                    }
                    if ($type == '17' && $row['account_folder_id'] != '') {
                        $comment2 = self::$translation_data['workspace_user_invited'];
                        $username = self::findByid($row['ref_id'], $site_id);
                        $activityDescription = $username['first_name'] . "  " . $username['last_name'];
                        $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate);
                    } else {
                        $comment2 = self::$translation_data['workspace_user_invited'];
                        $username = self::findByid($row['ref_id'], $site_id);
                        $activityDescription = $username['first_name'] . "  " . $username['last_name'];
                        $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, '', $activityDescription, $activityUrl, $activityDate);
                    }
                } elseif (isset($type) && $type == '19') {
                    $user_id_per = $user_id;
                    $role_id_per = $role_id;
                    if (self::_check_evaluator_permissions_invite($row['account_folder_id'], $user_id_per, $row['ref_id'], $site_id) == false) {
                        continue;
                    }
                    if ($type == '19' && $row['account_folder_id'] != '') {
                        $comment2 = self::$translation_data['workspace_user_invited'];
                        $username = self::findByid($row['ref_id'], $site_id);
                        $activityDescription = $username['first_name'] . "  " . $username['last_name'];
                        $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, '', 0, 1, $row['account_folder_id']);
                    } else {
                        $comment2 = self::$translation_data['workspace_user_invited'];
                        $username = self::findByid($row['ref_id'], $site_id);
                        $activityDescription = $username['first_name'] . "  " . $username['last_name'];
                        $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, '', $activityDescription, $activityUrl, $activityDate);
                    }
                } elseif (isset($type) && $type == '20') {
                    if ($row['user_id'] != $user_id && !\App\Models\AccountFolderUser::user_present_huddle_count($user_id, $row['account_folder_id'])) {
                        continue;
                    }
                    if (VideoController::check_if_evaluated_participant($row['account_folder_id'], $user_id)) {
                        continue;
                    }
                    $comment2 = self::$translation_data['workspace_observation_started'];
                    $video_name = \App\Models\AccountFolderDocument::get_document_name($ref_id, $site_id);
                    $video_name = isset($video_name) ? $video_name : self::$translation_data['workspace_untitled_video'];
                    if (strlen($video_name) > 50) {
                        $video_name = substr($video_name, 0, 50) . '...';
                    }
                    $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $video_name, $activityDescription, $activityUrl, $activityDate, $huddle_name, $row['ref_id'], 1, $row['account_folder_id']);
                    if (!self::check_if_recording_stopped($row['ref_id'])) {
                        $live_recording_check = '1';
                        // Following line is creating problem in get_user_activities -> Error: Array to string conversion
                        // Create or check for a function to add activity for Live Recording.
                        // Temporarily Disabled
                        // $recent_activity_des = $recent_activity_des . '<img src="/img/recording_img.png" id="img_recording_live" style="margin-right: 0px;margin-left: 10px;"> Live Recording';
                    }

                    $id = "processing-message-" . $row['id'];
                } elseif (isset($type) && $type == '21') {
                    if ($row['user_id'] != $user_id && !\App\Models\AccountFolderUser::user_present_huddle_count($user_id, $row['account_folder_id'])) {
                        continue;
                    }
                    if (VideoController::check_if_evaluated_participant($row['account_folder_id'], $user_id)) {
                        continue;
                    }
                    $comment2 = self::$translation_data['workspace_observation_stopped'];
                    $video_name = \App\Models\AccountFolderDocument::get_document_name($row['ref_id'], $site_id);
                    $video_name = isset($video_name) ? $video_name :self::$translation_data['workspace_untitled_video']; 
                    if (strlen($video_name) > 50) {
                        $video_name = substr($video_name, 0, 50) . '...';
                    }
                    $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $video_name, $activityDescription, $activityUrl, $activityDate, $huddle_name, $row['ref_id'], 1, $row['account_folder_id']);
                }
                
                elseif (isset($type) && $type == '28') {
                    $comment2 = self::$translation_data['workspace_observation_stopped'];
                    if ($row['user_id'] != $user_id && !\App\Models\AccountFolderUser::user_present_huddle_count($user_id, $row['account_folder_id'])) {
                        continue;
                    }
                    $evaluators_ids = self::get_evaluator_ids($row['account_folder_id'], $site_id);
                    $htype = HelperFunctions::get_huddle_type($row['account_folder_id']);
                    if ($htype == '3' && VideoController::check_if_evaluated_participant($row['account_folder_id'], $user_id) && $row['user_id'] != $user_id && !in_array($row['user_id'],$evaluators_ids) ) {
                        continue;
                    }
                    
                    $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, '', $activityDescription, $activityUrl, $activityDate, $huddle_name, $ref_id, 1, $row['account_folder_id'],$row['user_id'],$user_id);
                }
                elseif (isset($type) && $type == '29') {
                    
                    if ($row['user_id'] != $user_id && !\App\Models\AccountFolderUser::user_present_huddle_count($user_id, $row['account_folder_id'])) {
                        continue;
                    }
                    
                    $evaluators_ids = self::get_evaluator_ids($row['account_folder_id'], $site_id);
                    $htype = HelperFunctions::get_huddle_type($row['account_folder_id']);
                    if ($htype == '3' && VideoController::check_if_evaluated_participant($row['account_folder_id'], $user_id) && $row['user_id'] != $user_id && !in_array($row['user_id'],$evaluators_ids) ) {
                        continue;
                    }
                    if($htype == '3' && !VideoController::check_if_evaluated_participant($row['account_folder_id'], $user_id) && !in_array($row['user_id'],$evaluators_ids))
                    {
                        continue;
                    } 
                    $video_id = $row['ref_id'];
                    $get_video_details = \App\Models\Document::get_video_details($video_id, $site_id);
                    $user_id_per = $user_id;
                    $role_id_per = $role_id;
                    if (self::_check_evaluator_permissions($row['account_folder_id'], $get_video_details['created_by'], $user_id_per, $role_id_per, $site_id) == false) {
                        continue;
                    }
                    if ($row['user_id'] == $user_id) {
                            $comment2 = self::$translation_data['workspace_you'];
                    } else {
                        $comment2 = $row['first_name'] . " " . $row['last_name'];
                    }
                    $document_name = \App\Models\AccountFolderDocument::get_document_name($ref_id, $site_id);
                    if (strlen($document_name) > 50) {
                        $document_name = substr($document_name, 0, 50) . '...';
                    }
                   
                    $activityUrl = $get_video_details["url"];
                    $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $document_name, $activityDescription, $activityUrl, $activityDate, $huddle_name, 0, 1, $row['account_folder_id'],$row['user_id'],$user_id);
                   // var_dump( $recent_activity_des );
                   //continue;
                }
                elseif (isset($type) && $type == '30') {
                    if ($row['user_id'] != $user_id && !\App\Models\AccountFolderUser::user_present_huddle_count($user_id, $row['account_folder_id'])) {
                        continue;
                    }
                    $evaluators_ids = self::get_evaluator_ids($row['account_folder_id'], $site_id);
                    $htype = HelperFunctions::get_huddle_type($row['account_folder_id']);
                    if ($htype == '3' && VideoController::check_if_evaluated_participant($row['account_folder_id'], $user_id) && $row['user_id'] != $user_id && !in_array($row['user_id'],$evaluators_ids) ) {
                        continue;
                    }
                    if($htype == '3' && !VideoController::check_if_evaluated_participant($row['account_folder_id'], $user_id) && !in_array($row['user_id'],$evaluators_ids))
                    {
                        continue;
                    } 
                    $video_id = $row['ref_id'];
                    $get_video_details = \App\Models\Document::get_video_details($video_id, $site_id);
                    $user_id_per = $user_id;
                    $role_id_per = $role_id;
                    if (self::_check_evaluator_permissions($row['account_folder_id'], $get_video_details['created_by'], $user_id_per, $role_id_per, $site_id) == false) {
                        continue;
                    }
                    if ($row['user_id'] == $user_id) {
                            $comment2 = self::$translation_data['workspace_you'];
                    } else {
                        $comment2 = $row['first_name'] . " " . $row['last_name'];
                    }
                    $document_name = \App\Models\AccountFolderDocument::get_document_name($ref_id, $site_id);
                    if (strlen($document_name) > 50) {
                        $document_name = substr($document_name, 0, 50) . '...';
                    }
                    $activityUrl = $get_video_details["url"];
                    $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $document_name, $activityDescription, $activityUrl, $activityDate, $huddle_name, 0, 1, $row['account_folder_id'],$row['user_id'],$user_id);
                }
                elseif (isset($type) && $type == '24') {
                    $comment2 = self::$translation_data['live_streaming_started'];
                    $live_recording_check = '1';
                    $evaluators_ids = self::get_evaluator_ids($row['account_folder_id'], $site_id);
                    $htype = HelperFunctions::get_huddle_type($row['account_folder_id']);
                    if ($htype == '3' && VideoController::check_if_evaluated_participant($row['account_folder_id'], $user_id) && $row['user_id'] != $user_id && !in_array($row['user_id'],$evaluators_ids) ) {
                        continue;
                    }
                    $video_name = \App\Models\AccountFolderDocument::get_document_name($row['ref_id'], $site_id);
                    $video_name = isset($video_name) ? $video_name :self::$translation_data['workspace_untitled_video'];
                    if (strlen($video_name) > 50) {
                        $video_name = substr($video_name, 0, 50) . '...';
                    }
                    $recent_activity_des = self::log_activity($site_id, $activityType, $activityLogs_users, $video_name, $activityDescription, $activityUrl, $activityDate, $huddle_name, $ref_id, 1, $row['account_folder_id'],$row['user_id'],$user_id);
                }
                elseif (isset($type) && $type >= Goal::activityTypes['created_for_self'] && $type <= Goal::activityTypes['goal_deadline_7_days'])
                {
                    $recent_activity_des = [];
                    $recent_activity_des["activityDate"] = $row['date_added'];
                    $recent_activity_des["resource_url"] = $row['url'];
                    $recent_activity_des["user_id"] = $row['user_id'];
                    $recent_activity_des["resource_video_name"] = "";
                    $recent_activity_des["activityLogs_users"] = $activityLogs_users;
                    $recent_activity_des["document_id"] = 0;
                    $activity_desc = Goal::translations[$type][$lang];
                    $itemName_specialText = explode("___", $row['desc']);
                    $activity_desc = str_replace('{ITEM_NAME}', $itemName_specialText[0], $activity_desc);
                    if(isset($itemName_specialText[1]))
                    {
                        $special_lang = explode("||", $itemName_specialText[1]);
                        if(isset($special_lang[1]))
                        {
                            $activity_desc = str_replace('{SPECIAL}', ($lang == "en"?$special_lang[0]:$special_lang[1]), $activity_desc);
                        }
                        else
                        {
                            $activity_desc = str_replace('{SPECIAL}', $special_lang[0], $activity_desc);
                        }
                    }
                    if(isset($row['source_ref_id']) && !empty($row['source_ref_id']))
                    {
                        if ($row['source_ref_id'] == $user_id)
                        {
                            $other_user = self::$translation_data['workspace_you'];
                        }
                        else
                        {
                            $source_user = User::where("id",$row['source_ref_id'])->first();
                            $other_user = $source_user->first_name." ".$source_user->last_name;
                        }

                        $activity_desc = str_replace('{OTHER_USER_NAME}', $other_user,  $activity_desc);
                    }
                    $activity_desc = HelperFunctions::getGoalAltTranslation($activity_desc, $account_id, $site_id, $goals_name);
                    $comment2 = $recent_activity_des["resource_name"] = $activity_desc;
                }
               

                if (isset($comment) && ($comment != '' || $comment2 != '')):

                    if ($recent_activity_des != ''):
                        $recent_activity_des["object_id"] = $row["ref_id"];
                        $recent_activity_des["type"] = $type ;
                        if(isset($recent_activity_des["activityDate"]))
                        {
                            self::addTimestampsToActivity($recent_activity_des);
                            $replace_date = $recent_activity_des["activityTimestampDate"]. " at ". $recent_activity_des["activityTimestampTime"];
                            $recent_activity_des["resource_name"] = str_replace('{CREATED_DATE}', $replace_date,  $recent_activity_des["resource_name"]);
                            $recent_activity_des["live_recording_check"] = $live_recording_check ;
                        }
                        if (isset($row['user_id']) && isset($row['image']) && !empty($row['user_id']) && !empty($row['image'])) {
                            $recent_activity_des['image'] = 'https://s3.amazonaws.com/sibme.com/static/users/'.$row['user_id'].'/'.$row['image']; //this will add image
                        } else {
                            $recent_activity_des['image'] = null;
                        }
                        $allactivities[] = $recent_activity_des;
                    endif;
                endif;
                $bool = 0;
            endforeach;
            return $allactivities;
        }else {
            return [];
        }
    }

    public static function addTimestampsToActivity(&$recent_activity_des)
    {
        $lang = app("Illuminate\Http\Request")->header('current-lang');
        $time_date = strtotime($recent_activity_des["activityDate"]);
        $ano = date('Y', $time_date);
        $mes = date('n', $time_date);
        $dia = date('d', $time_date);
        $time = date('h:i A', $time_date);
        if ($lang == 'en') {
            $recent_activity_des["activityTimestamp"] = $mes . ' ' . $dia . ', ' . $ano . ' ' . $time;
            $month_num = $mes;
            $month_name = date("M", mktime(0, 0, 0, $month_num, 10));
            if ($dia == date('d') && date('m', $time_date) == date('m') && date('y', $time_date) == date('y')) {
                $recent_activity_des["activityTimestampDate"] = 'Today';
            } elseif ($dia == date('d', strtotime("-1 days")) && date('m') == date('m', $time_date) && date('y', $time_date) == date('y')) {
                $recent_activity_des["activityTimestampDate"] = "Yesterday";
            } else {
                $recent_activity_des["activityTimestampDate"] = $month_name . ' ' . $dia;
            }
            $recent_activity_des["activityTimestampTime"] = $time;
        } else {
            $recent_activity_des["activityTimestamp"] = self::_SpanishDate(strtotime($recent_activity_des["activityDate"]), 'discussion_date');
            $recent_activity_des["activityTimestampDate"] = self::_SpanishDate(strtotime($recent_activity_des["activityDate"]), 'spanish_month_date');
            $recent_activity_des["activityTimestampTime"] = self::_SpanishDate(strtotime($recent_activity_des["activityDate"]), 'spanish_time');
        }
        $recent_activity_des["activityTimestampForMobile"] = $time_date;
    }
    public static function _SpanishDate($FechaStamp,$location = '')
        {
           $ano = date('Y',$FechaStamp);
           $mes = date('n',$FechaStamp);
           $dia = date('d',$FechaStamp);
           $time = date('h:i A',$FechaStamp);
           
           $diasemana = date('w',$FechaStamp);
           $diassemanaN= array("Domingo","Lunes","Martes","Mi�rcoles",
                          "Jueves","Viernes","S�bado");
           $mesesN=array(1=>"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio",
                     "Agosto","Septiembre","Octubre","Noviembre","Diciembre");
           if($location == 'dashboard')
           {
              $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic"); 
              if($dia==date('d') &&  date('m') == date('m',$FechaStamp) && date('y',$FechaStamp) == date('y')){
                return "Hoy";
              }elseif($dia == date('d',strtotime("-1 days")) && date('m') == date('m',$FechaStamp) && date('y',$FechaStamp) == date('y')){
                return "Ayer";
              }else{
                return $mesesN[$mes]. ' '  .$dia ;
              }       
             
           }
           elseif($location == 'trackers' )
           {
            $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $dia . '-'.$mesesN[$mes] ; 
           }
           elseif($location == 'trackers_filter' )
           {  
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $mesesN[$mes] ; 
           }
           elseif($location == 'archive_module' )
           {  
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $mesesN[$mes].' '.$dia.', '.$ano ; 
           }
            elseif($location == 'discussion_time' )
            {  
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $time ; 
            }
            
            elseif($location == 'discussion_date' )
            {  
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $mesesN[$mes].' '.$dia.', '.$ano . ' '.$time ; 
            }
            
            elseif($location == 'spanish_month_date' )
            {  
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $mesesN[$mes].' '.$dia ; 
            }
           elseif($location == 'spanish_day_month_year' )
            {
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");
             return $mesesN[$mes].' '.$dia.', '.$ano  ;
            }
            
            elseif($location == 'spanish_time' )
            {  
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $time ; 
            }
            
            elseif($location == 'most_common' )
            {  
             $mesesN=array(1=>"Ene","Feb","March","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $mesesN[$mes].' '.$dia.', '.$ano ; 
            }
            
           //return $diassemanaN[$diasemana].", $dia de ". $mesesN[$mes] ." de $ano";
        } 

    public static function _check_evaluator_permissions_invite($huddle_id, $user_id, $ref_user_id, $site_id) {

        $is_avaluator = \App\Models\AccountFolderUser::check_if_evalutor($site_id, $huddle_id, $user_id);

        $isEditable = true;

        $isEditable = ($is_avaluator || ($user_id == $ref_user_id));

        $eval_huddles = \App\Models\AccountFolderMetaData::where(array(
                    'meta_data_value' => 3,
                    'meta_data_name' => 'folder_type',
                    'site_id' => $site_id,
                    'account_folder_id' => $huddle_id
                ))->get()->toArray();

        if (empty($eval_huddles)) {
            $isEditable = true;
        }


        return $isEditable;
    }

    public static function library_video($huddle_id, $site_id) {

        $result = \App\Models\AccountFolder::where(array(
                    'site_id' => $site_id,
                    'account_folder_id' => $huddle_id
                ))->get()->toArray();

        if (isset($result[0]['folder_type']) && $result[0]['folder_type'] == 2) {
            return 1;
        } else {
            return 0;
        }
    }

    public static function _check_evaluator_permissions($huddle_id, $created_by, $user_id, $role_id, $site_id) {

        if (!\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id)) {
            return true;
        }
        $is_avaluator = \App\Models\AccountFolderUser::check_if_evalutor($site_id, $huddle_id, $user_id);
        $evaluators_ids = self::get_evaluator_ids($huddle_id, $site_id);
        array_push($evaluators_ids, $created_by);
        $participants_ids = \App\Models\AccountFolderUser::get_participants_ids($huddle_id, 210, $site_id);
        $isEditable = '';
        if ($is_avaluator && $role_id == 120) {
            $isEditable = ($user_id == $created_by || (is_array($participants_ids) && in_array($created_by, $participants_ids)));
        } elseif ($is_avaluator && ($role_id == 110 || $role_id == 100 || $role_id == 115 )) {
            $isEditable = ($is_avaluator || ($user_id == $created_by || (is_array($evaluators_ids) && in_array($created_by, $evaluators_ids))));
        } else {
            $isEditable = (($user_id == $created_by || (is_array($evaluators_ids) && in_array($created_by, $evaluators_ids))));
        }
        return $isEditable;
    }

    public static function get_evaluator_ids($huddle_id, $site_id) {


        $result = \App\Models\AccountFolderUser::where(array(
                    'role_id' => 200,
                    'site_id' => $site_id,
                    'account_folder_id' => $huddle_id
                ))->get()->toArray();

        $evaluator = array();
        if ($result) {
            foreach ($result as $row) {
                $evaluator[] = $row['user_id'];
            }
            return $evaluator;
        } else {
            return FALSE;
        }
    }

    public static function coach_or_collab($huddle_id, $site_id) {
        $coaching_huddles = \App\Models\AccountFolderMetaData::where(array(
                    'meta_data_value' => 2,
                    'site_id' => $site_id,
                    "meta_data_name" => "folder_type",
                    'account_folder_id' => $huddle_id
                ))->get()->toArray();
        if (count($coaching_huddles) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public static function workspace_video($huddle_id, $site_id) {

        $result = \App\Models\AccountFolder::where(array(
                    'site_id' => $site_id,
                    'account_folder_id' => $huddle_id
                ))->get()->toArray();

        if (isset($result[0]['folder_type']) && $result[0]['folder_type'] == 3) {
            return 1;
        } else {
            return 0;
        }
    }

    function has_admin_access($huddleUsers, $account_folder_groups, $user_id) {
        $huddle_role = FALSE;
        if ($huddleUsers && count($huddleUsers) > 0 || $account_folder_groups && count($account_folder_groups) > 0 && $user_id != '') {
            foreach ($huddleUsers as $huddle_user) {
                if (isset($huddle_user['huddle_users']) && $huddle_user['huddle_users']['user_id'] == $user_id) {
                    $huddle_role = $huddle_user['huddle_users']['role_id'];
                    return $huddle_role;
                } else {
                    $huddle_role = FALSE;
                }
            }
            if ($huddle_role != '') {
                return $huddle_role;
            } else {
                if ($account_folder_groups && count($account_folder_groups) > 0) {
                    foreach ($account_folder_groups as $groups) {
                        if (isset($groups['user_groups']) && $groups['user_groups']['user_id'] == $user_id) {
                            $huddle_role = $groups['role_id'];
                            return $huddle_role;
                        } else {
                            $huddle_role = FALSE;
                        }
                    }
                } else {
                    return FALSE;
                }
            }
        } else {

            return FALSE;
        }
    }

    public static function log_activity($site_id, $activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '', $video_id = 0, $bool = 1, $huddle_id = 0 , $user_id = '',$current_user_id = '') {
        
        switch ($activityType) {
            case 1:
                $activityUrl = '/Huddles/view/' . $extra_title;
                return self::_get_huddle_log($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $bool);
                break;
            case 2:
                return self::_get_huddle_video($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id);
                break;
            case 3:
                return self::_get_huddle_doc($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $huddle_id,false,$user_id,$current_user_id);
                break;
            case 4:
                return self::_get_library_log($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate);
                break;
            case 5:
                return self::_get_video_comments($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id);
                break;
            case 6:
                return self::_get_video_discussion($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $huddle_id);
                break;
            case 7:
                return self::_get_account_users($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '');
                break;
            case 8:
                return self::_get_video_discussion_child($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title);
                break;
            case 17:
                return self::_get_account_users_delete($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '');
                break;
            case 18:
                return self::_get_users_added_Huddle($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '', $huddle_id);
                break;
            case 19:
                return self::_get_users_removed_Huddle($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '', $huddle_id);
                break;
            case 20:
                return self::observation_started($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id);
                break;
            case 21:
                return self::observation_stopped($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id);
                break;
            case 22:
                return self::copy_video($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id,false,$user_id,$current_user_id);
                break;
            case 27:
                return self::copy_video($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id);
                break;
            case 24:
                return self::live_stream_video_started($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id);
            case 25:
                return self::live_stream_video_stopped($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id);
            case 28:
                return self::assignment_submit_activity($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id,false,$user_id,$current_user_id);
            case 29:
                return self::_get_huddle_urls($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $huddle_id,false,$user_id,$current_user_id);
                break;
            case 30:
                return self::_get_shared_urls($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $huddle_id,false,$user_id,$current_user_id);
                break;
        }
    }

    private static function observation_started($site_id, $activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false) {
        $base = config('s3.sibme_base_url');
        if ($activityLogs_users != '' && $video_title != '') {

            // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            $text = self::$translation_data['started_the_video_observation'];


            $af_data = \App\Models\AccountFolderDocument::where(array(
                        'site_id' => $site_id,
                        'document_id' => $video_id
                    ))->get()->toArray();

            if (!empty($af_data)) {
                $huddle_id = $af_data[0]['account_folder_id'];
            }

            $check_data = \App\Models\Document::where(array(
                        'site_id' => $site_id,
                        'id' => $video_id
                    ))->get()->toArray();

            if (self::workspace_video($huddle_id, $site_id)) {
                $text = self::$translation_data['started_synced_video_notes_activity'];
                if (!empty($check_data) && $check_data[0]['doc_type'] == 3) {
                    $url = $base . '/MyFiles/view/1/' . $huddle_id;
                } else {
                    $url = $activityUrl;
                }
            } else {


                if (!empty($check_data) && $check_data[0]['doc_type'] == 3) {

                    if ($check_data[0]['current_duration'] > 0 && $check_data[0]['is_processed'] == 4 && $check_data[0]['is_associated'] > 0) {
                        $url = $base . '/video_details/home/' . $huddle_id . '/' . $check_data[0]['is_associated'];
                    } elseif ($check_data[0]['current_duration'] > 0 && $check_data[0]['is_processed'] == 4 && $check_data[0]['is_associated'] == 0) {
                        $url = $base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data[0]['id'];
                    } elseif ($check_data[0]['current_duration'] > 0) {
                        $url = $base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data[0]['id'];
                    } else {
                        $url = $base . '/video_details/scripted_observations/' . $huddle_id . '/' . $check_data[0]['id'];
                    }
                } else {
                    $url = $activityUrl;
                }
            }
//            if ($is_api == false) {
//                $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
//            } else {
            // $video_name = $huddle_name . " - " . $video_title;
            //}
            if(!empty($video_title)){
                $video_name = $video_title;
            } else {
                $video_name = $huddle_name;
            }

            return ['activityLogs_users' => $activityLogs_users, 'resource_video_name' => $video_name, 'resource_name' => $text, 'resource_url' => $url, 'activityDate' => $activityDate];
            //return $activityLogs_users . " " . $text . ": " . $video_name;
        } else {
            return FALSE;
        }
    }

    private static function observation_stopped($site_id, $activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false) {
        $base = config('s3.sibme_base_url');
        if ($activityLogs_users != '' && $video_title != '') {

            // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            $text = self::$translation_data['stopped_the_video_observation'];

            $af_data = \App\Models\AccountFolderDocument::where(array(
                        'site_id' => $site_id,
                        'document_id' => $video_id
                    ))->get()->toArray();

            if (!empty($af_data)) {
                $huddle_id = $af_data[0]['account_folder_id'];
            }

            $check_data = \App\Models\Document::where(array(
                        'site_id' => $site_id,
                        'id' => $video_id
                    ))->get()->toArray();


            if (self::workspace_video($huddle_id, $site_id)) {
                $text = self::$translation_data['stopped_synced_video_notes_activity'];
                if (!empty($check_data) && $check_data[0]['doc_type'] == 3) {
                    $url = $base . '/MyFiles/view/1/' . $huddle_id;
                } else {
                    $url = $activityUrl;
                }
            } else {

                if (!empty($check_data) && $check_data[0]['doc_type'] == 3) {

                    if ($check_data[0]['current_duration'] > 0 && $check_data[0]['is_processed'] == 4 && $check_data[0]['is_associated'] > 0) {
                        $url = $base . '/video_details/home/' . $huddle_id . '/' . $check_data[0]['is_associated'];
                    } elseif ($check_data[0]['current_duration'] > 0 && $check_data[0]['is_processed'] == 4 && $check_data[0]['is_associated'] == 0) {
                        $url = $base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data[0]['id'];
                    } elseif ($check_data[0]['current_duration'] > 0) {
                        $url = $base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data[0]['id'];
                    } else {
                        $url = $base . '/video_details/scripted_observations/' . $huddle_id . '/' . $check_data[0]['id'];
                    }
                } else {
                    $url = $activityUrl;
                }
            }
//            if ($is_api == false) {
//                $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
//            } else {
//            $video_name = $huddle_name . " - " . $video_title;
            // }
            if(!empty($video_title)){
                $video_name = $video_title;
            } else {
                $video_name = $huddle_name;
            }

            return ['activityLogs_users' => $activityLogs_users, 'resource_video_name' => $video_name, 'resource_name' => $text, 'resource_url' => $url, 'activityDate' => $activityDate];
            //return $activityLogs_users . " " . $text . ": " . $video_name;
        } else {
            return FALSE;
        }
    }

    private static function _get_huddle_log($site_id, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $huddle_id, $bool, $is_api = false) {
        if ($activityLogs_users != '' && $huddle_name != '') {
            $collaborcoach = self::coach_or_collab($huddle_id, $site_id);
            $evaluationHuddle = \App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id);

            $af_data = \App\Models\AccountFolder::where(array(
                        'site_id' => $site_id,
                        'account_folder_id' => $huddle_id
                    ))->get()->toArray();

            if (!empty($af_data)) {
                $huddle_type = $af_data[0]['folder_type'];
            }

            if ($huddle_type == 5)
               // $text = "created a Folder ";
                $text = self::$translation_data['created_folder_activity'];
            else {
                if ($collaborcoach == 1) {
                    $text =self::$translation_data['created_coaching_huddle_activity'];
                } else if ($evaluationHuddle == 1) {
                    $text =self::$translation_data['created_assessment_huddle_activity'];
                } else {
                    $text = self::$translation_data['created_collab_huddle_activity'];
                }

                if ($bool == 2) {
                    if ($collaborcoach == 1) {
                        $text = self::$translation_data['invited_coaching_huddle_activity'];
                    } else if ($evaluationHuddle == 1) {
                        $text = self::$translation_data['invited_assessment_huddle_activity'];
                    } else {
                        $text = self::$translation_data['invited_collab_huddle_activity'];
                    }
                }
            }
            if ($is_api == false) {
                // $huddle_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";
            }
            return ['activityLogs_users' => $activityLogs_users, 'resource_video_name' => $activityDescription, 'resource_name' => $text, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
        } else {
            return FALSE;
        }
    }

    private static function _get_huddle_video($site_id, $activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false) {
        $site_id = app("Illuminate\Http\Request")->header("site_id");
        
        if ($activityLogs_users != '' && $video_title != '') {

            $videoFilePath = pathinfo($activityDescription);
            if(isset($videoFilePath['extension']))
            {
                $videoFileName = @$videoFilePath['extension'];
            }
            else
            {
                $videoFileName = '';
            }
            // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            $text = self::$translation_data['invited_collab_huddle_activity'];
            if (self::coach_or_collab($huddle_id, $site_id)) {
                $text =  self::$translation_data['uploaded_video_coaching_activity'];
            } elseif (\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id)) {
                $text =  self::$translation_data['uploaded_video_assessment_activity'];
            } else {
                $text = self::$translation_data['uploaded_video_collab_activity']; 
            }
            if (self::workspace_video($huddle_id, $site_id)) {
                $text =  self::$translation_data['uploaded_video_workspace_activity'];
            }

            if ($videoFileName == 'mp3' || $videoFileName == 'm4a') {
                $text =  self::$translation_data['uploaded_audio_activity']; 

                if (self::coach_or_collab($huddle_id, $site_id)) {
                    $text = self::$translation_data['uploaded_audio_coaching_activity']; 
                } elseif (\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id)) {
                    $text = self::$translation_data['uploaded_audio_assessment_activity']; 
                } else {
                    $text = self::$translation_data['uploaded_audio_collab_activity']; 
                }
                if (self::workspace_video($huddle_id, $site_id)) {
                    $text = self::$translation_data['uploaded_audio_workspace_activity'];
                }
            }

            $af_data = \App\Models\AccountFolderDocument::where(array(
                        'site_id' => $site_id,
                        'document_id' => $video_id
                    ))->get()->toArray();

            if (!empty($af_data)) {
                $huddle_id = $af_data[0]['account_folder_id'];
            }

            $url = $activityUrl;

            if ($is_api == false) {
                $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            } else {
                $video_name = $video_title;
            }
            
            return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_url' => $activityUrl, 'resource_video_name' => $video_title, 'activityDate' => $activityDate];
        } else {
            return FALSE;
        }
    }

    private static function copy_video($site_id, $activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false,$user_id = '',$current_user_id = '') {
        if(stripos($activityDescription, 'Resource') !== false) {
            $copied_duplicated = self::$translation_data['shared_resource_activity'];
        } elseif(stripos($activityDescription, 'Scripted Note') !== false) {
            $copied_duplicated = self::$translation_data['shared_scripted_note_activity'];
        } elseif(stripos($activityDescription, 'Url') !== false) {
            $activityUrl = \App\Models\Document::where(['id' => $video_id, 'site_id' => $site_id])->value("url");
            $copied_duplicated = self::$translation_data['shared_url_activity'];
        } else {
            $copied_duplicated = self::$translation_data['copied_video_activity'];
        }
        $copied_item = str_replace(" Copied","",$activityDescription);
        if(strpos($activityDescription, 'Duplicated') !== false)
        {
            if(strpos($activityDescription, 'Scripted Note') !== false){
                $copied_duplicated = self::$translation_data['duplicated_scripted_note'];
            } elseif(strpos($activityDescription, 'Video') !== false) {
                $copied_duplicated = self::$translation_data['duplicated_video'];
            } else {
                $copied_duplicated = self::$translation_data['copy_duplicated'];
            }
            $copied_item = str_replace(" Duplicated","",$activityDescription);
        }

        if ($activityLogs_users != '' && $video_title != '') {

            $videoFilePath = pathinfo($activityDescription);
            if(isset($videoFilePath['extension']))
            {
                $videoFileName = @$videoFilePath['extension'];
            }
            else
            {
                $videoFileName = '';
            }
            
            // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            $text = "$copied_duplicated a $copied_item";
            if (self::coach_or_collab($huddle_id, $site_id)) {
                $text = "$copied_duplicated ".self::$translation_data['duplicte_video_to_coaching_huddle'];
            } elseif (\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id)) {
                $text = "$copied_duplicated ".self::$translation_data['duplicte_video_to_assessment_huddle'];
            } else {
                $text = "$copied_duplicated ".self::$translation_data['duplicte_video_to_collab_huddle'];
            }
            if (self::library_video($huddle_id, $site_id)) {
                // $text = "$copied_duplicated a $copied_item ".self::$translation_data['duplicte_to_library'];
                $text = "$copied_duplicated ".self::$translation_data['duplicte_to_library'];
            }


            if (self::workspace_video($huddle_id, $site_id)) {
                $text = "$copied_duplicated ".self::$translation_data['duplicte_to_workspace'];
            }

            if ($videoFileName == 'mp3' || $videoFileName == 'm4a') {
                $text = "$copied_duplicated ".self::$translation_data['duplicte_an_audio_file'];

                if (self::coach_or_collab($huddle_id, $site_id)) {
                    $text = "$copied_duplicated ".self::$translation_data['duplicte_an_audio_file_in_coaching_huddle'];
                } elseif (\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id)) {
                    $text = "$copied_duplicated ".self::$translation_data['duplicte_an_audio_file_in_coaching_huddle'];
                } else {
                    $text = "$copied_duplicated ".self::$translation_data['duplicte_an_audio_file_in_collab_huddle'];
                }
                if (self::workspace_video($huddle_id, $site_id)) {
                    $text = "$copied_duplicated ".self::$translation_data['duplicte_an_audio_file_in_workspace'];
                }
            }

            $url = $activityUrl;
            //}
            // if ($is_api == false) {
            //     $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            // } else {
            //     $video_name = $huddle_name . " - " . $video_title;
            // }
            // $video_name = $huddle_name . " - " . $video_title;
            if(!empty($video_title)){
                $video_name = $video_title;
            } else {
                $video_name = $huddle_name;
            }
            
            if(\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id))
            {
                $is_avaluator = \App\Models\AccountFolderUser::check_if_evalutor($site_id, $huddle_id, $user_id);
                if($is_avaluator)
                {
                    $sample_data = self::$translation_data['sample_data_global'];
                    $text = 'uploaded ' . $video_name . ' into ' . $huddle_name ;
                    $huddle_url = '/home/video_huddles/assessment/'.$huddle_id.'/huddle/details';
                }
                else {
                    $sample_data = '';
                    $text = 'uploaded ' . $video_name . ' into ' . $huddle_name ;
                    $huddle_url = '/home/video_huddles/assessment/'.$huddle_id.'/huddle/details';
                }
                
                if($user_id == $current_user_id)
                {
                   $activityLogs_users = self::$translation_data['you_global']; 
                }
                
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['uploaded_global'], 'resource_url' => $activityUrl, 'resource_video_name' => $video_name, 'activityDate' => $activityDate , 'activity_type' => '22' , 'huddle_name' => $huddle_name , 'sample_data' => $sample_data , 'huddle_url' => $huddle_url ];
            }
            
            

            return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_url' => $activityUrl, 'resource_video_name' => $video_name, 'activityDate' => $activityDate];
        } else {
            return FALSE;
        }
    }
    
    private static function assignment_submit_activity($site_id, $activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false,$user_id = '',$current_user_id) {
   

        

            $url = $activityUrl;
            //}
            // if ($is_api == false) {
            //     $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            // } else {
            //     $video_name = $huddle_name . " - " . $video_title;
            // }
            // $video_name = $huddle_name . " - " . $video_title;
          
            
            if(\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id))
            {
                $is_avaluator = \App\Models\AccountFolderUser::check_if_evalutor($site_id, $huddle_id, $current_user_id);
                if($is_avaluator)
                {
                    $activityUrl = $activityUrl;
                }
                else {
                    
                    $activityUrl = $activityDescription;
                }
                
                $lang = app("Illuminate\Http\Request")->header('current-lang');
                
                if($current_user_id == $user_id)
                {
                    if($lang == 'es')
                    {
                      $activityLogs_users = 'Publicaste';
                      $text = self::$translation_data['tu_evaluacion'];
                    }
                    else
                    {
                      $text = 'submitted assessment';
                    }
                }
                else {
                    if($lang == 'es')
                    {
                      $text = self::$translation_data['ha_publicado'];  
                    }
                    else {
                      $text = 'has submitted assessment';  
                    }
                    
                }
                
                
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_url' => $activityUrl, 'resource_video_name' => $huddle_name, 'activityDate' => $activityDate];
            }
       
    }

    private static function _get_huddle_doc($site_id, $activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $huddle_id, $is_api = false,$user_id,$current_user_id = '') {
        if ($activityLogs_users != '' && $video_title != '') {
            $collaborcoach = self::coach_or_collab($huddle_id, $site_id);
            if (strlen($huddle_name) > 50) {
                $huddle_name = substr($huddle_name, 0, 50) . '...';
            }
            //$activityUrl ="/home/video_huddles/huddle/details/$huddle_id/artifacts/grid";
            // if ($is_api == false) {
            //     $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            // } else {
            //     $video_name = $huddle_name . " - " . $video_title;
            // }
            $video_name = $huddle_name . " - " . $video_title;

            if (self::workspace_video($huddle_id, $site_id)) {
                if(!empty($video_title)){
                    $video_name = $video_title;
                } else {
                    $video_name = $huddle_name;
                }
    
                $text = self::$translation_data['uploaded_rescource_workspace_activity'] ;
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_name, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            }

            if (self::library_video($huddle_id, $site_id)) {
                $text = self::$translation_data['uploaded_rescource_library_activity'];
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_name, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            }

            if ($collaborcoach == 1) {
                $text = self::$translation_data['uploaded_rescource_coaching_huddle_activity'];
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_name, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            } elseif (\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id)) {
//                $text =self::$translation_data['uploaded_rescource_assessment_huddle_activity'];
//                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_name, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                $is_avaluator = \App\Models\AccountFolderUser::check_if_evalutor($site_id, $huddle_id, $user_id);
                if($is_avaluator)
                {
                    $sample_data = self::$translation_data['sample_data_global'];
                    $text = 'uploaded ' . $video_title . ' into ' . $huddle_name ;
                    $huddle_url = '/home/video_huddles/assessment/'.$huddle_id.'/huddle/details';
                }
                else {
                    $sample_data = '';
                    $text = 'uploaded ' . $video_title . ' into ' . $huddle_name ;
                    $huddle_url = '/home/video_huddles/assessment/'.$huddle_id.'/huddle/details';
                }
                
                if($user_id == $current_user_id)
                {
                   $activityLogs_users = self::$translation_data['you_global']; 
                }
                
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['uploaded_global'], 'resource_url' => $activityUrl, 'resource_video_name' => $video_title, 'activityDate' => $activityDate , 'activity_type' => '3' , 'huddle_name' => $huddle_name , 'sample_data' => $sample_data , 'huddle_url' => $huddle_url ];
            } else {
                $text = self::$translation_data['uploaded_rescource_collab_huddle_activity'];
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_name, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            }
           
        } else {
            return FALSE;
        }
    }

    private static function _get_huddle_urls($site_id, $activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $huddle_id, $is_api = false,$user_id,$current_user_id = '') {
        if ($activityLogs_users != '' && $video_title != '') {
            if (strlen($huddle_name) > 50) {
                $huddle_name = substr($huddle_name, 0, 50) . '...';
            }

            $video_name = $video_title;

            if (self::workspace_video($huddle_id, $site_id)) {

                if(!empty($video_title)){
                    $video_name = $video_title;
                } else {
                    $video_name = $huddle_name;
                }
    
                $text = self::$translation_data['added_url_workspace_activity'] ;
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_name, 'resource_url' => $activityUrl, 'activityDate' => $activityDate, 'workspace'=>true];
            }

            // $collaborcoach = self::coach_or_collab($huddle_id, $site_id);

            if (\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id)) {
//                $text =self::$translation_data['uploaded_rescource_assessment_huddle_activity'];
//                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_name, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                $is_avaluator = \App\Models\AccountFolderUser::check_if_evalutor($site_id, $huddle_id, $user_id);
                if($is_avaluator)
                {
                    $sample_data = self::$translation_data['sample_data_global'];
                    // $text = 'added ' . $video_title . ' into ' . $huddle_name ;
                    $huddle_url = '/home/video_huddles/assessment/'.$huddle_id.'/huddle/details';
                }
                else {
                    $sample_data = '';
                    // $text = 'added ' . $video_title . ' into ' . $huddle_name ;
                    $huddle_url = '/home/video_huddles/assessment/'.$huddle_id.'/huddle/details';
                }
                
                if($user_id == $current_user_id)
                {
                   $activityLogs_users = self::$translation_data['you_global']; 
                }
                
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['uploaded_global'], 'resource_url' => $activityUrl, 'resource_video_name' => $video_title, 'activityDate' => $activityDate , 'huddle_name' => $huddle_name , 'sample_data' => $sample_data , 'huddle_url' => $huddle_url, 'activity_type'=>29 ];
            } else {
                // This is coaching or collaboration huddle.
                $huddle_url = "/Huddles/view/".$huddle_id;
                $text = self::$translation_data['added_url_link_activity'] . " ";
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_name, 'url_title' => $video_title, 'huddle_name'=>$huddle_name, 'resource_url' => $activityUrl, 'activityDate' => $activityDate, 'huddle_url' => $huddle_url, 'activity_type'=>29];

                // $text = self::$translation_data['added_url_link_activity'] . " " . $video_title . " in " . $huddle_name;
                // return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_name, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            }
           
        } else {
            return FALSE;
        }
    }

    private static function _get_shared_urls($site_id, $activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $huddle_id, $is_api = false,$user_id,$current_user_id = '') {
        if ($activityLogs_users != '' && $video_title != '') {
            if (strlen($huddle_name) > 50) {
                $huddle_name = substr($huddle_name, 0, 50) . '...';
            }

            $video_name = $video_title;

            $text = self::$translation_data['shared_url_activity'] ;
            if (self::workspace_video($huddle_id, $site_id)) {
                if(!empty($video_title)){
                    $video_name = $video_title;
                } else {
                    $video_name = $huddle_name;
                }
                $text .= " in Workspace";
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_name, 'resource_url' => $activityUrl, 'activityDate' => $activityDate, 'workspace'=>true];
            }

            // $collaborcoach = self::coach_or_collab($huddle_id, $site_id);

            if (\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id)) {
//                $text =self::$translation_data['uploaded_rescource_assessment_huddle_activity'];
//                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_name, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                $is_avaluator = \App\Models\AccountFolderUser::check_if_evalutor($site_id, $huddle_id, $user_id);
                if($is_avaluator)
                {
                    $sample_data = self::$translation_data['sample_data_global'];
                    // $text = 'shared ' . $video_title . ' into ' . $huddle_name ;
                    $huddle_url = '/home/video_huddles/assessment/'.$huddle_id.'/huddle/details';
                }
                else {
                    $sample_data = '';
                    // $text = 'shared ' . $video_title . ' into ' . $huddle_name ;
                    $huddle_url = '/home/video_huddles/assessment/'.$huddle_id.'/huddle/details';
                }
                
                if($user_id == $current_user_id)
                {
                   $activityLogs_users = self::$translation_data['you_global']; 
                }
                
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['shared_url_activity'], 'resource_url' => $activityUrl, 'resource_video_name' => $video_title, 'activityDate' => $activityDate , 'huddle_name' => $huddle_name , 'sample_data' => $sample_data , 'huddle_url' => $huddle_url, 'activity_type'=>30 ];
            } else {
                // This is coaching or collaboration huddle.
                $huddle_url = "/Huddles/view/".$huddle_id;
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_name, 'url_title' => $video_title, 'huddle_name'=>$huddle_name, 'resource_url' => $activityUrl, 'activityDate' => $activityDate, 'huddle_url' => $huddle_url, 'activity_type'=>30];
            }
           
        } else {
            return FALSE;
        }
    }

    private static function _get_library_log($site_id, $activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $is_api = false) {
        if ($activityLogs_users != '' && $video_title != '') {
            // if ($is_api == false) {
            //     $video_title = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $video_title . "</a>";
            // }
            $text = self::$translation_data['added_video_library_activity'];
            return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_title, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
        } else {
            return FALSE;
        }
    }

    private static function _get_video_comments($site_id, $activityLogs_users, $huddle_name, $comments, $activityUrl, $activityDate, $video_title, $video_id, $hudd_id, $is_api = false) {
        $base = config('s3.sibme_base_url');
        if ($activityLogs_users != '' && $huddle_name != '') {


            $af_data = \App\Models\AccountFolderDocument::where(array(
                        'site_id' => $site_id,
                        'document_id' => $video_id
                    ))->get()->toArray();

            if (!empty($af_data)) {
                $huddle_id = $af_data[0]['account_folder_id'];
            }else{
                $huddle_id = $hudd_id;
            }

            $check_data = \App\Models\Document::where(array(
                        'site_id' => $site_id,
                        'id' => $video_id
                    ))->get()->toArray();

            if (!empty($check_data) && $check_data[0]['doc_type'] == 3 && !self::workspace_video($hudd_id, $site_id)) {
                if ($check_data[0]['current_duration'] > 0 && $check_data[0]['is_processed'] == 4 && $check_data[0]['is_associated'] > 0) {
                    $url = $base . '/video_details/home/' . $huddle_id . '/' . $check_data[0]['is_associated'];
                } elseif ($check_data[0]['current_duration'] > 0 && $check_data[0]['is_processed'] == 4 && $check_data[0]['is_associated'] == 0) {
                    $url = $base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data[0]['id'];
                } elseif ($check_data[0]['current_duration'] > 0) {
                    $url = $base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data[0]['id'];
                } else {
                    $url = $base . '/video_details/scripted_observations/' . $huddle_id . '/' . $check_data[0]['id'];
                }
            } else {
                $url = $activityUrl;
            }
            $text =  self::$translation_data['made_comment_activity'];
            if (self::coach_or_collab($hudd_id, $site_id)) {
                $text =self::$translation_data['made_coaching_huddle_comment_activity']; 
            } elseif (\App\Models\AccountFolderMetaData::check_if_eval_huddle($hudd_id, $site_id)) {
                $text = self::$translation_data['made_assessment_huddle_comment_activity'];
            } else {
                $text =self::$translation_data['made_collab_huddle_comment_activity'];
            }
            if(!empty($huddle_id)){
                $huddle_type = AccountFolder::where(array('account_folder_id' => $huddle_id))->first();
                if($huddle_type && $huddle_type->folder_type == '2')
                {
                    $text =self::$translation_data['made_a_comment_in_video_library'];
                }
            }
            
            // if (self::workspace_video($hudd_id, $site_id)) {
            //     $text =self::$translation_data['made_video_note_activity'];
            // } elseif($check_data[0]['doc_type'] == 3 && ($check_data[0]['is_processed'] < 4)) {
            //     $text =self::$translation_data['made_scripted_note_activity'];
            // }

            if(isset($check_data[0]) &&!empty($check_data[0]) && $check_data[0]['doc_type'] == 3 && ($check_data[0]['is_processed'] < 4)) {
                $text =self::$translation_data['made_scripted_note_activity'];
            } elseif (self::workspace_video($hudd_id, $site_id)) {
                $text =self::$translation_data['made_video_note_activity'];
            }

            $filename = \App\Models\Document::where(array(
                        'site_id' => $site_id,
                        'id' => $video_id
                    ))->get()->toArray();

            if (!empty($filename)) {
                $videoFilePath = pathinfo($filename[0]['original_file_name']);
                if(isset($videoFilePath['extension']))
                {
                    $videoFileName = $videoFilePath['extension'];
                }
                else
                {
                    $videoFileName = '';
                }
                
                if ($videoFileName == 'mp3' || $videoFileName == 'm4a') {
                    $text =self::$translation_data['made_comment_activity'];

                    if (self::coach_or_collab($hudd_id, $site_id)) {
                        $text =self::$translation_data['made_coaching_huddle_audio_comment_activity']; 
                    } elseif (\App\Models\AccountFolderMetaData::check_if_eval_huddle($hudd_id, $site_id)) {
                        $text =self::$translation_data['made_assessment_huddle_audio_comment_activity']; 
                    } else {
                        $text = self::$translation_data['made_collab_huddle_audio_comment_activity'];
                    }
                    if (self::workspace_video($hudd_id, $site_id)) {
                        $text =self::$translation_data['made_audio_note_activity'];
                    }
                }
            }
            // if ($is_api == false) {
            //     $huddle_video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            // } else {
            //     $huddle_video_name = $huddle_name . " - " . $video_title;
            // }
            if(!empty($video_title)){
                $huddle_video_name = $video_title;
            } else {
                $huddle_video_name = $huddle_name;
            }

            return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $huddle_video_name, 'resource_url' => $url, 'activityDate' => $activityDate, 'document_id' => $video_id];
        } else {
            return FALSE;
        }
    }

    private static function _get_video_discussion($site_id, $activityLogs_users, $dicussion_title, $activityDescription, $activityUrl, $activityDate, $extra_title, $huddle_id, $is_api = false) {
        if ($activityLogs_users != '' && ($dicussion_title != '' || $activityDescription != '')) {
            $collaborcoach = self::coach_or_collab($huddle_id, $site_id);
            // if ($dicussion_title != '' && $is_api == false) {
            //     $huddle_discussion_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $extra_title . " - " . $dicussion_title . "</a>";
            // } elseif ($activityDescription != '' && $is_api == false) {
            //     $huddle_discussion_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $extra_title . " - " . $activityDescription . "</a>";
            // } elseif ($activityDescription != '' && $is_api == true) {
            //     $huddle_discussion_name = $extra_title . " - " . $activityDescription;
            // } else {
            //     return FALSE;
            // }
            $huddle_discussion_name = $extra_title . " - " . $activityDescription;
            if ($collaborcoach == 1) {
                $text =self::$translation_data['created_new_discussion_coaching_huddle_activity'];
            } elseif (\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id)) {
                $text =self::$translation_data['created_new_discussion_assessment_huddle_activity'];
            } else {
                $text =self::$translation_data['created_new_discussion_collab_huddle_activity'];
            }
            return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => strip_tags($huddle_discussion_name), 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
        } else {
            return FALSE;
        }
    }

    private static function _get_video_discussion_child($site_id, $activityLogs_users, $dicussion_title, $activityDescription, $activityUrl, $activityDate, $extra_title, $is_api = false) {
        if ($activityLogs_users != '' && ($dicussion_title != '' || $activityDescription != '') && $is_api == false) {
            if ($dicussion_title != '') {
                $huddle_discussion_name = $dicussion_title;
            } elseif ($activityDescription != '') {
                $huddle_discussion_name = $extra_title . " - " . $activityDescription;
            } else {
                return FALSE;
            }
            $text = self::$translation_data['posted_comment_in_activity'];
            return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => strip_tags($huddle_discussion_name), 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
        } elseif ($activityLogs_users != '' && ($dicussion_title != '' || $activityDescription != '') && $is_api == true) {
            if ($dicussion_title != '') {
                $huddle_discussion_name = $extra_title . " - " . $dicussion_title;
            } elseif ($activityDescription != '') {
                $huddle_discussion_name = $extra_title . " - " . $activityDescription;
            } else {
                return FALSE;
            }
            $text = self::$translation_data['posted_comment_in_activity'];
            return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => strip_tags($huddle_discussion_name), 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
        } else {
            return FALSE;
        }
    }

    private static function _get_account_users($site_id, $activityLogs_users, $huddle_name, $username, $activityUrl, $activityDate, $is_api = false) {
        if ($activityLogs_users != '' && $username != '' && $is_api == false) {
            if ($huddle_name != '') {
                $username_link = $huddle_name;
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' =>self::$translation_data['invited_user_text'] . $username . self::$translation_data['invited_user_into_the_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            } else {
                $username_link = $username;
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' =>self::$translation_data['added_a_new_user'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            }
        } elseif ($activityLogs_users != '' && $username != '' && $is_api == true) {
            if ($huddle_name != '') {
                $username_link = $huddle_name;
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['invited_user_text'] . $username . self::$translation_data['invited_user_into_the_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            } else {
                $username_link = $username;
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['added_a_new_user'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            }
        } else {
            return FALSE;
        }
    }

    private static function _get_account_users_delete($site_id, $activityLogs_users, $huddle_name, $username, $activityUrl, $activityDate, $is_api = false) {
        if ($activityLogs_users != '' && $username != '' && $is_api == false) {
            if ($huddle_name != '') {
                $username_link = $huddle_name;
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' =>self::$translation_data['invited_user_text'] . $username . self::$translation_data['into_the_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            } else {
                $username_link = $username;
                $resource_name = HelperFunctions::parse_translation_params(self::$translation_data['removed_user_from_account_activity'], ['username_link' => '']);
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $resource_name, 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            }
        } elseif ($activityLogs_users != '' && $username != '' && $is_api == true) {
            if ($huddle_name != '') {
                $username_link = $huddle_name;
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['invited_user_text']. $username . self::$translation_data['into_the_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            } else {
                $username_link = $username;
                $resource_name = HelperFunctions::parse_translation_params(self::$translation_data['removed_user_from_account_activity'], ['username_link' => '']);
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $resource_name, 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            }
        } else {
            return FALSE;
        }
    }

    private static function _get_users_added_Huddle($site_id, $activityLogs_users, $huddle_name, $username, $activityUrl, $activityDate, $name, $huddle_id, $is_api = false) {
        if ($activityLogs_users != '' && $username != '' && $is_api == false) {
            if ($huddle_name != '') {
                if (self::coach_or_collab($huddle_id, $site_id)) {
                    $username_link = $huddle_name;
                    return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['invited_user_text'] . $username . self::$translation_data['into_the_coaching_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                } elseif (\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id)) {
                    $username_link = $huddle_name;
                    return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['invited_user_text'] . $username . self::$translation_data['into_the_assessment_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                } else {
                    $username_link = $huddle_name;
                    return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['invited_user_text'] . $username . self::$translation_data['into_the_collab_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                }
            } else {
                $username_link = $username;
                $resource_name = HelperFunctions::parse_translation_params(self::$translation_data['removed_user_from_account_activity'], ['username_link' => '']);
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $resource_name, 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            }
        } elseif ($activityLogs_users != '' && $username != '' && $is_api == true) {
            if ($huddle_name != '') {
                if (self::coach_or_collab($huddle_id, $site_id)) {
                    $username_link = $huddle_name;
                    return ['activityLogs_users' => $activityLogs_users, 'resource_name' => "invited " . $username . self::$translation_data['into_the_coaching_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                } elseif (\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id)) {
                    $username_link = $huddle_name;
                    return ['activityLogs_users' => $activityLogs_users, 'resource_name' => " invited " . $username . self::$translation_data['into_the_assessment_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                } else {
                    $username_link = $huddle_name;
                    return ['activityLogs_users' => $activityLogs_users, 'resource_name' => " invited " . $username .  self::$translation_data['into_the_collab_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                }
            } else {
                $username_link = $username;
                $resource_name = HelperFunctions::parse_translation_params(self::$translation_data['removed_user_from_account_activity'], ['username_link' => '']);
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $resource_name, 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            }
        } else {
            return FALSE;
        }
    }

    private static function _get_users_removed_Huddle($site_id, $activityLogs_users, $huddle_name, $username, $activityUrl, $activityDate, $name, $huddle_id, $is_api = false) {
        if ($activityLogs_users != '' && $username != '' && $is_api == false) {
            if ($huddle_name != '') {
                if (self::coach_or_collab($huddle_id, $site_id)) {
                    $username_link = $huddle_name;
                    return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['removed_text'] . $username . self::$translation_data['from_the_coaching_huddle'] , 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                } elseif (\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id)) {
                    $username_link = $huddle_name;
                    return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['removed_text']  . $username . self::$translation_data['from_the_assessment_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                } else {
                    $username_link = $huddle_name;
                    return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['removed_text']  . $username . self::$translation_data['from_the_collab_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                }
            } else {
                $username_link = $username;
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' =>  self::$translation_data['removed_a_user_account'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            }
        } elseif ($activityLogs_users != '' && $username != '' && $is_api == true) {
            if ($huddle_name != '') {
                if (self::coach_or_collab($huddle_id, $site_id)) {
                    $username_link = $huddle_name;
                    return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['removed_text']  . $username . self::$translation_data['from_the_coaching_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                } elseif (\App\Models\AccountFolderMetaData::check_if_eval_huddle($huddle_id, $site_id)) {
                    $username_link = $huddle_name;
                    return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['removed_text']  . $username . self::$translation_data['from_the_assessment_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                } else {
                    $username_link = $huddle_name;
                    return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['removed_text']  . $username . self::$translation_data['from_the_collab_huddle'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
                }
            } else {
                $username_link = $username;
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => self::$translation_data['removed_a_user_account'], 'resource_video_name' => $username_link, 'resource_url' => $activityUrl, 'activityDate' => $activityDate];
            }
        } else {
            return FALSE;
        }
    }

    private static function live_stream_video_started($site_id, $activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false) {
        $base = config('s3.sibme_base_url');
        if ($activityLogs_users != '' && $video_title != '') {


            // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            $text = self::$translation_data['started_a_new_live_stream'];

            $af_data = \App\Models\AccountFolderDocument::where(array(
                        'site_id' => $site_id,
                        'document_id' => $video_id
                    ))->get()->toArray();


            if (!empty($af_data)) {
                $huddle_id = $af_data[0]['account_folder_id'];
            }

            $check_data = \App\Models\Document::where(array(
                        'site_id' => $site_id,
                        'id' => $video_id
                    ))->get()->toArray();

            if (!empty($check_data) && $check_data[0]['doc_type'] == 4) {
                $url = $base . 'home/video_details/live-streaming/' . $huddle_id . '/' . $check_data[0]['id'];
            } else {
                $url = $activityUrl;
            }

            // if ($is_api == false) {
            //     $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            // } else {
            //     $video_name = $huddle_name . " - " . $video_title;
            // }
            $video_name = $huddle_name . " - " . $video_title;
            return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_url' => $url, 'resource_video_name' => $video_name, 'activityDate' => $activityDate];
        } else {
            return FALSE;
        }
    }

    private static function live_stream_video_stopped($site_id, $activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false) {
        $base = config('s3.sibme_base_url');
        if ($activityLogs_users != '' && $video_title != '') {
            // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            $text = self::$translation_data['stopped_a_new_live_stream'];

            $af_data = \App\Models\AccountFolderDocument::where(array(
                        'site_id' => $site_id,
                        'document_id' => $video_id
                    ))->get()->toArray();

            if (!empty($af_data)) {
                $huddle_id = $af_data[0]['account_folder_id'];
            }

            $check_data = \App\Models\Document::where(array(
                        'site_id' => $site_id,
                        'id' => $video_id
                    ))->get()->toArray();

            if (!empty($check_data) && $check_data[0]['doc_type'] == 4) {
                $url = $base . 'home/video_details/home/' . $huddle_id . '/' . $check_data[0]['id'];
            } else {
                $url = $activityUrl;
            }

            // if ($is_api == false) {
            //     $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            // } else {
            //     $video_name = $huddle_name . " - " . $video_title;
            // }
            $video_name = $huddle_name . " - " . $video_title;
            return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_url' => $url, 'resource_video_name' => $video_name, 'activityDate' => $activityDate];
        } else {
            return FALSE;
        }
    }

    public static function findById($ref_id, $site_id) {
        $ref_id = (int) $ref_id;
        $result = \App\Models\User::where(array(
                    'site_id' => $site_id,
                    'id' => $ref_id
                ))->get()->toArray();
        if ($result && count($result) > 0) {
            return $result[0];
        } else {
            return TRUE;
        }
    }

    public static function check_if_recording_stopped($ref_id) {

        $check_data = UserActivityLog::where("ref_id", $ref_id)->where("type", '21')->first();

        if (!empty($check_data)) {
            return 1;
        } else {
            return 0;
        }
    }

    private static function get_translation_key_for_reource_shared_activity($activityDescription, $huddle_type){
        $trans_key = null;
        if(strpos($activityDescription, 'Resource') !== false) {
            switch($huddle_type){
                case '1':
                    $trans_key = '';
                break;
                case '2':
                    $trans_key = '';
                break;
                case '3':
                    $trans_key = '';
                break;
            }
        } elseif(strpos($activityDescription, 'Scripted Note') !== false) {
            switch($huddle_type){
                case '1':
                    $trans_key = '';
                break;
                case '2':
                    $trans_key = '';
                break;
                case '3':
                    $trans_key = '';
                break;
            }
        } else {
            switch($huddle_type){
                case '1':
                    $trans_key = 'duplicte_video_to_collab_huddle';
                break;
                case '2':
                    $trans_key = 'duplicte_video_to_coaching_huddle';
                break;
                case '3':
                    $trans_key = 'duplicte_video_to_assessment_huddle';
                break;
            }
        }
        return $trans_key;
    }
    
}
