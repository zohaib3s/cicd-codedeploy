<?php
/**
 * Created by PhpStorm.
 * User: DT-Asad
 * Date: 8/22/2019
 * Time: 1:35 AM
 */

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;
class TrackerExports
{

    private $_request;

    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    public function view()
    {
        return self::export_coaching_tracker($this->_request);
    }

    public function export_coaching_tracker(Request $request)
    {
        $export_type = $request->get("export_type");
        $single_export = $request->get("single_export");
        $start_date = $request->get('start_date');
        $end_date = $request->get('end_date');
        $start_date = date("M d, Y",strtotime($start_date));
        $end_date = date("M d, Y",strtotime($end_date));
        $single_coachee_id = null;
        $single_coach_id = $request->get("single_coach_id");
        if($single_export)
        {
            $single_coachee_id = $request->get("single_coachee_id");
        }
        $tracker_data = app('App\Http\Controllers\DashboardController')->coach_tracker($request, $single_coach_id, $single_coachee_id,true);
        if($tracker_data)
        {
            if($export_type == "pdf")
            {
                $pdf = PDF::loadView('export.CoachingTrackerExport', array('tracker_data' => $tracker_data,'start_date' => $start_date,'end_date' => $end_date,'export_type'=>$export_type));
                return $pdf->download('CoachingTrackerExport.pdf');
            }
            else
            {
                return Excel::create('Coaching Tacker Export', function($excel) use ($tracker_data,$start_date, $end_date) {

                    $excel->sheet('Coaching Tracker', function($sheet) use ($tracker_data,$start_date, $end_date) {

                        $sheet->loadView('export.CoachingTrackerExport', array('tracker_data' => $tracker_data,'start_date' => $start_date,'end_date' => $end_date,'export_type'=>'excel'));

                    });

                })->download('xlsx');
            }
        }
        else
        {
            return $tracker_data;
        }
    }
}