<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Models\AuditError;
use PHPMailer\PHPMailer\PHPMailer;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
        NotFoundHttpException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if ($this->shouldReport($e) && !is_local()) {

            $error_detail = "Stack Trace : <br>" . $e->getMessage() . '<br>' . $e->getTraceAsString();
            $error_message = $e->getMessage();
            $error_trace = $e->getTraceAsString();

            $path = $request->url();
            $form_field_method = $request->getMethod();

            $form_fields = "";
            if ($form_field_method == "POST") {
                $p_var = $request->all();
                $form_fields = json_encode($p_var);
            } else {
                $form_fields = $form_field_method;
            }

            $audit_error = array(
                'error_detail' => $error_detail,
                'path' => $path,
                'form_fields' => $form_fields,
                'created_by' => 0,
                'created_date' => date("Y-m-d H:i:s")
            );
            
            DB::table('audit_errors')->insert($audit_error);
            $error_code = DB::getPdo()->lastInsertId();

            //Send Email
            $this->sendErrorEmail($error_code, $path, $error_message, $error_trace, $form_fields);
            return parent::render($request, $e);

        } else {
            return parent::render($request, $e);
        }
        
    }  

    public function sendErrorEmail($error_code, $path, $error_message, $error_trace, $req) {
        if(is_local()){
            // Do not send email if it's local environment.
            return false;
        }
        
        $mail = new PHPMailer(false);
        $body_msg = "
        
            <b>Error Number : '$error_code'</b> <br>
            <b>Path : '$path'</b> <br> 
            <b>Form Fields :'$req'</b> <br>
            <b>Stack Trace :</b><br>
            <pre>$error_message</pre><br>
            <pre>$error_trace</pre><br>
        
        ";


        try {
            //Server settings
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            $mail->SMTPDebug = 0;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = config('mail.host');                       // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = config('mail.username');                 // SMTP username
            $mail->Password = config('mail.password');                           // SMTP password
            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = config('mail.port');                                    // TCP port to connect to

            //Recipients
            $mail->setFrom('do-not-reply@sibme.com', 'SibmeAPI');
            $mail->addAddress('khurri.saleem@gmail.com');
            $mail->addAddress('khurrams@sibme.com');     // Add a recipient

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Sibme API Error';
            $mail->Body = $body_msg;
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            return array("status" => true, "message" => "Password Change");
        } catch (Exception $e) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
        }


    }
}
