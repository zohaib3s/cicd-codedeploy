<?php
namespace App\Http\Middleware;

use App\Models\Account;
use App\Models\AccountFolderDocument;
use App\Services\HelperFunctions;
use Closure;

class LibraryAccountMiddleware
{
    protected $env;

    public function __construct()
    {

    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
        $input = $request->all();
        if (isset($input['account_id'])) {
            $account_id = $input['account_id'];
            $document_id = null;
            $is_library = $request->has("library") ? ($request->get("library") == 1) : false;
            if(!$is_library)
            {
                if($request->has("document_id"))
                {
                    $document_id = $request->get("document_id");
                }
                elseif ($request->has("doc_id"))
                {
                    $document_id = $request->get("doc_id");
                }
                elseif ($request->has("video_id"))
                {
                    $document_id = $request->get("video_id");
                }
                elseif ($request->has("videoId"))
                {
                    $document_id = $request->get("videoId");
                }
                if($document_id)
                {
                    $afd = AccountFolderDocument::join("account_folders", "account_folders.account_folder_id", "=", "account_folder_documents.account_folder_id")->where("document_id", $document_id)->first();
                    if($afd && $afd->folder_type == 2)
                    {
                        $is_library = true;
                    }
                }

            }
            if($is_library)
            {
                $parent_library_check = HelperFunctions::get_show_parent_video_library($account_id);
                if($parent_library_check)
                {
                    $account_details = Account::where('id', $account_id)->first();
                    if($account_details && $account_details->parent_account_id != 0)
                    {
                        $account_id = $account_details->parent_account_id;
                        $input['account_id'] = $account_id;
                        $request->replace($input);
                    }
                }
            }

        }

        return $next($request);
    }
}