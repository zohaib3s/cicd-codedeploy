<?php
namespace App\Http\Middleware;

use Closure;

class CorsMiddleware
{
    protected $env;

    public function __construct()
    {
        $this->env = env('APP_ENV');
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url = $request->url();
        $site_id = $this->addParameters($url);
        
        $headers = [
            // 'Access-Control-Allow-Origin'      => '*',
            'Access-Control-Allow-Methods'     => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Credentials' => 'true',
            'Access-Control-Max-Age'           => '86400',
            'Access-Control-Allow-Headers'     => 'Content-Type, Authorization, X-Requested-With, Origin, Accept, current-lang, site_id, Angular-Request, Authentication-Token'
        ];

        if ($request->isMethod('OPTIONS'))
        {
            return response()->json('{"method":"OPTIONS"}', 200, $headers);
        }

        $request->headers->set('site_id', $site_id);
        $current_lang = $request->header('current-lang');
        if(empty($current_lang)){
            $lang = isset($request->lang) ? $request->lang : "en";
            $request->headers->set('current-lang', $lang);
        }
        
        $response = $next($request);
        foreach($headers as $key => $value)
        {
            $response->headers->set($key, $value);
        }
        return $response;
    }

    public function addParameters($url){
        if($this->env=='local'){
            if(strpos($url, 'hmh') !== false){
                $site_id = 2;
            }
            else if(strpos($url, 'analytics.sibme.com') !== false)
            {
                $site_id = 1;
                if(strpos($url, 'user_summary') !== false || strpos($url, 'account_overview') !== false)
                {
                    
                }
                else {
                    
                    echo response()->json(['error' => 'Not authorized.'],403);die;
                }
            }
            
            else if(strpos($url, 'analyticscs.sibme.com') !== false)
            {
                $site_id = 2;
                if(strpos($url, 'user_summary') !== false || strpos($url, 'account_overview') !== false)
                {
                    
                }
                else {
                    
                    echo response()->json(['error' => 'Not authorized.'],403);die;
                }
                
            }
            
            else{
                $site_id = 1;
            }
        }else{
            if(strpos($url, 'csapi') !== false){
                $site_id = 2;
            }
            
            else if(strpos($url, 'analytics.sibme.com') !== false)
            {
                $site_id = 1;
                if(strpos($url, 'user_summary') !== false || strpos($url, 'account_overview') !== false)
                {
                    
                }
                else {
                    
                    echo response()->json(['error' => 'Not authorized.'],403);die;
                }
            }
            
            else if(strpos($url, 'analyticscs.sibme.com') !== false)
            {
                $site_id = 2;
                if(strpos($url, 'user_summary') !== false || strpos($url, 'account_overview') !== false)
                {
                    
                }
                else {
                    
                    echo response()->json(['error' => 'Not authorized.'],403);die;
                }
                
            }
            
            else{
                $site_id = 1;
            }
        }
        return $site_id;
    }
}