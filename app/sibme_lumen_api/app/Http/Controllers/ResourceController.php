<?php

namespace App\Http\Controllers;

use App\Services\S3Services;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ResourceController extends Controller
{
    private $s3Client;

    public function __construct()
    {
        $this->s3Client = new S3Services(
            config('s3.amazon_base_url'), config('s3.bucket_name'), config('s3.access_key_id'), config('s3.secret_access_key')
        );
    }
    public function saveThumbnailToS3($resournce_url, $fileName, $s3_destination) {
        try {
            $s_url = false;
            $output_url = public_path('thumb_preview/'.$fileName);
            $guzzle_client = new Client();
            $guzzle_client->request('GET', $resournce_url, [
                'sink' => $output_url
            ]);
            if (file_exists($output_url)) {
                $s_url = $this->s3Client->publish_to_s3($output_url, $s3_destination."/", false);
                @unlink($output_url);
            }
            return $s_url;
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
            return false;
        }
    }

    public function generateResourceThumbnail($doc_type, $fileStackId, $ext, $s3_destination,$url = ''){
        $allowed_extensions= [
            'jpg', 'jpeg', 'png', 'gif', 'bmp', 'svg', 'pdf', 'doc', 'docx', 'odt',
            'ppt', 'pptx', 'potx', 'xls', 'xlsx', 'xlsm', 'rtf', 'csv', 'txt', 'tex'
        ];
        $s3_path = false;
        if ($doc_type == 2)
        {
            if(in_array(strtolower($ext), $allowed_extensions))
            {
                $s3_path = $this->getThumbnailFromFileStack($doc_type, $fileStackId, $s3_destination);
            }
            else
            {
                return false;
            }
        }
        else if($doc_type == 5)
        {
            $s3_path = $this->getThumbnailFromFileStack($doc_type, $fileStackId, $s3_destination,$url);
        }
        else
        {
            return false;
        }
        return $s3_path;
    }

    public function getThumbnailFromFileStack($doc_type,$fileStackId, $s3_destination,$url = ''){
        try
        {
            if($doc_type==2)
            {
                // $url ='https://process.filestackapi.com/output=format:jpg/resize=width:220,height:150,fit:scale/'.$fileStackId;
                $url ='https://process.filestackapi.com/output=format:jpg/resize=w:220,h:150,f:crop,a:faces/'.$fileStackId;
            }
            else if($doc_type == 5)
            {
                if(!$this->verifyURL($url)){
                    return false;
                }
                $url = 'https://process.filestackapi.com/A3w6JbXR2RJmKr3kfnbZtz/urlscreenshot=delay:10000,mode:window/resize=width:220,height:150,fit:scale/'.$url;
                // $url = 'https://process.filestackapi.com/A3w6JbXR2RJmKr3kfnbZtz/urlscreenshot=delay:10000/resize=w:220,h:150,f:crop,a:faces/'.$url;
            }
            else
            {
                return false;
            }
            $fileName = $fileStackId.'.jpg'; //cdn id
            return $this->saveThumbnailToS3($url, $fileName, $s3_destination);
        }
        catch(\Exception $e)
        {
            $this->log_error($e);
            return false;
        }
    }
    /*check if url content type is valid for screenshot*/
    public function verifyURL($url){
        $urlUploader = new \App\Services\URLUploader();
        return $urlUploader->validate_url($url);
        //URL verification logic is already written in above function no need to write new logic
        /*try{
            $context = stream_context_create( [
                'ssl' => [
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                ],
            ]);
            $headers = get_headers($url, 1, $context);
            $headers = array_change_key_case($headers, CASE_LOWER);
            $type = isset($headers["content-type"]) ? $headers["content-type"] : "";
            if($type == null || (is_string($type) && (strpos($type, 'text/html') !== false || strpos($type, 'image') !== false)) || (is_array($type) && strpos($type[0], 'text/html') !== false)){
                return true;
            }
        }catch(\Exception $e){
            $this->log_error($e);
            return false;
        }*/
        return false;
    }
    
      /*function written for cake request*/
    public function thumbnailsForMobile(Request $request) {
        $input = $request->all();
        $result = $this->generateResourceThumbnail($input['doc_type'], $input['file_Stack_id'], $input['extension'], $input['s3_path']);
        $res = array(
            'success'=>$result  
        );
        return response()->json($res);
    }

    public function log_error(\Exception $e){
        $error_detail = "Stack Trace : <br>\n " . $e->getMessage() . ' <br>\n ' . $e->getTraceAsString();
        Log::error($error_detail);
    }
}