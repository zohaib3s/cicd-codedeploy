<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CirqliveConfigs;

class CirQliveController extends Controller {

    public function __construct(Request $request) {
        
    }

    /* Get CirqLive configuration data against each account* */

    public function getCirqLiveData($account_id) {

        $result = CirqliveConfigs::where('account_id', $account_id)->where('status', '1')->first();
        return $result;
    }

    /* PASS CIRQ  CONFIG DATA GET AND RETURN THE FORM FOR IFRAME */

    public function getCirqLTIPage(Request $request) {

        $secret = substr($request->getRequestUri(), strpos($request->getRequestUri(), "i_secret=") + 9);
        $url = 'https://' . $request->input('lti_sub_domain') . '.meets.cirqlive.com/lti.exe';
        echo $this->lti_launcher
                (
                $request->input('u_id'), $request->input('u_f_name'), $request->input('u_l_name'), $request->input('lti_email'), $request->input('course_id'), urldecode($request->input('course_name')), array($request->input('role')), $url, $request->input('lti_key'), $secret
        ), "\n";
    }

    /* LTI LAUNCHER AND FORM RETURN */

    public function lti_launcher($user_id, $user_name_first, $user_name_last, $user_email, $course_id, $course_name, array $user_role_in_course, $lti_url, $lti_key, $lti_secret, $lti_language = 'en_US') {

        function lti_role_array_to_string(array $user_roles) {
            $roles = array
                (
                'student' => 'urn:lti:role:ims/lis/Learner',
                'teacher' => 'urn:lti:role:ims/lis/Instructor',
                'admin' => 'urn:lti:role:ims/lis/Administrator'
            );
            $user_roles_real = array();
            foreach ($user_roles as $role) {
                if (isset($roles[$role])) {
                    $user_roles_real[] = $roles[$role];
                }
            }
            return(implode(',', $user_roles_real));
        }

        function array_to_string(array $q) {
            $r = '';
            foreach ($q as $key => $value) {
                $r .= rawurlencode($key) . '=' . rawurlencode($value) . '&';
            }
            $r = substr($r, 0, -1);
            return $r;
        }

        function array_sort(array $q) {
            ksort($q);
            return $q;
        }

        function hmac_sha512($key, $data, $hex_output = false) {
            return hash_hmac('sha512', $data, $key, !$hex_output);
        }

        function mac_compute($method, $uri, array $params, $key_client = '', $key_token = '') {
            $params = array_to_string(array_sort($params));
            $str = rawurlencode($method) . '&' . rawurlencode($uri) . '&' . rawurlencode($params);
            $key = rawurlencode($key_client) . '&' . rawurlencode($key_token);
            return(base64_encode(hmac_sha512($key, $str)));
        }

        //Calculate LTI variables
        $variables = array();
        $variables['user_id'] = $user_id;
        $variables['lis_person_name_given'] = $user_name_first;
        $variables['lis_person_name_family'] = $user_name_last;
        $variables['lis_person_name_full'] = $variables['lis_person_name_given'] . ' ' . $variables['lis_person_name_family'];
        $variables['lis_person_contact_email_primary'] = $user_email;
        $variables['context_id'] = $course_id;
        $variables['context_title'] = $course_name;
        $variables['roles'] = lti_role_array_to_string($user_role_in_course);
        $variables['launch_presentation_locale'] = $lti_language;
        $variables['tool_consumer_info_product_family_code'] = 'CirQlive LTI Launcher'; //You could rename this to the name of the product

        $time = gettimeofday();
        $variables['oauth_signature_method'] = 'HMAC-SHA512';
        $variables['oauth_consumer_key'] = $lti_key;
        $variables['oauth_timestamp'] = $time['sec'];
        $variables['oauth_nonce'] = base64_encode(pack('H*', sprintf('%05x', $time['usec'])) . random_bytes(21));
        $variables['oauth_signature'] = mac_compute('POST', $lti_url, $variables, $lti_secret);
        //Construct autolaunching form
        $form = '<form id="lti_launch_form" action="' . htmlspecialchars($lti_url) . '" method="post">
        <div>';

        foreach ($variables as $key => $value) {
            $form .= ' <input type="hidden" name="' . htmlspecialchars($key) . '" value="' . htmlspecialchars($value) . '" />';
        }
        $form .= ' <input id="lti_form_launch_button" type="submit" value="Launch" />
        </div>
       </form>
       <script type="text/javascript">/*<![CDATA[*/var f=document.getElementById("lti_launch_form");f.style.display="none";f.submit();/*]]>*/
       </script>';

        return($form);
    }

    /* GET ALL UPCOMING EVENTS/CONFERFENCES */

    public function get_upcoming_conferences(Request $request) {
        $key = $request->key;
        $secret = $request->secret;
        $api_root_url = $request->api_root_url;
        $course_url = $api_root_url . 'from_lti_id_context?meetsIdConnection=' . $request->lti_zoom_key . '&ltiIdContext=' . $request->course_id;
        $course_response = $this->request_signature($key, $secret, $course_url);

        if ($course_response['code'] == 200) {
//           $dateTime= strtotime("now");
            $course_data = json_decode($course_response['data']);
//           $event_url = $api_root_url.'list_conferencing_events?meetsIdCourses='.$course_data->meetsIdCourse.'&timeBeginAfter='.$dateTime;
            $event_url = $api_root_url . 'list_conferencing_events?meetsIdCourses=' . $course_data->meetsIdCourse;
            $event_response = $this->request_signature($key, $secret, $event_url);
            return $event_response;
        } else {
            return $course_response;
        }
    }

    /* FOR INTERACT WITH REST API's CIRQ */

    public function request_signature($key, $secret, $url, $input = null, $response_type = null) {
        $response = array('data' => false, 'error' => '', 'code' => 0);
        $curl = curl_init();
        if (is_resource($curl)) {

            //Turn on debugging (if you need it), comment out if you don't require it
            curl_setopt($curl, CURLOPT_VERBOSE, true);
            curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
//            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
//            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); //Maximum verification
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            if ($response_type == 'get_header') {
                curl_setopt($curl, CURLOPT_HEADER, 1);
            }
            //Force TLS 1.2 because MEETS SaaS supports it
//            curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
//            //Force strong hash algorithms because MEETS SaaS support them (string below is for OpenSSL / LibreSSL only)
//            curl_setopt($curl, CURLOPT_SSL_CIPHER_LIST, '-ALL:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256');
            $method = 'GET';
            if ($input !== null) {
                $method = 'POST';
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input);
            }
            curl_setopt($curl, CURLOPT_HTTPHEADER, array($this->authorization($method, $key, $secret, $url, $input)));
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $response['data'] = curl_exec($curl);
            if ($response['data'] !== false) {
                $response['code'] = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            } else {
                $response['error'] = curl_error($curl);
            }
            curl_close($curl);
        }

        return $response;
    }

    /* CIRQ SIGNATURE FUNCTIONS START */

    function array_sort(array $q) {
        ksort($q);
        return $q;
    }

    function array_from_string($query_string) {
        $a = array();
        parse_str($query_string, $a); //This function really needs different semantics
        return $a;
    }

    function array_to_string(array $q) {
        $r = '';
        foreach ($q as $key => $value) {
            $r .= rawurlencode($key) . '=' . rawurlencode($value) . '&';
        }
        $r = substr($r, 0, -1);
        return $r;
    }

    function array_to_string_for_authorization(array $q) {
        $r = '';
        foreach ($q as $key => $value) {
            $r .= rawurlencode($key) . '="' . rawurlencode($value) . '", ';
        }
        $r = substr($r, 0, -2);
        return $r;
    }

    function compute($method, $url, $input, $auth, $key_client = '', $key_token = '') {

        //Check if $url contains a query string
        $pos = strpos($url, '?');
        if ($pos !== false) {
            //Split $url into the get query and the preceeding part
            $get = $this->array_from_string(substr($url, $pos + 1));
            $url = substr($url, 0, $pos);
        } else {
            $get = array();
        }
        $post = $this->array_from_string($input);
        $params = $this->array_to_string($this->array_sort(array_merge($get, $auth, $post)));
        $str = rawurlencode($method) . '&' . rawurlencode($url) . '&' . rawurlencode($params);
        $key = rawurlencode($key_client) . '&' . rawurlencode($key_token);
        return base64_encode($this->hmac_sha512($key, $str));
    }

    function authorization($method, $key, $secret, $url, $input) {
        if (!function_exists('random_bytes')) {

            function random_bytes($amount) {
                $r = false;
                if (function_exists('openssl_random_pseudo_bytes')) {
                    $r = openssl_random_pseudo_bytes($amount);
                }
                if ($r === false) {
                    $r = '';
                    while ($amount--) {
                        //This is really weak
                        $r .= chr(mt_rand(0, 255));
                    }
                }
                return $r;
            }

        }
        $time = gettimeofday();
        $auth = array(); //Variables for authorization
        $auth['oauth_signature_method'] = 'HMAC-SHA512';
        $auth['oauth_consumer_key'] = $key;
        $auth['oauth_timestamp'] = $time['sec'];
        $auth['oauth_nonce'] = base64_encode(pack('H*', sprintf('%05x', $time['usec'])) . random_bytes(21));
        $auth['oauth_signature'] = $this->compute($method, $url, $input, $auth, $secret);
        return 'Authorization: OAuth ' . $this->array_to_string_for_authorization($auth);
    }

    function hmac_sha512($key, $data, $hex_output = false) {
        return hash_hmac('sha512', $data, $key, !$hex_output);
    }

    /* CIRQ DIGEST FUNCTIONS END */
}
