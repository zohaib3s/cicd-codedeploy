<?php

namespace App\Http\Controllers;

use App\Api\Activities\GetActivities;
use App\Exports\TrackerExports;
use App\Models\AccountFolderDocument;
use App\Models\AccountFolderGroup;
use App\Models\AccountMetaData;
use App\Models\Goal;
use App\Models\UserActivityLog;
use App\Services\TranslationsManager;
use Illuminate\Http\Request;
use App\Models\AccountFolderUser;
use App\Models\UserGroup;
use App\Models\AccountFolder;
use App\Models\Document;
use App\Models\User;
use App\Models\Account;
use App\Models\UserAccount;
use App\Models\Comment;
use App\Models\AccountStudentPayments;
use Illuminate\Support\Facades\DB;
use Datetime;
use App\Http\Controllers\HuddleController;
use App\Services\HelperFunctions;

class DashboardController extends Controller {

    protected $httpreq;
    protected $site_id;
    protected $current_lang;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->current_lang = $this->httpreq->header('current-lang');
        $this->base = config('s3.sibme_base_url');
        //
    }

    function getDashboardAnalytics(Request $request) {
        $this->validate($request, [
            'account_id' => 'required',
            'user_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);
        $inputs = $request->all();
        $res = DB::select('SELECT
                SUM(up.video_upload_counts)+SUM(up.workspace_upload_counts)+SUM(up.library_upload_counts) AS total_account_videos,
                SUM(up.total_hours_viewed) AS total_hours_viewed,
                SUM(up.comments_initiated_count)+SUM(up.workspace_comments_initiated_count)+SUM(up.replies_initiated_count) AS total_comments_added,
                SUM(up.huddle_created_count) AS total_account_huddles
                FROM (
                SELECT
                SUM(IFNULL(video_upload_counts,0)) AS video_upload_counts,
                SUM(IFNULL(workspace_upload_counts,0)) AS workspace_upload_counts,
                SUM(IFNULL(library_upload_counts,0)) AS library_upload_counts,
                SUM(IFNULL(t3.total_hours_viewed,0)) AS total_hours_viewed,
                SUM(IFNULL(comments_initiated_count,0)) AS comments_initiated_count,
                SUM(IFNULL(workspace_comments_initiated_count,0)) AS workspace_comments_initiated_count,
                SUM(IFNULL(replies_initiated_count,0)) AS replies_initiated_count,
                SUM(IFNULL(huddle_created_count,0)) AS huddle_created_count
                FROM 
                (
                    SELECT account_id,
                    SUM(`huddle_created_count`) AS `huddle_created_count`,
                    SUM(`video_upload_counts`) AS video_upload_counts,
                    SUM(workspace_upload_counts) AS workspace_upload_counts,
                    SUM(library_upload_counts) AS library_upload_counts,
                    SUM(videos_viewed_count) AS videos_viewed_count,
                    SUM(workspace_videos_viewed_count) AS workspace_videos_viewed_count,
                    SUM(library_videos_viewed_count) AS library_videos_viewed_count,
                    SUM(documents_viewed_count) AS documents_viewed_count,
                    SUM(workspace_resources_viewed) AS workspace_resources_viewed,
                    SUM(comments_initiated_count) AS comments_initiated_count,
                    SUM(workspace_comments_initiated_count) AS workspace_comments_initiated_count,
                    SUM(replies_initiated_count) AS replies_initiated_count,
                    ROUND(SUM(total_hours_uploaded)/60/60 ,2) AS total_hours_uploaded,
                    SUM(shared_upload_counts) AS shared_upload_counts,
                    SUM(library_shared_upload_counts) AS library_shared_upload_counts,
                    SUM(documents_uploaded_count) AS documents_uploaded_count,
                    SUM(workspace_resources_uploaded) AS workspace_resources_uploaded,
                    SUM(scripted_observations) AS scripted_observations,
                    SUM(scripted_video_observations) AS scripted_video_observations,
                    SUM(huddle_discussion_created) AS huddle_discussion_created,
                    SUM(huddle_discussion_posts) AS huddle_discussion_posts,
                    SUM(scripted_notes_shared_upload_counts) AS scripted_notes_shared_upload_counts
                    
                    FROM `analytics_huddles_summary`
                    WHERE
                    `user_role_id`<125
                    AND `account_id` IN (
                                SELECT id FROM accounts WHERE id = '.$inputs['account_id'].' 
                                UNION ALL 
                                SELECT id FROM accounts WHERE parent_account_id = '.$inputs['account_id'].'
                            )  
                    AND `created_at`>=DATE(\''.$inputs['start_date'].'\') 
                    AND `created_at`<=DATE(\''.$inputs['end_date'].'\')
                    AND `is_account_active`=1 
                    AND `is_user_active`=1
                    AND `is_huddle_active`=1
                    AND user_id='.$inputs['user_id'].'
                    GROUP BY account_id
                ) t2
                LEFT OUTER JOIN
                (
                    SELECT ROUND(SUM(out_dvh.minutes_watched)/60/60,2) AS total_hours_viewed, out_dvh.account_id FROM
                    (
                    SELECT
                    MAX(dvh.minutes_watched) AS minutes_watched, dvh.`user_id` AS user_id, dvh.account_id
                    FROM
                    document_viewer_histories AS dvh
                    INNER JOIN account_folder_documents AS afd
                    ON afd.document_id = dvh.document_id
                    WHERE dvh.site_id = 1
                    AND dvh.account_id IN (
                                SELECT id FROM accounts WHERE id = '.$inputs['account_id'].' 
                                UNION ALL 
                                SELECT id FROM accounts WHERE parent_account_id = '.$inputs['account_id'].'
                                  ) 
                    AND DATE(dvh.created_date) >= DATE(\''.$inputs['start_date'].'\')
                    AND DATE(dvh.created_date) <= DATE(\''.$inputs['end_date'].'\')
                    AND dvh.user_id='.$inputs['user_id'].'
                    GROUP BY dvh.`document_id`, dvh.`user_id`,dvh.account_id
                    ) out_dvh
                    GROUP BY out_dvh.account_id
                ) t3
                ON t2.account_id = t3.account_id
                GROUP BY t2.account_id
                ) up');
        return response()->json($res[0]);
    }

    function getDashboardUserActivitiesB(Request $request) {
        $account_id = $request->get("account_id");
        $workspace = $request->get("workspace");
        $params = $request->all();
        $user_current_account = $request->get("user_current_account");
        $user_current_account = self::ConvertToArray($user_current_account);
        $site_id = app("Illuminate\Http\Request")->header("site_id");
        $account_id = (int) $account_id;

        $nowDate = date("Y-m-d", time());
        $nowDate = strtotime($nowDate);
        $nowDate = strtotime("-7 day", $nowDate);

        $nextDate = date('Y-m-d', $nowDate);
        
        $array = [1, 2, 3, 5, 6, 4, 8, 7, 17, 18, 19, 20, 21, 22, 25, 28, 29, 30];
        $live_result = [];
        if(!$workspace)
        {
            //AND UAL.date_added >= '$nextDate'
            $live_result = DB::select("SELECT UAL.*, users.id as user_id, users.first_name, users.last_name, users.image FROM user_activity_logs as UAL join users on users.id = UAL.user_id WHERE UAL.type IN (24,25) AND UAL.account_id = '$account_id'   GROUP BY UAL.ref_id HAVING COUNT(UAL.ref_id)= 1 ORDER BY UAL.date_added DESC LIMIT 3");//limit 3 because there can only 3 live recording in an account
        }
        $result = UserActivityLog::select('UserActivityLog.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image')
            ->join("users as User", "User.id", "UserActivityLog.user_id");
        $result2 = UserActivityLog::select('UserActivityLog.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image')
            ->join("users as User", "User.id", "UserActivityLog.user_id");    

        if ($workspace) {
            $result->join("account_folders as AccountFolder", "AccountFolder.account_folder_id", "UserActivityLog.account_folder_id")
                ->where("AccountFolder.folder_type", "=", 3);
            $result2->join("account_folders as AccountFolder", "AccountFolder.account_folder_id", "UserActivityLog.account_folder_id")
                ->where("AccountFolder.folder_type", "=", 3);
        }
        
        $limit= 10;
        $page = $params['page'];
        $offset = ($page == 1) ? 0 : ((int) ($page - 1) * (int) $limit);
            
        $live_counts = count($live_result);
        if($live_counts > 0 && $page ==1)
        {
            $limit = $limit - $live_counts;
        }
        //where("UserActivityLog.date_added", ">=", $nextDate)
        
        $result = $result->where("UserActivityLog.account_id", $account_id)
          // ->where("UserActivityLog.date_added", ">=", $nextDate)
            ->whereIn("UserActivityLog.type", $array)
            ->orderBy("UserActivityLog.date_added", "DESC")
            ->offset($offset)
            ->limit($limit)
            ->get();
       
        foreach ($live_result as &$item)
        {
            $item = (array)$item;
        }        
        
        $final_result1 = GetActivities::get_ctivities($live_result, $user_current_account["User"]["id"], $site_id, $account_id, $user_current_account["roles"]["role_id"]);
        
        $final_result2 = GetActivities::get_ctivities($result, $user_current_account["User"]["id"], $site_id, $account_id, $user_current_account["roles"]["role_id"]);
        
        $final_result = array_merge($final_result1, $final_result2);
       
       // $final_result['total_record'] = $total_records;
    //    if ($page == 1) {
    //      $total_rec  = $result2->where("UserActivityLog.account_id", $account_id)
    //         ->whereIn("UserActivityLog.type", $array)->get();
    //        // $total_records = $result2->get();
    //      //var_dump($total_rec);  
    //      $totlaCount = GetActivities::get_ctivities( $total_rec, $user_current_account["User"]["id"], $site_id, $account_id, $user_current_account["roles"]["role_id"]);
    //      if($live_counts > 0){
    //         $total_records = count($totlaCount) + 3;
    //      }else{
    //         $total_records = count($totlaCount);
    //      }     

    //     }else{
    //         $total_records = 0;
    //     }
        $result_data = array(
            'activity_record'=>$final_result,
            'total_record'=>100
        );
        return response()->json(mb_convert_encoding($result_data, 'UTF-8', 'UTF-8'), 200, [], JSON_PRETTY_PRINT);
    }

    function getDashboardUserActivities(Request $request) {
        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");
        $workspace = $request->get("workspace");
        $params = $request->all();
        $user_current_account = $request->get("user_current_account");
        $user_current_account = self::ConvertToArray($user_current_account);
        $site_id = app("Illuminate\Http\Request")->header("site_id");
        $account_id = (int) $account_id;

        $nowDate = date("Y-m-d", time());
        $nowDate = strtotime($nowDate);
        $nowDate = strtotime("-7 day", $nowDate);

        $nextDate = date('Y-m-d', $nowDate);
        
        $array = [1, 2, 3, 5, 6, 4, 8, 7, 17, 18, 19, 20, 21, 22, 25, 28, 29, 30];
        $live_result = [];
        if(!$workspace)
        {
            //AND UAL.date_added >= '$nextDate'
            $live_result = DB::select("SELECT UAL.*, users.id as user_id, users.first_name, users.last_name, users.image FROM user_activity_logs as UAL join users on users.id = UAL.user_id WHERE UAL.type IN (24,25) AND UAL.account_id = '$account_id'   GROUP BY UAL.ref_id HAVING COUNT(UAL.ref_id)= 1 ORDER BY UAL.date_added DESC LIMIT 3");//limit 3 because there can only 3 live recording in an account
        }
        $result = UserActivityLog::select('UserActivityLog.*', 'User.first_name', 'User.last_name', 'User.image')
            ->join("users as User", "User.id", "UserActivityLog.user_id");
        $result2 = UserActivityLog::select('UserActivityLog.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image')
            ->join("users as User", "User.id", "UserActivityLog.user_id");    

        if ($workspace) {
            $result->join("account_folders as AccountFolder", "AccountFolder.account_folder_id", "UserActivityLog.account_folder_id")
                ->where("AccountFolder.folder_type", "=", 3);
            $result2->join("account_folders as AccountFolder", "AccountFolder.account_folder_id", "UserActivityLog.account_folder_id")
                ->where("AccountFolder.folder_type", "=", 3);
        }
        
        $limit= 10;
        $page = $params['page'];
        $offset = ($page == 1) ? 0 : ((int) ($page - 1) * (int) $limit);
            
        $live_counts = count($live_result);
        if($live_counts > 0 && $page ==1)
        {
            $limit = $limit - $live_counts;
        }
        foreach ($live_result as &$item)
        {
            $item = (array)$item;
        }
        $final_result1 = GetActivities::get_ctivities($live_result, $user_current_account["User"]["id"], $site_id, $account_id, $user_current_account["roles"]["role_id"]);
        //where("UserActivityLog.date_added", ">=", $nextDate)
        $final_result = array();
        $result_data = array();
        $final_result2  = array();
        $data_result = array();
        $result_count = $result->where("UserActivityLog.account_id", $account_id)
        // ->where("UserActivityLog.date_added", ">=", $nextDate)
          ->whereIn("UserActivityLog.type", $array)
          ->orderBy("UserActivityLog.date_added", "DESC")
          //->offset($offset)
          //->limit($limit)
          ->count(); 
         $count = 0;
        $result->where("UserActivityLog.account_id", $account_id)
            // ->where("UserActivityLog.date_added", ">=", $nextDate)
            ->whereIn("UserActivityLog.type", $array);

        $a = DB::table(DB::raw("({$result->toSql()}) as a"))
            ->mergeBindings($result->getQuery());

        $b = DB::table(DB::raw(self::getGoalsActivityQuery($account_id, $user_id, $this->site_id, 1)));

        $a->union($b);

        while(count($data_result) < 10 ){
            $result_data = $a->orderBy("date_added", "DESC")
                ->offset($offset)
                ->limit($limit)
                ->get();

            $final_result2 = GetActivities::get_ctivities( $result_data, $user_current_account["User"]["id"], $site_id, $account_id, $user_current_account["roles"]["role_id"]);

            if(count($final_result2) > 0){
                foreach($final_result2 as $row){
                    $data_result[] =  $row;
                }
            }
            $page = $page+1;            
            if($count > 50 && (count($data_result)==0 OR count($data_result) < 10)){
                break;
            }
            $count = $page;
            $offset = ($page == 1) ? 0 : ((int) ($page - 1) * (int) $limit);
            if($result_count < 10){
             break;
            }
            
            
        }
        $final_result = array_merge($final_result1,$data_result);
        $result_data = array(
            'activity_record'=>$final_result,
            'total_count'=>$result_count,
            'next_page'=>$page
        );

        return response()->json(mb_convert_encoding($result_data, 'UTF-8', 'UTF-8'), 200, [], JSON_PRETTY_PRINT);
    }
    public static function getGoalsActivityQuery($account_id, $user_id, $site_id, $forUnion = 0)
    {
        $q = "SELECT 
                      UserActivityLog.*,users.first_name, users.last_name, users.image
                    FROM user_activity_logs AS UserActivityLog 
                    
                      JOIN users on UserActivityLog.user_id = users.id  
                        
                    WHERE (UserActivityLog.ref_id IN 
                    (
                      SELECT goal_id FROM goal_users WHERE user_id = $user_id AND account_id = $account_id 
                      
                      UNION DISTINCT 
                      
                      SELECT id AS goal_id FROM goals WHERE goal_type = ".Goal::goalTypeAccount." AND account_id = $account_id 
                        
                      UNION DISTINCT 
                      
                      SELECT id AS goal_id FROM goals WHERE goal_type = ".Goal::goalTypeTemplate." AND account_id = $account_id 
                        AND 
                        (SELECT COUNT(*) FROM goal_users_settings WHERE account_id = $account_id AND user_id = $user_id 
                          AND (
                            can_create_group_templates = 1 
                            OR can_use_templates_to_create_new_goals = 1
                          )) > 0
                    ) OR UserActivityLog.user_id = $user_id) 
                      AND UserActivityLog.type >= ".Goal::activityTypes['created_for_self']." 
                      AND UserActivityLog.type <= ".Goal::activityTypes['goal_deadline_7_days']."
                      AND UserActivityLog.site_id = ".$site_id ." 
                      AND UserActivityLog.account_id = ".$account_id ." 
                    GROUP BY UserActivityLog.id 
                    ORDER BY UserActivityLog.`date_added` DESC";
        if($forUnion)
        {
            return " ($q) as b";
        }
        return $q;
    }
    public function getGoalActivities(Request $request)
    {
        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");
        /*$gc = new GoalsController($request);
        $goal_settings = $gc->getGoalsSettings($account_id,0);*/
        $query = self::getGoalsActivityQuery($account_id, $user_id, $this->site_id);
        $goal_activities = DB::select($query);
        $activities = [];
        if(!empty($goal_activities))
        {
            foreach ($goal_activities as $activity)
            {
                $activities[] = $this->modifyGoalActivity($activity,$user_id);
            }
            return $activities;
        }
        return [];
    }

    function modifyGoalActivity($activity,$user_id)
    {
        $row = [];
        $row["activityDate"] = $activity->date_added;
        $row["resource_url"] = $activity->url;
        $row["user_id"] = $activity->user_id;
        $row["type"] = $activity->type;
        $row["image"] = 'https://s3.amazonaws.com/sibme.com/static/users/'.$activity->user_id.'/'.$activity->image;
        $row["resource_video_name"] = "";
        $row["object_id"] = $activity->ref_id;
        $row["live_recording_check"] = 0;
        $row["live_recording_check"] = 0;
        $row["document_id"] = 0;
        GetActivities::addTimestampsToActivity($row);

        if ($activity->user_id == $user_id)
        {
            $row["activityLogs_users"] = GetActivities::$translation_data['workspace_you'];
        }
        else
        {
            $row["activityLogs_users"] = $activity->first_name." ".$activity->last_name;
        }

        $activity->desc = str_replace('{CREATED_DATE}',$row["activityTimestamp"],  $activity->desc);
        if($activity->source_ref_id)
        {
            if ($activity->source_ref_id == $user_id)
            {
                $other_user = GetActivities::$translation_data['workspace_you'];
            }
            else
            {
                $source_user = User::where("id",$activity->source_ref_id)->first();
                $other_user = $source_user->first_name." ".$source_user->last_name;
            }

            $activity->desc = str_replace('{OTHER_USER_NAME}', $other_user,  $activity->desc);
        }
        $row["resource_name"] = $activity->desc;
        return $row;
    }

    function update_getting_started_model_show(Request $request) {
        $input = $request->all();
        $result = array();
        $user_id = $input['user_id'];
        $account_id = $input['account_id'];


        DB::table('users')->where(array('id' => $user_id))->update(array('getting_started_show' => '0'));

        $result['message'] = "Value Updated Successfully";
        $result['success'] = true;


        return response()->json($result);

    }

    function get_counts_of_comment_tags(Request $request) {


        $input = $request->all();
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];

        $type_of_account = 'Individual';
        $account_detail = Account::where(array(
            "id" => $account_id))->first()->toArray();

        $number_of_child_account = Account::where(array(
            "parent_account_id" => $account_id))->count();

        if(!empty($account_detail['parent_account_id']) && $account_detail['parent_account_id'] != '0' )
        {
            $type_of_account = 'Child Account';
        }
        else
        {
            $type_of_account = 'Individual';
        }


        if($number_of_child_account > 0)
        {
            $type_of_account = 'Parent Account';
        }
        else {
            $type_of_account = 'Individual';
        }


        $coaching_perfomance_level = app('db')->select("SELECT meta_data_value AS coaching_perfomance_level FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'coaching_perfomance_level'");
        $coaching_perfomance_level = json_decode(json_encode($coaching_perfomance_level), True);

        $assessment_perfomance_level = app('db')->select("SELECT meta_data_value AS assessment_perfomance_level FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'assessment_perfomance_level'");
        $assessment_perfomance_level = json_decode(json_encode($assessment_perfomance_level), True);

        $account_framework_setting = app('db')->select("SELECT meta_data_value AS account_framework_setting FROM `account_folders_meta_data` WHERE account_folder_id = $account_id AND meta_data_name = 'enable_framework_standard'");
        $account_framework_setting = json_decode(json_encode($account_framework_setting), True);

        $account_custom_marker_setting = app('db')->select("SELECT meta_data_value AS account_custom_marker_setting FROM `account_folders_meta_data` WHERE account_folder_id = $account_id AND meta_data_name = 'enable_tags'");
        $account_custom_marker_setting = json_decode(json_encode($account_custom_marker_setting), True);

        $assessment_tracker = app('db')->select("SELECT meta_data_value AS assessment_tracker FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'enable_matric'");
        $assessment_tracker = json_decode(json_encode($assessment_tracker), True);

        $coaching_tracker = app('db')->select("SELECT meta_data_value AS coaching_tracker FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'enable_tracker'");
        $coaching_tracker = json_decode(json_encode($coaching_tracker), True);

        $student_payment = app('db')->select("SELECT amount AS payment_amount , `type` AS payment_type FROM `account_student_payments` WHERE account_id = $account_id");
        $student_payment = json_decode(json_encode($student_payment), True);

        $user_limit = HelperFunctions::get_allowed_users($account_id);

        $enable_video_library = app('db')->select("SELECT meta_data_value AS enable_video_library FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'enable_video_library'");
        $enable_video_library = json_decode(json_encode($enable_video_library), True);

        $number_of_child_accounts = app('db')->select("SELECT COUNT(*) AS number_of_child_accounts FROM accounts WHERE parent_account_id = $account_id");
        $number_of_child_accounts = json_decode(json_encode($number_of_child_accounts), True);

        $tracking_duration = app('db')->select("SELECT meta_data_value AS tracking_duration FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'tracking_duration'");
        $tracking_duration = json_decode(json_encode($tracking_duration), True);

        $account_payments = AccountStudentPayments::where(array(
            "account_id" => $account_id,
        ))->get()->toArray();

        $payment_type = "";
        $payment_amount = "";

        foreach($account_payments as $key => $pay)
        {
            if(count($account_payments)- 1 > $key)
            {
                $payment_type .=  $pay['type'].'|';
                $payment_amount .=  $pay['amount'].'|';
            }
            else {
                $payment_type .=  $pay['type'];
                $payment_amount .=  $pay['amount'];
            }
        }
        $data = array(
            'assessment_perfomance_level' => (isset($assessment_perfomance_level[0]['assessment_perfomance_level']) ? $assessment_perfomance_level[0]['assessment_perfomance_level'] : "0" ) ,
            'coaching_perfomance_level' => (isset($coaching_perfomance_level[0]['coaching_perfomance_level']) ? $coaching_perfomance_level[0]['coaching_perfomance_level'] : "0"),
            'account_framework_setting' => (isset($account_framework_setting[0]['account_framework_setting']) ? $account_framework_setting[0]['account_framework_setting'] : "0"),
            'account_custom_marker_setting' => (isset($account_custom_marker_setting[0]['account_custom_marker_setting']) ? $account_custom_marker_setting[0]['account_custom_marker_setting'] : "0"),
            'account_detail' => $account_detail,
            'assessment_tracker' => (isset($assessment_tracker[0]['assessment_tracker']) ? $assessment_tracker[0]['assessment_tracker'] : "0") ,
            'coaching_tracker' => (isset($coaching_tracker[0]['coaching_tracker']) ? $coaching_tracker[0]['coaching_tracker'] : "0"),
            'enable_video_library' => (isset($enable_video_library[0]['enable_video_library']) ? $enable_video_library[0]['enable_video_library'] : "0"),
            'number_of_child_accounts' => (isset($number_of_child_accounts[0]['number_of_child_accounts']) ? $number_of_child_accounts[0]['number_of_child_accounts'] : "0"),
            'tracking_duration' => (isset($tracking_duration[0]['tracking_duration']) ? $tracking_duration[0]['tracking_duration'] : "0"),
            'payment_amount' => $payment_amount,
            'payment_type' => $payment_type,
            'type_of_account' => $type_of_account,
            'user_limit' => $user_limit

        );

        return response()->json($data);
    }

    function churnzero_attribute_list(Request $request)
    {

        $input = $request->all();


        $user_current_account = $input['user_current_account'];
        $user_current_account = self::ConvertToArray($user_current_account);
        $comment_tags_count = $input['comment_tags_count'];
        $comment_tags_count = self::ConvertToArray($comment_tags_count);
        $account_id = $input['account_id'];

        $user_role_id = $user_current_account['roles']['role_id'];
        $user_id = $user_current_account['User']['id'];
        $user_role_name = HelperFunctions::get_user_role_name($user_role_id);
        $user_email_info = User::where(array('id' => $user_id))->first()->toArray();
        $user_email = $user_email_info['email'];
        //Account Attributes

        $this->create_churnzero_attribute('Account+Number',$account_id,$user_email,'account',$user_current_account['accounts']['account_id']);
        $this->create_churnzero_attribute('Performance+Levels+Enabled+in+Assessment+Huddles',$account_id,$user_email,'account',$comment_tags_count['assessment_perfomance_level']);
        $this->create_churnzero_attribute('Performance+Levels+Enabled+in+Coaching+Huddles',$account_id,$user_email,'account',$comment_tags_count['coaching_perfomance_level']);
        $this->create_churnzero_attribute('Frameworks+Enabled',$account_id,$user_email,'account',$comment_tags_count['account_framework_setting']);
        $this->create_churnzero_attribute('Custom+Marker+Tags+Enabled',$account_id,$user_email,'account',$comment_tags_count['account_custom_marker_setting']);
        $this->create_churnzero_attribute('Account+Created+Date',$account_id,$user_email,'account',$comment_tags_count['account_detail']['created_at']);
        $this->create_churnzero_attribute('Assessment+Tracker+Enabled',$account_id,$user_email,'account',$comment_tags_count['assessment_tracker']);
        $this->create_churnzero_attribute('Coaching+Tracker+Enabled',$account_id,$user_email,'account',$comment_tags_count['coaching_tracker']);
        $this->create_churnzero_attribute('Video+Library+Enabled',$account_id,$user_email,'account',$comment_tags_count['enable_video_library']);
        $this->create_churnzero_attribute('edTPA+Enabled',$account_id,$user_email,'account',$comment_tags_count['account_detail']['enable_edtpa']);
        $this->create_churnzero_attribute('Credit+Card+Customer',$account_id,$user_email,'account',$comment_tags_count['account_detail']['braintree_customer_id']);
        $this->create_churnzero_attribute('Number+of+Child+Accounts',$account_id,$user_email,'account',$comment_tags_count['number_of_child_accounts']);
        $this->create_churnzero_attribute('Tracker+feedback+duration',$account_id,$user_email,'account',$comment_tags_count['tracking_duration']);
        $this->create_churnzero_attribute('Student+Fee+Type',$account_id,$user_email,'account',$comment_tags_count['payment_type']);
        $this->create_churnzero_attribute('Student+Fee+Amount',$account_id,$user_email,'account',$comment_tags_count['payment_amount']);
        $this->create_churnzero_attribute('Type+of+Account',$account_id,$user_email,'account',$comment_tags_count['type_of_account']);
        $this->create_churnzero_attribute('User+Limit',$account_id,$user_email,'account',$comment_tags_count['user_limit']);
        if($comment_tags_count['account_detail']['parent_account_id'] != '0' ) {
            $this->create_churnzero_attribute('ParentAccountExternalId',$account_id,$user_email,'account',$comment_tags_count['account_detail']['parent_account_id']);
        }
        //Contact Attributes

        $this->create_churnzero_attribute('User+Role',$account_id,$user_email,'contact',$user_role_name);
        $this->create_churnzero_attribute('User+ID',$account_id,$user_email,'contact',$user_id);
        $this->create_churnzero_attribute('Access+Video+Library',$account_id,$user_email,'contact',$user_current_account['users_accounts']['permission_access_video_library']);
        $this->create_churnzero_attribute('Upload+Videos+to+Video+Library',$account_id,$user_email,'contact',$user_current_account['users_accounts']['permission_video_library_upload']);
        $this->create_churnzero_attribute('Manage+Folders',$account_id,$user_email,'contact',$user_current_account['users_accounts']['permission_maintain_folders']);
        $this->create_churnzero_attribute('Manage+Collaboration+Huddles',$account_id,$user_email,'contact',$user_current_account['users_accounts']['manage_collab_huddles']);
        $this->create_churnzero_attribute('Manage+Coaching+Huddles',$account_id,$user_email,'contact',$user_current_account['users_accounts']['manage_coach_huddles']);
        $this->create_churnzero_attribute('Manage+Users',$account_id,$user_email,'contact',$user_current_account['users_accounts']['permission_administrator_user_new_role']);
        $this->create_churnzero_attribute('Access+Account-wide+analytics',$account_id,$user_email,'contact',$user_current_account['users_accounts']['permission_view_analytics']);
        $this->create_churnzero_attribute('Access+personal+analytics',$account_id,$user_email,'contact','1');
        $this->create_churnzero_attribute('User+Created+Date',$account_id,$user_email,'contact',$user_current_account['User']['created_date']);


        $result['success'] = true;


        return response()->json($result);



    }

    function create_churnzero_attribute($attribute_name,$account_id,$user_email,$entity,$value)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://analytics.churnzero.net/i?appKey=invw-q7Ivjwby8NI1F6qQcH1Gix0811ja7-Li4_1xWg&accountExternalId='.$account_id.'&contactExternalId='.$user_email.'&action=setAttribute&entity='.$entity.'&name='.$attribute_name.'&value='.$value );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return ['success'=> true];



    }

    public function coach_tracker(Request $request, $single_coach_id = null, $single_coachee_id = null,$return_array = false) {

        $loggedInUser = $request->get('user_current_account');
        $page = (!empty($request->get('page')) ? $request->get('page') : 1 );
        $limit = 10;
        $language_based_content = TranslationsManager::get_page_lang_based_content('tracker_module', $this->site_id, '', 'en');
        $user = $request->get('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_id = $user['User']['id'];
        $user_role_id = $user['roles']['role_id'];
        $acc_circule_users = HelperFunctions::get_users_of_admin_circle($account_id, $user_id, $this->site_id);
        $adminUsersIds = array();
        $tracking = AccountMetaData::where( array('account_id' => $account_id, 'meta_data_name' => 'tracking_duration'))->first();
        $tracking_duration = isset($tracking) ? $tracking->meta_data_value : "48";
        if ($user_role_id == 115) {

            $adminUsers = User::whereRaw("users.id IN(select
                        user_id
                      from
                        account_folder_users
                      where account_folder_id in
                        (select
                          afu.account_folder_id
                        from
                          account_folders as af
                          inner join `account_folder_users` as afu
                            on af.`account_folder_id` = afu.`account_folder_id`
                        where af.`account_id` = $account_id
                          and afu.`user_id` = $user_id
                          and afu.`role_id` = 200))")->get();
            if ($adminUsers) {
                foreach ($adminUsers as $row) {
                    if (is_array($acc_circule_users) && in_array($row->id, $acc_circule_users)) {
                        $adminUsersIds[] = $row->id;
                    }
                }
            }
        }

        if (empty($loggedInUser)) {
            return response()->json(["status"=>false,"message"=>"Please login to continue!","redirect_to"=>"users/login"]);
        }

        $get_analytics_duration = AccountMetaData::where( array(
            "account_id" => $account_id,
            "meta_data_name" => "analytics_duration"
        ))->first();
        if (!$get_analytics_duration)
            $acct_duration_start = '08';
        else
            $acct_duration_start = $get_analytics_duration->meta_data_value;

        /* if (!empty($request->get('duration'))) {
             $sel_duration = $request->get('duration');
             $dd = $request->get('duration') - 1;
             $acDt = $acct_duration_start;
             $d = $dd . '-' . $acDt . '-01';
             $stDate = date_create($d);
             $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
         } else {
             $sel_duration =  '';
             if (date('m') >= $acct_duration_start) {
                 $yr = date('Y');
                 $dd = $yr . '-' . $acct_duration_start . '-01';
             } else {
                 $yr = date('Y') - 1;
                 $dd = $yr . '-' . $acct_duration_start . '-01';
             }
             $stDate = date_create($dd);
             $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
         }

         if (!empty($request->get('duration'))) {
             $acDt = $acct_duration_start - 1;
             $d = $request->get('duration') . '-' . $acDt . '-01';
             $endDate = date_create($d);

             $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
         } else {
             if (date('m') >= $acct_duration_start) {
                 $yr = date('Y') + 1;
                 $acDt = $acct_duration_start - 1;
                 $dd = $yr . '-' . $acDt . '-01';
             } else {
                 $yr = date('Y');
                 $acDt = $acct_duration_start - 1;
                 $dd = $yr . '-' . $acDt . '-01';
             }
             $endDate = date_create($dd);
             $end_date = date_format($endDate, 'Y-m-t') . ' 23:59:59';
         }

         $stDt = date_format($stDate, 'M Y');
         $enDt = date_format($endDate, 'M Y');
         $duration = array();

         if (date('m') >= $acct_duration_start)
             $dy = date('Y');
         else
             $dy = date('Y') - 1;

         for ($i = $dy; $i > 2011; $i--) {
             if($this->current_lang == 'es')
             {
                 $stmonthName =   HelperFunctions::SpanishDate(mktime(null, null, null, $acct_duration_start),'trackers_filter');
             }
             else {
                 $stmonthName = date("M", mktime(null, null, null, $acct_duration_start));
             }
             $stDt = $stmonthName . ' ' . $i;
             $d = $i + 1;
             $m = $acct_duration_start - 1;
             if($this->current_lang == 'es')
             {
                 $enmonthName =  HelperFunctions::SpanishDate(mktime(null, null, null, $m),'trackers_filter');
             }
             else
             {
                 $enmonthName = date("M", mktime(null, null, null, $m));
             }
             $endDt = $enmonthName . ' ' . $d;
             $duration[$i]['dt'] = $stDt . ' to ' . $endDt;
             $duration[$i]['vl'] = $d;
         } */

        $is_user = null;
        if ($user['users_accounts']['role_id'] == 120) {
            $is_user = $user_id;
        }
        if ($user['users_accounts']['role_id'] == 115) {
            $is_user = $user_id;
        }
        $is_admin_cond = array();

        if ($user['users_accounts']['role_id'] == 115) {
            $is_admin_cond[] = array(
                'AccountFolderUser.user_id' => $adminUsersIds,
            );
        }

        $start_date = $request->get('start_date');
        $stDate = date_create($start_date);
        $st_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';

        $end_date = $request->get('end_date');
        $endDate = date_create($end_date);
        $end_date = date_format($endDate, 'Y-m-d') . ' 23:59:59';

        $mAVCnd = '';
        $mAVCnd = "documents.recorded_date >= '" . $st_date ."'" ;
        $mAVCnd .= " AND documents.recorded_date <= '" . $end_date ."'";

        if($single_coach_id)
        {
            $is_user = $single_coach_id;
        }

        $query = AccountFolder::select('User.*')->selectRaw("concat(User.first_name,' ',User.last_name) as coach_name")
            ->leftJoin('account_folders_meta_data as afmd','AccountFolder.account_folder_id','=','afmd.account_folder_id')
            ->leftJoin('account_folder_users as huddle_users','AccountFolder.account_folder_id','=','huddle_users.account_folder_id')
            ->leftJoin('users as User','User.id','=','huddle_users.user_id')
            ->where(array(

                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 1,
                'afmd.meta_data_name' => 'folder_type',
                'afmd.meta_data_value' => 2,
                'huddle_users.role_id' => 200
            ))
            ->where('AccountFolder.is_sample', '<>','1')
            ->whereRaw('AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)');
        if($is_user)
        {
            $query->where('huddle_users.user_id',$is_user);
        }

        if(!empty($request->get('search')))
        {
            $query = $query->whereRaw("CONCAT(User.first_name,' ',User.last_name) like '%".$request->get('search')."%'");
        }
        //$account_coaches_count = $query1->distinct()->count('huddle_users.user_id');
        $account_coaches = $account_coaches_count = $query->orderBy('coach_name')->groupBy('huddle_users.user_id')->get()->toArray();
        $account_coaches_count = count($account_coaches_count);
        if(!$single_coach_id)
        {
            $account_coaches = $query->orderBy('coach_name')->groupBy('huddle_users.user_id')->get()->forPage($page, $limit)->toArray();
            $account_coaches = array_values(array_filter($account_coaches));
        }

        if (count($account_coaches) > 0) {
            for ($i = 0; $i < count($account_coaches); $i++) {
                $total_sessions = 0;
                $account_coaches[$i]['Coachees'] = array();
                $account_coach_huddles = AccountFolder::select('huddle_users.account_folder_id', 'huddle_users.user_id')
                    ->leftJoin('account_folders_meta_data as afmd','AccountFolder.account_folder_id','=','afmd.account_folder_id')
                    ->leftJoin('account_folder_users as huddle_users','AccountFolder.account_folder_id','=','huddle_users.account_folder_id')
                    ->leftJoin('users as User','User.id','=','huddle_users.user_id')
                    ->where(array(
                        'AccountFolder.account_id' => $account_id,
                        'AccountFolder.folder_type' => 1,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => 2,
                        'huddle_users.role_id' => 200,
                        'huddle_users.user_id' => $account_coaches[$i]['id']
                    ))
                    ->where('AccountFolder.is_sample', '<>','1')
                    ->whereRaw('AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)')
                    ->groupBy('huddle_users.account_folder_id')
                    ->get()->toArray();
                $coach_hud = array();
                $account_folder_users = array();
                $is_admin_inner = array();
                foreach ($account_coach_huddles as $coach_huddles) {
                    $coach_hud[] = $coach_huddles['account_folder_id'];
                    $account_folder_users[] = $coach_huddles['user_id'];
                }
                $role_id_cond = array();
                if ($user['users_accounts']['role_id'] == 115) {
                    $is_admin_inner =  $account_folder_users;
                    $role_id_cond = array(210, 200);
                } else {
                    $role_id_cond = array(210);
                }
                $query = AccountFolderUser::select('User.*')
                    ->leftJoin('users as User','User.id','=','AccountFolderUser.user_id')
                    ->leftJoin('account_folders as af','af.account_folder_id','=','AccountFolderUser.account_folder_id')->whereIn('AccountFolderUser.account_folder_id' , $coach_hud)
                    ->where(array('User.is_active' => 1))
                    ->where('af.is_sample' , '<>', '1')
                    ->whereIn('AccountFolderUser.role_id',$role_id_cond)
                    ->groupBy('AccountFolderUser.user_id')
                    ->orderBy('User.first_name');

                if(!empty($single_coachee_id))
                {
                    $query->where('AccountFolderUser.user_id',$single_coachee_id);
                }
                else if(!empty($adminUsersIds) && $user['users_accounts']['role_id'] == 115)
                {
                    $query->whereIn('AccountFolderUser.user_id',$adminUsersIds);
                }
                $account_coachees = $query->get()->toArray();
                $account_coaches[$i]['Coachees'] = $account_coachees;
                if (is_array($account_coaches[$i]['Coachees']) && count($account_coaches[$i]['Coachees']) > 0) {
                    for ($j = 0; $j < count($account_coaches[$i]['Coachees']); $j++) {
                        $account_coaches[$i]['Coachees'][$j]['Videos'] = array();
                        $account_coachees_videos = Document::select('documents.*')
                            ->selectRaw('(SELECT "1" FROM document_meta_data WHERE document_id = afd.document_id AND meta_data_name = "remove_coaching_tracking" AND meta_data_value = 1 limit 1) AS RemoveTracking')
                            ->leftJoin('account_folder_documents as afd','documents.id','=','afd.document_id')
                            ->leftJoin('account_folder_users as afu','afd.account_folder_id','=','afu.account_folder_id')
                            ->whereIn('afd.account_folder_id',$coach_hud)
                            ->whereIn('documents.doc_type' , array(1, 3) )
                            ->where('afu.user_id' , $account_coaches[$i]['Coachees'][$j]['id'])
                            ->where('afu.role_id',210)
                            ->whereRaw($mAVCnd)
                            ->orderBy('documents.recorded_date')
                            ->groupBy('documents.id')->get()->toArray();

                        $output = array();
                        for ($l = 0; $l < count($account_coachees_videos); $l++) {
                            if ($account_coachees_videos [$l]['RemoveTracking'] != '1') {
                                if ($account_coachees_videos[$l]['doc_type'] == '1' || ( $account_coachees_videos[$l]['doc_type'] == '3' && empty($account_coachees_videos[$l]['is_associated'])) || ($account_coachees_videos[$l]['doc_type'] == '3' /* && !empty($account_coachees_videos[$l]['scripted_current_duration'])*/)) {
                                    $output[] = $account_coachees_videos[$l];
                                }
                            }
                        }
                        $account_coachees_videos = $output;

                        $account_coaches[$i]['Coachees'][$j]['Videos'] = $account_coachees_videos;
                        $total_sessions = $total_sessions + count($account_coachees_videos);
                        if (is_array($account_coachees_videos) && count($account_coachees_videos) > 0) {

                            for ($k = 0; $k < count($account_coaches[$i]['Coachees'][$j]['Videos']); $k++) {
                                $status = 'no_feedback';
                                $phpdate = strtotime($account_coaches[$i]['Coachees'][$j]['Videos'][$k]['recorded_date']);
                                $modified_date = strtotime('+' . $tracking_duration . ' hours', $phpdate);


                                if(!empty($request->header("current-lang")) && $request->header("current-lang") == 'es')
                                {
                                    $mysqldate = HelperFunctions::SpanishDate($phpdate,'trackers');
                                }
                                else
                                {
                                    $mysqldate = date('d-M', $phpdate);
                                }

                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['Comments'] = array();
                                $coach_comments = Comment::where(array(
                                    'ref_id' => $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['id']
                                , 'ref_type' => 2
                                , 'user_id' => $account_coaches[$i]['id']
                                ))->orderBy('created_date', 'ASC')->get();
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['Comments'] = $coach_comments;
                                $coach_comments = $coach_comments->toArray();
                                if (is_array($coach_comments) && count($coach_comments) > 0) {
                                    $cmntdate = strtotime($coach_comments[0]['created_date']);
                                    if ($modified_date >= $cmntdate) {
                                        $status = 'ontime_feedback';
                                    } else {
                                        $status = 'late_feedback';
                                    }
                                }
                                if($account_coaches[$i]['Coachees'][$j]['Videos'][$k]['doc_type'] == '3')
                                {
                                    $status = 'ontime_feedback';
                                }
                                $average_performance_rating = app('App\Http\Controllers\TrackerController')->video_standard_tags_with_performance_rating($account_id,$account_coaches[$i]['Coachees'][$j]['Videos'][$k]['id']);
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['average_performance_rating'] = $average_performance_rating;
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['coach_comment_count'] = count($coach_comments);
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['color_status'] = $status;
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['video_date'] = $mysqldate;
                            }
                        }
                        if (is_array($account_coaches[$i]['Coachees'][$j]['Videos']) && count($account_coaches[$i]['Coachees'][$j]['Videos']) > 0) {

                            for ($k = 0; $k < count($account_coaches[$i]['Coachees'][$j]['Videos']); $k++) {
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['attachment_count'] = Document::getVideoAttachmentNumbers($account_coaches[$i]['Coachees'][$j]['Videos'][$k]['id'], $this->site_id);
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['Feedback'] = array();
                                $coach_feedback = Comment::where( array(
                                    'ref_id' => $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['id'],
                                    'user_id' => $account_coaches[$i]['Coachees'][$j]['id']
                                ))->whereIn('ref_type', [2,3])->orderBy('created_date', 'ASC')->get();
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['Feedback'] = $coach_feedback;
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['coachee_comment_count'] = count($coach_feedback);
                                $session_summary = Comment::where(array(
                                    'ref_id' => $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['id'],
                                    'ref_type' => 4
                                ))->orderBy("id","desc")->first();
                                $account_coaches[$i]['Coachees'][$j]['Videos'][$k]['session_summary'] = $session_summary ? $session_summary->comment : "";
                            }
                        }
                    }
                }

                $account_coaches[$i]['total_sessions'] = $total_sessions;
            }
        }
        //Coachee Code Commented


        $data = [
            //"sel_duration"=>$sel_duration,
            "account_coaches"=>$account_coaches,
            "account_coaches_count" => $account_coaches_count,
            //   "duration"=>$duration,
            "tracking_duration"=>$tracking_duration,
            "language_based_content"=>$language_based_content,
        ];
        if($return_array)
        {
            return $data;
        }
        return response()->json(["data"=>$data,"message"=>"Success","status"=>true]);
    }

    public function export_coaching_tracker(Request $request)
    {
        if($request->get("export_type") != "pdf")
        {
            header('access-control-allow-origin: *');
        }
        return (new TrackerExports($request))->view();
    }
}