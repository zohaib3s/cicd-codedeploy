<?php

namespace App\Http\Controllers;

use Aacotroneo\Saml2\Saml2Auth;
use App\Models\Account;
use App\Models\AccountFolderGroup;
use App\Models\AccountMetaData;
use App\Models\AccountFolder;
use App\Models\AccountFolderMetaData;
use App\Models\AccountFolderUser;
use App\Models\CustomField;
use App\Models\Goal;
use App\Models\UserCustomField;
use App\Models\GoalUser;
use App\Models\Group;
use App\Models\Sites;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\UserActivityLog;
use App\Models\UserGroup;
use App\Models\GoalUserSettings;
use App\Models\EmailUnsubscribers;
use App\Models\AccountStudentPaymentsLogs;
use App\Models\AccountStudentPayments;
use App\Services\HelperFunctions;
use App\Services\SendGridEmailManager;
use App\Services\TranslationsManager;
use App\Services\UsersHelperFunctions;
use App\Services\Emails\Email;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use \DrewM\MailChimp\MailChimp;
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
use App\Services\BraintreeSubscription;
use OneLogin\Saml2\Auth as OneLogin_Saml2_Auth;

class UsersController extends Controller
{
    protected $httpreq;
    protected $site_id;
    protected $lang;
    public $mailchimp;
    public $base;
    public $language_based_messages;
    public $group_based_translation;
    public $subject_title;
    public $duplicated_emails="";
    public $is_from_hmh_licensing_admin=false;
    public $hmh_license_id=false;
    public $linked_emails;
    public $new_emails_added;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->lang = $this->httpreq->header('current-lang');
        $this->base = config('s3.sibme_base_url');
        $this->mailchimp = new MailChimp('007cb274d399d9290e1e6c5b42118d40-us3');
        $this->language_based_messages = TranslationsManager::get_page_lang_based_content('flash_messages', $this->site_id, '', $this->lang);
        $this->group_based_translation = TranslationsManager::get_page_lang_based_content('users/administrators_groups', $this->site_id, '', $this->lang);
        $this->subject_title = Sites::get_site_settings('email_subject', $this->site_id);
    }

    public function administrators_groups(Request $request)
    {
        $user_id = $request->get("user_id");
        $role_id = $request->get("role_id");
        $account_id = $request->get("account_id");
        $permission_access_video_library = $request->get("permission_access_video_library");
        $permission_video_library_upload = $request->get("permission_video_library_upload");
        $permission_administrator_user_new_role = $request->get("permission_administrator_user_new_role");
        $permissions = [
            "permission_administrator_user_new_role" => $permission_administrator_user_new_role,
            "permission_video_library_upload" => $permission_video_library_upload,
            "permission_access_video_library" => $permission_access_video_library,
        ];

        $language_based_content = TranslationsManager::get_page_lang_based_content('users/administrators_groups', $this->site_id, $user_id, $this->lang);

        $result = $this->check_is_account_expired($account_id, $user_id, $role_id);
        if (!$result["status"]) {
            return response()->json($result, 200, [], JSON_PRETTY_PRINT);
        }
        $result = $this->is_account_activated($account_id, $user_id);
        if (!$result["status"]) {
            return response()->json($result, 200, [], JSON_PRETTY_PRINT);
        }
        $result = $this->auth_permission($account_id, $user_id, $role_id, $permissions, 'people', 'administrators_groups');

        if (!$result) {
            return ["status" => false, "redirect_to" => '/NoPermissions/no_permission', "message" => "You don't have permission"];
        }

        $accountOwner = $this->_getAccountOwner($account_id);
        $supperAdmins = $this->_getSupperAdmins($account_id);
        $users = $this->_getUsers($account_id);
        $newAdmins = $this->_getAdmins($account_id);
        $viewers = $this->_getViewers($account_id);
        $deactivatedUsers = $this->_getDeactivatedUsers($account_id);
        $data = ["accountOwner" => $accountOwner, "supperAdmins" => $supperAdmins, "users" => $users, "viewers" => $viewers, "deactivatedUsers" => $deactivatedUsers, "newAdmins" => $newAdmins, "language_based_content" => $language_based_content];
        //$huddles = $this->AccountFolder->getAllHuddlesForPeople($account_id);
        $result = ["status" => true, "data" => $data, "message" => "Data fetched successfully"];
        return response()->json($result, 200, [], JSON_PRETTY_PRINT);
    }

    private function _getAccountOwner($account_id)
    {
        $supperUsers = User::getUsersByAccount2($account_id, 100,'','','','',$this->site_id);


        if (count($supperUsers) > 0) {
            return $supperUsers[0];
        } else {

            return [];
        }
    }

    private function _getSupperAdmins($account_id, $limit = '')
    {
        $supperAdmin = User::getUsersByAccount2($account_id, 110, $limit,'','','',$this->site_id);

        if (count($supperAdmin) > 0) {
            return $supperAdmin;
        } else {

            return [];
        }
    }

    private function _getUsers($account_id, $limit = '')
    {
        $admin = User::getUsersByAccount2($account_id, 120, $limit,'','','',$this->site_id);
        if (count($admin) > 0) {
            return $admin;
        } else {

            return [];
        }
    }

    private function _getViewers($account_id, $limit = '')
    {
        $admin = User::getUsersByAccount2($account_id, 125, $limit,'','','',$this->site_id);
        if (count($admin) > 0) {
            return $admin;
        } else {

            return [];
        }
    }
    private function _getDeactivatedUsers($account_id, $limit = '')
    {
        $admin = User::getUsersByAccount2($account_id, null, $limit, 0, '', 1,$this->site_id);
        if (count($admin) > 0) {
            return $admin;
        } else {

            return [];
        }
    }

    private function _getNewAdmins($account_id, $limit = '')
    {
        $admin = User::getUsersByAccount2($account_id, 110, $limit,'','','',$this->site_id);
        if (count($admin) > 0) {
            return $admin;
        } else {

            return [];
        }
    }
    private function _getAdmins($account_id, $limit = '')
    {
        $admin = User::getUsersByAccount2($account_id, 115, $limit,'','','',$this->site_id);
        if (count($admin) > 0) {
            return $admin;
        } else {

            return [];
        }
    }

    public function is_account_activated($account_id, $user_id)
    {
        $result = User::where("id", $user_id)->where("site_id", $this->site_id)->first();
        if ($result->type == 'Pending_Activation') {
            $link = config("s3.sibme_base_url") . '/users/activation_page/' . $account_id . '/' . $user_id;
            return ["status" => false, "redirect_to" => $link, "message" => "Account not activated"];
        }
        return ["status" => true, "Account is active"];
    }

    public function check_is_account_expired($account_id, $user_id, $role_id)
    {
        $logout = false;
        $link = "";
        $redirect = false;
        $message = "Success";
        if ($account_id != '') {
            $result = Account::where('id', $account_id)->where('in_trial', '1')->first();
            if ($result) {
                $trial_duration = config('general.trial_duration');
                $account_create_date = $result->created_at;
                //$this->Session->write('account_created_at', $account_create_date);
                $account_end_date = date('Y-m-d H:i:s', strtotime("+{$trial_duration} day", strtotime($account_create_date)));
                $current_date = date('Y-m-d', time());
                $expiry_date = date('Y-m-d', strtotime($account_end_date));
                $current_date = strtotime($current_date);
                $expiry_date = strtotime($expiry_date);
                if ($current_date > $expiry_date) {
                    $user_account_counts = UserAccount::where("user_id", $user_id)->where("site_id", $this->site_id)->count();
                    if ($role_id == 100) {
                        $message = TranslationsManager::parse_translation_params(TranslationsManager::get_translation('day_trial_has_expired_in_order_msg', "flash_messages", $this->site_id), ['trial_duration' => $trial_duration]);
                        $link = config('s3.sibme_base_url') . 'accounts/account_settings_all/' . $account_id . '/4';
                        $redirect = true;
                    } elseif ($role_id == 110) {
                        if (isset($user_account_counts) && count($user_account_counts) > 1) {
                            $message = TranslationsManager::parse_translation_params(TranslationsManager::get_translation('day_trial_has_expired_contact_account_owner_msg', "flash_messages", $this->site_id), ['trial_duration' => $trial_duration]);
                            $link = config('s3.sibme_base_url') . 'launchpad';
                            $redirect = true;
                        } else {
                            $logout = true;
                            $message = TranslationsManager::parse_translation_params(TranslationsManager::get_translation('day_trial_has_expired_contact_account_owner_msg', "flash_messages", $this->site_id), ['trial_duration' => $trial_duration]);
                            $link = config('s3.sibme_base_url') . 'users/login';
                            $redirect = true;
                        }
                    } elseif ($role_id == 120) {
                        if (isset($user_account_counts) && count($user_account_counts) > 1) {
                            $message = TranslationsManager::parse_translation_params(TranslationsManager::get_translation('day_trial_has_expired_contact_account_owner_msg', "flash_messages", $this->site_id), ['trial_duration' => $trial_duration]);
                            $link = config('s3.sibme_base_url') . 'launchpad';
                            $redirect = true;
                        } else {
                            $logout = true;
                            $message = TranslationsManager::parse_translation_params(TranslationsManager::get_translation('day_trial_has_expired_contact_account_owner_msg', "flash_messages", $this->site_id), ['trial_duration' => $trial_duration]);
                            $link = config('s3.sibme_base_url') . 'users/login';
                            $redirect = true;
                        }
                    }
                }
            }
        }
        return ["status" => !$redirect, "redirect_to" => $link, "message" => $message, "should_logout" => $logout];
    }

    public function auth_permission($account_id, $user_id, $role_id, $permissions, $module = '', $function = '', $current_user_role = '', $created_by_id = '', $current_user_id = '')
    {
        $user = User::getUserInformation($account_id, $user_id);

        if ($module && $user) {
            switch ($module) {
                case 'library':
                    return UsersHelperFunctions::_library_access($function, $role_id, $permissions);
                    break;
                case 'people':
                    return UsersHelperFunctions::_people_access($function, $permissions);
                    break;
                case 'permissions':
                    return UsersHelperFunctions::_permission_access($function, $permissions);
                    break;
                case 'huddle':
                    return UsersHelperFunctions::_permissions_on_huddle($function, $user, $current_user_role, $created_by_id, $current_user_id);
                    break;
            }
        }
    }

    public function import_users_csv(Request $request)
    {

        $send_email = (boolean)json_decode(strtolower($request->get("send_email")));
        $file = Input::file('additional_attachemnt');
        $account_id = $request->get("account_id");
        $current_user_id = $request->get("user_id");
        $current_first_name = $request->get("first_name");
        $current_last_name = $request->get("last_name");
        $attachment_1 = '';
        $attachment_2 = '';
        $errors = array();
        $maxsize = 2048000;
        $account_owner = UserAccount::where('account_id', $account_id)->where('role_id', 100)->first();
        if($account_owner)
        {
            $account_owner  = $account_owner->toArray();
        }
        $owner_id = isset($account_owner['user_id'])?$account_owner['user_id']: $current_user_id;
        $account_owner_info = User::where('id', $owner_id)->first();
        if($account_owner_info)
        {
            $account_owner_info = $account_owner_info->toArray();
        }
        if(!isset($_FILES['additional_attachemnt']['name'])){
            $msg_str = $this->language_based_messages['please_something_went_wrong'];
            $result = ["status" => true, "message" => $msg_str];
            return response()->json($result, 200, [], JSON_PRETTY_PRINT);
        }

        $acceptable = array(
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/msword',
            'application/octet-stream',
            'application/vnd.ms-excel',
            'application/xls',
            'txt/csv',
            'text/csv',
        );
        ini_set("auto_detect_line_endings", true);
        if (isset($_FILES['additional_attachemnt']['name']) && $_FILES['additional_attachemnt']['name'] != '') {
            $additional_attachment = $_FILES['additional_attachemnt']['tmp_name'];
            if (($_FILES['additional_attachemnt']['size'] >= $maxsize) || ($_FILES["additional_attachemnt"]["size"] == 0)) {
                $errors[] = 'File too large. File must be less than 2 megabytes.';
            }
            if (!in_array($_FILES['additional_attachemnt']['type'], $acceptable) && (!empty($_FILES["additional_attachemnt"]["type"]))) {
                $errors[] = 'Invalid file type. Only CSV type is accepted.';
            }

            if (count($errors) === 0) {
                $path = realpath(dirname(__FILE__) . '/../../..') . "/public/" . $_FILES['additional_attachemnt']['name'];
                $this->postUpload();
                // move_uploaded_file($additional_attachment, $path);
                $attachment_1 = $path;
            } else {
                $error_message = '';
                if (isset($errors[0])) {
                    $error_message .= $errors[0] . '<br/>';
                } elseif (isset($errors[1])) {
                    $error_message .= $errors[0];
                }

                $res = array(
                    'message' => $error_message,
                    'status' => 'failed',
                );
                return response()->json($res);
            }
        }

        $i = null;
        $error = null;
        $filename = realpath(dirname(__FILE__) . '/../../..') . "/public/attachments/bulk_import/" . $_FILES['additional_attachemnt']['name'];

        $handle = fopen($filename, "r");

        $header = fgetcsv($handle, 0, ",");
        $required_headers = ["first_name","last_name","email","role"];
        if(!empty(array_diff($required_headers,$header)))
        {
            return response()->json(["message"=>$this->group_based_translation["people_invalid_csv_headers"],"status"=>false]);
        }
        $return = array(
            //'messages' => array(),
            'errors' => array(),
        );
        $roles = array("super admin", "user", "admin", "account owner","viewer");
        $email_string = '';
        $this->duplicated_emails = '';
        $error_count = 0;
        $total_count = 0;
        $new_users = 0;
        while (($row = fgetcsv($handle, 0, ",")) !== false) {
            $new_users++;
        }

        $totalUsers = User::getTotalUsers($account_id) + $new_users;
        if ($totalUsers >= Account::get_allowed_users($account_id))
        {
            $result = Account::select('plans.category', 'plans.plan_type', 'plans.storage', 'plans.id', 'plans.users', 'accounts.*')
                ->leftJoin('plans', 'plans.id', '=', 'accounts.plan_id')
                ->where('accounts.id', $account_id)
                ->first();

            if($result)
            {
                $result = $result->toArray();
            }
            $msg_str = '';
            $user_account_detail = UserAccount::where(array( 'account_id' => $account_id , 'user_id' => $current_user_id  ))->first();
            if ($user_account_detail->role_id == 100) {
                if ($result['deactive_plan'] == 1) {

                    $msg_str = TranslationsManager::parse_translation_params($this->language_based_messages['your_account_has_reached_maximum_users_msg'], ['allowed_users' => Account::get_allowed_users($account_id), 'sales_email' => Sites::get_site_settings('static_emails', $this->site_id)['sales']]);

                } else {
                    $msg_str = TranslationsManager::parse_translation_params($this->language_based_messages['your_account_has_reached_maximum_users_upgrade_or_delete_msg'], ['allowed_users' => Account::get_allowed_users($account_id), 'account_id' => $account_id]);
                }
            } else {
                $msg_str = TranslationsManager::parse_translation_params($this->language_based_messages['your_account_has_reached_maximum_user_contact_account_owner_msg'], ['allowed_users' => Account::get_allowed_users($account_id), 'first_name' => $account_owner_info['first_name'], 'last_name' => $account_owner_info['last_name'], 'account_owner_email' => $account_owner_info['email']]);
            }
            return response()->json(["message"=>$msg_str,"status"=>false]);
        }
        fclose($handle);
        $handle = fopen($filename, "r");
        $header = fgetcsv($handle, 0, ",");
        while (($row = fgetcsv($handle, 0, ",")) !== false) {
            $i++;
            $total_count++;
            $data = array();
            foreach ($header as $k => $head) {

                if (strpos($head, '.') !== false) {
                    $h = explode('.', $head);
                    $data[$h[0]][$h[1]] = (isset($row[$k])) ? $row[$k] : '';
                } else {
                    $data['Project'][$head] = (isset($row[$k])) ? $row[$k] : '';
                }
            }
            $data['Project']['email'] = trim($data['Project']['email']);
            $data['Project']['first_name'] = trim($data['Project']['first_name']);
            $data['Project']['last_name'] = trim($data['Project']['last_name']);
            $data['Project']['role'] = trim($data['Project']['role']);
            if (!in_array(strtolower($data['Project']['role']), $roles)){
                $error_count++;
            }else{
                if(!$this->add_new_user($account_id, $current_user_id, $data['Project']['email'], utf8_encode($data['Project']['first_name']), utf8_encode($data['Project']['last_name']), $data['Project']['role'], $send_email, $current_first_name, $current_last_name)){
                    $error_count++;
                }
            }
            /*
            if(isset($data['Project']['email']) && isset($data['Project']['first_name']) && isset($data['Project']['last_name']) && isset($data['Project']['role']) && filter_var($data['Project']['email'], FILTER_VALIDATE_EMAIL))
            {
                $user_already_exist = User::where('email', $data['Project']['email'])->where('site_id', $this->site_id)->count();
                if (strtolower($data['Project']['role']) == 'super admin') {
                    $data['Project']['role'] = 110;
                } else if (strtolower($data['Project']['role']) == 'admin') {
                    $data['Project']['role'] = 115;
                } else if (strtolower($data['Project']['role']) == 'user') {
                    $data['Project']['role'] = 120;
                } else {
                    $data['Project']['role'] = 125;
                }
                if ($user_already_exist > 0) {
                    $email_string .= $data['Project']['email'] . '<br>';

                    $account_detail = User::where('email', $data['Project']['email'])->where('site_id', $this->site_id)->first()->toArray();

                    $user_id = $account_detail['id'];

                    $user_account_detail = UserAccount::where(array('user_id' => $user_id, 'account_id' => $account_id, 'site_id' => $this->site_id))->count();

                    if ($user_account_detail == 0) {

                        $user_account_data = array(
                            'user_id' => $user_id,
                            'account_id' => $account_id,
                            'role_id' => $data['Project']['role'],
                            'is_default' => '1',
                            'permission_maintain_folders' => '1',
                            'permission_access_video_library' => '1',
                            'permission_video_library_upload' => '1',
                            'permission_administrator_user_new_role' => '1',
                            'parmission_access_my_workspace' => '1',
                            'folders_check' => '1',
                            'manage_collab_huddles' => '1',
                            'manage_coach_huddles' => '1',
                            'manage_evaluation_huddles' => '1',
                            'created_date' => date('Y-m-d H:i:s'),
                            'created_by' => $current_user_id,
                            'last_edit_date' => date('Y-m-d H:i:s'),
                            'last_edit_by' => $current_user_id,
                            'is_active' => 1,
                            'site_id'=>$this->site_id
                        );

                        if ($data['Project']['role'] == 120) {
                            $user_account_data = array(
                                'user_id' => $user_id,
                                'account_id' => $account_id,
                                'role_id' => $data['Project']['role'],
                                'is_default' => '1',
                                //  'permission_maintain_folders' => '1',
                                'permission_access_video_library' => '1',
                                //   'permission_video_library_upload' => '1',
                                //   'permission_administrator_user_new_role' => '1',
                                //   'parmission_access_my_workspace' => '1',
                                //   'folders_check' => '1',
                                //   'manage_collab_huddles' => '1',
                                //   'manage_coach_huddles' => '1',
                                //    'manage_evaluation_huddles' => '1',
                                'created_date' => date('Y-m-d H:i:s'),
                                'created_by' => $user_id,
                                'last_edit_date' => date('Y-m-d H:i:s'),
                                'last_edit_by' => $user_id,
                                'is_active' => 1,
                                'site_id'=>$this->site_id
                            );
                        }
                        DB::table('users_accounts')->insert($user_account_data);

                        // if (DB::table('users_accounts')->insert($user_account_data)) {
                        //     return true;
                        // } else {
                        //     return false;
                        // }
                        //$this->UserAccount->save($user_account_data);
                        $users = User::getUserInformation($account_id, $user_id);
                        // if ($send_email) {//dont send email if user already exist
                        //     if ($users) {
                        //         $email_data = array(
                        //             'first_name' => $current_first_name,
                        //             'last_name' => $current_last_name,
                        //             'user_id' => $users->id,
                        //             'email' => $users->email,
                        //             'account_id' => $users->account_id,
                        //             'authentication_token' => $users->authentication_token,
                        //             'message' => '',
                        //             'company_name' => $users->company_name,
                        //         );
                        //     }
                        //     $this->sendConfirmation($email_data);
                        // }
                        $event = [
                            'channel' => "people_" . $account_id,
                            'event' => "user_added",
                            'user_id' => $users
                        ];
                        HelperFunctions::broadcastEvent($event);
                    } else {

                        continue;
                    }

                    continue;
                }

                if (isset($data['Project']) && $data['Project'] != '') {
                    $salt = config('apikeys.salt');
                    $user_data = array(
                        'first_name' => $data['Project']['first_name'],
                        'last_name' => $data['Project']['last_name'],
                        'email' => $data['Project']['email'],
                        'created_date' => date('Y-m-d H:i:s'),
                        'username' => $data['Project']['email'],
                        'authentication_token' => $this->digitalKey(20),
                        //'password' => ($this->site_id == 1 ? sha1($salt . 'sibme1') : sha1($salt . 'hmhcs1')),
                        'type' => 'Invite_Sent',
                        'is_active' => FALSE,
                        'created_by' => $current_user_id,
                        'last_edit_date' => date('Y-m-d H:i:s'),
                        'last_edit_by' => $current_user_id,
                        'site_url'=>$this->site_id
                    );
                    DB::table('users')->insert($user_data);
                    $user_id = DB::getPdo()->lastInsertId();
                    DB::table('users')->where(['id' => $user_id])->update(array('authentication_token' => md5($user_id)));
                    $user_account_data = array(
                        'user_id' => $user_id,
                        'account_id' => $account_id,
                        'role_id' => $data['Project']['role'],
                        'is_default' => '1',
                        'permission_maintain_folders' => '1',
                        'permission_access_video_library' => '1',
                        'permission_video_library_upload' => '1',
                        'permission_administrator_user_new_role' => '1',
                        'parmission_access_my_workspace' => '1',
                        'folders_check' => '1',
                        'manage_collab_huddles' => '1',
                        'manage_coach_huddles' => '1',
                        'manage_evaluation_huddles' => '1',
                        'created_date' => date('Y-m-d H:i:s'),
                        'created_by' => $current_user_id,
                        'last_edit_date' => date('Y-m-d H:i:s'),
                        'last_edit_by' => $current_user_id,
                        'is_active' => 1,
                        'site_url'=>$this->site_id
                    );

                    if (isset($data['Project']) && $data['Project']['role'] == 120) {
                        $user_account_data = array(
                            'user_id' => $user_id,
                            'account_id' => $account_id,
                            'role_id' => $data['Project']['role'],
                            'is_default' => '1',
                            //  'permission_maintain_folders' => '1',
                            'permission_access_video_library' => '1',
                            //   'permission_video_library_upload' => '1',
                            //   'permission_administrator_user_new_role' => '1',
                            //   'parmission_access_my_workspace' => '1',
                            //   'folders_check' => '1',
                            //   'manage_collab_huddles' => '1',
                            //   'manage_coach_huddles' => '1',
                            //    'manage_evaluation_huddles' => '1',
                            'created_date' => date('Y-m-d H:i:s'),
                            'created_by' => $current_user_id,
                            'last_edit_date' => date('Y-m-d H:i:s'),
                            'last_edit_by' => $current_user_id,
                            'is_active' => 1,
                            'site_url'=>$this->site_id
                        );
                    }
                    DB::table('users_accounts')->insert($user_account_data);
                    $users = User::getUserInformation($account_id, $user_id);
                    if ($send_email) {
                        if ($users) {
                            $email_data = array(
                                'first_name' => $current_first_name,
                                'last_name' => $current_last_name,
                                'user_id' => $users->id,
                                'email' => $users->email,
                                'account_id' => $users->account_id,
                                'authentication_token' => $users->authentication_token,
                                'message' => '',
                                'company_name' => $users->company_name,
                            );
                            $this->sendInvitation($email_data);
                        }

                    }
                    $event = [
                        'channel' => "people_" . $account_id,
                        'event' => "user_added",
                        'user_id' => $users
                    ];
                    HelperFunctions::broadcastEvent($event);
                }
            }
            else
            {
                $error_count++;
            }
            */
        }
        // var_dump($this->duplicated_emails);
        // var_dump($this->linked_emails);
        // die;
        if($total_count-$error_count == 0){
            return response()->json(["message"=>"There is no valid user in File.","status"=>false]);
        }
        if($this->duplicated_emails !='' || $this->linked_emails !='' || !empty($error_count))
        {
            if($error_count)
            {
                $msg = "Only ".($total_count - $error_count)." of $total_count users imported, other have invalid data.";
            }
            else
            {
                $msg = 'The import routine was successfull. Following users were skipped as they already exist in the account<br/>'.$this->duplicated_emails;
            }
            $res = array(
                //'message' => "Only ".($total_count - $error_count)." of $total_count users imported, other have invalid data.",
                //'message'=>'The import routine was successfull. Following users were skipped as they already exist in the account<br/>'.$this->duplicated_emails,
                'message' => $msg,
                'status' => 'true',
            );
            return response()->json($res);
        }else{
            $res = array(
                'message' => 'Users Successfully Imported',
                'status' => 'true',
            );
            return response()->json($res);
        }

        // $response_msg  ='';
        // if($this->linked_emails !=''){
        //     $response_msg .='<span style="font-weight: bold;">Following emails are linked successfully with this account</span> <br/>'.$this->linked_emails.'<br/>';
        // }
        // if($this->duplicated_emails != ''){
        //     $response_msg .='<span style="font-weight: bold;">The following users are not added due to duplication </span> <br/>'.$this->duplicated_emails.'<br/>';
        // }
        // if($this->new_emails_added !=''){
        //     $response_msg .= '<span style="font-weight: bold;">Following Users Successfully Imported </span><br/>'.$this->new_emails_added.'<br/>';
        // }


        // return response()->json(array(
        //     'message'=>$response_msg,
        //     'status' => 'true'
        // ));

        // if ($this->duplicated_emails != '') {
        //     $res = array(
        //         'message' => "The following users are not added due to duplication but are automatically linked with this account : </br>" .$this->duplicated_emails,
        //         'status' => 'true',
        //     );

        //     return response()->json($res);
        //     //$this->Session->setFlash('The following users are not added due to duplication but are automatically linked with this account : <br>' . $email_string);
        // } else {
        //     // $this->Session->setFlash('Users Successfully Imported');
        //     $res = array(
        //         'message' => 'Users Successfully Imported',
        //         'status' => 'true',
        //     );

        //     return response()->json($res);
        // }
    }

    public function postUpload()
    {
        $file = Input::file('additional_attachemnt');

        if (Input::hasFile('additional_attachemnt')) {

            $file = Input::file('additional_attachemnt');

            $name = $file->getClientOriginalName();

            //check out the edit content on bottom of my answer for details on $storage

            $path = realpath(dirname(__FILE__) . '/../../..') . "/public/attachments/bulk_import/";

            // Moves file to folder on server
            $file->move($path, $name);

            // Import the moved file to DB and return OK if there were rows affected
            return true;

        }
    }
    public function addUsers(Request $request)
    {
        //$this->auth_permission('people', 'addUsers');
        //$users = $this->Session->read('user_current_account');
        //$account_id = $users['accounts']['account_id'];
        $account_id = $request->input('account_id');
        $current_first_name = $request->input('first_name');
        $current_last_name = $request->input('last_name');
        $current_user_id = $request->input('user_id');
        $custom_message = $request->input('message');
        $send_email = true;
        //$account_owner = $this->UserAccount->find('first', array('conditions' => array('account_id' => $account_id, 'role_id' => 100)));
        $account_owner = UserAccount::where('account_id', $account_id)->where('role_id', 100)->first();
        if($account_owner)
        {
            $account_owner  = $account_owner->toArray();
        }
        $user_id = $account_owner['user_id'];
        //$account_owner_info = $this->User->find('first', array('conditions' => array('id' => $account_owner['UserAccount']['user_id'])));
        $account_owner_info = User::where('id', $user_id)->first();
        if($account_owner_info)
        {
            $account_owner_info = $account_owner_info->toArray();
        }

        // $accounts = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
        $accounts = Account::where('id', $account_id)->first();
        if($accounts)
        {
            $accounts = $accounts->toArray();
        }
        $deactive_plan = Account::select('deactive_plan', 'plan_qty')->where('id', $account_id)->first();
        //$deactive_plan = $this->Account->find('first', array('conditions' => array('id' => $account_id), 'fields' => array('deactive_plan', 'plan_qty')));
        if($deactive_plan)
        {
            $deactive_plan = $deactive_plan->toArray();
        }
        // $this->User->create();
        $userAccountCount = 0;

        if ($request->isMethod('post')) {


            //$account_info = $this->Account->find('first', array('conditions' => array('id' => $account_id)));
            $account_info = Account::where('id', $account_id)->first();
            if($account_info)
            {
                $account_info = $account_info->toArray();
            }

            $mailchimp_list_id = $account_info['mailchimp_list_id'];

            $data = array();
            $current_user_count = 0;
            $users_list = $request->input('users');
            // $r = $request->all();
            // echo "dd<br/>";
            // //print_r($r);
            // dd($r);

            foreach ($users_list as $key => $row) {
                if (isset($row[$key]) && !empty($row[$key])) {
                    $current_user_count++;
                }
            }

            foreach ($users_list as $key => $row) {

                $result = Account::select('plans.category', 'plans.plan_type', 'plans.storage', 'plans.id', 'plans.users', 'accounts.*')
                    ->leftJoin('plans', 'plans.id', '=', 'accounts.plan_id')
                    ->where('accounts.id', $account_id)
                    ->first();

                if($result)
                {
                    $result = $result->toArray();
                }

                $totalUsers = User::getTotalUsers($account_id) + $current_user_count;

                if ($result['in_trial'] == 1) {
                    $plan_users = 40;
                } else {
                    if (!empty($result['category'])) {
                        $plan_users = $deactive_plan['plan_qty'];
                    } else {
                        $plan_users = $result['users'];
                    }
                }

                if (($totalUsers >= Account::get_allowed_users($account_id)) && ($request->input('user_type') != 125)) {
                    $msg_str = '';
                    $user_account_detail = UserAccount::where(array( 'account_id' => $account_id , 'user_id' => $request->input('user_id')  ))->first();
                    if ($user_account_detail->role_id == 100) {
                        if ($result['deactive_plan'] == 1) {

                            $msg_str = TranslationsManager::parse_translation_params($this->language_based_messages['your_account_has_reached_maximum_users_msg'], ['allowed_users' => Account::get_allowed_users($account_id), 'sales_email' => Sites::get_site_settings('static_emails', $this->site_id)['sales']]);

                        } else {
                            $msg_str = TranslationsManager::parse_translation_params($this->language_based_messages['your_account_has_reached_maximum_users_upgrade_or_delete_msg'], ['allowed_users' => Account::get_allowed_users($account_id), 'account_id' => $account_id]);
                        }
                    } else {
                        $msg_str = TranslationsManager::parse_translation_params($this->language_based_messages['your_account_has_reached_maximum_user_contact_account_owner_msg'], ['allowed_users' => Account::get_allowed_users($account_id), 'first_name' => $account_owner_info['first_name'], 'last_name' => $account_owner_info['last_name'], 'account_owner_email' => $account_owner_info['email']]);
                    }
                    //this is remaing function
                    // $this->users_full_email($users['User']['email'], $request->get('users'][$key + 1]['email'], $account_id);
                    $result = ["status" => false, "message" => $msg_str];
                    return response()->json($result, 200, [], JSON_PRETTY_PRINT);

                }
                if (!empty($row)) {
                    $name = explode(' ', $row['name']);
                    $arr['first_name'] = isset($name[0]) ? $name[0] : '';
                    $arr['last_name'] = isset($name[1]) ? $name[1] : '';
                }

                if (isset($row['email'])) {
                    $arr['created_by'] = $request->input('user_id');
                    $arr['created_date'] = date('Y-m-d H:i:s');
                    $arr['last_edit_by'] = $request->input('user_id');
                    $arr['last_edit_date'] = date('Y-m-d H:i:s');
                    //$arr['account_id'] = $account_id;
                    $arr['email'] =trim($row['email']);
                    //$arr['company_name'] = $account_info["company_name"];
                    $arr['authentication_token'] = $this->digitalKey(20);
                    $arr['is_active'] = FALSE;
                    $arr['site_id'] = $this->site_id;
                    $arr['type'] = 'Invite_Sent';
                    if ($arr['first_name'] != '' && $arr['email'] != '') {
                        $data[] = $arr;
                    }
                }
            }

            $roleData = array();
            $newly_added_users ='';
            $exUser = null;
            $users_result =array();
            foreach ($data as $user) {

                if (!empty($mailchimp_list_id)) {
                    $role = $request->post('user_type');
                    $new_segment_id = '';
                    $segment_details = $this->mailchimp->get("lists/$mailchimp_list_id/segments");
                    if (!empty($segment_details['segments'])) {
                        foreach ($segment_details['segments'] as $row) {
                            if ($role == 110 && $row['name'] == 'Super Admins') {
                                $new_segment_id = $row['id'];
                            }

                            if ($role == 115 && $row['name'] == 'Admins') {
                                $new_segment_id = $row['id'];
                            }

                            if ($role == 120 && $row['name'] == 'Users') {
                                $new_segment_id = $row['id'];
                            }

                            if ($role == 125 && $row['name'] == 'Viewers') {
                                $new_segment_id = $row['id'];
                            }
                        }
                        $this->mailchimp->post('/lists/' . $mailchimp_list_id . '/members', [
                            "status" => "subscribed",
                            "email_address" => trim($user['email']),
                        ]);

                        $this->mailchimp->post('lists/' . $mailchimp_list_id . '/segments/' . $new_segment_id . '/members', [
                            //   "status" => "subscribed",
                            "email_address" => trim($user['email']),
                        ]);
                    }

                }

                if (isset($user['first_name']) && isset($user['email']) && $user['email']) {
                    $user['site_id'] = $this->site_id;
                    $exUser = User::where('email', $user['email'])->first();
                    if($exUser)
                    {
                        $exUser = $exUser->toArray();
                    }

                    if ($exUser) {
                        $user_id = $exUser['id'];
                    } else {
                        $user_id = DB::table('users')->insertGetId($user);
                        $data_res = array('authentication_token' =>  md5($user_id));
                        DB::table('users')->where('id', $user_id)->update($data_res);

                        $user['authentication_token'] = md5($user_id);
                    }
                    if (!empty($user_id)) {

                        $userAccountCount = UserAccount::where([
                            "user_id" => $user_id,
                            "account_id" => $account_id,
                        ])->count();
                        //save goals users settings
                        $this->saveGoalsettings($request->input('user_type'),$user_id,$account_id);
                        if ($userAccountCount == 0) {
                            $userAccount = array(
                                'account_id' => $account_id,
                                'user_id' => $user_id,
                                'role_id' => $request->input('user_type'),
                                'is_default' => true,
                                'created_by' => $request->input('user_id'),
                                'created_date' => date("Y-m-d H:i:s"),
                                'last_edit_date' => date("Y-m-d h:i:s"),
                                'last_edit_by' => $request->input('user_id'),
                            );
                            if ($request->input('user_type') == 110) {
                                $userAccount['permission_maintain_folders'] = true;
                                $userAccount['permission_access_video_library'] = true;
                                $userAccount['permission_video_library_upload'] = true;
                                $userAccount['permission_administrator_user_new_role'] = true;
                                $userAccount['parmission_access_my_workspace'] = true;
                                $userAccount['folders_check'] = true;
                                $userAccount['manage_collab_huddles'] = true;
                                $userAccount['manage_coach_huddles'] = true;
                                $userAccount['live_recording'] = true;
                            }
                            if ($request->input('user_type') == 115) {
                                $userAccount['permission_maintain_folders'] = true;
                                $userAccount['permission_access_video_library'] = true;
                                $userAccount['permission_video_library_upload'] = 0;
                                $userAccount['permission_administrator_user_new_role'] = true;
                                $userAccount['parmission_access_my_workspace'] = true;
                                $userAccount['folders_check'] = true;
                                $userAccount['manage_collab_huddles'] = true;
                                $userAccount['manage_coach_huddles'] = true;
                            }

                            if ($request->input('user_type') == 125) {
                                $userAccount['permission_maintain_folders'] = false;
                                $userAccount['permission_access_video_library'] = true;
                                $userAccount['permission_video_library_upload'] = false;
                                $userAccount['permission_administrator_user_new_role'] = false;
                                $userAccount['parmission_access_my_workspace'] = false;
                                $userAccount['folders_check'] = false;
                                $userAccount['manage_collab_huddles'] = false;
                                $userAccount['manage_coach_huddles'] = false;
                            }
                            if ($request->input('user_type') == 120) {
                                $userAccount['permission_access_video_library'] = true;
                            }
                            $userAccount['is_active'] = 0;
                            $userAccount['site_id'] = $this->site_id;
                            if ($exUser) {
                                $userAccount['is_active'] = 1;
                                $userAccount['status_type'] = 'Active';
                            }
                            DB::table('users_accounts')->insertGetId($userAccount);
                            if ($user) {
                                $first_name = isset($user['first_name']) ? $user['first_name'] : '';
                                $last_name = isset($user['last_name']) ? $user['last_name'] : '';

                                $user['user_id'] = $user_id;
                                $user['site_id'] = $this->site_id;
                                $user['message'] = $request->input('message');
                                $user['role_title'] = '';
                                if ($request->input('user_type') == 110) {
                                    $user['role_title'] = 'a Super Admin';
                                } elseif ($request->input('user_type') == 115) {
                                    $user['role_title'] = 'an Admiin';
                                } elseif ($request->input('user_type') == 120) {
                                    $user['role_title'] = 'a User';
                                } elseif ($request->input('user_type') == 125) {
                                    $user['role_title'] = 'a Viewer';
                                } else {
                                    $user['role_title'] = 'a User';
                                }
                                if (!$exUser && $send_email) {
                                    $user['account_id']= $account_id;
                                    $user['company_name'] = $account_info["company_name"];
                                    $user["first_name"] = $current_first_name;
                                    $user["last_name"] = $current_last_name;
                                    if(!empty($custom_message))
                                    {
                                        $user["custom_message"] = $custom_message;
                                    }

                                    $this->sendInvitation($user);

                                } else {
                                    $user['account_id']= $account_id;
                                    $user['company_name'] = $account_info["company_name"];
                                    $user["first_name"] = $current_first_name;
                                    $user["last_name"] = $current_last_name;
                                    if(!empty($custom_message))
                                    {
                                        $user["custom_message"] = $custom_message;
                                    }
                                    $this->sendConfirmation($user);

                                }
                                $user_activity_logs = array(
                                    'ref_id' => $user_id,
                                    'desc' => $accounts['company_name'],
                                    'url' => $this->base . '/users/administrators_groups/' . $account_id . '/' . $user_id,
                                    'account_folder_id' => '',
                                    'type' => '7',
                                );

                                $this->user_activity_logs($user_activity_logs);
                            }

                            //$newly_added_users = $this->Session->read('newly_added_users');
                            $newly_added_users = 'newly_added_users';
                            if (empty($newly_added_users)) {
                                $newly_added_users = $user_id;
                            } else {
                                $newly_added_users .= "," . $user_id;
                            }

                        }
                    }
                }
                $users = User::getUserInformation($account_id, $user_id);
                $users_result[] =$users;
                Goal::updateGoalSettingsForNewUser($user_id, $account_id, $request->input('user_type'));
                $event = [
                    'channel' => "people_".$account_id,
                    'event' => "user_added",
                    'user_id' => $users_result,
                    'role_id'=>$users['role_id']
                ];
                if($userAccountCount == 0)
                {
                    HelperFunctions::broadcastEvent($event);
                }

            }
            // $result = ["status" => true, "message" => $newly_added_users];
            // return response()->json($result, 200, [], JSON_PRETTY_PRINT);

            /*if($send_email && !$exUser)// invalid place to send email
            {
                if ($users) {
                    $email_data = array(
                        'first_name' => $current_first_name,
                        'last_name' => $current_last_name,
                        'user_id' => $users->id,
                        'email' => $users->email,
                        'account_id' => $users->account_id,
                        'authentication_token' => $users->authentication_token,
                        'message' => '',
                        'company_name' => $users->company_name,
                    );
                }
                $this->sendInvitation($email_data);
            }*/

            if ($userAccountCount == 0) {
                $role_name = '';
                if($request->input('user_type')==110){
                    $role_name = $this->language_based_messages['super_admin_msg'];
                }elseif($request->input('user_type')==115){
                    $role_name = $this->language_based_messages['admin_msg'];
                }elseif($request->input('user_type')==120){
                    $role_name = $this->language_based_messages['user_msg'];
                }elseif($request->input('user_type')==125){
                    $role_name = $this->language_based_messages['viewer_msg'];
                }
                $msg_str = $role_name . ' ' . $this->language_based_messages['has_been_added_successfully_msg'];
                $result = ["status" => true, "message" => $msg_str];
                return response()->json($result, 200, [], JSON_PRETTY_PRINT);
            } else {
                $msg_str = $this->language_based_messages['user_already_exists_msg'];
                $result = ["status" => true, "message" => $msg_str];
                return response()->json($result, 200, [], JSON_PRETTY_PRINT);
            }
        } else {
            $msg_str = $this->language_based_messages['please_provide_name_and_fields_msg'];
            $result = ["status" => true, "message" => $msg_str];
            return response()->json($result, 200, [], JSON_PRETTY_PRINT);
        }

    }

    public function user_activity_logs($data, $l_account_id = '', $l_user_id = '')
    {

        $data['account_id'] = $l_account_id;
        $data['user_id'] = $l_user_id;
        $data['site_id'] = $this->site_id;

        $data['date_added'] = date('Y-m-d H:i:s');
        $data['app_name'] = UserActivityLog::get_app_name();

        $result = DB::table('user_activity_logs')->insert($data);

        $analyticsHuddlesSummary = new \App\Models\AnalyticsHuddlesSummary;
        $analyticsHuddlesSummary->saveRecord($data['account_id'], $data['account_folder_id'], $data['user_id'], $this->site_id, $data['type']);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function sendInvitation($data, $account_folder_id = '')
    {

        $this->subject_title = Sites::get_site_settings('email_subject', $this->site_id);
        $site_title = Sites::get_site_settings('site_title', $this->site_id);
        $users = User::where('id', $data['user_id'])->first()->toArray();
        $email_from = $site_title . '<' . Sites::get_site_settings('static_emails', $this->site_id)['noreply'] . '>';
        $email_to = $data['email'];
        $lang = $users['lang'];
        if ($lang == 'en') {
            $email_subject = $this->subject_title . " - Account Invitation";
        } else {
            $email_subject = $this->subject_title . " - Invitación a Cuenta";
        }
        $user_id =  $data['user_id'];
        $sibme_base_url = config('s3.sibme_base_url');
        $unsubscribe = $sibme_base_url . 'subscription/unsubscirbe_now/' . $user_id . '/11';
        $key = "account_invitation_" . $this->site_id . "_" . $lang;
        //$sibme_base_url = config('s3.sibme_base_url');
        $activation_url = $sibme_base_url . '/Users/activation/' . md5($data['account_id']) . "/" . $data['authentication_token'] . "/" . md5($data['user_id']) . "/" . $account_folder_id;
        $result = SendGridEmailManager::get_send_grid_contents($key);
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $data['first_name'] . ' ' . $data['last_name'], $html);
        $html = str_replace('{company_name}', $data['company_name'], $html);
        $html = str_replace('{redirect}', $activation_url, $html);
        $html = str_replace('{site_title}', Sites::get_site_settings('site_title', $this->site_id), $html);
        $html = str_replace('{unsubscribe}',  $unsubscribe, $html);
        if(isset($data["custom_message"]) && !empty($data["custom_message"]))
        {
            $html = str_replace('{custom_message}', $this->group_based_translation["people_email_custom_message"].": ".$data["custom_message"], $html);
        }
        else
        {
            $html = str_replace('{custom_message}', "  ", $html);
        }
        $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
        $auditEmail = array(
            'account_id' => $data['account_id'],
            'site_id' => $this->site_id,
            'email_from' => $email_from,
            'email_to' => $data['email'],
            'email_subject' => $email_subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s"),
        );

        DB::table('audit_emails')->insert($auditEmail);

        $use_job_queue = config('s3.use_job_queue');
        if ($use_job_queue) {
            $this->add_job_queue(1, $email_to, $email_subject, $html, true);
            return true;
        }

    }

    public function sendInvitation_via_HMH_Licensing($data, $account_folder_id = '')
    {

        // $this->subject_title = Sites::get_site_settings('email_subject', $this->site_id);
        $site_title = Sites::get_site_settings('site_title', $this->site_id);
        $users = User::where('id', $data['user_id'])->first()->toArray();
        $email_from = $site_title . '<' . Sites::get_site_settings('static_emails', $this->site_id)['noreply'] . '>';
        $email_to = $data['email'];
        $email_subject = "Create Your HMH Coaching Studio Account";

        $key = "hmh_account_credentials_email_en";
        $sibme_base_url = config('s3.sibme_base_url');
        $activation_url = $sibme_base_url . 'Users/activation/' . md5($data['account_id']) . "/" . $data['authentication_token'] . "/" . md5($data['user_id']) . "/" . $account_folder_id;
        $result = SendGridEmailManager::get_send_grid_contents($key);
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{first_name}', $users['first_name'], $html);
        $html = str_replace('{redirect}', $activation_url, $html);
        $html = str_replace('{unsubscribe}', '#', $html);
        $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);

        $auditEmail = array(
            'account_id' => $data['account_id'],
            'site_id' => $this->site_id,
            'email_from' => $email_from,
            'email_to' => $data['email'],
            'email_subject' => $email_subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s"),
        );

        DB::table('audit_emails')->insert($auditEmail);

        $use_job_queue = 0; //config('s3.use_job_queue');
        if (!empty($use_job_queue)) {
            $this->add_job_queue(1, $email_to, $email_subject, $html, true);
        } else {
            $emailData = [
                'from' => Sites::get_site_settings('static_emails', $this->site_id)['noreply'],
                'from_name' => Sites::get_site_settings('site_title', $this->site_id),
                'to' => $data['email'],
                'subject' => $email_subject,
                'template' => $html,
            ];
            Email::sendCustomEmail($emailData);
        }
        return TRUE;

    }

    public function send_generic_email($data,$email_key, $email_subject, $redirect_url = "", $use_job_queue = null)
    {

        $this->subject_title = Sites::get_site_settings('email_subject', $this->site_id);
        $site_title = Sites::get_site_settings('site_title', $this->site_id);
        $email_from = $site_title . '<' . Sites::get_site_settings('static_emails', $this->site_id)['noreply'] . '>';
        $email_to = $data['email'];
        $lang = $this->lang;
        /*if ($lang == 'en') {
            $email_subject = $this->subject_title . " - Account Invitation";
        } else {
            $email_subject = $this->subject_title . " - Invitación a Cuenta";
        }*/

        $key = $email_key . "_" . $this->site_id . "_" . $lang;
        $sibme_base_url = config('s3.sibme_base_url');
        //$activation_url = $sibme_base_url . '/Users/activation/' . md5($data['account_id']) . "/" . $data['authentication_token'] . "/" . md5($data['user_id']) . "/" . $account_folder_id;
        $result = SendGridEmailManager::get_send_grid_contents($key);
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $data['first_name'] . ' ' . $data['last_name'], $html);
        $html = str_replace('{company_name}', $data['company_name'], $html);
        if(empty($redirect_url))
        {
            $redirect_url = $sibme_base_url;
        }
        $html = str_replace('{redirect}', $redirect_url, $html);
        $html = str_replace('{site_title}', Sites::get_site_settings('site_title', $this->site_id), $html);
        $html = str_replace('{unsubscribe}', '#', $html);
        $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
        if($email_key == "change_password_email" && isset($data['password']))
        {
            $html = str_replace('{new_password}', $data['password'], $html);
        }
        $auditEmail = array(
            'account_id' => $data['account_id'],
            'site_id' => $this->site_id,
            'email_from' => $email_from,
            'email_to' => $data['email'],
            'email_subject' => $email_subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s"),
        );

        DB::table('audit_emails')->insert($auditEmail);
        if (is_null($use_job_queue)) {
            $use_job_queue = config('s3.use_job_queue');
        }
        if ($use_job_queue) {
            $this->add_job_queue(1, $email_to, $email_subject, $html, true);
            return true;
        } else {
            // To do
            // Use Email::sendCustomEmail function here.
        }

    }

    public function add_job_queue($jobid, $to, $subject, $html, $is_html = false, $reply_to = '')
    {
        if ($is_html) {
            $ihtml = 'Y';
        } else {
            $ihtml = 'N';
        }

        if ($reply_to != '') {
            $reply_to = "<replyto>$reply_to</replyto>";
        }
        $html = '<![CDATA[' . $html . ']]>';
        $xml = "<mail><to>$to</to>$reply_to <from>" . Sites::get_site_settings('static_emails', $this->site_id)['noreply'] . "</from><fromname>" . Sites::get_site_settings('site_title', $this->site_id) . "</fromname><bcc /><cc /><subject><![CDATA[$subject]]></subject><body>$html</body><html>$ihtml</html></mail>";

        $data = array(
            'JobId' => $jobid,
            'CreateDate' => date("Y-m-d H:i:s"),
            'RequestXml' => $xml,
            'JobQueueStatusId' => 1,
            'site_id' => $this->site_id,
            'CurrentRetry' => 0,
            'JobSource' => Sites::get_base_url($this->site_id)
        );

        DB::table('JobQueue')->insert($data);
        return true;
    }

    public function sendConfirmation($data)
    {

        $users = User::where('id', $data['user_id'])->first()->toArray();
        $site_title = Sites::get_site_settings('site_title', $this->site_id);
        $email_from = $site_title . '<' . Sites::get_site_settings('static_emails', $this->site_id)['noreply'] . '>';
        $email_to = $data['email'];
        $lang = $users['lang'];
        $user_id = $data['user_id'];
        if ($lang == 'en') {
            $email_subject = $this->subject_title . " - Account Invitation";
        } else {
            $email_subject = $this->subject_title . " -  Invitación a Cuenta";
        }
        $sibme_base_url = config('s3.sibme_base_url');
        $unsubscribe = $sibme_base_url . 'subscription/unsubscirbe_now/' . $user_id . '/11';
        $key = "account_invitation_" . $this->site_id . "_" . $lang;
        $result = SendGridEmailManager::get_send_grid_contents($key);
        //$sibme_base_url = config('s3.sibme_base_url');
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $data['first_name'] . ' ' . $data['last_name'], $html);
        $html = str_replace('{site_title}', Sites::get_site_settings('site_title', $this->site_id), $html);
        $html = str_replace('{company_name}', $data['company_name'], $html);
        $html = str_replace('{redirect}', $sibme_base_url, $html);
        $html = str_replace('{unsubscribe}', $unsubscribe, $html);
        $html = str_replace('{site_title}', Sites::get_site_settings('site_title', $this->site_id), $html);
        if(isset($data["custom_message"]) && !empty($data["custom_message"]))
        {
            $html = str_replace('{custom_message}', $this->group_based_translation["people_email_custom_message"].": ".$data["custom_message"], $html);
        }
        else
        {
            $html = str_replace('{custom_message}', "  ", $html);
        }
        $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);

        $auditEmail = array(
            'account_id' => $data['account_id'],
            'site_id' => $this->site_id,
            'email_from' => $email_from,
            'email_to' => $data['email'],
            'email_subject' => $email_subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s"),
        );

        if (!empty($data['email'])) {
            DB::table('audit_emails')->insert($auditEmail);
            $use_job_queue = config('s3.use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $email_to, $email_subject, $html, true);
                return true;
            }
        }
        return false;
    }
    public function create_custom_request($data)
    {
        $request = new Request($data);
        return $request;
    }
    public function getGroupsAfterAdd(Request $request)
    {

        // $sql = "select * from groups where account_id=" . $account_id . " AND site_id =" . $this->site_id;
        // $Groups = $this->User->query($sql);
        $account_id = $request->input('account_id');
        $single_group = $request->has("single_group")?$request->get("single_group"):0;
        $query = Group::where([
            ['account_id', '=', $account_id],
            ['site_id', '=', $this->site_id],
        ]);
        if($single_group)
        {
            $group_id = $request->get("group_id");
            $query->where("id",$group_id);
        }
        $Groups = $query->orderBy('created_date', 'DESC' )->get()->toArray();

        $arr = array();
        $arr2 = array();
        $i = 0;

        if ($Groups) {
            foreach ($Groups as $res) {
                $arr[$i]['department_name'] = $res['name'];
                $arr[$i]['group_id'] = $res['id'];
                $arr[$i]['users'] = [];
                $userGroups = UserGroup::select('user_groups.*', 'u.*', 'ua.role_id', 'ua.account_id', 'user_groups.id as user_group_id')
                    ->leftJoin('users as u', 'u.id', '=', 'user_groups.user_id')
                    ->leftJoin('users_accounts as ua', 'ua.user_id', '=', 'u.id')
                    ->where([['user_groups.group_id', '=', $res['id']], ['ua.account_id', '=', $account_id]])
                    ->get()->toArray();

                // $userGroups = $this->User->query($sql);

                foreach ($userGroups as $res1) {

                    $arr2['username'] = $res1['username'];
                    $arr2['email'] = $res1['email'];
                    $arr2['created_at'] = $res1['created_date'];
                    $arr2['id'] = $res1['id'];
                    $arr2['first_name'] = $res1['first_name'];
                    $arr2['last_name'] = $res1['last_name'];
                    $arr2['full_name'] = $res1['first_name']." ".$res1['last_name'];
                    $arr2['account_id'] = $res1['account_id'];
                    $arr2['image'] = $res1['image'];
                    $arr2['user_group_id'] = $res1['user_group_id'];
                    $arr2['role'] = $res1['role_id'];
                    $arr[$i]['users'][] = $arr2;
                }
                $i++;
            }
            
            return  $arr;
        } else {           
            return false;
        }
    }
    public function getGroups(Request $request)
    {

        // $sql = "select * from groups where account_id=" . $account_id . " AND site_id =" . $this->site_id;
        // $Groups = $this->User->query($sql);
        $account_id = $request->input('account_id');
        $single_group = $request->has("single_group")?$request->get("single_group"):0;
        $query = Group::where([
            ['account_id', '=', $account_id],
            ['site_id', '=', $this->site_id],
        ]);
        if($single_group)
        {
            $group_id = $request->get("group_id");
            $query->where("id",$group_id);
        }
        $Groups = $query->orderBy('created_date', 'DESC' )->get()->toArray();

        $arr = array();
        $arr2 = array();
        $i = 0;

        if ($Groups) {
            foreach ($Groups as $res) {
                $arr[$i]['department_name'] = $res['name'];
                $arr[$i]['group_id'] = $res['id'];
                $arr[$i]['users'] = [];
                $userGroups = UserGroup::select('user_groups.*', 'u.*', 'ua.role_id', 'ua.account_id', 'user_groups.id as user_group_id')
                    ->leftJoin('users as u', 'u.id', '=', 'user_groups.user_id')
                    ->leftJoin('users_accounts as ua', 'ua.user_id', '=', 'u.id')
                    ->where([['user_groups.group_id', '=', $res['id']], ['ua.account_id', '=', $account_id]])
                    ->get()->toArray();

                // $userGroups = $this->User->query($sql);

                foreach ($userGroups as $res1) {

                    $arr2['username'] = $res1['username'];
                    $arr2['email'] = $res1['email'];
                    $arr2['created_at'] = $res1['created_date'];
                    $arr2['id'] = $res1['id'];
                    $arr2['first_name'] = $res1['first_name'];
                    $arr2['last_name'] = $res1['last_name'];
                    $arr2['full_name'] = $res1['first_name']." ".$res1['last_name'];
                    $arr2['account_id'] = $res1['account_id'];
                    $arr2['image'] = $res1['image'];
                    $arr2['user_group_id'] = $res1['user_group_id'];
                    $arr2['role'] = $res1['role_id'];
                    $arr[$i]['users'][] = $arr2;
                }
                $i++;
            }
            $result = ["status" => true, "data" => $arr];
            return response()->json($result, 200, [], JSON_PRETTY_PRINT);
        } else {
            $result = ["status" => false, "data" => []];
            return response()->json($result, 200, [], JSON_PRETTY_PRINT);
        }
    }

    public function deleteAccount(Request $request)
    {
        $user_id = $request->get('user_id');
        $current_user_id = $request->get('current_user_id');
        $logout = $request->has('logout') ? $request->get('logout') : 0;
        $flashRequired = 1;
        $company_name = $request->get('company_name');
        $MailChimp = $this->mailchimp;
        $input = $request->all();
        $current_account_id = $request->get('current_account_id');
        $affectedRows = 0;
        if ($user_id) {
//            $user_account = $this->UserAccount->find('first', array('conditions' => array(
            //                    'user_id' => $user_id,
            //                    'account_id' => $current_account_id
            //                ))
            //            );
            //get account owner
            $account_info = Account::where('id', $current_account_id)->first();
            $user_data = User::where('id', $user_id)->first();
            $email_id = $user_data->email;
            $mailchimp_list_id = $account_info->mailchimp_list_id;
            $subscriber_hash = $MailChimp->subscriberHash($email_id);
            $MailChimp->delete("lists/$mailchimp_list_id/members/$subscriber_hash");
//            $MailChimp->success()
            $user_activity_logs = array(
                'ref_id' => $user_id,
                'desc' => $company_name,
                'url' => $this->base . '/users/administrators_groups/' . $current_account_id . '/' . $user_id,
                'account_folder_id' => '',
                'type' => '17',
            );

            $this->user_activity_logs($user_activity_logs);

            $current_account_owner = UserAccount::where('role_id', '100')->where('account_id', $current_account_id)->first();

            if(empty($current_account_owner))
            {
                return response()->json(["data" => null, "message" => $this->language_based_messages['we_are_sorry_something_went_wrong_msg'], "status" => false]);
            }

            $current_account_owner_id = $current_account_owner->user_id;
            $account_folder = array(
                'created_by' => $current_account_owner_id,
                'last_edit_by' => $current_account_owner_id,
            );

            $account_folder_users = array(
                'user_id' => $current_account_owner_id,
                'created_by' => $current_account_owner_id,
                'last_edit_by' => $current_account_owner_id,
            );
            $accont_folders =  \DB::table("account_folder_users")->where('user_id', $user_id)

                ->whereIn('account_folder_id', function ($query) use ($current_account_id) {
                    $query->select('account_folder_id')
                        ->from('account_folders')
                        ->where('account_folders.account_id', $current_account_id);
                })
                ->whereIn('role_id', array(210, 220))->get();

            $result = UserAccount::where('user_id', $user_id)->where('account_id', $current_account_id)->first();
            if($accont_folders){
                foreach($accont_folders as $row){
                    $result_log = array(
                        'entity_id'=> $row->account_folder_id,
                        'deleted_entity_id'=>$user_id,
                        'entity_name'=>$company_name,
                        'entity_data'=>json_encode($result),
                        'created_by'=>$current_user_id,
                        'created_at'=> date("Y-m-d H:i:s"),
                        'site_id'=>$this->site_id,
                        'ip_address'=>$_SERVER['REMOTE_ADDR'],
                        'posted_data'=>json_encode($input),
                        'action'=> $_SERVER['REQUEST_URI'],
                        'user_agenet'=>$_SERVER['HTTP_USER_AGENT'],
                        'request_url'=>$_SERVER['REQUEST_URI'],
                        'platform'=>'',
                        'created_date' => date("Y-m-d H:i:s"),
                    );
                    DB::table('audit_deletion_log')->insert($result_log);
                }
            }

            if(empty($result))
            {
                return response()->json(["data" => null, "message" => $this->language_based_messages['we_are_sorry_something_went_wrong_msg'], "status" => false]);
            }
            AccountFolderUser::where(array(
                'user_id' => $user_id,
                'role_id' => 200,
            ))
                ->whereIn('account_folder_id', function ($query) use ($current_account_id) {
                    $query->select('account_folder_id')
                        ->from('account_folders')
                        ->where('account_folders.account_id', $current_account_id);
                })
                ->update($account_folder_users);

            \DB::table("account_folder_users")->where('user_id', $user_id)
                ->whereIn('account_folder_id', function ($query) use ($current_account_id) {
                    $query->select('account_folder_id')
                        ->from('account_folders')
                        ->where('account_folders.account_id', $current_account_id);
                })
                ->whereIn('role_id', array(210, 220))->delete();

            AccountFolder::where(array(
                'account_id' => $current_account_owner->account_id,
                'folder_type' => 3,
                'created_by' => $user_id,
            ))->update(array('active' => 0));

            AccountFolder::where(array(
                'created_by' => $user_id,
                'account_id' => $current_account_id,
            ))->whereIn('folder_type', array(1, 2))->update($account_folder);

//            $account_huddles = $this->AccountFolderUser->get_all_account_folders_user_by_account($user_id, $current_account_id);
            //            if ($account_huddles) {
            //
            //                for ($i = 0; $i < count($account_huddles); $i++) {
            //                    $this->AccountFolderUser->deleteAll(array('account_folder_user_id' => $account_huddles[$i]['AccountFolderUser']['account_folder_user_id']));
            //                }
            //            }

            UserGroup::where('user_id', $user_id)->delete();
            \DB::table("users_accounts")->where(array('user_id' => $user_id, 'account_id' => $current_account_id))->delete();
            \DB::table("account_user_codes")->where(array('user_id' => $user_id, 'account_id' => $current_account_id))->update(array('is_used' => '0' , 'num_tries' => '0' , 'user_id' => NULL ));
            Goal::setAccountOwnerForThisUser($user_id, $current_account_owner->account_id);
            $affectedRows = 1;
            if ($current_user_id == $user_id) {
                $logout = 1;
            } else {
                $logout = 0;
            }
            $user_accounts = UserAccount::where('user_id', $user_id)->count();
            if ($user_accounts == 0) {
                //$this->User->deleteAll(array('id' => $user_id));
                //                $user_data = $this->User->find('first', array('conditions' => array('id' => $user_id)));
                User::where('id', $user_id)->update(array(
                    'email' => $user_data->email . '-deleted',
                    'username' =>  $user_data->username . '-deleted',
                    'is_active' => FALSE,
                ));
            }
        }
        $event = [
            'channel' => "people_".$current_account_id,
            'event' => "user_deleted",
            'user_id' => $user_id,
            'role_id' => $result->role_id,
        ];
        HelperFunctions::broadcastEvent($event);
        $message = "Success";
        if ($flashRequired === 1) {
            if ($affectedRows > 0 && $logout == 1) {
                if ($result->role_id == 110) {
                    $message = TranslationsManager::get_translation('successfully_deleted_super_admin_msg', "flash_messages", $this->site_id);
                } elseif ($result->role_id == 115) {
                    $message = TranslationsManager::get_translation('successfully_deleted_admin_msg', "flash_messages", $this->site_id);
                } elseif ($result->role_id == 120) {
                    $message = TranslationsManager::get_translation('successfully_deleted_user_msg', "flash_messages", $this->site_id);
                } elseif ($result->role_id == 125) {
                    $message = TranslationsManager::get_translation('successfully_deleted_viewer_msg', "flash_messages", $this->site_id);
                }
                return response()->json(["data" => null, "message" => $message, "status" => true, "redirect_to" => '/users/logout', 'should_logout' => $logout]);
            } elseif ($affectedRows > 0) {
                if ($result->role_id == 110) {
                    $message = TranslationsManager::get_translation('successfully_deleted_super_admin_msg', "flash_messages", $this->site_id);
                } elseif ($result->role_id == 115) {
                    $message = TranslationsManager::get_translation('successfully_deleted_admin_msg', "flash_messages", $this->site_id);
                } elseif ($result->role_id == 120) {
                    $message = TranslationsManager::get_translation('successfully_deleted_user_msg', "flash_messages", $this->site_id);
                } elseif ($result->role_id == 125) {
                    $message = TranslationsManager::get_translation('successfully_deleted_viewer_msg', "flash_messages", $this->site_id);
                }
                return response()->json(["data" => null, "message" => $message, "status" => true, "redirect_to" => '/users/administrators_groups']);
            } else {
                if ($result->role_id == 110) {
                    $message = TranslationsManager::get_translation('unable_to_delete_super_admin_msg', "flash_messages", $this->site_id);
                } elseif ($result->role_id == 115) {
                    $message = TranslationsManager::get_translation('unable_to_delete_admin_msg', "flash_messages", $this->site_id);
                } elseif ($result->role_id == 120) {
                    $message = TranslationsManager::get_translation('unable_to_delete_user_msg', "flash_messages", $this->site_id);
                } elseif ($result->role_id == 125) {
                    $message = TranslationsManager::get_translation('unable_to_delete_viewer_msg', "flash_messages", $this->site_id);
                }
                return response()->json(["data" => null, "message" => $message, "status" => false, "redirect_to" => '/users/administrators_groups']);
            }
        } else {
            if ($affectedRows > 0) {
                return response()->json(["data" => null, "message" => "Successfully Removed " . $email_id, "status" => true]);
            }
        }
    }

    public function change_password_accounts(Request $request)
    {

        //$this->set('title_for_layout', "Change Password");
        //$this->Auth->logout();
        //$url = Router::url(array('controller' => 'users', 'action' => 'change_password'), true) . '/' . $key;
        //$success_url = Router::url(array('controller' => 'users', 'action' => 'login'), true);
        $message = "";
        $status = false;
        if (!empty($request->all())) {
            $validation_message = '';
            $password = $request->get('password');
            $conf_password = $request->get('confirm_password');
            $user_id = $request->get('user_id');
            $account_id = $request->get('account_id');
            $send_email = $request->get('send_email');
            $first_name = $request->get('first_name');
            $last_name = $request->get('last_name');

            if (empty($password)) {
                //$validation_message .='<p>New Password field required</p>';
                $message = TranslationsManager::get_translation('New_Password_field_required', "flash_messages", $this->site_id);
            } else if (strlen($password) < 6) {
                $message = TranslationsManager::get_translation('password-must-contain-a-minimum-of-6-characters', "flash_messages", $this->site_id);
            } else if (empty($conf_password)) {
                //$validation_message.='<p>Confirm Password field required.</p>';
                $message = TranslationsManager::get_translation('confirm-password-field-required', "flash_messages", $this->site_id);
            } else if ($password != $conf_password) {
                $message = TranslationsManager::get_translation('passwords-do-not-match', "flash_messages", $this->site_id);
            }

            //$validation_message = '';

            if (!empty($password) && !empty($conf_password) && $password == $conf_password && empty($message)) {
                //add change password logic
                $user = User::where('id', $user_id)->first();

                if ($user) {
                    $salt = config('apikeys.salt');
                    $password = sha1($salt . $password);
                    $data = array(
                        'password' => $password,
                        'reset_password_token' => null,
                    );
                    User::where("id", $user->id)->update($data);
                    if($send_email)
                    {
                        $lang = $this->lang;
                        if($lang == "es")
                        {
                            $email_subject = $this->subject_title. "Password Changed";
                        }
                        else
                        {
                            $email_subject = $this->subject_title. "Contraseña cambiada";
                        }
                        $users = User::getUserInformation($account_id, $user_id);
                        if ($users) {
                            $email_data = array(
                                'first_name' => $first_name,
                                'last_name' => $last_name,
                                'user_id' => $users->id,
                                'email' => $users->email,
                                'account_id' => $users->account_id,
                                'authentication_token' => $users->authentication_token,
                                'message' => '',
                                'company_name' => $users->company_name,
                                'password'=>$conf_password
                            );
                        }
                        $this->send_generic_email($email_data,"change_password_email", $email_subject, "");
                    }
                    $message = TranslationsManager::get_translation('people_password_changed_successfully', "flash_messages", $this->site_id);
                    $status = true;
//                    $this->Session->setFlash('Password has been successfully changed.', 'default', array('class' => 'message success'));
                    //                    $this->redirect('users/administrators_groups');
                } else {
                    $message = "User not found!";
                }
            }
        }
        return response()->json(["data" => null, "message" => $message, "status" => $status]);
    }

    function resendInvitation(Request $request) {
        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");
        $current_first_name = $request->get("first_name");
        $current_last_name = $request->get("last_name");
        $users = User::getUserInformation($account_id, $user_id);
        $status = true;
        if ($users) {
            $email = array(
                'first_name' => $current_first_name,
                'last_name' => $current_last_name,
                'user_id' => $users->id,
                'email' => $users->email,
                'account_id' => $users->account_id,
                'authentication_token' => $users->authentication_token,
                'message' => '',
                'company_name' => $users->company_name,
            );

            //if ($users['User']['email'] == 'janeroe@sibme.com') {
            //    $sent = true;
            //} else {
            $sent = $this->sendInvitation($email);
            //}

            if ($sent) {
                $message = TranslationsManager::get_translation('activation_email_resent_successfully_to_msg', "flash_messages", $this->site_id) . ' ' . $users->first_name . ' ' . $users->last_name;
                $redirect_to = '/permissions/assign_user/' . $account_id . '/' . $user_id;

            } else {
                $message = TranslationsManager::get_translation('activation_email_failed_msg', "flash_messages", $this->site_id);
                $redirect_to = '/permissions/assign_user/' . $account_id . '/' . $user_id;
                $status = false;
            }
        } else {
            $message = TranslationsManager::get_translation('user_is_already_activated_msg', "flash_messages", $this->site_id);
            $redirect_to = '/permissions/assign_user/' . $account_id . '/' . $user_id;
        }

        return response()->json(["data" => null, "message" => $message, "status" => $status, "redirect_to" => $redirect_to]);
    }

    function change_permission(Request $request) {
        $account_id = $request->get("account_id");
        $role = $request->get("role");
        $user_id = $request->get("user_id");
        $MailChimp = new MailChimp('007cb274d399d9290e1e6c5b42118d40-us3');
        $account_info = Account::where("id",$account_id)->first();

        $user_data = User::where("id",$user_id)->first();
        $user_account_data = UserAccount::where('user_id', $user_id)->where('account_id', $account_id)->first();
        $email_id = $user_data->email;
        $old_role = $user_account_data->role_id;
        $mailchimp_list_id = $account_info->mailchimp_list_id;
        $message = "Something went wrong!";
        $status = false;
        if(!empty($mailchimp_list_id))
        {
            $segment_details = $MailChimp->get("lists/$mailchimp_list_id/segments");
            $new_segment_id = '';
            $old_segment_id = '';
            if(isset($segment_details['segments']))
            {
                foreach($segment_details['segments'] as $row)
                {
                    if($role == '110' && $row['name'] == 'Super Admins')
                    {
                        $new_segment_id =   $row['id'];

                    }

                    if($old_role == '110' && $row['name'] == 'Super Admins')
                    {
                        $old_segment_id =   $row['id'];

                    }

                    if($role == '115' && $row['name'] == 'Admins')
                    {
                        $new_segment_id =   $row['id'];

                    }

                    if($old_role == '115' && $row['name'] == 'Admins')
                    {
                        $old_segment_id =   $row['id'];

                    }

                    if($role == '120' && $row['name'] == 'Users')
                    {
                        $new_segment_id =   $row['id'];

                    }

                    if($old_role == '120' && $row['name'] == 'Users')
                    {
                        $old_segment_id =   $row['id'];

                    }

                    if($role == '125' && $row['name'] == 'Viewers')
                    {
                        $new_segment_id =   $row['id'];

                    }

                    if($old_role == '125' && $row['name'] == 'Viewers')
                    {
                        $old_segment_id =   $row['id'];

                    }


                }
                $subscriber_hash = $MailChimp->subscriberHash($email_id);
                $MailChimp->delete("lists/$mailchimp_list_id/segments/$old_segment_id/members/$subscriber_hash");
                $MailChimp->post('lists/' . $mailchimp_list_id . '/segments/'.$new_segment_id.'/members', [
                    //   "status" => "subscribed",
                    "email_address" => $email_id,
                ]);
            }

        }



        $exUserAccount = UserAccount::where('user_id', $user_id)->where('account_id', $account_id)->first();

        $data = array(
            'role_id' =>  $role,
            'last_edit_date' => date("Y-m-d h:i:s")
        );
        if ($role == '110') {
            $data['permission_maintain_folders'] = true;
            $data['permission_access_video_library'] = true;
            $data['permission_video_library_upload'] = true;
            $data['permission_view_analytics'] = true;
            $data['permission_administrator_user_new_role'] = true;
            $data['parmission_access_my_workspace'] = true;
            $data['manage_collab_huddles'] = true;
            $data['manage_coach_huddles'] = true;
            $data['manage_evaluation_huddles'] = true;
            $data['folders_check'] = true;
            $data['permission_video_workspace_upload'] = '1';
        } elseif ($role == '115') {
            $data['permission_maintain_folders'] = true;
            $data['permission_access_video_library'] = true;
            $data['permission_video_library_upload'] = '0';
            $data['permission_view_analytics'] = '0';
            $data['permission_administrator_user_new_role'] = true;
            $data['parmission_access_my_workspace'] = true;
            $data['manage_collab_huddles'] = true;
            $data['manage_coach_huddles'] = true;
            $data['manage_evaluation_huddles'] = true;
            $data['folders_check'] = false;
            $data['permission_video_workspace_upload'] = '1';
        } elseif ($role == '120') {
            $data['permission_maintain_folders'] = '0';
            $data['permission_access_video_library'] = true;
            $data['permission_video_library_upload'] = '0';
            $data['permission_view_analytics'] = '1';
            $data['permission_administrator_user_new_role'] = '0';
            $data['parmission_access_my_workspace'] = TRUE;
            $data['manage_collab_huddles'] = '0';
            $data['manage_coach_huddles'] = '0';
            $data['manage_evaluation_huddles'] = '0';
            $data['folders_check'] = '0';
            $data['permission_video_workspace_upload'] = '1';
        } elseif ($role == '125') {
            $data['permission_maintain_folders'] = '0';
            $data['permission_access_video_library'] = true;
            $data['permission_video_library_upload'] = '0';
            $data['permission_view_analytics'] = '0';
            $data['permission_administrator_user_new_role'] = '0';
            $data['parmission_access_my_workspace'] = TRUE;
            $data['manage_collab_huddles'] = '0';
            $data['manage_coach_huddles'] = '0';
            $data['folders_check'] = '0';
            $data['permission_video_workspace_upload'] = '0';
            UserGroup::where('user_id', $user_id)->delete();
            Goal::setAccountOwnerForThisUser($user_id, $account_id);
        }


        $affectedRow = UserAccount::where('user_id', $user_id)->where('account_id', $account_id)->update($data);
        if ($affectedRow > 0) {
            if ($role == 125) {
                //$this->modify_account_folder($account_id, $user_id);
            }
            if ($exUserAccount->role_id != $role) {
                $message = $this->language_based_messages['role_has_been_changed_successfully_msg'];
                $status = true;
            } else {
                $message = $this->language_based_messages['privileges_has_been_changed_msg'];
                $status = true;
            }
        } else {
            $message = $this->language_based_messages['privileges_has_not_changed_msg'];
        }
        Goal::updateGoalSettingsForNewUser($user_id, $account_id, $role);
        $goal_user_settings = GoalUserSettings::where("user_id", $user_id)->where("account_id", $account_id)->where("site_id", $this->site_id)->first();
        $event = [
            'channel' => "people_".$account_id,
            'event' => "user_role_changed",
            'goal_user_settings' => $goal_user_settings,
            'user_id' => $user_id,
            'role_id' => $role,
        ];
        HelperFunctions::broadcastEvent($event);
        return response()->json(["message"=>$message,"status"=>$status,"data"=>null]);
    }

    function _getUsersRoles($account_id, $user_id) {
        $result = UserAccount::get($account_id, $user_id);
        if ($result) {
            $role_id = '';
            $key = $result->role_id;
            switch ($key) {
                case 100:
                    return $role_id = '200';
                    break;

                case 110:
                    return $role_id = '210';
                    break;

                case 120:
                    return $role_id = '220';
                    break;
            }
        }
    }

    function get_user_account_folder($user_id) {
        $account_folder_ids = [];
        $result = AccountFolderUser::get_all_account_folder_user($user_id);
        if ($result && count($result) > 0) {
            foreach ($result as $row) {
                $account_folder_ids[] = $row->account_folder_id;
            }
            return $account_folder_ids;
        }
        return FALSE;
    }

    function _get_associated_huddles_users($user_id) {

        $result = AccountFolderUser::get($user_id,$this->site_id);
        $groupResult = UserGroup::get_user_group($user_id,$this->site_id);
        $associated_users = array();
        if ($result && count($result) > 0) {
            foreach ($result as $row) {
                $associated_users[] = $row['account_folder_id'];
            }
            //return $associated_users;
        }
        if ($groupResult && count($groupResult) > 0) {
            foreach ($groupResult as $row1) {
                $associated_users[] = $row1['account_folder_id'];
            }
        }
        return $associated_users;
    }

    function assign_user(Request $request)
    {
        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");
        $logged_in_role = $request->get("logged_in_role");
        $logged_in_account_id = $request->get("logged_in_account_id");
        $not_logged_in_role = $request->get("not_logged_in_role");
        $permissions_set = UsersHelperFunctions::set_permissions($logged_in_role,$not_logged_in_role);
        $status = true;
        $message = "Success";
        $redirect_to = "";
        if ($logged_in_account_id != $account_id) {
            $message = $this->language_based_messages['you_dont_have_access_to_this_account_msg'];
            $status = false;
            $redirect_to = '/Dashboard/home';
        }

        $link_user_role_detail = UserAccount::where(array('user_id' => $user_id , 'account_id' => $account_id ))->first();
        if($link_user_role_detail==false){
            return response()->json(["data"=>'',"message"=>$this->language_based_messages['you_dont_have_access_to_this_users_permission_msg'],"status"=>$status,"redirect_to"=>'/Dashboard/home']);
        }
        $link_user_role = $link_user_role_detail->role_id;
        $current_user_role = $logged_in_role;

        if($current_user_role > $link_user_role )
        {
            $message = $this->language_based_messages['you_dont_have_access_to_this_users_permission_msg'];
            $redirect_to = '/Dashboard/home';
            $status = false;
        }


        $data['users'] = UserAccount::get($account_id, $user_id);
        $huddles = AccountFolder::get_all_huddle_users($account_id);
        //$huddleRoles = AccountFolderUser::get_all_account_folder_user($user_id);
        $huddle_data = array();
        if (count($huddles)) {
            foreach ($huddles as $huddle) { 
                $participants = AccountFolder::getHuddleUsersIncludingGroupsNew($this->site_id, $huddle->account_folder_id, $user_id, $not_logged_in_role,true)['participants'];
                if(count($participants) > 1){
                    foreach($participants as $row){
                        if($row['group_id'] ==''){
                            $huddle->role_id =  $row['role_id'] ;
                        }
                    }
                }else{
                    foreach($participants as $row){
                            $huddle->role_id =  $row['role_id'] ;
                        }
                    }
                    
                            
                
                $h_type = AccountFolderMetaData::where(array('account_folder_id' => $huddle->account_folder_id, 'meta_data_name' => 'folder_type'))->first();

                    if (empty($h_type)) {
                        $huddle->meta_data_value = 1;
                    }
                    else
                    {
                        $huddle->meta_data_value = $h_type->meta_data_value;
                    }
                    $huddle_data[] = $huddle;
            }  
        }      

        $data['huddle_data'] = $huddle_data;
        //$data['huddle_user_folder'] = $this->get_user_account_folder($user_id);
        $data['associate_huddle_users'] = $this->_get_associated_huddles_users($user_id);
        $data['permission_set'] = $permissions_set;
        $data['permotion_set'] = UsersHelperFunctions::set_promotions($logged_in_role,$not_logged_in_role);

        $user_check = GoalUserSettings::where("account_id",$account_id)->where("user_id",$user_id)->where("role_id",$not_logged_in_role)->where("site_id",$this->site_id)->first();
        if(!$user_check)
        {
            DB::table('goal_users_settings')->where("account_id",$account_id)->where("user_id" , $user_id)->where("site_id",$this->site_id)->delete();

            $updated_data = array(
                'account_id' => $account_id,
                'user_id' => $user_id,
                'role_id' => $not_logged_in_role,
                'site_id' => $this->site_id
            );
            DB::table('goal_users_settings')->insert($updated_data);
        }

        $goal_settings_for_user = GoalUserSettings::where(array(
            "account_id" => $account_id,
            "user_id" => $user_id
        ))->get()->first();

        $data['goal_settings_for_user'] = $goal_settings_for_user;

        $data['language_based_content'] = TranslationsManager::get_page_lang_based_content('users/administrators_groups',$this->site_id);
        return response()->json(["data"=>$data,"message"=>$message,"status"=>$status,"redirect_to"=>$redirect_to]);
    }

    function associate_huddle_user(Request $request) {

        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");
        $current_user_id = $request->get("current_user_id");
        $huddle_type_id = $request->get("huddle_type_id");
        $input  = $request->all();
        $status = false;

        $roles_id = $this->_getUsersRoles($account_id, $user_id);
        $error_message = '';
        $accountFolderData = array(
            'user_id' => $user_id,
            'role_id' => $roles_id,
            'created_by' => $current_user_id,
            'created_date' => date('Y-m-d H:i:s')
        );
        $accountFolderIds = $request->has('account_folder_ids') ? $request->get('account_folder_ids') : '';

        $huddle_ids = array_keys($accountFolderIds);
        $user_huddles = AccountFolder::getUserHuddles($user_id, $account_id,$huddle_type_id);
        for ($i = 0; $i < count($user_huddles); $i++) {
            $user_huddle = $user_huddles[$i];
            if(!in_array($user_huddle['account_folder_id'], $huddle_ids))
            {
                DB::table("account_folder_users")->where(array('user_id' => $user_id, 'account_folder_id' => $user_huddle['account_folder_id']))->delete();
                $result = array(
                    'entity_id'=>$user_huddle['account_folder_id'],
                    'deleted_entity_id'=>$user_id,
                    'entity_name'=>'Removed user from huddle',
                    'entity_data'=>json_encode($user_huddles),
                    'created_by'=>$current_user_id,
                    'created_at'=> date("Y-m-d H:i:s"),
                    'site_id'=>$this->site_id,
                    'ip_address'=>$_SERVER['REMOTE_ADDR'],
                    'posted_data'=>json_encode($input),
                    'action'=> $_SERVER['REQUEST_URI'],
                    'user_agenet'=>$_SERVER['HTTP_USER_AGENT'],
                    'request_url'=>$_SERVER['REQUEST_URI'],
                    'platform'=>'',
                    'created_date' => date("Y-m-d H:i:s"),
                );
                DB::table('audit_deletion_log')->insert($result);
            }
        }

        if (!empty($accountFolderIds)) {
            if ($huddle_type_id != 2) {
                foreach ($accountFolderIds as $key => $value) {

                    $huddle_meta_info = AccountFolderMetaData::where(array(
                        'account_folder_id' => $key,
                        'meta_data_name' => 'folder_type'
                    ))->first();

                    $huddle_type = isset($huddle_meta_info->meta_data_value) ? $huddle_meta_info->meta_data_value : 1;

                    $accountFolderData['account_folder_id'] = $key;
                    $accountFolderData['role_id'] = $value;
                    $accountFolderData['last_edit_by'] = $current_user_id;
                    $accountFolderData['last_edit_date'] = date('Y-m-d H:i:s');
                    $accountFolderData['user_id'] = $user_id;
                    $accountFolderData['created_by'] = $current_user_id;
                    $accountFolderData['created_date'] = date('Y-m-d H:i:s');
                    $accountFolderData['is_coach'] = '0';
                    $accountFolderData['is_mentee'] = '0';
                    $accountFolderData['site_id'] = $this->site_id;

                    if ($huddle_type == '2') {
                        if ($accountFolderData['role_id'] == '200') {
                            $accountFolderData['is_coach'] = '1';
                        } else if ($accountFolderData['role_id'] == '210') {
                            $accountFolderData['is_mentee'] = '1';
                        }
                    }
                    DB::table('account_folder_users')
                        ->updateOrInsert(
                            array('user_id' => $user_id, 'account_folder_id' => $key),
                            $accountFolderData
                        );
                }
            } else {
                $coacheeaccountFolderIds = $accountFolderIds;
                foreach ($coacheeaccountFolderIds as $key => $value) {

                    $check_value = AccountFolderUser::where(array(
                        'account_folder_id' => $key,
                        'user_id' => $user_id
                    ))->first();

                    $accountFolderData = array();
                    $accountFolderData['is_coach'] = '0';
                    $accountFolderData['is_mentee'] = '0';
                    $accountFolderData['site_id'] = $this->site_id;

                    if ($value == '200') {
                        $accountFolderData['is_coach'] = '1';
                        $accountFolderData['role_id'] = '200';

                        $permission = UsersHelperFunctions::huddle_check_on_permission_page($key, $user_id, 200);

                        if (isset($permission['success']) && $permission['success'] == true) {
                            $error_message .= $permission['message'] . '<br>';
                            break;
                        }
                    } else if ($value == '210') {
                        $accountFolderData['is_mentee'] = '1';
                        $accountFolderData['role_id'] = '210';

                        $permission = UsersHelperFunctions::huddle_check_on_permission_page($key, $user_id, 210);

                        if (isset($permission['success']) && $permission['success'] == true) {
                            $error_message .= $permission['message'] . '<br>';
                            break;
                        }

                    } else {

                        $accountFolderData['is_coach'] = '0';
                        $accountFolderData['is_mentee'] = '0';
                        $accountFolderData['role_id'] = '220';
                    }

                    $accountFolderData['last_edit_by'] = $current_user_id;
                    $accountFolderData['last_edit_date'] = date('Y-m-d H:i:s');

                    if (!$check_value) {

                        $accountFolderData['account_folder_id'] = $key;
                        $accountFolderData['created_by'] = $current_user_id;
                        $accountFolderData['created_date'] = date('Y-m-d H:i:s');
                        $accountFolderData['user_id'] = $user_id;

                    }

                    DB::table('account_folder_users')
                        ->updateOrInsert(
                            array('user_id' => $user_id, 'account_folder_id' => $key,'site_id'=>$this->site_id),
                            $accountFolderData
                        );
                }
            }

            //$this->_add_huddle_permissions($users_id);
            if (!empty($error_message)) {
                $message = $error_message;
            } else {
                $message = $this->language_based_messages['user_huddle_permission_has_been_changed_msg'];
                $status = true;
            }
        } else {
            $message = $this->language_based_messages['we_are_sorry_something_went_wrong_msg'];
        }

        return response()->json(["message"=>$message,"status"=>$status,"data"=>null,"is_permission"=>true],200);
    }

    function modify_account_folder($account_id, $user_id) {
        $result = AccountFolder::join('account_folder_users AS afu','afu.account_folder_id','=','AccountFolder.account_folder_id')
            ->whereIn("AccountFolder.account_folder_id", function($query) use ($user_id)
            {
                $query->select('account_folder_id')
                    ->from('account_folder_users')
                    ->where('user_id',$user_id);
            })
            ->where("AccountFolder.folder_type", 1)
            ->where("afu.role_id","<>", 220)
            ->get();
        $exUserAccount = UserAccount::where('account_id', $account_id)->where('role_id', 100)->first();
        if ($result) {
            foreach ($result as $row) {
                $account_folder_id = $row->account_folder_id;
                $huddles = AccountFolderUser::where('AccountFolderUser.account_folder_id', $account_folder_id)->where('AccountFolderUser.user_id', $user_id)->get();
                if (count($huddles) > 1) {
                    foreach ($huddles as $inner_row) {
                        $account_folder_user_id = $inner_row->account_folder_user_id;
                        AccountFolderUser::where('account_folder_user_id', $account_folder_user_id)->update(array('role_id' => '220', 'last_edit_date' =>  date("Y-m-d h:i:s")));
                    }
                } else {
                    $account_folder_user_id = $huddles[0]->account_folder_user_id;
                    $ownerUser_id = $exUserAccount->user_id;
                    $account_folder_id = $huddles[0]->account_folder_id;
                    $data = array(
                        'account_folder_id' => $account_folder_id,
                        'user_id' => $ownerUser_id,
                        'role_id' => 200,
                        'created_by' => $user_id,
                        'created_date' => date("Y-m-d h:i:s"),
                        'last_edit_by' => $user_id,
                        'last_edit_date' => date("Y-m-d h:i:s"),
                        'is_coach2' => 0,
                        'is_mentee' => 0,
                        'is_coach' => 0,
                        'site_id'=>$this->site_id
                    );
                    DB::table("account_folder_users")->insert($data);
                    AccountFolderUser::where('account_folder_user_id', $account_folder_user_id)->update(array('role_id' => '220', 'last_edit_date' =>  date("Y-m-d h:i:s")));
                }
            }
        }
    }

    function associate_video_huddle_user(Request $request) {
        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");
        $current_user_id = $request->get('current_user_id');
        $user = UserAccount::get($account_id, $user_id);
        $userAccountId = $request->get('user_account_id');

        if($user->role_id == '120')
        {
            $permission_video_workspace_upload = '1';
        }

        if($user->role_id == '125')
        {
            $permission_video_workspace_upload = '0';
        }

        // Goal Work Started
        if($user)
        {
            $user_check = GoalUserSettings::where("account_id",$account_id)->where("user_id",$user_id)->where("role_id",$user->role_id)->where("site_id",$this->site_id)->first();
            if(!$user_check)
            {
                DB::table('goal_users_settings')->where("account_id",$account_id)->where("user_id" , $user_id)->where("site_id",$this->site_id)->delete();

                $data = array(
                    'account_id' => $account_id,
                    'user_id' => $user_id,
                    'role_id' => $user->role_id,
                    'site_id' => $this->site_id
                );
                DB::table('goal_users_settings')->insert($data);
            }
        }

        $extraPermissionGoals = array(
            'can_create_individual_goals_for_others_people' => $request->has('can_create_individual_goals_for_others_people') ? $request->get('can_create_individual_goals_for_others_people') : '0',
            'can_create_individual_goals_for_themselves' => $request->has('can_create_individual_goals_for_themselves') ? $request->get('can_create_individual_goals_for_themselves') : '0',
            'can_create_group_goals' => $request->has('can_create_group_goals') ? $request->get('can_create_group_goals') : '0',
            'can_create_group_templates' => $request->has('can_create_group_templates') ? $request->get('can_create_group_templates') : '0',
            'can_use_templates_to_create_new_goals' => $request->has('can_use_templates_to_create_new_goals') ? $request->get('can_use_templates_to_create_new_goals') : '0',
            'can_create_goal_categories' => $request->has('can_create_goal_categories') ? $request->get('can_create_goal_categories') : '0',
            'can_be_added_as_goal_collab_with_view_rights' => $request->has('can_be_added_as_goal_collab_with_view_rights') ? $request->get('can_be_added_as_goal_collab_with_view_rights') : '0',
            'can_be_added_as_goal_collab_with_edit_rights' => $request->has('can_be_added_as_goal_collab_with_edit_rights') ? $request->get('can_be_added_as_goal_collab_with_edit_rights') : '0',
            'can_be_added_as_goal_collab_with_right_to_add_evidence' => $request->has('can_be_added_as_goal_collab_with_right_to_add_evidence') ? $request->get('can_be_added_as_goal_collab_with_right_to_add_evidence') : '0',
            'can_be_added_as_goal_collab_with_right_to_review_feedback' => $request->has('can_be_added_as_goal_collab_with_right_to_review_feedback') ? $request->get('can_be_added_as_goal_collab_with_right_to_review_feedback') : '0',
            'can_view_goal_progress' => $request->has('can_view_goal_progress') ? $request->get('can_view_goal_progress') : '0',
            'can_enter_data_for_measurement_where_goal_owners' => $request->has('can_enter_data_for_measurement_where_goal_owners') ? $request->get('can_enter_data_for_measurement_where_goal_owners') : '0',
            'can_enter_data_for_measurement_where_goal_reviewers' => $request->has('can_enter_data_for_measurement_where_goal_reviewers') ? $request->get('can_enter_data_for_measurement_where_goal_reviewers') : '0',
            'can_use_frameworks_in_goal_evidence' => $request->has('can_use_frameworks_in_goal_evidence') ? $request->get('can_use_frameworks_in_goal_evidence') : '0',
            'can_archive_goals' => $request->has('can_archive_goals') ? $request->get('can_archive_goals') : '0',
        );






        if ($user->role_id == '120' || $user->role_id == '125' ) {
            if (!$request->has('manage_coach_huddles') && !$request->has('manage_collab_huddles') && !$request->has('manage_evaluation_huddles')) {
                $extraPermission = array(
                    'folders_check' => $request->has('folders_check') ? $request->get('folders_check') : '0',
                    'permission_access_video_library' => $request->has('permission_access_video_library') ? $request->get('permission_access_video_library') : '0',
                    'permission_video_library_upload' => $request->has('permission_video_library_upload') ? $request->get('permission_video_library_upload') : '0',
                    'permission_video_library_download' => $request->has('permission_video_library_download') ? $request->get('permission_video_library_download') : '0',
                    'permission_video_library_comments' => $request->has('permission_video_library_comments') ? $request->get('permission_video_library_comments') : '0',
                    'permission_view_analytics' => $request->has('permission_view_analytics') ? $request->get('permission_view_analytics') : '0',
                    'permission_administrator_user_new_role' => $request->has('permission_administrator_user_new_role') ? $request->get('permission_administrator_user_new_role') : '0',
                    'parmission_access_my_workspace' => $request->has('parmission_access_my_workspace') ? $request->get('parmission_access_my_workspace') : '0',
                    'manage_collab_huddles' => $request->has('manage_collab_huddles') ? $request->get('manage_collab_huddles') : '0',
                    'manage_coach_huddles' => $request->has('manage_coach_huddles') ? $request->get('manage_coach_huddles') : '0',
                    'manage_evaluation_huddles' => $request->has('manage_evaluation_huddles') ? $request->get('manage_evaluation_huddles') : '0',
                    'huddle_to_workspace' => $request->has('huddle_to_workspace') ? $request->get('huddle_to_workspace') : '0',
                    'live_recording' => $request->has('live_recording') ? $request->get('live_recording') : '0',
                    'permission_scripted_notes' => $request->has('permission_scripted_notes') ? $request->get('permission_scripted_notes') : '0',
                    'permission_start_synced' => $request->has('permission_start_synced') ? $request->get('permission_start_synced') : '0',
                    'permission_video_workspace_upload' => $request->has('permission_video_workspace_upload') ? $request->get('permission_video_workspace_upload') : $permission_video_workspace_upload,
                    // 'permission_view_analytics' => $request->has('permission_view_account_wide_analytics') ? $request->get('permission_view_account_wide_analytics') : '0',
                    'permission_maintain_folders' => $request->has('permission_maintain_folders') ? $request->get('permission_maintain_folders') : '0',
                    'permission_share_library' => $request->has('permission_share_library') ? $request->get('permission_share_library') : '0'
                );
            } else {
                $extraPermission = array(
                    'folders_check' => $request->has('folders_check') ? $request->get('folders_check') : '0',
                    'permission_access_video_library' => $request->has('permission_access_video_library') ? $request->get('permission_access_video_library') : '0',
                    'permission_video_library_upload' => $request->has('permission_video_library_upload') ? $request->get('permission_video_library_upload') : '0',
                    'permission_video_library_download' => $request->has('permission_video_library_download') ? $request->get('permission_video_library_download') : '0',
                    'permission_video_library_comments' => $request->has('permission_video_library_comments') ? $request->get('permission_video_library_comments') : '0',
                    'permission_view_analytics' => $request->has('permission_view_analytics') ? $request->get('permission_view_analytics') : '0',
                    'permission_administrator_user_new_role' => $request->has('permission_administrator_user_new_role') ? $request->get('permission_administrator_user_new_role') : '0',
                    'parmission_access_my_workspace' => $request->has('parmission_access_my_workspace') ? $request->get('parmission_access_my_workspace') : '0',
                    'manage_collab_huddles' => $request->has('manage_collab_huddles') ? $request->get('manage_collab_huddles') : '0',
                    'manage_coach_huddles' => $request->has('manage_coach_huddles') ? $request->get('manage_coach_huddles') : '0',
                    'manage_evaluation_huddles' => $request->has('manage_evaluation_huddles') ? $request->get('manage_evaluation_huddles') : '0',
                    'huddle_to_workspace' => $request->has('huddle_to_workspace') ? $request->get('huddle_to_workspace') : '0',
                    'live_recording' => $request->has('live_recording') ? $request->get('live_recording') : '0',
                    'permission_scripted_notes' => $request->has('permission_scripted_notes') ? $request->get('permission_scripted_notes') : '0',
                    'permission_start_synced' => $request->has('permission_start_synced') ? $request->get('permission_start_synced') : '0',
                    'permission_video_workspace_upload' => $request->has('permission_video_workspace_upload') ? $request->get('permission_video_workspace_upload') : $permission_video_workspace_upload,
                    //'permission_view_analytics' => $request->has('permission_view_account_wide_analytics') ? $request->get('permission_view_account_wide_analytics') : '0',
                    'permission_maintain_folders' => $request->has('permission_maintain_folders') ? $request->get('permission_maintain_folders') : true,
                    'permission_share_library' => $request->has('permission_share_library') ? $request->get('permission_share_library') : '0'
                );
            }

        }

        elseif($user->role_id == '115')
        {
            if (!$request->has('manage_coach_huddles') && !$request->has('manage_collab_huddles') && !$request->has('manage_evaluation_huddles')) {
                $extraPermission = array(
                    'folders_check' => $request->has('folders_check') ? $request->get('folders_check') : 0,
                    'permission_access_video_library' => $request->has('permission_access_video_library') ? $request->get('permission_access_video_library') : true,
                    'permission_video_library_upload' => $request->has('permission_video_library_upload') ? $request->get('permission_video_library_upload') : '0',
                    'permission_video_library_download' => $request->has('permission_video_library_download') ? $request->get('permission_video_library_download') : '0',
                    'permission_video_library_comments' => $request->has('permission_video_library_comments') ? $request->get('permission_video_library_comments') : '0',
                    'permission_view_analytics' => $request->has('permission_view_analytics') ? $request->get('permission_view_analytics') : '0',
                    'permission_administrator_user_new_role' => $request->has('permission_administrator_user_new_role') ? $request->get('permission_administrator_user_new_role') : '0',
                    'parmission_access_my_workspace' => $request->has('parmission_access_my_workspace') ? $request->get('parmission_access_my_workspace') : true,
                    'manage_collab_huddles' => $request->has('manage_collab_huddles') ? $request->get('manage_collab_huddles') : true,
                    'manage_coach_huddles' => $request->has('manage_coach_huddles') ? $request->get('manage_coach_huddles') : true,
                    'manage_evaluation_huddles' => $request->has('manage_evaluation_huddles') ? $request->get('manage_evaluation_huddles') : '0',
                    'huddle_to_workspace' => $request->has('huddle_to_workspace') ? $request->get('huddle_to_workspace') : '0',
                    'live_recording' => $request->has('live_recording') ? $request->get('live_recording') : '0',
                    'permission_scripted_notes' => $request->has('permission_scripted_notes') ? $request->get('permission_scripted_notes') : '0',
                    'permission_start_synced' => $request->has('permission_start_synced') ? $request->get('permission_start_synced') : '0',
                    'permission_video_workspace_upload' => $request->has('permission_video_workspace_upload') ? $request->get('permission_video_workspace_upload') : '1',
                    'permission_view_analytics' => $request->has('permission_view_analytics') ? $request->get('permission_view_analytics') : '0',
                    'permission_maintain_folders' => $request->has('permission_maintain_folders') ? $request->get('permission_maintain_folders') : '0',
                    'permission_share_library' => $request->has('permission_share_library') ? $request->get('permission_share_library') : '0',
                    'admins_can_view_leaderboard' => $request->has('permission_view_leaderboard') ? $request->get('permission_view_leaderboard') : '0'
                );
            } else {
                $extraPermission = array(
                    'folders_check' => $request->has('folders_check') ? $request->get('folders_check') : 0,
                    'permission_access_video_library' => $request->has('permission_access_video_library') ? $request->get('permission_access_video_library') : true,
                    'permission_video_library_upload' => $request->has('permission_video_library_upload') ? $request->get('permission_video_library_upload') : '0',
                    'permission_video_library_download' => $request->has('permission_video_library_download') ? $request->get('permission_video_library_download') : '0',
                    'permission_video_library_comments' => $request->has('permission_video_library_comments') ? $request->get('permission_video_library_comments') : '0',
                    'permission_view_analytics' => $request->has('permission_view_analytics') ? $request->get('permission_view_analytics') : '0',
                    'permission_administrator_user_new_role' => $request->has('permission_administrator_user_new_role') ? $request->get('permission_administrator_user_new_role') : '0',
                    'parmission_access_my_workspace' => $request->has('parmission_access_my_workspace') ? $request->get('parmission_access_my_workspace') : true,
                    'manage_collab_huddles' => $request->has('manage_collab_huddles') ? $request->get('manage_collab_huddles') : true,
                    'manage_coach_huddles' => $request->has('manage_coach_huddles') ? $request->get('manage_coach_huddles') : true,
                    'manage_evaluation_huddles' => $request->has('manage_evaluation_huddles') ? $request->get('manage_evaluation_huddles') : '0',
                    'huddle_to_workspace' => $request->has('huddle_to_workspace') ? $request->get('huddle_to_workspace') : '0',
                    'live_recording' => $request->has('live_recording') ? $request->get('live_recording') : '0',
                    'permission_scripted_notes' => $request->has('permission_scripted_notes') ? $request->get('permission_scripted_notes') : '0',
                    'permission_start_synced' => $request->has('permission_start_synced') ? $request->get('permission_start_synced') : '0',
                    'permission_video_workspace_upload' => $request->has('permission_video_workspace_upload') ? $request->get('permission_video_workspace_upload') : '1',
                    'permission_view_analytics' => $request->has('permission_view_analytics') ? $request->get('permission_view_analytics') : '0',
                    'permission_maintain_folders' => $request->has('permission_maintain_folders') ? $request->get('permission_maintain_folders') : true,
                    'permission_share_library' => $request->has('permission_share_library') ? $request->get('permission_share_library') : '0',
                    'admins_can_view_leaderboard' => $request->has('permission_view_leaderboard') ? $request->get('permission_view_leaderboard') : '0'
                );
            }


        }


        $status = false;
        if ($user) {
            $extraPermission['last_edit_date'] = date('Y-m-d H:i:s');
            $extraPermission['last_edit_by'] = $current_user_id;
            $extraPermission['site_id'] = $this->site_id;
            UserAccount::where('id', $userAccountId)->update($extraPermission);
            DB::table("goal_users_settings")->where(array('account_id' => $account_id, 'user_id' => $user_id))->update($extraPermissionGoals);
            $channel_data = array(
                'channel' => 'global-channel',
                'event' => "refresh-header-settings",
                'account_id' => $account_id,
                'user_id' => $user_id,
                'from_goals' => true
            );
            HelperFunctions::broadcastEvent($channel_data,"broadcast_event");
            $message = $this->language_based_messages['extra_privileges_changed_successfully_msg'];
            $status = true;

        } else {
            $extraPermission['created_date'] = date('Y-m-d H:i:s');
            $extraPermission['created_by'] = $current_user_id;
            $new_id = DB::table('users_accounts')->insertGetId($extraPermission);
            if ($new_id > 0) {
                $message = $this->language_based_messages['extra_privileges_changed_successfully_msg'];
                $status = true;
            } else {
                $message = $this->language_based_messages['you_are_unable_to_change_extra_privileges_msg'];
            }
        }

        return response()->json(["message"=>$message,"status"=>$status,"data"=>null]);
    }

    public function addGrups(Request $request) {
        if ($request->isMethod('post')) {
            $account_id = $request->input('account_id');
            $groupsData = array(
                'name' => trim($request->input('name')),
                'created_date' => date('Y-m-d H:i:s'),
                'account_id' => $account_id,
                'site_id'=>$this->site_id

            );

            $groupCount = DB::table('groups')->where('name', trim($request->input('name')))->where('account_id' , $account_id)->count();
            if($groupCount > 0){
                return response()->json(["data" => null, "message" => $this->language_based_messages['group_name_already_exist'], "status" => true]);
            }

            $group_id = DB::table('groups')->insertGetId($groupsData);
            $error_message = '';
            if ($group_id != '') {
                foreach ($request->input('super_admin_ids') as $key => $val) {
                    DB::select("INSERT INTO user_groups (group_id, user_id, created_date,site_id) VALUES('" . $group_id . "','" . $val . "','" . date('Y-m-d H:i:s') . "','" . $this->site_id . "')");
                    $error_message = $request->input('name') . ' ' . $this->language_based_messages['group_has_been_added_sucessfully_msg'];
                }
                $data = [];
                $data["account_id"] = $account_id;
                $data["single_group"] = 1;
                $data["group_id"] = $group_id;
                $new_r = $this->create_custom_request( $data);
                $userGroup = $this->getGroupsAfterAdd($new_r);
                $event = [
                    'channel' => "people_".$account_id,
                    'event' => "group_added",
                    'data' => $userGroup
                ];

                HelperFunctions::broadcastEvent($event,'broadcast_event',false);
                return response()->json(["data" => $userGroup, "message" => $error_message, "status" => true]);
            }
            else
            {
                return response()->json(["data" => null, "message" => $this->language_based_messages['we_are_sorry_something_went_wrong_msg'], "status" => false]);
            }
        }
    }
    public function deleteGroups($group_id,$account_id)
    {
        if (isset($group_id) && $group_id != '') {
            // $groupCount = $this->Group->find('count', array('conditions' => array('id' => $group_id)));
            $groupCount = DB::table('groups')->where('id', $group_id)->count();
            if ($groupCount > 0) {
                // $usreGrupCount = $this->UserGroup->find('count', array('conditions' => array('group_id' => $group_id)));
                $usreGrupCount = UserGroup::where('group_id', $group_id)->count();
                if ($usreGrupCount > 0) {
                    //$result = $this->UserGroup->deleteAll(array('group_id' => $group_id));
                    $is_ug_deleted = UserGroup::where('group_id', $group_id)->delete();

                    if ($is_ug_deleted) {
                        // $result = $this->Group->deleteAll(array('id' => $group_id));
                        $is_g_deleted = DB::table('groups')->where('id', $group_id)->delete();
                        if ($is_g_deleted) {
                            $event = [
                                'channel' => "people_".$account_id,
                                'event' => "group_deleted",
                                'group_id' => $group_id
                            ];
                            HelperFunctions::broadcastEvent($event);
                            return response()->json(["data" => null, "message" => $this->language_based_messages['selected_group_has_been_deleted_successfully_msg'], "status" => true]);
                        }
                    } else {
                        //$this->Session->setFlash($this->language_based_messages['you_are_unable_to_delete_this_record_msg']);
                        return response()->json(["data" => null, "message" => $this->language_based_messages['you_are_unable_to_delete_this_record_msg'], "status" => false]);
                    }
                } else {
                    $is_g_deleted = DB::table('groups')->where('id', $group_id)->delete();
                    //$result = $this->Group->deleteAll(array('id' => $group_id));
                    if ($is_g_deleted) {
                        $event = [
                            'channel' => "people_".$account_id,
                            'event' => "group_deleted",
                            'group_id' => $group_id
                        ];
                        HelperFunctions::broadcastEvent($event);
                        return response()->json(["data" => null, "message" => $this->language_based_messages['selected_group_has_been_deleted_successfully_msg'], "status" => true]);
                        //$this->Session->setFlash($this->language_based_messages['selected_group_has_been_deleted_successfully_msg'], 'default', array('class' => 'message success'));
                    } else {
                        return response()->json(["data" => null, "message" => $this->language_based_messages['you_are_unable_to_delete_this_record_msg'], "status" => true]);
                        //$this->Session->setFlash($this->language_based_messages['you_are_unable_to_delete_this_record_msg']);
                    }
                }
            }
            //$this->redirect('/users/administrators_groups');
        } else {
            //$this->Session->setFlash($this->language_based_messages['please_provide_valid_credentials_msg']);
            return response()->json(["data" => null, "message" => $this->language_based_messages['please_provide_valid_credentials_msg'], "status" => true]);
            // $this->redirect('/users/administrators_groups');
        }
    }
    public function deleteGroupUsers($userGroupId)
    {
        if ($userGroupId != '') {
            //$usreGrupCount = $this->UserGroup->find('count', array('conditions' => array('id' => $userGroupId)));
            $usreGrupCount = UserGroup::where('id', $userGroupId)->count();
            if ($usreGrupCount > 0) {
                //$result = $this->UserGroup->deleteAll(array('id' => $userGroupId));
                $is_deleted = UserGroup::where('id', $userGroupId)->delete();
                if ($is_deleted) {
                    // $this->Session->setFlash($this->language_based_messages['selected_user_has_been_deleted_successfully_msg'], 'default', array('class' => 'message success'));
                    $result = array(
                        'data' => null,
                        'message' => $this->language_based_messages['please_provide_valid_credentials_msg'],
                        'status' => true,
                    );
                    return response()->json($result);
                } else {
                    //$this->Session->setFlash($this->language_based_messages['you_have_enable_to_delete_this_user_msg'], 'default', array('class' => 'message error'));
                    $result = array(
                        'data' => null,
                        'message' => $this->language_based_messages['you_have_enable_to_delete_this_user_msg'],
                        'status' => false,
                    );
                    return response()->json($result);
                }
            } else {
                //$this->Session->setFlash($this->language_based_messages['please_provide_valid_credentials_msg'], 'default', array('class' => 'message error'));
                $result = array(
                    'data' => null,
                    'message' => $this->language_based_messages['please_provide_valid_credentials_msg'],
                    'status' => false,
                );
                return response()->json($result);
            }
            //$this->redirect('/users/administrators_groups');
        } else {
            $this->Session->setFlash($this->language_based_messages['please_provide_valid_credentials_msg'], 'default', array('class' => 'message error'));
            $result = array(
                'data' => null,
                'message' => $this->language_based_messages['please_provide_valid_credentials_msg'],
                'status' => false,
            );
            return response()->json($result);
            //  $this->redirect('/users/administrators_groups');
        }
    }

    public function editGroups(Request $request,$group_id)
    {

        $account_id = $request->input('account_id');
        $accountOwner = $this->_getAccountOwner($account_id);
        $data = $request->all();
        $data["group_id"] = $group_id;
        $new_r = $this->create_custom_request( $data);
        $userGroup = $this->getGroups($new_r);
        $superAdmins = $this->_getSupperAdmins($account_id);
        $newAdminUsers = $this->_getAdmins($account_id);
        $result = array(
            'account_owner'=>$accountOwner,
            'userGroup'=>$userGroup,
            'superAdmins'=>$superAdmins,
            'newAdminUsers'=>$newAdminUsers,
            'group_id'=>$group_id,
            'language_based_content'=>$this->group_based_translation
        );
        return response()->json( $result);

    }

    public function renameGroup(Request $request)
    {
        $group_id = $request->get("group_id");
        $name = $request->input('name');
        $user_id = $request->input('user_id');
        $account_id = $request->input('account_id');
        if(empty($name) || empty($group_id) || empty($user_id))
        {
            return response()->json( ["data"=>null,"message"=>"Please enter required data","status"=>false]);
        }
        $groupsData = array(
            'name' =>  $name,
            'last_edit_date' => date('Y-m-d H:i:s') ,
            'last_edit_by' =>  $user_id
        );
        DB::table('groups')->where('id', $group_id)->update($groupsData);
        $event = [
            'channel' => "people_".$account_id,
            'event' => "group_renamed",
            'group_id' => $group_id,
            'group_name' => $name,
        ];
        HelperFunctions::broadcastEvent($event);
        return response()->json( ["data"=>null,"message"=>"Group renamed successfully","status"=>true]);
    }

    function editUserGroups(Request $request,$group_id) {
        $account_id = $request->input('account_id');
        $user_id = $request->input('user_id');

        $groupsData = array(
            'name' =>  $request->input('name'),
            'last_edit_date' =>date('Y-m-d H:i:s'),
            'account_id' =>  $account_id ,
            'last_edit_by' => $user_id
        );

        $affected_row =  DB::table('groups')->where('id', $group_id)->update($groupsData);
        $is_deleted = UserGroup::where('group_id',$group_id);
        $super_admin_ids = $request->input('super_admin_ids');
        if ($group_id != '' && isset($super_admin_ids) && count($super_admin_ids) > 0) {
            $user_group_ids = [];
            $user_ids_string = implode(",", $request->input('super_admin_ids'));
            $goal_ids = AccountFolderGroup::join("goals","goals.account_folder_id","=","account_folder_groups.account_folder_id")
                ->where("group_id",$group_id)
                ->where("goals.is_active",1)
                ->pluck("account_folder_groups.role_id","goals.id");
            $goalUsers = [];
            foreach ($request->input('super_admin_ids') as $key => $val) {
                //DB::select("INSERT INTO user_groups (group_id, user_id, last_edit_date,site_id) VALUES('" . $group_id . "','" . $val . "','" . date('Y-m-d H:i:s') . "','" . $this->site_id . "')");
                $user_group_data = ["group_id"=>$group_id,"user_id"=>$val,"last_edit_date"=>date('Y-m-d H:i:s'),"site_id"=>$this->site_id];
                //$user_group_data['site_id'] = $user_group_data;
                $goal_user_settings = GoalUserSettings::where("user_id", $val)->where("account_id", $account_id)->where("site_id", $this->site_id)->first();
                $user_group_id = DB::table('user_groups')->insertGetId($user_group_data) ;
                $user_group_ids[] = ["user_id"=>$val,"group_id"=>$group_id,"user_group_id"=>$user_group_id];
                if(!empty($goal_ids))
                {
                    foreach ($goal_ids as $goal_id => $role_id)
                    {
                        $user_type = $role_id == 200 ? 1 : 2;
                        $user_exist = GoalUser::where("goal_id",$goal_id)->where("user_id", $val)->where("user_type", $user_type)->count();
                        if($user_exist < 1)
                        {
                            $permission = 1;
                            if($user_type == 1)
                            {
                                $permission = 0;
                                if((!empty($goal_user_settings)) && ($goal_user_settings->can_be_added_as_goal_collab_with_view_rights || $goal_user_settings->can_be_added_as_goal_collab_with_right_to_add_evidence || $goal_user_settings->can_be_added_as_goal_collab_with_right_to_review_feedback))
                                {
                                    $permission = 1;
                                }
                                else if((!empty($goal_user_settings)) && $goal_user_settings->can_be_added_as_goal_collab_with_edit_rights)
                                {
                                    $permission = 2;
                                }
                            }
                            if($permission != 0)
                            {
                                $goalUsers[] = ["goal_id"=>$goal_id, "user_type"=>$user_type, "permission"=>$permission, "user_id"=>$val, "account_id"=>$account_id,"created_by"=>$user_id,"created_date"=>date('Y-m-d H:i:s'),"site_id"=>$this->site_id];
                            }
                        }
                    }
                }
                $error_message = $request->input('name') . ' ' . $this->language_based_messages['group_has_been_updated_successfully_msg'];
            }
            if(!empty($goalUsers))
            {
                GoalUser::insert($goalUsers);
                self::sendGoalSockets($goal_ids, $account_id);
            }
            return response()->json(["data" => $user_group_ids, "message" => $error_message, "status" => true]);
        } else {
            return response()->json(["data" => null, "message" => $this->language_based_messages['no_record_was_updated_please_try_again_msg'], "status" => false]);
        }

    }

    public static function sendGoalSockets($goal_ids, $account_id)
    {
        foreach ($goal_ids as $goal_id => $role_id)
        {
            $goal = app("App\Http\Controllers\GoalsController")->getSingleGoal($goal_id, $account_id);
            $event = [
                'channel' => "goals_" . $account_id,
                'event' => "goal_edited",
                'action' => "updated_collaborators",
                'data' => $goal,
                'item_id' => $goal->id
            ];
            HelperFunctions::broadcastEvent($event, "broadcast_event", false);
            $event['channel'] = "goal-" . $goal_id;
            HelperFunctions::broadcastEvent($event, "broadcast_event", false);

            $event = [
                'channel' => "goals_".$account_id,
                'event' => "goal_edited",
                'action' => "updated_owners",
                'data' => $goal,
                'item_id' => $goal->id
            ];
            HelperFunctions::broadcastEvent($event,"broadcast_event",false);
            $event['channel'] = "goal-".$goal_id;
            HelperFunctions::broadcastEvent($event,"broadcast_event",false);
        }
    }
    public function  getGoupMembers($account_id){
        $accountOwner = $this->_getAccountOwner($account_id);
        $superAdmins = $this->_getSupperAdmins($account_id);
        $newAdminUsers = $this->_getAdmins($account_id);
        $users = $this->_getUsers($account_id);
        $viewers = $this->_getViewers($account_id);
        $result = array(
            'account_owner'=>$accountOwner,
            'superAdmins'=>$superAdmins,
            'admins'=>$newAdminUsers,
            'users'=>$users,
            'viewers'=>$viewers,
            'language_based_content'=>$this->group_based_translation
        );
        return response()->json( $result);
    }

    function active(Request $request , $account_id, $user_id){
        $user = User::where('id',$user_id)->first()->toArray();
        $allowed_users =  Account::get_allowed_users($account_id);
        $total_users =User::getTotalUsers($account_id);
        $role_id = $request->input('role_id');
        $send_email = $request->input('send_email');
        $current_first_name = $request->input('first_name');
        $current_last_name = $request->input('last_name');
        $send_email = $request->input('send_email');
        if ($total_users < $allowed_users) {
            if($role_id=='115'){
                $data['permission_maintain_folders'] = true;
                $data['permission_access_video_library'] = true;
                $data['permission_video_library_upload'] = '0';
                $data['permission_view_analytics'] = '0';
                $data['permission_administrator_user_new_role'] = true;
                $data['parmission_access_my_workspace'] = true;
                $data['manage_collab_huddles'] = true;
                $data['manage_coach_huddles'] = true;
                $data['manage_evaluation_huddles'] = true;
                $data['folders_check'] = true;
            }elseif($role_id=='110'){
                $data['permission_maintain_folders'] = true;
                $data['permission_access_video_library'] = true;
                $data['permission_video_library_upload'] = true;
                $data['permission_view_analytics'] = true;
                $data['permission_administrator_user_new_role'] = true;
                $data['parmission_access_my_workspace'] = true;
                $data['manage_collab_huddles'] = true;
                $data['manage_coach_huddles'] = true;
                $data['manage_evaluation_huddles'] = true;
                $data['folders_check'] = true;
            }elseif($role_id=='120'){
                $data['permission_maintain_folders'] = '0';
                $data['permission_access_video_library'] = true;
                $data['permission_video_library_upload'] = '0';
                $data['permission_view_analytics'] = '0';
                $data['permission_administrator_user_new_role'] = '0';
                $data['parmission_access_my_workspace'] = TRUE;
                $data['manage_collab_huddles'] = '0';
                $data['manage_coach_huddles'] = '0';
                $data['folders_check'] = '0';
            }elseif ($role_id == '125') {
                $data['permission_maintain_folders'] = '0';
                $data['permission_access_video_library'] = true;
                $data['permission_video_library_upload'] = '0';
                $data['permission_view_analytics'] = '0';
                $data['permission_administrator_user_new_role'] = '0';
                $data['parmission_access_my_workspace'] = TRUE;
                $data['manage_collab_huddles'] = '0';
                $data['manage_coach_huddles'] = '0';
                $data['folders_check'] = '0';
            }
            /*$data = array(
                'role_id' =>  $role_id,
                'last_edit_date' => date("Y-m-d h:i:s")
            );

            $this->UserAccount->updateAll($data, array(
                'user_id' => $user_id,
                'account_id' => $account_id
            ));*/
            $data["role_id"] = $role_id;
            $data["last_edit_date"] = date("Y-m-d h:i:s");
            $data["is_active"] = 1;
            $data['status_type'] ='Active';

            UserAccount::where(array('user_id'=>$user_id,'account_id'=>$account_id))->update($data);
            $msg = $user['first_name'] . ' ' . $user['last_name'] . ' ' . $this->language_based_messages['has_been_activated_msg'];
            $result = array(
                'message'=> $msg,
                'status'=>true,
                'data'=>null
            );
            $users = User::getUserInformation($account_id, $user_id);
            if($send_email)
            {
                if ($users) {
                    $email_data = array(
                        'first_name' => $current_first_name,
                        'last_name' => $current_last_name,
                        'user_id' => $users->id,
                        'email' => $users->email,
                        'account_id' => $users->account_id,
                        'authentication_token' => $users->authentication_token,
                        'message' => '',
                        'company_name' => $users->company_name,
                    );
                }
                if ($this->lang == 'en') {
                    $email_subject = $this->subject_title . " - Account Invitation";
                } else {
                    $email_subject = $this->subject_title . " - Invitación a Cuenta";
                }
                $this->send_generic_email($email_data,"activate_user_email", $email_subject, "");
            }
            $event = [
                'channel' => "people_".$account_id,
                'event' => "user_activated",
                'data' => $users
            ];
            HelperFunctions::broadcastEvent($event);
            return response()->json( $result);
        } else {
            $account_owner = UserAccount::where('account_id', $account_id)->where('role_id', 100)->first();
            if($account_owner)
            {
                $account_owner  = $account_owner->toArray();
            }
            $owner_id = $account_owner['user_id'];
            $account_owner_info = User::where('id', $owner_id)->first();
            if($account_owner_info)
            {
                $account_owner_info = $account_owner_info->toArray();
            }
            //$this->Session->setFlash($view->Custom->parse_translation_params($this->language_based_messages['your_account_has_reached_maximum_users_msg'], ['allowed_users' => $view->Custom->get_allowed_users($account_id),'sales_email' => $this->custom->get_site_settings('static_emails')['sales'] ]), 'default', array('class' => 'message error'));
            $result = array(
                'message'=>TranslationsManager::parse_translation_params($this->language_based_messages['your_account_has_reached_maximum_user_contact_account_owner_msg'], ['allowed_users' => Account::get_allowed_users($account_id), 'first_name' => $account_owner_info['first_name'], 'last_name' => $account_owner_info['last_name'], 'account_owner_email' => $account_owner_info['email']]),
                'status'=>false,
                'data'=>null
            );
            return response()->json( $result);
        }
    }

    function inactive($account_id, $user_id, $role_id) {
        $user = User::where('id',$user_id)->first()->toArray();
        UserAccount::where(array('user_id'=>$user_id,'account_id'=>$account_id))->update(array('is_active' => 0,'status_type'=>'Inactive'));
        UserAccount::setDefaultAccountForUser($user_id);
        Goal::setAccountOwnerForThisUser($user_id, $account_id);
        $result = array(
            'message'=>$user['first_name'] . ' ' . $user['last_name'] . ' ' . $this->language_based_messages['has_been_deactivated_msg'],
            'status'=>true,
            'data'=>null
        );
        $event = [
            'channel' => "people_".$account_id,
            'event' => "user_deactivated",
            'data' => $user,
            'role_id' => $role_id,
        ];
        HelperFunctions::broadcastEvent($event);
        return response()->json( $result);
    }
    function getParticipants($account_folder_id){
        $participants_arranged = array();
        $huddle_users = AccountFolder::getHuddleUsers($account_folder_id, $this->site_id);
        if (!empty($huddle_users)) {
            foreach ($huddle_users as $huddle_user) {
                $participants_arranged[] = array(
                    'user_id' => $huddle_user['user_id'],
                    'role_id' => $huddle_user['role_id'],
                    'user_name' => $huddle_user['first_name'] . ' ' . $huddle_user['last_name'],
                    'image' => $huddle_user['image'],
                    'user_email' => $huddle_user['email']
                );
            }
        }
        return response()->json(["data" =>$participants_arranged, "message" => '', "status" => true]);
    }

    function check_coaching_huddle_scenario(Request $request)
    {
        $huddle_id = $request->input('huddle_id');
        $user_id = $request->input('user_id');

        $permission = UsersHelperFunctions::coaching_huddle_user_remove_check_on_permission_page($huddle_id, $user_id);

        if (isset($permission['success']) && $permission['success'] == true) {

            return response()->json(["status" => false,"message" => $permission['message'],"role_id" => $permission['role_id'] ]);

        }
        else {

            return response()->json(["status" => true]);
        }


    }

    function change_password(Request $request) {    //New Angular Change Password Api
        $input = $request->all();
        $language_based_content = TranslationsManager::get_page_lang_based_content('users/change_password', $this->site_id, '', $this->lang);

        if (!empty($input)) {
            $validation_message = '';
            $password = $input['password'];
            $conf_password = $input['confirm_password'];


            if(!isset($input['key']))
            {
                return response()->json(["status" => false,"message" => 'Reset Key Required.' ]);
            }

            if(isset($input['key']) && empty($input['key']))
            {
                return response()->json(["status" => false,"message" => 'Reset Key Required.' ]);
            }

            $key = $input['key'];

            if (empty($password)) {
                $validation_message .='<p>'.$language_based_content['new_password_field_required_cp'].'</p>';
            }
            if (empty($conf_password)) {
                $validation_message.='<p>'.$language_based_content['confirm_password_field_required_cp'].'</p>';
            }
            if (empty($password) || empty($conf_password)) {
                return response()->json(["status" => false,"message" => $validation_message ]);
            }
            $validation_message = '';

            if (strlen($password) < 8) {
                return response()->json(["status" => false,"message" => $language_based_content['password_limit_cp'] ]);
            }
            if (!empty($password) && !empty($conf_password) && $password == $conf_password) {
                //add change password logic
                $user = User::where('reset_password_token',$key)->first();
                if($user)
                {
                    $user = $user->toArray();
                }
                else
                {
                    $user = [];
                }
                if ($user && count($user) > 0) {
                    $salt = config('apikeys.salt');
                    $data = array(
                        'password' => sha1($salt . $password),
                        'reset_password_token' => NULL,
                    );
                    User::where(array('id' => $user['id']))->update($data);
                    return response()->json(["status" => true,"message" => $language_based_content['password_success_cp'] ]);
                } else {
                    return response()->json(["status" => false,"message" => $language_based_content['password_req_expired_cp'] ]);
                }
            } else {
                return response()->json(["status" => false,"message" => $language_based_content['password_dont_match_cp'] ]);
            }
        } else {

            return response()->json(["status" => false,"message" => 'No Valid Input' ]);
        }
    }

    function get_new_password_change_page_translations()
    {
        $language_based_content = TranslationsManager::get_page_lang_based_content('users/change_password', $this->site_id, '', $this->lang);

        return response()->json(["status" => true,"translations" => $language_based_content ]);
    }


    function add_new_user($account_id, $current_user_id, $email, $first_name, $last_name, $role, $send_email, $current_first_name, $current_last_name) {
        if(empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL) || empty($first_name) || empty($last_name) || empty($role)){
            return false;
        }
        $user_already_exist = User::where('email', $email)->where('site_id', $this->site_id)->count();
        if (strtolower($role) == 'super admin') {
            $role = 110;
        } else if (strtolower($role) == 'admin') {
            $role = 115;
        } else if (strtolower($role) == 'user') {
            $role = 120;
        } else {
            $role = 125;
        }
        if ($user_already_exist > 0) {


            $account_detail = User::where('email', $email)->where('site_id', $this->site_id)->first()->toArray();

            $user_id = $account_detail['id'];

            $user_account_detail = UserAccount::where(array('user_id' => $user_id, 'account_id' => $account_id, 'site_id' => $this->site_id))->count();

            //Checking default account is exist or not
            $defaultAccount = UserAccount::where(array('user_id' => $user_id, 'is_default' => 1))->count();
            if ($user_account_detail == 0) {

                $user_account_data = array(
                    'user_id' => $user_id,
                    'account_id' => $account_id,
                    'role_id' => $role,
                    'is_default' => ($defaultAccount > 0) ? 0 : 1,
                    'permission_maintain_folders' => '1',
                    'permission_access_video_library' => '1',
                    'permission_video_library_upload' => $role == 100 || $role == 110 ? '1' : 0,
                    'permission_administrator_user_new_role' => '1',
                    'parmission_access_my_workspace' => '1',
                    'folders_check' => '1',
                    'manage_collab_huddles' => '1',
                    'manage_coach_huddles' => '1',
                    'manage_evaluation_huddles' => '1',
                    'created_date' => date('Y-m-d H:i:s'),
                    'created_by' => $current_user_id,
                    'last_edit_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $current_user_id,
                    'is_active' => 1,
                    'site_id'=>$this->site_id
                );

                if ($role == 120) {
                    $user_account_data = array(
                        'user_id' => $user_id,
                        'account_id' => $account_id,
                        'role_id' => $role,
                        'is_default' => ($defaultAccount > 0) ? 0 : 1,
                        //  'permission_maintain_folders' => '1',
                        'permission_access_video_library' => '1',
                        //   'permission_video_library_upload' => '1',
                        //   'permission_administrator_user_new_role' => '1',
                        //   'parmission_access_my_workspace' => '1',
                        //   'folders_check' => '1',
                        //   'manage_collab_huddles' => '1',
                        //   'manage_coach_huddles' => '1',
                        //    'manage_evaluation_huddles' => '1',
                        'created_date' => date('Y-m-d H:i:s'),
                        'created_by' => $user_id,
                        'last_edit_date' => date('Y-m-d H:i:s'),
                        'last_edit_by' => $user_id,
                        'is_active' => 1,
                        'site_id'=>$this->site_id
                    );
                }
                DB::table('users_accounts')->insert($user_account_data);
                if($this->is_from_hmh_licensing_admin===true && !empty($this->hmh_license_id)) {
                    /*
                     * For HMH Licensing, mark Registered if User already exists
                     * */
                    DB::table('hmh_order_licenses')->where("id", $this->hmh_license_id)->update(['user_activated' => "1", 'updated_at' => date("Y-m-d H:i:s") ]);
                }
                // if (DB::table('users_accounts')->insert($user_account_data)) {
                //     return true;
                // } else {
                //     return false;
                // }
                //$this->UserAccount->save($user_account_data);
                $users = User::getUserInformation($account_id, $user_id);
                /*if ($send_email) {//dont send email if user already exist
                    if ($users) {
                        $email_data = array(
                            'first_name' => $current_first_name,
                            'last_name' => $current_last_name,
                            'user_id' => $users->id,
                            'email' => $users->email,
                            'account_id' => $users->account_id,
                            'authentication_token' => $users->authentication_token,
                            'message' => '',
                            'company_name' => $users->company_name,
                        );
                    }
                    $this->sendConfirmation($email_data);
                }*/
                $users_result[] = $users; //Changing this to Array because socket was not firing in case of bulk upload
                $this->linked_emails .= $email . '<br>';
                Goal::updateGoalSettingsForNewUser($user_id, $account_id, $role);
                $event = [
                    'channel' => "people_" . $account_id,
                    'event' => "user_added",
                    'user_id' => $users_result,
                    'role_id'=>$role
                ];
                HelperFunctions::broadcastEvent($event);
            }else{
                $this->duplicated_emails .= $email . '<br>';
            }
            return $user_id;
        }

        $salt = config('apikeys.salt');
        $user_data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'created_date' => date('Y-m-d H:i:s'),
            'username' => $email,
            'authentication_token' => $this->digitalKey(20),
            //'password' => ($this->site_id == 1 ? sha1($salt . 'sibme1') : sha1($salt . 'hmhcs1')),
            'type' => 'Invite_Sent',
            'is_active' => FALSE,
            'created_by' => $current_user_id,
            'last_edit_date' => date('Y-m-d H:i:s'),
            'last_edit_by' => $current_user_id,
            'site_id'=>$this->site_id
        );
        DB::table('users')->insert($user_data);
        $user_id = DB::getPdo()->lastInsertId();
        $this->new_emails_added .= $email."<br/>";
        DB::table('users')->where(['id' => $user_id])->update(array('authentication_token' => md5($user_id)));
        $user_account_data = array(
            'user_id' => $user_id,
            'account_id' => $account_id,
            'role_id' => $role,
            'is_default' => 1,
            'permission_maintain_folders' => '1',
            'permission_access_video_library' => '1',
            'permission_video_library_upload' => $role == 100 || $role == 110 ? '1' : 0,
            'permission_administrator_user_new_role' => '1',
            'parmission_access_my_workspace' => '1',
            'folders_check' => '1',
            'manage_collab_huddles' => '1',
            'manage_coach_huddles' => '1',
            'manage_evaluation_huddles' => '1',
            'created_date' => date('Y-m-d H:i:s'),
            'created_by' => $current_user_id,
            'last_edit_date' => date('Y-m-d H:i:s'),
            'last_edit_by' => $current_user_id,
            'is_active' => 1,
            'site_id'=>$this->site_id
        );

        if ($role == 120) {
            $user_account_data = array(
                'user_id' => $user_id,
                'account_id' => $account_id,
                'role_id' => $role,
                'is_default' => '1',
                //  'permission_maintain_folders' => '1',
                'permission_access_video_library' => '1',
                //   'permission_video_library_upload' => '1',
                //   'permission_administrator_user_new_role' => '1',
                //   'parmission_access_my_workspace' => '1',
                //   'folders_check' => '1',
                //   'manage_collab_huddles' => '1',
                //   'manage_coach_huddles' => '1',
                //    'manage_evaluation_huddles' => '1',
                'created_date' => date('Y-m-d H:i:s'),
                'created_by' => $current_user_id,
                'last_edit_date' => date('Y-m-d H:i:s'),
                'last_edit_by' => $current_user_id,
                'is_active' => 1,
                'site_id'=>$this->site_id
            );
        }
        DB::table('users_accounts')->insert($user_account_data);
        $users = User::getUserInformation($account_id, $user_id);
        if ($send_email) {
            if ($users) {
                $email_data = array(
                    'first_name' => $current_first_name,
                    'last_name' => $current_last_name,
                    'user_id' => $users->id,
                    'email' => $users->email,
                    'account_id' => $users->account_id,
                    'authentication_token' => $users->authentication_token,
                    'message' => '',
                    'company_name' => $users->company_name,
                );
                if($this->is_from_hmh_licensing_admin===true){
                    $this->sendInvitation_via_HMH_Licensing($email_data);
                    $this->is_from_hmh_licensing_admin = false;
                } else {
                    $this->sendInvitation($email_data);
                }
            }

        }
        Goal::updateGoalSettingsForNewUser($user_id, $account_id, $role);
        $users_result[] = $users; //Changing this to Array because socket was not firing in case of bulk upload
        $event = [
            'channel' => "people_" . $account_id,
            'event' => "user_added",
            'user_id' => $users_result,
            'role_id'=>$role
        ];
        HelperFunctions::broadcastEvent($event);
        return $user_id;
    }

    function check_if_coach_or_admin($account_id, $user_id) {
        $result = app('db')->select("SELECT * FROM `account_folder_users` AS afu 
        LEFT JOIN account_folders AS af ON af.account_folder_id = afu.account_folder_id 
        WHERE afu.user_id = " . $user_id . "
        AND af.account_id = " . $account_id . " 
        AND af.active = 1 AND af.folder_type = 1 
        AND afu.`role_id` = 200 
        AND af.site_id =" . $this->site_id);
        if (count($result) > 0) {
            return 1;
        } else {
            return 0;
        }
    }
    function getUserSettings(Request $request) {

        $input = $request->all();

        $language_based_content = TranslationsManager::get_page_lang_based_content('Users/editUser', $this->site_id, '', $this->lang);
        $user_current_account = !empty($input['user_current_account'])?$input['user_current_account']:false;
        if(!$user_current_account){
            $final_data = array('message' =>$language_based_content['no_account_access_us'] , 'success' => false );
            return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
        }
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];

        
        $user_accounts = $input['user_accounts'];
       
        $permissions_set = $this->set_permissions($user_id,$user_current_account);
        $is_coach = $this->check_if_coach_or_admin($account_id,$user_id);
        $user = User::where('id', $user_id)->where('site_id', $this->site_id)->first();
        if($user)
        {
            $user = $user->toArray();
        }
        //now find account of current user.

        $editing_user_accounts = UserAccount::where(array('user_id' => $user_id, 'account_id' => $account_id, 'site_id' => $this->site_id))->count();
        if ($editing_user_accounts == 0) {
            $final_data = array('message' => $language_based_content['no_account_access_us'] , 'success' => false );

            return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
        }
        $email_unsubscribers = array();
        $emailFormat = app('db')->select("select * from email_formats as EmailFormats");
        $emailFormatArray = json_decode(json_encode($emailFormat), True);
        $i =0;

        foreach($user_accounts as $key => $account )
        {
            $user_account_permissions = Account::where(array('id' => $account['accounts']['account_id'], 'site_id' => $this->site_id))->first();
            $permission_result = UserAccount::where(array('user_id' => $user_id, 'account_id' => $account['accounts']['account_id'], 'site_id' => $this->site_id))->first();
            //$permission_result = $this->set_permissions($user_id,$user_accounts);
            $email_unsubscribers[$i]['account_name'] = $account['accounts']['company_name'];
            $email_unsubscribers[$i]['role_id'] = $permission_result['role_id'];
            if($permission_result['role_id'] ==100 || $permission_result['role_id']==110){
                $email_unsubscribers[$i]['permission'] = true;
            }else{
                $email_unsubscribers[$i]['permission'] = false;
            }
            $email_unsubscribers[$i]['account_id'] = $account['accounts']['account_id'];
            //$email_unsubscribers[$key]['unsubscribers'] = EmailUnsubscribers::where(array('user_id' => $account['users_accounts']['user_id'], 'account_id' => $account['users_accounts']['account_id']))->get();
            $k = 0;
            foreach($emailFormatArray as $index => $format)
            {
                if(($permission_result['role_id']  > 110  ) && ($format['template_name'] =='weekly_report' || $format['template_name'] =='monthly_report' )){
                    continue;
                }
                if(($permission_result['role_id']  == 125  ) && (in_array($format['id'],array(2,6,8,9,10,11)))){
                    continue;
                }
                if($format['id'] == 13 && $user_account_permissions['transcribe_workspace_videos'] == '0' && $user_account_permissions['transcribe_huddle_videos'] == '0' && $user_account_permissions['transcribe_library_videos'] == '0')
                {
                    continue;
                }
                if($format['id'] == 14  && $user_account_permissions['enable_goals'] == 0)
                {
                    continue;
                }
                $email_unsubscribers[$i]['unsubscribers'][ $k] = $format;
                $email_unsubscribers[$i]['unsubscribers'][ $k]['full_name'] = $language_based_content['full_name_'.$format['id']];
                $unsubscriber =  EmailUnsubscribers::where(array('email_format_id' => $format['id']  ,'user_id' => $account['users_accounts']['user_id'], 'account_id' => $account['users_accounts']['account_id']))->first();
                if($unsubscriber)
                {
                    $email_unsubscribers[$i]['unsubscribers'] [$k]['status'] = 'unchecked';
                }
                else {
                    $email_unsubscribers[$i]['unsubscribers'] [$k]['status'] = 'checked';
                }
                $k++;
            }
            $i++;
        }
        $account_id = $user_current_account['accounts']['account_id'];
        $tracker_enable  =  AccountMetaData::where(array(
            "account_id" => $account_id,
            'site_id' => $this->site_id,
            "meta_data_name" => "enable_tracker"
        ))->first();
        $assess_tracker_enable  =  AccountMetaData::where(array(
            "account_id" => $account_id,
            'site_id' => $this->site_id,
            "meta_data_name" => "enable_matric"
        ))->first();
        $permission_result = UserAccount::where(array('user_id' => $user_id, 'account_id' => $account_id, 'site_id' => $this->site_id))->first();
        $user_role_id = $user_current_account['roles']['role_id'];
        $enable_coaching_tracker = isset($tracker_enable['meta_data_value']) ? $tracker_enable['meta_data_value'] : "0";

        $enable_assessment_tracker = isset($$assess_tracker_enable['meta_data_value']) ? $$assess_tracker_enable['meta_data_value'] : "0";
        $tracker_permission = false;
        if (($enable_coaching_tracker == "1" || $enable_assessment_tracker == "1" ) && ($user_role_id == 100 || $user_role_id == 110 || $user_role_id == 115 )) {
            $tracker_permission = true;
        }
        $user['allow_push_notifications']  = isset($user['enable_push_notifications'])?$user['enable_push_notifications']:0;
        $user['can_user_edit_custom_fields'] = CustomField::can_user_edit_custom_fields($user_role_id, $account_id);
        $user['user_custom_fields'] = UserCustomField::get_user_custom_fields($user_id, $account_id, $user['can_user_edit_custom_fields']);
        $userAccounts = $this->getUserAccountsByUserId($user['id']);
        $final_data = array(
            'email_unsubscribers' => $email_unsubscribers,
            'emailFormat' => $emailFormat ,
            'is_coach'=> $tracker_permission,
            'user' => $user,
            'user_accounts' => $userAccounts,
            'permissions' => $permissions_set,
            'default_account' => $this->getDefaultAccount($userAccounts),
            'allow_push_notifications'=> $user['enable_push_notifications'],
            'success' => true
        );

        return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);

    }

    function getUserProfile(Request $request) {

        $input = $request->all();
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        
        $user_current_account = $input['user_current_account'];
        $user_accounts = $input['user_accounts'];
        $language_based_content = TranslationsManager::get_page_lang_based_content('Users/editUser', $this->site_id, '', $this->lang);
        //$permissions_set = $this->set_permissions($user_id,$user_current_account);

        $user = User::where('id', $user_id)->where('site_id', $this->site_id)->first();

        if($user)
        {
            $user = $user->toArray();
        }
        //now find account of current user.
        /*
                   $editing_user_accounts = UserAccount::where(array('user_id' => $user_id, 'account_id' => $account_id, 'site_id' => $this->site_id))->count();
                   if ($editing_user_accounts == 0) {
                        $final_data = array('message' => $language_based_content['no_account_access_us'] , 'success' => false );

                        return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
                    }
                    $email_unsubscribers = array();
                    $emailFormat = app('db')->select("select * from email_formats as EmailFormats");
                    $emailFormatArray = json_decode(json_encode($emailFormat), True);
                    $i =0;

                    foreach($user_accounts as $key => $account )
                    {
                        $user_account_permissions = Account::where(array('id' => $account['accounts']['account_id'], 'site_id' => $this->site_id))->first();
                        $permission_result = UserAccount::where(array('user_id' => $user_id, 'account_id' => $account['accounts']['account_id'], 'site_id' => $this->site_id))->first();
                        //$permission_result = $this->set_permissions($user_id,$user_accounts);
                        $email_unsubscribers[$i]['account_name'] = $account['accounts']['company_name'];
                        $email_unsubscribers[$i]['role_id'] = $permission_result['role_id'];
                        if($permission_result['role_id'] ==100 || $permission_result['role_id']==110){
                            $email_unsubscribers[$i]['permission'] = true;
                        }else{
                            $email_unsubscribers[$i]['permission'] = false;
                        }
                        $email_unsubscribers[$i]['account_id'] = $account['accounts']['account_id'];
                        //$email_unsubscribers[$key]['unsubscribers'] = EmailUnsubscribers::where(array('user_id' => $account['users_accounts']['user_id'], 'account_id' => $account['users_accounts']['account_id']))->get();
                        $k = 0;
                        foreach($emailFormatArray as $index => $format)
                        {
                          if(($permission_result['role_id']  > 110  ) && ($format['template_name'] =='weekly_report' || $format['template_name'] =='monthly_report' )){
                            continue;
                          }
                          if(($permission_result['role_id']  == 125  ) && (in_array($format['id'],array(2,6,8,9,10,11)))){
                            continue;
                          }
                          if($format['id'] == 13 && $user_account_permissions['transcribe_workspace_videos'] == '0' && $user_account_permissions['transcribe_huddle_videos'] == '0' && $user_account_permissions['transcribe_library_videos'] == '0')
                          {
                              continue;
                          }
                          $email_unsubscribers[$i]['unsubscribers'][ $k] = $format;
                          $email_unsubscribers[$i]['unsubscribers'][ $k]['full_name'] = $language_based_content['full_name_'.$format['id']];
                          $unsubscriber =  EmailUnsubscribers::where(array('email_format_id' => $format['id']  ,'user_id' => $account['users_accounts']['user_id'], 'account_id' => $account['users_accounts']['account_id']))->first();
                          if($unsubscriber)
                          {
                             $email_unsubscribers[$i]['unsubscribers'] [$k]['status'] = 'unchecked';
                          }
                          else {
                             $email_unsubscribers[$i]['unsubscribers'] [$k]['status'] = 'checked';
                          }
                          $k++;
                        }
                        $i++;
                    }
                    */
        $account_id = $user_current_account['accounts']['account_id'];
        $tracker_enable  =  AccountMetaData::where(array(
            "account_id" => $account_id,
            'site_id' => $this->site_id,
            "meta_data_name" => "enable_tracker"
        ))->first();
        $assess_tracker_enable  =  AccountMetaData::where(array(
            "account_id" => $account_id,
            'site_id' => $this->site_id,
            "meta_data_name" => "enable_matric"
        ))->first();
        //  $permission_result = UserAccount::where(array('user_id' => $user_id, 'account_id' => $account['accounts']['account_id'], 'site_id' => $this->site_id))->first();
        $user_role_id = $user_current_account['roles']['role_id'];
        $enable_coaching_tracker = isset($tracker_enable['meta_data_value']) ? $tracker_enable['meta_data_value'] : "0";

        $enable_assessment_tracker = isset($$assess_tracker_enable['meta_data_value']) ? $$assess_tracker_enable['meta_data_value'] : "0";
        $tracker_permission = false;
        if (($enable_coaching_tracker == "1" || $enable_assessment_tracker == "1" ) && ($user_role_id == 100 || $user_role_id == 110 || $user_role_id == 115 )) {
            $tracker_permission = true;
        }

        $final_data = array(
            //  'email_unsubscribers' => $email_unsubscribers,
            // 'emailFormat' => $emailFormat ,
            'is_coach'=> $tracker_permission,
            'user' => $user,
            //'permissions' => $permissions_set,
            'success' => true
        );

        return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);

    }

    function set_permissions($user_id,$logged_in_user) {
        $not_logged_in_user = User::get($user_id);

        $logged_in_role = $logged_in_user['roles']['role_id'];
        $not_logged_in_role = $not_logged_in_user->role_id;

        $permission = '';
        if ($logged_in_role == $not_logged_in_role) {
            $permission = 'allow';
        } elseif ($logged_in_role < $not_logged_in_role) {
            $permission = "allow";
        } elseif ($logged_in_role > $not_logged_in_role) {
            $permission = "allow";
        }
        return $permission;
    }

    private function getUserAccountsByUserId($userId) {
        $userAccounts = DB::table('users_accounts')
            ->where(['users_accounts.user_id' => $userId, 'users_accounts.is_active' => 1, 'users_accounts.status_type' => 'Active'])
            ->join('accounts', 'users_accounts.account_id', '=', 'accounts.id')
            ->select('users_accounts.id', 'users_accounts.account_id', 'users_accounts.user_id', 'accounts.company_name', 'users_accounts.is_default')
            ->get()
            ->toArray();
        return $userAccounts;
    }

    private function getDefaultAccount($userAccounts) {
        foreach ($userAccounts as $key => $userAccount) {
            if ($userAccount->is_default == 1) {
                return $userAccount->id;
            }
        }
        return 0;
    }


    function updateUserSetting(Request $request)
    {
        $language_based_content = TranslationsManager::get_page_lang_based_content('Users/editUser', $this->site_id, '', $this->lang);
        $input = $request->all();
        $user_id = $input['user_id'];
        //Setting Default User Account
        if (isset($input['default_account']) && (int)$input['default_account'] > 0) {
            DB::table("users_accounts")->where(['user_id' => $user_id])->update(['is_default' => 0]);
            DB::table("users_accounts")->where(['user_id' => $user_id, 'id' => $input['default_account']])->update(['is_default' => 1]);
        }
        $affected_rows = User::where(array('id' => $user_id))->update(array('lang' => $input['lang']));
        if(isset($input['is_enviornment']) && $input['is_enviornment'] =='mobile'){
            if ((isset($input['password']) && $input['password'] != '') || (isset($input['confirm_password']) && $input['confirm_password'] != '') ) {
                if(empty($input['current_password'])){
                    $final_data = array('message' => $language_based_content['enter_current_password'] , 'success' => false );
                    return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
                }
                $salt_config = config('apikeys.salt');
                $current_password = sha1($salt_config . $input['current_password']);
                $current_password_check = User::where(array('id' => $user_id,'password'=>$current_password))->count();
                if($current_password_check==0){
                    $final_data = array('message' => $language_based_content['current_password_invalid'] , 'success' => false );
                    return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
                }
            }
        } else {
            $user_current_account = $input['user_current_account'];
            $user_role_id = $user_current_account['roles']['role_id'];
            $account_id = $user_current_account['accounts']['account_id'];

            $can_user_edit_custom_fields = CustomField::can_user_edit_custom_fields($user_role_id, $account_id);
            if(!empty($can_user_edit_custom_fields) && isset($input['user_custom_fields']) && !empty($input['user_custom_fields'])){
                // $user_custom_fields = json_decode(json_encode($input['user_custom_fields']), True);
                $user_custom_fields = $input['user_custom_fields'];
                UserCustomField::save_user_custom_fields($user_id, $user_id, $account_id, $user_role_id, $user_custom_fields);
            }
        }

        if (isset($input['allow_push_notifications'])) {
            DB::table('users')->where(array('id' => $user_id))->update(array('enable_push_notifications' => $input['allow_push_notifications']));
        }
        $password = '';
        $data = array(
            'first_name' => addslashes($input['first_name']),
            'last_name' => addslashes($input['last_name']),
            'username' => addslashes($input['username']),
            'title' => addslashes($input['title']),
            'email' => addslashes($input['email']),
            // 'time_zone' => '"' . $this->data['time_zone'] . '"',
            'last_edit_date' => date('Y-m-d H:i:s'),
            'last_edit_by' => $user_id,
            'institution_id' => $input['institution_id'],
            'user_description' => (isset($input['user_description'])) ? $input['user_description'] : '',
        );

        if (User::isUserExists($input['username'], $user_id,$this->site_id) > 0) {
            $final_data = array('message' => $language_based_content['username_already_exist_us'] , 'success' => false );
            return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
        }

        if (User::isUserEmailExists($input['email'], $user_id,$this->site_id) > 0) {
            $final_data = array('message' => $language_based_content['email_already_exist_us'] , 'success' => false );
            return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
        }

        $email_unsubscribers = json_decode(json_encode($input['email_subscribers']), True);

        foreach ($email_unsubscribers as $email){

            $inner_array = json_decode(json_encode($email['unsubscribers']), True);
            foreach ($inner_array as $row){

                if($row['status'] == 'checked'){

                    DB::table('email_unsubscribers')->where(array('email_format_id' => $row['id'],'user_id' => $user_id, 'account_id' => $email['account_id'] ))->delete();
                }
                else {
                    $unsubscriber =  EmailUnsubscribers::where(array('email_format_id' => $row['id']  ,'user_id' => $user_id, 'account_id' => $email['account_id']))->first();
                    if(!$unsubscriber)
                    {
                        $unsubscriber_data = array(
                            'user_id' => $user_id ,
                            'account_id' => $email['account_id'],
                            'email_format_id' => $row['id'],
                            'created_by' => $user_id,
                            'site_id' => $this->site_id,
                            'created_at' => date('Y-m-d H:i:s')
                        );
                        DB::table('email_unsubscribers')->insertGetId($unsubscriber_data);
                    }


                }
            }

        }

        if ((isset($input['password']) && $input['password'] != '') || (isset($input['confirm_password']) && $input['confirm_password'] != '')) {
            if ($input['password'] == $input['confirm_password']) {
                if (strlen($input['password']) >= 6) {
                    $salt = config('apikeys.salt');
                    $password = sha1($salt . $input['password']);
                    $data['password'] = $password;
                    $affected_rows = User::where(array('id' => $user_id))->update($data);

                    if ($affected_rows > 0) {
                            $final_data = array('message' => $language_based_content['user_profile_updated_us'] , 'success' => true );
                            return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
                    } else {
                        $final_data = array('message' => $language_based_content['user_profile_updated_us'] , 'success' => true );
                        return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
                    }
                } else {
                    $final_data = array('message' => $language_based_content['password_limit_us'] , 'success' => false );
                    return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
                }
            } else {
                $final_data = array('message' => $language_based_content['password_not_match_us'] , 'success' => false );
                return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
            }
        } else {
            $affected_rows = User::where(array('id' => $user_id))->update($data);
            if ($affected_rows > 0) {
                $final_data = array('message' => $language_based_content['user_profile_updated_us'] , 'success' => true );
                return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
            } else {
                $final_data = array('message' => $language_based_content['user_profile_updated_us'] , 'success' => true );
                return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
            }
        }
    }

        function uploadImage(Request $request)
        {
            $input = $request->all();
            $language_based_content = TranslationsManager::get_page_lang_based_content('flash_messages', $this->site_id, '', $this->lang);
            if (!isset($_FILES['image'])) {
                $final_data = array('message' => 'Image parameter is missing', 'success' => false);
                return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
            }
            $file = $_FILES['image'];
            $user_id = $input['user_id'];

            $file_ext = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
            $thumb_name = rand() . 'thumb.' . $file_ext;

            $bytes = $file['size'];
            $bytes = number_format($bytes / 1048576, 2) . ' MB';

            if ($this->upload_user_avatar($file, $thumb_name, $user_id)) {
                $file_name = $thumb_name;
                $imageData = array(
                    'image' => $file_name,
                    'file_size' => $file['size']
                );
                $affected_rows = User::where(array('id' => $user_id))->update($imageData);
                if ($affected_rows > 0) {
//                $filename = $this->uploadDir . Inflector:: slug(pathinfo($user->image, PATHINFO_FILENAME)) . '.' . pathinfo($user->image, PATHINFO_EXTENSION);
//                @unlink($filename);
                    //   $this->setupPostLogin(true);
                    $final_data = array('message' => $language_based_content['file_has_been_uploaded_successfully_msg'], 'success' => true, 'image' => $file_name);
                    return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
                } else {
//                $filename = $this->uploadDir . Inflector::slug(pathinfo($file ['name'], PATHINFO_FILENAME)) . '.' . pathinfo($file['name'], PATHINFO_EXTENSION);
//                @unlink($filename);
                    $final_data = array('message' => $language_based_content['image_is_not_uploaded_msg'], 'success' => false);
                    return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
                }
            } else {
                $final_data = array('message' => $language_based_content['image_is_not_uploaded_msg'], 'success' => false);
                return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
            }
        }

        function upload_user_avatar($file, $thumb_name, $user_id)
        {

            if (isset($file['error']) && $file['error'] == 0) {

                $fileName = $thumb_name;
                $maxDim = 85;
                list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);
                if ($width > $maxDim || $height > $maxDim) {
                    $target_filename = $file['tmp_name'];
                    $fn = $file['tmp_name'];
                    $size = getimagesize($fn);
                    $ratio = $size[0] / $size[1]; // width/height
                    if ($ratio > 1) {
                        $width = $maxDim;
                        $height = $maxDim / $ratio;
                    } else {
                        $width = $maxDim * $ratio;
                        $height = $maxDim;
                    }
                    $src = imagecreatefromstring(file_get_contents($fn));
                    $dst = imagecreatetruecolor($width, $height);
                    imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
                    imagedestroy($src);
                    imagepng($dst, $target_filename); // adjust format as needed
                    imagedestroy($dst);
                }
                $fileTempName = $file['tmp_name'];
                $fileType = $file['type'];


                $image_path = "static/users/" . $user_id . "/" . $thumb_name;
                $keyname = $image_path;


                $distribution_id = config('s3.cloudfront_distribution_id');

                $client = new S3Client([
                    'region' => 'us-east-1',
                    'version' => 'latest',
                    'credentials' => array(
                        'key' => config('s3.access_key_id'),
                        'secret' => config('s3.secret_access_key'),
                    )
                ]);

                $result = $client->putObject([
                    'Bucket' => 'sibme.com',
                    'Key' => $keyname,
                    'SourceFile' => $fileTempName,
                    'ContentType' => $fileType,
                    'ACL' => 'public-read'
                ]);

                $keyPairId = config('s3.cloudfront_keypair');

                //Read Cloudfront Private Key Pair
                /*
                $cloudFront = CloudFrontClient::factory(array(
                            'private_key' => realpath(dirname(__FILE__) . '/../..') . "/Config/cloudfront_key.pem",
                            'key_pair_id' => $keyPairId,
                            'key' => config('s3.access_key_id'),
                            'secret' => config('s3.secret_access_key')
                ));
                */
                $cloudFront = new CloudFrontClient([
                    'region' => 'us-east-1',
                    'version' => 'latest',
                    'credentials' => array(
                        'key' => config('s3.access_key_id'),
                        'secret' => config('s3.secret_access_key'),
                    )
                ]);

                $result = $cloudFront->createInvalidation(array(
// DistributionId is required
                    'DistributionId' => $distribution_id,
                    'InvalidationBatch' => [
                        'Paths' => array(
                            // Quantity is required
                            'Quantity' => 1,
                            'Items' => array("/" . $keyname)
                        ),
                        // CallerReference is required
                        'CallerReference' => time(),
                    ],
                    'private_key' => realpath(dirname(__FILE__) . '/../..') . "/Config/cloudfront_key.pem",
                    'key_pair_id' => config('s3.cloudfront_keypair'),
                ));

                return TRUE;
            } else {
                return FALSE;
            }
        }

        function brain_tree_testing()
        {

            $subscription = new BraintreeSubscription();
            $result = $subscription->find_transaction_detail('k6994asc');
            print_r($result);
            die;
        }

        function turn_off_email_subscriptions(Request $request)
        {
            $input = $request->all();
            $account_id = $input['account_id'];
            $emailFormat = app('db')->select("select * from email_formats as EmailFormats");
            $emailFormatArray = json_decode(json_encode($emailFormat), True);

            $account_users = UserAccount::where(array('account_id' => $account_id))->get()->toArray();
            foreach ($account_users as $user) {
                foreach ($emailFormatArray as $row) {
                    $unsubscriber = EmailUnsubscribers::where(array('email_format_id' => $row['id'], 'user_id' => $user['user_id'], 'account_id' => $account_id))->first();
                    if (!$unsubscriber) {
                        $unsubscriber_data = array(
                            'user_id' => $user['user_id'],
                            'account_id' => $account_id,
                            'email_format_id' => $row['id'],
                            'created_by' => $user['user_id'],
                            'site_id' => $this->site_id,
                            'created_at' => date('Y-m-d H:i:s')
                        );
                        DB::table('email_unsubscribers')->insertGetId($unsubscriber_data);
                    }
                }
            }

            $final_data = array('message' => 'Emails Notifications are successfully Unsubscribed', 'success' => true);
            return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
        }

    public function getSSOLink(Request $request) {
        $redirectTo = $request->get('redirect_to');
        $email = $request->get('email');
        $accountId = $request->get('account_id');
        if (!isset($redirectTo) || empty($redirectTo)) {
            $redirectTo = $this->base."users/sso_login_callback";
        }
        $res = new \stdClass();
        $res->success = false;
        $fetchUserAccount = function ($conditions) {
            $res = new \stdClass();
            $userAccount = DB::table('users_accounts')
                ->join('accounts', 'users_accounts.account_id', '=', 'accounts.id')
                ->select('users_accounts.id', 'users_accounts.account_id', 'accounts.is_saml_enabled', 'accounts.sso_url', 'accounts.sso_certificate', 'accounts.is_force_sso_enabled', 'accounts.idp_entityId')
                ->where($conditions)
                ->where("users_accounts.site_id", $this->site_id)
                ->first();
            if (isset($userAccount)) {
                if ($userAccount->is_saml_enabled == 1) {
                    $res->success = true;
                    $res->message = '';
                    $res->user_account = $userAccount;
                } else {
                    $res->success = false;
                    $res->message = $this->language_based_messages['sso_not_enabled'];
                }
            } else {
                $res->success = false;
                $res->message = $this->language_based_messages['email_not_exist'];
            }
            // Following is a funciton return, should not be a json response.
            return $res;
        };
        $setConfigWithUserAccountAttr = function ($userAccount) {
            config([
                'mytestidp1_idp_settings.idp.singleSignOnService.url' => $userAccount->sso_url,
                'mytestidp1_idp_settings.idp.x509cert' => $userAccount->sso_certificate,
                'mytestidp1_idp_settings.idp.entityId' => $userAccount->idp_entityId,
            ]);
        };
        if (isset($redirectTo) && !empty($redirectTo)) {
            if (filter_var($redirectTo, FILTER_VALIDATE_URL)) {
                if (isset($accountId) && !empty($accountId)) {
                    $fetchUserAccountRes = $fetchUserAccount(['users_accounts.account_id' => $accountId]);
                    if ($fetchUserAccountRes->success) {
                        $setConfigWithUserAccountAttr($fetchUserAccountRes->user_account);
                    } else {
                        return response()->json($fetchUserAccountRes);
                    }
                } elseif (isset($email) && !empty($email)) {
                    $user = DB::table('users')->where(['email' => $email])->first();
                    if (isset($user)) {
                        $fetchUserAccountRes = $fetchUserAccount(['users_accounts.user_id' => $user->id, 'users_accounts.is_default' => 1]);
                        if ($fetchUserAccountRes->success) {
                            $setConfigWithUserAccountAttr($fetchUserAccountRes->user_account);
                            $accountId = $fetchUserAccountRes->user_account->account_id;
                        } else {
                            return response()->json($fetchUserAccountRes);
                        }
                    } else {
                        $res->message = $this->language_based_messages['email_not_exist'];
                        return response()->json($res);
                    }
                } else {
                    $res->message = $this->language_based_messages['email_required'];
                    return response()->json($res);
                }

                try {
                    $saml2Auth = new Saml2Auth(Saml2Auth::loadOneLoginAuthFromIpdConfig('mytestidp1'));
                    $res->success = true;
                    $res->idp_login_url = $saml2Auth->login($redirectTo, [], false, false, true);
                    return response()->json($res);
                } catch (\Exception $e) {
                    return response()->json(["success"=>false,"message"=>$e->getMessage()]);
                }

            } else {
                $res->success = false;
                $res->message = $this->language_based_messages['invalid_url'] . ' = ' . $redirectTo;
                return response()->json($res);
            }
        }
        $res->redirect_to = $this->language_based_messages['is_required_new_settings'];
        return response()->json($res);
    }

    public function saveGoalsettings($role_id,$user_id,$account_id){
        if($role_id == 120){
            $user_check = GoalUserSettings::where("account_id",$account_id)->where("user_id",$user_id)->where("role_id",$role_id)->where("site_id",$this->site_id)->first();
            if(!$user_check)
            {
                DB::table('goal_users_settings')->where("account_id",$account_id)->where("user_id" , $user_id)->where("site_id",$this->site_id)->delete();

                $data = array(
                    'account_id' => $account_id,
                    'user_id' => $user_id,
                    'role_id' => $role_id,
                    'site_id' => $this->site_id,
                    'can_create_individual_goals_for_others_people' => 0,
                    'can_create_individual_goals_for_themselves' => 1 ,
                    'can_create_group_goals' => 0,
                    'can_create_group_templates' => 0 ,
                    'can_use_templates_to_create_new_goals' => 0,
                    'can_create_goal_categories' => 0 ,
                    'can_be_added_as_goal_collab_with_view_rights' => 0 ,
                    'can_be_added_as_goal_collab_with_edit_rights' => 0 ,
                    'can_be_added_as_goal_collab_with_right_to_add_evidence' => 0 ,
                    'can_be_added_as_goal_collab_with_right_to_review_feedback' => 0,
                    'can_view_goal_progress' => 0,
                    'can_enter_data_for_measurement_where_goal_owners' => 1,
                    'can_enter_data_for_measurement_where_goal_reviewers' => 1,
                    'can_use_frameworks_in_goal_evidence' => 0,
                    'can_archive_goals' => 0,
                );
                DB::table('goal_users_settings')->insert($data);
            }
        }
        else if($role_id == 110){
            $user_check = GoalUserSettings::where("account_id",$account_id)->where("user_id",$user_id)->where("role_id",$role_id)->where("site_id",$this->site_id)->first();
            if(!$user_check)
            {
                DB::table('goal_users_settings')->where("account_id",$account_id)->where("user_id" , $user_id)->where("site_id",$this->site_id)->delete();

                $data = array(
                    'account_id' => $account_id,
                    'user_id' => $user_id,
                    'role_id' => $role_id,
                    'site_id' => $this->site_id,
                    'can_create_individual_goals_for_others_people' => 1,
                    'can_create_individual_goals_for_themselves' => 1 ,
                    'can_create_group_goals' => 1,
                    'can_create_group_templates' => 1 ,
                    'can_use_templates_to_create_new_goals' => 1 ,
                    'can_create_goal_categories' => 1 ,
                    'can_be_added_as_goal_collab_with_view_rights' => 1 ,
                    'can_be_added_as_goal_collab_with_edit_rights' => 1 ,
                    'can_be_added_as_goal_collab_with_right_to_add_evidence' => 1 ,
                    'can_be_added_as_goal_collab_with_right_to_review_feedback' => 1,
                    'can_view_goal_progress' => 1,
                    'can_enter_data_for_measurement_where_goal_owners' => 1,
                    'can_enter_data_for_measurement_where_goal_reviewers' => 1,
                    'can_use_frameworks_in_goal_evidence' => 1,
                    'can_archive_goals' => 1,
                );
                DB::table('goal_users_settings')->insert($data);
            }
        }
        else if($role_id == 100){
            $user_check = GoalUserSettings::where("account_id",$account_id)->where("user_id",$user_id)->where("role_id",$role_id)->where("site_id",$this->site_id)->first();
            if(!$user_check)
            {
                DB::table('goal_users_settings')->where("account_id",$account_id)->where("user_id" , $user_id)->where("site_id",$this->site_id)->delete();

                $data = array(
                    'account_id' => $account_id,
                    'user_id' => $user_id,
                    'role_id' => $role_id,
                    'site_id' => $this->site_id,
                    'can_create_individual_goals_for_others_people' => 1,
                    'can_create_individual_goals_for_themselves' => 1 ,
                    'can_create_group_goals' => 1,
                    'can_create_group_templates' => 1 ,
                    'can_use_templates_to_create_new_goals' => 1 ,
                    'can_create_goal_categories' => 1 ,
                    'can_be_added_as_goal_collab_with_view_rights' => 1 ,
                    'can_be_added_as_goal_collab_with_edit_rights' => 1 ,
                    'can_be_added_as_goal_collab_with_right_to_add_evidence' => 1 ,
                    'can_be_added_as_goal_collab_with_right_to_review_feedback' => 1,
                    'can_view_goal_progress' => 1,
                    'can_enter_data_for_measurement_where_goal_owners' => 1,
                    'can_enter_data_for_measurement_where_goal_reviewers' => 1,
                    'can_use_frameworks_in_goal_evidence' => 1,
                    'can_archive_goals' => 1,
                );
                DB::table('goal_users_settings')->insert($data);
            }
        }
        else if($role_id == 115){
            $user_check = GoalUserSettings::where("account_id",$account_id)->where("user_id",$user_id)->where("role_id",$role_id)->where("site_id",$this->site_id)->first();
            if(!$user_check)
            {
                DB::table('goal_users_settings')->where("account_id",$account_id)->where("user_id" , $user_id)->where("site_id",$this->site_id)->delete();

                $data = array(
                    'account_id' => $account_id,
                    'user_id' => $user_id,
                    'role_id' => $role_id,
                    'site_id' => $this->site_id,
                    'can_create_individual_goals_for_others_people' => 1,
                    'can_create_individual_goals_for_themselves' => 1 ,
                    'can_create_group_goals' => 1,
                    'can_create_group_templates' => 0 ,
                    'can_use_templates_to_create_new_goals' => 1 ,
                    'can_create_goal_categories' => 0 ,
                    'can_be_added_as_goal_collab_with_view_rights' => 1 ,
                    'can_be_added_as_goal_collab_with_edit_rights' => 1 ,
                    'can_be_added_as_goal_collab_with_right_to_add_evidence' => 1 ,
                    'can_be_added_as_goal_collab_with_right_to_review_feedback' => 1,
                    'can_view_goal_progress' => 0,
                    'can_enter_data_for_measurement_where_goal_owners' => 1,
                    'can_enter_data_for_measurement_where_goal_reviewers' => 1,
                    'can_use_frameworks_in_goal_evidence' => 1,
                    'can_archive_goals' => 0,
                );
                DB::table('goal_users_settings')->insert($data);
            }
       }
    }    
}