<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ResourceController;
use App\Http\Controllers\HuddleController;
use App\Models\Account;
use App\Models\AccountCommentTag;
use App\Models\AccountFolder;
use App\Models\AccountFolderMetaData;
use App\Models\AccountFolderUser;
use App\Models\AccountMetaData;
use App\Models\AccountTag;
use App\Models\Document;
use App\Models\DocumentFolder;
use App\Models\DocumentSubtitle;
use App\Models\AccountFolderDocument;
use App\Models\DocumentStandardRating;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\UserActivityLog;
use App\Models\UserAssessmentCustomField;
use App\Models\AccountFolderGroup;
use App\Models\Comment;
use App\Models\EdtpaAssessmentPortfolioCandidateSubmissions;
use App\Models\Group;
use App\Models\UserGroup;
use App\Models\EmailUnsubscribers;
use App\Models\DocumentFiles;
use App\Models\AccountFrameworkSettingPerformanceLevel;
use App\Models\AccountFrameworkSetting;
use App\Models\PerformanceLevelDescription;
use App\Models\AccountStudentPayments;
use App\Models\JobQueue;
use App\Models\AccountFolderdocumentAttachment;
use App\Models\CommentAttachment;
use App\Models\UserOnlineStatus;
use App\Models\Sites;
use App\Models\DocumentMetaData;
use App\Services\S3Services;
use DateInterval;
use DatePeriod;
use Datetime;
use GuzzleHttp\TransferStats;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
use Intercom\IntercomClient;
use GuzzleHttp\Exception\ClientException;
use App\Services\SendGridEmailManager;
use App\Services\HelperFunctions;
use App\Services\TranslationsManager;
use Aws\Sns\SnsClient;
use App\Services\Emails\Email;
use App\Services\QueuesManager;
use App\Services\PDF;

class VideoController extends Controller {

    protected $httpreq;
    protected $site_id;
    protected $current_lang;
    protected $site_title;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        //
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->current_lang = $this->httpreq->header('current-lang');
        $this->site_title = Sites::get_site_title($this->site_id);
    }

    function get_video_comments_with_replies($video_id, $get_all_comments = 0, $account_folder_id, $user_id, $tags = '' , $comment_status = false , $page_sort = '', $search_text='' ) {
        // $support_audio_annotation = isset($this->request->data['support_audio_annotation']) ? $this->request->data['support_audio_annotation'] : 0;
        // $huddleVideo = array();
        $h_type = AccountFolderMetaData::getMetaDataValue($account_folder_id, $this->site_id);
        $support_audio_annotation = 1;
        $video_id = $video_id;

        if ((!$this->check_if_evalutor($account_folder_id, $user_id)) && (($h_type == '2' && $this->is_enabled_coach_feedback($account_folder_id, $this->site_id)) || $h_type == '3')) {

            $comments_res = $this->getVideoComments($video_id, $page_sort, '',$search_text, $tags , $support_audio_annotation, 1, false, '', true , false ,$comment_status);
        } else {
            $comments_res = $this->getVideoComments($video_id, $page_sort, '', $search_text, $tags , $support_audio_annotation, 0 , false, '', false ,false ,$comment_status);
        }

        //print_r($comments_res);die;


        $view = new View($this, false);
        $comment_result = $this->modifyComments($comments_res, $get_all_comments);

        $huddleVideo['Document']['comments'] = $comment_result;
        return $huddleVideo;
    }

    public function modifyComments($comments_res, $get_all_comments = 1) {
        $comment_result = array();
        foreach ($comments_res as $row) {

            if ($row['ref_type'] == 6) {
                $tem = explode(".", $row['comment']);
                if (isset($tem[1])) {
                    $row['extension'] = explode("?", $tem[1])[0];
                    if ($row['extension'] == 'm4a') {
                        $row['extension'] = 'mp4'; //html5 audio player treat m4a as audio/mp4
                    }
                }

                $row['comment'] = $this->get_cloudfront_url($row['comment']);
            }
            $created_date = new DateTime($row['created_date']);
            $last_edit_date = new DateTime($row['last_edit_date']);

            $row['Comment']['last_edit_date'] = $last_edit_date->format(DateTime::W3C);
            $row['Comment']['created_date'] = $created_date->format(DateTime::W3C);
            $commentDate = $row['created_date'];
            $start_date = new DateTime($commentDate);
            $row['is_new_comment'] = $start_date > new DateTime('2018-08-10 00:00:00');
            $row['created_date_string'] = $this->times_ago($row['created_date']);

            $response = $this->get_replies_from_comment($row, $get_all_comments);
            $row['Comment'] = $response;
            $row['replies_count'] = count($response['responses']);
            $new_reply_array = [];
            foreach($response['responses'] as $reply)
            {
                if ($reply['ref_type'] == 6) {
                    $tem = explode(".", $reply['comment']);
                    if (isset($tem[1])) {
                        $reply['extension'] = explode("?", $tem[1])[0];
                        if ($reply['extension'] == 'm4a') {
                            $reply['extension'] = 'mp4'; //html5 audio player treat m4a as audio/mp4
                        }
                    }

                    $reply['comment'] = $this->get_cloudfront_url($reply['comment']);
                }
                $new_reply_array[] = $reply;
            }
            $row['Comment']['responses'] = $new_reply_array;
            $get_standard = $this->gettagsbycommentid($row['id'], array('0')); //get standards
            $get_tags = $this->gettagsbycommentid($row['id'], array('1', '2')); //get tags
            $get_custom_marker = $this->gettagsbycommentid($row['id'], array('2'));
            if(!empty($get_custom_marker))
            {
                $row['none_tag'] = false;
            }
            else
            {
                $row['none_tag'] = true;
            }
            $get_tags_length = $this->gettagsbycommentid($row['id'], array('1'));
            $row['tags_length'] = count($get_tags_length);
            $row['standard'] = $get_standard;
            $row['default_tags'] = $get_tags;

            $comment_result[] = $row;
        }
        return $comment_result;
    }

    public function get_cloudfront_url($url) {
        //$url = $row['comment'];
        $videoFilePath = pathinfo($url);
        $videoFileName = $videoFilePath['filename'];
        if ($videoFilePath['dirname'] == ".") {
            $video_path = $this->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a"));
            $relative_video_path = $videoFileName . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a");
        } else {
            $video_path = $this->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a"));
            $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a");
        }
        return $video_path;
    }

    public function get_secure_aws_url($url) {
        //$url = $row['comment'];
        $videoFilePath = pathinfo($url);
        $videoFileName = $videoFilePath['filename'];

        if ($videoFilePath['dirname'] == ".") {

            $video_path = $this->getSecureAmazonUrl($videoFileName . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a"));
            $relative_video_path = $videoFileName . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a");
        } else {

            $video_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . $videoFileName . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a"));
            $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a");
        }
        return $video_path;
    }

    public function times_ago($commentDate) {
        $translations = TranslationsManager::get_page_lang_based_content("Api/video_details", $this->site_id, '', $this->current_lang);
        //$commentDate = $row['created_date'];
        $currentDate = date('Y-m-d H:i:s');

        $start_date = new DateTime($commentDate);
        $since_start = $start_date->diff(new DateTime($currentDate));

        $commentsDate = '';
        if ($since_start->y > 0) {
            $commentsDate = ($since_start->y > 1 ? str_replace("xx", $since_start->y, $translations["vd_years_ago"]) : $translations["vd_year_ago"]);
        } elseif ($since_start->m > 0) {
            $commentsDate = ($since_start->m > 1 ? str_replace("xx", $since_start->m, $translations["vd_months_ago"]) : $translations["vd_month_ago"]);
        } elseif ($since_start->d > 0) {
            $commentsDate = ($since_start->d > 1 ? str_replace("xx", $since_start->d, $translations["vd_days_ago"]) : $translations["vd_day_ago"]);
        } elseif ($since_start->h > 0) {
            $commentsDate = ($since_start->h > 1 ? str_replace("xx", $since_start->h, $translations["vd_hours_ago"]) : $translations["vd_hour_ago"]);
        } elseif ($since_start->i > 0) {
            $commentsDate = ($since_start->i > 1 ? str_replace("xx", $since_start->i, $translations["vd_minutes_ago"]) : $translations["vd_minute_ago"]);
        } elseif ($since_start->s > 0) {
            $commentsDate = $translations["vd_less_than_a_minute_ago"];
        } else {
            $commentsDate = $translations["vd_just_now"];
        }
        return $commentsDate;
    }

    function getVideoCommentsBySTP($video_id, $changType = '', $huddel_id = '', $searchCmt = '', $tags = '', $support_audio_annotation = 0, $get_all_comments = 0, $include_replys = false, $account_tag_id = '', $active = false , $page = false , $comment_status = false) {
        $video_id = (int) $video_id;
        if ($huddel_id != '') {
            $huddel_id = (int) $huddel_id;
        }

        if ($support_audio_annotation == 1) {
            if ($get_all_comments == 1)
                $cmt_type = array('2', '6', '7');
            else
                $cmt_type = array('2', '6');
        }else {
            $cmt_type = array('2');
        }
        $cmt_type = implode(',', $cmt_type);
        if (empty($changType))
            $changType = 0;
        $data = DB::select('call getusercomments(?,?,?)', array($video_id, "'$cmt_type'", $changType));
        $data = $this->convert_array($data);
        return $data;
    }

    public function convert_array($data) {
        $data = collect($data)->map(function($x) {
                    return (array) $x;
                })->toArray();
        return $data;
    }

    public function getSingleComment(Request $request) {
        $result = $this->getVideoComments($request->get("video_id"), '', '', '', '', 1, 1, true, '', false, $request->get("comment_id"));
        $comment_result = $this->modifyComments($result);
        return isset($comment_result[0]) ? $comment_result[0] : [];
    }

    function getVideoComments($video_id, $changType = '', $huddel_id = '', $searchCmt = '', $tags = '', $support_audio_annotation = 0, $get_all_comments = 0, $include_replys = false, $account_tag_id = '', $active = false, $comment_id = false,$comment_status = false) {
        $video_id = (int) $video_id;
        if ($huddel_id != '') {
            $huddel_id = (int) $huddel_id;
        }


        $query = Comment::leftJoin('users as User', 'User.id', '=', 'Comment.user_id')
                ->leftJoin('account_comment_tags AS act', 'Comment.id', '=', 'act.comment_id');

        $fields = "Comment.*, User.id as user_id, User.first_name, User.last_name, User.image,User.email";

        if ($changType == '1') {
            $order_by = 'Comment.id ASC';
            $query->selectRaw($fields);
        } elseif ($changType == '2') {
            $order_by = 'time_modified ASC';
            $query->selectRaw($fields)->selectRaw('CASE WHEN time IS NULL THEN (SELECT MAX(id) + 1 FROM comments) ELSE time  END time_modified');
        } elseif ($changType == '3') {
            $order_by = 'User.first_name ASC,Comment.time ASC';
            $query->selectRaw($fields);
        } elseif ($changType == '4') {
            $order_by = 'Comment.id DESC';
            $query->selectRaw($fields);
        } elseif ($changType == '5') {
            $order_by = 'Comment.id DESC';
            $query->selectRaw($fields);
        } elseif ($changType == '6') {
            $order_by = 'Comment.time ASC';
            $query->selectRaw($fields);
        } elseif ($changType == '7') {
            $order_by = 'Comment.page ASC';
            $query->selectRaw($fields);
        }
        else {
            $order_by = 'Comment.time ASC';
            $query->selectRaw($fields);
        }
        if ($support_audio_annotation == 1) {
            if ($get_all_comments == 1)
                $cmt_type = array('2', '6', '7');
            else
                $cmt_type = array('2', '6');
        }else {
            $cmt_type = array('2');
        }



        if ($include_replys) {
            array_push($cmt_type, '3');
            $parent_condition = '';
        } else {
            $parent_condition = 'Comment.parent_comment_id IS NULL';
            $query->whereRaw($parent_condition);
        }
        $cmt_type = implode(',', $cmt_type);
        $search_string = $searchCmt;
        $searchCmt = '';
        if (!empty($searchCmt)) {
            if (!empty($tags)) {
                $conditions = array('Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, 'OR' => array("Comment.comment Like" => "%$searchCmt%", "act.tag_title Like" => "%$searchCmt%"), "act.account_tag_id" => $tags);
            } else {
                $conditions = array('Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, 'OR' => array("Comment.comment Like" => "%$searchCmt%", "act.tag_title Like" => "%$searchCmt%"));
            }
        } else {
            if (!empty($tags)) {
                $conditions = array('Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id, "act.account_tag_id" => $tags);

                $query->where(array(
                    'Comment.ref_id' => $video_id
                ))->whereRaw("Comment.ref_type IN (" . $cmt_type . ")")->whereRaw("act.account_tag_id IN (" . $tags . ")");
            } else {
                $conditions = array($parent_condition, 'Comment.ref_type' => $cmt_type, 'Comment.ref_id' => $video_id);

                if (!empty($account_tag_id)) {
                    $query->where(array(
                        'Comment.ref_id' => $video_id,
                        "act.account_tag_id" => $account_tag_id
                    ))->whereRaw("Comment.ref_type IN (" . $cmt_type . ")");
                } else {

                    $query->where(array(
                        'Comment.ref_id' => $video_id
                    ))->whereRaw("Comment.ref_type IN (" . $cmt_type . ")");
                }
            }
        }

        if ($active) {
            $query->where('Comment.active', 1);
        }
        if(!empty($search_string))
        {
            $query->whereRaw("(Comment.`ref_id` = $video_id AND(Comment.`comment` LIKE '%$search_string%' OR EXISTS (SELECT 1 FROM comments WHERE ref_type = 3 AND parent_comment_id = Comment.id AND `comment` LIKE '%$search_string%')) AND Comment.ref_type = 2)");
        }
        if ($comment_id) {
            $query->where('Comment.id', $comment_id);
        }

        if($comment_status)
        {
            $query->where('Comment.comment_status', $comment_status);
        }

        $query->where('Comment.site_id', $this->site_id);

        $result = $query->groupBy('Comment.id')->orderByRaw($order_by)->get()->toArray();
        return $result;
    }

    public function gettagsbycommentid($comment_id, $tag_type) {
        $tag_type = implode(',', $tag_type);
        $standards = array();   
        $get_tags = AccountCommentTag::leftJoin('account_tags', 'AccountCommentTag.account_tag_id', '=', 'account_tags.account_tag_id')
                ->selectRaw("account_tags.*,AccountCommentTag.*,account_tags.tag_title as tag_title2")
                ->where(array("comment_id" => $comment_id, 'AccountCommentTag.site_id' => $this->site_id))
                ->whereRaw('ref_type IN(' . $tag_type . ')')
                ->get()
                ->toArray();
        if($get_tags){
            foreach($get_tags as $row){
                if($row['tag_title'] ==''){
                    $row['tag_title'] = $row['tag_html'];
                }
                $standards[] = $row;
            }
        }
        return  $standards;
    }

    function addComment(Request $request) {
        $input = $request->all();

        //$input['videoId'] = '87977';
        //$input['for'] =  '' ;
        //$input['synchro_time'] = '';
        //$input['ref_type'] = '2';
        //$input['comment'] = 'Hello Api Commet Finally 2';
        //$input['user_id'] = '621';
        //$standards_acc_tags = '5234';
        //$input['default_tags'] = 'fgfdg,saad,waqas';
        //$input['assessment_value'] = '# Glow more';
        //$input['standards'] = '# A.1.3-Management';


        if (empty($input['videoId']))
            die('miss videoId');

        $account_folder_document = $this->getAccountFolderDocumentDetail($input['videoId']);
        $original_document = Document::where("id", $input['videoId'])->where("site_id", $this->site_id)->first();

        if (empty($account_folder_document))
            die('invalid videoId');

        if (!isset($input['user_id']) || empty($input['user_id']))
            die('User ID is required.');
        
        $latest_comment = array();
       if(!isset($input['platform']))
       {
          if (!isset($input['created_at_gmt']) || empty($input['created_at_gmt'])){
              $template="<b>This user is using older version of app</b><br>
                         <b>Form Fields : </b>".json_encode($input)." <br> 
                        <b>URL :</b>".$request->url();
                    $emailData = [
                            'from' => Sites::get_site_settings('static_emails', $this->site_id)['noreply'],
                            'from_name' => Sites::get_site_settings('site_title', $this->site_id),
                            'to' => 'khurrams@sibme.com',
                            'cc' => 'waqasn@sibme.com',
                            'subject' => "created_at_gmt is required for add/edit comment",
                            'template' => $template
                        ];

                 Email::sendCustomEmail($emailData);
          }else{
               $comment_exist = Comment::where(array(
                        'ref_id' => $input["videoId"],
                        'created_at_gmt' => $input['created_at_gmt'],
                        'user_id' => $input['user_id'],
                        'site_id' => $this->site_id
                    ))->first();
                if(!empty($comment_exist)){
                    $latest_comment = $this->get_latest_comment($comment_exist->id);
                    $get_standard = $this->gettagsbycommentid($comment_exist->id, array('0')); //get standards
                    $get_tags = $this->gettagsbycommentid($comment_exist->id, array('1', '2')); //get tags
                    $get_tags_length = $this->gettagsbycommentid($comment_exist->id, array('1'));
                    $latest_comment['standard'] = $get_standard;
                    $latest_comment['default_tags'] = $get_tags;
                    $latest_comment['fake_id'] = isset($input['fake_id']) ? $input['fake_id'] : '';
                    $latest_comment['is_new_comment'] = true;
                    $latest_comment['tags_length'] = count($get_tags_length);
                    $latest_comment['created_date_string'] = $this->times_ago(date('Y-m-d H:i:s'));
                    $latest_comment['comment_id'] = $comment_exist->id;
                    $res = array(
                        $latest_comment,
                        'status' => 'success',
                        'code'=>409
                    );
                    return response()->json($res);
                }
          }
       }

        $account_folder_id = $account_folder_document['account_folder_id'];

        $huddle = AccountFolder
                ::where(array(
                    'account_folder_id' => $account_folder_id,
                    'site_id' => $this->site_id
                ))->first()->toArray();

        if ($huddle['folder_type'] == '1' && !isset($input['platform'])) {
            $huddle_participant_check = AccountFolderUser
                    ::where(array(
                        'account_folder_id' => $account_folder_id,
                        'site_id' => $this->site_id,
                        'user_id' => $input['user_id']
                    ))->first();

            $group_ids = array();

            $huddle_groups = AccountFolderGroup::where(array(
                        'account_folder_id' => $account_folder_id,
                        'site_id' => $this->site_id
                    ))->get()->toArray();

            foreach ($huddle_groups as $group) {
                $group_ids[] = $group['group_id'];
            }

            $huddle_group_users = UserGroup::where(array(
                        'user_id' => $input['user_id'],
                        'site_id' => $this->site_id
                    ))->whereIn('group_id', $group_ids)->first();



            if ($huddle_participant_check || $huddle_group_users) {
                //$huddle_participant_check = $huddle_participant_check->toArray();
            } else {
                $res = array(
                    'message' => 'You are not Participating in the Huddle.',
                    'status' => 'failed'
                );
                return response()->json($res);
            }
        }



        $time = '';
        $accessLevel = '';
        $input['for'] = isset($input['for']) && !empty($input['for']) ? $input['for'] : "";
        $input['synchro_time'] = isset($input['synchro_time']) && !empty($input['synchro_time']) ? $input['synchro_time'] : "";
        if ($input['for'] == 'synchro_time' && $input['synchro_time'] != '') {
            if ($original_document && $original_document->doc_type == 4) {
                $live_start_time = strtotime($original_document->created_date);
                $current_time = time();
                $activityLogs = DB::table('user_activity_logs')->where(['ref_id' => $original_document->id, 'type' => 24])->first();
                $diff = 5; //Android
                if ($activityLogs && $activityLogs->environment_type == 1) {//IOS
                    $diff = 5;
                }
                $time = $current_time - $live_start_time - $diff;
            } else {
                $time = $input['synchro_time'];
            }
        } else {
            $accessLevel = 'All';
        }

        if (isset($input['ref_type']) && ($input['ref_type'] == '6' || $input['ref_type'] == '7')) {
            $ref_type = $input['ref_type'];
        } else {
            $ref_type = '2';
        }
        $active = '1';
        if (isset($input['audio_duration']) && $input['audio_duration'] > 0) {
            $audio_duration = $input['audio_duration'];
        } else {
            $audio_duration = '0';
        }

        $data = array(
            'comment' => $input['comment'],
            'time' => $time,
            'site_id' => $this->site_id,
            // 'access_level' => $accessLevel,
            'ref_id' => $input['videoId'],
            'ref_type' => $ref_type,
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $input['user_id'],
            'last_edit_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $input['user_id'],
            'user_id' => $input['user_id'],
            'audio_duration' => $audio_duration,
            'active' => $active
        );

        if(isset($input['xPos']) && isset($input['yPos']) && isset($input['page']) )
        {
            $data['xPos'] = $input['xPos'];
            $data['yPos'] = $input['yPos'];
            $data['page'] = $input['page'];
        }

        if(isset($input['width']))
        {
            $data['width'] = $input['width'];
        }
        if(isset($input['height']))
        {
            $data['height'] = $input['height'];
        }
        if(isset($input['type']))
        {
            $data['type'] = $input['type'];
        }
        
        if(isset($input['page_width']))
        {
            $data['page_width'] = $input['page_width'];
        }
        
        if(isset($input['page_height']))
        {
            $data['page_height'] = $input['page_height'];
        }

        if (isset($input['comment_status']) && !empty($input['comment_status'])) {
            $data['comment_status'] = $input['comment_status'];
        }
        if (isset($input['created_at_gmt']) && !empty($input['created_at_gmt'])) {
            $data['created_at_gmt'] = $input['created_at_gmt'];
        }
        if (isset($input['end_time']) && $input['end_time'] != '') {
            $data['end_time'] = $input['end_time'];
        }
        if (isset($input['annotation_data']) && $input['annotation_data'] != '') {
            $data['annotation_data'] = json_encode($input['annotation_data']);
        }

        $htype = AccountFolderMetaData::getMetaDataValue($account_folder_id, $this->site_id);
        $huddle_name = '';
        if ($htype == '1') {
            $huddle_name = 'Collaboration Huddle';
        } elseif ($htype == '2') {
            $huddle_name = 'Coaching Huddle';
        } else {
            $huddle_name = 'Assessment Huddle';
        }

        if (isset($input['account_role_id']) && isset($input['current_user_email']) && !isset($input['platform'])) {
            $user_role = $this->get_user_role_name($input['account_role_id']);
            $in_trial_intercom = $this->check_if_account_in_trial_intercom($huddle['account_id']);

            if ($huddle['folder_type'] == '3') {

                $meta_data = array(
                    "comment_added" => $input['comment'],
                    "workspace_comment" => 'True',
                    "user_role" => $user_role,
                    "is_in_trial" => $in_trial_intercom,
                    'Platform' => 'Web'
                );

                $this->create_intercom_event('comment-added', $meta_data, $input['current_user_email']);
                $this->create_churnzero_event('Workspace+Comment+Added', $huddle['account_id'], $input['current_user_email']);
            } else {

                $meta_data = array(
                    "comment_added" => $input['comment'],
                    "huddle_type" => $huddle_name,
                    "user_role" => $user_role,
                    "is_in_trial" => $in_trial_intercom,
                    'Platform' => 'Web'
                );

                $this->create_intercom_event('comment-added', $meta_data, $input['current_user_email']);
                $this->create_churnzero_event('Huddle+Comment+Added', $huddle['account_id'], $input['current_user_email']);
            }
        }


        if (!isset($input['platform']) && $htype == 3 && $this->check_if_evaluated_participant($account_folder_id, $input['user_id']) && !$this->can_assessee_comment($account_folder_id, $this->site_id)) {
            $res = array(
                'message' => TranslationsManager::get_translation('assessed_participant_cannot_add_comments', 'Api/addComment', $this->site_id),
                'status' => 'failed'
            );
            return response()->json($res);
        }

        if (!isset($input['platform']) && ($htype == 3 || ($htype == 2 && $this->is_enabled_coach_feedback($account_folder_id, $this->site_id))) && $this->check_if_evalutor($account_folder_id, $input['user_id'])) {   //coaching huddle feedback
            $is_feedback_published = false;
            $coach_evaluator_ids = $this->get_evaluator_ids($account_folder_id);
            $video_comments_list = $this->getVideoComments($input['videoId'], '', '', '', '', 1, 0, false);
            if (!empty($video_comments_list)):
                foreach ($video_comments_list as $single_video_comment):
                    if ($single_video_comment['active'] == '1' && in_array($single_video_comment['created_by'], $coach_evaluator_ids)) {
                        $is_feedback_published = true;
                    }
                endforeach;
            endif;


            if ($is_feedback_published) {
                $data['active'] = '1';
            }elseif($huddle['is_published_feedback'] ==1){
                $data['active'] = '1';
            } else {
                $data['active'] = '0';
            }
        }

        if ((isset($input['observation_check']) && $input['observation_check']) || ($original_document && $original_document->doc_type == 4)) {
            $data['active'] = '1';
        }

        $data["encoder_status"] = "Complete";
        if ($ref_type == '6' || $ref_type == '7') {
            $s3_service = new S3Services(
                    config('s3.amazon_base_url'), config('s3.bucket_name'), config('s3.access_key_id'), config('s3.secret_access_key')
            );
            $full_path = $input['comment'];
            $videoFilePath = pathinfo($full_path);
            $new_path = $videoFilePath['dirname'] . "/" . $videoFilePath['filename'];
            $s_url = config('s3.amazon_base_url') . $full_path;
            $s3_zencoder_id = $s3_service->encode_audios($s_url, $videoFilePath['dirname'], $new_path);
            $data["zencoder_output_id"] = $s3_zencoder_id;
            if(isset($input['platform']) && $input['platform'] == 'document-commenting' )
            {
                $data["encoder_status"] = "Processing";
            }
            else
            {
                $data["encoder_status"] = "Processing";
            }
            $data["comment"] = $full_path;
        }
        $comment_id = DB::table('comments')->insertGetId($data);
        if ($comment_id) {
            if(!empty($input['videoId']))//sw-5005
            {
                Document::where('id',$input['videoId'])->update(['last_edit_date'=>date("Y-m-d H:i:s"), 'last_edit_by'=>$input['user_id']]);
            }
            if(!empty($account_folder_id))//sw-5004
            {
                AccountFolder::where('account_folder_id', $account_folder_id)->update(['last_edit_date'=>date("Y-m-d H:i:s"), 'last_edit_by'=>$input['user_id']]);
            }
            if (isset($huddle) && isset($input['user_id'])) {
                $activity_type = '5';
                if(isset($input['platform']))//Document Comenting
                {
                    $activity_type = '66';
                }
                $environment_type = 1;
                if (isset($input['environment_type'])) {
                    $environment_type = $input['environment_type'];
                }
                if ($huddle['folder_type'] == 1) {
                    $user_activity_logs = array(
                        'ref_id' => $comment_id,
                        'desc' => $input['comment'],
                        'url' => '/Huddles/view/' . $account_folder_id . "/1/" . $input['videoId'],
                        'account_folder_id' => $account_folder_id,
                        'type' => $activity_type,
                        'account_id' => $huddle['account_id'],
                        'site_id' => $this->site_id,
                        'environment_type' => $environment_type
                    );
                } 
                else if ($huddle['folder_type'] == 2) {
                     $user_activity_logs = array(
                        'ref_id' => $comment_id,
                        'desc' => $input['comment'],
                        'url' => '/home/library_video/home/' . $account_folder_id . '/' . $input['videoId'],
                        'account_folder_id' => $account_folder_id,
                        'type' => $activity_type,
                        'account_id' => $huddle['account_id'],
                        'site_id' => $this->site_id,
                        'environment_type' => $environment_type
                    );
                }
                
                else {
                    $user_activity_logs = array(
                        'ref_id' => $comment_id,
                        'desc' => $input['comment'],
                        'url' => '/home/workspace_video/home/' . $account_folder_id . '/' . $input['videoId'],
                        'account_folder_id' => $account_folder_id,
                        'type' => $activity_type,
                        'account_id' => $huddle['account_id'],
                        'site_id' => $this->site_id,
                        'environment_type' => $environment_type
                    );
                }

                if (isset($input['is_scripted_note'])) {
                    if ($huddle['folder_type'] == 1) {
                        $user_activity_logs['url'] = '/home/video_details/scripted_observations/' . $account_folder_id . '/' . $input['videoId'];
                    } else {
                        $user_activity_logs['url'] = '/home/video_details/scripted_observations/' . $account_folder_id . '/' . $input['videoId'] . '?workspace=true';
                    }
                }

                $this->user_activity_logs($user_activity_logs, $huddle['account_id'], $input['user_id']);
            }


            $latest_comment = $this->get_latest_comment($comment_id);


            $standards = '';
            $standards_acc_tags = isset($input['standards_acc_tags']) ? $input['standards_acc_tags'] : '';
            if (!empty($standards_acc_tags)) {
                if (isset($input['account_role_id']) && isset($input['current_user_email']) && !isset($input['platform'])) {
                    $user_role = $this->get_user_role_name($input['account_role_id']);
                    $in_trial_intercom = $this->check_if_account_in_trial_intercom($huddle['account_id']);


                    $meta_data = array(
                        'framework_added_in_comment' => 'true',
                        'user_role' => $user_role,
                        'is_in_trial' => $in_trial_intercom,
                        'Platform' => 'Web'
                    );


                    $this->create_intercom_event('framework-added-in-comment', $meta_data, $input['current_user_email']);
                }

                $this->save_standard_tag($standards, $comment_id, $input['videoId'], $huddle['account_id'], $input['user_id'], $account_folder_id, $standards_acc_tags);
            }
            $tags = isset($input['default_tags']) ? $input['default_tags'] : '';
            if (!empty($tags)) {
                $this->save_tag($tags, $comment_id, $input['videoId'], $huddle['account_id'], $input['user_id']);
            }

            $tags_1 = isset($input['assessment_value']) ? $input['assessment_value'] : '';
            if (!empty($tags_1)) {
                if (isset($input['account_role_id']) && isset($input['current_user_email']) && !isset($input['platform'])) {
                    $user_role = $this->get_user_role_name($input['account_role_id']);
                    $in_trial_intercom = $this->check_if_account_in_trial_intercom($huddle['account_id']);
                    $meta_data = array(
                        'custom_marker_tags_added' => 'true',
                        'user_role' => $user_role,
                        'is_in_trial' => $in_trial_intercom,
                        'Platform' => 'Web'
                    );

                    $this->create_intercom_event('custom-marker-tags-added', $meta_data, $input['current_user_email']);
                }
                $this->save_tag_1($tags_1, $comment_id, $input['videoId'], $huddle['account_id'], $input['user_id']);
            }

            $get_standard = $this->gettagsbycommentid($comment_id, array('0')); //get standards
            $get_tags = $this->gettagsbycommentid($comment_id, array('1', '2')); //get tags
            $get_tags_length = $this->gettagsbycommentid($comment_id, array('1'));
            $latest_comment['standard'] = $get_standard;
            $latest_comment['default_tags'] = $get_tags;
            $latest_comment['fake_id'] = isset($input['fake_id']) ? $input['fake_id'] : '';
            $latest_comment['is_new_comment'] = true;
            $latest_comment['tags_length'] = count($get_tags_length);
            $latest_comment['created_date_string'] = $this->times_ago(date('Y-m-d H:i:s'));
            $latest_comment['comment_id'] = $comment_id;
            

            $custom_input = [];
            $custom_input["user_id"] = $input["user_id"];
            $custom_input["video_id"] = $input["videoId"];
            $custom_input["huddle_id"] = $account_folder_id;
            $files = self::getVideoDocuments($request, $custom_input);
            $latest_comment["files"] = $files;
            if (isset($input['fake_id'])) {
                // This is for Add Comment in videos where we don't need files, because we fetch files from Websocket after the comment is added.
                $latest_comment["files"] = [];
            }
            $latest_comment["valid"] = true;

            if(empty($doc_res["title"])){
                $doc_res["title"] =[];
            }

            $current_user_id = $input["user_id"];

            $latest_comment['assessed'] =  false;
            if ($latest_comment['ref_type'] == 6) {
                $latest_comment['comment'] = $this->get_cloudfront_url($latest_comment['comment']);
            }

            $latest_comment['responses'] = array();
            $response = $this->get_replies_from_comment($latest_comment, '1');
            $latest_comment['replies_count'] = count($response['responses']);
            $latest_comment['Comment'] = $latest_comment;

            //$latest_comment['assessors_list'] = AccountFolder::getHuddleUsersIncludingGroups($site_id, $account_folder_id, $current_user_id, $current_user_role,true, 200)['participants'];
            $res = array(
                $latest_comment,
                'status' => 'success',
            );

            // $this->send_add_comment_websockets($htype, $huddle, $original_document, $input, $this->site_id, $comment_id, $account_folder_id, $latest_comment);
            // No need of queue if assessees_list is disabled.
            QueuesManager::sendToNotificationQueue(__CLASS__, "send_add_comment_websockets", $htype, $huddle, $original_document, $input, $this->site_id, $comment_id, $account_folder_id, $latest_comment);
            if ($htype == 3 || ($htype == 2 && $this->is_enabled_coach_feedback($account_folder_id, $this->site_id) && $this->check_if_evalutor($account_folder_id, $input['user_id']))) {

            } else {
                if ($huddle['folder_type'] == 1 && !isset($input['observation_check']) && !isset($input['platform'])) {
                    $input['image'] = isset($input['image']) && !empty($input['image']) ? $input['image'] : "";
                    $input['first_name'] = isset($input['first_name']) && !empty($input['first_name']) ? $input['first_name'] : "";
                    $input['last_name'] = isset($input['last_name']) && !empty($input['last_name']) ? $input['last_name'] : "";
                    $input['company_name'] = isset($input['company_name']) && !empty($input['company_name']) ? $input['company_name'] : "";
                    if(HelperFunctions::is_email_notification_allowed($huddle['account_id'], 'enable_emails_for_add_comment'))
                    {
                        // \Log::info(">>> Sending to Queue via : send_comment_email <<<");
                        QueuesManager::sendToEmailQueue(__CLASS__, "send_comment_email", $this->site_id, $account_folder_id, $huddle['account_id'], $input['user_id'], $input['videoId'], $comment_id, $input['comment'], $input['first_name'], $input['last_name'], $input['company_name'], $input['image']);
                        // $this->send_comment_email($this->site_id, $account_folder_id, $huddle['account_id'], $input['user_id'], $input['videoId'], $comment_id, $input['comment'], $input['first_name'], $input['last_name'], $input['company_name'], $input['image']);
                    }
                }
            }
            return response()->json($res);
        } else {
            $res = array(
                'Comment' => '',
                'status' => 'failed'
            );
            return response()->json($res);
        }

    }

    function send_add_comment_websockets($htype, $huddle, $original_document, $input, $site_id, $comment_id, $account_folder_id, $latest_comment){
        \Log::info(">>>>> Executing send_add_comment_websockets <<<<<");
        \Log::info(['executing_at' => date("d/m/Y H:i:s"),'htype' => $htype, 'site_id' => $site_id, 'comment_id' => $comment_id, 'account_folder_id' => $account_folder_id, 'comment'=>$latest_comment['comment']]);
        // $cp = new \App\Services\CodeProfiler();
        // $cp->start("send_add_comment_websockets_".__LINE__);
        $is_assessed = false;
        $assessees_list = [];
        if ($htype == '3') {
            $huddle_users = AccountFolder::getHuddleUsers($account_folder_id, $site_id);
            $huddle_groups = AccountFolder::getHuddleGroupsUsers($account_folder_id, $site_id);
            $current_user_role = $this->has_admin_access_for_socket($huddle_users, $huddle_groups, $input["user_id"]);
            // Moving Assessees List calculation in the end of function to improve the websocket speed, because publish feedback button appears with a big delay.
            // $assessees_list= AccountFolderUser::getAssesseesList($site_id, $account_folder_id, $input["user_id"], $current_user_role);
            //$assessor_ids[] =$input['user_id'];
            $assessor_ids = AccountFolderUser::get_participants_ids($account_folder_id, 200, $site_id);
            $ids = [];
            foreach($assessor_ids as $assessor){
                $ids [] = $assessor['user_id'];
            }
            $assessor_ids = $ids;
            $is_assessed =  AccountFolderUser::isAssessedByVideo($account_folder_id, $original_document['created_by'], $assessor_ids,$input["videoId"]);
        }
        $doc_res =  Document::get_single_video_data($site_id, $original_document->id, $huddle['folder_type'], 1, $account_folder_id, $input['user_id']);

        $doc_res['assessed'] =  $is_assessed;
        $latest_comment['assessed'] =  $is_assessed;

        $is_workspace_comment = 0;
        if ($huddle && $huddle['folder_type'] == '3') {
            $is_workspace_comment = 1;
        }

        $event = [
            'channel' => 'huddle-details-' . $account_folder_id,
            'event' => "comment_added",
            'notification_type' => ($original_document->doc_type == 4 ? '5' : 10),
            'data' => $latest_comment,
            'huddle_id' => $account_folder_id,
            'is_workspace_comment' => $is_workspace_comment,
            'reference_id' => $original_document->id,
            'video_id' => $original_document->id,
            'item_id' => $comment_id,
            'comment' => $input['comment'],
            'assessees_list'=>$assessees_list,// One Signal is throwing an error: '413 Request Entity Too Large'. This is because of a large dataset in $assessees_list. That's why we are going to send empty $assessees_list to prevent from this error.
            'user' => User::where(array('id' => $input["user_id"], 'site_id' => $site_id))->first()->toArray(),
            'participant_id' => !empty($original_document->created_by) ? $original_document->created_by : false
        ];

        if(isset($input['deviceToken']) && !empty($input['deviceToken'])){
            $event['deviceToken'] = $input['deviceToken'];
        }

        if(HelperFunctions::is_push_notification_allowed($huddle['account_id']) )
        {
            HelperFunctions::broadcastEvent($event,'broadcast_event',true,false);
        }
        else
        {
            HelperFunctions::broadcastEvent($event,'broadcast_event',false,false);
        }

        if ($htype == '3') {
            // Disabling Temporarily to improve the websocket speed, because publish feedback button appears with a big delay.
            // $assessees_list= AccountFolderUser::getAssesseesList($site_id, $account_folder_id, $input["user_id"], $current_user_role);
        }
        $event = [
            'channel' => ($huddle['folder_type'] == '2' ? 'library-'.$huddle['account_id'] : ($huddle['folder_type'] == '3' ? 'workspace-'.$huddle['account_id']."-". $input['user_id']:'huddle-details-' . $account_folder_id)),
            'event' => "resource_renamed",
            'data' => $doc_res,
            'assessees_list'=>$assessees_list,
            'video_file_name' => !empty($doc_res["title"])?$doc_res["title"]:'',
            'is_dummy' => 1,
            'assessed' => $is_assessed
        ];
        HelperFunctions::broadcastEvent($event, "broadcast_event", false); //not sending push to prevent multiple refresh
        // $cp->end();
    }


    function addDocumentComment(Request $request) {
        $input = $request->all();
        $time = '';
        $accessLevel = '';
        $input['for'] = isset($input['for']) && !empty($input['for']) ? $input['for'] : "";
        $input['synchro_time'] = isset($input['synchro_time']) && !empty($input['synchro_time']) ? $input['synchro_time'] : "";


        if (isset($input['ref_type']) && ($input['ref_type'] == '10' || $input['ref_type'] == '11')) {
            $ref_type = $input['ref_type'];
        } else {
            $ref_type = '8';
        }
        $active = '1';
        if (isset($input['audio_duration']) && $input['audio_duration'] > 0) {
            $audio_duration = $input['audio_duration'];
        } else {
            $audio_duration = '0';
        }

        $data = array(
            'comment' => $input['comment'],
            'time' => $time,
            'site_id' => $this->site_id,
            'ref_id' => $input['document_id'],
            'ref_type' => $ref_type,
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $input['user_id'],
            'last_edit_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $input['user_id'],
            'user_id' => $input['user_id'],
            'audio_duration' => $audio_duration,
            'active' => $active,
            'xPos' => $input['xPos'],
            'yPos' => $input['yPos']
        );
        if (isset($input['created_at_gmt']) && !empty($input['created_at_gmt'])) {
            $data['created_at_gmt'] = $input['created_at_gmt'];
        }


        $data["encoder_status"] = "Complete";
        if ($ref_type == '6' || $ref_type == '7') {
            $s3_service = new S3Services(
                    config('s3.amazon_base_url'), config('s3.bucket_name'), config('s3.access_key_id'), config('s3.secret_access_key')
            );
            $full_path = $input['comment'];
            $videoFilePath = pathinfo($full_path);
            $new_path = $videoFilePath['dirname'] . "/" . $videoFilePath['filename'];
            $s_url = config('s3.amazon_base_url') . $full_path;
            $s3_zencoder_id = $s3_service->encode_audios($s_url, $videoFilePath['dirname'], $new_path);
            $data["zencoder_output_id"] = $s3_zencoder_id;
            $data["encoder_status"] = "Processing";
            $data["comment"] = $full_path;
        }

        if (DB::table('comments')->insert($data)) {

            $comment_id = DB::getPdo()->lastInsertId();

            $standards = '';
            $standards_acc_tags = isset($input['standards_acc_tags']) ? $input['standards_acc_tags'] : '';
            if (!empty($standards_acc_tags)) {
                $this->save_standard_tag($standards, $comment_id, $input['document_id'], $input['account_id'], $input['user_id'], $account_folder_id, $standards_acc_tags);
            }
            $tags = isset($input['default_tags']) ? $input['default_tags'] : '';
            if (!empty($tags)) {
                $this->save_tag($tags, $comment_id, $input['document_id'], $input['account_id'], $input['user_id']);
            }

            $tags_1 = isset($input['assessment_value']) ? $input['assessment_value'] : '';
            if (!empty($tags_1)) {
                $this->save_tag_1($tags_1, $comment_id, $input['document_id'], $input['account_id'], $input['user_id']);
            }

            $res = array(
                'status' => 'success'
            );

            return response()->json($res);
        } else {
            $res = array(
                'Comment' => '',
                'status' => 'failed'
            );
            return response()->json($res);
        }
    }

    function has_admin_access_for_socket($huddleUsers, $account_folder_groups, $user_id) {
        $huddle_role = FALSE;
        if ($huddleUsers && count($huddleUsers) > 0 || $account_folder_groups && count($account_folder_groups) > 0 && $user_id != '') {
            foreach ($huddleUsers as $huddle_user) {
                if (isset($huddle_user) && $huddle_user['user_id'] == $user_id) {
                    $huddle_role = $huddle_user['role_id'];
                    return $huddle_role;
                } else {
                    $huddle_role = FALSE;
                }
            }
            if ($huddle_role != '') {
                return $huddle_role;
            } else {
                if ($account_folder_groups && count($account_folder_groups) > 0) {
                    foreach ($account_folder_groups as $groups) {

                        if (isset($groups['id']) && $groups['id'] == $user_id) {
                            $huddle_role = $groups['role_id'];
                            return $huddle_role;
                        } else {
                            $huddle_role = FALSE;
                        }
                    }
                } else {
                    return FALSE;
                }
            }
        } else {

            return FALSE;
        }
    }
    /*Edit and update video Subtitles*/
    public function updateSubtitles(Request $request){
    $input = $request->all();
    $document_id=$input['document_id'];
    $DocumentSubtitle= DocumentSubtitle::where("id", $input['id'])->update([
        "subtitles" => $input['subtitles'],
        "last_edit_by" => $input['user_id'],
        "last_edit_date" => date("Y-m-d H:i:s")
        ]);

    $DocumentSubtitle= $this->getSubtitlesFromDb($document_id);
    if($DocumentSubtitle){
    $numItems = count($DocumentSubtitle);
    $fileArr=["WEBVTT\n\n\n"];
    $i = 0;
    foreach ($DocumentSubtitle as $title){
        $fileArr[] ="$title->time_range\n";
        $fileArr[] =(++$i === $numItems)? "- $title->subtitles" : "- $title->subtitles\n\n";
    }

    $vttFilePath = public_path("tempTranscribeFiles/" . $document_id . '_transcribe.vtt');
        file_put_contents($vttFilePath , $fileArr);
        $filesToUpload[] = $vttFilePath ;
        $srtFilePath=$this->createSrtFromVtt($vttFilePath ,$document_id);
        if (!empty($srtFilePath)){
            $filesToUpload[] = $srtFilePath;
            $subtitle_paths = $this->uploadToS3($filesToUpload,$input['video_url']);
            foreach ($filesToUpload as $file) {
                unlink($file);
            }
        }
        $res = array(
                'message' => 'Subtitles updated successfully. Please refresh video player',
                'status' => 'Success'
            );
            return response()->json($res);
       }
       $res = array(
                'messasge' => 'Something get wrong',
                'status' => 'failed'
            );
            return response()->json($res);


//        $file_path='uploads/2936/340843/2020/04/01';
//$client = S3Client::factory(array(
//                    'key' => config('s3.access_key_id'),
//                    'secret' => config('s3.secret_access_key')
//        ));
//$result = $client->getObject([
//    'Bucket'                     => config('s3.bucket_name'),
//    'Key'                        => $file_path . '/562000_transcribe.vtt',
//    'SaveAs' => public_path('tempTranscribeFiles/562000_transcribe.vtt')
//]);
//echo $result['Body']->getUri() . "\n";
////var_dump($result->body);
//print_r($result);
    }

    public function getSubtitlesFromDb($doc_id){
          try {
              $DocumentSubtitle= DocumentSubtitle::where('document_id', $doc_id)->get();
              return $DocumentSubtitle;
          } catch (Exception $ex) {
              $this->error("An error occurred while getting subtitles from database\n".$ex->getMessage());
              return false;
          }
    }
    function createSrtFromVtt($webVttFile, $document_id) {
        $filePath = "";
        try {
            $lines = file($webVttFile);
            $length = count($lines);
            for ($index = 3; $index < $length; $index++) {
                // A line is a timestamp line if the second line above it is an empty line
                if (trim($lines[$index - 2]) === '') {
                    $lines[$index] = str_replace('.', ',', $lines[$index]);
                }
            }

            // Remove 2 first lines of WebVTT format
            unset($lines[0]);
            unset($lines[1]);

            $filePath = public_path("tempTranscribeFiles/" . $document_id . '_transcribe.srt');
            file_put_contents($filePath, implode('', $lines));
        } catch (\Exception $e) {
            $this->error("An error occurred while converting vtt to srt file.");
            Log::error("Transcribe create SRT file error: \n" . $e->getMessage());
            $this->error($e->getMessage());
        }

        return $filePath;;
    }
      function getAccountFolderDocumentDetail($document_id) {

        $result = AccountFolderDocument
                ::where(array(
                    'document_id' => (int) $document_id,
                    'site_id' => $this->site_id
                ))->get()->toArray();

        if ($result) {
            return $result[0];
        } else {
            return FALSE;
        }
    }
    public function uploadToS3($localFiles, $doc_url) {
        $client = new S3Client([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);
        $pathInfo = pathinfo($doc_url);
        $new_paths = [];
        foreach ($localFiles as $localFile) {
            $localPath = pathinfo($localFile);
            $path = $client->putObject(array(
                'Bucket' => config('s3.bucket_name'),
                'Key' => $pathInfo["dirname"] . '/' . $localPath["filename"] . "." . $localPath["extension"],
                'SourceFile' => $localFile
            ));
            $new_paths[$localPath["extension"]] = VideoController::getSecureAmazonUrl($pathInfo["dirname"] . '/' . $localPath["filename"] . "." . $localPath["extension"]);
        }

        return $new_paths;
    }

    public static function check_if_evaluated_participant($huddle_id, $user_id, $check_with_new_flow = 0, $return_with_data = 0) {
        $goal_evidence = (app("Illuminate\Http\Request")->get("goal_evidence") == 1);
        if($goal_evidence)
        {
            return true;
        }
        $site_id = app("Illuminate\Http\Request")->header("site_id");
        $result = AccountFolderUser
                ::where(array(
                    'user_id' => $user_id,
                    'account_folder_id' => $huddle_id,
                    'site_id' => $site_id
                ))->get()->toArray();



        $result_groups = AccountFolderGroup::join('user_groups as ug', 'ug.group_id', '=', 'account_folder_groups.group_id')->where(array(
                    'ug.user_id' => $user_id,
                    'account_folder_groups.account_folder_id' => $huddle_id,
                    'account_folder_groups.site_id' => $site_id
                ))->get()->toArray();
        $object_to_return = null;
        $response = false;

        if (($result && $result[0]['role_id'] == 210) || ($result_groups && $result_groups[0]['role_id'] == 210)) {
            if ($result) {
                $object_to_return = $result[0];
            } else {
                $object_to_return = $result_groups[0];
            }
            $response = true;
        } else {
            if ($result && $result[0]['role_id'] == 200) {
                $object_to_return = $result[0];
                if ($check_with_new_flow) {

                    $response = true;
                } else {

                    $response = false;
                }
            }
        }
        if ($return_with_data) {
            return ["status" => $response, "data" => $object_to_return];
        } else {
            return $response;
        }
    }

    function is_enabled_coach_feedback($account_folder_id, $site_id) {

        $check_data = AccountFolderMetaData
                ::where(array(
                    "account_folder_id" => $account_folder_id,
                    "meta_data_name" => "coach_hud_feedback",
                    "site_id" => $this->site_id
                ))->get()->toArray();

        if (isset($check_data[0]['meta_data_value'])) {
            if ($check_data[0]['meta_data_value'] == '1') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function check_if_evalutor($huddle_id, $user_id) {

        $result = AccountFolderUser
                ::where(array(
                    'user_id' => $user_id,
                    'account_folder_id' => $huddle_id,
                    'site_id' => $this->site_id
                ))->get()->toArray();

        $result_groups = AccountFolderGroup::join('user_groups as ug', 'ug.group_id', '=', 'account_folder_groups.group_id')->where(array(
                    'ug.user_id' => $user_id,
                    'account_folder_groups.account_folder_id' => $huddle_id,
                    'account_folder_groups.site_id' => $this->site_id
                ))->get()->toArray();



        if (($result && $result[0]['role_id'] == 200) || ($result_groups && $result_groups[0]['role_id'] == 200)) {
            return true;
        } else {
            return false;
        }
    }

    function can_assessee_comment($account_folder_id, $site_id) {
        $check_data = AccountFolderMetaData
                ::where(array(
                    "account_folder_id" => $account_folder_id,
                    "meta_data_name" => "can_comment_reply",
                    "site_id" => $this->site_id
                ))->get()->toArray();

        if (isset($check_data[0]['meta_data_value']) && $check_data[0]['meta_data_value'] == '1') {
            return true;
        } else {
            return false;
        }
    }

    function user_activity_logs($data, $l_account_id = '', $l_user_id = '') {


        $data['account_id'] = $l_account_id;
        $data['user_id'] = $l_user_id;
        $data['site_id'] = $this->site_id;

        $data['date_added'] = date('Y-m-d H:i:s');
        $data['app_name'] = UserActivityLog::get_app_name();

        $result = DB::table('user_activity_logs')->insert($data);

        $analyticsHuddlesSummary = new \App\Models\AnalyticsHuddlesSummary;
        $analyticsHuddlesSummary->saveRecord($data['account_id'], $data['account_folder_id'], $data['user_id'], $this->site_id, $data['type']);

        if ($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function save_tag($tags, $comment_id, $ref_id, $account_id, $user_id) {

//        $users = $user_current_account;
//        $user_id = $users['User']['id'];
//        $account_id = $users['accounts']['account_id'];
        $tag = str_replace('# ', '', $tags);
        $taginfos = explode(',', $tag);
        foreach ($taginfos as $info) {

            $get_tag_info = '';


            //  $get_tag_info = $this->gettagbytitle($info, $account_id);


            if (!empty($get_tag_info)) {
                $save_cmt_tag[] = array(
                    "comment_id" => $comment_id,
                    "ref_type" => "1",
                    "ref_id" => $ref_id,
                    "tag_title" => $get_tag_info['tag_title'],
                    "account_tag_id" => $get_tag_info['account_tag_id'],
                    "created_by" => $user_id,
                    "site_id" => $this->site_id,
                    "created_date" => date('y-m-d H:i:s', time())
                );

                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'site_id' => $this->site_id,
                    'desc' => $get_tag_info['tag_title'],
                    'url' => '/' . $comment_id,
                    'type' => '14',
                    'account_id' => $account_id,
                    'account_folder_id' => $get_tag_info['account_tag_id'],
                    'environment_type' => 2
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            } else {
                $save_cmt_tag[] = array(
                    "comment_id" => $comment_id,
                    "ref_type" => "1",
                    "ref_id" => $ref_id,
                    "tag_title" => $info,
                    "account_tag_id" => '',
                    "created_by" => $user_id,
                    "site_id" => $this->site_id,
                    "created_date" => date('y-m-d H:i:s', time())
                );

                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'desc' => $info,
                    'url' => '/' . $comment_id,
                    'type' => '14',
                    'account_id' => $account_id,
                    'site_id' => $this->site_id,
                    'account_folder_id' => '',
                    'environment_type' => 2
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            }
        }
        if (DB::table('account_comment_tags')->insert($save_cmt_tag)) {
            return true;
        } else {
            return false;
        }
    }

    function save_tag_1($tags, $comment_id, $ref_id, $account_id, $user_id) {


        $tag = str_replace('# ', '', $tags);
        $taginfos = explode(',', $tag);
        foreach ($taginfos as $info) {

            $get_tag_info = '';


            $get_tag_info = $this->gettagbytitle($info, $account_id);


            if (!empty($get_tag_info)) {
                $save_cmt_tag[] = array(
                    "comment_id" => $comment_id,
                    'site_id' => $this->site_id,
                    "ref_type" => "2",
                    "ref_id" => $ref_id,
                    "tag_title" => $get_tag_info['tag_title'],
                    "account_tag_id" => $get_tag_info['account_tag_id'],
                    "created_by" => $user_id,
                    "site_id" => $this->site_id,
                    "created_date" => date('y-m-d H:i:s', time())
                );

                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'site_id' => $this->site_id,
                    'desc' => $get_tag_info['tag_title'],
                    'url' => '/' . $comment_id,
                    'type' => '14',
                    'account_id' => $account_id,
                    'account_folder_id' => $get_tag_info['account_tag_id'],
                    'environment_type' => 1
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            } else {
                $save_cmt_tag[] = array(
                    "comment_id" => $comment_id,
                    "ref_type" => "2",
                    "ref_id" => $ref_id,
                    "tag_title" => $info,
                    "created_by" => $user_id,
                    "site_id" => $this->site_id,
                    "created_date" => date('y-m-d H:i:s', time())
                );

                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'site_id' => $this->site_id,
                    'desc' => $info,
                    'url' => '/' . $comment_id,
                    'type' => '14',
                    'account_id' => $account_id,
                    'account_folder_id' => '',
                    'environment_type' => 1
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            }
        }

        $user_detail = User::where(array('id' => $user_id, 'site_id' => $this->site_id))->first();

        if ($user_detail) {

            $user_detail = $user_detail->toArray();
            $this->create_churnzero_event('Custom+Marker+Tagged', $account_id, $user_detail['email']);
        }


        if (DB::table('account_comment_tags')->insert($save_cmt_tag)) {
            return true;
        } else {
            return false;
        }
    }

    public function gettagbytitle($info, $account_id) {

        $tag_code = AccountTag
                ::where(array(
                    "account_id" => $account_id,'site_id' => $this->site_id
                ))->whereRaw("BINARY `tag_title`= ?",[$info])->first();

        if ($tag_code) {
            return $tag_code->toArray();
        } else {
            return [];
        }
    }

    function get_latest_comment($comment_id) {
        $result = Comment::getParentDiscussion($comment_id, $this->site_id);
        return $result;
    }

    function view_page(Request $request) {
        $input = $request->all();

        $video_id = $input['video_id'];

        $huddle_id = $input['huddle_id'];

        $account_id = $input['account_id'];

        $user_id = $input['user_id'];

        $role_id = $input['role_id'];
        $library = $request->has("library") ? $request->get("library") : 0;

        $assessment_summary_check = false;

        $coaching_summary_check = false;

        $performance_level_check = false;

        $coaching_perfomance_level = true;

        $assessment_perfomance_level = true;

        $rubric_check = false;

        $can_comment = true;

        $can_reply = true;

        $can_rate = true;

        $can_dl_edit_delete_copy_video = true;

        $can_crop_video = true;

        $coaching_link = true;

        $get_user_huddle_permission = '';

        $assessment_link = true;

        $coachee_permission = '0';

        $final_array = array();

        $huddle_id_check = AccountFolderDocument::where(array('document_id' => $video_id, 'site_id' => $this->site_id))->first();
        if ($huddle_id_check) {
            $huddle_id_check = $huddle_id_check->toArray();

            $huddle_id_check_id = $huddle_id_check['account_folder_id'];

            if ($huddle_id != $huddle_id_check_id) {
                return array(
                    'message' => TranslationsManager::get_translation('you_dont_have_access_to_this_video', 'flash_messages', $this->site_id),
                    'success' => false
                );
            }
        }


        $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

        if ($video_creater_detail) {
            $video_creater_detail = $video_creater_detail->toArray();
        }
        
        $original_account = true;
        if($account_id != $video_creater_detail['account_id'] )
        {
            $original_account = false;
        }

        $view_count = (int) $video_creater_detail['view_count'];

        // DB::table('documents')->where(['id' => $video_id, 'site_id' => $this->site_id])->update(['view_count' => ($view_count + 1), 'site_id' => $this->site_id]);

        $user_detail = User::where(array('id' => $user_id, 'site_id' => $this->site_id))->first();

        if ($user_detail) {

            $user_detail = $user_detail->toArray();
            //    $this->create_churnzero_event('Huddle+Video+Views', $account_id, $user_detail['email']);    
        }

        $video_data = Document::where(array(
                    'id' => $video_id
                ))->first();

        if ($video_data) {
            $video_data = $video_data->toArray();
        }
        if(!$library)
        {
            if ($video_data['post_rubric_per_video'] == '1') {

                $account_folder_details = AccountFolderDocument::where(array(
                    'document_id' => $video_id,
                    'site_id' => $this->site_id
                ))->first();

                if ($account_folder_details) {
                    $account_folder_details = $account_folder_details->toArray();
                }

                if (!empty($account_folder_details)) {
                    if (!empty($account_folder_details['video_framework_id'])) {
                        $final_array['framework_selected_for_video'] = '1';
                    } else {
                        $final_array['framework_selected_for_video'] = '0';
                    }
                }
            } else {
                $final_array['framework_selected_for_video'] = '1';
            }
        }



        $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

        if (!$video_creater_detail) {
            return array(
                'message' => TranslationsManager::get_translation('you_dont_have_access_to_this_video', 'flash_messages', $this->site_id),
                'success' => false);
        }

        $user_pause_settings = UserAccount::where(array(
                    "user_id" => $user_id, "account_id" => $account_id, "site_id" => $this->site_id
                ))->first()->toArray();
      if($library){
            if(!$user_pause_settings || ($user_pause_settings && $user_pause_settings["permission_access_video_library"] != 1))
            {
                return array(
                    'message' => TranslationsManager::get_translation('you_dont_have_access_to_this_video', 'flash_messages', $this->site_id),
                    'success' => false);
            }
      }          
        


        $get_all_comments = isset($input['get_all_comments']) ? $input['get_all_comments'] : 0;
        $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);
        $get_user_huddle_permission = $this->get_huddle_permissions($huddle_id, $user_id);

        if ((($h_type == '2' || $h_type == '3' ) && $get_user_huddle_permission == '200') || $library) {
            $get_all_comments = 1;
        } else {
            $get_all_comments = 0;
        }
        $comments = $this->get_video_comments_with_replies($video_id, $get_all_comments, $huddle_id, $user_id);

        $final_array['comments'] = $comments;
        $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);

        $final_array['huddle_type'] = 'Collaboration';

        $coachee_permission = AccountFolderMetaData::where(array('account_folder_id' => $huddle_id, "meta_data_name" => "coachee_permission", 'site_id' => $this->site_id))->get()->toArray();
        $coachee_permissions = isset($coachee_permission[0]['meta_data_value']) ? $coachee_permission[0]['meta_data_value'] : "0";


        if ($h_type == '2') {
            $get_user_huddle_permission = $this->get_huddle_permissions($huddle_id, $user_id);
            $coaches_name = AccountFolderUser::getUserNameCoaches($huddle_id, $this->site_id);
            $coachee_name = AccountFolderUser::getUserNameMente($huddle_id, $this->site_id);
            $final_array['coaches_name'] = $coaches_name;
            $final_array['coachee_name'] = $coachee_name;
            $final_array['huddle_type'] = 'Coaching';

            $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

            if ($video_creater_detail) {
                $video_creater_detail = $video_creater_detail->toArray();
            }

            if (( ($get_user_huddle_permission == '210' || $get_user_huddle_permission == '220') && !$coachee_permissions && $user_id == $video_creater_detail['created_by'] ) || (($get_user_huddle_permission == '210' || $get_user_huddle_permission == '220') && $user_id != $video_creater_detail['created_by'] ) ) {
                $can_dl_edit_delete_copy_video = false;
            }

            $coaching_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'coaching_perfomance_level', 'site_id' => $this->site_id))->first();
            if ($coaching_perfomance_level) {
                $coaching_perfomance_level = $coaching_perfomance_level->toArray();
            }
            $coaching_perfomance_level = isset($coaching_perfomance_level['meta_data_value']) ? $coaching_perfomance_level['meta_data_value'] : "0";


            $coachee_permission = AccountFolderMetaData::where(array('account_folder_id' => $huddle_id, 'meta_data_name' => 'coachee_permission', 'site_id' => $this->site_id))->first();
            if ($coachee_permission) {
                $coachee_permission = $coachee_permission->toArray();
            }
            $coachee_permission = isset($coachee_permission['meta_data_value']) ? $coachee_permission['meta_data_value'] : "0";

            $assessment_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'assessment_perfomance_level', 'site_id' => $this->site_id))->first();
            if ($assessment_perfomance_level) {
                $assessment_perfomance_level = $assessment_perfomance_level->toArray();
            }
            $assessment_perfomance_level = isset($assessment_perfomance_level['meta_data_value']) ? $assessment_perfomance_level['meta_data_value'] : "0";
        } elseif ($h_type == '3') {
            $get_user_huddle_permission = $this->get_huddle_permissions($huddle_id, $user_id);
            $assessor_names = AccountFolderUser::getUsernameEvaluator($this->site_id, $huddle_id);
            if ($get_user_huddle_permission == '200') {
                $eval_participant_names = AccountFolderUser::getParticipants($this->site_id, $huddle_id);
            } else {
                $eval_participant_names = AccountFolderUser::getParticipants($this->site_id, $huddle_id, $user_id);
            }
            $final_array['assessor_names'] = $assessor_names;

            $final_array['eval_participant_names'] = $eval_participant_names;
            $final_array['huddle_type'] = 'Assessment';
            if ($this->check_if_evaluated_participant($huddle_id, $user_id, $this->site_id) == 1) {
                $get_eval_participant_videos = Document::get_eval_participant_videos($huddle_id, $user_id, $this->site_id);
                $submission_allowed_count = Document::get_submission_allowed($huddle_id, $this->site_id);
                $submission_date = $this->get_submission_date($huddle_id);
                $is_date_expired = false;
                if ($submission_date != '') {
                    $submission_string = strtotime($submission_date);
                    $current_time = strtotime(date('Y-m-d h:i:s a', time()));
                    if ($submission_string < $current_time) {
                        $is_date_expired = true;
                    }
                }
                if ($get_eval_participant_videos >= $submission_allowed_count || $is_date_expired == true) {
                    $can_crop_video = false;
                }
                if ($get_user_huddle_permission == 210 && $user_id != $video_creater_detail['created_by']) {
                    $can_dl_edit_delete_copy_video = false;
                }
            } else {
                if ($get_user_huddle_permission == 210 && $user_id != $video_creater_detail['created_by']) {
                    $can_dl_edit_delete_copy_video = false;
                }
            }
        }
        $huddle_detail = AccountFolder::where(array('account_folder_id' => $huddle_id, 'site_id' => $this->site_id))->first();

        $has_access_to_huddle = false;
        $p_huddle_users = AccountFolder::getHuddleUsers($huddle_id, $this->site_id);
        $p_huddle_group_users = AccountFolder::getHuddleGroupsWithUsers($huddle_id, $this->site_id);
        if ($p_huddle_users) {

            for ($i = 0; $i < count($p_huddle_users); $i++) {
                $p_huddle_user = $p_huddle_users[$i];
                if ($p_huddle_user['user_id'] == $user_id) {
                    $has_access_to_huddle = true;
                }
            }
        }
        if ($p_huddle_group_users) {
            for ($i = 0; $i < count($p_huddle_group_users); $i++) {
                $p_huddle_group_user = $p_huddle_group_users[$i];
                if (isset($p_huddle_group_user['id']) && $p_huddle_group_user['id'] == $user_id)
                    $has_access_to_huddle = true;
            }
        }

        if (!$has_access_to_huddle && !$library && (isset($input['goal_evidence']) && ($input['goal_evidence'] == 'false' ||  $input['goal_evidence'] == false)) ) {
            return array(
                'message' => TranslationsManager::get_translation('you_dont_have_access_to_this_huddle', 'flash_messages', $this->site_id),
                'success' => false);
        }
        if ($h_type == '3') {

            $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

            if ($video_creater_detail) {
                $video_creater_detail = $video_creater_detail->toArray();
            }

            $isEditable = $this->_check_evaluator_permissions($huddle_id, $video_creater_detail['created_by'], $user_id, $role_id);
            if ($isEditable == false) {
                return array(
                    'message' => TranslationsManager::get_translation('you_dont_have_access_to_this_video', 'flash_messages', $this->site_id),
                    'success' => false);
            }
        }



        if ($huddle_detail) {
            $huddle_detail = $huddle_detail->toArray();
        }
        $video_detail_coachee = '';
        $user_huddle_level_permissions = $this->get_huddle_permissions($huddle_id, $user_id);

        $video_data = AccountFolder::getHuddleUsers($huddle_id, $this->site_id);

        $tracker_enable = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'enable_tracker', 'site_id' => $this->site_id))->first();
        if ($tracker_enable) {
            $tracker_enable = $tracker_enable->toArray();
        }

        $enable_tracker = isset($tracker_enable['meta_data_value']) ? $tracker_enable['meta_data_value'] : "0";



        $main_tracker_enable = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'enabletracking', 'site_id' => $this->site_id))->first();
        if ($main_tracker_enable) {
            $main_tracker_enable = $main_tracker_enable->toArray();
        }

        $main_enable_check = isset($main_tracker_enable['meta_data_value']) ? $main_tracker_enable['meta_data_value'] : "0";

        $enable_assessment = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'enable_matric', 'site_id' => $this->site_id))->first();
        if ($enable_assessment) {
            $enable_assessment = $enable_assessment->toArray();
        }

        $enable_tracker_check = isset($enable_assessment['meta_data_value']) ? $enable_assessment['meta_data_value'] : "0";

        foreach ($video_data as $hudd_user) {
            if ($hudd_user['role_id'] == 210) {
                $video_detail_coachee = $hudd_user['user_id'];
            }
        }


        $can_view_summary = AccountFolderMetaData::where(array(
                    'account_folder_id' => $huddle_id, 'meta_data_name' => 'coachee_can_view_summary', 'site_id' => $this->site_id
                ))->get()->toArray();

        $can_view_summary_value = isset($can_view_summary[0]['meta_data_value']) ? $can_view_summary[0]['meta_data_value'] : "0";


        if (($user_huddle_level_permissions == 210 && $can_view_summary_value == '1' ) || $user_huddle_level_permissions == 200 || ( ($user_id == $huddle_detail['created_by']) && $input['permission_maintain_folders'] == '1')) {

            if ($h_type == 2) {
                if ($enable_tracker == 1 && !empty($video_detail_coachee)) {
                    $coaching_summary_check = true;
                }
            }

            $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

            if ($video_creater_detail) {
                $video_creater_detail = $video_creater_detail->toArray();
            }

            if ($h_type == 3) {
                if ($enable_tracker_check && !empty($video_detail_coachee)) {
                    // if ($this->check_if_evaluated_participant($huddle_id, $video_creater_detail['created_by'])) {
                    $assessment_summary_check = true;
                    // }
                }
            }
        }

        $id_framework = AccountFolderMetaData::where(array(
                    'account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id', 'site_id' => $this->site_id
                ))->get()->toArray();


        $framework_id = isset($id_framework[0]['meta_data_value']) ? $id_framework[0]['meta_data_value'] : "0";


        if ($framework_id == -1 || $framework_id == 0) {

            $id_framework = AccountMetaData::where(array(
                        'account_id' => $account_id, 'meta_data_name' => 'default_framework', 'site_id' => $this->site_id
                    ))->get()->toArray();
            $framework_id = isset($id_framework[0]['meta_data_value']) ? $id_framework[0]['meta_data_value'] : "0";
        }

        $account_framework_settings = AccountFrameworkSetting::select('AccountFrameworkSetting.id', 'AccountFrameworkSetting.account_id', 'AccountFrameworkSetting.account_tag_id', 'AccountFrameworkSetting.published_at', 'AccountFrameworkSetting.updated_at', 'AccountFrameworkSetting.published', 'AccountFrameworkSetting.framework_name', 'AccountFrameworkSetting.enable_unique_desc', 'AccountFrameworkSetting.enable_ascending_order', 'AccountFrameworkSetting.enable_performance_level', 'AccountFrameworkSetting.tier_level', 'AccountFrameworkSetting.checkbox_level', 'AccountFrameworkSetting.parent_child_share')->where(array(
                    'AccountFrameworkSetting.account_tag_id' => $framework_id,
                    'AccountFrameworkSetting.site_id' => $this->site_id,
                ))->first();

        if ($account_framework_settings) {
            $account_framework_settings = $account_framework_settings->toArray();
        }



        if (( (isset($account_framework_settings['enable_performance_level']) && !empty($account_framework_settings['enable_performance_level'])) || ( $main_enable_check && (($enable_tracker_check && $h_type == 3 ) || ($enable_tracker && $h_type == 2 ) ) ) ) && ( ($h_type == 3 && ($user_huddle_level_permissions == 200 || $user_huddle_level_permissions == 210 ) ) || ($this->coaching_perfomance_level($account_id, $h_type, $user_huddle_level_permissions)))) {
            $performance_level_check = true;
        }

        if ($this->is_enabled_framework_and_standards($account_id)) {
            if ($this->is_enabled_huddle_framework_and_standards($huddle_id)) {
                $rubric_check = true;
            }
        }

        if ($h_type == 3 && ($user_huddle_level_permissions == 210 || $user_huddle_level_permissions == 220 )) {
            $can_comment = false;

            $can_comment_reply = AccountFolderMetaData::where(array(
                        'account_folder_id' => $huddle_id, 'meta_data_name' => 'can_comment_reply', 'site_id' => $this->site_id
                    ))->get()->toArray();

            $can_comment_reply_value = isset($can_comment_reply[0]['meta_data_value']) ? $can_comment_reply[0]['meta_data_value'] : "0";

            if ($can_comment_reply_value != "0" && $video_creater_detail['created_by'] == $user_id) {
                $can_comment = true;
            }

            if ($can_comment_reply_value == "0" && $video_creater_detail['created_by'] == $user_id && $user_huddle_level_permissions == 210) {
                $can_reply = false;
            } else {
                $can_reply = true;
            }

            $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

            if ($video_creater_detail) {
                $video_creater_detail = $video_creater_detail->toArray();
            }

            if ($video_creater_detail['created_by'] != $user_id) {
                $can_reply = false;
            }
        }

        if ($user_huddle_level_permissions != 200) {
            $can_rate = false;
        }

        if ($h_type != 2 && $h_type != 3 && $user_huddle_level_permissions == 220) {
            $can_comment = false;
            $can_reply = false;
            $can_dl_edit_delete_copy_video = false;
        }

        if ($h_type == 1) {
            if (($get_user_huddle_permission == 210 || $get_user_huddle_permission == 220 ) && $user_id != $video_creater_detail['created_by']) {
                $can_dl_edit_delete_copy_video = false;
            }
        }

        if ($h_type != '3') {
            $allow_per_video_details = AccountFolderMetaData::where(array('account_folder_id' => $huddle_id, "meta_data_name" => "allow_per_video_to_coachee", 'site_id' => $this->site_id))->get()->toArray();
            $allow_per_video_check = isset($allow_per_video_details[0]['meta_data_value']) ? $allow_per_video_details[0]['meta_data_value'] : "0";

            $final_array['allow_per_video'] = $allow_per_video_check;
        }


        $huddle_video_marker_tags = AccountFolderMetaData::where(array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_tags', 'site_id' => $this->site_id))->first();
        if ($huddle_video_marker_tags) {
            $huddle_video_marker_tags = $huddle_video_marker_tags->toArray();
        }
        $huddle_custom_markers_permission = isset($huddle_video_marker_tags['meta_data_value']) ? $huddle_video_marker_tags['meta_data_value'] : "0";

        //SW-4770
        $assessment_huddle_publish_all = AccountFolderMetaData::where(array('account_folder_id' => $huddle_id, 'meta_data_name' => 'publish_all', 'site_id' => $this->site_id))->first();
        if ($assessment_huddle_publish_all) {
            $assessment_huddle_publish_all = $assessment_huddle_publish_all->toArray();
        }
        $assessment_huddle_publish_all = isset($assessment_huddle_publish_all['meta_data_value']) ? $assessment_huddle_publish_all['meta_data_value'] : false;

        $account_custom_markers_permission = 0;
        $video_marker_tags = AccountFolderMetaData::where(array(
                    "account_folder_id" => $account_id,
                    "site_id" => $this->site_id,
                    "meta_data_name" => "enable_tags"
                ))->first();

        if ($video_marker_tags) {
            $video_marker_tags = $video_marker_tags->toArray();
        }

        if (isset($video_marker_tags['meta_data_value']) && $video_marker_tags['meta_data_value'] == '1') {
            $account_custom_markers_permission = 1;
        } else {
            $account_custom_markers_permission = 0;
        }

        if ($account_custom_markers_permission == '1' && $huddle_custom_markers_permission == '1') {
            $final_array['video_markers_check'] = '1';
        } else {
            $final_array['video_markers_check'] = '0';
        }


        if ($h_type == '3' && ($user_huddle_level_permissions == 210 || $user_huddle_level_permissions == 220 )) {
            $final_array['video_markers_check'] = '0';
        }

        if ($user_huddle_level_permissions == 210 && $can_view_summary_value == '0') {
            $assessment_summary_check = false;
            $coaching_summary_check = false;
        }


        $get_account_video_permissions = $this->get_account_video_permissions($account_id);
        $can_download = 1;
        $can_share = 1;
        if($library)
        {
            $can_view_summary_value = 0;
            $can_dl_edit_delete_copy_video = 0;
            $can_rate = 0;
            $can_comment = 0;
            $can_reply = 0;
            $can_crop_video = 0;
            $assessment_summary_check = 0;
            $coaching_summary_check = 0;
            $performance_level_check = 0;
            $rubric_check = 0;
            $can_share = 1;
            $can_download = 0;
            $h_type = 0;
            
            if($user_pause_settings["permission_video_library_upload"])
            {
                $can_dl_edit_delete_copy_video = 1;
                $can_crop_video = 1;
                $can_share = 1;
            }
            if($user_pause_settings["permission_video_library_comments"])
            {
                $can_comment = 1;
                $can_reply = 1;
            }
            if($user_pause_settings["permission_video_library_download"])
            {
                $can_download = 1;
            }
            if($role_id == 100 || $role_id == 110)
            {
                $can_comment = 1;
                $can_reply = 1;
                $can_dl_edit_delete_copy_video = 1;
                $can_crop_video = 1;
                $can_download = 1;
                $can_share = 1;
            }
            if($role_id == 125 || ($role_id == 120 && $user_pause_settings['permission_share_library'] != '1' ) || ($role_id == 115 && $user_pause_settings['permission_share_library'] != '1' ) )
            {
                $can_share = 0;
            }
            $c_request = new Request(["account_folder_id"=>$huddle_id, "account_id"=> $account_id]);
            $final_array['categories'] = app("App\Http\Controllers\VideoLibraryController")->get_video_categories($c_request, 1);
        }

        $final_array['publish_all'] = $assessment_huddle_publish_all;
       
        $final_array['get_account_video_library_permissions'] = $get_account_video_permissions;

        $final_array['can_view_summary'] = $can_view_summary_value;

        $final_array['huddle_info'] = $huddle_detail;

        $final_array['can_dl_edit_delete_copy_video'] = $can_dl_edit_delete_copy_video;

        $final_array['can_crop_video'] = $can_crop_video;

        $final_array['can_comment'] = $can_comment;

        $final_array['can_rate'] = $can_rate;

        $final_array['can_reply'] = $can_reply;
        $final_array['can_download'] = $can_download;
        $final_array['can_share'] = $can_share;

        $final_array['h_type'] = $h_type;

        $final_array['assessment_summary_check'] = $assessment_summary_check;

        $final_array['coaching_summary_check'] = $coaching_summary_check;

        $final_array['performance_level_check'] = $performance_level_check;

        $final_array['rubric_check'] = $rubric_check;
        
        $final_array['original_account'] = $original_account;

        $final_array['coachee_permission'] = $coachee_permission;

        $video_detail = $this->getVideoDetails($video_id);

        if (isset($video_detail['url'])) {
            $videoFilePath = pathinfo($video_detail['url']);
        } else {
            $videoFilePath = "";
        }

        if (isset($videoFilePath['extension'])) {
            $video_detail['file_type'] = $videoFilePath['extension'];
        }

        $documentfiles = DocumentFiles::where(array(
                    "document_id" => $video_id
                ))->first();

        if ($documentfiles) {
            $documentfiles->toArray();
            $video_detail['transcoding_status'] = $documentfiles['transcoding_status'];
        }

        $video_url = $this->get_document_url($video_detail);
        $video_detail['account_folder_id'] = $huddle_id;
        $video_detail['role_id'] = $user_huddle_level_permissions;
        if(!empty($video_detail["subtitle_available"])){
            if ($video_detail["subtitle_available"] == 1) {
               $video_detail["subtitle_vtt"] = self::getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($video_detail["id"]) . "_transcribe.vtt");
               $video_detail["subtitle_srt"] = self::getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($video_detail["id"]) . "_transcribe.srt");
//            $video_detail["subtitles"] = $this->getSubtitlesFromDb($video_detail["id"]);
        }
    }
        $userPermissions = AccountFolderUser::where('user_id', $user_id)->where('account_folder_id', $huddle_id)->first();
        $is_coach = false;
        if ($userPermissions) {
            $userPermissions= $userPermissions->toArray();
            if ($userPermissions['role_id'] != '' && $userPermissions['role_id'] ==200) {
                $is_coach = true;
            }
        }
        $video_detail["created_date_string"] = !empty($video_detail['created_date'])?date('D d, Y', strtotime($video_detail['created_date'])):'';
        $video_detail['account_folder_id'] = $huddle_id;
        $video_detail['coaching_feedback_permission'] = $this->is_enabled_coach_feedback($huddle_id,$this->site_id);
        $video_detail['is_coach'] = $is_coach;
        $final_array['video_detail'] = $video_detail;
        if($video_detail && !empty($video_detail['parent_folder_id']))
        {
            $final_array['breadcumbs'] = DocumentFolder::getNLevelParents($video_detail['parent_folder_id']);
        }

        $custom_markers = $this->getmarkertags($account_id, '1');

        $final_array['custom_markers'] = $custom_markers;

        $final_array['static_url'] = $video_url['url'];
        $final_array['thubnail_url'] = $video_url['thumbnail'];
        $comments_counts = $this->get_comments_count($video_id, $user_id);
        $attached_document_numbers = Document::getVideoAttachmentNumbers($video_id, $this->site_id);

        if (($h_type == '3') && ( $get_user_huddle_permission != 200 || (!$main_enable_check || !$enable_tracker_check ) )) {
            $assessment_link = false;
            $coaching_link = false;
        }
        if (($h_type == '2') && ( $get_user_huddle_permission != 200 || (!$main_enable_check || !$enable_tracker ) )) {
            $assessment_link = false;
            $coaching_link = false;
        }

        $id_framework = AccountFolderMetaData::where(array(
                    'account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id', 'site_id' => $this->site_id
                ))->first();

        if ($id_framework) {
            $id_framework->toArray();
        }

        $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";


        if ($framework_id == -1 || $framework_id == 0) {

            $id_framework = AccountMetaData::where(array(
                        'account_id' => $account_id, 'meta_data_name' => 'default_framework', 'site_id' => $this->site_id
                    ))->first();


            if ($id_framework) {
                $id_framework->toArray();
            }

            $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";
        }


        $user_activity_logs = array(
            'ref_id' => $video_id,
            'site_id' => $this->site_id,
            'desc' => 'Video Viewed',
            'url' => '/' . $video_id,
            'type' => '11',
            'account_id' => $account_id,
            'account_folder_id' => $huddle_id,
            'environment_type' => 2,
        );
        // $this->user_activity_logs($user_activity_logs, $account_id, $user_id);

        $duration = DocumentFiles::where(array(
                    "document_id" => $video_id
                ))->whereRaw("url like '%".$account_id."%'")->first();

        if ($duration) {
            $duration->toArray();
            $final_array['video_duration'] = $duration['duration'];
            $final_array['video_detail']['video_duration'] = $duration['duration'];
        } else {
            $final_array['video_duration'] = '0';
        }

        $coaching_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'coaching_perfomance_level', 'site_id' => $this->site_id))->first();
        if ($coaching_perfomance_level) {
            $coaching_perfomance_level = $coaching_perfomance_level->toArray();
        }
        $coaching_perfomance_level = isset($coaching_perfomance_level['meta_data_value']) ? $coaching_perfomance_level['meta_data_value'] : "0";


        $assessment_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'assessment_perfomance_level', 'site_id' => $this->site_id))->first();
        if ($assessment_perfomance_level) {
            $assessment_perfomance_level = $assessment_perfomance_level->toArray();
        }
        $assessment_perfomance_level = isset($assessment_perfomance_level['meta_data_value']) ? $assessment_perfomance_level['meta_data_value'] : "0";
        $used_slot = [];
        $huddleController = app('App\Http\Controllers\HuddleController');
        $streaming_auth_details = $huddleController->getStreamingPlatformDetails($account_id);
        $original_file_name = !empty($video_detail['original_file_name'])?$video_detail['original_file_name']:'';
        $stream_name = str_replace(".mp4","",$original_file_name);
        $stream_url = "https://viewer.millicast.com/v2?streamId=".$streaming_auth_details["username"]."/".$stream_name;
        //$stream_url = "https://viewer.millicast.com/v2?streamId=M8Reue/Saad";
        $streamer_url = "";
        if(isset($video_detail['doc_type']) && $video_detail['doc_type'] == 4 && $streaming_auth_details["id"] == $huddleController->platform_wowza)
        {
            $used_slot = DB::select("SELECT 
                                              LC.*, CSS.id as slot_id, CSS.stream_name, CSS.connection_counts
                                            FROM
                                              live_clusters AS LC 
                                              JOIN cluster_stream_slots AS CSS 
                                                ON LC.id = CSS.cluster_id 
                                            WHERE LC.is_active = 1
                                              AND LC.site_id = $this->site_id 
                                              AND CSS.is_reserved = 1
                                              AND CSS.current_stream_id = $video_id
                                              AND (LC.account_id = $account_id OR LC.account_id IS NULL)
                                            ORDER BY LC.account_id DESC 
                                            LIMIT 1");


            if(isset($used_slot[0]))
            {
                $stream_url = $this->getSecureStreamUrl($request,1,$used_slot[0]->streamer_url,$used_slot[0]->stream_name);
                $stream_name = $used_slot[0]->stream_name;
                $streamer_url = $used_slot[0]->streamer_url;
            }
        }

        if(isset($video_detail['doc_type']) && $video_detail['doc_type'] == 4)
        {
            $screenLog = LiveStreamController::checkScreenSharingInternal($video_id, $this->site_id);
            $final_array['is_screen_sharing'] = $screenLog['is_sharing'];
            $final_array['screen_stream_log'] = $screenLog['log'];
        }
        $final_array['default_framework'] = $framework_id;
        $final_array['coaching_link'] = $coaching_link;
        $final_array['assessment_link'] = $assessment_link;

        $final_array['comments_counts'] = $comments_counts;
        $final_array['coaching_perfomance_level'] = $coaching_perfomance_level;
        $final_array['assessment_perfomance_level'] = $assessment_perfomance_level;

        $final_array['user_pause_settings'] = $user_pause_settings['type_pause'];
        $final_array['attached_document_numbers'] = $attached_document_numbers;
        $final_array['bread_crumb_output'] = $this->bread_crumb_output($huddle_id);
        $final_array['user_huddle_level_permissions'] = $user_huddle_level_permissions;
        $final_array['streamer_url'] = $streamer_url;
        $final_array['stream_name'] = $stream_name;
        $final_array['stream_url'] = $stream_url;
        $final_array['streaming_auth_details'] = $streaming_auth_details;
        $final_array['video_detail']['role_id'] =  isset($userPermissions['role_id']) ? $userPermissions['role_id'] : null;
        $final_array['coaching_feedback_permission'] = $this->is_enabled_coach_feedback($huddle_id,$this->site_id);
        $final_array['success'] = true;



        return $final_array;
    }

    function update_view_count(Request $request) {
        $input = $request->all();
        $video_id = $input['video_id'];
        $huddle_id = $input['huddle_id'];
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];

        $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

        if ($video_creater_detail) {
            $video_creater_detail = $video_creater_detail->toArray();
            $account_id = $video_creater_detail['account_id'];
        }

        $view_count = (int) $video_creater_detail['view_count'];

        DB::table('documents')->where(['id' => $video_id, 'site_id' => $this->site_id])->update(['view_count' => ($view_count + 1), 'site_id' => $this->site_id]);


        $user_detail = User::where(array('id' => $user_id, 'site_id' => $this->site_id))->first();

        $huddle_detail = AccountFolder::where(array('account_folder_id' => $huddle_id, 'site_id' => $this->site_id))->first();

        if ($user_detail) {

            $user_detail = $user_detail->toArray();
            if ($huddle_detail['folder_type'] == '1') {
                $this->create_churnzero_event('Huddle+Video+Views', $account_id, $user_detail['email']);
            } else {
                $this->create_churnzero_event('Workspace+Video+Views', $account_id, $user_detail['email']);
            }
        }

        $user_activity_logs = array(
            'ref_id' => $video_id,
            'site_id' => $this->site_id,
            'desc' => 'Video Viewed',
            'url' => '/' . $video_id,
            'type' => '11',
            'account_id' => $account_id,
            'account_folder_id' => $huddle_id,
            'environment_type' => 2,
        );
        $this->user_activity_logs($user_activity_logs, $account_id, $user_id);

        return ['success' => true];
    }

    /*
      function workspace_video_page(Request $request) {
      $video_id = $request->video_id;
      $huddle_id = $request->huddle_id;
      $account_id = $request->account_id;
      $user_id = $request->user_id;
      $role_id = $request->role_id;
      $get_all_comments = $request->get_all_comments;
      $permission_maintain_folders = $request->permission_maintain_folders;

      $videos = new \App\Services\Workspace\Videos();
      $video = $videos->getVideoDetail($video_id, $huddle_id, $account_id, $user_id, $role_id, $permission_maintain_folders);

      return response()->json($video);
      }
     */

    function workspace_video_page(Request $request) {
        $video_id = $request->video_id;
        $huddle_id = $request->huddle_id;
        $account_id = $request->account_id;
        $user_id = $request->user_id;
        $input = $request->all();
        $rubric_check = false;
        $final_array = array();

        $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

        if (!$video_creater_detail) {
            return array('message' => TranslationsManager::get_translation('you_dont_have_access_to_this_video', 'flash_messages', $this->site_id),
                'success' => false);
        }

        if ($video_creater_detail) {
            if (!empty($video_creater_detail->created_by) && $video_creater_detail->created_by != $user_id && (!isset($input['goal_evidence']) || (isset($input['goal_evidence']) && $input['goal_evidence'] == 'false' )) ) {
                return array('message' => TranslationsManager::get_translation('you_dont_have_access_to_this_video', 'flash_messages', $this->site_id),
                    'success' => false);
            }
        }

        $user_settings = UserAccount::where(array(
                    "user_id" => $user_id, "account_id" => $account_id, "site_id" => $this->site_id
                ))->first()->toArray();

        $comments = $this->get_video_comments_with_replies($video_id, 0, $huddle_id, $user_id);

        $final_array['comments'] = $comments;
/// Check
        $huddle_detail = AccountFolder::where(array('account_folder_id' => $huddle_id, 'site_id' => $this->site_id))->first();

        if ($huddle_detail) {
            $huddle_detail = $huddle_detail->toArray();
        }

        $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

        if ($video_creater_detail) {
            $video_creater_detail = $video_creater_detail->toArray();
        }

        $view_count = (int) $video_creater_detail['view_count'];

        // DB::table('documents')->where(['id' => $video_id, 'site_id' => $this->site_id])->update(['view_count' => ($view_count + 1), 'site_id' => $this->site_id]);

        $user_detail = User::where(array('id' => $user_id, 'site_id' => $this->site_id))->first();

        if ($user_detail) {

            $user_detail = $user_detail->toArray();
            // $this->create_churnzero_event('Workspace+Video+Views', $account_id, $user_detail['email']);
        }


        if ($this->is_enabled_framework_and_standards_ws($account_id)) {
            $rubric_check = true;
        }

        $video_data = Document::where(array(
                    'id' => $video_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($video_data) {
            $video_data = $video_data->toArray();
        }

        if ($video_data['post_rubric_per_video'] == '1') {

            $account_folder_details = AccountFolderDocument::where(array(
                        'account_folder_id' => $huddle_id,
                        'document_id' => $video_id,
                        'site_id' => $this->site_id
                    ))->first();

            if ($account_folder_details) {
                $account_folder_details = $account_folder_details->toArray();
            }

            if (!empty($account_folder_details)) {
                if (!empty($account_folder_details['video_framework_id'])) {
                    $final_array['framework_selected_for_video'] = '1';
                } else {
                    $final_array['framework_selected_for_video'] = '0';
                }
            }
        } else {
            $final_array['framework_selected_for_video'] = '1';
        }

        $huddle_video_marker_tags = AccountFolderMetaData::where(array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_tags', 'site_id' => $this->site_id))->first();
        if ($huddle_video_marker_tags) {
            $huddle_video_marker_tags = $huddle_video_marker_tags->toArray();
        }
        $huddle_custom_markers_permission = isset($huddle_video_marker_tags['meta_data_value']) ? $huddle_video_marker_tags['meta_data_value'] : "0";

        $account_custom_markers_permission = 0;
        $video_marker_tags = AccountFolderMetaData::where(array(
                    "account_folder_id" => $account_id,
                    "meta_data_name" => "enable_tags_ws",
                    'site_id' => $this->site_id
                ))->first();

        if ($video_marker_tags) {
            $video_marker_tags = $video_marker_tags->toArray();
        }

        $video_marker_tags_ac = AccountFolderMetaData::where(array(
                    "account_folder_id" => $account_id,
                    "meta_data_name" => "enable_tags",
                    'site_id' => $this->site_id
                ))->first();

        if ($video_marker_tags_ac) {
            $video_marker_tags_ac = $video_marker_tags_ac->toArray();
        }

        if (isset($video_marker_tags_ac['meta_data_value']) && $video_marker_tags_ac['meta_data_value'] == '1' && isset($video_marker_tags['meta_data_value']) && $video_marker_tags['meta_data_value'] == '1') {
            $account_custom_markers_permission = 1;
        } else {
            $account_custom_markers_permission = 0;
        }

        if ($account_custom_markers_permission == '1') {
            $final_array['video_markers_check'] = '1';
        } else {
            $final_array['video_markers_check'] = '0';
        }
        $framework_id = -1;
        if (isset($final_array['framework_selected_for_video']) && $final_array['framework_selected_for_video'] == 1) {
            $account_folder_document_details = AccountFolderDocument::where(array(
                        'account_folder_id' => $huddle_id,
                        'site_id' => $this->site_id,
                        'document_id' => $video_id
                    ))->first();

            if ($account_folder_document_details) {
                $account_folder_document_details = $account_folder_document_details->toArray();
            }
            $framework_id = $account_folder_document_details['video_framework_id'];
        }

        $framework_id_ws = -1;
        if ($framework_id == -1 || $framework_id == 0) {

            $id_framework = AccountMetaData::where(array(
                        'account_id' => $account_id, 'meta_data_name' => 'default_framework', 'site_id' => $this->site_id
                    ))->first();


            if ($id_framework) {
                $id_framework->toArray();
            }

            $framework_id_ws = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";
        }


        $final_array['account_framework'] = $framework_id_ws;
        $final_array['default_framework'] = $framework_id;

        $final_array['rubric_check'] = $rubric_check;

        $final_array['huddle_info'] = $huddle_detail;

        $video_detail = $this->getVideoDetails($video_id);

        $videoFilePath =$video_detail ? pathinfo($video_detail['url']) : '';

        if (isset($videoFilePath['extension'])) {
            $video_detail['file_type'] = $videoFilePath['extension'];
        }

        $documentfiles = DocumentFiles::where(array(
                    "document_id" => $video_id
                ))->first();

        if ($documentfiles) {
            $documentfiles->toArray();
            $video_detail['transcoding_status'] = $documentfiles['transcoding_status'];
        }

        $video_url = $this->get_document_url($video_detail);

        if ($video_detail && $video_detail["subtitle_available"] == 1) {
            $video_detail["subtitle_vtt"] = self::getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($video_detail["id"]) . "_transcribe.vtt");
            $video_detail["subtitle_srt"] = self::getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($video_detail["id"]) . "_transcribe.srt");
//            $video_detail["subtitles"] = $this->getSubtitlesFromDb($video_detail["id"]);
        }
        if($video_detail && !empty($video_detail['parent_folder_id']))
        {
            $final_array['breadcumbs'] = DocumentFolder::getNLevelParents($video_detail['parent_folder_id']);
        }
        $final_array['video_detail'] = $video_detail;

        $custom_markers = $this->getmarkertags($account_id, '1');

        $final_array['custom_markers'] = $custom_markers;

        $final_array['static_url'] = $video_url['url'];

        $comments_counts = $this->get_comments_count($video_id, $user_id);
        $attached_document_numbers = Document::getVideoAttachmentNumbers($video_id, $this->site_id);

        $user_activity_logs = array(
            'ref_id' => $video_id,
            'site_id' => $this->site_id,
            'desc' => 'Video Viewed',
            'url' => '/' . $video_id,
            'type' => '11',
            'account_id' => $account_id,
            'account_folder_id' => $huddle_id,
            'environment_type' => 2,
        );
        //  $this->user_activity_logs($user_activity_logs, $account_id, $user_id);

        $get_account_video_permissions = $this->get_account_video_permissions($account_id);

        $duration = DocumentFiles::where(array(
                    "document_id" => $video_id
                ))->whereRaw("url like '%".$account_id."%'")->first();

        if ($duration) {
            $duration->toArray();
            $final_array['video_duration'] = $duration['duration'];
            $final_array['video_detail']['video_duration'] = $duration['duration'];
        } else {
            $final_array['video_duration'] = '0';
        }


        $final_array['get_account_video_library_permissions'] = $get_account_video_permissions;

        $final_array['comments_counts'] = $comments_counts;
        $final_array['user_pause_settings'] = $user_settings['type_pause'];
        $final_array['user_autoscroll_settings'] = $user_settings['autoscroll_switch'];
        $final_array['attached_document_numbers'] = $attached_document_numbers;
        $final_array['account_folder_id'] = $huddle_id;
        $coaching_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'coaching_perfomance_level', 'site_id' => $this->site_id))->first();
        if ($coaching_perfomance_level) {
            $coaching_perfomance_level = $coaching_perfomance_level->toArray();
        }
        $coaching_perfomance_level = isset($coaching_perfomance_level['meta_data_value']) ? $coaching_perfomance_level['meta_data_value'] : "0";
        $final_array['coaching_perfomance_level'] = $coaching_perfomance_level;
        $final_array['success'] = true;
        $user_permissions = UserAccount::where(array('user_id' => $user_id , 'account_id' => $account_id))->first();
        $final_array['allow_evidence_view'] = 0;
        if($video_creater_detail['created_by'] != $user_id)
        {
            $final_array['allow_evidence_view'] = 1;
        }
        if($user_permissions['role_id'] == 120 && $user_permissions['parmission_access_my_workspace'] == '0' || ($final_array['allow_evidence_view'] == 1) )
        {
            $final_array['success'] = false;
        }

        return response()->json($final_array);
    }

    function view_page_observation(Request $request) {
        $input = $request->all();

        $video_id = $input['video_id'];

        $huddle_id = $input['huddle_id'];

        $account_id = $input['account_id'];

        $user_id = $input['user_id'];

        $role_id = $input['role_id'];


        $assessment_summary_check = false;

        $coaching_summary_check = false;

        $performance_level_check = false;

        $rubric_check = false;

        $can_comment = true;

        $can_reply = true;

        $can_rate = true;

        $can_dl_edit_delete_copy_video = true;

        $coaching_perfomance_level = true;

        $assessment_perfomance_level = true;

        $final_array = array();

        $video_data = Document::where(array(
                    'id' => $video_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($video_data) {
            $video_data = $video_data->toArray();
        }

        if ($video_data['post_rubric_per_video'] == '1') {

            $account_folder_details = AccountFolderDocument::where(array(
                        'document_id' => $video_id,
                        'site_id' => $this->site_id
                    ))->first();

            if ($account_folder_details) {
                $account_folder_details = $account_folder_details->toArray();
            }

            if (!empty($account_folder_details)) {
                if (!empty($account_folder_details['video_framework_id'])) {
                    $final_array['framework_selected_for_video'] = '1';
                } else {
                    $final_array['framework_selected_for_video'] = '0';
                }
            }
        } else {
            $final_array['framework_selected_for_video'] = '1';
        }

        $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

        if (!$video_creater_detail) {
            return array('message' => TranslationsManager::get_translation('you_dont_have_access_to_this_video', 'flash_messages', $this->site_id),
                'success' => false);
        }

        $user_pause_settings = UserAccount::where(array(
                    "user_id" => $user_id, "account_id" => $account_id, 'site_id' => $this->site_id
                ))->first()->toArray();



        $get_all_comments = isset($input['get_all_comments']) ? $input['get_all_comments'] : 0;

        $comments = $this->get_video_comments_with_replies($video_id, 0, $huddle_id, $user_id);

        $final_array['comments'] = $comments;
        $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);

        $final_array['huddle_type'] = 'Collaboration';

        $coachee_permission = AccountFolderMetaData::where(array('account_folder_id' => $huddle_id, "meta_data_name" => "coachee_permission", 'site_id' => $this->site_id))->get()->toArray();
        $coachee_permissions = isset($coachee_permission[0]['meta_data_value']) ? $coachee_permission[0]['meta_data_value'] : "0";


        if ($h_type == '2') {
            $get_user_huddle_permission = $this->get_huddle_permissions($huddle_id, $user_id);
            $coaches_name = AccountFolderUser::getUserNameCoaches($huddle_id, $this->site_id);
            $coachee_name = AccountFolderUser::getUserNameMente($huddle_id, $this->site_id);
            $final_array['coaches_name'] = $coaches_name;
            $final_array['coachee_name'] = $coachee_name;
            $final_array['huddle_type'] = 'Coaching';

            $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

            if ($video_creater_detail) {
                $video_creater_detail = $video_creater_detail->toArray();
            }

            if ($coachee_permissions && ( $get_user_huddle_permission == '210' || $get_user_huddle_permission == '220') && $user_id != $video_creater_detail['created_by']) {
                $can_dl_edit_delete_copy_video = false;
            }

            $coaching_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'coaching_perfomance_level', 'site_id' => $this->site_id))->first();
            if ($coaching_perfomance_level) {
                $coaching_perfomance_level = $coaching_perfomance_level->toArray();
            }
            $coaching_perfomance_level = isset($coaching_perfomance_level['meta_data_value']) ? $coaching_perfomance_level['meta_data_value'] : "0";


            $assessment_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'assessment_perfomance_level', 'site_id' => $this->site_id))->first();
            if ($assessment_perfomance_level) {
                $assessment_perfomance_level = $assessment_perfomance_level->toArray();
            }
            $assessment_perfomance_level = isset($assessment_perfomance_level['meta_data_value']) ? $assessment_perfomance_level['meta_data_value'] : "0";
        } elseif ($h_type == '3') {
            $get_user_huddle_permission = $this->get_huddle_permissions($huddle_id, $user_id);
            $assessor_names = AccountFolderUser::getUsernameEvaluator($this->site_id, $huddle_id);
            if ($get_user_huddle_permission == '200') {
                $eval_participant_names = AccountFolderUser::getParticipants($this->site_id, $huddle_id);
            } else {
                $eval_participant_names = AccountFolderUser::getParticipants($this->site_id, $huddle_id, $user_id);
            }
            $final_array['assessor_names'] = $assessor_names;
            $final_array['eval_participant_names'] = $eval_participant_names;
            $final_array['huddle_type'] = 'Assessment';
        }
        $huddle_detail = AccountFolder::where(array('account_folder_id' => $huddle_id, 'site_id' => $this->site_id))->first();

        $has_access_to_huddle = false;
        $p_huddle_users = AccountFolder::getHuddleUsers($huddle_id, $this->site_id);
        $p_huddle_group_users = AccountFolder::getHuddleGroupsWithUsers($huddle_id, $this->site_id);
        if (count($p_huddle_users) > 0) {

            for ($i = 0; $i < count($p_huddle_users); $i++) {
                $p_huddle_user = $p_huddle_users[$i];
                if ($p_huddle_user['user_id'] == $user_id) {
                    $has_access_to_huddle = true;
                }
            }
        }
        if (count($p_huddle_group_users) > 0) {
            for ($i = 0; $i < count($p_huddle_group_users); $i++) {
                $p_huddle_group_user = $p_huddle_group_users[$i];
                if (isset($p_huddle_group_user['id']) && $p_huddle_group_user['id'] == $user_id)
                    $has_access_to_huddle = true;
            }
        }

        if (!$has_access_to_huddle) {
            return array('message' => TranslationsManager::get_translation('you_dont_have_access_to_this_huddle', 'flash_messages', $this->site_id),
                'success' => false);
        }
        if ($h_type == '3') {

            $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

            if ($video_creater_detail) {
                $video_creater_detail = $video_creater_detail->toArray();
            }

            $isEditable = $this->_check_evaluator_permissions($huddle_id, $video_creater_detail['created_by'], $user_id, $role_id);
            if ($isEditable == false) {
                return array('message' => TranslationsManager::get_translation('you_dont_have_access_to_this_video', 'flash_messages', $this->site_id),
                    'success' => false);
            }
        }



        if ($huddle_detail) {
            $huddle_detail = $huddle_detail->toArray();
        }
        $video_detail_coachee = '';
        $user_huddle_level_permissions = $this->get_huddle_permissions($huddle_id, $user_id);

        $video_data = AccountFolder::getHuddleUsers($huddle_id, $this->site_id);

        $tracker_enable = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'enable_tracker', 'site_id' => $this->site_id))->first();
        if ($tracker_enable) {
            $tracker_enable = $tracker_enable->toArray();
        }

        $enable_tracker = isset($tracker_enable['meta_data_value']) ? $tracker_enable['meta_data_value'] : "0";

        foreach ($video_data as $hudd_user) {
            if ($hudd_user['role_id'] == 210) {
                $video_detail_coachee = $hudd_user['user_id'];
            }
        }

        if ($user_huddle_level_permissions == 200 || ( ($user_id == $huddle_detail['created_by']) && $input['permission_maintain_folders'] == '1')) {

            if ($h_type == 2) {
                if ($enable_tracker == 1 && !empty($video_detail_coachee)) {
                    $coaching_summary_check = true;
                }
            }

            $enable_assessment = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'enable_matric', 'site_id' => $this->site_id))->first();
            if ($enable_assessment) {
                $enable_assessment = $enable_assessment->toArray();
            }

            $enable_tracker_check = isset($enable_assessment['meta_data_value']) ? $enable_assessment['meta_data_value'] : "0";

            $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

            if ($video_creater_detail) {
                $video_creater_detail = $video_creater_detail->toArray();
            }

            if ($h_type == 3) {
                if ($enable_tracker_check && !empty($video_detail_coachee)) {
                    if ($this->check_if_evaluated_participant($huddle_id, $video_creater_detail['created_by'])) {
                        $assessment_summary_check = true;
                    }
                }
            }
        }

        if (($h_type == 3 && ($user_huddle_level_permissions == 200 || $user_huddle_level_permissions == 210 ) ) || ($this->coaching_perfomance_level($account_id, $h_type, $user_huddle_level_permissions))) {
            $performance_level_check = true;
        }

        if ($this->is_enabled_framework_and_standards($account_id)) {
            if ($this->is_enabled_huddle_framework_and_standards($huddle_id)) {
                $rubric_check = true;
            }
        }

        if ($h_type == 3 && ($user_huddle_level_permissions == 210 || $user_huddle_level_permissions == 220 )) {
            $can_comment = false;
            $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

            if ($video_creater_detail) {
                $video_creater_detail = $video_creater_detail->toArray();
            }

            if ($video_creater_detail['created_by'] != $user_id) {
                $can_reply = false;
            }
        }

        if ($user_huddle_level_permissions != 200) {
            $can_rate = false;
        }

        if ($h_type != 2 && $h_type != 3 && $user_huddle_level_permissions == 220) {
            $can_comment = false;
            $can_dl_edit_delete_copy_video = false;
        }

        if ($h_type != '3') {
            $allow_per_video_details = AccountFolderMetaData::where(array('account_folder_id' => $huddle_id, "meta_data_name" => "allow_per_video_to_coachee", 'site_id' => $this->site_id))->get()->toArray();
            $allow_per_video_check = isset($allow_per_video_details[0]['meta_data_value']) ? $allow_per_video_details[0]['meta_data_value'] : "0";

            $final_array['allow_per_video'] = $allow_per_video_check;
        }


        $id_framework = AccountFolderMetaData::where(array(
                    'account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id', 'site_id' => $this->site_id
                ))->first();

        if ($id_framework) {
            $id_framework->toArray();
        }

        $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";


        if ($framework_id == -1 || $framework_id == 0) {

            $id_framework = AccountMetaData::where(array(
                        'account_id' => $account_id, 'meta_data_name' => 'default_framework', 'site_id' => $this->site_id
                    ))->first();


            if ($id_framework) {
                $id_framework->toArray();
            }

            $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";
        }


        $huddle_video_marker_tags = AccountFolderMetaData::where(array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_tags', 'site_id' => $this->site_id))->first();
        if ($huddle_video_marker_tags) {
            $huddle_video_marker_tags = $huddle_video_marker_tags->toArray();
        }
        $huddle_custom_markers_permission = isset($huddle_video_marker_tags['meta_data_value']) ? $huddle_video_marker_tags['meta_data_value'] : "0";

        $account_custom_markers_permission = 0;
        $video_marker_tags = AccountFolderMetaData::where(array(
                    "account_folder_id" => $account_id,
                    "meta_data_name" => "enable_tags",
                    'site_id' => $this->site_id
                ))->first();

        if ($video_marker_tags) {
            $video_marker_tags = $video_marker_tags->toArray();
        }

        if (isset($video_marker_tags['meta_data_value']) && $video_marker_tags['meta_data_value'] == '1') {
            $account_custom_markers_permission = 1;
        } else {
            $account_custom_markers_permission = 0;
        }

        if ($account_custom_markers_permission == '1' && $huddle_custom_markers_permission == '1') {
            $final_array['video_markers_check'] = '1';
        } else {
            $final_array['video_markers_check'] = '0';
        }


        if ($h_type == '3' && ($user_huddle_level_permissions == 210 || $user_huddle_level_permissions == 220 )) {
            $final_array['video_markers_check'] = '0';
        }


        $coaching_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'coaching_perfomance_level', 'site_id' => $this->site_id))->first();
        if ($coaching_perfomance_level) {
            $coaching_perfomance_level = $coaching_perfomance_level->toArray();
        }
        $coaching_perfomance_level = isset($coaching_perfomance_level['meta_data_value']) ? $coaching_perfomance_level['meta_data_value'] : "0";


        $assessment_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'assessment_perfomance_level', 'site_id' => $this->site_id))->first();
        if ($assessment_perfomance_level) {
            $assessment_perfomance_level = $assessment_perfomance_level->toArray();
        }
        $assessment_perfomance_level = isset($assessment_perfomance_level['meta_data_value']) ? $assessment_perfomance_level['meta_data_value'] : "0";


        $final_array['default_framework'] = $framework_id;

        $final_array['huddle_info'] = $huddle_detail;

        $final_array['can_dl_edit_delete_copy_video'] = $can_dl_edit_delete_copy_video;

        $final_array['can_comment'] = $can_comment;

        $final_array['can_rate'] = $can_rate;

        $final_array['can_reply'] = $can_reply;

        $final_array['h_type'] = $h_type;

        $final_array['assessment_summary_check'] = $assessment_summary_check;

        $final_array['coaching_summary_check'] = $coaching_summary_check;

        $final_array['coaching_perfomance_level'] = $coaching_perfomance_level;

        $final_array['assessment_perfomance_level'] = $assessment_perfomance_level;

        $final_array['performance_level_check'] = $performance_level_check;

        $final_array['rubric_check'] = $rubric_check;

        $custom_markers = $this->getmarkertags($account_id, '1');

        $final_array['custom_markers'] = $custom_markers;

        $comments_counts = $this->get_comments_count($video_id, $user_id);
        $attached_document_numbers = Document::getVideoAttachmentNumbers($video_id, $this->site_id);
        $video_object_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

        if ($video_object_detail) {
            $video_object_detail = $video_object_detail->toArray();
            $video_object_detail['title'] = AccountFolderDocument::get_document_name($video_id, $this->site_id);
        }

        $final_array['video_object_detail'] = $video_object_detail;
        $final_array['comments_counts'] = $comments_counts;
        $final_array['user_pause_settings'] = $user_pause_settings['type_pause'];
        $final_array['attached_document_numbers'] = $attached_document_numbers;
        $final_array['bread_crumb_output'] = $this->bread_crumb_output($huddle_id);
        $user_huddle_level_permissions = $this->get_huddle_permissions($huddle_id, $user_id);
        $final_array['user_huddle_level_permissions'] = $user_huddle_level_permissions;
        $assessment_link = true;
        $coaching_link = true;
        $main_tracker_enable = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'enabletracking', 'site_id' => $this->site_id))->first();
        if ($main_tracker_enable) {
            $main_tracker_enable = $main_tracker_enable->toArray();
        }

        $main_enable_check = isset($main_tracker_enable['meta_data_value']) ? $main_tracker_enable['meta_data_value'] : "0";
        if (($h_type == '3') && ( $user_huddle_level_permissions != 200 || (!$main_enable_check || !$enable_tracker_check ) )) {
            $assessment_link = false;
            $coaching_link = false;
        }
        if (($h_type == '2') && ( $user_huddle_level_permissions != 200 || (!$main_enable_check || !$enable_tracker ) )) {
            $assessment_link = false;
            $coaching_link = false;
        }

        $coaching_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'coaching_perfomance_level', 'site_id' => $this->site_id))->first();
        if ($coaching_perfomance_level) {
            $coaching_perfomance_level = $coaching_perfomance_level->toArray();
        }
        $coaching_perfomance_level = isset($coaching_perfomance_level['meta_data_value']) ? $coaching_perfomance_level['meta_data_value'] : "0";
        $can_view_summary = AccountFolderMetaData::where(array(
                    'account_folder_id' => $huddle_id, 'meta_data_name' => 'coachee_can_view_summary', 'site_id' => $this->site_id
                ))->get()->toArray();

        $final_array['can_view_summary'] = isset($can_view_summary[0]['meta_data_value']) ? $can_view_summary[0]['meta_data_value'] : "0";

        $final_array['coaching_perfomance_level'] = $coaching_perfomance_level;
        $final_array['assessment_link'] = $assessment_link;
        $final_array['coaching_link'] = $coaching_link;
        $final_array['success'] = true;



        return $final_array;
    }

    function workspace_view_page_observation(Request $request) {
        $input = $request->all();

        $video_id = $input['video_id'];

        $huddle_id = $input['huddle_id'];

        $account_id = $input['account_id'];

        $user_id = $input['user_id'];

        $role_id = $input['role_id'];


        $assessment_summary_check = false;

        $coaching_summary_check = false;

        $performance_level_check = false;

        $rubric_check = false;

        $can_comment = true;

        $can_reply = true;

        $can_rate = true;

        $can_dl_edit_delete_copy_video = true;

        $coaching_perfomance_level = true;

        $assessment_perfomance_level = true;

        $final_array = array();

        $video_data = Document::where(array(
                    'id' => $video_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($video_data) {
            $video_data = $video_data->toArray();
        } else {
            return array('message' => TranslationsManager::get_translation('you_dont_have_access_to_this_video', 'flash_messages', $this->site_id),
                'success' => false);
        }

        if ($video_data['post_rubric_per_video'] == '1') {

            $account_folder_details = AccountFolderDocument::where(array(
                        'document_id' => $video_id,
                        'site_id' => $this->site_id
                    ))->first();

            if ($account_folder_details) {
                $account_folder_details = $account_folder_details->toArray();
            }

            if (!empty($account_folder_details)) {
                if (!empty($account_folder_details['video_framework_id'])) {
                    $final_array['framework_selected_for_video'] = '1';
                } else {
                    $final_array['framework_selected_for_video'] = '0';
                }
            }
        } else {
            $final_array['framework_selected_for_video'] = '1';
        }

        $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

        if (!$video_creater_detail) {
            return array('message' => TranslationsManager::get_translation('you_dont_have_access_to_this_video', 'flash_messages', $this->site_id),
                'success' => false);
        }

        $user_pause_settings = UserAccount::where(array(
                    "user_id" => $user_id, "account_id" => $account_id, 'site_id' => $this->site_id
                ))->first()->toArray();



        $get_all_comments = isset($input['get_all_comments']) ? $input['get_all_comments'] : 0;

        $comments = $this->get_video_comments_with_replies($video_id, 0, $huddle_id, $user_id);

        $final_array['comments'] = $comments;
        $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);

        $final_array['huddle_type'] = 'Collaboration';


        $huddle_detail = AccountFolder::where(array('account_folder_id' => $huddle_id, 'site_id' => $this->site_id))->first();

        if ($huddle_detail) {
            $huddle_detail = $huddle_detail->toArray();
        }
        $video_detail_coachee = '';


        $video_data = AccountFolder::getHuddleUsers($huddle_id, $this->site_id);

        $tracker_enable = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'enable_tracker', 'site_id' => $this->site_id))->first();
        if ($tracker_enable) {
            $tracker_enable = $tracker_enable->toArray();
        }

        $user_huddle_level_permissions = $this->get_huddle_permissions($huddle_id, $user_id);
        //if ($user_huddle_level_permissions == 200 || ( ($user_id == $huddle_detail['created_by']) && $input['permission_maintain_folders'] == '1')) {



        $enable_assessment = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'enable_matric', 'site_id' => $this->site_id))->first();
        if ($enable_assessment) {
            $enable_assessment = $enable_assessment->toArray();
        }

        $enable_tracker_check = isset($enable_assessment['meta_data_value']) ? $enable_assessment['meta_data_value'] : "0";

        $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

        if ($video_creater_detail) {
            $video_creater_detail = $video_creater_detail->toArray();
        }

        $assessment_summary_check = true;
        //}
        //if (($h_type == 3 && ($user_huddle_level_permissions == 200 || $user_huddle_level_permissions == 210 ) ) || ($this->coaching_perfomance_level($account_id, $h_type, $user_huddle_level_permissions))) {
        $performance_level_check = true;
        // }

        if ($this->is_enabled_framework_and_standards_ws($account_id)) {
            $rubric_check = true;
        }

        //if ($h_type == 3 && ($user_huddle_level_permissions == 210 || $user_huddle_level_permissions == 220 )) {
        $can_comment = false;
        // $can_comment_reply = AccountFolderMetaData::where(array(
        //     'account_folder_id' => $huddle_id, 'meta_data_name' => 'can_comment_reply', 'site_id' => $this->site_id
        // ))->get()->toArray();

        //$can_comment_reply_value = isset($can_comment_reply[0]['meta_data_value']) ? $can_comment_reply[0]['meta_data_value'] : "0";

        if ($video_creater_detail['created_by'] == $user_id || $user_huddle_level_permissions == 210) {
            $can_comment = true;
        }else{
            $can_reply = false;
        }


        /***************************************/
        $video_creater_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

        if ($video_creater_detail) {
            $video_creater_detail = $video_creater_detail->toArray();
        }


        //}
        //   if ($user_huddle_level_permissions != 200) {

     $can_rate = true;



      // }
        //  if ($h_type != 2 && $h_type != 3 && $user_huddle_level_permissions == 220) {
        $can_comment = false;
        $can_dl_edit_delete_copy_video = false;
        // }
        //if ($h_type != '3') {
        $allow_per_video_details = AccountFolderMetaData::where(array('account_folder_id' => $huddle_id, "meta_data_name" => "allow_per_video_to_coachee", 'site_id' => $this->site_id))->get()->toArray();
        $allow_per_video_check = isset($allow_per_video_details[0]['meta_data_value']) ? $allow_per_video_details[0]['meta_data_value'] : "0";

        $final_array['allow_per_video'] = $allow_per_video_check;
        // }


        $id_framework = AccountFolderMetaData::where(array(
                    'account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id', 'site_id' => $this->site_id
                ))->first();

        if ($id_framework) {
            $id_framework->toArray();
        }

        $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";


        if ($framework_id == -1 || $framework_id == 0) {

            $id_framework = AccountMetaData::where(array(
                        'account_id' => $account_id, 'meta_data_name' => 'default_framework', 'site_id' => $this->site_id
                    ))->first();


            if ($id_framework) {
                $id_framework->toArray();
            }

            $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";
        }


        $huddle_video_marker_tags = AccountFolderMetaData::where(array('account_folder_id' => $huddle_id, 'meta_data_name' => 'chk_tags', 'site_id' => $this->site_id))->first();
        if ($huddle_video_marker_tags) {
            $huddle_video_marker_tags = $huddle_video_marker_tags->toArray();
        }
        $huddle_custom_markers_permission = isset($huddle_video_marker_tags['meta_data_value']) ? $huddle_video_marker_tags['meta_data_value'] : "0";

        $account_custom_markers_permission = 0;
        $video_marker_tags = AccountFolderMetaData::where(array(
                    "account_folder_id" => $account_id,
                    "meta_data_name" => "enable_tags_ws",
                    'site_id' => $this->site_id
                ))->first();

        if ($video_marker_tags) {
            $video_marker_tags = $video_marker_tags->toArray();
        }

        if (isset($video_marker_tags['meta_data_value']) && $video_marker_tags['meta_data_value'] == '1') {
            $account_custom_markers_permission = 1;
        } else {
            $account_custom_markers_permission = 0;
        }

        if ($account_custom_markers_permission == '1') {
            $final_array['video_markers_check'] = '1';
        } else {
            $final_array['video_markers_check'] = '0';
        }



        //  if ($h_type == '3' && ($user_huddle_level_permissions == 210 || $user_huddle_level_permissions == 220 )) {
        //$final_array['video_markers_check'] = '0';
        //  }


        $coaching_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'coaching_perfomance_level', 'site_id' => $this->site_id))->first();
        if ($coaching_perfomance_level) {
            $coaching_perfomance_level = $coaching_perfomance_level->toArray();
        }
        $coaching_perfomance_level = isset($coaching_perfomance_level['meta_data_value']) ? $coaching_perfomance_level['meta_data_value'] : "0";


        $assessment_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'assessment_perfomance_level', 'site_id' => $this->site_id))->first();
        if ($assessment_perfomance_level) {
            $assessment_perfomance_level = $assessment_perfomance_level->toArray();
        }
        $assessment_perfomance_level = isset($assessment_perfomance_level['meta_data_value']) ? $assessment_perfomance_level['meta_data_value'] : "0";


        $final_array['default_framework'] = $framework_id;

        $final_array['huddle_info'] = $huddle_detail;

        $final_array['can_dl_edit_delete_copy_video'] = $can_dl_edit_delete_copy_video;

        $final_array['can_comment'] = $can_comment;

        $final_array['can_rate'] = $can_rate;

        $final_array['can_reply'] = $can_reply;

        $final_array['h_type'] = $h_type;

        $final_array['assessment_summary_check'] = $assessment_summary_check;

        $final_array['coaching_summary_check'] = $coaching_summary_check;

        $final_array['coaching_perfomance_level'] = $coaching_perfomance_level;

        $final_array['assessment_perfomance_level'] = $assessment_perfomance_level;

        $final_array['performance_level_check'] = $performance_level_check;

        $final_array['user_huddle_level_permissions'] =$user_huddle_level_permissions;

        $final_array['rubric_check'] = $rubric_check;

        $custom_markers = $this->getmarkertags($account_id, '1');

        $final_array['custom_markers'] = $custom_markers;

        $comments_counts = $this->get_comments_count($video_id, $user_id);
        $attached_document_numbers = Document::getVideoAttachmentNumbers($video_id, $this->site_id);
        $video_object_detail = Document::where(array('id' => $video_id, 'site_id' => $this->site_id))->first();

        if ($video_object_detail) {
            $video_object_detail = $video_object_detail->toArray();
            $video_object_detail['title'] = AccountFolderDocument::get_document_name($video_id, $this->site_id);
        }

        $final_array['video_object_detail'] = $video_object_detail;
        if($video_object_detail && !empty($video_object_detail['parent_folder_id']))
        {
            $final_array['breadcumbs'] = DocumentFolder::getNLevelParents($video_object_detail['parent_folder_id']);
        }
        $final_array['comments_counts'] = $comments_counts;
        $final_array['user_pause_settings'] = $user_pause_settings['type_pause'];
        $final_array['attached_document_numbers'] = $attached_document_numbers;
        $final_array['bread_crumb_output'] = $this->bread_crumb_output($huddle_id);
        $final_array['success'] = true;



        return $final_array;
    }

    function getVideoDetails($document_id, $doc_type = 1) {

        $document_id = (int) $document_id;

        $result = Document::leftJoin('users as User', 'documents.created_by', '=', 'User.id')
                        ->join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                        ->select('documents.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'afd.id as document_id', 'documents.id as doc_id')
                        ->selectRaw('DATE_FORMAT(documents.recorded_date, "%b %d, %Y %H:%i") as recorded_date')
                        ->where('documents.id', $document_id)
                        ->where('documents.site_id', $this->site_id)
                        ->whereIn('doc_type', [1, 3, 4])
                        ->get()->toArray();

        return (!empty($result)) ? $result[0] : [];
    }

    public function getmarkertags($account_id, $ref_type) {

        $tag_code = AccountTag::where(array(
                    "account_id" => $account_id, "tag_type" => $ref_type, "site_id" => $this->site_id
                ))->get()->toArray();

        return $tag_code;
    }

    public function save_standard_tag($standard_tag, $comment_id, $ref_id, $account_id, $user_id, $huddle_id, $standards_acc_tags, $previous_standards_ids = '') { 
        $framework_id = AccountTag::getFrameworkId($account_id, $huddle_id, $this->site_id);


        $video_data = Document::where(array(
                    'id' => $ref_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($video_data) {
            $video_data = $video_data->toArray();
        }

        $account_folder_details = AccountFolderDocument::where(array(
                    'document_id' => $ref_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($video_data['post_rubric_per_video'] == '1' || !empty($account_folder_details['video_framework_id'])) {

            $account_folder_details = AccountFolderDocument::where(array(
                        'document_id' => $ref_id,
                        'site_id' => $this->site_id
                    ))->first();

            if ($account_folder_details) {
                $account_folder_details = $account_folder_details->toArray();
            }

            if (!empty($account_folder_details)) {
                if (!empty($account_folder_details['video_framework_id'])) {
                    $framework_id = $account_folder_details['video_framework_id'];
                }
            }
        }
        // $standards = str_replace('# ', '', $standard_tag);
        //$standardtags = explode(',', $standards);
        $standardtags_acc_tags = explode(',', $standards_acc_tags);
        $save_cmt_tag = [];
        foreach ($standardtags_acc_tags as $standardtag) {
            $tagcode = explode('-', $standardtag);
            $code_standard = AccountTag::getStandardByCodeWeb($this->site_id, $standardtag, $account_id, $framework_id);
            $save_cmt_tag [] = [
                "comment_id" => $comment_id,
                "ref_type" => "0",
                "ref_id" => $ref_id,
                "tag_title" => isset($code_standard->tag_title) ? $code_standard->tag_title : '',
                "account_tag_id" => isset($code_standard->account_tag_id) ? $code_standard->account_tag_id : '',
                "created_by" => $user_id,
                "site_id" => $this->site_id,
                "created_date" => date('Y-m-d H:i:s')
            ];

            $user_activity_logs = [
                'ref_id' => $comment_id,
                'desc' => isset($code_standard->tag_title) ? $code_standard->tag_title : '',
                'url' => '/' . $comment_id,
                'type' => '15',
                'account_id' => $account_id,
                'account_folder_id' => isset($code_standard->account_tag_id) ? $code_standard->account_tag_id : '',
                'environment_type' => 2,
                'site_id' => $this->site_id
            ];

            $this->user_activity_logs($user_activity_logs, $account_id, $user_id);

            if (empty($previous_standards_ids)) {

                $user_detail = User::where(array('id' => $user_id, 'site_id' => $this->site_id))->first();

                if ($user_detail) {

                    $user_detail = $user_detail->toArray();
                    $this->create_churnzero_event('Standard+Tagged', $account_id, $user_detail['email']);
                }
            }


            if (is_array($previous_standards_ids) && !in_array($code_standard->account_tag_id, $previous_standards_ids)) {
                $user_detail = User::where(array('id' => $user_id, 'site_id' => $this->site_id))->first();

                if ($user_detail) {

                    $user_detail = $user_detail->toArray();
                    $this->create_churnzero_event('Standard+Tagged', $account_id, $user_detail['email']);
                }
            }
        }



        return empty($save_cmt_tag) ? false : DB::table('account_comment_tags')->insert($save_cmt_tag);
    }

    public static function getSecureAmazonUrl($file, $name = null) {


        $aws_access_key_id = 'AKIAJ4ZWDR5X5JKB7CZQ';
        $aws_secret_key = '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';
        $aws_bucket = config('s3.bucket_name');
        $expires = '+240 minutes';

        // Create an Amazon S3 client object
        $client = new S3Client([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);

        try {
            $cmd = $client->getCommand('GetObject', [
                'Bucket' => $aws_bucket,
                'Key' => $file,
                'ResponseCacheControl' => 'No-cache',
                'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : basename($file))
            ]);
            $url = $client->createPresignedRequest($cmd, $expires)->getUri()->__toString();
            return $url;
        } catch (\Aws\S3\Exception\S3Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    public static function getSecureAmazonCloudFrontUrl($resource, $name = '', $timeout = 600, $useHTTP = false) {
        //Read Cloudfront Private Key Pair
        $cloudFront = new CloudFrontClient([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);

        $streamHostUrl = 'https://d1ebp8oyqi4ffr.cloudfront.net';

        if ($useHTTP == true) {
            $streamHostUrl = str_replace("https://", "http://", $streamHostUrl);
        }
        $resourceKey = $resource;
        $expires = time() + 21600;

        //Construct the URL
        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
            'private_key' => realpath(dirname(__FILE__) . '/../..') . "/Config/cloudfront_key.pem",
            'key_pair_id' => config('s3.cloudfront_keypair'),
        ));

        return $signedUrlCannedPolicy;
    }

    public static function getSecureAmazonSibmeUrl($file, $name = null, $bucket_sibme = 0) {


        $aws_access_key_id = 'AKIAJ4ZWDR5X5JKB7CZQ';
        $aws_secret_key = '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';
        $aws_bucket = config('s3.bucket_name');
        if ($bucket_sibme == 1) {
            $aws_bucket = 'sibme.com';
        }


        $expires = '+240 minutes';

        // Create an Amazon S3 client object
        $client = new S3Client([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);
/*
        $url = $client->getObjectUrl($aws_bucket, $file, $expires, array(
            'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : basename($file)),
        ));
*/

        try {
            $cmd = $client->getCommand('GetObject', [
                'Bucket' => $aws_bucket,
                'Key' => $file,
                'ResponseCacheControl' => 'No-cache',
                'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : basename($file))
            ]);
            $url = $client->createPresignedRequest($cmd, $expires)->getUri()->__toString();
            return $url;
        } catch (\Aws\S3\Exception\S3Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    public static function get_document_url($document_video, $force_default=false) {
        $thumnail_type = 'default';


        if(isset($document_video['site_id']))
        {
            $site_id = $document_video['site_id'];
        }
        else
        {
            $site_id = app("Illuminate\Http\Request")->header("site_id");
        }
        if( $site_id==1){
            $default_thumbnail = self::getSecureAmazonSibmeUrl('app/img/video-thumbnail-sibme.svg', null, 1);
        } else {
            $default_thumbnail = self::getSecureAmazonSibmeUrl('app/img/video-thumbnail-2.png', null, 1);
        }

        $error_init = array(
            'url' => '',
            'relative_url' => '',
            'thumbnail' => $default_thumbnail,
            'duration' => '',
            'encoder_status' => 'Error'
        );
        if (!isset($document_video['url'])) {
            return $error_init;
        }
        if ($document_video['published'] == 0) {

            return array(
                'url' => $document_video['url'],
                'thumbnail' => '',
                'thumbnail_type' => '',
                'encoder_status' => $document_video['encoder_status']
            );
        }
        $videoFilePath = pathinfo($document_video['url']);
        if (empty($videoFilePath['extension'])) {
            return $error_init;
        }
        $videoFileExtension = $videoFilePath['extension'];


        $check_data = DocumentFiles::where(array(
                    "document_id" => @$document_video['doc_id'] ? @$document_video['doc_id'] : $document_video['id'],
                    "site_id" => $site_id,
                    "default_web" => true
                ))->whereRaw("url LIKE '%/".$document_video['account_id']."/%'")->first();

        if ($check_data) {
            $check_data = $check_data->toArray();
        }
        if(empty($check_data))
        {

            $check_data_revised = DocumentFiles::where(array(
                        "document_id" => @$document_video['doc_id'] ? @$document_video['doc_id'] : $document_video['id'],
                        "site_id" => $site_id,
                        "default_web" => true
                    ))->first();

            $check_data = $check_data_revised;
        }

        $aws_transcoder_type = $document_video['aws_transcoder_type'];

        $duration = 0;
        if (isset($check_data) && isset($check_data['url'])) {

            $url = $check_data['url'];
            $duration = $check_data['duration'];
            $thumbnail = '';
            $videoFilePath = pathinfo($url);
            if (!isset($videoFilePath['dirname'])) {
                return array(
                    'url' => '',
                    'relative_url' => '',
                    'thumbnail' => $default_thumbnail,
                    'duration' => '',
                    'encoder_status' => 'Error'
                );
            }
            $videoFileName = $videoFilePath['filename'];
            //these are old files before JobQueue
            if ($check_data['transcoding_status'] == '-1') {

                $videoFilePathThumb = pathinfo($document_video['url']);
                $videoFileNameThumb = $videoFilePathThumb['filename'];
            } else {
                $videoFileNameThumb = $videoFileName;
            }


            $poster_end_extension = "_thumb.png";
            if ($document_video['encoder_provider'] == '2' && $aws_transcoder_type!='2') {
                $poster_end_extension = empty($document_video['thumbnail_number']) ?
                        "_thumb_00001.png" :
                        '_thumb_' . sprintf('%05d', $document_video['thumbnail_number']) . '.png';
            }
            elseif ($aws_transcoder_type=='2') {
                $poster_end_extension = "_thumbnail.0000000.png";
            }
            $thumbnail_image_path = '';
            $thumnail_type = 'default';
            if (true) {
                if ($check_data['transcoding_status'] == '2') {
                    if (in_array(strtolower( $videoFileExtension), Document::$audioExt) ) {
                        if ($site_id == 1) {
                            $thumbnail_image_path = self::getSecureAmazonSibmeUrl('app/img/audio-thumbnail.png', null, 1);
                        } else {
                            $thumbnail_image_path = self::getSecureAmazonSibmeUrl('app/img/audio-hmh-thumbnail.png', null, 1);
                        }
                    } else {
                        $thumnail_type = 'default';
                        if ($site_id == 1) {
                            $thumbnail_image_path = self::getSecureAmazonSibmeUrl('app/img/video-thumbnail-sibme.svg', null, 1);
                        } else {
                            $thumbnail_image_path = self::getSecureAmazonSibmeUrl('app/img/video-thumbnail-2.png', null, 1);
                        }
                    }
                } else {
                    if (in_array(strtolower( $videoFileExtension), Document::$audioExt) ) {
                        if ($site_id == 1) {
                            $thumbnail_image_path = self::getSecureAmazonSibmeUrl('app/img/audio-thumbnail.png', null, 1);
                        } else {
                            $thumbnail_image_path = self::getSecureAmazonSibmeUrl('app/img/audio-hmh-thumbnail.png', null, 1);
                        }
                    } else {
                        $thumnail_type = 'custom';
                        if ($videoFilePath['dirname'] == "."){
                            $thumbnail_image_path = self::getSecureAmazonUrl(($videoFileNameThumb) . "$poster_end_extension");
                        }else{
                            $thumbnail_image_path = self::getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileNameThumb) . "$poster_end_extension");
                        }
                    }
                }
            } else {
                if (in_array(strtolower( $videoFileExtension), Document::$audioExt) ) {

                    if ($site_id == 1) {
                        $thumbnail_image_path = self::getSecureAmazonSibmeUrl('app/img/audio-thumbnail.png', null, 1);
                    } else {
                        $thumbnail_image_path = self::getSecureAmazonSibmeUrl('app/img/audio-hmh-thumbnail.png', null, 1);
                    }
                } else {
                    if ($videoFilePath['dirname'] == ".")
                        $thumbnail_image_path = self::getSecureAmazonUrl(($videoFileNameThumb) . "$poster_end_extension");
                    else
                        $thumbnail_image_path = self::getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileNameThumb) . "$poster_end_extension");
                }
            }



            $video_path = '';
            $lvideo_path = '';
            $relative_video_path = '';

            if (true) {

                if ($videoFilePath['dirname'] == ".") {
                    $videoFileNameEncoded = urlencode($videoFileName);
                    $videoFileNameEncoded = str_replace('%7E', '~', $videoFileNameEncoded);
                    if($videoFileExtension =='mp4'){
                        $video_path = self::getSecureAmazonCloudFrontUrl($videoFileNameEncoded . ".mp4");
                        $lvideo_path = self::getSecureAmazonCloudFrontUrl($videoFileNameEncoded . ".mp4", '', 600, true);
                        $relative_video_path = $videoFileName . ".mp4";
                    }elseif($videoFileExtension =='mp3'){
                        $video_path = self::getSecureAmazonCloudFrontUrl($videoFileNameEncoded . ".mp3");
                        $lvideo_path = self::getSecureAmazonCloudFrontUrl($videoFileNameEncoded . ".mp3", '', 600, true);
                        $relative_video_path = $videoFileName . ".mp3";
                    }else{
                        $video_path = self::getSecureAmazonCloudFrontUrl($videoFileNameEncoded . ".mp4");
                        $lvideo_path = self::getSecureAmazonCloudFrontUrl($videoFileNameEncoded . ".mp4", '', 600, true);
                        $relative_video_path = $videoFileName . ".mp4";
                    }
                    
                } else {
                    $videoFileNameEncoded = urlencode($videoFileName);
                    $videoFileNameEncoded = str_replace('%7E', '~', $videoFileNameEncoded);

                    if($videoFileExtension =='mp4'){
                        $video_path = self::getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . $videoFileNameEncoded . ".mp4");
                        $lvideo_path = self::getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . $videoFileNameEncoded . ".mp4", "", 600, true);
                        $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".mp4";
                    }elseif($videoFileExtension =='mp3'){
                        $video_path = self::getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . $videoFileNameEncoded . ".mp3");
                        $lvideo_path = self::getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . $videoFileNameEncoded . ".mp3", "", 600, true);
                        $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".mp3";
                    }else{
                        $video_path = self::getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . $videoFileNameEncoded . ".mp4");
                        $lvideo_path = self::getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . $videoFileNameEncoded . ".mp4", "", 600, true);
                        $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".mp4";
                    }

                    
                }
            } else {

                if ($videoFilePath['dirname'] == ".") {
                    if($videoFileExtension =='mp4'){
                        $video_path = self::getSecureAmazonUrl(($videoFileName) . ".mp4");
                        $lvideo_path = self::getSecureAmazonUrl(($videoFileName) . ".mp4");
                        $relative_video_path = $videoFileName . ".mp4";
                    }elseif($videoFileExtension =='mp3'){
                        $video_path = self::getSecureAmazonUrl(($videoFileName) . ".mp3");
                        $lvideo_path = self::getSecureAmazonUrl(($videoFileName) . ".mp3");
                        $relative_video_path = $videoFileName . ".mp3";
                    }else{
                        $video_path = self::getSecureAmazonUrl(($videoFileName) . ".mp4");
                        $lvideo_path = self::getSecureAmazonUrl(($videoFileName) . ".mp4");
                        $relative_video_path = $videoFileName . ".mp4";
                    }
                   
                } else {
                    if($videoFileExtension =='mp4'){
                        $video_path = self::getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileName) . ".mp4");
                        $lvideo_path = self::getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileName) . ".mp4");
                        $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".mp4";
                    }elseif($videoFileExtension =='mp3'){
                        $video_path = self::getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileName) . ".mp3");
                        $lvideo_path = self::getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileName) . ".mp3");
                        $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".mp3";
                    }else{
                        $video_path = self::getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileName) . ".mp4");
                        $lvideo_path = self::getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileName) . ".mp4");
                        $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".mp4";
                    }
                   
                }
            }
            if ($thumbnail_image_path == '' || self::url_exists($thumbnail_image_path, $force_default) == false)
                $thumbnail_image_path = $default_thumbnail;
            
            
            if(strpos($thumbnail_image_path, 'video-thumbnail-sibme.svg') !== false){
                $thumnail_type = 'default'; 
            }
            
            

            return array(
                'url' => $video_path,
                'relative_url' => $relative_video_path,
                'thumbnail' => $thumbnail_image_path,
                'duration' => $duration,
                'encoder_status' => (isset($document_video['encoder_status']) ? $document_video['encoder_status'] : ''),
                'thumbnail_type' => $thumnail_type
            );
        } else {
            return array(
                'url' => '',
                'relative_url' => '',
                'thumbnail' => $default_thumbnail,
                'duration' => '',
                'encoder_status' => 'Error',
                'thumbnail_type' => $thumnail_type
            );
        }
    }

    public static function url_exists($url, $force_default) {
        if ($force_default) {
            return false;
        }
        $hdrs = @get_headers($url);
        return is_array($hdrs) ? preg_match('/^HTTP\\/\\d+\\.\\d+\\s+2\\d\\d\\s+.*$/', $hdrs[0]) : false;
    }

    function get_framework_settings(Request $request) {


        $input = $request->all();

        $account_id = $input['account_id'];
        $huddle_id = $input['huddle_id'];
        $video_id = $input['video_id'];

//        $account_id ='181';
//        $huddle_id = '56177' ;

        $id_framework = AccountFolderMetaData::where(array(
                    'account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id', 'site_id' => $this->site_id
                ))->first();

        if ($id_framework) {
            $id_framework->toArray();
        }

        $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";


        if ($framework_id == -1 || $framework_id == 0) {

            $id_framework = AccountMetaData::where(array(
                        'account_id' => $account_id, 'meta_data_name' => 'default_framework', 'site_id' => $this->site_id
                    ))->first();


            if ($id_framework) {
                $id_framework->toArray();
            }

            $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";
        }

        $account_tag_id = $framework_id;

        $video_data = Document::where(array(
                    'id' => $video_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($video_data) {
            $video_data = $video_data->toArray();
        }

        if ($video_data['post_rubric_per_video'] == '1') {


            $account_folder_details = AccountFolderDocument::where(array(
                        'document_id' => $video_id,
                        'site_id' => $this->site_id
                    ))->first();

            if ($account_folder_details) {
                $account_folder_details = $account_folder_details->toArray();
            }

            if (!empty($account_folder_details)) {
                if (!empty($account_folder_details['video_framework_id'])) {
                    $account_tag_id = $account_folder_details['video_framework_id'];
                }
            }
        }



        if (!empty($account_tag_id)):
            $framework_settings = array();

            $account_tag_details = array();

            $acc_tag_type_h = AccountTag::select('AccountTag.*')->where(array(
                        'AccountTag.framework_id' => $account_tag_id,
                        'AccountTag.site_id' => $this->site_id,
                            // 'AccountTag.tag_type' => 0,
                    ))->whereRaw('parent_account_tag_id IS NULL')->orderBy('standard_position')->get()->toArray();



            if (count($acc_tag_type_h) > 0) {
                foreach ($acc_tag_type_h as $act) {
                    $act['tag_html'] = $act['tag_title'];
                    $account_tag_details[] = $act;
                    $acc_tag_type_c = AccountTag::select('AccountTag.*')->where(array(
                                'AccountTag.framework_id' => $account_tag_id,
                                'AccountTag.site_id' => $this->site_id,
                                    // 'AccountTag.tag_type' => 0,
                            ))->whereRaw('parent_account_tag_id = ' . $act['account_tag_id'])->orderBy('standard_position')->get()->toArray();

                    if (count($acc_tag_type_c) > 0) {
                        foreach ($acc_tag_type_c as $act_c) {
                            $account_tag_details[] = $act_c;
                        }
                    }
                }
            } else {
                $acc_tag_type_0 = AccountTag::select('AccountTag.*')->where(array(
                            'AccountTag.framework_id' => $account_tag_id,
                            'AccountTag.tag_type' => 0,
                            'AccountTag.site_id' => $this->site_id
                        ))->orderBy('standard_position')->get()->toArray();
                if (count($acc_tag_type_0) > 0) {

                    foreach ($acc_tag_type_0 as $act_0) {
                        $account_tag_details[] = $act_0;
                    }
                }
            }



            $framework_settings['account_tag_type_0'] = $account_tag_details;


            $account_framework_settings = AccountFrameworkSetting::select('AccountFrameworkSetting.id', 'AccountFrameworkSetting.account_id', 'AccountFrameworkSetting.account_tag_id', 'AccountFrameworkSetting.published_at', 'AccountFrameworkSetting.updated_at', 'AccountFrameworkSetting.published', 'AccountFrameworkSetting.framework_name', 'AccountFrameworkSetting.enable_unique_desc', 'AccountFrameworkSetting.enable_ascending_order', 'AccountFrameworkSetting.enable_performance_level', 'AccountFrameworkSetting.tier_level', 'AccountFrameworkSetting.checkbox_level', 'AccountFrameworkSetting.parent_child_share')->where(array(
                        'AccountFrameworkSetting.account_tag_id' => $account_tag_id,
                        'AccountFrameworkSetting.site_id' => $this->site_id,
                    ))->first();

            if ($account_framework_settings) {
                $account_framework_settings = $account_framework_settings->toArray();
            }



            $framework_settings['account_framework_settings'] = is_null($account_framework_settings) ? [] : $account_framework_settings;
            if (!empty($account_framework_settings)) {

                $account_framework_settings_performance_levels = AccountFrameworkSettingPerformanceLevel::select('AccountFrameworkSettingPerformanceLevel.*')->where(array(
                            'AccountFrameworkSettingPerformanceLevel.account_framework_setting_id' => $account_framework_settings['id'],
                            'AccountFrameworkSettingPerformanceLevel.site_id' => $this->site_id
                        ))->orderBy('performance_level_rating')->get()->toArray();


                if (!empty($framework_settings['account_tag_type_0'])) {
                    $single_type_0_array = array();
                    foreach ($framework_settings['account_tag_type_0'] as $single_acc_tag_type_0):
                        if ($single_acc_tag_type_0['standard_level'] == $account_framework_settings['checkbox_level']) {
                            $single_acc_tag_type_0['account_framework_settings_performance_levels'] = $account_framework_settings_performance_levels;
                        } else {
                            $single_acc_tag_type_0['account_framework_settings_performance_levels'] = array();
                        }
                        $single_type_0_array[] = $single_acc_tag_type_0;
                    endforeach;
                    $framework_settings['account_tag_type_0'] = $single_type_0_array;
                }
//                $framework_settings['account_framework_settings_performance_levels'] = $account_framework_settings_performance_levels;
                if ($account_framework_settings['enable_unique_desc'] == 1) {

                    $acc_tag_type_0_tag_ids = array();
                    if (!empty($acc_tag_type_0)) {
                        foreach ($acc_tag_type_0 as $acc_tag_type_0_single):
                            array_push($acc_tag_type_0_tag_ids, $acc_tag_type_0_single['account_tag_id']);
                        endforeach;
                    }
                    if (!empty($acc_tag_type_0_tag_ids)) {
                        $account_framework_settings_performance_levels_ids = array();
                        if (!empty($account_framework_settings_performance_levels)) {
                            foreach ($account_framework_settings_performance_levels as $single_performance_level):
                                array_push($account_framework_settings_performance_levels_ids, $single_performance_level['id']);
                            endforeach;
                        }

                        $account_framework_settings_performance_levels_ids = implode(',', $account_framework_settings_performance_levels_ids);
                        $acc_tag_type_0_tag_ids = implode(',', $acc_tag_type_0_tag_ids);

                        $performance_level_descriptions = PerformanceLevelDescription::select('PerformanceLevelDescription.*')->where('PerformanceLevelDescription.site_id', $this->site_id)->whereRaw(
                                        'PerformanceLevelDescription.performance_level_id IN (' . $account_framework_settings_performance_levels_ids . ')')->whereRaw(
                                        'PerformanceLevelDescription.account_tag_id IN (' . $acc_tag_type_0_tag_ids . ')')->get()->toArray();

                        if (!empty($framework_settings['account_tag_type_0']) && !empty($performance_level_descriptions)) {
                            $single_type_0_array = array();
                            foreach ($framework_settings['account_tag_type_0'] as $single_acc_tag_type_0):
                                $performance_level_descriptions_arr = array();
                                foreach ($performance_level_descriptions as $single_performance_level_description):
                                    if ($single_acc_tag_type_0['standard_level'] == $account_framework_settings['checkbox_level'] && $single_acc_tag_type_0['account_tag_id'] == $single_performance_level_description['account_tag_id']) {
                                        array_push($performance_level_descriptions_arr, $single_performance_level_description);
                                    }
                                endforeach;
                                $single_acc_tag_type_0['performance_level_descriptions'] = $performance_level_descriptions_arr;
                                $single_type_0_array[] = $single_acc_tag_type_0;
                            endforeach;
                            $framework_settings['account_tag_type_0'] = $single_type_0_array;
                        }
//                        $framework_settings['performance_level_descriptions'] = $performance_level_descriptions;
                    }
                }
            }
            if (!empty($framework_settings)) {
                $result['status'] = true;
                $result['msg'] = "Account framework settings fetched successfully.";
                $result['data'] = $framework_settings;
            } else {
                $result['status'] = true;
                $result['msg'] = "Nothing found for your request.";
            }
        else:
            $result['status'] = false;
            $result['msg'] = "Please provide a valid account tag id.";
        endif;
        return response()->json($result);
    }

    function get_comments_count($video_id, $user_id) {


        $result = $this->getVideoComments($video_id, '', '', '', '', 1);


        $account_folder_details = AccountFolderDocument::where(array(
                    'document_id' => $video_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($account_folder_details) {
            $account_folder_details = $account_folder_details->toArray();
        }

        $account_folder_id = $account_folder_details['account_folder_id'];
        $total_comments_reply = 0;

        $h_type = AccountFolderMetaData::getMetaDataValue($account_folder_id, $this->site_id);

        if ((!$this->check_if_evalutor($account_folder_id, $user_id)) && (($h_type == '2' && $this->is_enabled_coach_feedback($account_folder_id, $this->site_id)) || $h_type == '3')) {


            $comment_result_count = Comment::where(array(
                        'ref_id' => $video_id,
                        'active' => 1,
                        'site_id' => $this->site_id
                    ))->whereIN('ref_type', ['2', '3', '6'])->get()->toArray();
        } else {
            $comment_result_count = Comment::where(array(
                        'ref_id' => $video_id,
                        'site_id' => $this->site_id
                    ))->whereIN('ref_type', ['2', '3', '6'])->get()->toArray();
        }

        $total_comments_reply = count($comment_result_count);

        $total_comments = '';
        $response = array();

        $total_commnets_count = 0;


        if (count($comment_result_count) > 0) {
            $response['total_comments'] = $total_comments_reply;
        } else {
            $response['total_comments'] = 0;
        }

        return $response;
    }

    function get_huddle_permissions($huddle_id, $user_id) {

        $result = AccountFolderUser::where(array(
                    'account_folder_id' => $huddle_id,
                    'user_id' => $user_id,
                    'site_id' => $this->site_id
                ))->get()->toArray();



        $result_groups = AccountFolderGroup::join('user_groups as ug', 'ug.group_id', '=', 'account_folder_groups.group_id')->where(array(
                    'ug.user_id' => $user_id,
                    'account_folder_groups.account_folder_id' => $huddle_id,
                    'account_folder_groups.site_id' => $this->site_id
                ))->get()->toArray();


        if ($result) {
            if (isset($result[0]['role_id']) && !empty($result[0]['role_id'])) {
                return $result[0]['role_id'];
            } else {
                $empty_variable = '';
                return $empty_variable;
            }
        } elseif ($result_groups) {
            if (isset($result_groups[0]['role_id']) && !empty($result_groups[0]['role_id'])) {
                return $result_groups[0]['role_id'];
            } else {
                $empty_variable = '';
                return $empty_variable;
            }
        } else {
            return FALSE;
        }
    }

    function get_replies_from_comment($comment, $get_all_comments,$cloudfront = false) {
        
        if ($comment['ref_type'] == 6 && $cloudfront) 
        {
            $comment['comment'] = $this->get_cloudfront_url($comment['comment']);
        }
        $result = $this->commentReplys($comment['id'], $get_all_comments);
        $responses = array();
        foreach ($result as $row) {
            $row = $this->get_replies_from_comment($row, $get_all_comments);
            if ($row['ref_type'] == 6) {
                $row['Comment']['comment'] = $this->get_cloudfront_url($row['comment']);
                if($cloudfront)    
                {
                    $row['comment'] = $this->get_cloudfront_url($row['comment']);
                }
            }
            $row['created_date_string'] = $this->times_ago($row['created_date']);
            ;

            $responses[] = $row;
        }
        $comment['responses'] = $responses;
        return $comment;
    }

    function commentReplys($comment_id, $get_all_comments = 0) {
        $comment_id = (int) $comment_id;
        $active = "0";
        if ($get_all_comments == 1) {
            //$active = array(0, 1);
            //$active = implode(',', $active);
            $active = "'0','1',''";
        } else {
            //$active = array(1);
            //$active = implode(',', $active);
            $active = "'1'";
        }

        // $data = DB::select('call getusercommentreplies(?,?)',array($comment_id, $active));
        // $data = $this->convert_array($data);
        // return $data;
        $result = Comment::leftJoin('users as User', 'User.id', '=', 'Comment.user_id')
                        ->leftJoin('comment_attachments as CommentAttachment', 'Comment.id', '=', 'CommentAttachment.comment_id')
                        ->leftJoin('documents as Document', 'CommentAttachment.document_id', '=', 'Document.id')
                        ->select('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image', 'Document.url', 'Document.original_file_name', 'Document.id as document_id')
                        ->where(array('Comment.parent_comment_id' => $comment_id))
                        ->where(array('Comment.site_id' => $this->site_id))
                        ->whereRaw('Comment.active IN (' . $active . ')')
                        ->whereRaw('user_id!=""')
                        ->orderBy('Comment.created_date')
                        ->groupBy('Comment.id')
                        ->get()->toArray();

        return $result;
    }

    function editComment(Request $request) {
        $result = array();

        $input = $request->all();
        $document_comment = false;
        if(isset($input['document_commenting']) || (isset($input['platform']) && $input['platform'] == 'document-commenting' ))
        {
            $document_comment = true;
        }
        if (!isset($input['comment_id'])) {
            $result['status'] = "failed";
            return $result;
        }
        $comment_id = $input['comment_id'];
        $get_custom_marker = $this->gettagsbycommentid($comment_id, array('2'));
        if(!empty($get_custom_marker))
        {
            $no_custom_marker = false;
        }
        else
        {
            $no_custom_marker = true;
        }
        
        if(!empty($input['standards_acc_tags']))
        {
            $no_frameworks = false;
        }
        else
        {
            $no_frameworks = true;
        }

        if($document_comment && $no_custom_marker && $no_frameworks && empty($input['comment']) && empty($input['default_tags']))
        {
            $result['status'] = "empty";
            $result['msg'] = "Comment cannot be empty.";
            return $result;
        }


         if (!isset($input['created_at_gmt']) || empty($input['created_at_gmt'])){
              $template="<b>This user is using older version of app</b><br>
                         <b>Form Fields : </b>".json_encode($input)." <br> 
                         <b>URL :</b>".$request->url();
                    $emailData = [
                            'from' => Sites::get_site_settings('static_emails', $this->site_id)['noreply'],
                            'from_name' => Sites::get_site_settings('site_title', $this->site_id),
                            'to' => 'khurrams@sibme.com',
                            'cc' => 'waqasn@sibme.com',
                            'subject' => "created_at_gmt is required for edit comment",
                            'template' => $template
                        ];

                 Email::sendCustomEmail($emailData);
          }else{
               $comment_exist = Comment::where(array(
                        'id' => $comment_id,
                        'created_at_gmt' => $input['created_at_gmt'],
                    ))->first();
                if(!empty($comment_exist)){
                    $latest_comment = $this->getSingleCommentDetails($request);
                    $result['updated_comment'] = $latest_comment;
                    $result['code'] = 409;
                    $result['status']= 'success';
                    $result['message']= 'comment already edited';
                    return $result;
               }
          }
        if (isset($input['synchro_time']) && $input['synchro_time'] != '') {
            $data = array(
                'comment' => $input['comment'],
                'last_edit_date' => date('Y-m-d H:i:s'),
                'last_edit_by' => $input['user_id'],
                'time' => $input['synchro_time'],
                'site_id' => $this->site_id
            );
        } else {
            $data = array(
                'comment' => $input['comment'],
                'last_edit_date' => date('Y-m-d H:i:s'),
                'last_edit_by' => $input['user_id'],
                'site_id' => $this->site_id
            );
        }

        if (isset($input['end_time']) && $input['end_time'] != '') {
            $data['end_time'] = $input['end_time'];
        }

        if (isset($input['audio_duration']) && $input['audio_duration'] > 0) {
            $data['audio_duration'] = $input['audio_duration'];
        }
        $data["encoder_status"] = "Complete";
        if (strpos($input['comment'], '.m4a') === (strlen($input['comment']) - 4) || strpos($input['comment'], '.mp3') === (strlen($input['comment']) - 4) ||
                strpos($input['comment'], '.webm') === (strlen($input['comment']) - 5) || strpos($input['comment'], '.ogg') === (strlen($input['comment']) - 4) ||
                strpos($input['comment'], '.wav') === (strlen($input['comment']) - 4)) {
            $data['ref_type'] = 6;
            $old_comment = DB::table('comments')->where(['id' => $comment_id])->first();
            if ($old_comment->comment != $input['comment']) {//New audio recorded in edit comment, so submit for transcoding
                $s3_service = new S3Services(
                        config('s3.amazon_base_url'), config('s3.bucket_name'), config('s3.access_key_id'), config('s3.secret_access_key')
                );
                $full_path = $input['comment'];
                $videoFilePath = pathinfo($full_path);
                $new_path = $videoFilePath['dirname'] . "/" . $videoFilePath['filename'];
                $s_url = config('s3.amazon_base_url') . $full_path;
                $s3_zencoder_id = $s3_service->encode_audios($s_url, $videoFilePath['dirname'], $new_path);
                $data["zencoder_output_id"] = $s3_zencoder_id;
                if($document_comment)
                {
                    $data["encoder_status"] = "Processing";
                }
                else
                {
                    $data["encoder_status"] = "Processing";
                }
                
                if(isset($input['environment_type']) && $input['environment_type'] == '1' && $document_comment)
                {
                    $data["encoder_status"] = "Complete";
                }
                
                $data["comment"] = $full_path;
            }
        } else {
//            $data['ref_type'] = 2;
            $data['ref_type'] = $input['ref_type'];
        }
         if (isset($input['created_at_gmt']) && !empty($input['created_at_gmt'])) {
            $data['created_at_gmt'] = $input['created_at_gmt'];
        }
         if (isset($input['comment_status']) && !empty($input['comment_status'])) {
            $data['comment_status'] = $input['comment_status'];
        }

        if(isset($input['xPos']))
        {
            $data['xPos'] = $input['xPos'];
        }

        if(isset($input['yPos']))
        {
            $data['yPos'] = $input['yPos'];
        }

        if(isset($input['page']))
        {
            $data['page'] = $input['page'];
        }

        if(isset($input['width']))
        {
            $data['width'] = $input['width'];
        }
        if(isset($input['height']))
        {
            $data['height'] = $input['height'];
        }
        if(isset($input['type']))
        {
            $data['type'] = $input['type'];
        }
        
        if (isset($input['annotation_data']) && $input['annotation_data'] != '') {
            $data['annotation_data'] = json_encode($input['annotation_data']);
        }

        if (DB::table('comments')->where(['id' => $comment_id])->update($data)) {
            DB::table('user_activity_logs')->where(['ref_id' => $comment_id])->update(array('desc' => $input['comment']));
            $tags = isset($input['default_tags']) ? $input['default_tags'] : '';
            $standards = isset($input['standards']) ? $input['standards'] : '';
            if (!empty($tags) && isset($input['account_id'])) {
                $this->update_tag($tags, $comment_id, $input['videoId'], $input['account_id'], $input['user_id']);
            } elseif (isset($input['default_tags']) && empty($input['default_tags'])) {
                DB::table('account_comment_tags')->where(array('comment_id' => $comment_id, "ref_type" => "1", 'site_id' => $this->site_id))->delete();
            }
            $standards_acc_tags = isset($input['standards_acc_tags']) ? $input['standards_acc_tags'] : '';
            if (!empty($standards_acc_tags) && isset($input['account_id'])) {
                if (isset($input['changed_standards']) && $input['changed_standards']) {
                    $user_role = $this->get_user_role_name($input['account_role_id']);
                    $in_trial_intercom = $this->check_if_account_in_trial_intercom($input['account_id']);
                    $meta_data = array(
                        'framework_added_in_comment' => 'true',
                        'user_role' => $user_role,
                        "is_in_trial" => $in_trial_intercom,
                        'Platform' => 'Web'
                    );

                    $this->create_intercom_event('framework-added-in-comment', $meta_data, $input['current_user_email']);
                }
                $previous_standards_ids = array();
                $previous_standards = AccountCommentTag::where(array('comment_id' => $comment_id, "ref_type" => "0", 'site_id' => $this->site_id))->get()->toArray();
                foreach ($previous_standards as $prev) {
                    if(isset($prev['account_tag_id']) && $prev['account_tag_id'] !=''){
                        $previous_standards_ids[] = $prev['account_tag_id'];
                    }
                    
                }

                DB::table('account_comment_tags')->where(array('comment_id' => $comment_id, "ref_type" => "0", 'site_id' => $this->site_id))->delete();

                if (isset($input['huddle_id'])) {
                    $this->save_standard_tag($standards, $comment_id, $input['videoId'], $input['account_id'], $input['user_id'], $input['huddle_id'], $standards_acc_tags, $previous_standards_ids);
                } else {
                    $this->save_standard_tag($standards, $comment_id, $input['videoId'], $input['account_id'], $input['user_id'], '', $standards_acc_tags, $previous_standards_ids);
                }
            } elseif (isset($standards_acc_tags) && empty($standards_acc_tags)) {
                DB::table('account_comment_tags')->where(array('comment_id' => $comment_id, "ref_type" => "0"))->delete();
            }

            $tags_1 = isset($input['assessment_value']) ? $input['assessment_value'] : '';
            if (!empty($tags_1)) {
                if(!$document_comment)
                {
                    if (isset($input['changed_custom_markers']) && $input['changed_custom_markers']) {
                        $user_role = $this->get_user_role_name($input['account_role_id']);
                        $in_trial_intercom = $this->check_if_account_in_trial_intercom($input['account_id']);

                        $meta_data = array(
                            'custom_marker_tags_added' => 'true',
                            'user_role' => $user_role,
                            "is_in_trial" => $in_trial_intercom,
                            'Platform' => 'Web'
                        );

                        $user_detail = User::where(array('id' => $input['user_id'], 'site_id' => $this->site_id))->first();

                        if ($user_detail) {

                            $user_detail = $user_detail->toArray();
                            $this->create_churnzero_event('Custom+Marker+Tagged', $input['account_id'], $user_detail['email']);
                        }

                        $this->create_intercom_event('custom-marker-tags-added', $meta_data, $input['current_user_email']);
                    }

                    $this->save_tag_2($tags_1, $comment_id, $input['videoId'], $input['account_id'], $input['user_id']);
                }
            } else {
               if(!$document_comment)
               {
                    DB::table('account_comment_tags')->where(array('comment_id' => $comment_id, "ref_type" => "2", 'site_id' => $this->site_id))->delete();
               }
            }



            $latest_comment = $this->getSingleCommentDetails($request);
            $result['updated_comment'] = $latest_comment;
            $result['status'] = "success";
            $response = $this->get_replies_from_comment($latest_comment, '1');
            $replies_count = count($response['responses']);
            if ($latest_comment['ref_type'] == 6) {
                $latest_comment['comment'] = $this->get_cloudfront_url($latest_comment['comment']);
            }
            $event = [
                'channel' => 'video-details-' . $latest_comment['ref_id'],
                'event' => "comment_edited",
                'data' => $latest_comment,
                'comment' => $input['comment'],
                'item_id' => $comment_id,
                'is_workspace_comment' => $latest_comment['is_workspace_comment'],
                'reference_id' => $latest_comment['ref_id'],
                'video_id' => $latest_comment['ref_id'],
                'replies_count' => $replies_count
            ];
            HelperFunctions::broadcastEvent($event);
        } else {
            $result['status'] = "failed";
        }
        return $result;
    }

    function update_tag($tags, $comment_id, $ref_id, $account_id, $user_id) {
        $tag = str_replace('# ', '', $tags);
        $taginfos = explode(',', $tag);
        foreach ($taginfos as $info) {

            $get_tag_info = '';
            DB::table('account_comment_tags')->where(array('comment_id' => $comment_id, "ref_type" => "1", 'site_id' => $this->site_id))->delete();

            //$get_tag_info = $this->gettagbytitle($info, $account_id);

            if (!empty($get_tag_info)) {
                $tag_exist = AccountCommentTag::where(array(
                            'account_tag_id' => $get_tag_info['account_tag_id'], 'comment_id' => $comment_id,
                            'site_id' => $this->site_id,
                        ))->count();
                if ($tag_exist == 0) {
                    $save_cmt_tag[] = array(
                        "comment_id" => $comment_id,
                        'site_id' => $this->site_id,
                        "ref_type" => "1",
                        "ref_id" => $ref_id,
                        "tag_title" => $get_tag_info['tag_title'],
                        "account_tag_id" => $get_tag_info['account_tag_id'],
                        "created_by" => $user_id,
                        "created_date" => date('y-m-d H:i:s', time())
                    );

                    $user_activity_logs = array(
                        'ref_id' => $comment_id,
                        'site_id' => $this->site_id,
                        'desc' => $get_tag_info['tag_title'],
                        'url' => '/' . $comment_id,
                        'type' => '14',
                        'account_id' => $account_id,
                        'account_folder_id' => $get_tag_info['account_tag_id'],
                        'environment_type' => 2
                    );
                    $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
                }
            } else {
                $save_cmt_tag[] = array(
                    "comment_id" => $comment_id,
                    'site_id' => $this->site_id,
                    "ref_type" => "1",
                    "ref_id" => $ref_id,
                    "tag_title" => $info,
                    "created_by" => $user_id,
                    "created_date" => date('y-m-d H:i:s', time())
                );

                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'site_id' => $this->site_id,
                    'desc' => $info,
                    'url' => '/' . $comment_id,
                    'type' => '14',
                    'account_id' => $account_id,
                    'account_folder_id' => '',
                    'environment_type' => 2
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            }
        }
        if (DB::table('account_comment_tags')->insert($save_cmt_tag)) {
            return true;
        } else {
            return false;
        }
    }

    function save_tag_2($tags, $comment_id, $ref_id, $account_id, $user_id) {

        $tag = str_replace('# ', '', $tags);
        $taginfos = explode(',', $tag);
        DB::table('account_comment_tags')->where(array('comment_id' => $comment_id, "ref_type" => "2", 'site_id' => $this->site_id))->delete();
        foreach ($taginfos as $info) {

            $get_tag_info = '';


            $get_tag_info = $this->gettagbytitle($info, $account_id);


            if (!empty($get_tag_info)) {
                $save_cmt_tag[] = array(
                    "comment_id" => $comment_id,
                    'site_id' => $this->site_id,
                    "ref_type" => "2",
                    "ref_id" => $ref_id,
                    "tag_title" => $get_tag_info['tag_title'],
                    "account_tag_id" => $get_tag_info['account_tag_id'],
                    "created_by" => $user_id,
                    "created_date" => date('y-m-d H:i:s', time())
                );

                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'site_id' => $this->site_id,
                    'desc' => $get_tag_info['tag_title'],
                    'url' => '/' . $comment_id,
                    'type' => '14',
                    'account_id' => $account_id,
                    'account_folder_id' => $get_tag_info['account_tag_id'],
                    'environment_type' => 1
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            } else {
                $save_cmt_tag[] = array(
                    "comment_id" => $comment_id,
                    'site_id' => $this->site_id,
                    "ref_type" => "2",
                    "ref_id" => $ref_id,
                    "tag_title" => $info,
                    "created_by" => $user_id,
                    "created_date" => date('y-m-d H:i:s', time())
                );

                $user_activity_logs = array(
                    'ref_id' => $comment_id,
                    'site_id' => $this->site_id,
                    'desc' => $info,
                    'url' => '/' . $comment_id,
                    'type' => '14',
                    'account_id' => $account_id,
                    'account_folder_id' => '',
                    'environment_type' => 1
                );
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            }
        }
        if (DB::table('account_comment_tags')->insert($save_cmt_tag)) {
            return true;
        } else {
            return false;
        }
    }


    function cake_multiple_upload_documents_mobile_emails_to_queues(Request $request){
        $input = $request->all();
        $user_id = $input['user_id'];
        $account_id = $input['account_id'];
        $account_folder_id = $input['account_folder_id'];
        $site_id = $input['site_id'];
        $video_id = $input['video_id'];

        $is_email_notification_allowed = HelperFunctions::is_email_notification_allowed($account_id);
        if(!$is_email_notification_allowed){
            return response()->json(["status"=>true,"message"=>"emails are off for this account."]);
        }

        $documentUploader = new \App\Services\DocumentUploader();
        $documentUploader->cake_send_emails_to_queue( $user_id, $account_id, $account_folder_id, $video_id, $site_id);
        $res = ["success"=>true];
        return response()->json($res);
        // return true;
        //return $res;
    }

    function uploadDocuments(Request $request) {
        $documentUploader = new \App\Services\DocumentUploader();
//        $account_folder_id = "138804";
//        $account_id = "2936";
//        $user_id = "18737";
//        $video_id = "220235";
//        $url_stack = 1;
//        $workspace = "";
//
//        $url = "https://cdn.filestackcontent.com/z8ibbw6TiWXctFhvNbH4";
//        $video_url = "tempupload/2936/2018/07/02/vp7EMC3XQxOOUeEofUH5_CV - Saad Zia.pdf";
//        $video_file_name = "CV - Saad Zia.pdf";
//        $video_file_size = "40682";
//        $video_desc = "";
//        $current_user_role_id = "100";
//        $current_user_email = "testinglandingpage9@gmail.com.sg";
        $input = $request->all();

        $account_folder_id = isset($input['huddle_id'])?$input['huddle_id']:'';
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $video_id = $input['video_id'];
        $workspace = '';
        $url = $input['stack_url'];
        // $video_url = $input['video_url'];
        $video_url = $request->get('video_url');
        $fileStack_handle = $request->get('fileStack_handle');
        $fileStack_url = $request->get('fileStack_url');
        $video_file_name = $input['video_file_name'];
        $video_file_size = $input['video_file_size'];
        $video_desc = isset($input['video_desc']) ? $input['video_desc'] : "";
        $current_user_role_id = $input['current_user_role_id'];
        $current_user_email = $input['current_user_email'];
        $url_stack = $input['url_stack_check'];
        $slot_index = $request->has("slot_index") ? $request->get("slot_index") : 0;
        $parent_folder_id = $request->has("parent_folder_id") ? $request->get("parent_folder_id") : 0;

        if (empty($input['huddle_id'])) {
            $data = array(
                'account_id' => $account_id,
                'folder_type' => 3,
                'name' => $video_file_name,
                'desc' => '',
                'active' => 1,
                'created_date' => date("Y-m-d H:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d H:i:s"),
                'last_edit_by' => $user_id,
                'site_id' => $this->site_id
            );

            $af_id = DB::table('account_folders')->insertGetId($data);
        }
        $account_folder_id = $input['huddle_id'] ? $input['huddle_id'] : $af_id;
        if (isset($input['comment_id'])) {
            $comment_id = $input['comment_id'];
        } else {
            $comment_id = '';
        }
        if (isset($input['time'])) {
            $time = $input['time'];
        } else {
            $time = '';
        }
        if (isset($input['sample']) && !empty($input['sample'])) {
            $is_sample = (string) $input['sample'];
        } else {
            $is_sample = '0';
        }

        $huddle_detail = AccountFolder::where(array('account_folder_id' => $account_folder_id, 'site_id' => $this->site_id))->first();

        if ($huddle_detail) {
            $huddle_detail = $huddle_detail->toArray();
        }

        if ($huddle_detail['folder_type'] == '3') {
            $workspace = true;
        } else {
            $workspace = false;
        }

        if ($workspace) {
            $user_role = $this->get_user_role_name($current_user_role_id);
            $in_trial_intercom = $this->check_if_account_in_trial_intercom($account_id);


            $meta_data = array(
                'workspace_resource' => 'true',
                'user_role' => $user_role,
                "is_in_trial" => $in_trial_intercom,
                'Platform' => 'Web'
            );

            $this->create_intercom_event('workspace-resource-uploaded', $meta_data, $current_user_email);
            $this->create_churnzero_event('Workspace+Resource+Uploaded', $account_id, $current_user_email);
        } else {
            $htype = AccountFolderMetaData::getMetaDataValue($account_folder_id, $this->site_id);
            if ($htype == 1) {
                $huddle_type = 'Collaboration Huddle';
            } elseif ($htype == 2) {
                $huddle_type = 'Coaching Huddle';
            } else {
                $huddle_type = 'Assessment Huddle';
            }

            $user_role = $this->get_user_role_name($current_user_role_id);
            $in_trial_intercom = $this->check_if_account_in_trial_intercom($account_id);


            $meta_data = array(
                'huddle_resource' => 'true',
                'huddle_type' => $huddle_type,
                'user_role' => $user_role,
                "is_in_trial" => $in_trial_intercom,
                'Platform' => 'Web'
            );
            $this->create_intercom_event('huddle-resource-uploaded', $meta_data, $current_user_email);
            $this->create_churnzero_event('Huddle+Resource+Uploaded', $account_id, $current_user_email);
        }

        $res = $documentUploader->uploadDocuments($this->site_id, $account_folder_id, $account_id, $user_id, $video_id, $url_stack, $workspace, $url, $video_url, $video_file_name, $video_file_size, $video_desc, $current_user_role_id, $current_user_email, $time, $comment_id, $is_sample, $slot_index, $fileStack_handle, $fileStack_url, $parent_folder_id);

        return response()->json($res);
    }

    function addURL(Request $request) {
        $input = $request->all();
        $parent_folder_id = $request->has("parent_folder_id") ? $request->get("parent_folder_id") : 0;

        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $title = $input['title'];
        $url = $input['url'];
        $current_user_role_id = $input['current_user_role_id'];
        $current_user_email = $input['current_user_email'];
        $slot_index = $request->has("slot_index") ? $request->get("slot_index") : 0;
        $environment_type = isset($input['environment_type']) ? $input['environment_type'] : 2;
        if (empty($input['huddle_id'])) {
            $data = array(
                'account_id' => $account_id,
                'folder_type' => 3,
                'name' => $title,
                'desc' => '',
                'active' => 1,
                'created_date' => date("Y-m-d H:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d H:i:s"),
                'last_edit_by' => $user_id,
                'site_id'=>$this->site_id
            );

            $af_id = DB::table('account_folders')->insertGetId($data);
        }
        $account_folder_id = !empty($input['huddle_id']) ? $input['huddle_id'] : $af_id;

        if (isset($input['sample']) && !empty($input['sample'])) {
            $is_sample = (string) $input['sample'];
        } else {
            $is_sample = '0';
        }

        $huddle_detail = AccountFolder::where(array('account_folder_id' => $account_folder_id, 'site_id' => $this->site_id))->first();

        if ($huddle_detail) {
            $huddle_detail = $huddle_detail->toArray();
        }

        if ($huddle_detail['folder_type'] == '3') {
            $workspace = true;
        } else {
            $workspace = false;
        }

/*
        if ($workspace) {
            $in_trial_intercom = $this->check_if_account_in_trial_intercom($account_id);

            $meta_data = array(
                'workspace_resource' => 'true',
                'user_role' => $user_role,
                "is_in_trial" => $in_trial_intercom,
                'Platform' => 'Web'
            );

            $this->create_intercom_event('workspace-resource-uploaded', $meta_data, $current_user_email);
            $this->create_churnzero_event('Workspace+Resource+Uploaded', $account_id, $current_user_email);

            $user_role = $this->get_user_role_name($current_user_role_id);
        } else {
            $htype = AccountFolderMetaData::getMetaDataValue($account_folder_id, $this->site_id);
            if ($htype == 1) {
                $huddle_type = 'Collaboration Huddle';
            } elseif ($htype == 2) {
                $huddle_type = 'Coaching Huddle';
            } else {
                $huddle_type = 'Assessment Huddle';
            }

            $user_role = $this->get_user_role_name($current_user_role_id);
            $in_trial_intercom = $this->check_if_account_in_trial_intercom($account_id);


            $meta_data = array(
                'huddle_resource' => 'true',
                'huddle_type' => $huddle_type,
                'user_role' => $user_role,
                "is_in_trial" => $in_trial_intercom,
                'Platform' => 'Web'
            );
            $this->create_intercom_event('huddle-resource-uploaded', $meta_data, $current_user_email);
            $this->create_churnzero_event('Huddle+Resource+Uploaded', $account_id, $current_user_email);
        }
*/


        $urlUploader = new \App\Services\URLUploader();
        if(!$urlUploader->validate_url($url)){
            return response()->json(['success'=>false, 'message'=>TranslationsManager::get_translation('invalid_url') ]);
        }
        $res = $urlUploader->addURL($this->site_id, $account_folder_id, $account_id, $user_id, $workspace, $url, $title, $current_user_role_id, $current_user_email, $is_sample, $slot_index, $environment_type, $parent_folder_id);
        if(!empty($res)&& isset($res['document_id'])){
            if(!empty($parent_folder_id))
            {
                DocumentFolder::where('id', $parent_folder_id)->update(['last_edit_date'=>date('Y-m-d H:i:s'), 'last_edit_by'=> $user_id]);
            }
            if(!empty($account_folder_id))//sw-5004
            {
                AccountFolder::where('account_folder_id', $account_folder_id)->update(['last_edit_date'=>date("Y-m-d H:i:s"), 'last_edit_by'=>$user_id]);
            }
        $s3_dest_folder_path_only = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
        $resourceCtrl = new ResourceController();
        $url_title = $title.'-'.time();
        $s3_path = $resourceCtrl->generateResourceThumbnail(5, $url_title, null, "uploads/" .$s3_dest_folder_path_only,$res['url']);
        if($s3_path)
        {
            Document::where('id',$res['document_id'])->update(['s3_thumbnail_url'=> "uploads/" .$s3_dest_folder_path_only.'/'.$url_title.'.jpg']);
        }
        $resp = Document::get_single_video_data($this->site_id, $res['document_id'], $huddle_detail['folder_type']);
        if($resp)
        {
                $data_ws = app('App\Http\Controllers\WorkSpaceController')->conversion_to_thmubs($resp[0]);
                $channel_name = 'huddle-details-' . $account_folder_id;
                if ($huddle_detail['folder_type'] == '3') {
                    $account_id = $account_id;
                    $channel_name = "workspace-" . $account_id . "-" . $user_id;
                }
                $event = [
                    'channel' => $channel_name,
                    'event' => "resource_renamed",
                    'data' => $data_ws,
                    'video_file_name' => $data_ws["title"],
                ];
                HelperFunctions::broadcastEvent($event);
        }
        }

        return response()->json($res);
    }

    function editURL(Request $request)
    {
        $input = $request->all();
        $document_id = $input['document_id'];
        $url = $input['url'];
        $title = $input['title'];
        $user_id = $input['user_id'];
        $account_id = $input['account_id'];

        $urlUploader = new \App\Services\URLUploader();
        if(!$urlUploader->validate_url($url)){
            return response()->json(['success'=>false, 'message'=>TranslationsManager::get_translation('invalid_url') ]);
        }

        $account_folder_details = AccountFolderDocument::where(array('document_id' => $document_id))->first();
        $account_folder_id = $account_folder_details['account_folder_id'];

        $previous_document = Document::where(array( 'id' => $document_id ))->first();
        $url_title = !empty($previous_document['s3_thumbnail_url']) ? basename($previous_document['s3_thumbnail_url'],'.jpg') : '';
        $file_title = !empty($url_title) ? $url_title : $title.'-'.time() ;
        $update_document_value = $previous_document->update(array('url' => $url,'last_edit_date'=>date("Y-m-d H:i:s"), 'last_edit_by'=>$user_id ));
        $update_account_folder_document_value = AccountFolderDocument::where(array( 'document_id' => $document_id ))->update(array('title' => $title ));

        if($update_account_folder_document_value || $update_document_value )
        {
            //Generate thumbnail
            $s3_dest_folder_path_only = !empty($url_title) ? dirname($previous_document['s3_thumbnail_url']) :  "uploads/$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
            $resourceCtrl = new ResourceController();
            $s3_path = $resourceCtrl->generateResourceThumbnail(5, $file_title, null, $s3_dest_folder_path_only,$url);
          if($s3_path)
            {
                Document::where('id',$document_id)->update(['s3_thumbnail_url'=> $s3_dest_folder_path_only.'/'.$file_title.'.jpg']);
            }
            //$previous_document->update(['last_edit_date'=>date("Y-m-d H:i:s"), 'last_edit_by'=>$user_id]);
            $final_result['success'] = true;
            $final_result['message'] = "URL updated successfully.";
        }
        else
        {
            $final_result['success'] = false;
            $final_result['message'] = "URL updated successfully.";
        }

        $folder_type = AccountFolder::get_folder_type($account_folder_id, $this->site_id);

        $resp = Document::get_single_video_data($this->site_id, $document_id, $folder_type);

         if($resp)
            {
                $data_ws = app('App\Http\Controllers\WorkSpaceController')->conversion_to_thmubs($resp[0]);
                $resp['updated_by'] = $user_id;
                $channel_name = 'huddle-details-' . $account_folder_id;
                if ($folder_type == '3') {
                    $account_id = $input["account_id"];
                    $channel_name = "workspace-" . $account_id . "-" . $user_id;
                }
                $event = [
                    'channel' => $channel_name,
                    'event' => "resource_renamed",
                    'data' => $data_ws,
                    'video_file_name' => $data_ws["title"],
                ];
                HelperFunctions::broadcastEvent($event);
            }

        return response()->json($final_result);
    }

    function downloadDocument(Request $request) {
        $input = $request->all();
        $user_id = $input['user_id'];
        $account_id = $input['account_id'];
        $id = $input['document_id'];
        $documentDownloader = new \App\Services\DocumentDownloader();
        $document = $documentDownloader->getDocument($id, $account_id, $user_id, $this->site_id);
        if (!$document) {
            return response()->json(['error' => 'File Not Found']);
        } elseif (isset($document['error']) && $document['error'] == '1') {
            return response()->json($document);
        }
        // return response()->download($document);

        // header('Location:' . $document);

        $file_name = Document::get_document_filename_with_extension($id);
        header("Content-Transfer-Encoding: Binary");
        header("Content-disposition: attachment; filename=\"".$file_name."\"");
        readfile($document);
        exit;
    }

    function deleteVideoDocument(Request $request) {
//        $document_id = '220336';
//        $video_id = '192112';
//        $account_folder_id = '47555';
        $input = $request->all();
        $document_id = $input['document_id'];
        $video_id = $input['video_id'];
        $account_folder_id = $input['huddle_id'];
        $comment_id = CommentAttachment::where('document_id',$document_id)->pluck('comment_id')->first();
        $document_count = CommentAttachment::where('comment_id',$comment_id)->pluck('document_id');
        $document_count = count($document_count);
        $account_folder_data_of_deleted_video = AccountFolderDocument::where(array(
                    'site_id' => $this->site_id,
                    'document_id' => $input['document_id']
                ))->first();
        $deleted_video_data = Document::where(array(
                        'site_id' => $this->site_id,
                        'id' => $input['document_id']
                    ))->first();
        $htype = AccountFolderMetaData::getMetaDataValue($account_folder_id, $this->site_id);
        $logged_in_user_huddle_role = HelperFunctions::get_huddle_roles($account_folder_id,$input['user_id']);
        if($input['user_id'] != $deleted_video_data['created_by'] || $logged_in_user_huddle_role == '210')
            {
                if($htype == 2)
                {
                    $user_who_deleted = User::where(array('site_id' => $this->site_id,'id' => $input['user_id']))->first();
                    $user_who_created = User::where(array('site_id' => $this->site_id,'id' => $deleted_video_data['created_by']))->first();
                    $huddle_detail = AccountFolder::where(array('site_id' => $this->site_id,'account_folder_id' => $account_folder_id))->first();
                    if($logged_in_user_huddle_role == '210')
                    {
                        QueuesManager::sendToEmailQueue(__CLASS__, "sendDeleteVideoCommentEmails", $this->site_id, $account_folder_id, $user_who_deleted, $huddle_detail, $account_folder_data_of_deleted_video);
                        // $this->sendDeleteVideoCommentEmails($this->site_id, $account_folder_id, $user_who_deleted, $huddle_detail, $account_folder_data_of_deleted_video);
                    }
                    else
                    {
                        if($this->site_id == '1')
                        {
                            if($user_who_created['lang'] == 'en')
                            {
                                 $email_subject = 'Sibme - Attachment Deleted';
                            }
                            else
                            {
                                 $email_subject = 'Sibme - Adjunto Eliminado';
                            }
                        }
                        else
                        {
                            $email_subject = 'HMH - Attachment Deleted';
                        }
                        $key = "delete_coaching_artifact_huddle_" . $this->site_id . "_" . $user_who_created['lang'];
                        $results = SendGridEmailManager::get_send_grid_contents($key);
                        $html = $results->versions[0]->html_content;
                        $html = str_replace('<%body%>', '', $html);
                        $html = str_replace('{user_who_deleted}',$user_who_deleted['first_name'] . ' ' . $user_who_deleted['last_name'], $html);
                        $html = str_replace('{huddle_name}',$huddle_detail['name'],$html);
                        $html = str_replace('{artifact_type}', 'attachment' ,$html);
                        $html = str_replace('{artifact_title}', $account_folder_data_of_deleted_video['title'] ,$html);

                        $sibme_base_url = config('s3.sibme_base_url');
                        $unsubscribe = $sibme_base_url . 'subscription/unsubscirbe_now/' . $user_who_created['id'] . '/12';
                        $html = str_replace('{unsubscribe}', $unsubscribe ,$html);
                        $send_to[] =  $user_who_created['email'];
                        $html = str_replace('{deleted_your}', 'deleted your' ,$html);
                        $emailData = [
                            'from' => Sites::get_site_settings('static_emails', $this->site_id)['noreply'],
                            'from_name' => Sites::get_site_settings('site_title', $this->site_id),
                            'to' => $user_who_created['email'],
                            'subject' => $email_subject,
                            'template' => $html,
                        ];
                        if(EmailUnsubscribers::check_subscription($user_who_created['id'], '12', $huddle_detail['account_id'], $this->site_id))
                        {
                            Email::sendCustomEmail($emailData);
                        }
                    }
                    

                }
            }

          $audit_deletion_log = array(
                'entity_id'=> $input['huddle_id'],
                'deleted_entity_id'=>$input['document_id'],
                'entity_name'=>$account_folder_data_of_deleted_video['title'],
                'entity_data'=>json_encode($deleted_video_data),
                'created_by'=>$input['user_id'],
                'created_at'=> date("Y-m-d H:i:s"),
                'site_id'=>$this->site_id,
                'ip_address'=>$_SERVER['REMOTE_ADDR'],
                'posted_data'=>json_encode($input),
                'action'=> $_SERVER['REQUEST_URI'],
                'user_agenet'=>$_SERVER['HTTP_USER_AGENT'],
                'request_url'=>$_SERVER['REQUEST_URI'],
                'platform'=>'',
                'created_date' => date("Y-m-d H:i:s"),
            );
            $flag = Document::deleteVideoDocument($document_id, $video_id, $account_folder_id, $this->site_id, $input['user_id']);
            $comment_deleted = Comment::where('id',$comment_id)->first();
            
            if(empty($comment_deleted->comment) && $document_count == 1 && !isset($input['attachment_delete_textarea'])){
                // DB::table('comments')->where(array('id' => $comment_id))->delete();
                DB::table('comment_attachments')->where(array('comment_id' => $comment_id))->delete();
                $request->request->add(['comment_id' => $comment_id]);
                $this->deleteVideoComments($request);
                // print_r($commentDeleted);
            }
            DB::table('comment_attachments')->where(array('document_id' => $document_id))->delete();
            if($flag)
            {
                 DB::table('audit_deletion_log')->insert($audit_deletion_log);
            }

        return response()->json(['error' => !$flag]);
    }

    function sendDeleteVideoCommentEmails($site_id, $account_folder_id, $user_who_deleted, $huddle_detail, $account_folder_data_of_deleted_video){
        $send_to = HelperFunctions::get_evaluator_emails($account_folder_id,'200');
        // \Log::info("--------- sendDeleteVideoCommentEmails ---------", [count($send_to)]);
        foreach($send_to as $row)
        {
            if($site_id == '1')
            {
                if($row['lang'] == 'en')
                {
                        $email_subject = 'Sibme - Attachment Deleted';
                }
                else
                {
                        $email_subject = 'Sibme - Adjunto Eliminado';
                }
            }
            else
            {
                $email_subject = 'HMH - Attachment Deleted';
            }
            $key = "delete_coaching_artifact_huddle_" . $site_id . "_" . $row['lang'];
            $results = SendGridEmailManager::get_send_grid_contents($key);
            $html = $results->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{user_who_deleted}',$user_who_deleted['first_name'] . ' ' . $user_who_deleted['last_name'], $html);
            $html = str_replace('{huddle_name}',$huddle_detail['name'],$html);
            $html = str_replace('{artifact_type}', 'attachment' ,$html);
            $html = str_replace('{artifact_title}', $account_folder_data_of_deleted_video['title'] ,$html);
            $html = str_replace('{deleted_your}', 'deleted' ,$html);
            $sibme_base_url = config('s3.sibme_base_url');
            $unsubscribe = $sibme_base_url . 'subscription/unsubscirbe_now/' . $row['id'] . '/12';
            $html = str_replace('{unsubscribe}', $unsubscribe ,$html);
            $emailData = [
            'from' => Sites::get_site_settings('static_emails', $site_id)['noreply'],
            'from_name' => Sites::get_site_settings('site_title', $site_id),
            'to' => $row['email'],
            'subject' => $email_subject,
            'template' => $html,
            ];
            if(EmailUnsubscribers::check_subscription($row['id'], '12', $huddle_detail['account_id'], $site_id))
            {
                Email::sendCustomEmail($emailData);
            }
            // \Log::info("--------- Sent to ".$row['email']." ---------");
        }
    }

    function getVideoDocuments(Request $request, $input = null) {
//        $document_id = '192112';
//        $account_folder_id = '47555';
        //dd("testing");
        //dd(SendGridEmailManager::get_send_grid_contents());
        if (empty($input)) {
            $input = $request->all();
        }

        $document_id = $input['video_id'];
        $account_folder_id = $input['huddle_id'];
        $user_id = $input['user_id'];
        $is_library = isset($input['library']) ? ($input['library'] == 1) : 0;
        $if_evaluator = $this->check_if_evalutor($account_folder_id, $user_id);
        $h_type = AccountFolderMetaData::getMetaDataValue($account_folder_id, $this->site_id);
        $is_coach_enable = $this->is_enabled_coach_feedback($account_folder_id, $this->site_id);
        $temp_attachments = Document::getVideoDocumentsByVideo($document_id, $account_folder_id, $user_id, $h_type, $if_evaluator, $is_coach_enable, $this->site_id,$is_library);
        $attachments = array();
        $temp_attachments = json_decode(json_encode($temp_attachments), True);
        $base = config('s3.sibme_base_url');
        $is_mobile_request = isset($input['app_name']) ? HelperFunctions::is_mobile_request($input['app_name']) : false;
        foreach ($temp_attachments as $index => $attachment)
                {
                        $attachments[$index] = $attachment;


                        if(!empty($attachment['stack_url']))
                        {
                          if($is_mobile_request)
                          {
                            $attachments[$index]['stack_url'] = $attachment['stack_url'];
                          }
                          else
                          {
                            $attachments[$index]['stack_url'] = $base . 'app/view_document/'.$attachment['stack_url'];
                          }
                        }
                        else {
                           $attachments[$index]['stack_url'] = '';
                        }


                        $ext = pathinfo($attachment['original_file_name'], PATHINFO_EXTENSION);
                        if ($ext == 'xls' || $ext == 'xlsx') {
                            $attachments[$index]['thubnail_url'] = $base . 'img/excel_gry_icon.svg';
                            $attachments[$index]['file_type'] = $ext;
                        } elseif ($ext == 'pdf') {
                            $attachments[$index]['thubnail_url'] = $base . 'img/pdf_gry_icon.svg';
                            $attachments[$index]['file_type'] = $ext;
                        } elseif ($ext == "pptx" || $ext == "ppt") {
                            $attachments[$index]['thubnail_url'] = $base . 'img/power_point.png';
                            $attachments[$index]['file_type'] = $ext;
                        } elseif ($ext == "docx" || $ext == "doc") {
                            $attachments[$index]['thubnail_url'] = $base . 'img/word_icon.png';
                            $attachments[$index]['file_type'] = $ext;
                        } elseif ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                            $attachments[$index]['thubnail_url'] = $base . 'img/image_icon.png';
                            $attachments[$index]['file_type'] = $ext;
                        } else {
                            $attachments[$index]['thubnail_url'] = $base . 'img/file_gry_icon.svg';
                            $attachments[$index]['file_type'] = $ext;
                        }
                }
        return response()->json($attachments);
    }

    function performace_level_update(Request $request) {

        $input = $request->all();
//        $input['huddle_id'] = '47555';
//        $input['account_id'] = '181';
//        $input['video_id'] = '192112';

        $huddle_id = $input['huddle_id'];
        $account_id = $input['account_id'];
        $video_id = $input['video_id'];
        $user_id = $input['user_id'];


        $frameworks = $this->get_framework_with_performance_level($account_id, $huddle_id, $video_id, $user_id);



        $metric_old = AccountMetaData::where(array(
                    'account_id' => $account_id,
                    'site_id' => $this->site_id
                ))->whereRaw('meta_data_name like "metric_value_%"')->orderBy('meta_data_value')->get()->toArray();


        $id_framework = AccountFolderMetaData::where(array('site_id' => $this->site_id, 'account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id'
                ))->get()->toArray();
       $publish_all = AccountFolderMetaData::where(array('site_id' => $this->site_id, 'account_folder_id' => $huddle_id, 'meta_data_name' => 'publish_all'
                ))->first();
        if($publish_all){
            $publish_all = $publish_all->toArray();
        }
        

        $huddle_detail = AccountFolder::where(array('site_id' => $this->site_id, 'account_folder_id' => $huddle_id
                ))->first();


        $framework_id = isset($id_framework[0]['meta_data_value']) ? $id_framework[0]['meta_data_value'] : "0";


        if ($framework_id == -1 || $framework_id == 0) {

            $id_framework = AccountMetaData::where(array('site_id' => $this->site_id,
                        'account_id' => $account_id, 'meta_data_name' => 'default_framework'
                    ))->get()->toArray();
            $framework_id = isset($id_framework[0]['meta_data_value']) ? $id_framework[0]['meta_data_value'] : "0";
        }

        $standard_name_details = '';
        if ($framework_id != -1 && $framework_id != 0) {


            $standard_name_details = AccountTag::where(array('site_id' => $this->site_id, "account_tag_id" => $framework_id, "account_id" => $account_id
                    ))->get()->toArray();
        }
        $standard_name = '';
        if (!empty($standard_name_details)) {
            $standard_name = $standard_name_details[0]['tag_title'];
        }

        $average_rating = $this->getAverageRatingOfVideo($account_id, $huddle_id, $video_id);

        //To get Video Feedback Publish Status
        $htype = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);
        $is_feedback_published = false;
        $coach_evaluator_ids = $this->get_evaluator_ids($huddle_id);
        if (($htype == 3 || ($htype == 2 && $this->is_enabled_coach_feedback($huddle_id, $this->site_id)))) {   //coaching huddle feedback
            $is_feedback_published = false;
            $video_comments_list = $this->getVideoComments($video_id, '', '', '', '', 1, 0, false);
            if (!empty($video_comments_list)):
                foreach ($video_comments_list as $single_video_comment):
                    if ($single_video_comment['active'] == '1' && in_array($single_video_comment['created_by'], $coach_evaluator_ids)) {
                        $is_feedback_published = true;
                    }
                endforeach;
            endif;
        }

        if ($huddle_detail['folder_type'] == '3') {
            $is_feedback_published = true;
        }

        $publish_all = isset($publish_all['meta_data_value']) ? $publish_all['meta_data_value'] : "0";
        if( $publish_all == 1 && $huddle_detail['is_published_feedback']==1 && $htype == 3){            
            $average_rating = $average_rating;
        }elseif($publish_all==0 && $htype == 3){
            $average_rating = $average_rating;
        }elseif (!$is_feedback_published && !in_array($user_id, $coach_evaluator_ids)) {
            $average_rating = '0 - No Rating';
        }
        
        //Code Ends Here




        $params_comments = array(
            'standards' => $frameworks,
            'old_ratings' => $metric_old,
            'video_id' => $video_id,
            'huddle_id' => $huddle_id,
            'account_id' => $account_id,
            'standard_name' => $standard_name,
            'average_rating' => $average_rating
        );
        return $params_comments;
    }

    function get_framework_with_performance_level($account_id, $huddle_id, $video_id, $user_id) {




        $id_framework = AccountFolderMetaData::where(array('site_id' => $this->site_id, 'account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id'
                ))->first();

        if ($id_framework) {
            $id_framework->toArray();
        }

        $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";


        if ($framework_id == -1 || $framework_id == 0) {

            $id_framework = AccountMetaData::where(array('site_id' => $this->site_id,
                        'account_id' => $account_id, 'meta_data_name' => 'default_framework'
                    ))->first();


            if ($id_framework) {
                $id_framework->toArray();
            }

            $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";
        }

        $account_tag_id = $framework_id;

        $video_data = Document::where(array(
                    'site_id' => $this->site_id,
                    'id' => $video_id
                ))->first();

        if ($video_data) {
            $video_data = $video_data->toArray();
        }

        if ($video_data['post_rubric_per_video'] == '1') {

            $account_folder_details = AccountFolderDocument::where(array(
                        'site_id' => $this->site_id,
                        'document_id' => $video_id
                    ))->first();

            if ($account_folder_details) {
                $account_folder_details = $account_folder_details->toArray();
            }

            if (!empty($account_folder_details)) {
                if (!empty($account_folder_details['video_framework_id'])) {
                    $account_tag_id = $account_folder_details['video_framework_id'];
                }
            }
        }


        if (!empty($account_tag_id)):
            $framework_settings = array();
            $account_tag_details = array();

            $acc_tag_type_h = AccountTag::select('AccountTag.*')->where(array('site_id' => $this->site_id, 'AccountTag.framework_id' => $account_tag_id,
                            // 'AccountTag.tag_type' => 0,
                    ))->whereRaw('parent_account_tag_id IS NULL')->orderBy('standard_position')->get()->toArray();

            if (count($acc_tag_type_h) > 0) {
                foreach ($acc_tag_type_h as $act) {
                    $act['tag_html'] = $act['tag_title'];
                    $account_tag_details[] = $act;
                    $acc_tag_type_c = AccountTag::select('AccountTag.*')->where(array(
                                'AccountTag.site_id' => $this->site_id,
                                'AccountTag.framework_id' => $account_tag_id,
                                    // 'AccountTag.tag_type' => 0,
                            ))->whereRaw('parent_account_tag_id = ' . $act['account_tag_id'])->orderBy('standard_position')->get()->toArray();



                    if (count($acc_tag_type_c) > 0) {
                        foreach ($acc_tag_type_c as $act_c) {
                            $account_tag_details[] = $act_c;
                        }
                    }
                }
            } else {
                $acc_tag_type_0 = AccountTag::select('AccountTag.*')->where(array(
                            'AccountTag.site_id' => $this->site_id,
                            'AccountTag.framework_id' => $account_tag_id,
                            'AccountTag.tag_type' => 0
                        ))->orderBy('standard_position')->get()->toArray();
                if (count($acc_tag_type_0) > 0) {

                    foreach ($acc_tag_type_0 as $act_0) {
                        $account_tag_details[] = $act_0;
                    }
                }
            }



            $framework_settings['account_tag_type_0'] = $account_tag_details;


            $account_framework_settings = AccountFrameworkSetting::select('AccountFrameworkSetting.id', 'AccountFrameworkSetting.account_id', 'AccountFrameworkSetting.account_tag_id', 'AccountFrameworkSetting.published_at', 'AccountFrameworkSetting.updated_at', 'AccountFrameworkSetting.published', 'AccountFrameworkSetting.framework_name', 'AccountFrameworkSetting.enable_unique_desc', 'AccountFrameworkSetting.enable_ascending_order', 'AccountFrameworkSetting.enable_performance_level', 'AccountFrameworkSetting.tier_level', 'AccountFrameworkSetting.checkbox_level', 'AccountFrameworkSetting.parent_child_share')->where(array(
                        'AccountFrameworkSetting.account_tag_id' => $account_tag_id,
                        'AccountFrameworkSetting.site_id' => $this->site_id,
                    ))->first();

            if ($account_framework_settings) {
                $account_framework_settings = $account_framework_settings->toArray();
            }


            $framework_settings['account_framework_settings'] = $account_framework_settings;
            if (!empty($account_framework_settings)) {

                $account_framework_settings_performance_levels = AccountFrameworkSettingPerformanceLevel::select('AccountFrameworkSettingPerformanceLevel.*')->where(array(
                            'AccountFrameworkSettingPerformanceLevel.account_framework_setting_id' => $account_framework_settings['id'],
                            'AccountFrameworkSettingPerformanceLevel.site_id' => $this->site_id
                        ))->orderBy('performance_level_rating')->get()->toArray();



                if (!empty($framework_settings['account_tag_type_0'])) {
                    $single_type_0_array = array();
                    foreach ($framework_settings['account_tag_type_0'] as $single_acc_tag_type_0):
                        if ($single_acc_tag_type_0['standard_level'] == $account_framework_settings['checkbox_level']) {
                            $single_acc_tag_type_0['account_framework_settings_performance_levels'] = $account_framework_settings_performance_levels;
                        } else {
                            $single_acc_tag_type_0['account_framework_settings_performance_levels'] = array();
                        }
                        $single_type_0_array[] = $single_acc_tag_type_0;
                    endforeach;
                    $framework_settings['account_tag_type_0'] = $single_type_0_array;
                }
//                $framework_settings['account_framework_settings_performance_levels'] = $account_framework_settings_performance_levels;
                if ($account_framework_settings['enable_unique_desc'] == 1) {

                    $acc_tag_type_0_tag_ids = array();
                    if (!empty($account_tag_details)) {
                        foreach ($account_tag_details as $acc_tag_type_0_single):
                            array_push($acc_tag_type_0_tag_ids, $acc_tag_type_0_single['account_tag_id']);
                        endforeach;
                    }

                    if (!empty($acc_tag_type_0_tag_ids)) {
                        $account_framework_settings_performance_levels_ids = array();
                        if (!empty($account_framework_settings_performance_levels)) {
                            foreach ($account_framework_settings_performance_levels as $single_performance_level):
                                array_push($account_framework_settings_performance_levels_ids, $single_performance_level['id']);
                            endforeach;
                        }

                        //$account_framework_settings_performance_levels_ids = implode(',', $account_framework_settings_performance_levels_ids);
                        $acc_tag_type_0_tag_ids = implode(',', $acc_tag_type_0_tag_ids);

                        $performance_level_descriptions = PerformanceLevelDescription::select('PerformanceLevelDescription.*')->where(array(
                                    'PerformanceLevelDescription.site_id' => $this->site_id
                                ))->whereIn('PerformanceLevelDescription.performance_level_id' ,$account_framework_settings_performance_levels_ids)->whereRaw(
                                        'PerformanceLevelDescription.account_tag_id IN (' . $acc_tag_type_0_tag_ids . ')')->get()->toArray();

                        if (!empty($framework_settings['account_tag_type_0']) && !empty($performance_level_descriptions)) {
                            $single_type_0_array = array();
                            foreach ($framework_settings['account_tag_type_0'] as $single_acc_tag_type_0):
                                $performance_level_descriptions_arr = array();
                                foreach ($performance_level_descriptions as $single_performance_level_description):
                                    if ($single_acc_tag_type_0['standard_level'] == $account_framework_settings['checkbox_level'] && $single_acc_tag_type_0['account_tag_id'] == $single_performance_level_description['account_tag_id']) {
                                        array_push($performance_level_descriptions_arr, $single_performance_level_description);
                                    }
                                endforeach;
                                $single_acc_tag_type_0['performance_level_descriptions'] = $performance_level_descriptions_arr;
                                $single_type_0_array[] = $single_acc_tag_type_0;
                            endforeach;

                            $framework_settings['account_tag_type_0'] = $single_type_0_array;
                        }
//                        $framework_settings['performance_level_descriptions'] = $performance_level_descriptions;
                    }
                }
                $single_type_0_array = array();
                foreach ($framework_settings['account_tag_type_0'] as $single_acc_tag_type_0):
                    $single_acc_tag_type_0['get_standard_tagged_count'] = $this->get_standard_tagged_count($video_id, $single_acc_tag_type_0['account_tag_id'], $huddle_id, $user_id);
                    $single_acc_tag_type_0['get_selected_rating'] = $this->get_selected_rating($single_acc_tag_type_0['account_tag_id'], $account_id, $huddle_id, $video_id);
                    $single_type_0_array[] = $single_acc_tag_type_0;

                endforeach;
                $framework_settings['account_tag_type_0'] = $single_type_0_array;
            }
            if (!empty($framework_settings)) {
                $result['status'] = true;
                $result['msg'] = "Account framework settings fetched successfully.";
                $result['data'] = $framework_settings;
            } else {
                $result['status'] = true;
                $result['msg'] = "Nothing found for your request.";
            }
        else:
            $result['status'] = false;
            $result['msg'] = "Please provide a valid account tag id.";
        endif;
        return $result;
    }

    function get_standard_tagged_count($video_id, $standard_id, $huddle_id, $user_id) {


        $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);

        if ((!$this->check_if_evalutor($huddle_id, $user_id)) && (($h_type == '2' && $this->is_enabled_coach_feedback($huddle_id, $this->site_id)) || $h_type == '3')) {

            $result = AccountCommentTag::join('comments as cmt', 'cmt.id', '=', 'AccountCommentTag.comment_id')->where(array(
                        'AccountCommentTag.account_tag_id' => $standard_id,
                        'AccountCommentTag.site_id' => $this->site_id,
                        'AccountCommentTag.ref_id' => $video_id,
                        'cmt.active' => '1'
                    ))->get()->toArray();
        } else {

            $result = AccountCommentTag::where(array(
                        'account_tag_id' => $standard_id,
                        'site_id' => $this->site_id,
                        'ref_id' => $video_id,
                    ))->get()->toArray();
        }


        if (count($result) == 0) {
            return 0;
        } else {
            return count($result);
        }
    }

    function get_selected_rating($standard_id, $account_id, $huddle_id, $video_id) {


        $result = DocumentStandardRating::where(array(
                    'account_folder_id' => $huddle_id, 'site_id' => $this->site_id, 'document_id' => $video_id, 'account_id' => $account_id, 'standard_id' => $standard_id
                ))->first();

        if ($result) {
            $result = $result->toArray();
        }

        return $result;
    }

    function add_multiple_standard_ratings(Request $request) {

        $input = $request->all();
//         $input['standard_ids'] = array('5228');
//        $input['huddle_id'] = '47555';
//         $input['video_id'] = '192112';
//          $input['account_id'] = '181';
//          $input['user_id'] = '621';
//          $input['rating_value_5228'] = '0';
//          $input['rating_id_5228'] = '7446';


        $standard_ids = $input['standard_ids'];
        $huddle_id = $input['huddle_id'];
        $video_id = $input['video_id'];

        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        if (isset($input['account_role_id']) && isset($input['current_user_email'])) {
            $user_role = $this->get_user_role_name($input['account_role_id']);

            $in_trial_intercom = $this->check_if_account_in_trial_intercom($account_id);
            $meta_data = array(
                "perfomance_level_saved" => 'true',
                "user_role" => $user_role,
                "is_in_trial" => $in_trial_intercom,
                'Platform' => 'Web'
            );

            $this->create_intercom_event('perfomance-level-saved', $meta_data, $input['current_user_email']);
        }


        $account_details = Account::where(array(
                    'id' => $account_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($account_details) {
            $account_details = $account_details->toArray();
        }



        $id_framework = AccountFolderMetaData::where(array('site_id' => $this->site_id,
                    'account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id'
                ))->first();

        if ($id_framework) {
            $id_framework->toArray();
        }

        $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";


        if ($framework_id == -1 || $framework_id == 0) {

            $id_framework = AccountMetaData::where(array('site_id' => $this->site_id,
                        'account_id' => $account_id, 'meta_data_name' => 'default_framework'
                    ))->first();


            if ($id_framework) {
                $id_framework->toArray();
            }

            $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";
        }


        $video_data = Document::where(array(
                    'id' => $video_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($video_data) {
            $video_data = $video_data->toArray();
        }

        if ($video_data['post_rubric_per_video'] == '1') {

            $account_folder_details = AccountFolderDocument::where(array(
                        'site_id' => $this->site_id,
                        'document_id' => $video_id
                    ))->first();

            if ($account_folder_details) {
                $account_folder_details = $account_folder_details->toArray();
            }

            if (!empty($account_folder_details)) {
                if (!empty($account_folder_details['video_framework_id'])) {
                    $framework_id = $account_folder_details['video_framework_id'];
                }
            }
        }





//        $standards_settings = AccountFrameworkSetting::where(array(
//                    "account_id" => $account_id,
//                    "account_tag_id" => $framework_id
//                ))->whereOr(
//                        'account_id' =>
//
//                        )->get()->toArray();

        $standards_settings = AccountFrameworkSetting::where("account_tag_id", $framework_id)
                        ->where("site_id", $this->site_id)
                        ->whereIn("account_id", [$account_id, $account_details['parent_account_id']])
                        ->get()->toArray();



        $performace_level_switch = isset($standards_settings[0])?$standards_settings[0]['enable_performance_level']:false;

        if (!empty($standard_ids)) {
            foreach ($standard_ids as $standard_id) {
                $rating_value = $input['rating_value_' . $standard_id];
                $rating_id = $input['rating_id_' . $standard_id];

                DB::table('documents')->where(['id' => $video_id, 'site_id' => $this->site_id])->update(array('not_observed' => 1, 'site_id' => $this->site_id));

                if ($rating_value == 0) {
                    DB::table('document_standard_ratings')->where(array('account_folder_id' => $huddle_id, "account_id" => $account_id, 'document_id' => $video_id, 'standard_id' => $standard_id, 'site_id' => $this->site_id))->delete();
                } else {
                    $result = DocumentStandardRating::where(array(
                                'account_folder_id' => $huddle_id, 'document_id' => $video_id, 'account_id' => $account_id, 'standard_id' => $standard_id, 'site_id' => $this->site_id
                            ))->first();

                    if ($result) {
                        $result = $result->toArray();
                    }


                    if (empty($result)) {
                        $data = array(
                            'document_id' => $video_id,
                            'site_id' => $this->site_id,
                            'account_folder_id' => $huddle_id,
                            'standard_id' => $standard_id,
                            'rating_id' => $rating_id,
                            'rating_value' => $rating_value,
                            'account_id' => $account_id,
                            'user_id' => $user_id,
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s")
                        );

                        if ($performace_level_switch) {
                            $data['new_performance_level_check'] = '1';
                        }
                        $this->create_churnzero_event('Performance+Level+Rating', $account_id, $input['current_user_email'], $rating_value);
                        DB::table('document_standard_ratings')->insert($data);
                    } else {
                        DB::table('document_standard_ratings')->where(['id' => $result['id'], 'site_id' => $this->site_id])->update(array('site_id' => $this->site_id, 'rating_id' => $rating_id, 'rating_value' => $rating_value, 'updated_at' => date("Y-m-d H:i:s"), 'include_in_analytics' => '1'));
                    }
                }
            }
        }


        $results = DocumentStandardRating::where(array('site_id' => $this->site_id,
                    'document_id' => $video_id, 'account_id' => $account_id, 'account_folder_id' => $huddle_id
                ))->get()->toArray();



        if (!empty($results)) {
            $avg = 0;
            foreach ($results as $result) {
                $avg = $avg + (int) $result['rating_value'];
            }
//            $view = new View($this, false);
//            $rating_name = $view->Custom->get_rating_name($view->Custom->get_average_video_rating($video_id, $account_id, $huddle_id), $account_id);

            $data = array('rating_score' => round($avg / count($results))
            );
        } else {
            $data = array('rating_score' => 'N/O'
            );
        }
        if(!empty($video_id))//sw-5005
        {
            Document::where('id',$video_id)->update(['last_edit_date'=>date("Y-m-d H:i:s"), 'last_edit_by'=>$user_id]);
        }
        $data2 = $data;
        $data2["input_data"] = $input;
        $event = [
            'channel' => "video-details-" . $video_id,
            'event' => "ratings_updated",
            'data' => $data2,
            'document_id' => $video_id,
            'huddle_id' => $huddle_id,
        ];
        HelperFunctions::broadcastEvent($event);
        return $data;
    }

    function load_perfomance_level_comments(Request $request) {
        $input = $request->all();

//        $input['huddle_id'] = '47555';
//        $input['user_id'] = '621';
//        $input['video_id'] = '192112';
//        $input['account_tag_id'] = '5227';


        $huddle_id = $input['huddle_id'];
        // $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $video_id = $input['video_id'];
        $account_tag_id = $input['account_tag_id'];
        $detail_id = $video_id;

        $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);
        $comments_result = array();

        if (!empty($detail_id)) {
            if ((!$this->check_if_evalutor($huddle_id, $user_id)) && (($h_type == '2' && $this->is_enabled_coach_feedback($huddle_id, $this->site_id)) || $h_type == '3')) {

                $comments_result = $this->getVideoComments($detail_id, '', $huddle_id, '', '', 1, 1, false, $account_tag_id, true);
            } else {
                $comments_result = $this->getVideoComments($detail_id, '', $huddle_id, '', '', 1, 1, false, $account_tag_id);
            }
        }
        $htype = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);

        $comments = array();
        foreach ($comments_result as $comment) {
            // If comment is audio type, then get cloudfront url and update it in returned result
            if ($comment['ref_type'] == 6) {
                $comment['comment'] = $this->get_cloudfront_url($comment['comment']);
            }

            $get_standard = $this->gettagsbycommentid($comment['id'], array('0')); //get standards
            $get_tags = $this->gettagsbycommentid($comment['id'], array('1', '2')); //get tags
            if ($htype == 3) {
                if ($this->check_if_evalutor($huddle_id, $user_id)) {
                    $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
                } else {
                    if ($comment['active'] == 1) {
                        $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
                    }
                }
            } else {
                $comments[] = array_merge($comment, array("standard" => $get_standard), array("default_tags" => $get_tags));
            }
        }

        $comments_result = $comments;

        $videoComments = '';
        if (is_array($comments_result) && count($comments_result) > 0) {
            $videoComments = $comments_result;
        } else {
            $videoComments = '';
        }

        $params_comments = array(
            'videoComments' => $videoComments,
        );

        return $params_comments;
    }

    function feedback_publish(Request $request) {
        $input = $request->all();
//        $input['huddle_id'] = '56177';
//        $input['video_id'] = '96633';
//        $input['user_id'] = '621';
//        $input['account_id'] = '621';

        $huddle_id = $input['huddle_id'];
        $video_id = $input['video_id'];
        $user_id = $input['user_id'];
        $account_id = $input['account_id'];

        if (isset($input['account_role_id']) && isset($input['current_user_email'])) {

            $user_role = $this->get_user_role_name($input['account_role_id']);
            $in_trial_intercom = $this->check_if_account_in_trial_intercom($account_id);


            $meta_data = array(
                "comment_published" => 'Comment Published',
                "user_role" => $user_role,
                'is_in_trial' => $in_trial_intercom,
                'Platform' => 'Web',
                'site_title' => $this->site_title
            );

            $this->create_intercom_event('comments-published', $meta_data, $input['current_user_email']);
        }

        $change_row_count = 0;
        $data = array(
            'active' => 1,
            'site_id' => $this->site_id,
            'published_by' => $user_id
        );
        $change_row_count = DB::table('comments')->where(['site_id' => $this->site_id, 'ref_id' => $video_id])->update($data);
        if ($change_row_count > 0) {

            QueuesManager::sendToOperationQueue(__CLASS__,"process_feedback_publish_asynch_operations", $this->site_id, $user_id, $account_id, $huddle_id, $video_id);

            return array(
                'message' => TranslationsManager::get_translation('Comments_have_been_posted', 'flash_messages', $this->site_id),
                'status' => true,
                'success' => true);
        } else {
            return array(
                'message' => TranslationsManager::get_translation('Comments_are_not_posted_successfully', 'flash_messages', $this->site_id),
                'status' => false,
                'success' => false);
        }
    }

    function process_feedback_publish_asynch_operations($site_id, $user_id, $account_id, $huddle_id, $video_id){
        $this->site_id = $site_id;
        $user_results = User::where(array(
            'site_id' => $site_id,
            'id' => $user_id
        ))->first()->toArray();

        $account_results = Account::where(array(
                    'site_id' => $site_id,
                    'id' => $account_id
                ))->first()->toArray();

        $current_user_id = $user_id;
        $huddle_users = AccountFolder::getHuddleUsers($huddle_id, $site_id);
        $huddle_groups = AccountFolder::getHuddleGroupsUsers($huddle_id, $site_id);
        $current_user_role = $this->has_admin_access_for_socket($huddle_users, $huddle_groups, $user_id);
        // $assessor_ids[] =$input['user_id'];
        $assessor_ids = AccountFolderUser::get_participants_ids($huddle_id, 200, $site_id);
        $ids = [];
        foreach($assessor_ids as $assessor){
            $ids [] = $assessor['user_id'];
        }
        $assessor_ids = $ids;
        $doc_res = Document::get_single_video_data($site_id, $video_id, '1', 1, $huddle_id, $user_id);
        $is_assessed =  AccountFolderUser::isAssessedByVideo($huddle_id, $doc_res['created_by'], $assessor_ids,$video_id);
        $doc_res['assessed'] =  $is_assessed;
        $assessees_list= [];
        $event = [
            'channel' => "video-details-" . $video_id,
            'event' => "feedback_published",
            'assessees_list'=> $assessees_list, // If there are lots and lots of data in assessee list. It breaks on OneSignal side. So, for now we are sending empty list.
            'data' => $video_id,
            'document_id' => $video_id,
            'huddle_id' => $huddle_id,
        ];
        HelperFunctions::broadcastEvent($event, "broadcast_event", true, false);

        $assessees_list= AccountFolderUser::getAssesseesList($site_id, $huddle_id, $current_user_id, $current_user_role);
        \Log::info("--- process_feedback_publish_asynch_operations --- ".__LINE__);

       // $doc_res = Document::get_single_video_data($site_id, $video_id, '1', 1, $huddle_id, $input['user_id']);
        $event = [
            'channel' => 'huddle-details-' . $huddle_id,
            'event' => "resource_renamed",
            'assessees_list'=>$assessees_list,
            'data' => $doc_res,
            'video_file_name' => $doc_res["title"],
            'is_dummy' => 1
        ];
        HelperFunctions::broadcastEvent($event, "broadcast_event", false, false);

        $is_email_notification_allowed = HelperFunctions::is_email_notification_allowed($account_id);
        if($is_email_notification_allowed){
            $this->EmailFeedbackVideo($huddle_id, $video_id, $account_id, $account_results, $user_results, $site_id);
        }
    }

    function deleteVideoComments(Request $request) {
        $input = $request->all();
        $videoId = $input['comment_id'];
        $current_video_id = $input['video_id'];
        $comments = Comment::where(array('id' => $videoId, 'site_id' => $this->site_id))->with('replies')->get();
        //dd($comments->replies()->exists());
        $comment = DB::table('comments')->where(array('id' => $videoId, 'site_id' => $this->site_id))->first();
        $parent_comment_detail = DB::table('comments')->where(array('id' => $comment->parent_comment_id, 'site_id' => $this->site_id))->first();
        if($parent_comment_detail)
        {
            $parent_comment_detail = json_decode(json_encode($parent_comment_detail), True);
        }
        else
        {
            $parent_comment_detail = array();
        }
        DB::table('comments')->where(array('id' => $videoId, 'site_id' => $this->site_id))->delete();

        $all_replies = Comment::where(array('parent_comment_id' => $videoId, 'site_id' => $this->site_id))->get()->toArray();

        $del_rep_count = $del_rep_parnt_count = 0;
        foreach ($all_replies as $reply) {
            $res_rep = DB::table('comments')->where(array('id' => $reply['id'], 'site_id' => $this->site_id))->delete();
            $res_rep_parnt = DB::table('comments')->where(array('parent_comment_id' => $reply['id'], 'site_id' => $this->site_id))->delete();
            if($res_rep){
                $del_rep_count += 1;
            }
            if($res_rep_parnt){
                $del_rep_parnt_count += 1;
            }
        }
        $del_total_rep_count = $del_rep_count + $del_rep_parnt_count;

        $comment_tags = AccountCommentTag::where(array(
                    'site_id' => $this->site_id,
                    'comment_id' => $videoId
                ))->get()->toArray();

        if (!empty($comment_tags)) {
            foreach ($comment_tags as $comment_tag) {

                DB::table('account_comment_tags')->where(array('account_comment_tag_id' => $comment_tag['account_comment_tag_id'], 'site_id' => $this->site_id))->delete();

                $bool = 1;

                $comments_video_related = Comment::where(array(
                            'ref_id' => $current_video_id,
                            'site_id' => $this->site_id
                        ))->get()->toArray();

                foreach ($comments_video_related as $row) {

                    $comments_related_tags = AccountCommentTag::where(array(
                                'comment_id' => $row['id'], 'account_tag_id' => $comment_tag['account_tag_id'], 'site_id' => $this->site_id
                            ))->get()->toArray();

                    if (count($comments_related_tags) > 0) {
                        $bool = 0;
                    }
                }
                if ($bool) {


                    DB::table('document_standard_ratings')->where(array(
                        "standard_id" => $comment_tag['account_tag_id'],
                        "document_id" => $current_video_id,
                        'site_id' => $this->site_id
                    ))->delete();
                }
            }
        }


        $is_deleted = Comment::where(array(
                    'site_id' => $this->site_id,
                    'id' => $videoId
                ))->count();

        if ($is_deleted == 0) {
            $folder_type = DB::select("select * from account_folders where account_folder_id = (SELECT account_folder_id FROM account_folder_documents WHERE document_id = $current_video_id)");
            $is_workspace_comment = 0;
            $is_library = 0;
            $account_folder_id = 0;
            if (isset($folder_type[0])) {
                $account_folder_id = $folder_type[0]->account_folder_id;
                if ($folder_type[0]->folder_type == '3') {
                    $is_workspace_comment = 1;
                }
                else if($folder_type[0]->folder_type == '2')
                {
                    $is_library = 1;
                }
            }

            $account_id = isset($folder_type[0])? $folder_type[0]->account_id:'';
            if($comment && $comment->ref_type!=1){
                // This check is to exclude Discussions because in original analytics discussions/replies are not being decreased.
                $analyticsHuddlesSummary = new \App\Models\AnalyticsHuddlesSummary;
                $account_folder_id = isset($folder_type[0])? $folder_type[0]->account_folder_id:'';

                foreach($comments as $commenting)
                {
                    if($commenting->ref_type == 3)
                    {
                        $analyticsHuddlesSummary->saveRecord($account_id, $account_folder_id, $commenting->user_id, $this->site_id, 8, date('Y-m-d',strtotime($commenting->created_date)), 1, true);
                    }
                    else
                    {
                        $analyticsHuddlesSummary->saveRecord($account_id, $account_folder_id, $commenting->user_id, $this->site_id, 5, date('Y-m-d',strtotime($commenting->created_date)), 1, true);
                    }

                    foreach($commenting->replies as $reply)
                    {
                        $analyticsHuddlesSummary->saveRecord($account_id, $account_folder_id, $reply->user_id, $this->site_id, 8, date('Y-m-d',strtotime($reply->created_date)), 1, true);

                            foreach($reply->replies as $nestedReply)
                            {
                                $analyticsHuddlesSummary->saveRecord($account_id, $account_folder_id, $nestedReply->user_id, $this->site_id, 8, date('Y-m-d',strtotime($nestedReply->created_date)), 1, true);
                            }

                    }
                }
                // $analyticsHuddlesSummary->saveRecord($account_id, $account_folder_id, $input['user_id'], $this->site_id, 5, date('Y-m-d',strtotime($comment->created_date)), 1, true);
                // $analyticsHuddlesSummary->saveRecord($account_id, $account_folder_id, $input['user_id'], $this->site_id, 8, date('Y-m-d',strtotime($comment->created_date)), $del_total_rep_count, true);
            }
    
            $parent_comment_id = null;
            if ($comment) {
                $parent_comment_id = $comment->parent_comment_id;
            }
            $folders_type = isset($folder_type[0])?$folder_type[0]->folder_type:'';
            $res = Document::get_single_video_data($this->site_id, $current_video_id,  $folders_type, 1, $account_folder_id, $input['user_id']);
            $logged_in_user_huddle_role = HelperFunctions::get_huddle_roles($account_folder_id,$input["user_id"]);
            if(($comment && $input["user_id"] != $comment->created_by) || $logged_in_user_huddle_role == '210' )
            {
                $htype = AccountFolderMetaData::getMetaDataValue($account_folder_id, $this->site_id);
                if($htype == 2)
                {
                    $user_who_deleted = User::where(array('site_id' => $this->site_id,'id' => $input["user_id"]))->first();
                    $user_who_created = User::where(array('site_id' => $this->site_id,'id' => $comment->created_by))->first();
                    $huddle_detail = AccountFolder::where(array('site_id' => $this->site_id,'account_folder_id' => $account_folder_id))->first();
                    $video_title = DB::table('account_folder_documents')->where(array('document_id' => $current_video_id, 'site_id' => $this->site_id))->first();
                    if($this->site_id == '1')
                    {
                        if($user_who_created['lang'] == 'en')
                        {
                             $email_subject = 'Sibme - Comment Deleted';
                        }
                        else
                        {
                             $email_subject = 'Sibme - Comentario Eliminado';
                        }
                    }
                    else
                    {
                        $email_subject = 'HMH - Comment Deleted';
                    }
                    $key = "delete_coaching_comment_huddle_" . $this->site_id . "_" . $user_who_created['lang'];
                    $results = SendGridEmailManager::get_send_grid_contents($key);
                    $html = $results->versions[0]->html_content;
                    $html = str_replace('<%body%>', '', $html);
                    $html = str_replace('{user_who_deleted}',$user_who_deleted['first_name'] . ' ' . $user_who_deleted['last_name'], $html);
                    $html = str_replace('{huddle_name}',$huddle_detail['name'],$html);
                    $html = str_replace('{comment_or_post}', 'comment' ,$html);
                    $html = str_replace('{comment_data}', $comment->comment ,$html);
                    $html = str_replace('{title}', $video_title->title ,$html);
                    $html = str_replace('{video_or_discussion}', 'video' ,$html);

                    if($logged_in_user_huddle_role == '210')
                    {
                        $send_to = HelperFunctions::get_evaluator_emails($account_folder_id,'200');
                        QueuesManager::sendToEmailQueue(__CLASS__, "sendAddDeletetVideoCommentToParticipants",$send_to,$this->site_id,$user_who_deleted,$huddle_detail['name'],$comment,$video_title,$huddle_detail['account_id']);
                        //sendAddDeletetVideoCommentToParticipants
                    }

                    else
                    {
                        $sibme_base_url = config('s3.sibme_base_url');
                        $unsubscribe = $sibme_base_url . 'subscription/unsubscirbe_now/' . $user_who_created['id'] . '/12';
                        $html = str_replace('{unsubscribe}', $unsubscribe ,$html);
                        $send_to[] =  $user_who_created['email'];
                        $html = str_replace('{deleted_your}', 'deleted your' ,$html);
                        $emailData = [
                            'from' => Sites::get_site_settings('static_emails', $this->site_id)['noreply'],
                            'from_name' => Sites::get_site_settings('site_title', $this->site_id),
                            'to' => $user_who_created['email'],
                            'subject' => $email_subject,
                            'template' => $html,
                        ];
                        if(EmailUnsubscribers::check_subscription($user_who_created['id'], '12', $huddle_detail['account_id'], $this->site_id))
                        {
                            Email::sendCustomEmail($emailData);
                        }
                    }


                }
            }

            // check if video is assessed - start by Arif Sami
            $assessor_ids = AccountFolderUser::get_participants_ids($account_folder_id, 200, $this->site_id);
            $ids = [];
            foreach($assessor_ids as $assessor){
                $ids [] = $assessor['user_id'];
            }
            $assessor_ids = $ids;
            $original_document = Document::where("id", $input['video_id'])->where("site_id", $this->site_id)->first();
            $is_assessed =  AccountFolderUser::isAssessedByVideo($account_folder_id, $original_document['created_by'], $assessor_ids,$input["video_id"]);
            $res['assessed'] =  $is_assessed;
            // check if video is assessed - end

            $event = [
                'channel' => ($is_library ? 'library-'.$account_id : ($is_workspace_comment ? 'workspace-'.$account_id."-". $input['user_id']:'huddle-details-' . $account_folder_id)),
                'event' => "resource_renamed",
                'data' => $res,
                'video_file_name' => !empty($res["title"])?$res["title"]:'',
                'is_dummy' => 1
            ];
            HelperFunctions::broadcastEvent($event, "broadcast_event", false); //not sending push to prevent multiple refresh
            $replies_count = 0;
            if(!empty($parent_comment_detail))
            {
                $replies_count_data = $this->get_replies_from_comment($parent_comment_detail, '1');
                $replies_count = count($replies_count_data['responses']);
            }

            $event = [
                'channel' => 'video-details-' . $current_video_id,
                'event' => "comment_deleted",
                'data' => $videoId,
                'parent_comment_id' => $parent_comment_id,
                'is_workspace_comment' => $is_workspace_comment,
                'is_library_comment' => $is_library,
                'item_id' => $videoId,
                'huddle_id' => $account_folder_id,
                'reference_id' => $current_video_id,
                'video_id' => $current_video_id,
                'replies_count' => $replies_count
            ];
            HelperFunctions::broadcastEvent($event);
            $event = [
                'channel' => 'huddle-details-' . $account_folder_id,
                'event' => "comment_deleted",
                'data' => isset($comment->created_by) ? $comment->created_by : $input['user_id'],
                'parent_comment_id' => $parent_comment_id,
                'huddle_id' => $account_folder_id,
                'is_workspace_comment' => $is_workspace_comment,
                'is_library_comment' => $is_library,
                'reference_id' =>  $current_video_id,
                'video_id' =>  $current_video_id,
                'item_id' => $videoId,
                'user' => User::where(array('id' => $input["user_id"], 'site_id' => $this->site_id))->first()->toArray(),
                'replies_count' => $replies_count
            ];
            HelperFunctions::broadcastEvent($event);
            return array(
                'status' => "success"
            );
        } else {
            return array(
                "status" => "failed"
            );
        }
        exit;
    }

    function sendAddDeletetVideoCommentToParticipants($send_to,$site_id,$user_who_deleted,$huddle_name,$comment,$video_title,$account_id){
        foreach($send_to as $row)
        {
            if($site_id == '1')
            {
                if($row['lang'] == 'es')
                {
                    $email_subject = 'Sibme - Comentario Eliminado';
                }
                else
                {
                    $email_subject = 'Sibme - Comment Deleted';
                }
            }
            else
            {
                $email_subject = 'HMH - Comment Deleted';
            }
                $key = "delete_coaching_comment_huddle_" . $site_id . "_" . $row['lang'];
                $results = SendGridEmailManager::get_send_grid_contents($key);
                $html = $results->versions[0]->html_content;
                $html = str_replace('<%body%>', '', $html);
                $html = str_replace('{user_who_deleted}',$user_who_deleted['first_name'] . ' ' . $user_who_deleted['last_name'], $html);
                $html = str_replace('{huddle_name}',$huddle_name,$html);
                $html = str_replace('{comment_or_post}', 'comment' ,$html);
                $html = str_replace('{comment_data}', $comment->comment ,$html);
                $html = str_replace('{title}', $video_title->title ,$html);
                $html = str_replace('{video_or_discussion}', 'video' ,$html);
                $html = str_replace('{deleted_your}', 'deleted' ,$html);
                $sibme_base_url = Sites::get_base_url($site_id);
                $unsubscribe = $sibme_base_url . 'subscription/unsubscirbe_now/' . $row['id'] . '/12';
                $html = str_replace('{unsubscribe}', $unsubscribe ,$html);
                $emailData = [
                    'from' => Sites::get_site_settings('static_emails', $site_id)['noreply'],
                    'from_name' => Sites::get_site_settings('site_title', $site_id),
                    'to' => $row['email'],
                    'subject' => $email_subject,
                    'template' => $html,
                ];
            if(EmailUnsubscribers::check_subscription($row['id'], '12', $account_id, $site_id))
            {
                    Email::sendCustomEmail($emailData);
            }

        }
    }

    function print_pdf_comments(Request $request) {

        $input = $request->all();
        $translations = TranslationsManager::get_page_lang_based_content("export_data", $this->site_id, '', $input['current_lang']);
        $video_id = $input['video_id'];
        $huddle_id = $input['huddle_id'];
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $sort_by = isset($input['sort_by']) ? $input['sort_by'] : '';          
        $accounts_data = DB::table('accounts')->where(array('id' => $account_id, 'site_id' => $this->site_id))->first();      
        $user_data = DB::table('users')->where(array('id' => $user_id, 'site_id' => $this->site_id))->first();
        $huddle = AccountFolder::where(array('account_folder_id' => $huddle_id ))->first();
        $huddle_role = AccountFolderUser::where(array(
            'account_folder_id' => $huddle_id,
            'user_id' => $user_id,
            'site_id' => $this->site_id
        ))
        ->first();
        if($huddle->folder_type ==1){
            $framewok_id = AccountTag::getFrameworkId($account_id,$huddle_id,$this->site_id);
        }else{
            $account_folder_document_details = AccountFolderDocument::where(array(
                'account_folder_id' => $huddle_id,
                'site_id' => $this->site_id,
                'document_id' => $video_id
            ))->first();

            if ($account_folder_document_details) {
                $account_folder_document_details = $account_folder_document_details->toArray();
            }

            $framewok_id =  $account_folder_document_details['video_framework_id'];  
        }
        
        $huddle_role = AccountFolderUser::where(array(
                    'account_folder_id' => $huddle_id,
                    'user_id' => $user_id,
                    'site_id' => $this->site_id
                ))
                ->first();
        $framework_data = array();
        $framework_data = AccountTag::where('account_tag_id', $framewok_id )->where('tag_type',2)->where('site_id',$this->site_id)->first();      
        $role_id = '';
        if ($huddle_role) {
            $huddle_role = $huddle_role->toArray();
            $role_id =  $huddle_role['role_id'];
        }
        $all_particpants = "";
        $participants = AccountFolder::getHuddleUsersIncludingGroups($this->site_id, $huddle_id, $user_id, $role_id,true)['participants'];
        if(count($participants) > 0){
            foreach($participants as $key=>$row){
                if(!empty($row['user_name'])){
                    $participants[$key] = $row['user_name'];
                }
                
            }
            $all_particpants = implode(",", $participants);
        } else{
            $all_particpants = "";
        }            
        $account_folder_document_data = DB::table('account_folder_documents')->where(array('document_id' => $video_id, 'site_id' => $this->site_id))->first();
        
        $document_data = DB::table('documents')->where(array('id' => $video_id, 'site_id' => $this->site_id))->first();
        if(strlen($all_particpants) > 85){
            $all_particpants = substr($all_particpants,'85').'...';
        }
        $video_url = '';
       if($huddle->folder_type ==1){
         $video_url = config('s3.sibme_base_url').'home/video_details/home/'. $huddle_id.'/'.$video_id;
       }elseif($huddle->folder_type ==2){
        $video_url = config('s3.sibme_base_url').'home/library_video/home/'. $huddle_id.'/'.$video_id;
       }elseif($huddle->folder_type ==3){
        $video_url = config('s3.sibme_base_url').'home/workspace_video/home/'. $huddle_id.'/'.$video_id;
       }
        $header_details = array(
            'logo_url'=>config('s3.sibme_base_url') ,
            'account_name'=> $accounts_data->company_name,
            'huddle_name'=>$huddle->name,
            'folder_type'=>$huddle->folder_type,
            'video_title'=>$account_folder_document_data->title,
            'uploaded_date'=> $document_data->created_date,
            'video_url'=>$video_url,
            'exported_by'=>$user_data->first_name.' '.$user_data->last_name,
            'huddle_participants'=>$all_particpants,
            'framework_title'=>!empty($framework_data->tag_title)?$framework_data->tag_title:''

        );
        $exportPDFComments = new \App\Services\Exports\Comments\ExportPDFComments;
      
        return $exportPDFComments->export($this->site_id, $video_id, $sort_by, $huddle_id, $account_id, $user_id, $translations, $header_details);
      
        
    }

    
    function sent_feedback_email(Request $request) {

        $input = $request->all();

//        $huddle_id = "47555";
//        $video_id = "192112";
//        $account_id = "181";
//        $emails = ['abc@asdfasdfasf.us'];
//        $subject = "Test Video Message";
//        $message = "This is Test Message";
    
        $huddle_id = $input['huddle_id'];
        $video_id = $input['video_id'];
        $account_id = $input['account_id'];
        $emails = strstr($input['email'], ",") ? explode(",", $input['email']) : [$input['email']];
        $subject = $input['subject'];
        $message = isset($input['message']) ? $input['message'] : "";
        $file = isset($_FILES['additional_attachemnt']) && !empty($_FILES['additional_attachemnt']) ? $_FILES['additional_attachemnt'] : '';
       
        $comments_attachment_url = $this->export_comments_attachment($input);

        return \App\Services\Emails\VideoFeedback::sendEmail($this->site_id, $huddle_id, $video_id, $account_id, $emails, $subject, $message, $file,$comments_attachment_url);
    }

    function export_comments_attachment($input) {     
        $translations = TranslationsManager::get_page_lang_based_content("export_data", $this->site_id, '', $input['current_lang']);
        $video_id = $input['video_id'];
        $huddle_id = $input['huddle_id'];
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $file_name = $input['subject'];
        $sort_by = isset($input['sort_by']) ? $input['sort_by'] : '';          
        $accounts_data = DB::table('accounts')->where(array('id' => $account_id, 'site_id' => $this->site_id))->first();      
        $user_data = DB::table('users')->where(array('id' => $user_id, 'site_id' => $this->site_id))->first();
        $huddle = AccountFolder::where(array('account_folder_id' => $huddle_id ))->first();
        $huddle_role = AccountFolderUser::where(array(
            'account_folder_id' => $huddle_id,
            'user_id' => $user_id,
            'site_id' => $this->site_id
        ))
        ->first();
        if($huddle->folder_type ==1){
            $framewok_id = AccountTag::getFrameworkId($account_id,$huddle_id,$this->site_id);
        }else{
            $account_folder_document_details = AccountFolderDocument::where(array(
                'account_folder_id' => $huddle_id,
                'site_id' => $this->site_id,
                'document_id' => $video_id
            ))->first();

            if ($account_folder_document_details) {
                $account_folder_document_details = $account_folder_document_details->toArray();
            }

            $framewok_id =  $account_folder_document_details['video_framework_id'];  
        }
        
        $huddle_role = AccountFolderUser::where(array(
                    'account_folder_id' => $huddle_id,
                    'user_id' => $user_id,
                    'site_id' => $this->site_id
                ))
                ->first();
        $framework_data = array();
        $framework_data = AccountTag::where('account_tag_id', $framewok_id )->where('tag_type',2)->where('site_id',$this->site_id)->first();      
        $role_id = '';
        if ($huddle_role) {
            $huddle_role = $huddle_role->toArray();
            $role_id =  $huddle_role['role_id'];
        }
        $all_particpants = "";
        $participants = AccountFolder::getHuddleUsersIncludingGroups($this->site_id, $huddle_id, $user_id, $role_id,true)['participants'];
        if(count($participants) > 0){
            foreach($participants as $key=>$row){
                if(!empty($row['user_name'])){
                    $participants[$key] = $row['user_name'];
                }
                
            }
            $all_particpants = implode(",", $participants);
        } else{
            $all_particpants = "";
        }            
        $account_folder_document_data = DB::table('account_folder_documents')->where(array('document_id' => $video_id, 'site_id' => $this->site_id))->first();
        
        $document_data = DB::table('documents')->where(array('id' => $video_id, 'site_id' => $this->site_id))->first();
        if(strlen($all_particpants) > 85){
            $all_particpants = substr($all_particpants,'85').'...';
        }
        $video_url = '';
       if($huddle->folder_type ==1){
         $video_url = config('s3.sibme_base_url').'home/video_details/home/'. $huddle_id.'/'.$video_id;
       }elseif($huddle->folder_type ==2){
        $video_url = config('s3.sibme_base_url').'home/library_video/home/'. $huddle_id.'/'.$video_id;
       }elseif($huddle->folder_type ==3){
        $video_url = config('s3.sibme_base_url').'home/workspace_video/home/'. $huddle_id.'/'.$video_id;
       }
        $header_details = array(
            'logo_url'=>config('s3.sibme_base_url') ,
            'account_name'=> $accounts_data->company_name,
            'huddle_name'=>$huddle->name,
            'folder_type'=>$huddle->folder_type,
            'video_title'=>$account_folder_document_data->title,
            'uploaded_date'=> $document_data->created_date,
            'video_url'=>$video_url,
            'exported_by'=>$user_data->first_name.' '.$user_data->last_name,
            'huddle_participants'=>$all_particpants,
            'framework_title'=>!empty($framework_data->tag_title)?$framework_data->tag_title:''

        );
        $exportPDFComments = new \App\Services\Exports\Comments\ExportPDFComments;      
        return $exportPDFComments->export_attachment($this->site_id, $video_id, $sort_by, $huddle_id, $account_id, $user_id, $translations, $header_details,$file_name);
               
    }

    function print_excel_comments(Request $request) {
        $input = $request->all();
        $translations = TranslationsManager::get_page_lang_based_content("export_data", $this->site_id, '', $input['current_lang']);
        $video_id = $request->video_id;

        $huddle_id = $request->huddle_id;
        $account_id = $request->account_id;
        $user_id = $request->user_id;
        $sort_by = isset($request->sort_by) ? $request->sort_by : '';

        $exportExcelComments = new \App\Services\Exports\Comments\ExportExcelComments;
        return $exportExcelComments->export($this->site_id, $video_id, $sort_by, $huddle_id, $account_id, $user_id, $translations);
    }


    function print_excel_transcriptions(Request $request) {
        $input = $request->all();
//        $translations = TranslationsManager::get_page_lang_based_content("export_data", $this->site_id, '', $input['current_lang']);
        $subtitles = $this->getSubtitlesFromDb(
            $input['video_id']);

        $exportExcelTranscriptions = new \App\Services\Exports\Transcriptions\ExportExcelTranscriptions;
        return $exportExcelTranscriptions->export($this->site_id, $subtitles);
    }
    function print_pdf_transcriptions(Request $request) {
        include(app()->basePath('app') . '/Header/WriteHTML.php');
        $pdf =new \PDF_HTML();
        $input = $request->all();
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $huddle_id = $input['huddle_id'];
        $video_id = $input['video_id'];
        $accounts_data = DB::table('accounts')->where(array('id' => $account_id, 'site_id' => $this->site_id))->first();      
        $user_data = DB::table('users')->where(array('id' => $user_id, 'site_id' => $this->site_id))->first();
        $huddle = AccountFolder::where(array('account_folder_id' => $huddle_id ))->first();
        $framewok_id = 0;
        if($huddle->folder_type==1){
            $framewok_id = AccountTag::getFrameworkId($account_id,$huddle_id,$this->site_id);                  
          }else{
            $account_folder_document_details = AccountFolderDocument::where(array(
                'account_folder_id' => $huddle_id,
                'site_id' => $this->site_id,
                'document_id' => $video_id
            ))->first();

            if ($account_folder_document_details) {
                $account_folder_document_details = $account_folder_document_details->toArray();
            }

            $framewok_id =  $account_folder_document_details['video_framework_id'];            
        }

        $framework_data = array();
        $framework_data = AccountTag::where('account_tag_id', $framewok_id )->where('tag_type',2)->where('site_id',$this->site_id)->first();
          
        $role_id = '';
        $huddle_role = AccountFolderUser::where(array(
            'account_folder_id' => $huddle_id,
            'user_id' => $user_id,
            'site_id' => $this->site_id
        ))
        ->first();
        if ($huddle_role) {
            $huddle_role = $huddle_role->toArray();
            $role_id =  $huddle_role['role_id'];
        }
        $all_particpants = "";
        $participants = AccountFolder::getHuddleUsersIncludingGroups($this->site_id, $huddle_id, $user_id, $role_id,true)['participants'];
        if(count($participants) > 0){
            foreach($participants as $key=>$row){
                if(!empty($row['user_name'])){
                    $participants[$key] = $row['user_name'];
                }
                
            }
            $all_particpants = implode(",", $participants);
        } else{
            $all_particpants = "";
        }            
        $account_folder_document_data = DB::table('account_folder_documents')->where(array('document_id' => $video_id, 'site_id' => $this->site_id))->first();
        
        $document_data = DB::table('documents')->where(array('id' => $video_id, 'site_id' => $this->site_id))->first();
        if(strlen($all_particpants) > 85){
            $all_particpants = substr($all_particpants,'85').'...';
        }
        $video_url = '';
        
       if($huddle->folder_type ==1){
         $video_url = config('s3.sibme_base_url').'home/video_details/home/'. $huddle_id.'/'.$video_id;
       }elseif($huddle->folder_type ==2){
        $video_url = config('s3.sibme_base_url').'home/library_video/home/'. $huddle_id.'/'.$video_id;
       }elseif($huddle->folder_type ==3){
        $video_url = config('s3.sibme_base_url').'home/workspace_video/home/'. $huddle_id.'/'.$video_id;
       }
        $header_details = array(
            'logo_url'=>config('s3.sibme_base_url') ,
            'account_name'=> $accounts_data->company_name,
            'huddle_name'=>$huddle->name,
            'folder_type'=>$huddle->folder_type,
            'video_title'=>$account_folder_document_data->title,
            'uploaded_date'=> $document_data->created_date,
            'video_url'=>$video_url,
            'exported_by'=>$user_data->first_name.' '.$user_data->last_name,
            'huddle_participants'=>$all_particpants,
            'framework_title'=>!empty($framework_data->tag_title)?$framework_data->tag_title:''

        );

       $subtitles = $this->getSubtitlesFromDb($input['video_id']);
        $pdf->AddPage();
        // $pdf->SetFont('Arial','B',16);
        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(true, 15);
        
        $pdf->Image(config('s3.sibme_base_url')."/img/pdf_logo.png", 10, 10);
        $pdf->SetFont('Arial');

        $pdf->cell(50,5,'',0,0);
        $pdf->SetFont('Arial','',7);
        $pdf->cell(60,5,'Account Name :'.$header_details['account_name'],0,0);
        $pdf->cell(5,5,'',"L",0);
        $pdf->SetFont('Arial','',7);
        // $pdf->SetTextColor(6,69,173);
        $pdf->cell(60,5,'URL :'.$header_details['video_url'],0,1,'','',$header_details['video_url']);
        // $pdf->SetTextColor(0, 0, 0);

        $pdf->cell(50,5,'',0,0);
        $pdf->SetFont('Arial','',7);
        $pdf->cell(60,5,'Huddle Name:'. $header_details['huddle_name'],0,0);
        $pdf->cell(5,5,'',"L",0);
        $pdf->SetFont('Arial','',7);
        $pdf->cell(60,5,'Exported By:'. $header_details['exported_by'],0,1);

        $pdf->cell(50,5,'',0,0);
        $pdf->SetFont('Arial','',7);
        $pdf->cell(60,5,'Video Name:'. $header_details['video_title'],0,0);
        $pdf->cell(5,5,'',"L",0);
        $pdf->SetFont('Arial','',7);
        $pdf->cell(60,5,'Huddle Participants:'. $header_details['huddle_participants'],0,1);

        $pdf->cell(50,5,'',0,0);
        $pdf->SetFont('Arial','',7);
        $pdf->cell(60,5,'Upload Date:'. $header_details['uploaded_date'],0,0);
        $pdf->cell(5,5,'',"L",0);
        $pdf->SetFont('Arial','',7);
        $pdf->cell(60,5,'Framework:'. $header_details['framework_title'],0,1);
        
        $pdf->cell(0,5,'','B',1);
        // $contents = view('export/transcription_pdf', compact('subtitles','header_details'))->render(); 
        $cell_data = "";
        foreach($subtitles as $subtitle){
            $pdf->SetFont('Arial','B',16);
            $pdf->cell(0,10,$subtitle ['time_range'],0,1);
            $pdf->SetFont('Arial','',16);
            $pdf->cell(0,10,$subtitle ['subtitles'],0,1);
            $pdf->cell(0,10,'',0,1);
        }
      
        
        // $pdf->WriteHTML($contents);
        $today = "document";
        return $pdf->Output('D',$today .'.pdf',true);
        // $exportExcelTranscriptions = new \App\Services\Exports\Transcriptions\ExportPDFTranscriptions;
        // return $exportExcelTranscriptions->export($this->site_id, $subtitles,$header_details);
    }

    function addReply(Request $request) {
        $input = $request->all();

//        $input['parent_id'] = '312011';
//        $input['account_id'] = '2936';
//        $input['comment'] = 'My replyy bro';
//        $input['access_level'] = 'nested';
//        $input['huddle_id'] = '138804';
//        $input['user_id'] = '18738';
//        $input['first_name'] = 'Saad';
//        $input['last_name'] = 'Zia';
//        $input['company_name'] = 'Saad Company';
//        $input['image'] = '1960417554thumb.jpg';


        if (!isset($input['created_at_gmt']) || empty($input['created_at_gmt'])){
              $template="<b>".$input['first_name']." " .$input['last_name']." is using older version of app</b><br>
                        <b>Form Fields : </b>".json_encode($input)." <br> 
                        <b>URL :</b>".$request->url();
                    $emailData = [
                            'from' => Sites::get_site_settings('static_emails', $this->site_id)['noreply'],
                            'from_name' => Sites::get_site_settings('site_title', $this->site_id),
                            'to' => 'khurrams@sibme.com',
                            'cc' => 'waqasn@sibme.com',
                            'subject' => "created_at_gmt is required for add reply",
                            'template' => $template
                        ];

                 Email::sendCustomEmail($emailData);
          }else{
               $comment_exist = Comment::where(array(
                        'parent_comment_id' => $input["parent_id"],
                        'created_at_gmt' => $input['created_at_gmt'],
                        'user_id' => $input['user_id'],
                        'site_id' => $this->site_id
                    ))->first();
                if(!empty($comment_exist)){
                    $final_result = array(
                    'success' => 'true',
                    'comment_id' => $comment_exist->id,
                    'code'=>409,
                    'latest_reply_added' => $comment_exist
                    );
                    return $final_result;
                }
          }
        $parentId = $input['parent_id'];
        $account_id = $input['account_id'];

        $commentDocId = Comment::where(array(
                    'id' => $parentId,
                    'site_id' => $this->site_id
                ))->first();

        if ($commentDocId) {
            $commentDocId = $commentDocId->toArray();
        }
        $active = '1';
        if ($commentDocId['active'] == '1') {
            $active = '1';
        } else {
            $active = '0';
        }

        $user_id = $input['user_id'];


        if (isset($input['ref_type']) && ($input['ref_type'] == '6' || $input['ref_type'] == '7')) {
            $ref_type = $input['ref_type'];
        } else {
            $ref_type = '3';
        }
        if (isset($input['audio_duration']) && $input['audio_duration'] > 0) {
            $audio_duration = $input['audio_duration'];
        } else {
            $audio_duration = '0';
        }

        $data = array(
            'comment' => $input['comment'],
            //'access_level' => $input['access_level'],
            //'ref_id' => $parentId,
            'ref_id' => $commentDocId['ref_id'],
            'parent_comment_id' => $parentId,
            'ref_type' => $ref_type,
            'created_date' => date('y-m-d H:i:s', time()),
            'created_by' => $user_id,
            'last_edit_date' => date('y-m-d H:i:s', time()),
            'last_edit_by' => $user_id,
            'user_id' => $user_id,
            'active' => $active,
            'site_id' => $this->site_id,
            'audio_duration' => $audio_duration
        );

        $parent_comment_detail = Comment::where(array('id' => $parentId))->first();

        if (isset($input['created_at_gmt']) && !empty($input['created_at_gmt'])) {
            $data['created_at_gmt'] = $input['created_at_gmt'];
        }
        $account_folder_id = $input['huddle_id'];
        $huddle = AccountFolder::getHuddle($this->site_id, $account_folder_id, $account_id);

         if (isset($huddle[0]) && $huddle[0]['folder_type'] == '1') {
            $huddle_participant_check = AccountFolderUser
                    ::where(array(
                        'account_folder_id' => $account_folder_id,
                        'site_id' => $this->site_id,
                        'user_id' => $input['user_id']
                    ))->first();
             
             $group_ids = array();

            $huddle_groups = AccountFolderGroup::where(array(
                        'account_folder_id' => $account_folder_id,
                        'site_id' => $this->site_id
                    ))->get()->toArray();

            foreach ($huddle_groups as $group) {
                $group_ids[] = $group['group_id'];
            }
            $huddle_group_users = UserGroup::where(array(
                        'user_id' => $input['user_id'],
                        'site_id' => $this->site_id
                    ))->whereIn('group_id', $group_ids)->first();
            
          if ($huddle_participant_check || $huddle_group_users) {              
               // $huddle_participant_check = $huddle_participant_check->toArray();               
            } else {
                
                $res = array(
                    'message' => 'You are not Participating in the Huddle.',
                    'status' => 'failed'
                );
                return response()->json($res);
            }
        }

        if (isset($huddle[0]['folder_type']) && $huddle[0]['folder_type'] == '1') {
            $url = '/Huddles/view/' . $account_folder_id . '/1/' . $commentDocId['ref_id'];
        } 
        else if(isset($huddle[0]['folder_type']) && $huddle[0]['folder_type'] == '2')
        {
            $url = '/home/library_video/home/' . $account_folder_id . '/' . $commentDocId['ref_id'];
        }
        else {
            $url = '/home/workspace_video/home/' . $account_folder_id . '/' . $commentDocId['ref_id'];
        }

        $data["encoder_status"] = "Complete";
        if ($ref_type == '6' || $ref_type == '7') {
            $s3_service = new S3Services(
                    config('s3.amazon_base_url'), config('s3.bucket_name'), config('s3.access_key_id'), config('s3.secret_access_key')
            );
            $full_path = $input['comment'];
            $videoFilePath = pathinfo($full_path);
            $new_path = $videoFilePath['dirname'] . "/" . $videoFilePath['filename'];
            $s_url = config('s3.amazon_base_url') . $full_path;
            $s3_zencoder_id = $s3_service->encode_audios($s_url, $videoFilePath['dirname'], $new_path);
            $data["zencoder_output_id"] = $s3_zencoder_id;
            if(isset($input['platform']) && $input['platform'] == 'document-commenting' )
            {
                $data["encoder_status"] = "Processing";
            }
            else
            {
                $data["encoder_status"] = "Processing";
            }
            
            if(isset($input['environment_type']) && $input['environment_type'] == '1' && isset($input['platform']) && $input['platform'] == 'document-commenting')
            {
                $data["encoder_status"] = "Complete";
            }
            
            $data["comment"] = $full_path;
        }

        if (DB::table('comments')->insert($data)) {
            $comment_id = DB::getPdo()->lastInsertId();
            if(!empty($commentDocId['ref_id']))//sw-5005
            {
                Document::where('id',$commentDocId['ref_id'])->update(['last_edit_date'=>date("Y-m-d H:i:s"), 'last_edit_by'=>$input['user_id']]);
            }
            if(!empty($account_folder_id))//sw-5004
            {
                AccountFolder::where('account_folder_id', $account_folder_id)->update(['last_edit_date'=>date("Y-m-d H:i:s"), 'last_edit_by'=>$input['user_id']]);
            }
            $user_activity_logs = array(
                'desc' => $input['comment'],
                //'url' => 'comments/view/' . $comment_id,
                'url' => $url,
                'account_folder_id' => $account_folder_id,
                'ref_id' => $comment_id,
                'type' => 8,
                'environment_type' => 2,
                'site_id' => $this->site_id
            );



            $huddleUsers = AccountFolder::getHuddleUsers($account_folder_id, $this->site_id);
            $userGroups = AccountFolderGroup::getHuddleGroups($account_folder_id, $this->site_id);

            $huddle_user_ids = array();
            if ($userGroups && count($userGroups) > 0) {
                foreach ($userGroups as $row) {
                    $huddle_user_ids[] = $row['user_id'];
                }
            }
            if ($huddleUsers && count($huddleUsers) > 0) {
                foreach ($huddleUsers as $row) {
                    $huddle_user_ids[] = $row['user_id'];
                }
            }

            if (count($huddle_user_ids) > 0) {
                $user_ids = implode(',', $huddle_user_ids);
                if (!empty($user_ids))
                    $huddleUserInfo = User::where(array('site_id' => $this->site_id))->whereRaw('id IN(' . $user_ids . ')')->get()->toArray();
            } else {
                $huddleUserInfo = array();
            }


            $reciver_detail = User::where(array('id' => $commentDocId['user_id'], 'site_id' => $this->site_id))->first();

            if ($reciver_detail) {
                $reciver_detail = $reciver_detail->toArray();
            }

            $commentsData = array(
                'account_folder_id' => $account_folder_id,
                'huddle_name' => isset($huddle[0]['name']) ? $huddle[0]['name'] : '',
                'comment' => $input['comment'],
                'video_link' => '/Huddles/view/' . $account_folder_id . '/1/' . $commentDocId['ref_id'],
                'participating_users' => $huddleUserInfo,
                'email' => $reciver_detail['email'],
                'comment_id' => $parentId,
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'company_name' => $input['company_name'],
                'image' => $input['image'],
                'user_id' => $input['user_id'],
                'site_id' => $this->site_id,
                'lang' => $reciver_detail['lang']
            );
            if (!isset($input['platform']) && EmailUnsubscribers::check_subscription($commentDocId['user_id'], '6', $account_id, $this->site_id) && ($commentDocId['user_id'] != $user_id) && HelperFunctions::is_email_notification_allowed($account_id, 'enable_emails_for_add_comment')) {
                QueuesManager::sendToEmailQueue(__CLASS__, "sendVideoReplies", $this->site_id, $commentsData, $commentDocId['user_id']);
                // $this->sendVideoReplies($this->site_id, $commentsData, $commentDocId['user_id']);
            }
            
            if(!isset($input['platform']))
            {
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
            }

            $user_detail = User::where(array('id' => $user_id, 'site_id' => $this->site_id))->first();

            if ($user_detail) {
                $user_detail = $user_detail->toArray();
            }
            if (isset($huddle[0]['folder_type']) && $huddle[0]['folder_type'] == '1') {
                $this->create_churnzero_event('Huddle+Reply+Added', $account_id, $user_detail['email']);
            }

            $latest_reply = $this->get_latest_comment($comment_id);

            if($latest_reply['ref_type'] == 6) {
                $latest_reply['comment'] = $this->get_cloudfront_url($latest_reply['comment']);
            }

            //$response = $this->get_replies_from_comment($latest_reply ,'1');
            //$latest_reply['Comment'] = $response;
            $latest_reply['created_date_string'] = $this->times_ago(date('Y-m-d H:i:s'));

            $final_result = array(
                'success' => 'true',
                'comment_id' => $comment_id,
                'latest_reply_added' => $latest_reply
            );
            $custom_input = [];
            $custom_input["user_id"] = $input["user_id"];
            $custom_input["video_id"] = $commentDocId['ref_id'];
            $custom_input["huddle_id"] = $account_folder_id;
            $files = self::getVideoDocuments($request, $custom_input);
            $latest_reply["files"] = $files;
            $latest_reply["valid"] = true;
            $latest_reply["uuid"] = isset($input['uuid']) ? $input['uuid'] : null;
            if (isset($commentDocId['ref_id'])) {
                $folder_type = isset($huddle[0])?$huddle[0]['folder_type']:"";
                $doc_res = Document::get_single_video_data($this->site_id, $commentDocId['ref_id'], $folder_type, 1, $account_folder_id, $input['user_id']);
                $event = [
                    'channel' => (isset($huddle[0]['folder_type']) && $huddle[0]['folder_type'] == '2' ? 'library-'. $account_id : (isset($huddle[0]['folder_type']) && $huddle[0]['folder_type'] == '3' ? 'workspace-'. $account_id ."-". $input['user_id']:'huddle-details-' . $account_folder_id)),
                    'event' => "resource_renamed",
                    'data' => $doc_res,
                    'video_file_name' => isset($doc_res["title"])?$doc_res["title"]:'',
                    'is_dummy' => 1
                ];
                HelperFunctions::broadcastEvent($event, "broadcast_event", false); //not sending push to prevent multiple refresh
            }
            $latest_reply['responses'] = array();
            $response = $this->get_replies_from_comment($parent_comment_detail, '1');
            $latest_reply['replies_count'] = count($response['responses']);
            $latest_reply['Comment'] = $latest_reply;
            $event = [
                'channel' => 'huddle-details-' . $account_folder_id,
                'event' => "comment_added",
                'is_reply' => 1,
                'is_workspace' => (isset($huddle[0]['folder_type']) && $huddle[0]['folder_type'] == '1' ? false : true),
                'is_library' => (isset($huddle[0]['folder_type']) && $huddle[0]['folder_type'] == '2' ? true : false),
                'data' => $latest_reply,
                'reference_id' => $commentDocId['ref_id']
            ];
           if(HelperFunctions::is_push_notification_allowed($account_id))
           {
                HelperFunctions::broadcastEvent($event);
           }
           else
           {
               HelperFunctions::broadcastEvent($event,'broadcast_event',false);
           }
            return $final_result;
        } else {

            $final_result = array(
                'success' => 'false',
                'comment_id' => null
            );

            return $final_result;
        }
    }

    function addDocumentReply(Request $request) {
        $input = $request->all();

        $parentId = $input['parent_id'];

        $commentDocId = Comment::where(array(
                    'id' => $parentId,
                    'site_id' => $this->site_id
                ))->first();

        if ($commentDocId) {
            $commentDocId = $commentDocId->toArray();
        }
        $active = '1';
        if ($commentDocId['active'] == '1') {
            $active = '1';
        } else {
            $active = '0';
        }

        $user_id = $input['user_id'];

        $data = array(
            'comment' => $input['comment'],
            //'access_level' => $input['access_level'],
            //'ref_id' => $parentId,
            'ref_id' => $commentDocId['ref_id'],
            'parent_comment_id' => $parentId,
            'ref_type' => '9',
            'created_date' => date('y-m-d H:i:s', time()),
            'created_by' => $user_id,
            'last_edit_date' => date('y-m-d H:i:s', time()),
            'last_edit_by' => $user_id,
            'user_id' => $user_id,
            'active' => $active,
            'site_id' => $this->site_id,
            'xPos' => $commentDocId['xPos'],
            'yPos' => $commentDocId['yPos']
        );
        if (isset($input['created_at_gmt']) && !empty($input['created_at_gmt'])) {
            $data['created_at_gmt'] = $input['created_at_gmt'];
        }
        if (DB::table('comments')->insert($data)) {
            $comment_id = DB::getPdo()->lastInsertId();

            $final_result = array(
                'success' => 'true',
                'comment_id' => $comment_id
            );

            return $final_result;
        } else {

            $final_result = array(
                'success' => 'false',
                'comment_id' => null
            );

            return $final_result;
        }
    }

    function sendVideoReplies($site_id, $data, $user_id = '') {
        $this->site_id = $site_id;
        $site_email_subject = Sites::get_site_settings('email_subject', $this->site_id);
        $from = $data['first_name'] . " " . $data['last_name'] . '  ' . $data['company_name'] . '<' . Sites::get_site_settings('static_emails', $this->site_id)['noreply'] . '>';
        $to = $data['email'];
        //$subject = "Re: " . $data['huddle_name'] . " Reply to video comment";

        $reply_to = 'comment_' . $data['comment_id'] . config('s3.reply_to_email');

        $lang = $data['lang'];
        if ($lang == 'en') {
            $subject = "$site_email_subject - Comment Reply";
        } else {
            $subject = "$site_email_subject - Respuesta a comentario";
        }
        $key = "video_replies_" . $this->site_id . "_" . $lang;
        $sibme_base_url = config('s3.sibme_base_url');
        $redirect_url = $sibme_base_url . $data['video_link'];
        $result = SendGridEmailManager::get_send_grid_contents($key);
        if (!isset($result->versions[0])) {
            return false;
        }
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $data['first_name'] . " " . $data['last_name'], $html);
        $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
        $html = str_replace('{redirect}', $redirect_url . $data['video_link'], $html);
        $html = str_replace('{unsubscribe}', $sibme_base_url . '/subscription/unsubscirbe_now/' . $user_id . '/6', $html);
        $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
        //$html = view($this->site_id . ".video_replies", $params)->render();
        $auditEmail = array(
            'account_id' => $data['account_folder_id'],
            'email_from' => $from,
            'email_to' => $data['email'],
            'email_subject' => $subject,
            'email_body' => $html,
            'is_html' => true,
            'site_id' => $this->site_id,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {
            DB::table('audit_emails')->insert($auditEmail);
            $use_job_queue = config('s3.use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['email'], $subject, $html, true, $reply_to);
                return TRUE;
            }
        }

        return FALSE;
    }

    function add_job_queue($jobid, $to, $subject, $html, $is_html = false, $reply_to = '') {
        if ($is_html)
            $ihtml = 'Y';
        else
            $ihtml = 'N';

        if ($reply_to != '') {
            $reply_to = "<replyto>$reply_to</replyto>";
        }
        $html = '<![CDATA[' . $html . ']]>';
        $xml = "<mail><to>$to</to>$reply_to <from>" . Sites::get_site_settings('static_emails', $this->site_id)['noreply'] . "</from><fromname>" . Sites::get_site_settings('site_title', $this->site_id) . "</fromname><bcc /><cc /><subject><![CDATA[$subject]]></subject><body>$html</body><html>$ihtml</html></mail>";

        $data = array(
            'JobId' => $jobid,
            'CreateDate' => date("Y-m-d H:i:s"),
            'RequestXml' => $xml,
            'JobQueueStatusId' => 1,
            'site_id' => $this->site_id,
            'CurrentRetry' => 0,
            'JobSource' => Sites::get_base_url($this->site_id)
        );

        DB::table('JobQueue')->insert($data);
        return TRUE;
    }

    function deleteHuddleVideo(Request $request) {
        $input = $request->all();
        if(isset($input['huddle_id']))
        {
            $huddleId = $input['huddle_id'];
        }
        else
        {
            $videoId = @$input['document_id'] ? @$input['document_id'] : $input['video_id'];
            $account_folder_detail = AccountFolderDocument::where(array('document_id' => $videoId ))->first();
            if($account_folder_detail)
            {
               $huddleId = $account_folder_detail->account_folder_id; 
            }
            else
            {
               $huddleId = '';
            }
        }
        
        $tab = '1';
        $videoId = @$input['document_id'] ? @$input['document_id'] : $input['video_id'];
        $doc_type = isset($input['doc_type']) ? $input['doc_type'] : -1;
        $workspace = (isset($input['workspace']) && $input['workspace'] == 1) ? 1 : 0;
        $library = (isset($input['library']) && $input['library'] == 1) ? 1 : 0;
        $is_synced_note = isset($input['is_synced_note']) ? $input['is_synced_note'] : 0;
        $is_assignment =  EdtpaAssessmentPortfolioCandidateSubmissions::where('document_id',$videoId)->count();
        if($is_assignment){
            $is_confirmed = @$input['is_confirmed'] ? @$input['is_confirmed'] : false;
            if($is_confirmed == false){
                return array(
                    'is_assignment' => true,                   
                    'message' => TranslationsManager::get_translation('edtpa_vidoe_delete_confirmation', 'flash_messages', $this->site_id)
                );
            }
            
        }
        $assessment_sample = 0;
        if ($doc_type == 1) {
            $current_item = "Video";
            $email_current_item = "video";
        } else if ($doc_type == 2) {
            $current_item = "Resource File";
            $email_current_item = "resource";
        } else if ($doc_type == 3 && $is_synced_note) {
            $current_item = "Synced Note";
            $email_current_item = "scripted notes";
        } else if ($doc_type == 3) {
            $current_item = "Scripted Note";
            $email_current_item = "scripted notes";
        } else if ($doc_type == 5) {
            $current_item = "URL";
            $email_current_item = "URL";
        }
        else {
            $current_item = "Artifact";
            // $email_current_item = "Artifact";
        }
        $user_id = $input['user_id'];

        $account_folder = AccountFolder::where("account_folder_id", $huddleId)->first();
        $account_id = isset($input['account_id']) ? $input['account_id'] : $account_folder->account_id;
        $htype = AccountFolderMetaData::getMetaDataValue($huddleId, $this->site_id);
        $document = \DB::table("documents")->where('site_id', $this->site_id)->where('id', $videoId)->first();
        if(empty($document) || $document == NULL){
            return array(
                'success' => false,
                'sucess' => false,
                'message' => TranslationsManager::get_translation('video_is_not_deleted_please_try_again', 'flash_messages', $this->site_id)
            );
        }
        //$htype = isset($h_type['meta_data_value']) ? $h_type['meta_data_value'] : "1";
        if ($htype == 3) {
            $evaluated_participant = $this->check_if_evaluated_participant($huddleId, $user_id, 1, 1);
            if ($evaluated_participant["status"]) {
                if ($evaluated_participant["data"] && $evaluated_participant["data"]["role_id"] == 210) {
                    if ($this->check_if_assignment_submitted($huddleId, $user_id)) {
                        return array(
                            'success' => false,
                            'sucess' => false,
                            'message' => 'You cannot delete your video because you already submitted the assignment'
                        );
                    }
                }
                $AFD = DB::table('account_folder_documents')->where(array('document_id' => $videoId, 'site_id' => $this->site_id))->first();
                
                $assessment_sample = $AFD->assessment_sample;
                if ($AFD && !empty($AFD->assessment_sample) && $AFD->assessment_sample != 1) {
                    $submission_date = $this->get_submission_date($huddleId);
                    if ($submission_date != '') {
                        $submission_string = strtotime($submission_date);
                        $current_time = strtotime(date('Y-m-d H:i:s a', time()));
                        // As per SW-1910 Assessor must be able to delete the video even if the deadline is passed that's why we are excluding the role_id = 200 from following condition.
                        if ($submission_string < $current_time && $evaluated_participant["data"]["role_id"] != 200) {
                            return array(
                                'success' => false,
                                'sucess' => false,
                                'message' => TranslationsManager::get_translation('you_cannot_delete_your_video_because_your_submission_date_has_expired', 'flash_messages', $this->site_id)
                            );
                        }
                    }
                }
            }
        }


        // if ($htype != 2)
        // $this->auth_huddle_permission('deleteHuddleVideo', $huddleId, $videoId);


        if ($videoId != '') {
            $account_folder_data_of_deleted_video = AccountFolderDocument::where(array(
                        'site_id' => $this->site_id,
                        'document_id' => $videoId
                    ))->first();
            DB::table('account_folder_documents')->where(array('document_id' => $videoId, 'site_id' => $this->site_id))->delete();


            $comments = Comment::where(array('ref_id' => $videoId,'site_id' => $this->site_id))->with('replies')->get();

            $video_comments = Comment::where(array(
                        'ref_id' => $videoId,
                        'site_id' => $this->site_id
                    ))->get()->toArray();

            $del_doc_count = $del_cmt_count = $del_rep_count = 0;

            if (!empty($video_comments)) {
                foreach ($video_comments as $video_comment) {

                    $is_deleted = DB::table('comments')->where(array('site_id' => $this->site_id, 'id' => $video_comment['id']))->delete();
                    if($is_deleted){
                        if(empty($video_comment['parent_comment_id'])){
                            $del_cmt_count++;
                        } else {
                            $del_rep_count++;
                        }
                    }
                    $comment_tags = AccountCommentTag::where(array(
                                'comment_id' => $video_comment['id'],
                                'site_id' => $this->site_id
                            ))->get()->toArray();
                    if (!empty($comment_tags)) {
                        foreach ($comment_tags as $comment_tag) {
                            DB::table('account_comment_tags')->where(array('site_id' => $this->site_id, 'account_comment_tag_id' => $comment_tag['account_comment_tag_id']))->delete();
                        }
                    }
                }
            }

            $assignment_unsubmitted = 0;
            $participant_id = $user_id;
            if ($htype == 3 && ($AFD && $AFD->assessment_sample != 1)) {
                if ($evaluated_participant["data"] && $evaluated_participant["data"]["role_id"] == 200) {
                    $evaluated_participant = $this->check_if_evaluated_participant($huddleId, $document->created_by, 1, 1);
                    if ($evaluated_participant["data"] && $evaluated_participant["data"]["role_id"] == 210) {
                        $assignment_unsubmitted = 1;
                        $participant_id = $document->created_by;
                        AccountFolderUser::where("account_folder_id", $huddleId)->where("user_id", $participant_id)->update(["is_submitted" => 0]);
                        UserAssessmentCustomField::remove_user_assessment_custom_fields($participant_id, $huddleId);
                        $account_results = Account::where(array(
                                    'id' => $account_id,
                                    'site_id' => $this->site_id
                                ))->first()->toArray();

                        $data = [
                            "user_id" => $user_id,
                            "account_folder_id" => $huddleId,
                            "current_item" => strtolower($current_item),
                            "doc_title" => $AFD->title,
                            "huddle_name" => $account_folder->name,
                            "huddle_link" => "/video_huddles/assessment/" . $huddleId . "/huddle/details",
                            "company_name" => $account_results['company_name'],
                        ];
                        $this->sendUnsubmitEmail($data, $participant_id);
                    }
                }
            }
            $deleted_video_data = Document::where(array(
                        'site_id' => $this->site_id,
                        'id' => $videoId
                    ))->first();
            
            $logged_in_user_huddle_role = HelperFunctions::get_huddle_roles($huddleId,$user_id);
            if($user_id != $deleted_video_data['created_by'] || $logged_in_user_huddle_role == '210' )
            {
                $is_email_notification_allowed = HelperFunctions::is_email_notification_allowed($account_id);
                if($htype == 2 && $is_email_notification_allowed)
                {
                    $user_who_deleted = User::where(array('site_id' => $this->site_id,'id' => $user_id))->first();
                    $user_who_created = User::where(array('site_id' => $this->site_id,'id' => $deleted_video_data['created_by']))->first();
                    $huddle_detail = AccountFolder::where(array('site_id' => $this->site_id,'account_folder_id' => $huddleId))->first();
                    if($this->site_id == '1')
                    {
                       if($user_who_created['lang'] == 'en')
                       {
                            $email_subject = 'Sibme - Artifact Deleted';
                       }
                       else
                       {
                            $email_subject = 'Sibme - Artefacto Eliminado';
                       }
                    }
                    else {
                        $email_subject = 'HMH - Artifact Deleted';
                    }
                    $key = "delete_coaching_artifact_huddle_" . $this->site_id . "_" . $user_who_created['lang'];
                    $results = SendGridEmailManager::get_send_grid_contents($key);
                    $html = $results->versions[0]->html_content;
                    $html = str_replace('<%body%>', '', $html);
                    $html = str_replace('{user_who_deleted}',$user_who_deleted['first_name'] . ' ' . $user_who_deleted['last_name'], $html);
                    $html = str_replace('{huddle_name}',$huddle_detail['name'],$html);
                    $html = str_replace('{artifact_type}', $email_current_item ,$html);
                    $html = str_replace('{artifact_title}', $account_folder_data_of_deleted_video['title'] ,$html);

                    if($logged_in_user_huddle_role == '210')
                    {

                        $send_to = HelperFunctions::get_evaluator_emails($huddleId,'200');
                        QueuesManager::sendToEmailQueue(__CLASS__,"sendDeleteHuddleVideoLEmailToParticipants",$send_to,$this->site_id,$user_who_deleted,$huddle_detail['name'],$email_current_item,$account_folder_data_of_deleted_video['title'],$huddle_detail['account_id']);

                    }

                    else
                    {
                        $sibme_base_url = config('s3.sibme_base_url');
                        $unsubscribe = $sibme_base_url . 'subscription/unsubscirbe_now/' . $user_who_created['id'] . '/12';
                        $html = str_replace('{unsubscribe}', $unsubscribe ,$html);
                        $send_to[] =  $user_who_created['email'];
                        $html = str_replace('{deleted_your}', 'deleted your' ,$html);
                        $emailData = [
                            'from' => Sites::get_site_settings('static_emails', $this->site_id)['noreply'],
                            'from_name' => Sites::get_site_settings('site_title', $this->site_id),
                            'to' => $user_who_created['email'],
                            'subject' => $email_subject,
                            'template' => $html,
                        ];
                        if(!empty($huddle_detail['account_id'])){
                            $account_id = $huddle_detail['account_id'];
                        }                       
                        if(EmailUnsubscribers::check_subscription($user_who_created['id'], '12', $account_id, $this->site_id))
                        {
                             Email::sendCustomEmail($emailData);
                        }
                    }
                    

                }
            }

            $del_doc_count = DB::table('documents')->where(array('site_id' => $this->site_id, 'id' => $videoId))->delete();
            if($del_doc_count){
                $is_assignment =  EdtpaAssessmentPortfolioCandidateSubmissions::where('document_id',$videoId)->count();
                if($is_assignment){
                  EdtpaAssessmentPortfolioCandidateSubmissions::where('document_id',$videoId)->delete();
                }
              }
            // Update Analytics Summary Table
            $videos_viewed_count = UserActivityLog::where('ref_id',$videoId)->where('type',11)->count();

            $analyticsHuddlesSummary = new \App\Models\AnalyticsHuddlesSummary;
            // Disabled following because count is also not decreasing in old analytics.

            //decrease the count from analytics when a video is deleted

            $account_folder_id = $huddleId;

            foreach($comments as $commenting)
            {
                if($commenting->ref_type == 3)
                {
                    $analyticsHuddlesSummary->saveRecord($account_id, $account_folder_id, $commenting->user_id, $this->site_id, 8, date('Y-m-d',strtotime($commenting->created_date)), 1, true);
                }
                else
                {
                    $analyticsHuddlesSummary->saveRecord($account_id, $account_folder_id, $commenting->user_id, $this->site_id, 5, date('Y-m-d',strtotime($commenting->created_date)), 1, true);
                }

                foreach($commenting->replies as $reply)
                {
                    $analyticsHuddlesSummary->saveRecord($account_id, $account_folder_id, $reply->user_id, $this->site_id, 8, date('Y-m-d',strtotime($reply->created_date)), 1, true);

                        foreach($reply->replies as $nestedReply)
                        {
                            $analyticsHuddlesSummary->saveRecord($account_id, $account_folder_id, $nestedReply->user_id, $this->site_id, 8, date('Y-m-d',strtotime($nestedReply->created_date)), 1, true);
                        }

                }
            }
            ////////////////////////

            if($del_doc_count){
//                $analyticsHuddlesSummary->saveRecord($account_folder->account_id, $account_folder->account_folder_id, $deleted_video_data['created_by'], $this->site_id, 2, date('Y-m-d',strtotime($deleted_video_data['created_date'])), 1, true);
            }
            if($del_cmt_count>0){
                // $analyticsHuddlesSummary->saveRecord($account_folder->account_id, $account_folder->account_folder_id, $deleted_video_data['created_by'], $this->site_id, 5, date('Y-m-d',strtotime($deleted_video_data['created_date'])), $del_cmt_count, true);
            }
            if($del_rep_count>0){
                // $analyticsHuddlesSummary->saveRecord($account_folder->account_id, $account_folder->account_folder_id, $deleted_video_data['created_by'], $this->site_id, 8, date('Y-m-d',strtotime($deleted_video_data['created_date'])), $del_rep_count, true);
            }
            if($videos_viewed_count>0){
                $analyticsHuddlesSummary->saveRecord($account_folder->account_id, $account_folder->account_folder_id, $deleted_video_data['created_by'], $this->site_id, 11, date('Y-m-d',strtotime($deleted_video_data['created_date'])), $videos_viewed_count, true);
            }

            // $analyticsHuddlesSummary->saveRecord($folder_type[0]->account_id, $folder_type[0]->account_folder_id, $user_id, $this->site_id, 8, date('Y-m-d',strtotime($comment->created_date)), $del_total_rep_count, true);
        

            $audit_deletion_log = array(
                'entity_id' => $huddleId,
                'deleted_entity_id' => $videoId,
                'entity_name' => $account_folder_data_of_deleted_video['title'],
                'entity_data' => json_encode($deleted_video_data),
                'created_by' => $user_id,
                'created_at' => date("Y-m-d H:i:s"),
                'site_id' => $this->site_id,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'posted_data' => json_encode($input),
                'action' => $_SERVER['REQUEST_URI'],
                'user_agenet' => $_SERVER['HTTP_USER_AGENT'],
                'request_url' => $_SERVER['REQUEST_URI'],
                'platform' => '',
                'created_date' => date("Y-m-d H:i:s"),
            );
            DB::table('audit_deletion_log')->insert($audit_deletion_log);



            DB::table('document_standard_ratings')->where(array('site_id' => $this->site_id, 'document_id' => $videoId))->delete();
            $this->delete_video_attachments($videoId, $input, $user_id);
            $this->CreateJobQueueArchiveJob($videoId);


            $account_folder_observation = Document::where(array(
                        'doc_type' => 3,
                        'site_id' => $this->site_id,
                        'is_associated' => $videoId
                    ))->first();

            if ($account_folder_observation) {
                $account_folder_observation = $account_folder_observation->toArray();
            }


            if (is_array($account_folder_observation) && count($account_folder_observation) > 0) {
                DB::table('account_folder_documents')->where(array('site_id' => $this->site_id, 'account_folder_id' => $huddleId,
                    'document_id' => $account_folder_observation['id']))->delete();

                $del_doc_count = DB::table('documents')->where(array('site_id' => $this->site_id, 'id' => $account_folder_observation['id']))->delete();
            }
            //Following query will only run for library and workspace
            //DB::table('account_folders')->where(array('site_id' => $this->site_id, 'account_folder_id' => $huddleId))->whereIn("folder_type",[2,3])->update(["active"=>0]);
            DB::table('documents')->where(array('site_id' => $this->site_id, 'id' => $videoId))->update(["active"=>0]);
            $channel_name = 'huddle-details-' . $huddleId;
            $subjects = [];
            $uncat_videos = 0;
            if ($workspace) {
                $account_id = $input["account_id"];
                $channel_name = "workspace-" . $account_id . "-" . $user_id;
            }
            else if($library)
            {
                $channel_name = "library-" . $account_id;
                $subjects = AccountFolder::getSubjects($account_id, $this->site_id);
                $uncat_videos = AccountFolder::getVideoLibraryCount('uncat', $account_id, $this->site_id, null, false, "", "");
            }
            $channel_data = array(
                'item_id' => $videoId,
                'data' => $videoId,
                'assessment_sample' => $assessment_sample,
                'huddle_id' => $huddleId,
                'deleted_by' => $user_id,
                'channel' => $channel_name,
                'sidebar_categories' => $subjects,
                'uncat_videos' => $uncat_videos,
                'participant_id' => $participant_id,
                'assignment_unsubmitted' => $assignment_unsubmitted,
                'doc_type' => $doc_type,
                'parent_folder_id' => !empty($document->parent_folder_id)?$document->parent_folder_id:'',
                'is_processed' => !empty($document->is_processed)?$document->is_processed:'',
                'event' => "resource_deleted"
            );
            
            $is_push_notification_allowed = HelperFunctions::is_push_notification_allowed($account_id);
            if(!$is_push_notification_allowed){
                HelperFunctions::broadcastEvent($channel_data, 'broadcast_event', false);
            } else {
                HelperFunctions::broadcastEvent($channel_data, 'broadcast_event', true,false);
                //HelperFunctions::broadcastEvent($channel_data);
            }

            

            if ($doc_type == 1) {
                return array(
                    'success' => true,
                    'sucess' => true,
                    'message' => TranslationsManager::get_translation('video_has_been_deleted_successfully', 'flash_messages', $this->site_id)
                );
            } elseif ($doc_type == 2) {
                return array(
                    'success' => true,
                    'sucess' => true,
                    'message' => TranslationsManager::get_translation('resource_has_been_deleted_successfully_msg', 'flash_messages', $this->site_id)
                );
            } elseif (($doc_type == 3) && $is_synced_note) {

                return array(
                    'success' => true,
                    'sucess' => true,
                    'message' => TranslationsManager::get_translation('synced_notes_has_been_deleted_successfully_msg', 'flash_messages', $this->site_id)
                );
            } elseif (($doc_type == 3)) {

                return array(
                    'success' => true,
                    'sucess' => true,
                    'message' => TranslationsManager::get_translation('scripted_notes_has_been_deleted_successfully_msg', 'flash_messages', $this->site_id)
                );
            } else {
                return array(
                    'success' => true,
                    'sucess' => true,
                    'message' => TranslationsManager::get_translation('artifacts_notes_has_been_deleted_successfully_msg', 'flash_messages', $this->site_id)
                );
            }
        } else {
            return array(
                'success' => false,
                'sucess' => false,
                'message' => TranslationsManager::get_translation('video_is_not_deleted_please_try_again', 'flash_messages', $this->site_id)
            );
        }
    }

    function sendUnsubmitEmail($data, $reciver_id = '') {
        $site_email_subject = Sites::get_site_settings('email_subject', $this->site_id);
        $from = $data['company_name'] . '<' . Sites::get_site_settings('static_emails', $this->site_id)['noreply'] . '>';

        $data['reciver_id'] = $reciver_id;
        $reciver_detail = User::where(array('id' => $reciver_id, 'site_id' => $this->site_id))->first();
        $lang = $reciver_detail->lang;
        if ($lang == 'en') {
            $subject = "$site_email_subject - Assignment Unsubmitted";
        } else {
            $subject = "$site_email_subject - Asignación no enviada";
        }

        $key = "assignment_unsubmitted_email_" . $this->site_id . "_" . $lang;
        $sibme_base_url = config('s3.sibme_base_url');
        $result = SendGridEmailManager::get_send_grid_contents($key);
        if (!$result || !isset($result->versions[0]) && empty($result->versions[0]->html_content)) {
            return false;
        }

        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{assessee_name}', $reciver_detail->first_name . " " . $reciver_detail->last_name, $html);
        $html = str_replace('{current_item}', $data['current_item'], $html);
        $html = str_replace('{doc_title}', $data['doc_title'], $html);
        $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
        $html = str_replace('{redirect_url}', $sibme_base_url . $data['huddle_link'], $html);
        $html = str_replace('{unsubscribe_url}', $sibme_base_url . '/subscription/unsubscirbe_now/' . $reciver_id . '/7', $html);
        $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
        //$html = view($this->site_id . ".feedback_email", $params)->render();
        $auditEmail = array(
            'account_id' => $data['account_folder_id'],
            'site_id' => $this->site_id,
            'email_from' => $from,
            'email_to' => $reciver_detail->email,
            'email_subject' => $subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($reciver_detail->email)) {
            DB::table('audit_emails')->insert($auditEmail);
            $use_job_queue = config('s3.use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $reciver_detail->email, $subject, $html, true);
                return TRUE;
            }
        }

        return FALSE;
    }

    function sendDeleteHuddleVideoLEmailToParticipants ($send_to,$site_id,$user_who_deleted,$huddle_name,$email_current_item,$artifact_title,$account_id){
        foreach($send_to as $row)
        {
            if($site_id == '1')
            {
                if($row['lang'] == 'en')
                {
                    $email_subject = 'Sibme - Artifact Deleted';
                }
                else
                {
                    $email_subject = 'Sibme - Artefacto Eliminado';
                }
            }
            else
            {
                $email_subject = 'HMH - Artifact Deleted';
            }
                $key = "delete_coaching_artifact_huddle_" . $site_id . "_" . $row['lang'];
                $results = SendGridEmailManager::get_send_grid_contents($key);
                $html = $results->versions[0]->html_content;
                $html = str_replace('<%body%>', '', $html);
                $html = str_replace('{user_who_deleted}',$user_who_deleted['first_name'] . ' ' . $user_who_deleted['last_name'], $html);
                $html = str_replace('{huddle_name}',$huddle_name,$html);
                $html = str_replace('{artifact_type}', $email_current_item ,$html);
                $html = str_replace('{artifact_title}', $artifact_title ,$html);
                $html = str_replace('{deleted_your}', 'deleted' ,$html);
                $sibme_base_url = Sites::get_base_url($site_id);
                $unsubscribe = $sibme_base_url . 'subscription/unsubscirbe_now/' . $row['id'] . '/12';
                $html = str_replace('{unsubscribe}', $unsubscribe ,$html);
                $emailData = [
                'from' => Sites::get_site_settings('static_emails', $site_id)['noreply'],
                'from_name' => Sites::get_site_settings('site_title', $site_id),
                'to' => $row['email'],
                'subject' => $email_subject,
                'template' => $html,
                ];
        if(EmailUnsubscribers::check_subscription($row['id'], '12', $account_id, $site_id))
        {
        Email::sendCustomEmail($emailData);
        }
    }
    }
    function check_if_assignment_submitted($huddle_id, $user_id, $group_id = null) {
        $submission_record = AccountFolderUser::where("account_folder_id", $huddle_id)->where("user_id", $user_id)->where("site_id", $this->site_id)->first();

        if ($submission_record && $submission_record->is_submitted == 1) {
            return true;
        }
        return false;
    }

    function get_submission_date($huddle_id, $return = false) {

        $submission_deadline_date = AccountFolderMetaData::where(array(
                    'site_id' => $this->site_id,
                    "meta_data_name" => "submission_deadline_date",
                    "account_folder_id" => $huddle_id
                ))->first();
        if ($submission_deadline_date) {
            $submission_deadline_date = $submission_deadline_date->toArray();
        }

        $submission_deadline_time = AccountFolderMetaData::where(array(
                    'site_id' => $this->site_id,
                    "meta_data_name" => "submission_deadline_time",
                    "account_folder_id" => $huddle_id
                ))->first();


        if ($submission_deadline_time) {
            $submission_deadline_time = $submission_deadline_time->toArray();
        }


        if ($submission_deadline_date['meta_data_value'] != '') {

            //$date_modified = explode('-', $submission_deadline_da     te['meta_data_value']);
            //$date_modified = date_create($submission_deadline_date['meta_data_value']);
            //$date = $date_modified[2] . '-' . $date_modified[0] . '-' . $date_modified[1];
            //$date = date_format($date_modified, "Y-m-d");
            $date_modified = explode('-', $submission_deadline_date['meta_data_value']);
            $date = $date_modified[2] . '-' . $date_modified[0] . '-' . $date_modified[1];
            //$date = date('Y-m-d', strtotime(trim($submission_deadline_date['meta_data_value'])));
            // $time = $submission_deadline_time['AccountFolderMetaData']['meta_data_value'] . ':00';
            $time = date("h:i:s a", strtotime($submission_deadline_time['meta_data_value']));
            return $date . ' ' . $time;
        } else {
            return '';
        }
    }

    function CreateJobQueueArchiveJob($document_id) {

        $jobQueueString = "<ArchiveJob><DocumentID>$document_id</DocumentID></ArchiveJob>";


        $data = array(
            'JobId' => '6',
            'CreateDate' => date("Y-m-d H:i:s"),
            'RequestXml' => $jobQueueString,
            'JobQueueStatusId' => 1,
            'site_id' => $this->site_id,
            'CurrentRetry' => 0,
            'JobSource' => Sites::get_base_url($this->site_id)
        );


        if (DB::table('JobQueue')->insert($data)) {

            return DB::getPdo()->lastInsertId();
        } else {
            return false;
        }
    }

    function type_pause(Request $request) {
        $user_id = $request->user_id;
        $account_id = $request->account_id;
        $value = $request->value;

        $data = array(
            'type_pause' => $value,
            'site_id' => $this->site_id
        );
        DB::table('users_accounts')->where(['site_id' => $this->site_id, 'user_id' => $user_id, 'account_id' => $account_id])->update($data);

        return array(
            'success' => true
        );
    }

    function bread_crumb_output($huddle_id) {

        $huddle_info = AccountFolder::where(array(
                    'account_folder_id' => $huddle_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($huddle_info) {
            $huddle_info = $huddle_info->toArray();
        }

        if ($huddle_info['parent_folder_id'] != '0' && !empty($huddle_info['parent_folder_id'])) {

            $folder_details = AccountFolder::where(array(
                        'account_folder_id' => $huddle_info['parent_folder_id'],
                        'site_id' => $this->site_id
                    ))->first();
            if ($folder_details) {
                $folder_details = $folder_details->toArray();
            }


            $folder_label = AccountFolder::select('name')->where(array(
                        'account_folder_id' => $folder_details['account_folder_id'],
                        'site_id' => $this->site_id
                    ))->first();

            if ($folder_label) {
                $folder_label = $folder_label->toArray();
            }

            $crumb_output = $this->add_new_folder_breadcrum($folder_label['name'] . '_0f', $folder_details['account_folder_id']);

            $new_array = array(
                'folder_id' => $folder_details['account_folder_id'],
                'folder_name' => $folder_details['name']
            );

            array_push($crumb_output, $new_array);

            //$crumb_output =  $crumb_output. ' <a href="/Folder/' . addslashes ($folder_details['account_folder_id']) . '">' . addslashes ($folder_details['name']) . '</a>';

            return $crumb_output;
        }
    }

    function add_new_folder_breadcrum($label, $url) {


        $crumb = array();
        $crumb[$label] = $url;
        $parents = array();
        $bread_crumb_path = ($this->get_all_parents($url, $parents));
        $bread_crumb_path['Home'] = 'huddles';
        $bread_crumb_path = array_reverse($bread_crumb_path);



        $check_duplicate = $this->check_duplicate($crumb, $bread_crumb_path);

        if ($check_duplicate > 0) {

            $break_array = $this->break_array($bread_crumb_path, $crumb);


            $array_merge = $this->safe_array_merge($break_array, $crumb);
            //clear session variable
            //and prepare the session variable to be loaded with the exact information location
            $this->unset_variable();

            $output = $this->output_new_breadcrum($break_array, $label, $url);
        } else {

            $output = $this->output_new_breadcrum($bread_crumb_path, $label, $url);
        }
        //return the output

        return $output;
    }

    function get_all_parents($huddle_id, $parents) {



        $result = AccountFolder::where(array(
                    "account_folder_id" => $huddle_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($result) {
            $result = $result->toArray();
        }


        if ($result['parent_folder_id'] == NULL) {
            return $parents;
        } else {


            $result1 = AccountFolder::where(array(
                        "account_folder_id" => $result['parent_folder_id'],
                        'site_id' => $this->site_id
                    ))->first();

            if ($result1) {
                $result1 = $result1->toArray();
            }


            $parents[$result1['name'] . '_0f'] = $result['parent_folder_id'];
            return $this->get_all_parents($result['parent_folder_id'], $parents);
        }
    }

    function check_duplicate($array1, $array2) {


        $array = array_intersect_assoc($array1, $array2);

        //return the number of occurences
        return count($array);
    }

    function safe_array_merge($array1, $array2) {
        return array_merge($array1, $array2);
    }

    function output_new_breadcrum($array, $label, $url) {

        $count = 0;
        $link = "";
        $ouput_array = array();

        foreach ($array as $key => $value) {
            //    $count++;
            //    if ($count < $this->url_count) {
            if ($key == 'Home') {

            } else {
                $k = substr($key, 0, -3);
                //  $link .="<a href='" . $this->base . '/Folder/' . $value . "'>" . $k . "</a>";
                $link .= '<a href = "/Folder/' . $value . '"   >' . addslashes($k) . '</a>';

                $ouput_array[] = array(
                    'folder_id' => $value,
                    'folder_name' => addslashes($k)
                );
            }
            //  }
        }

        return $ouput_array;
    }

    function break_array($array1, $array2) {

        $count = 0;
        foreach ($array1 as $key => $value) {
            $count++;
            if (($value == @$array2[$key])) {

                $num = $count;
            }
        }

        while ((count($array1) + 1) > ($num)) {

            array_pop($array1); //prune until we reach the $level we've allocated to this page
        }

        return $array1;
    }

    function EmailFeedbackVideo($account_folder_id, $video_id, $account_id, $account_results, $user_results, $site_id) {
        $this->site_id = $site_id;
        $video_data = Document::where(array('site_id' => $site_id,
                    'id' => $video_id))->first();

        if ($video_data) {
            $video_data = $video_data->toArray();
        }

        $video_data_name = AccountFolderDocument::where(array('site_id' => $site_id, 'document_id' => $video_id))->first();

        if ($video_data_name) {
            $video_data_name = $video_data_name->toArray();
        }

        $huddle = AccountFolder::getHuddle($site_id, $account_folder_id, $account_id);
        $huddleUsers = AccountFolder::getHuddleUsers($account_folder_id, $site_id);
        $userGroups = AccountFolderGroup::getHuddleGroups($account_folder_id, $site_id);

        $coach_ids = $this->get_evaluator_ids($account_folder_id);
        $huddle_user_ids = array();
        if ($userGroups && count($userGroups) > 0) {
            foreach ($userGroups as $row) {
                $huddle_user_ids[] = $row['user_id'];
            }
        }
        if ($huddleUsers && count($huddleUsers) > 0) {
            foreach ($huddleUsers as $row) {
                $huddle_user_ids[] = $row['user_id'];
            }
        }

        if (count($huddle_user_ids) > 0) {
            $user_ids = implode(',', $huddle_user_ids);
            if (!empty($user_ids))
                $huddleUserInfo = User::where(array('site_id' => $site_id))->whereRaw('id IN(' . $user_ids . ')')->get()->toArray();
        } else {
            $huddleUserInfo = array();
        }

        $coach_details = User::where(array('site_id' => $site_id,
                    'id' => $coach_ids))->get()->toArray();
        $h_type = AccountFolderMetaData::getMetaDataValue($account_folder_id, $site_id);

        if ($huddleUserInfo && count($huddleUserInfo) > 0) {
            $commentsData = array(
                'account_folder_id' => $account_folder_id,
                'site_id' => $site_id,
                'huddle_name' => $huddle[0]['name'],
                'video_link' => '/Huddles/view/' . $account_folder_id . '/1/' . $video_id,
                'participating_users' => $huddleUserInfo,
                'video_title' => $video_data_name['title'],
                'coach_details' => $coach_details,
                'company_name' => $account_results['company_name'],
                'first_name' => $user_results['first_name'],
                'last_name' => $user_results['last_name'],
                'image' => $user_results['image'],
            );

            foreach ($huddleUserInfo as $row) {
                if (($h_type == '3' && $user_results['id'] == $row['id']) || ($h_type == '2' && !$this->check_if_evalutor($account_folder_id, $row['id']))) {
                    $commentsData ['email'] = $row['email'];
                    if (EmailUnsubscribers::check_subscription($row['id'], '7', $account_id, $site_id)) {
                        $this->sendEmailFeedBackVideo($commentsData, $row['id'],$site_id);
                    }
                }
            }
        }
    }

    function get_evaluator_ids($huddle_id) {

        $result = AccountFolderUser::where(array(
                    'role_id' => 200,
                    'site_id' => $this->site_id,
                    'account_folder_id' => $huddle_id
                ))->get()->toArray();


        $evaluator = array();
        if ($result) {
            foreach ($result as $row) {
                $evaluator[] = $row['user_id'];
            }
            return $evaluator;
        } else {
            return [];
        }
    }

    function sendEmailFeedBackVideo($data, $user_id = '',$site_id) {
        $site_email_subject = Sites::get_site_settings('email_subject', $site_id);
        $site_title = Sites::get_site_title($site_id);
        $from = $data['first_name'] . " " . $data['last_name'] . '  ' . $data['company_name'] . '<' . Sites::get_site_settings('static_emails', $site_id)['noreply'] . '>';
        //$subject = "Re:[Huddle] " . $data['huddle_name'] . ": Feedback is ready for you to view";


        $data['user_id'] = $user_id;

        $h_type = AccountFolderMetaData::getMetaDataValue($data['account_folder_id'], $site_id);
        $params = array(
            'data' => $data,
            'site_id' => $site_id,
            'user_id' => $user_id,
            'htype' => $h_type,
            'site_title' => $site_title
        );

        $reciver_detail = User::where(array('id' => $user_id, 'site_id' => $site_id))->first();
        $lang = $reciver_detail['lang'];
        if ($lang == 'en') {
            $subject = "$site_email_subject - Comments Published";
        } else {
            $subject = "$site_email_subject - Comentarios Publicados";
        }

        $key = "feedback_email_" . $site_id . "_" . $lang;
        $sibme_base_url = Sites::get_base_url($site_id);
        $result = SendGridEmailManager::get_send_grid_contents($key);
        if (!$result || !isset($result->versions[0]) && empty($result->versions[0]->html_content)) {
            return false;
        }
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $data['first_name'] . " " . $data['last_name'], $html);
        $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
        $html = str_replace('{redirect_url}', $sibme_base_url . $data['video_link'], $html);
        $html = str_replace('{unsubscribe_url}', $sibme_base_url . '/subscription/unsubscirbe_now/' . $user_id . '/7', $html);
        $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
        //$html = view($this->site_id . ".feedback_email", $params)->render();
        $auditEmail = array(
            'account_id' => $data['account_folder_id'],
            'site_id' => $site_id,
            'email_from' => $from,
            'email_to' => $data['email'],
            'email_subject' => $subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {
            DB::table('audit_emails')->insert($auditEmail);
            $use_job_queue = config('s3.use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['email'], $subject, $html, true);
                return TRUE;
            }
        }

        return FALSE;
    }

    function copyMultipleArtificats(Request $request, $fire_websocket = 1) {
        $input = $request->all();


//        $input['document_id'] = '216336';
//        $input['account_folder_id'] = array('56177','51897','-1');
//        $input['current_huddle_id'] = '54397';
//        $input['account_id'] = '181';
//        $input['user_id'] = '621';
//        $input['copy_notes'] = 1;

        $document_id = $input['document_id'];

        $account_folder_id = isset($input['account_folder_id']) ? $input['account_folder_id'] : '';
        $current_account_folder_id = isset($input['current_huddle_id']) ? $input['current_huddle_id'] : '';
        $workspace = $request->has("workspace") ? $request->get("workspace") : 0;
        $parent_folder_id = $request->has("parent_folder_id") ? $request->get("parent_folder_id") : 0;
        $from_workspace = $request->has("from_workspace") ? $request->get("from_workspace") : 0;
        $is_scripted_note = $request->has("is_scripted_note") ? $request->get("is_scripted_note") : 0;
        $is_duplicated = $request->has("is_duplicated") ? $request->get("is_duplicated") : 0;
        $doc_type = $request->has("doc_type") ? $request->get("doc_type") : 1;
        $lang = app("Illuminate\Http\Request")->header('current-lang');
        $site_id = $request->header('site_id');



        $h_type = AccountFolderMetaData::where('account_folder_id', $account_folder_id)->where('meta_data_name', 'folder_type')->first();
        $htype = isset($h_type->meta_data_value) ? $h_type->meta_data_value : "1";
            if ($htype == 1) {
                $huddle_type = 'Collaboration Huddle';
            } elseif ($htype == 2) {
                $huddle_type = 'Coaching Huddle';
            } else {
                $huddle_type = 'Assessment Huddle';
            }
        if ($huddle_type == 'Assessment Huddle' && !HelperFunctions::check_if_evalutor($account_folder_id, $input['user_id'])) {

            $user_account_detail = UserAccount::where('account_id', $input['account_id'])->where('user_id', $input['user_id'])->first();
            $user_detail = User::where('id', $input['user_id'])->first();
            $user_role = HelperFunctions::get_user_role_name($user_account_detail['role_id']);
            $in_trial_intercom = HelperFunctions::check_if_account_in_trial_intercom($input['account_id']);

            $meta_data = array(
                'huddle_video' => 'true',
                'huddle_type' => $huddle_type,
                'user_role' => $user_role,
                'is_in_trial' => $in_trial_intercom,
                'Platform' => 'Web'
            );



            //todo : commented by saad and hamid due to duplication.
          //  HelperFunctions::create_intercom_event('huddle-video-submitted-for-assessment', $meta_data, $user_detail['email']);
        }


        $is_mobile_request = isset($input['app_name']) ? HelperFunctions::is_mobile_request($input['app_name']) : false;

        if($is_mobile_request)
        {
            if(isset($input['environment_type']) && $input['environment_type'] == '1' )
            {
              $platform = 'iOS';
            }
            elseif(isset($input['environment_type']) && $input['environment_type'] == '3')
            {
               $platform = 'Android';
            }
            else {
                $platform = 'Mobile';
            }

        }
        else
        {
           $platform = 'Web';
        }

        $h_type = AccountFolderMetaData::where('account_folder_id', $account_folder_id)->where('meta_data_name', 'folder_type')->first();
        $htype = isset($h_type->meta_data_value) ? $h_type->meta_data_value : "1";
            if ($htype == 1) {
                $huddle_type = 'Collaboration Huddle';
            } elseif ($htype == 2) {
                $huddle_type = 'Coaching Huddle';
            } else {
                $huddle_type = 'Assessment Huddle';
            }
        if ($huddle_type == 'Assessment Huddle' && !HelperFunctions::check_if_evalutor($account_folder_id, $input['user_id'])) {
            $user_account_detail = UserAccount::where('account_id', $input['account_id'])->where('user_id', $input['user_id'])->first();
            $user_detail = User::where('id', $input['user_id'])->first();
            $user_role = HelperFunctions::get_user_role_name($user_account_detail['role_id']);
            $in_trial_intercom = HelperFunctions::check_if_account_in_trial_intercom($input['account_id']);
            $meta_data = array(
                'huddle_video' => 'true',
                'huddle_type' => $huddle_type,
                'user_role' => $user_role,
                'is_in_trial' => $in_trial_intercom,
                'Platform' => $platform
            );
            HelperFunctions::create_intercom_event('huddle-video-submitted-for-assessment', $meta_data, $user_detail['email']);
        }

        if(!$is_mobile_request)
        {

            if($is_duplicated || $input['copy_notes'] == 0  )
            {
                $input['copy_notes'] = false;
            }
        }

        if ($is_mobile_request) {
            if (isset($input['is_duplicated']) && $input['is_duplicated'] && $input['is_duplicated'] != '0') {
                $input['copy_notes'] = false;
            }
        }


        if ($is_duplicated) {
            $source_type = 3;
        } else {
            $source_type = 4;
        }


        if ($doc_type == 1) {
            $current_resource = "video";
        } else if ($doc_type == 2) {
            $current_resource = "resource";
        } else if ($doc_type == 3) {
            if ($lang == 'es') {
                $current_resource = TranslationsManager::get_translation('scripted_note_msg', 'flash_messages', $this->site_id);
            } else {
                $current_resource = TranslationsManager::get_translation('scripted_note_msg', 'flash_messages', $this->site_id);
            }
            $current_resource = 'scripted note';
            if(!$is_scripted_note)
            {
               $current_resource = "video";
            }
        }
        else if($doc_type == 5)
        {
            $current_resource = "url";
        }
        else
        {
            $current_resource = "artifact";
        }
        $result = array(
            'success' => FALSE,
            'message' => ''
        );

        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $destination_huddle_type = null;

        if (isset($input['sample']) && !empty($input['sample'])) {
            $is_sample = (string) $input['sample'];
        } else {
            $is_sample = '0';
        }

        $user_results = User::where(array(
                    'id' => $user_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($user_results) {
            $user_results = $user_results->toArray();
        }

        $account_results = Account::where(array(
                    'id' => $account_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($account_results) {
            $account_results = $account_results->toArray();
        }

        if (!empty($document_id)) {
            $conditions = array('site_id' => $this->site_id, 'document_id' => $document_id, 'account_folder_id' => $current_account_folder_id);

            $myfilesRow = AccountFolderDocument::get_row($conditions);

            if (!empty($account_folder_id) && count($account_folder_id) > 0 && $myfilesRow != '') {
                $copyRecord = array();
                $docs_data = array();
                $is_copied_video_library = false;
                $copied_video_id = array();
                $all_parent_folder_ids = array();
                $all_account_folder_ids = array();
                $total_records = count($account_folder_id);
                for ($i = 0; $i <$total_records;  $i++) {
                    $parent_folder_id = explode('_',$account_folder_id[$i]);
                    $account_folder_id =  $parent_folder_id[0];
                    $parent_folder_id =  isset($parent_folder_id[1])?$parent_folder_id[1]:'';
                    $all_parent_folder_ids[] = $parent_folder_id;
                    $all_account_folder_ids[] = $account_folder_id;
                    foreach ($myfilesRow as $row) {
                        if ((int) $account_folder_id == -1) {
                            $user_account_data = UserAccount::where("account_id", $account_id)->where("user_id", $user_id)->first();
                            if ($user_account_data->permission_access_video_library != 1 || $user_account_data->permission_video_library_upload != 1) {
                                $result["message"] = "You don't have permissions to share $current_resource to library";
                                return $result;
                            }
                            $this->copyToVideoLibrary($myfilesRow, $account_id, $user_id, $request->get("categories"));
                            $is_copied_video_library = true;
                            continue;
                        }
                        if (AccountFolderMetaData::check_if_eval_huddle($account_folder_id, $this->site_id) == 1) {
                            $destination_huddle_type = '3';
                            if ($this->check_if_evaluated_participant($account_folder_id, $user_id) == 1) {
                                $get_eval_participant_videos = Document::get_eval_participant_videos($account_folder_id, $user_id, $this->site_id);
                                $submission_allowed_count = Document::get_submission_allowed($account_folder_id, $this->site_id);
                                if ($get_eval_participant_videos >= $submission_allowed_count) {
                                    $result['message'] = str_replace("video", $current_resource, TranslationsManager::get_translation('you_have_already_submitted_video_msg', 'flash_messages', $this->site_id));
                                    return $result;
                                }

                                $submission_date = $this->get_submission_date($account_folder_id);
                                if ($submission_date != '') {
                                    $submission_string = strtotime($submission_date);
                                    $current_time = strtotime(date('Y-m-d H:i:s a', time()));
                                    if ($submission_string < $current_time) {
                                        return array(
                                            'success' => false,
                                            'message' => TranslationsManager::get_translation('discussion_copy_resource_warn_msg_part_1', 'Api/huddle_list', $this->site_id, $user_id) . " $current_resource " . TranslationsManager::get_translation('discussion_copy_resource_warn_msg_part_2', 'Api/huddle_list', $this->site_id, $user_id)
                                        );
                                    }
                                }
                            } else {
                                $is_sample = 1;
                            }
                        }
                        $document = Document::get_document_row($row['document_id'], $this->site_id);
                        if (empty($document))
                            continue;
                        /* $no_of_copies = $row['no_of_copies'] + 1;
                          if ($from_workspace) { */

                        $temp_count = Document::join("account_folder_documents", "documents.id", "=", "account_folder_documents.document_id")->where("documents.account_id", $account_id)->where("documents.created_by", $user_id)->where(function ($query) use ($row) {
                            $query->where("account_folder_documents.title", 'like', "'%" . $this->mysql_escape($row['title']) . "%'");
                            //->orWhere("account_folder_documents.title", "REGEXP", "'".$row['title'] . " - [2-50]$)"."'");
                        });
                        if (!$from_workspace) {
                            $temp_count->where("account_folder_documents.account_folder_id", $account_folder_id);
                        }
                        $temp_count = $temp_count->count();
                        if ($temp_count) {
                            $no_of_copies = $temp_count + 1;
                        } else {
                            $no_of_copies = 2;
                        }
                        // }
                        $old_video_id[] = $document['id'];
                        $new_doc_type = $document['doc_type'];
                        //if ($from_workspace) {
                        if ($document['doc_type'] == '1' || (($document['doc_type'] == '3' && ($document['is_processed'] == 4 || $document['is_processed'] == 5)) && !$is_scripted_note)) {
                            $new_doc_type = 1;
                        }
                        //}
                        $docs_data[] = array('source_type' => $source_type, 'source_document_id' => $document_id, 'id' => null, 'created_date' => date("Y-m-d H:i:s"), 'last_edit_date' => date("Y-m-d H:i:s"), 'recorded_date' => date("Y-m-d H:i:s"), 'created_by' => $user_id, 'last_edit_by' => $user_id, 'doc_type' => $new_doc_type) + $document;
                        $post_fix = !$is_duplicated ? "" : " - " . $no_of_copies;
                        $copyRecord[] = array(
                            'account_folder_id' => $account_folder_id,
                            'document_id' => '',
                            'is_viewed' => $row['is_viewed'],
                            'title' => $row['title'] . $post_fix,
                            'desc' => $row['desc'],
                            'zencoder_output_id' => $row['zencoder_output_id'],
                            'video_framework_id' => $row['video_framework_id']
                        );

                        AccountFolderDocument::where("account_folder_id", $account_folder_id)->update(["no_of_copies" => $no_of_copies]);
                    }

                    $user_email_info = User::where(array(
                                'id' => $user_id
                            ))->first();

                    if ($user_email_info) {

                        $user_email_info = $user_email_info->toArray();
                        if ($is_copied_video_library) {
                            $this->create_churnzero_event('Videos+Shared+to+Video+Library', $account_id, $user_email_info['email']);
                        } else {
                            $this->create_churnzero_event('Videos+Shared+to+Huddles', $account_id, $user_email_info['email']);
                        }
                    }
                }

                $cnt = 0;
                $document_latest_id = 0;
                if (count($docs_data) > 0) {
                    foreach ($docs_data as $row) {
                        $row['post_rubric_per_video'] = '1';
                        $row['site_id'] = $this->site_id;
                        $row['parent_folder_id'] = $parent_folder_id;
                        if($row['is_screen_recording'] == '1'){
                            $this->create_churnzero_event('Screen+Recording+shared+to+Huddle', $account_id, $user_email_info['email']);
                        }
                        $new_doc_id = DB::table('documents')->insertGetId($row);

                        $document_latest_id = $new_doc_id;
                        $copied_video_id[] = $new_doc_id;
                        if ($row['published'] == 0 && $row["doc_type"] == 1) {
                            $requestXml = '<TranscodeVideoJob><document_id>' . $new_doc_id . '</document_id><source_file>' . $row['url'] . '</source_file></TranscodeVideoJob>';
                            $video_data = array(
                                'JobId' => '2',
                                'site_id' => $this->site_id,
                                'CreateDate' => date("Y-m-d H:i:s"),
                                'RequestXml' => $requestXml,
                                'JobQueueStatusId' => 1,
                                'CurrentRetry' => 0,
                                'JobSource' => Sites::get_base_url($this->site_id)
                            );
                            DB::table('JobQueue')->insert($video_data);
                        } else {
                            $documentFiles = DocumentFiles::get_document_row($old_video_id[$cnt], $this->site_id);
                            if (!empty($documentFiles)) {
                                $this->create_churnzero_event('Video+Hours+Uploaded', $account_id, $user_email_info['email'], $documentFiles['duration']);
                                $video_data = array('site_id' => $this->site_id, 'id' => null, 'document_id' => $document_latest_id) + $documentFiles;

                                DB::table('document_files')->insert($video_data);
                            }
                        }
                        $cnt++;
                    }
                }
                $result_ids = array();

                $count = 0;

                foreach ($copyRecord as $data) {
                    $increment_shared_value = true;
                    if ($document['subtitle_available'] == "1") {
                        // Duplicate the transcribe files on s3
                        $videoFilePath = pathinfo($document['url']);
                        $videoFileName = $videoFilePath['dirname'];

                        $this->copyTranscribedFile($videoFileName, $copied_video_id[$count], $document_id);
                    }

                    $data['document_id'] = $copied_video_id[$count];
                    if (($is_duplicated && isset($input['shareAssets']) && $input['shareAssets']) || (isset($input['copy_notes']) && ($input['copy_notes'] == "true" || $input['copy_notes'] == 1))) {
                        $this->copy_video_attachments($document_id, $data['document_id'], $is_duplicated, $workspace, $data['account_folder_id']);
                    }else{
                         $data['video_framework_id']=null;
                    }
                    $data['site_id'] = $this->site_id;
                    $data['assessment_sample'] = $is_sample;
                    if(AccountFolderMetaData::check_if_eval_huddle($data['account_folder_id'],$this->site_id) > 0)
                    {
                        $increment_shared_value = false;
                    }
                    if (AccountFolderMetaData::check_if_eval_huddle($data['account_folder_id'],$this->site_id) > 0 && (empty($is_sample) || $is_sample ==NULL)) {
                        // The slot should only be set if it's from Assessee
                        $slot_index = 0;
                        if ($request->has("slot_index")) {
                            $slot_index = $request->get("slot_index");
                        } else {
                            $slot_doc_type = isset($new_doc_type) ? $new_doc_type : '1';
                            $slot_index = $this->getNextAvailableSlot($user_id, $data['account_folder_id'], $slot_doc_type);
                        }
                        $data['slot_index'] = $slot_index;
                    } else {
                        $data['slot_index'] = '0';
                    }
                    $afd_id = DB::table('account_folder_documents')->insertGetId($data);
                    $result_ids[] = $afd_id;
                    //var_dump($result_ids);
                    //var_dump($input);

                    if (($is_duplicated && isset($input['shareAssets']) && $input['shareAssets']) || (isset($input['copy_notes']) && ($input['copy_notes'] == "true" || $input['copy_notes'] == 1))) {
                        @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 2, $is_duplicated, $workspace, $data['account_folder_id']);
                        //   @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 3);
                        @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 6, $is_duplicated, $workspace, $data['account_folder_id']);

                        @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 4, $is_duplicated, $workspace, $data['account_folder_id']);
                    }
                    $count++;


                    $activity_desc = ucwords($current_resource) . ' Shared';
                    if ($is_duplicated) {
                        $activity_desc = ucwords($current_resource) . ' Duplicated';
                    }

                    $huddle_or_workspsace = AccountFolder::where(array(
                                "account_folder_id" => $data['account_folder_id'],
                                'site_id' => $this->site_id
                            ))->first();

                    if ($huddle_or_workspsace) {
                        $huddle_or_workspsace = $huddle_or_workspsace->toArray();
                        $location_of_video = $huddle_or_workspsace['folder_type'];
                    } else {
                        $location_of_video = '1';
                    }
                    if ($location_of_video == '1') {
                        $activity_url = '/Huddles/view/' . $data['account_folder_id'] . "/1/" . $data['document_id'];
                    } else if ($location_of_video == '3') {
                        $activity_url = '/home/workspace_video/home/' . $data['account_folder_id'] . '/' . $data['document_id'];
                    } else if ($location_of_video == '2') {
                        $activity_url = '/VideoLibrary/view/' . $data['account_folder_id'] . '/';
                    } else {
                        $activity_url = '/Huddles/view/' . $data['account_folder_id'] . "/1/" . $data['document_id'];
                    }

                    if ($current_resource == 'scripted note') {
                        $query_string = "";
                        if ($is_duplicated && $workspace) {
                            $query_string = "?workspace=true";
                        }
                        $activity_url = '/video_details/scripted_observations/' . $data['account_folder_id'] . "/" . $data['document_id'] . $query_string;
                    }
                 if($increment_shared_value)
                 {
                    if ($current_resource == 'scripted note' && !$is_duplicated) {

                        $user_activity_logs = array(
                        'ref_id' => $data['document_id'],
                        'desc' => $activity_desc,
                        'url' => $activity_url,
                        'type' => '26',
                        'account_id' => $account_id,
                        'account_folder_id' => $data['account_folder_id'],
                        'environment_type' => 2,
                        'site_id' => $this->site_id,
                        'source_ref_id'  => $document_id
                    );

                    }
                    elseif($current_resource == 'url')
                    {

                        $user_activity_logs = array(
                        'ref_id' => $data['document_id'],
                        'desc' => $activity_desc,
                        'url' => $activity_url,
                        'type' => '30',
                        'account_id' => $account_id,
                        'account_folder_id' => $data['account_folder_id'],
                        'environment_type' => 2,
                        'site_id' => $this->site_id,
                        'source_ref_id'  => $document_id
                    );

                    }
                    else {

                        $user_activity_logs = array(
                            'ref_id' => $data['document_id'],
                            'desc' => $activity_desc,
                            'url' => $activity_url,
                            'type' => '22',
                            'account_id' => $account_id,
                            'account_folder_id' => $data['account_folder_id'],
                            'environment_type' => 2,
                            'site_id' => $this->site_id,
                            'source_ref_id' => $document_id
                        );
                    }

                    $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
                    if ($from_workspace && !$workspace && !$is_duplicated) {
                        $user_activity_logs2 = array(
                            'ref_id' => $data['document_id'],
                            'desc' => $activity_desc,
                            'url' => $activity_url,
                            'type' => '27',
                            'account_id' => $account_id,
                            'account_folder_id' => $current_account_folder_id,
                            'environment_type' => 2,
                            'site_id' => $this->site_id,
                            'source_ref_id' => $document_id
                        );
                        $this->user_activity_logs($user_activity_logs2, $account_id, $user_id);
                    }

                 }
                else {
                    $user_activity_logs = array(
                            'ref_id' => $data['document_id'],
                            'desc' => $activity_desc,
                            'url' => $activity_url,
                            'account_folder_id' => $data['account_folder_id'],
                            'type' => '2',
                            'environment_type' => 2,
                            'account_id' => $account_id,
                            'site_id' => $this->site_id,
                        );
                    $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
                }

                if($fire_websocket) {

                    $new_video = Document::get_single_video_data($this->site_id, $data['document_id'], 1, 1, $data['account_folder_id'], $user_id);
                    if ($new_video && $new_video['folder_type'] == '1') {
                        $channel_data = array(
                            "multiple_huddle_ids" => array($data['account_folder_id']),
                            "data_array" => array(
                                'document_id' => $data['document_id'],
                                'data' => $new_video,
                                'channel' => 'huddle-details',
                                'event' => "resource_added"
                            )
                        );
                        if (HelperFunctions::is_push_notification_allowed($account_id)) {
                            HelperFunctions::broadcastEvent($channel_data, "broadcast_multiple_event");
                        } else {
                            HelperFunctions::broadcastEvent($channel_data, "broadcast_multiple_event", false);
                        }
                        $channel_data2 = array(
                            'account_folder_id' => $data['account_folder_id'],
                            'document_id' => $data['document_id'],
                            'data' => $new_video,
                            'channel' => 'huddle-' . $account_id,
                            'event' => "resource_added"
                        );
                        HelperFunctions::broadcastEvent($channel_data2, false);

                    }

                    if ($workspace) {
                        $new_video = Document::get_single_video_data($this->site_id, $data['document_id'], 3, 1, $data['account_folder_id'], $user_id);
                        $channel_data = array(
                            'data' => $new_video,
                            'document_id' => $data['document_id'],
                            'channel' => 'workspace-' . $account_id . "-" . $user_id,
                            'event' => "resource_added"
                        );
                        if (HelperFunctions::is_push_notification_allowed($account_id)) {
                            HelperFunctions::broadcastEvent($channel_data);
                        } else {
                            HelperFunctions::broadcastEvent($channel_data, 'broadcast_event', false);
                        }
                    }
                }
             }


                if (count($result_ids) > 0 || $is_copied_video_library == TRUE) {

                    if ($is_copied_video_library == true && count($result_ids) > 0) {
                        if(HelperFunctions::is_email_notification_allowed($account_id, 'enable_emails_for_upload_video'))
                        {
                            if(EmailUnsubscribers::check_subscription($user_id, '8', $account_id, !empty($this->site_id) ? $this->site_id : $site_id )){
                                $this->EmailCopyVideo($all_account_folder_ids, $data['document_id'], $doc_type, $account_id, $user_id, $user_results['first_name'], $user_results['last_name'], $account_results['company_name'], $user_results['image'], $is_scripted_note);
                            }
                        }
                        $result['message'] = str_replace("video", $current_resource, TranslationsManager::get_translation('you_have_successfully_copied_video_to_library_huddle_msg', 'flash_messages', $this->site_id));
                    } elseif ($is_copied_video_library == true && count($result_ids) <= 0) {
                        $result['message'] = str_replace("video", $current_resource, TranslationsManager::get_translation('you_have_successfully_copied_video_to_library_msg', 'flash_messages', $this->site_id));
                    } else {
                        // The Copy email should only be sent to Collab and Coaching huddles.
                        if ($destination_huddle_type != '3' && $fire_websocket) {
                            if(HelperFunctions::is_email_notification_allowed($account_id, 'enable_emails_for_upload_video'))
                            {
                                if(EmailUnsubscribers::check_subscription($user_id, '8', $account_id, !empty($this->site_id) ? $this->site_id : $site_id )){
                                    $this->EmailCopyVideo($all_account_folder_ids, $data['document_id'], $doc_type, $account_id, $user_id, $user_results['first_name'], $user_results['last_name'], $account_results['company_name'], $user_results['image'], $is_scripted_note);
                            }
                          }
                        }
                        if (!$workspace) {
                            if ($is_duplicated) {
                                $result['message'] = "$current_resource " . TranslationsManager::get_translation('discussion_copy_duplication_success', 'Api/huddle_list', $this->site_id, $user_id);
                            } else {
                                $result['message'] = str_replace("video", $current_resource, TranslationsManager::get_translation('you_have_successfully_copied_video_into_huddle_msg', 'flash_messages', $this->site_id));
                            }
                        } else {
                            if ($is_duplicated) {
                                $result['message'] = "$current_resource " . TranslationsManager::get_translation('discussion_copy_duplication_success', 'Api/huddle_list', $this->site_id, $user_id);
                            } else {
                                $result['message'] = TranslationsManager::get_translation('discussion_copy_resource_success_msg_part_1', 'Api/huddle_list', $this->site_id, $user_id) . " $current_resource " . TranslationsManager::get_translation('discussion_copy_resource_success_msg_part_2', 'Api/huddle_list', $this->site_id, $user_id);
                            }
                        }
                    }

                    $result['success'] = TRUE;
                    $folder_type = 2;

                    ///////
                    AccountFolder::where('account_folder_id', $account_folder_id)->update(['last_edit_date'=>date('Y-m-d H:i:s'), 'last_edit_by'=> $user_id]);
                    ///////

                    $new_video = Document::get_single_video_data($this->site_id, $document_latest_id, $folder_type, 1, $account_folder_id, $user_id);
                    $result["data"] = $new_video;
                    if(count($all_parent_folder_ids) > 0)
                    {
                        DocumentFolder::whereIn('id', $all_parent_folder_ids)->update(['last_edit_date'=>date('Y-m-d H:i:s'), 'last_edit_by'=> $user_id]);
                    }
                    if(empty($result["data"]) && isset($input['environment_type']) && $input['environment_type'] == '3')
                    {
                        $result["data"] = (object)[];
                    }
                } else {
                    $result['message'] = str_replace("video", $current_resource, TranslationsManager::get_translation('Video_is_not_Copied_into_selected_huddle', 'flash_messages', $this->site_id));
                }
            } else {
                $result['message'] = str_replace("video", $current_resource, TranslationsManager::get_translation('Please_select_at_least_one_video_to_copied_into_huddle', 'flash_messages', $this->site_id));
            }
        } else {
            $result['message'] = str_replace("video", $current_resource, TranslationsManager::get_translation('Please_select_at_least_one_video_to_copied_into_huddle', 'flash_messages', $this->site_id));
        }
        return $result;
        exit;
    }

    function copy(Request $request, $fire_websocket = 1) {
        $input = $request->all();


//        $input['document_id'] = '216336';
//        $input['account_folder_id'] = array('56177','51897','-1');
//        $input['current_huddle_id'] = '54397';
//        $input['account_id'] = '181';
//        $input['user_id'] = '621';
//        $input['copy_notes'] = 1;

        $document_ids = $input['document_id'];

        $account_folder_id = isset($input['account_folder_id']) ? $input['account_folder_id'] : '';
        //$current_account_folder_id = isset($input['current_huddle_id']) ? $input['current_huddle_id'] : '';
        $workspace = $request->has("workspace") ? $request->get("workspace") : 0;
        $parent_folder_id = $request->has("parent_folder_id") ? $request->get("parent_folder_id") : 0;
        $from_workspace = $request->has("from_workspace") ? $request->get("from_workspace") : 0;
        $is_scripted_note = $request->has("is_scripted_note") ? $request->get("is_scripted_note") : 0;
        $is_duplicated = $request->has("is_duplicated") ? $request->get("is_duplicated") : 0;
        $doc_type = $request->has("doc_type") ? $request->get("doc_type") : 1;
        $lang = app("Illuminate\Http\Request")->header('current-lang');
        $site_id = $request->header('site_id');



        $h_type = AccountFolderMetaData::where('account_folder_id', $account_folder_id)->where('meta_data_name', 'folder_type')->first();
        $htype = isset($h_type->meta_data_value) ? $h_type->meta_data_value : "1";
            if ($htype == 1) {
                $huddle_type = 'Collaboration Huddle';
            } elseif ($htype == 2) {
                $huddle_type = 'Coaching Huddle';
            } else {
                $huddle_type = 'Assessment Huddle';
            }
        if ($huddle_type == 'Assessment Huddle' && !HelperFunctions::check_if_evalutor($account_folder_id, $input['user_id'])) {

            $user_account_detail = UserAccount::where('account_id', $input['account_id'])->where('user_id', $input['user_id'])->first();
            $user_detail = User::where('id', $input['user_id'])->first();
            $user_role = HelperFunctions::get_user_role_name($user_account_detail['role_id']);
            $in_trial_intercom = HelperFunctions::check_if_account_in_trial_intercom($input['account_id']);

            $meta_data = array(
                'huddle_video' => 'true',
                'huddle_type' => $huddle_type,
                'user_role' => $user_role,
                'is_in_trial' => $in_trial_intercom,
                'Platform' => 'Web'
            );



            //todo : commented by saad and hamid due to duplication.
          //  HelperFunctions::create_intercom_event('huddle-video-submitted-for-assessment', $meta_data, $user_detail['email']);
        }


        $is_mobile_request = isset($input['app_name']) ? HelperFunctions::is_mobile_request($input['app_name']) : false;

        if($is_mobile_request)
        {
            if(isset($input['environment_type']) && $input['environment_type'] == '1' )
            {
              $platform = 'iOS';
            }
            elseif(isset($input['environment_type']) && $input['environment_type'] == '3')
            {
               $platform = 'Android';
            }
            else {
                $platform = 'Mobile';
            }

        }
        else
        {
           $platform = 'Web';
        }

        $h_type = AccountFolderMetaData::where('account_folder_id', $account_folder_id)->where('meta_data_name', 'folder_type')->first();
        $htype = isset($h_type->meta_data_value) ? $h_type->meta_data_value : "1";
            if ($htype == 1) {
                $huddle_type = 'Collaboration Huddle';
            } elseif ($htype == 2) {
                $huddle_type = 'Coaching Huddle';
            } else {
                $huddle_type = 'Assessment Huddle';
            }
        if ($huddle_type == 'Assessment Huddle' && !HelperFunctions::check_if_evalutor($account_folder_id, $input['user_id'])) {
            $user_account_detail = UserAccount::where('account_id', $input['account_id'])->where('user_id', $input['user_id'])->first();
            $user_detail = User::where('id', $input['user_id'])->first();
            $user_role = HelperFunctions::get_user_role_name($user_account_detail['role_id']);
            $in_trial_intercom = HelperFunctions::check_if_account_in_trial_intercom($input['account_id']);
            $meta_data = array(
                'huddle_video' => 'true',
                'huddle_type' => $huddle_type,
                'user_role' => $user_role,
                'is_in_trial' => $in_trial_intercom,
                'Platform' => $platform
            );
            HelperFunctions::create_intercom_event('huddle-video-submitted-for-assessment', $meta_data, $user_detail['email']);
        }

        if(!$is_mobile_request)
        {

            if($is_duplicated || $input['copy_notes'] == 0  )
            {
                $input['copy_notes'] = false;
            }
        }

        if ($is_mobile_request) {
            if (isset($input['is_duplicated']) && $input['is_duplicated'] && $input['is_duplicated'] != '0') {
                $input['copy_notes'] = false;
            }
        }


        if ($is_duplicated) {
            $source_type = 3;
        } else {
            $source_type = 4;
        }


        if ($doc_type == 1) {
            $current_resource = "video";
        } else if ($doc_type == 2) {
            $current_resource = "resource";
        } else if ($doc_type == 3) {
            if ($lang == 'es') {
                $current_resource = TranslationsManager::get_translation('scripted_note_msg', 'flash_messages', $this->site_id);
            } else {
                $current_resource = TranslationsManager::get_translation('scripted_note_msg', 'flash_messages', $this->site_id);
            }
            $current_resource = 'scripted note';
            if(!$is_scripted_note)
            {
               $current_resource = "video";
            }
        }
        else if($doc_type == 5)
        {
            $current_resource = "url";
        }
        else
        {
            $current_resource = "artifact";
        }
        $result = array(
            'success' => FALSE,
            'message' => ''
        );

        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $destination_huddle_type = null;

        if (isset($input['sample']) && !empty($input['sample'])) {
            $is_sample = (string) $input['sample'];
        } else {
            $is_sample = '0';
        }

        $user_results = User::where(array(
                    'id' => $user_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($user_results) {
            $user_results = $user_results->toArray();
        }

        $account_results = Account::where(array(
                    'id' => $account_id,
                    'site_id' => $this->site_id
                ))->first();

        if ($account_results) {
            $account_results = $account_results->toArray();
        }

        if (!empty($document_ids)) {
            $document_ids = explode(',', $document_ids);
            $document_ids = array_unique($document_ids);//if frontend send duplicate ids dont add multiple times
            foreach ($document_ids as $document_id)
            {

                $conditions = array('site_id' => $this->site_id, 'document_id' => $document_id);

                $myfilesRow = AccountFolderDocument::get_row($conditions);

                if (!empty($account_folder_id) && count($account_folder_id) > 0 && $myfilesRow != '') {
                    $myfilesRow = [$myfilesRow[0]];//There are some currpt records in account_folder_documents so only accessing first one assuming it is correct
                    $copyRecord = array();
                    $docs_data = array();
                    $old_video_id = array();
                    $is_copied_video_library = false;
                    $copied_video_id = array();
                    for ($i = 0; $i < count($account_folder_id); $i++) {
                        foreach ($myfilesRow as $row) {
                            if ((int) $account_folder_id[$i] == -1) {
                                $user_account_data = UserAccount::where("account_id", $account_id)->where("user_id", $user_id)->first();
                                if ($user_account_data->permission_access_video_library != 1 || $user_account_data->permission_video_library_upload != 1) {
                                    $result["message"] = "You don't have permissions to share $current_resource to library";
                                    return $result;
                                }
                                $this->copyToVideoLibrary($myfilesRow, $account_id, $user_id, $request->get("categories"));
                                $is_copied_video_library = true;
                                continue;
                            }
                            if (AccountFolderMetaData::check_if_eval_huddle($account_folder_id[$i], $this->site_id) == 1) {
                                $destination_huddle_type = '3';
                                if ($this->check_if_evaluated_participant($account_folder_id[$i], $user_id) == 1) {
                                    $get_eval_participant_videos = Document::get_eval_participant_videos($account_folder_id[$i], $user_id, $this->site_id, $doc_type);
                                    $submission_allowed_count = Document::get_submission_allowed($account_folder_id[$i], $this->site_id, $doc_type);
                                    if ($get_eval_participant_videos >= $submission_allowed_count) {
                                        $result['message'] = str_replace("video", $current_resource, TranslationsManager::get_translation('you_have_already_submitted_video_msg', 'flash_messages', $this->site_id));
                                        return $result;
                                    }

                                    $submission_date = $this->get_submission_date($account_folder_id[$i]);
                                    if ($submission_date != '') {
                                        $submission_string = strtotime($submission_date);
                                        $current_time = strtotime(date('Y-m-d H:i:s a', time()));
                                        if ($submission_string < $current_time) {
                                            return array(
                                                'success' => false,
                                                'message' => TranslationsManager::get_translation('discussion_copy_resource_warn_msg_part_1', 'Api/huddle_list', $this->site_id, $user_id) . " $current_resource " . TranslationsManager::get_translation('discussion_copy_resource_warn_msg_part_2', 'Api/huddle_list', $this->site_id, $user_id)
                                            );
                                        }
                                    }
                                } else {
                                    $is_sample = 1;
                                }
                            }
                            $document = Document::get_document_row($row['document_id'], $this->site_id);
                            if (empty($document))
                                continue;
                            /* $no_of_copies = $row['no_of_copies'] + 1;
                              if ($from_workspace) { */

                            $temp_count = Document::join("account_folder_documents", "documents.id", "=", "account_folder_documents.document_id")->where("documents.account_id", $account_id)->where("documents.created_by", $user_id)->where(function ($query) use ($row) {
                                $query->where("account_folder_documents.title", 'like', "'%" . $this->mysql_escape($row['title']) . "%'");
                                //->orWhere("account_folder_documents.title", "REGEXP", "'".$row['title'] . " - [2-50]$)"."'");
                            });
                            if (!$from_workspace) {
                                $temp_count->where("account_folder_documents.account_folder_id", $account_folder_id[$i]);
                            }
                            $temp_count = $temp_count->count();
                            if ($temp_count) {
                                $no_of_copies = $temp_count + 1;
                            } else {
                                $no_of_copies = 2;
                            }
                            // }
                            $old_video_id[] = $document['id'];
                            $new_doc_type = $document['doc_type'];
                            //if ($from_workspace) {
                            if ($document['doc_type'] == '1' || (($document['doc_type'] == '3' && ($document['is_processed'] == 4 || $document['is_processed'] == 5)) && !$is_scripted_note)) {
                                $new_doc_type = 1;
                            }
                            //}
                            if($document['doc_type'] == '2' && $is_duplicated)
                            {
                              $copy_num_detail =  AccountFolderDocument::where(array('document_id' => $row['document_id'] ))->first();
                              $no_of_copies = $copy_num_detail->no_of_copies + 1;
                              
                            }
                            $post_fix = !$is_duplicated ? "" : " - " . $no_of_copies;
                            $docs_data[] = array('source_type' => $source_type, 'source_document_id' => $document_id, 'id' => null, 'created_date' => date("Y-m-d H:i:s"), 'last_edit_date' => date("Y-m-d H:i:s"), 'recorded_date' => date("Y-m-d H:i:s"), 'created_by' => $user_id, 'last_edit_by' => $user_id, 'doc_type' => $new_doc_type) + $document;
                            $path_info = pathinfo($row['title']);
                            $copyRecord[] = array(
                                'account_folder_id' => $account_folder_id[$i],
                                'document_id' => '',
                                'is_viewed' => $row['is_viewed'],
                                'title' => $path_info['filename'] . $post_fix,
                                'desc' => $row['desc'],
                                'zencoder_output_id' => $row['zencoder_output_id'],
                                'current_account_folder_id' => $row['account_folder_id'],
                                'video_framework_id' => $row['video_framework_id']
                            );
                            if($document['doc_type'] == '2' && $is_duplicated)
                            {
                                
                                AccountFolderDocument::where("document_id", $row['document_id'])->update(["no_of_copies" => $no_of_copies]);
                            }
                            else
                            {
                                AccountFolderDocument::where("account_folder_id", $account_folder_id)->update(["no_of_copies" => $no_of_copies]);
                            }
                            
                        }

                        $user_email_info = User::where(array(
                            'id' => $user_id
                        ))->first();

                        if ($user_email_info) {

                            $user_email_info = $user_email_info->toArray();
                            if ($is_copied_video_library) {
                                $this->create_churnzero_event('Videos+Shared+to+Video+Library', $account_id, $user_email_info['email']);
                            } else {
                                $this->create_churnzero_event('Videos+Shared+to+Huddles', $account_id, $user_email_info['email']);
                            }
                        }
                    }

                    $cnt = 0;
                    $document_latest_id = 0;
                    if (count($docs_data) > 0) {
                        foreach ($docs_data as $row) {
                            $row['post_rubric_per_video'] = '1';
                            $row['site_id'] = $this->site_id;
                            $row['parent_folder_id'] = $parent_folder_id;
                            if($row['is_screen_recording'] == '1'){
                                $this->create_churnzero_event('Screen+Recording+shared+to+Huddle', $account_id, $user_email_info['email']);
                            }
                            $new_doc_id = DB::table('documents')->insertGetId($row);

                            $document_latest_id = $new_doc_id;
                            $copied_video_id[] = $new_doc_id;
                            if ($row['published'] == 0 && $row["doc_type"] == 1) {
                                $requestXml = '<TranscodeVideoJob><document_id>' . $new_doc_id . '</document_id><source_file>' . $row['url'] . '</source_file></TranscodeVideoJob>';
                                $video_data = array(
                                    'JobId' => '2',
                                    'site_id' => $this->site_id,
                                    'CreateDate' => date("Y-m-d H:i:s"),
                                    'RequestXml' => $requestXml,
                                    'JobQueueStatusId' => 1,
                                    'CurrentRetry' => 0,
                                    'JobSource' => Sites::get_base_url($this->site_id)
                                );
                                DB::table('JobQueue')->insert($video_data);
                            } else {
                                $documentFiles = DocumentFiles::get_document_row($old_video_id[$cnt], $this->site_id);
                                if (!empty($documentFiles)) {
                                    $this->create_churnzero_event('Video+Hours+Uploaded', $account_id, $user_email_info['email'], $documentFiles['duration']);
                                    $video_data = array('site_id' => $this->site_id, 'id' => null, 'document_id' => $document_latest_id, 'created_at'=>date('Y-m-d H:i:s'), "debug_logs" => "Lumen::VideoController::copy::line=7749::document_id=".$document_latest_id."::status=".$documentFiles['transcoding_status']."::created"
                                        ) + $documentFiles;

                                    DB::table('document_files')->insert($video_data);
                                }
                            }
                            $cnt++;
                        }
                    }
                    $result_ids = array();

                    $count = 0;

                    foreach ($copyRecord as $data) {
                        $current_account_folder_id = $data['current_account_folder_id'];
                        unset($data['current_account_folder_id']);
                        $increment_shared_value = true;
                        if ($document['subtitle_available'] == "1") {
                            // Duplicate the transcribe files on s3
                            $videoFilePath = pathinfo($document['url']);
                            $videoFileName = $videoFilePath['dirname'];

                            $this->copyTranscribedFile($videoFileName, $copied_video_id[$count], $document_id);
                        }

                        $data['document_id'] = $copied_video_id[$count];
                        if (($is_duplicated && isset($input['shareAssets']) && $input['shareAssets']) || (isset($input['copy_notes']) && ($input['copy_notes'] == "true" || $input['copy_notes'] == 1))) {
                            $this->copy_video_attachments($document_id, $data['document_id'], $is_duplicated, $workspace, $data['account_folder_id']);
                        }else{
                            $data['video_framework_id']=null;
                        }
                        $data['site_id'] = $this->site_id;
                        $data['assessment_sample'] = $is_sample;
                        if(AccountFolderMetaData::check_if_eval_huddle($data['account_folder_id'],$this->site_id) > 0)
                        {
                            $increment_shared_value = false;
                        }
                        if (AccountFolderMetaData::check_if_eval_huddle($data['account_folder_id'],$this->site_id) > 0 && (empty($is_sample) || $is_sample ==NULL)) {
                            // The slot should only be set if it's from Assessee
                            $slot_index = 0;
                            if ($request->has("slot_index")) {
                                $slot_index = $request->get("slot_index");
                            } else {
                                $slot_doc_type = isset($new_doc_type) ? $new_doc_type : '1';
                                $slot_index = $this->getNextAvailableSlot($user_id, $data['account_folder_id'], $slot_doc_type);
                            }
                            $data['slot_index'] = $slot_index;
                        } else {
                            $data['slot_index'] = '0';
                        }
                        $afd_id = DB::table('account_folder_documents')->insertGetId($data);
                        $result_ids[] = $afd_id;
                        //var_dump($result_ids);
                        //var_dump($input);

                        if (($is_duplicated && isset($input['shareAssets']) && $input['shareAssets']) || (isset($input['copy_notes']) && ($input['copy_notes'] == "true" || $input['copy_notes'] == 1))) {
                            @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 2, $is_duplicated, $workspace, $data['account_folder_id']);
                            //   @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 3);
                            @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 6, $is_duplicated, $workspace, $data['account_folder_id']);

                            @$this->copyComments($old_video_id[$count], $copied_video_id[$count], $user_id, 4, $is_duplicated, $workspace, $data['account_folder_id']);
                        }
                        $count++;


                        $activity_desc = ucwords($current_resource) . ' Shared';
                        if ($is_duplicated) {
                            $activity_desc = ucwords($current_resource) . ' Duplicated';
                        }

                        $huddle_or_workspsace = AccountFolder::where(array(
                            "account_folder_id" => $data['account_folder_id'],
                            'site_id' => $this->site_id
                        ))->first();

                        if ($huddle_or_workspsace) {
                            $huddle_or_workspsace = $huddle_or_workspsace->toArray();
                            $location_of_video = $huddle_or_workspsace['folder_type'];
                        } else {
                            $location_of_video = '1';
                        }
                        if ($location_of_video == '1') {
                            $activity_url = '/Huddles/view/' . $data['account_folder_id'] . "/1/" . $data['document_id'];
                        } else if ($location_of_video == '3') {
                            $activity_url = '/home/workspace_video/home/' . $data['account_folder_id'] . '/' . $data['document_id'];
                        } else if ($location_of_video == '2') {
                            $activity_url = '/VideoLibrary/view/' . $data['account_folder_id'] . '/';
                        } else {
                            $activity_url = '/Huddles/view/' . $data['account_folder_id'] . "/1/" . $data['document_id'];
                        }

                        if ($current_resource == 'scripted note') {
                            $query_string = "";
                            if ($is_duplicated && $workspace) {
                                $query_string = "?workspace=true";
                            }
                            $activity_url = '/video_details/scripted_observations/' . $data['account_folder_id'] . "/" . $data['document_id'] . $query_string;
                        }
                        if($increment_shared_value)
                        {
                            if ($current_resource == 'scripted note' && !$is_duplicated) {

                                $user_activity_logs = array(
                                    'ref_id' => $data['document_id'],
                                    'desc' => $activity_desc,
                                    'url' => $activity_url,
                                    'type' => '26',
                                    'account_id' => $account_id,
                                    'account_folder_id' => $data['account_folder_id'],
                                    'environment_type' => 2,
                                    'site_id' => $this->site_id,
                                    'source_ref_id'  => $document_id
                                );

                            }
                            elseif($current_resource == 'url')
                            {

                                $user_activity_logs = array(
                                    'ref_id' => $data['document_id'],
                                    'desc' => $activity_desc,
                                    'url' => $activity_url,
                                    'type' => '30',
                                    'account_id' => $account_id,
                                    'account_folder_id' => $data['account_folder_id'],
                                    'environment_type' => 2,
                                    'site_id' => $this->site_id,
                                    'source_ref_id'  => $document_id
                                );

                            }
                            else {

                                $user_activity_logs = array(
                                    'ref_id' => $data['document_id'],
                                    'desc' => $activity_desc,
                                    'url' => $activity_url,
                                    'type' => '22',
                                    'account_id' => $account_id,
                                    'account_folder_id' => $data['account_folder_id'],
                                    'environment_type' => 2,
                                    'site_id' => $this->site_id,
                                    'source_ref_id' => $document_id
                                );
                            }

                            $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
                            if ($from_workspace && !$workspace && !$is_duplicated) {
                                $user_activity_logs2 = array(
                                    'ref_id' => $data['document_id'],
                                    'desc' => $activity_desc,
                                    'url' => $activity_url,
                                    'type' => '27',
                                    'account_id' => $account_id,
                                    'account_folder_id' => $current_account_folder_id,
                                    'environment_type' => 2,
                                    'site_id' => $this->site_id,
                                    'source_ref_id' => $document_id
                                );
                                $this->user_activity_logs($user_activity_logs2, $account_id, $user_id);
                            }

                        }
                        else {
                            $user_activity_logs = array(
                                'ref_id' => $data['document_id'],
                                'desc' => $activity_desc,
                                'url' => $activity_url,
                                'account_folder_id' => $data['account_folder_id'],
                                'type' => '2',
                                'environment_type' => 2,
                                'account_id' => $account_id,
                                'site_id' => $this->site_id,
                            );
                            $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
                        }

                        if($fire_websocket) {

                    $new_video = Document::get_single_video_data($this->site_id, $data['document_id'], 1, 1, $data['account_folder_id'], $user_id);
                    if ($new_video && $new_video['folder_type'] == '1') {
                        $channel_data = array(
                            "multiple_huddle_ids" => array($data['account_folder_id']),
                            "data_array" => array(
                                'document_id' => $data['document_id'],
                                'data' => $new_video,
                                'channel' => 'huddle-details',
                                'event' => "resource_added"
                            )
                        );
                        if (HelperFunctions::is_push_notification_allowed($account_id)) {
                            HelperFunctions::broadcastEvent($channel_data, "broadcast_multiple_event");
                        } else {
                            HelperFunctions::broadcastEvent($channel_data, "broadcast_multiple_event", false);
                        }
                        $channel_data2 = array(
                            'account_folder_id' => $data['account_folder_id'],
                            'document_id' => $data['document_id'],
                            'data' => $new_video,
                            'channel' => 'huddle-' . $account_id,
                            'event' => "resource_added"
                        );
                        HelperFunctions::broadcastEvent($channel_data2, "broadcast_event", false);
                    }

                            if ($workspace) {
                                $new_video = Document::get_single_video_data($this->site_id, $data['document_id'], 3, 1, $data['account_folder_id'], $user_id);
                                $channel_data = array(
                                    'data' => $new_video,
                                    'document_id' => $data['document_id'],
                                    'channel' => 'workspace-' . $account_id . "-" . $user_id,
                                    'event' => "resource_added"
                                );
                                if (HelperFunctions::is_push_notification_allowed($account_id)) {
                                    HelperFunctions::broadcastEvent($channel_data);
                                } else {
                                    HelperFunctions::broadcastEvent($channel_data, 'broadcast_event', false);
                                }
                            }
                        }
                    }


                if (count($result_ids) > 0 || $is_copied_video_library == TRUE) {
                    if ($is_copied_video_library == true && count($result_ids) > 0) {
                        if(HelperFunctions::is_email_notification_allowed($account_id, 'enable_emails_for_upload_video'))
                        {
                            if(EmailUnsubscribers::check_subscription($user_id, '8', $account_id, !empty($this->site_id) ? $this->site_id : $site_id )){
                                QueuesManager::sendToEmailQueue(__CLASS__, "EmailCopyVideo", $this->site_id, $account_folder_id, $data['document_id'], $doc_type, $account_id, $user_id, $user_results['first_name'], $user_results['last_name'], $account_results['company_name'], $user_results['image'], $is_scripted_note);
                                // $this->EmailCopyVideo($this->site_id, $account_folder_id, $data['document_id'], $doc_type, $account_id, $user_id, $user_results['first_name'], $user_results['last_name'], $account_results['company_name'], $user_results['image'], $is_scripted_note);
                            }
                        }
                        $result['message'] = str_replace("video", $current_resource, TranslationsManager::get_translation('you_have_successfully_copied_video_to_library_huddle_msg', 'flash_messages', $this->site_id));
                    } elseif ($is_copied_video_library == true && count($result_ids) <= 0) {
                        $result['message'] = str_replace("video", $current_resource, TranslationsManager::get_translation('you_have_successfully_copied_video_to_library_msg', 'flash_messages', $this->site_id));
                    } else {


                        // The Copy email should only be sent to Collab and Coaching huddles.
                        if ($destination_huddle_type != '3' && $fire_websocket) {
                            if(HelperFunctions::is_email_notification_allowed($account_id, 'enable_emails_for_upload_video'))
                            {
                                if(EmailUnsubscribers::check_subscription($user_id, '8', $account_id, !empty($this->site_id) ? $this->site_id : $site_id )){
                                    QueuesManager::sendToEmailQueue(__CLASS__, "EmailCopyVideo", $this->site_id, $account_folder_id, $data['document_id'], $doc_type, $account_id, $user_id, $user_results['first_name'], $user_results['last_name'], $account_results['company_name'], $user_results['image'], $is_scripted_note);
                                    // $this->EmailCopyVideo($this->site_id, $account_folder_id, $data['document_id'], $doc_type, $account_id, $user_id, $user_results['first_name'], $user_results['last_name'], $account_results['company_name'], $user_results['image'], $is_scripted_note);
                                }
                            }
                        }
                        if (!$workspace) {
                            if ($is_duplicated) {
                                $result['message'] = "$current_resource " . TranslationsManager::get_translation('discussion_copy_duplication_success', 'Api/huddle_list', $this->site_id, $user_id);
                            } else {
                                $result['message'] = str_replace("video", $current_resource, TranslationsManager::get_translation('you_have_successfully_copied_video_into_huddle_msg', 'flash_messages', $this->site_id));
                            }
                        } else {
                            if ($is_duplicated) {
                                $result['message'] = "$current_resource " . TranslationsManager::get_translation('discussion_copy_duplication_success', 'Api/huddle_list', $this->site_id, $user_id);
                            } else {
                                $result['message'] = TranslationsManager::get_translation('discussion_copy_resource_success_msg_part_1', 'Api/huddle_list', $this->site_id, $user_id) . " $current_resource " . TranslationsManager::get_translation('discussion_copy_resource_success_msg_part_2', 'Api/huddle_list', $this->site_id, $user_id);
                            }
                        }
                    }

                        $result['success'] = TRUE;
                        $folder_type = 2;

                        ///////
                        AccountFolder::where('account_folder_id', $account_folder_id)->update(['last_edit_date'=>date('Y-m-d H:i:s'), 'last_edit_by'=> $user_id]);
                        ///////

                        $new_video = Document::get_single_video_data($this->site_id, $document_latest_id, $folder_type, 1, $account_folder_id, $user_id);
                        $result["data"] = $new_video;
                        if(!empty($parent_folder_id))
                        {
                            DocumentFolder::where('id', $parent_folder_id)->update(['last_edit_date'=>date('Y-m-d H:i:s'), 'last_edit_by'=> $user_id]);
                        }
                        if(empty($result["data"]) && isset($input['environment_type']) && $input['environment_type'] == '3')
                        {
                            $result["data"] = (object)[];
                        }
                    } else {
                        $result['message'] = str_replace("video", $current_resource, TranslationsManager::get_translation('Video_is_not_Copied_into_selected_huddle', 'flash_messages', $this->site_id));
                    }
                } else {
                    $result['message'] = str_replace("video", $current_resource, TranslationsManager::get_translation('Please_select_at_least_one_video_to_copied_into_huddle', 'flash_messages', $this->site_id));
                }
            }
        } else {
            $result['message'] = str_replace("video", $current_resource, TranslationsManager::get_translation('Please_select_at_least_one_video_to_copied_into_huddle', 'flash_messages', $this->site_id));
        }
        return $result;
        exit;
    }

    function mysql_escape($inp) {
        if (is_array($inp))
            return array_map(__METHOD__, $inp);

        if (!empty($inp) && is_string($inp)) {
            return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp);
        }

            return $inp; 
        }
    function copyToVideoLibrary($myfilesRow, $account_id, $user_id, $categories = []) {

        $myfilesRow = $myfilesRow[0];

        $document = Document::getVideoDetails($this->site_id, $myfilesRow['document_id']);



        $data_af = array(
            'account_id' => $account_id,
            'site_id' => $this->site_id,
            'folder_type' => 2,
            'name' => $myfilesRow['title'],
            'desc' => $myfilesRow['desc'],
            'active' => $document['active'],
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $user_id,
        );

        DB::table('account_folders')->insert($data_af);


        $account_folder_id = DB::getPdo()->lastInsertId();
        $account_folders_meta_data = array(
            'account_folder_id' => $account_folder_id,
            'site_id' => $this->site_id,
            'meta_data_name' => 'tag',
            'meta_data_value' => '',
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $user_id,
        );

        DB::table('account_folders_meta_data')->insert($account_folders_meta_data);

        if ($document['doc_type'] == '3') {
            $doc_type = '1';
        } else {
            $doc_type = $document['doc_type'];
        }

        $data_doc = array(
            'account_id' => $document['account_id'],
            'site_id' => $this->site_id,
            'doc_type' => $doc_type,
            'url' => $document['url'],
            'original_file_name' => $document['original_file_name'],
            'active' => $document['active'],
            'view_count' => 0,
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'file_size' => $document['file_size'] ? $document['file_size'] : '',
            'last_edit_by' => $user_id,
            'encoder_provider' => $document['encoder_provider'],
            'published' => $document['published'],
            'video_is_saved' => $document['video_is_saved'],
            'is_processed' => $document['is_processed'],
            'post_rubric_per_video' => '1',
            'source_document_id' => $myfilesRow['document_id'],
            'video_duration' => $document['video_duration'],
            'source_type' => '4',
            'subtitle_available' => $document['subtitle_available'],
            'allow_transcribe' => $document['allow_transcribe'],
        );

        DB::table('documents')->insert($data_doc);

        $video_id = DB::getPdo()->lastInsertId();

        if ($document['published'] == 0) {

            $requestXml = '<TranscodeVideoJob><document_id>' . $video_id . '</document_id><source_file>' . $document['Document']['url'] . '</source_file></TranscodeVideoJob>';
            $video_data = array(
                'JobId' => '2',
                'site_id' => $this->site_id,
                'CreateDate' => date("Y-m-d H:i:s"),
                'RequestXml' => $requestXml,
                'JobQueueStatusId' => 1,
                'CurrentRetry' => 0,
                'JobSource' => Sites::get_base_url($this->site_id)
            );

            DB::table('JobQueue')->insert($video_data);
        } else {
            $documentFiles = DocumentFiles::get_document_row($myfilesRow['document_id'], $this->site_id);
            $user_email_info = User::where(array(
                        'id' => $user_id
                    ))->first();

            if ($user_email_info) {
                $user_email_info = $user_email_info->toArray();
                $this->create_churnzero_event('Video+Hours+Uploaded', $account_id, $user_email_info['email'], $documentFiles['duration']);
            }
            $video_data = array('site_id' => $this->site_id, 'id' => null, 'document_id' => $video_id, "created_at"=>date('Y-m-d H:i:s'), "debug_logs" => "Lumen::VideoController::copyToVideoLibrary::line=8141::document_id=".$video_id."::status=".$documentFiles['transcoding_status']."::created") + $documentFiles;

            DB::table('document_files')->insert($video_data);
        }
        if ($document['subtitle_available'] == "1") {
            // Duplicate the transcribe files on s3
            $videoFilePath = pathinfo($document['url']);
            $videoFileName = $videoFilePath['dirname'];
            $this->copyTranscribedFile($videoFileName, $video_id,$document['id']);
        }

        $data_afd = array(
            'account_folder_id' => $account_folder_id,
            'site_id' => $this->site_id,
            'document_id' => $video_id,
            'title' => $myfilesRow['title'],
            'zencoder_output_id' => $myfilesRow['zencoder_output_id'],
            'desc' => $myfilesRow['desc']
        );

        DB::table('account_folder_documents')->insert($data_afd);
        $user_activity_logs = array(
            'ref_id' => $video_id,
            'site_id' => $this->site_id,
            'desc' => 'Video Duplicated',
            'url' => '/videoLibrary/view/' . $account_folder_id . '/' . $account_id,
            'type' => '22',
            'account_id' => $account_id,
            'account_folder_id' => $account_folder_id,
            'environment_type' => 2
        );
        if(!empty($categories))
        {
            $custom_request = new Request(["account_folder_id"=>$account_folder_id, "user_id"=>$user_id, "categories"=>$categories]);
            app('App\Http\Controllers\VideoLibraryController')->change_video_categories($custom_request);
        }
        $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
        $new_video = Document::get_single_video_data($this->site_id, $video_id, 2, 1, $account_folder_id, $user_id);
        $result["data"] = $new_video;

        $channel_data = array(
            'data' => $new_video,
            'document_id' => $video_id,
            'channel' => 'library-' . $account_id,
            'video_categories' => $categories,
            'sidebar_categories' => AccountFolder::getSubjects($account_id, $this->site_id),
            'uncat_videos' => AccountFolder::getVideoLibraryCount('uncat', $account_id, $this->site_id, null, false, "", ""),
            'event' => "resource_added"
        );
        HelperFunctions::broadcastEvent($channel_data);
        return true;
    }

    function copyAccountComments($destAcc, $sourceVideoId, $destinationVideoId, $sourceUserId, $refType = 2, $account_folder_id = '') {

        $comments = Comment::where(array(
                    'ref_type' => $refType,
                    'site_id' => $this->site_id,
                    'ref_id' => $sourceVideoId,
                    'active' => 1
                ))->orderBy('created_date')->get()->toArray();


        if (count($comments) == 0)
            return;
        foreach ($comments as $comment) {

            $get_tags = $this->gettagsbycommentid($comment['id'], array('1')); //get tags


            $acc = UserAccount::where(array(
                        'user_id' => $sourceUserId,
                        'site_id' => $this->site_id
                    ))->first();

            if ($acc) {
                $acc = $acc->toArray();
            }


            $comment_user_id = $comment['user_id'];
            $comment_data = array(
                'parent_comment_id' => ($refType == 3) ? $destinationVideoId : null,
                'ref_type' => $refType,
                'site_id' => $this->site_id,
                'ref_id' => $destinationVideoId,
//                'user_id' => $comment_user_id,
//                'created_by' => $comment_user_id,
//                'last_edit_by' => $comment_user_id,
                'user_id' => $sourceUserId,
                'created_by' => $sourceUserId,
                'last_edit_by' => $sourceUserId,
                    ) + $comment;
            unset($comment_data['id']);
            if (DB::table('comments')->insert($comment_data)) {
                $latest_comment_id = DB::getPdo()->lastInsertId();
                $comment_attachments = CommentAttachment::where(array('site_id' => $this->site_id, 'comment_id' => $comment['id']))->get()->toArray();
                foreach ($comment_attachments as $comment_attachment) {
                    $this->comment_attachment_transfer($comment_attachment['document_id'], $latest_comment_id, $account_folder_id, $destinationVideoId, 1, 1, $destAcc);
                }



                $replies = Comment::where(array('site_id' => $this->site_id, 'parent_comment_id' => $comment['id']))->get()->toArray();

                foreach ($replies as $replies) {
                    $repliesToreplies = Comment::where(array('site_id' => $this->site_id, 'parent_comment_id' => $replies['id']))->get()->toArray();

                    $comment_data = array(
                        'parent_comment_id' => $latest_comment_id,
                        'site_id' => $this->site_id,
                        'ref_type' => '3',
                        'ref_id' => $destinationVideoId,
//                        'user_id' => $comment_user_id,
//                        'created_by' => $comment_user_id,
//                        'last_edit_by' => $comment_user_id,
                        'user_id' => $sourceUserId,
                        'created_by' => $sourceUserId,
                        'last_edit_by' => $sourceUserId,
                            ) + $replies;
                    unset($comment_data['id']);

                    $parent_comment_id_ = DB::table('comments')->insertGetId($comment_data);
                    foreach ($repliesToreplies as $replyToreply) {
                        $comment_data = array(
                            'parent_comment_id' => $parent_comment_id_,
                            'ref_type' => '3',
                            'site_id' => $this->site_id,
                            'ref_id' => $destinationVideoId,
//                            'user_id' => $comment_user_id,
//                            'created_by' => $comment_user_id,
//                            'last_edit_by' => $comment_user_id,
                            'user_id' => $sourceUserId,
                            'created_by' => $sourceUserId,
                            'last_edit_by' => $sourceUserId,
                                ) + $replyToreply;
                        unset($comment_data['id']);

                        DB::table('comments')->insert($comment_data);
                    }
                }



                if (!empty($get_tags)) {
                    foreach ($get_tags as $tag) {
                        $commentTag = AccountTag::where(array(
                                    'account_id' => $acc['account_id'],
                                    'site_id' => $this->site_id,
                                    'tag_title' => $tag['tag_title']
                                ))->first();

                        if ($commentTag) {
                            $commentTag = $commentTag->toArray();
                        }

                        $comment_tag_data = array(
                            'comment_id' => $latest_comment_id,
                            'site_id' => $this->site_id,
                            'ref_id' => $destinationVideoId,
                            'ref_type' => 1,
                            'tag_title' => $tag['tag_title'],
                            'account_tag_id' => $commentTag['account_tag_id'],
                            'created_by' => $comment_user_id,
                                //'last_edit_by' => $comment_user_id,
                        );
                        DB::table('account_comment_tags')->insert($comment_tag_data);
                    }
                }
            }

            // $this->copyAccountComments($destAcc, $comment['id'], $latest_comment_id, $sourceUserId, 3);
        }
    }

    function copyComments($sourceVideoId, $destinationVideoId, $sourceUserId, $refType = 2, $is_duplicated = false, $workspace = false, $huddle_id = 0, $trim = false, $start_duration = '', $end_duration = '') {

        $comments = Comment::where(array(
                    'ref_type' => $refType,
                    'ref_id' => $sourceVideoId,
                    'site_id' => $this->site_id,
                    'active' => 1
                ))->orderBy('created_date')->get()->toArray();


        $standard_performance_levels = DocumentStandardRating::where(array('site_id' => $this->site_id, 'document_id' => $sourceVideoId))
                ->get()
                ->toArray();
        $account_folder_document = AccountFolderDocument::where(array('site_id' => $this->site_id, 'document_id' => $destinationVideoId))->first();
        if ($account_folder_document) {
            $account_folder_document = $account_folder_document->toArray();
        } else {
            $account_folder_document = [];
        }

        if (!empty($standard_performance_levels) && isset($account_folder_document['account_folder_id'])) {
            foreach ($standard_performance_levels as $row) {
                unset($row['document_id']);
                unset($row['account_folder_id']);
                unset($row['created_at']);
                unset($row['updated_at']);
                unset($row['id']);
                unset($row['include_in_analytics']);
                $performance_level_data = array(
                    'document_id' => $destinationVideoId,
                    'account_folder_id' => $account_folder_document['account_folder_id'],
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'include_in_analytics' => '0'
                        ) + $row;

                $standard_performance_levels = DocumentStandardRating::where(array('site_id' => $this->site_id, 'standard_id' => $row['standard_id'], 'rating_id' => $row['rating_id'], 'account_folder_id' => $account_folder_document['account_folder_id'], 'document_id' => $destinationVideoId))
                        ->first();

                if (!$standard_performance_levels) {
                    DB::table('document_standard_ratings')->insert($performance_level_data);
                }
            }
        }


        if (count($comments) == 0)
            return;

        foreach ($comments as $comment) {
            $get_tags = $this->gettagsbycommentid($comment['id'], array('1', '2')); //get tags
            $get_standards = $this->gettagsbycommentid($comment['id'], array('0')); //get standards

            $acc = UserAccount::where(array(
                        'user_id' => $sourceUserId,
                        'site_id' => $this->site_id
                    ))->first();

            if ($acc) {
                $acc = $acc->toArray();
            }


            $comment_user_id = $comment['user_id'];
            if ($refType == 3 || ($refType == 6 && $comment['parent_comment_id'] > 0))
                $parentCommentId = $destinationVideoId;
            else
                $parentCommentId = null;
            $comment_data = array(
                'parent_comment_id' => $parentCommentId,
                'site_id' => $this->site_id,
                'ref_type' => $refType,
                'ref_id' => $destinationVideoId,
                'user_id' => $comment_user_id,
                'created_by' => $comment_user_id,
                'last_edit_by' => $comment_user_id,
                    ) + $comment;
            unset($comment_data['id']);

            if ($trim) {
                if ($comment_data['time'] < $start_duration || $comment_data['time'] > $end_duration) {
                    $comment_data['time'] = '0';
                }

                if ($comment_data['time'] >= $start_duration && $comment_data['time'] <= $end_duration) {
                    $comment_data['time'] = (int) $comment_data['time'] - (int) $start_duration;
                }

                //for ios change
                if($comment_data['end_time'] < $start_duration || $comment_data['end_time'] > $end_duration  )
                {
                    $comment_data['end_time'] = '0';
                }
                
                if($comment_data['end_time'] >= $start_duration && $comment_data['end_time'] <= $end_duration )
                {
                    $comment_data['end_time'] = (int) $comment_data['end_time'] - (int) $start_duration;
                }
                
                
            }

            if (DB::table('comments')->insert($comment_data)) {
                $latest_comment_id = DB::getPdo()->lastInsertId();

                $comment_attachments = CommentAttachment::where(array('site_id' => $this->site_id, 'comment_id' => $comment['id']))->get()->toArray();
                foreach ($comment_attachments as $comment_attachment) {
                    $this->comment_attachment_transfer($comment_attachment['document_id'], $latest_comment_id, $huddle_id, $destinationVideoId, $is_duplicated, $workspace);
                }


                $replies = Comment::where(array('site_id' => $this->site_id, 'parent_comment_id' => $comment['id']))->get()->toArray();

                foreach ($replies as $replies) {
                    $repliesToreplies = Comment::where(array('site_id' => $this->site_id, 'parent_comment_id' => $replies['id']))->get()->toArray();

                    $comment_data = array(
                        'parent_comment_id' => $latest_comment_id,
                        'site_id' => $this->site_id,
                        'ref_type' => $replies['ref_type'],
                        'ref_id' => $destinationVideoId,
                        'user_id' => $replies['user_id'],
                        'created_by' => $replies['user_id'],
                        'last_edit_by' => $replies['user_id'],
                            ) + $replies;
                    unset($comment_data['id']);

                    $reply_to_reply_id = DB::table('comments')->insertGetId($comment_data);

                    foreach ($repliesToreplies as $replyToreply) {
                        $comment_data = array(
                            'parent_comment_id' => $reply_to_reply_id,
                            'ref_type' => $replyToreply['ref_type'],
                            'site_id' => $this->site_id,
                            'ref_id' => $destinationVideoId,
                            'user_id' => $replyToreply['user_id'],
                            'created_by' => $replyToreply['user_id'],
                            'last_edit_by' => $replyToreply['user_id'],
                                ) + $replyToreply;
                        unset($comment_data['id']);

                        DB::table('comments')->insert($comment_data);
                    }
                }


                if (!empty($get_tags)) {
                    foreach ($get_tags as $tag) {


                        $commentTag = AccountTag::where(array(
                                    'account_id' => $acc['account_id'],
                                    'site_id' => $this->site_id,
                                    'tag_title' => $tag['tag_title']
                                ))->first();

                        if ($commentTag) {
                            $commentTag = $commentTag->toArray();
                        }

                        $comment_tag_data = array(
                            'comment_id' => $latest_comment_id,
                            'site_id' => $this->site_id,
                            'ref_id' => $destinationVideoId,
                            'ref_type' => $tag['ref_type'],
                            'tag_title' => $tag['tag_title'],
                            'account_tag_id' => $commentTag['account_tag_id'],
                            'created_by' => $comment_user_id,
                            'created_date' => date("Y-m-d H:i:s"),
                            'include_in_analytics' => '0'
                                // 'last_edit_by' => $comment_user_id,
                        );
                        DB::table('account_comment_tags')->insert($comment_tag_data);
                    }
                }
                if (!empty($get_standards)) {
                    foreach ($get_standards as $tag) {


                        $commentTag = AccountTag::where(array(
                                    'account_id' => $acc['account_id'],
                                    'site_id' => $this->site_id,
                                    'tag_title' => $tag['tag_title']
                                ))->first();

                        if ($commentTag) {
                            $commentTag = $commentTag->toArray();
                        }

                        $comment_tag_data = array(
                            'comment_id' => $latest_comment_id,
                            'site_id' => $this->site_id,
                            'ref_id' => $destinationVideoId,
                            'ref_type' => 0,
                            'tag_title' => $tag['tag_title'],
                            'account_tag_id' => $tag['account_tag_id'],
                            'created_by' => $comment_user_id,
                            'created_date' => date("Y-m-d H:i:s"),
                            'include_in_analytics' => '0'
                                //    'last_edit_by' => $comment_user_id,
                        );
                        DB::table('account_comment_tags')->insert($comment_tag_data);
                    }
                }
            }

            //$this->copyComments($comment['Comment']['id'], $this->Comment->id, $sourceUserId, 3);
        }
    }

    function copytoaccounts(Request $request) {
        $input = $request->all();
//            $input['account_ids'] = array('2936','181');
//            $input['document_id'] = '216336';
//            $input['user_id'] = '18737';
//            $input['copy_notes'] = 1;

        $account_ids = $input['account_ids'];
        $documents = $input['document_id'];
        $user_id = $input['user_id'];
        $parent_folder_id = $request->has('parent_folder_id') ? $request->get('parent_folder_id') : 0;
        /*
          if (isset($input['copy_notes']) && ($input['copy_notes'] == "true" || $input['copy_notes'] == 1 ))
          $copy_notes = 1;
          else
          $copy_notes = 0;

          if($input['copy_notes'] === 0 || $input['copy_notes'] === "0" || empty($input['copy_notes']))
          {
          $copy_notes = 0;
          }
         */

        if (isset($input['copy_notes']) && !empty($input['copy_notes']) && $input['copy_notes'] !== "false")
            $copy_notes = 1;
        else
            $copy_notes = 0;

        if ($account_ids) {
            foreach ($account_ids as $account_id) {
                $msg = $this->copyToAccountWorkSpace($account_id, $user_id, $documents, $copy_notes, $parent_folder_id);
            }
        } else {

            return array(
                'success' => false,
                'message' => TranslationsManager::get_translation('please_select_atleast_one_account_msg', 'flash_messages', $this->site_id)
            );
        }
        return array(
            'success' => true,
            'message' => $msg
        );
    }

    function copyToAccountWorkSpace($account_id, $user_id, $document_ids, $copy_notes = 0, $parent_folder_id = 0, $fire_websocket = 1) {
        $doc_ids = explode(",", $document_ids);
        $response_msg = "";
        $reciver_detail = User::where(array('id' => $user_id, 'site_id' => $this->site_id))->first();
        $lang = $reciver_detail['lang'];
        try {
            foreach ($doc_ids as $doc) {
                $temp = explode("_", $doc);
                if(isset($temp[1]))
                {
                    $parent_folder_id = $temp[1];
                }
                $doc = $temp[0];
                $conditions = array('site_id' => $this->site_id, 'document_id' => $doc);
                $myfilesRow = AccountFolderDocument::get_row($conditions);
                $myfilesRow = $myfilesRow[0];
                $document = Document::getResourcesDetails($this->site_id, $myfilesRow['document_id']);

                if (empty($document)) {
                    continue;
                }
                $data_af = array(
                    'account_id' => $account_id,
                    'site_id' => $this->site_id,
                    'folder_type' => 3,
                    'name' => (!empty($myfilesRow['title']) ? $myfilesRow['title'] : $document['original_file_name']),
                    'desc' => $myfilesRow['desc'],
                    'active' => $document['active'],
                    'created_date' => date("Y-m-d H:i:s"),
                    'created_by' => $user_id,
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'last_edit_by' => $user_id,
                );

                DB::table('account_folders')->insert($data_af);

                $account_folder_id = DB::getPdo()->lastInsertId();

                $data_account_folder_meta_data = array(
                    'account_folder_id' => $account_folder_id,
                    'meta_data_name' => 'folder_type',
                    'meta_data_value' => '5',
                    'created_by' => $user_id,
                    'created_date' => date('Y-m-d H:i:s'),
                );

                DB::table('account_folders_meta_data')->insertGetId($data_account_folder_meta_data);


                $data_doc = array(
                    'account_id' => $account_id,
                    'site_id' => $this->site_id,
                    'doc_type' => $document['doc_type'],
                    'url' => $document['url'],
                    'original_file_name' => $document['original_file_name'],
                    'active' => $document['active'],
                    'view_count' => 0,
                    'created_date' => date("Y-m-d H:i:s"),
                    'created_by' => $user_id,
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'file_size' => $document['file_size'] ? $document['file_size'] : '',
                    'last_edit_by' => $user_id,
                    'encoder_provider' => $document['encoder_provider'],
                    'published' => $document['published'],
                    'video_is_saved' => $document['video_is_saved'],
                    'post_rubric_per_video' => '1',
                    'is_processed' => $document['is_processed'],
                    'source_document_id' => $myfilesRow['document_id'],
                    's3_thumbnail_url' => $document['s3_thumbnail_url'],
                    'stack_url' => $document['stack_url'],
                    'source_type' => '4',
                    'parent_folder_id' => $parent_folder_id,
                    'subtitle_available' => $document['subtitle_available'],
                    'allow_transcribe' => $document['allow_transcribe'],
                );
                DB::table('documents')->insert($data_doc);

                $video_id = DB::getPdo()->lastInsertId();
                if(!empty($parent_folder_id))
                {
                    DocumentFolder::where('id', $parent_folder_id)->update(['last_edit_date'=>date('Y-m-d H:i:s'), 'last_edit_by'=> $user_id]);
                }
                if ($copy_notes == 1) {
                    $this->copy_video_attachments($doc, $video_id, 1, 1, $account_folder_id, $account_id);
                }

                if ($document['published'] == 0) {

                    $requestXml = '<TranscodeVideoJob><document_id>' . $video_id . '</document_id><source_file>' . $document['url'] . '</source_file></TranscodeVideoJob>';
                    $video_data = array(
                        'JobId' => '2',
                        'site_id' => $this->site_id,
                        'CreateDate' => date("Y-m-d H:i:s"),
                        'RequestXml' => $requestXml,
                        'JobQueueStatusId' => 1,
                        'CurrentRetry' => 0,
                        'JobSource' => Sites::get_base_url($this->site_id)
                    );
                    DB::table('JobQueue')->insert($video_data);
                } else {

                    $documentFiles = DocumentFiles::get_document_row($myfilesRow['document_id'], $this->site_id);
                    if ($documentFiles) {
                        $video_data = array('site_id' => $this->site_id, 'id' => null, 'document_id' => $video_id, "created_at"=>date('Y-m-d H:i:s'), "debug_logs" => "Lumen::VideoController::copyToAccountWorkSpace::line=8671::document_id=".$video_id."::status=".$documentFiles['transcoding_status']."::created") + $documentFiles;
                        DB::table('document_files')->insert($video_data);
                    }
                }


                $data_afd = array(
                    'account_folder_id' => $account_folder_id,
                    'site_id' => $this->site_id,
                    'document_id' => $video_id,
                    'title' => $myfilesRow['title'],
                    'zencoder_output_id' => $myfilesRow['zencoder_output_id'],
                    'desc' => $myfilesRow['desc']
                );

                DB::table('account_folder_documents')->insert($data_afd);

                if ($copy_notes == 1) {
                    @$this->copyAccountComments($account_id, $myfilesRow['document_id'], $video_id, $user_id, 2, $account_folder_id);
                    @$this->copyAccountComments($account_id, $myfilesRow['document_id'], $video_id, $user_id, 6, $account_folder_id);
                }

                $doc_type = $document['doc_type'];
                if($doc_type == 1)
                {
                    $current_resource = "video";
                }
                else if($doc_type == 2)
                {
                    $current_resource = "resource";
                }
                else if($doc_type == 3)
                {
                    if($lang == 'es')
                    {
                    $current_resource = TranslationsManager::get_translation('scripted_note_msg', 'flash_messages', $this->site_id);
                    }
                    else
                    {
                    $current_resource = TranslationsManager::get_translation('scripted_note_msg', 'flash_messages', $this->site_id);
                    }

                    if(!$copy_notes)
                    {
                       $current_resource = "video";
                    }
                }
                else if($doc_type == 5)
                {
                    $current_resource = "url";
                }
                else
                {
                    $current_resource = "artifact";
                }


                $activity_desc = ucwords($current_resource) ." ". ucwords($myfilesRow['title']).' shared.';
                $user_activity_logs = array(
                    'ref_id' => $video_id,
                    'desc' => $activity_desc,
                    'url' => '/home/workspace_video/home/' . $account_folder_id . '/' . $video_id,
                    'type' => '22',
                    'account_id' => $account_id,
                    'account_folder_id' => $account_folder_id,
                    'environment_type' => 2,
                    'site_id' => $this->site_id
                );
                if ($document['subtitle_available'] == "1") {
                        // Duplicate the transcribe files on s3
                        $videoFilePath = pathinfo($document['url']);
                        $videoFileName = $videoFilePath['dirname'];

                       $this->copyTranscribedFile($videoFileName, $video_id, $document['id']);

                    }
                $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
                if($fire_websocket)
                {
                    $new_video = Document::get_single_video_data($this->site_id, $video_id, 3, 1, $account_folder_id, $user_id);
                    $channel_data = array(
                        'data' => $new_video,
                        'channel' => 'workspace-' . $account_id . "-" . $user_id,
                        'event' => "resource_added"
                    );
                    HelperFunctions::broadcastEvent($channel_data);
                }

                $response_msg .= $current_resource . " " . TranslationsManager::get_translation('copied_successfully');
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
        return $response_msg;
    }

    function get_copy_huddle(Request $request) {


         
//        $user_id = '18737';
//        $account_id = '2936';

        $input = $request->all();
        $user_id = $input['user_id'];
        $account_id = $input['account_id'];
        $only_huddles = $request->has('only_huddles') ? ($request->get('only_huddles') == 1 ? 1 : 0) : 0;
        $search = isset($input['search']) ? $input['search'] : null;
        $without_assessment = 0;
        if(isset($input['without_assessment']))
        {
            $without_assessment = 1;
        }

        $accountFolderUsers = AccountFolderUser::get($user_id, $this->site_id);
        $accountFolderGroups = UserGroup::get_user_group($user_id, $this->site_id);

        $accountFolderIds = array();
        $huddle_ids = array();
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFolderIds[] = $row['account_folder_id'];
                $huddle_ids[] = $row['account_folder_id'];
            }
            $accountFolderIds = "'" . implode("','", $accountFolderIds) . "'";
        } else {
            $accountFolderIds = '0';
        }
        $accountFolderGroupsIds = array();
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_id'];
            }
            $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }
        
        if($search != null && $search != '')
        {

            //huddles ids for the user
            $huddles = AccountFolder::select("AccountFolder.account_folder_id")
            ->join("account_folders_meta_data","AccountFolder.account_folder_id","account_folders_meta_data.account_folder_id")
            ->where("account_folders_meta_data.meta_data_name","folder_type")
            ->join("account_folder_users","AccountFolder.account_folder_id","account_folder_users.account_folder_id")
            ->leftjoin('account_folder_groups',"AccountFolder.account_folder_id","account_folder_groups.account_folder_id")
            ->leftjoin('user_groups', function ($join) use($user_id) {
                $join->on('account_folder_groups.group_id', '=', 'user_groups.group_id')
                    ->where('user_groups.user_id',$user_id);
            })
            ->where(function($query) use($user_id){
                $query->orWhere('user_groups.user_id',$user_id)
                        ->orWhere("account_folder_users.user_id",$user_id);
            })
            ->where('AccountFolder.account_id',$account_id)
            ->where('AccountFolder.folder_type',1)
            ->when(isset($input['without_assessment']) && $input['without_assessment'] == 1, function($query){
                $query->where("account_folders_meta_data.meta_data_value","<>" ,3);
            })
            ->where('AccountFolder.active',1)
            ->where('AccountFolder.site_id',$this->site_id)
            ->distinct("AccountFolder.account_folder_id")
            ->get()->pluck('account_folder_id')->toArray();

            // dd($huddles);

            //DB::enableQueryLog();
            $role_id = $input['role_id'];
            $page = $request->has("page") ? $request->get("page") : 1;
            $allData = [];
            $allData["htype"] = '';
            $allData["user_id"] = $user_id;
            $allData["account_id"] = $account_id;
            $allData["role_id"] = $role_id;
            $allData["search"] = $search;
            $allData["limit"] = 20;
            $allData["page"] = $page;
            $allData["doc_type"] = $request->has("doc_type") ? $request->get("doc_type") : 1;
            $c_request = new Request($allData);
            $docs = app('App\Http\Controllers\GoalsController')->getHuddleDocuments($c_request, $huddles);
            //$executed_quires = DB::getQueryLog();
            return response()->json(["status" => true, "message" => "success", "data" => $docs["items"], "huddle_counts" => $docs["total_records"]]);
        }

        $all_huddles = AccountFolder::copyAllHuddles($this->site_id, $account_id, 'date', FALSE, $accountFolderIds, $accountFolderGroupsIds,$without_assessment);
        if($only_huddles == 0)
        {
            $all_accounts = User::get_user_accounts($user_id, $this->site_id);
        }
        else
        {
            $all_accounts = [];
        }
        $new_all_huddles = array();
        if ($all_huddles) {
            foreach ($all_huddles as $row) {
                if (empty($row['folderType']) && $row['is_sample'] == '1') {
                    continue;
                }
                $vide_huddle_id = $row['account_folder_id'];
                if (!AccountFolderMetaData::check_if_eval_huddle($vide_huddle_id, $this->site_id)) {
                    $submission_allowed = AccountFolderMetaData::where('account_folder_id', $vide_huddle_id)->where('meta_data_name', 'submission_allowed')->first();
                    //$submission_allowed = $this->AccountFolderMetaData->find('first', array('conditions' => array('account_folder_id' => $video_huddle_id, 'meta_data_name' => 'submission_allowed')));
                    $vide_max_limit = 1;
                    if (!empty($submission_allowed) && $submission_allowed['meta_data_value'] != '') {
                        $vide_max_limit = $submission_allowed['meta_data_value'];
                    }
                    $row['max_allowed_videos'] = $vide_max_limit;
                    $row['logged_in_participant_uploaded_videos'] = (string) HelperFunctions::get_eval_participant_videos($vide_huddle_id, $user_id, $this->site_id);
                } else {
                    $row['max_allowed_videos'] = 0;
                    $row['logged_in_participant_uploaded_videos'] = 0;
                }
                if(isset($row['folder_count']) && $row['folder_count'] > 0)
                {
                    $row['hasChildren'] = true;
                }
                $new_all_huddles[] = $row;
                
            }
        }
        
        $final_array = array(
            'all_huddles' => $new_all_huddles,
            'all_accounts' => $all_accounts
        );
        
        return ($final_array);
    }

    public function video_detail(Request $request) {

        $input = $request->all();

//        $input['account_id'] = '181';
//        $input['role_id'] = '100';
//        $input['user_id'] = '621';
//        $input['document_id'] = '190956';
//        $input['coachee_id'] = '12329';
//        $input['huddle_id'] = '54397';


        $account_id = $input['account_id'];
        $user_role_id = $input['role_id'];
        $user_id = $input['user_id'];
        $document_id = $input['document_id'];
        $coachee_id = $input['coachee_id'];
        $huddle_id = $input['huddle_id'];

        $htype = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);
        $coaching_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'coaching_perfomance_level', 'site_id' => $this->site_id))->first();
        if ($coaching_perfomance_level) {
            $coaching_perfomance_level = $coaching_perfomance_level->toArray();
        }
        $coaching_perfomance_level = isset($coaching_perfomance_level['meta_data_value']) ? $coaching_perfomance_level['meta_data_value'] : "0";

        $assessment_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'assessment_perfomance_level', 'site_id' => $this->site_id))->first();
        if ($assessment_perfomance_level) {
            $assessment_perfomance_level = $assessment_perfomance_level->toArray();
        }
        $assessment_perfomance_level = isset($assessment_perfomance_level['meta_data_value']) ? $assessment_perfomance_level['meta_data_value'] : "0";


        $metric_old = AccountMetaData::where(array(
                    'account_id' => $account_id,
                    'site_id' => $this->site_id
                ))->whereRaw('meta_data_name like "metric_value_%"')->orderBy('meta_data_value')->get()->toArray();


        $metric_new = array();
        foreach ($metric_old as $metric) {
            $metric_new [] = array('name' => strtolower(substr($metric['meta_data_name'], 13)), 'value' => $metric['meta_data_value']);
        }

        $id_framework = AccountFolderMetaData::where(array('site_id' => $this->site_id, 'account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id'
                ))->first();

        if ($id_framework) {
            $id_framework->toArray();
        }

        $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";


        if ($framework_id == -1 || $framework_id == 0) {

            $id_framework = AccountMetaData::where(array('site_id' => $this->site_id,
                        'account_id' => $account_id, 'meta_data_name' => 'default_framework'
                    ))->first();


            if ($id_framework) {
                $id_framework->toArray();
            }

            $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";
        }

        $account_folder_document = AccountFolderDocument::where(array(
                    "document_id" => $document_id,
                    "site_id" => $this->site_id
                ))->first();

        if ($account_folder_document) {
            $account_folder_document = $account_folder_document->toArray();
            if (!empty($account_folder_document['video_framework_id'])) {
                $framework_id = $account_folder_document['video_framework_id'];
            }
        }


        $standards_settings = AccountFrameworkSetting::where(array(
                    'site_id' => $this->site_id,
                    "account_id" => $account_id,
                    "account_tag_id" => $framework_id
                ))->get()->toArray();

        if (isset($standards_settings[0]['enable_performance_level'])) {
            $performace_level_switch = $standards_settings[0]['enable_performance_level'];
        } else {
            $performace_level_switch = false;
        }


        $joins = array(
            array(
                'table' => 'account_comment_tags as act',
                'type' => 'inner',
                'conditions' => 'AccountTag.account_tag_id = act.account_tag_id'
            )
        );
        $conditions = array(
            'AccountTag.tag_type' => 0,
            'AccountTag.site_id' => $this->site_id,
            'act.ref_id' => $document_id
        );
        $fields = array(
            'AccountTag.tag_title',
            'AccountTag.tads_code',
            'AccountTag.account_tag_id',
            'act.created_date AS AccountTag__tags_date',
            'COUNT(act.account_comment_tag_id) AccountTag__total_tags'
        );
        $get_value = array(
            'joins' => $joins,
            'conditions' => $conditions,
            'fields' => $fields,
            'group' => array('AccountTag.account_tag_id'),
            'order' => array('AccountTag__total_tags' => 'desc'),
            'limit' => 5
        );

        $video_tags = AccountTag
                ::join('account_comment_tags as act', 'AccountTag.account_tag_id', '=', 'act.account_tag_id')
                ->select('AccountTag.tag_title', 'AccountTag.tads_code', 'AccountTag.account_tag_id', 'AccountTag.standard_analytics_label')
                ->selectRaw('COUNT(act.account_comment_tag_id) AccountTag__total_tags')
                ->selectRaw('act.created_date AS AccountTag__tags_date')
                ->where(array('AccountTag.tag_type' => 0, 'act.ref_id' => $document_id, 'AccountTag.site_id' => $this->site_id))
                ->orderByRaw('AccountTag__total_tags DESC')
                ->groupBy('AccountTag.account_tag_id')
                //->limit(5)
                ->orderBy('AccountTag.standard_position')
                ->get()
                ->toArray();
        $new_video_tags = array();

        foreach ($video_tags as $video_tag) {

            if (!empty($video_tag['standard_analytics_label'])) {
                $video_tag['tag_title'] = $video_tag['standard_analytics_label'];
            }

            $new_video_tags[] = $video_tag;
        }

        $video_tags = $new_video_tags;


        $account_coach_tags_analytics = $video_tags;




        $mt_arr = array();

        if ($performace_level_switch) {
            $account_framework_settings_performance_levels = AccountFrameworkSettingPerformanceLevel::select('AccountFrameworkSettingPerformanceLevel.*')->where(array(
                        'AccountFrameworkSettingPerformanceLevel.account_framework_setting_id' => $standards_settings[0]['id'],
                        'AccountFrameworkSettingPerformanceLevel.site_id' => $this->site_id,
                    ))->orderBy('performance_level_rating')->get()->toArray();

            foreach ($account_framework_settings_performance_levels as $pl) {

                $mt_arr[] = $pl['performance_level'];
            }
        } else {
            foreach ($metric_old as $mt_old) {
                $mt_arr[] = substr($mt_old['meta_data_name'], 13);
            }
        }

        $assessments = array(0 => 'No Assessment');
        $arr_all = array_merge($assessments, $mt_arr);


        $count_t_avg = 0;
        $total_avg_rattings = 0;
        foreach ($account_coach_tags_analytics as $key => $row) {

            $ratings_array = DocumentStandardRating::where(array('site_id' => $this->site_id, 'document_id' => $document_id, 'standard_id' => $row['account_tag_id'], 'account_id' => $account_id))
                    ->get()
                    ->toArray();

            $count = 0;
            $avg_sum = 0;
            $avg = 0;
            foreach ($ratings_array as $values) {



                $avg = $avg + (int) $values['rating_value'];
                $count++;
            }
            if ($count > 0) {

                $final_average = round($avg / $count);
            } else {
                $final_average = 0;
            }
            if ($final_average == 0) {
                $account_coach_tags_analytics[$key]['label'] = 'No Rating';
            }

            $total_avg_rattings = $total_avg_rattings + $final_average;
            if ($final_average != 0) {
                $count_t_avg ++;
            }

            if ($final_average > 0) {
                $final_average_name = '';

                if ($performace_level_switch) {
                    foreach ($account_framework_settings_performance_levels as $pl) {
                        if ($final_average == $pl['performance_level_rating']) {
                            $final_average_name = $pl['performance_level'];
                        }
                    }
                } else {

                    foreach ($metric_new as $metric) {
                        if ($final_average == $metric['value']) {
                            $final_average_name = $metric['name'];
                        }
                    }
                }

                if ($coaching_perfomance_level == '0' && $htype == '2') {
                    $final_average = 0;
                    $final_average_name = 'No Ratings';
                }

                if ($assessment_perfomance_level == '0' && $htype == '3') {
                    $final_average = 0;
                    $final_average_name = 'No Ratings';
                }

                $account_coach_tags_analytics[$key]['label'] = $final_average_name;
                $account_coach_tags_analytics[$key]['average_rating'] = $final_average;
                $account_coach_tags_analytics[$key]['color_rating'] = '#000';
            }
        }


        if ($count_t_avg == 0) {
            $total_ratting_avg = 0;
        } else {
            $total_ratting_avg = round($total_avg_rattings / $count_t_avg);
        }


        $total_ratting_name = 'No Rating';

        if ($total_ratting_avg > 0) {
            if ($performace_level_switch) {
                foreach ($account_framework_settings_performance_levels as $pl) {
                    if ($total_ratting_avg == $pl['performance_level_rating']) {
                        $total_ratting_name = $pl['performance_level'];
                    }
                }
            } else {
                foreach ($metric_new as $metric) {
                    if ($total_ratting_avg == $metric['value']) {
                        $total_ratting_name = $metric['name'];
                    }
                }
            }
        }


        $total_ratting_avg = $total_ratting_avg . ' - ' . $total_ratting_name;


        $final_array = array();

        $final_array ['ratting_title'] = 'Average Performance Level';
        $final_array ['standard_title'] = 'Total tagged standards';
        $final_array ['total_ratting'] = $total_ratting_avg;

        $final_array['video_tags'] = $account_coach_tags_analytics;



        $doc_count = Document::join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                ->join('account_folderdocument_attachments as afda', function($join) {
                    $join->on('afda.account_folder_document_id', '=', 'afd.id');
                    //   $join->whereRaw('afda.attach_id=' . $document_id);
                })
                ->select('documents.*', 'afd.title', 'afd.desc', 'afd.account_folder_id')
                ->where(array('doc_type' => '2', 'afd.site_id' => $this->site_id))
                ->whereRaw('afda.attach_id=' . $document_id)
                ->count();




        $final_array['resources'] = $doc_count;

        $addtional_join = '';
        $additionalCon = '';
        if ($user_role_id == 115) {

            $additionalCon = array(
                'afu.role_id' => 200,
                'afu.user_id' => $user_id
            );
        }

        if ($user_role_id == 115) {

            $video_detail = Document::leftJoin('users as User', 'documents.created_by', '=', 'User.id')
                    ->join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                    ->join('account_folder_users as afu', 'afd.account_folder_id', '=', 'afd.account_folder_id')
                    ->select('documents.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'afd.account_folder_id')
                    ->whereIn('doc_type', [1, 3])
                    ->where(array('documents.id' => $document_id, 'documents.site_id' => $this->site_id))
                    ->where($additionalCon)
                    ->first();
        } else {
            $video_detail = Document::leftJoin('users as User', 'documents.created_by', '=', 'User.id')
                    ->join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                    ->select('documents.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'afd.account_folder_id')
                    ->whereIn('doc_type', [1, 3])
                    ->where(array('documents.id' => $document_id, 'documents.site_id' => $this->site_id))
                    ->first();
        }

        if ($video_detail) {
            $video_detail = $video_detail->toArray();
        }


        $h_id = AccountFolderDocument::leftJoin('account_folder_users as afu', 'afu.account_folder_id', '=', 'account_folder_documents.account_folder_id')
                        ->select('afu.user_id')
                        ->where(array('account_folder_documents.document_id' => $document_id, 'afu.role_id' => 200, 'afu.site_id' => $this->site_id))
                        ->get()->toArray();

        $coach_users = array();
        foreach ($h_id as $coach_huddles) {
            $coach_users[] = $coach_huddles['user_id'];
        }




        $coach_feedback = Comment::where(array('site_id' => $this->site_id, 'ref_id' => $document_id, 'ref_type' => 4))
                ->whereIn('user_id', $coach_users)
                ->orderByRaw('created_date DESC')
                ->first();

        if ($coach_feedback) {
            $coach_feedback = $coach_feedback->toArray();
        }

        $final_array['coach_feedback'] = $coach_feedback;
        $final_array['video_detail'] = $video_detail;


        $coachee_name = User::where(array('site_id' => $this->site_id, 'id' => $coachee_id))
                ->first();
        if ($coachee_name) {
            $coachee_name = $coachee_name->toArray();
        }


        $final_array['coachee_name'] = $coachee_name;
        $final_array['assessment_array'] = $arr_all;

        $coach_comments = Comment::where(array('site_id' => $this->site_id, 'ref_id' => $document_id))
                ->whereRaw('ref_type IN (2,3)')
                ->whereIn('user_id', $coach_users)
                ->count();

        $final_array['coach_comments'] = $coach_comments;

        $coachee_comments = Comment::where(array('site_id' => $this->site_id, 'ref_id' => $document_id))
                ->whereRaw('ref_type IN (2,3)')
                ->where(array('user_id' => $coachee_id))
                ->count();
        $final_array['coachee_comments'] = $coachee_comments;


        $document = Document::where(array('site_id' => $this->site_id, 'id' => $document_id))
                ->first();
        if ($document) {
            $document = $document->toArray();
        }
        $final_array['document'] = $document;

        $can_view_summary = AccountFolderMetaData::where(array(
                    'account_folder_id' => $huddle_id, 'meta_data_name' => 'coachee_can_view_summary', 'site_id' => $this->site_id
                ))->get()->toArray();

        $final_array['can_view_summary_value'] = isset($can_view_summary[0]['meta_data_value']) ? $can_view_summary[0]['meta_data_value'] : "0";

        return $final_array;
    }

    function getAverageRatingOfVideo($account_id, $huddle_id, $document_id) {

        $framework_id = $this->getFrameworkIdByHuddleId($account_id, $huddle_id);

        $account_folder_document = AccountFolderDocument::where(array(
                    "document_id" => $document_id,
                    "site_id" => $this->site_id
                ))->first();

        if ($account_folder_document) {
            $account_folder_document = $account_folder_document->toArray();
            if (!empty($account_folder_document['video_framework_id'])) {
                $framework_id = $account_folder_document['video_framework_id'];
            }
        }



        $standards_settings = AccountFrameworkSetting::where(array(
                    "account_id" => $account_id,
                    "site_id" => $this->site_id,
                    "account_tag_id" => $framework_id
                ))->get()->toArray();

        if (isset($standards_settings[0]['enable_performance_level'])) {
            $performace_level_switch = $standards_settings[0]['enable_performance_level'];
        } else {
            $performace_level_switch = false;
        }

        $standards = AccountTag :: where(array('framework_id'=>$framework_id,'tag_type'=>"0"))->get()->toArray();


        $video_tags = AccountTag
                ::join('account_comment_tags as act', 'AccountTag.account_tag_id', '=', 'act.account_tag_id')
                ->select('AccountTag.tag_title', 'AccountTag.tads_code', 'AccountTag.account_tag_id', 'AccountTag.standard_analytics_label')
                ->selectRaw('COUNT(act.account_comment_tag_id) AccountTag__total_tags')
                ->selectRaw('act.created_date AS AccountTag__tags_date')
                ->where(array('AccountTag.tag_type' => 0, 'AccountTag.site_id' => $this->site_id, 'act.ref_id' => $document_id))
                ->orderByRaw('AccountTag__total_tags DESC')
                ->groupBy('AccountTag.account_tag_id')
                //->limit(5)
                ->get()
                ->toArray();
        $new_video_tags = array();

        foreach ($video_tags as $video_tag) {

            if (!empty($video_tag['standard_analytics_label'])) {
                $video_tag['tag_title'] = $video_tag['standard_analytics_label'];
            }

            $new_video_tags[] = $video_tag;
        }

        $video_tags = $new_video_tags;


        $account_coach_tags_analytics = $video_tags;

        $metric_old = AccountMetaData::where(array(
                    'account_id' => $account_id,
                    'site_id' => $this->site_id
                ))->whereRaw('meta_data_name like "metric_value_%"')->orderBy('meta_data_value')->get()->toArray();
        $metric_new = array();
        foreach ($metric_old as $metric) {
            $metric_new [] = array('name' => strtolower(substr($metric['meta_data_name'], 13)), 'value' => $metric['meta_data_value']);
        }

        $mt_arr = array();

        if ($performace_level_switch) {
            $account_framework_settings_performance_levels = AccountFrameworkSettingPerformanceLevel::select('AccountFrameworkSettingPerformanceLevel.*')->where(array(
                        'AccountFrameworkSettingPerformanceLevel.account_framework_setting_id' => $standards_settings[0]['id'],
                        'AccountFrameworkSettingPerformanceLevel.site_id' => $this->site_id
                    ))->orderBy('performance_level_rating')->get()->toArray();

            foreach ($account_framework_settings_performance_levels as $pl) {

                $mt_arr[] = $pl['performance_level'];
            }
        } else {
            foreach ($metric_old as $mt_old) {
                $mt_arr[] = substr($mt_old['meta_data_name'], 13);
            }
        }

        $assessments = array(0 => 'No Assessment');
        $arr_all = array_merge($assessments, $mt_arr);


        $count_t_avg = 0;
        $total_avg_rattings = 0;
        foreach ($standards as $key => $row) {

            $ratings_array = DocumentStandardRating::where(array('document_id' => $document_id, 'site_id' => $this->site_id, 'standard_id' => $row['account_tag_id'], 'account_id' => $account_id))
                    ->get()
                    ->toArray();

            $count = 0;
            $avg_sum = 0;
            $avg = 0;
            foreach ($ratings_array as $values) {
                $avg = $avg + (int) $values['rating_value'];
                $count++;
            }
            if ($count > 0) {

                $final_average = round($avg / $count);
            } else {
                $final_average = 0;
            }
            if ($final_average == 0) {
                $account_coach_tags_analytics[$key]['label'] = 'No Rating';
            }

            $total_avg_rattings = $total_avg_rattings + $final_average;
            if ($final_average != 0) {
                $count_t_avg ++;
            }

            if ($final_average > 0) {
                $final_average_name = '';

                if ($performace_level_switch) {
                    foreach ($account_framework_settings_performance_levels as $pl) {
                        if ($final_average == $pl['performance_level_rating']) {
                            $final_average_name = $pl['performance_level'];
                        }
                    }
                } else {

                    foreach ($metric_new as $metric) {
                        if ($final_average == $metric['value']) {
                            $final_average_name = $metric['name'];
                        }
                    }
                }

                $account_coach_tags_analytics[$key]['label'] = $final_average_name;
                $account_coach_tags_analytics[$key]['average_rating'] = $final_average;
                $account_coach_tags_analytics[$key]['color_rating'] = '#000';
            }
        }


        if ($count_t_avg == 0) {
            $total_ratting_avg = 0;
        } else {
            $total_ratting_avg = round($total_avg_rattings / $count_t_avg);
        }


        $total_ratting_name = 'No Rating';

        if ($total_ratting_avg > 0) {
            if ($performace_level_switch) {
                foreach ($account_framework_settings_performance_levels as $pl) {
                    if ($total_ratting_avg == $pl['performance_level_rating']) {
                        $total_ratting_name = $pl['performance_level'];
                    }
                }
            } else {
                foreach ($metric_new as $metric) {
                    if ($total_ratting_avg == $metric['value']) {
                        $total_ratting_name = $metric['name'];
                    }
                }
            }
        }


        $total_ratting_avg = $total_ratting_avg . ' - ' . $total_ratting_name;

        return $total_ratting_avg;
    }

    function getFrameworkIdByHuddleId($account_id, $huddle_id) {
        $id_framework = AccountFolderMetaData::where(array('site_id' => $this->site_id, 'account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id'
                ))->first();

        if ($id_framework) {
            $id_framework->toArray();
        }

        $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";


        if ($framework_id == -1 || $framework_id == 0) {

            $id_framework = AccountMetaData::where(array('site_id' => $this->site_id,
                        'account_id' => $account_id, 'meta_data_name' => 'default_framework'
                    ))->first();


            if ($id_framework) {
                $id_framework->toArray();
            }

            $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";
        }
        return $framework_id;
    }

    function show_publish_button_or_not(Request $request) {
        $input = $request->all();

//        $input['video_id'] =  '220782';
//       $input['huddle_id'] = '56177';
//       $input['user_id'] = '621';

        $huddle_id = $input['huddle_id'];
        $user_id = $input['user_id'];
        $video_id = $input['video_id'];

        $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);
        if ($h_type == 3 || ($h_type == 2 && $this->is_enabled_coach_feedback($huddle_id, $this->site_id))) {

            if ($this->check_if_evalutor($huddle_id, $user_id)) {
                $total_incative_comments = Comment::where(array('site_id' => $this->site_id, 'ref_id' => $video_id))
                        ->whereIn('active', ['', '0'])
                        ->count();
            } else {
                $total_incative_comments = 0;
            }
        } else {
            $total_incative_comments = 0;
        }
        if ($total_incative_comments > 0) {
            return array('show_publish_button' => true);
        } else {
            return array('show_publish_button' => false);
        }
    }

    public function coaching_tracker_note(Request $request) {

        $input = $request->all();
        $user_id = $input['user_id'];
        $video_id = $input['document_id'];

        if (empty($input['coach_feedback_id'])) {

            $data = array(
                'title' => '',
                'comment' => $input['coach_response'],
                'site_id' => $this->site_id,
                'ref_id' => $input['document_id'],
                'ref_type' => '4',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );

            if (DB::table('comments')->insert($data)) {
                $saved_id = DB::getPdo()->lastInsertId();
                Document::where('id',$video_id)->update(['last_edit_date'=>date("Y-m-d H:i:s"), 'last_edit_by'=>$user_id]);
                return array(
                    'success' => true,
                    'message' => TranslationsManager::get_translation('coach_comment_note_has_been_added_successfully', 'flash_messages', $this->site_id),
                    'coach_feedback_id' => $saved_id
                );
            }
        } else {
            $data = array(
                'comment' => addslashes($input['coach_response']),
                'site_id' => $this->site_id,
            );

            DB::table('comments')->where(['id' => $input['coach_feedback_id']])->update($data);
            Document::where('id',$video_id)->update(['last_edit_date'=>date("Y-m-d H:i:s"), 'last_edit_by'=>$user_id]);
            return array(
                'success' => true,
                'message' => TranslationsManager::get_translation('coach_comment_note_has_been_added_successfully', 'flash_messages', $this->site_id),
                'coach_feedback_id' => $input['coach_feedback_id']
            );
        }
    }

    public function assessment_note(Request $request) {
        $input = $request->all();
        $user_id = $input['user_id'];
        if (empty($input['assessment_feedback_id'])) {
            $data = array(
                'title' => '',
                'comment' => $input['assessment_note'],
                'site_id' => $this->site_id,
                'ref_id' => $input['document_id'],
                'ref_type' => '5',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id
            );

            if (DB::table('comments')->insert($data)) {
                $saved_id = DB::getPdo()->lastInsertId();
                return array(
                    'success' => true,
                    'message' => TranslationsManager::get_translation('assessment_has_been_added_successfully', 'flash_messages', $this->site_id),
                    'coach_feedback_id' => $saved_id
                );
            }
        } else {
            $data = array(
                'comment' =>addslashes($input['assessment_note']),
                'site_id' => $this->site_id,
            );
            DB::table('comments')->where(['id' => $input['assessment_feedback_id']])->update($data);
            return array(
                'success' => true,
                'message' => TranslationsManager::get_translation('assessment_has_been_added_successfully', 'flash_messages', $this->site_id),
                'coach_feedback_id' => $input['assessment_feedback_id']
            );
        }
    }

    function assessment_detail(Request $request) {
//        $account_id = 181 ;
//        $user_id = 621 ;
//        $document_id = 110271;
//        $coach_id = 621;
//        $coachee_id = 12329;
//        $huddle_id = 51897;
        $input = $request->all();
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $document_id = $input['document_id'];
        $coachee_id = $input['coachee_id'];
        $huddle_id = $input['huddle_id'];

        // $account_id = 181;
        // $user_id = 621;
        // $document_id = 88106;
        // $coachee_id = ;

        $details = \App\Services\AssessmentDetail::getDetails($this->site_id, $huddle_id, $account_id, $user_id, $document_id, $coachee_id);

        return response()->json($details);
    }

    function coaching_perfomance_level($account_id, $huddle_type, $logged_user_role) {


        $check_data = AccountMetaData::where(array(
                    "account_id" => $account_id,
                    "site_id" => $this->site_id,
                    "meta_data_name" => "coaching_perfomance_level"
                ))->first();

        if ($check_data) {
            $check_data = $check_data->toArray();
        }

        if (!empty($check_data)) {

            if ($check_data['meta_data_value'] && ($logged_user_role == 200 || $logged_user_role == 210) && $huddle_type == 2) {
                return 1;
            } else {
                return 0;
            }
        } else {

            return 0;
        }
    }

    function is_enabled_framework_and_standards($account_id) {

        $check_data = AccountFolderMetaData::where(array(
                    "account_folder_id" => $account_id,
                    "site_id" => $this->site_id,
                    "meta_data_name" => "enable_framework_standard"
                ))->first();

        if ($check_data) {
            $check_data = $check_data->toArray();
        }

        if (isset($check_data['meta_data_value']) && $check_data['meta_data_value'] == '1') {
            return true;
        } else {
            return false;
        }
    }

    function is_enabled_framework_and_standards_ws($account_id) {

        $check_data = AccountFolderMetaData::where(array(
                    "account_folder_id" => $account_id,
                    "site_id" => $this->site_id,
                    "meta_data_name" => "enable_framework_standard_ws"
                ))->first();

        if ($check_data) {
            $check_data = $check_data->toArray();
        }

        if (isset($check_data['meta_data_value']) && $check_data['meta_data_value'] == '1') {
            return true;
        } else {
            return false;
        }
    }

    function is_enabled_huddle_framework_and_standards($account_folder_id) {

        $check_data = AccountFolderMetaData::where(array(
                    "account_folder_id" => $account_folder_id,
                    "site_id" => $this->site_id,
                    "meta_data_name" => "chk_frameworks"
                ))->first();

        if ($check_data) {
            $check_data = $check_data->toArray();
        }

        if (isset($check_data['meta_data_value'])) {
            if ($check_data['meta_data_value'] == '1') {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    function _check_evaluator_permissions($huddle_id, $created_by, $user_id, $role_id) {
        if (!$this->check_if_eval_huddle($huddle_id)) {
            return true;
        }
        $is_avaluator = $this->check_if_evalutor($huddle_id, $user_id);
        $evaluators_ids = $this->get_evaluator_ids($huddle_id);
        $participants_ids = $this->get_participants_ids($huddle_id);
        $isEditable = '';
        if ($is_avaluator && $role_id == 120) {
            $isEditable = ($user_id == $created_by || (is_array($participants_ids) && in_array($created_by, $participants_ids)));
        } elseif ($is_avaluator && ($role_id == 110 || $role_id == 100 || $role_id == 115 || $role_id == 125 )) {
            $isEditable = ($is_avaluator || ($user_id == $created_by || (is_array($evaluators_ids) && in_array($created_by, $evaluators_ids))));
        } else {
            $isEditable = (($user_id == $created_by || (is_array($evaluators_ids) && in_array($created_by, $evaluators_ids))));
        }
        return $isEditable;
    }

    function get_participants_ids($huddle_id) {


        $result = AccountFolderUser::where(array(
                    'role_id' => 210,
                    'site_id' => $this->site_id,
                    'account_folder_id' => $huddle_id))
                ->get()
                ->toArray();

        $participants = array();
        if ($result) {
            foreach ($result as $row) {
                $participants[] = $row['user_id'];
            }
            return $participants;
        } else {
            return false;
        }
    }

    public static function check_if_eval_huddle($huddle_id) {
        $site_id = app("Illuminate\Http\Request")->header("site_id");
        $eval_huddles = AccountFolderMetaData::where(array(
                    "meta_data_value" => "3",
                    "site_id" => $site_id,
                    "meta_data_name" => "folder_type",
                    "account_folder_id" => $huddle_id
                ))->count();

        if ($eval_huddles > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function mobile_video(Request $request) {
        $input = $request->all();
        $video_id = $input['video_id'];
        $video = Document::find($video_id);
        $huddle_detail = array();
        $huddle_id = "";
        if (isset($input['huddle_id'])) {
            $huddle_id = $input['huddle_id'];
            $huddle_detail = AccountFolder::where(array('site_id' => $this->site_id, 'account_folder_id' => $huddle_id))->first();
            if ($huddle_detail) {
                $huddle_detail = $huddle_detail->toArray();
            }

            if (!empty($huddle_id) && $video) {
                $video->huddle_info = $huddle_detail;
            }
        }
        $rubric_check = false;
        if (isset($input['huddle_id']) && isset($input['account_id']) && isset($input['user_id']) && $video) {
            $huddle_id = $input['huddle_id'];
            $account_id = $input['account_id'];
            $user_id = $input['user_id'];
            if ($this->is_enabled_framework_and_standards($account_id)) {
                if ($this->is_enabled_huddle_framework_and_standards($huddle_id)) {
                    $rubric_check = true;
                }
            }
            $allow_per_video_details = AccountFolderMetaData::where(array('site_id' => $this->site_id, 'account_folder_id' => $huddle_id, "meta_data_name" => "allow_per_video_to_coachee"))->get()->toArray();
            $allow_per_video_check = isset($allow_per_video_details[0]['meta_data_value']) ? $allow_per_video_details[0]['meta_data_value'] : "0";

            $account_folder_details = AccountFolderDocument::where(array('site_id' => $this->site_id,
                        'document_id' => $video_id
                    ))->first();

            if ($account_folder_details) {
                $account_folder_details = $account_folder_details->toArray();
            }

            $user_huddle_level_permissions = $this->get_huddle_permissions($huddle_id, $user_id);

            $custom_markers = $this->getmarkertags($account_id, '1');

            if (!empty($account_folder_details['video_framework_id'])) {
                $video->framework_selected_for_video = '1';
            } else {
                $video->framework_selected_for_video = '0';
            }


            $account_custom_markers_permission = 0;
            $video_marker_tags = AccountFolderMetaData::where(array(
                        "account_folder_id" => $account_id,
                        "site_id" => $this->site_id,
                        "meta_data_name" => "enable_tags"
                    ))->first();

            if ($video_marker_tags) {
                $video_marker_tags = $video_marker_tags->toArray();
            }

            if (isset($video_marker_tags['meta_data_value']) && $video_marker_tags['meta_data_value'] == '1') {
                $account_custom_markers_permission = 1;
            } else {
                $account_custom_markers_permission = 0;
            }


            $video->rubric_check = $rubric_check;
            $video->allow_per_video = $allow_per_video_check;
            $video->huddle_permission = $user_huddle_level_permissions;
            $video->custom_markers = $custom_markers;
            $video->video_markers_check = $account_custom_markers_permission;
        }

        if ($video && $video->upload_progress == 100) {
            $video_detail = $this->getVideoDetails($video_id, '3');
            $video_url = $this->get_document_url($video_detail);
            $video->video_url = $video_url;
        }

        return response()->json($video);
    }

    function publish_observation(Request $request) {
        $video_id = $request->video_id;
        $account_id = $request->account_id;
        $huddle_id = $request->huddle_id;
        $user_current_account = $request->user_current_account;

        $observationPublisher = new \App\Services\ObservationPublisher;
        $final_result = $observationPublisher->publish_observation($this->site_id, $video_id, $account_id, $huddle_id, $user_current_account, $this->site_title);

        $res = array(
            'data' => $final_result,
            'huddle_id' => $huddle_id,
            'channel' => 'huddle-details-' . $huddle_id,
            'event' => 'publish_observation'
        );

        HelperFunctions::broadcastEvent($res);

        return $final_result;
    }

    function addScriptedObservation(Request $request) {
        $input = $request->all();
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $observation_title = 'observation_' . date('m-d-Y');
        $huddle_id = isset($input['huddle_id']) ? $input['huddle_id'] : '';

        $data = array(
            'account_id' => $account_id,
            'site_id' => $this->site_id,
            'doc_type' => '3',
            'url' => str_replace(' ', '_', $observation_title) . '.mp4',
            'original_file_name' => str_replace(' ', '_', $observation_title) . '.mp4',
            'active' => 1,
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'recorded_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $user_id,
            'published' => '0',
            'post_rubric_per_video' => '1'
        );
        if (DB::table('documents')->insert($data)) {
            $video_id = DB::getPdo()->lastInsertId();


            DB::table('document_files')->insert(array(
                'document_id' => $video_id,
                'site_id' => $this->site_id,
                'url' => "",
                'resolution' => 1080,
                'duration' => 0,
                'file_size' => 0,
                'default_web' => 1,
                'transcoding_job_id' => '1',
                'transcoding_status' => '0',
                'created_at'=>date('Y-m-d H:i:s')
            ));
            if (DB::table('account_folder_documents')->insert(array(
                        'account_folder_id' => $huddle_id,
                        'site_id' => $this->site_id,
                        'document_id' => $video_id,
                        'title' => $observation_title,
                        'zencoder_output_id' => 0,
                        'desc' => 'observation call notes'
                    ))) {

                $result = array(
                    'document_id' => $video_id,
                    'status' => 'success'
                );
                $channel_data = array(
                    'document_id' => $video_id,
                    'data' => $video_id,
                    'huddle_id' => $huddle_id,
                    'channel' => 'scripted-observation-' . $huddle_id,
                    'event' => "resource_deleted"
                );
                HelperFunctions::broadcastEvent($channel_data);

                return $result;
            } else {
                $result = array(
                    'document_id' => '',
                    'status' => 'failed'
                );

                return $result;
            }
        }
    }

    public function update_scripted_observation_duration(Request $request) {
        $input = $request->all();

        $video_id = $input['video_id'];

        $user_role = $this->get_user_role_name($input['account_role_id']);
        $in_trial_intercom = $this->check_if_account_in_trial_intercom($input['account_id']);

        $meta_data = array(
            "observation_completed" => 'true',
            "user_role" => $user_role,
            "is_in_trial" => $in_trial_intercom,
            'Platform' => 'Web'
        );

        $this->create_intercom_event('observation_completed', $meta_data, $input['current_user_email']);

        $data = array(
            'scripted_current_duration' => $input['duration'],
            'current_duration' => NULL,
            'site_id' => $this->site_id,
        );

        DB::table('documents')->where(['site_id' => $this->site_id, 'id' => $video_id])->update($data);

        return array(
            'success' => true,
        );
        $channel_data = array(
            'item_id' => $video_id,
            'data' => $video_id,
            'huddle_id' => $huddle_id,
            'channel' => 'scripted-observation-' . $huddle_id,
            'event' => "resource_deleted"
        );
        HelperFunctions::broadcastEvent($channel_data);
    }

    function publish_scripted_observation(Request $request) {
        $input = $request->all();

        $account_id = $input['account_id'];

        $huddle_id = $input['huddle_id'];

        $video_id = $input['video_id'];

        $user_id = $input['user_id'];

        $user_current_account = $input['user_current_account'];

        DB::table('documents')->where(array('id' => $video_id))->update(array('site_id' => $this->site_id, 'is_associated' => 1, 'published' => 1));

        $user_activity_logs = array(
            'ref_id' => $video_id,
            'site_id' => $this->site_id,
            'desc' => 'Scripted Notes',
            'url' => '/Huddles/observation_details_1/' . $huddle_id . '/' . $video_id,
            'account_folder_id' => $huddle_id,
            'date_added' => date("Y-m-d H:i:s"),
            'type' => '23',
            'environment_type' => '2'
        );
        $this->user_activity_logs($user_activity_logs, $account_id, $user_id);

        $url = '/Huddles/observation_details_1/' . $huddle_id . '/' . $video_id;
        $video = Document::where(array('site_id' => $this->site_id, 'id' => $video_id))->first();
        if ($video) {
            $video = $video->toArray();
        }

        $account_folder_document = AccountFolderDocument::where(array('site_id' => $this->site_id, 'document_id' => $video_id))->first();

        if ($account_folder_document) {
            $account_folder_document = $account_folder_document->toArray();
        }

        $account_folder = AccountFolder::where(array('site_id' => $this->site_id, 'account_folder_id' => $account_folder_document['account_folder_id']))->first();

        if ($account_folder) {
            $account_folder = $account_folder->toArray();
        }


        $creator = User::where(array('site_id' => $this->site_id, 'id' => $video['created_by']))->first();
        if ($creator) {
            $creator = $creator->toArray();
        }

        $huddleUsers = AccountFolder::getHuddleUsers($account_folder_document['account_folder_id'], $this->site_id);
        $is_video_published_now = true;


        if ($account_folder['folder_type'] != 1) {

        } else {
            // dd($huddleUsers);
            if (count($huddleUsers) > 0) {
                for ($i = 0; $i < count($huddleUsers); $i++) {
                    $huddleUser = $huddleUsers[$i];
                    if ($is_video_published_now && $huddleUser['role_id'] == 210) {

                        if (EmailUnsubscribers::check_subscription($huddleUser['id'], '8', $account_id, $this->site_id)) {
                            $this->sendVideoPublishedNotification_AfterPublish(
                                    $account_folder['name']
                                    , $account_folder['account_folder_id']
                                    , $video['id'], $creator, $account_folder['account_id'], $huddleUsers, $huddleUser['email'], $huddleUser['id'], $account_folder['folder_type'], $user_current_account, '8', $url);
                        }
                    }
                }
            }
        }

        return array(
            'success' => true,
            'message' => TranslationsManager::get_translation('myfile_scripted_observation', 'Api/myfile_video_details', $this->site_id)
        );
    }

    function sendVideoPublishedNotification_AfterPublish($huddle_name, $huddle_id, $video_id, $creator, $account_id, $huddleUsers, $recipient_email, $huddle_user_id, $huddle_type, $user_current_account, $email_type = '', $url_scripted = '') {
        // return response()->json(['r json']);

        if (!filter_var($recipient_email, FILTER_VALIDATE_EMAIL)) {
            return ['error' => 1, 'message' => TranslationsManager::get_translation('Invalid_Email', 'flash_messages', $this->site_id)];
        }
//        echo "<pre>";
        //return response()->json($creator);

        $reciver_detail = User::where(array('id' => $creator['id'], 'site_id' => $this->site_id))->first();


        $site_email_subject = Sites::get_site_settings('email_subject', $this->site_id);
        $user = $user_current_account;
        $first_name = $user['User']['first_name'];
        $last_name = $user['User']['last_name'];
        $company_name = $user['accounts']['company_name'];

        $account_info = Account::where('id', $account_id)->where('site_id', $this->site_id)->first()->toArray();
        $sibme_base_url = config('s3.sibme_base_url');

        $auth_token = md5($creator['id']);
        $from = $first_name . " " . $last_name . " " . $company_name . " <" . Sites::get_site_settings('static_emails', $this->site_id)['noreply'] . ">";
        $to = $recipient_email;
        //$to = 'hamidbscs8@gmail.com';
        if ($huddle_type == '2') {
            $subject = $account_info['company_name'] . " Video Library.";
        } else {
            $subject = $huddle_name . " You've got a new video to view.";
        }
        if ($reciver_detail['lang'] == 'en') {
            $subject = "$site_email_subject - Video Added";
        } else {
            $subject = "$site_email_subject - Video Añadido";
        }

        $h_type = AccountFolderMetaData::where('account_folder_id', $huddle_id)->where('meta_data_name', 'folder_type')->where('site_id', $this->site_id)->first()->toArray();
        $htype = isset($h_type['meta_data_value']) ? $h_type['meta_data_value'] : "1";

        if ($huddle_type == '1') {
            $huddle_name = $huddle_name;
            $url = "huddles/view/" . $huddle_id . "/1/" . $video_id;
        } elseif ($huddle_type == '2') {
            $huddle_name = $huddle_name;
            $url = "VideoLibrary/view/" . $huddle_id;
        } elseif ($huddle_type == '3') {
            $huddle_name = '' . $huddle_name;
            $url = "MyFiles/view/1/" . $huddle_id;
        } else {
            $huddle_type = '1';
            $huddle_name = 'Huddle: ' . $huddle_name;
            $url = "huddles/view/" . $huddle_id . "/1/" . $video_id;
        }

        if (!empty($url_scripted)) {
            $url = $url_scripted;
            //$subject = "Re: " . $huddle_name . " You've got a new scripted observation to view. ";
            $reciver_detail = User::where(array('id' => $creator['id'], 'site_id' => $this->site_id))->first();
            $lang_lang = $reciver_detail['lang'];
            if ($lang_lang == 'en') {
                $subject = $site_email_subject . " - Scripted Observation Published";
            } else {
                $subject = $site_email_subject . " -  Observación con Guión Publicados";
            }
        }
        //$obj = new \App\Services\GetSitesDetails;
        //$site_title = $obj->fetch_details($this->site_id, 'site_title');
        $site_title = Sites::get_site_title($this->site_id);
        $params = array(
            'huddle_name' => $huddle_name,
            'site_id' => $this->site_id,
            'account_name' => $account_info['company_name'],
            'huddle_id' => $huddle_id,
            'video_id' => $video_id,
            'creator' => $creator,
            'huddle_type' => $huddle_type,
            'participated_user' => $huddleUsers,
            'authentication_token' => $auth_token,
            'url' => $url,
            'htype' => $htype,
            'huddle_user_id' => $huddle_user_id,
            'email_type' => $email_type,
            'site_title' => $site_title
        );
        $html = '';
        if (!empty($url_scripted)) {
            //$email_view = "emails.observation." . $this->site_id . ".after_scriptedob_publish_email";
            // $redirect_url = $sibme_base_url . "/huddles/observation_details_1/" . $huddle_id . "/1/" . $video_id;
            $redirect_url = $sibme_base_url . "video_details/scripted_observations/" . $huddle_id . "/" . $video_id;
            $lang = $reciver_detail['lang'];
            $key = "after_scriptedob_publish_email_" . $this->site_id . "_" . $lang;
            $result = SendGridEmailManager::get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $creator['first_name'] . " " . $creator['last_name'], $html);
            $html = str_replace('{huddle_name}', $huddle_name, $html);
            $html = str_replace('{redirect_url}', $redirect_url, $html);
            $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
            $html = str_replace('{unsubcribe}', $sibme_base_url . '/subscription/unsubscirbe_now/' . $creator['id'] . '/6', $html);
        } else {
            $redirect_url = $sibme_base_url . "/huddles/view/" . $huddle_id . "/1/" . $video_id;
            $reciver_detail = User::where(array('id' => $creator['id'], 'site_id' => $this->site_id))->first();
            $lang = $reciver_detail['lang'];
            $key = "after_video_publish_email_" . $this->site_id . "_" . $lang;
            $result = SendGridEmailManager::get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $creator['first_name'] . " " . $creator['last_name'], $html);
            $html = str_replace('{huddle_name}', $huddle_name, $html);
            $html = str_replace('{redirect_url}', $redirect_url, $html);
            $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
            $html = str_replace('{unsubscribe}', $sibme_base_url . '/subscription/unsubscirbe_now/' . $creator['id'] . '/6', $html);
            // $email_view = "emails.observation." . $this->site_id . ".after_video_publish_email";
        }
        //$html = view($email_view, $params)->render();
        $auditEmail = array(
            'account_id' => $account_id,
            'site_id' => $this->site_id,
            'email_from' => $from,
            'email_to' => $creator['email'],
            'email_subject' => $subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $use_job_queue = config('s3.use_job_queue');

        if ($use_job_queue) {
            \DB::table('audit_emails')->insert($auditEmail);
            JobQueue::add_job_queue($this->site_id, 1, $to, $subject, $html, true);
            return ['error' => 0];
        } else {
            $response = Email::sendEmail($this->site_id, $email_view, $params, $to, '', $subject);
            if ($response !== true) {
                $auditEmail['error_msg'] = $response;
                \DB::table('audit_emails')->insert($auditEmail);
                return ['error' => 1,
                    'message' => TranslationsManager::get_translation('sorry_your_email_could_not_be_sent_right_now_please_try_again', 'flash_messages', $this->site_id),
                    'exception' => $response];
            } else {
                \DB::table('audit_emails')->insert($auditEmail);
                return ['error' => 0];
            }
        }
    }

    function get_framework_settings_by_id(Request $request) {


        $input = $request->all();

        $account_tag_id = $input['framework_id'];

        if (!empty($account_tag_id)):
            $framework_settings = array();

            $account_tag_details = array();

            $acc_tag_type_h = AccountTag::select('AccountTag.*')->where(array(
                        'AccountTag.site_id' => $this->site_id,
                        'AccountTag.framework_id' => $account_tag_id,
                            // 'AccountTag.tag_type' => 0,
                    ))->whereRaw('parent_account_tag_id IS NULL')->orderBy('standard_position')->get()->toArray();



            if (count($acc_tag_type_h) > 0) {
                foreach ($acc_tag_type_h as $act) {
                    $act['tag_html'] = $act['tag_title'];
                    $account_tag_details[] = $act;
                    $acc_tag_type_c = AccountTag::select('AccountTag.*')->where(array(
                                'AccountTag.site_id' => $this->site_id,
                                'AccountTag.framework_id' => $account_tag_id,
                                    // 'AccountTag.tag_type' => 0,
                            ))->whereRaw('parent_account_tag_id = ' . $act['account_tag_id'])->orderBy('standard_position')->get()->toArray();

                    if (count($acc_tag_type_c) > 0) {
                        foreach ($acc_tag_type_c as $act_c) {
                            $account_tag_details[] = $act_c;
                        }
                    }
                }
            } else {
                $acc_tag_type_0 = AccountTag::select('AccountTag.*')->where(array(
                            'AccountTag.site_id' => $this->site_id,
                            'AccountTag.framework_id' => $account_tag_id,
                            'AccountTag.tag_type' => 0
                        ))->orderBy('standard_position')->get()->toArray();
                if (count($acc_tag_type_0) > 0) {

                    foreach ($acc_tag_type_0 as $act_0) {
                        $account_tag_details[] = $act_0;
                    }
                }
            }



            $framework_settings['account_tag_type_0'] = $account_tag_details;


            $account_framework_settings = AccountFrameworkSetting::select('AccountFrameworkSetting.id', 'AccountFrameworkSetting.account_id', 'AccountFrameworkSetting.account_tag_id', 'AccountFrameworkSetting.published_at', 'AccountFrameworkSetting.updated_at', 'AccountFrameworkSetting.published', 'AccountFrameworkSetting.framework_name', 'AccountFrameworkSetting.enable_unique_desc', 'AccountFrameworkSetting.enable_ascending_order', 'AccountFrameworkSetting.enable_performance_level', 'AccountFrameworkSetting.tier_level', 'AccountFrameworkSetting.checkbox_level', 'AccountFrameworkSetting.parent_child_share')->where(array(
                        'AccountFrameworkSetting.site_id' => $this->site_id,
                        'AccountFrameworkSetting.account_tag_id' => $account_tag_id,
                    ))->first();

            if ($account_framework_settings) {
                $account_framework_settings = $account_framework_settings->toArray();
            }



            $framework_settings['account_framework_settings'] = $account_framework_settings;
            if (!empty($account_framework_settings)) {

                $account_framework_settings_performance_levels = AccountFrameworkSettingPerformanceLevel::select('AccountFrameworkSettingPerformanceLevel.*')->where(array(
                            'AccountFrameworkSettingPerformanceLevel.site_id' => $this->site_id,
                            'AccountFrameworkSettingPerformanceLevel.account_framework_setting_id' => $account_framework_settings['id']
                        ))->orderBy('performance_level_rating')->get()->toArray();


                if (!empty($framework_settings['account_tag_type_0'])) {
                    $single_type_0_array = array();
                    foreach ($framework_settings['account_tag_type_0'] as $single_acc_tag_type_0):
                        if ($single_acc_tag_type_0['standard_level'] == $account_framework_settings['checkbox_level']) {
                            $single_acc_tag_type_0['account_framework_settings_performance_levels'] = $account_framework_settings_performance_levels;
                        } else {
                            $single_acc_tag_type_0['account_framework_settings_performance_levels'] = array();
                        }
                        $single_type_0_array[] = $single_acc_tag_type_0;
                    endforeach;
                    $framework_settings['account_tag_type_0'] = $single_type_0_array;
                }
//                $framework_settings['account_framework_settings_performance_levels'] = $account_framework_settings_performance_levels;
                if ($account_framework_settings['enable_unique_desc'] == 1) {

                    $acc_tag_type_0_tag_ids = array();
                    if (!empty($acc_tag_type_0)) {
                        foreach ($acc_tag_type_0 as $acc_tag_type_0_single):
                            array_push($acc_tag_type_0_tag_ids, $acc_tag_type_0_single['account_tag_id']);
                        endforeach;
                    }
                    if (!empty($acc_tag_type_0_tag_ids)) {
                        $account_framework_settings_performance_levels_ids = array();
                        if (!empty($account_framework_settings_performance_levels)) {
                            foreach ($account_framework_settings_performance_levels as $single_performance_level):
                                array_push($account_framework_settings_performance_levels_ids, $single_performance_level['id']);
                            endforeach;
                        }

                        //$account_framework_settings_performance_levels_ids = implode(',', $account_framework_settings_performance_levels_ids);
                        $acc_tag_type_0_tag_ids = implode(',', $acc_tag_type_0_tag_ids);

                        $performance_level_descriptions = PerformanceLevelDescription::select('PerformanceLevelDescription.*')->where(array(
                                    'PerformanceLevelDescription.site_id' => $this->site_id,
                                ))->whereIn('PerformanceLevelDescription.performance_level_id',$account_framework_settings_performance_levels_ids )->whereRaw(
                                        'PerformanceLevelDescription.account_tag_id IN (' . $acc_tag_type_0_tag_ids . ')')->get()->toArray();

                        if (!empty($framework_settings['account_tag_type_0']) && !empty($performance_level_descriptions)) {
                            $single_type_0_array = array();
                            foreach ($framework_settings['account_tag_type_0'] as $single_acc_tag_type_0):
                                $performance_level_descriptions_arr = array();
                                foreach ($performance_level_descriptions as $single_performance_level_description):
                                    if ($single_acc_tag_type_0['standard_level'] == $account_framework_settings['checkbox_level'] && $single_acc_tag_type_0['account_tag_id'] == $single_performance_level_description['account_tag_id']) {
                                        array_push($performance_level_descriptions_arr, $single_performance_level_description);
                                    }
                                endforeach;
                                $single_acc_tag_type_0['performance_level_descriptions'] = $performance_level_descriptions_arr;
                                $single_type_0_array[] = $single_acc_tag_type_0;
                            endforeach;
                            $framework_settings['account_tag_type_0'] = $single_type_0_array;
                        }
//                        $framework_settings['performance_level_descriptions'] = $performance_level_descriptions;
                    }
                }
            }
            if (!empty($framework_settings)) {
                $result['status'] = true;
                $result['msg'] = "Account framework settings fetched successfully.";
                $result['data'] = $framework_settings;
            } else {
                $result['status'] = true;
                $result['msg'] = "Nothing found for your request.";
            }
        else:
            $result['status'] = false;
            $result['msg'] = "Please provide a valid account tag id.";
        endif;
        return response()->json($result);
    }

    function autoscroll_switch(Request $request) {
        $input = $request->all();
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $value = $input['value'];

        \DB::table('users_accounts')
                ->where('user_id', $user_id)
                ->where('site_id', $this->site_id)
                ->where('account_id', $account_id)
                ->update(['autoscroll_switch' => $value]);

        return response()->json(['success' => 'Autoscroll value is saved as = ' . $value]);
    }

    function select_video_framework(Request $request) {
        $input = $request->all();

        if(!isset( $input['huddle_id']) || !isset($input['video_id'])){
            return response()->json(['status' => false,'message'=>'You have missing Video Id or Huddle Id']);
        }
        $huddle_id = $input['huddle_id'];
        $video_id = $input['video_id'];
        $framework_id = $input['framework_id'];

        $account_folder_document_details = AccountFolderDocument::where(array(
                    'account_folder_id' => $huddle_id,
                    'site_id' => $this->site_id,
                    'document_id' => $video_id
                ))->first();

        if ($account_folder_document_details) {
            $account_folder_document_details = $account_folder_document_details->toArray();
        }

        if (!empty($account_folder_document_details['video_framework_id'])) {
            return response()->json(['status' => false, 'video_framework_id' => $account_folder_document_details['video_framework_id']]);
        }


        DB::table('account_folder_documents')->where(['site_id' => $this->site_id, 'document_id' => $video_id, 'account_folder_id' => $huddle_id])->update(['video_framework_id' => $framework_id, 'site_id' => $this->site_id]);
        $user_id  = isset($input['user_id'])?$input['user_id']:'0';
        //sw-5004
        $date = date("Y-m-d H:i:s");
        Document::where('id',$video_id)->update(['last_edit_date'=>$date, 'last_edit_by'=>$user_id]);
        
        $event = [
            'channel' => 'video-details-' . $video_id,
            'event' => "framework_selected",
            'huddle_id' => $huddle_id,
            'last_edit_date' => $date,
            'data' => $framework_id,
            'reference_id' => $video_id
        ];
        HelperFunctions::broadcastEvent($event);
        return response()->json(['status' => true]);
    }

    function send_comment_email($site_id, $account_folder_id, $account_id, $user_id, $video_id, $comment_id, $comment, $first_name, $last_name, $company_name, $image) {
        \Log::info(">>> Executing send_comment_email for <<<", [$site_id, $account_folder_id, $account_id, $user_id, $video_id, $comment_id, $comment, $first_name, $last_name, $company_name, $image]);
        $this->site_id = $site_id;
        $this->site_title = Sites::get_site_title($site_id);
        $huddle = AccountFolder::getHuddle($site_id, $account_folder_id, $account_id);
        $huddleUsers = AccountFolder::getHuddleUsers($account_folder_id, $site_id);
        $userGroups = AccountFolderGroup::getHuddleGroups($account_folder_id, $site_id);

        $huddle_user_ids = array();
        if ($userGroups && count($userGroups) > 0) {
            foreach ($userGroups as $row) {
                $huddle_user_ids[] = $row['user_id'];
            }
        }
        if ($huddleUsers && count($huddleUsers) > 0) {
            foreach ($huddleUsers as $row) {
                if ($row['is_active'] == 0) {
                    continue;
                }
                $huddle_user_ids[] = $row['user_id'];
            }
        }

        if (count($huddle_user_ids) > 0) {
            $user_ids = implode(',', $huddle_user_ids);
            if (!empty($user_ids))
                $huddleUserInfo = User::where(array('site_id' => $site_id))->whereRaw('id IN(' . $user_ids . ')')->get()->toArray();
        } else {
            $huddleUserInfo = array();
        }

        if ($huddleUserInfo && count($huddleUserInfo) > 0) {
            $commentsData = array(
                'account_folder_id' => $account_folder_id,
                'site_id' => $site_id,
                'huddle_name' => isset($huddle[0]['name']) ? $huddle[0]['name'] : '',
                'comment' => $comment,
                'video_link' => '/Huddles/view/' . $account_folder_id . '/1/' . $video_id . '/9',
                'participating_users' => $huddleUserInfo,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'company_name' => $company_name,
                'image' => $image,
                'user_id' => $user_id
            );
            if ($huddleUserInfo) {
                foreach ($huddleUserInfo as $row) {
                    \Log::info("Checking: ", [$user_id, $row['id']]);
                    if ($user_id == $row['id']) {
                        continue;
                    }
                    $commentsData['email'] = $row['email'];
                    $t = EmailUnsubscribers::check_subscription($row['id'], '6', $account_id, $site_id);
                    \Log::info("Checking Subscription: ", [$t, $row['id'], '6', $account_id, $site_id]);
                    if (EmailUnsubscribers::check_subscription($row['id'], '6', $account_id, $site_id)) {
                        $commentsData['comment_id'] = $comment_id;
                        $this->sendVideoComments($commentsData, $row['id']);
                    }
                }
            }
        }
    }

    function sendVideoComments($data, $user_id = '') {
        $from = $data['first_name'] . " " . $data['last_name'] . '  ' . $data['company_name'] . '<' . Sites::get_site_settings('static_emails', $this->site_id)['noreply'] . '>';
        $to = $data['email'];
        $site_email_subject = Sites::get_site_settings('email_subject', $this->site_id);

        $params = array(
            'data' => $data,
            'user_id' => $user_id,
            'site_title' => $this->site_title
        );
        $reciver_detail = User::where(array('id' => $data['user_id'], 'site_id' => $this->site_id))->first();
        $lang = $reciver_detail['lang'];
        if ($lang == 'en') {
            $subject = $site_email_subject . " - Comment Added";
        } else {
            $subject = $site_email_subject . " - Comentario añadido";
        }

        $key = "video_comments_" . $this->site_id . "_" . $lang;
        $sibme_base_url = config('s3.sibme_base_url');
        $redirect_url = $sibme_base_url . $data['video_link'];
        $result = SendGridEmailManager::get_send_grid_contents($key);
        if (!isset($result->versions[0]->html_content) || empty($result->versions[0]->html_content)) {
            return false;
        }
        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{sender}', $data['first_name'] . " " . $data['last_name'], $html);
        $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
        $html = str_replace('{redirect}', $sibme_base_url . $data['video_link'], $html);
        $html = str_replace('{unsubscribe}', $sibme_base_url . '/subscription/unsubscirbe_now/' . $user_id . '/6', $html);
        $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);

        //$html = view($this->site_id . ".video_comments", $params)->render();
        $reply_to = 'comment_' . $data['comment_id'] . config('s3.reply_to_email');


        $auditEmail = array(
            'account_id' => $data['account_folder_id'],
            'email_from' => $from,
            'email_to' => $data['email'],
            'email_subject' => $subject,
            'email_body' => $html,
            'is_html' => true,
            'site_id' => $this->site_id,
            'sent_date' => date("Y-m-d H:i:s")
        );



        if (!empty($data['email'])) {
            DB::table('audit_emails')->insert($auditEmail);
            $use_job_queue = config('s3.use_job_queue');
            if ($use_job_queue) {
                \Log::info(">>> Executing add_job_queue <<<", [$data['email'], $subject, $reply_to]);
                $this->add_job_queue(1, $data['email'], $subject, $html, true, $reply_to);
                return TRUE;
            }
        }

        return FALSE;
    }



    function EmailCopyVideo($site_id, $account_folder_id, $video_id = '', $doc_type, $account_id, $user_id, $first_name, $last_name, $company_name, $image, $is_scripted_note, $is_folder = 0) {

        if($is_folder)
        {
            return;
        }
        for ($i = 0; $i < count($account_folder_id); $i++) {

            $huddle = AccountFolder::getHuddle($site_id, $account_folder_id[$i], $account_id);
            $huddleUsers = AccountFolder::getHuddleUsers($account_folder_id[$i], $site_id);
            $userGroups = AccountFolderGroup::getHuddleGroups($account_folder_id[$i], $site_id);

            $huddle_user_ids = array();
            if ($userGroups && count($userGroups) > 0) {
                foreach ($userGroups as $row) {
                    $huddle_user_ids[] = $row['user_id'];
                }
            }
            if ($huddleUsers && count($huddleUsers) > 0) {
                foreach ($huddleUsers as $row) {
                    if ($row['is_active'] == 0) {
                        continue;
                    }
                    $huddle_user_ids[] = $row['user_id'];
                }
            }

            if (count($huddle_user_ids) > 0) {
                $user_ids = implode(',', $huddle_user_ids);
                if (!empty($user_ids))
                    $huddleUserInfo = User::whereRaw('id IN(' . $user_ids . ')')->where('site_id', $site_id)->get()->toArray();
            } else {
                $huddleUserInfo = array();
            }

            if ($huddleUserInfo && count($huddleUserInfo) > 0) {
                $commentsData = array(
                    'account_folder_id' => $account_folder_id[$i],
                    'site_id' => $site_id,
                    'huddle_name' => isset($huddle[0]['name'])?$huddle[0]['name']:'',
                    'video_link' => $doc_type == 2 ? '/Huddles/view/' . $account_folder_id[$i] . '/2/' : !$is_scripted_note ? '/Huddles/view/' . $account_folder_id[$i] . '/1/' . $video_id : 'home/video_details/scripted_observations/' . $account_folder_id[$i] . '/' . $video_id,
                    'participating_users' => $huddleUserInfo,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'company_name' => $company_name,
                    'image' => $image,
                    'user_id' => $user_id
                );
                foreach ($huddleUserInfo as $row) {
                    if ($user_id == $row['id'] && $doc_type!=5) {
                        continue;
                    }
                    $commentsData ['email'] = $row['email'];
                   // var_dump(EmailUnsubscribers::check_subscription($row['id'], '7', $account_id, $site_id));
                   if($is_scripted_note ){
                    if (EmailUnsubscribers::check_subscription($row['id'], '11', $account_id, $site_id)) {
                        $this->sendCopyVideoEmail($site_id, $commentsData, $row['id'],$is_scripted_note);
                    }
                   }else{
                    if (EmailUnsubscribers::check_subscription($row['id'], '7', $account_id, $site_id)) {
                        if($doc_type==2){                            
                            $documentUploader = new \App\Services\DocumentUploader();
                            $documentUploader->sendDocumentEmail($commentsData,$user_id,$account_id,$site_id);
                        } elseif($doc_type==5) {
                            $commentsData ['huddle_url'] = "/Huddles/view/".$huddle[0]['account_folder_id'];
                            $commentsData ['user_id'] = $row['id'];
                            $urlUploader = new \App\Services\URLUploader();
                            $urlUploader->sendUrlEmail($commentsData,$user_id,$account_id,$site_id,"shared");
                        } else {
                            $this->sendCopyVideoEmail($site_id, $commentsData, $row['id'],$is_scripted_note);
                        }
                    }
                   }
                    
                }
            }
        }
    }

    function sendCopyVideoEmail($site_id, $data, $user_id = '', $is_scripted_note = 0) {
        $from = $data['first_name'] . " " . $data['last_name'] . '  ' . $data['company_name'] . '<' . Sites::get_site_settings('static_emails', $site_id)['noreply'] . '>';

        $h_type = AccountFolderMetaData::where('account_folder_id', $data['account_folder_id'])->where('meta_data_name', 'folder_type')->where('site_id', $site_id)->first();
        if (!empty($h_type)) {
            $h_type = $h_type->toArray();
            $htype = isset($h_type['meta_data_value']) ? $h_type['meta_data_value'] : "1";
        } else {
            $htype = "1";
        }

        $users = \DB::table("users")->where('id', $data['user_id'])->first();
        $sibme_base_url = config('s3.sibme_base_url');
        $lang_id = $users->lang;
        if (!$is_scripted_note && $lang_id == 'en') {
            $subject = Sites::get_site_settings('email_subject', $site_id) . ' - Video Added';
        } else if (!$is_scripted_note && $lang_id != 'en') {
            $subject = Sites::get_site_settings('email_subject', $site_id) . ' - Video Añadido';
        } else if ($is_scripted_note && $lang_id == 'en') {
            $subject = Sites::get_site_settings('email_subject', $site_id) . ' - Scripted Notes Added';
        } else if ($is_scripted_note && $lang_id != 'en') {
            $subject = Sites::get_site_settings('email_subject', $site_id) . ' - Notas con gui�n Añadido';
        }
        $copiedObject = $is_scripted_note ? "scripted_notes_copy_" : "video_copy_";
        $key = $copiedObject . $site_id . "_" . $lang_id;
        $result = SendGridEmailManager::get_send_grid_contents($key);
        if (!empty($result->versions) && !empty($result->versions[0]->html_content)) {
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $users->first_name . ' ' . $users->last_name, $html);
            $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
            $html = str_replace('{redirect}', $sibme_base_url . $data['video_link'], $html);
            if ($is_scripted_note) {
                $html = str_replace('{unsubscribe}', $sibme_base_url . 'subscription/unsubscirbe_now/' . $user_id . '/11', $html);
            } else {
                $html = str_replace('{unsubscribe}', $sibme_base_url . 'subscription/unsubscirbe_now/' . $user_id . '/7', $html);
            }

            $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);

            $auditEmail = array(
                'account_id' => $data['account_folder_id'],
                'site_id' => $site_id,
                'email_from' => $from,
                'email_to' => $data['email'],
                'email_subject' => $subject,
                'email_body' => $html,
                'is_html' => true,
                'sent_date' => date("Y-m-d H:i:s")
            );
            if (!empty($data['email'])) {
                DB::table('audit_emails')->insert($auditEmail);
                $use_job_queue = config('s3.use_job_queue');
                if ($use_job_queue) {
                    JobQueue::add_job_queue($site_id, 1, $data['email'], $subject, $html, true);
                    return TRUE;
                }
            }
        }
        return false;
    }

    function sendCopyVideoEmail1($data, $user_id = '') {
        $site_email_subject = Sites::get_site_settings('email_subject', $this->site_id);

        $from = $data['first_name'] . " " . $data['last_name'] . '  ' . $data['company_name'] . '<' . Sites::get_site_settings('static_emails', $this->site_id)['noreply'] . '>';

        //$subject = "Re:[Huddle] " . $data['huddle_name'] . ": You've got a new video to view";
        $subject = "$site_email_subject - Video Added";

        $h_type = AccountFolderMetaData::where('account_folder_id', $data['account_folder_id'])->where('meta_data_name', 'folder_type')->where('site_id', $this->site_id)->first();
        if (!empty($h_type)) {
            $h_type = $h_type->toArray();
            $htype = isset($h_type['meta_data_value']) ? $h_type['meta_data_value'] : "1";
        } else {
            $htype = "1";
        }


        $params = array(
            'data' => $data,
            'site_id' => $this->site_id,
            'user_id' => $user_id,
            'htype' => $htype
        );

        $html = view('video_copy', $params)->render();

        $auditEmail = array(
            'account_id' => $data['account_folder_id'],
            'site_id' => $this->site_id,
            'email_from' => $from,
            'email_to' => $data['email'],
            'email_subject' => $subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($data['email'])) {
            DB::table('audit_emails')->insert($auditEmail);
            $use_job_queue = config('s3.use_job_queue');
            if ($use_job_queue) {
                $this->add_job_queue(1, $data['email'], $subject, $html, true);
                return TRUE;
            }
        }




        return FALSE;
    }

    function get_user_role_name($role_id) {

        $user_role = '';
        if ($role_id == '100') {
            $user_role = 'Account Owner';
        }
        if ($role_id == '110') {
            $user_role = 'Super Admin';
        }
        if ($role_id == '115') {
            $user_role = 'Admin';
        }
        if ($role_id == '120') {
            $user_role = 'User';
        }

        if ($role_id == '125') {
            $user_role = 'Viewer';
        }

        return $user_role;
    }

    function check_if_account_in_trial_intercom($account_id) {
        $result = Account::where(array('id' => $account_id, 'site_id' => $this->site_id))->first();

        if ($result) {
            $result = $result->toArray();
        }

        $value = "";

        if (isset($result['in_trial']) && $result['in_trial'] == '1') {
            $value = "True";
        } else {
            $value = "False";
        }

        return $value;
    }

    function delete_video_attachments($video_id, $input = '', $user_id) {

        $video_attachments = AccountFolderdocumentAttachment::where('attach_id', $video_id)->where('site_id', $this->site_id)->get();

        if ($video_attachments) {
            $video_attachments = $video_attachments->toArray();

            foreach ($video_attachments as $video_attachment) {
                $account_folder_document = AccountFolderDocument::where(array(
                            'id' => $video_attachment['account_folder_document_id'],
                            'site_id' => $this->site_id
                        ))->first();

                $deleted_video_data = Document::where(array(
                            'site_id' => $this->site_id,
                            'id' => $account_folder_document['document_id']
                        ))->first();


                if ($account_folder_document) {
                    $account_folder_document = $account_folder_document->toArray();

                    $audit_deletion_log = array(
                        'entity_id' => $video_id,
                        'deleted_entity_id' => $account_folder_document['document_id'],
                        'entity_name' => $account_folder_document['title'],
                        'entity_data' => json_encode($deleted_video_data),
                        'created_by' => $user_id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'site_id' => $this->site_id,
                        'ip_address' => $_SERVER['REMOTE_ADDR'],
                        'posted_data' => 'Attached Document with Deleted Video in entity_id',
                        'action' => $_SERVER['REQUEST_URI'],
                        'user_agenet' => $_SERVER['HTTP_USER_AGENT'],
                        'request_url' => $_SERVER['REQUEST_URI'],
                        'platform' => '',
                        'created_date' => date("Y-m-d H:i:s"),
                    );
                    DB::table('audit_deletion_log')->insert($audit_deletion_log);

                    DB::table('documents')->where(array('site_id' => $this->site_id, 'id' => $account_folder_document['document_id']))->delete();
                    DB::table('account_folder_documents')->where(array('site_id' => $this->site_id, 'document_id' => $account_folder_document['document_id']))->delete();
                }
            }


            DB::table('account_folderdocument_attachments')->where(array('site_id' => $this->site_id, 'attach_id' => $video_id))->delete();
        }
    }

    function get_account_video_permissions($account_id) {

        $check_data = AccountMetaData::where(array(
                    "account_id" => $account_id,
                    'site_id' => $this->site_id,
                    "meta_data_name" => "enable_video_library"
                ))->first();

        if ($check_data) {
            $check_data = $check_data->toArray();
            if (isset($check_data['meta_data_value'])) {
                if ($check_data['meta_data_value'] == '1') {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function create_intercom_event($event_name, $meta_data, $loggedInUserEmail) {
        try {

            if ($this->site_id == '2') {
                $client = new IntercomClient(config('general.intercom_access_token_hmh'), null);
            } else {
                $client = new IntercomClient(config('general.intercom_access_token'), null);
            }

            $client->events->create([
                "event_name" => $event_name,
                "created_at" => time(),
                "email" => $loggedInUserEmail,
                "metadata" => $meta_data
            ]);
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $status_code = $response->getStatusCode();
            return $status_code;
        }
    }

    function create_churnzero_event($event_name, $account_id, $user_email, $value = 1) {
        if(strpos($user_email,"@sibme.com") || strpos($user_email,"@jjtestsite.us")   )
        {
        return true;
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://analytics.churnzero.net/i?appKey=invw-q7Ivjwby8NI1F6qQcH1Gix0811ja7-Li4_1xWg&accountExternalId=' . $account_id . '&contactExternalId=' . $user_email . '&action=trackEvent&eventName=' . $event_name . '&description=' . $event_name . '&quantity=' . $value);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    function get_security(Request $request) {
        $data = $request->all();
        $huddle_id = $data['huddle_id'];
        $request_type = $data['request_type'];
        $vidoes_object = $data['vidoes_obj'];
        $result = HelperFunctions::get_security_permissions($huddle_id, $request_type, $vidoes_object);
    }

    function get_single_video(Request $request) {
        $video_id = $request->get("document_id");
        $folder_type = $request->has("folder_type") ? $request->get("folder_type") : 3;
        $huddle_id = $request->get("account_folder_id");
        $user_id = $request->get("user_id");

        $with_comments = $request->has("with_comments") ? $request->get("with_comments") : 0;
        $support_audio_annotation = $request->has('support_audio_annotation') ? $request->get('support_audio_annotation') : 1;
        $default_photo = $request->has('use_default_photo') ? $request->get('use_default_photo') : 1;
        $get_all_comments = $request->has('get_all_comments') ? $request->get('get_all_comments') : 1;
        $with_data = 1;
        if ($with_comments) {
            $with_data = 0;
        }

        $video_detail = $this->getVideoDetails($video_id);

        if (isset($video_detail['url'])) {
            $videoFilePath = pathinfo($video_detail['url']);
        } else {
            $videoFilePath = "";
        }

        if (isset($videoFilePath['extension'])) {
            $video_detail['file_type'] = $videoFilePath['extension'];
        }

        $documentfiles = DocumentFiles::where(array(
                    "document_id" => $video_id
                ))->first();

        if ($documentfiles) {
            $documentfiles->toArray();
            $video_detail['transcoding_status'] = $documentfiles['transcoding_status'];
        }

        $video_url = $this->get_document_url($video_detail);

        $coachee_permission = AccountFolderMetaData::where(array('account_folder_id' => $huddle_id, "meta_data_name" => "coachee_permission", 'site_id' => $this->site_id))->get()->toArray();
        $coachee_permissions = isset($coachee_permission[0]['meta_data_value']) ? $coachee_permission[0]['meta_data_value'] : "0";

        $item = Document::get_single_video_data($this->site_id, $video_id, $folder_type, $with_data, $huddle_id, $user_id);

        if (count($item) > 0 && $with_comments) {
            $htype = app('App\Http\Controllers\HuddleController')->get_huddle_type($huddle_id);
            for ($j = 0; $j < count($item); $j++) {
                $item[$j]['total_comments'] = HelperFunctions::get_video_comment_numbers($item[$j]['doc_id'], $huddle_id, $user_id);
                //$item[$j]['total_comments'] = Comment::comments_count($item[$j]['doc_id'], $this->site_id);
                //$item[$j]['total_attachment'] = Document::getDocumentsCount($item[$j]['doc_id'], $this->site_id);
                $if_evaluator = HelperFunctions::check_if_evalutor($huddle_id, $user_id);
                $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);
                $is_coach_enable = HelperFunctions::is_enabled_coach_feedback($huddle_id);
                $item[$j]['total_attachment'] = count(Document::getVideoDocumentsByVideo($item[$j]['doc_id'], $huddle_id, $user_id, $h_type, $if_evaluator, $is_coach_enable, $this->site_id));
                if ($h_type == 3) {
                    $assessor_ids = AccountFolderUser::get_participants_ids($huddle_id, 200, $this->site_id);
                    $item[$j]['assessed'] = app('App\Http\Controllers\HuddleController')->check_video_assessed($item[$j]['doc_id'], $assessor_ids);
                }
                if (!AccountFolderUser::check_if_evalutor($this->site_id, $huddle_id, $user_id) && (($htype == '2' && $this->is_enabled_coach_feedback($huddle_id, $this->site_id)) || $htype == '3')) {
                    $comments_res = $this->getVideoComments($item[$j]['doc_id'], '', '', '', '', $support_audio_annotation, 1, false, '', true);
                } else {
                    $comments_res = $this->getVideoComments($item[$j]['doc_id'], '', '', '', '', $support_audio_annotation, $get_all_comments);
                }

                $comment_result = array();

                foreach ($comments_res as $row) {
                    if ($row['ref_type'] == 6) {
                        if (config('use_cloudfront') == true) {
                            $url = $row['comment'];
                            $videoFilePath = pathinfo($url);
                            $videoFileName = $videoFilePath['filename'];
                            if ($videoFilePath['dirname'] == ".") {

                                $video_path = $this->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a"));
                                $relative_video_path = $videoFileName . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a");
                            } else {

                                $video_path = $this->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a"));
                                $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a");
                            }
                        } else {
                            $url = $row['comment'];
                            $videoFilePath = pathinfo($url);
                            $videoFileName = $videoFilePath['filename'];

                            if ($videoFilePath['dirname'] == ".") {

                                $video_path = $this->getSecureAmazonUrl($videoFileName . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a"));
                                $relative_video_path = $videoFileName . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a");
                            } else {

                                $video_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . $videoFileName . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a"));
                                $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . "." . (isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a");
                            }
                        }
                        $row['comment'] = $video_path;
                    }
                    $created_date = new DateTime($row['created_date']);
                    $last_edit_date = new DateTime($row['last_edit_date']);

                    $row['last_edit_date'] = $last_edit_date->format(DateTime::W3C);
                    $row['created_date'] = $created_date->format(DateTime::W3C);

                    if (isset($row['image']) && $row['image'] != '') {
                        $use_folder_imgs = config('use_local_file_store');
                        if ($use_folder_imgs == false) {
                            $file_name = "sibme.com/static/users/" . $row['created_by'] . "/" . $row['image'];
                            $row['image'] = config('s3.amazon_base_url') . config('bucket_name_cdn') . "/" . $file_name;
                        }

                        //$item['User']['image'] = $sibme_base_url . '/img/users/' . $item['User']['image'];
                        //else
                        //$item['User']['image'] = $view->Custom->getSecureSibmecdnUrl("static/users/" . $item['User']['id'] . "/" . $item['User']['image']);
                    } else {
                        if ($default_photo == 1) {
                            $row['image'] = config('s3.sibme_base_url') . '/img/home/photo-default.png';
                        }
                    }

                    $commentDate = $row['created_date'];
                    $currentDate = date('Y-m-d H:i:s');

                    $start_date = new DateTime($commentDate);
                    $since_start = $start_date->diff(new DateTime($currentDate));

//                    $commentsDate = '';
//                    if ($since_start->y > 0) {
//                        $commentsDate = "About " . $since_start->y . ($since_start->y > 1 ? ' Years Ago' : ' Year Ago');
//                    } elseif ($since_start->m > 0) {
//                        $commentsDate = "About " . $since_start->m . ($since_start->m > 1 ? ' Months Ago' : ' Month Ago');
//                    } elseif ($since_start->d > 0) {
//                        $commentsDate = $since_start->d . ($since_start->d > 1 ? ' Days Ago' : ' Day Ago');
//                    } elseif ($since_start->h > 0) {
//                        $commentsDate = $since_start->h . ($since_start->h > 1 ? ' Hours Ago' : ' Hour Ago');
//                    } elseif ($since_start->i > 0) {
//                        $commentsDate = $since_start->i . ($since_start->i > 1 ? ' Minutes Ago' : ' Minute Ago');
//                    } elseif ($since_start->s > 0) {
//                        $commentsDate = 'Less Than A Minute Ago';
//                    } else {
//                        $commentsDate = 'Just Now';
//                    }

                    $row['Comment']['created_date_string'] = $this->times_ago($row['created_date']);

                    $response = $this->get_replies_from_comment($row, $get_all_comments);
                    $row['attachment_count'] = app('App\Http\Controllers\HuddleController')->match_timestamp_count_comment($row['id'], $item[$j]['doc_id']);
                    $row['attachment_names'] = app('App\Http\Controllers\HuddleController')->get_comment_attachment_file_names($row['id'], $item[$j]['doc_id']);
                    $row['Comment'] = $response;
                    $get_standard = $this->gettagsbycommentid($row['id'], array('0')); //get standards
                    $get_tags = $this->gettagsbycommentid($row['id'], array('1', '2')); //get tags
                    $row['standard'] = $get_standard;
                    $row['default_tags'] = $get_tags;

                    $comment_result[] = $row;
                }
                //echo "<pre>";

                $item[$j]['comments'] = $comment_result;
                $item[$j]['static_url'] = $video_url['url'];
            }
        }

        if ($request->has("account_id")) {
            $account_id = $request->get("account_id");
            $is_enabled_workspace_framework = HelperFunctions::is_enabled_workspace_framework($account_id, $this->site_id);
            $is_enabled_workspace_custom_marker = HelperFunctions::is_enabled_workspace_custom_marker($account_id, $this->site_id);
            if ($is_enabled_workspace_framework) {
                $is_enabled_workspace_framework = '1';
            } else {
                $is_enabled_workspace_framework = '0';
            }

            if ($is_enabled_workspace_custom_marker) {
                $is_enabled_workspace_custom_marker = '1';
            } else {
                $is_enabled_workspace_custom_marker = '0';
            }

            return ["status" => true, "data" => $item, "is_enabled_workspace_framework" => $is_enabled_workspace_framework, 'is_enabled_workspace_custom_marker' => $is_enabled_workspace_custom_marker, 'coachee_permissions' => $coachee_permissions];
        } else {
            return ["status" => true, "data" => $item];
        }
    }

    function copy_discussion_attachments(Request $request) {
        $input = $request->all();
        $document_id = $input['document_id'];
        $user_id = $input['user_id'];

        $account_folder_id = isset($input['account_folder_id']) ? $input['account_folder_id'] : '';

        $document = Document::get_document_row($document_id, $this->site_id);


        foreach ($account_folder_id as $id) {

            $docs_data = array('id' => null, 'created_date' => date("Y-m-d H:i:s"), 'last_edit_date' => date("Y-m-d H:i:s"), 'recorded_date' => date("Y-m-d H:i:s"), 'created_by' => $user_id, 'last_edit_by' => $user_id, 'doc_type' => '2') + $document;

            DB::table('documents')->insert($docs_data);
            $document_latest_id = DB::getPdo()->lastInsertId();

            $copyRecord = array(
                'account_folder_id' => $id,
                'document_id' => $document_latest_id,
                'is_viewed' => '0',
                'title' => $document['original_file_name'],
                'desc' => '',
                'zencoder_output_id' => '',
                'site_id' => $this->site_id
            );

            DB::table('account_folder_documents')->insert($copyRecord);
            //Socket Implementation    
            $folder_type = AccountFolder::get_folder_type($id, $this->site_id);
            $resp = Document::get_single_video_data($this->site_id, $document_latest_id, $folder_type);
            $data_ws = null;
            if ($resp) {
                $data_ws = app('App\Http\Controllers\WorkSpaceController')->conversion_to_thmubs($resp[0]);
            }

            $channel = 'huddle-details-' . $id;

            $reference_id = $document_latest_id;

            $data_ws["comment_id"] = '0';
            $res = array(
                'document_id' => $document_latest_id,
                'reference_id' => $reference_id,
                'data' => $data_ws,
                'url' => $document['url'],
                'huddle_id' => $id,
                'channel' => $channel,
                'event' => 'resource_added',
                'allowed_participants' => [],
                'assessment_sample' => '0'
            );

            HelperFunctions::broadcastEvent($res);
        }

        $result['message'] = "Attachment Shared Successfully";
        $result['success'] = true;


        return response()->json($result);
    }

    function start_scripted_notes(Request $request) {
        $input = $request->all();
        $result = array();
        $video_id = $input['video_id'];
        $user_id = $input['user_id'];
        $account_id = $input['account_id'];
        $huddle_id = $input['huddle_id'];

        DB::table('documents')->where(array('id' => $video_id))->update(array('is_scripted_note_start' => 1,'published'=>1));

        $result['message'] = "Scripted Note Started Successfully";
        $result['success'] = true;

        $doc_res = Document::get_single_video_data($this->site_id, $video_id, 3, 1, $huddle_id, $user_id);
        $event = [
            'channel' => 'workspace-' . $account_id . "-" . $user_id,
            'event' => "resource_added",
            'data' => $doc_res,
            'video_file_name' => $doc_res["title"],
            'user_id' => $user_id
        ];
        HelperFunctions::broadcastEvent($event, "broadcast_event", false);
        return response()->json($result);
    }

    function get_counts_of_comment_tags($account_id, $user_id) {


        $type_of_account = 'Individual';
        $account_detail = Account::where(array(
                    "id" => $account_id))->first()->toArray();

        $number_of_child_account = Account::where(array(
                    "parent_account_id" => $account_id))->count();

        if (!empty($account_detail['parent_account_id']) && $account_detail['parent_account_id'] != '0') {
            $type_of_account = 'Child Account';
        } else {
            $type_of_account = 'Individual';
        }


        if ($number_of_child_account > 0) {
            $type_of_account = 'Parent Account';
        } else {
            $type_of_account = 'Individual';
        }










        // Can't use find() here because it adds site_id in where clause which we don't need here. The labels are same for both HMH and WL.
//         $avg_rating = $db->query("SELECT ROUND(AVG(rating_value),2) AS avg_rating FROM `document_standard_ratings` WHERE account_id = $account_id AND user_id = $user_id");
//         
//            
//         $custom_markers = $db->query("SELECT 
//                        COUNT(*) AS custom_markers 
//                      FROM
//                        `account_comment_tags` act 
//                        JOIN documents d 
//                          ON act.`ref_id` = d.`id` 
//                      WHERE act.`created_by` = $user_id 
//                        AND act.`ref_type` IN (2)
//                        AND d.`account_id` = $account_id");
// 
//         
//         
//         $tagged_standards = $db->query("SELECT 
//                        COUNT(*) AS tagged_standards 
//                      FROM
//                        `account_comment_tags` act 
//                        JOIN documents d 
//                          ON act.`ref_id` = d.`id` 
//                      WHERE act.`created_by` = $user_id 
//                        AND act.`ref_type` IN (0)
//                        AND d.`account_id` = $account_id");
//         $total_super_admins = $db->query("SELECT COUNT(*) AS total_super_admins FROM `users_accounts` WHERE role_id = 110 AND account_id = $account_id");
//         
//         $total_admins = $db->query("SELECT COUNT(*) AS total_admins FROM `users_accounts` WHERE role_id = 115 AND account_id = $account_id");
//         
//         $total_users = $db->query("SELECT COUNT(*) AS total_users FROM `users_accounts` WHERE role_id = 120 AND account_id = $account_id");
//         
//         $total_viewers = $db->query("SELECT COUNT(*) AS total_viewers FROM `users_accounts` WHERE role_id = 125 AND account_id = $account_id");
//         
        // $number_of_frameworks = $db->query("SELECT COUNT(*) AS number_of_frameworks FROM `account_tags` WHERE account_id = $account_id AND tag_type = 2 ");

        $coaching_perfomance_level = app('db')->select("SELECT meta_data_value AS coaching_perfomance_level FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'coaching_perfomance_level'");
        $coaching_perfomance_level = json_decode(json_encode($coaching_perfomance_level), True);

        $assessment_perfomance_level = app('db')->select("SELECT meta_data_value AS assessment_perfomance_level FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'assessment_perfomance_level'");
        $assessment_perfomance_level = json_decode(json_encode($assessment_perfomance_level), True);

        $account_framework_setting = app('db')->select("SELECT meta_data_value AS account_framework_setting FROM `account_folders_meta_data` WHERE account_folder_id = $account_id AND meta_data_name = 'enable_framework_standard'");
        $account_framework_setting = json_decode(json_encode($account_framework_setting), True);

        $account_custom_marker_setting = app('db')->select("SELECT meta_data_value AS account_custom_marker_setting FROM `account_folders_meta_data` WHERE account_folder_id = $account_id AND meta_data_name = 'enable_tags'");
        $account_custom_marker_setting = json_decode(json_encode($account_custom_marker_setting), True);

        $assessment_tracker = app('db')->select("SELECT meta_data_value AS assessment_tracker FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'enable_matric'");
        $assessment_tracker = json_decode(json_encode($assessment_tracker), True);

        $coaching_tracker = app('db')->select("SELECT meta_data_value AS coaching_tracker FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'enable_tracker'");
        $coaching_tracker = json_decode(json_encode($coaching_tracker), True);

        $student_payment = app('db')->select("SELECT amount AS payment_amount , `type` AS payment_type FROM `account_student_payments` WHERE account_id = $account_id");
        $student_payment = json_decode(json_encode($student_payment), True);

        $view = new View($this, false);
        $user_limit = HelperFunctions::get_allowed_users($account_id);

        $enable_video_library = app('db')->select("SELECT meta_data_value AS enable_video_library FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'enable_video_library'");
        $enable_video_library = json_decode(json_encode($enable_video_library), True);

        $number_of_child_accounts = app('db')->select("SELECT COUNT(*) AS number_of_child_accounts FROM accounts WHERE parent_account_id = $account_id");
        $number_of_child_accounts = json_decode(json_encode($number_of_child_accounts), True);

        $tracking_duration = app('db')->select("SELECT meta_data_value AS tracking_duration FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'tracking_duration'");
        $tracking_duration = json_decode(json_encode($tracking_duration), True);

        $account_payments = AccountStudentPayments::where(array(
                    "account_id" => $account_id,
                ))->get()->toArray();

        $payment_type = "";
        $payment_amount = "";

        foreach ($account_payments as $key => $pay) {
            if (count($account_payments) - 1 > $key) {
                $payment_type .= $pay['type'] . '|';
                $payment_amount .= $pay['amount'] . '|';
            } else {
                $payment_type .= $pay['type'];
                $payment_amount .= $pay['amount'];
            }
        }
        $data = array(
            //    'custom_markers' => $custom_markers[0][0]['custom_markers'],
            //     'tagged_standards' => $tagged_standards[0][0]['tagged_standards'],
            //     'avg_rating' => $avg_rating[0][0]['avg_rating'],
            // 'total_super_admins' => $total_super_admins[0][0]['total_super_admins'],
            // 'total_admins' => $total_admins[0][0]['total_admins'],
            // 'total_users' =>  $total_users[0][0]['total_users'],
            // 'total_viewers' =>  $total_viewers[0][0]['total_viewers'],
            // 'number_of_frameworks' => $number_of_frameworks[0][0]['number_of_frameworks'],
            'assessment_perfomance_level' => (isset($assessment_perfomance_level[0]['assessment_perfomance_level']) ? $assessment_perfomance_level[0]['assessment_perfomance_level'] : "0" ),
            'coaching_perfomance_level' => (isset($coaching_perfomance_level[0]['coaching_perfomance_level']) ? $coaching_perfomance_level[0]['coaching_perfomance_level'] : "0"),
            'account_framework_setting' => (isset($account_framework_setting[0]['account_framework_setting']) ? $account_framework_setting[0]['account_framework_setting'] : "0"),
            'account_custom_marker_setting' => (isset($account_custom_marker_setting[0]['account_custom_marker_setting']) ? $account_custom_marker_setting[0]['account_custom_marker_setting'] : "0"),
            'account_detail' => $account_detail,
            'assessment_tracker' => (isset($assessment_tracker[0]['assessment_tracker']) ? $assessment_tracker[0]['assessment_tracker'] : "0"),
            'coaching_tracker' => (isset($coaching_tracker[0]['coaching_tracker']) ? $coaching_tracker[0]['coaching_tracker'] : "0"),
            'enable_video_library' => (isset($enable_video_library[0]['enable_video_library']) ? $enable_video_library[0]['enable_video_library'] : "0"),
            'number_of_child_accounts' => (isset($number_of_child_accounts[0]['number_of_child_accounts']) ? $number_of_child_accounts[0]['number_of_child_accounts'] : "0"),
            'tracking_duration' => (isset($tracking_duration[0]['tracking_duration']) ? $tracking_duration[0]['tracking_duration'] : "0"),
            'payment_amount' => $payment_amount,
            'payment_type' => $payment_type,
            'type_of_account' => $type_of_account,
            'user_limit' => $user_limit
        );


        return $data;
    }

    function getNextAvailableSlot($user_id, $account_folder_id, $doc_type) {
        $occupied_slots = AccountFolderDocument::join('documents', 'account_folder_documents.document_id', '=', 'documents.id')
                ->where('documents.doc_type', $doc_type)
                ->where('account_folder_id', $account_folder_id)
                ->where('created_by', $user_id)
                ->pluck('slot_index')
                ->toArray();

        $missing_slot = 0;
        if (!empty($occupied_slots)) {
            $range = range($occupied_slots[0], max($occupied_slots));
            // use array_diff to get the missing elements 
            $missing_slots = array_diff($range, $occupied_slots);
            if (isset($missing_slots[0]) && !empty($missing_slots[0])) {
                $missing_slot = $missing_slots[0];
            } else {
                $missing_slot = 0;
            }
        }
        return $missing_slot;
    }

    function copy_video_attachments($src_video_id, $dest_video_id, $is_duplicated, $workspace, $huddle_id, $account_id = '') {
        $video_attachments = AccountFolderdocumentAttachment::where('attach_id', $src_video_id)->where('site_id', $this->site_id)->get();
        foreach ($video_attachments as $attachment) {
            $dest_account_folder_id = '';
            $account_folder_document = AccountFolderDocument::where(array(
                        'id' => $attachment['account_folder_document_id'],
                        'site_id' => $this->site_id
                    ))->first();

            if ($account_folder_document) {

                $comment_attachment = CommentAttachment::where(array('site_id' => $this->site_id, 'document_id' => $account_folder_document['document_id']))->first();
                if ($comment_attachment) {
                    continue;
                }

                $account_folder_document = $account_folder_document->toArray();
                $original_document = Document::where("id", $account_folder_document['document_id'])->where("site_id", $this->site_id)->first();

                $account_folder = AccountFolder::where("account_folder_id", $account_folder_document['account_folder_id'])->where("site_id", $this->site_id)->first();
                $account_folder = $account_folder->toArray();

                if ($is_duplicated && $workspace) {

                    $data_af = array(
                        'account_id' => (empty($account_id)) ? $account_folder['account_id'] : $account_id,
                        'site_id' => $this->site_id,
                        'folder_type' => 3,
                        'name' => $account_folder['name'],
                        'desc' => $account_folder['desc'],
                        'active' => $account_folder['active'],
                        'created_date' => date("Y-m-d H:i:s"),
                        'created_by' => $account_folder['created_by'],
                        'last_edit_date' => date("Y-m-d H:i:s"),
                        'last_edit_by' => $account_folder['last_edit_by'],
                    );

                    // $dest_account_folder_id = DB::table('account_folders')->insertGetId($data_af);
                    $account_folder = AccountFolder::where("account_folder_id", $huddle_id)->where("site_id", $this->site_id)->first();
                    $account_folder = $account_folder->toArray();
                    $dest_account_folder_id = $account_folder['account_folder_id'];
                } else {
                    $account_folder = AccountFolder::where("account_folder_id", $huddle_id)->where("site_id", $this->site_id)->first();
                    $account_folder = $account_folder->toArray();
                    $dest_account_folder_id = $account_folder['account_folder_id'];
                }



                if ($original_document) {
                    $original_document = $original_document->toArray();
                    $original_document['id'] = null;
                    if (!empty($account_id)) {
                        $original_document['account_id'] = $account_id;
                    }
                    $new_attachment_id = DB::table('documents')->insertGetId($original_document);

                    $account_folder_document['document_id'] = $new_attachment_id;
                    $account_folder_document['account_folder_id'] = $dest_account_folder_id;
                    $account_folder_document['id'] = null;

                    $account_folder_documents_id = DB::table('account_folder_documents')->insertGetId($account_folder_document);
                    $data_attachment = array(
                        'attach_id' => $dest_video_id,
                        'account_folder_document_id' => $account_folder_documents_id,
                        'site_id' => $this->site_id
                    );
                    DB::table('account_folderdocument_attachments')->insertGetId($data_attachment);
                }
            }
        }
    }

    function comment_attachment_transfer($document_id, $comment_id, $huddle_id, $dest_video_id, $is_duplicated, $workspace, $account_id = '') {


        $original_document = Document::where("id", $document_id)->where("site_id", $this->site_id)->first();
        $account_folder_document = AccountFolderDocument::where("document_id", $document_id)->where("site_id", $this->site_id)->first();
        if ($account_folder_document) {
            $account_folder_document = $account_folder_document->toArray();
        }

        $account_folder = AccountFolder::where("account_folder_id", $account_folder_document['account_folder_id'])->where("site_id", $this->site_id)->first();
        if ($account_folder) {
            $account_folder = $account_folder->toArray();
        }

        if ($is_duplicated && $workspace) {

            $data_af = array(
                'account_id' => (empty($account_id)) ? $account_folder['account_id'] : $account_id,
                'site_id' => $this->site_id,
                'folder_type' => 3,
                'name' => $account_folder['name'],
                'desc' => $account_folder['desc'],
                'active' => $account_folder['active'],
                'created_date' => date("Y-m-d H:i:s"),
                'created_by' => $account_folder['created_by'],
                'last_edit_date' => date("Y-m-d H:i:s"),
                'last_edit_by' => $account_folder['last_edit_by'],
            );

            //   $dest_account_folder_id = DB::table('account_folders')->insertGetId($data_af);
            $account_folder = AccountFolder::where("account_folder_id", $huddle_id)->where("site_id", $this->site_id)->first();
            $account_folder = $account_folder->toArray();
            $dest_account_folder_id = $account_folder['account_folder_id'];
        } else {
            $account_folder = AccountFolder::where("account_folder_id", $huddle_id)->where("site_id", $this->site_id)->first();
            $account_folder = $account_folder->toArray();
            $dest_account_folder_id = $account_folder['account_folder_id'];
        }



        if ($original_document) {
            $original_document = $original_document->toArray();
            $original_document['id'] = null;
            $comment_time = Comment::where(array('site_id' => $this->site_id, 'id' => $comment_id))->first();
            if (!empty($account_id)) {
                $original_document['account_id'] = $account_id;
            }
            if ($comment_time) {
                $comment_time = $comment_time->toArray();
                $original_document['scripted_current_duration'] = $comment_time['time'];
            }
            $new_attachment_id = DB::table('documents')->insertGetId($original_document);

            $account_folder_document['document_id'] = $new_attachment_id;
            $account_folder_document['account_folder_id'] = $dest_account_folder_id;
            $account_folder_document['id'] = null;

            $account_folder_documents_id = DB::table('account_folder_documents')->insertGetId($account_folder_document);
            $data_attachment = array(
                'attach_id' => $dest_video_id,
                'account_folder_document_id' => $account_folder_documents_id,
                'site_id' => $this->site_id
            );
            DB::table('account_folderdocument_attachments')->insertGetId($data_attachment);

            $comment_attachment = array(
                'comment_id' => $comment_id,
                'document_id' => $new_attachment_id,
                'site_id' => $this->site_id
            );

            DB::table('comment_attachments')->insertGetId($comment_attachment);
        }
    }

    function get_resource_url_for_mobile(Request $request) {
        $input = $request->all();
        $huddle_id = $input['huddle_id'];
        $document_id = $input['document_id'];
        $user = json_decode($input['user_current_account']);
        $user_id = is_array($user) ? $user['User']['id'] : $user->User->id;

        $document = Document::getResourcesDetails($this->site_id, $document_id);
        if (empty($document)) {
            return response()->json(['success' => false, 'message' => 'resource not found!']);
        }
        $folder_type = AccountFolder::where(array('account_folder_id' => $huddle_id, 'site_id' => $this->site_id))->value("folder_type");

        if ($folder_type == '1') {
            $is_allowed = AccountFolderUser::user_present_huddle($user_id, $huddle_id);
        } elseif ($folder_type == '3') {
            $is_allowed = ($document['created_by'] == $user_id) ? true : false;
        } else {
            return response()->json(['success' => false, 'message' => 'huddle type not found']);
        }
        if ($is_allowed) {
            $data = [
                "title" => $document['title'],
                "stack_url" => $document['stack_url']
            ];
            return response()->json(['success' => true, 'data' => $data]);
        } else {
            return response()->json(['success' => false, 'message' => "You don't have permissions to view this resource"]);
        }
    }

    function copy_comments_trimming(Request $request) {
        $input = $request->all();
        $src_video_id = $input['src_video_id'];
        $dest_video_id = $input['dest_video_id'];
        $is_duplicated = $input['is_duplicated'];
        $workspace = isset($input['workspace']) ? $input['workspace'] : 0;
        $library = isset($input['library']) ? $input['library'] : 0;
        $huddle_id = $input['huddle_id'];
        $user_id = $input['user_id'];
        $start_duration = $input['start_duration'];
        $end_duration = $input['end_duration'];
        $trim = true;

        if ($huddle_id == 0) {
            $account_folder_details = AccountFolderDocument::where(array(
                        'document_id' => $input['dest_video_id'],
                        'site_id' => $this->site_id
                    ))->first();

            if ($account_folder_details) {
                $account_folder_details = $account_folder_details->toArray();
                $huddle_id = $account_folder_details['account_folder_id'];
            }
        }

        $this->copy_video_attachments($src_video_id, $dest_video_id, $is_duplicated, $workspace, $huddle_id);

        $this->copyComments($src_video_id, $dest_video_id, $user_id, 2, $is_duplicated, $workspace, $huddle_id, $trim, $start_duration, $end_duration);
        @$this->copyComments($src_video_id, $dest_video_id, $user_id, 6, $is_duplicated, $workspace, $huddle_id, $trim, $start_duration, $end_duration);
        @$this->copyComments($src_video_id, $dest_video_id, $user_id, 4, $is_duplicated, $workspace, $huddle_id, $trim, $start_duration, $end_duration);

        $final_array['success'] = true;

        if($workspace == 1 && !$library)
        {
            $folder_type = '3';
        }
        elseif ($library)
        {
            $folder_type = '2';
        }
        else
        {
            $folder_type = '1';
        }

        $doc_object = Document::get_single_video_data($this->site_id, $dest_video_id, $folder_type, 1, $huddle_id, $user_id);
        if($doc_object)
        {
            $account_id = $doc_object['account_id'];
            $subjects = [];
            $uncat_videos = [];
            if($workspace == 1)
            {
                $channel = "workspace-" . $account_id . "-" . $user_id;
            }
            else if($library)
            {
                $channel = 'library-' . $account_id;
                $subjects = AccountFolder::getSubjects($account_id, $this->site_id);
                $uncat_videos = AccountFolder::getVideoLibraryCount('uncat', $account_id, $this->site_id, null, false, "", "");

            }
            else
            {
                $channel = 'huddle-details-' . $huddle_id;
            }
            $channel_data = array(
                'item_id' => $dest_video_id,
                'document_id' => $dest_video_id,
                'reference_id' => $dest_video_id,
                'is_video_crop' => 1,
                'data' => $doc_object,
                'huddle_id' => $huddle_id,
                'channel' => $channel,
                'sidebar_categories' => $subjects,
                'video_categories' => [],
                'uncat_videos' => $uncat_videos,
                'event' => "resource_added"
            );
           if(HelperFunctions::is_push_notification_allowed($account_id))
           {
                HelperFunctions::broadcastEvent($channel_data);
           }
           else
           {
               HelperFunctions::broadcastEvent($channel_data,'broadcast_event',false);
           }
        }

        return response()->json($final_array);
    }

    public function getSecureStreamUrl(Request $request, $internal = 0, $streamer_url = null, $stream_name = null) {
        $stream_url = "";
        if (!$internal) {
            $streamer_url = $request->get("streamer_url");
            $stream_name = $request->get("stream_name");
        }

        $server_url = "http://" . $streamer_url . ":1935/redirect/live?type=m3u8";
        //$server_url = "http://".$used_slot[0]->streamer_url.":1935/liveEdge/smil:$used_slot[0]->stream_name.smil/playlist.m3u8";
        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $redir = '';
        $response = $client->get($server_url, [
            'on_stats' => function (TransferStats $stats) use(&$redir) {
                $redir = (string) $stats->getEffectiveUri();
            }
        ]);
        if (!empty($redir)) {
            $url_parts = explode("/", $redir);
            if (isset($url_parts[2])) {
                $ip = explode(":", $url_parts[2])[0];
                $streamlock_record = \DB::table("cluster_edge_servers")->where("edge_server_ip", $ip)->first();
                if ($streamlock_record) {
                    $stream_url = "https://" . $streamlock_record->streamlock_domain . "/liveEdge/smil:" . $stream_name . ".smil/playlist.m3u8";
                }
            }
        }
        if ($internal) {
            return $stream_url;
        }
        return response()->json(["stream_url" => $stream_url, "success" => true]);
    }

    function push_notification_sns() {
        $notificationTitle = "Test Notification";
        $notificationMessage = "Hi!! Its a test notification";
        $sns = SnsClient::factory(array(
                    'key' => 'AKIAJ4ZWDR5X5JKB7CZQ',
                    'secret' => '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',
                    'region' => 'us-east-1'
        ));

        $result = $sns->createPlatformEndpoint(array(
            'PlatformApplicationArn' => 'arn:aws:sns:us-east-1:614596103109:app/GCM/Sibme_Android',
            'Token' => 'fpbGAppYJ90:APA91bGdK5KiIZfMzMkEZ5btK0RZOFKRHrxpIosqnr2utwx5N_8gSeAM2cSfbCwTbqdsOtv0HI49Mo5sb1ZPS1HxXkHIaqt0NjC7Hhb4BD4qowZxOr0N2qr1yuWW2hiMVyb20GZcrcVl',
        ));


        $fcmPayload = json_encode(
                [
                    "notification" =>
                    [
                        "title" => $notificationTitle,
                        "body" => $notificationMessage,
                        "sound" => 'default'
                    ],
                    "data" => [] // data key is used for sending content through notification.
                ]
        );
        $message = json_encode(["default" => $notificationMessage]);
        $publish = $sns->publish([
            'TargetArn' => $result['EndpointArn'],
            'Message' => $message,
            'MessageStructure' => 'json'
        ]);


        print_r($publish);
        die;
    }

    function oneSignal_addDevice() {
        $fields = array(
            'app_id' => "8c10bced-a2df-4486-9733-033a510baf94",
            'identifier' => "dz5poSTkOhM:APA91bF5_ggwSgJBL-zSYWaiPGXHQpubfY5VTIrh6P0Dl_h6EYnfoUTGIE0iycbtjegDXdZewqhu0kVNosCmO9Q2Ifg1og8zBt_DOOdlfB3JtO-oop7-03sgjQosMlrJKentgQ8sVh7W",
            'device_type' => '1', // 1 for android. 0 for iOS. See full list: https://documentation.onesignal.com/reference#add-a-device
            'test_type' => '1' // remove/comment-out this field for iOS App Store build.
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/players");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    function send_onesignal_notifications() {
        $fields = array(
            'app_id' => "8c10bced-a2df-4486-9733-033a510baf94",
            'include_player_ids' => ["1ac0ca30-0ec0-4c65-9d36-c23562b1ac3e", "20e9aaa0-86b6-4a0f-84ec-785f4e5f704b", "7f5e7386-7074-4638-b1d0-58919bef224b"],
            'headings' => ["en" => "English Message"],
            'contents' => ["en" => "English Notification"],
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public function update_comment_time(Request $request) {
        $result = array();

        $input = $request->all();
        if (!isset($input['comment_id'])) {
            $result['status'] = "failed";
            return $result;
        }
        $comment_id = $input['comment_id'];
        $data = array(
            'last_edit_date' => date('Y-m-d H:i:s'),
            'last_edit_by' => $input['user_id'],
            'site_id' => $this->site_id
        );
        if (isset($input['synchro_time']) && $input['synchro_time'] != '') {
            $data['time'] = $input['synchro_time'];
        }

        if (isset($input['end_time']) && $input['end_time'] != '') {
            $data['end_time'] = $input['end_time'];
        }
          if (!isset($input['created_at_gmt']) || empty($input['created_at_gmt'])){
              $template="<b>This user is using older version of app</b><br>
                         <b>Form Fields : </b>".json_encode($input)." <br> 
                        <b>URL :</b>".$request->url();
                    $emailData = [
                            'from' => Sites::get_site_settings('static_emails', $this->site_id)['noreply'],
                            'from_name' => Sites::get_site_settings('site_title', $this->site_id),
                            'to' => 'khurrams@sibme.com',
                            'cc' => 'waqasn@sibme.com',
                            'subject' => "IOS/Android created_at_gmt is required for update comment time",
                            'template' => $template
                        ];

                 Email::sendCustomEmail($emailData);
          }else{
               $comment_exist = Comment::where(array(
                        'id' => $comment_id,
//                        'created_at_gmt' => $input['created_at_gmt']
                       ));
               if(isset($data['time'])){
                   $comment_exist->where(array('time' => $data['time']));
               }

               if(isset( $data['end_time'])){
                   $comment_exist->where(array('end_time' => $data['end_time']));
               }
               $comment=$comment_exist->first();
                if(!empty($comment)){
                    $res = array(
                        'updated_comment'=>$comment,
                        'status' => 'success',
                        'code'=>409
                    );
                     return $res;
                }
          }
        if (DB::table('comments')->where(['id' => $comment_id])->update($data)) {
            $latest_comment = $this->getSingleCommentDetails($request);
            $result['updated_comment'] = $latest_comment;
            $result['status'] = "success";
            $event = [
                'channel' => 'video-details-' . $latest_comment['ref_id'],
                'event' => "comment_edited",
                'data' => $latest_comment,
                'is_workspace_comment' => $latest_comment["is_workspace_comment"],
                'reference_id' => $latest_comment['ref_id']
            ];
            HelperFunctions::broadcastEvent($event);
        } else {
            $result['status'] = "failed";
        }
        return $result;
    }

    public function getSingleCommentDetails(Request $request) {
        $comment_id = $request->get("comment_id");
        $input = $request->all();
        $latest_comment = $this->get_latest_comment($comment_id);
        $get_standard = $this->gettagsbycommentid($comment_id, array('0')); //get standards
        $get_tags = $this->gettagsbycommentid($comment_id, array('1', '2')); //get tags
        $get_custom_marker = $this->gettagsbycommentid($comment_id, array('2'));
        if(!empty($get_custom_marker))
        {
            $latest_comment['none_tag'] = false;
        }
        else
        {
            $latest_comment['none_tag'] = true;
        }
        $latest_comment['standard'] = $get_standard;
        $latest_comment['default_tags'] = $get_tags;

        $commentDate = $latest_comment['created_date'];
        $currentDate = date('Y-m-d H:i:s');

        $start_date = new DateTime($commentDate);
        $latest_comment['is_new_comment'] = $start_date > new DateTime('2018-08-10 00:00:00');
        $since_start = $start_date->diff(new DateTime($currentDate));

        $commentsDate = '';
        if ($since_start->y > 0) {
            $commentsDate = "About " . $since_start->y . ($since_start->y > 1 ? ' Years Ago' : ' Year Ago');
        } elseif ($since_start->m > 0) {
            $commentsDate = "About " . $since_start->m . ($since_start->m > 1 ? ' Months Ago' : ' Month Ago');
        } elseif ($since_start->d > 0) {
            $commentsDate = $since_start->d . ($since_start->d > 1 ? ' Days Ago' : ' Day Ago');
        } elseif ($since_start->h > 0) {
            $commentsDate = $since_start->h . ($since_start->h > 1 ? ' Hours Ago' : ' Hour Ago');
        } elseif ($since_start->i > 0) {
            $commentsDate = $since_start->i . ($since_start->i > 1 ? ' Minutes Ago' : ' Minute Ago');
        } elseif ($since_start->s > 0) {
            $commentsDate = 'Less Than A Minute Ago';
        } else {
            $commentsDate = 'Just Now';
        }
        $latest_comment['created_date_string'] = $commentsDate;
        //$result['comments'] = $this->get_comemnts_with_replies($input['videoId']);
        // $result['attachment_count'] = $view->Custom->match_timestamp_count_comment($comment_id, $this->data['videoId']);
        $response = $this->get_replies_from_comment($latest_comment, '1',true);
        $latest_comment['Comment'] = $response;
        $latest_comment['replies_count'] = count($response['responses']);

        $is_workspace_comment = 0;
        if (isset($input['huddle_id'])) {
            $huddle = AccountFolder::where(array(
                        'account_folder_id' => $input['huddle_id'],
                        'site_id' => $this->site_id
                    ))->first()->toArray();

            if ($huddle && $huddle['folder_type'] == '3') {
                $is_workspace_comment = 1;
            }
        }
        $custom_input = [];
        $custom_input["user_id"] = $input["user_id"];
        $custom_input["video_id"] = $input['videoId'];
        $custom_input["huddle_id"] = $input['huddle_id'];
        $files = self::getVideoDocuments($request, $custom_input);
        $latest_comment["files"] = $files;
        $latest_comment["is_workspace_comment"] = $is_workspace_comment;
        return $latest_comment;
    }
    
    public function send_push_from_lumen(Request $request)
    {
        $input = $request->all();
        $msg_payload = $input['msg_payload'];
        $dummy_token_ids = $input['dummy_token_ids'];
        $site_id = $input['site_id'];
        return HelperFunctions::ios_and_android($msg_payload,$dummy_token_ids,$site_id);
    }

    function live_streaming_allowed(Request $request)
    {
        $input = $request->all();
        $user_id = $input['user_id'];
        $account_id = $input['account_id'];
        $huddle_id = $input['huddle_id'];
        $site_id = $input['site_id'];
        $allowed = false;

        $result = AccountFolderUser
                ::where(array(
                    'user_id' => $user_id,
                    'account_folder_id' => $huddle_id,
                    'site_id' => $site_id
                ))->get()->toArray();
        if(!empty($result) && $result[0]['role_id']== 220){
        return response()->json(array('allowed' => false));
        }


        $result_groups = AccountFolderGroup::join('user_groups as ug', 'ug.group_id', '=', 'account_folder_groups.group_id')->where(array(
                    'ug.user_id' => $user_id,
                    'account_folder_groups.account_folder_id' => $huddle_id,
                    'account_folder_groups.site_id' => $site_id
                ))->get()->toArray();

        $account_detail = Account::where(array("id" => $account_id))->first()->toArray();

        if( (!empty($result_groups) || !empty($result)) && $account_detail['enable_live_rec'] == '1' )
        {
            $allowed = true;
        }
        else
        {
            $allowed = false;
        }

        $final_array = array('allowed' => $allowed );
        return response()->json($final_array);


    }

    public function copyTranscribedFile($videoFileName, $copiedVideoId, $OriginalVideoId) {
        try {
            $client = new S3Client([
                'region' => 'us-east-1',
                'version' => 'latest',
                'credentials' => array(
                    'key' => config('s3.access_key_id'),
                    'secret' => config('s3.secret_access_key'),
                )
            ]);
    
            $client->copyObject([
                'Bucket' => config('s3.bucket_name'),
                'Key' => $videoFileName . "/" . $copiedVideoId . "_transcribe.json",
                'CopySource' => config('s3.bucket_name') . '/' . $videoFileName . "/" . $OriginalVideoId . "_transcribe.json",
            ]);
            $client->copyObject([
                'Bucket' => config('s3.bucket_name'),
                'Key' => $videoFileName . "/" . $copiedVideoId . "_transcribe.vtt",
                'CopySource' => config('s3.bucket_name') . '/' . $videoFileName . "/" . $OriginalVideoId . "_transcribe.vtt",
            ]);
            $client->copyObject([
                'Bucket' => config('s3.bucket_name'),
                'Key' => $videoFileName . "/" . $copiedVideoId . "_transcribe.srt",
                'CopySource' => config('s3.bucket_name') . '/' . $videoFileName . "/" . $OriginalVideoId . "_transcribe.srt",
            ]);
            $this->duplicateTranslationsInDb($OriginalVideoId,$copiedVideoId);
        } catch (ErrorException $e) {
            return FALSE;
        }
    }

    public function duplicateTranslationsInDb($copied_document_id,$new_id){
       $DocumentSubtitle= DocumentSubtitle::where('document_id', $copied_document_id)->get();
        foreach($DocumentSubtitle as $s_title){
        try {
            $newTask = $s_title->replicate();
            $newTask->document_id = $new_id; // the new document_id
            $newTask->save();
        } catch (\Exception $e) {
            $this->error("An error occurred while duplicating subtitles into database\n".$e->getMessage());
        }
        }
    }

    function get_document_comments(Request $request){

        $input = $request->all();
        $video_id = $input['document_id'];
        $user_id = $input['user_id'];
        $huddle_id = $input['huddle_id'];
        $account_id = $input['account_id'];
        $tags = isset($input['custom_marker']) ? $input['custom_marker'] : '';
        $page_sort = isset($input['page_sort']) ? $input['page_sort'] : '';
        $comment_status = isset($input['comment_status']) ? $input['comment_status'] : false;
        $search_text = isset($input['search_text']) ? $input['search_text'] : '';
        $account_details = Account::where(array('id' => $account_id))->first();
        if($account_details && $account_details->enable_document_commenting == '0')
        {
            return response()->json(array('success' => false));
        }
        if(empty($comment_status))
        {
            $comment_status = false;
        }
        $account_custom_markers_permission = 0;
        $workspace_custom_markers_permission = 0;
        $video_marker_tags = AccountFolderMetaData::where(array(
                    "account_folder_id" => $account_id,
                    "meta_data_name" => "enable_tags_ws",
                    'site_id' => $this->site_id
                ))->first();

        if ($video_marker_tags) {
            $video_marker_tags = $video_marker_tags->toArray();
        }

        $video_marker_tags_ac = AccountFolderMetaData::where(array(
                    "account_folder_id" => $account_id,
                    "meta_data_name" => "enable_tags",
                    'site_id' => $this->site_id
                ))->first();

        if ($video_marker_tags_ac) {
            $video_marker_tags_ac = $video_marker_tags_ac->toArray();
        }

        if (isset($video_marker_tags_ac['meta_data_value']) && $video_marker_tags_ac['meta_data_value'] == '1' && isset($video_marker_tags['meta_data_value']) && $video_marker_tags['meta_data_value'] == '1') {
            $workspace_custom_markers_permission = '1';
        } else {
            $workspace_custom_markers_permission = '0';
        }

        if (isset($video_marker_tags_ac['meta_data_value']) && $video_marker_tags_ac['meta_data_value'] == '1') {
            $account_custom_markers_permission = '1';
        } else {
            $account_custom_markers_permission = '0';
        }

        $huddle = AccountFolder::where(array(
                        'account_folder_id' => $input['huddle_id'],
                        'site_id' => $this->site_id
                    ))->first()->toArray();

        if ($huddle && $huddle['folder_type'] == '3') {
            $is_workspace = true;
        }
        else
        {
            $is_workspace = false;
        }

        $account_folder_document = AccountFolderDocument::where(array(
                        'document_id' => $video_id,
                        'site_id' => $this->site_id
                    ))->first();

        if($account_folder_document)
        {
            if(empty($account_folder_document->video_framework_id))
            {
                $final_array['framework_assigned'] = false;
                $final_array['framework_id'] = $account_folder_document->video_framework_id;
            }
            else
            {
                $final_array['framework_assigned'] = true;
                $final_array['framework_id'] = $account_folder_document->video_framework_id;
            }
        }
        else
        {
            $final_array['framework_assigned'] = false;
            $final_array['framework_id'] = '';
        }

        if($is_workspace)
        {
           $final_array['video_markers_check'] =  $workspace_custom_markers_permission;
        }
        else
        {
            $final_array['video_markers_check'] =  $account_custom_markers_permission;
        }

        $document_detail = Document::get_single_video_data($this->site_id, $video_id, $huddle['folder_type'], 1, $huddle_id, $user_id);

        $final_array['is_workspace'] = $is_workspace;
        if($document_detail && !empty($document_detail['parent_folder_id']))
        {
            $final_array['breadcumbs'] = DocumentFolder::getNLevelParents($document_detail['parent_folder_id']);
        }

        if(!$is_workspace)
        {
            $final_array['bread_crumb_output'] = $this->bread_crumb_output($huddle_id);
            $final_array['huddle_detail'] = $huddle;
        }

        $comments = $this->get_video_comments_with_replies($video_id, '1', $huddle_id, $user_id , $tags , $comment_status,$page_sort,$search_text);
        $custom_markers = $this->getmarkertags($account_id, '1');
        $user_huddle_level_permissions = $this->get_huddle_permissions($huddle_id, $user_id);
        $htype = app('App\Http\Controllers\HuddleController')->get_huddle_type($huddle_id);
        $final_array['custom_markers'] = $custom_markers;
        $final_array['huddle_type'] = $htype;
        $allow_per_video_to_coachee = AccountFolderMetaData::where(array('account_folder_id' => $huddle_id, "meta_data_name" => "allow_per_video_to_coachee", 'site_id' => $this->site_id))->get()->toArray();
        $allow_per_video_to_coachee = isset($allow_per_video_to_coachee[0]['meta_data_value']) ? $allow_per_video_to_coachee[0]['meta_data_value'] : "0";
        $final_array['allow_per_video_to_coachee'] = $allow_per_video_to_coachee;
        if($htype == '2')
        {
          $huddle_settings =  AccountFolderMetaData::where(array('account_folder_id' => $huddle_id , 'meta_data_name' => 'coachee_permission'))->first();
          $coachee_permission = $huddle_settings->meta_data_value;
          $final_array['coachee_permission'] = $coachee_permission;
          
        }
        else
        {
            $final_array['coachee_permission'] = '0';
        }

        if($htype == '3')
        {
            $account_folder_user_detail = AccountFolderUser::where(array('account_folder_id' => $huddle_id , 'user_id' => $user_id))->first();
            if($account_folder_user_detail)
            {
                    $is_submitted = $account_folder_user_detail->is_submitted;
            }
            else
            {
                    $is_submitted = 0;
            }
            $final_array['is_submitted'] = $is_submitted;
        }
        else
        {
            $is_submitted = 0;
            $final_array['is_submitted'] = $is_submitted;
        }

        $rubric_check = false;
        if($is_workspace)
        {
            if ($this->is_enabled_framework_and_standards_ws($account_id)) {
                $rubric_check = true;
            }
        }
        else
        {
            if ($this->is_enabled_framework_and_standards($account_id)) {
                $rubric_check = true;
            }
        }
        //Uncomment following code to get default framework
/*
        if ($document_detail['post_rubric_per_video'] == '1') {

            $account_folder_details = AccountFolderDocument::where(array(
                'account_folder_id' => $huddle_id,
                'document_id' => $video_id,
                'site_id' => $this->site_id
            ))->first();

            if ($account_folder_details) {
                $account_folder_details = $account_folder_details->toArray();
            }

            if (!empty($account_folder_details)) {
                if (!empty($account_folder_details['video_framework_id'])) {
                    $final_array['framework_selected_for_video'] = '1';
                } else {
                    $final_array['framework_selected_for_video'] = '0';
                }
            }
        } else {
            $final_array['framework_selected_for_video'] = '1';
        }

        $framework_id = -1;
        if (isset($final_array['framework_selected_for_video']) && $final_array['framework_selected_for_video'] == 1) {
            if(!isset($account_folder_details))
            {
                $account_folder_details = AccountFolderDocument::where(array(
                    'account_folder_id' => $huddle_id,
                    'site_id' => $this->site_id,
                    'document_id' => $video_id
                ))->first();

                if ($account_folder_details) {
                    $account_folder_details = $account_folder_details->toArray();
                }
            }

            $framework_id = $account_folder_details['video_framework_id'];
        }
        if((!isset($framework_id) || empty($framework_id)) && !$is_workspace)
        {
            $id_framework = AccountFolderMetaData::where(array(
                'account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id', 'site_id' => $this->site_id
            ))->first();

            if ($id_framework) {
                $id_framework->toArray();
            }

            $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";
        }

        $framework_id_ws = -1;
        if ($framework_id == -1 || $framework_id == 0) {

            $id_framework = AccountMetaData::where(array(
                'account_id' => $account_id, 'meta_data_name' => 'default_framework', 'site_id' => $this->site_id
            ))->first();


            if ($id_framework) {
                $id_framework->toArray();
            }

            $framework_id_ws = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";
        }


        $final_array['account_framework'] = $framework_id_ws;
        $final_array['default_framework'] = $framework_id;
        */

        $user_activity_logs = array(
            'ref_id' => $video_id,
            'desc' => 'Download Resource',
            'url' => '/Huddles/download/' . $video_id,
            'type' => '13',
            'account_folder_id' => $huddle_id ,
            'environment_type' => '2',
        );
        $this->user_activity_logs($user_activity_logs, $account_id, $user_id);
        $user = User::where('id', $user_id)->first();
        if($user)
        {
            $this->create_churnzero_event('Resource+Viewed', $account_id, $user->email);
        }

        $final_array['rubric_check'] = $rubric_check;
        $final_array['huddle_role'] = $user_huddle_level_permissions;
        $final_array['comments'] = $comments;
        $final_array['document_detail'] = $document_detail;
        $file_info = pathinfo($document_detail['original_file_name']);
        $final_array['document_extension'] = $file_info['extension'];
        
        $final_array['success'] = true;
        return response()->json($final_array);
    }


    function duplicateDocumentsIDsTracker()
    {
            $document_ids =  app('db')->select("SELECT
                COUNT(duration) AS count1 , COUNT(DISTINCT duration) AS count2 ,
                document_id , url
              FROM
                `document_files` df
                WHERE url IS NOT NULL AND duration > 0
                 GROUP BY document_id
              HAVING count1 > 1 AND count1 = count2");
            $doc_ids = array();
            foreach($document_ids as $id)
            {
                $doc_ids[] = $id->document_id;
            }


            $final_ids = implode(',',$doc_ids);

            $html = 'Following is the List of Document IDs : ' . $final_ids;
                            $emailData = [
                                'from' => Sites::get_site_settings('static_emails', '1')['noreply'],
                                'from_name' => Sites::get_site_settings('site_title', '1'),
                                'to' => 'testinglandingpage9@gmail.com,khurri.saleem@gmail.com,saadziafast@gmail.com',
                                'subject' => 'Duplicated Document IDs in Document Files',
                                'template' => $html,
                            ];
            Email::sendCustomEmail($emailData);


    }


    function update_document_comment_custom_marker(Request $request)
    {
        $input = $request->all();
        $comment_id = $input['comment_id'];
        $tags_1 = isset($input['assessment_value']) ? $input['assessment_value'] : '';
        if (!empty($tags_1)) {
            $this->save_tag_2($tags_1, $comment_id, $input['videoId'], $input['account_id'], $input['user_id']);
        } else {
            DB::table('account_comment_tags')->where(array('comment_id' => $comment_id, "ref_type" => "2", 'site_id' => $this->site_id))->delete();
        }


        $latest_comment = $this->getSingleCommentDetails($request);
        $result['updated_comment'] = $latest_comment;
        $result['status'] = "success";
        if ($latest_comment['ref_type'] == 6) {
            $latest_comment['comment'] = $this->get_cloudfront_url($latest_comment['comment']);
        }
        $event = [
            'channel' => 'video-details-' . $latest_comment['ref_id'],
            'event' => "comment_edited",
            'data' => $latest_comment,
            'comment' => $input['comment'],
            'item_id' => $comment_id,
            'is_workspace_comment' => $latest_comment['is_workspace_comment'],
            'reference_id' => $latest_comment['ref_id'],
            'video_id' => $latest_comment['ref_id']
        ];
        HelperFunctions::broadcastEvent($event);

        return $result;
    }

    function update_document_comment_status(Request $request)
    {
        $input = $request->all();
        $comment_id = $input['comment_id'];

        $data = array('comment_status' => $input['comment_status'] );

        DB::table('comments')->where(['id' => $comment_id])->update($data);

        $latest_comment = $this->getSingleCommentDetails($request);
        $result['updated_comment'] = $latest_comment;
        $result['status'] = "success";
        if ($latest_comment['ref_type'] == 6) {
            $latest_comment['comment'] = $this->get_cloudfront_url($latest_comment['comment']);
        }
        $event = [
            'channel' => 'video-details-' . $latest_comment['ref_id'],
            'event' => "comment_edited",
            'data' => $latest_comment,
            'comment' => $input['comment'],
            'item_id' => $comment_id,
            'is_workspace_comment' => $latest_comment['is_workspace_comment'],
            'reference_id' => $latest_comment['ref_id'],
            'video_id' => $latest_comment['ref_id']
        ];
        HelperFunctions::broadcastEvent($event);

        return $result;
    }

    function get_video_information(Request $request)
    {
        $input = $request->all();
        $video_ids = explode(',', $input['video_ids']);
        $videos_data = array();
        foreach ($video_ids as $index => $video_id)
        {
          $document_local_id_finder = DocumentMetaData::where(array('meta_data_value' => $video_id,'meta_data_name' => 'document_local_id' ))->first();
          if(!$document_local_id_finder)
          {
              $videos_data[$index]['transcoded_duration'] = '';
              $videos_data[$index]['document_local_id'] = '';
              $videos_data[$index]['document_duration'] = '';
              continue;
          }
          $documentfiles =  DocumentFiles::where(array('document_id' => $document_local_id_finder->document_id))->first();
          $document_local_id = DocumentMetaData::where(array('document_id' => $document_local_id_finder->document_id,'meta_data_name' => 'document_local_id' ))->first();
          $document_duration = DocumentMetaData::where(array('document_id' => $document_local_id_finder->document_id,'meta_data_name' => 'document_duration' ))->first();
            if($documentfiles && $documentfiles->duration != NULL)
            {
                $videos_data[$index]['transcoded_duration'] = $documentfiles->duration;
            }
            else
            {
                $videos_data[$index]['transcoded_duration'] = HuddleController::getVideoDuration($document_local_id_finder->document_id)["data"];
            }
            if($document_local_id)
            {
                $videos_data[$index]['document_local_id'] = $document_local_id->meta_data_value;
            }
            else
            {
                $videos_data[$index]['document_local_id'] = '0';
            }
            if($document_duration)
            {
                $videos_data[$index]['document_duration'] = $document_duration->meta_data_value;
            }
            else
            {
                $videos_data[$index]['document_duration'] = '0';
            }
        }


        return response()->json(["videos_data" => $videos_data, "success" => true]);

    }

    public function downloadPdfAnnotations(Request $request){

        $input = $request->all();
        $video_id = $input['document_id'];
        $user_id = $input['user_id'];
        $huddle_id = $input['huddle_id'];
        $account_id = $input['account_id'];
        $tags = isset($input['custom_marker']) ? $input['custom_marker'] : '';
        $page_sort = isset($input['page_sort']) ? $input['page_sort'] : '';
        $comment_status = isset($input['comment_status']) ? $input['comment_status'] : false;


        $huddle = AccountFolder::where(array(
                        'account_folder_id' => $input['huddle_id'],
                        'site_id' => $this->site_id
                    ))->first()->toArray();

        $document_detail = Document::get_single_video_data($this->site_id, $video_id, $huddle['folder_type'], 1, $huddle_id, $user_id);


        $comments = $this->get_video_comments_with_replies($video_id, '1', $huddle_id, $user_id , $tags , $comment_status,$page_sort);

        $actions = $comments['Document']['comments'];

        $svgImage = config('s3.sibme_base_url') . "/home/assets/img/text-comment.svg";
        $pdf = new PDF(null, 'px');
        $pdf->SetAutoPageBreak(FALSE, PDF_MARGIN_BOTTOM);
        $videoFilePath = pathinfo($document_detail['original_file_name']);
        if($videoFilePath['extension'] != 'pdf')
        {
           $temp = explode('/',$document_detail['stack_url']);
           $inputPath = "https://cdn.filestackcontent.com/output=f:pdf/pdfconvert=pageorientation:portrait,pageformat:a3/".$temp[3];
        }
        else
        {
        $inputPath = $document_detail['stack_url']; //"https://cdn.filestackcontent.com/xC5Go7xBRkqFck19XyUu";
        }
        $outputName = "annotation.pdf";
        $pdf->numPages = $pdf->setSourceFile($inputPath);


        foreach(range(1, $pdf->numPages, 1) as $page) {
            $rotate = false;
            $degree = 0;
            try {
              $pdf->_tplIdx = $pdf->importPage($page);
            }
            catch(\Exception $e) {
              return false;
            }

            $size = $pdf->getTemplateSize($pdf->_tplIdx);
            $scale =  round($size['w'] /  str_replace('px','',$input['page_width']), 3);
            if($size['w'] > $size['h'] )
            {
                $page_format = 'L';
            }
            else
            {
                $page_format = 'P';
            }
            $pdf->AddPage($page_format, array($size['w'] , $size['h'], 'Rotate'=>$degree), true);
            $pdf->useTemplate($pdf->_tplIdx);
           // $pdf->Annotation(83, 27, 10, 10, "Text annotation example\naccented letters test: àèéìòù", array('Subtype'=>'Text', 'Name' => 'Comment', 'T' => 'title example', 'Subj' => 'example', 'C' => array(255, 255, 0)));
               $style4 = array('L' => 0,
                'T' => array('width' => 0.25, 'cap' => 'butt', 'join' => 'miter', 'dash' => '20,10', 'phase' => 10, 'color' => array(100, 100, 255)),
                'R' => array('width' => 0.50, 'cap' => 'round', 'join' => 'miter', 'dash' => 0, 'color' => array(50, 50, 127)),
                'B' => array('width' => 0.75, 'cap' => 'square', 'join' => 'miter', 'dash' => '30,10,5,10'));
                        $style3 = array('width' => 1, 'cap' => 'round', 'join' => 'round', 'dash' => '2,10', 'color' => array(255, 0, 0));
                        $style6 = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => '10,10', 'color' => array(0, 128, 0));
            $pdf->SetLineStyle(array('width' => 3, 'cap' => 'butt', 'join' => 'miter', 'color' => array(0, 128, 0)));
            $page_diff = 0;
            foreach($actions as $action) {
                    if($page == $action['page']){
                        
                        if(!empty($action['page_width']) && $action['page_width'] != NULL && $action['page_width'] != 0 )
                        {
                           $scale =  round($size['w'] /  $action['page_width'], 3); 
                        }

                        $position = ($action['page'] - 1) * str_replace('px','',$input['page_height']);
                        $commenter_name = HelperFunctions::user_name_from_id($action['created_by'],$this->site_id);
                        if($action['page'] > 1)
                        {
                           // $page_diff = $page_diff + 0.5;
                           // $position = $position - ((6.7+$page_diff) * ($action['page'] - 1));
                            $position = 0;
                        }
                        if($action['type'] == 'rect-comment')
                        {
                            // $pdf->Annotation(($action['xPos'] *  $scale) - 14, ($action['yPos'] * $scale) - 16, $action['width'] *  $scale , $action['height'] *  $scale, strip_tags($action['comment']),array('Subtype'=>'Square'));
                            // $pdf->Rect(($action['xPos'] *  $scale) - 14, ($action['yPos'] *  $scale) - 16 , $action['width'] *  $scale , $action['height'] *  $scale);
                            if($action['ref_type'] != '6')
                            {
                                $comment = str_replace('</p>',PHP_EOL,$action['comment']);
                                $pdf->Annotation(($action['xPos'] *  $scale) , (($action['yPos'] - $position) * $scale) , $action['width'] *  $scale , $action['height'] *  $scale, $commenter_name."\n".strip_tags($comment),array('Subtype'=>'Square'));
                            }
                            $pdf->Rect(($action['xPos'] *  $scale), (($action['yPos'] - $position) *  $scale) , $action['width'] *  $scale , $action['height'] *  $scale);
                        }
                        
                        else if($action['type'] == 'line-comment')
                        {
                            // $pdf->Annotation(($action['xPos'] *  $scale) - 14, ($action['yPos'] * $scale) - 16, $action['width'] *  $scale , $action['height'] *  $scale, strip_tags($action['comment']),array('Subtype'=>'Square'));
                            // $pdf->Rect(($action['xPos'] *  $scale) - 14, ($action['yPos'] *  $scale) - 16 , $action['width'] *  $scale , $action['height'] *  $scale);
                            $annotation_data = json_decode($action['annotation_data'],true);
                            if($action['ref_type'] != '6')
                            {
                                $comment = str_replace('</p>',PHP_EOL,$action['comment']);
                                $pdf->Annotation(($action['xPos'] *  $scale) , (($action['yPos'] - $position) * $scale) , abs($annotation_data['x1'] - $annotation_data['x2']) *  $scale , abs($annotation_data['y1'] - $annotation_data['y2']) *  $scale, $commenter_name."\n".strip_tags($comment),array('Subtype'=>'Square'));
                            }
                            $pdf->Arrow($annotation_data['x1'] *  $scale, $annotation_data['y1'] *  $scale , $annotation_data['x2'] *  $scale , $annotation_data['y2'] *  $scale,2);
                        }
                        
                        else if($action['type'] == 'ellipse-comment')
                        {
                            // $pdf->Annotation(($action['xPos'] *  $scale) - 14, ($action['yPos'] * $scale) - 16, $action['width'] *  $scale , $action['height'] *  $scale, strip_tags($action['comment']),array('Subtype'=>'Square'));
                            // $pdf->Rect(($action['xPos'] *  $scale) - 14, ($action['yPos'] *  $scale) - 16 , $action['width'] *  $scale , $action['height'] *  $scale);
                            $annotation_data = json_decode($action['annotation_data'],true);
                            if($action['ref_type'] != '6')
                            {
                                $comment = str_replace('</p>',PHP_EOL,$action['comment']);
                                $pdf->Annotation(($action['xPos'] *  $scale) , (($action['yPos'] - $position) * $scale) , 2 * $annotation_data['rx'] *  $scale , 2 * $annotation_data['ry'] *  $scale, $commenter_name."\n".strip_tags($comment),array('Subtype'=>'Square'));
                            }
                            $pdf->ellipse($annotation_data['cx'] *  $scale, $annotation_data['cy'] *  $scale , $annotation_data['rx'] *  $scale , $annotation_data['ry'] *  $scale);
                        }
                        else if($action['type'] == 'foreignObject-comment')
                        {
                            $annotation_data = json_decode($action['annotation_data'],true);
                            $pdf->SetFont('times', '', 14);
                            //$pdf->SetDrawColor(255,0,0);
                            $pdf->SetTextColor(0,63,127);
                            $innerHtml = str_replace('<p>','',$annotation_data['innerHTML']);
                            $innerHtml = str_replace('</p>',"<br>", $innerHtml);
                            $innerHtml = str_replace('<div>',"<br>", $innerHtml);
                            $innerHtml = str_replace('</div>',"", $innerHtml);
                            $pdf->writeHTMLCell($action['width'] *  $scale , $action['height'] *  $scale, ($action['xPos'] *  $scale), (($action['yPos'] - $position) *  $scale), $innerHtml, 0, 1, 0, true, 'J');
                        }
                        else
                        {
                            // $pdf->Annotation(($action['xPos'] *  $scale) - 14, ($action['yPos'] * $scale) - 16  , 25, 25, strip_tags($action['comment']),array('Subtype'=>'Square'));
                            // $pdf->ImageSVG($svgImage, ($action['xPos'] *  $scale) - 14, ($action['yPos'] *  $scale) - 16 , 25, 25, $scale, '', '', '', 0, false);
                            if($action['ref_type'] != '6')
                            {
                                $comment = str_replace('</p>',PHP_EOL,$action['comment']);
                                $pdf->Annotation(($action['xPos'] *  $scale), (($action['yPos'] - $position) * $scale) , 48 *  $scale, 48 *  $scale, $commenter_name."\n".strip_tags($comment),array('Subtype'=>'Square'));
                            }
                            $pdf->ImageSVG($svgImage, ($action['xPos'] *  $scale), (($action['yPos'] - $position) *  $scale) , 48 *  $scale, 48 *  $scale, $scale, '', '', '', 0, false);
                        }

                        //$pdf->Rect(145, 10, 40, 20, 'D', array('all' => $style3));

                        //$pdf->Annotation(83, 27, 10, 10, "Text annotation example\naccented letters test: àèéìòù", array('Subtype'=>'Text', 'Name' => 'Comment', 'T' => 'title example', 'Subj' => 'example', 'C' => array(255, 255, 0)));
                    }
            }
        }

            
            $account_folder_document = DB::table("account_folder_documents")->where("document_id",$video_id)->first();
            $account_folder_document->title = str_replace(',', '', $document_detail['original_file_name']);
            $account_folder_document->title = str_replace("&","",str_replace(";","",/*str_replace("amp","",*/strip_tags($document_detail['original_file_name'])/*)*/));
            $path_info = $videoFilePath;
            $extension= $path_info['extension'];
            if($extension != 'pdf')
            {
              $account_folder_document->title = $path_info['filename'].'.pdf';   
            }
            $pdf->Output($account_folder_document->title, 'D');
    }

    function set_user_online_status(Request $request)
    {
        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");
        $device_type = $request->has("environment_type");
        $viewer_exist = UserOnlineStatus::where("user_id",$user_id)->where("account_id",$account_id)->first();
        if($viewer_exist)
        {

            $viewer_exist->updated_at = date("Y-m-d H:i:s");
            $viewer_exist->platform = $device_type;
            $viewer_exist->save();
        }
        else
        {
            $viewer = new UserOnlineStatus();
            $viewer->account_id = $account_id;
            $viewer->user_id = $user_id;
            $viewer->platform = $device_type;
            $viewer->created_at = date("Y-m-d H:i:s");
            $viewer->updated_at = date("Y-m-d H:i:s");
            $viewer->save();
        }
        return array('success' => true);
    }

    function get_user_online_status(Request $request)
    {
        $account_id = $request->get("account_id");
        $user_ids = Comment::where(array(
                    'ref_id' => $request->get("document_id"),
                ))->get()->pluck('created_by')->toArray();

        $online_user_ids = [];
        $online_user_ids[] = $request->get("user_id");
        foreach($user_ids as $user_id)
        {
             $viewer_exist = UserOnlineStatus::where("user_id",$user_id)->where("account_id",$account_id)->first();
             if($viewer_exist)
             {
                 if( abs(strtotime(date("Y-m-d H:i:s")) - strtotime($viewer_exist->updated_at)) < 60 )
                 {
                     if(!in_array($user_id,$online_user_ids))
                     {
                        $online_user_ids[] = $user_id;
                     }
                 }
             }
        }

        return $online_user_ids;
    }

    function onlineUserCheck()
    {
       $event = [
            'channel' => 'user_online_check',
            'event' => "user_online_check",
        ];
        HelperFunctions::broadcastEvent($event, "broadcast_event", false);
        return ['success' => true];
    }

}
