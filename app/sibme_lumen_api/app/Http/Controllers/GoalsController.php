<?php
namespace App\Http\Controllers;
use App\Api\Activities\GetActivities;
use App\Models\Account;
use App\Models\AccountFolder;
use App\Models\AccountFolderDocument;
use App\Models\AccountFolderGroup;
use App\Models\AccountFolderUser;
use App\Models\AccountFrameworkSetting;
use App\Models\AccountFrameworkSettingPerformanceLevel;
use App\Models\AccountMetaData;
use App\Models\AccountFolderMetaData;
use App\Models\Document;
use App\Models\DocumentFolder;
use App\Models\EmailUnsubscribers;
use App\Models\Goal;
use App\Http\Controllers\HuddleController;
use App\Models\GoalCategory;
use App\Models\GoalItem;
use App\Models\GoalItemEvidence;
use App\Models\GoalItemsFeedback;
use App\Models\GoalItemStandardRating;
use App\Models\GoalItemSubmittedEvidence;
use App\Models\GoalSettings;
use App\Models\GoalUser;
use App\Models\JobQueue;
use App\Models\Sites;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\GoalUserSettings;
use App\Models\UserGroup;
use App\Models\Category;
use App\Services\Emails\Email;
use App\Services\HelperFunctions;
use App\Services\SendGridEmailManager;
use App\Services\TranslationsManager;
use App\Services\QueuesManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GoalsController extends Controller {

    protected $httpreq;
    protected $site_id;
    protected $current_lang;
    protected $base;
    protected $language_based_content;
    public $goals_type_translation_key = ['1'=>'template', '2'=>'vd_account', '3'=>'group', '4'=>'individual'];
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->current_lang = $this->httpreq->header('current-lang');
        $this->base = config('s3.sibme_base_url');
        $this->language_based_content = TranslationsManager::get_page_lang_based_content('global', $this->site_id, '', 'en');
    }
    public function addHttpWithLinks($comment) {
        $tidy_config = array(
            //'clean' => true,
            'output-xhtml' => true,
            'show-body-only' => true,
            'wrap' => 0,
            //'fix-style-tags'=>false
        );
        
        $tidy = tidy_parse_string( $comment, $tidy_config, 'UTF8');
        $tidy->cleanRepair();
        $dom = new \DOMdocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML('<meta http-equiv="Content-Type" content="text/html; charset=utf-8">' . (string) $tidy);
        libxml_use_internal_errors(false);

        foreach ($dom->getElementsByTagName('a') as $item) {
            $href = $item->getAttribute('href');
            if (!$this->isURLContainsHttp($href)) {
                $href = "https://" . $href;
                $item->setAttribute('href', $href);
                $dom->saveHTML($item);
            }
        }
        // echo "Ddd";
        // var_dump($dom->saveHTML());
        // die;
        return $dom->saveHTML();
    }
    public function isURLContainsHttp($url) {
        $url = parse_url($url);
        return isset($url['scheme']) && in_array($url['scheme'], ['http', 'https']);
    }
    public function addEditGoal(Request $request)
    {
        $goal_id = $request->get("goal_id");
        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");
        $template_id = $request->has("template_id") ? $request->get("template_id") : 0;
        $title = $request->get("title");
        $desc = $request->get("desc");
        $desc = $this->addHttpWithLinks( $desc);
        $desc = str_replace('<em','<i',$desc);
        $desc = str_replace('</em>','</i>',$desc);
        $start_date = $request->get("start_date");
        $is_published = $request->has("is_published") ? ($request->get("is_published") == 1 ? 1 : 0) : 0;
        $end_date = $request->get("end_date");
        $categories = $request->get("categories");
        $goal_type = $request->has("goal_type") ? $request->get("goal_type") : Goal::goalTypeIndividual;
        $user_email = User::where('id',$user_id)->pluck('email')->first();
        
        if(!is_array($categories))
        {
            $categories = explode(",", $categories);
        }
        $goal = Goal::where("id", $goal_id)->where("is_active", 1)->where("account_id", $account_id)->first();
        if($goal)
        {
            $old = clone $goal;
        }
        else
        {
            $old = null;
        }

        if(empty($goal))
        {
            $data = array(
                'account_id' => $account_id,
                'folder_type' => 4,
                'name' => $title,
                'desc' => $desc,
                'active' => 1,
                'created_date' => date("Y-m-d H:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d H:i:s"),
                'last_edit_by' => $user_id,
                'site_id' => $this->site_id
            );
            $af_id = DB::table('account_folders')->insertGetId($data);
            $goal = new Goal();
            $goal->account_id = $account_id;            
            $goal->created_by = $user_id;
            $goal->account_folder_id = $af_id;
            $goal->site_id = $this->site_id;
        }

        if($goal_id && $is_published && $title != $goal->title)
        {
            $activity_type2 = Goal::activityTypes["goal_title_edited"];
            $desc2 = "edited the Goal title for $goal->title on {CREATED_DATE}.";
            $activity_data = ["goal_id"=>$goal->id, "desc"=>$desc2, "account_folder_id"=>$goal->account_folder_id, "account_id"=>$account_id, "user_id"=>$user_id, "type"=>$activity_type2, "item_name"=>$goal->title];
            // $this->addRecentActivity($activity_data);
            $activity_data['current_lang'] = $this->current_lang;
            $activity_data['site_id'] = $this->site_id;
            QueuesManager::sendToEmailQueue(__CLASS__, "addRecentActivity", $activity_data);
        }

        $goal->goal_type = $goal_type;
        $goal->title = $title;
        $goal->desc = $desc;
        if(!empty($start_date))
        {
            $goal->start_date = date("Y-m-d H:i:s", strtotime($start_date));
            if($old && $goal_id && $is_published && date("Y-m-d", strtotime($goal->start_date)) != date("Y-m-d", strtotime($old->start_date)))
            {
                $activity_type2 = Goal::activityTypes["goal_start_date_edited"];
                $special = date("M d, Y", strtotime($start_date))."||".GetActivities::_SpanishDate(strtotime($start_date), 'spanish_day_month_year');
                $desc2 = "edited the Goal Start Date for $title to ".$special." on {CREATED_DATE}.";
                $activity_data = ["goal_id"=>$goal->id, "desc"=>$desc2, "account_folder_id"=>$goal->account_folder_id, "account_id"=>$account_id, "user_id"=>$user_id, "type"=>$activity_type2, "item_name"=>$goal->title, "special"=>$special];
                // $this->addRecentActivity($activity_data);
                $activity_data['current_lang'] = $this->current_lang;
                $activity_data['site_id'] = $this->site_id;
                QueuesManager::sendToEmailQueue(__CLASS__, "addRecentActivity", $activity_data);
            }
        }
        if(!empty($end_date))
        {
            $goal->end_date = date("Y-m-d H:i:s", strtotime($end_date));
            if($old && $goal_id && $is_published && date("Y-m-d", strtotime($goal->end_date)) != date("Y-m-d", strtotime($old->end_date)))
            {
                $activity_type2 = Goal::activityTypes["goal_deadline_edited"];
                $special = date("M d, Y", strtotime($end_date))."||".GetActivities::_SpanishDate(strtotime($end_date), 'spanish_day_month_year');
                $desc2 = "edited the Goal Deadline for $title to ".$special." on {CREATED_DATE}.";
                $activity_data = ["goal_id"=>$goal->id, "desc"=>$desc2, "account_folder_id"=>$goal->account_folder_id, "account_id"=>$account_id, "user_id"=>$user_id, "type"=>$activity_type2, "item_name"=>$goal->title, "special"=>$special];
                // $this->addRecentActivity($activity_data);
                $activity_data['current_lang'] = $this->current_lang;
                $activity_data['site_id'] = $this->site_id;
                QueuesManager::sendToEmailQueue(__CLASS__, "addRecentActivity", $activity_data);
            }
        }
        $goal->last_edit_by = $user_id;
        $already_published = empty($goal_id) ? 0 : $goal->is_published;
        if($is_published && !$already_published)
        {
            $goal->publish_date = date("Y-m-d H:i:s");
        }
        $goal->is_published = $is_published;
        $goal->save();
        $request->merge(['goal_id' => $goal->id]);
        if(!$goal_id || empty($goal_id))
        {
            $this->addGoalOwners($request, 1);
            $this->addGoalCollaborators($request, 1);
        }

        $new_cats = [];
        foreach ($categories as $category_id)
        {
            $new_cats[] = ["goal_id"=> $goal->id, "category_id"=> $category_id];
        }
        GoalCategory::where("goal_id", $goal->id)->delete();
        if(!empty($new_cats))
        {
            GoalCategory::insert($new_cats);
        }
        if(!empty($template_id))
        {
            $items = GoalItem::with("allowedEvidence")->where("goal_id", $template_id)->get();
            foreach ($items as $action_item)
            {
                $item = new GoalItem();
                $item->goal_id = $goal->id;
                $item->account_id = $account_id;
                $item->created_by = $user_id;
                $item->site_id = $this->site_id;
                $item->title = $action_item->title;
                $item->description = $action_item->description;
                $item->deadline = $goal->end_date;
                $item->add_measurement = $action_item->add_measurement;
                $item->measurement_type = $action_item->measurement_type;
                $item->start_value = $action_item->start_value;
                $item->end_value = $action_item->end_value;
                $item->last_edit_by = $user_id;
                $item->save();
                $new_evidence = [];
                foreach ($action_item->allowedEvidence as $allowed)
                {
                    $new_evidence[] = ["goal_item_id"=> $item->id, "goal_id"=> $goal->id, "evidence_type"=> $allowed->evidence_type];
                }
                if(!empty($new_evidence))
                {
                    GoalItemEvidence::insert($new_evidence);
                }
            }
        }
        $goal = $this->getSingleGoal($goal->id);
        $activity_type = 0;
        $new_owners = $goal->owners;
        if($goal->goal_type == Goal::goalTypeIndividual)
        {
            if(count($new_owners) == 1)
            {
                if($new_owners[0]->user_id == $user_id)
                {
                    $activity_type = Goal::activityTypes["created_for_self"];
                    $desc = "created the personal goal $title on {CREATED_DATE}.";
                }
                else
                {
                    $activity_type = Goal::activityTypes["created_for_someone"];
                    $other_user_id = $new_owners[0]->user_id;
                    $desc = "created an individual goal for {OTHER_USER_NAME} called $title on {CREATED_DATE}.";
                }
            }
            else if(count($new_owners) > 1)
            {
                $activity_type = Goal::activityTypes["created_for_multiple"];
                $desc = "created an individual goal for multiple people called $title on {CREATED_DATE}.";
            }
        }
        else if($goal->goal_type == Goal::goalTypeGroup)
        {
            $activity_type = Goal::activityTypes["created_group_goal"];
            $desc = "created a Group Goal called $title on {CREATED_DATE}.";
        }
        elseif($goal->goal_type == Goal::goalTypeAccount)
        {
            $activity_type = Goal::activityTypes["created_account_goal"];
            $desc = "created a Account Goal called $title on {CREATED_DATE}.";
        }
        elseif($goal->goal_type == Goal::goalTypeTemplate)
        {
            $activity_type = Goal::activityTypes["created_template_goal"];
            $desc = "created a Goal Template called $title on {CREATED_DATE}.";
        }
        if(((!$goal_id && $is_published || ($goal_id && !$already_published && $is_published)) && ($activity_type == Goal::activityTypes["created_for_self"] || $activity_type == Goal::activityTypes["created_account_goal"])) || (!$goal_id && in_array($activity_type, [Goal::activityTypes["created_for_someone"], Goal::activityTypes["created_for_multiple"], Goal::activityTypes["created_group_goal"], Goal::activityTypes["created_template_goal"]])))//Activity for self individual goal only added when published
        {
            $activity_data = ["goal_id"=>$goal->id, "desc"=>$desc, "account_folder_id"=>$goal->account_folder_id, "account_id"=>$account_id, "user_id"=>$user_id, "type"=>$activity_type, "item_name" => $goal->title];
            if(isset($other_user_id))
            {
                $activity_data["other_user_id"] = $other_user_id;
            }
            // $this->addRecentActivity($activity_data);
            $activity_data['current_lang'] = $this->current_lang;
            $activity_data['site_id'] = $this->site_id;
            QueuesManager::sendToEmailQueue(__CLASS__, "addRecentActivity", $activity_data);
        }
        $event = [
            'channel' => "goals_".$account_id,
            'event' => $goal_id ? "goal_edited" : "goal_created",
            'action' => $goal_id ? 'edit' : 'add',
            'data' => $goal,
            'uuid' => $request->has("uuid") ? $request->get("uuid") : $goal_id,
            'item_id' => $goal->id,
            'goal_id' => $goal->id,
            'from_goals' => true
        ];
        
        $this->create_churnzero_event('Goal+Created', $account_id, $user_email);
        HelperFunctions::broadcastEvent($event,"broadcast_event");
        $event['channel'] = "goal-".$goal->id;
        HelperFunctions::broadcastEvent($event,"broadcast_event");
        return response()->json(["data"=>$goal, "message"=>$this->language_based_content["goals_created"], "success"=> true]);
    }

    public function addGoalOwners(Request $request, $internal = 0)
    {
        $goal_id = $request->get("goal_id");
        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");
        $owners = $request->get("owners");
        $groupIds = $request->get("ownerGroupIds");
        $goal = Goal::where("id", $goal_id)->where("is_active",1)->first();
        if(!empty($owners) && !is_array($owners))
        {
            $owners = explode(",", $owners);
        }
        if(!empty($groupIds) && !is_array($groupIds))
        {
            $groupIds = explode(",", $groupIds);
        }
        $new_owners = [];
        $new_users = [];
        if(!empty($owners))
        {
            foreach ($owners as $owner_id)
            {
                $new_users[] = $owner_id;
                $new_owners[] = ["goal_id" => $goal_id, "account_id" => $account_id, "user_id"=> $owner_id, "user_type"=> Goal::userTypeOwner, "created_by"=> $user_id];
            }
        }

        if(!empty($groupIds))
        {
            $group_users_ids = UserGroup::get_group_users($groupIds);
            //Following Query filter out users that are not viewers (role 125)
            $group_users_ids = UserAccount::whereIn("user_id", $group_users_ids)->where("account_id", $account_id)->where("role_id", "!=", 125)->pluck("user_id");
            foreach($group_users_ids as $group_users_id){
                if(in_array($group_users_id,$owners))
                {
                    continue;//don't add duplicate users
                }
                $new_users[] = $group_users_id;
                $new_owners[] = ["goal_id" => $goal_id, "account_id" => $account_id, "user_id"=> $group_users_id, "user_type"=> Goal::userTypeOwner, "created_by"=> $user_id];
            }
            AccountFolderGroup::where("account_folder_id", $goal->account_folder_id)->where("role_id",210)->delete();
            if(!is_array($groupIds))
            {
                $groupIds = explode(",",$groupIds);
            }
            foreach ($groupIds as $groupId)
            {
                $GoalGroups = ["account_folder_id" => $goal->account_folder_id, "group_id"=>$groupId, "role_id" => 210, "created_date"=>date("Y-m-d H:i:s"), "created_by"=>$user_id, "site_id"=>$this->site_id];
            }
            if(!empty($GoalGroups))
            {
                AccountFolderGroup::insert($GoalGroups);
            }
        }
        $existing_users = GoalUser::where("goal_id", $goal_id)->where("account_id", $account_id)->where("user_type", Goal::userTypeOwner)->pluck("user_id")->toArray();
        GoalUser::where("goal_id", $goal_id)->where("account_id", $account_id)->where("user_type", Goal::userTypeOwner)->delete();
        if(!empty($new_owners))
        {
            GoalUser::insert($new_owners);
        }

        if(!$internal)
        {
            $goal = $this->getSingleGoal($goal_id);
            $exact_new_users = array_diff($new_users, $existing_users);
            $title = $goal->title;
            $numOfUsers = count($exact_new_users);
            if($numOfUsers)
            {
                if($goal->goal_type == Goal::goalTypeIndividual)
                {
                    if($numOfUsers == 1 && $exact_new_users[0] != $goal->created_by)
                    {
                        $activity_type = Goal::activityTypes["created_for_someone"];
                        $other_user_id = $exact_new_users[0];
                        $desc = "created an individual goal for {OTHER_USER_NAME} called $title on {CREATED_DATE}.";
                    }
                    else if($numOfUsers > 1)
                    {
                        $activity_type = Goal::activityTypes["created_for_multiple"];
                        $desc = "added multiple people in individual goal called $title on {CREATED_DATE}.";
                    }
                }
                else if($goal->goal_type == Goal::goalTypeGroup)
                {
                    if($numOfUsers == 1)
                    {
                        $subject = "{OTHER_USER_NAME}";
                        $other_user_id = $exact_new_users[0];
                    }
                    else
                    {
                        $subject = "multiple people";
                    }
                    $activity_type = Goal::activityTypes["created_group_goal"];
                    $desc = "added $subject in Group Goal called $title on {CREATED_DATE}.";
                }
                if($goal->is_published && isset($activity_type))//Activity for self individual goal only added when published
                {
                    $activity_data = ["goal_id"=>$goal->id, "desc"=>$desc, "account_folder_id"=>$goal->account_folder_id, "account_id"=>$account_id, "user_id"=>$user_id, "type"=>$activity_type, "item_name"=>$title];
                    if(isset($other_user_id))
                    {
                        $activity_data["other_user_id"] = $other_user_id;
                    }
                    $activity_data['send_email'] = false;
                    // $this->addRecentActivity($activity_data);
                    $activity_data['current_lang'] = $this->current_lang;
                    $activity_data['site_id'] = $this->site_id;
                    QueuesManager::sendToEmailQueue(__CLASS__, "addRecentActivity", $activity_data);
                }
            }

            $event = [
                'channel' => "goals_".$account_id,
                'event' => "goal_edited",
                'action' => "updated_owners",
                'data' => $goal,
                'item_id' => $goal->id,
                'goal_id'=>$goal->id,
                'from_goals' => true
            ];
            HelperFunctions::broadcastEvent($event,"broadcast_event",false,false);
            $event['channel'] = "goal-".$goal_id;
            HelperFunctions::broadcastEvent($event,"broadcast_event",false,false);
        }
        return response()->json(["data"=>true, "message"=>$this->language_based_content["goals_owners_updated"], "success"=> true]);
    }

    public function addGoalCollaborators(Request $request, $internal = 0)
    {
        $goal_id = $request->get("goal_id");
        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");
        $collaborators = $request->get("collaborators");
        $collaboratorsGroupIds = $request->get("collaboratorsGroupIds");
        if(!empty($collaborators) && !is_array($collaborators))
        {
            $collaborators = json_decode($collaborators, true);
        }
        if(!empty($collaboratorsGroupIds) && !is_array($collaboratorsGroupIds))
        {
            $collaboratorsGroupIds = json_decode($collaboratorsGroupIds, true);
        }
        $goal = Goal::where("id", $goal_id)->where("is_active",1)->first();
        $new_owners = [];
        $already_added = [];
        if(!empty($collaborators))
        {
            foreach ($collaborators as $collaborator)
            {
                $already_added[] = $collaborator["user_id"];
                if(is_array($collaborator["permission"]))
                {
                    $collaborator["permission"] = implode(",", $collaborator["permission"]);
                }
                if(!empty($collaborator["permission"]))
                {
                    $new_owners[] = ["goal_id" => $goal_id, "account_id" => $account_id, "user_id"=> $collaborator["user_id"], "user_type"=> Goal::userTypeCollaborator, "permission" => $collaborator["permission"], "created_by"=> $user_id];
                }
            }
        }
        $groupIds = [];
        if(!empty($collaboratorsGroupIds))
        {
            foreach ($collaboratorsGroupIds as $group)
            {
                $groupIds[] = isset($group["group_id"])?$group["group_id"]:0;
            }
            $group_users_ids = $this->get_group_users($groupIds);
            foreach($group_users_ids as $group_users_id){
                $c_user_id = $group_users_id->user_id;
                foreach ($collaboratorsGroupIds as $group)
                {
                    if(isset($group["group_id"]) && $group["group_id"] == $group_users_id->group_id)
                    {
                        $permission = isset($group["permission"])?$group["permission"]:0;
                        if(is_array($permission))
                        {
                            $permission = implode(",", $permission);
                        }
                        break;
                    }

                }

                if(in_array($c_user_id,$already_added))
                {
                    continue;//don't add duplicate users
                }
                if(!empty($permission))
                {
                    $allowedPermission = $this->getAllowedPermissions($c_user_id, $account_id, $permission);
                    if($allowedPermission)
                    {
                        $new_owners[] = ["goal_id" => $goal_id, "account_id" => $account_id, "user_id"=> $c_user_id, "user_type"=> Goal::userTypeCollaborator, "permission" => $allowedPermission, "created_by"=> $user_id];
                    }
                }
            }
            AccountFolderGroup::where("account_folder_id", $goal->account_folder_id)->where("role_id",200)->delete();
            foreach ($groupIds as $groupId)
            {
                $GoalGroups = ["account_folder_id" => $goal->account_folder_id, "group_id"=>$groupId, "role_id" => 200, "created_date"=>date("Y-m-d H:i:s"), "created_by"=>$user_id, "site_id"=>$this->site_id];
            }
            if(!empty($GoalGroups))
            {
                AccountFolderGroup::insert($GoalGroups);
            }
        }

        $existing_users = GoalUser::where("goal_id", $goal_id)->where("account_id", $account_id)->where("user_type", Goal::userTypeCollaborator)->pluck("permission","user_id")->toArray();
        
        GoalUser::where("goal_id", $goal_id)->where("account_id", $account_id)->where("user_type", Goal::userTypeCollaborator)->delete();
        if(!empty($new_owners))
        {
            GoalUser::insert($new_owners);
        }
        $creator_exist = GoalUser::where("goal_id", $goal_id)->where("account_id", $account_id)->where("user_id", $goal->created_by)->first();
        if($internal && !$creator_exist)
        {
            GoalUser::insert(["goal_id" => $goal_id, "account_id" => $account_id, "user_id"=> $goal->created_by, "user_type"=> Goal::userTypeCollaborator, "permission" => "1,2", "created_by"=> $user_id]);
        }
        $new_users = GoalUser::where("goal_id", $goal_id)->where("account_id", $account_id)->where("user_type", Goal::userTypeCollaborator)->pluck("permission","user_id")->toArray();
        
        $exact_new_users = array_diff_assoc($new_users, $existing_users);

        foreach ($exact_new_users as $user => $whole_permission)
        {
            $activity_type = Goal::activityTypes["added_collaborator"];
            $permissions = explode(",", $whole_permission);
            $right = "";
            $right_es = "";
            foreach ($permissions as $permission)
            {
                if($permission == GoalUser::permissionEdit)
                {
                    $right .= "Edit rights, ";
                    $right_es .= "derechos para Editar, ";
                }
                else if($permission == GoalUser::permissionEvidence)
                {
                    $right .= "rights to Add evidence to Action Items, ";
                    $right_es .= "derechos de Agregar evidencia a acciones, ";
                }
                else if($permission == GoalUser::permissionReview)
                {
                    $right .= "Review rights, ";
                    $right_es .= "derechos de Revisar, ";
                }
                else
                {
                    $right .= "View rights, ";
                    $right_es .= "derechos para ver, ";
                }
            }
            $right = rtrim($right,", ");
            $right_es = rtrim($right_es,", ");
            $right = $right."||".$right_es;

            $desc = "added {OTHER_USER_NAME} to $goal->title as a Collaborator with $right on {CREATED_DATE}.";

            $activity_data = ["goal_id"=>$goal->id, "desc"=>$desc, "account_folder_id"=>$goal->account_folder_id, "account_id"=>$account_id, "user_id"=>$user_id, "type"=>$activity_type, "other_user_id" => $user, "item_name"=>$goal->title, "special"=>$right];
            if((in_array(GoalUser::permissionEdit, $permissions) || $goal->is_published))
            {
                // $this->addRecentActivity($activity_data);
                $activity_data['current_lang'] = $this->current_lang;
                $activity_data['site_id'] = $this->site_id;
                QueuesManager::sendToEmailQueue(__CLASS__, "addRecentActivity", $activity_data);
            }
        }
        if(!$internal)
        {
            $goal = $this->getSingleGoal($goal_id);
            $event = [
                'channel' => "goals_".$account_id,
                'event' => "goal_edited",
                'action' => "updated_collaborators",
                'data' => $goal,
                'item_id' => $goal->id,
                'goal_id' => $goal->id,
                'from_goals' => true
            ];
            HelperFunctions::broadcastEvent($event,"broadcast_event");
            $event['channel'] = "goal-".$goal_id;
            HelperFunctions::broadcastEvent($event,"broadcast_event");
        }
        return response()->json(["data"=>true, "message"=>"Added new Goal Collaborators!", "success"=> true]);
    }

    public function  getAllowedPermissions($user_id,$account_id, $permissions)
    {
        if(!is_array($permissions))
        {
            $permissions = explode(",", $permissions);
        }
        $goal_user_settings = GoalUserSettings::join("users_accounts", function ($join){
            $join->on("goal_users_settings.user_id", "=", "users_accounts.user_id");
            $join->on("goal_users_settings.account_id", "=", "users_accounts.account_id");
        })
            ->where("goal_users_settings.user_id", $user_id)
            ->where("goal_users_settings.account_id", $account_id)
            ->where("goal_users_settings.site_id", $this->site_id)
            ->where("users_accounts.role_id", "!=", 125)->first();
        if($goal_user_settings)
        {
            $notAllowed = [];
            if(!$goal_user_settings->can_be_added_as_goal_collab_with_view_rights)
            {
                $notAllowed[] = GoalUser::permissionView;
            }
            if(!$goal_user_settings->can_be_added_as_goal_collab_with_edit_rights)
            {
                $notAllowed[] = GoalUser::permissionEdit;
            }
            if(!$goal_user_settings->can_be_added_as_goal_collab_with_right_to_add_evidence)
            {
                $notAllowed[] = GoalUser::permissionEvidence;
            }
            if(!$goal_user_settings->can_be_added_as_goal_collab_with_right_to_review_feedback)
            {
                $notAllowed[] = GoalUser::permissionReview;
            }
            $allowedPermissions = array_diff($permissions,  $notAllowed);
            if(empty($allowedPermissions))
            {
                $allowedPermissions = 0;
            }
            else
            {
                $allowedPermissions = implode(",", $allowedPermissions);
            }
        }
        else
        {
            $allowedPermissions = 0;
        }
        return $allowedPermissions;
    }

    private static function get_group_users($group_ids = []){
        settype($group_ids, "array");
        return UserGroup::whereIn('group_id',$group_ids)->distinct()->select('user_id', 'group_id')->get();
    }

    public function getGoals(Request $request, $all = false, $getDataOnly = false)
    {
        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");

        $page = $request->has("page") ? $request->get("page") : 0;
        $goal_settings = $this->getGoalsSettings($account_id, 1);
        $goal_user_settings = GoalUserSettings::where("user_id", $user_id)->where("account_id", $account_id)->where("site_id", $this->site_id)->first();
        $category_id = ($request->has("category_id") && !empty($request->has("category_id")) && $request->get("category_id") != "null") ? $request->get("category_id") : "" ;
        $search = ($request->has("search") && !empty($request->get("search")) && $request->get("search") != "null") ? $request->get("search") : "" ;
        $goal_type = ($request->has("goal_type") && !empty($request->get("goal_type")) && $request->get("goal_type") != "null") ? $request->get("goal_type") : null ;
        $sort_by = ($request->has("sort_by") && !empty($request->get("sort_by")) && $request->get("sort_by") != "null") ? $request->get("sort_by") : "created_date" ;
        $start_date = ($request->has("start_date") && !empty($request->get("start_date")) && $request->get("start_date") != "null") ? date("Y-m-d", strtotime($request->get("start_date"))) : null ;
        $end_date = ($request->has("end_date") && !empty($request->get("end_date")) && $request->get("end_date") != "null") ? date("Y-m-d", strtotime($request->get("end_date"))) : null ;
        $user_email = User::where('id',$user_id)->pluck('email')->first();
        $goal_user_type = GoalUser::where('user_id',$user_id)->pluck('user_type')->first();
        $filter_goals = $request->has("my_goals") ? $request->get("my_goals") : [1];
        if(!is_array($filter_goals) && !empty($filter_goals)){
            $filter_goals = [1];
        }
        $last_update = ($request->has("last_update") && !empty($request->get("last_update")) && $request->get("last_update") != "null") ? date("Y-m-d", strtotime($request->get("last_update"))) : null ;

        if($category_id && !is_array($category_id))
        {
            $category_id = explode(",", $category_id);
        }
        if($goal_type && !is_array($goal_type))
        {
            $goal_type = explode(",", $goal_type);
        }
        $search_required = false;
        if(!empty($search) || !empty($goal_type) || !empty($start_date) || !empty($end_date) || !empty($category_id) || ($sort_by!="created_date")){
            $search_required = true;
        }
        $user_role = UserAccount::join("accounts", "accounts.id", "=", "UserAccount.account_id")->select("UserAccount.*", "accounts.enable_goals")->where("account_id",$account_id)->where("UserAccount.user_id",$user_id)->where("UserAccount.site_id",$this->site_id)->first();
        if(!$user_role->enable_goals)
        {
            return response()->json(["data"=>[], "message"=>"Goals are disabled for this account", "success"=> false]);
        }
        //DB::enableQueryLog();
        
        $main_query = Goal::where("goals.account_id",$account_id)->where("goals.is_active",1)
                ->where(function ($sub_query) use ($user_id,$goal_settings,$user_role,$goal_user_settings){
                $sub_query->where(function ($sub_query_1) use ($user_id,$goal_settings,$user_role,$goal_user_settings){
                    $sub_query_1->where("goals.is_published", 1)   
                                            ->where(function ($sub_query_2) use ($user_id,$goal_settings,$user_role,$goal_user_settings) {
                            if(!($goal_settings->make_all_goals_public || $goal_user_settings->can_view_goal_progress) && $user_role->role_id != User::roleAccountOwner && $user_role->role_id != User::roleSuperAdmin)
                            {
                                $sub_query_2->where( function ($query) use ($user_id,$goal_settings,$user_role)
                                {

                                    if(!$goal_settings->group_type_goals_public && !$goal_settings->individual_type_goals_public && !$goal_settings->account_type_goals_public)
                                    {
                                        $query->whereRaw(" EXISTS (SELECT * FROM `goal_users` WHERE `goals`.`id` = `goal_users`.`goal_id` AND `goal_users`.`user_id` = $user_id)");
                                        /*$query->where(function ($sub_query) use ($user_id){
                                            $sub_query->where("goals.goal_type",Goal::goalTypeTemplate)->orWhere("goal_users.user_id", $user_id);
                                        });*/
                                    }
                                    else
                                    {
                                        if(!$goal_settings->group_type_goals_public)
                                        {
                                            $query->where(function ($sub_query) use ($user_id){
                                                $sub_query->where("goals.goal_type","!=",Goal::goalTypeGroup)->orWhere(function ($s_sub_query) use ($user_id){
                                                    $s_sub_query->where("goals.goal_type", Goal::goalTypeGroup)->whereRaw(" EXISTS (SELECT * FROM `goal_users` WHERE `goals`.`id` = `goal_users`.`goal_id` AND `goal_users`.`user_id` = $user_id)");
                                                });
                                            });
                                        }
                                        if(!$goal_settings->individual_type_goals_public)
                                        {
                                            $query->where(function ($sub_query) use ($user_id){
                                                $sub_query->where("goals.goal_type","!=",Goal::goalTypeIndividual)->orWhere(function ($s_sub_query) use ($user_id){
                                                    $s_sub_query->where("goals.goal_type", Goal::goalTypeIndividual)->whereRaw(" EXISTS (SELECT * FROM `goal_users` WHERE `goals`.`id` = `goal_users`.`goal_id` AND `goal_users`.`user_id` = $user_id)");
                                                });
                                            });
                                        }
                                        if(!$goal_settings->account_type_goals_public)
                                        {
                                            $query->where(function ($sub_query) use ($user_id){
                                                $sub_query->where("goals.goal_type","!=",Goal::goalTypeAccount)->orWhere(function ($s_sub_query) use ($user_id){
                                                    $s_sub_query->where("goals.goal_type", Goal::goalTypeAccount)->whereRaw(" EXISTS (SELECT * FROM `goal_users` WHERE `goals`.`id` = `goal_users`.`goal_id` AND `goal_users`.`user_id` = $user_id)");
                                                });
                                            });
                                        }
                                    }
                                })->orWhereHas("owners", function ($sub_query_4) use ($user_id){
                                    $sub_query_4->where("goal_users.user_id", $user_id);
                                })->orWhere("goals.goal_type", Goal::goalTypeAccount);
                            }
                        });
                })->orWhere(function ($sub_query_3) use ($user_id, $goal_user_settings){
                    $sub_query_3->where("goals.created_by", $user_id)
                    ->orWhereHas("collaborators", function ($sub_query_4) use ($user_id){
                        $sub_query_4->where("goal_users.user_id", $user_id)->where("goal_users.permission", 'like', "%2%");
                    });
                    if($goal_user_settings->can_use_templates_to_create_new_goals)
                    {
                        $sub_query_3->orWhere("goals.goal_type", Goal::goalTypeTemplate);
                    }
                });
            }) 
            ->where("goals.site_id", $this->site_id);
                    
        $query = clone $main_query;
        $goal_types = [];
        if($page == 0)
        {
            $goal_types = $main_query->groupBy("goal_type")->pluck("goal_type");
        }
        
        $query->with(Goal::getGoalRelations(1,1,1,1, 1))
            ->select("goals.*", DB::raw("(SELECT SUM(goal_items_feedback.`is_done`) FROM goal_items_feedback WHERE goal_id = goals.`id`) AS completed_items, (SELECT COUNT(goal_items.id) FROM goal_items WHERE goal_id = goals.`id`) AS total_items "));



        if($category_id)
        {
            $query->whereHas("categories", function ($q) use ($category_id){
                $q->whereIn("goal_categories.category_id", $category_id);
            });
        }

        if(in_array(2, $filter_goals) && !in_array(3, $filter_goals) && !in_array(1, $filter_goals)){//My goals(owner)
            // Based on our discussion, in case of All Goals we should show all goals (except archived) no matter if My Goals or My Collaborations is also selected.
            $query->WhereHas("owners", function ($qq) use ($user_id){
                $qq->Where("goal_users.user_id",$user_id);
            });
            // ->orWhere(
            //     "goals.goal_type",Goal::goalTypeAccount
            // );
        }
        if(in_array(3, $filter_goals) && !in_array(2, $filter_goals) && !in_array(1, $filter_goals)){//My collaborations
            // Based on our discussion, in case of All Goals we should show all goals (except archived) no matter if My Goals or My Collaborations is also selected.
            $query->WhereHas("collaborators", function ($qq) use ($user_id){
                $qq->Where("goal_users.user_id",$user_id);
            });
        }
        if(in_array(2, $filter_goals) && in_array(3, $filter_goals) && !in_array(1, $filter_goals) && !$search_required){
            // Based on our discussion, in case of All Goals we should show all goals (except archived) no matter if My Goals or My Collaborations is also selected.
            // !$search_required is a Jugaarr to Skip following queries because OR condition is creating problem for Search/Sort etc in case of multi select.
            $query->WhereHas("owners", function ($qq) use ($user_id, $account_id){
                        $qq->Where("goal_users.user_id",$user_id)->where("goals.account_id", $account_id)->where("goals.is_active", "1");
                    })->orWhereHas("collaborators", function ($qq) use ($user_id, $account_id){
                        $qq->Where("goal_users.user_id",$user_id)->where("goals.account_id", $account_id)->where("goals.is_active", "1");
                    });
        }
        if((count($filter_goals)==1 && in_array(4, $filter_goals)) || (count($filter_goals)>1 && in_array(4, $filter_goals) && in_array(1, $filter_goals)) && !$search_required){// Only Archived or Archived with All Goals.
            // !$search_required is a Jugaarr to Skip following queries because OR condition is creating problem for Search/Sort etc in case of multi select.
            $query->where( function ($qq2) use ($user_id, $filter_goals,$goal_settings,$goal_user_settings, $user_role){
                $qq2->where( function ($qq) use ($user_id){
                    $qq->where(function ($q) use ($user_id){
                        $q->where("goals.goal_type",'!=', 4)->where('goals.is_archived', 1)->where(function ($q1) use ($user_id){
                            $q1->where('goals.created_by', $user_id)
                                ->orWhereHas("collaborators", function ($sub_query_4) use ($user_id){
                                    $sub_query_4->where("goal_users.user_id", $user_id)->where("goal_users.permission",'like', "%2%");
                                })
                                ->orWhereHas("owners", function ($sub_query_4) use ($user_id){
                                    $sub_query_4->where("goal_users.user_id", $user_id);
                                });
                        });
                    })->orWhere(function ($q) use ($user_id){
                        $q->where("goals.goal_type", 4)->where(function ($nq) use ($user_id){
                            $nq->WhereHas("owners", function ($qqq) use ($user_id){
                                $qqq->Where("goal_users.user_id",$user_id)->Where("goal_users.is_archived",1);
                            })
                                ->orWhereHas("collaborators", function ($sub_query_4) use ($user_id){
                                    $sub_query_4->where("goal_users.user_id", $user_id)->where("goal_users.permission",'like', "%2%")->whereRaw("(select count(*) from goal_users where user_type = 2 and goal_id = goals.id and is_archived = 1) > 0");
                                })
                                ->orWhere(function ($nq2) use ($user_id){
                                    $nq2->where('goals.created_by', $user_id)->whereRaw("(select count(*) from goal_users where user_type = 2 and goal_id = goals.id and is_archived = 1) > 0");
                                });
                        });
                    });
                });

                // In case of All Goals, we need to Add All goals with Archived.
                if(in_array(1, $filter_goals))
                {
                    if($goal_settings->make_all_goals_public || $goal_settings->individual_type_goals_public || $goal_user_settings->can_view_goal_progress || $user_role->role_id == User::roleAccountOwner || $user_role->role_id == User::roleSuperAdmin){
                    $qq2->orWhere( function ($qq) use ($user_id){
                        $qq->where(function ($q){
                            $q->where('goals.is_archived', 0)->where("goals.goal_type", '!=' ,4);
                        })->orWhere(function ($q) use ($user_id){
                            $q->where("goals.goal_type", 4)->WhereHas("owners", function ($qqq) use ($user_id){
                                $qqq->Where("goal_users.is_archived",0);
                            });
                        })->orWhere(function ($q) use ($user_id){
                            $q->where("goals.goal_type", 4)->WhereHas("collaborators", function ($qqq) use ($user_id){
                                $qqq->whereRaw('(select count(*) from goal_users where user_type = 2 and is_archived = 0 and goal_users.goal_id = goals.id) > 0');
                                //$qqq->Where("goal_users.user_id",$user_id)->Where("goal_users.is_archived",0);
                            });
                        });
                    });

                    }else{
                    $qq2->orWhere( function ($qq) use ($user_id){
                        $qq->where(function ($q){
                            $q->where('goals.is_archived', 0)->where("goals.goal_type", '!=' ,4);
                        })->orWhere(function ($q) use ($user_id){
                            $q->where("goals.goal_type", 4)->WhereHas("owners", function ($qqq) use ($user_id){
                                $qqq->Where("goal_users.user_id",$user_id)->Where("goal_users.is_archived",0);
                            });
                        })->orWhere(function ($q) use ($user_id){
                            $q->where("goals.goal_type", 4)->WhereHas("collaborators", function ($qqq) use ($user_id){
                                $qqq->Where("goal_users.user_id",$user_id)->whereRaw('(select count(*) from goal_users where user_type = 2 and is_archived = 0 and goal_users.goal_id = goals.id) > 0');
                                //$qqq->Where("goal_users.user_id",$user_id)->Where("goal_users.is_archived",0);
                            });
                        });
                    });
                }
                }
            });
        }elseif(count($filter_goals)>1 && in_array(4, $filter_goals) && !$search_required){//Archived but also have other options selected
            // !$search_required is a Jugaarr to Skip following queries because OR condition is creating problem for Search/Sort etc in case of multi select.
            $query->orWhere( function ($qq2) use ($user_id, $account_id, $filter_goals){
                $qq2->where( function ($qq) use ($user_id, $account_id){
                    $qq->where(function ($q) use ($user_id, $account_id){
                        $q->where("goals.account_id", $account_id)->where("goals.goal_type",'!=', 4)->where('goals.is_archived', 1)->where(function ($q1) use ($user_id){
                            $q1->where('goals.created_by', $user_id)
                                ->orWhereHas("collaborators", function ($sub_query_4) use ($user_id){
                                    $sub_query_4->where("goal_users.user_id", $user_id)->where("goal_users.permission",'like', "%2%");
                                })
                                ->orWhereHas("owners", function ($sub_query_4) use ($user_id){
                                    $sub_query_4->where("goal_users.user_id", $user_id);
                                });
                        });
                    })->orWhere(function ($q) use ($user_id, $account_id){
                        $q->where("goals.account_id", $account_id)->where("goals.goal_type", 4)->where(function ($nq) use ($user_id){
                            $nq->WhereHas("owners", function ($qqq) use ($user_id){
                                $qqq->Where("goal_users.user_id",$user_id)->Where("goal_users.is_archived",1);
                            })
                            ->orWhereHas("collaborators", function ($sub_query_4) use ($user_id){
                                $sub_query_4->where("goal_users.user_id", $user_id)->where("goal_users.permission",'like', "%2%")->whereRaw("(select count(*) from goal_users where user_type = 2 and goal_id = goals.id and is_archived = 1) > 0");
                            })
                            ->orWhere(function ($nq2) use ($user_id){
                                $nq2->where('goals.created_by', $user_id)->whereRaw("(select count(*) from goal_users where user_type = 2 and goal_id = goals.id and is_archived = 1) > 0");
                            });
                        });
                    });
                });
            });
        }
        elseif(!$search_required)
        {//All goals except archived -> $request->get("my_goals") == 1
            // !$search_required is a Jugaarr to Skip following queries because OR condition is creating problem for Search/Sort etc in case of multi select.
            if($goal_settings->make_all_goals_public || $goal_settings->individual_type_goals_public || $goal_user_settings->can_view_goal_progress || $user_role->role_id == User::roleAccountOwner || $user_role->role_id == User::roleSuperAdmin){
                $query->where( function ($qq) use ($user_id, $account_id){
                    $qq->where(function ($q){
                        $q->where('goals.is_archived', 0)->where("goals.goal_type", '!=' ,4);
                    })->orWhere(function ($q) use ($user_id, $account_id){
                        $q->where("goals.goal_type", 4)->where("goals.account_id", $account_id)->WhereHas("owners", function ($qqq) use ($user_id){
                            $qqq->Where("goal_users.is_archived",0);
                        });
                    })->orWhere(function ($q) use ($user_id, $account_id){
                        $q->where("goals.goal_type", 4)->where("goals.account_id", $account_id)->WhereHas("collaborators", function ($qqq) use ($user_id){
                            $qqq->whereRaw('(select count(*) from goal_users where user_type = 2 and is_archived = 0 and goal_users.goal_id = goals.id) > 0');
                        });
                    });
                });
            }else{
                $query->where( function ($qq) use ($user_id, $account_id){
                    $qq->where(function ($q){
                        $q->where('goals.is_archived', 0)->where("goals.goal_type", '!=' ,4);
                    })->orWhere(function ($q) use ($user_id, $account_id){
                        $q->where("goals.goal_type", 4)->where("goals.account_id", $account_id)->WhereHas("owners", function ($qqq) use ($user_id){
                            $qqq->Where("goal_users.user_id",$user_id)->Where("goal_users.is_archived",0);
                        });
                    })->orWhere(function ($q) use ($user_id, $account_id){
                        $q->where("goals.goal_type", 4)->where("goals.account_id", $account_id)->WhereHas("collaborators", function ($qqq) use ($user_id){
                            $qqq->Where("goal_users.user_id",$user_id)->whereRaw('(select count(*) from goal_users where user_type = 2 and is_archived = 0 and goal_users.goal_id = goals.id) > 0');
                            //$qqq->Where("goal_users.user_id",$user_id)->Where("goal_users.is_archived",0);
                        });
                    });
                });
            }
        }
        if($goal_type)
        {
            $query->whereIn("goals.goal_type", $goal_type);
        }
        //In case of mobile when No Template goals required
        if($request->has("without_template_goals")){
            $query->where("goals.goal_type", '!=' ,1);
        }
        
        //In case of mobile when No Unpublished goals required
        if($request->has("without_unpublished_goals")){
            $query->where("goals.is_published", '=' ,1);
        }
        
        if($search)
        {
            $search = addslashes(trim($search));
            $query->where(function ($q) use ($search) {
                $q->where("goals.title", "like", "%$search%")
                    ->orWhere("desc", "like", "%$search%")
                    ->orWhereHas("owners", function ($qq) use ($search){
                        $qq->join("users", "users.id", "=", "goal_users.user_id")
                            ->where("users.first_name", "like", "%$search%")
                            ->orWhere("users.last_name", "like", "%$search%")
                            ->orWhereRaw("CONCAT(users.first_name,' ', users.last_name) like '%$search%'");
                    })
                     ->orWhereHas("actionItems", function ($qqq) use ($search){
                            $qqq->where("goal_items.title", "like", "%$search%");
                    });
            });
        }

        if($start_date && !$end_date)
        {
            $query->whereRaw("DATE(goals.start_date)='$start_date'");
        }
        else if($end_date && !$start_date)
        {
            $query->whereRaw("DATE(goals.end_date)='$end_date'");
        }
        else if($end_date && $start_date)
        {
            $query->whereRaw("DATE(goals.start_date)>='$start_date'")->whereRaw("DATE(goals.end_date)<='$end_date'");
        }
        if($last_update){
            $query->whereRaw("DATE(goals.last_edit_date)='$last_update'");
        }
        if($sort_by == "complete" || $sort_by == "incomplete")
        {
            $is_complete = 1;
            $extra = "";
            if($sort_by == "incomplete")
            {
                $is_complete = 0;
                $extra = " AND goals.`end_date` < '".date("Y-m-d")."' ";
                // if($goal_user_type == '2' || $goal_type == '2'){
                // $this->create_churnzero_event('Goal+Incomplete', $account_id, $user_email);
                // }
            }
            $query->where(function ($sQuery) use ($is_complete, $extra, $user_id){
                $complete_query = self::getCompleteNotCompleteQuery($user_id);
                $sQuery->whereRaw("($complete_query  
                      WHERE goals.`goal_type` != 1 AND goals.is_published = 1 
                      AND (SELECT COUNT(*) FROM goal_items gi WHERE gi.goal_id = goals.`id`) > 0
                        $extra) = $is_complete");
            });
            // if($goal_user_type == '2' || $goal_type == '2'){
            // $this->create_churnzero_event('Goal+Incomplete', $account_id, $user_email);
            // }
        }else if($sort_by =='in_progress'){
            $query->whereRaw("((SELECT COUNT(*) FROM goal_items_feedback gif 
                        WHERE gif.goal_id = goals.`id` AND gif.is_done = 1 
                          AND ( goals.`goal_type` = 3 
                            OR ( goals.`goal_type` != 3 AND
                              If(goals.goal_type = 4 and (select count(*) from goal_users gu where gu.goal_id = goals.id and user_type = 2) > 1, $user_id , (select user_id from goal_users gu where gu.goal_id = goals.id and user_type = 2)) = gif.owner_id
                            )
                          )) NOT IN (IF((select count(*) from goal_item_submitted_evidence ge where ge.goal_id = goals.id), -1, 0), (SELECT COUNT(*) FROM goal_items gi 
                        WHERE gi.goal_id = goals.`id`))) AND goals.`goal_type` != 1 AND goals.is_published = 1 AND goals.end_date >= '".(date('Y-m-d')." 00:00:00")."' ");
        }
        // else if($sort_by == "not_updated")
        // {
        //     $query->where(function ($sQuery) use ($user_id){
        //         $complete_query = self::getCompleteNotCompleteQuery($user_id);
        //         $sQuery->whereRaw("($complete_query 
        //                ) != 1");
        //     })->orderBy("goals.last_edit_date", "ASC");
        // }

        else if($sort_by == "not_updated")
        {
            $query->has('actionItems')->whereDoesntHave('actionItems', function($actionItems) {
                return $actionItems->has('itemFeedbacks')->orHas('submittedEvidence');
            })->get();
        }
        
        else if($sort_by == "last_updated")
        {
            $query->orderBy("goals.last_edit_date", "DESC");
        }
        else if($sort_by == "due_date")
        {
            $query->where(function ($sQuery) use ($user_id){
                $complete_query = self::getCompleteNotCompleteQuery($user_id);
                $sQuery->whereRaw("($complete_query 
                       ) != 1");
            })->orderBy("goals.end_date", "ASC")->orderBy("goals.created_date", "DESC");
        }
        else if($sort_by == "title")
        {
            $query->orderBy("goals.title", "ASC");
        }
        else
        {
            $query->orderBy("goals.created_date", "DESC");
        }
       
        if($request->has("limit") && $request->input("limit") != ""){
            $perPage = $request->input("limit");
        }else{
            $perPage = 15;
        }
        
        $count_query = clone $query;
        if($all){
            $goals = $query->groupBy("goals.id")->get();
        }else{
            $goals = $query->groupBy("goals.id")->offset($perPage * $page)->limit($perPage)->get();
        }
        $total_goals = $count_query->count();
        $categories = false;
        if($page == 0)
        {
            $categories = Category::where("account_id", $account_id)->orderBY("created_date", "DESC")->get();
        }
        foreach ($goals as &$goal)
        {
            if($goal->completed_items > $goal->total_items)
            {
                $goal->completed_items = $goal->total_items;
            }
            if($goal->publish_date)
            {
                $goal->publish_date_updated = date("M d, Y");
            }
            $goal = $this->specialIndividualGoalWork($goal, $user_id, $goal_settings, $goal_user_settings, $filter_goals);//SW-3795
        }
        $executed_quires = "";//DB::getQueryLog();
        if ($getDataOnly) {
            return $goals;
        }
        return response()->json(["total_goals" => $total_goals,  "data" => $goals, "goal_types" => $goal_types, "categories" => $categories, "user_settings" => $goal_user_settings, "goal_settings" => $goal_settings, "message"=>"success", "success"=> true, "executed_quires" => $executed_quires]);
    }

    public static function getCompleteNotCompleteQuery($user_id)
    {
        return "SELECT ((SELECT COUNT(*) FROM goal_items gi 
                        WHERE gi.goal_id = goals.`id`) = 
                        (SELECT COUNT(*) FROM goal_items_feedback gif 
                        WHERE gif.goal_id = goals.`id` AND gif.is_done = 1 
                          AND ( goals.`goal_type` = 3 
                            OR ( goals.`goal_type` != 3 AND
                              If(goals.goal_type = 4 and (select count(*) from goal_users gu where gu.goal_id = goals.id and user_type = 2) > 1, $user_id , (select user_id from goal_users gu where gu.goal_id = goals.id and user_type = 2)) = gif.owner_id
                            )
                          ))) AS is_complete ";
    }

    public function specialIndividualGoalWork($goal, $user_id, $goal_settings = false, $goal_user_settings = false, $filter_goals = 1)//SW-3795
    {
        $goal->owners_updated = $goal->owners;
        if(!$goal_user_settings)
        {
            $goal_user_settings = GoalUserSettings::where("user_id", $user_id)->where("account_id", $goal->account_id)->first();
        }
        if(!$goal_settings)
        {
            $goal_settings = self::getPreferredAccountSettings($goal->account_id, $user_id);
        }
        if($goal)
        {
            $owners = $goal->owners->toArray();
            $unarchived_owners = [];
            if(count($owners) && $filter_goals != 4)
            {
                foreach ($owners as $owner)
                {
                    if(is_array($owner))
                    {
                        if($owner['is_archived'] != 1)
                        {
                            $unarchived_owners[] = $owner;
                        }
                    }
                    else
                    {
                        if($owner->is_archived != 1)
                        {
                            $unarchived_owners[] = $owner;
                        }
                    }
                }
            }
            else
            {
                $unarchived_owners = $owners;
            }

            $owners = $unarchived_owners;
            $goal->owners_updated = $unarchived_owners;

            $owner_index = HelperFunctions::searchArrayByValue($user_id,$owners);
            $goal->current_user_is_owner = false;
            $collaborators = $goal->collaborators->toArray();
            $goal->current_user_is_collaborator = false;
            if(count($collaborators) && HelperFunctions::searchArrayByValue($user_id,$collaborators) !== null)
            {
                $goal->current_user_is_collaborator = true;
            }
            if($owner_index !== null)
            {
                $goal->current_user_is_owner = true;
                if(count($collaborators) < 1 || !$goal->current_user_is_collaborator)
                {
                    if($goal->goal_type == Goal::goalTypeIndividual && !($goal_user_settings->can_view_goal_progress == 1 || $goal_settings->make_all_goals_public == 1 || $goal_settings->individual_type_goals_public == 1))
                    {
                        $goal->owners_updated = [$owners[$owner_index]];
                        $goal->completed_items = GoalItemsFeedback::where("goal_id", $goal->id)->where("owner_id", $user_id)->where("is_done",1)->count();
                        if($goal->actionItems)
                        {
                            $feedbacks = GoalItemsFeedback::select('goal_id','goal_item_id','owner_id','is_done','current_value','created_date','created_by','last_edit_date','last_edit_by','completed_at','is_completed_on_time')->where('owner_id', $user_id)->where('goal_id', $goal->id)->get();
                            foreach ($goal->actionItems as &$actionItem)
                            {
                                if($actionItem->itemFeedbacks && $actionItem->itemFeedbacks->owner_id != $user_id)
                                {
                                    unset($actionItem->itemFeedbacks);//We need to do this to override relation data
                                    $actionItem->item_feedbacks = $actionItem->itemFeedbacks = $feedbacks->where('owner_id', $user_id)->where('goal_id', $actionItem->goal_id)->where('goal_item_id', $actionItem->id)->first();
                                }


                            }

                        }
                    }
                }
            }
        }
        if($goal->completed_items == $goal->total_items && $goal->total_items > 0)
        {
            $goal->is_complete = 1;
            if(empty($goal->completion_date))
            {
                $goal->completion_date = date("Y-m-d H:i:s");
            }
        }
        else
        {
            $goal->is_complete = 0;
        }
        $this->formatGoal($goal, $user_id, $goal_user_settings, $goal_settings);
        if($goal->total_items > 0)
        {
            $goal->progress = round($goal->completed_items/$goal->total_items * 100);
        }
        else
        {
            $goal->progress = 0;
        }
        return $goal;
    }
    
    public function goalCompletedItem($goal_id,$user_id){
        $completed_items =GoalItemsFeedback::where("goal_id", $goal_id)->where("owner_id", $user_id)->where("is_done",1)->count();
         return response()->json(["complete_items"=>$completed_items,"message"=>"success", "success"=> true]);
    }

    public function getGoalDetails(Request $request)
    {
        //DB::enableQueryLog(); 
        $account_id = $request->get("account_id");
        $goal_id = $request->get("goal_id");
        $user_id = $request->get("user_id");
        $owner_id = $request->has("owner_id") ? $request->get("owner_id") : 0;
        $goal_user_settings = GoalUserSettings::where("user_id", $user_id)->where("account_id", $account_id)->first();
        $goal_settings = self::getPreferredAccountSettings($account_id, $user_id);
        $goal = $this->getSingleGoal($goal_id, $owner_id, $goal_settings, $goal_user_settings);
        $goal_user_sett = GoalSettings::where("account_id", $account_id)->pluck('goal_owner_setting')->first();


        //$goal = json_decode(json_encode($goal), False);
        
        if(!empty($goal) && $goal->goal_type == Goal::goalTypeTemplate){
         $goal->start_date = date("Y-m-d H:i:s");
         $goal->end_date = date('Y-m-d H:i:s', strtotime('1 year'));
         if(!empty($goal->actionItems)){
             foreach ($goal->actionItems as $item){
                 $item->deadline = date('Y-m-d H:i:s', strtotime('1 year'));
              }
        }
        }
        $categories = [];
        $all_users = [];
        if(!$owner_id)
        {
            $categories = Category::where("account_id", $account_id)->get();
            $c_request = new Request(["account_id"=> $account_id, "huddle_type" => 3, 'with_goal_permissions' => 1, 'avoid_to_array' => 1]);
            $all_users = app("App\Http\Controllers\ApiController")->get_all_users($c_request);
        }
        if($goal && $goal_settings && $goal_user_settings)
        {
            $goal->hide_action_item_details = 0;
            if(!$goal->current_user_is_collaborator && !$goal_settings->make_all_goals_public && $owner_id != $user_id && (($goal->goal_type == 3 && !$goal_settings->group_type_goals_public) || $goal->goal_type != 3) && (($goal->goal_type == 4 && !$goal_settings->individual_type_goals_public) || $goal->goal_type != 4) && (($goal->goal_type == 2 && !$goal_settings->account_type_goals_public) || $goal->goal_type != 2) && $goal_user_settings->can_view_goal_progress)
            {
                $goal->hide_action_item_details = 1;
            }
        }
        $executed_quires = '';//DB::getQueryLog();
       $goal_permssion = true;
        if($goal_id > 0 && ($goal ==null||$goal ==''||$goal ==false)){
            $goal_permssion = false;
        }
        return response()->json(["data"=>$goal, "all_categories"=> $categories, "all_users"=> $all_users, "message"=>"success","goal_permission"=>$goal_permssion,"goal_owner_setting"=>$goal_user_sett,'goal_user_privilleges'=>$goal_user_settings, "success"=> true, "executed_quires" => $executed_quires]);
    }

    public function exportActionItems(Request $request){

        $format = $request->get("format");
        $user_id = $request->get("user_id");
        $account_id = $request->get("account_id");

        $current_lang = $request->has("current_lang") ? $request->get("current_lang") : 'en';
        $translations = TranslationsManager::get_page_lang_based_content("global", $this->site_id, $user_id, $current_lang);

        $goalDetails = $this->getGoalDetails($request,true)->getData();
        
        $goalDetails = json_decode(json_encode($goalDetails),true);
        
        $user_data = DB::table('users')->where(array('id' => $user_id, 'site_id' => $this->site_id))->first();
        if($format == "pdf"){

            $accounts_data = DB::table('accounts')->where(array('id' => $account_id, 'site_id' => $this->site_id))->first();      
            

            $username = $user_data->first_name.' '.$user_data->last_name;
            $account_name = $accounts_data->company_name;

            $exportActionItems = new \App\Services\Exports\Goals\ActionItems\Export;
            return $exportActionItems->exportToPdf($goalDetails, $translations,$username,$account_name);

        }else if($format == "excel"){
            $username = $user_data->first_name.' '.$user_data->last_name;
            $exportActionItems = new \App\Services\Exports\Goals\ActionItems\Export;
            return $exportActionItems->export($goalDetails, $translations,$username);
        }
        

    }

    public function getSingleGoal($goal_id, $owner_id = null, $goal_settings = false, $goal_user_settings = false)
    {
        if(!$owner_id)
        {
            $owner_id = app("Illuminate\Http\Request")->get("owner_id");
        }
        $user_id = app("Illuminate\Http\Request")->get("user_id");
        $relations = Goal::getGoalRelations(1, 1,1, 1, 1); //this request is not from listing but sending from listing one to not get reflection and feedback content

        $goal = Goal::with($relations)->withCount(["actionItems"])
            ->where("id", $goal_id)
            ->where("is_active", 1)
            ->where("site_id", $this->site_id)->first();

        if($goal)
        {
            $goal->owner_id = $owner_id;
            if($goal->publish_date)
            {
                $goal->publish_date_updated = date("M d, Y");
            }
            if($goal->goal_type == Goal::goalTypeAccount)
            {
                $user = User::selectRaw("$goal_id as goal_id,id as user_id,email,first_name,last_name,image,2 as user_type, 0 as is_archived")->where("id", ($owner_id ? $owner_id : $user_id))->first();
                if($user)
                {
                    $goal->owners[] = $user->toArray();
                }
            }
            $goal->progress = 0;
            $goal->completed_items = 0;
            $goal->total_items = $goal->action_items_count;
            $goal = self::specialIndividualGoalWork($goal, $user_id, $goal_settings, $goal_user_settings);//SW-3795
            if($goal->action_items_count)
            {
                $condition = "";
                if($goal->goal_type == Goal::goalTypeGroup)
                {
                    $condition = "AND owner_id IS null";
                }
                else if($owner_id)
                {
                    $condition = "AND owner_id = '$owner_id'";
                }
                $done_items = DB::select("SELECT COUNT(*) as done_items FROM goal_items_feedback WHERE goal_id = $goal_id $condition AND is_done = 1");
                $goal->completed_items = $done_items[0]->done_items;
                if($goal->completed_items > $goal->action_items_count)
                {
                    $goal->completed_items = $goal->action_items_count;
                }
                $goal->progress = round($goal->completed_items/$goal->action_items_count * 100);
            }
            $actionItems = !empty($goal->actionItems) ? $goal->actionItems : $goal->action_items;
            $new_action_items = [];

            if(!empty($actionItems))
            {
                $count = 1;
                foreach($actionItems as $key => $item)
                {
                    $new_action_items[$key] = $item;
                    if($count == 1)
                    {
//                        $feedback = GoalItemsFeedback::select('reflection', 'feedback')->where('goal_id', $goal_id)->where('goal_item_id', $item->id)->where('owner_id', "<=>",($owner_id ? $owner_id : DB::raw('NULL')))->first();
                        $feedback = GoalItemsFeedback::select('reflection', 'feedback')->where('goal_id', $goal_id)->where('goal_item_id', $item->id)->whereRaw((DB::raw(($owner_id && $goal->goal_type != Goal::goalTypeGroup) ? "owner_id = ".$owner_id : ' owner_id is NULL')))->first();

                        if($feedback)
                        {
                            $new_action_items[$key]->reflection = $new_action_items[$key]->itemFeedbacks->reflection = $feedback->reflection;
                            $new_action_items[$key]->feedback = $new_action_items[$key]->itemFeedbacks->feedback = $feedback->feedback;
                        }
                        $count++;
                    }
                    $submitted_evidences = !empty($item->submittedEvidence) ? $item->submittedEvidence : $item->submitted_evidence;
                    $new_evidences = [];
                    if(!empty($submitted_evidences))
                    {
                        foreach($submitted_evidences as $index => $evidence)
                        {
                            $new_evidences[$index] = $evidence;
                            if($evidence->evidence_type == '1')
                            {
                                $video_detail =  Document::join("account_folder_documents", "documents.id", "=", "account_folder_documents.document_id")->select('documents.*','account_folder_documents.account_folder_id')->where(array('documents.id' => $evidence->evidence_ref_id ))->first();
                                if($video_detail)
                                {
                                    $video_detail = $video_detail->toArray();
                                    $evidence_detail = app('App\Http\Controllers\VideoController')->get_document_url($video_detail);
                                    $huddle_id = $video_detail['account_folder_id'];
                                    $new_evidences[$index]->thumbnail = $evidence_detail['thumbnail'];
                                    $new_evidences[$index]->thumbnail_type = $evidence_detail['thumbnail_type'];
                                    $new_evidences[$index]->published = $video_detail['published'];
                                    $new_evidences[$index]->encoder_status = $video_detail['encoder_status'];
                                    $new_evidences[$index]->upload_status = $video_detail['upload_status'];
                                    $new_evidences[$index]->is_processed = $video_detail['is_processed'];
                                    if ($video_detail['published'] == 0)
                                    {
                                        if( $this->site_id == 1){
                                            $new_evidences[$index]->thumbnail = app('App\Http\Controllers\VideoController')->getSecureAmazonSibmeUrl('app/img/video-thumbnail-sibme.svg', null, 1);
                                            $new_evidences[$index]->thumbnail_type = 'default';
                                        } else {
                                            $new_evidences[$index]->thumbnail = app('App\Http\Controllers\VideoController')->getSecureAmazonSibmeUrl('app/img/video-thumbnail-2.png', null, 1);
                                            $new_evidences[$index]->thumbnail_type = 'default';
                                        }
                                    }
                                    $if_evaluator = HelperFunctions::check_if_evalutor($huddle_id, $user_id);
                                    $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);
                                    $is_coach_enable = HelperFunctions::is_enabled_coach_feedback($huddle_id);
                                    $new_evidences[$index]->total_comments = HelperFunctions::get_video_comment_numbers($evidence->evidence_ref_id, $huddle_id, $user_id);
                                    $new_evidences[$index]->total_attachment = count(Document::getVideoDocumentsByVideo($evidence->evidence_ref_id, $huddle_id, $user_id, $h_type, $if_evaluator, $is_coach_enable, $this->site_id));
                                    $new_evidences[$index]->created_date = $video_detail['created_date'];
                                    $new_evidences[$index]->username = HelperFunctions::user_name_from_id($evidence->created_by,$this->site_id);
                                    $result = app('App\Http\Controllers\HuddleController')->getVideoDuration($evidence->evidence_ref_id);
                                    if ($result["status"]) {
                                        $new_evidences[$index]->duration = round($result["data"]);
                                    }
                                    else
                                    {
                                        $new_evidences[$index]->duration = 0;
                                    }
                                }
                                else
                                {
                                    unset($new_evidences[$index]);
                                    /*$new_evidences[$index]->total_comments = '';
                                    $new_evidences[$index]->total_attachment = '';
                                    $new_evidences[$index]->created_date = '';
                                    $new_evidences[$index]->username = '';
                                    $new_evidences[$index]->thumbnail = '';
                                    $new_evidences[$index]->duration = 0;
                                    $new_evidences[$index]->thumbnail_type = 'default';*/
                                }
                            }
                            if($evidence->evidence_type == '2')
                            {
                                $video_detail =  Document::where(array('id' => $evidence->evidence_ref_id ))->first();
                                if($video_detail)
                                {

                                    $thumbnail_detail = app('App\Http\Controllers\WorkSpaceController')->conversion_to_thmubs($video_detail, 0, 0);
                                    $new_evidences[$index]->thumbnail = $thumbnail_detail['thubnail_url'];
                                    $new_evidences[$index]->thumbnail_type = $thumbnail_detail['thumbnail_type'];
                                    $new_evidences[$index]->created_date = $video_detail->created_date;
                                    $new_evidences[$index]->username = HelperFunctions::user_name_from_id($evidence->created_by,$this->site_id);
                                    $new_evidences[$index]->doc_type = $video_detail->doc_type;
                                    $new_evidences[$index]->url = $video_detail->url;
                                }
                                else
                                {
                                    unset($new_evidences[$index]);
                                    /*$new_evidences[$index]->thumbnail = '';
                                    $new_evidences[$index]->created_date = '';
                                    $new_evidences[$index]->username = '';
                                    $new_evidences[$index]->thumbnail_type = '';
                                    $new_evidences[$index]->doc_type = '';
                                    $new_evidences[$index]->url = '';*/
                                }
                            }

                            if($evidence->evidence_type == '5')
                            {
                                $video_detail =  Document::join("account_folder_documents", "documents.id", "=", "account_folder_documents.document_id")->where(array('documents.id' => $evidence->evidence_ref_id ))->first();
                                if($video_detail)
                                {
                                    $video_detail = $video_detail->toArray();
                                    $huddle_id = $video_detail['account_folder_id'];
                                    $if_evaluator = HelperFunctions::check_if_evalutor($huddle_id, $user_id);
                                    $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);
                                    $is_coach_enable = HelperFunctions::is_enabled_coach_feedback($huddle_id);
                                    $new_evidences[$index]->total_comments = HelperFunctions::get_video_comment_numbers($evidence->evidence_ref_id, $huddle_id, $user_id);
                                    $new_evidences[$index]->total_attachment = count(Document::getVideoDocumentsByVideo($evidence->evidence_ref_id, $huddle_id, $user_id, $h_type, $if_evaluator, $is_coach_enable, $this->site_id));
                                    $new_evidences[$index]->created_date = $video_detail['created_date'];
                                    $new_evidences[$index]->username = HelperFunctions::user_name_from_id($evidence->created_by,$this->site_id);
                                }
                                else
                                {
                                    unset($new_evidences[$index]);
                                    /*$new_evidences[$index]->total_comments = '';
                                    $new_evidences[$index]->total_attachment = '';
                                    $new_evidences[$index]->created_date = '';
                                    $new_evidences[$index]->username = '';*/
                                }
                            }

                            if($evidence->evidence_type == '3')
                            {
                                $video_detail =  Document::join("account_folder_documents", "documents.id", "=", "account_folder_documents.document_id")->select('documents.*','account_folder_documents.account_folder_id')->where(array('account_folder_documents.account_folder_id' => $evidence->evidence_ref_id ))->first();
                                $huddle_details = AccountFolder::where(array('account_folder_id' => $evidence->evidence_ref_id))->select('AccountFolder.created_date')->first();

                                if($video_detail)
                                {
                                    $account_folder_id =  $video_detail['account_folder_id'];
                                    $new_evidences[$index]->total_docs = Document::getHuddleArtifactsCount($account_folder_id,$this->site_id, 2);
                                    $new_evidences[$index]->total_vidoe = Document::getHuddleArtifactsCount($account_folder_id,$this->site_id, 1);

                                }else{
                                    $new_evidences[$index]->total_docs = 0;
                                    $new_evidences[$index]->total_vidoe = 0;
                                }
                                $new_evidences[$index]->username = HelperFunctions::user_name_from_id($evidence->created_by,$this->site_id);
                                $new_evidences[$index]->huddle_type = AccountFolderMetaData::getMetaDataValue($evidence->evidence_ref_id, $this->site_id);
                                $new_evidences[$index]->created_date = $huddle_details['created_date'];
                            }

                            if($evidence->evidence_type == '4')
                            {
                                $new_evidences[$index]->username = HelperFunctions::user_name_from_id($evidence->created_by,$this->site_id);
                            }


                        }
                    }
                    $new_action_items[$key]->submittedEvidence = $new_action_items[$key]->submitted_evidence = $new_evidences;

                }
                $goal->actionItems = $goal->action_items = $new_action_items;
            }
        }

        return $goal;
    }

    public static function getGoalsSettings($account_id, $internal = 0)
    {
        $max_insert_size = 700;
        $goal_settings = GoalSettings::where("account_id", $account_id)->first();
        $site_id = app("Illuminate\Http\Request")->header('site_id');

        $users_in_account = UserAccount::select('account_id','user_id','role_id')->where("account_id",$account_id)->where("role_id",'120')->where("site_id",$site_id)->whereNotIn('user_id',function ($query) use ($account_id, $site_id){
            $query->select('user_id')
                ->from('goal_users_settings')
                ->where("account_id",$account_id)->where("role_id",'120')->where("site_id",$site_id);
        })->get();
        $users_ids = $users_in_account->pluck('user_id')->toArray();
        $users_in_account = $users_in_account->toArray();
        if(!empty($users_in_account))
        {
            DB::table('goal_users_settings')->where("account_id",$account_id)->whereIn("user_id" , $users_ids)->where("site_id",$site_id)->delete();
            $data = [];
            foreach ($users_in_account as $user)
            {
                //$user_check = GoalUserSettings::where("account_id",$account_id)->where("user_id",$user['user_id'])->where("role_id",'120')->where("site_id",$site_id)->first();
                $data[] = array(
                        'account_id' => $user['account_id'],
                        'user_id' => $user['user_id'],
                        'role_id' => $user['role_id'],
                        'site_id' => $site_id,
                        'can_create_individual_goals_for_others_people' => 0,
                        'can_create_individual_goals_for_themselves' => 1 ,
                        'can_create_group_goals' => 0,
                        'can_create_group_templates' => 0 ,
                        'can_use_templates_to_create_new_goals' => 0,
                        'can_create_goal_categories' => 0 ,
                        'can_be_added_as_goal_collab_with_view_rights' => 0 ,
                        'can_be_added_as_goal_collab_with_edit_rights' => 0 ,
                        'can_be_added_as_goal_collab_with_right_to_add_evidence' => 0 ,
                        'can_be_added_as_goal_collab_with_right_to_review_feedback' => 0,
                        'can_view_goal_progress' => 0,
                        'can_enter_data_for_measurement_where_goal_owners' => 1,
                        'can_enter_data_for_measurement_where_goal_reviewers' => 1,
                        'can_use_frameworks_in_goal_evidence' => 0,
                        'can_archive_goals' => 0,
                    );
            }
            foreach (array_chunk($data,$max_insert_size) as $chunk) {
                DB::table('goal_users_settings')->insert($chunk);
            }
        }


        $SAdmins_in_account = UserAccount::select('account_id','user_id','role_id')->where("account_id",$account_id)->where("role_id",'110')->whereNotIn('user_id',function ($query) use ($account_id, $site_id){
            $query->select('user_id')
                ->from('goal_users_settings')
                ->where("account_id",$account_id)->where("role_id",'110')->where("site_id",$site_id);
        })->where("site_id",$site_id)->get();
        $sadmin_ids = $SAdmins_in_account->pluck('user_id')->toArray();
        $SAdmins_in_account = $SAdmins_in_account->toArray();
        if(!empty($SAdmins_in_account))
        {
            DB::table('goal_users_settings')->where("account_id",$account_id)->whereIn("user_id" , $sadmin_ids)->where("site_id",$site_id)->delete();
            $data = [];
            foreach ($SAdmins_in_account as $user)
            {
                $data[] = array(
                        'account_id' => $user['account_id'],
                        'user_id' => $user['user_id'],
                        'role_id' => $user['role_id'],
                        'site_id' => $site_id,
                        'can_create_individual_goals_for_others_people' => 1,
                        'can_create_individual_goals_for_themselves' => 1 ,
                        'can_create_group_goals' => 1,
                        'can_create_group_templates' => 1 ,
                        'can_use_templates_to_create_new_goals' => 1 ,
                        'can_create_goal_categories' => 1 ,
                        'can_be_added_as_goal_collab_with_view_rights' => 1 ,
                        'can_be_added_as_goal_collab_with_edit_rights' => 1 ,
                        'can_be_added_as_goal_collab_with_right_to_add_evidence' => 1 ,
                        'can_be_added_as_goal_collab_with_right_to_review_feedback' => 1,
                        'can_view_goal_progress' => 1,
                        'can_enter_data_for_measurement_where_goal_owners' => 1,
                        'can_enter_data_for_measurement_where_goal_reviewers' => 1,
                        'can_use_frameworks_in_goal_evidence' => 1,
                        'can_archive_goals' => 1,
                    );}
            foreach (array_chunk($data,$max_insert_size) as $chunk) {
                DB::table('goal_users_settings')->insert($chunk);
            }
        }


        $owner_in_account = UserAccount::select('account_id','user_id','role_id')->where("account_id",$account_id)->where("role_id",'100')->whereNotIn("user_id",function ($query) use ($account_id, $site_id){
            $query->select('user_id')
                ->from('goal_users_settings')
                ->where("account_id",$account_id)->where("role_id",'100')->where("site_id",$site_id);
        })->where("site_id",$site_id)->get();
        $owner_ids = $owner_in_account->pluck('user_id')->toArray();
        $owner_in_account = $owner_in_account->toArray();

        if(!empty($owner_in_account))
        {
            DB::table('goal_users_settings')->where("account_id",$account_id)->whereIn("user_id" , $owner_ids)->where("site_id",$site_id)->delete();
            $data = [];
            foreach ($owner_in_account as $user)
            {
                $data[] = array(
                        'account_id' => $user['account_id'],
                        'user_id' => $user['user_id'],
                        'role_id' => $user['role_id'],
                        'site_id' => $site_id,
                        'can_create_individual_goals_for_others_people' => 1,
                        'can_create_individual_goals_for_themselves' => 1 ,
                        'can_create_group_goals' => 1,
                        'can_create_group_templates' => 1 ,
                        'can_use_templates_to_create_new_goals' => 1 ,
                        'can_create_goal_categories' => 1 ,
                        'can_be_added_as_goal_collab_with_view_rights' => 1 ,
                        'can_be_added_as_goal_collab_with_edit_rights' => 1 ,
                        'can_be_added_as_goal_collab_with_right_to_add_evidence' => 1 ,
                        'can_be_added_as_goal_collab_with_right_to_review_feedback' => 1,
                        'can_view_goal_progress' => 1,
                        'can_enter_data_for_measurement_where_goal_owners' => 1,
                        'can_enter_data_for_measurement_where_goal_reviewers' => 1,
                        'can_use_frameworks_in_goal_evidence' => 1,
                        'can_archive_goals' => 1,
                    );}
            foreach (array_chunk($data,$max_insert_size) as $chunk) {
                DB::table('goal_users_settings')->insert($chunk);
            }
        }


        $admins_in_account = UserAccount::select('account_id','user_id','role_id')->where("account_id",$account_id)->where("role_id",'115')->whereNotIn('user_id' ,function ($query) use ($account_id, $site_id){
            $query->select('user_id')
                ->from('goal_users_settings')
                ->where("account_id",$account_id)->where("role_id",'115')->where("site_id",$site_id);
        })->where("site_id",$site_id)->get();
        $users_ids = $admins_in_account->pluck('user_id')->toArray();
        $admins_in_account = $admins_in_account->toArray();

        if(!empty($admins_in_account))
        {
            DB::table('goal_users_settings')->where("account_id",$account_id)->whereIn("user_id" , $users_ids)->where("site_id",$site_id)->delete();
            $data = [];
            foreach ($admins_in_account as $admin)
            {
                $data[] = array(
                    'account_id' => $admin['account_id'],
                    'user_id' => $admin['user_id'],
                    'role_id' => $admin['role_id'],
                    'site_id' => $site_id,
                    'can_create_individual_goals_for_others_people' => 1,
                    'can_create_individual_goals_for_themselves' => 1 ,
                    'can_create_group_goals' => 1,
                    'can_create_group_templates' => 0 ,
                    'can_use_templates_to_create_new_goals' => 1 ,
                    'can_create_goal_categories' => 0 ,
                    'can_be_added_as_goal_collab_with_view_rights' => 1 ,
                    'can_be_added_as_goal_collab_with_edit_rights' => 1 ,
                    'can_be_added_as_goal_collab_with_right_to_add_evidence' => 1 ,
                    'can_be_added_as_goal_collab_with_right_to_review_feedback' => 1,
                    'can_view_goal_progress' => 0,
                    'can_enter_data_for_measurement_where_goal_owners' => 1,
                    'can_enter_data_for_measurement_where_goal_reviewers' => 1,
                    'can_use_frameworks_in_goal_evidence' => 1,
                    'can_archive_goals' => 0,
                );
            }
            foreach (array_chunk($data,$max_insert_size) as $chunk) {
                DB::table('goal_users_settings')->insert($chunk);
            }
        }

        $can_create_individual_goals_for_others_people = true;
        $can_create_individual_goals_for_themselves = true;
        $can_create_group_goals = true;
        $can_create_group_templates = true;
        $can_use_templates_to_create_new_goals = true;
        $can_create_goal_categories = true;
        $can_be_added_as_goal_collab_with_view_rights = true;
        $can_be_added_as_goal_collab_with_edit_rights = true;
        $can_be_added_as_goal_collab_with_right_to_add_evidence = true;
        $can_be_added_as_goal_collab_with_right_to_review_feedback = true;
        $can_view_goal_progress = true;
        $can_enter_data_for_measurement_where_goal_owners = true;
        $can_enter_data_for_measurement_where_goal_reviewers= true;
        $can_use_frameworks_in_goal_evidence = true;
        $can_archive_goals = true;

        
        $admin_can_create_individual_goals_for_others_people = true;
        $admin_can_create_individual_goals_for_themselves = true;
        $admin_can_create_group_goals = true;
        $admin_can_create_group_templates = true;
        $admin_can_use_templates_to_create_new_goals = true;
        $admin_can_create_goal_categories = true;
        $admin_can_be_added_as_goal_collab_with_view_rights = true;
        $admin_can_be_added_as_goal_collab_with_edit_rights = true;
        $admin_can_be_added_as_goal_collab_with_right_to_add_evidence = true;
        $admin_can_be_added_as_goal_collab_with_right_to_review_feedback = true;
        $admin_can_view_goal_progress = true;
        $admin_can_enter_data_for_measurement_where_goal_owners = true;
        $admin_can_enter_data_for_measurement_where_goal_reviewers= true;
        $admin_can_use_frameworks_in_goal_evidence = true;
        $admin_can_archive_goals = true;

        $user_privileges = GoalUserSettings::where(array(
                "account_id" => $account_id,
                "role_id" => 120
        ))->get()->toArray();
        
        foreach ($user_privileges as $row) 
        {

            if ($row['can_create_individual_goals_for_others_people'] == 0) {
                $can_create_individual_goals_for_others_people = false;
            }
            if ($row['can_create_individual_goals_for_themselves'] == 0) {
                $can_create_individual_goals_for_themselves = false;
            }
            if ($row['can_create_group_goals'] == 0) {
                $can_create_group_goals = false;
            }
            if ($row['can_create_group_templates'] == 0) {
                $can_create_group_templates = false;
            }
            if ($row['can_use_templates_to_create_new_goals'] == 0) {
                $can_use_templates_to_create_new_goals = false;
            }
            if ($row['can_create_goal_categories'] == 0) {
                $can_create_goal_categories = false;
            }
            if ($row['can_be_added_as_goal_collab_with_view_rights'] == 0) {
                $can_be_added_as_goal_collab_with_view_rights = false;
            }
            if ($row['can_be_added_as_goal_collab_with_edit_rights'] == 0) {
                $can_be_added_as_goal_collab_with_edit_rights = false;
            }

            if ($row['can_be_added_as_goal_collab_with_right_to_add_evidence'] == 0) {
                $can_be_added_as_goal_collab_with_right_to_add_evidence = false;
            }
            if ($row['can_be_added_as_goal_collab_with_right_to_review_feedback'] == 0) {
                $can_be_added_as_goal_collab_with_right_to_review_feedback = false;
            }
            if ($row['can_view_goal_progress'] == 0) {
                $can_view_goal_progress = false;
            }
            if ($row['can_enter_data_for_measurement_where_goal_owners'] == 0) {
                $can_enter_data_for_measurement_where_goal_owners = false;
            }
            if ($row['can_enter_data_for_measurement_where_goal_reviewers'] == 0) {
                $can_enter_data_for_measurement_where_goal_reviewers = false;
            }
            if ($row['can_use_frameworks_in_goal_evidence'] == 0) {
                $can_use_frameworks_in_goal_evidence = false;
            }
            if ($row['can_archive_goals'] == 0) {
                $can_archive_goals = false;
            }
        }
        
            $admin_privileges = GoalUserSettings::where(array(
                    "account_id" => $account_id,
                    "role_id" => 115
            ))->get()->toArray();
        
        foreach ($admin_privileges as $row) 
        {

            if ($row['can_create_individual_goals_for_others_people'] == 0) {
                $admin_can_create_individual_goals_for_others_people = false;
            }
            if ($row['can_create_individual_goals_for_themselves'] == 0) {
                $admin_can_create_individual_goals_for_themselves = false;
            }
            if ($row['can_create_group_goals'] == 0) {
                $admin_can_create_group_goals = false;
            }
            if ($row['can_create_group_templates'] == 0) {
                $admin_can_create_group_templates = false;
            }
            if ($row['can_use_templates_to_create_new_goals'] == 0) {
                $admin_can_use_templates_to_create_new_goals = false;
            }
            if ($row['can_create_goal_categories'] == 0) {
                $admin_can_create_goal_categories = false;
            }
            if ($row['can_be_added_as_goal_collab_with_view_rights'] == 0) {
                $admin_can_be_added_as_goal_collab_with_view_rights = false;
            }
            if ($row['can_be_added_as_goal_collab_with_edit_rights'] == 0) {
                $admin_can_be_added_as_goal_collab_with_edit_rights = false;
            }

            if ($row['can_be_added_as_goal_collab_with_right_to_add_evidence'] == 0) {
                $admin_can_be_added_as_goal_collab_with_right_to_add_evidence = false;
            }
            if ($row['can_be_added_as_goal_collab_with_right_to_review_feedback'] == 0) {
                $admin_can_be_added_as_goal_collab_with_right_to_review_feedback = false;
            }
            if ($row['can_view_goal_progress'] == 0) {
                $admin_can_view_goal_progress = false;
            }
            if ($row['can_enter_data_for_measurement_where_goal_owners'] == 0) {
                $admin_can_enter_data_for_measurement_where_goal_owners = false;
            }
            if ($row['can_enter_data_for_measurement_where_goal_reviewers'] == 0) {
                $admin_can_enter_data_for_measurement_where_goal_reviewers = false;
            }
            if ($row['can_use_frameworks_in_goal_evidence'] == 0) {
                $admin_can_use_frameworks_in_goal_evidence = false;
            }
            if ($row['can_archive_goals'] == 0) {
                $admin_can_archive_goals = false;
            }
        }
        
        $users_privileges_check = array(
            'can_create_individual_goals_for_others_people' => $can_create_individual_goals_for_others_people,
            'can_create_individual_goals_for_themselves' => $can_create_individual_goals_for_themselves ,
            'can_create_group_goals' => $can_create_group_goals,
            'can_create_group_templates' => $can_create_group_templates ,
            'can_use_templates_to_create_new_goals' => $can_use_templates_to_create_new_goals ,
            'can_create_goal_categories' => $can_create_goal_categories ,
            'can_be_added_as_goal_collab_with_view_rights' => $can_be_added_as_goal_collab_with_view_rights ,
            'can_be_added_as_goal_collab_with_edit_rights' => $can_be_added_as_goal_collab_with_edit_rights ,
            'can_be_added_as_goal_collab_with_right_to_add_evidence' => $can_be_added_as_goal_collab_with_right_to_add_evidence ,
            'can_be_added_as_goal_collab_with_right_to_review_feedback' => $can_be_added_as_goal_collab_with_right_to_review_feedback,
            'can_view_goal_progress' => $can_view_goal_progress,
            'can_enter_data_for_measurement_where_goal_owners' => $can_enter_data_for_measurement_where_goal_owners,
            'can_enter_data_for_measurement_where_goal_reviewers' => $can_enter_data_for_measurement_where_goal_reviewers,
            'can_use_frameworks_in_goal_evidence' => $can_use_frameworks_in_goal_evidence,
            'can_archive_goals' => $can_archive_goals,
          );
        
        $admins_privileges_check = array(
            'can_create_individual_goals_for_others_people' => $admin_can_create_individual_goals_for_others_people,
            'can_create_individual_goals_for_themselves' => $admin_can_create_individual_goals_for_themselves ,
            'can_create_group_goals' => $admin_can_create_group_goals,
            'can_create_group_templates' => $admin_can_create_group_templates ,
            'can_use_templates_to_create_new_goals' => $admin_can_use_templates_to_create_new_goals ,
            'can_create_goal_categories' => $admin_can_create_goal_categories ,
            'can_be_added_as_goal_collab_with_view_rights' => $admin_can_be_added_as_goal_collab_with_view_rights ,
            'can_be_added_as_goal_collab_with_edit_rights' => $admin_can_be_added_as_goal_collab_with_edit_rights ,
            'can_be_added_as_goal_collab_with_right_to_add_evidence' => $admin_can_be_added_as_goal_collab_with_right_to_add_evidence ,
            'can_be_added_as_goal_collab_with_right_to_review_feedback' => $admin_can_be_added_as_goal_collab_with_right_to_review_feedback,
            'can_view_goal_progress' => $admin_can_view_goal_progress,
            'can_enter_data_for_measurement_where_goal_owners' => $admin_can_enter_data_for_measurement_where_goal_owners,
            'can_enter_data_for_measurement_where_goal_reviewers' => $admin_can_enter_data_for_measurement_where_goal_reviewers,
            'can_use_frameworks_in_goal_evidence' => $admin_can_use_frameworks_in_goal_evidence,
            'can_archive_goals' => $admin_can_archive_goals,
          );
        
        
        if(empty($goal_settings))
        {
            $goal_settings = new GoalSettings();
            $goal_settings->account_id = $account_id;
            $goal_settings->created_by = Account::where("id", $account_id)->first()->created_by;
            $goal_settings->site_id = $site_id;
            $goal_settings->goal_alt_name = "";
            $goal_settings->item_alt_name = "";
            $goal_settings->evidence_alt_name = "";
            $goal_settings->measurement_alt_name = "";
            $goal_settings->make_all_goals_public = 0;
            $goal_settings->group_type_goals_public = 0;
            $goal_settings->individual_type_goals_public = 0;
            $goal_settings->account_type_goals_public = 0;
            $goal_settings->goal_owners_cannot_uncheck_action_items = 0;
            $goal_settings->enable_measurement = 1;
            $goal_settings->enable_performance_level = 1;
            $goal_settings->save();
            $channel_data = array(
                'channel' => 'global-channel',
                'event' => "refresh-header-settings",
                'account_id' => $account_id
            );
            HelperFunctions::broadcastEvent($channel_data,"broadcast_event",false);
        }
        else if($internal)
        {
            $request = app("Illuminate\Http\Request");
            $user_id = $request->has("user_id") ? $request->get("user_id") : 0;
            $goal_settings = self::getPreferredAccountSettings($account_id, $user_id, false);
        }
        if($internal)
        {
            return $goal_settings;
        }
        return response()->json(["data"=>$goal_settings, 'users_privileges_check' => $users_privileges_check , 'admins_privileges_check' => $admins_privileges_check , "message"=>"success", "success"=> true]);
    }

    public static function getPreferredAccountSettings($account_id, $user_id, $role_id = false)
    {
        $goal_settings = GoalSettings::where("account_id", $account_id)->first();
        if(!$goal_settings)
        {
            $goal_settings = self::getGoalsSettings($account_id, 1);
        }
        if(!$role_id)
        {
            $role_id = UserAccount::where("account_id",$account_id)->where("user_id", $user_id)->first()->role_id;
        }
        if($goal_settings->make_all_goals_public)
        {
            $goal_settings->group_type_goals_public = 1;
            $goal_settings->individual_type_goals_public = 1;
            $goal_settings->account_type_goals_public = 1;
            return $goal_settings;
        }
        else if($goal_settings->make_all_goals_public_admin_super_owner && $role_id == 115)//Admin
        {
            $goal_settings->make_all_goals_public = 1;
            $goal_settings->group_type_goals_public = 1;
            $goal_settings->individual_type_goals_public = 1;
            $goal_settings->account_type_goals_public = 1;
            return $goal_settings;
        }
        else if($goal_settings->make_all_goals_public_super_owner && $role_id <= 110)//SuperAdmin,AccountOwner
        {
            $goal_settings->make_all_goals_public = 1;
            $goal_settings->group_type_goals_public = 1;
            $goal_settings->individual_type_goals_public = 1;
            $goal_settings->account_type_goals_public = 1;
            return $goal_settings;
        }

        if(!$goal_settings->group_type_goals_public && (($goal_settings->group_type_goals_public_admin_super_owner && $role_id == 115) || ($goal_settings->group_type_goals_public_super_owner && $role_id <= 110)))
        {
            $goal_settings->group_type_goals_public = 1;
        }
        if(!$goal_settings->individual_type_goals_public && (($goal_settings->individual_type_goals_public_admin_super_owner && $role_id == 115) || ($goal_settings->individual_type_goals_public_super_owner && $role_id <= 110)))
        {
            $goal_settings->individual_type_goals_public = 1;
        }
        if(!$goal_settings->account_type_goals_public && (($goal_settings->account_type_goals_public_admin_super_owner && $role_id == 115) || ($goal_settings->account_type_goals_public_super_owner && $role_id <= 110)))
        {
            $goal_settings->account_type_goals_public = 1;
        }
        return $goal_settings;
    }

    public function updateGoalsSettings(Request $request)
    {   $input = $request->all();
        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");
        $tab =$input['opened_tab'];
        $goal_settings = $this->getGoalsSettings($account_id, 1);
        if($tab == 'terminology'){
        $goal_settings->goal_alt_name = $request->has("goal_alt_name") ? $request->get("goal_alt_name") : "";
        $goal_settings->item_alt_name = $request->has("item_alt_name") ? $request->get("item_alt_name") : "";
        $goal_settings->evidence_alt_name = $request->has("evidence_alt_name") ? $request->get("evidence_alt_name") : "";
        $goal_settings->measurement_alt_name = $request->has("measurement_alt_name") ? $request->get("measurement_alt_name") : "";
        $goal_settings->last_edit_by = $user_id;
        $goal_settings->save();
        }
        if($tab == 'general'){
        $goal_settings->make_all_goals_public = $request->has("make_all_goals_public") ? ($request->get("make_all_goals_public") == true ? 1 : 0) : 0;
        $goal_settings->make_all_goals_public_admin_super_owner = $request->has("make_all_goals_public_admin_super_owner") ? ($request->get("make_all_goals_public_admin_super_owner") == true ? 1 : 0) : 0;
        $goal_settings->make_all_goals_public_super_owner = $request->has("make_all_goals_public_super_owner") ? ($request->get("make_all_goals_public_super_owner") == true ? 1 : 0) : 0;
        $goal_settings->group_type_goals_public = $request->has("group_type_goals_public") ? ($request->get("group_type_goals_public") == true ? 1 : 0) : 0;
        $goal_settings->group_type_goals_public_admin_super_owner = $request->has("group_type_goals_public_admin_super_owner") ? ($request->get("group_type_goals_public_admin_super_owner") == true ? 1 : 0) : 0;
        $goal_settings->group_type_goals_public_super_owner = $request->has("group_type_goals_public_super_owner") ? ($request->get("group_type_goals_public_super_owner") == true ? 1 : 0) : 0;
        $goal_settings->individual_type_goals_public = $request->has("individual_type_goals_public") ? ($request->get("individual_type_goals_public") == true ? 1 : 0) : 0;
        $goal_settings->individual_type_goals_public_admin_super_owner = $request->has("individual_type_goals_public_admin_super_owner") ? ($request->get("individual_type_goals_public_admin_super_owner") == true ? 1 : 0) : 0;
        $goal_settings->individual_type_goals_public_super_owner = $request->has("individual_type_goals_public_super_owner") ? ($request->get("individual_type_goals_public_super_owner") == true ? 1 : 0) : 0;
        $goal_settings->account_type_goals_public = $request->has("account_type_goals_public") ? ($request->get("account_type_goals_public") == true ? 1 : 0) : 0;
        $goal_settings->account_type_goals_public_admin_super_owner = $request->has("account_type_goals_public_admin_super_owner") ? ($request->get("account_type_goals_public_admin_super_owner") == true ? 1 : 0) : 0;
        $goal_settings->account_type_goals_public_super_owner = $request->has("account_type_goals_public_super_owner") ? ($request->get("account_type_goals_public_super_owner") == true ? 1 : 0) : 0;
        $goal_settings->enable_measurement = $request->has("enable_measurement") ? ($request->get("enable_measurement") == true ? 1 : 0) : 0;
        $goal_settings->enable_performance_level = $request->has("enable_performance_level") ? ($request->get("enable_performance_level") == true ? 1 : 0) : 0;
        $goal_settings->goal_owners_cannot_uncheck_action_items = $request->has("goal_owners_cannot_uncheck_action_items") ? ($request->get("goal_owners_cannot_uncheck_action_items") == true ? 1 : 0) : 0;
        $goal_settings->review_collaborators = $request->has("review_collaborators") ? ($request->get("review_collaborators") == true ? 1 : 0) : 0;
        $goal_settings->goal_owner_setting = $request->has("goal_owner_setting") ? ($request->get("goal_owner_setting") == true ? 1 : 0) : 0;
        $goal_settings->last_edit_by = $user_id;
        $goal_settings->save();
        }
       
        if($tab == 'privileges'){
        $users_privileges_check = json_decode(json_encode($input['users_privileges_check']), True);

        $admins_privileges_check = json_decode(json_encode($input['admins_privileges_check']), True);
        
        
        $extraPermission = array(
                    'can_create_individual_goals_for_others_people' => $users_privileges_check['can_create_individual_goals_for_others_people'] ? '1' : '0',
                    'can_create_individual_goals_for_themselves' => $users_privileges_check['can_create_individual_goals_for_themselves'] ? '1' : '0',
                    'can_create_group_goals' => $users_privileges_check['can_create_group_goals'] ? '1' : '0',
                    'can_create_group_templates' => $users_privileges_check['can_create_group_templates'] ? '1' : '0',
                    'can_use_templates_to_create_new_goals' => $users_privileges_check['can_use_templates_to_create_new_goals'] ? '1' : '0',
                    'can_create_goal_categories' => $users_privileges_check['can_create_goal_categories'] ? '1' : '0',
                    'can_be_added_as_goal_collab_with_view_rights' => $users_privileges_check['can_be_added_as_goal_collab_with_view_rights'] ? '1' : '0',
                    'can_be_added_as_goal_collab_with_edit_rights' => $users_privileges_check['can_be_added_as_goal_collab_with_edit_rights'] ? '1' : '0',
                    'can_be_added_as_goal_collab_with_right_to_add_evidence' => $users_privileges_check['can_be_added_as_goal_collab_with_right_to_add_evidence'] ? '1' : '0',
                    'can_be_added_as_goal_collab_with_right_to_review_feedback' => $users_privileges_check['can_be_added_as_goal_collab_with_right_to_review_feedback'] ? '1' : '0',
                    'can_view_goal_progress' => $users_privileges_check['can_view_goal_progress'] ? '1' : '0',
                    'can_enter_data_for_measurement_where_goal_owners' => $users_privileges_check['can_enter_data_for_measurement_where_goal_owners'] ? '1' : '0',
                    'can_enter_data_for_measurement_where_goal_reviewers' => $users_privileges_check['can_enter_data_for_measurement_where_goal_reviewers'] ? '1' : '0',
                    'can_use_frameworks_in_goal_evidence' => $users_privileges_check['can_use_frameworks_in_goal_evidence'] ? '1' : '0',
                    'can_archive_goals' => $users_privileges_check['can_archive_goals'] ? '1' : '0',
            );
            
        DB::table("goal_users_settings")->where(array('account_id' => $account_id, 'role_id' => 120))->update($extraPermission);
        
                $extraPermission = array(
                    'can_create_individual_goals_for_others_people' => $admins_privileges_check['can_create_individual_goals_for_others_people'] ? '1' : '0',
                    'can_create_individual_goals_for_themselves' => $admins_privileges_check['can_create_individual_goals_for_themselves'] ? '1' : '0',
                    'can_create_group_goals' => $admins_privileges_check['can_create_group_goals'] ? '1' : '0',
                    'can_create_group_templates' => $admins_privileges_check['can_create_group_templates'] ? '1' : '0',
                    'can_use_templates_to_create_new_goals' => $admins_privileges_check['can_use_templates_to_create_new_goals'] ? '1' : '0',
                    'can_create_goal_categories' => $admins_privileges_check['can_create_goal_categories'] ? '1' : '0',
                    'can_be_added_as_goal_collab_with_view_rights' => $admins_privileges_check['can_be_added_as_goal_collab_with_view_rights'] ? '1' : '0',
                    'can_be_added_as_goal_collab_with_edit_rights' => $admins_privileges_check['can_be_added_as_goal_collab_with_edit_rights'] ? '1' : '0',
                    'can_be_added_as_goal_collab_with_right_to_add_evidence' => $admins_privileges_check['can_be_added_as_goal_collab_with_right_to_add_evidence'] ? '1' : '0',
                    'can_be_added_as_goal_collab_with_right_to_review_feedback' => $admins_privileges_check['can_be_added_as_goal_collab_with_right_to_review_feedback'] ? '1' : '0',
                    'can_view_goal_progress' => $admins_privileges_check['can_view_goal_progress'] ? '1' : '0',
                    'can_enter_data_for_measurement_where_goal_owners' => $admins_privileges_check['can_enter_data_for_measurement_where_goal_owners'] ? '1' : '0',
                    'can_enter_data_for_measurement_where_goal_reviewers' => $admins_privileges_check['can_enter_data_for_measurement_where_goal_reviewers'] ? '1' : '0',
                    'can_use_frameworks_in_goal_evidence' => $admins_privileges_check['can_use_frameworks_in_goal_evidence'] ? '1' : '0',
                    'can_archive_goals' => $admins_privileges_check['can_archive_goals'] ? '1' : '0',
            );
            
        DB::table("goal_users_settings")->where(array('account_id' => $account_id, 'role_id' => 115))->update($extraPermission);
        
        }
        
       
        $channel_data = array(
            'channel' => 'global-channel',
            'event' => "refresh-header-settings",
            'account_id' => $account_id,
            'from_goals' => true
        );
        HelperFunctions::broadcastEvent($channel_data,"broadcast_event");
        return response()->json(["data"=> $goal_settings, "message"=>"success", "success"=> true]);
    }

    public function addEditGoalItem(Request $request)
    {
        $goal_id = $request->get("goal_id");
        $item_id = $request->has("item_id") ? $request->get("item_id") : null;
        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");
        $title = $request->get("title");
        $description = $request->get("description");
        $deadline = $request->get("deadline");
        $add_measurement = $request->has("add_measurement") ? ($request->get("add_measurement") == true ? 1 : 0) : 0;
        $measurement_type = $request->has("measurement_type") ? $request->get("measurement_type") : 0;
        $start_value = $request->has("start_value") ? $request->get("start_value") : 0;
        $end_value = $request->has("end_value") ? $request->get("end_value") : 0;
        $evidence = $request->get("allowed_evidence");
        if(!is_array($evidence))
        {
            $evidence = explode(",", $evidence);
        }
        $item = GoalItem::find($item_id);
        if($item)
        {
            $old = clone $item;
        }
        else
        {
            $old = null;
        }
        $goal = Goal::where("id", $goal_id)->where("is_active",1)->first();
        $is_created = 0;
        if(empty($item))
        {
            $is_created = 1;
            $item = new GoalItem();
            $item->goal_id = $goal_id;
            $item->account_id = $account_id;
            $item->created_by = $user_id;
            $item->site_id = $this->site_id;
        }
        $item->title = $title;
        if(!empty($description))
        {
            $item->description = $description;
        }else{
            $item->description = '';
        }
        if(!empty($deadline))
        {
            $item->deadline = date("Y-m-d H:i:s", strtotime($deadline));
            if($old && date("Y-m-d", strtotime($old->deadline)) != date("Y-m-d", strtotime($item->deadline)))
            {
                $activity_type = Goal::activityTypes["action_item_deadline_edited"];
                $special = date("M d, Y", strtotime($deadline))."||".GetActivities::_SpanishDate(strtotime($deadline), 'spanish_day_month_year');
                $desc = "edited the Action Item Deadline for $item->title to ".$special." on {CREATED_DATE}";
                $activity_data = ["goal_id"=>$goal->id, "desc"=>$desc, "account_folder_id"=>$goal->account_folder_id, "account_id"=>$account_id, "user_id"=>$user_id, "type"=>$activity_type, "item_name"=>$item->title, "special"=>$special];
                // $this->addRecentActivity($activity_data);
                $activity_data['current_lang'] = $this->current_lang;
                $activity_data['site_id'] = $this->site_id;
                QueuesManager::sendToEmailQueue(__CLASS__, "addRecentActivity", $activity_data);
            }
        }
        $item->add_measurement = $add_measurement;
        $item->measurement_type = $measurement_type;
        $item->start_value = $start_value;
        $item->end_value = $end_value;
        $item->measurement_type = $measurement_type;
        $item->last_edit_by = $user_id;
        $item->save();
        
            $new_evidence = [];
            foreach ($evidence as $evidence_id)
            {
                $new_evidence[] = ["goal_item_id"=> $item->id, "goal_id"=> $goal_id, "evidence_type"=> $evidence_id];
            }
            GoalItemEvidence::where("goal_item_id", $item->id)->where("goal_id", $goal_id)->delete();
            GoalItemEvidence::insert($new_evidence);
        $item->allowed_evidence = GoalItemEvidence::where("goal_item_id", $item->id)->get();

        if($goal->is_published && $is_created)
        {
            $activity_type = Goal::activityTypes["added_action_item"];
            $desc = "added the Action Item $title to $goal->title on {CREATED_DATE}.";
            $activity_data = ["goal_id"=>$goal->id, "desc"=>$desc, "account_folder_id"=>$goal->account_folder_id, "account_id"=>$account_id, "user_id"=>$user_id, "type"=>$activity_type, "item_name"=>$goal->title, "special"=>$title];
            // $this->addRecentActivity($activity_data);
            $activity_data['current_lang'] = $this->current_lang;
            $activity_data['site_id'] = $this->site_id;
            QueuesManager::sendToEmailQueue(__CLASS__, "addRecentActivity", $activity_data);
        }
        else if($old && $goal->is_published && $old->title != $title)
        {
            $activity_type = Goal::activityTypes["action_item_title_edited"];
            $desc = "edited the title for the Action Item titled $old->title in $goal->title on {CREATED_DATE}.";
            $activity_data = ["goal_id"=>$goal->id, "desc"=>$desc, "account_folder_id"=>$goal->account_folder_id, "account_id"=>$account_id, "user_id"=>$user_id, "type"=>$activity_type, "item_name"=>$goal->title, "special"=>$old->title];
            // $this->addRecentActivity($activity_data);
            $activity_data['current_lang'] = $this->current_lang;
            $activity_data['site_id'] = $this->site_id;
            QueuesManager::sendToEmailQueue(__CLASS__, "addRecentActivity", $activity_data);
        }
        $sItem = GoalItem::with(["allowedEvidence","submittedEvidence"])->selectRaw("goal_items.*, NULL AS feedback_id, NULL AS feedback, NULL AS reflection, NULL AS is_done, NULL AS owner_id,  goal_items.start_value AS current_value, NULL AS feedback_created_at, NULL AS feedback_updated_at")->where("id", $item->id)->first();
        $event = [
            'channel' => "goal-".$goal_id,
            'event' => $is_created ? "item_created" : "item_edited",
            'data' => $sItem,
            'uuid' => $request->has("uuid") ? $request->get("uuid") : ($is_created ? null : $sItem->id),
            'item_id' => $sItem->id,
            'goal_id' => $goal_id,
            'from_goals' => true
        ];
        HelperFunctions::broadcastEvent($event,"broadcast_event");
        return response()->json(["data"=> $item, "evidence"=>$evidence, "message"=>"success", "success"=> true]);
    }

    public function goalItemReview(Request $request)
    {
        $item_id = $request->get("item_id");
        $goal_id = $request->get("goal_id");
        $owner_id = $request->get("owner_id");
        $user_id = $request->get("user_id");
        $user_email = User::where('id',$user_id)->pluck('email')->first();
        $goal = Goal::withCount(['actionItems as total_items'])->where("id", $goal_id)->where("is_active", 1)->first();
        $goal_item = GoalItem::find($item_id);
        $is_owner = GoalUser::where('user_id',$user_id)->where('user_type',2)->first();
        $is_confirm = $request->has('is_confirm') && $request->get('is_confirm') == 1 ? 1 : 0;
        $set_feedback_empty = 0;
        if(!$goal || !$goal_item)
        {
            return response()->json(["data"=> null, "message"=>"Goal not found!", "success"=> false]);
        }

        if($request->has('environment_type') && $request->get('environment_type') !=2 && !$is_confirm)//not checking permissions if web request or already confirmed
        {
            $permissions = $this->actionItemDone($request);
            if(isset($permissions['is_checked']) && $permissions['is_checked'] ==true){
                $permissions['is_checked']  = 1;
            }else{
                $permissions['is_checked']  = 0;
            }

            if($permissions['status'] == false)
            {
                $permissions['success'] = $permissions['status'];
                unset($permissions['status']);
                $permissions['data'] = null;
                if(empty($permissions['message'])){
                $permissions['message'] = "You do not have permission for this action";
                }
                return response()->json($permissions);
            }
            else if (isset($permissions['set_feedback_empty']) && $permissions['set_feedback_empty'] == 1)
            {
                $set_feedback_empty = 1;
            }
        }
        DB::unprepared('LOCK TABLES goal_items_feedback WRITE');
        $query = GoalItemsFeedback::where("goal_item_id",$item_id)->where("goal_id", $goal_id);
        if($goal->goal_type == Goal::goalTypeGroup)
        {
            $query->whereNull('owner_id');
        }
        else
        {
            $query->where('owner_id', $owner_id);
        }
        $feedback_item = $query->first();
        if(empty($feedback_item))
        {
            $feedback_item = new GoalItemsFeedback();
            $feedback_item->goal_id = $goal_id;
            $feedback_item->goal_item_id = $item_id;
            $feedback_item->owner_id = $owner_id;
            $feedback_item->is_done = 0;
            if($goal->goal_type == Goal::goalTypeGroup)
            {
                $feedback_item->owner_id = null;
            }
            $feedback_item->created_by = $user_id;
        }

        if($request->has("current_value"))
        {
            $feedback_item->current_value = $request->get("current_value");
        }
        if($request->has("is_done"))
        {
            $is_done = ($request->get("is_done") == 1) ? 1 : 0;
            if($is_done && $feedback_item->is_done != $is_done)
            {
                $activity_type = Goal::activityTypes["mark_complete"];
                $desc = "marked $goal_item->title complete on {CREATED_DATE}.";
                if($is_owner || $goal->goal_type == '2'){
                $this->create_churnzero_event('ActionItem+Completed', $goal->account_id, $user_email);
                }
               
                $feedback_item->completed_at = date('Y-m-d H:i:s');
                if(strtotime(date("Y-m-d")) <= strtotime($goal_item->deadline))
                {
                    $feedback_item->is_completed_on_time = 1;
                }
                else
                {
                    $feedback_item->is_completed_on_time = 0;
                }
            }
            else if ($feedback_item->is_done != $is_done)
            {
                $activity_type = Goal::activityTypes["mark_incomplete"];
                $desc = "marked $goal_item->title incomplete on {CREATED_DATE}.";
                if($is_owner || $goal->goal_type == '2'){
                $this->create_churnzero_event('ActionItem+INComplete', $goal->account_id, $user_email);
                }
                $feedback_item->is_completed_on_time = 0;
            }
            $feedback_item->is_done = $is_done;
        }
        if($request->has("reflection"))
        {
            $feedback_item->reflection = $request->get("reflection");
        }
        if($request->has("feedback"))
        {
            if(!empty($request->get("feedback")))
            {
                $activity_type = Goal::activityTypes["added_feedback"];
                $desc = "provided feedback on $goal_item->title on {CREATED_DATE}.";
            }
            $feedback_item->feedback = $request->get("feedback");
        }
        if($set_feedback_empty)
        {
            $feedback_item->feedback = '';
        }
        $feedback_item->last_edit_by = $user_id;
        $feedback_item->save();
        DB::unprepared('UNLOCK TABLES;');
        $other_user_id = $feedback_item->owner_id;
        $done_items = DB::select("SELECT COUNT(*) as done_items FROM goal_items_feedback WHERE goal_id = $goal_id AND goal_item_id = $item_id AND is_done = 1 GROUP BY owner_id");
        $is_complete = 0;
        $completion_date = null;
        
        if($done_items && isset($done_items[0]))
        {
            foreach ($done_items as $owner_done_item)
            {
                if($goal->total_items == $owner_done_item->done_items)
                {
                    $is_complete = 1;
                    $completion_date = date("Y-m-d H:i:s");
                }
                else
                {
                    
                    $is_complete = 0;
                    $completion_date = null;
                }
            }
        }
        $goal->is_complete = $is_complete;
        $goal->completion_date = $completion_date;
        $goal->save();
        $goal = $this->getSingleGoal($goal_id);
        if(isset($activity_type))
        {
            $activity_data = ["goal_id"=>$goal->id, "desc"=>$desc, "account_folder_id"=>$goal->account_folder_id, "account_id"=>$goal->account_id, "user_id"=>$user_id, "type"=>$activity_type, "other_user_id"=>$other_user_id, "item_name"=>$goal->title, "owner_id"=> $owner_id];
            // $this->addRecentActivity($activity_data);
            $activity_data['current_lang'] = $this->current_lang;
            $activity_data['site_id'] = $this->site_id;
            QueuesManager::sendToEmailQueue(__CLASS__, "addRecentActivity", $activity_data);
        }
        foreach ($goal->action_items as $item)
        {
            if($item->id == $item_id)
            {
                $item->reflection = $item->itemFeedbacks->reflection = $feedback_item->reflection;
                $item->feedback = $item->itemFeedbacks->feedback = $feedback_item->feedback;
                break;
            }
        }
        $event = [
            'channel' => "goals_".$goal->account_id,
            'event' => "goal_edited",
            'action' => "review_added",
            'data' =>  $goal,
            'item_id' => $goal->id,
            'goal_item_id' => $item_id,
            'goal_id' => $goal->id,
            'from_goals' => true
        ];
        $feedback_item['goal_item'] = $goal;
        HelperFunctions::broadcastEvent($event,"broadcast_event");
        $event['channel'] = "goal-".$goal_id;
        HelperFunctions::broadcastEvent($event,"broadcast_event");
        return response()->json(["data"=> $feedback_item,  "message"=>"Action item updated successfully!", "success"=> true]);
    }

    public function addEvidence(Request $request)
    {
        $item_id = $request->get("item_id");
        $goal_id = $request->get("goal_id");
        $user_id = $request->get("user_id");
        $owner_id = $request->get("owner_id");
        $evidence_type = $request->get("evidence_type");
        $evidence_ref_ids = $request->get("evidence_ref_id");
        $goal = Goal::where("id", $goal_id)->where("is_active",1)->first();
        $evidence_ref_ids = explode(',', $evidence_ref_ids);
        $evidence_ref_ids = array_unique($evidence_ref_ids);//if frontend send duplicate ids dont add multiple times
        if(empty($evidence_ref_ids))
        {
            return response()->json(["data"=> [],  "message"=>"Please select at least one evidence!", "success"=> false]);
        }
        foreach ($evidence_ref_ids as $evidence_ref_id)
        {
            $check_query = GoalItemSubmittedEvidence::where("goal_id", $goal_id)->where("goal_item_id", $item_id)->where("evidence_ref_id",$evidence_ref_id)->where("evidence_type", $evidence_type);
            if($goal->goal_type != Goal::goalTypeGroup)
            {
                $check_query->where("owner_id", $owner_id);
            }
            $already_exist = $check_query->first();
            if($already_exist)
            {
                return response()->json(["data"=> [],  "message"=>TranslationsManager::get_translation('goals_evidence_already_exist'), "success"=> false]);
            }
            $evidence = new GoalItemSubmittedEvidence();
            $evidence->goal_id = $goal_id;
            $evidence->goal_item_id = $item_id;
            $evidence->owner_id = $owner_id;
            $evidence->evidence_type = $evidence_type;
            $evidence->evidence_ref_id = $evidence_ref_id;
            $evidence->created_by = $user_id;
            $evidence->last_edit_by = $user_id;
            $evidence->site_id = $this->site_id;
            $evidence->save();

            $eviResult = $this->getEvidenceTitle($evidence_ref_id, $evidence_type);
            $evidence->title = $eTitle = $eviResult->title;
            $evidence->stack_url = $eviResult->stack_url;
            $eTitle = explode("___", $eTitle)[0];
            $desc = "added $eTitle to $goal->title on {CREATED_DATE}.";
            $activity_type = Goal::activityTypes["added_evidence"];
            $activity_data = ["goal_id"=>$goal->id, "desc"=>$desc, "account_folder_id"=>$goal->account_folder_id, "account_id"=>$goal->account_id, "user_id"=>$user_id, "type"=>$activity_type, "item_name"=>$goal->title, "special"=>$eTitle];
            // $this->addRecentActivity($activity_data);
            $activity_data['current_lang'] = $this->current_lang;
            $activity_data['site_id'] = $this->site_id;
            QueuesManager::sendToEmailQueue(__CLASS__, "addRecentActivity", $activity_data);

            if($evidence_type == '1')
            {
                $video_detail =  Document::join("account_folder_documents", "documents.id", "=", "account_folder_documents.document_id")->select('documents.*','account_folder_documents.account_folder_id')->where(array('documents.id' => $evidence_ref_id ))->first();
                if($video_detail)
                {
                    $video_detail = $video_detail->toArray();
                    $evidence_detail = app('App\Http\Controllers\VideoController')->get_document_url($video_detail);
                    $huddle_id = $video_detail['account_folder_id'];
                    $evidence->thumbnail = $evidence_detail['thumbnail'];
                    $evidence->thumbnail_type = $evidence_detail['thumbnail_type'];
                    $evidence->published = $video_detail['published'];
                    $evidence->encoder_status = $video_detail['encoder_status'];
                    $evidence->upload_status = $video_detail['upload_status'];
                    $evidence->is_processed = $video_detail['is_processed'];
                    if ($video_detail['published'] == 0)
                    {
                        if( $this->site_id == 1){
                            $evidence->thumbnail = app('App\Http\Controllers\VideoController')->getSecureAmazonSibmeUrl('app/img/video-thumbnail-sibme.svg', null, 1);
                            $evidence->thumbnail_type = 'default';
                        } else {
                            $evidence->thumbnail = app('App\Http\Controllers\VideoController')->getSecureAmazonSibmeUrl('app/img/video-thumbnail-2.png', null, 1);
                            $evidence->thumbnail_type = 'default';
                        }
                    }
                    $if_evaluator = HelperFunctions::check_if_evalutor($huddle_id, $user_id);
                    $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);
                    $is_coach_enable = HelperFunctions::is_enabled_coach_feedback($huddle_id);
                    $evidence->total_comments = HelperFunctions::get_video_comment_numbers($evidence_ref_id, $huddle_id, $user_id);
                    $evidence->total_attachment = count(Document::getVideoDocumentsByVideo($evidence_ref_id, $huddle_id, $user_id, $h_type, $if_evaluator, $is_coach_enable, $this->site_id));
                    $evidence->created_date = $video_detail['created_date'];
                    $evidence->username = HelperFunctions::user_name_from_id($user_id,$this->site_id);
                    $result = app('App\Http\Controllers\HuddleController')->getVideoDuration($evidence_ref_id);
                    if ($result["status"]) {
                        $evidence->duration = round($result["data"]);
                    }
                    else
                    {
                        $evidence->duration = 0;
                    }
                }
                else
                {
                    $evidence->total_comments = '';
                    $evidence->total_attachment = '';
                    $evidence->created_date = '';
                    $evidence->username = '';
                    $evidence->thumbnail = '';
                    $evidence->thumbnail_type = 'default';
                    $evidence->duration = 0;
                }
            }
            if($evidence_type == '2')
            {
                $video_detail =  Document::where(array('id' => $evidence_ref_id ))->first();
                if($video_detail)
                {
                    $thumbnail_detail = app('App\Http\Controllers\WorkSpaceController')->conversion_to_thmubs($video_detail, 0, 0);
                    $evidence->thumbnail = $thumbnail_detail['thubnail_url'];
                    $evidence->thumbnail_type = $thumbnail_detail['thumbnail_type'];
                    $evidence->created_date = $video_detail->created_date;
                    $evidence->username = HelperFunctions::user_name_from_id($user_id,$this->site_id);
                    $evidence->doc_type = $video_detail->doc_type;
                    $evidence->url = $video_detail->url;
                }
                else
                {
                    $evidence->thumbnail = '';
                    $evidence->created_date = '';
                    $evidence->username = '';
                    $evidence->thumbnail_type = '';
                    $evidence->doc_type = '';
                    $evidence->url = '';
                }
            }

            if($evidence_type == '5')
            {
                $video_detail =  Document::join("account_folder_documents", "documents.id", "=", "account_folder_documents.document_id")->where(array('documents.id' => $evidence_ref_id ))->first();
                if($video_detail)
                {
                    $video_detail = $video_detail->toArray();
                    $huddle_id = $video_detail['account_folder_id'];
                    $if_evaluator = HelperFunctions::check_if_evalutor($huddle_id, $user_id);
                    $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $this->site_id);
                    $is_coach_enable = HelperFunctions::is_enabled_coach_feedback($huddle_id);
                    $evidence->total_comments = HelperFunctions::get_video_comment_numbers($evidence_ref_id, $huddle_id, $user_id);
                    $evidence->total_attachment = count(Document::getVideoDocumentsByVideo($evidence_ref_id, $huddle_id, $user_id, $h_type, $if_evaluator, $is_coach_enable, $this->site_id));
                    $evidence->created_date = $video_detail['created_date'];
                    $evidence->username = HelperFunctions::user_name_from_id($user_id,$this->site_id);
                }
                else
                {
                    $evidence->total_comments = '';
                    $evidence->total_attachment = '';
                    $evidence->created_date = '';
                    $evidence->username = '';
                }
            }

            if($evidence_type == '3')
            {
                $video_detail =  Document::join("account_folder_documents", "documents.id", "=", "account_folder_documents.document_id")->select('documents.*','account_folder_documents.account_folder_id')->where(array('account_folder_documents.account_folder_id' => $evidence_ref_id ))->first();
                $huddle_details = AccountFolder::where(array('account_folder_id' => $evidence_ref_id))->select('AccountFolder.created_date')->first();
                if($video_detail)
                {
                    $account_folder_id =  $video_detail['account_folder_id'];
                    $evidence->total_docs = Document::getHuddleArtifactsCount($account_folder_id,$this->site_id, 2);
                    $evidence->total_vidoe = Document::getHuddleArtifactsCount($account_folder_id,$this->site_id, 1);
                }else{
                    $evidence->total_docs= 0;
                    $evidence->total_vidoe = 0;
                }
                $evidence->username = HelperFunctions::user_name_from_id($user_id,$this->site_id);
                $evidence->huddle_type = AccountFolderMetaData::getMetaDataValue($evidence_ref_id, $this->site_id);
                $evidence->created_date = $huddle_details['created_date'];
            }

            if($evidence_type == '4')
            {
                $evidence->username = HelperFunctions::user_name_from_id($user_id,$this->site_id);
            }


            $event = [
                'channel' => "goal-".$goal_id,
                'event' => "evidence_added",
                'data' => $evidence,
                'item_id' => $evidence->id,
                'goal_item_id' => $evidence->goal_item_id,
                'goal_id' => $goal_id,
                'from_goals' => true
            ];
            HelperFunctions::broadcastEvent($event,"broadcast_event");
        }

        return response()->json(["data"=> $evidence,  "message"=>HelperFunctions::getGoalAltTranslation(TranslationsManager::get_translation('evidence_added'), $goal->account_id, $this->site_id), "success"=> true]);
    }

    public function getEvidenceTitle($evidence_ref_id, $evidence_type)
    {
        $result = DB::select("Select IF($evidence_type IN (1,2,5) , (select CONCAT(afd.title, '___', afd.account_folder_id, '___', af.folder_type) from account_folder_documents afd join account_folders af on af.account_folder_id = afd.account_folder_id where document_id = $evidence_ref_id limit 1)
                   , IF($evidence_type = 3, (select af.name from account_folders af where account_folder_id = $evidence_ref_id limit 1)
                   ,  IF($evidence_type = 4, (select at.tag_title from account_tags at where account_tag_id = $evidence_ref_id limit 1), null )))  
                   as title, IF($evidence_type = 2, (select IF(d.doc_type = 5, url,stack_url) as stack_url  from documents d where d.id = $evidence_ref_id limit 1), NULL) as stack_url");
        return $result[0];
    }

    public function removeEvidence(Request $request)
    {
        $evidence_ref_id = $request->get("evidence_ref_id");
        $evidence_id = $request->get("evidence_id");
        $goal_id = $request->get("goal_id");
        $item_id = $request->get("item_id");
        $owner_id = $request->get("owner_id");
        GoalItemSubmittedEvidence::where("goal_id", $goal_id)->where("evidence_ref_id", $evidence_ref_id)->where("goal_item_id", $item_id)->where("owner_id", $owner_id)->delete();
        GoalItemStandardRating::where("evidence_id", $evidence_id)->delete();//For Frameworks
        $event = [
            'channel' => "goal-".$goal_id,
            'event' => "evidence_removed",
            'data' => $evidence_id,
            'evidence_id' => $evidence_id,
            'item_id' => $item_id,
            'goal_id' => $goal_id,
            'owner_id' => $owner_id,
            'from_goals' => true
        ];
        HelperFunctions::broadcastEvent($event,"broadcast_event");
        return response()->json(["data"=> [],  "message"=>"Evidence removed successfully!", "success"=> true]);
    }

    public function deleteActionItem(Request $request)
    {
        $goal_id = $request->get("goal_id");
        $item_id = $request->get("item_id");
        $user_id = $request->get("user_id");
        $is_confirmed = $request->has("is_confirmed") ? ($request->get("is_confirmed") == 1) : 0;
        if(!$is_confirmed)
        {
            $being_used = GoalItemSubmittedEvidence::where("goal_id",$goal_id)->where("goal_item_id", $item_id)->count();
            if($being_used > 0)
            {
                return response()->json(["data"=> $being_used, "is_confirmed" => $is_confirmed,  "message"=>"Users already added evidence against this item!", "success"=> false]);
            }
        }
        $goal_item = GoalItem::find($item_id);
        GoalItem::where("goal_id",$goal_id)->where("id",$item_id)->delete();
        GoalItemEvidence::where("goal_id",$goal_id)->where("goal_item_id",$item_id)->delete();
        GoalItemSubmittedEvidence::where("goal_id",$goal_id)->where("goal_item_id",$item_id)->delete();
        $goal = Goal::where("id", $goal_id)->where("is_active",1)->first();
        if($goal && $goal->is_published)
        {
            $activity_type = Goal::activityTypes["action_item_deleted"];
            $desc = "deleted the Action Item $goal_item->title from $goal->title on {CREATED_DATE}.";
            $activity_data = ["goal_id"=>$goal->id, "desc"=>$desc, "account_folder_id"=>$goal->account_folder_id, "account_id"=>$goal->account_id, "user_id"=>$user_id, "type"=>$activity_type, "item_name"=>$goal->title, "special"=>$goal_item->title];
            // $this->addRecentActivity($activity_data);
            $activity_data['current_lang'] = $this->current_lang;
            $activity_data['site_id'] = $this->site_id;
            QueuesManager::sendToEmailQueue(__CLASS__, "addRecentActivity", $activity_data);
        }
        $event = [
            'channel' => "goal-".$goal_id,
            'event' => "item_deleted",
            'data' => $item_id,
            'item_id' => $item_id,
            'goal_id' => $goal_id,
            'from_goals' => true
        ];
        HelperFunctions::broadcastEvent($event,"broadcast_event");

        return response()->json(["data"=> null,  "message"=>"Action Item deleted successfully!", "success"=> true]);
    }

    public function deleteCategory(Request $request)
    {
        $category_id = $request->get("category_id");
        $account_id = $request->get("account_id");
        Category::where("id", $category_id)->delete();
        $event = [
            'channel' => 'goals_'.$account_id,
            'event' => "category_deleted",
            'data' => $category_id,
            'item_id' => $category_id
        ];
        HelperFunctions::broadcastEvent($event, "broadcast_event",false);
        return response()->json(["data"=> null,  "message"=>"Category deleted successfully!", "success"=> true]);
    }

    public function addEditCategories(Request $request)
    {
        $categories = $request->get('categories');
        $account_id = $request->get('account_id');
        $user_id = $request->get('user_id');
        if(!is_array($categories))
        {
            $categories = json_decode($categories, true);
        }
        $new_categories = [];
        $updated_categories = [];
        if(count($categories) > 0){
            foreach($categories as $row) {
                if ($row['id'] <= 0) {
                    $categories = array(
                        'name' => $row['name'],
                        'created_by' => $user_id,
                        'account_id' => $account_id,
                        'created_date' => date('y-m-d H:i:s', time()),
                        'last_edit_date' => date('y-m-d H:i:s', time()),
                        'last_edit_by' => $user_id,
                    );
                    $cat_id = DB::table('categories')->insertGetId($categories);
                    $new_categories[] = ["name" => $row['name'], "id" => $cat_id];
                } else {
                    $categories = array(
                        'name' => $row['name'],
                        'last_edit_date' => date('y-m-d H:i:s', time()),
                        'last_edit_by' => $user_id,
                    );
                    DB::table('categories')->where(['id' => $row['id']])->update($categories);
                    $updated_categories[] = ["name" => $row['name'], "id" => $row['id']];
                }
            }
        }
        $all_categories = Category::where("account_id", $account_id)->orderBy("created_date", "DESC")->get();
        $socket_categories = [];
        $socket_categories["added"] = $new_categories;
        $socket_categories["updated"] = $updated_categories;
        $socket_categories["all"] = $all_categories;
        $event = [
            'channel' => 'goals_'.$account_id,
            'event' => "categories",
            'data' => $socket_categories,
            'user_id' => $user_id
        ];
        HelperFunctions::broadcastEvent($event, "broadcast_event",FALSE);
        return response()->json(["status" => true,"data"=>'', "message" => "Success"]);
    }

    public function deleteGoal(Request $request)
    {
        $goal_id = $request->get("goal_id");
        $user_id = $request->get("user_id");
        $account_id = $request->get("account_id");
        $goal = Goal::where("id", $goal_id)->where("is_active",1)->first();
        Goal::where("id", $goal_id)->update(["is_active"=>0]);
        if($goal && $goal->is_published)
        {
            $activity_type = Goal::activityTypes["goal_deleted"];
            $desc = "deleted the Goal $goal->title on {CREATED_DATE}.";
            $activity_data = ["goal_id"=>$goal->id, "desc"=>$desc, "account_folder_id"=>$goal->account_folder_id, "account_id"=>$goal->account_id, "user_id"=>$user_id, "type"=>$activity_type, "item_name"=>$goal->title, "goal_type" => $goal->goal_type];
            // $this->addRecentActivity($activity_data);
            $activity_data['current_lang'] = $this->current_lang;
            $activity_data['site_id'] = $this->site_id;
            QueuesManager::sendToEmailQueue(__CLASS__, "addRecentActivity", $activity_data);
        }
        $event = [
            'channel' => "goals_".$account_id,
            'event' => "goal_deleted",
            'action' => "delete",
            'data' => $goal_id,
            'item_id' => $goal_id,
            'goal_id' => $goal_id,
            'from_goals' => true
        ];
        HelperFunctions::broadcastEvent($event,"broadcast_event");
        $event['channel'] = "goal-".$goal_id;
        HelperFunctions::broadcastEvent($event,"broadcast_event");
        return response()->json(["status" => true,"data"=>$goal_id, "message" => "Goal Deleted Successfully!"]);
    }

    public function getGoalStatus(Request $request, $internal = 0)
    {
        $goal_id = $request->get("goal_id");
        $account_id = $request->get("account_id");
        $enable_pagination = $request->has("enable_pagination") ? $request->get("enable_pagination")  == 1 : 0;
        $only_user_id = $request->has("only_user_id") ? $request->get("only_user_id") : false;
        $page = $request->has("page") ? $request->get("page")  : 0;
        $sortBy = $request->has("sort_by") ? $request->get("sort_by")  : 'name';
        $limit = 15;
        $pagination_query = "";
        if($enable_pagination)
        {
            $page = $page * $limit;
            $pagination_query = " LIMIT $limit OFFSET $page";
        }
        if($sortBy == 'status')
        {
            $sortBy = 'is_complete DESC, completed_items DESC';
        }
        else if($sortBy == 'last_modified')
        {
            $sortBy = 'last_edit_date DESC';
        }
        else
        {
            $sortBy = 'users.first_name ASC, users.last_name ASC';
        }
        $goal = Goal::with(Goal::getGoalRelations(1,0,1,1))->select('goals.*', DB::raw("(SELECT COUNT(goal_items.id) FROM goal_items WHERE goal_id = $goal_id) AS total_items"))->where("id",$goal_id)->where("is_active",1)->first();
        if(empty($goal))
        {
            return response()->json(["status" => false, "data"=>null, "message" => "Goal Not Found!"]);
        }
        if($goal->actionItems)
        {
            foreach ($goal->actionItems as &$actionItem)
            {
                unset($actionItem->itemFeedbacks);
                $actionItem->itemFeedbacks = GoalItemsFeedback::select('is_done', 'owner_id', 'current_value')->where('goal_id', $actionItem->goal_id)->where("goal_item_id", $actionItem->id)->get();
            }
        }
        $count = 1;
        if($goal->goal_type == Goal::goalTypeGroup)
        {
            $data = DB::select("SELECT 
                                          $goal_id AS goal_id,
                                          (SELECT 
                                            SUM(goal_items_feedback.`is_done`) 
                                          FROM
                                            goal_items_feedback 
                                          WHERE goal_id = $goal_id) AS completed_items,
                                          (SELECT 
                                            COUNT(goal_items.id) 
                                          FROM
                                            goal_items 
                                          WHERE goal_id = $goal_id) AS total_items ");
            if($data && isset($data[0]) && $data[0]->completed_items > $data[0]->total_items)
            {
                $data[0]->completed_items = $data[0]->total_items;
            }
        }
        else if($goal->goal_type == Goal::goalTypeAccount)
        {
            $where = "ua.account_id = $account_id";
            if($only_user_id){
                $where ="ua.account_id = $account_id and users.id = $only_user_id";
            }
            $data = DB::select("SELECT 
                                          IF(gise.id IS NULL, gt.created_date, gise.created_date) AS created_date,
                                          IF(gise.id IS NULL, gt.last_edit_date, gise.last_edit_date) AS last_edit_date,
                                          users.id AS user_id,
                                          users.first_name,
                                          users.last_name,
                                          users.email,
                                          users.image,
                                          COUNT(gise.id) AS completed_items,
                                          IF(
                                            (
                                              COUNT(gise.id) = $goal->total_items 
                                              AND $goal->is_published = 1
                                            ),
                                            1,
                                            0
                                          ) AS is_complete 
                                        FROM
                                          users 
                                          JOIN users_accounts ua 
                                            ON ua.user_id = users.id 
                                          LEFT JOIN goal_items_feedback gise 
                                            ON gise.owner_id = users.id AND gise.is_done = 1 AND gise.goal_id = $goal_id 
                                          LEFT JOIN goal_items gt 
                                            ON gt.id = gise.goal_item_id 
                                        WHERE $where 
                                        GROUP BY users.id ORDER BY $sortBy $pagination_query");
            $count = DB::select("SELECT 
                                          count(users.id) as total_record
                                        FROM
                                          users 
                                          JOIN users_accounts ua 
                                            ON ua.user_id = users.id 
                                          LEFT JOIN goal_items_feedback gise 
                                            ON gise.owner_id = users.id AND gise.is_done = 1 AND gise.goal_id = $goal_id 
                                          LEFT JOIN goal_items gt 
                                            ON gt.id = gise.goal_item_id 
                                        WHERE ua.account_id = $account_id");
            $count = $count[0]->total_record;
        }
        else if($goal->goal_type == Goal::goalTypeIndividual)
        {
            $data = DB::select("SELECT 
                                          IF(gise.id IS NULL, gt.created_date, gise.created_date) AS created_date,
                                          IF(gise.id IS NULL, gt.last_edit_date, gise.last_edit_date) AS last_edit_date,
                                          goal_users.user_id AS user_id,
                                          users.first_name,
                                          users.last_name,
                                          users.email,
                                          users.image,
                                          goal_users.is_archived,
                                          COUNT(gise.id) AS completed_items,
                                          IF(
                                            (
                                              COUNT(gise.id) = $goal->total_items 
                                              AND $goal->is_published = 1
                                            ),
                                            1,
                                            0
                                          ) AS is_complete 
                                        FROM
                                          goal_users 
                                          JOIN users 
                                            ON users.id = goal_users.user_id 
                                          LEFT JOIN goal_items_feedback gise 
                                            ON gise.owner_id = goal_users.user_id AND gise.is_done = 1 AND gise.goal_id = $goal_id
                                          LEFT JOIN goal_items gt 
                                            ON gt.id = gise.goal_item_id 
                                        WHERE goal_users.goal_id = $goal_id 
                                          AND goal_users.user_type = 2 
                                          AND goal_users.is_archived = 0 
                                        GROUP BY goal_users.user_id ORDER BY $sortBy $pagination_query");

            $count = DB::select("SELECT 
                                          count(users.id) as total_record
                                        FROM
                                          goal_users 
                                          JOIN users 
                                            ON users.id = goal_users.user_id 
                                          LEFT JOIN goal_items_feedback gise 
                                            ON gise.owner_id = goal_users.user_id AND gise.is_done = 1 AND gise.goal_id = $goal_id
                                          LEFT JOIN goal_items gt 
                                            ON gt.id = gise.goal_item_id 
                                        WHERE goal_users.goal_id = $goal_id 
                                          AND goal_users.user_type = 2 
                                          AND goal_users.is_archived = 0");
            $count = $count[0]->total_record;
        }
        else
        {
            $data = null;
        }
        if ($goal->goal_type != Goal::goalTypeGroup) {
            $total_items = $goal->total_items;
            foreach ($data as &$item) {
                if ($total_items && $total_items > 0) {
                    $item->percentage = 0;
                    $share = number_format((100 / $total_items), 0);
                    foreach ($goal->actionItems as $itm) {

                        if ($itm->itemFeedbacks) {
                            foreach ($itm->itemFeedbacks as $feedback) {
                                if ($item->user_id == $feedback->owner_id) {
                                    if ($feedback->is_done == 1) {
                                        $item->percentage += (int)($share);
                                    } else {
                                        $itm->current_value = $feedback->current_value;
                                        $itemProgress = $this->checkItemProgress($itm);
                                        if (!is_bool($itemProgress) && is_numeric($itemProgress)) {
                                            $item->percentage += ((int)($share) * $itemProgress / 100);
                                        }
                                    }
                                }

                            }

                        }
                    }
                    $item->percentage = floor($item->percentage);

                } else $item->percentage = '0';
            }
        }

        $result = ["goal_data" => $goal, "user_data" => $data, "total_records"=>$count];
        if($internal)
        {
            return $result;
        }
        return response()->json(["status" => true, "data"=>$result, "message" => "Goal Status Successfully!"]);
    }

    public function getHuddlesList(Request $request)
    {
        $input = $request->all();
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $role_id = $input['role_id'];
        $with_documents = isset($input['with_documents']) ? $input['with_documents'] : 0;
        $search = isset($input['title']) ? $input['title'] : null;
        $huddle_sort = isset($input['sort']) ? $input['sort'] :  2;
        $without_assessment = 1;
        if(isset($input['with_assessment']))
        {
            $without_assessment = 0;
        }

        $accountFolderUsers = AccountFolderUser::join('account_folders', 'AccountFolderUser.account_folder_id', '=', 'account_folders.account_folder_id')
            ->selectRaw("AccountFolderUser.*,folder_type,is_published")
            ->selectRaw("(SELECT meta_data_value FROM account_folders_meta_data as afmd WHERE afmd.account_folder_id=AccountFolderUser.account_folder_id AND meta_data_name = 'folder_type' LIMIT 1) as htype")
            ->where('user_id', $user_id)->where('active', 1)->where('archive', 0)->where('AccountFolderUser.site_id', $this->site_id)->get();

        $accountFolderGroups = UserGroup::get_user_group($user_id, $this->site_id);
        $accountFoldereIds = array();
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                if($row->htype == '3' && $row->folder_type == '1' && $row->role_id != '200' &&  $row->is_published == '0') {
                    continue;
                }
                $accountFoldereIds[] = $row->account_folder_id;
            }
        } else {
            $accountFoldereIds = '0';
        }
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFoldereIds = $row['account_folder_id'];
            }
        }
        $limit = 0;
        $page = 1;
        if($search != null && $search != '')
        {
            //DB::enableQueryLog();
            $page = $request->has("page") ? $request->get("page") : 1;
            $allData = [];
            $allData["htype"] = '';
            $allData["user_id"] = $user_id;
            $allData["role_id"] = $role_id;
            $allData["search"] = $search;
            $allData["limit"] = 8;
            $allData["page"] = $page;
            $allData["doc_type"] = $request->has("doc_type") ? $request->get("doc_type") : 1;
            $c_request = new Request($allData);
            $docs = $this->getHuddleDocuments($c_request, $accountFoldereIds);
            //$executed_quires = DB::getQueryLog();
            return response()->json(["status" => true, "message" => "success", "data" => $docs["items"], "huddle_counts" => $docs["total_records"]]);
        }
        if($with_documents)
        {
            $page = $request->has("page") ? $request->get("page") : 1;
            $limit = 5;
        }
        $results = AccountFolder::getAllAccountHuddles($this->site_id, $account_id, '', FALSE, $accountFoldereIds, false, false, false, $limit, $page, false, "", $huddle_sort,'0', $role_id, 0,1,$without_assessment);
        $huddles = $results["huddles"];
        $huddle_counts = $results["counts"];
        $modified_huddles = [];
        foreach($huddles as $key => &$huddle)
        {
            if ($huddle->folderType == 'assessment') {
                $htype = 3;
                $huddle_name_type = TranslationsManager::get_translation('huddle_list_assessment');
                $huddle_name = 'assessment';
            } elseif ($huddle->folderType == 'coaching') {
                $htype = 2;
                $huddle_name_type = TranslationsManager::get_translation('coaching_list_coaching');
                $huddle_name = 'coaching';
            } else {
                $htype = 1;
                $huddle_name_type = TranslationsManager::get_translation('huddle_list_collaboration');
                $huddle_name = 'collaboration';
            }
            $tmp = $huddle;//sending all data for mobile
            $tmp->type = $huddle_name;
            $tmp->huddle_type = $huddle_name_type;
            $tmp->title = $huddles[$key]['name'];
            $huddle_id = $huddles[$key]['account_folder_id'];
            $tmp->huddle_id = $huddle_id;
            $tmp->created_on = self::getCurrentLangDate(strtotime($huddles[$key]['created_date']), "archive_module");
            if($huddles[$key]['meta_data_value'] == 1)
            {
                $tmp->icon_path = "./assets/img/collaboration_icon.svg";
            }
            else if($huddles[$key]['meta_data_value'] == 2)
            {
                $tmp->icon_path = "./assets/img/coaching_icon.svg";
            }
            else if($huddles[$key]['meta_data_value'] == 3)
            {
                $tmp->icon_path = "./assets/img/assessment_icon.svg";
            }
            else
            {
                $tmp->icon_path = "./assets/img/folder_gry.svg";
            }
            if($with_documents)
            {
                $allData = [];
                $allData["huddle_id"] = $huddle_id;
                $allData["htype"] = $htype;
                $allData["user_id"] = $user_id;
                $allData["role_id"] = $role_id;
                $allData["doc_type"] = $request->has("doc_type") ? $request->get("doc_type") : 1;
                $c_request = new Request($allData);
                $docs = $this->getHuddleDocuments($c_request);
                $tmp->total_documents = $docs["total_records"];
                $tmp->documents = $docs["items"];
                if($tmp->total_documents > 0)
                {
                    $modified_huddles[] = $tmp;
                }
            }
            else
            {
                $modified_huddles[] = $tmp;
            }

        }
       
        return response()->json(["status" => true, "message" => "success", "data" => $modified_huddles, "huddle_counts" => $huddle_counts]);
    }

    public function getHuddleDocuments(Request $request, $huddle_id = false)
    {
        if(!$huddle_id)
        {
            $huddle_id = $request->get("huddle_id");
        }
        $htype = $request->get("htype");
        $user_id = $request->get("user_id");
        $account_id = $request->get("account_id");
        $role_id = $request->get("role_id");
        $doc_type = $request->has("doc_type") ? $request->get("doc_type") : 1;
        $parent_folder_id = $request->has("parent_folder_id") ? $request->get("parent_folder_id") : 0;
        $search = $request->has("search") ? $request->get("search") : "";
        $limit = $request->has("limit") ? $request->get("limit") : 0;
        $page = $request->has("page") ? $request->get("page") : 1;
        $sort = $request->has("sort") ? $request->get("sort") : 'video_title';

        $document_ids = array();
        if(!is_array($huddle_id))
        {
            $evaluator = AccountFolderUser::check_if_evalutor($this->site_id, $huddle_id, $user_id);
        }
        else
        {
            $evaluator = true;
        }

        $is_mobile_request = false;
        if($doc_type == 2)
        {
            $doc_types = [2,5];//also send url artifacts with resource artifacts for goals add evidence!
        }
        else
        {
            $doc_types = [$doc_type];
        }
        if (!$evaluator && $htype == 3) {

            $evaluator_ids = HelperFunctions::get_evaluator_ids($huddle_id, 200);
            array_push($evaluator_ids, $user_id);
            $all_videos = Document::getArtifects($this->site_id, $huddle_id, "", $sort, $limit, $page, $evaluator_ids, $doc_types, '', $htype, $is_mobile_request,"", $parent_folder_id);
            $total_records = $all_videos["total_records"];
            $all_videos = $all_videos["data"];
        } else {
            $all_videos = Document::getArtifects($this->site_id, $huddle_id, $search, $sort, $limit, $page, '', $doc_types, $document_ids, $htype, $is_mobile_request,"", $parent_folder_id);
            $total_records = $all_videos["total_records"];
            $all_videos = $all_videos["data"];
        }

        $items = [];
        if ($all_videos) {
            foreach ($all_videos as $row) {
                $created_by = $row['created_by'];
                if (HuddleController::_check_evaluator_permissions($row["account_folder_id"], $created_by, $user_id, $role_id) == false) {
                    continue;
                } else {
                    $items[] = app('App\Http\Controllers\WorkSpaceController')->conversion_to_thmubs($row, 1);
                }
            }
        }
        $folders = DocumentFolder::getFolders($huddle_id, $account_id, $this->site_id, 0, $parent_folder_id);
        return ["total_records" => $total_records, "items" => $items, "folders" => $folders];
    }

    public function addRecentActivity($data)
    {
        $lang =  isset($data['current_lang']) ? $data['current_lang'] : $this->current_lang ;
        $type = $data["type"];
        $site_id = isset($data['site_id']) ? $data['site_id'] : $this->site_id;
        $desc = $data["item_name"];
        if(isset($data["special"]))
        {
            $desc .= "___".$data["special"];
        }
        $user_activity_logs = array(
            'ref_id' => $data["goal_id"],
            'desc' => $desc,
            'url' => "/goals/view/".$data["goal_id"],
            'account_folder_id' => $data["account_folder_id"],
            'type' => $type,
            'site_id' => $site_id,
            'user_id' => $data["user_id"],
            'source_ref_id' => isset($data["other_user_id"]) ? $data["other_user_id"] : 0,
            'account_id' => $data["account_id"],
            'date_added' => date("Y-m-d H:i:s")
        );
         if (!isset($data['send_notifications']) || (isset($data['send_notifications']) && $data['send_notifications'] == true)){
        DB::table('user_activity_logs')->insertGetId($user_activity_logs);
        }
        //Following Code is for Email make in Asynchronous

        if(!in_array($type, [Goal::activityTypes["created_for_self"],Goal::activityTypes["created_template_goal"],Goal::activityTypes["added_action_item"],Goal::activityTypes["added_evidence"],Goal::activityTypes["action_item_deadline_passed"],Goal::activityTypes["goal_deadline_passed"],Goal::activityTypes["goal_start_date_edited"]]))
        {
            $emails_enabled  = HelperFunctions::is_email_notification_allowed($data["account_id"], 'enable_emails_for_goals');
            if ((!isset($data['send_email']) || (isset($data['send_email']) && $data['send_email'] == true)) && $emails_enabled)
            {

                $goals_name = HelperFunctions::getGoalsAltName($data["account_id"], $site_id);
                $goals_button = HelperFunctions::getGoalAltTranslation(TranslationsManager::get_translation('goals_click_to_view_goal', "", 1, "", "en"), $data["account_id"], $site_id, $goals_name);
                $goals_button_es = HelperFunctions::getGoalAltTranslation(TranslationsManager::get_translation('goals_click_to_view_goal', "", 1, "", "es"), $data["account_id"], $site_id, $goals_name);
                $goal_type = isset($data["goal_type"]) ? $data["goal_type"] : Goal::where("id",$data["goal_id"])->first()->goal_type;
                $user = User::find($data["user_id"]);
                $email_message = $this->getEmailMessage($data,$goals_name,"en",$site_id);
                $email_message_es = $this->getEmailMessage($data,$goals_name,"es",$site_id);
                $date = date("M d, Y \a\\t H:i A");
                $date_es = GetActivities::_SpanishDate(time(), "spanish_day_month_year"). " a las ". date("H:i A");

                $site_email_subject = Sites::get_site_settings('email_subject', $site_id);
                $site_title = Sites::get_site_settings('site_title', $site_id);
                $from = $site_title . '<' . Sites::get_site_settings('static_emails', $site_id)['noreply'] . '>';

                $data["replace_ables"] = [
                    "{USER_NAME}" => $user->first_name ." ". $user->last_name,
                    "{ITEM_NAME}" => $data["item_name"],
                ];
                if($type == Goal::activityTypes['goal_deleted'])
                {
                    $data["replace_ables"]['id="link_to_goal"'] = 'id="link_to_goal" style="display:none"';
                }
                if(isset($data["other_user_id"]))
                {
                    $ouser = User::find($data["other_user_id"]);
                    $data["replace_ables"]["{OTHER_USER_NAME}"] = $ouser->first_name." ".$ouser->last_name;
                }
                $subject = HelperFunctions::getGoalAltTranslation(Goal::translations[$type]["s_en"], $data["account_id"], $site_id, $goals_name);
                $subject_es = HelperFunctions::getGoalAltTranslation(Goal::translations[$type]["s_es"], $data["account_id"], $site_id, $goals_name);
                $data["key"] = "goal_notifications_";
                $data["url"] = "home".$user_activity_logs["url"];
                $data["unsubscribe_email_id"] = 14;
                $data["email"] = "";
                if($user->lang == "en")
                {
                    $data["email"] = $user->email.",";
                }
                if($goal_type == Goal::goalTypeAccount)
                {
                    $query = User::join("users_accounts", "users.id", "users_accounts.user_id")->where("users_accounts.account_id", $data["account_id"])->where("users_accounts.is_active", 1)->select("users.id","users.email", DB::raw('(SELECT IF(COUNT(*) > 0, user_type, 2) AS user_type FROM goal_users WHERE  goal_id = '.$data["goal_id"].' AND user_id = users.id LIMIT 1) AS user_type'));
                }
                else
                {
                    $query = GoalUser::join("users", "users.id", "=", "goal_users.user_id")->join("users_accounts", "users.id", "users_accounts.user_id")->select("users.id","users.email", "goal_users.user_type")->where("goal_id", $data["goal_id"])->where("users_accounts.account_id", $data["account_id"])->where("users_accounts.is_active", 1);
                }
                $en_query = clone $query;
                $gUsers = $en_query->where("users.lang", "en")
                    ->whereRaw("(select count(*) from email_unsubscribers where user_id = users.id and account_id = ".$data['account_id']. " and email_format_id = 14) = 0")
                    ->get();

                $data["subject"] = $subject;
                $data["lang"] = "en";
                $data["replace_ables"]["{MESSAGE}"] = $email_message;
                $data["replace_ables"]["{CREATED_DATE}"] = $date;
                $data["replace_ables"]["{BUTTON_TEXT}"] = $goals_button;
                $jobQueues = [];
                $auditEmails = [];

                $key = $data["key"] . $site_id . "_en";
                $result = SendGridEmailManager::get_send_grid_contents($key);
                $html = $result->versions[0]->html_content;

                $html = str_replace('<%body%>', '', $html);
                foreach ($gUsers as $gUser)
                {
                    if($goal_type != Goal::goalTypeGroup && in_array($type, [Goal::activityTypes["mark_incomplete"],Goal::activityTypes["added_feedback"],Goal::activityTypes["mark_complete"]]) && $gUser->user_type == Goal::userTypeOwner && $gUser->id != $data['owner_id'])
                    {
                        continue;
                    }
                    $data["email"] = $gUser->email;
                    $data["user_id"] = $gUser->id;
                    $emailData = $this->sendGenericEmail($data, true, true, $site_email_subject,$from,$site_title, $html);
                    if($emailData["error"] == 0)
                    {
                        $jobQueues[] = $emailData["jobQueue"];
                        $auditEmails[] = $emailData["auditEmail"];
                    }
                }

                // Following code send spanish email
                $data["email"] = "";
                $data["subject"] = $subject_es;
                $data["lang"] = "es";
                $data["replace_ables"]["{MESSAGE}"] = $email_message_es;
                $data["replace_ables"]["{CREATED_DATE}"] = $date_es;
                $data["replace_ables"]["{BUTTON_TEXT}"] = $goals_button_es;
                $data["replace_ables"]["{ITEM_NAME}"] = $data["item_name"];
                $gUsers_es = $query->where("users.lang", "es")
                    ->whereRaw("(select count(*) from email_unsubscribers where user_id = users.id and account_id = ".$data['account_id']. " and email_format_id = 14) = 0")
                    ->get();
                $key = $data["key"] . $site_id . "_es";
                $result = SendGridEmailManager::get_send_grid_contents($key);
                $html = $result->versions[0]->html_content;

                $html = str_replace('<%body%>', '', $html);
                foreach ($gUsers_es as $gUser)
                {
                    if($goal_type != Goal::goalTypeGroup && in_array($type, [Goal::activityTypes["mark_incomplete"],Goal::activityTypes["added_feedback"],Goal::activityTypes["mark_complete"]]) && $gUser->user_type == Goal::userTypeOwner && $gUser->id != $data['owner_id'])
                    {
                        continue;
                    }
                    $data["email"] = $gUser->email;
                    $data["user_id"] = $gUser->id;
                    $emailData = $this->sendGenericEmail($data,  true, true, $site_email_subject,$from,$site_title, $html);
                    if($emailData["error"] == 0)
                    {
                        $jobQueues[] = $emailData["jobQueue"];
                        $auditEmails[] = $emailData["auditEmail"];
                    }
                }
                if(!empty($jobQueues))
                {
                    $jobQueueChunks = array_chunk($jobQueues, 1000);
                    foreach ($jobQueueChunks as $jobQueueChunk)
                    {
                        JobQueue::insert($jobQueueChunk);
                    }
                }
                if(!empty($auditEmails))
                {
                    $auditEmailChunks = array_chunk($auditEmails, 1000);
                    foreach ($auditEmailChunks as $auditEmailChunk)
                    {
                        \DB::table('audit_emails')->insert($auditEmailChunk);
                    }
                }
            }
        }
    }

    public function getEmailMessage($data,$goals_name,$lang,$site_id)
    {
        $type = $data["type"];
        $desc = Goal::translations[$type][$lang];
        $email_message = HelperFunctions::getGoalAltTranslation($desc, $data["account_id"], $site_id, $goals_name);
        if(isset($data["special"]))
        {
            $special_lang = explode("||", $data["special"]);
            if(isset($special_lang[1]))
            {
                $email_message = str_replace('{SPECIAL}', ($lang == "en"?$special_lang[0]:$special_lang[1]), $email_message);
            }
            else
            {
                $email_message = str_replace('{SPECIAL}', $special_lang[0], $email_message);
            }
        }
        $email_message = str_replace(' {ITEM_NAME} on {CREATED_DATE}', "", $email_message);
        $email_message = str_replace('{Link_SEPRATE}', "", $email_message);
        $email_message = str_replace(' on {CREATED_DATE}', "", $email_message);
        $email_message = str_replace('{ITEM_NAME}', "", $email_message);

        if(isset($data["other_user_id"]))
        {
            $other_user = User::where("id", $data["other_user_id"])->first();
            $email_message = str_replace('{OTHER_USER_NAME}', $other_user->first_name . " " . $other_user->last_name, $email_message);
        }
        return $email_message;
    }

    public function addGoalItemStandards(Request $request)
    {
        $goal_id = $request->get("goal_id");
        $goal_item_id = $request->get("item_id");
        $evidence_id = $request->get("evidence_id");
        $standards = $request->get("standards");
        $user_id = $request->get("user_id");
        if(!empty($standards) && !is_array($standards))
        {
            $standards = json_decode($standards, true);
        }
        GoalItemStandardRating::where("evidence_id", $evidence_id)->delete();
        $data = [];
        foreach ($standards as $standard)
        {
            $data[] = [
                "evidence_id" => $evidence_id,
                "standard_id" => $standard["standard_id"],
                "rating_id" => $standard["rating_id"],
                "rating_value" => $standard["rating_value"],
                "created_by" => $user_id,
                "created_at" => date("Y-m-d H:i:s"),
                "site_id" => $this->site_id
            ];
        }
        GoalItemStandardRating::insert($data);
        $standards = GoalItemStandardRating::where("evidence_id", $evidence_id)->get();
        $event = [
            'channel' => "goal-".$goal_id,
            'event' => "added_standards",
            'data' => $standards,
            'goal_id' => $goal_id,
            'goal_item_id' => $goal_item_id,
            "evidence_id" => $evidence_id,
            'from_goals' => true
        ];
        HelperFunctions::broadcastEvent($event,"broadcast_event");
        return response()->json(["status" => true, "message" => "Success", "data" => $standards] );
    }

    public function getEvidenceStandards(Request $request)
    {
        $evidence_id = $request->get("evidence_id");
        $goal_id = $request->get("goal_id");
        $user_id = $request->get("user_id");
        $account_id = $request->get("account_id");
        $framework_id = $request->get("framework_id");
        $standards = GoalItemStandardRating::where("evidence_id", $evidence_id)->get();

        $standards_settings = AccountFrameworkSetting::where(array(
            "account_id" => $account_id,
            "site_id" => $this->site_id,
            "account_tag_id" => $framework_id
        ))->first();
        $performance_levels = array();
        if($standards_settings)
        {
            $performance_levels = AccountFrameworkSettingPerformanceLevel::where(array(
                'account_framework_setting_id' => $standards_settings->id,
                'site_id' => $this->site_id
            ))->get();
        }
        else
        {
            $performance_levels = [];
        }

        $old_ratings = AccountMetaData::where(array(
            'account_id' => $account_id,
            'site_id' => $this->site_id
        ))->whereRaw('meta_data_name like "metric_value_%"')->orderBy('meta_data_value')->get()->toArray();
        $goal = Goal::with(['collaborators','owners'])->where("id",$goal_id)->where("is_active",1)->first();
        $is_allowed = false;
        $edit_permission = false;
        $goal_settings = $this->getGoalsSettings($account_id, 1);
        $goal_user_settings = GoalUserSettings::where("user_id", $user_id)->where("account_id", $account_id)->where("site_id", $this->site_id)->first();
        if($goal_settings->make_all_goals_public || $goal_user_settings->can_view_goal_progress)
        {
            $is_allowed = true;
            $edit_permission = false;
        }
        if(($goal_settings->group_type_goals_public && $goal->goal_type == Goal::goalTypeGroup) && ($goal_settings->individual_type_goals_public && $goal->goal_type == Goal::goalTypeIndividual) && ($goal_settings->account_type_goals_public && $goal->goal_type == Goal::goalTypeAccount))
        {
            $is_allowed = true;
            $edit_permission = false;
        }
        if($goal)
        {
            $owners = $goal->owners->toArray();
            $collaborators = $goal->collaborators->toArray();
            $owner_index = HelperFunctions::searchArrayByValue($user_id,$owners);
            $collaborator_index = HelperFunctions::searchArrayByValue($user_id,$collaborators);
            if($goal->goal_type == Goal::goalTypeAccount)
            {
                $is_allowed = true;
                $edit_permission = true;
            }
            else if($owner_index !== null)
            {
                $is_allowed = true;
                $edit_permission = true;
            }
            else if($collaborator_index !== null)
            {
                $permissions = explode(",", $collaborators[$collaborator_index]['permission']);
                if(in_array(GoalUser::permissionEvidence, $permissions) || in_array(GoalUser::permissionReview, $permissions) )
                {
                    $is_allowed = true;
                    $edit_permission = true;
                }
                else if(in_array(GoalUser::permissionView, $permissions))
                {
                    $is_allowed = true;
                    $edit_permission = false;
                }
            }
        }
        else
        {
            return response()->json(["status" => false, "message" => "Goal not found!", "data" => null]);
        }
        return response()->json(["status" => true, "message" => "Success", "data" => $standards, "ratings" => $old_ratings, "performance_levels" =>$performance_levels, "is_allowed" => $is_allowed, "edit_permission" => $edit_permission] );
    }

    public function getAverageRating(Request $request)
    {
        $account_id = $request->get("account_id");
        $framework_id = $request->get("framework_id");
        $evidence_id = $request->get("evidence_id");
        $average_rating = round(GoalItemStandardRating::where("evidence_id", $evidence_id)->avg("rating_value"));
        if(empty($average_rating))
        {
            $rating_name = "No Rating";
        }
        else
        {
            $standards_settings = AccountFrameworkSetting::where(array(
                "account_id" => $account_id,
                "site_id" => $this->site_id,
                "account_tag_id" => $framework_id
            ))->first();

            $performance_levels = AccountFrameworkSettingPerformanceLevel::where(array(
                'account_framework_setting_id' => $standards_settings->id,
                'performance_level_rating' => $average_rating,
                'site_id' => $this->site_id
            ))->first();

            $rating_name = $performance_levels ? $performance_levels->performance_level : "";
            if(!$performance_levels)
            {
                $old_rating = AccountMetaData::where(array(
                    'account_id' => $account_id,
                    'meta_data_value' => $average_rating,
                    'site_id' => $this->site_id
                ))->whereRaw('meta_data_name like "metric_value_%"')->orderBy('meta_data_value')->first();
                $rating_name = $old_rating ? explode("_", $old_rating->meta_data_name)[2] : "";
            }
        }

        return response()->json(["status" => true, "message" => "Success", "data" => ["average_rating" => $average_rating, "rating_name" => $rating_name]] );
    }

    function sendGenericEmail($data, $onlyJobQueue = false, $isBulk = false, $site_email_subject = '', $from = '', $site_title = '', $html = '') {
        $site_id = isset($data['site_id']) ? $data['site_id'] : $this->site_id;
        if ($isBulk && !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            return ['error' => 1, 'message' => TranslationsManager::get_translation('Invalid_Email', 'flash_messages', $this->site_id)];
        }

        $lib_user_id = $data['user_id'];
        //$users = User::where('id', $lib_user_id)->first();
        if(empty($site_title))
        {
            $site_title = Sites::get_site_settings('site_title', $site_id);
        }
        if(empty($from))
        {
            $from = $site_title . '<' . Sites::get_site_settings('static_emails', $site_id)['noreply'] . '>';
        }
        $to = $data['email'];
        $sibme_base_url = config('s3.sibme_base_url');
        $params = array(
            'data' => $data,
        );
        if(empty($site_email_subject))
        {
            $site_email_subject = Sites::get_site_settings('email_subject', $site_id);
        }
        $lang = $data['lang'];

        $subject = $site_email_subject. " - " . $data["subject"];
        if(empty($html))
        {
            $key = $data["key"] . $site_id . "_" . $lang;

            $result = SendGridEmailManager::get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;

            $html = str_replace('<%body%>', '', $html);
        }

        if(isset($data["replace_ables"]))
        {
            foreach ($data["replace_ables"] as $replace_key => $replace_able_value)
            {
                $html = str_replace($replace_key, $replace_able_value, $html);
            }
        }
        //TODO following $lib_user_id is incorrect for email going to multiple users
        $unsubscribe_url = $sibme_base_url . '/subscription/unsubscirbe_now/' . $lib_user_id . '/'.$data["unsubscribe_email_id"];
        $html = str_replace('{redirect_url}', $sibme_base_url . $data['url'] , $html);
        $html = str_replace('{redirect}', $sibme_base_url . $data['url'] , $html);
        $html = str_replace('{unsubscribe_url}', $unsubscribe_url, $html);
        $html = str_replace('{unsubscribe}', $unsubscribe_url, $html);
        $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);


        $auditEmail = array(
            'account_id' => $data['account_id'],
            'site_id' =>$site_id,
            'email_from' => $from,
            'email_to' => $data['email'],
            'email_subject' => $subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $use_job_queue = config('s3.use_job_queue');

        if ($use_job_queue || $onlyJobQueue) {
            $jobQueueEntry = JobQueue::add_job_queue($site_id, 1, $to, $subject, $html, true, '', '', '', $isBulk);
            if($isBulk)
            {
                return ['error' => 0, 'jobQueue' => $jobQueueEntry, 'auditEmail' => $auditEmail];
            }
            \DB::table('audit_emails')->insert($auditEmail);
            return ['error' => 0];
        } else {
            $response = Email::sendEmail($site_id, $html, $params, $to, '', $subject);
            if ($response !== true) {
                $auditEmail['error_msg'] = $response;
                \DB::table('audit_emails')->insert($auditEmail);
                return ['error' => 1,
                    'message' => TranslationsManager::get_translation('sorry_your_email_could_not_be_sent_right_now_please_try_again', 'flash_messages', $this->site_id),
                    'exception' => $response];
            } else {
                \DB::table('audit_emails')->insert($auditEmail);
                return ['error' => 0];
            }
        }

        return FALSE;
    }
    function print_excel_goal_comment0s(Request $request){
        $input = $request->all();
        $user_id = $input['user_id'];
    }

    function print_pdf_goal_comments(Request $request){
        $input = $request->all();
        $user_id = $input['user_id'];
    }

    function sortActionItem(Request $request){
        $input = $request->all();
        $data =!empty($input['actionItems'])?$input['actionItems']:'';
        $is_updated = false;
        if(count($data) > 0){
            foreach($data as $key=>$row){
                $is_updated = GoalItem::where("id", $row['id'])->update([
                        "item_order" => $key,
                        "last_edit_by" => $row['user_id'],
                        "last_edit_date" => date("Y-m-d H:i:s")
                    ]);   
            }
            if($is_updated){
                return response()->json(["status" => true, "data" => null]);
            }else{
                return response()->json(["status" => false,"data" => null]);
            }
            
        }else{
            return response()->json(["status" => false,"data" => null]);
        }
    }

    public function goalsExportData(Request $request)
    {
        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");
        $current_lang = $request->has("current_lang") ? $request->get("current_lang") : 'en';
        $translations = TranslationsManager::get_page_lang_based_content("global", $this->site_id, $user_id, $current_lang);
        $get_goals = $this->getGoals($request,true,true);
        // $get_goals = $get_goals->getData();
        // $get_goals = json_decode(json_encode($get_goals),true);
        $goalss = [];
        foreach($get_goals as $goal){
            $goal = json_decode(json_encode($goal), true);
            $goals_owners = $this->getOwners($goal['owners']);
            $goal_row = [];
            $goal_row ['title'] = $goal['title'];
            $goal_row ['edtpa_type'] = $this->getGoalsType($goal['goal_type'],$translations);
            $goal_row ['last_update'] = HelperFunctions::formatReadableDate($goal['last_edit_date']);
            $goal_row ['due_date_all'] = HelperFunctions::formatReadableDate($goal['end_date']);
            $goal_row ['goal_published'] = empty($goal['is_published']) ? "Unpublished" : HelperFunctions::formatReadableDate($goal['publish_date']);
            // if(empty($goal_row ['goal_published'])){
            //     $goal_row ['goal_published'] = 'Dec 31, 1969';
            // }
            $goal_row ['goals_owner'] = $goals_owners;
            $goal_row ['status_all'] = 0;
            $goal_row['progress'] = '';
        
            if($goal['goal_type']==3 || ($goal['goal_type']==4 && count($goal['owners_updated'])==1)){
                // print_r("val :" . $goal->is_published);

                if ($goal['total_items'] && $goal['total_items'] > 0) {
                    $totalItems = count($goal['action_items']);
                    $share =  floor(1/$totalItems*100);
                    foreach($goal['action_items'] as $item)
                    {
                        if($item["item_feedbacks"] ){
                            if($item["item_feedbacks"]["is_done"] == 1){
                                $goal_row ['status_all'] += $share;
                            }else{
                              $item["current_value"] = $item["item_feedbacks"]["current_value"];
                              $actionItem =  \App\Services\Exports\Goals\ActionItems\Export::ActionItems($item);
                              $itemProgress = $actionItem->getProgress(true);
        
                              if($itemProgress > 0){
                                $goal_row ['status_all'] += ($share * $itemProgress/100);
                                }
                            }
                          }
                    }
                    $goal_row ['status_all'] = floor($goal_row ['status_all'] ) . "%";
                }
                else{
                    $goal_row ['status_all'] = "0%";
                }

                // $items_count = !empty($goal->total_items) ? $goal->total_items : $goal->action_items_count;
                if($goal['total_items'] && ($goal['total_items'] == $goal['completed_items'])){
                    //$goal_row['status_all'] = round(($goal['completed_items'] / $goal['total_items']) * 100). "%"; 
                    $goal_row['progress'] = 'Complete';
                    
                }
                else{
                   // $goal_row ['status_all'] = '0%';
                    $goal_row['progress'] = 'Not Started';
                }
            // print_r($goal->action_items);
            
            }
            if($goal['is_complete'] == '0' && strtotime($goal['end_date']) < strtotime(date('Y-m-d')) && ($goal['goal_type']==3 || ($goal['goal_type']==4 && count($goal['owners_updated'])==1)) && $goal['is_published'] == '1' )
                {
                    // $goal_row ['status_all'] = '%0';
                    //$goal_row ['status_all'] = !empty($goal['total_items']) ? round(($goal['completed_items'] / $goal['total_items']) * 100). "%" : "0%";
                    $goal_row['progress'] = 'In Complete';
                }
            if(($goal['goal_type']==1 || $goal['goal_type']==2) && $goal['is_published'] == '0'){
                //$goal_row ['status_all'] = '';
                $goal_row['progress'] = '';
            }
            if($goal['is_complete'] == '0'  && strtotime($goal['end_date']) > strtotime(date('Y-m-d')) && ($goal['goal_type']==3 || ($goal['goal_type']==4 && count($goal['owners_updated'])==1))  && $goal['is_published'] == '1'){ 
            if(count($goal['action_items'])>0){
                foreach($goal['action_items'] as $row){
                    $itemFeedbacks = isset($row["item_feedbacks"]) ? $row["item_feedbacks"] : (isset($row["itemFeedbacks"]) ? $row["itemFeedbacks"] : null);

                    if($itemFeedbacks){
                    if((isset($itemFeedbacks['reflection']) && !empty($itemFeedbacks['reflection']))  || $itemFeedbacks['current_value']){
                    //$goal_row ['status_all'] = !empty($goal['total_items']) ? round(($goal['completed_items'] / $goal['total_items']) * 100). "%" : "0%";
                    // $goal_row ['status_all'] = '%0';  
                    $goal_row['progress'] = 'In Progress';
                    }
                    }
                    if(count($row['submitted_evidence'])>0){
                        // $goal_row ['status_all'] = '%0';
                       // $goal_row ['status_all'] = !empty($goal['total_items']) ? round(($goal['completed_items'] / $goal['total_items']) * 100). "%" : "0%";
                      $goal_row['progress'] = 'In Progress';
                    }
                }
            }
        }

            $goalss [] = $goal_row;
            // print_r($goalss);
        }
        
        

        $goal_settings = $this->getGoalsSettings($account_id, 1);
        $goal_user_settings = GoalUserSettings::where("user_id", $user_id)->where("account_id", $account_id)->where("site_id", $this->site_id)->first();
        $export_type = $request->has("export_type") ? $request->get("export_type") : 'excel';

        $user_role = UserAccount::join("accounts", "accounts.id", "=", "UserAccount.account_id")->select("UserAccount.*", "accounts.enable_goals")->where("account_id",$account_id)->where("UserAccount.user_id",$user_id)->where("UserAccount.site_id",$this->site_id)->first();
        if(!$user_role->enable_goals)
        {
            return response()->json(["data"=>[], "message"=>"Goals are disabled for this account", "success"=> false]);
        }
        //DB::enableQueryLog();

        $translations = TranslationsManager::get_page_lang_based_content("global", $this->site_id, $user_id, $current_lang);
        $accounts_data = DB::table('accounts')->where(array('id' => $account_id, 'site_id' => $this->site_id))->first();      
        $user_data = DB::table('users')->where(array('id' => $user_id, 'site_id' => $this->site_id))->first();

        // if($export_type == 'excel'){
        //     $exportGoals = new \App\Services\Exports\Goals\GoalsList\ExportToExcel;
        // }elseif($export_type == 'pdf'){
        //     $exportGoals = new \App\Services\Exports\Goals\GoalsList\ExportToPDF;

        // }
        $header_data = array(
            'exported_by'=>$user_data->first_name.' '.$user_data->last_name,
            'account_name'=>$accounts_data->company_name,
            'report_name'=>$accounts_data->company_name.'_Goals-List_'.date('m_d_y'),
            'export_date'=>date('m/d/Y h:iA')
        );
        include(app()->basePath('app') . '/Header/WriteHTML.php');
        $pdf =new \PDF_HTML();
        $pdf->AddPage();
        // $pdf->SetFont('Arial','B',16);
        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(true, 15);
        
        $pdf->Image(config('s3.sibme_base_url')."/img/pdf_logo.png", 10, 10);
        $pdf->SetFont('Arial');

        $pdf->cell(105,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Account Name:',0,0);
        $pdf->SetFont('Arial','',8);
        // $pdf->SetTextColor(6,69,173);
        $pdf->cell(22,5,$header_data['account_name'],0,1);
        // $pdf->SetTextColor(0, 0, 0);

        $pdf->cell(105,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Report Name:',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->cell(22,5,$header_data['report_name'],0,1);

        $pdf->cell(105,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Exported By:',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->cell(22,5,$header_data['exported_by'],0,1);

        $pdf->cell(105,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Exported Date:',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->cell(22,5,$header_data['export_date'],0,1);
        
        $pdf->cell(0,5,'','B',1);
        
        $pdf->Ln(7);
        foreach ($goalss as $goal){  
            $cell_data = "";
               $pdf->SetFont('Arial','',13);
                $cell_data .= "\t\t\t\t" .chr(149) . "\t\t" . "Title : " . $goal['title'] . "\n"; 
                $cell_data .= "\t\t\t\t" .chr(149) . "\t\t" . "Type : " . $goal['edtpa_type'] . "\n"; 
                $cell_data .= "\t\t\t\t" .chr(149) . "\t\t" . "Last Update : " . $goal['last_update'] . "\n"; 
                $cell_data .= "\t\t\t\t" .chr(149) . "\t\t" . "Due Date : " . $goal['due_date_all'] . "\n"; 
                $cell_data .= "\t\t\t\t" .chr(149) . "\t\t" . "Published : " . $goal['goal_published'] . "\n"; 
                $cell_data .="\t\t\t\t" . chr(149) . "\t\t" . "Owner : " . $goal['goals_owner'] . "\n"; 
                $cell_data .= "\t\t\t\t" .chr(149) . "\t\t" . "Status : " . $goal['status_all'] . "\n"; 
                $cell_data .= "\t\t\t\t" .chr(149) . "\t\t" . "Progress  : " . $goal['progress'] . "\n"; 
            $pdf->MultiCell(180,8, $cell_data
                    ,1,2);
            // $pdf->cell(0,10,$subtitle ['time_range'],0,1);
                $pdf->Ln(4);
                // if($rec % 2){
                //     $pdf->AddPage();
                // }
               
        }
      
        
        // $pdf->WriteHTML($contents);
        if($export_type == 'pdf'){
        $today = "goals";
        return $pdf->Output('D',$today .'.pdf',true);
        }
        elseif($export_type == 'excel'){
            $exportGoals = new \App\Services\Exports\Goals\GoalsList\ExportToExcel;
            return $exportGoals->export($goalss, $translations,$header_data);
        }
        return $get_goals;
    }

    public function getOwners($owners){
        $final_owners = [];
        foreach ($owners as $owner) {
            $final_owners [] = $owner['first_name'] ." ". $owner['last_name'];
        }
        return implode(", ", $final_owners);
    }

    public function getGoalsType($type_id,$translations){
        $trans_key = $this->goals_type_translation_key[$type_id];
        return $translations[$trans_key];
    }

    public function getGoalStatusExported(Request $request)
    {
        include(app()->basePath('app') . '/Header/WriteHTML.php');
        $pdf =new \PDF_HTML();
        $goal_id = $request->get("goal_id");
        $account_id = $request->get("account_id");
        $user_id = $request->get("user_id");
        $current_lang = $request->has("current_lang") ? $request->get("current_lang") : 'en';
        $export_type = $request->has("export_type") ? $request->get("export_type") : 'excel';
        $result = $this->getGoalStatus($request, 1);

        $translations = TranslationsManager::get_page_lang_based_content("global", $this->site_id, null, $current_lang);
        $accounts_data = DB::table('accounts')->where(array('id' => $account_id, 'site_id' => $this->site_id))->first();      
        $user_data = DB::table('users')->where(array('id' => $user_id, 'site_id' => $this->site_id))->first();
        $header_data = array(
            'exported_by'=>$user_data->first_name.' '.$user_data->last_name,
            'account_name'=>$accounts_data->company_name,
            'report_name'=>$accounts_data->company_name.'_Multi-Individual_'.date('m_d_y'),
            'export_date'=>date('m/d/Y h:iA')
        );
        $pdf->AddPage();
        // $pdf->SetFont('Arial','B',16);
        $pdf->AliasNbPages();
        $pdf->SetAutoPageBreak(true, 15);
        
        $pdf->Image(config('s3.sibme_base_url')."/img/pdf_logo.png", 10, 10);
        $pdf->SetFont('Arial');

        $pdf->cell(105,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Account Name:',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->cell(22,5,$header_data['account_name'],0,1);

        $pdf->cell(105,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Report Name:',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->cell(22,5,$header_data['report_name'],0,1);

        $pdf->cell(105,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Exported By:',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->cell(22,5,$header_data['exported_by'],0,1);

        $pdf->cell(105,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Exported Date:',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->cell(22,5,$header_data['export_date'],0,1);
        
        $pdf->cell(0,5,'','B',1);
        
        $goal = $result['goal_data'];
        $users = $this->formatUsers($result['user_data'], $goal);
        $result['user_data'] = $users;
        $pdf->SetAutoPageBreak(true, 15);
        
        $pdf->cell(2,5,'',0,10);
        $pdf->SetFont('Arial','B',15);
        $pdf->cell(40,5,'Title: '.$goal->title,0,1);

        $pdf->cell(2,5,'',0,4);
        $pdf->SetFont('Arial','B',10);
        $pdf->cell(40,5,'Description: '. strip_tags($goal->desc),0,1);

        $pdf->cell(20,5,'',0,4);
        $pdf->SetFont('Arial','B',10);
        $pdf->cell(40,5,'Start date: '. \App\Services\HelperFunctions::formatReadableDate($goal->start_date) . ' - Due date: ' . \App\Services\HelperFunctions::formatReadableDate($goal->end_date),0,1);

        $pdf->cell(2,5,'',0,4);
        $pdf->SetFont('Arial','B',10);
        $pdf->cell(40,5,'Export date: '. date("M d, Y"),0,1);
        $pdf->Ln(3);
        $pdf->SetFont('Arial','',10);
        
        $pdf->Cell(130,6,'User',1,0);
        $pdf->Cell(30,6,'Last Update',1,0);
        $pdf->Cell(30,6,'Status',1,1);
        // $pdf->SetFont('Arial','B',7);
        // $pdf->MultiCell(80,6, 'User'
        //             ,1,1);
        // $pdf->MultiCell(30,6, 'Last Update'
        //             ,1,1);
        // $pdf->MultiCell(40,6, 'Status'
        //             ,1,1);
        foreach($users as $user){ 
            $pdf->cell(2,1,'',0,2);  
            // $cell_data = "";
            $pdf->Cell(130,6,$user['title'],1,0);
            $pdf->Cell(30,6,$user['last_update'],1,0);
            $pdf->Cell(30,6,$user['status_all'],1,1);
            //     $cell_data .=  $user['title'] . "\n"; 
            //     $cell_data .=  $user['last_update'] . "\n"; 
            //     $cell_data .= $user['status_all'] . "\n"; 
            // $pdf->MultiCell(0,6, $cell_data
            //         ,0,1);
            // $pdf->cell(0,10,$subtitle ['time_range'],0,1);
                // $pdf->Ln(2);
               
        }
      
        if($export_type == 'excel'){
            $exportGoals = new \App\Services\Exports\Goals\Detail\ExportToExcel;
            return $exportGoals->export($result, $translations,$header_data);
        }
        // $pdf->WriteHTML($contents);
        elseif($export_type == 'pdf'){
        $today = "goal_details";
        return $pdf->Output('D',$today .'.pdf',true);
        }

        // elseif($export_type == 'pdf'){
        //     $exportGoals = new \App\Services\Exports\Goals\Detail\ExportToPDF;
        // }
        // return response()->json(["status" => true, "data"=>$result, "message" => "Goal Status Successfully!"]);
    }
    public function formatUsers($usersToExport, $goal){
        $users = [];
        foreach ($usersToExport as $user) {
            // $goal_row = [];
            // $goal_row ['title'] = $goal->title;
            // $goal_row ['edtpa_type'] = $goal->goal_type;
            // $goal_row ['last_update'] = $goal->last_edit_date;
            // $goal_row ['due_date_all'] = $goal->end_date;
            // $goal_row ['goal_published'] = $goal->publish_date;
            // $goal_row ['goals_owner'] = $goals_owners;
            // $items_count = !empty($goal->total_items) ? $goal->total_items : $goal->action_items_count;
            // $goal_row ['status_all'] = !empty($items_count) ? round(($goal->completed_items / $items_count) * 100). "%" : "0%";
            $user_row = [];
            $user_row ['title'] = $user->first_name ." ".$user->last_name . " (" .$user->email. ")";
            $user_row ['last_update'] = HelperFunctions::formatReadableDate($user->last_edit_date);

            $total_items = $items_count = !empty($goal->total_items) ? $goal->total_items : $goal->action_items_count;
            if ($goal->goal_type != Goal::goalTypeGroup) {

                    if ($total_items && $total_items > 0) {
                        $user->percentage = 0;
                        $share = number_format((100 / $total_items), 0);
                        foreach ($goal->actionItems as $itm) {

                            if ($itm->itemFeedbacks) {
                                foreach ($itm->itemFeedbacks as $feedback) {
                                    if ($user->user_id == $feedback->owner_id) {
                                        if ($feedback->is_done == 1) {
                                            $user->percentage += (int)($share);
                                        } else {
                                            $itm->current_value = $feedback->current_value;
                                            $itemProgress = $this->checkItemProgress($itm);
                                            if (!is_bool($itemProgress) && is_numeric($itemProgress)) {
                                                $user->percentage += ((int)($share) * $itemProgress / 100);
                                            }
                                        }
                                    }

                                }

                            }
                        }
                        $user->percentage = floor($user->percentage);

                    } else $user->percentage = '0';
                }

            $user_row ['status_all'] = !empty($user->percentage) ? $user->percentage."%" : "0%";
        
            // $user_row ['status_all'] = "0%";

            $users [] = $user_row;
        }
        return $users;
    }
    function create_churnzero_event($event_name,$account_id,$user_email,$quantity=1,$custom_fields='')
    {
           if(strpos($user_email,"@sibme.com") || strpos($user_email,"@jjtestsite.us")   )
            {
              return true;
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://analytics.churnzero.net/i?appKey=invw-q7Ivjwby8NI1F6qQcH1Gix0811ja7-Li4_1xWg&accountExternalId='.$account_id.'&contactExternalId='.$user_email.'&action=trackEvent&eventName='.$event_name.'&description='.$event_name.'&quantity='.$quantity.$custom_fields );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            return true;
        
    }

    public function archive_unarchive_goal(Request $request)
    {
        $user_id = $request->get('user_id');
        $account_id = $request->get('account_id');
        $archive = $request->has('archive') ? $request->get('archive')  : 0;
        $archive_all = 0;
        if($archive == 2)
        {
            $archive = 1;
            $archive_all = 1;
        }
        $goal_id = $request->get('goal_id');
        $is_collaborator_edit = false;

        $collaborator = GoalUser::where('user_id', $user_id)->where('goal_id', $goal_id)->where('user_type', Goal::userTypeCollaborator)->first();
        $owner = GoalUser::where('user_id', $user_id)->where('goal_id', $goal_id)->where('user_type', Goal::userTypeOwner)->first();
        if($collaborator)
        {
            if(in_array(GoalUser::permissionEdit, explode(',',$collaborator->permission)))
            {
                $is_collaborator_edit = true;
            }
        }
        $goal = Goal::find($goal_id);
        if(!$goal || empty($user_id))
        {
            return response()->json(['success'=>false, 'message'=> 'Invalid data given']);
        }
        $is_creator = $goal->created_by == $user_id;
        if($goal->goal_type != Goal::goalTypeIndividual)
        {
            $goal->is_archived = $archive;
            $goal->last_edit_by = $user_id;

            $goal->save();
            $updated = 1;
        }
        else
        {
            $query = GoalUser::where('goal_id', $goal->id)->where('user_type', 2);
            if((!$is_creator || ($is_creator && !$archive_all && $owner && $archive)) && (!$is_collaborator_edit || ($is_collaborator_edit && !$archive_all && $owner)))
            {
                $query->where('user_id', $user_id);
            }
            $updated = $query->update(['is_archived'=>$archive, 'last_edit_by'=>$user_id]);
        }
        $event = [
            'channel' => 'goals_'.$account_id,
            'event' => "goal_archived_unarchived",
            'data' => $goal,
            'is_archived' => $archive,
            'goal_id' => $goal->id,           
            'from_goals' => true
        ];
        HelperFunctions::broadcastEvent($event, "broadcast_event");
        return response()->json(['success'=>true, 'message'=> 'Goal successfully '.($archive ? 'archived':'unarchived'), 'archived'=>$archive, 'updated'=>$updated]);
    }

    public function getActionItemRF(Request $request)
    {
        $goal_id = $request->get('goal_id');
        $item_id = $request->get('item_id');
        $owner_id = $request->get('goal_type') != 3 ? $request->get('owner_id') : DB::raw('NULL');
        $feedback = GoalItemsFeedback::select('reflection', 'feedback')->where('goal_id', $goal_id)->where('goal_item_id', $item_id)->where('owner_id', "<=>",$owner_id)->first();
        return response()->json(['success'=>true, 'message'=> 'Action Item Feedback fetch successfully', 'data'=>$feedback]);
    }

    public function formatGoal(&$goal, $user_id, $goal_user_settings, $goal_settings)
    {
        $goal->percentage = 0;
        if ($goal->goal_type === 3 || ($goal->goal_type === 4 && count($goal->owners_updated) === 1)) {
            // Calculate completion percentage
            if ($goal->total_items && $goal->total_items > 0) {
                $share = round(100 / $goal->total_items);
                foreach ($goal->actionItems as &$item) {
                    if ($item->itemFeedbacks) {
                        $item->current_value = $item->itemFeedbacks->current_value;
                        $item->is_done = $item->itemFeedbacks->is_done;
                        $itemProgress = $this->checkItemProgress($item);
                        if ($item->itemFeedbacks->is_done == 1) {
                            $goal->percentage += (integer)($share);
                        } else {
                            if (is_numeric($itemProgress)) {
                                $goal->percentage += (((integer)$share) * $itemProgress / 100);
                            }
                        }
                    }
                    else
                    {
                        $item->percentage = 0;
                    }
                }
                $goal->percentage = floor($goal->percentage);
            } else $goal->percentage = '0';
        }
        $goal->goal_owners_cannot_uncheck_action_items = $goal_settings->goal_owners_cannot_uncheck_action_items;
        $goal->is_incomplete = !$goal->is_complete && $goal->is_published && date('Y-m-d', strtotime($goal->end_date)) < date('Y-m-d 00:00:00'); // compare only date, not time
        $goal->is_progress = !$goal->is_complete && $goal->percentage != '0' && $goal->is_published && date('Y-m-d', strtotime($goal->end_date)) > date('Y-m-d 00:00:00'); // goal is in progress if goal is not complete and due date should be greater than current data, compare only date, not time

        if ($goal->is_progress) {
            if (count($goal->actionItems)) {
                $goal->is_progress = false;
                foreach ($goal->actionItems as $item) {
                    if ($item->itemFeedbacks) {
                        if ($item->itemFeedbacks->current_value || $item->itemFeedbacks->reflection) {
                            $goal->is_progress = true;
                        }
                    }
                    if (count($item->submittedEvidence)) {
                        $goal->is_progress = true;
                    }
                }
            }
        }

        $this->checkGaolPermissions($goal, $user_id, $goal_settings, $goal_user_settings);
        //if all action items are completed on time a goal will be considered completed on time
        $goal->is_completed_on_time = 1;
        if(count($goal->actionItems))
        {
            foreach ($goal->actionItems as $item) {
                if (!$item->itemFeedbacks || !$item->itemFeedbacks->is_completed_on_time) {
                    $goal->is_completed_on_time = 0;
                }

            }
        }
    }

    public static function checkItemProgress(&$item)
    {
        if ($item->add_measurement == 1) {
            $percentage = 0;
            if ($item->start_value == $item->current_value) {
                $percentage = 0;
            } else if ($item->end_value > $item->start_value) {
                if ($item->current_value <= $item->start_value) {
                    $percentage = 0;
                } else if ($item->current_value >= $item->end_value) {
                    $percentage = 100;
                } else {
                    $percentage = floor(($item->current_value - $item->start_value) / ($item->end_value - $item->start_value) * 100);
                }

            } else if ($item->start_value > $item->end_value) {
                if ($item->current_value >= $item->start_value) {
                    $percentage = 0;
                } else if ($item->current_value <= $item->end_value) {
                    $percentage = 100;
                } else {
                    $percentage = floor(($item->start_value - $item->current_value) / ($item->start_value - $item->end_value) * 100);
                }
            }
            if($percentage)
            {
                $item->percentage = $percentage;
                return $percentage;
            }
            $item->percentage = 0;
            return true;
        } else {
            if($item->is_done)
            {
                $item->percentage = 100;
            }
            else
            {
                $item->percentage = 0;
            }
            return false;
        }
    }

    function checkGaolPermissions(&$goal, $user_id, $goal_settings,$goal_user_settings)
    {
        $goal->isCollaborator = collect($goal->collaborators)->where('user_id', $user_id)->first(); // user is collaborator
        $goal->isCollaboratorWithPermissionE = $goal->isCollaborator && strpos($goal->isCollaborator->permission, (string)2)  !== false; // user is collaborator with edit permission
        $goal->isCreator = $goal->created_by == $user_id;
        $goal->isOwner = collect($goal->owners_updated)->where('user_id', $user_id)->first();

        $goal->canEditDelete = ($goal->isCreator || $goal->isCollaboratorWithPermissionE || ($goal->goal_type == 3 && $goal->isOwner));

        $goal->addEvidence = false;
        $goal->addReview = false;
        $goal->addMeasurement = false;
        $goal->addReflection = false;
        $goal->addDueDateCheck = true;
        if($goal_settings->goal_owner_setting)
        {
            $goal->addDueDateCheck = false;
        }
        $goal->veiwer = false;
        if ($goal->isOwner) {
            $goal->addEvidence = true;
            $goal->addReview = false;
            $goal->addReflection = true;
        }
        if ($goal->isCollaborator && strpos($goal->isCollaborator->permission, (string)3) !== false) {
            $goal->addEvidence = true;
        }
        if ($goal->isCollaborator && strpos($goal->isCollaborator->permission, (string)4) !== false) {
            $goal->addReview = true;
            if($goal_settings->review_collaborators)
            {
                $goal->addReflection = true;
            }
        }

        if (($goal_user_settings->can_enter_data_for_measurement_where_goal_owners == 1 && $goal->isOwner)
            || ($goal_user_settings->can_enter_data_for_measurement_where_goal_reviewers == 1 &&
                $goal->isCollaborator && strpos($goal->isCollaborator->permission, (string)4) !== false))
        {
            if($goal_settings->enable_measurement)
            {
                $goal->addMeasurement = true;
            }
        }

        if ($goal->isCollaborator && !(strpos($goal->isCollaborator->permission, (string)4)  !== false) && !(strpos($goal->isCollaborator->permission, (string)3)  !== false) && !$goal->isOwner){
            $goal->veiwer = true;
        }
        if($goal->current_user_is_owner && $goal->owner_id != $user_id){
              $goal->addEvidence=false;
              $goal->addMeasurement=false;
              $goal->addReflection=false;
              $goal->addReview=false;
              $goal->canEditDelete=false;
              $goal->veiwer=true;
        }
        if(!$goal->current_user_is_owner && !$goal->isCollaborator && ($goal_settings->make_all_goals_public || $goal_settings->make_all_goals_public_super_owner || $goal_settings->make_all_goals_public_admin_super_owner
               || $goal_settings->group_type_goals_public_admin_super_owner ||  $goal_settings->group_type_goals_public_super_owner
                ||  $goal_settings->group_type_goals_public ||  $goal_settings->individual_type_goals_public_admin_super_owner
                ||  $goal_settings->individual_type_goals_public_super_owner ||  $goal_settings->individual_type_goals_public
                ||  $goal_settings->account_type_goals_public_admin_super_owner ||  $goal_settings->account_type_goals_public_super_owner
                ||  $goal_settings->account_type_goals_public)){
            $goal->veiwer=true;
        }
    }

    public function actionItemDone($request) {
        $goal_id = $request->get('goal_id');
        $item_id = $request->get('item_id');
        $user_id = $request->get('user_id');
        $reflection = $request->get('reflection');

        $goal = $this->getSingleGoal($goal_id);
        if(!$goal)
        {
            return ['status'=>false, 'message' =>"Goal not found!"];
        }
        $goalSettings = self::getPreferredAccountSettings($goal->account_id, $user_id);
        foreach ($goal->actionItems as $item)
        {
            if($item->id == $item_id)
            {
                $singleGoalData = $item;
                break;
            }
        }
        $account_id = $goal->account_id;
        $current_lang = $request->has("current_lang") ? $request->get("current_lang") : 'en';
        $translations = TranslationsManager::get_page_lang_based_content("global", $this->site_id, null, $current_lang);
        $translations = (object)$translations;

        $site_id = $request->get('site_id');
        $is_checked = $request->has('is_done') && $request->get('is_done') == 1 ? 1 : 0;
        $goals_name = HelperFunctions::getGoalsAltName($account_id, $site_id);
        $is_owner = $goal->isOwner;
        $is_viewer = $goal->viewer;
        $addReview = $goal->addReview;
        $addEvidence = $goal->addEvidence;

        if (!(date('Y-m-d',strtotime($singleGoalData->deadline)) >= date('Y-m-d')) && $is_owner && $goalSettings->goal_owner_setting) {
            if($is_checked)
            {
                if (count($singleGoalData->submittedEvidence) > 0 || ($singleGoalData->current_value > $singleGoalData->start_value) || $singleGoalData->reflection != null)
                {
                    if (is_null($singleGoalData->current_value) && $singleGoalData->add_measurement)
                    {

                        $is_checked = false;
                        $singleGoalData->is_done = 0;
                        $message = ($translations->vd_please_enter_valid_number);

                        return ["status"=>false, "message"=>$message, "is_checked"=>$is_checked, "show_confirmation"=>false];
                    }
                    if ($singleGoalData->current_value < $singleGoalData->start_value)
                    {
                        $is_checked = false;
                        $singleGoalData->is_done = 0;
                        $message = HelperFunctions::getGoalAltTranslation($translations->measurement_error_for_input, $account_id, $site_id, $goals_name);
                        return ["status"=>false, "message"=>$message, "is_checked"=>$is_checked, "show_confirmation"=>false];
                    }
                    $message = HelperFunctions::getGoalAltTranslation($translations->goal_action_item_done_check_confirmation, $account_id, $site_id, $goals_name);
                    return ["status"=>false, "message"=>$message, "is_checked"=>$is_checked, "show_confirmation"=>true];
                }
                else
                {
                    if($singleGoalData->add_measurement !=1 || !$goalSettings->enable_measurement)
                    {
                        $message = $translations->goal_work_error;
                    }
                    else
                    {
                        $message = HelperFunctions::getGoalAltTranslation($translations->goal_work_error_detail, $account_id, $site_id, $goals_name);
                    }
                    $is_checked = false;
                    return ["status"=>false, "message"=>$message, "is_checked"=>$is_checked, "show_confirmation"=>true];
                }
            }
            else
            {
                return ["status"=>true, "message"=>'', "is_checked"=>0, "show_confirmation"=>true, 'set_feedback_empty'=>1, "here"=>1];
            }
        }
        if(!(date('Y-m-d',strtotime($singleGoalData->deadline)) >= date('Y-m-d')) && $is_owner && !$goalSettings->goal_owner_setting)
        {
            $is_checked = !$is_checked;
            $singleGoalData->is_done = !$singleGoalData->is_done;
            return ["status"=>false, "message"=>'', "is_checked"=>$is_checked, "show_confirmation"=>false];
        }
        if ((!(date('Y-m-d',strtotime($singleGoalData->deadline)) >= date('Y-m-d')) || ($is_viewer)) && (!$goalSettings->review_collaborators && !$addReview)){
            $is_checked = !$is_checked;
            return ["status"=>false, "message"=>'', "is_checked"=>$is_checked, "show_confirmation"=>false];
        }
        if (!$is_owner && !$addReview && !$is_checked) {
            $is_checked = true;
            return ["status"=>false, "message"=>'', "is_checked"=>$is_checked, "show_confirmation"=>false];
        }
        if(($addReview && $is_checked && !$addEvidence) && (!$goalSettings->review_collaborators && !$addReview)){
            $is_checked = false;
            return ["status"=>false, "message"=>'', "is_checked"=>$is_checked, "show_confirmation"=>false];
        }
        if ($is_checked)
        {
            if(count($singleGoalData->submittedEvidence) > 0 || ($singleGoalData->current_value > $singleGoalData->start_value) || $reflection!=null)
            {
                if (is_null($singleGoalData->current_value))
                {
                    if($singleGoalData->add_measurement !=1){
                            return ["status"=>true, "message"=>'', "is_checked"=>1, "show_confirmation"=>false];
                    }
                    $is_checked = false;
                    $message = ($translations->vd_please_enter_valid_number);
                    return ["status"=>false, "message"=>$message, "is_checked"=>$is_checked, "show_confirmation"=>false];
                }
                if ($singleGoalData->current_value < $singleGoalData->start_value)
                {
                    $is_checked = false;
                    $message = HelperFunctions::getGoalAltTranslation($translations->measurement_error_for_input, $account_id, $site_id, $goals_name);
                    return ["status"=>false, "message"=>$message, "is_checked"=>$is_checked, "show_confirmation"=>false];
                }
                if ($goalSettings->goal_owners_cannot_uncheck_action_items) {
                    $message = HelperFunctions::getGoalAltTranslation($translations->goal_action_item_done_check_confirmation, $account_id, $site_id, $goals_name);
                    return ["status"=>false, "message"=>$message, "is_checked"=>$is_checked, "show_confirmation"=>true];
                }
                else
                {
                    $is_checked = true;
                    return ["status"=>true, "message"=>'', "is_checked"=>$is_checked, "show_confirmation"=>true, 'set_feedback_empty'=> ($singleGoalData->is_done != $is_checked ? 1 : 0), "here"=>2];
                }
            }
            else
            {
                if($singleGoalData->add_measurement !=1 || !$goalSettings->enable_measurement)
                {
                    $message = $translations->goal_work_error;
                }
                else
                {
                    $message = HelperFunctions::getGoalAltTranslation($translations->goal_work_error_detail, $account_id, $site_id, $goals_name);
                }
                $is_checked = false;
                return ["status"=>false, "message"=>$message, "is_checked"=>$is_checked, "show_confirmation"=>true];
            }
        }
        else
        {
            if ($goalSettings->goal_owners_cannot_uncheck_action_items && $is_owner && !$addReview)
            {
                $is_checked = true;
                return ["status"=>true, "message"=>'', "is_checked"=>$is_checked, "show_confirmation"=>false];
            }
            if ($is_owner)
            {
                $is_checked = false;
                return ["status"=>true, "message"=>'', "is_checked"=>$is_checked, "show_confirmation"=>true, 'set_feedback_empty'=>1, "here"=>3];
            }
            if($singleGoalData->is_done != 1){
            return ["status"=>true, "message"=>'', "is_checked"=>true, "show_confirmation"=>false];
            }
            $message = HelperFunctions::getGoalAltTranslation($translations->goal_action_item_done_uncheck_confirmation, $account_id, $site_id, $goals_name);
            return ["status"=>false, "message"=>$message, "is_checked"=>$is_checked, "show_confirmation"=>true];
        }
    }
}
