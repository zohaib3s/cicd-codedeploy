<?php

namespace App\Http\Controllers;

use Aacotroneo\Saml2\Events\Saml2LoginEvent;
use Aacotroneo\Saml2\Http\Controllers\Saml2Controller;
use Aacotroneo\Saml2\Saml2Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

use App\Models\Account;

class SSOController extends Saml2Controller
{
    /**
     * Generate local sp metadata.
     *
     * @param Saml2Auth $saml2Auth
     * @return \Illuminate\Http\Response
     */
    public function metadata(Saml2Auth $saml2Auth)
    {
        $metadata = $saml2Auth->getMetadata();
        Log::info(["SSO MetaData" => $metadata]);
        return response($metadata, 200, ['Content-Type' => 'text/xml']);
    }

    /**
     * Process an incoming saml2 assertion request.
     * Fires 'Saml2LoginEvent' event if a valid user is found.
     *
     * @param Saml2Auth $saml2Auth
     * @param $idpName
     * @return \Illuminate\Http\Response
     */
//    public function acs(Saml2Auth $saml2Auth, $idpName)
//    {
//        $errors = $saml2Auth->acs();
//        if (!empty($errors)) {
//            logger()->error('Saml2 error_detail', ['error' => $saml2Auth->getLastErrorReason()]);
//            session()->flash('saml2_error_detail', [$saml2Auth->getLastErrorReason()]);
//
//            logger()->error('Saml2 error', $errors);
//            session()->flash('saml2_error', $errors);
//            return redirect(config('saml2_settings.errorRoute'));
//        }
//        $user = $saml2Auth->getSaml2User();
//
////        event(new Saml2LoginEvent($idpName, $user, $saml2Auth));
//
//        $redirectUrl = $user->getIntendedUrl();
//
//        if ($redirectUrl !== null) {
//            return redirect($redirectUrl);
//        } else {
//
//            return redirect(config('saml2_settings.loginRoute'));
//        }
//    }

    public function assertionConsumerService (Saml2Auth $saml2Auth, Request $request, $account_id) {
        if (!$this->validate_sso_config($account_id)) {
            return response()->json(['error' => 'SSO Config Not Found for Account = '.$account_id]);
        }
        $account = Account::find($account_id);
        config([
            'mytestidp1_idp_settings.idp.singleSignOnService.url' => $account->sso_url,
            'mytestidp1_idp_settings.idp.x509cert' => $account->sso_certificate,
            'mytestidp1_idp_settings.idp.entityId' => $account->idp_entityId,
        ]);

        $saml2Auth = new Saml2Auth(Saml2Auth::loadOneLoginAuthFromIpdConfig('mytestidp1'));

        $errors = $saml2Auth->acs();
        if (!empty($errors)) {
            return response()->json(['error' => $saml2Auth->getLastErrorReason()]);
//            return redirect(config('saml2_settings.errorRoute'));
        }
        $user = $saml2Auth->getSaml2User();

        $userData = [
            'id' => $user->getUserId(),
            'attributes' => $user->getAttributes(),
            'assertion' => $user->getRawSamlAssertion()
        ];
//        event(new Saml2LoginEvent($idpName, $user, $saml2Auth));
        $email = $user->getUserId();
        $authentication_token = \App\Models\User::where('email',$email)->where('is_active',1)->where('type','Active')->value('authentication_token');
        $query_string = http_build_query(['email'=>$email, 'authentication_token'=>$authentication_token]);

        $redirectUrl = $request->get('RelayState');
        if (!empty($redirectUrl)) {
            $separator = stristr($redirectUrl, "?") ? "&" : "?";
        } else {
            $redirectUrl = config('saml2_settings.loginRoute');
            $separator = stristr($redirectUrl, "?") ? "&" : "?";
        }
        $redirect_url = $redirectUrl.$separator.$query_string;
        return redirect($redirect_url);
    }

    public function validate_sso_config($account_id){
        if (empty($account_id)) {
            return false;
        }
        $account = Account::find($account_id);
        if (empty($account)) {
            return false;
        }
        if (empty($account->sso_url) || empty($account->sso_certificate) || empty($account->idp_entityId)) {
            return false;
        }
        return true;
    }

    /**
     * Process an incoming saml2 logout request.
     * Fires 'Saml2LogoutEvent' event if its valid.
     * This means the user logged out of the SSO infrastructure, you 'should' log them out locally too.
     *
     * @param Saml2Auth $saml2Auth
     * @param $idpName
     * @return \Illuminate\Http\Response
     */
    public function sls(Saml2Auth $saml2Auth, $idpName)
    {
        $errors = $saml2Auth->sls($idpName, config('saml2_settings.retrieveParametersFromServer'));
        if (!empty($errors)) {
            logger()->error('Saml2 error', $errors);
            session()->flash('saml2_error', $errors);
            throw new \Exception("Could not log out");
        }

        return redirect(config('saml2_settings.logoutRoute')); //may be set a configurable default
    }

    /**
     * Initiate a logout request across all the SSO infrastructure.
     *
     * @param Saml2Auth $saml2Auth
     * @param Request $request
     */
    public function logout(Saml2Auth $saml2Auth, Request $request)
    {
        $returnTo = $request->query('returnTo');
        $sessionIndex = $request->query('sessionIndex');
        $nameId = $request->query('nameId');
        $saml2Auth->logout($returnTo, $nameId, $sessionIndex); //will actually end up in the sls endpoint
        //does not return
    }

    /**
     * Initiate a login request.
     *
     * @param Saml2Auth $saml2Auth
     */
    public function login(Saml2Auth $saml2Auth)
    {
        $saml2Auth->login(config('saml2_settings.loginRoute'));
    }
}