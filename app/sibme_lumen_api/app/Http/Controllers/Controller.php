<?php

namespace App\Http\Controllers;

use App\Console\Commands\CraeteAudioAwsForTranscribe;
use App\Models\Document;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use App\Services\TranslationsManager;

class Controller extends BaseController
{
    //
    private $sortByKey;
    
    public static function ConvertToArray($arrayOfObjects)
    {
        if(is_array($arrayOfObjects) || is_object($arrayOfObjects))
        {
            $temp = json_encode($arrayOfObjects);
        }
        else
        {
            $temp = $arrayOfObjects;
        }
        $arrayOfArrays = json_decode($temp, true);
        return $arrayOfArrays;
    }

    public static function dateToString($date) {
        $site_id = app("Illuminate\Http\Request")->header('site_id');
        $currentDate = date('Y-m-d H:i:s');
        $start_date = new \DateTime($date);

        $since_start = $start_date->diff(new \DateTime($currentDate));
        $week_total = $since_start->format('%a')/7;
        $week_total = floor($week_total);
        if ($since_start->y > 0) {
            $formatDate = "About " . $since_start->y . ($since_start->y > 1 ?" ".TranslationsManager::get_translation('gen_years_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_year_ago', 'Api/video_details',  $site_id));
        } elseif ($since_start->m > 0) {
            $formatDate = "About " . $since_start->m . ($since_start->m > 1 ?" ".TranslationsManager::get_translation('gen_months_ago', 'Api/video_details',  $site_id) :" ".TranslationsManager::get_translation('gen_month_ago', 'Api/video_details',  $site_id));
        } elseif ($week_total > 0) {
            $formatDate = $week_total . ($week_total > 1 ?" ".TranslationsManager::get_translation('gen_weeks_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_week_ago', 'Api/video_details',  $site_id));
        } elseif ($since_start->d > 0) {
            $formatDate = $since_start->d . ($since_start->d > 1 ?" ".TranslationsManager::get_translation('gen_days_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_day_ago', 'Api/video_details',  $site_id));
        } elseif ($since_start->h > 0) {
            $formatDate = $since_start->h . ($since_start->h > 1 ? " ".TranslationsManager::get_translation('gen_hours_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_hour_ago', 'Api/video_details',  $site_id));
        } elseif ($since_start->i > 0) {
            $formatDate = $since_start->i . ($since_start->i > 1 ? " ".TranslationsManager::get_translation('gen_minutes_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_minute_ago', 'Api/video_details',  $site_id));
        } elseif ($since_start->s > 0) {
            $formatDate =" ".TranslationsManager::get_translation('gen_less_then_minut_ago', 'Api/video_details',  $site_id) ;
        } else {
            $formatDate = " ".TranslationsManager::get_translation('gen_just_now', 'Api/video_details',  $site_id) ;
        }
        return $formatDate;
    }

    public static function dateToString2($date,$current_lang) {
        $site_id = app("Illuminate\Http\Request")->header('site_id');
        $currentDate = date('Y-m-d H:i:s');
        $start_date = new \DateTime($date);

        $since_start = $start_date->diff(new \DateTime($currentDate));
        $week_total = $since_start->format('%a')/7;
        $week_total = floor($week_total);
        if ($since_start->y > 0) {
            if($current_lang == 'es'){
                $formatDate = ($since_start->y > 1 ?" ".TranslationsManager::get_translation('gen_years_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_year_ago', 'Api/video_details',  $site_id));
                $formatDate = str_replace("xx",$since_start->y,$formatDate);
            }
            else{
                $formatDate = "About " . $since_start->y . ($since_start->y > 1 ?" ".TranslationsManager::get_translation('gen_years_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_year_ago', 'Api/video_details',  $site_id));
            }
        } elseif ($since_start->m > 0) {
            if($current_lang == 'es'){
                $formatDate = ($since_start->m > 1 ?" ".TranslationsManager::get_translation('gen_months_ago', 'Api/video_details',  $site_id) :" ".TranslationsManager::get_translation('gen_month_ago', 'Api/video_details',  $site_id));
                $formatDate = str_replace("xx",$since_start->m,$formatDate);
              }
            else{
                $formatDate = "About " . $since_start->m . ($since_start->m > 1 ?" ".TranslationsManager::get_translation('gen_months_ago', 'Api/video_details',  $site_id) :" ".TranslationsManager::get_translation('gen_month_ago', 'Api/video_details',  $site_id));
            }
         } elseif ($week_total > 0) {
            if($current_lang == 'es'){
                $formatDate =  ($week_total > 1 ?" ".TranslationsManager::get_translation('gen_weeks_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_week_ago', 'Api/video_details',  $site_id));
                $formatDate = str_replace("xx",$week_total,$formatDate);
              }
              else{
               $formatDate = $week_total . ($week_total > 1 ?" ".TranslationsManager::get_translation('gen_weeks_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_week_ago', 'Api/video_details',  $site_id));
              }
            } elseif ($since_start->d > 0) {
            if($current_lang == 'es'){
                $formatDate =  ($since_start->d > 1 ?" ".TranslationsManager::get_translation('gen_days_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_day_ago', 'Api/video_details',  $site_id));
                $formatDate = str_replace("xx",$since_start->d,$formatDate);
              }
            else{
               $formatDate = $since_start->d . ($since_start->d > 1 ?" ".TranslationsManager::get_translation('gen_days_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_day_ago', 'Api/video_details',  $site_id));
            }
            } elseif ($since_start->h > 0) {
            if($current_lang == 'es'){
                $formatDate = ($since_start->h > 1 ? " ".TranslationsManager::get_translation('gen_hours_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_hour_ago', 'Api/video_details',  $site_id));
                $formatDate = str_replace("xx",$since_start->h,$formatDate);
            }
            else{
                $formatDate = $since_start->h . ($since_start->h > 1 ? " ".TranslationsManager::get_translation('gen_hours_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_hour_ago', 'Api/video_details',  $site_id));
            }
            } elseif ($since_start->i > 0) {
                if($current_lang == 'es'){
                    $formatDate = ($since_start->i > 1 ? " ".TranslationsManager::get_translation('gen_minutes_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_minute_ago', 'Api/video_details',  $site_id));
                    $formatDate = str_replace("xx",$since_start->i,$formatDate);
                  }
                  else{
                  $formatDate = $since_start->i . ($since_start->i > 1 ? " ".TranslationsManager::get_translation('gen_minutes_ago', 'Api/video_details',  $site_id) : " ".TranslationsManager::get_translation('gen_minute_ago', 'Api/video_details',  $site_id));
                  }
                } elseif ($since_start->s > 0) {
            $formatDate =" ".TranslationsManager::get_translation('gen_less_then_minut_ago', 'Api/video_details',  $site_id) ;
        } else {
            $formatDate = " ".TranslationsManager::get_translation('gen_just_now', 'Api/video_details',  $site_id) ;
        }
        return $formatDate;
    }

    public static function GetRealAttachments($attachments)
    {
        $real_attachments = [];
        for($i =0; $i < count($attachments[0]["error"]); $i++)
        {
            $real_attachments[$i]["name"] = $attachments[0]["name"][$i];
            $real_attachments[$i]["type"] = $attachments[0]["type"][$i];
            $real_attachments[$i]["tmp_name"] = $attachments[0]["tmp_name"][$i];
            $real_attachments[$i]["size"] = $attachments[0]["size"][$i];
            $real_attachments[$i]["error"] = $attachments[0]["error"][$i];
        }
        return $real_attachments;
    }
    public static function getCurrentLangDate($FechaStamp,$location = '')
    {
        $currentLang = app("Illuminate\Http\Request")->header("current-lang");
        $ano = date('Y', $FechaStamp);
        $mes = date('n', $FechaStamp);
        $dia = date('d', $FechaStamp);
        $time = date('h:i A', $FechaStamp);
        $time_with_secs = date('h:i:s A', $FechaStamp);
        $time_with_secs_wo_am_pm = date('h:i:s', $FechaStamp);
        $month = date('m', $FechaStamp);
        if($location == 'rubrics')
        {
            $ano = date('Y', $FechaStamp);
            $mes = date('n', $FechaStamp);
            $dia = date('d', $FechaStamp);
            $time = date('H:i A', $FechaStamp);
            $time_with_secs = date('H:i:s A', $FechaStamp);
            $time_with_secs_wo_am_pm = date('H:i:s', $FechaStamp);
            $month = date('m', $FechaStamp);
        }
        /*$diasemana = date('w',$FechaStamp);
        $diassemanaN= array("Domingo","Lunes","Martes","Mi�rcoles",
        "Jueves","Viernes","S�bado");
        $mesesN=array(1=>"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio",
        "Agosto","Septiembre","Octubre","Noviembre","Diciembre");*/
        if ($currentLang == "es") {
            $mesesN = array(1 => "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul",
                "Ago", "Sep", "Oct", "Nov", "Dic");
        } else {
            $mesesN = array(1 => "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul",
                "Aug", "Sep", "Oct", "Nov", "Dec");
        }

        if ($location == 'dashboard') {
            return $mesesN[$mes] . ' ' . $dia;
        } elseif ($location == 'trackers') {
            return $dia . '-' . $mesesN[$mes];
        } elseif ($location == 'trackers_filter') {
            return $mesesN[$mes];
        } elseif ($location == 'archive_module') {
            return $mesesN[$mes] . ' ' . $dia . ', ' . $ano;
        } elseif ($location == 'discussion_time') {
            return $time;
        } elseif ($location == 'discussion_date') {
            return $mesesN[$mes] . ' ' . $dia . ', ' . $ano . ' ' . $time;
        } elseif ($location == 'archive_module_folders') {
            return $mesesN[$mes] . ' ' . $dia . ', ' . $ano . ' '.$time_with_secs;
        } elseif ($location == 'archive_module_folders_date') {
            return $ano . '-' . $month . '-' . $dia . ' '.$time_with_secs;
        } elseif ($location == 'rubrics') {
            return $mesesN[$mes] . ' ' . $dia . ', ' . $ano . ' - '.$time_with_secs_wo_am_pm;
        }
        

        //return $diassemanaN[$diasemana].", $dia de ". $mesesN[$mes] ." de $ano";
    }

    public function getPreferredLanguage()
    {

        $langs = array();
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            // break up string into pieces (languages and q factors)
            preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);
            if (count($lang_parse[1])) {
                // create a list like "en" => 0.8
                $langs = array_combine($lang_parse[1], $lang_parse[4]);
                // set default to 1 for any without q factor
                foreach ($langs as $lang => $val) {
                    if ($val === '') {
                        $langs[$lang] = 1;
                    }

                }
                // sort list based on value
                arsort($langs, SORT_NUMERIC);
            }
        }
        //extract most important (first)
        foreach ($langs as $lang => $val) {
            break;
        }
        //if complex language simplify it
        if (isset($lang) && stristr($lang, "-")) {
            $tmp = explode("-", $lang);
            $lang = $tmp[0];
        } else {
            $lang = 'en'; // default language
        }
        return ["lang" => $lang];
    }
    public function digitalKey($length)
    {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
    }

    public function sortMultiDimensionalArray($array, $sort_by)
    {
        if(empty($array))
        {
            return [];
        }
        $this->sortByKey = $sort_by;
        usort($array, [$this, 'sortMultiDimensionalCallback']);
        return $array;
    }

    private function sortMultiDimensionalCallback($a, $b) {
        return strcmp( strtolower($a[$this->sortByKey]), strtolower($b[$this->sortByKey]) );
    }

    public function manuallySubmitAudioForTranscription(Request $request)
    {
        $document = Document::where('id', $request->get('dociment_id'))->first();
        if(!$document)
        {
            return ['status'=>false, "message"=>'Document not found'];
        }
        if($document->doc_type != 1 || empty($document->url))
        {
            return ['status'=>false, "message"=>'Document is not video or have empty url'];
        }
        $helper = new CraeteAudioAwsForTranscribe();
        $audioAvailable = $helper->audioAvailable($document->id);
        if (!$audioAvailable) {
            $helper->createJobForAudio($document->url,$document->id);
            return ['status'=>true, "message"=>"Audio job created for transcription!"];
        }
        else
        {
            return ['status'=>false, "message"=>"Audio is already available for this document!"];
        }
    }
}
