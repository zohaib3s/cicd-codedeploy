<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ZencoderController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    function receive_notification(){
        $zencoder = new \App\Services\Zencoder();
        $response = $zencoder->processJobNotification();
        return response()->json($response);
    }

    function receive_notification_coconut(Request $request){
        $coconut = new \App\Services\Coconut();
        $response = $coconut->processJobNotification($request->all());
        return response()->json($response);
    }

}
