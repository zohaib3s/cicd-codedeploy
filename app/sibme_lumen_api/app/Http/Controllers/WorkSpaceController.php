<?php

namespace App\Http\Controllers;
use App\Api\Activities\GetActivities;
use App\Models\AccountFolderDocument;
use App\Models\AccountFolderGroup;
use App\Models\DocumentFolder;
use App\Models\UserActivityLog;
use Illuminate\Http\Request;
use App\Models\AccountFolderUser;
use App\Models\UserGroup;
use App\Models\AccountFolder;
use App\Models\Document;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\Comment;
use App\Models\EdtpaAssessmentPortfolioCandidateSubmissions;
use Illuminate\Support\Facades\DB;
use Datetime;

use App\Services\HelperFunctions;

class WorkSpaceController extends Controller {

    protected $httpreq;
    protected $site_id;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->base = config('s3.sibme_base_url');
        //
    }

    public function get_workspace_artifects(Request $request) {
        $input = $request->all();
        $user_id = @$input['user_id'];
        $account_id = @$input['account_id'];
        $title = @$input['title'];
        $sort = @$input['sort'];
        $page = @$input['page'];
        $doc_type = @$input['doc_type'];
        $from_goals = isset($input['from_goals']) ? $input['from_goals'] : 0;
        $artifact_moved_count = isset($input['artifact_moved_count']) ? $input['artifact_moved_count'] : 0;
        $document_id = isset($input['document_id']) && !empty($input['document_id']) ? $input['document_id'] : null;
        $parent_folder_id = isset($input['parent_folder_id']) && !empty($input['parent_folder_id']) ? $input['parent_folder_id'] : 0;
        $with_folders = isset($input['with_folders']) && !empty($input['with_folders']) ? $input['with_folders'] : 0;
        $apply_inner_search = isset($input['apply_inner_search']) ? $input['apply_inner_search'] : 1;//This is for assessment huddle folders and goal add evidence
        $apply_parent_check = 1;
        if(!empty($title) || empty($with_folders))
        {
            $apply_parent_check = 0;
        }
        $default_photo = 1;
        if (!empty($doc_type)) {
            if($from_goals && $doc_type == 2)
            {
                $doc_type = [2,5];//also send url artifacts with resource artifacts for goals add evidence!
            }
            else
            {
                $doc_type = [$doc_type];
            }


        } else {
            $doc_type = [1, 2, 3, 5];
        }
        
        $user_permissions = UserAccount::where(array('user_id' => $user_id , 'account_id' => $account_id))->first();
       
        /*
        $is_mobile_request = false;
        if (!empty($input['app_name']) && ($input['app_name'] == 'com.sibme.mobileapp' || $input['app_name'] == 'com.sibme.sibme')) {
            $is_mobile_request = true;
        } else {
            $is_mobile_request = false;
        }
        */
        $is_mobile_request = isset($input['app_name']) ? HelperFunctions::is_mobile_request($input['app_name']) : false;

        $workspace_data = [];
            if($is_mobile_request)
            {
              $limit = 20;  
            }
            else
            {
              $limit = 12;
            }
       $workspaceORhuddle = 3;      // SW-2495 Code Written for Mobile Apis when they are fetching data in workspace activtiy for Video Which are Shared to Huddles
       if(!empty($document_id)) 
       {
         $account_folder_details = AccountFolderDocument::where(array(
                        'document_id' => $document_id,
                        'site_id' => $this->site_id
                    ))->first();    
        if($account_folder_details)
        {
            $account_folder_details = $account_folder_details->toArray();
            $huddle_detail = AccountFolder::where(array(
                        'account_folder_id' => $account_folder_details['account_folder_id'],
                        'site_id' => $this->site_id
                    ))->first(); 
            if($huddle_detail)
            {
                $huddle_detail = $huddle_detail->toArray();
                $workspaceORhuddle = $huddle_detail['folder_type'];
            }
            else {
                $workspaceORhuddle = 3;
            }
            
        }
            else
            {
                $workspaceORhuddle = 3;
            }
       }
       
        else {
                $workspaceORhuddle = 3;
             }            // SW-2495 Code Ends here
       
        $workspace_data = Document::getWorkSpaceVideos($this->site_id, $account_id, $user_id, $title, $sort, $limit, $page, $doc_type, $document_id,$workspaceORhuddle, $parent_folder_id, $apply_parent_check, @$input['doc_type'], $artifact_moved_count, $apply_inner_search);

        $item = [];
        if (!empty($workspace_data['data'])) {
            foreach ($workspace_data['data'] as $row) {               
                $item[] = self::conversion_to_thmubs($row, 1, 1);
            }
        }
        if (count($item) > 0) {
            for ($j = 0; $j < count($item); $j++) {
                $is_assignments = EdtpaAssessmentPortfolioCandidateSubmissions::where('document_id',$item[$j]['doc_id'])->count();
                $item[$j]['total_comments'] = Comment::comments_count($item[$j]['doc_id'], $this->site_id);
                $item[$j]['total_attachment'] = Document::getDocumentsCount($item[$j]['doc_id'], $this->site_id);
                //$item[$j]['total_comments'] = Comment::comments_count($item[$j]['doc_id'], $this->site_id);
                //$item[$j]['total_attachment'] = Document::getDocumentsCount($item[$j]['doc_id'], $this->site_id);
                if($is_assignments  > 0){
                    $item[$j]['is_assignments'] =true; 
                }else{
                    $item[$j]['is_assignments'] =false; 
                }
                //$comments_res = app('App\Http\Controllers\HuddleController')->getVideoComments($item[$j]['doc_id'], '', '', '', '', 1);
            $comments_res = array();
            $comment_result = array();
            //Forcefully stooped comments array due to mobile devs request. Tahir //
            $is_mobile_request = false;
            if($is_mobile_request)
            {
                foreach ($comments_res as $row) {                    
                    if ($row['ref_type'] == 6) {
                        if (config('use_cloudfront') == true) {
                            $url = $row['comment'];
                            $videoFilePath = pathinfo($url);
                            $videoFileName = $videoFilePath['filename'];
                            if ($videoFilePath['dirname'] == ".") {
                                $video_path = app('App\Http\Controllers\HuddleController')->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) ."." .(isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a"));
                                $relative_video_path = $videoFileName ."." .(isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a");
                            } else {
                                $video_path = app('App\Http\Controllers\HuddleController')->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) ."." .(isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a"));
                                $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName ."." .(isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a");
                            }
                        } else {
                            $url = $row['comment'];
                            $videoFilePath = pathinfo($url);
                            $videoFileName = $videoFilePath['filename'];

                            if ((isset($videoFilePath['dirname']) && $videoFilePath['dirname'] == ".") || !isset($videoFilePath['dirname'])) {

                                $video_path = app('App\Http\Controllers\HuddleController')->getSecureAmazonUrl($videoFileName ."." .(isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a"));
                                $relative_video_path = $videoFileName ."." .(isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a");
                            } else {

                                $video_path = app('App\Http\Controllers\HuddleController')->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . $videoFileName ."." .(isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a"));
                                $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName ."." .(isset($videoFilePath['extension']) ? $videoFilePath['extension'] : "m4a");
                            }
                        }
                        $row['comment'] = $video_path;
                    }
                    $created_date = new DateTime($row['created_date']);
                    $last_edit_date = new DateTime($row['last_edit_date']);

                    $row['last_edit_date'] = $last_edit_date->format(DateTime::W3C);
                    $row['created_date'] = $created_date->format(DateTime::W3C);

                    if (isset($row['image']) && $row['image'] != '') {
                        $use_folder_imgs = config('use_local_file_store');
                        if ($use_folder_imgs == false) {
                            $file_name = "static/users/" . $row['id'] . "/" . $row['image'];
                            $row['image'] = config('amazon_base_url') . config('bucket_name_cdn') . "/" . $file_name;
                        }

                        //$item['User']['image'] = $sibme_base_url . '/img/users/' . $item['User']['image'];
                        //else
                        //$item['User']['image'] = $view->Custom->getSecureSibmecdnUrl("static/users/" . $item['User']['id'] . "/" . $item['User']['image']);
                    } else {
                        if ($default_photo == 1) {
                            $row['image'] = config('sibme_base_url') . '/img/home/photo-default.png';
                        }
                    }

                    $commentDate = $row['created_date'];
                    $currentDate = date('Y-m-d H:i:s');

                    $start_date = new DateTime($commentDate);
                    $since_start = $start_date->diff(new DateTime($currentDate));

                    $commentsDate = '';
                    if ($since_start->y > 0) {
                        $commentsDate = "About " . $since_start->y . ($since_start->y > 1 ? ' Years Ago' : ' Year Ago');
                    } elseif ($since_start->m > 0) {
                        $commentsDate = "About " . $since_start->m . ($since_start->m > 1 ? ' Months Ago' : ' Month Ago');
                    } elseif ($since_start->d > 0) {
                        $commentsDate = $since_start->d . ($since_start->d > 1 ? ' Days Ago' : ' Day Ago');
                    } elseif ($since_start->h > 0) {
                        $commentsDate = $since_start->h . ($since_start->h > 1 ? ' Hours Ago' : ' Hour Ago');
                    } elseif ($since_start->i > 0) {
                        $commentsDate = $since_start->i . ($since_start->i > 1 ? ' Minutes Ago' : ' Minute Ago');
                    } elseif ($since_start->s > 0) {
                        $commentsDate = 'Less Than A Minute Ago';
                    } else {
                        $commentsDate = 'Just Now';
                    }

                    $row['Comment']['created_date_string'] = $commentsDate;

                    $response = app('App\Http\Controllers\HuddleController')->get_replies_from_comment($row,1);
                    $row['attachment_count'] = app('App\Http\Controllers\HuddleController')->match_timestamp_count_comment($row['id'], $item[$j]['doc_id']);
                    $row['attachment_names'] = app('App\Http\Controllers\HuddleController')->get_comment_attachment_file_names($row['id'], $item[$j]['doc_id']);
                    $row['Comment'] = $response;
                    $get_standard = app('App\Http\Controllers\HuddleController')->gettagsbycommentid($row['id'], array('0')); //get standards
                    $get_tags = app('App\Http\Controllers\HuddleController')->gettagsbycommentid($row['id'], array('1', '2')); //get tags
                    $row['standard'] = $get_standard;
                    $row['default_tags'] = $get_tags;

                    $comment_result[] = $row;
                }
                
            }
                //echo "<pre>";

                $item[$j]['comments'] = $comment_result;
                if($item[$j]['doc_type'] == 2){
                    $item[$j]['title'] = htmlspecialchars_decode(pathinfo($item[$j]['title'], PATHINFO_FILENAME));
                }else{
                    $item[$j]['title'] = htmlspecialchars_decode($item[$j]['title']);
                }
               
            }
        }
        
        $is_enabled_workspace_framework = HelperFunctions::is_enabled_workspace_framework($account_id,$this->site_id);
        $is_enabled_workspace_custom_marker = HelperFunctions::is_enabled_workspace_custom_marker($account_id,$this->site_id);
        $workspace['data'] = $item;
        if($document_id && isset($item[0]) && $item[0]['created_by'] != $user_id)//goal_evidence
        {
            $workspace['has_access_to_video'] = false;
        }
        $workspace['total_records'] = $workspace_data['total_records'];
        if($is_enabled_workspace_framework)
        {
            $is_enabled_workspace_framework = '1';
        }
        else {
            $is_enabled_workspace_framework = '0';
        }
        $workspace['is_enabled_workspace_framework'] = $is_enabled_workspace_framework;

        if($is_enabled_workspace_custom_marker)
        {
            $is_enabled_workspace_custom_marker = '1';
        }
        else {
            $is_enabled_workspace_custom_marker = '0';
        }
        $workspace['is_enabled_workspace_custom_marker'] = $is_enabled_workspace_custom_marker;
        if ($page == 1 || $page == 0) {
            $workspace['folders'] = DocumentFolder::getFolders(0, $account_id, $this->site_id, $user_id, $parent_folder_id, $sort, $title, $apply_parent_check);
        }
        $workspace['title_for_header'] = AccountFolder::get_title_for_header($parent_folder_id, 0);
        $workspace['success'] = true;
        
        if($user_permissions['role_id'] == 120 && $user_permissions['parmission_access_my_workspace'] == '0' )
        {
            $workspace['success'] = false;
        }
        $workspace['breadcumbs'] = DocumentFolder::getNLevelParents($parent_folder_id);
        $workspace_data['breadcumbs'] = DocumentFolder::getNLevelParents($parent_folder_id);

        if (!empty($workspace)) {
            return response()->json($workspace, 200, [], JSON_PRETTY_PRINT);
        }
        return response()->json($workspace_data, 200, [], JSON_PRETTY_PRINT);
    }

    /*
      public function get_workspace_videos_for_popup(Request $request){
      $input = $request->all();
      $user_id = $input['user_id'];
      $account_id = $input['account_id'];
      $workspace_data = Document::getWorkSpaceVideos($this->site_id, $account_id, $user_id, "", "", null, 1, [1]);

      $item = [];
      if (!empty($workspace_data['data'])) {
      foreach ($workspace_data['data'] as $row) {
      $item[] = self::conversion_to_thmubs($row,1,1);
      }
      }

      $workspace_data['data'] = $item;

      return response()->json($workspace_data, 200, [], JSON_PRETTY_PRINT);
      }
     */

    public static function conversion_to_thmubs($row, $use_doc_id = 0, $form_workspace = 0 , $flag = false) {
        $base = config('s3.sibme_base_url');
        $ext = pathinfo($row['original_file_name'], PATHINFO_EXTENSION);
        $row['file_type'] = $ext ? $ext : "mp4";
        $extensions = ['aiff','3gp', '3gpp', 'avi', 'divx', 'dv', 'flv', 'm4v', 'mjpeg', 'mkv', 'mod', 'mov' , 'mpeg', 'mpg', 'm2ts', 'mts', 'mxf', 'ogv','aif', 'mp3', 'm4a', 'ogg', 'wav', 'wma'];
                
                if(in_array(strtolower($ext), $extensions) && $flag)
                {
                   $row['published'] = 0; 
                }
        if ($row['doc_type'] == 1 || ($form_workspace && $row['doc_type'] == 3 && $row['scripted_current_duration'] == null && $row['is_processed'] >= 4)) {
            $data = app('App\Http\Controllers\VideoController')->get_document_url($row);          
            $row['thubnail_url'] = $data['thumbnail'];
            if(isset($data['thumbnail_type']))
             {
                 $row['thumbnail_type']   = $data['thumbnail_type'];
             }
             else
             {
                 $row['thumbnail_type']   = '';
             }
            if($row['doc_type'] == 3)
            {
                $row['scripted_current_duration_bool'] = 0;
            }
            $row['static_url'] = $data['url'];

            if (empty($row["video_duration"])) {
                $field = ($use_doc_id == 0 ) ? "id" : "doc_id";
                $result = app('App\Http\Controllers\HuddleController')->getVideoDuration($row[$field]);
                if ($result["status"]) {
                    $row['video_duration'] = round($result["data"]);
                    
                }
            }
            $row["video_duration"] = (string)$row["video_duration"];
        } elseif ($row['doc_type'] == 2) {

            if ($ext == 'xls' || $ext == 'xlsx') {
                $row['thubnail_url'] = $base . 'img/excel_gry_icon.svg';

            } elseif ($ext == 'pdf') {
                $row['thubnail_url'] = $base . 'img/pdf_gry_icon.svg';

            } elseif ($ext == "pptx" || $ext == "ppt") {
                $row['thubnail_url'] = $base . 'img/power_point.png';

            } elseif ($ext == "docx" || $ext == "doc") {
                $row['thubnail_url'] = $base . 'img/word_icon.png';

            } elseif ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                $row['thubnail_url'] = $base . 'img/image_icon.png';

            } else {
                $row['thubnail_url'] = $base . 'img/file_gry_icon.svg';

            }
            $row['thumbnail_type'] = 'default';
            if(isset($row['s3_thumbnail_url']) && !empty($row['s3_thumbnail_url']))
            {
                $row['thubnail_url'] = VideoController::getSecureAmazonSibmeUrl($row['s3_thumbnail_url']);
                $row['thumbnail_type'] = 'custom';
            }
        } elseif ($row['doc_type'] == 3) {
            $row['scripted_current_duration_bool'] = 1;
            $row['thubnail_url'] = $base . 'img/nots_gry_con.svg';
            // $row['file_type'] = $ext;
        }elseif ($row['doc_type'] == 5) {
            if(isset($row['s3_thumbnail_url']) && !empty($row['s3_thumbnail_url']))
            {
                $row['thubnail_url'] = VideoController::getSecureAmazonSibmeUrl($row['s3_thumbnail_url']);
                $row['thumbnail_type'] = 'custom';
            }else{
                $row['thubnail_url'] = 'assets/img/url-icon.svg';
                $row['thumbnail_type'] = 'default';
            }
        }else {
            $row['thubnail_url'] = $base . 'img/file_gry_icon.svg';
            //$row['file_type'] = $ext;
        }
        if (!isset($row["static_url"])) {
            $row["static_url"] = $row["stack_url"];
        }

        return $row;
    }

    function getUserActivities(Request $request) {
        $account_id = $request->get("account_id");
        $workspace = $request->get("workspace");
        $user_current_account = $request->get("user_current_account");
        $user_current_account = self::ConvertToArray($user_current_account);
        $site_id = app("Illuminate\Http\Request")->header("site_id");
        $account_id = (int) $account_id;
        /*
        $is_mobile_request = false;
        if (!empty($request->get('app_name')) && ($request->get('app_name') == 'com.sibme.mobileapp' || $request->get('app_name') == 'com.sibme.sibme')) {
            $is_mobile_request = true;
        } else {
            $is_mobile_request = false;
        }
        */
        $is_mobile_request = HelperFunctions::is_mobile_request($request->get('app_name'));

        $nowDate = date("Y-m-d", time());
        $nowDate = strtotime($nowDate);
        $nowDate = strtotime("-7 day", $nowDate);

        $nextDate = date('Y-m-d', $nowDate);

        $array = [1, 2, 3, 5, 6, 4, 8, 7, 17, 18, 19, 20, 21, 22, 24, 25, 27, 29, 30];

        $result = UserActivityLog::select('UserActivityLog.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image')
                ->join("users as User", "User.id", "UserActivityLog.user_id");

        if ($workspace) {
            $result->join("account_folders as AccountFolder", "AccountFolder.account_folder_id", "UserActivityLog.account_folder_id")
                    ->where("AccountFolder.folder_type", "=", 3);
        }

        $result = $result->where("UserActivityLog.date_added", ">=", $nextDate)
                ->where("UserActivityLog.account_id", $account_id)
                ->whereIn("UserActivityLog.type", $array)
                ->orderBy("UserActivityLog.date_added", "DESC")
                ->limit(100)
                ->get();
        if(count($result))
        {
            $result = $result->toArray();
        }
        if($is_mobile_request)
        {
            $final_result = GetActivities::get_ctivities($result, $user_current_account["User"]["id"], $site_id, $account_id, $user_current_account["roles"]["role_id"],$request->get('lang'));
        }
        else
        {
            $final_result = GetActivities::get_ctivities($result, $user_current_account["User"]["id"], $site_id, $account_id, $user_current_account["roles"]["role_id"]);
        }

        return self::convert_to_utf8_recursively($final_result);
    }

    function convert_to_utf8_recursively($dat){
        if( is_string($dat) ){
           return mb_convert_encoding($dat, 'UTF-8', 'UTF-8');
        }
        elseif( is_array($dat) ){
           $ret = [];
           foreach($dat as $i => $d){
             $ret[$i] = self::convert_to_utf8_recursively($d);
           }
           return $ret;
        }
        else{
           return $dat;
        }
  }

}
