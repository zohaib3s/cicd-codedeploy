<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Document;
use App\Models\AccountFolder;
use App\Models\AccountFolderMetaData;
use App\Models\AccountMetaData;
use App\Models\AccountTag;
use App\Models\AccountFolderUser;
use App\Models\AccountFrameworkSetting;
use App\Models\Group;
use App\Models\Sites;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\UserActivityLog;
use App\Models\UserGroup;
use App\Models\EmailUnsubscribers;

use App\Models\AccountFolderDocument;
use App\Models\AccountCommentTag;
use App\Models\DocumentStandardRating;
use App\Models\PerformanceLevelDescription;
use App\Models\AccountFrameworkSettingPerformanceLevel;

use App\Services\HelperFunctions;
use App\Services\SendGridEmailManager;
use App\Services\TranslationsManager;
use App\Services\UsersHelperFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use \DrewM\MailChimp\MailChimp;
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;

class RubricsController extends Controller
{
    protected $httpreq;
    protected $site_id;
    protected $lang;
    public $mailchimp;
    public $base;
    public $language_based_messages;
    public $group_based_translation;
    public $subject_title;
    public $duplicated_emails;
    public $linked_emails;
    public $new_emails_added;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->lang = $this->httpreq->header('current-lang');
        $this->base = config('s3.sibme_base_url');
        $this->mailchimp = new MailChimp('007cb274d399d9290e1e6c5b42118d40-us3');
        $this->language_based_messages = TranslationsManager::get_page_lang_based_content('flash_messages', $this->site_id, '', $this->lang);
        $this->group_based_translation = TranslationsManager::get_page_lang_based_content('users/administrators_groups', $this->site_id, '', $this->lang);
        $this->subject_title = Sites::get_site_settings('email_subject', $this->site_id);
        $this->module_url = 'Api/huddle_list';
    }
    
    function get_rubrics_list(Request $request)
    {
        $input = $request->all();
        $account_id = $input['account_id'];
        $page = (int)$input['page'];
        $page = $page - 1;
        $limit = 12;
        $offset = $page*$limit;
        $account_details = Account::where(array('id' => $account_id ))->first();
        $frameworks = AccountTag::select('AccountTag.tag_title', 'afs.*' ,'a.company_name')
                      ->join('accounts as a','a.id','=','AccountTag.account_id')
                      ->join('account_framework_settings as afs','afs.account_tag_id','=','AccountTag.account_tag_id')
                      ->where(array("tag_type" => '2',"AccountTag.account_id" => $account_id , 'afs.standards_added' => '1'))
                      ->orWhere(function($query) use ($account_details) {$query->where(array("tag_type" => '2',"AccountTag.account_id" => $account_details->parent_account_id , 'afs.standards_added' => '1' , 'afs.parent_child_share' => '1' )); })
                      ->offset($offset)
                      ->limit($limit)
                      ->orderBy('AccountTag.account_tag_id','DESC')
                      ->groupBy('afs.account_tag_id')
                      ->get();
        $new_frameworks_array = array();
        foreach ($frameworks as $framework)
        {
            if($framework->account_id != $account_id)
            {
                $framework->is_from_parent = true;
            }
            else
            {
                $framework->is_from_parent = false;
            }
            if(!empty($framework->published_at))
            {
                $framework->published_at = self::getCurrentLangDate(strtotime($framework->published_at), "rubrics");
            }
            
            $framework->updated_at = self::getCurrentLangDate(strtotime($framework->updated_at), "rubrics");
            $new_frameworks_array[] = $framework;
        }
        $frameworks = $new_frameworks_array;
        $total_frameworks =  $this->rubrics_count($account_id);
        return response()->json(['success' => true , 'framework_list' => $frameworks, 'frameworks_count'=>$total_frameworks ]);
        
    }
    
    public function rubrics_count($account_id){
        $account_details = Account::where(array('id' => $account_id ))->first();
        $framework_count = AccountTag::join('accounts as a','a.id','=','AccountTag.account_id')
                      ->join('account_framework_settings as afs','afs.account_tag_id','=','AccountTag.account_tag_id')
                      ->where(array("tag_type" => '2',"AccountTag.account_id" => $account_id , 'afs.standards_added' => '1'))
                      ->orWhere(function($query) use ($account_details) {$query->where(array("tag_type" => '2',"AccountTag.account_id" => $account_details->parent_account_id , 'afs.standards_added' => '1' , 'afs.parent_child_share' => '1' )); })
                      ->distinct()
                      ->count('AccountTag.account_tag_id');
       return $framework_count;
    }
    
    function save_frame_work_first_level(Request $request) {
        $input = $request->all();
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        if(isset($input['framework_id']))
        {
            $framework_id = $input['framework_id'];
        }
        else
        {
            $framework_id = '';
        }
        
        if (empty($framework_id)) {

            $save_tags = array(
                "account_id" => $account_id,
                "tag_type" => '2',
                "tag_title" => $input['rubric_name'],
                "created_by" => $user_id,
                "created_date" => date('Y-m-d H:i:s'),
                "last_edit_by" => $user_id,
                "last_edit_date" => date('Y-m-d H:i:s'),
                'site_id' => $this->site_id
            );
            $account_tag_id = DB::table('account_tags')->insertGetId($save_tags);

            $framework_settings = array(
                "account_id" => $account_id,
                "account_tag_id" => $account_tag_id,
                "updated_at" => date('Y-m-d H:i:s'),
                "framework_name" => $input['rubric_name'],
                "enable_unique_desc" => '0',
                "enable_ascending_order" => '0',
                "enable_performance_level" => '0',
                "published" => '0',
                "tier_level" => $input['tier_level'],
                "checkbox_level" => $input['checkbox_level'],
                "framework_sample" => htmlentities($input['framework_sample']),
                "parent_child_share" => $input['parent_sharing'],
                'site_id' => $this->site_id
            );
            DB::table('account_framework_settings')->insertGetId($framework_settings);
            
            $get_framework_settings = AccountFrameworkSetting::where(array('account_tag_id' => $account_tag_id))->first();

            $data = array(
                'framework_id' => $account_tag_id
            );
            return response()->json(['success' => true , 'message' => 'Framework Settings Successfully Saved.' , 'framework_settings' => $get_framework_settings ]);
        } else {

            $save_tags = array(
                "account_id" => $account_id ,
                "tag_type" => '2',
                "tag_title" => $input['rubric_name'],
                "last_edit_by" => $user_id ,
                "last_edit_date" =>  date('Y-m-d H:i:s') 
            );
            $get_framework = AccountFrameworkSetting::where(array(
                    'account_tag_id' => $framework_id))->first();

            AccountTag::where(['account_tag_id' => $framework_id])->update($save_tags);
            if ($get_framework && $get_framework->published != 1) {
               // AccountTag::where(['framework_id' => $framework_id])->whereRaw("parent_account_tag_id IS NOT NULL")->update(array('standard_level' => $input['checkbox_level']));
              $account_tags = AccountTag::where(['framework_id' => $framework_id , 'tag_type' => '0'])->get();
              foreach ($account_tags as $act)
              {
                    if($act['standard_level'] > $input['tier_level'])
                    {
                        AccountTag::where(['account_tag_id' => $act['account_tag_id']])->update(['standard_level' =>$input['tier_level']]);
                    }
              }
            }
            $account_tag_id = $framework_id;
            $framework_settings = array(
                "account_id" => $account_id,
                "updated_at" => date('Y-m-d H:i:s') ,
                "framework_name" => $input['rubric_name'] ,
                "tier_level" => $input['tier_level'],
                "checkbox_level" => $input['checkbox_level'],
                "framework_sample" => htmlentities($input['framework_sample']),
                "parent_child_share" => $input['parent_sharing'] 
            );
            AccountFrameworkSetting::where(array('account_tag_id' => $framework_id))->update($framework_settings);

            $data = array(
                'framework_id' => $framework_id
            );
            $get_framework_settings = AccountFrameworkSetting::where(array('account_tag_id' => $framework_id))->first();
            return response()->json(['success' => true , 'message' => 'Framework Settings Successfully Updated.' , 'framework_settings' => $get_framework_settings ]);
        }
       
    }
    
    function publish_framework(Request $request) {
        
        $input = $request->all();
        $framework_id = $input['framework_id'];
        $data = array(
            'published_at' => date('Y-m-d H:i:s') ,
            'published' => '1',
         //   'updated_at' => date('Y-m-d H:i:s') 
        );

        AccountFrameworkSetting::where(array('account_tag_id' => $framework_id))->update($data);
        $updated_framework = AccountFrameworkSetting::where(array('account_tag_id' => $framework_id))->first();
        return response()->json(['success' => true , 'message' => 'Framework is Successfully Published.' , 'updated_framework' => $updated_framework ]);

    }
    
    function unpublish_framework(Request $request) {
        $input = $request->all();
        $framework_id = $input['framework_id'];
        $account_id = $input['account_id'];

        $data = array(
            'published_at' => NULL,
            'published' => '0',
         //   'updated_at' => date('Y-m-d H:i:s')
        );

        AccountFrameworkSetting::where(array('account_tag_id' => $framework_id))->update($data);

        $data = array(
            'video_framework_id' => NULL
            );

        AccountFolderDocument::where(array('video_framework_id' => $framework_id))->update($data);
        
        $data = array(
            'meta_data_value' => NULL
            );
        
        //DB::table('account_meta_data')->where(array('meta_data_name' => 'default_framework' , 'account_id' => $account_id))->update($data);
       // DB::table('account_folders_meta_data')->where(array('meta_data_value' => $framework_id , 'meta_data_name' => 'framework_id' ))->update($data);
        
        $account_tags_type = AccountTag::select('AccountTag.*')->where(array('AccountTag.tag_type' => 0,'AccountTag.framework_id' => $framework_id))->get()->toArray();

        foreach ($account_tags_type as $row) {

            DB::table('account_comment_tags')->where(array('account_tag_id' => $row['account_tag_id']))->delete();
            DB::table('document_standard_ratings')->where(array('standard_id' => $row['account_tag_id']))->delete();
            //DB::table('performance_level_descriptions')->where(array('account_tag_id' => $row['account_tag_id']))->delete();
        }
        
         $updated_framework = AccountFrameworkSetting::where(array('account_tag_id' => $framework_id))->first();
         return response()->json(['success' => true , 'message' => 'Framework is Successfully Unpublished.' , 'updated_framework' => $updated_framework ]);

    }
    
    function delete_framework(Request $request) {
        
        $input = $request->all();
        $framework_id = $input['framework_id'];
        
        $framework_is_present_in_account = AccountMetaData::where(array(
                "meta_data_value" => $framework_id,
                "meta_data_name" => 'default_framework'
        ))->first();

        if ($framework_is_present_in_account) {
            return response()->json(['success' => false , 'message' => TranslationsManager::get_translation("framework_cannot_be_deleted_because_associated_msg")]);
        }
        
        $framework_is_present_in_huddle = AccountFolderMetaData::join('account_folders as af','AccountFolderMetaData.account_folder_id','=','af.account_folder_id')
                              ->where(array("meta_data_value" => $framework_id,"af.active" => '1',"meta_data_name" => 'framework_id'))->first();


        if ($framework_is_present_in_huddle) {
           return response()->json(['success' => false , 'message' => TranslationsManager::get_translation("framework_cannot_be_deleted_because_associated_msg")]);
        }
        $framework_is_present_in_video = AccountFolderDocument::where(array(
                "video_framework_id" => $framework_id
        ))->first();

        if ($framework_is_present_in_video) {
            return response()->json(['success' => false , 'message' => TranslationsManager::get_translation("framework_cannot_be_deleted_because_associated_msg")]);
        }





        $framework_settings_details = AccountFrameworkSetting::where(array(
                "account_tag_id" => $framework_id,
        ))->first();
        
        if($framework_settings_details)
        {
            DB::table('account_framework_setting_performance_levels')->where(array('account_framework_setting_id' => $framework_settings_details->account_framework_setting_id))->delete();
        }
        $account_tag_details = AccountTag::where(array(
                "framework_id" => $framework_id,
        ))->get()->toArray();


        foreach ($account_tag_details as $atd) {
            DB::table('performance_level_descriptions')->where(array('account_tag_id' => $atd['account_tag_id']))->delete();
        }
        
        DB::table('account_framework_settings')->where(array('account_tag_id' => $framework_id))->delete();
        DB::table('account_tags')->where(array('framework_id' => $framework_id))->delete();
        DB::table('account_tags')->where(array('account_tag_id' => $framework_id))->delete();
        return response()->json(['success' => true , 'message' => TranslationsManager::get_translation("you_have_sucessfully_deleted_the_framework_msg")]);
    }
    
    function create_performance_levels(Request $request) {
        $input = $request->all();
        $account_id = $input['account_id'];
        $framework_id = $input['framework_id'];

        $framework_settings_details = AccountFrameworkSetting::where(array(
                "account_tag_id" => $framework_id))->first();

        $framework_settings_id = $framework_settings_details->id;

        $performace_levels = $input['performance_levels'];
        $performace_levels = json_decode(json_encode($input['performance_levels']), True);
        if(isset($input['delete_pls']))
        {
            $delete_pls = json_decode(json_encode($input['delete_pls']), True);
            foreach ($delete_pls as $dl)
            {
                DB::table('account_framework_setting_performance_levels')->where(array('id' => $dl ))->delete();
                DB::table('performance_level_descriptions')->where(array('performance_level_id' => $dl ))->delete();
            }
        }

        foreach ($performace_levels as $pl) {

//            $account_framework_performance_level = AccountFrameworkSettingPerformanceLevel::where(array(
//                    "performance_level_rating" => $pl['rating_value'],
//                    "account_framework_setting_id" => $framework_settings_id
//            ))->first();

            if (isset($pl['id'])) {
                $performace_level_data = array(
                    'account_id' => $account_id,
                    'account_framework_setting_id' => $framework_settings_id,
                    'performance_level' => $pl['name'],
                    'performance_level_rating' =>  $pl['rating_value']
                );
                if ($input['unique_description'] == '0') {
                    $performace_level_data['description'] = $pl['description'];
                } else {
                   // $performace_level_data['description'] = NULL;
                }
                DB::table('account_framework_setting_performance_levels')->where(array('id' => $pl['id'], "account_framework_setting_id" => $framework_settings_id))->update($performace_level_data);
            } else {
                $performace_level_data = array(
                    'account_id' => $account_id,
                    'account_framework_setting_id' => $framework_settings_id,
                    'performance_level' => $pl['name'],
                    'performance_level_rating' => $pl['rating_value'],
                    'site_id' => $this->site_id
                );
                if ($input['unique_description'] == '0') {
                    $performace_level_data['description'] = $pl['description'];
                } else {
                    $performace_level_data['description'] = NULL;
                }
                DB::table('account_framework_setting_performance_levels')->insert($performace_level_data);
            }
        }
            $data = array(
                'enable_unique_desc' => $input['unique_description'] ,
                'enable_performance_level' => '1',
                'updated_at' => date('Y-m-d H:i:s')
            );
            
            if(isset($input['do_publish']) && $input['do_publish'] )
            {
                $data['published_at'] = date('Y-m-d H:i:s');
                $data['published'] = '1';
            }

            DB::table('account_framework_settings')->where(array('account_tag_id' => $framework_id))->update($data);
            return response()->json(['success' => true , 'message' => 'Performance levels are saved successfully.']);
    }
    
    function save_rubric_standards(Request $request)
    {
        $input = $request->all();
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $framework_id = $input['framework_id'];
        $data = $input['standards'];
        $data = json_decode(json_encode($input['standards']), True);
        if(isset($input['delete_standards']) && !empty($input['delete_standards']))
        {
            $delete_standards = json_decode(json_encode($input['delete_standards']), True);
            DB::table('account_tags')->where(array('framework_id' => $framework_id))->whereIn('account_tag_id',$delete_standards)->delete();
        }
        foreach ($data as $row) {
                $standard_level = array();
                $result = array();
                $result2 = array();
                $framework_hierarchy = array();


                $parent_account_tag_id = '';


                $standards_data = array(
                    'account_id' => $account_id,
                    'parent_account_tag_id' => '',
                    'tag_type' => 0,
                    'tag_code' => $row['prefix'],
                    'created_by' => $user_id,
                    'created_date' => date('Y-m-d H:i:s'),
                    'last_edit_by' => $user_id,
                    'last_edit_date' => date('Y-m-d H:i:s'),
                    'tads_code' => NULL,
                    'framework_id' => $framework_id,
                    'standard_level' => $row['standard_level'],
                    'standard_position' => $row['standard_position'],
                    'site_id' => $this->site_id
                );

                if (isset($row['account_tag_id'])) {
                    unset($standards_data['parent_account_tag_id']);
                    unset($standards_data['created_date']);
                    $standards_data['tag_code'] = $row['prefix'];
                    $standards_data['tag_title'] = $row['standard_text'];
                    $standards_data['tag_html'] = $row['standard_text'];
                    $standards_data['last_edit_date'] =  date('Y-m-d H:i:s');

                    if ($row['standard_analytics_label'] != '') {
                        $standards_data['standard_analytics_label'] = $row['standard_analytics_label'];
                    }


                    AccountTag::where(array('account_tag_id' => $row['account_tag_id']))->update($standards_data);
        
                } else {

                    if ($row['standard_analytics_label'] != '') {
                        $standards_data['standard_analytics_label'] = $row['standard_analytics_label'];
                    }
                    
                    if(isset($row['source_account_tag_id']))
                    {
                        $standards_data['source_standard_id'] = $row['source_account_tag_id'];
                    }
                    
                    $standards_data['tag_title'] = $row['standard_text'];
                    $standards_data['tag_html'] = $row['standard_text'];
                    DB::table('account_tags')->insertGetId($standards_data);
                }
        }
        $data = array();
        $data['standards_added'] = '1';
        DB::table('account_framework_settings')->where(array('account_tag_id' => $framework_id))->update($data);
        
        if(isset($input['edit_mode']) && $input['edit_mode'] )
        {
            if($input['do_publish'])
            {
                $data = array();
                $data['published_at'] = date('Y-m-d H:i:s');
                $data['published'] = '1';
                $data['updated_at'] = date('Y-m-d H:i:s');
                DB::table('account_framework_settings')->where(array('account_tag_id' => $framework_id))->update($data);
            }
        }
        
        return response()->json(['success' => true , 'message' => 'Standards are saved successfully.']);
    }
    
    function get_framework_standards_with_pl_levels(Request $request)
    {
        $input = $request->all();
        $framework_id = $input['framework_id'];
        $get_framework = AccountFrameworkSetting::where(array('account_tag_id' => $framework_id))->first();
        if(isset($input['user_id']) && isset($input['account_id']))
        {
           $users_accounts = UserAccount::where(array('user_id' => $input['user_id'] , 'account_id' => $input['account_id']))->first();
           $account_details = Account::where(array('id' => $input['account_id']))->first();
            if($account_details)
            {
                $parent_account_id = $account_details->parent_account_id;
            }
            else
            {
                $parent_account_id = '0';
            }
            if(!$get_framework || !$users_accounts || ($input['account_id'] != $get_framework->account_id) || ($users_accounts->role_id != 100 && $users_accounts->role_id != 110 ) )
            {
               if($get_framework && $parent_account_id == $get_framework->account_id && $get_framework->parent_child_share == '1' && isset($input['is_duplicate']) && $input['is_duplicate'] )
               {

               }
              else 
               {
                  return response()->json(['success' => false ,'message' => 'This framework is not accessible or doesnt exist.']);
               }
                
            }
        }
        $standards =  AccountTag::where(array('framework_id' => $framework_id , 'tag_type' => '0'))->orderBy('standard_position')->get();
        $new_standards = array();
        foreach ($standards as $key => $standard)
        {
            $new_standards[$key] =  $standard;
            $new_standards[$key]['prefix'] = $standard['tag_code'];
            if(!empty($standard['tag_title']))
            {
               $new_standards[$key]['standard_text'] = $standard['tag_title'];  
            }
            else if(!empty($standard['tag_html']))
            {
               $new_standards[$key]['standard_text'] = $standard['tag_html']; 
            }
            else
            {
               $new_standards[$key]['standard_text'] = $standard['standard_analytics_label'];
            }
        }
        $standards = $new_standards;
        $performance_levels = AccountFrameworkSettingPerformanceLevel::where(array('account_framework_setting_id' => $get_framework['id']))->orderBy('performance_level_rating','DESC')->get();
        $final_array = array(
            'standards' => $standards ,
            'performance_levels' => $performance_levels,
            'framework_settings' => $get_framework
        );
        return response()->json(['success' => true ,'data' => $final_array ]);
    }
    
    function save_unique_description(Request $request)
    {
        $input = $request->all();
        $data = $input['standards'];
        $data = json_decode(json_encode($input['standards']), True);
        foreach ($data as $row) {
        if (!empty($row['performance_levels'])) {
            $row['performance_levels'] = json_decode(json_encode($row['performance_levels']), True);
            foreach($row['performance_levels'] as $pl) {

                $performance_level_desc_data = array(
                    'description' => $pl['pl_desc'] 
                );
                
                if($pl['pl_id'] == -1)
                {
                  $get_pl_id = AccountFrameworkSettingPerformanceLevel::where(array('account_framework_setting_id' => $input['account_framework_setting_id'] , 'performance_level_rating' => $pl['rating_value']))->first();
                  if($get_pl_id)
                  {
                     $pl['pl_id'] = $get_pl_id->id;
                  }
                }
                
                $PerformanceLevelDescription = PerformanceLevelDescription::where(array('performance_level_id' => $pl['pl_id'] , 'account_tag_id' => $row['account_tag_id']))->first();

                if ($PerformanceLevelDescription) {
                    DB::table('performance_level_descriptions')->where(array('performance_level_id' => $pl['pl_id'], 'account_tag_id' => $row['account_tag_id']))->update($performance_level_desc_data);
                } else {
                    $performance_level_desc_data = array(
                        'account_tag_id' => $row['account_tag_id'],
                        'performance_level_id' => $pl['pl_id'],
                        'description' => $pl['pl_desc'],
                        'site_id' => $this->site_id
                    );
                    DB::table('performance_level_descriptions')->insert($performance_level_desc_data);
                }
            }
        }
      }
            if(isset($input['do_publish']) && $input['do_publish'] )
            {
                $data = array();
                $data['published_at'] = date('Y-m-d H:i:s');
                $data['published'] = '1';
                $data['updated_at'] = date('Y-m-d H:i:s');
                DB::table('account_framework_settings')->where(array('account_tag_id' => $input['framework_id']))->update($data);
            }
            if(isset($input['account_framework_setting_id']))
            {
               $data = array();
               $data['updated_at'] = date('Y-m-d H:i:s');
               DB::table('account_framework_settings')->where(array('id' => $input['account_framework_setting_id']))->update($data); 
            }
        return response()->json(['success' => true , 'message' => 'Unique Descriptions are Saved']);
    }
    
    function get_framework_settings(Request $request)
    {
        $input = $request->all();
        $framework_id = $input['framework_id'];
        $get_framework_settings = AccountFrameworkSetting::where(array('account_tag_id' => $framework_id))->first();
        if(isset($input['user_id']) && isset($input['account_id']))
        {
           $users_accounts = UserAccount::where(array('user_id' => $input['user_id'] , 'account_id' => $input['account_id']))->first();
           $account_details = Account::where(array('id' => $input['account_id']))->first();
            if($account_details)
            {
                $parent_account_id = $account_details->parent_account_id;
            }
            else
            {
                $parent_account_id = '0';
            }
            if(!$get_framework_settings || !$users_accounts || ($input['account_id'] != $get_framework_settings->account_id) || ($users_accounts->role_id != 100 && $users_accounts->role_id != 110 ) )
            {
               if($get_framework_settings && $parent_account_id == $get_framework_settings->account_id && $get_framework_settings->parent_child_share == '1' && isset($input['is_duplicate']) && $input['is_duplicate'] )
               {

               }
               else
               {
                    return response()->json(['success' => false ,'message' => 'This framework is not accessible or doesnt exist.']);
               }
            }
        }
        return response()->json(['success' => true , 'framework_settings' => $get_framework_settings]);
    }
    
    function get_performance_levels_for_framework(Request $request)
    {
        $input = $request->all();
        $framework_id = $input['framework_id'];
        $get_framework = AccountFrameworkSetting::where(array('account_tag_id' => $framework_id))->first();
        $performance_levels = AccountFrameworkSettingPerformanceLevel::where(array('account_framework_setting_id' => $get_framework['id']))->orderBy('performance_level_rating', 'DESC')->get();
        $new_pl_array = array();
        foreach ($performance_levels as $key => $pl)
        {
            $new_pl_array[$key] = $pl;
            $new_pl_array[$key]['name'] = $pl['performance_level'];
            $new_pl_array[$key]['rating_value'] = $pl['performance_level_rating'];
        }
        $performance_levels = $new_pl_array;
        return response()->json(['success' => true , 'performance_levels' => $performance_levels , 'framework_settings' => $get_framework]);
    }
    
    function get_standards_with_unique_performance_levels(Request $request)
    {
        $input = $request->all();
        $framework_id = $input['framework_id'];
        $get_framework = AccountFrameworkSetting::where(array('account_tag_id' => $framework_id))->first();
        if(isset($input['user_id']) && isset($input['account_id']))
        {
           $users_accounts = UserAccount::where(array('user_id' => $input['user_id'] , 'account_id' => $input['account_id']))->first();
            if(!$get_framework || !$users_accounts || ($input['account_id'] != $get_framework->account_id) || ($users_accounts->role_id != 100 && $users_accounts->role_id != 110 ) )
            {
               return response()->json(['success' => false ,'message' => 'This framework is not accessible or doesnt exist.']); 
            }
        }
        $standards =  AccountTag::where(array('framework_id' => $framework_id , 'tag_type' => '0'))->orderBy('standard_position')->get();
        $performance_levels = AccountFrameworkSettingPerformanceLevel::where(array('account_framework_setting_id' => $get_framework['id']))->orderby('performance_level_rating','DESC')->get();
        $pl_ids = array();
        foreach ($performance_levels as $pl)
        {
            $pl_ids[] = $pl['id'];
        }
        $new_standards = array();
        foreach ($standards as $key => $standard)
        {
            $new_standards[$key] = $standard;
            //$unique_descriptions = DB::table('performance_level_descriptions')->where(array('account_tag_id' => $standard['account_tag_id']))->get();
            $unique_descriptions_with_pls = array();
            foreach($performance_levels as $pl)
            {
                
                $ud = DB::table('performance_level_descriptions')->where(array('account_tag_id' => $standard['account_tag_id'] , 'performance_level_id' => $pl->id ))->first();
                if($ud)
                {
                    
                    $ud->performance_level = $pl->performance_level;
                    $ud->performance_level_rating = $pl->performance_level_rating;
                }
                else 
                {
                    $ud = array();
                    $ud['performance_level'] = $pl->performance_level;
                    $ud['performance_level_rating'] = $pl->performance_level_rating;
                    $ud['account_tag_id'] = $standard['account_tag_id'];
                    $ud['performance_level_id'] = $pl->id;
                    $ud['description'] = NULL;
                    $ud['id'] = NULL;
                }
                $unique_descriptions_with_pls[] = $ud; 
            }
            $new_standards[$key]['unique_performance_level_desc'] = $unique_descriptions_with_pls;
        }
        
        return response()->json(['success' => true , 'standards_with_unique_descriptions' => $new_standards, 'performance_levels' => $performance_levels, 'get_framework_settings' => $get_framework]);
    }
    
    
    function script_for_adding_position_for_standards(Request $request)
    {
        $offset = $request->offset;
        $limit = $request->limit;
        $frameworks = AccountTag::where(array('tag_type' => '2'))->offset($offset)->limit($limit)->get();
        foreach($frameworks as $framework)
        {
          $standards = AccountTag::where(array('tag_type' => '0' , 'framework_id' => $framework['account_tag_id'] ))->get();
          foreach ($standards as $key => $standard)
          {
              DB::table('account_tags')->where(array('account_tag_id' => $standard['account_tag_id']))->update(array('standard_position' => $key+1  ));
          }
        }
        return response()->json(['success' => true , 'message' => 'All Framework Standards Postitions are set']);
    }
    
    function duplicate_performance_levels(Request $request)
    {
        $input = $request->all();
        $original_framework_id = $input['original_framework_id'];
        $duplicate_framework_id = $input['duplicate_framework_id'];
        
        $get_original_framework_settings = AccountFrameworkSetting::where(array('account_tag_id' => $original_framework_id))->first();
        $get_duplicate_framework_settings = AccountFrameworkSetting::where(array('account_tag_id' => $duplicate_framework_id))->first();
        
        $performance_levels = AccountFrameworkSettingPerformanceLevel::where(array('account_framework_setting_id' => $get_original_framework_settings->id))->orderby('performance_level_rating','DESC')->get();
        
        foreach ($performance_levels as $pl)
        {
              $performace_level_data = array(
                    'account_id' => $pl['account_id'],
                    'account_framework_setting_id' => $get_duplicate_framework_settings->id,
                    'performance_level' => $pl['performance_level'],
                    'performance_level_rating' => $pl['performance_level_rating'],
                    'description' => $pl['description'],
                    'site_id' => $pl['site_id']
                );
                
            $performance_level_id = DB::table('account_framework_setting_performance_levels')->insertGetId($performace_level_data);
            $unique_descs = PerformanceLevelDescription::where(array('performance_level_id' => $pl['id'] ))->get();
            foreach ($unique_descs as $desc)
            {
               $copied_standard = AccountTag::where(array('source_standard_id' => $desc['account_tag_id'] , 'framework_id' => $duplicate_framework_id  ))->first();
               $unique_desc_data = array(
                   'account_tag_id' => $copied_standard->account_tag_id,
                   'performance_level_id' => $performance_level_id,
                   'description' => $desc->description,
                   'site_id' => $desc->site_id
               );
              DB::table('performance_level_descriptions')->insertGetId($unique_desc_data);
            }
        }
        
        if($get_original_framework_settings->enable_performance_level == '1')
        {
            DB::table('account_framework_settings')->where(['account_tag_id' => $duplicate_framework_id])->update(['enable_performance_level' => '1' , 'enable_unique_desc' => $get_original_framework_settings->enable_unique_desc ]);
        }
        
        return response()->json(['success' => true , 'message' => 'Performance Levels are Successfully Duplicated.']);
        
    }
    
    function delete_all_performance_levels(Request $request)
    {
        $input = $request->all();
        $framework_id = $input['framework_id'];
        $framework_settings = AccountFrameworkSetting::where(array('account_tag_id' => $framework_id))->first();
        $performance_levels = AccountFrameworkSettingPerformanceLevel::where(array('account_framework_setting_id' => $framework_settings->id ))->get();
        DB::table('account_framework_setting_performance_levels')->where(array('account_framework_setting_id' => $framework_settings->id ))->delete();
        foreach ($performance_levels as $pl)
        {
            DB::table('performance_level_descriptions')->where(array('performance_level_id' => $pl->id ))->delete();
            DB::table('document_standard_ratings')->where(array('rating_id' => $pl->id  ))->delete();
            DB::table('goal_item_standard_ratings')->where(array('rating_id' => $pl->id  ))->delete();
        }
        AccountFrameworkSetting::where(array('account_tag_id' => $framework_id))->update(array('enable_performance_level' => '0' , 'published' => '0' , 'updated_at' => date('Y-m-d H:i:s')));
        return response()->json(['success' => true , 'message' => 'Performance Levels are Successfully Deleted.']);
    }
    
}