<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountFolder;
use App\Models\Document;
use App\Models\DocumentFolder;
use App\Models\User;
use App\Models\UserActivityLog;
use App\Services\HelperFunctions;
use App\Services\TranslationsManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpParser\Comment\Doc;


class FoldersController extends Controller
{
    protected $site_id;

    public function __construct(Request $request)
    {
        $this->site_id = $request->header('site_id');
    }
    function create(Request $request) {
        $user_id = $request->get('user_id');
        $account_id = $request->get('account_id');
        $parent_folder_id = $request->get('parent_folder_id');
        $folder_name = $request->get('name');
        $huddle_id = $request->has('account_folder_id')?$request->get('account_folder_id'):0;
        $conditions = array('site_id' => $this->site_id, 'active' => '1', 'name' => $folder_name, 'account_id' => $account_id);
        if (!empty($parent_folder_id)) {
            $parents = DocumentFolder::getNLevelParents($parent_folder_id);
            if($parents)
            {
                $totalParents = count($parents);
                if($totalParents >= 5)
                {
                    $json_data = array(
                        'success' => false,
                        'message' => TranslationsManager::get_translation('df_you_may_not_nest_more', 'Api/huddle_list', $this->site_id)
                    );
                    return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
                }
            }
        }
        if(!empty($huddle_id))
        {
            $conditions['account_folder_id'] = $huddle_id;
        }
        else
        {
            $conditions['created_by'] = $user_id;
        }
        $query = DocumentFolder::where($conditions);
        if(empty($huddle_id))
        {
            $query->where(function ($q){
                $q->where('account_folder_id', 0)->orWhereNull('account_folder_id');
            });
        }
        if(empty($parent_folder_id) || $parent_folder_id == 0)
        {
            $query->where(function ($q){
                $q->where('parent_folder_id', 0)->orWhereNull('parent_folder_id');
            });
        }
        else
        {
            $query->where('parent_folder_id', $parent_folder_id);
        }
        $result = $query->first();

        if (!empty($result)) {
            $json_data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('Folder_with_this_name_already_exists', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
        }
        $data = array(
            'name' => trim($folder_name),
            'created_date' => date("Y-m-d H:i:s"),
            'last_edit_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_by' => $user_id,
            'account_folder_id' => $huddle_id,
            'active' => 1,
            'account_id' => $account_id,
            'site_id' => $this->site_id
        );
        if (!empty($parent_folder_id)) {
            $data['parent_folder_id'] = $parent_folder_id;
        }

        $account_folder_id = DB::table('document_folders')->insertGetId($data);

        if(!empty($parent_folder_id))
        {
            DocumentFolder::where('id', $parent_folder_id)->update(['last_edit_date'=>date('Y-m-d H:i:s'), 'last_edit_by'=> $user_id]);
        }
        if (!empty($account_folder_id)) {

            AccountFolder::where('account_folder_id', $huddle_id)->update(['last_edit_date'=>date('Y-m-d H:i:s'), 'last_edit_by'=> $user_id]);

            if($huddle_id)
            {
                $activity_url = "/home/video_huddles/huddle/details/$huddle_id/artifacts/grid/".$account_folder_id;
            }
            else
            {
                $activity_url = "/home/workspace/workspace/home/grid/".$account_folder_id;
            }
            $user_activity_logs = array(
                'ref_id' => $account_folder_id,
                'desc' => $folder_name,
                'url' => $activity_url,
                'account_folder_id' => $account_folder_id,
                'date_added' => date("Y-m-d H:i:s"),
                'type' => '60',
                'site_id' => $this->site_id
            );
            // DB::table('user_activity_logs')->insertGetId($user_activity_logs);
            UserActivityLog::user_activity_logs($this->site_id, $user_activity_logs, $account_id, $user_id);

            $created_folder_object = DocumentFolder::where(array('id' => $account_folder_id, 'site_id' => $this->site_id))
                ->first();

            $arranged_folder_object = DocumentFolder::formatFolder($created_folder_object);
            $is_push_notification_allowed = HelperFunctions::is_push_notification_allowed($account_id);
            $channel = 'huddle-details-' . $huddle_id;
            if (!$huddle_id) {
                $channel = 'workspace-' . $account_id . "-" . $user_id;
            }
            $channel_data = array(
                'item_id' => $account_folder_id,
                'is_video_crop' => 0,
                'data' => $arranged_folder_object,
                'huddle_id' => $huddle_id,
                'channel' => $channel,
                'is_folder' => 1,
                'event' => "resource_added"
            );
            if(!$is_push_notification_allowed){
                HelperFunctions::broadcastEvent($channel_data, 'broadcast_event', false);
            } else {
                HelperFunctions::broadcastEvent($channel_data);
            }
            $json_data = array(
                'success' => true,
                'message' => TranslationsManager::get_translation('huddle_folder_created_successfully', 'Api/huddle_list', $this->site_id),
                'folder_object' => $arranged_folder_object
            );
            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
        } else {

            $json_data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('Folder_not_created', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
        }
    }

    public function rename(Request $request)
    {
        $folder_id = $request->get('folder_id');
        $user_id = $request->get('user_id');
        $account_id = $request->get('account_id');
        $account_folder_id = $request->has('account_folder_id') ? $request->get('account_folder_id') : 0;
        $parent_folder_id = $request->has('parent_folder_id') ? $request->get('parent_folder_id') : 0;
        $name = $request->get('name');

        if(empty($folder_id) || empty($name))
        {
            return response()->json(['status'=>false, "message"=>"Missing required data"], 200);
        }

        $conditions = array('site_id' => $this->site_id, 'active' => '1', 'name' => $name, 'account_id' => $account_id);

        $query = DocumentFolder::where($conditions);
        if(empty($account_folder_id))
        {
            $query->where(function ($q){
                $q->where('account_folder_id', 0)->orWhereNull('account_folder_id');
            });
            $query->where('created_by', $user_id);
        }
        else
        {
            $query->where('account_folder_id', $account_folder_id);
        }
        if(empty($parent_folder_id) || $parent_folder_id == 0)
        {
            $query->where(function ($q){
                $q->where('parent_folder_id', 0)->orWhereNull('parent_folder_id');
            });
        }
        else
        {
            $query->where('parent_folder_id', $parent_folder_id);
        }
        $result = $query->first();

        if (!empty($result) && $folder_id != $result->id) {
            $json_data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('Folder_with_this_name_already_exists', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
        }

        $name = trim($name);
        DocumentFolder::where('id', $folder_id)->update(['name'=>$name, 'last_edit_date' => date("Y-m-d H:i:s"), 'last_edit_by' => $user_id]);
        $folder = DocumentFolder::find($folder_id);
        $folder = DocumentFolder::formatFolder($folder);
        $is_push_notification_allowed = HelperFunctions::is_push_notification_allowed($account_id);
        $channel = 'huddle-details-' . $account_folder_id;
        if (!$account_folder_id) {
            $channel = 'workspace-' . $account_id . "-" . $user_id;
        }
        $channel_data = array(
            'item_id' => $folder_id,
            'is_video_crop' => 0,
            'data' => $folder,
            'huddle_id' => $account_folder_id,
            'channel' => $channel,
            'is_folder' => 1,
            'video_file_name' =>  str_replace('amp;','',strip_tags($name)),
            'event' => "resource_renamed"
        );
        if(!$is_push_notification_allowed){
            HelperFunctions::broadcastEvent($channel_data, 'broadcast_event', false);
        } else {
            HelperFunctions::broadcastEvent($channel_data);
        }
        $json_data = array(
            'success' => true,
            'message' => TranslationsManager::get_translation('Folder_updated_successfully', 'Api/huddle_list', $this->site_id)
        );
        return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
    }

    function delete(Request $request) {

        $folder_id = $request->get('folder_id');
        $user_id = $request->get('user_id');
        $account_id = $request->get('account_id');
        $account_folder_id = $request->has('account_folder_id') ? $request->get('account_folder_id') : 0;

        $folder = DocumentFolder::find($folder_id);
        if(!$folder)
        {
            $json_data = array(
                'success' => false,
                'message' => "Folder is Invalid or Already deleted!"
            );
            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
        }
        $result = DocumentFolder::where(array('parent_folder_id' => $folder_id, 'active' => '1', 'site_id' => $this->site_id))
            ->count();
        $artifacts = Document::where('parent_folder_id', $folder_id)->count();
        if (!empty($result) || !empty($artifacts)) {
            $json_data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('Folder_cannot_be_deleted_artifact', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
        }


        $data = array(
            'active' => 0,
            'last_edit_by' => $user_id,
            'last_edit_date' => date('Y-m-d H:i:s'),
        );

        DocumentFolder::where('id', $folder_id)->update($data);
        $is_push_notification_allowed = HelperFunctions::is_push_notification_allowed($account_id);
        $channel = 'huddle-details-' . $account_folder_id;
        if (!$account_folder_id) {
            $channel = 'workspace-' . $account_id . "-" . $user_id;
        }
        $channel_data = array(
            'item_id' => $folder_id,
            'data' => $folder_id,
            'huddle_id' => $account_folder_id,
            'parent_folder_id' => $folder->parent_folder_id,
            'channel' => $channel,
            'is_folder' => 1,
            'event' => "resource_deleted"
        );
        if(!$is_push_notification_allowed){
            HelperFunctions::broadcastEvent($channel_data, 'broadcast_event', false);
        } else {
            HelperFunctions::broadcastEvent($channel_data);
        }
        $json_data = array(
            'success' => true,
            'message' => TranslationsManager::get_translation('Folder_deleted_successfully', 'Api/huddle_list', $this->site_id)
        );
        return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);

    }

    public function getFoldersInFolder()
    {
        $folders = DocumentFolder::with('childrenRecursive')->where(function ($q){
            $q->where('parent_folder_id', 0)->orWhereNull('parent_folder_id');
        })->get();
    }

    public function treeview_data(Request $request, $internal = 0)
    {
        $account_id = $request->get('account_id');
        $user_id = $request->get('user_id');
        $account_folder_id = $request->has('account_folder_id') && !empty($request->get('account_folder_id')) ? $request->get('account_folder_id') : 0;
        $parent_folder_id = $request->has('parent_folder_id') && !empty($request->get('parent_folder_id')) ? $request->get('parent_folder_id') : 0;
        $append_root = ($request->has('append_root') && $request->get('append_root') == 1) ? 1 : 0;
        $check_if_exist = ($request->has('check_if_exist') && $request->get('check_if_exist') == 1) ? 1 : 0;
        $skip_folder_id = $request->has('skip_folder_id') ? $request->get('skip_folder_id') : 0;
        $name = $request->has('name') && !empty($request->get('name')) ? $request->get('name') : 0;
        $query = DocumentFolder::where(array('account_id' => $account_id, 'active' => 1, 'site_id' => $this->site_id));
        if($account_folder_id)
        {
            $query->where('account_folder_id', $account_folder_id);
        }
        else
        {
            $query->where(function ($q){
                $q->where('account_folder_id', 0)->orWhereNull('account_folder_id');
            });
        }
        if(empty($account_folder_id))
        {
            $query->where('created_by', $user_id);
        }
        if($name)
        {
            $query->where('name', 'like', ($check_if_exist ? trim($name) : "%".trim($name)."%"));
        }
        if($skip_folder_id)
        {
            $query->where('id', "!=", $skip_folder_id);
        }
        if($check_if_exist)
        {
            if($parent_folder_id)
            {
                $query->where('parent_folder_id', $parent_folder_id);
            }
            else
            {
                $query->where(function ($q){
                    $q->where('parent_folder_id', 0)->orWhereNull('parent_folder_id');
                });
            }
        }

        $folders = $query->get()->toArray();
        if($check_if_exist)
        {
            return $internal ? $folders : response()->json(['status' => true, 'data'=>$folders, 'message'=>'success!']);
        }
        $data = $this->convertToTreeStructure($folders, $account_folder_id, $append_root);

        if($internal)
        {
            return $data;
        }

        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }

    public static function convertToTreeStructure($folders, $account_folder_id, $append_root = 0)
    {

        if ($folders) {
            $ids_arr = array();
            foreach ($folders as $folder) {
                array_push($ids_arr, $folder['id']);
            }
            $accessible_folders = array();
            foreach ($folders as $folder)
            {
                $temp = array();
                $temp['parent'] = in_array($folder['parent_folder_id'], $ids_arr) ? $folder['parent_folder_id'] : '#';
                $temp['id'] = $folder['id'];
                $temp['text'] = $folder['name'];
                $temp['workspace'] = (isset($folder['account_folder_id']) && !empty($folder['account_folder_id'])) ? 0 : 1;
                $temp['account_folder_id'] = (isset($folder['account_folder_id']) ? $folder['account_folder_id'] : 0);
                $temp['text'] = $folder['name'];
                $accessible_folders[] = $temp;
            }
            $folders = $accessible_folders;
        }

        $data = $folders;

        foreach ($data as $key => &$item) {

            if ($item['parent'] == null || empty($item['parent']))
                $item['parent'] = '#';
        }
        if($append_root)
        {
            $huddle = AccountFolder::where('account_folder_id', $account_folder_id)->first();
            $dat = array(
                'id' => '0',
                'text' => $account_folder_id ? ($huddle ? $huddle->name : TranslationsManager::get_translation('huddle_title')) : TranslationsManager::get_translation('myfile_workspace'),
                'parent' => '#');

            array_unshift($data, $dat);
        }
        return $data;
    }

    public function move_document_folder(Request $request) {

        $object_id = $request->get('object_id');
        $folder_id = $request->get('folder_id');
        $account_id = $request->get('account_id');
        $workspace = $request->has('workspace') ? $request->get('workspace') : 0;
        $user_id = $request->get('user_id');
        $account_folder_id = $request->has('account_folder_id') ? $request->get('account_folder_id') : 0;
        $folder_type = 1;
        if ($workspace) {
            $folder_type = 3;
        }
        $is_folder = $request->has('is_folder') ? $request->get('is_folder') : 0;
        if (!$object_id)
        {
            $data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('df_artifact_not_moved', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($data, 200, [], JSON_PRETTY_PRINT);
        }
        if($is_folder)
        {
            $get_current_directory = DocumentFolder::where(array('id' => $object_id, 'site_id' => $this->site_id))->first()->toArray();
        }
        else
        {
            $get_current_directory = Document::where(array('id' => $object_id, 'site_id' => $this->site_id))->first()->toArray();
        }

        if ($folder_id > 0) {

            $get_destination_folder = DocumentFolder::where(array('id' => $folder_id, 'site_id' => $this->site_id))->first()->toArray();

            if (($is_folder && $get_destination_folder['parent_folder_id'] == $object_id)) {

                $data = array(
                    'success' => false,
                    'message' => TranslationsManager::get_translation('Destination_folder_is_inside_the_folder', 'Api/huddle_list', $this->site_id)
                );

                return response()->json($data, 200, [], JSON_PRETTY_PRINT);
            }
        }

        if (empty($get_current_directory['parent_folder_id']) && $folder_id < 1) {
            $data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('df_artifact_folder_is_already_in_this_Folder', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($data, 200, [], JSON_PRETTY_PRINT);
        }

        if ($get_current_directory['parent_folder_id'] == $folder_id) {

            $data = array(
                'success' => false,
                'message' => TranslationsManager::get_translation('df_artifact_folder_is_already_in_this_Folder', 'Api/huddle_list', $this->site_id)
            );
            return response()->json($data, 200, [], JSON_PRETTY_PRINT);
        }

        if($is_folder)
        {
            DocumentFolder::where('id',$object_id)->update(['parent_folder_id'=>$folder_id]);
        }
        else
        {
            Document::where('id',$object_id)->update(['parent_folder_id'=>$folder_id]);
        }
        DocumentFolder::where('id', $folder_id)->update(['last_edit_date'=>date('Y-m-d H:i:s'), 'last_edit_by'=> $user_id]);

        AccountFolder::where('account_folder_id', $account_folder_id)->update(['last_edit_date'=>date('Y-m-d H:i:s'), 'last_edit_by'=> $user_id]);



        $is_push_notification_allowed = HelperFunctions::is_push_notification_allowed($account_id);
        $channel = 'huddle-details-' . $account_folder_id;
        if (!$account_folder_id) {
            $channel = 'workspace-' . $account_id . "-" . $user_id;
        }
        if($is_folder)
        {
            $data = DocumentFolder::formatFolder($get_current_directory);
        }
        else
        {
            $data = Document::get_single_video_data($this->site_id, $object_id, $folder_type, 1, $account_folder_id, $user_id);
        }
        $channel_data = array(
            'item_id' => $object_id,
            'data' => $data,
            'huddle_id' => $account_folder_id,
            'dest_folder_id' => $folder_id,
            'old_folder_id' => $get_current_directory['parent_folder_id'],
            'channel' => $channel,
            'is_folder' => $is_folder,
            'event' => "resource_moved"
        );
        if(!$is_push_notification_allowed){
            HelperFunctions::broadcastEvent($channel_data, 'broadcast_event', false);
        } else {
            HelperFunctions::broadcastEvent($channel_data);
        }

        $data = array(
            'success' => true,
            'message' => TranslationsManager::get_translation('df_artifact_moved_to_folder', 'Api/huddle_list', $this->site_id)
        );
        return response()->json($data, 200, [], JSON_PRETTY_PRINT);
    }

    public function search_huddle_folders(Request $request)
    {
        $name = strtolower(trim($request->get('name')));
        $account_id = $request->get('account_id');
        $user_id = $request->get('user_id');
        $is_sharing_folder = $request->has('is_sharing_folder') ? $request->get('is_sharing_folder') : 0;
        $from_web = $request->has('from_web') ? $request->get('from_web') : 0;
        $custom_request = new Request(['user_id'=>$user_id, "account_id" => $account_id]);
        $videoController = new VideoController($request);
        $huddles = $videoController->get_copy_huddle($custom_request);
        $accounts = [];
        if(isset($huddles['all_accounts']))
        {
            $accounts = $huddles['all_accounts'];
        }
        if(isset($huddles['all_huddles']))
        {
            $huddles = $huddles['all_huddles'];
        }

        $final_huddles = [];
        foreach ($huddles as $huddle)
        {
            if($huddle['meta_data_value'] == 3)
            {
                //dont show assessment huddle in share modal sw-5306
                continue;
            }
            $request->merge(['account_folder_id' => $huddle['account_folder_id']]);
            $request->merge(['name' => '']);
            $folders = $this->treeview_data($request, 1);
            $folder_matched = 0;
            foreach ($folders as $folder)
            {
                if(!empty($folder['text']) && !empty($name) && strpos(strtolower($folder['text']), $name) !== false)
                {
                    $folder_matched = 1;
                    break;
                }
            }
            if($folder_matched == 0 && !empty($huddle['name']) && !empty( $name) && strpos(strtolower($huddle['name']), $name) === false)
            {
                //Huddle name dont mach the search and it also not contain any matching folder
                continue;
            }
            $huddle['folders'] = $folders;
            $final_huddles[] = $huddle;
        }
        if($from_web)
        {
            $final_huddles = ['all_huddles'=>$final_huddles,"all_accounts"=>$accounts];
        }
        return response()->json($final_huddles, 200, [], JSON_PRETTY_PRINT);
    }

    public function share_folder(Request $request)
    {
        $folder_id = $request->get('folder_id');
        $user_id = $request->get('user_id');
        $account_id = $request->get('account_id');
        $account_folder_id = $request->get('account_folder_id');
        $dest_folder_id = $request->has('dest_folder_id') ? $request->get('dest_folder_id') : 0;
        $dest_huddle_id = $request->has('dest_huddle_id') ? $request->get('dest_huddle_id') : 0;
        $from_workspace = $request->has('from_workspace') ? $request->get('from_workspace') : 0;
        $workspace = $request->has('workspace') ? $request->get('workspace') : 0;
        $copy_notes = $request->has('copy_notes') ? $request->get('copy_notes') : 0;
        $dest_account_id = $request->has('dest_account_id') ? $request->get('dest_account_id') : $account_id;
        if($dest_huddle_id)
        {
            $dest_account_id = $account_id;
        }
        $folder_to_share = DocumentFolder::find($folder_id);
        if(!$folder_to_share)
        {
            return response()->json(['status'=>false, "message"=>"Folder not Found!"], 200);
        }
        else
        {
            $c_request = new Request([
                'name' => $folder_to_share->name,
                'user_id' => $user_id,
                'account_id' => $dest_account_id,
                'account_folder_id' => $dest_huddle_id,
                'parent_folder_id' => $dest_folder_id,
                'site_id' => $this->site_id,
                'check_if_exist' => 1,
            ]);
            $search_folder = $this->treeview_data($c_request, 1);
            if(!empty($search_folder))
            {
                $json_data = array(
                    'success' => false,
                    'message' => TranslationsManager::get_translation('Folder_with_this_name_already_exists', 'Api/huddle_list', $this->site_id)
                );
                return response()->json($json_data, 200, [], JSON_PRETTY_PRINT);
            }
        }

        $folders = DocumentFolder::getNLevelChilds($folder_id,1);
        $folder_ids = [];
        $parents = [];
        $videoController = new VideoController($request);
        $first_iteration = 1;
        $main_folder_id = 0;
        foreach ($folders as $folder)
        {
            $temp = new DocumentFolder();
            $temp->name = $folder->name;
            $temp->created_by = $user_id;
            $temp->site_id = $this->site_id;
            $temp->created_date = date('Y-m-d H:i:s');
            $temp->account_folder_id = $dest_huddle_id;
            $temp->account_id = $dest_account_id;
            $temp->active = 1;
            if(isset($parents[$folder->parent_folder_id]))
            {
                $temp->parent_folder_id = $parents[$folder->parent_folder_id];
            }
            else
            {
                $temp->parent_folder_id = 0;
            }
            if($first_iteration)
            {
                $temp->parent_folder_id = $dest_folder_id;
            }
            $temp->save();
            if($first_iteration)
            {
                $main_folder_id = $temp->id;
                if(!empty($dest_folder_id))
                {
                    DocumentFolder::where('id', $dest_folder_id)->update(['last_edit_date'=>date('Y-m-d H:i:s'), 'last_edit_by'=> $user_id]);
                }
            }
            $parents[$folder->id] = $temp->id;
            $folder_ids[] = $folder->id;
            $first_iteration = 0;
        }
        $documents = Document::leftJoin("account_folder_documents", "account_folder_documents.document_id","=","documents.id")->select('documents.*','account_folder_documents.account_folder_id')->whereIn('parent_folder_id', $folder_ids)->where('doc_type', '!=', 4)->get();
        $doc_ids = "";
        foreach ($documents as &$document)
        {
            $document->parent_folder_id = isset($parents[$document->parent_folder_id]) ? $parents[$document->parent_folder_id] : 0;
            $doc_ids .= $document->id."_".$document->parent_folder_id.",";
        }
        $doc_ids = rtrim($doc_ids,',');
        if(empty($dest_huddle_id))
        {
            $videoController->copyToAccountWorkSpace($dest_account_id, $user_id, $doc_ids, $copy_notes, 0, 0);
        }
        else
        {
            foreach ($documents as $document)
            {
                $c_request = new Request(
                    [
                        "document_id"=>$document->id,
                        "account_folder_id"=>[$dest_huddle_id],
                        "current_huddle_id"=>!empty($document->account_folder_id) ? $document->account_folder_id : $account_folder_id,
                        "parent_folder_id"=>$document->parent_folder_id,
                        "from_workspace"=>$from_workspace,
                        "workspace"=>$workspace,
                        "doc_type"=>$document->doc_type,
                        "user_id"=>$user_id,
                        "account_id"=>$dest_account_id,
                        "copy_notes"=>$copy_notes,
                        "app_name"=>$request->has('app_name') ? $request->get('app_name') : false,
                        "environment_type"=>$request->has('environment_type') ? $request->get('environment_type') : 2,
                        "is_scripted_note"=>$document->doc_type == 3 ? 1 : 0,
                    ]);
                $response = $videoController->copy($c_request, 0);
                if(!$response['success'])
                {
                    var_dump($response);die;
                }
            }
        }
        if($main_folder_id)
        {
            $folder = DocumentFolder::find($main_folder_id);
            if($folder)
            {
                $folder = DocumentFolder::formatFolder($folder);
                if(HelperFunctions::is_email_notification_allowed($account_id))
                {
                    $user = User::find($folder->created_by);
                    $account = Account::find($account_id);
                    $videoController->EmailCopyVideo($this->site_id, $account_folder_id, '', 0, $account_id, $user_id, $user->first_name, $user->last_name, $account->company_name, $user->image, false, 1);
                }
                $is_push_notification_allowed = HelperFunctions::is_push_notification_allowed($account_id);
                $channel = 'huddle-details-' . $dest_huddle_id;
                if (!$dest_huddle_id) {
                    $channel = 'workspace-' . $dest_account_id . "-" . $user_id;
                }
                $channel_data = array(
                    'item_id' => $account_folder_id,
                    'is_video_crop' => 0,
                    'data' => $folder,
                    'huddle_id' => $dest_huddle_id,
                    'channel' => $channel,
                    'is_folder' => 1,
                    'event' => "resource_added"
                );
                if(!$is_push_notification_allowed){
                    HelperFunctions::broadcastEvent($channel_data, 'broadcast_event', false);
                } else {
                    HelperFunctions::broadcastEvent($channel_data);
                }
            }
        }
        return response()->json(["status"=>true, "message"=>TranslationsManager::get_translation('df_folder_shared_successfully')]);
    }
}
