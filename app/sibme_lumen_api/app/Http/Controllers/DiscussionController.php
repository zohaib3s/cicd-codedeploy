<?php

namespace App\Http\Controllers;

use App\Models\AccountFolder;
use App\Models\Comment;
use App\Models\User;
use App\Models\UserReadComments;
use App\Models\Document;
use App\Models\CommentAttachment;
use App\Services\Emails\Email;
use App\Services\TranslationsManager;
use App\Services\HelperFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use Illuminate\Support\Facades\View;
use CreateDocx;

class DiscussionController extends Controller
{

    protected $httpreq;
    protected $site_id;
    protected $site_title;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        //
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        if ($this->site_id == 1) {
            $this->site_title = 'Sibme';
        } else {
            $this->site_title = 'Coaching Studio';
        }
    }

    /* ===========FOR DETAIL=============*/
    function detail(Request $request)
    {
        $input = $request->all();

        $huddle_id = $input['huddle_id'];
        $detail_id = !empty($input['detail_id'])?$input['detail_id']:'';
        $goal_evidence = isset($input['goal_evidence']) ? $input['goal_evidence'] == 1 : 0;
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $sort = $input['sort'];
        if($detail_id =='' || $detail_id =='undefined'){           
            return response()->json(array(
                'status'=>false,
                'message'=>'Invalid Details Reference key , please try with correct key',
                'data'=>''
            ));
        }
        if($sort == 'newest')
        {
            $orderBy = 'desc';
        }elseif($sort == 'oldest')
        {
            $orderBy = 'asc';
        }
        $reply_sorting = 'asc';
        
        /*
        $is_mobile_request = false;
        if (!empty($input['app_name']) && ($input['app_name'] == 'com.sibme.mobileapp' || $input['app_name'] == 'com.sibme.sibme')) {
            $is_mobile_request = true;
        } else {
            $is_mobile_request = false;
        }
        */
        $is_mobile_request = isset($input['app_name']) ? HelperFunctions::is_mobile_request($input['app_name']) : false;
        $huddle_type = HelperFunctions::get_huddle_type($huddle_id);

        $huddle_info = AccountFolder::where('account_folder_id', $huddle_id)->first();
        /*FOR comments*/
        
        if (isset($detail_id) && $detail_id != '') {
             $read_comments_ids = UserReadComments::where("discussion_id",$detail_id)->where("user_id",$user_id)->pluck('comment_id')->toArray();
            $replyDiscussions = Comment::commentReplys($detail_id,0,$orderBy,$is_mobile_request);
            
            $newreplyDiscussions = array();
            if($replyDiscussions)
            {
                $replyDiscussions = $replyDiscussions->toArray();
                foreach ($replyDiscussions as $key => $discussion)
                {
                $newreplyDiscussions[$key] = $discussion;
                $newreplyDiscussions[$key]->discussion_delete_perm_in_coaching = HelperFunctions::discussion_delete_perm_in_coaching($huddle_id,$user_id,$discussion->created_by);
                $newreplyDiscussions[$key]->huddle_type = $huddle_type;
                $newreplyDiscussions[$key]->user_huddle_role = HelperFunctions::get_huddle_roles($huddle_id, $user_id);
                $newreplyDiscussions[$key]->coachee_permission = HelperFunctions::coachee_permissions($huddle_id);
                $replyreplyObject = Comment::commentReplys($discussion->id,0,$reply_sorting,$is_mobile_request);
                             
                    if($replyreplyObject)
                    {
                    $replyreplyObject = $replyreplyObject->toArray();
                   
                    foreach($replyreplyObject as $line=>$row)
                    {
                        $row->discussion_delete_perm_in_coaching = HelperFunctions::discussion_delete_perm_in_coaching($huddle_id,$user_id,$row->created_by);
                        $row->huddle_type = $huddle_type;
                        $row->user_huddle_role = HelperFunctions::get_huddle_roles($huddle_id, $user_id);
                        $row->coachee_permission = HelperFunctions::coachee_permissions($huddle_id);
                        if(!in_array($row->id,$read_comments_ids))
                        {
                            $replyreplyObject[$line]->unread = 0;
                        }
                        else
                        {
                            $replyreplyObject[$line]->unread  = 1;
                        } 
                         
                    }
                    $newreplyDiscussions[$key]->replies = $replyreplyObject;                     
                   
                                     
                    }
                    else
                    {
                    $newreplyDiscussions[$key]->replies = [];  
                    }
                }
            }
                      
            $replyDiscussions = $newreplyDiscussions;
            
        }
        
       
        $read_comments_ids = UserReadComments::where("discussion_id",$detail_id)->where("user_id",$user_id)->pluck('comment_id')->toArray();
        $new_comment_ids = [];
        
        if($is_mobile_request)
        {
            $newreplyDiscussions = array();
            foreach ($replyDiscussions as $key => $reply)
            {
                $newreplyDiscussions[$key] = $reply;
                if(!in_array($reply->id,$read_comments_ids))
                {
                    $newreplyDiscussions[$key]->unread = 0;
                }
                else
                {
                    $newreplyDiscussions[$key]->unread = 1;
                }
            }
            
          $replyDiscussions = $newreplyDiscussions;  
            
        }
        else {
            
            foreach ($replyDiscussions as $reply)
            {
                if(!in_array($reply->id,$read_comments_ids) || UserReadComments::checkIfCommentAlreadyExists($reply->id, $user_id))
                {
                    $new_comment_ids[$reply->id] = ["comment_id"=>$reply->id,"discussion_id"=>$detail_id,"user_id"=>$user_id];
                }
                if(isset($reply->replies) && count($reply->replies) > 0){
                    $usb_replies = $reply->replies;
                   
                    foreach( $usb_replies  as $sub_reply){    
                        if(!in_array($sub_reply->id,$read_comments_ids) || UserReadComments::checkIfCommentAlreadyExists($sub_reply->id, $user_id))
                        {
                            $new_comment_ids[$sub_reply->id] = ["comment_id"=>$sub_reply->id,"discussion_id"=>$detail_id,"user_id"=>$user_id];
                        }  
                    }

                }

            }
           
           
            if(!empty($new_comment_ids))
            {
                $prepare_result = [];
                foreach($new_comment_ids as $row){
                    $prepare_result[] =  $row;
                }
                          
                // UserReadComments::insert($new_comment_ids);
                $insert_res = UserReadComments::insertIgnore($prepare_result);
                if($insert_res===false){
                    return response()->json(['success'=>false,'message'=>'Error while inserting read comments!']);
                }
            }
            
            
        }
        $user_huddle_role = HelperFunctions::get_huddle_roles($huddle_id, $user_id);
        $discussionsData = array(
            'discussions' => Comment::getsingleDiscussion($huddle_id, $this->site_id,$detail_id,$user_id,$is_mobile_request),
            /*'discussion_attachment'=>Comment::getattachments($detail_id),*/
            'adminUsers' => User::getUsersByRole($account_id, array('110', '100','115','120')),
            'huddle_id' => $huddle_id,
            'huddle' => $huddle_info,
            'huddles_users' => AccountFolder::getHuddleUsers($huddle_id, $this->site_id),
            'videoHuddleGroups' => AccountFolder::getHuddleGroupsWithUsers($huddle_id, $this->site_id),
            'user_id' => $user_id,
            'replys' => isset($replyDiscussions) ? $replyDiscussions : '',
            'user_huddle_role' => $user_huddle_role,
            'isGoalViewer' => $goal_evidence,
            'coachee_permission' => HelperFunctions::coachee_permissions($huddle_id)
        );
        
        return response()->json($discussionsData);

    }

    /* ===========FOR DOWNLOAD DISCUSSION =============*/

    function export_download(Request $request)
    {
        $input = $request->all();
        $huddle_id = $input['huddle_id'];
        $detail_id = $input['detail_id'];
        //$account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $format = $request->get("export_type");
        /*FOR comments*/

        $replyDiscussions = Comment::commentReplys($detail_id);
        $newreplyDiscussions = array();
        if($replyDiscussions)
        {
            $replyDiscussions = $replyDiscussions->toArray();
            foreach ($replyDiscussions as $key => $discussion)
            {
                $newreplyDiscussions[$key] = $discussion;
                $replyreplyObject = Comment::commentReplys($discussion->id);
                if($replyreplyObject)
                {
                    $replyreplyObject = $replyreplyObject->toArray();
                    $newreplyDiscussions[$key]->replies = $replyreplyObject;
                }
                else
                {
                    $newreplyDiscussions[$key]->replies = [];
                }
            }
        }

        $discussionReplies = $newreplyDiscussions;
        $discussionAttachments = Comment::getattachments($detail_id);

        $discussion = Comment::getsingleDiscussion($huddle_id, $this->site_id,$detail_id,$user_id)[0];



        /*$phpWord = new PhpWord();


        $section = $phpWord->addSection();*/

        $html = view('export/discussion',compact("discussion","discussionReplies", "discussionAttachments"));
        
        if($format == "pdf"){
            // @ini_set("memory_limit", "1000M");
            // set_time_limit(100);
            // $file = str_replace('/', '-', $discussion["title"]).' - '.TranslationsManager::get_translation('discussion_discussion_export').'.pdf';
            // $pdf = app()->make('dompdf.wrapper');
            // $pdf->loadHTML($html);
            // return $pdf->download($file);

            $docx = new CreateDocx();
            $docx->embedHTML($html);
            $file_name = str_replace('/', '-', $discussion["title"]).'-'.TranslationsManager::get_translation('discussion_discussion_export');
            $file_docx = $file_name.'.docx';
            $file_pdf = $file_name.'.pdf';
            $fileDocxFullPathWithFile = public_path("attachments/phpdocx/".$file_docx);
            $filePdfFullPath = public_path("attachments/phpdocx/");
            $filePdfFullPathWithFile = public_path("attachments/phpdocx/".$file_pdf);
            $docx->createDocx($fileDocxFullPathWithFile);

            $result = exec('export HOME=/tmp/ && /usr/bin/libreoffice7.0 --headless --convert-to pdf:writer_pdf_Export --outdir "'.$filePdfFullPath.'" "'.$fileDocxFullPathWithFile.'" 2>&1');
            \Log::info("============ LibreOffice @ Line:".__LINE__." =============");
            \Log::info($result);
            // dd($result); 

            // $docx->transformDocument($fileDocxFullPathWithFile, $filePdfFullPathWithFile);

            header('Content-Type: application/pdf');
            header("Content-Disposition: attachment; filename=\"$file_pdf\"");
            header('Content-Length: ' . filesize($filePdfFullPathWithFile));
            readfile($filePdfFullPathWithFile);

        }else if($format == "word"){
            $docx = new CreateDocx();
            //$text = 'Phpdocx. Easily create Word and PDF documents online';

            //$docx->addText($text);
            
            $docx->embedHTML($html);
            $file = str_replace('/', '-', $discussion["title"]).' - '.TranslationsManager::get_translation('discussion_discussion_export').'docx';
            $filefullpath = public_path("attachments/phpdocx/".$file);
            $docx->createDocxAndDownload($filefullpath);
        }
        /*\PhpOffice\PhpWord\Shared\Html::addHtml($section,$html,false,false);


        $file = $discussion["title"].' Discussion - Export.docx';
        $filefullpath = public_path($file);
        //$section->addImage("./images/Krunal.jpg");
        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
        //$objWriter->save($filefullpath);
        //return response()->download(public_path('export/wordexample'.time().'.docx'));

        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="' . $file . '"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        $objWriter->save('php://output');*/
    }

    function all_export_download(Request $request)
    {
        $input = $request->all();
        $huddle_id = $input['huddle_id'];
        $user_id = $input['user_id'];
        $format = $request->get("export_type");
        
        
        $main_discussion_ids_query = Comment::where(array('ref_id' => $huddle_id,'ref_type' => '1','active' => '1'))->whereRaw('(parent_comment_id IS NULL OR parent_comment_id = 0)')->get()->toArray();
        $html = '';
        if(empty($main_discussion_ids_query))
        {
            $html = '<html><body style="background-color: #fff;font-family: arial;"><p><b>No Discussion Found.</b></p></body></html>';
            //return response()->json(['success' => false , 'message' => 'No Discussion Found.' ]);
        }
        foreach($main_discussion_ids_query as $detail_id)
        {
        /*FOR comments*/

        $replyDiscussions = Comment::commentReplys($detail_id['id']);
        $huddle_detail = AccountFolder::where(array('account_folder_id' => $huddle_id))->first();
        $newreplyDiscussions = array();
        if($replyDiscussions)
        {
            $replyDiscussions = $replyDiscussions->toArray();
            foreach ($replyDiscussions as $key => $discussion)
            {
                $newreplyDiscussions[$key] = $discussion;
                $replyreplyObject = Comment::commentReplys($discussion->id);
                if($replyreplyObject)
                {
                    $replyreplyObject = $replyreplyObject->toArray();
                    $newreplyDiscussions[$key]->replies = $replyreplyObject;
                }
                else
                {
                    $newreplyDiscussions[$key]->replies = [];
                }
            }
        }

        $discussionReplies = $newreplyDiscussions;
        $discussionAttachments = Comment::getattachments($detail_id['id']);

        $discussion = Comment::getsingleDiscussion($huddle_id, $this->site_id,$detail_id['id'],$user_id)[0];



        /*$phpWord = new PhpWord();


        $section = $phpWord->addSection();*/

        $html.= view('export/discussion',compact("discussion","discussionReplies", "discussionAttachments"));
        $html.= "<br><br>";
        $html.= '<table width="100%"><tr><td style="border-bottom:1px dotted black;"></td></tr></table>';
        
        }
        if($format == "pdf"){
            // @ini_set("memory_limit", "1000M");
            // set_time_limit(100);
            // $file = str_slug($huddle_detail->name).' - '.TranslationsManager::get_translation('discussion_discussion_export').'.pdf';
            // $pdf = app()->make('dompdf.wrapper');
            // $pdf->loadHTML($html);
            // return $pdf->download($file);

            $docx = new CreateDocx();
            $docx->embedHTML($html);
            $file_name = str_slug($huddle_detail->name).'-'.TranslationsManager::get_translation('discussion_discussion_export');
            $file_docx = $file_name.'.docx';
            $file_pdf = $file_name.'.pdf';
            $fileDocxFullPathWithFile = public_path("attachments/phpdocx/".$file_docx);
            $filePdfFullPath = public_path("attachments/phpdocx/");
            $filePdfFullPathWithFile = public_path("attachments/phpdocx/".$file_pdf);
            $docx->createDocx($fileDocxFullPathWithFile);

            $result = exec('export HOME=/tmp/ && /usr/bin/libreoffice7.0 --headless --convert-to pdf:writer_pdf_Export --outdir "'.$filePdfFullPath.'" "'.$fileDocxFullPathWithFile.'" 2>&1');
            \Log::info("============ LibreOffice @ Line:".__LINE__." =============");
            \Log::info($result);
            // dd($result); 

            // $docx->transformDocument($fileDocxFullPathWithFile, $filePdfFullPathWithFile);

            header('Content-Type: application/pdf');
            header("Content-Disposition: attachment; filename=\"$file_pdf\"");
            header('Content-Length: ' . filesize($filePdfFullPathWithFile));
            readfile($filePdfFullPathWithFile);
        }else if($format == "word"){
            $docx = new CreateDocx();
            //$text = 'Phpdocx. Easily create Word and PDF documents online';

            //$docx->addText($text);
            
            $docx->embedHTML($html);
            $file = str_slug($huddle_detail->name).' - '.TranslationsManager::get_translation('discussion_discussion_export').'.docx';
            $filefullpath = public_path("attachments/phpdocx/".$file);
            $docx->createDocxAndDownload($filefullpath);
        }
        /*\PhpOffice\PhpWord\Shared\Html::addHtml($section,$html,false,false);


        $file = $discussion["title"].' Discussion - Export.docx';
        $filefullpath = public_path($file);
        //$section->addImage("./images/Krunal.jpg");
        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
        //$objWriter->save($filefullpath);
        //return response()->download(public_path('export/wordexample'.time().'.docx'));

        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename="' . $file . '"');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        $objWriter->save('php://output');*/
    }


    function dummy(Request $request)
    {
        $input = $request->all();
        $huddle_id = $input['huddle_id'];
        $detail_id = $input['detail_id'];
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];

        $huddle_info = AccountFolder::where('account_folder_id', $huddle_id)->first();
        /*FOR comments*/
        if (isset($detail_id) && $detail_id != '') {
            $replyDiscussions = Comment::commentReplys($detail_id);
        }

            $discussions = Comment::getCommentsSearch($huddle_id, $this->site_id);
            $adminUsers = User::getUsersByRole($account_id, array('110', '100'));
            $huddle_id = $huddle_id;
            $huddle = $huddle_info;
            $huddles_users = AccountFolder::getHuddleUsers($huddle_id, $this->site_id);
            $videoHuddleGroups = AccountFolder::getHuddleGroupsWithUsers($huddle_id, $this->site_id);
            $user_id = $user_id;
            $replys = isset($replyDiscussions) ? $replyDiscussions : '';


        $phpWord = new PhpWord();

        /**/

        $view = View::make('export/discussion');

        $html = $view->render();
        // or cast the content into a string
        $html = (string) $view;
        $section = $phpWord->addSection();
        //$phpWord = IOFactory::load(, 'HTML');

        //$html = "<h1>".$discussions[0]['title']."</h1>";
        //$html .= "<p style='font-size:5px; '>".$discussions[0]['comment']."</p>";
        \PhpOffice\PhpWord\Shared\Html::addHtml($section,$html,false,false);



        //$section->addImage("./images/Krunal.jpg");
        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save(public_path('export/wordexample'.time().'.docx'));
        //return response()->download(public_path('export/wordexample'.time().'.docx'));

    }
    public  static function cleanString($text) {
        $regEx = '/([^<]*<\s*[a-z](?:[0-9]|[a-z]{0,9}))(?:(?:\s*[a-z\-]{2,14}\s*=\s*(?:"[^"]*"|\'[^\']*\'))*)(\s*\/?>[^<]*)/i'; // match any start tag

        $chunks = preg_split($regEx, $text, -1,  PREG_SPLIT_DELIM_CAPTURE);
        $chunkCount = count($chunks);

        $strippedString = '';
        for ($n = 1; $n < $chunkCount; $n++) {
            $strippedString .= $chunks[$n];
        }

        return $strippedString;
    }

    public function exportEmail(Request $request)
    {
        $huddle_id = $request->get("huddle_id");
        $detail_id = $request->get("detail_id");
        $email_message = $request->get("email_message");
        $replyDiscussions = Comment::commentReplys($detail_id);
        $newreplyDiscussions = array();
        if($replyDiscussions)
        {
            $replyDiscussions = $replyDiscussions->toArray();
            foreach ($replyDiscussions as $key => $discussion)
            {
                $newreplyDiscussions[$key] = $discussion;
                $replyreplyObject = Comment::commentReplys($discussion->id);
                if($replyreplyObject)
                {
                    $replyreplyObject = $replyreplyObject->toArray();
                    $newreplyDiscussions[$key]->replies = $replyreplyObject;
                }
                else
                {
                    $newreplyDiscussions[$key]->replies = [];
                }
            }
        }

        $discussionReplies = $newreplyDiscussions;
        $discussionAttachments = Comment::getattachments($detail_id);
        $discussion = Comment::getsingleDiscussion($huddle_id, $this->site_id,$detail_id)[0];

        $html = view('export/discussion',compact("discussion","discussionReplies", "discussionAttachments"));
        $docx = new CreateDocx();

        $docx->embedHTML($html);
        $directory_name = "attachments/phpdocx";
        $name = self::remove_special_chars($discussion["title"]).' - '.TranslationsManager::get_translation('discussion_discussion_export').'.docx';
        $full_file_name = $directory_name.'/'.$name;
        $docx->createDocx($full_file_name);

        $site_id = app("Illuminate\Http\Request")->header("site_id");
        $email = $request->get("user_email");
        
        $response = Email::sendEmail($site_id, 'emails.feedback', ['email_message'=>$email_message], $email, '', TranslationsManager::get_translation('discussion_discussion_export'), [public_path($full_file_name)]);
        if(file_exists(public_path($full_file_name)))
        {
            unlink(public_path($full_file_name));
        }
        if($response)
        {
            return ["status" => true,"message" => TranslationsManager::get_translation('discussion_discussions_emailed_successfully'), "data"=>$response];
        }
        else
        {
            return ["status" => false,"message" => $response];
        }
    }

    public static function remove_special_chars($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }
    
    function deleteDiscussionAttachment(Request $request) {
        
        $input = $request->all();
        $document_id = $input['document_id'];
        $response = response()->json(['success' => Document::deleteDiscussionAttachment($document_id,$this->site_id)]);
        if(isset($input['huddle_id']) && isset($input['discussion_id']) && isset($input['main_discussion_id']) && isset($input['user_id']) && !empty($input['discussion_id']) )
        {
            $discussion = Comment::getsingleDiscussion($input['huddle_id'], $this->site_id, $input['discussion_id'], $input['user_id']);
            $channel = 'discussions-details-' . $input['main_discussion_id'];

            $res = array(
                'document_id' => $input['discussion_id'],
                'reference_id' => $input['main_discussion_id'],
                'data' => $discussion,
                'huddle_id' => $input['huddle_id'],
                'channel' => $channel,
                'event' => 'discussion_edited'
            );

            HelperFunctions::broadcastEvent($res);
        }

        return $response;
    }
    
     function renameDiscussionAttachment(Request $request) {
        
        $input = $request->all();
        $title = $input['title'];
        $document_id = $input['document_id'];
        
        $response = response()->json(Document::renameDiscussionAttachment($title,$document_id,$this->site_id));
        
        if(isset($input['huddle_id']) && isset($input['discussion_id']) && isset($input['main_discussion_id']) && isset($input['user_id']) && !empty($input['discussion_id']) )
        {
            $discussion = Comment::getsingleDiscussion($input['huddle_id'], $this->site_id, $input['discussion_id'], $input['user_id']);
            $channel = 'discussions-details-' . $input['main_discussion_id'];

            $res = array(
                'document_id' => $input['discussion_id'],
                'reference_id' => $input['main_discussion_id'],
                'data' => $discussion,
                'huddle_id' => $input['huddle_id'],
                'channel' => $channel,
                'event' => 'discussion_edited'
            );

            HelperFunctions::broadcastEvent($res);
        }

        return $response;
    }
    
    function discussion_replies_mark_read(Request $request)
    {
        $input = $request->all();
        $new_comment_ids = array();
        $detail_id = $input['discussion_id'];
        $user_id = $input['user_id'];
        $replyDiscussions = $input['discussion_reply_ids'];
        
         foreach ($replyDiscussions as $reply)
            {
                $count = 0;
                $read_comments_ids = UserReadComments::where("discussion_id",$detail_id)->where("user_id",$user_id)->where('comment_id',$reply)->first();
                if($read_comments_ids)
                {
                   $read_comments_ids = $read_comments_ids->toArray(); 
                   $count = count($read_comments_ids);
                }
                else {
                    
                  $count = 0;  
                }
                
                if($count == 0)
                {
                $new_comment_ids[] = ["comment_id"=> $reply,"discussion_id"=>$detail_id,"user_id"=>$user_id];
                }
                
            }
            if(!empty($new_comment_ids))
            {
                UserReadComments::insert($new_comment_ids);
            }
            
            return response()->json(['success' => true ]);
        
        
    }

    function downloadDiscussionDocument(Request $request) {
        $input = $request->all();
        $user_id = $input['user_id'];
        $account_id = $input['account_id'];
        $id = $input['document_id'];
        $documentDownloader = new \App\Services\DocumentDownloader();
        $document = $documentDownloader->getDiscussionDocument($id, $account_id, $user_id, $this->site_id);
        if (!$document) {
            return response()->json(['error' => 'File Not Found']);
        } elseif (isset($document['error']) && $document['error'] == '1') {
            return response()->json($document);
        }
        // return response()->download($document);

        header('Location:' . $document);
        exit;
    }

}
