<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\CodeProfiler;

use App\Services\Emails\Email;
use App\Services\HmhLicense;
use App\Services\HelperFunctions;

use App\Models\Hmh_order;
use App\Models\Hmh_order_license;
use App\Models\User;

class LicensingController extends Controller
{
    protected $site_id;
    protected $cp;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->site_id = $request->header('site_id');
    }

    function create_order(Request $request)
    {
        $valid = $this->validate_hmh_access();
        if($valid!==true){
            return response()->json($valid,403);
        }
        $input = $request->all();
        if(!$this->authenticate()){
            $response = ["success"=>false, "message"=>"Unauthorized Request!"];
            $response_code = 401;
            $log_id = $this->log_request(true, $input, $response, $response_code);
            $response ["log_id"] = $log_id;
            return response()->json($response, $response_code);
        }
        // else {
        //     $log_id = $this->log_request(true, $input, ["message"=>"processing..."], 200);
        // }
/**
 * Following code is disabled till HMH is done with their side of preparation for API calls.
 */
/*
        $error = $this->validate_payload($input);
        if(!empty($error)){
            $response = ["success"=>false, "message"=>$error];
            $response_code = 200;
            $log_id = $this->log_request(true, $input, $response, $response_code);
            $response ["log_id"] = $log_id;
            return response()->json($response, $response_code);
        }
        $message = $this->process_orders($input['sap_type'], $input['orders']);
        $response = ["success"=>true, "message"=>$message];
        $response_code = 200;
        return response()->json($response, $response_code);
*/
        $orders [] = $input;
        // $message = $this->process_orders($orders);
        // $response = ["success"=>true, "message"=>$message];
        try {
            $response = $this->process_orders($orders);
            $response_code = 200;
            $response ["log_id"] = $this->log_request(false, $input, $response, $response_code);
            return response()->json($response, $response_code);
        } catch (\Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
            $response = ["success"=>false, "message"=>"Can't create order due to internal ERROR at API side."];
            $response_code = 200;
            $log_id = $this->log_request(true, $input, $response, $response_code);
            $response ["log_id"] = $log_id;
            return response()->json($response, $response_code);
        }
    }

    function log_request($error, $request, $response, $response_code){
        $log = [
            'url' => app('url')->full(),
            'error' => $error,
            'request_object' => json_encode($request),
            'response_object' => json_encode($response),
            'response_code' => $response_code,
            'created_at' => date("Y-m-d H:i:s"),
            'ip' => HelperFunctions::getUserIP(),
        ];
        return \DB::table('hmh_orders_request_logs')->insertGetId($log);
    }

    /**
     * A basic Authentication using headers
     * curl -i -H "authorization: Basic am9uYXM6Zm9vYmFy" -H "Content-Type: application/json" -H "Accept: application/json" -X POST -d '{"person":{"name":"bob"}}' https://q3api.sibme.com/create_order
     * 
     * => In PHP =>
     * Authorization: Basic dXNlcjpwYXNzd29yZA==
     * $headers = array(
     * 'Content-Type:application/json',
     * 'Authorization: Basic '. base64_encode("user:password") // <---
     * );
     * curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
     */
    function authenticate(){
        // Status flag:
        $LoginSuccessful = false;
        $http_auth = false;
        if(isset($_SERVER['REDIRECT_HTTP_AUTHORIZATION'])){
            $http_auth = $_SERVER['REDIRECT_HTTP_AUTHORIZATION'];
        }elseif(isset($_SERVER['HTTP_AUTHORIZATION'])){
            $http_auth = $_SERVER['HTTP_AUTHORIZATION'];
        }

        // Check username and password:
        if (!empty($http_auth)){
            list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':', base64_decode(substr($http_auth, 6)));
            $Username = $_SERVER['PHP_AUTH_USER'];
            $Password = $_SERVER['PHP_AUTH_PW'];
        
            if ($Username == config('apikeys.hmh_licensing_user') && $Password == config('apikeys.hmh_licensing_pass')){
                $LoginSuccessful = true;
            }
        }
        
        return $LoginSuccessful;
    }

/*
// This function was validating when Orders were coming from API
    function validate_payload($input){
        $error = false;
        if(!isset($input['sap_type']) || !in_array($input['sap_type'], [1,2])){
            $error="invalid sap type";
        }
        if(!isset($input['orders']) || !is_array($input['orders']) || empty($input['orders'])){
            $error="no orders found";
        }
        return $error;
    }
*/
    function process_orders($orders) {
        // $message = HmhLicense::process_orders($orders);
        // return $message;
        return HmhLicense::process_orders($orders);
    }

    function get_orders(Request $request){
        $valid = $this->validate_hmh_access();
        if($valid!==true){
            return response()->json($valid,403);
        }
        $input = $request->all();
        if(!isset($input['admin_key']) || empty($input['admin_key']) || !HmhLicense::validate_admin($input['admin_key']) ){
            return response()->json(["success"=>false, "message"=>"Unauthorized Request!"], 401);
        }
        $admin_key = $input['admin_key'];
        // $sap_type = (isset($input['sap_type']) && in_array($input['sap_type'], ['1','2'])) ? $input['sap_type'] : '1';

        // Call Services of HmhLicense
        $orders = HmhLicense::get_orders($admin_key);

        if($orders->isNotEmpty()){
            return response()->json($orders);
        } else {
            return response()->json(["success"=>false, "message"=>"No Orders Found!"]);
        }
    }

    function send_invitations(Request $request){
        $this->cp = App(\App\Services\CodeProfiler::class);
        // $this->cp->p_open('validations');
        // $this->cp->p_close('validations');
        // dd($this->cp->p_dump());
        $valid = $this->validate_hmh_access();
        if($valid!==true){
            return response()->json($valid,403);
        }
        $input = $request->all();
        if(!isset($input['admin_key']) || empty($input['admin_key'])){
            return response()->json(["success"=>false, "message"=>"Unauthorized Request!"], 401);
        }
        if(!isset($input['order_id']) || empty($input['order_id'])){
            return response()->json(["success"=>false, "message"=>"Order ID not provided!"], 200);
        }
        if(!isset($input['licenses']) || empty($input['licenses'])){
            return response()->json(["success"=>false, "message"=>"Licenses not provided!"], 200);
        }
        // $this->cp->p_open('send_invitations');
        $owner = $this->get_hmh_owner($input['order_id']);
        if(!$owner){
            return response()->json(["success"=>false, "message"=>"Account owner not found!"], 200);
        }

        // Check if License already exist
        $check_emails = [];
        foreach($input["licenses"] as $license){
            if(in_array($license["email"], $check_emails)){
                return response()->json(["success"=>false, "message"=>"Same Email Provided multiple times: \n".$license["email"]], 200);
                exit;
            }
            $check_emails [] = $license["email"];
        }
        
        $email_exists = User::isEmailExistInAccount($owner['account_id'], $check_emails);
        if(!empty($email_exists)){
            return response()->json(["success"=>false, "message"=>"Email already exist!"], 200);
            exit;
        }

        foreach ($input['licenses'] as $license) {
            // $user_controller = new UsersController;
            // $user_controller->is_from_hmh_licensing_admin=true;

            // $this->cp->p_open('add_new_user');
            $user_controller = app("App\Http\Controllers\UsersController");
            $user_controller->is_from_hmh_licensing_admin=true;
            $user_controller->hmh_license_id=$license['id'];

            $user_id = $user_controller->add_new_user($owner['account_id'], $owner['current_user_id'], $license['email'], $license['first_name'], $license['last_name'], $license['role'], true, $owner['current_first_name'], $owner['current_last_name']);
            // $this->cp->p_close('add_new_user');            
            // $user_id = app("App\Http\Controllers\UsersController")->add_new_user($owner['account_id'], $owner['current_user_id'], $license['email'], $license['first_name'], $license['last_name'], $license['role'], true, $owner['current_first_name'], $owner['current_last_name']);
            if(empty($user_id)){
                continue;
            }
            if(!empty($user_id)){
                $license['user_id'] = $user_id;
            }
            $license['is_used'] = 1;
            $license['is_invited'] = 1;
            $license['updated_at'] = date("Y-m-d H:i:s");

            \App\Models\Hmh_order_license::where("id", $license['id'])->update($license);
        }
        // $this->cp->p_close('send_invitations');
        // dd($this->cp->p_dump());
        return response()->json(["success"=>true, "message"=>"Invitations Sent Successfully!"]);
    }

    function resend_invitation(Request $request){
        $valid = $this->validate_hmh_access();
        if($valid!==true){
            return response()->json($valid,403);
        }
        $input = $request->all();
        if(!isset($input['admin_key']) || empty($input['admin_key'])){
            return response()->json(["success"=>false, "message"=>"Unauthorized Request!"], 401);
        }

        if(!isset($input['license_id']) || empty($input['license_id']) || !isset($input['order_id']) || empty($input['order_id'])){
            return response()->json(["success"=>false, "message"=>"Order ID or License not provided!"], 200);
        }
        
        $objLicense = \App\Models\Hmh_order_license::where("id", $input['license_id'])->first();
        if(!$objLicense){
            return response()->json(["success"=>false, "message"=>"License not found!"], 200);
        }

        $owner = $this->get_hmh_owner($input['order_id']);
        if(!$owner){
            return response()->json(["success"=>false, "message"=>"Account owner not found!"], 200);
        }
        
        $users = User::getUserInformation($owner['account_id'], $objLicense->user_id);
        if ($users) {
            $email_data = array(
                'first_name' => $users->first_name,
                'last_name' => $users->last_name,
                'user_id' => $users->id,
                'email' => $users->email,
                'account_id' => $users->account_id,
                'authentication_token' => $users->authentication_token,
                'message' => '',
                'company_name' => $users->company_name,
            );
            app("App\Http\Controllers\UsersController")->sendInvitation_via_HMH_Licensing($email_data);
        }
        $license['is_used'] = 1;
        $license['is_invited'] = 1;
        $license['updated_at'] = date("Y-m-d H:i:s");
        \App\Models\Hmh_order_license::where("id", $input['license_id'])->update($license);
        return response()->json(["success"=>true, "message"=>"Invitation Sent Successfully!"]);
    }

    function get_hmh_owner($order_id){
        // Get Account ID.
        // $account_id = 3040; // Temporarily Hardcoding ID because of following reason.
        /**
         * Carlos gave us a file to match and get account id for an order.
         * But in account transition he told that all child accounts might be consolidated under one account.
         * That's why we will not need that file to get related account. So for now we can use the main HMH account.
         */

        $order = Hmh_order::find($order_id);
        if(!$order){
            return false;
        }

        $owner = User::getUsersByAccount2($order->account_id, 100,'','','','',$this->site_id);
        if($owner->isEmpty()){
            return false;
        }
        return ['account_id' => $order->account_id, 'current_user_id' => $owner[0]->id, 'current_first_name' => $owner[0]->first_name, 'current_last_name' => $owner[0]->last_name];
    }

    function validate_hmh_access(){
        if( $this->site_id != 2 ){
            return ["success"=>false, "message"=>"This utility is only available for HMH. Make sure to access it from HMH enabled server."];
        } else {
            return true;
        }
    }

}
