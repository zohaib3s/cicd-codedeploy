<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountCommentTag;
use App\Models\AccountFolder;
use App\Models\AccountFolderMetaData;
use App\Models\AccountFolderUser;
use App\Models\AccountMetaData;
use App\Models\AccountTag;
use App\Models\Document;
use App\Models\AccountFolderDocument;
use App\Models\DocumentStandardRating;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\UserActivityLog;
use App\Models\AccountFolderGroup;
use App\Models\Comment;
use App\Models\Group;
use App\Models\UserGroup;
use App\Models\EmailUnsubscribers;
use App\Models\DocumentFiles;
use App\Models\AccountFrameworkSettingPerformanceLevel;
use App\Models\AccountFrameworkSetting;
use App\Models\Transaction;
use App\Models\PerformanceLevelDescription;
use App\Models\AccountStudentPayments;
use App\Models\JobQueue;
use App\Models\AccountFolderdocumentAttachment;
use App\Models\CommentAttachment;
use App\Models\Plans;
use App\Models\Sites;
use App\Services\S3Services;
use DateInterval;
use DatePeriod;
use Datetime;
use GuzzleHttp\TransferStats;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
use Intercom\IntercomClient;
use GuzzleHttp\Exception\ClientException;
use App\Services\SendGridEmailManager;
use App\Services\HelperFunctions;
use App\Services\BraintreeSubscription;
use App\Services\TranslationsManager;
use Aws\Sns\SnsClient;
use App\Services\Emails\Email;

class PaymentController extends Controller {

    protected $httpreq;
    protected $site_id;
    protected $current_lang;
    protected $site_title;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        //
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->current_lang = $this->httpreq->header('current-lang');
        if ($this->site_id == 1) {
            $this->site_title = 'Sibme';
        } else {
            $this->site_title = 'Coaching Studio';
        }
    }
    
    function fetch_account_plan_details_angular(Request $request) {
        $input = $request->all();
        $account_id = $input['account_id'];
        $account = Account::where(array('id' => $account_id))->first();
        $plans = Plans::where(array(
                "id" => $account['plan_id'],
        ))->first();
        $no_of_users = HelperFunctions::get_allowed_users($account_id);
        $allowed_storage = HelperFunctions::get_allowed_storage($account_id);
        
        $subscription = new BraintreeSubscription();
        $result = array();

        if ($account && $account['braintree_customer_id'] != '' && $account['braintree_subscription_id'] != '' && $account['deactive_plan'] != '1') {
            $subscriptions = $subscription->subscriptions_detail($account['braintree_subscription_id']);
            
        }
        
        if($account && $account['braintree_customer_id'] != '' && $account['deactive_plan'] != '1')
        {
            $card_info = $this->_get_card_informaiton($account_id);
            $transaction_detail = $this->fetch_transactions_details($account_id);
        }
        
        if (!empty($subscriptions)) {
            
            $subscriptions = json_decode(json_encode($subscriptions), true);
            
            $nextBillingDate = $subscriptions['nextBillingDate']['date'];
            $billingPeriodStartDate = $subscriptions['billingPeriodStartDate']['date'];
            $billingPeriodEndDate = $subscriptions['billingPeriodEndDate']['date'];
            


            $billingPeriodStartDate = date('F d,Y', strtotime($billingPeriodStartDate));
            $nextBillingDate = date('F d, Y', strtotime($nextBillingDate));
            $billingPeriodEndDate = date('F d,Y', strtotime($billingPeriodEndDate));

            $result = array(
                'billing_start' => $billingPeriodStartDate,
                'next_billing' => $nextBillingDate,
                'billing_end' => $billingPeriodEndDate
            );
            
            
        } 
        else
        {
             $result = array(
                'billing_start' => '',
                'next_billing' => '',
                'billing_end' => ''
            );
            
        }
        
        if(!empty($card_info))
        {
            $result['creditCardNumber'] = '**** ' . $card_info['last4'];
            $result['creditCardImage'] = $card_info['imageUrl'];
        }
        else
        {
            $result['creditCardNumber'] = '';
            $result['creditCardImage'] = ''; 
        }
        
        
        if(!empty($transaction_detail))
        {
           $result['transactions'] =  $transaction_detail;
        }
        else
        {
           $result['transactions'] = array(); 
        }
        
        $result['number_of_users'] = $no_of_users;
        $result['storage'] = $allowed_storage;
        if(!empty($plans))
        {
            $yearly_bool = $plans['per_year'];
            if($yearly_bool)
            {
               if($plans['id'] < 9)
               {
                 $result['cost'] = '$' . $plans['price'] . '.00/yr';
                 $result['user_cost'] = '$' . round($plans['price'] / $plans['users']) . ' yr/user';
               }
               else
               {
                 $result['cost'] = '$' . ( $plans['price_yearly'] * $no_of_users ) . '.00/yr';
                 $result['user_cost'] = '$' . ($plans['price_yearly'] ) . ' yr/user';
               }
               
               
               $result['plan_name'] =   $plans['plan_type']; 
            }
            else
            {
               if($plans['id'] < 9)
               {
                 $result['cost'] = '$' . $plans['price'] . '.00/mo';
                 $result['user_cost'] = '$' . round($plans['price'] / $plans['users']) . ' mo/user';
               }
               else
               {
                 $result['cost'] = '$' . $plans['price'] * $no_of_users . '.00/mo';
                 $result['user_cost'] = '$' . $plans['price'] . ' mo/user';
               }
               
               $result['plan_name'] =  $plans['plan_type'];
            }
            
        }
        else if($account['in_trial'] == '1')
        {  
           $result['cost'] = '';
           $result['user_cost'] = '';
           $result['plan_name'] = 'Trial Account';
        }
        else
        {
           $result['cost'] = '';
           $result['user_cost'] = ''; 
        }
        
    $today_date = date('Y-m-d');
    $time = strtotime($today_date);
    
        
    if(isset($input['selected_plan_id']) && isset($input['selected_number_of_users']))
    {
      $selected_plans = Plans::where(array("id" => $input['selected_plan_id']))->first();
       if(!empty($selected_plans))
        {
            $yearly_bool = $selected_plans['per_year'];
            if($yearly_bool)
            {
                
                 $result['selected_cost'] = '$' . ( $selected_plans['price_yearly'] * $input['selected_number_of_users'] ) . '.00/yr';
                 $result['selected_plan_price'] = ( $selected_plans['price_yearly'] * $input['selected_number_of_users'] );
                 $result['selected_storage'] =   $selected_plans['storage'] * $input['selected_number_of_users'];
                 $result['selected_user_cost'] = '$' . ( $selected_plans['price_yearly'] ) . ' yr/user';
                 $result['selected_plan_name'] =  $selected_plans['plan_type'];
                 $renew_date =  date("Y-m-d", strtotime("+1 year", $time));
                 //$result['description'] = $result['selected_plan_name'] . ' - ' . 'User: '.$result['selected_user_cost'] . ' - ' . 'Total Users: ' . $input['selected_number_of_users'] . ' - ' . 'Total Storage: ' . $result['selected_storage'].'GB (' . date('m/d/Y') . ' - ' . date('m/d/Y', strtotime($renew_date)) . ')' ;
                 $result['description'] = $result['selected_plan_name'] . ' - ' . 'User: '.$result['selected_user_cost'] . ' - ' . 'Total Users: ' . $input['selected_number_of_users'] . ' - ' . 'Total Storage: ' . $result['selected_storage'].'GB ';
                 if(!empty($subscriptions))
                 {
                     $result['selected_renews_on'] = $nextBillingDate;
                 }
                else {
                    $result['selected_renews_on'] = date('F d, Y', strtotime($renew_date));
                }
                 
            }
            else
            {
                 $result['selected_cost'] = '$' . $selected_plans['price'] * $input['selected_number_of_users'] . '.00/mo';
                 $result['selected_plan_price'] = $selected_plans['price'] * $input['selected_number_of_users'];
                 $result['selected_storage'] =   $selected_plans['storage'] * $input['selected_number_of_users'];
                 $result['selected_user_cost'] = '$' . $selected_plans['price'] . ' mo/user';
                 $result['selected_plan_name'] =  $selected_plans['plan_type'];
                 $renew_date =  date("Y-m-d", strtotime("+1 month", $time));
                 //$result['description'] = $result['selected_plan_name'] . ' - ' . 'User: '.$result['selected_user_cost'] . ' - ' . 'Total Users: ' . $input['selected_number_of_users'] . ' - ' . 'Total Storage: ' . $result['selected_storage'].'GB (' . date('m/d/Y') . ' - ' . date('m/d/Y', strtotime($renew_date)) . ')';
                 $result['description'] = $result['selected_plan_name'] . ' - ' . 'User: '.$result['selected_user_cost'] . ' - ' . 'Total Users: ' . $input['selected_number_of_users'] . ' - ' . 'Total Storage: ' . $result['selected_storage'].'GB';
                 if(!empty($subscriptions))
                 {
                     $result['selected_renews_on'] = $nextBillingDate;
                 }
                else {
                    $result['selected_renews_on'] = date('F d, Y', strtotime($renew_date));
                }
            }
            
        }
        
    }
        if($account && $account['braintree_customer_id'] != '' && $account['deactive_plan'] != '1')
        {
            $customer_info = $subscription->get_customer_information($account['braintree_customer_id']);
            $customer_details = array(
                'firstName' => $customer_info->firstName,
                'lastName' => $customer_info->lastName,
                'company' => $customer_info->company ,
                'email' => $customer_info->email,
                'locality' => $customer_info->creditCards[0]->billingAddress->locality,
                'region' => $customer_info->creditCards[0]->billingAddress->region,
                'postalCode' => $customer_info->creditCards[0]->billingAddress->postalCode,
                'streetAddress' => $customer_info->creditCards[0]->billingAddress->streetAddress,
                'countryName' => $customer_info->creditCards[0]->billingAddress->countryName,
                'extendedAddress' => $customer_info->creditCards[0]->billingAddress->extendedAddress,
                    
                );
            $result['customer_details'] = $customer_details;
        }
        
        if($account && $account['braintree_subscription_id'] != '' && $account['deactive_plan'] != '1')
        {
             $plans_cycle = Plans::where(array(
                "id" => $account['plan_id']))->first();
             if($plans_cycle && $plans_cycle['per_year'] == '1' )
             {
                $result['plan_cycle'] = 'yearly';
             }
             else
             {
                $result['plan_cycle'] = 'monthly'; 
             }
            $result['number_of_users_in_plan'] = $account['plan_qty'];
        }
        else
        {
            $result['plan_cycle'] = '';
            $result['number_of_users_in_plan'] = '';
        }
        
        
        if(!empty($result))
        {
            return response()->json($result);
        }
        
        else {
            return response()->json($result);
        }
    }
    
    function _get_card_informaiton($account_id) {
        $subscription = new BraintreeSubscription();
        if (isset($account_id) && $account_id != '') {
            $customer_info = array(
                'plan_info' => '',
                'card_info' => ''
            );
            $account = Account::where(array('id' => $account_id))->first();
            if ($account && $account['braintree_customer_id'] != '' && $account['braintree_subscription_id'] != '' && $account['deactive_plan'] != '1') {
                $plan_info = '';
                $credit_card_info = '';

                $customer_info = $subscription->get_customer_information($account['braintree_customer_id']);
                $credit_cards = json_decode(json_encode($customer_info->creditCards), true);
                return $credit_cards[0];
            }
        }
    }
    
    function fetch_transactions_details($account_id) {
        $account = Account::where(array('id' => $account_id))->first();
        $subscription = new BraintreeSubscription();
        $result = array();

        if ($account && $account['braintree_customer_id'] != '') {
            $transactions = $subscription->transactions_detail($account['braintree_customer_id']);
        }
        if (!empty($transactions)) {
            foreach ($transactions as $transaction) {
                $date = (array) $transaction->createdAt;
                $amount = $transaction->amount;
                $status = $transaction->status;
                $id = $transaction->id;
                $credit_card_number = $transaction->creditCard['bin'].'******'.$transaction->creditCard['last4'];
                $card_image_url = $transaction->creditCard['imageUrl'];
                if(!empty($transaction->planId))
                {
                    $plans_cycle = Plans::where(array("id" => $transaction->planId))->first();
                     if($plans_cycle && $plans_cycle['per_year'] == '1' )
                     {
                        $billing_cycle = '1 year';
                     }
                     else
                     {
                        $billing_cycle = '1 month'; 
                     }
                }
                else
                {
                    $billing_cycle = '';
                }
                $trans_date = strtotime($date['date']);
                $trans_date = date('F d, Y', $trans_date);

                $transaction_detail = Transaction::where(array('transaction_id' => $id ))->first();
                if($transaction_detail)
                {
                    $trans_desc = $transaction_detail['description'];
                }
                else
                {
                    $trans_desc = '';
                }
                $result[] = array(
                    'amount' => $amount,
                    'date' => $trans_date,
                    'id' => $id,
                    'status' => $status,
                    'description' => $trans_desc,
                    'billing_cycle' => $billing_cycle,
                    'credit_card_number' => $credit_card_number,
                    'card_image_url' => $card_image_url,
                    'customer_name' => $transaction->customer['firstName'] . ' ' . $transaction->customer['lastName'] ,
                    'company' => $transaction->customer['company']
                );
              
            }
        }
        return $result;
    }
    
    
    function purchase_new_plan(Request $request)
    {
        $input = $request->all();
        $subscription = new BraintreeSubscription();
        $account_id = $input['account_id'];
        $account = Account::where(array('id' => $account_id))->first();
        $braintree_subscription_id = $account['braintree_subscription_id'];
        $customer_id = $account['braintree_customer_id'];
                
              
        $account_exists = Account::where(array('id' => $account_id))->first();
        $user_exists = User::where(array('id' => $input['user_id']))->first();
        $account_owner_auth = UserAccount::where(array('user_id' => $input['user_id'] , 'account_id' => $account_id , 'role_id' => '100' ))->first();
        
        if(empty($account_exists))
        {
            $result = ['success' => false , 'error_message' => 'Invalid AccountID'];
            return response()->json($result);
        }
        
        if(empty($user_exists))
        {
            $result = ['success' => false , 'error_message' => 'Invalid UserID'];
            return response()->json($result);
        }
        
        if($user_exists['authentication_token'] != $input['auth_token'])
        {
            $result = ['success' => false , 'error_message' => 'Not an authenticated user to perform this action.'];
            return response()->json($result); 
        }
        
        if(empty($account_owner_auth))
        {
            $result = ['success' => false , 'error_message' => 'You do not have access to perform this action.'];
            return response()->json($result); 
        }
                    
                if(isset($input['old_card']) && $input['old_card'])
                {
                    $customer = array();   
                }
                
                else {
                    $credit_card_number = $input['card_number'];
                    $expiration_date = $input['card_expiration'];
                    $cvv = $input['card_cvv'];
                    
                    $customer = array(
                               'firstName' => $input['first_name'],
                               'lastName' => $input['last_name'],
                               'company' => $input['company_name'],
                               'email' => $input['email'],
                               'creditCard' => array(
                               'number' => $credit_card_number,
                               'expirationDate' => $expiration_date,
                               'cvv' => $cvv,
                               'billingAddress' => array( 
                               'locality' => $input['city'],
                               'postalCode' => $input['zip_code'],
                               'region' => (isset($input['state'])) ? $input['state'] : ''  ,
                               'streetAddress' => $input['address'],
                               'countryName' => $input['country'] ,
                               'extendedAddress' => (isset($input['extendedAddress'])) ? $input['extendedAddress'] : ''     
                                      )
                               )
                           );  
                    
                    }
                
            $selected_plans = Plans::where(array("id" => $input['plan_id']))->first();
            $yearly_bool = $selected_plans['per_year'];
            if($yearly_bool == '1')
            {
                $monthly_bool = "0";
            }
            else
            {
                $monthly_bool = "1";
            }
            
            
            $current_plans = Plans::where(array("id" => $account['plan_id']))->first();        
              
            if($current_plans && $account['plan_id'] > 8 && !empty($braintree_subscription_id))
            {
                if($monthly_bool == '1' && $current_plans['per_year'] == '0')
                {
                    $subscription_operation = 'update';
                }
               elseif($monthly_bool == '0' && $current_plans['per_year'] == '1')
               {
                    $subscription_operation = 'update';
               }
               elseif($monthly_bool == '0' && $current_plans['per_year'] == '0')
               {
                    $subscription_operation = 'monthlytoyearly';
               }
               else
               {
                   $subscription_operation = 'new';
               }

            }
            else
            {
                $subscription_operation = 'new';
            }
            
            
                
            if($customer_id != NULL)
            {
                $result = $subscription->create_subscription($account['plan_id'],$account['plan_qty'],$subscription_operation,$input['plan_id'], $customer,$input['plan_price'],$monthly_bool,$braintree_subscription_id,$customer_id,$input['description']);
            }
            else{

                $result = $subscription->create_subscription($account['plan_id'],$account['plan_qty'],$subscription_operation,$input['plan_id'], $customer,$input['plan_price'],$monthly_bool,$braintree_subscription_id,'',$input['description']);
            }
            
            
           if($result['success'])
           {
               
              if(isset($input['user_id']))
              {
                if($input['plan_id'] == '11' || $input['plan_id'] == '12')
                {
                    $this->pro_plan_feature_activate($account_id,$input['user_id']);
                }
                else if($input['plan_id'] == '9' || $input['plan_id'] == '10')
                {
                    $this->basic_plan_feature_activate($account_id,$input['user_id']);
                }
              }
                $account_update_data = array(
                            'braintree_customer_id' => $result['braintree_customer_id'] ,
                            'braintree_subscription_id' =>  $result['braintree_subscription_id'] ,
                            'updated_at' =>  date('Y-m-d H:i:s'),
                            'is_active' => TRUE,
                            'suspended_at' => NULL,
                            'in_trial' => '0',
                            'is_suspended' => '0',
                            'plan_id' => $result['braintree_plan_id'],
                            'plan_qty' => $input['plan_quantity']
                        );
                DB::table('accounts')->where(array('id' => $account_id))->update($account_update_data);
                $subscriptions_detail = $subscription->subscriptions_detail($result['braintree_subscription_id']);
                $subscriptions_detail = json_decode(json_encode($subscriptions_detail), true);
                $billingPeriodStartDate = $subscriptions_detail['billingPeriodStartDate']['date'];
                $billingPeriodEndDate = $subscriptions_detail['billingPeriodEndDate']['date'];
                $billingPeriodStartDate = date('Y-m-d', strtotime($billingPeriodStartDate));
                $billingPeriodEndDate = date('Y-m-d', strtotime($billingPeriodEndDate));
                    $subscription_data = array(
                            
                            'braintree_subscription_id' => $result['braintree_subscription_id'],
                            'Subscription_start_date' => $billingPeriodStartDate ,
                            'Subscription_end_date' => $billingPeriodEndDate,
                            'number_of_users' => $input['plan_quantity'],
                            'plan_id' => $result['braintree_plan_id'],
                            'subscription_type' => $result['subscription_type'],
                            'payment_status' => (empty($result['payment_status']))? 'settled' : $result['payment_status'],
                            'Subscription_Status' => $result['subscription_status'],
                            'account_id' => $account_id
                        );
                    
                    DB::table('account_braintree_subscriptions')->insertGetId($subscription_data);
                   $transaction_data = $subscription->find_transaction_detail($result['transaction_id']);
                   
                     $transaction_date = (array) $transaction_data->createdAt;
                     $trans_date = strtotime($transaction_date['date']);
                     $trans_date = date('Y-m-d H:i:s', $trans_date);
                     $trans_date = date('F d,Y', strtotime($trans_date));
                  
                  if($result['subscription_type'] == '1')
                  {
                      $subscription_period = '1 year';
                  }
                 else 
                  {
                      $subscription_period = '1 month';
                  }
                   
                   $transaction_receipt_data = array(
                       'customer_name' => $transaction_data->customer['firstName'] . ' ' . $transaction_data->customer['lastName'] ,
                       'company_name' =>$account['company_name'],
                       'transaction_id' => $transaction_data->id ,
                       'amount' => $transaction_data->amount ,
                       'description' => $input['description'] ,
                       'maskedCreditCardNumber' => 'xxxx ' . $transaction_data->creditCardDetails->last4,
                       'creditCardImageUrl' => $transaction_data->creditCardDetails->imageUrl,
                       'transaction_date' => $trans_date,
                       'subscription_period' => $subscription_period
                       
                   );
                   
                $result['transaction_receipt_data'] = $transaction_receipt_data;
                
           }
                        
                      
           return response()->json($result);              
                        
                  
    }
    
    function pro_plan_feature_activate($account_id,$user_id)
    {
        DB::table("accounts")->where(array('id' => $account_id))->update(array('enable_live_rec' => '1'));
        DB::table("users_accounts")->where(array('account_id' => $account_id))->update(array('live_recording' => '1'));
        DB::table("accounts")->where(array('id' => $account_id))->update(array('transcribe_library_videos' => '1'));
        DB::table("accounts")->where(array('id' => $account_id))->update(array('is_evaluation_activated' => '1'));
        DB::table("accounts")->where(array('id' => $account_id))->update(array('enable_goals' => '1'));
        app('App\Http\Controllers\AccountsController')->update_account_meta_data_params($account_id,$user_id,'1','transcribe_library_videos');
        app('App\Http\Controllers\AccountsController')->update_account_meta_data_params($account_id,$user_id,'1','enable_video_library');
        app('App\Http\Controllers\AccountsController')->update_account_meta_data_params($account_id,$user_id,'1','enable_matric');
        app('App\Http\Controllers\AccountsController')->update_account_meta_data_params($account_id,$user_id,'1','enable_tracker');
        app('App\Http\Controllers\AccountsController')->update_account_meta_data_params($account_id,$user_id,'1','enableanalytics');
        app('App\Http\Controllers\AccountsController')->update_account_meta_data_params($account_id,$user_id,'1','enabletracking');
        app('App\Http\Controllers\AccountsController')->update_account_folders_meta_data_params($account_id,$user_id,'1','enable_tags');
        app('App\Http\Controllers\AccountsController')->update_account_folders_meta_data_params($account_id,$user_id,'1','enable_tags_ws');
        app('App\Http\Controllers\AccountsController')->update_account_folders_meta_data_params($account_id,$user_id,'1','enable_framework_standard');
        app('App\Http\Controllers\AccountsController')->update_account_folders_meta_data_params($account_id,$user_id,'1','enable_framework_standard_ws');
    }
    
    function basic_plan_feature_activate($account_id,$user_id)
    {
        DB::table("accounts")->where(array('id' => $account_id))->update(array('enable_live_rec' => '0'));
        DB::table("users_accounts")->where(array('account_id' => $account_id))->update(array('live_recording' => '0'));
        DB::table("accounts")->where(array('id' => $account_id))->update(array('transcribe_library_videos' => '0'));
        app('App\Http\Controllers\AccountsController')->update_account_meta_data_params($account_id,$user_id,'0','enable_video_library');
        app('App\Http\Controllers\AccountsController')->update_account_meta_data_params($account_id,$user_id,'0','transcribe_library_videos');
        app('App\Http\Controllers\AccountsController')->update_account_meta_data_params($account_id,$user_id,'0','enable_matric');
        app('App\Http\Controllers\AccountsController')->update_account_meta_data_params($account_id,$user_id,'0','enable_tracker');
        app('App\Http\Controllers\AccountsController')->update_account_meta_data_params($account_id,$user_id,'0','enableanalytics');
        app('App\Http\Controllers\AccountsController')->update_account_folders_meta_data_params($account_id,$user_id,'1','enable_tags');
        app('App\Http\Controllers\AccountsController')->update_account_folders_meta_data_params($account_id,$user_id,'1','enable_tags_ws');
        app('App\Http\Controllers\AccountsController')->update_account_folders_meta_data_params($account_id,$user_id,'0','enable_framework_standard');
        app('App\Http\Controllers\AccountsController')->update_account_folders_meta_data_params($account_id,$user_id,'0','enable_framework_standard_ws');
    }
    
    function update_customer_info(Request $request)
    {
        $input = $request->all();
        $subscription = new BraintreeSubscription();
        $account_id = $input['account_id'];
        $account = Account::where(array('id' => $account_id))->first();
        $credit_card_number = $input['card_number'];
        $expiration_date = $input['card_expiration'];
        $cvv = $input['card_cvv'];
        $customer_id = $account['braintree_customer_id'];
        $customer = array(
                   'firstName' => $input['first_name'],
                   'lastName' => $input['last_name'],
                   'company' => $input['company_name'],
                   'email' => $input['email'],
                   'creditCard' => array(
                   'number' => $credit_card_number,
                   'expirationDate' => $expiration_date,
                   'cvv' => $cvv,
                   'billingAddress' => array( 
                   'locality' => $input['city'],
                   'postalCode' => $input['zip_code'],
                   'region' => $input['state'] ,
                   'streetAddress' => $input['address'],
                   'countryName' => $input['country'],
                   'extendedAddress' => (isset($input['extendedAddress'])) ? $input['extendedAddress'] : '' 
                          )
                   )
               );
           $result = $subscription->update_customer_credit_card_info($customer,$customer_id);
            if($result->success)
            {
                //$card_info = $this->_get_card_informaiton($account_id);
                $card_info = array();
                $card_info['success'] = true;
                $card_info['message'] = 'Card information is updated successfully.';
                return response()->json($card_info);  
                
            }
            
            else
            {
                $card_info = array();
                $card_info['success'] = false;
                $card_info['message'] = $result->message;
                return response()->json($card_info);
            }
        
    }
    
    function cron_for_checking_card_failures()
    {
        $results = app('db')->select("SELECT id FROM accounts where site_id = 1");
        foreach($results as $result)
        {
            echo 'Starting Account ID : ' . $result->id . '<br>';
            $this->credit_card_failure_email($result->id);
        }
        $final_array['success'] = true;
        $final_array['message'] = 'Cron Run Successfully.';
        return response()->json($final_array);
    }
    
    
    function credit_card_failure_email($account_id)
    {
     
      $account_detail = Account::where(array('id' => $account_id))->first();
      $account_owner_detail = UserAccount::where(array('account_id' => $account_id , 'role_id' => 100 ))->first();
      if($account_owner_detail)
      {
          $account_owner_email_detail = User::where(array('id' => $account_owner_detail['user_id']))->first();
          if($account_owner_email_detail)
          {
              $account_owner_email = $account_owner_email_detail['email'];
          }
          else 
          {
              $account_owner_email = '';  
          }
      }
      else
      {
              $account_owner_email = '';
      }
      $result = $this->check_card_expiration_failure($account_id);
      $subscription = new BraintreeSubscription();
      
      if($result['cardExpired'])
      {
          $subscription_detail = $subscription->subscriptions_detail($account_detail['braintree_subscription_id']);
          $subscription_detail = json_decode(json_encode($subscription_detail), true);
          $nextBillingDate = $subscription_detail['nextBillingDate']['date'];
          $nextBillingDate = date('Y-m-d', strtotime($nextBillingDate));
          $today_date = date('Y-m-d');
          //$today_date = date('Y-m-d', strtotime('2021-06-03'));
          
          $days_difference = $this->dateDiffInDays($nextBillingDate,$today_date);
          $email_subject = 'Update Payment Information';
          $html = "Please update the expiration date for your credit card or change the credit card.";
          $emailData = [
                'from' => Sites::get_site_settings('static_emails', '1')['noreply'],
                'from_name' => Sites::get_site_settings('site_title', '1'),
                'to' => $account_owner_email,
                'subject' => $email_subject,
                'template' => $html,
            ];
          if($days_difference == 21)
          {
              Email::sendCustomEmail($emailData);
          }
          
          if($days_difference == 14)
          {
              Email::sendCustomEmail($emailData);
          }
          
          if($days_difference == 7)
          {
              Email::sendCustomEmail($emailData);
          }
          
          if($days_difference == 1)
          {
              Email::sendCustomEmail($emailData);
          }
          
          
          
      }
      
      
      if($result['cardFailure'])
      {
          $subscription_detail = $subscription->subscriptions_detail($account_detail['braintree_subscription_id']);
          $subscription_detail = json_decode(json_encode($subscription_detail), true);
          $billingPeriodStartDate = $subscription_detail['billingPeriodStartDate']['date'];
          $billingPeriodStartDate = date('Y-m-d', strtotime($billingPeriodStartDate));
          
          $today_date = date('Y-m-d');
          //$today_date = date('Y-m-d', strtotime('2020-06-11'));
          
          $days_difference = $this->dateDiffInDays($billingPeriodStartDate,$today_date);
          $email_subject = 'Update Payment Information';
          $html = "We couldn't process payment for Sibme due to a problem with your payment details. We're happy to try again, but we need your help.<br>";
          $html .= "Please update your credit card or change the credit card.";
          $emailData = [
                'from' => Sites::get_site_settings('static_emails', '1')['noreply'],
                'from_name' => Sites::get_site_settings('site_title', '1'),
                'to' => $account_owner_email,
                'subject' => $email_subject,
                'template' => $html,
            ];
          
          if($days_difference == 21)
          {
              Email::sendCustomEmail($emailData);
          }
          
          if($days_difference == 14)
          {
              Email::sendCustomEmail($emailData);
          }
          
          if($days_difference == 7)
          {
              Email::sendCustomEmail($emailData);
          }
          
          if($days_difference == 1)
          {
              Email::sendCustomEmail($emailData);
          }
          
          if($days_difference == 31)
          {
              $account_update_data = array('is_active' => '0');
              DB::table('users_accounts')->where(array('account_id' => $account_id))->whereRaw('role_id <> 100')->update($account_update_data);
          }
          
          
      }
      
      
      return true;
    }
    
    function check_card_expiration_failure($account_id)
    {
        $account_detail = Account::where(array('id' => $account_id))->first();
        $subscription = new BraintreeSubscription();
        $result = array(
            'cardExpired' => false,
            'cardFailure' => false
        );
        if(!empty($account_detail['braintree_customer_id']) && $account_detail['deactive_plan'] == '0'  )
        {
            
        $customer_info = $subscription->get_customer_information($account_detail['braintree_customer_id']);
          if($customer_info)
          {
                $expires = DateTime::createFromFormat('mY', $customer_info->creditCards[0]->expirationMonth . $customer_info->creditCards[0]->expirationYear );
                $now     = new DateTime();
                if ($expires < $now) {
                    $result['cardExpired'] = true;   
                }
                else
                {
                    $result['cardExpired'] = false; 
                }
          }
            
        }
        
        if(!empty($account_detail['braintree_subscription_id']) && $account_detail['deactive_plan'] == '0'  )
        {
         $subscription_detail = $subscription->subscriptions_detail($account_detail['braintree_subscription_id']);
          if($subscription_detail)
          {
                if( $subscription_detail->status == 'PastDue' )
                {
                    $result['cardFailure'] = true;

                }
                else
                {
                    $result['cardFailure'] = false;
                }
          }
            
            
        }
        
        
        return $result;
        
        
    }
    
    function dateDiffInDays($date1, $date2)  
    { 
        $diff = strtotime($date2) - strtotime($date1); 
        return abs(round($diff / 86400)); 
    } 



}
