<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\AccountCommentTag;
use App\Models\AccountFolder;
use App\Models\AccountFolderDocument;
use App\Models\AccountFolderGroup;
use App\Models\AccountFolderMetaData;
use App\Models\AccountFolderUser;
use App\Models\AccountFrameworkSetting;
use App\Models\AccountFrameworkSettingPerformanceLevel;
use App\Models\AccountFrameworkSettings;
use App\Models\AccountMetaData;
use App\Models\AccountTag;
use App\Models\Comment;
use App\Models\Document;
use App\Models\DocumentStandardRating;
use App\Models\EmailUnsubscribers;
use App\Models\Group;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\UserActivityLog;
use App\Models\UserGroup;
use App\Models\Sites;
use App\Models\JobQueue;
use DateInterval;
use DatePeriod;
use Datetime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Intercom\IntercomClient;
use GuzzleHttp\Exception\ClientException;
use App\Services\SendGridEmailManager;
use App\Services\HelperFunctions;
use App\Services\TranslationsManager;
use App\Services\Emails\Email;

class VideoLibraryController extends Controller {

    public $httpreq;
    public $site_id;
    public $current_lang;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        //
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->current_lang = $this->httpreq->header('current-lang');
    }
    
    public function getLibraryData(Request $request)
    {
        $language_based_content = TranslationsManager::get_page_lang_based_content("VideoLibrary", $this->site_id, '', $this->current_lang);
        $account_id = $request->get('account_id');
        $user_id = $request->get('user_id');
        $user_current_account = $request->get('user_current_account');
        $users = self::ConvertToArray($user_current_account);
        $subject_id = $request->get('subject_id');
        $title = $request->get('title');
        $order = $request->has('order_by') ? $request->get('order_by') : "n";
        $except_categories = $request->has('except_categories') ? $request->get('except_categories'): "";
        if(is_array($except_categories))
        {
            $except_categories = implode(",",$except_categories);
        }
        $selected_domain = $request->has('domain') ? $request->get('domain') : false;
        $page = $request->has('page') ? $request->get('page'):0;
        $parent_library_check = HelperFunctions::get_show_parent_video_library($users['accounts']['account_id']);

        $account_details = Account::where('id', $users['accounts']['account_id'])->first();

        $final_array = [];
        $final_array["selected_account"] = $account_id;
        $final_array["video_upload_button"] = true;
        
        if($parent_library_check && $account_details->parent_account_id != 0)
        {
            $account_id = $account_details->parent_account_id;
            //$final_array["selected_account"] = '';
            $final_array["video_upload_button"] = false;
        }


        $account_info = Account::getMyAccounts($account_id);
        $final_array["account_info"] = $account_info;

        $have_parent = true;//Account::haveParent($users['accounts']['account_id']);
        if($parent_library_check)
        {
            $have_parent = false;
        }
        if ($have_parent) {
            $accounts = Account::getRelatedAccounts($users['accounts']['account_id'], $this->site_id, true);
        } else {
            $accounts = Account::getRelatedAccounts($users['accounts']['account_id'], $this->site_id);
        }
        $final_array["have_parent"] = $have_parent;
        $final_array["user_accounts"] = $accounts;
        
        if($parent_library_check)
        {
            $final_array["user_accounts"][] = $account_info;
        }

        $accounts_ids_arr = array();
        if (!empty($accounts)):
            foreach ($accounts as $account):
                array_push($accounts_ids_arr, $account->id);
            endforeach;
        endif;
        if (!empty($account_id) && $account_id != $users['accounts']['account_id']) {
            if (!in_array($account_id, $accounts_ids_arr) && !$parent_library_check) {
                return response()->json(["status"=>false,"message"=>"No Permission"]);
            }
        }
        
        $original_account = true;
        if($account_id != $users['accounts']['account_id'] )
        {
            $original_account = false;
        }
        
        $final_array["account_id"] = $account_id;
        $final_array["accounts_ids_arr"] = $accounts_ids_arr;
        $uncat_videos = AccountFolder::getVideoLibraryCount('uncat', $account_id, $this->site_id, null, false, "", $title);
        $final_array["uncat_videos"] = $uncat_videos;
        $domain = "all";
        if(!empty($subject_id) || !empty($except_categories))
        {
            $domain = "subjects";
        }
        else if($selected_domain == "uncategorized")
        {
            $domain = "uncat";
        }
        $total_videos = AccountFolder::getVideoLibraryCount($domain, $account_id, $this->site_id, $subject_id, ($selected_domain == "uncategorized"), $except_categories, $title);
        $final_array["total_videos"] = $total_videos;

        $rows = 12;
        if($subject_id !='' )
        {
            $get_videos = AccountFolder::getSubjectTopicVideos($account_id, $subject_id,'subject' , $order, $page, $rows, '-1', $this->site_id, ($selected_domain == "uncategorized"), $except_categories, $title);
        }
        else if ($selected_domain == "all")
        {
            $get_videos = AccountFolder::getSubjectTopicVideos($account_id, '-1','all', $order, $page, $rows, '-1', $this->site_id, false, "", $title);
        }
        else if($selected_domain == "uncategorized")
        {
            $get_videos = AccountFolder::getSubjectTopicVideos($account_id, '-1','uncat' , $order, $page, $rows, '-1', $this->site_id, false, "", $title);
        }

        //unset($get_videos['total_rec']);      
        $all_videos = array();
        if($get_videos){
            $i = 0;
            foreach($get_videos['videos'] as $row){
                $data = app('App\Http\Controllers\VideoController')->get_document_url($row);                
                $row['thubnail_url'] = $data['thumbnail'];
                $row['static_url'] = $data['url'];
                if(isset($data['thumbnail_type']))
                {
                    $row['thumbnail_type']   = $data['thumbnail_type'];
                }
                else
                {
                    $row['thumbnail_type']   = '';
                }
                $activity = UserActivityLog::where("ref_id", $row['document_id'])->where("type",11)->where("site_id", $this->site_id)->where("account_id", $account_id)->orderBy("date_added", "DESC")->first();
                $row['created_date_string'] = $activity ? self::dateToString2($activity->date_added,$this->current_lang) : self::dateToString2($row['created_date'],$this->current_lang);
                $row['total_comments'] = HelperFunctions::get_video_comment_numbers($row['document_id'], $row['id'], $user_id);
                $row['total_attachment'] = count(Document::getVideoDocumentsByVideo($row['document_id'], $row['id'], $user_id, 1, 1, false, $this->site_id,1));
                if (empty($row["video_duration"])) {
                    $result = app('App\Http\Controllers\HuddleController')->getVideoDuration($row['document_id'],true);
                    if ($result["status"]) {
                        $row['video_duration'] = $result["data"];
                    }
                }               
                
                $row['title'] =  htmlspecialchars_decode($row['title']);
                $all_videos[$i] = $row;
                $i++;
            }
            $get_videos['videos'] = $all_videos;
        }
        $final_array["get_videos"] = $all_videos;
        $final_array["rows"] = $rows;

        /*$subject_videos = AccountFolder::getVideoLibraryCount('subjects', $account_id, $this->site_id);
        $final_array["subject_videos"] = $subject_videos;

        $unique_subjects = AccountFolder::getVideoLibraryCount('unique_subjects', $account_id, $this->site_id);
        $final_array["unique_subjects"] = $unique_subjects;

        $unique_topics = AccountFolder::getVideoLibraryCount('unique_topics', $account_id, $this->site_id);
        $final_array["unique_topics"] = $unique_topics;*/

        $subjects = AccountFolder::getSubjects($account_id, $this->site_id);
        $final_array["subjects"] = $subjects;

       /* $total_topics = AccountFolder::getVideoLibraryCount('topics', $account_id, $this->site_id);
        $final_array["total_topics"] = $total_topics;

        $topics = AccountFolder::getTopics($account_id, $this->site_id);
        $final_array["topics"] = $topics;*/

        //$huddle_permission = $this->Session->read('user_huddle_level_permissions');
        $final_array["huddle_permission"] = true;//$huddle_permission;

        $final_array["user_current_account"] = $users;
        $user_permissions = UserAccount::where(array('user_id' => $user_id, 'account_id' => $account_id, 'site_id' => $this->site_id))->first();
        if ($user_permissions) {
            $user_permissions = $user_permissions->toArray();
        }
        $final_array["user_permissions"] = $user_permissions;

        $final_array["language_based_content"] = $language_based_content;
        $final_array['original_account'] = $original_account;
        
        $user_permissions_library = AccountMetaData::where(array('meta_data_name' => 'enable_video_library' , 'account_id' => $account_id))->first();
        
        if($user_permissions_library['meta_data_value'] == '0' )
        {
            return response()->json(["status" => false,"data"=>$final_array, "message" => "Success"]);
        }

        return response()->json(["status" => true,"data"=>$final_array, "message" => "Success"]);
        
    }

    public function get_video_categories(Request $request, $internal = 0)
    {
        $account_folder_id = $request->get("account_folder_id");
        $account_id = $request->get("account_id");
        $subjects = AccountFolder::getSubjects($account_id, $this->site_id, $account_folder_id);
        if($internal)
        {
            return $subjects;
        }
        return response()->json(["status"=>true, "message"=> "done", "data"=>$subjects]);
    }

    public function get_categories(Request $request)
    {
        $account_id = $request->get("account_id");
        $uncat_counts = $request->get("uncat_counts");
        $subjects = AccountFolder::getSubjects($account_id, $this->site_id);
        $account_folder_id = $request->get("account_folder_id");
        $video_categories = [];
        if(!empty($account_folder_id))
        {
            $video_categories = $this->get_video_categories($request, 1);
        }
        $uncat_videos = [];
        if($uncat_counts)
        {
            $uncat_videos = AccountFolder::getVideoLibraryCount('uncat', $account_id, $this->site_id, null, false, "", "");
        }
        return response()->json(["status"=>true, "message"=> "done", "data"=>$subjects, "uncat_counts"=>$uncat_videos, "video_categories"=>$video_categories]);
    }

    public function change_video_categories(Request $request)
    {
        $account_folder_id = $request->get("account_folder_id");
        $document_id = $request->get("document_id");
        $user_id = $request->get("user_id");
        $account_id = $request->get("account_id");
        $categories = $request->get("categories");
        if(!is_array($categories))
        {
            $categories = explode(",", $categories);
        }
        DB::table("account_folder_subjects")->where("account_folder_id", $account_folder_id)->delete();
        AccountFolder::where("account_folder_id",$account_folder_id)->update(["last_edit_date" => date('Y-m-d H:i:s')]);
        if(!empty($categories))
        {
            $data = [];
            foreach ($categories as $category_id)
            {
                $data = array(
                    'subject_id' => $category_id,
                    'account_folder_id' => $account_folder_id,
                    'created_date' => date("Y-m-d H:i:s"),
                    'last_edit_date' => date("Y-m-d H:i:s"),
                    'created_by' => $user_id,
                    'last_edit_by' => $user_id
                );
               $query_for_check = "Select * from account_folder_subjects where subject_id = $category_id and account_folder_id = $account_folder_id  " ;
               $query_for_check_result = \DB::select($query_for_check);
               if(empty($query_for_check_result))
               {
                    DB::table("account_folder_subjects")->insert($data);
               }
            }
//            if(!empty($data))
//            {
//                DB::table("account_folder_subjects")->insert($data);
//            }
        }
        $subjects = AccountFolder::getSubjects($account_id, $this->site_id);
        $uncat_videos = AccountFolder::getVideoLibraryCount('uncat', $account_id, $this->site_id, null, false, "", "");
        $event = [
            'channel' => 'library-'.$account_id,
            'event' => "video_category",
            'data' => $categories,
            'sidebar_categories' => $subjects,
            'uncat_videos' => $uncat_videos,
            'user_id' => $user_id,
            'item_id' => $document_id,
            'video_id' => $document_id,
            'account_folder_id' => $account_folder_id,
        ];
        HelperFunctions::broadcastEvent($event);
        return response()->json(["status"=>true, "message"=> "Video categories updated successfully!"]);
    }

    public function change_video_description(Request $request)
    {
        $account_folder_id = $request->get("account_folder_id");
        $document_id = $request->get("document_id");
        $user_id = $request->get("user_id");
        $account_id = $request->get("account_id");
        $description = $request->get("description");
        AccountFolder::where("account_folder_id", $account_folder_id)->update(["desc"=>$description, "last_edit_date"=>date("Y-m-d H:i:s"), "last_edit_by"=> $user_id]);
        AccountFolderDocument::where("account_folder_id", $account_folder_id)->where("document_id", $document_id)->update(["desc"=>$description]);
        $event = [
            'channel' => 'library-'.$account_id,
            'event' => "video_description",
            'data' => $description,
            'user_id' => $user_id,
            'item_id' => $document_id,
            'video_id' => $document_id,
            'account_folder_id' => $account_folder_id,
        ];
        HelperFunctions::broadcastEvent($event);
        return response()->json(["status"=>true, "message"=> "Video description updated successfully!"]);
    }

   public function add_categories(Request $request){
      $categories = $request->get('categories');
      $deleted_categories = $request->get('deleted_categories');
      $account_id = $request->get('account_id');
      $user_id = $request->get('user_id');
      $new_categories = [];
      $updated_categories = [];
      if(count($categories) > 0){
        foreach($categories as $row){
            if($row['subject_id'] < 0){
                $parent_categories = array(
                    'name'=>$row['name'],
                    'created_by'=>$user_id,
                    'account_id'=>$account_id,
                    'created_date' => date('y-m-d H:i:s', time()),
                    'last_edit_date' => date('y-m-d H:i:s', time()),
                    'last_edit_by' => $user_id,
                    'site_id' => $this->site_id
                );
                $parent_cat_id = DB::table('subjects')->insertGetId($parent_categories);
                $child_items = $row['childs'];
                $new_childs = [];
                if(count($child_items)>0){
                    foreach($child_items as $child_row){
                        $child_categories = array(
                            'name'=>$child_row['name'],
                            'created_by'=>$user_id,
                            'account_id'=>$account_id,
                            'parent_id'=> $parent_cat_id,
                            'created_date' => date('y-m-d H:i:s', time()),
                            'last_edit_date' => date('y-m-d H:i:s', time()),
                            'last_edit_by' => $user_id,
                            'site_id' => $this->site_id
                        );
                         $child_cat_id = DB::table('subjects')->insertGetId($child_categories);
                         $new_childs[] = ["name"=>$child_row['name'],"count"=>0,"subject_id"=>$child_cat_id];
                    }
                }
                $new_categories[] = ["name"=>$row['name'],"count"=>0,"subject_id"=>$parent_cat_id, "childs"=>$new_childs];
            }else{
                $parent_categories = array(
                    'name'=>$row['name'],
                    'last_edit_date' => date('y-m-d H:i:s', time()),
                    'last_edit_by' => $user_id,
                );
                DB::table('subjects')->where(['id' => $row['subject_id']])->update($parent_categories);
                $child_items = $row['childs'];
                $parent_id = $row['subject_id'];
                $updated_childs = [];
                if(count($child_items)>0){
                    foreach($child_items as $child_row){
                        if($child_row['subject_id'] < 0){
                            $child_categories = array(
                                'name'=>$child_row['name'],
                                'created_by'=>$user_id,
                                'account_id'=>$account_id,
                                'parent_id'=> $parent_id,
                                'created_date' => date('y-m-d H:i:s', time()),
                                'last_edit_date' => date('y-m-d H:i:s', time()),
                                'last_edit_by' => $user_id,
                                'site_id' => $this->site_id
                            );
                             $child_cat_id = DB::table('subjects')->insertGetId($child_categories);
                             $new_categories[] = ["subject_id"=>$child_cat_id, "count"=>0, "parent_subject_id"=>$parent_id,"name"=>$child_row['name']];
                        }else{
                            $child_categories = array(
                                'name'=>$child_row['name'],
                                'last_edit_date' => date('y-m-d H:i:s', time()),
                                'last_edit_by' => $user_id,
                            );
                            DB::table('subjects')->where(['id' => $child_row['subject_id']])->update($child_categories);
                            $updated_childs[] = ["subject_id"=>$child_row['subject_id'],"name"=>$child_row['name']];
                        }
                        
                    }
                }
                $updated_categories[] = ["name"=>$row['name'],"subject_id"=>$row['subject_id'], "childs"=>$updated_childs];
            }
        }

      }
       if(!is_array($deleted_categories))
       {
           $deleted_categories = explode(",",$deleted_categories);
       }

       if(!empty($deleted_categories))
       {
           $child_ids = DB::table('subjects')->whereIn('parent_id', $deleted_categories)->pluck("id")->toArray();
           DB::table('subjects')->whereIn('parent_id', $deleted_categories)->delete();
           DB::table('subjects')->whereIn('id', $deleted_categories)->delete();
           $deleted_categories = array_merge($child_ids,$deleted_categories);
           DB::table('account_folder_subjects')->whereIn('subject_id', $deleted_categories)->delete();
       }
       $socket_categories = [];
       $socket_categories["deleted"] = $deleted_categories;
       $socket_categories["added"] = $new_categories;
       $socket_categories["updated"] = $updated_categories;
       $event = [
           'channel' => 'library-'.$account_id,
           'event' => "categories",
           'sidebar_categories' => AccountFolder::getSubjects($account_id, $this->site_id),
           'uncat_videos' => AccountFolder::getVideoLibraryCount('uncat', $account_id, $this->site_id, null, false, "", ""),
           'data' => $socket_categories,
           'sidebar_categories' => AccountFolder::getSubjects($account_id, $this->site_id),
           'uncat_videos' => AccountFolder::getVideoLibraryCount('uncat', $account_id, $this->site_id, null, false, "", ""),
           'user_id' => $user_id
       ];
       HelperFunctions::broadcastEvent($event);
       return response()->json(["status" => true,"data"=>'', "message" => "Success"]);
   }

   function rename(Request $request){
       $document_id = $request->get('document_id');
       $account_id = $request->get('account_id');
       $user_id = $request->get('user_id');
       $title = $request->get("title");
       if($document_id !=''){
        $document_data = array(
            'title'=>$title,
            'last_edit_date' => date('y-m-d H:i:s', time()),
            'last_edit_by' => $user_id,
        );
        DB::table('account_folder_documents')->where(['id' =>$document_id])->update( $document_data);
        return response()->json(["status" => true,"data"=>'', "message" => "Failed"]);    
       }
   }
    function downloadVideos(Request $request) {
        $input = $request->all();
        $user_id = $input['user_id'];
        $account_id = $input['account_id'];
        $id = $input['document_id'];
        $documentDownloader = new \App\Services\DocumentDownloader();
        $document = $documentDownloader->getDocument($id, $account_id, $user_id, $this->site_id);
        if (!$document) {
            return response()->json(['error' => 'File Not Found']);
        } elseif(isset($document['error']) && $document['error']=='1'){
            return response()->json($document);
        }    
        header('Location:' . $document);
        exit;
    }

    public function uploadLibraryVideo(Request $request)
    {
        $user_current_account = $request->get('user_current_account');
        $users = self::ConvertToArray($user_current_account);
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        $user_email = $users['User']['email'];
        $HuddleController = app('App\Http\Controllers\HuddleController');

        $user_accounts = Account::where('parent_account_id', $account_id)->get();
        $total_childs = count($user_accounts);

        if($total_childs > 0 && false)//SW-3276
        {
            $account_details = Account::where('id', $account_id)->first();
            $data['company_name'] = $account_details->company_name;
            $data['account_id'] = $account_id;
            $data['user_id'] = $user_id;
            $child_names[0] =$account_details->company_name;
            foreach ($user_accounts as $account):
                $child_names[] =    $account->company_name;
            endforeach;

            $data['child_names'] = $child_names;
            $data['video_name'] = $request->get('video_title');
            $data['email'] = $user_email;
            $request->merge(['suppress_render' => false,'suppress_success_email' => true,'workspace' => false]);
            $account_folder_id = $HuddleController->uploadVideos($request);
            $data['huddle_id'] = $account_folder_id;
            if (EmailUnsubscribers::check_subscription($user_id, '7', $account_id, $this->site_id)) {
                $this->sendEmailVideoLibrary($data);
            }

        }
        else
        {
            $request->merge(['suppress_render' => false,'suppress_success_email' => false,'workspace' => false]);
            $HuddleController->uploadVideos($request);
        }

        //Child Accounts

        $user_role_id = $users['roles']['role_id'];
        $video_library_permission = $users['users_accounts']['permission_video_library_upload'];
        if($total_childs > 0 && false)//SW-3276
        {
            foreach ($user_accounts as $account):
                $c_account_id = $account->id;
                $request->merge(['account_id'=>$c_account_id,'suppress_render' => false,'suppress_success_email' => true,'workspace' => false, "no_add_activity_log" => 1]);
                if($user_role_id == 100 || $user_role_id == 110 || $video_library_permission == 1){
                    $HuddleController->uploadVideos($request);
                }
            endforeach;
        }
        return response()->json(["status" => true,"data"=>[], "message" => "Video Uploaded Successfully!"]);
    }

    function sendEmailVideoLibrary($data, $user_id = '') {
        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            return ['error' => 1, 'message' => TranslationsManager::get_translation('Invalid_Email', 'flash_messages', $this->site_id)];
        }

        $lib_user_id = $data['user_id'];
        $users = User::where('id', $lib_user_id)->first();

        $site_title = Sites::get_site_settings('site_title', $this->site_id);
        $from = $site_title . '<' . Sites::get_site_settings('static_emails', $this->site_id)['noreply'] . '>';
        $to = $data['email'];
        $sibme_base_url = config('s3.sibme_base_url');
        $params = array(
            'data' => $data,
        );
        $site_email_subject = Sites::get_site_settings('email_subject', $this->site_id);
        $lang = $users->lang;
        if ($lang == 'en') {
            $subject = $site_email_subject. " - Video Added to Libraries";
        } else {
            $subject = $site_email_subject . " - Video Añadido a Biblioteca";
        }
        $key = "video_library_email_" . $this->site_id . "_" . $lang;

        $result = SendGridEmailManager::get_send_grid_contents($key);
        $html = $result->versions[0]->html_content;

        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{title}', $data['video_name'], $html);
        if (isset($data['child_names']) && count($data['child_names']) > 0) {
            $html = str_replace('{accounts}', implode(", ", $data['child_names']), $html);
        }
        $video_id_detail = AccountFolderDocument::where(array('account_folder_id' => $data['huddle_id']))->first();
        $html = str_replace('{redirect_url}', $sibme_base_url . '/home/library_video/home/' . $data['huddle_id'] . '/' . $video_id_detail->document_id , $html);
        $html = str_replace('{unsubscribe_url}', $sibme_base_url . '/subscription/unsubscirbe_now/' . $users->id . '/7', $html);
        $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);


        $auditEmail = array(
            'account_id' => $data['account_id'],
            'site_id' => $this->site_id,
            'email_from' => $from,
            'email_to' => $users->email,
            'email_subject' => $subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $use_job_queue = config('s3.use_job_queue');

        if ($use_job_queue) {
            \DB::table('audit_emails')->insert($auditEmail);
            JobQueue::add_job_queue($this->site_id, 1, $to, $subject, $html, true);
            return ['error' => 0];
        } else {
            $response = Email::sendEmail($this->site_id, $html, $params, $to, '', $subject);
            if ($response !== true) {
                $auditEmail['error_msg'] = $response;
                \DB::table('audit_emails')->insert($auditEmail);
                return ['error' => 1,
                    'message' => TranslationsManager::get_translation('sorry_your_email_could_not_be_sent_right_now_please_try_again', 'flash_messages', $this->site_id),
                    'exception' => $response];
            } else {
                \DB::table('audit_emails')->insert($auditEmail);
                return ['error' => 0];
            }
        }

        return FALSE;
    }

}
