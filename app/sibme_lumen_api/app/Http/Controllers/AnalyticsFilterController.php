<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Services\DocumentDownloader;
use App\Models\User;
use App\Models\AnalyticsFilter;

use App\Services\HelperFunctions;

class AnalyticsFilterController extends Controller {
    protected $httpreq;
    protected $site_id;
    protected $base;
    protected $current_lang;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->base = config('s3.sibme_base_url');
        $this->current_lang = $this->httpreq->header('current-lang');
        //
    }

    public function save(Request $request) {
        $input = $request->all();

        $custom_messages = [
            'max'    => 'The :attribute should not be greater than :max characters',
        ];
        $filter_added_message = "Filter Added Successfully.";
        if($this->current_lang == 'es'){
            $custom_messages = [
                'max'    => 'El nombre del filtro no debe tener más de :max caracteres.',
                'required'    => 'El campo de :attribute es obligatorio.',
            ];
            $filter_added_message = "Filtro agregado con éxito.";
        }
        $validator = Validator::make($input, [
            'filter_name' => 'required|max:255',
            'subAccount' => 'integer',
            'months' => [
                'required',
                Rule::in(['-1','1', '3', '6', '12']),
            ],
            'search_preference' => [
                'required',
                Rule::in(['month','date']),
            ],
            'start_date' => 'required|date_format:m/d/Y',
            'end_date' => 'required|date_format:m/d/Y',
            'user_current_account' => 'required',
        ], $custom_messages);

        if ($validator->fails()) {
            $messages = $validator->errors();
            return response()->json(['success'=>false, 'messages' => $messages]);
        }
        $user_current_account = $input['user_current_account'];
        // $user_current_account = json_decode($input['user_current_account']);

        $data['filter_name'] = $input['filter_name'];
        $data['subAccount'] = $input['subAccount'];
        $data['months'] = $input['months'];
        $data['start_date'] = $input['start_date'];
        $data['end_date'] = $input['end_date'];
        // $data['search_preference'] = ($data['months']=='-1') ? 'date' : 'month';
        $data['search_preference'] = $input['search_preference'];
        $data['current_user_id'] = $user_current_account['User']['id'];
        $data['current_account_id'] = $user_current_account['accounts']['account_id'];
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['site_id'] = $this->site_id;

        $filter_id = DB::table('analytics_filters')->insertGetId($data);
        return response()->json(['success'=>true, 'filter_id'=>$filter_id, 'messages'=>$filter_added_message]);
    }

    public function get_all(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'user_current_account' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            return response()->json(['success'=>false, 'messages' => $messages]);
        }
        $user_current_account = $input['user_current_account'];
        // $user_current_account = json_decode($input['user_current_account']);
        $current_user_id = $user_current_account['User']['id'];
        $current_account_id = $user_current_account['accounts']['account_id'];
        $filters = AnalyticsFilter::get_my_analytics_filters($current_user_id, $current_account_id, $this->site_id);
        return response()->json(['success'=>true, 'data' => $filters]);
    }

    public function delete(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'id' => 'required|integer',
            'user_current_account' => 'required',
        ]);
        $filter_deleted_message = "Filter Deleted Successfully.";
        if($this->current_lang == 'es'){
            $filter_deleted_message = "Filtro eliminado correctamente.";
        }
        if ($validator->fails()) {
            $messages = $validator->errors();
            return response()->json(['success'=>false, 'messages' => $messages]);
        }
        $id = $input['id'];
        $user_current_account = $input['user_current_account'];
        // $user_current_account = json_decode($input['user_current_account']);
        $current_user_id = $user_current_account['User']['id'];
        $current_account_id = $user_current_account['accounts']['account_id'];
        $filters = AnalyticsFilter::where('current_user_id', $current_user_id)
                                  ->where('current_account_id', $current_account_id)
                                  ->where('id', $id)
                                  ->where('site_id', $this->site_id)
                                  ->delete();
        return response()->json(['success'=>true, 'messages' => $filter_deleted_message]);
    }
}
