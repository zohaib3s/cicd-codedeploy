<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Validation\Rule;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Services\TranslationsManager;

use App\Models\Account;
use App\Models\AccountFolderUser;
use App\Models\CustomField;
use App\Models\UserCustomField;
use App\Models\AssessmentCustomField;
use App\Models\UserAssessmentCustomField;

class CustomFieldsController extends Controller
{
    protected $httpreq;
    protected $site_id;
    protected $current_lang;
    protected $translations;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        //
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->current_lang = $this->httpreq->header('current-lang');
        $this->translations = TranslationsManager::get_page_lang_based_content('global', $this->site_id, '', $this->current_lang);
    }

    function get_custom_fields(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'account_id' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            return response()->json(['success'=>false, 'messages' => $messages]);
        }
        $account_id = $input['account_id'];
        $ref_type = isset($input['ref_type']) && !empty($input['ref_type']) ? $input['ref_type'] : null;
        $account = Account::where("id", $account_id)->first();
        if(!$account){
            return response()->json(['success'=>false, 'messages' => "Invalid Account!"]);
        }
        $response['admins_can_edit_cf'] = $account->admins_can_edit_cf;
        $response['users_can_edit_cf'] = $account->users_can_edit_cf;
        $response['custom_fields'] = CustomField::get_custom_fields($account_id, $ref_type);

        return response()->json($response);
    }

    function delete_custom_field(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'user_current_account' => 'required',
            'custom_field_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            return response()->json(['success'=>false, 'messages' => $messages]);
        }
        $user_current_account = $input['user_current_account'];
        $role_id = $user_current_account['users_accounts']['role_id'];
        if($role_id>110){
            return response()->json(['success'=>false, 'messages' => 'You have no permission to access this page!']);
        }
        $custom_field = CustomField::find($input['custom_field_id']);
        if(!$custom_field){
            return response()->json(['success'=>false, 'messages' => 'Custom field not found!']);
        }
        CustomField::delete_carefully($custom_field);
        return response()->json(['success'=>true, 'messages' => $this->translations['cf_customfield_deleted_successfully'] ]);
    }

    function save_custom_fields(Request $request)
    {
        $input = $request->all();
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        // $custom_fields = json_decode(json_encode($input['custom_fields']), True);
        $result = array();
        $data = array(
            'admins_can_edit_cf' => (isset($input['admins_can_edit_cf']) && !empty($input['admins_can_edit_cf'])) ? '1' : '0',
            'users_can_edit_cf' => (isset($input['users_can_edit_cf']) && !empty($input['users_can_edit_cf'])) ? '1' : '0',
        );
        Account::where(array('id' => $account_id ))->update($data);

        if(isset($input['custom_fields']) && !empty($input['custom_fields'])){
            $custom_fields = $input['custom_fields'];
            foreach($custom_fields as $cf)
            {
                if(isset($cf['id']))
                {
                    //SW-4469 start
                    if($cf['ref_type'] == 2) //assessment
                    {
                        DB::update("UPDATE `user_assessment_custom_fields` uacf
                            INNER JOIN `assessment_custom_fields` acf ON uacf.`assessment_custom_field_id` = acf.id  
                            INNER JOIN `custom_fields` cf ON cf.id = acf.`custom_field_id`
                            SET uacf.`assessment_custom_field_value` = '' 
                            WHERE cf.id = ".$cf['id']." AND cf.`field_type` != ".$cf['field_type']);
                    }
                    else //user
                    {
                        DB::update("UPDATE `user_custom_fields` ucf 
                            INNER JOIN `custom_fields` cf ON cf.id = ucf.`custom_field_id`
                            SET ucf.`custom_field_value` = ''  
                            WHERE cf.id = ".$cf['id']." AND cf.`field_type` != ".$cf['field_type']);
                    }
                    //SW-4469 end
                    CustomField::unshare($cf['id'], $cf['is_shared']);
                    $updated_data = array(
                        'field_label' => $cf['field_label'],
                        'field_type' => $cf['field_type'],
                        'is_shared' => ($cf['is_shared']) ? '1':'0',
                        'updated_by' => $user_id,
                        'updated_at' => date('Y-m-d H:i:s')
                    ); 
                    DB::table('custom_fields')->where(array('id' => $cf['id'] ))->update($updated_data);  
                }
                else
                {
                    $insertion_data = array(
                        'account_id' => $account_id,
                        'field_label' => $cf['field_label'],
                        'ref_type' => $cf['ref_type'],
                        'field_type' => $cf['field_type'],
                        'is_shared' => ($cf['is_shared']) ? '1':'0',
                        'created_by' => $user_id,
                        'updated_by' => $user_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                    DB::table('custom_fields')->insert($insertion_data); 
                }
            }
        }
        $result['message'] = $this->translations['cf_customfield_settings_saved_successfully'];
        $result['success'] = true;
        return response()->json($result);
    }

    function get_user_custom_fields(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'account_id' => 'required',
            'current_user_id' => 'required',
            'current_user_role_id' => 'required',
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            return response()->json(['success'=>false, 'messages' => $messages]);
        }
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $current_user_role_id = $input['current_user_role_id'];

        $can_user_edit_custom_fields = CustomField::can_user_edit_custom_fields($current_user_role_id, $account_id);

        return response()->json(UserCustomField::get_user_custom_fields($user_id, $account_id, $can_user_edit_custom_fields));
    }

    function save_user_custom_fields(Request $request){
        $input = $request->all();
        $validator = Validator::make($input, [
            'account_id' => 'required',
            'current_user_id' => 'required',
            'current_user_role_id' => 'required',
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            return response()->json(['success'=>false, 'messages' => $messages]);
        }

        $account_id = $input['account_id'];
        $current_user_id = $input['current_user_id'];
        $current_user_role_id = $input['current_user_role_id'];
        $user_id = $input['user_id'];
        $user_custom_fields = $input['user_custom_fields'];
        $can_user_edit_custom_fields = CustomField::can_user_edit_custom_fields($current_user_role_id, $account_id);
        if(!$can_user_edit_custom_fields){
            return response()->json(['success'=>false, 'messages' => "You have no permission to access this page!"]);
        }

        UserCustomField::save_user_custom_fields($user_id, $current_user_id, $account_id, $current_user_role_id, $user_custom_fields);
        return response()->json(['success'=>true, 'messages' => $this->translations['cf_customfield_saved_successfully'] ]);
    }

    function get_assessment_custom_fields(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'account_id' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            return response()->json(['success'=>false, 'messages' => $messages]);
        }
        $account_id = $input['account_id'];
        $account_folder_id = isset($input['account_folder_id']) && !empty($input['account_folder_id']) ? $input['account_folder_id'] : null;

        return response()->json(AssessmentCustomField::get_assessment_custom_fields($account_id, $account_folder_id));
    }

    /*
    function save_assessment_custom_fields(Request $request){
        $input = $request->all();
        $validator = Validator::make($input, [
            'account_id' => 'required',
            'current_user_id' => 'required',
            'current_user_role_id' => 'required',
            'user_id' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            return response()->json(['success'=>false, 'messages' => $messages]);
        }

        $account_id = $input['account_id'];
        $current_user_id = $input['current_user_id'];
        $current_user_role_id = $input['current_user_role_id'];
        $user_id = $input['user_id'];
        $user_custom_fields = $input['user_custom_fields'];
        $can_user_edit_custom_fields = CustomField::can_user_edit_custom_fields($current_user_role_id, $account_id);
        if(!$can_user_edit_custom_fields){
            return response()->json(['success'=>false, 'messages' => "You have no permission to access this page!"]);
        }

        AssessmentCustomField::save_assessment_custom_fields($account_folder_id, $current_user_id, $assessment_custom_fields);
        return response()->json(['success'=>true, 'messages' => "Custom Fields saved successfully!"]);
    }
    */

    function save_user_assessment_custom_fields(Request $request) {
        $input = $request->all();
        // Get current user object.
        // Get huddle role id of current user.
        // if Huddle Role ID is 200 then allow to save.
        // Get assessee_id and user_assessment_custom_fields
        $validator = Validator::make($input, [
            'user_current_account' => 'required',
            'assessee_id' => 'required',
            'huddle_id' => 'required',
            'assessment_custom_fields' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            return response()->json(['success'=>false, 'messages' => $messages]);
        }

        $user_current_account = $input['user_current_account'];
        $assessee_id = $input['assessee_id'];
        $huddle_id = $input['huddle_id'];
        $current_user_id = $user_current_account['User']['id'];
        $assessment_custom_fields = $input['assessment_custom_fields'];
// Get User Huddle Role. And check if he is assessor.
        $is_assessor = AccountFolderUser::check_if_evalutor($this->site_id, $huddle_id, $current_user_id);
        if(!$is_assessor){
            return response()->json(['success'=>false, 'messages' => "You have no permission to access this page!"]);
        }

        UserAssessmentCustomField::save_user_assessment_custom_fields($huddle_id, $assessee_id, $current_user_id, $assessment_custom_fields);
        return response()->json(['success'=>true, 'messages' => $this->translations['cf_customfield_saved_successfully'] ]);
    }

    function get_user_assessment_custom_fields(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, [
            'huddle_id' => 'required',
            'assessee_id' => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            return response()->json(['success'=>false, 'messages' => $messages]);
        }
        $huddle_id = $input['huddle_id'];
        $assessee_id = $input['assessee_id'];

        return response()->json(UserAssessmentCustomField::get_user_assessment_custom_fields($assessee_id, $huddle_id));
    }


}
