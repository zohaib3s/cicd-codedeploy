<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\DocumentFiles;
use App\Models\GlobalSettings;
use App\Models\LiveStreamStatus;
use App\Models\ScreenShareLogs;
use App\Models\Sites;
use App\Models\SNSAuditLogs;
use App\Models\SNSElementalUpdates;
use App\Services\Emails\Email;
use App\Services\HelperFunctions;
use App\Services\TranslationsManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LiveStreamController extends Controller {

    protected $httpreq;
    protected $site_id;
    protected $current_lang;
    protected $base;
    //protected $language_based_content;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->httpreq = app("Illuminate\Http\Request");
        $this->site_id = $this->httpreq->header('site_id');
        $this->current_lang = $this->httpreq->header('current-lang');
        $this->base = config('s3.sibme_base_url');
        //$this->language_based_content = TranslationsManager::get_page_lang_based_content('global', $this->site_id, '', 'en');
    }

    public function screenShareEvent(Request $request)
    {
        $account_id = $request->get("account_id");
        $document_id = $request->get("document_id");
        $huddle_id = $request->get("huddle_id");
        $started_at = $request->has("started_at") ? $request->get("started_at") : 0;
        $stopped_at = $request->has("stopped_at") ? $request->get("stopped_at") : 0;
        $file_name_recorded_video = $request->get("file_name_recorded_video");
        $huddleController = app('App\Http\Controllers\HuddleController');
        $streaming_auth_details = $huddleController->getStreamingPlatformDetails($account_id);
        $stream_url = "https://viewer.millicast.com/v2?streamId=".$streaming_auth_details["username"]."/".$file_name_recorded_video;

        $stream_log = ScreenShareLogs::where("document_id",$document_id)->where("site_id",$this->site_id)->where("file_name_recorded_video", $file_name_recorded_video)->first();
        $channel_data = array(
            'channel' => 'video-details-' . $document_id,
            'event' => "live_screen_sharing",
            'live_video_event' => "1",
            'item_id' => $document_id,
            'huddle_id' => $huddle_id,
            'file_name_recorded_video' => $file_name_recorded_video,
            'stream_url' => $stream_url
        );
        if(!$stream_log)
        {
            $stream_log = new ScreenShareLogs();
            $stream_log->document_id = $document_id;
            $stream_log->file_name_recorded_video = $file_name_recorded_video;
            $stream_log->site_id = $this->site_id;
        }
        if($started_at)
        {
            $channel_data["sharing_screen"] = 1;
            $channel_data["notification_type"] = 11;
            $stream_log->started_at = $started_at;
            $stream_log->created_at = date("Y-m-d H:i:s", time() - date("Z"));//UTC time
        }
        if($stopped_at)
        {
            $channel_data["sharing_screen"] = 0;
            $channel_data["notification_type"] = 12;
            $stream_log->stopped_at = $stopped_at;
            $stream_log->updated_at = date("Y-m-d H:i:s", time() - date("Z"));//UTC time
        }
        $stream_log->save();

        HelperFunctions::broadcastEvent($channel_data,"broadcast_event");
        return response()->json(["status"=>true, "message"=>"Live Screen Log added", "data"=>$stream_log]);
    }

    public function checkScreenSharing(Request $request)
    {
        $document_id = $request->get("document_id");
        $result = self::checkScreenSharingInternal($document_id, $this->site_id);
        return response()->json(["status"=>true, "message"=>'success', "screen_share_log"=>$result['log'], "is_sharing"=>$result['is_sharing']]);
    }

    public static function checkScreenSharingInternal($document_id, $site_id)
    {
        $stream_log = ScreenShareLogs::where("document_id",$document_id)->where("site_id",$site_id)->orderBy("id", "desc")->first();
        $is_sharing = 0;
        if($stream_log && empty($stream_log->stopped_at))
        {
            $is_sharing = 1;
        }
        return ["log" => $stream_log, "is_sharing" => $is_sharing];
    }

    public function getClippingData($document_id)
    {
        $stream_log = ScreenShareLogs::where("document_id",$document_id)->get();
        if(count($stream_log) == 0)
        {
            return [];
        }
        $number_of_clips = (count($stream_log)*2+1);
        $clips = [];
        $stream_counter = 0;
        for($i = 0; $i < $number_of_clips; $i++)
        {
            if($i%2 == 0)//even number->make clip
            {
                if($i == 0)//first iteration
                {
                    $clips[] = ["start"=>"00:00:00:00", "end"=>$this->getTimeFromSeconds($stream_log[0]->started_at)];
                }
                else if($i != ($number_of_clips-1))//middle iterations
                {
                    $clips[] = ["start"=>$this->getTimeFromSeconds($stream_log[$stream_counter-1]->stopped_at), "end"=>$this->getTimeFromSeconds($stream_log[$stream_counter]->started_at)];
                }
                else//last iteration
                {
                    $clips[] = ["start"=>$this->getTimeFromSeconds($stream_log[$stream_counter-1]->stopped_at), "end"=>"00:00:00:00"];
                }
            }
            else
            {
                $clips[] = ["s3_path" => $stream_log[$stream_counter]->s3_path];//no need to give clip data for screen share videos
                $stream_counter++;
            }
        }
        return $clips;
    }

    public function getTimeFromSeconds($seconds)
    {
        $hours = floor($seconds / 3600);
        $mins = floor(($seconds - $hours*3600) / 60);
        $s = $seconds - ($hours*3600 + $mins*60);

        $mins = ($mins<10?"0".$mins:"".$mins);
        $s = ($s<10?"0".$s:"".$s);

        $time = ($hours<10?"0".$hours:"".$hours).":".$mins.":".$s;
        return $time.":00";//this 00 is for frames for AWS
    }

    public function updateLiveStreamStatus(Request $request)
    {
        $document_id = $request->get("document_id");
        if($document_id)
        {
            $document = Document::where("id", $document_id)->first();
            if($document)
            {
                if($document->doc_type == 4)//live
                {
                    $livestream_status = LiveStreamStatus::where("document_id", $document_id)->first();
                    if(!$livestream_status)
                    {
                        $livestream_status = new LiveStreamStatus();
                        $livestream_status->document_id = $document_id;
                    }
                    $livestream_status->last_live_at = date("Y-m-d H:i:s");
                    $livestream_status->save();
                    return response()->json(["success"=>true, "message"=>"Live status updated successfully!"]);
                }
                else
                {
                    return response()->json(["success"=>false, "message"=>"This is not Live now!"]);
                }
            }
            else
            {
                return response()->json(["success"=>false, "message"=>"Invalid Live stream id."]);
            }
        }
        else
        {
            return response()->json(["success"=>false, "message"=>"document_id is required."]);
        }
    }

    public function submit_clip_merging_job(Request $request)
    {
        try {
            $s3Service = HelperFunctions::getS3ServiceObject();
            $folder_path = $request->get('folder_path');
            $s3_dest_folder_path = $request->get('s3_dest_folder_path');
            $name_modifier = $request->has('name_modifier_not_required') ? '' : '_screen_sharing_1_screen_share';
            $rotate_180 = $request->has('rotate_180') ? $request->get('rotate_180') : 0;
            $base_name = $request->get('file_name') ? $request->get('file_name') : 'output';
            $use_elastic = $request->has('use_elastic') ? $request->get('use_elastic') : 0;
            $document_id = $request->get('document_id');
            $clips =  explode(",", $request->get('clips'));
            if($use_elastic)
            {
                $job_id = $s3Service->elastic_clip_merging_job($folder_path, $clips, $s3_dest_folder_path, $name_modifier, 1, $rotate_180, $base_name);
                if(!empty($job_id))
                {
                    $out_url = $s3_dest_folder_path."/".$base_name.$name_modifier."_720_enc.mp4";
                    $updates = array(
                        'zencoder_output_id' => $job_id,
                        'url' => $out_url,
                        'encoder_provider' => 2,
                        'encoder_status' => '',
                        'aws_transcoder_type' => 1,
                        'published' => 0,
                        's3_thumbnail_url' => $s3_dest_folder_path."/".$base_name.$name_modifier."_720_enc_thumb_00001.png",
                    );

                    Document::where('id', $document_id)->update($updates);
                    $doc_file = DocumentFiles::where('document_id', $document_id)->first();
                    if(!$doc_file)
                    {
                        $doc_file = new DocumentFiles();
                    }
                    $doc_file->document_id = $document_id;
                    $doc_file->transcoding_job_id = $job_id;
                    $doc_file->transcoding_status = 3;
                    $doc_file->url = $out_url;
                    $doc_file->default_web = true;
                    $doc_file->site_id = $request->get('site_id');
                    $doc_file->save();

                }
                else
                {
                    return response()->json(["status" => false, "message" => "Error creating elemental job"]);
                }
            }
            else
            {
                $job_id = $s3Service->submitElementalChunksMergingJob($folder_path, $clips, $s3_dest_folder_path, $name_modifier, 1, $rotate_180);
                if(!empty($job_id))
                {
                    $updates = array(
                        'zencoder_output_id' => $job_id,
                        'url' => $s3_dest_folder_path."/".$request->get('file_name').$name_modifier.".mp4",
                        'encoder_provider' => 2,
                        'encoder_status' => 'complete',
                        'aws_transcoder_type' => 2,
                    );
                    Document::where('id', $document_id)->update($updates);
                }
                else
                {
                    return response()->json(["status" => false, "message" => "Error creating elemental job"]);
                }
            }
        }
        catch (\Exception $e)
        {
            return response()->json(["status" => false, "message" => $e->getMessage()]);
        }
        return response()->json(["status" => true, "message" => "success"]);
    }

    public function add_websocket_logs(Request $request)
    {
        try
        {
            HelperFunctions::insertWebsocketLogs($request->get('data'), $request->get('token_ids'), 1);
        }
        catch (\Exception $e)
        {
            Log::error("WebsocketLog error : ".$e->getMessage());
        }
    }

    public function elementalStatusUpdates(Request $request)
    {
        try
        {
            Log::info("AWS SNS Job Notification-> \n");
            $result = json_decode($request->getContent(), true);
            $jobDetails = json_decode($result['Message'],true);
            $jobDetails = $jobDetails['detail'];
            $record = SNSElementalUpdates::where('job_id', $jobDetails['jobId'])->first();

            if(!$record)
            {
                $record = new SNSElementalUpdates();
                $record->created_date = date('Y-m-d H:i:s');
            }

            $record->job_id = $jobDetails['jobId'];
            $record->job_status = $jobDetails['status'];
            if(isset($jobDetails['errorMessage']))
            {
                $record->error_message = $jobDetails['errorCode']."::".$jobDetails['errorMessage'];
            }
            if(isset($jobDetails['outputGroupDetails'][0]['outputDetails'][0]['durationInMs']))
            {
                $record->duration =  ($jobDetails['outputGroupDetails'][0]['outputDetails'][0]['durationInMs']/1000);
            }
            if(isset($jobDetails['outputGroupDetails'][0]['outputDetails'][0]['videoDetails']['heightInPx']))
            {
                $record->height = ($jobDetails['outputGroupDetails'][0]['outputDetails'][0]['videoDetails']['heightInPx']);
            }
            if(isset($jobDetails['outputGroupDetails'][0]['outputDetails'][0]['videoDetails']['widthInPx']))
            {
                $record->width = ($jobDetails['outputGroupDetails'][0]['outputDetails'][0]['videoDetails']['widthInPx']);
            }
            if(isset($jobDetails['outputGroupDetails'][0]['outputDetails'][0]['outputFilePaths'][0]))
            {
                $record->s3_path = ($jobDetails['outputGroupDetails'][0]['outputDetails'][0]['outputFilePaths'][0]);
            }
            if(isset($jobDetails['outputGroupDetails'][1]['outputDetails'][0]['outputFilePaths'][0]))
            {
                $record->thumbnail_s3_path = ($jobDetails['outputGroupDetails'][1]['outputDetails'][0]['outputFilePaths'][0]);
            }

            if(isset($jobDetails['jobProgress']['jobPercentComplete']))
            {
                $record->transcoding_percentage = $jobDetails['jobProgress']['jobPercentComplete'];
            }

            $record->last_edit_date = date('Y-m-d H:i:s');
            $record->save();
            $log = new SNSAuditLogs();
            $log->message = "Success";
            $log->data = $request->getContent();
            $log->is_error = 0;
            $log->record_id = $record->id;
            $log->created_at = date('Y-m-d H:i:s');
            $log->save();
        }
        catch (\Exception $e)
        {
            Log::info("Error while processing AWS SNS Job Notification-> \n");
            Log::error($e->getMessage());
            $log = new SNSAuditLogs();
            $log->message = "Error :: ". $e->getMessage();
            $log->data = $request->getContent();
            $log->is_error = 1;
            if(isset($record) && !empty($record))
            {
                $log->record_id = $record->id;
            }
            $log->created_at = date('Y-m-d H:i:s');
            $log->save();
            $html = "Please check Following error occurred while processing AWS SNS Elemental Event. \n If you dont resolve the issue videos will stop getting processed. \n Error :: \n " . $e->getMessage();
            $to = GlobalSettings::getSettingByName('error_email_receivers','testinglandingpage9@gmail.com,khurri.saleem@gmail.com');
            $emailData = [
                'from' => Sites::get_site_settings('static_emails', '1')['noreply'],
                'from_name' => Sites::get_site_settings('site_title', '1'),
                'to' => $to ,
                'subject' => 'Alert AWS SNS EVENT Processing Failed!',
                'template' => $html,
            ];
            Email::sendCustomEmail($emailData);
        }
    }

    public function getTranscodingJobDetails($document_id)
    {
        $document = Document::where('id', $document_id)->first();
        $document_file = DocumentFiles::where('document_id', $document_id)->first();
        if($document_file && !empty($document_file->transcoding_job_id))
        {
            $document->zencoder_output_id = $document_file->transcoding_job_id;//Sometimes job id is not correct in documents table
        }
        $details = ["success"=>false, "message"=>"Unknown Error", "data"=>[]];
        if($document)
        {
            if(!empty($document->zencoder_output_id) && strpos( $document->zencoder_output_id,'-') !== false)
            {
                $s3_service = HelperFunctions::getS3ServiceObject();
                if($document->aws_transcoder_type == 2)
                {
                    $details = $s3_service->getElementalJobDetail($document, 1);
                }
                else if($document->aws_transcoder_type == 1)
                {
                    $details = $s3_service->getElasticJobDetail($document);
                }
            }
            else
            {
                $details = ["success"=>false, "message"=>"Invalid Job Id or Video Not Yet Submitted For Transcoding!", "data"=>[]];
            }
        }
        else
        {
            $details = ["success"=>false, "message"=>"Invalid Video Id", "data"=>[]];
        }
        $details['document'] = $document;
        $details['document_files'] = $document_file;
        return response()->json($details);
    }
}
