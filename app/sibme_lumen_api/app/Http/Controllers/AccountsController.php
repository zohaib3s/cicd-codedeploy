<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Document;
use App\Models\AccountFolder;
use App\Models\AccountFolderMetaData;
use App\Models\AccountMetaData;
use App\Models\AccountTag;
use App\Models\AccountFolderUser;
use App\Models\AccountFolderDocument;
use App\Models\Group;
use App\Models\Sites;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\UserActivityLog;
use App\Models\UserGroup;
use App\Models\EmailUnsubscribers;
use App\Services\HelperFunctions;
use App\Services\SendGridEmailManager;
use App\Services\TranslationsManager;
use App\Services\UsersHelperFunctions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use \DrewM\MailChimp\MailChimp;
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;

class AccountsController extends Controller
{
    protected $httpreq;
    protected $site_id;
    protected $lang;
    public $mailchimp;
    public $base;
    public $language_based_messages;
    public $group_based_translation;
    public $subject_title;
    public $duplicated_emails;
    public $linked_emails;
    public $new_emails_added;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->lang = $this->httpreq->header('current-lang');
        $this->base = config('s3.sibme_base_url');
        $this->mailchimp = new MailChimp('007cb274d399d9290e1e6c5b42118d40-us3');
        $this->language_based_messages = TranslationsManager::get_page_lang_based_content('flash_messages', $this->site_id, '', $this->lang);
        $this->group_based_translation = TranslationsManager::get_page_lang_based_content('users/administrators_groups', $this->site_id, '', $this->lang);
        $this->subject_title = Sites::get_site_settings('email_subject', $this->site_id);
        $this->module_url = 'Api/huddle_list';
    }

    function get_general_account_settings(Request $request)
    {
        $input = $request->all();
        $account_id = $input['account_id'];

        $account_detail = Account::where(array('id' => $account_id))->first();

        if($account_detail)
        {
            $account_detail = $account_detail->toArray();
            $account_detail['is_saml_enabled'] = ($account_detail['is_saml_enabled'] == 1) ? true : false;
            $account_detail['is_force_sso_enabled'] = ($account_detail['is_force_sso_enabled'] == 1) ? true : false;
            $account_detail['force_auto_delete'] = ($account_detail['force_auto_delete'] == 1) ? true : false;
            $account_detail['allow_auto_delete_on_mobile'] = ($account_detail['allow_auto_delete_on_mobile'] == 1) ? true : false;
        }
        $get_analytics_duration_value = AccountMetaData::where(array(
            "account_id" => $account_id,
            "meta_data_name" => "tracking_duration"
        ))->first();


        if($get_analytics_duration_value)
        {
            $get_analytics_duration_value = $get_analytics_duration_value->toArray();
            $get_analytics_duration_value = $get_analytics_duration_value['meta_data_value'];

        }
        else {
            $get_analytics_duration_value = '';
        }

        $enable_video_library = AccountMetaData::where(array(
            "account_id" => $account_id,
            "meta_data_name" => "enable_video_library"
        ))->first();

        if($enable_video_library)
        {
            $enable_video_library = $enable_video_library->toArray();
            if($enable_video_library['meta_data_value'] == '1' )
            {
                $enable_video_library = true;
            }
            else
            {
                $enable_video_library = false;
            }
        }
        else {
            $enable_video_library = false;
        }

        $enable_analytics = AccountMetaData::where(array(
            "account_id" => $account_id,
            "meta_data_name" => "enableanalytics"
        ))->first();

        if($enable_analytics)
        {
            $enable_analytics = $enable_analytics->toArray();
            if($enable_analytics['meta_data_value'] == '1' )
            {
                $enable_analytics = true;
            }
            else
            {
                $enable_analytics = false;
            }
        }

        else
        {
            $enable_analytics = false;
        }

        $enable_analytics_enableleaderboard = AccountMetaData::where(array(
            "account_id" => $account_id,
            "meta_data_name" => "enable_analytics_enableleaderboard"
        ))->first();
            
        if($enable_analytics_enableleaderboard)
        {
            $enable_analytics_enableleaderboard = $enable_analytics_enableleaderboard->toArray();
            if($enable_analytics_enableleaderboard['meta_data_value'] == '1' )
            {
                $enable_analytics_enableleaderboard = true;
            }
            else
            {
                $enable_analytics_enableleaderboard = false;
            }
        }
        else
        {
            $enable_analytics_enableleaderboard = true;
        }

        $enable_assessment_tracker = AccountMetaData::where(array(
            "account_id" => $account_id,
            "meta_data_name" => "enable_matric"
        ))->first();

        if($enable_assessment_tracker)
        {
            $enable_assessment_tracker = $enable_assessment_tracker->toArray();
            if($enable_assessment_tracker['meta_data_value'] == '1' )
            {
                $enable_assessment_tracker = true;
            }
            else
            {
                $enable_assessment_tracker = false;
            }
        }
        else {
            $enable_assessment_tracker = false;
        }

        $enable_coaching_tracker = AccountMetaData::where(array(
            "account_id" => $account_id,
            "meta_data_name" => "enable_tracker"
        ))->first();

        if($enable_coaching_tracker)
        {
            $enable_coaching_tracker = $enable_coaching_tracker->toArray();
            if($enable_coaching_tracker['meta_data_value'] == '1' )
            {
                $enable_coaching_tracker = true;
            }
            else
            {
                $enable_coaching_tracker = false;
            }
        }
        else
        {
            $enable_coaching_tracker = false;
        }

        $default_framework = AccountMetaData::where(array(
            "account_id" => $account_id,
            "meta_data_name" => "default_framework"
        ))->first();

        if($default_framework)
        {
            $default_framework = $default_framework->toArray();
            $default_framework = $default_framework['meta_data_value'];

        }
        else {
            $default_framework = '';
        }

        $enable_custom_markers_in_account = AccountFolderMetaData::where(array(
            "account_folder_id" => $account_id,
            "meta_data_name" => "enable_tags"
        ))->first();

        if($enable_custom_markers_in_account)
        {
            $enable_custom_markers_in_account = $enable_custom_markers_in_account->toArray();
            if($enable_custom_markers_in_account['meta_data_value'] == '1' )
            {
                $enable_custom_markers_in_account = true;
            }
            else
            {
                $enable_custom_markers_in_account = false;
            }
        }
        else {
            $enable_custom_markers_in_account = false;
        }

        $enable_custom_markers_in_workspace = AccountFolderMetaData::where(array(
            "account_folder_id" => $account_id,
            "meta_data_name" => "enable_tags_ws"
        ))->first();

        if($enable_custom_markers_in_workspace)
        {
            $enable_custom_markers_in_workspace = $enable_custom_markers_in_workspace->toArray();
            if($enable_custom_markers_in_workspace['meta_data_value'] == '1' )
            {
                $enable_custom_markers_in_workspace = true;
            }
            else
            {
                $enable_custom_markers_in_workspace = false;
            }
        }
        else {
            $enable_custom_markers_in_workspace = false;
        }

        $enable_framework_in_account = AccountFolderMetaData::where(array(
            "account_folder_id" => $account_id,
            "meta_data_name" => "enable_framework_standard"
        ))->first();

        if($enable_framework_in_account)
        {
            $enable_framework_in_account = $enable_framework_in_account->toArray();
            if($enable_framework_in_account['meta_data_value'] == '1' )
            {
                $enable_framework_in_account = true;
            }
            else
            {
                $enable_framework_in_account = false;
            }
        }
        else {
            $enable_framework_in_account = false;
        }


        $enable_framework_in_workspace = AccountFolderMetaData::where(array(
            "account_folder_id" => $account_id,
            "meta_data_name" => "enable_framework_standard_ws"
        ))->first();
        $assessment_perfomance_level = AccountMetaData::where(array(
                    "account_id" => $account_id,
                    "meta_data_name" => "assessment_perfomance_level"
            ))->first();

        if($enable_framework_in_workspace)
        {
            $enable_framework_in_workspace = $enable_framework_in_workspace->toArray();
            if($enable_framework_in_workspace['meta_data_value'] == '1' )
            {
                $enable_framework_in_workspace = true;
            }
            else
            {
                $enable_framework_in_workspace = false;
            }
        }
        else {
            $enable_framework_in_workspace = false;
        }

        $account_owner_id = UserAccount::where(array('account_id' => $account_id, 'role_id' => 100))->first();
        if($account_owner_id)
        {
            $account_owner_id = $account_owner_id['user_id'];
        }
        $role_detail = UserAccount::where(array('account_id' => $account_id, 'user_id' => $input['user_id']))->first();
        $user_detail = User::where(array('id' => $input['user_id'] ))->first();

        if($role_detail->role_id != 100 && $role_detail->role_id != 110  )
        {
            $final_array = array(
                'success' => false,
                'message' => 'You do not have permission to this page.'
            );
            return response()->json($final_array);
        }

        $frameworks = AccountTag::select('AccountTag.tag_title', 'AccountTag.account_tag_id' ,'a.company_name')
            ->join('accounts as a','a.id','=','AccountTag.account_id')
            ->join('account_framework_settings as afs','afs.account_tag_id','=','AccountTag.account_tag_id')
            ->where(array("tag_type" => '2',"AccountTag.account_id" => $account_id, 'afs.published' => '1' ))
            ->get();

        $get_tags = AccountTag::select('AccountTag.tag_title','AccountTag.account_tag_id')->where(array(
            "account_id" => $account_id,
            "tag_type" => 1))->get();

        $get_metrics = AccountMetaData::select('account_meta_data.account_meta_data_id','account_meta_data.meta_data_value')->selectRaw('SUBSTRING(account_meta_data.meta_data_name,14) as rating_name')->where(array(
            "account_id" => $account_id
        ))->whereRaw("meta_data_name like 'metric_value_%'")->orderBy('meta_data_value', 'asc')->get();

        $folders_check = true;
        $permission_access_video_library = true;
        $permission_video_library_upload = true;
        $permission_administrator_user_new_role = true;
        $parmission_access_my_workspace = true;
        $manage_collab_huddles = true;
        $manage_coach_huddles = true;
        $manage_evaluation_huddles = true;
        $permission_view_analytics = true;
        $huddle_to_workspace = true;
        $permission_start_synced_scripted_notes = true;
        $permission_video_library_download = true;
        $permission_video_library_comments = true;
        $permission_share_library = true;

        $admin_permission_access_video_library= true;
        $admin_permission_video_library_download = true;
        $admin_permission_video_library_comments = true;
        $admin_permission_video_library_upload = true;
        $admin_permission_administrator_user_new_role = true;
        $admin_view_analytics = true;
        $admin_permission_share_library = true;
        $admins_can_view_leaderboard =true;

        $user_privileges = UserAccount::where(array(
            "account_id" => $account_id,
            "role_id" => 120
        ))->get()->toArray();

        $admin_privileges = UserAccount::where(array(
            "account_id" => $account_id,
            "role_id" => 115
        ))->get()->toArray();

        foreach ($user_privileges as $row) {

            if ($row['folders_check'] == 0) {
                $folders_check = false;
            }

            if ($row['permission_share_library'] == 0) {
                $permission_share_library = false;
            }

            if ($row['permission_video_library_download'] == 0) {
                $permission_video_library_download = false;
            }

            if ($row['permission_video_library_comments'] == 0) {
                $permission_video_library_comments = false;
            }

            if ($row['permission_access_video_library'] == 0) {
                $permission_access_video_library = false;
            }
            if ($row['permission_video_library_upload'] == 0) {
                $permission_video_library_upload = false;
            }
            if ($row['permission_administrator_user_new_role'] == 0) {
                $permission_administrator_user_new_role = false;
            }
            if ($row['manage_collab_huddles'] == 0) {
                $manage_collab_huddles = false;
            }
            if ($row['manage_coach_huddles'] == 0) {
                $manage_coach_huddles = false;
            }
            if ($row['manage_evaluation_huddles'] == 0) {
                $manage_evaluation_huddles = false;
            }
            if ($row['parmission_access_my_workspace'] == 0) {
                $parmission_access_my_workspace = false;
            }

            if ($row['permission_view_analytics'] == 0) {
                $permission_view_analytics = false;
            }
            if ($row['huddle_to_workspace'] == 0) {
                $huddle_to_workspace = false;
            }
            if ($row['permission_start_synced_scripted_notes'] == 0) {
                $permission_start_synced_scripted_notes = false;
            }
        }

        foreach ($admin_privileges as $row) {

            if ($row['permission_video_library_upload'] == 0) {
                $admin_permission_video_library_upload = false;
            }
            if ($row['permission_administrator_user_new_role'] == 0) {
                $admin_permission_administrator_user_new_role = false;
            }
            if ($row['permission_view_analytics'] == 0) {
                $admin_view_analytics = false;
            }
            if ($row['permission_access_video_library'] == 0) {
                $admin_permission_access_video_library = false;
            }
            if ($row['permission_video_library_download'] == 0) {
                $admin_permission_video_library_download = false;
            }
            if ($row['permission_video_library_comments'] == 0) {
                $admin_permission_video_library_comments = false;
            }
            if ($row['permission_share_library'] == 0) {
                $admin_permission_share_library = false;
            }
            if ($row['admins_can_view_leaderboard'] == 0) {
                $admins_can_view_leaderboard = false;
            }

            
        }

        $admins_count = count($admin_privileges);
        $users_count = count($user_privileges);
        if($users_count  == 0)
        {
            $folders_check = false;
            $permission_access_video_library = false;
            $permission_video_library_upload = false;
            $permission_administrator_user_new_role = false;
            $parmission_access_my_workspace = false;
            $manage_collab_huddles = false;
            $manage_coach_huddles = false;
            $manage_evaluation_huddles = false;
            $permission_view_analytics = false;
            $huddle_to_workspace = false;
            $permission_start_synced_scripted_notes = false;
            $permission_video_library_download = false;
            $permission_video_library_comments = false;
            $permission_share_library = false;
        }
        if($admins_count  == 0)
        {
            $admin_permission_access_video_library= false;
            $admin_permission_video_library_download = false;
            $admin_permission_video_library_comments = false;
            $admin_permission_video_library_upload = false;
            $admin_permission_administrator_user_new_role = false;
            $admin_view_analytics = false;
            $admin_permission_share_library = false;
            $admins_can_view_leaderboard = false;
        }

        $users_privileges_check = array(
            'folders_check' => $folders_check,
            'permission_access_video_library' => $permission_access_video_library ,
            'permission_video_library_upload' => $permission_video_library_upload,
            'permission_administrator_user_new_role' => $permission_administrator_user_new_role ,
            'parmission_access_my_workspace' => $parmission_access_my_workspace ,
            'manage_collab_huddles' => $manage_collab_huddles ,
            'manage_coach_huddles' => $manage_coach_huddles ,
            'manage_evaluation_huddles' => $manage_evaluation_huddles ,
            'permission_view_analytics' => $permission_view_analytics ,
            'huddle_to_workspace' => $huddle_to_workspace,
            'permission_start_synced_scripted_notes' => $permission_start_synced_scripted_notes,
            'permission_video_library_download' => $permission_video_library_download,
            'permission_video_library_comments' => $permission_video_library_comments,
            'permission_share_library' => $permission_share_library

        );

        $admins_privileges_check = array(
            'admin_permission_video_library_upload'  =>  $admin_permission_video_library_upload,
            'admin_permission_administrator_user_new_role' => $admin_permission_administrator_user_new_role,
            'admin_view_analytics' => $admin_view_analytics ,
            'admin_permission_access_video_library' => $admin_permission_access_video_library,
            'admin_permission_video_library_download' => $admin_permission_video_library_download,
            'admin_permission_video_library_comments' => $admin_permission_video_library_comments,
            'admin_permission_share_library' => $admin_permission_share_library,
            'admins_can_view_leaderboard' => $admins_can_view_leaderboard
            
        );


        foreach ($get_metrics as $metric)
        {
            $metric->is_new = false;
            $metric->is_update = false;
            $metric->is_delete = false;
        }

        foreach ($get_tags as $tags)
        {
            $tags->is_new = false;
            $tags->is_update = false;
            $tags->is_delete = false;
        }

        $enable_live_rec = false;
        $transcribe_library_videos = false;

        if($account_detail['enable_live_rec'] == '1')
        {
            $enable_live_rec = true;
        }

        if($account_detail['transcribe_library_videos'] == '1')
        {
            $transcribe_library_videos = true;
        }

        if($assessment_perfomance_level['meta_data_value'] == 1){
            $assessment_perfomance_level = true;
        }else{
            $assessment_perfomance_level = false;
        }
        if(isset($account_detail['is_evaluation_activated']) && $account_detail['is_evaluation_activated']==1){
            $is_evaluation_activated = true;
        }else{
            $is_evaluation_activated = false;
        }

        $transcribe_workspace_option = AccountMetaData::where(array(
            "account_id" => $account_id,
            "meta_data_name" => "transcribe_workspace_videos"
        ))->first();
        if(!empty($transcribe_workspace_option['meta_data_value']) && $transcribe_workspace_option['meta_data_value'] == 1){
            $transcribe_workspace_option = true;
        }else{
            $transcribe_workspace_option = false;
        }
        $transcribe_huddle_option = AccountMetaData::where(array(
            "account_id" => $account_id,
            "meta_data_name" => "transcribe_huddle_videos"
        ))->first();
        if(!empty($transcribe_huddle_option['meta_data_value']) && $transcribe_huddle_option['meta_data_value'] == 1){
            $transcribe_huddle_option = true;
        }else{
            $transcribe_huddle_option = false;
        }
        $transcribe_library_option = AccountMetaData::where(array(
            "account_id" => $account_id,
            "meta_data_name" => "transcribe_library_videos"
        ))->first();
        if(!empty($transcribe_library_option['meta_data_value']) && $transcribe_library_option['meta_data_value'] == 1){
            $transcribe_library_option = true;
        }else{
            $transcribe_library_option = false;
        }
        $final_array = array(
            'transcribe_library_option'=>$transcribe_library_option,
            'transcribe_huddle_option'=>$transcribe_huddle_option,
            'transcribe_workspace_option'=>$transcribe_workspace_option,
            'assessment_perfomance_level' => $assessment_perfomance_level,
            'is_evaluation_activated'=>$is_evaluation_activated,
            'account_detail' => $account_detail,
            'enable_goals' => $account_detail["enable_goals"],
            'force_auto_delete' => $account_detail["force_auto_delete"],
            'allow_auto_delete_on_mobile' => $account_detail["allow_auto_delete_on_mobile"],
            'user_detail' => $user_detail,
            'get_analytics_duration_value' =>  $get_analytics_duration_value,
            'enable_video_library' => $enable_video_library,
            'enable_analytics' => $enable_analytics,
            'enable_analytics_enableleaderboard' => $enable_analytics_enableleaderboard,
            'enable_assessment_tracker' => $enable_assessment_tracker,
            'enable_coaching_tracker' => $enable_coaching_tracker,
            'default_framework' => $default_framework,
            'enable_custom_markers_in_account' => $enable_custom_markers_in_account,
            'enable_custom_markers_in_workspace' => $enable_custom_markers_in_workspace,
            'enable_framework_in_account' => $enable_framework_in_account,
            'transcribe_library_videos' => $transcribe_library_videos,
            'enable_live_rec' => $enable_live_rec,
            'enable_framework_in_workspace' => $enable_framework_in_workspace,
            'transcribe_huddle_videos'=>$account_detail['transcribe_huddle_videos'],
            'transcribe_workspace_videos'=>$account_detail['transcribe_workspace_videos'],
            'account_owner_list' => $this->accountOwnersList($account_id),
            'account_owner_id' => $account_owner_id,
            'frameworks_list' => $frameworks,
            'custom_marker_tags' => $get_tags,
            'assessment_ratings' => $get_metrics,
            'users_privileges_check' => $users_privileges_check,
            'admins_privileges_check' => $admins_privileges_check ,
            'users_count'=>$users_count,
            'admins_count'=>$admins_count,
            'success' => true
        );

        return response()->json($final_array);

    }

    function update_general_account_settings(Request $request)
    {
        $input = $request->all();
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];

        if($input['enable_video_library'])
        {
            $enable_video_library = '1';
        }
        else {
            $enable_video_library = '0';
        }

        if($input['enable_assessment_tracker'])
        {
            $enable_assessment_tracker = '1';
        }
        else {
            $enable_assessment_tracker = '0';
        }

        if($input['enable_coaching_tracker'])
        {
            $enable_coaching_tracker = '1';
        }
        else {
            $enable_coaching_tracker = '0';
        }

        if($input['enable_custom_markers_in_account'])
        {
            $enable_custom_markers_in_account = '1';
        }
        else {
            $enable_custom_markers_in_account = '0';
        }

        if($input['enable_custom_markers_in_workspace'])
        {
            $enable_custom_markers_in_workspace = '1';
        }
        else {
            $enable_custom_markers_in_workspace = '0';
        }

        if($input['enable_framework_in_account'])
        {
            $enable_framework_in_account = '1';
        }
        else {
            $enable_framework_in_account = '0';
        }

        if($input['enable_framework_in_workspace'])
        {
            $enable_framework_in_workspace = '1';
        }
        else {
            $enable_framework_in_workspace = '0';
        }

        if($input['enable_analytics'])
        {
            $enable_analytics = '1';
        }
        else {
            $enable_analytics = '0';
        }

        if($input['enable_analytics_enableleaderboard'])
        {
            $enable_analytics_enableleaderboard = '1';
        }
        else {
            $enable_analytics_enableleaderboard = '0';
        }


        if(isset($input['enable_goals']) && $input['enable_goals'] == 1)
        {
            $enable_goals = '1';
        }
        else {
            $enable_goals = '0';
        }

        if($input['enable_live_rec'])
        {
            DB::table("accounts")->where(array('id' => $account_id))->update(array('enable_live_rec' => '1'));
            DB::table("users_accounts")->where(array('account_id' => $account_id))->update(array('live_recording' => '1'));
        }
        else
        {
            DB::table("accounts")->where(array('id' => $account_id))->update(array('enable_live_rec' => '0'));
            DB::table("users_accounts")->where(array('account_id' => $account_id))->update(array('live_recording' => '0'));
        }

        if($input['transcribe_library_videos'])
        {
            DB::table("accounts")->where(array('id' => $account_id))->update(array('transcribe_library_videos' => '1'));

        }
        else
        {
            DB::table("accounts")->where(array('id' => $account_id))->update(array('transcribe_library_videos' => '0'));
        }



        if($input['is_evaluation_activated'])
        {
            DB::table("accounts")->where(array('id' => $account_id))->update(array('is_evaluation_activated' => '1'));

        }
        else
        {
            DB::table("accounts")->where(array('id' => $account_id))->update(array('is_evaluation_activated' => '0'));
        }

        if (isset($input["transcribe_workspace_videos"]) && $input["transcribe_workspace_videos"] > 0) {
            DB::table("accounts")->where(array('id' => $account_id))->update(array('transcribe_workspace_videos' => '1'));
        } else {
            DB::table("accounts")->where(array('id' => $account_id))->update(array('transcribe_workspace_videos' => '0'));
        }

        if (isset($input["transcribe_huddle_videos"]) && $input["transcribe_huddle_videos"] > 0) {
            DB::table("accounts")->where(array('id' => $account_id))->update(array('transcribe_huddle_videos' => '1'));
        } else {
            DB::table("accounts")->where(array('id' => $account_id))->update(array('transcribe_huddle_videos' => '0'));
        }

        if (isset($input["force_auto_delete"]) && $input["force_auto_delete"] > 0) {
            DB::table("accounts")->where(array('id' => $account_id))->update(array('force_auto_delete' => 1));
        } else {
            DB::table("accounts")->where(array('id' => $account_id))->update(array('force_auto_delete' => 0));
        }

        if (isset($input["allow_auto_delete_on_mobile"]) && $input["allow_auto_delete_on_mobile"] > 0) {
            DB::table("accounts")->where(array('id' => $account_id))->update(array('allow_auto_delete_on_mobile' => 1));
        } else {
            DB::table("accounts")->where(array('id' => $account_id))->update(array('allow_auto_delete_on_mobile' => 0));
        }
        $custom_tags = json_decode(json_encode($input['custom_marker_tags']), True);

        $assessment_ratings = json_decode(json_encode($input['assessment_ratings']), True);

        $users_privileges_check = json_decode(json_encode($input['users_privileges_check']), True);

        $admins_privileges_check = json_decode(json_encode($input['admins_privileges_check']), True);

        foreach ($custom_tags as $tags)
        {
            if(isset($tags['is_new']) && $tags['is_new'])
            {
                $save_tags = array(
                    "account_id" => $account_id,
                    "tag_type" => '1',
                    "tag_title" => $tags['tag_title'],
                    "created_by" => $user_id,
                    "created_date" => date('Y-m-d H:i:s'),
                    "last_edit_by" => $user_id,
                    "last_edit_date" => date('Y-m-d H:i:s')
                );

                DB::table('account_tags')->insert($save_tags);

            }
            elseif(isset($tags['is_update']) && $tags['is_update'])
            {
                if(empty(trim($tags['tag_title'])))
                {
                    DB::table('account_tags')->where(array('account_tag_id' => $tags['account_tag_id']))->delete();
                }
                $save_tags = array(
                    "account_id" => $account_id,
                    "tag_type" => '1',
                    "tag_title" => $tags['tag_title'] ,
                    "last_edit_by" => $user_id,
                    "last_edit_date" => date('Y-m-d H:i:s')
                );
                DB::table("account_tags")->where(array('account_tag_id' => $tags['account_tag_id']))->update($save_tags);

            }
            elseif(isset($tags['is_delete']) && $tags['is_delete'])
            {
                DB::table('account_tags')->where(array('account_tag_id' => $tags['account_tag_id']))->delete();
            }
        }


        foreach ($assessment_ratings as $rating)
        {
            if(isset($rating['is_new']) && $rating['is_new'])
            {
                $data =  array(
                    'account_id' => $account_id,
                    'meta_data_name' => 'metric_value_' . $rating['rating_name'],
                    'meta_data_value' => $rating['meta_data_value'] ,
                    'created_by' => $user_id,
                    'created_date' => date('Y-m-d H:i:s')
                );
                DB::table('account_meta_data')->insert($data);

            }
            elseif(isset($rating['is_update']) && $rating['is_update'])
            {
                $data = array(
                    'account_id' => $account_id,
                    'meta_data_name' => 'metric_value_' . $rating['rating_name'] ,
                    'created_by' => $user_id,
                    'created_date' =>  date('Y-m-d H:i:s')
                );
                DB::table("account_meta_data")->where(array('account_meta_data_id' => $rating['account_meta_data_id']))->update($data);

            }
            elseif(isset($rating['is_delete']) && $rating['is_delete'])
            {
                DB::table('account_meta_data')->where(array('account_meta_data_id' => $rating['account_meta_data_id']))->delete();
            }

        }

        $extraPermission = array(
                    'folders_check' => $users_privileges_check['folders_check'] ? '1' : '0',
                    'permission_video_library_upload' => $users_privileges_check['permission_video_library_upload'] ? '1' : '0',
                    'permission_access_video_library' => $users_privileges_check['permission_access_video_library'] ? '1' : '0',
                    'permission_view_analytics' => $users_privileges_check['permission_view_analytics'] ? '1' : '0',
                    'permission_administrator_user_new_role' => $users_privileges_check['permission_administrator_user_new_role'] ? '1' : '0',
                    'parmission_access_my_workspace' => $users_privileges_check['parmission_access_my_workspace'] ? '1' : '0',
                    'manage_collab_huddles' => $users_privileges_check['manage_collab_huddles'] ? '1' : '0',
                    'manage_coach_huddles' => $users_privileges_check['manage_coach_huddles'] ? '1' : '0',
                    'manage_evaluation_huddles' => $users_privileges_check['manage_evaluation_huddles'] ? '1' : '0',
                    'permission_view_analytics' => $users_privileges_check['permission_view_analytics'] ? '1' : '0',
                    'permission_start_synced_scripted_notes' => $users_privileges_check['permission_start_synced_scripted_notes'] ? '1' : '0',
                    'huddle_to_workspace' => $users_privileges_check['huddle_to_workspace'] ? '1' : '0',
                    'permission_video_library_download' => $users_privileges_check['permission_video_library_download'] ? '1' : '0',
                    'permission_video_library_comments' => $users_privileges_check['permission_video_library_comments'] ? '1' : '0',
                    'permission_share_library' => $users_privileges_check['permission_share_library'] ? '1' : '0',
                    'permission_maintain_folders' => '0'
            );
            
        DB::table("users_accounts")->where(array('account_id' => $account_id, 'role_id' => 120))->update($extraPermission); 
            
         $adminExtraPermission = array(
                    'permission_video_library_upload' => $admins_privileges_check['admin_permission_video_library_upload'] ? '1' : '0',
                    'permission_view_analytics' => $admins_privileges_check['admin_view_analytics'] ? '1' : '0',
                    'permission_administrator_user_new_role' => $admins_privileges_check['admin_permission_administrator_user_new_role'] ? '1' : '0',
                    'permission_video_library_download' => $admins_privileges_check['admin_permission_video_library_download'] ? '1' : '0',
                    'permission_video_library_comments' => $admins_privileges_check['admin_permission_video_library_comments'] ? '1' : '0',
                    'permission_access_video_library' => $admins_privileges_check['admin_permission_access_video_library'] ? '1' : '0',
                    'permission_share_library' => $admins_privileges_check['admin_permission_share_library'] ? '1' : '0',
                    'admins_can_view_leaderboard' => $admins_privileges_check['admins_can_view_leaderboard'] ? '1' : '0',
            );
         DB::table("users_accounts")->where(array('account_id' => $account_id, 'role_id' => 115))->update($adminExtraPermission);

        $input['is_force_sso_enabled'] = ($input['is_force_sso_enabled']) ? '1' : '0';
        if(isset($input['is_saml_enabled']) && !empty($input['is_saml_enabled'])){
            $input['is_saml_enabled'] = '1';
        }else{
            $input['is_saml_enabled'] = '0';
            $input['is_force_sso_enabled'] = '0';
        }
        $is_logout_required_for_sso = $this->is_logout_required_for_sso($account_id, $input['is_force_sso_enabled']);
        DB::table("accounts")->where(array('id' => $account_id))->update([
            'company_name' => $input['company_name'],
            "enable_goals" => $enable_goals,
            'is_saml_enabled' => $input['is_saml_enabled'],
            'is_force_sso_enabled' => $input['is_force_sso_enabled'],
            'sso_url' => $input['sso_url'],
            'sso_certificate' => $input['sso_certificate'],
            'idp_entityId' => $input['idp_entityId'],
        ]);
        $this->update_account_meta_data_params($account_id,$user_id,$input['get_analytics_duration_value'],'tracking_duration');
        $this->update_account_meta_data_params($account_id,$user_id,$enable_video_library,'enable_video_library');
        $this->update_account_meta_data_params($account_id,$user_id,$enable_assessment_tracker,'enable_matric');
        $this->update_account_meta_data_params($account_id,$user_id,$enable_coaching_tracker,'enable_tracker');
        $this->update_account_meta_data_params($account_id,$user_id,$input['default_framework'],'default_framework');
        $this->update_account_meta_data_params($account_id,$user_id,$enable_analytics,'enableanalytics');
        $this->update_account_meta_data_params($account_id,$user_id,$enable_analytics_enableleaderboard,'enable_analytics_enableleaderboard');
        $this->update_account_meta_data_params($account_id,$user_id, $input['assessment_perfomance_level'],'assessment_perfomance_level');
        $this->update_account_folders_meta_data_params($account_id,$user_id,$enable_custom_markers_in_account,'enable_tags');
        $this->update_account_folders_meta_data_params($account_id,$user_id,$enable_custom_markers_in_workspace,'enable_tags_ws');
        $this->update_account_folders_meta_data_params($account_id,$user_id,$enable_framework_in_account,'enable_framework_standard');
        $this->update_account_folders_meta_data_params($account_id,$user_id,$enable_framework_in_workspace,'enable_framework_standard_ws');
        //$this->update_account_folders_meta_data_params($account_id,$user_id,$deactive_eval_huddle,'deactive_eval_huddle');

        return response()->json(['success' => true , 'message' => 'You have successfully updated the General Settings.' , 'is_logout_required_for_sso' => $is_logout_required_for_sso ]);
    }

    function is_logout_required_for_sso($account_id, $input_is_force_sso_enabled){
        $existing_force_sso_enabled = Account::where('id', $account_id)->value('is_force_sso_enabled');
        return !is_null($existing_force_sso_enabled) && $existing_force_sso_enabled!=$input_is_force_sso_enabled && $input_is_force_sso_enabled=='1';
    }
    
    function update_account_meta_data_params($account_id, $user_id , $meta_data_value,$meta_data_name) {
            $check_data = AccountMetaData::where(array(
                    "account_id" => $account_id,
                    "meta_data_name" => $meta_data_name
            ))->first();

            if ($check_data) {
                $data = array(
                    'meta_data_value' => $meta_data_value,
                    'last_edit_by' => $user_id,
                    'last_edit_date' => date('Y-m-d H:i:s')
                );
                DB::table("account_meta_data")->where(array('account_id' => $account_id, 'meta_data_name' => $meta_data_name))->update($data);
            } else {

            $data = array(
                'account_id' => $account_id,
                'meta_data_name' => $meta_data_name,
                'meta_data_value' => $meta_data_value,
                'created_by' => $user_id,
                'created_date' => date('Y-m-d H:i:s')
            );
            DB::table('account_meta_data')->insert($data);
        }
        return true;

    }

    function update_account_folders_meta_data_params($account_id, $user_id , $meta_data_value,$meta_data_name) {


        $check_data = AccountFolderMetaData::where(array(
            "account_folder_id" => $account_id,
            "meta_data_name" => $meta_data_name
        ))->first();


        if ($check_data) {
            $data = array(
                'meta_data_value' => $meta_data_value,
                'last_edit_by' => $user_id,
                'last_edit_date' => date('Y-m-d H:i:s')
            );
            DB::table("account_folders_meta_data")->where(array('account_folder_id' => $account_id, 'meta_data_name' => $meta_data_name))->update($data);
        } else {

            $data = array(
                'account_folder_id' => $account_id,
                'meta_data_name' => $meta_data_name,
                'meta_data_value' => $meta_data_value,
                'created_by' => $user_id,
                'created_date' => date('Y-m-d H:i:s')
            );
            DB::table('account_folders_meta_data')->insert($data);
        }
        return true;

    }


    function accountOwnersList($account_id) {
        $result = User::getUsersByRole($account_id, array('110', '100'));
        $accountOwnerList = array();
        if ($result) {
            foreach ($result as $row) {
                $accountOwnerList[$row->id] = $row->first_name . " " . $row->last_name . " (" . HelperFunctions::get_user_role_name($row->role_id,$this->lang) . ")";
            }
        }

        return $accountOwnerList;
    }

    function account_users_storage_stats(Request $request)
    {
        $input = $request->all();
        $account_id = $input['account_id'];
        $myAccount = Account::where(array('id' => $account_id ))->first();

        $storageUsed = $myAccount->storage_used;

        $allowed_users = HelperFunctions::get_allowed_users($account_id);
        $allowed_storage = HelperFunctions::get_allowed_storage($account_id);
        $total_users = User::getTotalUsers($account_id);

        $storageUsed = $storageUsed / 1024;
        $storageUsed = $storageUsed / 1024;
        $storageUsed = $storageUsed / 1024;
        $total_storage_used = $storageUsed;

        $final_array = array(
            'allowed_users' => $allowed_users,
            'allowed_storage' => $allowed_storage,
            'total_users_used' => $total_users,
            'total_storage_used' => round($total_storage_used)
        );

        return response()->json(['success' => true , 'stats' => $final_array ]);

    }

    function changeAccountOwner(Request $request) {

        $input = $request->all();
        $account_owner_user_id = $input['account_owner_user_id'];
        if(isset($input['user_id']))
        {
            $super_admin_user_id = $input['user_id'];
        }
        else
        {
            $super_admin_user_id = $account_owner_user_id;
        }
        $account_id = $input['account_id'];
        $language_based_content = TranslationsManager::get_page_lang_based_content('flash_messages', $this->site_id, '', $this->lang);
        if ($account_owner_user_id == '' || $super_admin_user_id == '') {
            $final_data = array('message' => $language_based_content['please_select_AO_SU_role_swaping_msg'] , 'success' => false );
            return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
        }

        $account_owner_role = UserAccount::where( array('account_id' => $account_id, 'user_id' => $account_owner_user_id))->first();
        if ($account_owner_role->role_id != '100') {
            $final_data = array('message' => $language_based_content['only_account_owner_can_swap_roles_msg'] , 'success' => false );
            return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
        }
        if ($account_owner_user_id == $super_admin_user_id ) {
            $final_data = array('message' => 'You are already an Account Owner.' , 'success' => false );
            return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
        }


        //super_admin_user_id
        $super_admin_role = UserAccount::where( array('account_id' => $account_id, 'user_id' => $super_admin_user_id))->first();
        $data1 = array(
            'role_id' =>  $super_admin_role->role_id ,
            'permission_maintain_folders' => '1',
            'permission_access_video_library' => '1',
            'permission_video_library_upload' => '1',
            'permission_administrator_user_new_role' => '1',
            'manage_collab_huddles' => '1',
            'manage_coach_huddles' => '1',
            'folders_check' => '1',
            'last_edit_date' => date('Y-m-d H:i:s')
        );


        $affected_rows = DB::table("users_accounts")->where(array('user_id' => $account_owner_user_id, 'account_id' => $account_id))->update($data1);
        if ($affected_rows > 0) {
            $data2 = array(
                'role_id' =>  $account_owner_role->role_id ,
                'permission_maintain_folders' => '1',
                'permission_access_video_library' => '1',
                'permission_video_library_upload' => '1',
                'permission_administrator_user_new_role' => '1',
                'manage_collab_huddles' => '1',
                'manage_coach_huddles' => '1',
                'folders_check' => '1',
                'last_edit_date' =>  date('Y-m-d H:i:s')
            );
            $affected_rows = DB::table("users_accounts")->where(array('user_id' => $super_admin_user_id, 'account_id' => $account_id))->update($data2);
            if ($affected_rows > 0) {
                $final_data = array('message' => $language_based_content['account_owner_changed_successfully_msg'] , 'success' => true );
                return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
            } else {

                $data1 = array(
                    'role_id' => $account_owner_role['UserAccount']['role_id'] ,
                    'last_edit_date' =>  date('Y-m-d H:i:s')
                );
                $affected_rows = DB::table("users_accounts")->where(array('user_id' => $account_owner_user_id, 'account_id' => $account_id))->update($data1);
                $final_data = array('message' => $language_based_content['please_select_different_person_msg'] , 'success' => false );
                return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
            }
        } else {
            $final_data = array('message' => $language_based_content['please_select_different_person_msg'] , 'success' => false );
            return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);

        }

    }

    function colorsAndLogo(Request $request) {
        $language_based_content = TranslationsManager::get_page_lang_based_content('flash_messages', $this->site_id, '', $this->lang);
        $input = $request->all();

        $account_id = $input['account_id'];
        $data = array(
            'updated_at' =>  date('Y-m-d H:i:s')
        );
        if ($input['header_background_color'] != '') {
            $data['header_background_color'] =  $input['header_background_color'];
        }
        if ($input['nav_bg_color'] != '') {
            $data['usernav_bg_color'] = $input['nav_bg_color'] ;
        }


        $image_url = '';
        if(isset($_FILES['image_logo']))
        {
            $file = $_FILES['image_logo'];
            $fileName = $this->upload_company_logo($file, $account_id);
            if ($fileName) {
                $data['image_logo'] =  $fileName ;
                $image_url = "https://s3.amazonaws.com/sibme.com/static/companies/$account_id/$fileName";
            }

        }
        else
        {
            $fileName = '';
            $image_url = '';
        }

        $affected_rows = Account::where(array('id' => $account_id))->update($data);
        if ($affected_rows > 0) {
            $final_data = array('message' => $language_based_content['you_have_successfully_changed_your_info_msg'] , 'success' => true , 'image' => $fileName , 'image_url' => $image_url );
            return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
        } else {
            $final_data = array('message' => $language_based_content['your_info_changing_failed_msg'] , 'success' => false , 'image' => '', 'image_url' => '' );
            return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
        }
    }

    function upload_company_logo($file, $account_id) {

        if (isset($file['error']) && $file['error'] == 0) {
            $maxDim = 250;
            list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);
            if ($width > $maxDim || $height > $maxDim) {
                $target_filename = $file['tmp_name'];
                $fn = $file['tmp_name'];
                $size = getimagesize($fn);
                $ratio = $size[0] / $size[1]; // width/height
                if ($ratio > 1) {
                    $width = $maxDim;
                    $height = $maxDim / $ratio;
                } else {
                    $width = $maxDim * $ratio;
                    $height = $maxDim;
                }
                $src = imagecreatefromstring(file_get_contents($fn));
                $dst = imagecreatetruecolor($width, $height);
                imagecopyresampled($dst, $src, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
                imagedestroy($src);
                imagepng($dst, $target_filename); // adjust format as needed
                imagedestroy($dst);
            }
            $fileName =   $file['name'];
            $fileTempName = $file['tmp_name'];
            $fileType = $file['type'];

            // $bucket_name = Configure::read('bucket_name_cdn');

            $image_path = "static/companies/" . $account_id . "/" . $fileName;
            $keyname = $image_path;

            // Instantiate the client.
            $client = new S3Client([
                'region' => 'us-east-1',
                'version' => 'latest',
                'credentials' => array(
                    'key' => config('s3.access_key_id'),
                    'secret' => config('s3.secret_access_key'),
                )
            ]);

            $result = $client->putObject([
                'Bucket' => 'sibme.com',
                'Key' => $keyname,
                'SourceFile' => $fileTempName,
                'ContentType' => $fileType,
                'ACL' => 'public-read'
            ]);

            /*
                        //Read Cloudfront Private Key Pair
                        $cloudFront = new CloudFrontClient([
                            'region' => 'us-east-1',
                            'version' => 'latest',
                            'credentials' => array(
                                'key' => config('s3.access_key_id'),
                                'secret' => config('s3.secret_access_key'),
                            )
                        ]);
            */
            return $fileName;
        } else {
            // $this->Session->setFlash(__($this->language_based_messages['please_upload_file_msg'], true));
            return FALSE;
        }
    }


    public function get_unarchive_huddles(Request $request) {
        $input = $request->all();
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $role_id = $input['role_id'];
        $page = $input['page'];
        $user_current_account = $input['user_current_account'];
        $folder_id = (isset($input['folder_id']) && !empty($input['folder_id'])) ? $input['folder_id'] : false;
        $title = (isset($input['title']) && !empty($input['title'])) ? $input['title'] : "";
        $huddle_sort = (isset($input['huddle_sort']) && !empty($input['huddle_sort'])) ? $input['huddle_sort'] : '0';
        $huddle_type = (isset($input['huddle_type']) && !empty($input['huddle_type'])) ? $input['huddle_type'] : '0';

        if(isset($input['start_date']) && !empty($input['start_date']))
        {
            $start_date = @$input['start_date'];
            $stDate = date_create($start_date);
            $start_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        }
        else
        {
            $start_date = '';
        }

        if(isset($input['end_date']) && !empty($input['end_date']))
        {
            $end_date = @$input['end_date'];
            $endDate = date_create($end_date);
            $end_date = date_format($endDate, 'Y-m-d') . ' 23:59:59';
        }

        else
        {
            $end_date = '';
        }

        if ($folder_id) {
            $title = '';
            $page = 1;
            $start_date = '';
            $end_date = '';
//            $folder_data = AccountFolder::where(array('account_folder_id' => $folder_id, 'site_id' => $this->site_id))->first();
//            if ($folder_data) {
//                $folder_data = $folder_data->toArray();
//                if (!(AccountFolder::isUserParticipatingInHuddle($folder_id, $account_id, $user_id, $this->site_id) || $folder_data['created_by'] == $user_id)) {
//
//                    $final_data = array(
//                        'message' => TranslationsManager::get_translation('Folder_is_not_accessible', 'Api/huddle_list', $this->site_id),
//                        'success' => false
//                    );
//
//                    return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
//                }
//            } else {
//
//                $final_data = array(
//                    'message' => TranslationsManager::get_translation('Folder_is_not_accessible', 'Api/huddle_list', $this->site_id),
//                    'success' => false
//                );
//
//                return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
//            }
        }


        $limit = 10;
        //$limit = 300;

        $final_data = [];

        $accountFolderUsers = AccountFolderUser::join('account_folders', 'AccountFolderUser.account_folder_id', '=', 'account_folders.account_folder_id')
            ->selectRaw("AccountFolderUser.*,folder_type,is_published")
            ->selectRaw("(SELECT meta_data_value FROM account_folders_meta_data as afmd WHERE afmd.account_folder_id=AccountFolderUser.account_folder_id AND meta_data_name = 'folder_type' LIMIT 1) as htype")
            ->where('user_id', $user_id)->where('AccountFolderUser.site_id', $this->site_id)->get();
        //$accountFolderUsers = AccountFolderUser::where('user_id', $user_id)->where('site_id', $this->site_id)->get();
        $accountFolderGroups = UserGroup::get_user_group($user_id, $this->site_id);
        // $userPermissions = AccountFolderUser::where('user_id', $user_id)->where('account_folder_id', $account_id)->get();
        $accountFoldereIds = array();
        //dd($accountFolderUsers);
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                //$htype =  $this->get_huddle_type($row->account_folder_id);
                //dd($htype);
                // Excluded unpublished assessment huddles for assessees
                if($row->htype == '3' && $row->folder_type == '1' && $row->role_id != '200' &&  $row->is_published == '0') {
                    continue;
                }
                $accountFoldereIds[] = $row->account_folder_id;
            }
            // $accountFoldereIds = "'" . implode("','", $accountFoldereIds) . "'";
        } else {
            $accountFoldereIds = '0';
        }
        // $accountFolderGroupsIds = '';
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_id'];
            }
            // $accountFolderGroupsIds = "'" . implode("','", $accountFolderGroupsIds) . "'";
        } else {
            $accountFolderGroupsIds = '0';
        }

        if (!empty($title)) {
            $title = str_replace('"', '""', $title);
            $search_bool = 1;
        } else {
            $search_bool = 0;
        }

        if(isset($input['switch']) && $input['switch'] == 'archived' )
        {
            $folders = AccountFolder::getAllAccountUnArchiveFolders($this->site_id, $account_id, FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id, '/root/', $search_bool, $title, $huddle_sort,'archived',$start_date,$end_date);
            $huddles = AccountFolder::getAllAccountUnArchiveHuddles($this->site_id, $account_id, '', FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id, $search_bool, $limit, $page, false, $title, $huddle_sort, $huddle_type, $role_id,'archived',$start_date,$end_date);
            $huddles_count = AccountFolder::getAllAccountUnArchiveHuddles($this->site_id, $account_id, '', FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id, $search_bool, $limit, $page, true, $title, $huddle_sort, $huddle_type, $role_id,'archived',$start_date,$end_date);

        }

        else
        {

            $folders = AccountFolder::getAllAccountUnArchiveFolders($this->site_id, $account_id, FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id, '/root/', $search_bool, $title, $huddle_sort,'unarchived',$start_date,$end_date);
            $huddles = AccountFolder::getAllAccountUnArchiveHuddles($this->site_id, $account_id, '', FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id, $search_bool, $limit, $page, false, $title, $huddle_sort, $huddle_type, $role_id,'unarchived',$start_date,$end_date);
            $huddles_count = AccountFolder::getAllAccountUnArchiveHuddles($this->site_id, $account_id, '', FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id, $search_bool, $limit, $page, true, $title, $huddle_sort, $huddle_type, $role_id,'unarchived',$start_date,$end_date);

        }

        // dd($huddles);

        if ($folders && $page == '1' && $huddle_type == '0') {
            $accessible_folders = array();
            foreach ($folders as $folder) {
                //   if (AccountFolder::isUserParticipatingInHuddle($folder->account_folder_id, $account_id, $user_id, $this->site_id) || $folder->created_by == $user_id) {
                $stats = [];
                // Get Subfolders Count in this Folder
                $stats ['folders'] = AccountFolder::count_folders_in_folder($folder->account_folder_id, $user_id, $account_id, $this->site_id);
                // Get Collaboration Huddles Count in this Folder
                $stats ['collaboration'] = AccountFolder::count_coaching_colab_huddles_in_folder($folder->account_folder_id, $user_id, 1, $this->site_id);
                // Get Coaching Huddles Count in this Folder
                $stats ['coaching'] = AccountFolder::count_coaching_colab_huddles_in_folder($folder->account_folder_id, $user_id, 2, $this->site_id);
                // Get Assessment Huddles Count in this Folder
                $stats ['assessment'] = AccountFolder::count_coaching_colab_huddles_in_folder($folder->account_folder_id, $user_id, 3, $this->site_id);

                $folder->stats = $stats;
                $folder->folder_id = $folder->account_folder_id;
                $folder->title = $folder->name;
                $creator = app('App\Http\Controllers\HuddleController')->is_creator($folder->created_by, $user_id);

                $user_permissions = UserAccount::where(array('user_id' => $user_id, 'account_id' => $account_id, 'site_id' => $this->site_id))->first();
                if ($user_permissions) {
                    $user_permissions = $user_permissions->toArray();
                    if ($creator && ($user_permissions['folders_check'] == '1')) {
                        $folder_permissions = true;
                    } else {
                        $folder_permissions = false;
                    }
                } else {
                    $folder_permissions = false;
                }

                $folder->created_date = self::getCurrentLangDate(strtotime($folder->created_date), "archive_module"); //date('M d, Y', strtotime($folder->created_date));
                $folder->last_edit_date = self::getCurrentLangDate(strtotime($folder->last_edit_date), "archive_module_folders"); //date('M d, Y', strtotime($folder->last_edit_date));

                $folder->folder_permissions = $folder_permissions;
                if(isset($input['switch']) && $input['switch'] == 'archived' )
                {
                    $folders = AccountFolder::getAllAccountUnArchiveFolders($this->site_id, $account_id, FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder->account_folder_id, '/root/', $search_bool, $title, $huddle_sort,'archived',$start_date,$end_date);
                    $huddles_count_child = AccountFolder::getAllAccountUnArchiveHuddles($this->site_id, $account_id, '', FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder->account_folder_id, $search_bool, $limit, $page, true, $title, $huddle_sort, $huddle_type, $role_id,'archived',$start_date,$end_date);
                    $child_count = count($folders) + $huddles_count_child ; //AccountFolder::count_folders_in_folder_archive($folder->account_folder_id, $user_id, $account_id, $this->site_id,'archive');
                }
                else {
                    $folders = AccountFolder::getAllAccountUnArchiveFolders($this->site_id, $account_id, FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder->account_folder_id, '/root/', $search_bool, $title, $huddle_sort,'unarchived',$start_date,$end_date);
                    $huddles_count_child = AccountFolder::getAllAccountUnArchiveHuddles($this->site_id, $account_id, '', FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder->account_folder_id, $search_bool, $limit, $page, true, $title, $huddle_sort, $huddle_type, $role_id,'unarchived',$start_date,$end_date);
                    $child_count = count($folders) + $huddles_count_child ; //AccountFolder::count_folders_in_folder_archive($folder->account_folder_id, $user_id, $account_id, $this->site_id,'unarchive');
                }
                $folder->child_count = $child_count;
                //$folder['videos'] = $this->AccountFolder->getFolderVideos($folder['AccountFolder']['account_folder_id']);


                if (!$search_bool && empty($folder_id))
                {
                    if(isset($input['switch']) && $input['switch'] == 'archived' )
                    {
                        if($this->unarchive_parent_pass($folder['account_folder_id']))
                        {
                            $accessible_folders[] = $folder;
                        }
                    }

                    else
                    {
                        if($this->archive_parent_pass($folder['account_folder_id']))
                        {
                            $accessible_folders[] = $folder;
                        }

                    }
                }
                else {

                    $accessible_folders[] = $folder;
                }


                //   }
            }

            $folders = $accessible_folders;
            if ($role_id != 125) {
                $final_data['folders'] = $folders;
                // $total_huddles = $huddles_count;
            }


        } else {

            $folders = array();
        }

        if ($huddles) {
            $huddles_temp = array();
            foreach ($huddles as $key => $huddle) {
                // dd($huddle->v_total);
                // echo $huddle->folderType . "<br>";
                $item = array();
                if ($huddle->folderType == 'assessment') {
                    // get total video count
                    if (AccountFolderUser::check_if_evalutor($this->site_id, $huddle->account_folder_id, $user_id) && $role_id == 120) {
                        $participants_ids = AccountFolderUser::get_participants_ids($huddle->account_folder_id, 210, $this->site_id);
                        $huddles[$key]['total_videos'] = Document::countVideosEvaluator($this->site_id, $huddle->account_folder_id, $user_id, 1, $participants_ids);
                    } elseif (AccountFolderUser::check_if_evalutor($this->site_id, $huddle->account_folder_id, $user_id) && in_array($role_id, [110, 100, 115])) {
                        $huddles[$key]['total_videos'] = $huddle->v_total;
                    } else {
                        $evaluators_ids = AccountFolderUser::get_participants_ids($huddle->account_folder_id, 200, $this->site_id);
                        $huddles[$key]['total_videos'] = Document::countVideosEvaluator($this->site_id, $huddle->account_folder_id, $user_id, 1, $evaluators_ids);
                    }

                    // get total resources count
                    $total_resources = 0;
                    if (AccountFolderUser::check_if_evalutor($this->site_id, $huddle->account_folder_id, $user_id)) {
                        //  $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                        $huddles[$key]['total_docs'] = $huddle->doc_total;
                        // echo("Docs:". $huddle->doc_total . "<br>");
                    } else {

                        $participants_ids = AccountFolderUser::get_participants_ids($huddle->account_folder_id, 210, $this->site_id);
                        $evaluators_ids = AccountFolderUser::get_participants_ids($huddle->account_folder_id, 200, $this->site_id);
                        // $total_resources = Document::countVideosEvaluator($this->site_id, $huddle->account_folder_id, $user_id, 2, $evaluators_ids);
                        $total_resources = Document::countVideosEvaluator($this->site_id, $huddle->account_folder_id, $user_id, 2);
                        $huddles[$key]['total_docs'] = $total_resources;
                        /*
                          if ($total_participant_videos) {
                          foreach ($total_participant_videos as $row) {

                          if ($row['Document']['created_by'] == $user_id || (isset($evaluators_ids) && is_array($evaluators_ids) && in_array($row['Document']['created_by'], $evaluators_ids))) {

                          $total_count_r = count($this->Document->getVideoDocumentsByVideo($row['Document']['id'], $huddles[$i]['AccountFolder']['account_folder_id']));


                          $total_resources = $total_resources + $total_count_r;
                          }
                          }
                          }
                         */

// echo("Resources:".__LINE__. " - " . $total_resources)."<br>";
                        /*
                                                if (AccountFolderUser::check_if_evalutor($this->site_id, $huddle->account_folder_id, $user_id)) {
                                                    $huddles[$key]['total_docs'] = $total_resources;
                                                } else {
                                                    // $huddles[$key]['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                                                    $huddles[$key]['total_docs'] = Document::countAsseseeDocuments($huddle->account_folder_id, $this->site_id);
                                                }
                        */
                    }
                } else {
                    $huddles[$key]['total_videos'] = $huddle->v_total;



                    //  $huddles[$i]['AccountFolder']['total_docs'] = $this->Document->countDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                    if (AccountFolderUser::check_if_evalutor($this->site_id, $huddle->account_folder_id, $user_id)) {
                        $huddles[$key]['total_docs'] = $huddle->doc_total;
                    } else {
                        // $huddles[$key]['total_docs'] = $this->Document->countAsseseeDocuments($huddles[$i]['AccountFolder']['account_folder_id']);
                        $huddles[$key]['total_docs'] = Document::countAsseseeDocuments($huddle->account_folder_id, $this->site_id);
                    }
                }

                $huddle_users = AccountFolder::getHuddleUsers($huddle->account_folder_id, $this->site_id);

                $huddle_group_users = AccountFolder::getHuddleGroupsUsers($huddle->account_folder_id, $this->site_id);

                $participants_arranged = array();

                if (!empty($huddle_groups)) {

                    foreach ($huddle_groups as $huddle_group) {

                        $participants_arranged[] = array(
                            'user_id' => $huddle_group['id'],
                            'role_id' => $huddle_group['role_id'],
                            'user_name' => $huddle_group['first_name'] . ' ' . $huddle_group['last_name'],
                            'image' => $huddle_group['image'],
                            'user_email' => $huddle_group['email']
                        );
                    }
                }
                $participants = array();
                $huddle_name ='';
                $huddle_name_type = '';
                if (!empty($huddle_users)) {

                    foreach ($huddle_users as $huddle_user) {
                        $participants_arranged[] = array(
                            'user_id' => $huddle_user['user_id'],
                            'role_id' => $huddle_user['role_id'],
                            'user_name' => $huddle_user['first_name'] . ' ' . $huddle_user['last_name'],
                            'image' => $huddle_user['image'],
                            'user_email' => $huddle_user['email']
                        );
                    }

                   
                    $huddle_name_type = TranslationsManager::get_translation('huddle_list_collaboration', $this->module_url, $this->site_id, $user_id);
                    $huddle_name = 'collaboration';
                    if ($huddle->folderType == 'assessment') {
                        $role_200 = 'assessor';
                        $role_210 = 'assessed_participants';
                        $role_220 = '';
                        $huddle_name_type = TranslationsManager::get_translation('huddle_list_assessment', $this->module_url, $this->site_id, $user_id);
                        $huddle_name = 'assessment';
                    } elseif ($huddle->folderType == 'coaching') {

                        $role_200 = 'coach';
                        $role_210 = 'coachee';
                        $role_220 = '';
                        $huddle_name_type = TranslationsManager::get_translation('coaching_list_coaching', $this->module_url, $this->site_id, $user_id);
                        $huddle_name = 'coaching';
                    } else {

                        $role_200 = 'collaboration_participants';
                        $role_210 = 'collaboration_participants';
                        $role_220 = 'collaboration_participants';
                        $huddle_name_type = TranslationsManager::get_translation('huddle_list_collaboration', $this->module_url, $this->site_id, $user_id);
                        $huddle_name = 'collaboration';
                    }

                    foreach ($participants_arranged as $row) {
                        if ($row['role_id'] == '200') {
                            $row['role_name'] = 'Admin';
                            $participants[$role_200][] = $row;
                        } elseif ($row['role_id'] == '210') {
                            $row['role_name'] = 'Member';
                            $participants[$role_210][] = $row;
                        } else {
                            $row['role_name'] = 'Viewer';
                            $participants[$role_220][] = $row;
                        }
                    }
                }

                // $huddle_participants = AccountFolder::getHuddleUsersIncludingGroups($huddle->account_folder_id, $this->site_id, $user_id, $role_id, true);

                $stats = array();

                $user_huddle_role = app('App\Http\Controllers\HuddleController')->has_admin_access($huddle_users, $huddle_group_users, $user_id);
                $creator = app('App\Http\Controllers\HuddleController')->is_creator($huddle->created_by, $user_id);

                if ($user_huddle_role == 200 || $creator) {
                    $huddle_permissions = true;
                } else {
                    $huddle_permissions = false;
                }

                $stats['videos'] = $huddles[$key]['total_videos'];
                $stats['attachments'] = $huddles[$key]['total_docs'];


                $huddle->participants = $participants; //$huddle_participants['participants'];
                $huddle->stats = $stats;
                $huddle->type = $huddle_name; //$huddle_participants['huddle_name'];
                $huddle->huddle_type = $huddle_name_type; //$huddle_participants['huddle_name_type'];
                $huddle->title = $huddles[$key]['name'];
                $huddle->created_on = self::getCurrentLangDate(strtotime($huddles[$key]['created_date']), "archive_module"); //date('M d, Y', strtotime($huddles[$key]['created_date']));
                $huddle->modified_on = self::getCurrentLangDate(strtotime($huddles[$key]['last_edit_date']), "archive_module");
                $huddle->last_edit_date = self::getCurrentLangDate(strtotime($huddles[$key]['last_edit_date']), "archive_module"); //date('M d, Y', strtotime($huddles[$key]['last_edit_date']));
                $huddle->created_by = app('App\Http\Controllers\HuddleController')->get_user_name_from_id($huddles[$key]['created_by']);
                $huddle->huddle_id = $huddles[$key]['account_folder_id'];
                $huddle->huddle_permissions = $huddle_permissions;
                $huddle->role_id = app('App\Http\Controllers\HuddleController')->get_huddle_role($huddle->account_folder_id, $user_id);

                if ( (!$search_bool && ($huddle_type != '1' && $huddle_type != '2' && $huddle_type != '3' ) ) && empty($folder_id) )
                {
                    if(isset($input['switch']) && $input['switch'] == 'archived' )
                    {
                        if($this->unarchive_parent_pass($huddle['account_folder_id']))
                        {
                            $huddles_temp[] = $huddle;
                        }
                    }

                    else
                    {
                        if($this->archive_parent_pass($huddle['account_folder_id']))
                        {
                            $huddles_temp[] = $huddle;
                        }

                    }
                }
                else {

                    $huddles_temp[] = $huddle;
                }



            }

            $huddles = $huddles_temp;

            $folder_create_permission = app('App\Http\Controllers\HuddleController')->folder_create_permission($user_current_account);
            $huddle_create_permission = app('App\Http\Controllers\HuddleController')->huddle_create_permission($user_current_account);

            $final_data['huddles'] = $huddles;
            $final_data['total_huddles'] = $huddles_count;
            $final_data['folder_create_permission'] = $folder_create_permission;
            $final_data['huddle_create_permission'] = $huddle_create_permission;
            //$final_data['folders_count_for_move'] = $this->treeview_detail_function($account_id, $user_id);
            $final_data['folders_count_for_move'] =  2;
            $final_data['success'] = true;



            // dd("....Ending....",$huddles);
            /*
              $all_huddles = array();
              if ($huddles && $role_id == 125) {
              foreach ($huddles as $row) {
              if ($row[0]['folderType'] == 'collaboration' || $row[0]['folderType'] == '') {
              $all_huddles[] = $row;
              }
              }
              } else {
              $all_huddles = $huddles;
              }

             */
            /*
              if ($role_id == 125) {
              $total_huddles = $all_huddles;
              }
             */
        }
        $user_detail = User::where(array('id' => $user_id ))->first();
        $final_data['gs_archiving'] = $user_detail->gs_archiving;
        return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);
    }

    function get_colors_logos_api(Request $request)
    {
        $input = $request->all();
        $account_id  = $input['account_id'];

        $account_detail = Account::where(array('id' => $account_id))->first();

        if($account_detail)
        {
            $account_detail = $account_detail->toArray();
        }
        else {

            $account_detail = array();
        }

        $globel_background_color = '#808184';
        if ($this->site_id == 1) {
            $globel_background_color = '#336699';
        }

        $final_array = array(
            'header_background_color' => isset($account_detail['header_background_color']) ? '#' . $account_detail['header_background_color'] : $globel_background_color ,
            'usernav_bg_color' => isset($account_detail['usernav_bg_color']) ? '#' . $account_detail['usernav_bg_color'] : '#fff'
        );

        return $final_array;

    }

    function reset_colors_logos(Request $request) {
        $input = $request->all();

        $account_id = $input['account_id'];

        $data = array(
            'updated_at' =>  date('Y-m-d H:i:s')
        );
        if ($this->site_id == 1) {
            $data['header_background_color'] = "004061";
        } else {
            $data['header_background_color'] = "7b7e81";
        }

        $data['nav_bg_color'] = "668dab";
        $data['image_logo'] = "";
        $data['usernav_bg_color'] = "fff";

        $affected_rows = Account::where(array('id' => $account_id))->update($data);


        if ($affected_rows > 0) {
            $final_array = array(
                'message' => TranslationsManager::get_translation('you_have_successfully_changed_your_info_msg', 'flash_messages', $this->site_id),
                'success' => true,
                'header_background_color' => '#'.$data['header_background_color'],
                'nav_bg_color' => '#fff'
            );

            return response()->json($final_array, 200, [], JSON_PRETTY_PRINT);

        } else {
            $final_array = array(
                'message' => TranslationsManager::get_translation('your_info_changing_failed_msg', 'flash_messages', $this->site_id),
                'success' => true
            );

            return response()->json($final_array, 200, [], JSON_PRETTY_PRINT);

        }
    }


    function global_export(Request $request) {
        $input = $request->all();
        $account_id = $input['account_id'];
        $data = [];
        $data['account_id'] = $account_id;
        $account_owner_email = $this->_get_account_owner_email($account_id);

        if($account_owner_email['success'] == true)
        {
            $data['account_owner_email'] = $account_owner_email['email'];
        }
        else {

            return response()->json($account_owner_email, 200, [], JSON_PRETTY_PRINT);
        }

        $data['export_type'] = $input['export_type'];
        $data['file_type'] = isset($input['file_type']) && !empty($input['file_type']) ? $input['file_type'] : "csv";

        if ($data['export_type'] == "general") {
            $export_fields = $input['export_fields'];

            $export_fields_array = array();

            if($export_fields['comment'] == 1)
            {
                $export_fields_array[] = 'comment';
            }

            if($export_fields['time_stamp'] == 1)
            {
                $export_fields_array[] = 'time_stamp';
            }

            if($export_fields['date_stamp'] == 1)
            {
                $export_fields_array[] = 'date_stamp';
            }

            if($export_fields['framework_name'] == 1)
            {
                $export_fields_array[] = 'framework_name';
            }

            if($export_fields['tagged_standards'] == 1)
            {
                $export_fields_array[] = 'tagged_standards';
            }

            if($export_fields['custom_marker_tags'] == 1)
            {
                $export_fields_array[] = 'custom_marker_tags';
            }

            if($export_fields['attachment_file_names'] == 1)
            {
                $export_fields_array[] = 'attachment_file_names';
            }

            $data['export_query'] = $this->_format_general_export_query($export_fields_array, $account_id);
        } elseif ($data['export_type'] == "performance_level") {
            $data['export_query'] = $this->_format_performance_level_export_query($account_id);
        }

        $this->_create_global_export_request($data);
        $final_result = array(
            'message' => $this->language_based_messages['the_global_export_request'] ,
            'success' => true
        );

        return response()->json($final_result, 200, [], JSON_PRETTY_PRINT);
    }


    function _get_account_owner_email($account_id){


        $result = User::join('users_accounts', 'users.id', '=', 'users_accounts.user_id')
            ->select("email")
            ->where(array('users_accounts.account_id' => $account_id, 'users_accounts.role_id' => 100))->where('users_accounts.site_id', $this->site_id)->first();


        if(empty($result['email'])){

            $final_array = array(
                'message' => false,
                'message' => "Account Owner Email not found"
            );

            return $final_array;

        }

        $final_array = array(
            'success' => true,
            'email' => $result['email'],
            'message' => ''
        );


        return $final_array;

    }


    function _format_general_export_query($fields=[], $account_id){
        $query = "
        (SELECT 
        accounts.`company_name` AS 'Account Name', 
        accounts.`id` AS 'Accout-GUID', 
        account_folders.`name` AS 'Huddle Name', 
        account_folders.`account_folder_id` AS 'Huddle-GUID', 
        account_folder_documents.`title` AS 'Video Name',
        account_folder_documents.`document_id` AS 'Video-GUID',
        users.`id` AS 'User-GUID', 
        users.`institution_id` AS 'Institution ID',
        users.`username` AS 'Username', 
        users.`first_name` AS 'First Name', 
        users.`last_name` AS 'Last Name', 
         
        (	SELECT 
                ( CASE
                    WHEN account_folders_meta_data.meta_data_value='1' AND role_id=200 THEN 'admin'
                    WHEN account_folders_meta_data.meta_data_value='1' AND role_id=210 THEN 'member'
                    WHEN account_folders_meta_data.meta_data_value='1' AND role_id=220 THEN 'viewer'
                    WHEN account_folders_meta_data.meta_data_value='2' AND role_id=200 THEN 'coach'
                    WHEN account_folders_meta_data.meta_data_value='2' AND role_id=210 THEN 'coachee'
                    WHEN account_folders_meta_data.meta_data_value='2' AND role_id=220 THEN 'coachee'
                    WHEN account_folders_meta_data.meta_data_value='3' AND role_id=200 THEN 'assessor'
                    WHEN account_folders_meta_data.meta_data_value='3' AND role_id=210 THEN 'assessee'
                    WHEN account_folders_meta_data.meta_data_value='3' AND role_id=220 THEN 'assessee'
                END ) AS user_role
            FROM account_folder_users WHERE user_id=comments.`created_by` AND account_folder_id=account_folders.`account_folder_id` LIMIT 1) AS user_type 
        
        ";

        if( in_array("comment", $fields) ){
            $query .= ", comments.`comment` AS 'Comment' ";
        }
        if( in_array("time_stamp", $fields) ){
            $query .= ", (IF(comments.time=0, 'ALL', TIME_FORMAT(SEC_TO_TIME(comments.time),'%H:%i:%s'))) AS 'Time Stamp' ";
        }
        if( in_array("date_stamp", $fields) ){
            $query .= ", comments.`created_date` AS 'Date Stamp' ";
        }
        if( in_array("framework_name", $fields) ){
            $query .= ", (SELECT account_tags.`tag_title` FROM account_tags WHERE account_tags.`account_id`=".$account_id." AND account_tags.`account_tag_id` IN (SELECT account_tags.`framework_id` FROM account_tags WHERE  account_tags.`account_id`=".$account_id." AND account_tags.`account_tag_id` = tbl_act_standards.`account_tag_id` ) ) AS 'Framework Name' ";
        }
        if( in_array("tagged_standards", $fields) ){
            $query .= ", TRIM( CONCAT( COALESCE((SELECT tag_code FROM account_tags WHERE account_tag_id = tbl_act_standards.`account_tag_id` ),''),' ',tbl_act_standards.`tag_title`) ) AS `Tagged Standards` ";
        }
        if( in_array("custom_marker_tags", $fields) ){
            $query .= ", tbl_act_custom_markers.`tag_title` AS `Custom Marker` ";
        }
        if( in_array("attachment_file_names", $fields) ){
            $query .= ", (SELECT GROUP_CONCAT(original_file_name SEPARATOR ', ') FROM `documents` INNER JOIN `comment_attachments` ON documents.`id` = `comment_attachments`.`document_id` WHERE comment_attachments.`comment_id` = comments.`id`) AS 'Attachment File Names' ";
        }

        $query .= "
                FROM
                accounts 
                INNER JOIN account_folders 
                  ON accounts.`id` = account_folders.`account_id` 
                LEFT JOIN account_folder_documents 
                  ON account_folders.`account_folder_id` = account_folder_documents.`account_folder_id` 
                LEFT JOIN account_folders_meta_data 
                  ON account_folders.`account_folder_id` = account_folders_meta_data.`account_folder_id` 
                LEFT JOIN documents 
                  ON account_folder_documents.`document_id` = documents.`id` 
                LEFT JOIN comments 
                  ON documents.`id` = comments.`ref_id` AND comments.ref_type IN (2,3,6) 
                LEFT JOIN users 
                  ON comments.`created_by` = users.`id`
        ";

        $query .= "
            LEFT OUTER JOIN account_comment_tags AS `tbl_act_standards` ON (comments.id=tbl_act_standards.`comment_id` AND tbl_act_standards.`ref_type`=0)
            LEFT OUTER JOIN account_comment_tags AS `tbl_act_custom_markers` ON (comments.id=tbl_act_custom_markers.`comment_id` AND tbl_act_custom_markers.`ref_type`=2)
            LEFT OUTER JOIN account_tags ON (tbl_act_standards.`account_tag_id`=account_tags.`framework_id`)
        ";
        /*
                if( in_array("tagged_standards","framework_name", $fields) ) {
                    $query .= "
                    LEFT OUTER JOIN account_comment_tags AS `tbl_act_standards` ON (comments.id=tbl_act_standards.`comment_id` AND tbl_act_standards.`ref_type`=0)
                    ";
                }
                if( in_array("custom_marker_tags", $fields) ) {
                    $query .= "
                    LEFT OUTER JOIN account_comment_tags AS `tbl_act_custom_markers` ON (comments.id=tbl_act_custom_markers.`comment_id` AND tbl_act_custom_markers.`ref_type`=1)
                    ";
                }
                if( in_array("framework_name","tagged_standards", $fields) ) {
                    $query .= "
                    LEFT OUTER JOIN account_tags ON (tbl_act_standards.`account_tag_id`=account_tags.`framework_id`)
                    ";
                }
        */

        $query .= "
        WHERE 
        account_folders.`account_id`=".$account_id." AND folder_type=1 AND 
        account_folders_meta_data.`meta_data_name`='folder_type' AND 
        account_folders.`active`='1'
        AND (documents.`doc_type` IN (1,3) OR documents.`doc_type` IS NULL )
        AND (comments.`ref_type` IN (2,3,6,1,4) OR comments.`ref_type` IS NULL )
        )";

        $query .= "UNION ALL ";


        $query .= "(SELECT 
  accounts.`company_name` AS 'Account Name',
  accounts.`id` AS 'Accout-GUID',
  account_folders.`name` AS 'Huddle Name',
  account_folders.`account_folder_id` AS 'Huddle-GUID',
  NULL AS 'Video Name',
  NULL AS 'Video-GUID',
  users.`id` AS 'User-GUID',
  users.`institution_id` AS 'Institution ID',
  users.`username` AS 'Username',
  users.`first_name` AS 'First Name',
  users.`last_name` AS 'Last Name',
  (SELECT 
    (
      CASE
        WHEN account_folders_meta_data.meta_data_value = '1' 
        AND role_id = 200 
        THEN 'admin' 
        WHEN account_folders_meta_data.meta_data_value = '1' 
        AND role_id = 210 
        THEN 'member' 
        WHEN account_folders_meta_data.meta_data_value = '1' 
        AND role_id = 220 
        THEN 'viewer' 
        WHEN account_folders_meta_data.meta_data_value = '2' 
        AND role_id = 200 
        THEN 'coach' 
        WHEN account_folders_meta_data.meta_data_value = '2' 
        AND role_id = 210 
        THEN 'coachee' 
        WHEN account_folders_meta_data.meta_data_value = '2' 
        AND role_id = 220 
        THEN 'coachee' 
        WHEN account_folders_meta_data.meta_data_value = '3' 
        AND role_id = 200 
        THEN 'assessor' 
        WHEN account_folders_meta_data.meta_data_value = '3' 
        AND role_id = 210 
        THEN 'assessee' 
        WHEN account_folders_meta_data.meta_data_value = '3' 
        AND role_id = 220 
        THEN 'assessee' 
      END
    )) AS user_type ";

        if( in_array("comment", $fields) ){
            $query .= ", NULL AS 'Comment' ";
        }
        if( in_array("time_stamp", $fields) ){
            $query .= ", NULL AS 'Time Stamp' ";
        }
        if( in_array("date_stamp", $fields) ){
            $query .= ", NULL AS 'Date Stamp' ";
        }
        if( in_array("framework_name", $fields) ){
            $query .= ", NULL AS 'Framework Name' ";
        }
        if( in_array("tagged_standards", $fields) ){
            $query .= ", NULL AS `Tagged Standards` ";
        }
        if( in_array("custom_marker_tags", $fields) ){
            $query .= ", NULL AS `Custom Marker` ";
        }
        if( in_array("attachment_file_names", $fields) ){
            $query .= ", NULL AS 'Attachment File Names' ";
        }


        $query .= "
               FROM
            accounts 
            INNER JOIN account_folders 
              ON accounts.`id` = account_folders.`account_id` 
            LEFT JOIN account_folders_meta_data 
              ON account_folders.`account_folder_id` = account_folders_meta_data.`account_folder_id` 
            LEFT JOIN `account_folder_users` 
              ON account_folders.`account_folder_id` = account_folder_users.`account_folder_id` 
            LEFT JOIN users 
              ON account_folder_users.`user_id` = users.`id` 
        ";


        $query .= "WHERE account_folders.`account_id` = ".$account_id." 
             AND account_folders.folder_type = 1 
             AND account_folders_meta_data.`meta_data_name` = 'folder_type' 
             AND account_folders.`active` = '1' 
             AND users.id NOT IN 
             (SELECT 
            comments.created_by 
          FROM
            comments 
            JOIN documents 
              ON comments.ref_id = documents.id
            JOIN `account_folder_documents` 
              ON account_folder_documents.document_id = documents.id
            JOIN `account_folders`
              ON account_folders.account_folder_id = account_folder_documents.account_folder_id
          WHERE account_folders.account_id = ".$account_id."  AND comments.ref_type IN (2,3,6) AND account_folders.folder_type = '1')) ";


        $query .= " UNION ALL ";

        $query .= "(SELECT 
                    accounts.`company_name` AS 'Account Name',
                    accounts.`id` AS 'Accout-GUID',
                    NULL AS 'Huddle Name',
                    NULL AS 'Huddle-GUID',
                    NULL AS 'Video Name',
                    NULL AS 'Video-GUID',
                    users.`id` AS 'User-GUID',
                    users.`institution_id` AS 'Institution ID',
                    users.`username` AS 'Username',
                    users.`first_name` AS 'First Name',
                    users.`last_name` AS 'Last Name',
                    NULL AS user_type ";

        if( in_array("comment", $fields) ){
            $query .= ", NULL AS 'Comment' ";
        }
        if( in_array("time_stamp", $fields) ){
            $query .= ", NULL AS 'Time Stamp' ";
        }
        if( in_array("date_stamp", $fields) ){
            $query .= ", NULL AS 'Date Stamp' ";
        }
        if( in_array("framework_name", $fields) ){
            $query .= ", NULL AS 'Framework Name' ";
        }
        if( in_array("tagged_standards", $fields) ){
            $query .= ", NULL AS `Tagged Standards` ";
        }
        if( in_array("custom_marker_tags", $fields) ){
            $query .= ", NULL AS `Custom Marker` ";
        }
        if( in_array("attachment_file_names", $fields) ){
            $query .= ", NULL AS 'Attachment File Names' ";
        }


        $query .= " FROM
        accounts 
        LEFT JOIN `users_accounts` 
          ON accounts.`id` = users_accounts.`account_id` 
        LEFT JOIN `users` 
          ON users_accounts.`user_id` = users.`id` ";



        $query .= " WHERE accounts.`id` = ".$account_id." 
        AND users.id NOT IN 
        (SELECT 
          comments.created_by 
        FROM
          comments 
          JOIN documents 
            ON comments.ref_id = documents.id 
        WHERE documents.account_id = ".$account_id.") 
        AND users.id NOT IN 
        (SELECT 
          account_folder_users.`user_id` 
        FROM
          `account_folder_users` 
          JOIN `account_folders` 
            ON account_folder_users.`account_folder_id` = account_folders.`account_folder_id` 
        WHERE account_folders.`account_id` = ".$account_id.")) ";



        return $query;
    }


    function _format_performance_level_export_query($account_id){
        $query = "SELECT
                        ac.`company_name` AS 'Account Name',
                        ac.`id` AS 'Accout GUID',
                        af.name AS 'Huddle Name',
                        af.account_folder_id AS 'Huddle-GUID',
                        afd.title AS 'Video Name',
                        afd.document_id AS 'Video GUID',
                        (    SELECT
                            `account_folder_users`.`user_id` AS coachee_id
                        FROM account_folder_users WHERE account_folder_users.account_folder_id = af.`account_folder_id`  AND role_id=210 GROUP BY af.`account_folder_id`) AS 'Coachee User GUID',
                        (    SELECT
                            GROUP_CONCAT( CONCAT(users.`first_name`, ' ', users.`last_name`) SEPARATOR ', ') AS coachee_id
                        FROM account_folder_users INNER JOIN users ON (account_folder_users.`user_id`=users.`id`) WHERE account_folder_users.account_folder_id=af.`account_folder_id` AND role_id=200 GROUP BY af.`account_folder_id`) AS 'Coach(s)',
                        act.tag_title AS 'Standard',
                        ( CASE WHEN dsr.`rating_value` IS NULL THEN 0 ELSE dsr.`rating_value` END ) AS 'Rating'
                    FROM
                    `account_comment_tags` act
                    INNER JOIN `account_folder_documents` afd ON afd.`document_id` = act.`ref_id` 
                    LEFT JOIN `document_standard_ratings` dsr ON dsr.`standard_id` = act.`account_tag_id` AND dsr.`document_id` = act.`ref_id` 
                    INNER JOIN `account_folders` af ON af.`account_folder_id` = afd.`account_folder_id` 
                    INNER JOIN `account_folders_meta_data` afmd  ON afmd.`account_folder_id` = afd.`account_folder_id` AND afmd.`meta_data_name` = 'folder_type' 
                    INNER JOIN accounts ac ON ac.`id` = af.`account_id` 
                    WHERE 
                    act.`ref_type` = '0' 
                    AND afmd.`meta_data_value` = '2' 
                    AND af.`account_id` = ".$account_id." 
                    AND af.`active`='1' ";

        return $query;
    }


    function _create_global_export_request($data){

        $requestXml = '<GlobalExportJob>
                        <account_id>' . $data['account_id'] . '</account_id>
                        <account_owner_email>' . $data['account_owner_email'] . '</account_owner_email>
                        <file_type>' . $data['file_type'] . '</file_type>
                        <export_type>' . $data['export_type'] . '</export_type>
                        <export_query>' . $data['export_query'] . '</export_query>
                       </GlobalExportJob>';
        $site_id = Account::where("id", $data['account_id'])->value('site_id');
        $job_data = array(
            'JobId' => '6',
            'CreateDate' => date("Y-m-d H:i:s"),
            'RequestXml' => $requestXml,
            'JobQueueStatusId' => 1,
            'CurrentRetry' => 0,
            'JobSource' => Sites::get_base_url($site_id)
        );

        DB::table('JobQueue')->insert($job_data);
    }

    function archive(Request $request) {
        $input = $request->all();
        $ids = $input['huddle_ids'];
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->archive_folder_update($id);
            }
        }

        $final_array = array(
            'success' => true,
            'message' => 'Selected Folders and Huddles are Successfully Archived.'
        );
        return response()->json($final_array);
    }

    function unarchive(Request $request) {
        $input = $request->all();
        $ids = $input['huddle_ids'];
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->unarchive_folder_update($id);
            }
        }

        $final_array = array(
            'success' => true,
            'message' => 'Selected Folders and Huddles are Successfully Unarchived.'
        );
        return response()->json($final_array);

    }

    function archive_folder_update($huddle_id) {

        $data = array(
            'active' => 0,
            'archive' => 1
        );

        $affected_rows = AccountFolder::where(array('account_folder_id' => $huddle_id))->update($data);

        $folder_childs = AccountFolder::where(array(
            'parent_folder_id' => $huddle_id, 'active' => 1
        ))->get()->toArray();


        if (!empty($folder_childs)) {
            foreach ($folder_childs as $folder_child) {
                $folder_childs_ids[] = $folder_child['account_folder_id'];
                $this->archive_folder_update_recursive($folder_child['account_folder_id']);
            }
        }

    }

    function unarchive_folder_update($huddle_id) {

        $data = array(
            'active' => 1,
            'archive' => 0
        );

        $affected_rows = AccountFolder::where(array('account_folder_id' => $huddle_id))->update($data);

        $this->recursive_unarchive_parents($huddle_id);

        $folder_childs = AccountFolder::where(array(
            'parent_folder_id' => $huddle_id, 'active' => 0 , 'archive' => 1
        ))->get()->toArray();


        if (!empty($folder_childs)) {
            foreach ($folder_childs as $folder_child) {
                $folder_childs_ids[] = $folder_child['account_folder_id'];
                $this->unarchive_folder_update_recursive($folder_child['account_folder_id']);
            }
        }

    }


    function recursive_unarchive_parents($huddle_id)
    {
        $data = array(
            'active' => 1,
            'archive' => 0
        );

        if($huddle_id == '0')
        {
            return false;
        }

        $huddle_parent = AccountFolder::where(array(
            'account_folder_id' => $huddle_id
        ))->first();

        if($huddle_parent && $huddle_parent->parent_folder_id != '0')
        {
            $affected_rows = AccountFolder::where(array('account_folder_id' => $huddle_parent->parent_folder_id))->update($data);

            $this->recursive_unarchive_parents($huddle_parent->parent_folder_id);
        }
        else
        {
            return false;
        }

    }

    function archive_folder_update_recursive($huddle_id) {


        $data = array(
            'active' => 0,
            'archive' => 1
        );

        $affected_rows = AccountFolder::where(array('account_folder_id' => $huddle_id))->update($data);

        $folder_childs = AccountFolder::where(array(
            'parent_folder_id' => $huddle_id, 'active' => 1
        ))->get()->toArray();

        if (!empty($folder_childs)) {
            foreach ($folder_childs as $folder_child) {
                $folder_childs_ids[] = $folder_child['account_folder_id'];
                $this->archive_folder_update_recursive($folder_child['account_folder_id']);
            }
        }
    }

    function unarchive_folder_update_recursive($huddle_id) {


        $data = array(
            'active' => 1,
            'archive' => 0
        );

        $affected_rows = AccountFolder::where(array('account_folder_id' => $huddle_id))->update($data);

        $folder_childs = AccountFolder::where(array(
            'parent_folder_id' => $huddle_id, 'active' => 0 , 'archive' => 1
        ))->get()->toArray();

        if (!empty($folder_childs)) {
            foreach ($folder_childs as $folder_child) {
                $folder_childs_ids[] = $folder_child['account_folder_id'];
                $this->unarchive_folder_update_recursive($folder_child['account_folder_id']);
            }
        }
    }


    function update_getting_started_archiving(Request $request) {
        $input = $request->all();
        $result = array();
        $user_id = $input['user_id'];
        DB::table('users')->where(array('id' => $user_id))->update(array('gs_archiving' => '0'));

        $result['message'] = "Value Updated Successfully";
        $result['success'] = true;


        return response()->json($result);

    }


    function archive_parent_pass($huddle_id) {

        $result =  AccountFolder::where(array('account_folder_id' => $huddle_id))->first()->toArray();
        if(empty($result['parent_folder_id']))
        {
            return true;
        }

        if(!empty($result['parent_folder_id']))
        {
            $parent_data =  AccountFolder::where(array('account_folder_id' => $result['parent_folder_id'],'active' => '0' , 'archive' => '1' ))->first();
            if($parent_data)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        else
        {
            return false;
        }

    }

    function unarchive_parent_pass($huddle_id) {

        $result =  AccountFolder::where(array('account_folder_id' => $huddle_id))->first()->toArray();
        if(empty($result['parent_folder_id']))
        {
            return true;
        }

        if(!empty($result['parent_folder_id']))
        {
            $parent_data =  AccountFolder::where(array('account_folder_id' => $result['parent_folder_id'],'active' => '1' , 'archive' => '0' ))->first();
            if($parent_data)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        else
        {
            return false;
        }


    }
    function setDefaultAccountForAllUsers () {
        $managedUserAccounts = [];
        $userAccounts = DB::table('users_accounts')
            ->join('accounts', 'users_accounts.account_id', '=', 'accounts.id')
            ->select('users_accounts.id', 'users_accounts.account_id', 'users_accounts.user_id', 'accounts.company_name', 'users_accounts.is_default')
//            ->where(['users_accounts.user_id' => 621])
            ->get();
        foreach ($userAccounts as $key => $userAccount) {
            if (!isset($managedUserAccounts[$userAccount->user_id])) {
                $managedUserAccounts[$userAccount->user_id] = [];
            }
            $managedUserAccounts[$userAccount->user_id][] = $userAccount;
        }
        foreach ($managedUserAccounts as $userId => $userAccounts) {
//            DB::table("users_accounts")->where(['user_id' => $userId])->update(['is_default' => 0]);
//            DB::table("users_accounts")->where(['user_id' => $userId, 'id' => $userAccounts[0]['id']])->update(['is_default' => 1]);
        }

        return response()->json(true);
    }


    
    function check_is_submitted_assessments(){
        $huddles = DB::table("account_folders_meta_data")
        ->where("account_folders_meta_data.meta_data_name","folder_type")
        ->where("account_folders_meta_data.meta_data_value","3")->get();
       
        $huddle_ids = [];
        foreach ($huddles as $huddle)
        {
            $submission_date = HelperFunctions::get_submission_date($huddle->account_folder_id, false, $huddle->site_id,true);
            if(strtotime($submission_date) > time())
            {
                $assignments=AccountFolderDocument::where('account_folder_id',$huddle->account_folder_id)->first();
                // var_dump($huddle->account_folder_id, $submission_date);
                if(!empty($assignments)){
                    $huddle_ids[] = $huddle->account_folder_id;
                }
            }
        }
        if(!empty($huddle_ids))
        {
            foreach($huddle_ids as $key=>$huddle_id){
                $acount_folder_id = $huddle_id;
                $accountFolderUsers = AccountFolderUser::where('account_folder_id', $huddle_id)->where('role_id',210)->where('is_submitted',1)->where('site_id', $this->site_id)->get();
                
                if($accountFolderUsers){
                    foreach($accountFolderUsers as $row){
                        $user_id = $row->user_id;                        
                        $accountFolderDocuments = AccountFolderDocument::join('documents','documents.id','=','account_folder_documents.document_id')
                                                ->where('account_folder_documents.account_folder_id', $acount_folder_id) 
                                                ->where('documents.created_by', $user_id)->get();
                        if($accountFolderDocuments){
                            $accountFolderDocuments = $accountFolderDocuments->toArray();
                        }
                        if(count($accountFolderDocuments) > 0){
                            echo "If Section <br/>";
                            echo "User id = $user_id </br>";
                            echo "Huddle id = $acount_folder_id <br/>";
                            echo "Total submission = ".count($accountFolderDocuments)."<br/>";
                            echo "------------------------------------------------------";
                            continue;
                        }else{         
                            echo "else Section <br/>";                  
                            echo "User id = $user_id </br>";
                            echo "Huddle id = $acount_folder_id <br/>";
                            echo "Total submission = ".count($accountFolderDocuments)."<br/>";
                            echo "------------------------------------------------------";
                            //AccountFolderUser::where("account_folder_id",$acount_folder_id)->where('user_id',$user_id)->update(["is_submitted"=>0,"last_submission"=>date("Y-m-d H:i:s")]); 
                        }
                    }
                    echo "---------------------------------------------------------------------<br/>";
                }                
               
            }
           
        }
    }

    public function update_auto_delete(Request $request)
    {
        $auto_delete = $request->get('auto_delete') == 1 ? 1 : 0;
        $account_id = $request->get('account_id');
        $user_id = $request->get('user_id');
        Account::where("id", $account_id)->update(['auto_delete'=> $auto_delete]);
        $event = [
            'channel' => "account_settings_".$account_id,
            'event' => "auto_delete",
            'account_id' => $account_id,
            'user_id' => $user_id,
            'auto_delete' => $auto_delete,
            'item_id' => $account_id,
            'notification_type' => 10
        ];
        HelperFunctions::broadcastEvent($event);
        return response()->json(["status"=>true, "message"=>"Success"]);
    }

}