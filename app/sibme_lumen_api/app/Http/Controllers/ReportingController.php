<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Document;
use App\Models\AccountFolder;
use App\Models\AccountFolderMetaData;
use App\Models\AccountMetaData;
use App\Models\AccountTag;
use App\Models\AccountFolderUser;
use App\Models\AccountFrameworkSetting;
use App\Models\Group;
use App\Models\Sites;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\UserActivityLog;
use App\Models\UserGroup;
use App\Models\EmailUnsubscribers;
use App\Models\UserCustomField;
use Codedge\Fpdf\Fpdf\Fpdf;

use App\Models\AccountFolderDocument;
use App\Models\AccountCommentTag;
use App\Models\DocumentStandardRating;
use App\Models\PerformanceLevelDescription;
use App\Models\AccountFrameworkSettingPerformanceLevel;
use App\Models\ReportConfiguration;
use App\Models\Report;
use App\Models\CustomField;
use App\Models\AssessmentCustomField;
use App\Models\Comment;

use App\Services\HelperFunctions;
use App\Services\SendGridEmailManager;
use App\Services\TranslationsManager;
use App\Services\UsersHelperFunctions;
//use App\fpdf\fpdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use \DrewM\MailChimp\MailChimp;
use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
use App\Services\Emails\Email;

use Illuminate\Support\Facades\Log;

class ReportingController extends Controller
{
    protected $httpreq;
    protected $site_id;
    protected $lang;
    public $mailchimp;
    public $base;
    public $language_based_messages;
    public $group_based_translation;
    public $subject_title;
    public $duplicated_emails;
    public $linked_emails;
    public $new_emails_added;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->lang = $this->httpreq->header('current-lang');
        $this->current_lang = $this->httpreq->header('current-lang');
        $this->base = config('s3.sibme_base_url');
        $this->mailchimp = new MailChimp('007cb274d399d9290e1e6c5b42118d40-us3');
        $this->language_based_messages = TranslationsManager::get_page_lang_based_content('flash_messages', $this->site_id, '', $this->lang);
        $this->group_based_translation = TranslationsManager::get_page_lang_based_content('users/administrators_groups', $this->site_id, '', $this->lang);
        //$this->subject_title = Sites::get_site_settings('email_subject', $this->site_id);
        $this->module_url = 'Api/huddle_list';
    }
    
    function coaching_report(Request $request)
    {
        $input = $request->all();
        $paginate = isset($input['paginate']) && $input['paginate'] == false ? false : true;
        if(!is_array($input['account_ids']))
        {
            $input['account_ids'] = json_decode($input['account_ids']);
        }
        if(isset($input['standard_ids']) && !is_array($input['standard_ids']))
        {
            //$input['standard_ids'] = json_decode($input['standard_ids']);
            $input['standard_ids'] = explode(',',$input['standard_ids']);
        } 
       if(isset($input['huddle_ids']) && !is_array($input['huddle_ids']))
        {
            $input['huddle_ids'] = json_decode($input['huddle_ids']);
        }
        $coach_bool = false;
        $coachee_bool = false;
        if(in_array('1', $input['huddle_ids']))
        {
            $coach_bool = true;
        }
        
        if(in_array('2', $input['huddle_ids']))
        {
            $coachee_bool = true;
        }
        
        $account_id = $input['account_id'];
        $account_ids = $input['account_ids'];
        if(empty($account_ids))
        {
            $account_ids = array($input['account_id']);
        }
        $page = (int)$input['page'];
        $page = $page - 1;
        $limit = $input['limit'];
        $active = $input['active'];
        $offset = $page*$limit;
        if(isset($input['report_id']))
        {
            $report_config_id = $input['report_id'];
        }
        else 
        {
            $default_report_config = ReportConfiguration::where(array('report_type' => 'coaching_report','is_default' => '1'))->first();
            $report_config_id = $default_report_config->id;
        }
        $account_user_custom_fields = CustomField::get_custom_fields($account_id,'1');
        $report_data = ReportConfiguration::where(array("id" => $report_config_id))->first();
        $report_attributes = json_decode($report_data->report_attributes);
        $report_attributes_array = array();
        $start_date = $input['start_date'];
        $end_date = $input['end_date'];
        $stDate = date_create($start_date);
        $start_date = date_format($stDate, 'Y-m-d');
        $framework_data = array();
        $custom_marker_data = array();
        
        $endDate = date_create($end_date);
        $end_date = date_format($endDate, 'Y-m-d');
        $user_id = $input['user_id'];
        $is_user = null;
        if ($input['user_role'] == 120) {
            $is_user = $user_id;
        }
        if ($input['user_role'] == 115) {
            $is_user = $user_id;
        }
        
        foreach($report_attributes as $row)
        {
            if($row->status)
            {
                $report_attributes_array[] = $row->name; 
            }
        }
        $query = AccountFolder::select('User.id','User.image', 'User.email','AccountFolder.account_id')->selectRaw("concat(User.first_name,' ',User.last_name) as coach_name")
            ->leftJoin('account_folders_meta_data as afmd','AccountFolder.account_folder_id','=','afmd.account_folder_id')
            ->leftJoin('account_folder_users as huddle_users','AccountFolder.account_folder_id','=','huddle_users.account_folder_id')
            ->leftJoin('users as User','User.id','=','huddle_users.user_id')
            ->where(array(
                'AccountFolder.folder_type' => 1,
                'afmd.meta_data_name' => 'folder_type',
                'afmd.meta_data_value' => 2,
                'huddle_users.role_id' => 200
            ))
            ->whereIn('AccountFolder.account_id',$account_ids)    
            ->where('AccountFolder.is_sample', '<>','1')
            ->whereRaw('AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)');
        if(!empty($request->get('search')))
        {
            $search = $this->escape_string($request->get('search'));
            $query = $query->whereRaw("(CONCAT(User.first_name,' ',User.last_name) like '%".$search."%' OR User.email like '%".$search."%' )");
        }
        if($is_user)
        {
            $query->where('huddle_users.user_id',$is_user);
        }
        $sorting_by = 'coach_name';
        $sorting_by_order = 'ASC';
        if($request->has('sort_by') && $request->get('sort_by') )
        {
            $sorting_by = 'sorting_key';
            $sorting_by_order = $request->get('sort_by_order');
            $query->select(DB::raw('(SELECT SUM('.$request->get('sort_by').') FROM analytics_huddles_summary WHERE huddle_id = AccountFolder.`account_folder_id` AND user_id = User.`id`) AS sorting_key'),'User.id','User.image', 'User.email','AccountFolder.account_id')->selectRaw("concat(User.first_name,' ',User.last_name) as coach_name");
        }
        $query2 = clone $query;
        
        if($active == 'true')
        {
            $account_coaches = $query->orderBy($sorting_by,$sorting_by_order)->groupBy('huddle_users.user_id')->get()->toArray();
        }
        else
        {
            if($paginate){
                $account_coaches = $query->orderBy($sorting_by,$sorting_by_order)->groupBy('huddle_users.user_id')->offset($offset)->limit($limit)->get()->toArray();
            }else{
                $account_coaches = $query->orderBy($sorting_by,$sorting_by_order)->groupBy('huddle_users.user_id')->get()->toArray();
            }    
        }
        $account_coaches_without_limit = $query2->orderBy($sorting_by,$sorting_by_order)->groupBy('huddle_users.user_id')->get()->toArray();
        $account_coaches_count = count($account_coaches_without_limit);
        $final_array = array();
        $coach_count = 0;
        foreach($account_coaches as $key => $coach)
        {
            $account_coach_huddles = AccountFolder::select('huddle_users.account_folder_id', 'huddle_users.user_id')
                    ->leftJoin('account_folders_meta_data as afmd','AccountFolder.account_folder_id','=','afmd.account_folder_id')
                    ->leftJoin('account_folder_users as huddle_users','AccountFolder.account_folder_id','=','huddle_users.account_folder_id')
                    ->leftJoin('users as User','User.id','=','huddle_users.user_id')
                    ->where(array(
                        'AccountFolder.folder_type' => 1,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => 2,
                        'huddle_users.role_id' => 200,
                        'huddle_users.user_id' => $coach['id']
                    ))
                    ->whereIn('AccountFolder.account_id',array($coach['account_id']))
                    ->where('AccountFolder.is_sample', '<>','1')
                    ->whereRaw('AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)')
                    ->groupBy('huddle_users.account_folder_id')
                    ->get()->toArray();
                $coach_hud = array();
                $account_folder_users = array();
                foreach ($account_coach_huddles as $coach_huddles) {
                    $coach_hud[] = $coach_huddles['account_folder_id'];
                    $account_folder_users[] = $coach_huddles['user_id'];
                }
                
                $query = AccountFolderUser::select('User.id','User.image', 'User.email' ,'af.account_id')
                    ->selectRaw("concat(User.first_name,' ',User.last_name) as coachee_name")
                    ->leftJoin('users as User','User.id','=','AccountFolderUser.user_id')
                    ->leftJoin('account_folders as af','af.account_folder_id','=','AccountFolderUser.account_folder_id')->whereIn('AccountFolderUser.account_folder_id' , $coach_hud)
                    ->where(array('User.is_active' => 1))
                    ->where('af.is_sample' , '<>', '1')
                    ->whereIn('AccountFolderUser.role_id',array(210))
                    ->groupBy('AccountFolderUser.user_id')
                    ->orderBy('User.first_name');
                $account_coachees = $query->get()->toArray();
                $user_ids = array();
                $user_ids[] = $coach['id'];
                foreach($account_coachees as $coachee)
                {
                    $user_ids[] = $coachee['id'];
                }
                $attribute_data = $this->attributes_query($account_id,array($coach['account_id']),$user_ids,$coach_hud,$report_config_id,$start_date,$end_date,false,'','',$active);
                $attribute_data = json_decode(json_encode($attribute_data), True);
                $custom_markers_ids = AccountTag::select('account_tag_id')->where(array('tag_type' => '1' , 'account_id' => $account_id))->get()->toArray();
                $updated_attribute_data = array();
                foreach($attribute_data as $value => $data)
                {
                    if($data['user_id'] == $coach['id'])
                    {
                        $documents_ids = AccountFolderDocument::select('document_id')->whereIn('account_folder_id',$coach_hud)->get()->toArray();
                        if(isset($input['standard_ids']))
                        {
                            $frameworks = $this->get_framework_tags_data($documents_ids,$input['standard_ids'],$coach_bool);
                            $framework_data = $frameworks['standard_data'];
                        }
                        else 
                        {
                            $framework_data = array();
                        }
                        $avg_pl_level = $this->get_avg_performance_level($documents_ids,$input['framework_id']);
                        $custom_marker_data = $this->get_custom_marker_data($documents_ids,$custom_markers_ids,$coach_bool,$start_date,$end_date);
                    }
                    else {
                          $documents_ids = Document::select('documents.id')
                              ->leftJoin('account_folder_documents as afd','documents.id','=','afd.document_id')
                              ->leftJoin('account_folder_users as afu','afd.account_folder_id','=','afu.account_folder_id')
                              ->whereIn('afd.account_folder_id',$coach_hud)
                              ->whereIn('documents.doc_type' , array(1, 3) )
                              ->where('afu.user_id' , $data['user_id'])
                              ->where('afu.role_id',210)
                              ->groupBy('documents.id')->get()->toArray();
                          if(isset($input['standard_ids']))
                          {
                                $frameworks = $this->get_framework_tags_data($documents_ids,$input['standard_ids'],$coachee_bool);
                                $framework_data = $frameworks['standard_data'];
                          }
                          else 
                          {
                                $framework_data = array();
                          }
                          $avg_pl_level = $this->get_avg_performance_level($documents_ids,$input['framework_id']);
                          $custom_marker_data = $this->get_custom_marker_data($documents_ids,$custom_markers_ids,$coachee_bool,$start_date,$end_date);
                    }
                 
                    
                    foreach($report_attributes as $row)
                    {
                        if(!empty($row->value) && $row->status)
                        {
                            $updated_attribute_data[$value][] = $data[$row->value];
                        }
                        if($row->name == 'Framework Tags' && $row->status && !in_array('Standard Performance Levels',$report_attributes_array))
                        {
                            foreach ($framework_data as $standard_data)
                            {
                               $updated_attribute_data[$value][] = $standard_data['count'];
                            }
                        }
                        if($row->name == 'Standard Performance Levels' && $row->status && !in_array('Framework Tags',$report_attributes_array))
                        {
                            foreach ($framework_data as $standard_data)
                            {
                               $updated_attribute_data[$value][] = $standard_data['pl_average'];
                            }
                        }
                        if($row->name == 'Framework Tags' && $row->status && in_array('Standard Performance Levels',$report_attributes_array ))
                        {
                            foreach ($framework_data as $standard_data)
                            {
                               $updated_attribute_data[$value][] = $standard_data['count']; 
                               $updated_attribute_data[$value][] = $standard_data['pl_average'];
                            }
                        }
                        if($row->name == 'Custom Video Marker Tags' && $row->status)
                        {
                            foreach ($custom_marker_data as $marker_data)
                            {
                               $updated_attribute_data[$value][] = $marker_data['count'];
                            }
                        }
                        if($row->name == 'Avg Performance Levels' && $row->status)
                        {
                            $updated_attribute_data[$value][] = $avg_pl_level;
                        }
                        
                        if($row->name == 'Session Summary' && $row->status)
                        {
                             $session_summary_count = $this->session_summary_count($documents_ids,$data['user_id']);
                             $updated_attribute_data[$value][] = $session_summary_count;
                        }
                        if($row->name == 'Is Active' && $row->status)
                        {
                            $user_active_detail = UserAccount::where(array('user_id' => $data['user_id'] , 'account_id' => $data['account_id']))->first();
                            if($user_active_detail->is_active == '1' && $user_active_detail->status_type == 'Active' )
                            {
                                $updated_attribute_data[$value][] = 'Yes';
                            }
                            else 
                            {
                                $updated_attribute_data[$value][] = 'No';
                            }
                        }
                        if($row->name == 'Huddle Role' && $row->status)
                        {
                            if($data['user_id'] == $coach['id'])
                            {
                                $updated_attribute_data[$value][] = "Coach";
                            }
                            else
                            {
                                $updated_attribute_data[$value][] = "Coachee";
                            }
                                                  
                        }
                    if($row->name == 'Custom Fields' && $row->status)
                        {
                            
                            if(isset($account_user_custom_fields['user_summary']['custom_fields']))
                            {
                                $custom_fields_array = $account_user_custom_fields['user_summary']['custom_fields'];
                                foreach($custom_fields_array as $cf)
                                {
                                    $user_custom_values = UserCustomField::get_users_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,'');
                                    if(!empty($user_custom_values))
                                    {
                                        if($user_custom_values[0]->custom_field_value == 'true')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'Yes'; 
                                        }
                                        if($user_custom_values[0]->custom_field_value == 'false')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'No'; 
                                        }
                                        $updated_attribute_data[$value][] = $user_custom_values[0]->custom_field_value;
                                    }
                                    else 
                                    {
                                        $updated_attribute_data[$value][] = '';
                                    }
                                }
                            }

                            if(isset($account_user_custom_fields['user_summary']['parent_custom_fields']))
                            {
                                $custom_fields_array = $account_user_custom_fields['user_summary']['parent_custom_fields'];
                                foreach($custom_fields_array as $cf)
                                {
                                    $user_custom_values = UserCustomField::get_users_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,'');
                                    if(!empty($user_custom_values))
                                    {
                                        if($user_custom_values[0]->custom_field_value == 'true')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'Yes'; 
                                        }
                                        if($user_custom_values[0]->custom_field_value == 'false')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'No'; 
                                        }
                                        $updated_attribute_data[$value][] = $user_custom_values[0]->custom_field_value;
                                    }
                                    else 
                                    {
                                        $updated_attribute_data[$value][] = '';
                                    }
                                }

                            }

                        }
                    }
                    if($data['user_id'] == $coach['id'])
                    {
                        $updated_attribute_data[$value]['huddle_role'] = 'Coach';
                        $updated_attribute_data[$value]['image'] = $coach['image'];
                        $updated_attribute_data[$value]['account_id'] =  $coach['account_id'];
                        $updated_attribute_data[$value]['user_id'] = $coach['id'];
                        $updated_attribute_data[$value]['email'] = $coach['email'];
                        $updated_attribute_data[$value]['user_name'] = $coach['coach_name'];
                        $updated_attribute_data[$value]['coach_id'] = $coach['id'];
                    }
                    else
                    {
                        $updated_attribute_data[$value]['huddle_role'] = 'Coachee';
                        $updated_attribute_data[$value]['image'] = $data['image'];
                        $updated_attribute_data[$value]['account_id'] =  $data['account_id'];
                        $updated_attribute_data[$value]['user_id'] = $data['user_id'];
                        $updated_attribute_data[$value]['email'] = $data['email'];
                        $updated_attribute_data[$value]['user_name'] = $data['user_name'];
                        $updated_attribute_data[$value]['coach_id'] = $coach['id'];
                    }
                }
                $huddle_role = array_column($updated_attribute_data, 'huddle_role');
                array_multisort($huddle_role, SORT_ASC, $updated_attribute_data);
                $single_set = array();
                if(!empty($updated_attribute_data))
                {
                    $single_set = array_slice($updated_attribute_data, 0,1)[0];
                    $single_set['coachees'] = array_slice($updated_attribute_data , 1 ,count($updated_attribute_data));
                }
                if(!empty($single_set))
                {
                    $final_array[$coach_count] = $single_set;
                    $coach_count++;
                }
                
        }
        $header_data = array();
        $count = 0;
        $translations =  TranslationsManager::get_page_lang_based_content("Api/analytics", $request->header("site_id"), '', $request->header("current-lang"));
        //$key = "is-active";
        //dd( $translations);
        foreach($report_attributes as $key => $row)
        {
            if(!empty($row->value) && $row->status)
            {
                $key = str_replace(' ','-',strtolower($row->name));
              
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] :  $row->name;
                $header_data[$count]['key'] = str_replace(' ','_',strtolower($row->name));
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['sorting'] = true;
                $header_data[$count]['sorting_key'] = $row->value;
                if($row->name == 'Account Name' || $row->name == 'User Role' || $row->name == 'User Type'  )
                {
                    $header_data[$count]['detail'] = false;
                    $header_data[$count]['sorting'] = false;
                    $header_data[$count]['sorting_key'] = '';
                }
                $count++;
                
            }
            
            if($row->name == 'Is Active' && $row->status)
            {
                $header_data[$count]['name'] =  isset($translations["is-active"]) ? $translations['is-active'] :  'Is Active';
                $header_data[$count]['key'] = str_replace(' ','_',strtolower('Is Active'));
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['detail'] = false;
                $count++;            
            }
            
            if($row->name == 'Custom Fields' && $row->status)
            {
                $custom_fields_array = array();
                if(isset($account_user_custom_fields['user_summary']['custom_fields']))
                {
                    $custom_fields_array = $account_user_custom_fields['user_summary']['custom_fields'];
                    foreach($custom_fields_array as $cf)
                    {
                        $key = str_replace(' ','-',strtolower($cf->field_label));
                        $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] :  $cf->field_label;
                        $header_data[$count]['key'] = '';
                        $header_data[$count]['standard'] = false;
                        $header_data[$count]['performance'] = false;
                        $header_data[$count]['markers'] = false;
                        $header_data[$count]['standard_id'] = false;
                        $header_data[$count]['custom_fields'] = true;
                        $header_data[$count]['detail'] = false;
                        $count++;
                    }
                }
                if(isset($account_user_custom_fields['user_summary']['parent_custom_fields']))
                {
                    $custom_fields_array = $account_user_custom_fields['user_summary']['parent_custom_fields'];
                    foreach($custom_fields_array as $cf)
                    {
                        $key = str_replace(' ','-',strtolower($cf->field_label));
                        $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] :  $cf->field_label;
                        //$header_data[$count]['name'] = $cf->field_label;
                        $header_data[$count]['key'] = '';
                        $header_data[$count]['standard'] = false;
                        $header_data[$count]['performance'] = false;
                        $header_data[$count]['markers'] = false;
                        $header_data[$count]['standard_id'] = false;
                        $header_data[$count]['custom_fields'] = true;
                        $header_data[$count]['detail'] = false;
                        $count++;
                    }
                }
            }
            
            if($row->name == 'Framework Tags' && $row->status && !in_array('Standard Performance Levels',$report_attributes_array))
            {
                foreach ($framework_data as $standard_data)
                {
                    $key = str_replace(' ','-',strtolower($standard_data['label']));
                    $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] :  $standard_data['label'];
                   //$header_data[$count]['name'] = $standard_data['label'];
                   $header_data[$count]['key'] = 'standard';
                   $header_data[$count]['standard'] = true;
                   $header_data[$count]['performance'] = false;
                   $header_data[$count]['markers'] = false;
                   $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                   $count++;
                }
            }
            if($row->name == 'Standard Performance Levels' && $row->status && !in_array('Framework Tags',$report_attributes_array))
            {
                foreach ($framework_data as $standard_data)
                {
                    $key = str_replace(' ','-',strtolower($standard_data['label']));
                    $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] :  $standard_data['label'];
                   //$header_data[$count]['name'] = $standard_data['label'];
                   $header_data[$count]['key'] = 'performance';
                   $header_data[$count]['standard'] = false;
                   $header_data[$count]['performance'] = true;
                   $header_data[$count]['markers'] = false;
                   $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                   $count++;
                }
            }
            if($row->name == 'Framework Tags' && $row->status && in_array('Standard Performance Levels',$report_attributes_array))
            {
                foreach ($framework_data as $standard_data)
                {
                    $key = str_replace(' ','-',strtolower($standard_data['label']));
                    $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] :  $standard_data['label'];
                   //$header_data[$count]['name'] = $standard_data['label'];
                   $header_data[$count]['key'] = 'standard';
                   $header_data[$count]['standard'] = true;
                   $header_data[$count]['performance'] = false;
                   $header_data[$count]['markers'] = false;
                   $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                   $count++;
                    $key = str_replace(' ','-',strtolower($standard_data['label']));
                    $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] :  $standard_data['label'];
                   //$header_data[$count]['name'] = $standard_data['label'];
                   $header_data[$count]['key'] = 'performance';
                   $header_data[$count]['standard'] = false;
                   $header_data[$count]['performance'] = true;
                   $header_data[$count]['markers'] = false;
                   $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                   $count++;
                }
            }
            if($row->name == 'Custom Video Marker Tags' && $row->status)
            {
                foreach ($custom_marker_data as $marker_data)
                {
                    $key = str_replace(' ','-',strtolower($marker_data['label']));
                    $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] :  $marker_data['label'];
                   //$header_data[$count]['name'] = $marker_data['label'];
                   $header_data[$count]['key'] = 'markers';
                   $header_data[$count]['standard'] = false;
                   $header_data[$count]['performance'] = false;
                   $header_data[$count]['markers'] = true;
                   $header_data[$count]['standard_id'] = $marker_data['account_tag_id'];
                   $count++;
                }
            }
            if($row->name == 'Avg Performance Levels' && $row->status)
            {
                
                   $key = str_replace(' ','-',strtolower('Avg Performance Levels'));
                   $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] :  'Avg Performance Levels';
                   //$header_data[$count]['name'] = 'Avg Performance Levels';
                   $header_data[$count]['key'] = 'avg_performance_levels';
                   $header_data[$count]['standard'] = false;
                   $header_data[$count]['performance'] = false;
                   $header_data[$count]['markers'] = false;
                   $header_data[$count]['standard_id'] = false;
                   $header_data[$count]['avg_performance_levels'] = true;
                   $header_data[$count]['detail'] = false;
                   $count++;
                
            }
            if($row->name == 'Session Summary' && $row->status)
            {
                    $key = str_replace(' ','-',strtolower('Session Summary'));
                    $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] :  'Session Summary';
                   //$header_data[$count]['name'] = 'Session Summary';
                   $header_data[$count]['key'] = 'session_summary';
                   $header_data[$count]['standard'] = false;
                   $header_data[$count]['performance'] = false;
                   $header_data[$count]['markers'] = false;
                   $header_data[$count]['standard_id'] = false;
                   $count++;
            }
            if($row->name == 'Huddle Role' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Huddle Role'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Huddle Role';
                //$header_data[$count]['name'] = 'Is Active';
                $header_data[$count]['key'] = str_replace(' ','_',strtolower('Huddle Role'));
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['detail'] = false;
                $count++;            
            }
        }
        $result = array();
        $result['attributes_header'] = $header_data;
        $result['attributes_data'] = $final_array;
        $result['total_coach'] = $account_coaches_count;
        $result['page'] = $page+1;
        $result['limit'] = $limit;
        $result['sort_by'] = ($request->has('sort_by')) ? $request->get('sort_by') : '';
        $result['sort_by_order'] = ($request->has('sort_by_order')) ? $request->get('sort_by_order') : '';
        return response()->json($result, 200, [], JSON_PRETTY_PRINT);
        
    }
    
    function get_avg_performance_level($document_ids,$framework_id,$bool = true)
    {
        $standard_ids = array();
        $standards = DB::table('account_tags')->where(array('framework_id' => $framework_id , 'tag_type' => '0'))->get()->toArray();
        foreach ($standards as $standard)
        {
            $standard_ids[] = $standard->account_tag_id;
        }
        $performance_details = DB::table('document_standard_ratings')->whereIn('document_id',$document_ids)->whereIn('standard_id' , $standard_ids )->get()->toArray();
        $total_sum = 0;
        $avg_pl_level = 0;
        $pl_avg_array = array();
        foreach($performance_details as $performance)
        {
                 $total_sum = $total_sum + (int)$performance->rating_value; 
                 $pl_avg_array[] = $performance;


        }
        if(count($pl_avg_array) != 0)
        {
             $avg_pl_level = $total_sum/count($pl_avg_array);
        }
        $avg_pl_level_name = 'No Ratings';
        $avg_pl_level_name = $this->get_average_name(round($avg_pl_level),$framework_id,$standards[0]->account_id);
        if($bool)
        {
            return $avg_pl_level_name;
        }
        else
        {
            return 'No Ratings';
        }
    }
    
    function get_framework_tags_data($document_ids,$standard_ids,$coach_or_coachee,$huddle_id = [])
    {
        $standard_data = array();
        $huddle_performance_details = [];
        if(!empty($huddle_id))
        {
            $huddle_document_ids = [];
            $huddle_videos = DB::table('account_folder_documents')->whereIn('account_folder_id',$huddle_id)->get()->toArray();
            foreach ($huddle_videos as $video)
            {
                $huddle_document_ids[] = $video->document_id;
            }
          $huddle_performance_details = DB::table('document_standard_ratings')->whereIn('document_id',$huddle_document_ids)->whereIn('standard_id' , $standard_ids )->get()->toArray();  
        }
        $account_comment_tags = DB::table('account_comment_tags')->whereIn('ref_id',$document_ids)->whereIn('account_tag_id' , $standard_ids)->get()->toArray();
        $performance_details = DB::table('document_standard_ratings')->whereIn('document_id',$document_ids)->whereIn('standard_id' , $standard_ids )->get()->toArray();
        $standard_details = AccountTag::whereIn('account_tag_id' , $standard_ids)->get()->toArray();
        $total_sum = 0;
        $avg_pl_level = 0;
        $pl_avg_array = array();
        foreach($performance_details as $performance)
        {
                 $total_sum = $total_sum + (int)$performance->rating_value; 
                 $pl_avg_array[] = $performance;


        }
        if(count($pl_avg_array) != 0)
        {
             $avg_pl_level = $total_sum/count($pl_avg_array);
        }
        foreach ($standard_ids as $key => $standard)
        {
           $st_array = array(); 
           foreach($standard_details as $st)
           {
               if($st['account_tag_id'] == $standard)
               {
                   $st_array = $st;
               }
           }
           $count_standard = array();
           foreach($account_comment_tags as $act)
           {
               if($act->account_tag_id == $standard)
               {
                   $count_standard[] = $act;
               }
           }
           if($coach_or_coachee)
           {
                $standard_data[$key]['count'] = count($count_standard);
           }
           else
           {
                $standard_data[$key]['count'] = 0;
           }
           $standard_data[$key]['key'] = 'Framework_Standard_'.($key+1);
           $standard_data[$key]['key_pl'] = 'Performance_Level_'.($key+1);
           
           
           $sum = 0;
           $average = 'No Ratings';
           $pl_array = array();
           foreach($performance_details as $performance)
           {
               if($performance->standard_id == $standard)
               {
                    $sum = $sum + (int)$performance->rating_value; 
                    $pl_array[] = $performance;
               }
              
           }
           if(count($pl_array) != 0)
           {
                $average_number = $sum/count($pl_array);
                $average = $this->get_average_name($average_number,$st_array['framework_id'],$st_array['account_id']);
                
           }
           
           
           $sum_huddle = 0;
           $average_huddle = 'No Ratings';
           $pl_array_huddle = array();
           foreach($huddle_performance_details as $performance)
           {
               if($performance->standard_id == $standard)
               {
                    $sum_huddle = $sum_huddle + (int)$performance->rating_value; 
                    $pl_array_huddle[] = $performance;
               }
              
           }
           if(count($pl_array_huddle) != 0)
           {
                $average_number_huddle = $sum_huddle/count($pl_array_huddle);
                $average_huddle = $this->get_average_name($average_number_huddle,$st_array['framework_id'],$st_array['account_id']);
                
           }
           
           
           if($coach_or_coachee)
           {
                $standard_data[$key]['pl_average'] = $average;
           }
           else 
           {
                $standard_data[$key]['pl_average'] = 'No Ratings';
           }
           $standard_data[$key]['pl_average_huddle'] =  $average_huddle;
           if(!empty($st_array['standard_analytics_label']))
           {
               $standard_data[$key]['label'] = $st_array['standard_analytics_label'];
           }
           else if(!empty($st_array['tag_title']))
           {
               $standard_data[$key]['label'] = $st_array['tag_title'];
           }
           else 
           {
               $standard_data[$key]['label'] = $st_array['tag_html'];
           }
           $standard_data[$key]['account_tag_id'] = $st_array['account_tag_id'];
        }
        $result = array();
        $result['standard_data'] = $standard_data;
        $result['avg_pl_level'] = $avg_pl_level;
        return $result;
    }
    
    function get_average_name($average,$framework_id,$account_id)
    {
        $average_rating_name = 'No Ratings';
        if (!empty($framework_id)) {
            $standards_settings = AccountFrameworkSetting::where(array(
                        "account_tag_id" => $framework_id,
                        "site_id" => $this->site_id
                    ))->get()->toArray();
        }
        if (isset($standards_settings[0]['enable_performance_level'])) {
            $performace_level_switch = $standards_settings[0]['enable_performance_level'];
        } else {
            $performace_level_switch = false;
        }

        if ($performace_level_switch) {
            $account_framework_settings_performance_levels = AccountFrameworkSettingPerformanceLevel::select('AccountFrameworkSettingPerformanceLevel.*')->where(array(
                        'AccountFrameworkSettingPerformanceLevel.account_framework_setting_id' => $standards_settings[0]['id'],
                        'AccountFrameworkSettingPerformanceLevel.site_id' => $this->site_id
                    ))->orderBy('performance_level_rating')->get()->toArray();


            foreach ($account_framework_settings_performance_levels as $pl) {
                if ($average == $pl['performance_level_rating']) {
                    $average_rating_name = $pl['performance_level'];
                }
            }
        } else {

            $average_rating_name_details = AccountMetaData::where(array(
                        'account_id' => $account_id,
                        'site_id' => $this->site_id,
                        'meta_data_value' => $average
                    ))->whereRaw('meta_data_name like "metric_value_%"')->get()->toArray();
            $average_rating_name_details = isset($average_rating_name_details[0]) ? $average_rating_name_details[0] : '';
            if (!empty($average_rating_name_details)) {
                $average_rating_name = substr($average_rating_name_details['meta_data_name'], 13);
            } else {
                $average_rating_name = 'No Ratings';
            }
        }
        return $average_rating_name;
    }
    
     function get_custom_marker_data($document_ids,$marker_ids,$coach_or_coachee,$start_date,$end_date)
    {
        $markers_data = array();
        foreach ($marker_ids as $key => $marker)
        {
           $account_comment_tags = AccountCommentTag::whereIn('ref_id',$document_ids)->where(array('account_tag_id' => $marker  ))->whereRaw('AccountCommentTag.created_date >= "'.$start_date. '" AND AccountCommentTag.created_date <= "' .$end_date . '"' )->get()->toArray();
           if($coach_or_coachee)
           {
                $markers_data[$key]['count'] = count($account_comment_tags);
           }
           else 
           {
                $markers_data[$key]['count'] = 0;
           }
           $markers_data[$key]['key'] = 'Custom_Marker_'.($key+1);
           $standard_details = AccountTag::where(array('account_tag_id' => $marker ))->first();
           $markers_data[$key]['label'] = $standard_details['tag_title'];
           $markers_data[$key]['account_tag_id'] = $standard_details['account_tag_id'];
        }
        return $markers_data;
    }
    
    function add_report(Request $request){

      $report_name = $request->get("report_name");
      $report_type = $request->get("report_type");
      $report_attribute = json_encode($request->get("report_attribute"));
      $user_id = $request->get('user_id');
      $account_id = $request->get('account_id');

      $report_data [] =[
        "report_name" => $report_name,
        "report_type" => $report_type,
        "report_attributes" => $report_attribute,
        "created_by" => $user_id,
        "created_at" => date("Y-m-d H:i:s"),
        "updated_by" => $user_id,
        "updated_at" => date("Y-m-d H:i:s"),
        "account_id" => $account_id
      ];

      ReportConfiguration::insert($report_data);
      return response()->json(["status" => true, "message" => "Success"] );
    }
    
    
    function attributes_query($account_id,$account_ids,$user_ids,$all_huddle_ids,$report_config_id,$start_date,$end_date,$user_summary = false,$limit = '',$search = '',$all = false,$role = 130,$sort_by = false,$sorting_order = 'ASC',$paginate=true)
    {
                //dd($all);
                if($sort_by)
                {
                    $order_by = 'ORDER BY '.$sort_by . ' ' .$sorting_order ;
                }
                else
                {
                    $order_by = 'ORDER BY t0.full_name , t0.email'. ' ' .$sorting_order;
                }
                if($role == 125)
                {
                    $roles = "AND ua.role_id< 125 AND (u.is_active = '1' AND u.type = 'Active')";
                }
                else
                {
                    $roles = "AND ua.role_id< 130";
                }
                $report_data = ReportConfiguration::where(array("id" => $report_config_id))->first();
                $report_attributes = json_decode($report_data->report_attributes,True);
                $report_attributes_array = array();
                foreach($report_attributes as $row)
                {
                    if($row['status'])
                    {
                        $report_attributes_array[] = $row['name']; 
                    }
                }
                $report_attributes = $report_attributes_array;
                $exclude_user_ids = "2421,2422,2423,2427";
                $user_ids = implode(',', $user_ids);
                $all_huddle_ids = implode(',', $all_huddle_ids);
                if(empty($all_huddle_ids))
                {
                    $all_huddle_ids = '0';
                }
                if(empty($user_ids))
                {
                    $user_ids = '0';
                }
                $users_check = "";
                $users_check_ua = "";
                $users_check_dvh = "";
                $huddles_check = "";
                $huddles_check_afd = "";
                $huddles_check_ua = "";
                $users_check = " AND user_id IN (".$user_ids.") ";
                $users_check_ua = " AND ua.user_id IN (".$user_ids.") ";
                $users_check_dvh = " AND dvh.user_id IN (".$user_ids.") ";
                $huddles_check = " AND huddle_id IN (".$all_huddle_ids.") ";
                $huddles_check_afd = " AND afd.account_folder_id IN (".$all_huddle_ids.") ";
                $huddles_check_ua = " AND ua.account_folder_id IN (".$all_huddle_ids.") ";
                if($user_summary && empty($user_ids) && empty($all_huddle_ids))
                {
                    $users_check = "";
                    $users_check_ua = "";
                    $users_check_dvh = "";
                    $huddles_check = "";
                    $huddles_check_afd = "";
                    $huddles_check_ua = "";
                    $users_check = "";
                    $users_check_ua = "";
                    $users_check_dvh = "";
                    $huddles_check = "";
                    $huddles_check_afd = "";
                    $huddles_check_ua = "";
                }

                if($user_summary && !empty($user_ids) && empty($all_huddle_ids))
                {
                    //$users_check = "";
                    //$users_check_ua = "";
                    //$users_check_dvh = "";
                    $huddles_check = "";
                    $huddles_check_afd = "";
                    $huddles_check_ua = "";
                    //$users_check = "";
                    //$users_check_ua = "";
                    //$users_check_dvh = "";
                    $huddles_check = "";
                    $huddles_check_afd = "";
                    $huddles_check_ua = "";
                }
             
            $parent_child_all_accounts = implode(',', $account_ids);
            $default_user_type = $this->current_lang=='en' ? 'Participant' : 'Partícipe';
            $if_null_video_uploaded_counts = '';
            $sum_video_upload_counts = '';
            $if_null_share_upload_counts = '';
            $sum_share_upload_counts = '';
            $if_null_scripted_notes_shared_upload_counts = '';
            $sum_scripted_notes_shared_upload_counts = '';
            $if_null_videos_viewed_count = '';
            $sum_videos_viewed_count = '';
            $if_null_hours_uploaded = '';
            $sum_hours_uploaded = '';
            $if_null_comments_initiated_count = '';
            $sum_comments_initiated_count = '';
            $if_null_replies_initiated_count = '';
            $sum_replies_initiated_count = '';
            $if_null_huddle_discussion_created = '';
            $sum_huddle_discussion_created = '';
            $if_null_huddle_discussion_posts = '';
            $sum_huddle_discussion_posts = '';
            $if_null_documents_uploaded_count = '';
            $sum_documents_uploaded_count = '';
            $if_null_documents_viewed_count = '';
            $sum_documents_viewed_count = '';
            $if_null_live_sessions_count = '';
            $sum_live_sessions_count = '';
            $if_null_web_login_counts = '';
            $sum_web_login_counts = '';
            $hours_viewed = '';
            $live_hours_viewed = '';
            $if_null_live_hours_viewed = '';
            $if_null_hours_viewed = '';
            $if_null_live_hours_streamed = '';
            $live_hours_streamed = '';
            $if_null_videos_uploaded_to_workspace = '';
            $sum_videos_uploaded_to_workspace = '';
            $sum_videos_viewed_in_workspace = '';
            $sum_library_videos_viewed = '';
            $sum_huddles_created = '';
            $sum_workspace_video_notes = '';
            $sum_workspace_scripted_notes = '';
            $sum_workspace_resources_uploaded = '';
            $sum_workspace_resources_viewed = '';
            $if_null_videos_viewed_in_workspace = '';
            $if_null_library_videos_viewed = '';
            $if_null_huddles_created = '';
            $if_null_workspace_video_notes = '';
            $if_null_workspace_scripted_notes = '';
            $if_null_workspace_resources_uploaded = '';
            $if_null_workspace_resources_viewed = '';
            $sum_library_shared_upload = '';
            $if_null_library_shared_upload = '';
            $if_null_library_upload_counts = '';
            $sum_library_upload_counts = '';
            $full_name = '';
            $email = '';
            $user_type = '';
            
            
            $dvh_st_date = "AND DATE(dvh.created_date) >= DATE('".$start_date."')";
            $dvh_end_date = "AND DATE(dvh.created_date) <= DATE('".$end_date."')";
            $d_start_date = " AND d.created_date >='$start_date'";
            $d_end_date = "AND d.created_date <= '$end_date'";
            $cond_date = "AND `created_at`>=DATE('".$start_date."') AND `created_at`<=DATE('".$end_date."')"; 
            $all =  filter_var($all, FILTER_VALIDATE_BOOLEAN);
            if($all)
            {
                
                    //dd($all);
                    $dvh_st_date = '';
                    $dvh_end_date = '';
                    $d_start_date = '';
                    $d_end_date = '';
                    $cond_date = ''; 
                    $limit = '';
                    
                
            }else{
                if(!$paginate){
                    
                    $limit = '';
                }
            }
            
            if(in_array('Name',$report_attributes))
            {
                $full_name = 't0.full_name as user_name,';
            }
            if(in_array('Email',$report_attributes))
            {
                $email = 't0.email,';
            }
            if(in_array('User Type',$report_attributes) || in_array('Huddle Role',$report_attributes) )
            {
                $user_type = "IFNULL(t0.user_type_".$this->current_lang.",'".$default_user_type."') as user_type,";
            }
            if(in_array('Videos Uploaded to Huddles',$report_attributes) || true)
            {
                $if_null_video_uploaded_counts = 'IFNULL(video_upload_counts,0) AS video_upload_counts,';
                $sum_video_upload_counts = 'SUM(`video_upload_counts`) AS video_upload_counts,';
            }
            if(in_array('Videos Shared to Huddles',$report_attributes))
            {
                $if_null_share_upload_counts = 'IFNULL(shared_upload_counts,0) AS shared_upload_counts,';
                $sum_share_upload_counts = 'SUM(shared_upload_counts) AS shared_upload_counts,';
            }
            if(in_array('Notes Shared to Huddles',$report_attributes))
            {
               $if_null_scripted_notes_shared_upload_counts = 'IFNULL(scripted_notes_shared_upload_counts,0) AS scripted_notes_shared_upload_counts,';   
               $sum_scripted_notes_shared_upload_counts = 'SUM(scripted_notes_shared_upload_counts) AS scripted_notes_shared_upload_counts,';
            }
            if(in_array('Huddle Videos Viewed',$report_attributes))
            {
               $if_null_videos_viewed_count  = 'IFNULL(videos_viewed_count,0) AS videos_viewed_count,';
               $sum_videos_viewed_count = 'SUM(videos_viewed_count) AS videos_viewed_count,';
            }
            if(in_array('Hours Uploaded',$report_attributes))
            {
               $if_null_hours_uploaded = 'IFNULL(total_hours_uploaded,0) AS total_hours_uploaded,';
               $sum_hours_uploaded = 'ROUND(SUM(total_hours_uploaded)/60/60,2) AS total_hours_uploaded,';
            }
            if(in_array('Hours Viewed',$report_attributes) || true)
            {
                $if_null_hours_viewed = "IFNULL(t3.total_hours_viewed,0) AS total_hours_viewed,";
                $hours_viewed = "LEFT OUTER JOIN
                        (
                            SELECT ROUND(SUM(out_dvh.minutes_watched)/60/60,2) as total_hours_viewed, out_dvh.user_id, out_dvh.account_id FROM
                            (
                                  SELECT
                                  SUM((dvh.playback_rate * 5)) AS minutes_watched, dvh.`user_id` AS user_id, dvh.account_id
                                  FROM
                                    document_viewer_histories AS dvh
                                    INNER JOIN account_folder_documents AS afd
                                      ON afd.document_id = dvh.document_id ".$huddles_check_afd."
                                  WHERE dvh.account_id IN (".$parent_child_all_accounts.") ".$users_check_dvh." 
                                    ".$dvh_st_date."
                                    ".$dvh_end_date."
                                    GROUP BY dvh.`document_id`, dvh.`user_id`,dvh.account_id
                            ) out_dvh
                            GROUP BY out_dvh.account_id, out_dvh.user_id
                        ) t3
                        ON t0.account_id = t3.account_id AND t0.user_id=t3.user_id";
            }
            if(in_array('Huddle Video Comments',$report_attributes) || true)
            {
               $if_null_comments_initiated_count = 'IFNULL(comments_initiated_count,0) AS comments_initiated_count,';
               $sum_comments_initiated_count = 'SUM(comments_initiated_count) AS comments_initiated_count,';
            }
            if(in_array('Huddle Video Replies',$report_attributes) || true)
            {
               $if_null_replies_initiated_count = 'IFNULL(replies_initiated_count,0) AS replies_initiated_count,';
               $sum_replies_initiated_count = 'SUM(replies_initiated_count) AS replies_initiated_count,';
            }
            if(in_array('Huddle Resources Uploaded',$report_attributes) || true)
            {
               $if_null_documents_uploaded_count = 'IFNULL(documents_uploaded_count,0) AS documents_uploaded_count,';
               $sum_documents_uploaded_count = 'SUM(documents_uploaded_count) AS documents_uploaded_count,';
            }
            if(in_array('Huddle Resources Viewed',$report_attributes) || true)
            {
               $if_null_documents_viewed_count = 'IFNULL(documents_viewed_count,0) AS documents_viewed_count,';
               $sum_documents_viewed_count = 'SUM(documents_viewed_count) AS documents_viewed_count,';
            }
            if(in_array('Huddle Discussions Created',$report_attributes) || true)
            {
              $if_null_huddle_discussion_created = 'IFNULL(huddle_discussion_created,0) AS huddle_discussion_created,';
              $sum_huddle_discussion_created ='SUM(huddle_discussion_created) AS huddle_discussion_created,';
            }
            if(in_array('Huddle Discussions Posts',$report_attributes) || true)
            {
              $if_null_huddle_discussion_posts = 'IFNULL(huddle_discussion_posts,0) AS huddle_discussion_posts,';
              $sum_huddle_discussion_posts = 'SUM(huddle_discussion_posts) AS huddle_discussion_posts,';
            }
            if(in_array('Total Logins',$report_attributes))
            {
                $if_null_web_login_counts = 'IFNULL(t1.web_login_counts,0) as web_login_counts,';
                $sum_web_login_counts = ', SUM(web_login_counts) AS web_login_counts';
            }
            if(in_array('Standard Performance Levels',$report_attributes))
            {
                
            }
            if(in_array('Session Summary',$report_attributes))
            {
               
            }
            if(in_array('Framework Tags',$report_attributes))
            {
                
            }
            if(in_array('Custom Video Marker Tags',$report_attributes))
            {
                
            }
            if(in_array('Live Video Hours Watched',$report_attributes))
            {
                        $if_null_live_hours_viewed = "IFNULL(t4.live_total_hours_viewed,0) AS live_total_hours_viewed,";
                        $live_hours_viewed = "LEFT OUTER JOIN
                        (
                            SELECT ROUND(SUM(out_dvh.minutes_watched)/60/60,2) as live_total_hours_viewed, out_dvh.user_id, out_dvh.account_id FROM
                            (
                                  SELECT
                                    MAX(dvh.minutes_watched) AS minutes_watched, dvh.`user_id` AS user_id, dvh.account_id
                                  FROM
                                    document_viewer_histories AS dvh
                                    INNER JOIN account_folder_documents AS afd
                                      ON afd.document_id = dvh.document_id ".$huddles_check_afd."
                                    INNER JOIN user_activity_logs as ua       
                                    ON ua.ref_id = dvh.document_id
                                  WHERE dvh.account_id IN (".$parent_child_all_accounts.") ".$users_check_dvh." 
                                    ".$dvh_st_date."
                                    ".$dvh_end_date."
                                    AND ua.type IN (24)    
                                  GROUP BY dvh.`document_id`, dvh.`user_id`,dvh.account_id
                            ) out_dvh
                            GROUP BY out_dvh.account_id, out_dvh.user_id
                        ) t4
                        ON t0.account_id = t4.account_id AND t0.user_id=t4.user_id";
                }
            if(in_array('Live Sessions',$report_attributes))
            {
               $if_null_live_sessions_count  = 'IFNULL(live_sessions_count,0) AS live_sessions_count,';
               $sum_live_sessions_count ='SUM(live_sessions_count) AS live_sessions_count,';   
            }
            if(in_array('Live Hours Streamed',$report_attributes))
            {
                $if_null_live_hours_streamed = "IFNULL(t5.total_live_hours_streamed,0) AS total_live_hours_streamed,";
                $live_hours_streamed = "LEFT OUTER JOIN
		(
		   SELECT
		   d.created_by,
		   d.account_id,
                   ROUND(SUM(df.duration)/60/60,2) AS total_live_hours_streamed
		   FROM documents AS d
		   INNER JOIN document_files AS df
			 ON df.`document_id` = d.`id`
		   INNER JOIN user_activity_logs AS  ua
			 ON ua.ref_id = df.document_id
		   WHERE
		   d.site_id = " . $this->site_id . " AND
			d.account_id IN (" . $parent_child_all_accounts . ")
		   ".$d_start_date."
		   ".$d_end_date."
		   AND ua.type IN (24)
		   $huddles_check_ua
		   GROUP BY d.created_by,account_id
		) AS t5
                ON  t0.user_id = t5.created_by && t0.account_id = t5.account_id";
            }
            if(in_array('Videos Uploaded to Workspace',$report_attributes) || true)
            {
                $if_null_videos_uploaded_to_workspace = 'IFNULL(workspace_upload_counts,0) AS workspace_upload_counts,';
                $sum_videos_uploaded_to_workspace = 'SUM(workspace_upload_counts) AS workspace_upload_counts,';
            }
            if(in_array('Videos Viewed in Workspace',$report_attributes))
            {
                $if_null_videos_viewed_in_workspace = 'IFNULL(workspace_videos_viewed_count,0) AS workspace_videos_viewed_count,';
                $sum_videos_viewed_in_workspace = 'SUM(workspace_videos_viewed_count) AS workspace_videos_viewed_count,';
            }
            if(in_array('Library Videos Viewed',$report_attributes))
            {
                $if_null_library_videos_viewed = 'IFNULL(library_videos_viewed_count,0) AS library_videos_viewed_count,';
                $sum_library_videos_viewed = 'SUM(library_videos_viewed_count) AS library_videos_viewed_count,';
            }
            if(in_array('Huddles Created',$report_attributes))
            {
                $if_null_huddles_created = 'IFNULL(huddle_created_count,0) AS huddle_created_count,';
                $sum_huddles_created = 'SUM(huddle_created_count) AS huddle_created_count,';
            }
            if(in_array('Workspace Video Notes',$report_attributes) || true)
            {
                $if_null_workspace_video_notes = 'IFNULL(workspace_comments_initiated_count,0) AS workspace_comments_initiated_count,';
                $sum_workspace_video_notes = 'SUM(workspace_comments_initiated_count) AS workspace_comments_initiated_count,';
            }
            if(in_array('Workspace Notes',$report_attributes))
            {
                $if_null_workspace_scripted_notes = 'IFNULL(scripted_observations,0) AS scripted_observations,';
                $sum_workspace_scripted_notes = 'SUM(scripted_observations) AS scripted_observations,';
            }
            if(in_array('Workspace Resources Uploaded',$report_attributes) || true)
            {
                $if_null_workspace_resources_uploaded = 'IFNULL(workspace_resources_uploaded,0) AS workspace_resources_uploaded,';
                $sum_workspace_resources_uploaded = 'SUM(workspace_resources_uploaded) AS workspace_resources_uploaded,';
            }
            if(in_array('Workspace Resources Viewed',$report_attributes) || true)
            {
                $if_null_workspace_resources_viewed = 'IFNULL(workspace_resources_viewed,0) AS workspace_resources_viewed,';
                $sum_workspace_resources_viewed = 'SUM(workspace_resources_viewed) AS workspace_resources_viewed,';
            }
            if(in_array('Videos Shared to Library',$report_attributes))
            {
                $if_null_library_shared_upload = 'IFNULL(library_shared_upload_counts,0) AS library_shared_upload_counts,';
                $sum_library_shared_upload = 'SUM(library_shared_upload_counts) AS library_shared_upload_counts,';
            }
            if(in_array('Videos Uploaded to Library',$report_attributes) || true)
            {
                $if_null_library_upload_counts = 'IFNULL(library_upload_counts,0) AS library_upload_counts,';
                $sum_library_upload_counts = 'SUM(library_upload_counts) AS library_upload_counts,';
            }
            $translations = TranslationsManager::get_page_lang_based_content("Api/analytics", $this->httpreq->header("site_id"), '', $this->httpreq->header("current-lang"));
            //dd($translations);
                $query = "
                    SELECT 
                         t0.account_id, t0.company_name, t0.user_id, t0.image ,t0.full_name as user_name,t0.email,t0.full_name as account_name, t0.user_role, t0.is_user_active, ".$user_type." 
                        ".$if_null_web_login_counts." 
                        ".$if_null_video_uploaded_counts." 
                        ".$if_null_videos_viewed_count." 
                        ".$if_null_documents_viewed_count."  
                        ".$if_null_comments_initiated_count." 
                        ".$if_null_replies_initiated_count." 
                        ".$if_null_hours_uploaded."
                        ".$if_null_hours_viewed."
                        ".$if_null_live_hours_viewed."
                        ".$if_null_live_hours_streamed."    
                        ".$if_null_share_upload_counts." 
                        ".$if_null_documents_uploaded_count."
                        ".$if_null_huddle_discussion_created." 
                        ".$if_null_huddle_discussion_posts."
                        ".$if_null_scripted_notes_shared_upload_counts."
                        ".$if_null_live_sessions_count."
                        ".$if_null_videos_uploaded_to_workspace."
                        ".$if_null_videos_viewed_in_workspace."
                        ".$if_null_library_videos_viewed."
                        ".$if_null_huddles_created."
                        ".$if_null_workspace_video_notes."
                        ".$if_null_workspace_scripted_notes."
                        ".$if_null_workspace_resources_uploaded."
                        ".$if_null_workspace_resources_viewed."
                        ".$if_null_library_shared_upload."
                        ".$if_null_library_upload_counts."
                        'Extra Attribute'   
                        FROM
                        (
                            SELECT
                                ua.account_id, ua.user_id, image , username, email, CONCAT(`first_name`,' ',`last_name`) AS full_name, a.`company_name`, IF(u.is_active=1 AND u.`type`='Active', 1, 0) as is_user_active, ua.user_type_en, ua.user_type_es, 
                                CASE
                                    WHEN ua.role_id = 100 THEN '" . (isset($translations['account-owner']) ? $translations["account-owner"] : "Account Owner") . "' 
                                    WHEN ua.role_id = 110 THEN '" . (isset($translations["super-admin"]) ? $translations["super-admin"] : "Super Admin") . "'
                                    WHEN ua.role_id = 115 THEN '" . (isset($translations["admin"]) ? $translations["admin"] : "Admin") . "'
                                    WHEN ua.role_id = 120 THEN '" . (isset($translations["user"]) ? $translations["user"] : "User") . "'
                                    WHEN ua.role_id = 125 THEN '" . (isset($translations["viewer"]) ? $translations["viewer"] : "Viewer") . "'
                                END AS user_role
                            FROM
                            users_accounts AS ua 
                            INNER JOIN users u ON (ua.user_id = u.id)
                            INNER JOIN accounts a ON (ua.`account_id` = a.id)
                            WHERE (ua.account_id IN (".$parent_child_all_accounts.") ".$users_check_ua." AND u.id NOT IN ( ".$exclude_user_ids." ) ".$roles." ) 
                        ) AS t0
                        LEFT OUTER JOIN  
                        (
                            SELECT account_id, company_name, user_id, user_role, user_type ".$sum_web_login_counts."
                            FROM `analytics_login_summary`
                            WHERE 1=1 ".$users_check." AND `account_id` IN (".$parent_child_all_accounts.") ".$cond_date."
                            GROUP BY account_id, user_id
                        ) t1 
                        ON t0.account_id = t1.account_id AND t0.user_id = t1.user_id
                        LEFT OUTER JOIN
                        (
                            SELECT analytics_huddles_summary.account_id, analytics_huddles_summary.user_id, 
                                ".$sum_video_upload_counts." 
                                ".$sum_videos_viewed_count." 
                                ".$sum_documents_viewed_count."
                                ".$sum_comments_initiated_count."
                                ".$sum_replies_initiated_count."
                                ".$sum_hours_uploaded."
                                ".$sum_share_upload_counts."
                                ".$sum_documents_uploaded_count."
                                ".$sum_huddle_discussion_created."
                                ".$sum_huddle_discussion_posts."
                                ".$sum_scripted_notes_shared_upload_counts."
                                ".$sum_live_sessions_count."
                                ".$sum_videos_uploaded_to_workspace."
                                ".$sum_videos_viewed_in_workspace."
                                ".$sum_library_videos_viewed."
                                ".$sum_huddles_created."
                                ".$sum_workspace_video_notes."
                                ".$sum_workspace_scripted_notes."
                                ".$sum_workspace_resources_uploaded."
                                ".$sum_workspace_resources_viewed."
                                ".$sum_library_shared_upload."
                                ".$sum_library_upload_counts."
                                'Extra Attribute'   
                            FROM `analytics_huddles_summary`
                            join `users_accounts` ua on analytics_huddles_summary.account_id = ua.account_id and analytics_huddles_summary.user_id=ua.user_id
                            WHERE 
                            1=1 ".$huddles_check." 
                            AND analytics_huddles_summary.`account_id` IN (".$parent_child_all_accounts.") ".$users_check_ua." 
                            AND analytics_huddles_summary.user_id NOT IN ( ".$exclude_user_ids." ) 
                            ".$cond_date." 
                            AND `is_account_active`=1 AND `is_user_active`=1 
                            -- AND `is_huddle_active`=1 
                            GROUP BY analytics_huddles_summary.account_id, analytics_huddles_summary.user_id 
                        ) t2
                        ON t0.account_id = t2.account_id AND t0.user_id=t2.user_id
                        ".$hours_viewed."
                        ".$live_hours_viewed."
                        ".$live_hours_streamed."    
                        ".$search." 
                            
                        ".$order_by."
                        
                        ".$limit.";
                    ";

         return DB::select($query);
    }
    function delete_report(Request $request){
        $report_id = $request->get("report_id");
        ReportConfiguration::where("id", $report_id)->delete();
        return response()->json(["message"=>"Report deleted successfully!", "success"=> true]);
    }

    function get_reports(Request $request){
        $account_id = $request->get("account_id");
        $report_type = $request->get("report_type");
        $report_configurations = ReportConfiguration::where("account_id", $account_id)->where('report_type',$report_type)->orWhereRaw('(report_type = "'.$report_type.'" AND is_default = 1 )')->get();
        foreach($report_configurations as $key => $report_configuration){
            $report_configurations[$key]['report_attributes'] = (json_decode($report_configuration['report_attributes']));
        }
        $report_data = Report::where('report_type',$report_type)->first();
        $report_data['attributes'] = (json_decode($report_data['attributes']));
        return response()->json(["report_configuration"=> $report_configurations,"Report"=>$report_data,  "message"=>"Report fetched successfully!", "success"=> true]);
    }

    function update_reports(Request $request){
        $report_configurations = $request->get('report_configurations');
        $user_id = $request->get("user_id");
        $account_id = $request->get('account_id');
        $report_type = $request->get('report_type');
        foreach($report_configurations as  $report_configuration){
        if(isset($report_configuration['id'])){
        $update_data = array(
            "report_name" => $report_configuration['report_name'],
            "report_attributes" => json_encode($report_configuration['report_attributes']),
            "updated_at" => date("Y-m-d H:i:s"),
            "updated_by" => $user_id
        );
        $data = ReportConfiguration::where("id", $report_configuration['id'])->update($update_data);  
        }
        else{
        $report_data = array(
            "report_name" => $report_configuration['report_name'],
            "report_type" => $report_configuration['report_type'],
            "report_attributes" => json_encode($report_configuration['report_attributes']),
            "created_by" => $user_id,
            "created_at" => date("Y-m-d H:i:s"),
            "updated_by" => $user_id,
            "updated_at" => date("Y-m-d H:i:s"),
            "is_default" => isset($report_configuration['is_default']) ? $report_configuration['is_default'] : 0 ,
            "account_id" => $report_configuration['account_id'],
            'site_id' => $this->site_id
        );
         DB::table('report_configurations')->insert($report_data);  
      }
    }
    $report_configurations = ReportConfiguration::where("account_id", $account_id)->where('report_type',$report_type)->get();
    foreach($report_configurations as $key => $report_configuration){
        $report_configurations[$key]['report_attributes'] = (json_decode($report_configuration['report_attributes']));
    }
    $report_data = Report::where('report_type',$report_type)->first();
    $report_data['attributes'] = (json_decode($report_data['attributes']));
    return response()->json(["report_configuration"=> $report_configurations,"Report"=>$report_data ,"message"=>"Report Updated successfully!", "success"=> true]);
    }
   
    
    function coaching_report_csv(Request $request)
    {
        if($request->has('async') && $request->async == '1')
        {
            ini_set('memory_limit','5000M');
            ini_set('max_execution_time', 1000); //3 minutes
            set_time_limit(0);
        $response = $this->coaching_report($request);
        $response = $response->getData();
        $account_id = $request->account_id;
        $report_summary = json_decode(json_encode($response),true);
        
        $result = array();
        $count = 0;
        $header_data_array = array();
        $data = array();
        $user_name_email = array("User Name", "Email");
        $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
        $account_name = implode('_',$account_name);
        foreach($report_summary['attributes_header'] as $row){
            $header_data_array[] = $row['name'];
        }
    
        $header_data_array =  array_merge($user_name_email,$header_data_array);
        foreach ($report_summary['attributes_data'] as $row)
        {
                unset($row['huddle_role']);
                unset($row['image']);
                unset($row['account_id']);
                unset($row['user_id']);
                array_unshift($row,$row['email']);
                array_unshift($row,$row['user_name']);
                unset($row['email']);
                unset($row['user_name']);
                unset($row['coachees']);
                unset($row['huddle_ids']);
                unset($row['coach_id']);
                unset($row['document_ids']);
                $result[$count] = $row;
                $count++;
            
        }

        foreach ($report_summary['attributes_data'] as $row)
        {
                if(isset($row['coachees']))
            {
                foreach($row['coachees'] as $coachee)
                {
                    unset($coachee['huddle_role']);
                    unset($coachee['image']);
                    unset($coachee['account_id']);
                    unset($coachee['user_id']);
                    array_unshift($coachee,$coachee['email']);
                    array_unshift($coachee,$coachee['user_name']);
                    unset($coachee['email']);
                    unset($coachee['user_name']);
                    unset($coachee['huddle_ids']);
                    unset($coachee['coach_id']);
                    unset($coachee['document_ids']);
                    $data[$count] = $coachee;
                    $count++;
                }
               
            }
        }
        $result = array_merge($result,$data);
        $exportReportsToCSV = new \App\Services\Exports\Reporting\ExportToCSV;
        return $exportReportsToCSV->export($header_data_array,$result,$account_name,$request->file_name,$request->user_id);
        }
         else
        {
            
            $input = $request->all();
            $account_id = $request->account_id;
            $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
            $account_name = implode('_',$account_name);
            $account_name = $account_name . "_coaching_report_";
            $file_name = $account_name .date('YmdHis').'.csv';
            $input['file_name'] = $file_name;
            $input['async'] = 1;
            $input['paginate'] = false;
            //$query_string = http_build_query($input);
            //print_r($query_string);die;
            //exec('wget -O - "http://local.sibme.local/coaching_report_csv?'.$query_string.'" > /dev/null 2>&1 &');
            exec("php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "' --report=coaching_report_csv > /dev/null 2>&1 &" );
            $final_result = array();
            $final_result['filename'] = $file_name;
            return response()->json($final_result);
        }
    }

    function coaching_report_excel(Request $request){
        if($request->has('async') && $request->async == '1')
        {
            ini_set('memory_limit','5000M');
            ini_set('max_execution_time', 1000); //3 minutes
            set_time_limit(0);
        $response = $this->coaching_report($request);
        $response = $response->getData();
        $account_id = $request->account_id;
        $report_summary = json_decode(json_encode($response),true);

        $result = array();
        $count = 0;
        $header_data_array = array();
        $data = array();
        $user_name_email = array("User Name", "Email");
        $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
        $account_name = implode('_',$account_name);
        foreach($report_summary['attributes_header'] as $row){
            $header_data_array[] = $row['name'];
        }
    
        $header_data_array =  array_merge($user_name_email,$header_data_array);
        foreach ($report_summary['attributes_data'] as $row)
        {
                unset($row['huddle_role']);
                unset($row['image']);
                unset($row['account_id']);
                unset($row['user_id']);
                array_unshift($row,$row['email']);
                array_unshift($row,$row['user_name']);
                unset($row['email']);
                unset($row['user_name']);
                unset($row['coachees']);
                unset($row['huddle_ids']);
                unset($row['coach_id']);
                unset($row['document_ids']);
                $result[$count] = $row;
                $count++;
            
        }

        foreach ($report_summary['attributes_data'] as $row)
        {
                if(isset($row['coachees']))
            {
                foreach($row['coachees'] as $coachee)
                {
                    unset($coachee['huddle_role']);
                    unset($coachee['image']);
                    unset($coachee['account_id']);
                    unset($coachee['user_id']);
                    array_unshift($coachee,$coachee['email']);
                    array_unshift($coachee,$coachee['user_name']);
                    unset($coachee['email']);
                    unset($coachee['user_name']);
                    unset($coachee['huddle_ids']);
                    unset($coachee['coach_id']);
                    unset($coachee['document_ids']);
                    $data[$count] = $coachee;
                    $count++;
                }
               
            }
        }
        $result = array_merge($result,$data);
        $exportReportsToExcel = new \App\Services\Exports\Reporting\ExportToExcel;
        return $exportReportsToExcel->export($header_data_array,$result,$account_name,$request->file_name,$request->user_id);
        }
         else{

            $input = $request->all();
            $account_id = $request->account_id;
            $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
            $account_name = implode('_',$account_name);
            $account_name = $account_name . "_coaching_report_";
            $file_name = $account_name .date('YmdHis').'.xlsx';
            $input['file_name'] = $file_name;
            $input['async'] = 1;
            $input['paginate'] = false;
            //$query_string = http_build_query($input);
            //exec('wget -O - "http://local.sibme.local/coaching_report_excel?'.$query_string.'" > /dev/null 2>&1 &');
            exec("php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "' --report=coaching_report_excel > /dev/null 2>&1 &" );
            $final_result = array();
            $final_result['filename'] = $file_name;
            return response()->json($final_result);
        }
    }

    function coaching_report_pdf(Request $request){
      if($request->has('async') && $request->async == '1')
        {
            ini_set('memory_limit','5000M');
            ini_set('max_execution_time', 1000); //3 minutes
            set_time_limit(0);  
        $response = $this->coaching_report($request);
        $response = $response->getData();
        $account_id = $request->account_id;
        $report_summary = json_decode(json_encode($response),true);

        $result = array();
        $count = 0;
        $header_data_array = array();
        $data = array();
        $user_name_email = array("User Name", "Email");
        $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
        $account_name = implode('_',$account_name);
        foreach($report_summary['attributes_header'] as $row){
            $header_data_array[] = $row['name'];
        }
    
        $header_data_array =  array_merge($user_name_email,$header_data_array);
        foreach ($report_summary['attributes_data'] as $row)
        {
                unset($row['huddle_role']);
                unset($row['image']);
                unset($row['account_id']);
                unset($row['user_id']);
                array_unshift($row,$row['email']);
                array_unshift($row,$row['user_name']);
                unset($row['email']);
                unset($row['user_name']);
                unset($row['coachees']);
                $result[$count] = $row;
                $count++;
            
        }

        foreach ($report_summary['attributes_data'] as $row)
        {
                if(isset($row['coachees']))
            {
                foreach($row['coachees'] as $coachee)
                {
                    unset($coachee['huddle_role']);
                    unset($coachee['image']);
                    unset($coachee['account_id']);
                    unset($coachee['user_id']);
                    array_unshift($coachee,$coachee['email']);
                    array_unshift($coachee,$coachee['user_name']);
                    unset($coachee['email']);
                    unset($coachee['user_name']);
                    $data[$count] = $coachee;
                    $count++;
                }
               
            }
        }
        $result = array_merge($result,$data);
        $account_name = $account_name . "_coaching_report_";
        $this->export_pdf($header_data_array,$result,$account_name,$request->file_name);
        }
        else
        {

            $input = $request->all();
            $account_id = $request->account_id;
            $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
            $account_name = implode('_',$account_name);
            $account_name = $account_name . "_coaching_report_";
            $file_name = $account_name .date('YmdHis').'.pdf';
            $input['file_name'] = $file_name;
            $input['async'] = 1;
            $input['paginate'] = false;
            //$query_string = http_build_query($input);
            //exec('wget -O - "http://local.sibme.local/coaching_report_pdf?'.$query_string.'" > /dev/null 2>&1 &');
            exec("php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "' --report=coaching_report_pdf > /dev/null 2>&1 &" );
            $final_result = array();
            $final_result['filename'] = $file_name;
            return response()->json($final_result);
        
        }
    }
    
    function huddle_report(Request $request)
    {
         
        $input = $request->all();
        $paginate = isset($input['paginate']) && $input['paginate'] == false ? false : true;
        if(!is_array($input['account_ids']))
        {
            $input['account_ids'] = json_decode($input['account_ids']);
        }
        if(isset($input['standard_ids']) && !is_array($input['standard_ids']))
        {
            $input['standard_ids'] = explode(',',$input['standard_ids']);
        }
        if(isset($input['huddle_ids']) && !is_array($input['huddle_ids']))
        {
            $input['huddle_ids'] = json_decode($input['huddle_ids']);
        }
        $account_id = $input['account_id'];
        $account_ids = $input['account_ids'];
        if(empty($account_ids))
        {
            $account_ids = array($input['account_id']);
        }
        $user_id = $input['user_id'];
        $start_date = $input['start_date'];
        $end_date = $input['end_date'];
        $stDate = date_create($start_date);
        $start_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        $endDate = date_create($end_date);
        $end_date = date_format($endDate, 'Y-m-d'). ' 23:59:59';
        $page = (int)$input['page'];
        $page = $page - 1;
        $limit = $input['limit'];
        $offset = $page*$limit;
        $coach_bool = false;
        $coachee_bool = false;
        $framework_data = array();
        $custom_marker_data = array();
        $active = $input['active'];
        $huddle_include = [];
        if(in_array('1', $input['huddle_ids']) || in_array('3', $input['huddle_ids']))
        {
            $coach_bool = true;
        }
        if(in_array('2', $input['huddle_ids']) || in_array('4', $input['huddle_ids']))
        {
            $coachee_bool = true;
        }
        
        if(in_array('1', $input['huddle_ids']) || in_array('2', $input['huddle_ids']))
        {
            $huddle_include[] = 2;
        }
        
        if(in_array('3', $input['huddle_ids']) || in_array('4', $input['huddle_ids']) || in_array('8', $input['huddle_ids']) )
        {
            $huddle_include[] = 3;
        }
        
        if(in_array('5', $input['huddle_ids']))
        {
            $huddle_include[] = 1;
        }
        
        if(isset($input['report_id']))
        {
            $report_config_id = $input['report_id'];
        }
        else 
        {
            $default_report_config = ReportConfiguration::where(array('report_type' => 'huddle_report','is_default' => '1'))->first();
            $report_config_id = $default_report_config->id;
        }
        $report_data = ReportConfiguration::where(array("id" => $report_config_id))->first();
        $report_attributes = json_decode($report_data->report_attributes);
        $report_attributes_array = array();
        foreach($report_attributes as $row)
        {
            if($row->status)
            {
                $report_attributes_array[] = $row->name; 
            }
        }
        
        if($input['user_role'] == '100' || $input['user_role'] == '110')
        {
            $query_builder = AccountFolder::
                            join('account_folders_meta_data as afmd','AccountFolder.account_folder_id','=','afmd.account_folder_id')
                            ->where(array('AccountFolder.folder_type' => '1' , 'active' => '1','afmd.meta_data_name' => 'folder_type'))
                            ->whereIn('AccountFolder.account_id' , $account_ids)
                            ->whereIn('afmd.meta_data_value',$huddle_include);
                            if($active != 'true')
                            {
                                $query_builder = $query_builder->whereRaw('AccountFolder.created_date >= "'.$start_date. '" AND AccountFolder.created_date <= "' .$end_date . '"' );
                            }
            if(!empty($request->get('search')))
            {
                $search = $this->escape_string($request->get('search'));
                $query_builder = $query_builder->join('account_folder_users as afu','AccountFolder.account_folder_id','=','afu.account_folder_id')->join('users as u','afu.user_id','=','u.id');
                $query_builder = $query_builder
                                  //->whereRaw("( name like '%".$search."%' )");
                                  ->where(function($query) use ($search) {
                                      $query->orWhere('name','like','%'.$search.'%')
                                             ->orWhere('u.username','like','%'.$search.'%')
                                             ->orWhere('u.email','like','%'.$search.'%')
                                             ->orWhereIn('u.first_name',explode(' ',$search))
                                             ->orWhereIn('u.last_name',explode(' ',$search));
                                  });                
            }
            if($request->has('sort_by') && $request->get('sort_by') )
            {
                $query_builder
                        ->select(DB::raw('(SELECT SUM('.$request->get('sort_by').') FROM analytics_huddles_summary WHERE huddle_id = AccountFolder.`account_folder_id`) AS sorting_key'),'AccountFolder.*')
                        ->orderBy('sorting_key',$request->get('sort_by_order'));
            }
            $query_builder_clone = clone $query_builder;
            
            if($active == 'true')
            {

                $huddles = $query_builder->groupBy('AccountFolder.account_folder_id')->get()->toArray();
            }
            else
            {
                if($paginate){
                    $huddles = $query_builder->offset($offset)->limit($limit)->groupBy('AccountFolder.account_folder_id')->get()->toArray();
                }else{
                    $huddles = $query_builder->groupBy('AccountFolder.account_folder_id')->get()->toArray();
                }
            } 
            $huddle_counter = $query_builder_clone->distinct('AccountFolder.account_folder_id')->count('AccountFolder.account_folder_id');
            //dd($huddle_counter);
        }
        else
        {
            $query_array = array();
            $query_array['user_id'] = $user_id;
            $query_array['account_ids'] = $account_ids;
            $query_builder = AccountFolder::select('AccountFolder.*')->where(array('folder_type' => '1' , 'active' => '1'))
                        ->join('account_folders_meta_data as afmd','AccountFolder.account_folder_id','=','afmd.account_folder_id')
                        ->join('account_folder_users as afu','afu.account_folder_id','=','AccountFolder.account_folder_id')
                        ->leftJoin('account_folder_groups', 'AccountFolder.account_folder_id', '=', 'account_folder_groups.account_folder_id')
                        ->leftJoin('user_groups as ug', 'ug.group_id', '=', 'account_folder_groups.group_id')
                        ->whereIn('AccountFolder.account_id' , $account_ids)
                        ->where(array('afu.user_id' => $user_id,'afmd.meta_data_name' => 'folder_type'))
                        ->whereIn('afmd.meta_data_value',$huddle_include)
                
                        ->orWhere(function($query) use ($query_array) {$query->where(array('ug.user_id' => $query_array['user_id']))->whereIn('AccountFolder.account_id' , $query_array['account_ids']) ; });

                        if($active != 'true')
                        {
                            $query_builder= $query_builder->whereRaw('AccountFolder.created_date >= "'.$start_date. '" AND AccountFolder.created_date <= "' .$end_date . '"' );
                        }


            if(!empty($request->get('search')))
            {
                $search = $this->escape_string($request->get('search'));
                $query_builder = $query_builder->join('account_folder_users as afu','AccountFolder.account_folder_id','=','afu.account_folder_id')->join('users as u','afu.user_id','=','u.id');
                $query_builder = $query_builder
                                  //->whereRaw("( name like '%".$search."%' )");
                                  ->where(function($query) use ($search) {
                                      $query->orWhere('name','like','%'.$search.'%')
                                             ->orWhere('u.username','like','%'.$search.'%')
                                             ->orWhere('u.email','like','%'.$search.'%');
                                  });
            }
            if($request->has('sort_by') && $request->get('sort_by') )
            {
                $query_builder
                        ->select(DB::raw('(SELECT SUM('.$request->get('sort_by').') FROM analytics_huddles_summary WHERE huddle_id = AccountFolder.`account_folder_id`) AS sorting_key'),'AccountFolder.*')
                        ->orderBy('sorting_key',$request->get('sort_by_order'));
            }
            $query_builder_clone = clone $query_builder;
            if($active == 'true')
            {
                $huddles = $query_builder->groupBy('AccountFolder.account_folder_id')->get()->toArray();
            }
            else
            {
                if($paginate){
                    $huddles = $query_builder->offset($offset)->limit($limit)->groupBy('AccountFolder.account_folder_id')->get()->toArray();
                }else{
                    $huddles = $query_builder->groupBy('AccountFolder.account_folder_id')->get()->toArray();
                }
                
            }           
            $huddle_counter = $query_builder_clone->distinct('AccountFolder.account_folder_id')->count('AccountFolder.account_folder_id');
        }
        $final_array = array();
        foreach ($huddles as $key => $huddle)
        {
            // $participants = HelperFunctions::get_huddle_participant_ids($huddle['account_folder_id'],$this->site_id);
            // //$attribute_data = $this->attributes_query($account_id,array($huddle['account_id']),$participants,array($huddle['account_folder_id']),$report_config_id,$start_date,$end_date,false,'','',$active);
            // $attribute_data = $this->attributes_query($account_id,array($huddle['account_id']),$participants,array($huddle['account_folder_id']),$report_config_id,$start_date,$end_date,false,'LIMIT 10 OFFSET 0','',$active,130,$request->get('sort_by'),$request->get('sort_by_order'),true);
            // //dd($attribute_data);
            // $attribute_data_count = $this->attributes_query($account_id,array($huddle['account_id']),$participants,array($huddle['account_folder_id']),$report_config_id,$start_date,$end_date,false,'','',$active,130,$request->get('sort_by'),$request->get('sort_by_order'),true);

            // $attribute_data_count = count($attribute_data_count);

            // $updated_attribute_data = array();
            // $attribute_data = json_decode(json_encode($attribute_data), True);
            // $huddle_type = HelperFunctions::get_huddle_type($huddle['account_folder_id'],$this->site_id);
            // $documents_ids = AccountFolderDocument::select('document_id')->whereIn('account_folder_id',array($huddle['account_folder_id']))->get()->toArray();
            // $custom_markers_ids = AccountTag::select('account_tag_id')->where(array('tag_type' => '1' , 'account_id' => $account_id))->get()->toArray();
            // $avg_pl_level = $this->get_avg_performance_level($documents_ids,$input['framework_id'],true);
            // $custom_marker_data = $this->get_custom_marker_data($documents_ids,$custom_markers_ids,true,$start_date,$end_date);
            // $account_user_custom_fields = CustomField::get_custom_fields($account_id,'1');
            // if(isset($input['standard_ids']))
            // {
            //     $frameworks = $this->get_framework_tags_data($documents_ids,$input['standard_ids'],true,array($huddle['account_folder_id']));
            //     $framework_data = $frameworks['standard_data'];
            // }
            // else
            // {
            //     $framework_data = array();
            // }
            // $huddle_sum_array = array();
            // foreach($attribute_data as $value => $data)
            // {

            //     $huddle_role = HelperFunctions::get_huddle_roles($huddle['account_folder_id'],$data['user_id']);

            //     $bool = false;
            //     if($huddle_role == 200 && $huddle_type != '1' )
            //     {
            //         $bool = $coach_bool;
            //     }
            //     else
            //     {
            //         $bool = $coachee_bool;
            //     }
            //     $new_cm_arr = array();
            //     $new_fd_arr = array();
            //     if(!$bool)
            //     {
            //         foreach($custom_marker_data as $ind => $cm)
            //         {
            //             $new_cm_arr[$ind] = $cm;
            //             $new_cm_arr[$ind]['count'] = 0;
            //         }
            //         foreach($framework_data as $fd_num => $fd)
            //         {
            //             $new_fd_arr[$fd_num] = $fd;
            //             $new_fd_arr[$fd_num]['count'] = 0;
            //             $new_fd_arr[$fd_num]['pl_average'] = 0;
            //         }
            //         $avg_pl_level = 0;
            //         $framework_data = $new_fd_arr;
            //         $custom_marker_data = $new_cm_arr;
            //     }

            //     if($huddle_type == '1')
            //     {
            //         $bool = true;
            //     }
            //     if($huddle_type == '2' && in_array('3', $input['huddle_ids']) && !in_array('1', $input['huddle_ids'])  )
            //     {
            //         $bool = false;
            //     }
            //     if($huddle_type == '2' && in_array('4', $input['huddle_ids']) && !in_array('2', $input['huddle_ids'])  )
            //     {
            //         $bool = false;
            //     }
            //     if($huddle_type == '3' && in_array('1', $input['huddle_ids']) && !in_array('3', $input['huddle_ids'])  )
            //     {
            //         $bool = false;
            //     }
            //     if($huddle_type == '3' && in_array('2', $input['huddle_ids']) && !in_array('4', $input['huddle_ids'])  )
            //     {
            //         $bool = false;
            //     }

            //     $count = 0;
                
            //     foreach($report_attributes as $row)
            //         {
            //             if(!isset($huddle_sum_array[$count]) && $row->status && !empty($row->value) && is_numeric($data[$row->value]))
            //             {
            //                 $huddle_sum_array[$count] = 0;
            //             }
            //             else if($row->status && !empty($row->value) && !is_numeric($data[$row->value]))
            //             {
            //                 $huddle_sum_array[$count] = '';
            //             }
            //             if(!empty($row->value) && $row->status)
            //             {
            //                 $updated_attribute_data[$value][] = $data[$row->value];
            //                 if(is_numeric($data[$row->value]))
            //                 {
            //                     $huddle_sum_array[$count] = round($huddle_sum_array[$count] + $data[$row->value],2);
            //                 }
            //             }

            //             if($row->status && !empty($row->value) )
            //             {
            //                 $count++;
            //             }

            //             if($row->name == 'Framework Tags' && $row->status && !in_array('Standard Performance Levels',$report_attributes_array))
            //             {
            //                 foreach ($framework_data as $standard_data)
            //                 {
            //                     if(!isset($huddle_sum_array[$count]))
            //                      {
            //                          $huddle_sum_array[$count] = 0;
            //                      }
            //                    $updated_attribute_data[$value][] = $standard_data['count'];
            //                    $huddle_sum_array[$count] = $huddle_sum_array[$count] + $standard_data['count'];
            //                    $count++;
            //                 }
            //             }

            //             if($row->name == 'Standard Performance Levels' && $row->status && !in_array('Framework Tags',$report_attributes_array))
            //             {
            //                foreach ($framework_data as $standard_data)
            //                 {
            //                     if(!isset($huddle_sum_array[$count]))
            //                     {
            //                         $huddle_sum_array[$count] = $standard_data['pl_average_huddle'];
            //                     }
            //                    $updated_attribute_data[$value][] = $standard_data['pl_average'];
            //                    //$huddle_sum_array[$count] = $huddle_sum_array[$count] + $standard_data['pl_average'];
            //                    $count++;
            //                 }
            //             }
            //             if($row->name == 'Framework Tags' && $row->status && in_array('Standard Performance Levels',$report_attributes_array ))
            //             {

            //                 foreach ($framework_data as $standard_data)
            //                 {
            //                     if(!isset($huddle_sum_array[$count]))
            //                     {
            //                         $huddle_sum_array[$count] = 0;
            //                     }
            //                    $updated_attribute_data[$value][] = $standard_data['count'];
            //                    $huddle_sum_array[$count] = $huddle_sum_array[$count] + $standard_data['count'];
            //                    $count++;
            //                     if(!isset($huddle_sum_array[$count]))
            //                     {
            //                         $huddle_sum_array[$count] = $standard_data['pl_average_huddle'];
            //                     }
            //                    $updated_attribute_data[$value][] = $standard_data['pl_average'];
            //                   // $huddle_sum_array[$count] = $huddle_sum_array[$count] + $standard_data['pl_average'];
            //                    $count++;
            //                 }
            //             }
                        
            //             if($row->name == 'Custom Video Marker Tags' && $row->status)
            //             {
            //                 foreach ($custom_marker_data as $marker_data)
            //                 {
            //                     if(!isset($huddle_sum_array[$count]))
            //                     {
            //                         $huddle_sum_array[$count] = 0;
            //                     }
            //                    $huddle_sum_array[$count] = $huddle_sum_array[$count] + $marker_data['count'];
            //                    $updated_attribute_data[$value][] = $marker_data['count'];
            //                    $count++;
            //                 }
            //             }
                        
            //             if($row->name == 'Avg Performance Levels' && $row->status)
            //             {
            //                 if(!isset($huddle_sum_array[$count]))
            //                     {
            //                         $huddle_sum_array[$count] = '';
            //                     }
            //                     $count++;
            //                     $updated_attribute_data[$value][] = $avg_pl_level;
            //             }
            //             if($row->name == 'Is Active' && $row->status)
            //             {
            //                 $user_active_detail = UserAccount::where(array('user_id' => $data['user_id'] , 'account_id' => $data['account_id']))->first();
            //                 if($user_active_detail->is_active == '1' && $user_active_detail->status_type == 'Active' )
            //                 {
            //                     $updated_attribute_data[$value][] = 'Yes';
            //                 }
            //                 else
            //                 {
            //                     $updated_attribute_data[$value][] = 'No';
            //                 }
            //                 if(!isset($huddle_sum_array[$count]))
            //                 {
            //                     $huddle_sum_array[$count] = '';
            //                 }
            //                 $count++;
            //             }
            //             if($row->name == 'Huddle Role' && $row->status)
            //             {
            //                $updated_attribute_data[$value][] = $this->huddle_role_name($huddle_role,$huddle_type);
            //                 if(!isset($huddle_sum_array[$count]))
            //                 {
            //                     $huddle_sum_array[$count] = '';
            //                 }
            //                 $count++;
            //             }
            //             if($row->name == 'Last Modified' && $row->status)
            //             {
            //                 $updated_attribute_data[$value][] = isset($huddle['last_edit_date'])? self::getCurrentLangDate(strtotime($huddle['last_edit_date']),'archive_module') : '';
            //                 if(!isset($huddle_sum_array[$count]))
            //                 {
            //                     $huddle_sum_array[$count] = '';
            //                 }
            //                 $count++;
            //             }

            //         if($row->name == 'Custom Fields (User-level data)' && $row->status)
            //         {

            //             if(isset($account_user_custom_fields['user_summary']['custom_fields']))
            //             {
            //                 $custom_fields_array = $account_user_custom_fields['user_summary']['custom_fields'];
            //                 foreach($custom_fields_array as $cf)
            //                 {
            //                     if(!isset($huddle_sum_array[$count]))
            //                     {
            //                         $huddle_sum_array[$count] = '';
            //                     }
            //                     $count++;
            //                     $user_custom_values = UserCustomField::get_users_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,'');
            //                     if(!empty($user_custom_values))
            //                     {
            //                             if($user_custom_values[0]->custom_field_value == 'true')
            //                             {
            //                                $user_custom_values[0]->custom_field_value = 'Yes';
            //                             }
            //                             if($user_custom_values[0]->custom_field_value == 'false')
            //                             {
            //                                $user_custom_values[0]->custom_field_value = 'No';
            //                             }
            //                         $updated_attribute_data[$value][] = $user_custom_values[0]->custom_field_value;
            //                     }
            //                     else
            //                     {
            //                         $updated_attribute_data[$value][] = '';
            //                     }
            //                 }
            //             }
                        
            //             if(isset($account_user_custom_fields['user_summary']['parent_custom_fields']))
            //             {
            //                 $custom_fields_array = $account_user_custom_fields['user_summary']['parent_custom_fields'];
            //                 foreach($custom_fields_array as $cf)
            //                 {
            //                     if(!isset($huddle_sum_array[$count]))
            //                     {
            //                         $huddle_sum_array[$count] = '';
            //                     }
            //                     $count++;
            //                     $user_custom_values = UserCustomField::get_users_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,'');
            //                     if(!empty($user_custom_values))
            //                     {
            //                             if($user_custom_values[0]->custom_field_value == 'true')
            //                             {
            //                                $user_custom_values[0]->custom_field_value = 'Yes';
            //                             }
            //                             if($user_custom_values[0]->custom_field_value == 'false')
            //                             {
            //                                $user_custom_values[0]->custom_field_value = 'No';
            //                             }
            //                         $updated_attribute_data[$value][] = $user_custom_values[0]->custom_field_value;
            //                     }
            //                     else
            //                     {
            //                         $updated_attribute_data[$value][] = '';
            //                     }
            //                 }

            //             }

            //         }
            //     }

            //             $updated_attribute_data[$value]['image'] = $data['image'];
            //             $updated_attribute_data[$value]['account_id'] =  $data['account_id'];
            //             $updated_attribute_data[$value]['user_id'] = $data['user_id'];
            //             $updated_attribute_data[$value]['email'] = $data['email'];
            //             $updated_attribute_data[$value]['user_name'] = $data['user_name'];
            //             $updated_attribute_data[$value]['huddle_id'] = $huddle['account_folder_id'];

            // }




            $_data = $this->huddle_report_users($request,$huddle['account_folder_id'],false);
            $updated_attribute_data = $_data['updated_attribute_data'];
            $huddle_sum_array = $_data['huddle_sum_array'];
            $attribute_data_count = $_data['attribute_data_count'];
            $huddle_type = $_data['huddle_type'];

            $account_user_custom_fields = $_data['account_user_custom_fields'];
            $custom_markers_ids = $_data['custom_markers_ids'];
            $avg_pl_level = $_data['avg_pl_level'];
            $custom_marker_data = $_data['custom_marker_data'];
            $framework_data = $_data['framework_data'];


            $huddle_detail = AccountFolder::where(array('account_folder_id' => $huddle['account_folder_id']))->first();
            $final_array[$key] = $huddle_sum_array;
            $final_array[$key]['huddle_name'] = $huddle_detail->name;
            if($huddle_type == '3')
            {
               $final_array[$key]['huddle_type'] = 'Assessment Huddle'; 
            }
            else if($huddle_type == '2')
            {
               $final_array[$key]['huddle_type'] = 'Coaching Huddle';  
            }
            else 
            {
                $final_array[$key]['huddle_type'] = 'Collaboration Huddle'; 
            }
            $final_array[$key]['huddle_users_data'] = $updated_attribute_data;
            $final_array[$key]['huddle_users_count'] = $attribute_data_count;

        }
        $header_data = array();
        $count = 0;
        $translations =  TranslationsManager::get_page_lang_based_content("Api/analytics", $request->header("site_id"), '', $request->header("current-lang"));
        foreach($report_attributes as $key => $row)
        {
            if(!empty($row->value) && $row->status)
            {
                $key = str_replace(' ','-',strtolower($row->name));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : $row->name;
                //$header_data[$count]['name'] = $row->name;
                $header_data[$count]['key'] = str_replace(' ','_',strtolower($row->name));
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['sorting'] = true;
                $header_data[$count]['sorting_key'] = $row->value;
                if($row->name == 'Account Name' || $row->name == 'User Role' || $row->name == 'User Type'  )
                {
                    $header_data[$count]['detail'] = false;
                    $header_data[$count]['sorting'] = false;
                    $header_data[$count]['sorting_key'] = '';
                }
                $count++;
            }
            
            if($row->name == 'Is Active' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Is Active'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Is Active';
                //$header_data[$count]['name'] = 'Is Active';
                $header_data[$count]['key'] = str_replace(' ','_',strtolower('Is Active'));
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['detail'] = false;
                $count++;            
            }
            
            if($row->name == 'Huddle Role' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Huddle Role'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Huddle Role';
                //$header_data[$count]['name'] = 'Is Active';
                $header_data[$count]['key'] = str_replace(' ','_',strtolower('Huddle Role'));
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['detail'] = false;
                $count++;            
            }
            
            if($row->name == 'Last Modified' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Last Modified'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Last Modified';
                //$header_data[$count]['name'] = 'Last Modified';
                $header_data[$count]['key'] = str_replace(' ','_',strtolower('Last Modified'));
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['detail'] = false;
                $count++; 
            }
            
            if($row->name == 'Framework Tags' && $row->status && !in_array('Standard Performance Levels',$report_attributes_array))
            {
                foreach ($framework_data as $standard_data)
                {
                    $header_data[$count]['name'] = $standard_data['label'];
                    $header_data[$count]['key'] = 'standard';
                    $header_data[$count]['standard'] = true;
                    $header_data[$count]['performance'] = false;
                    $header_data[$count]['markers'] = false;
                    $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                    $count++;
                }
            }
            if($row->name == 'Standard Performance Levels' && $row->status && !in_array('Framework Tags',$report_attributes_array))
            {
                foreach ($framework_data as $standard_data)
                {
                    $header_data[$count]['name'] = $standard_data['label'];
                    $header_data[$count]['key'] = 'performance';
                    $header_data[$count]['standard'] = false;
                    $header_data[$count]['performance'] = true;
                    $header_data[$count]['markers'] = false;
                    $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                    $count++;
                }
            }
            if($row->name == 'Framework Tags' && $row->status && in_array('Standard Performance Levels',$report_attributes_array))
            {
                foreach ($framework_data as $standard_data)
                {
                    $header_data[$count]['name'] = $standard_data['label'];
                    $header_data[$count]['key'] = 'standard';
                    $header_data[$count]['standard'] = true;
                    $header_data[$count]['performance'] = false;
                    $header_data[$count]['markers'] = false;
                    $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                    $count++;
                    $header_data[$count]['name'] = $standard_data['label'];
                    $header_data[$count]['key'] = 'performance';
                    $header_data[$count]['standard'] = false;
                    $header_data[$count]['performance'] = true;
                    $header_data[$count]['markers'] = false;
                    $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                    $count++;
                }
            }
            if($row->name == 'Custom Video Marker Tags' && $row->status)
            {
                foreach ($custom_marker_data as $marker_data)
                {
                    $header_data[$count]['name'] = $marker_data['label'];
                    $header_data[$count]['key'] = 'markers';
                    $header_data[$count]['standard'] = false;
                    $header_data[$count]['performance'] = false;
                    $header_data[$count]['markers'] = true;
                    $header_data[$count]['standard_id'] = $marker_data['account_tag_id'];
                    $count++;
                }
            }
            if($row->name == 'Avg Performance Levels' && $row->status)
            {
                
                    $key = str_replace(' ','-',strtolower('Avg Performance Levels'));
                    $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Avg Performance Levels';
                    $header_data[$count]['name'] = 'Avg Performance Levels';
                    $header_data[$count]['key'] = 'avg_performace_levels';
                    $header_data[$count]['standard'] = false;
                    $header_data[$count]['performance'] = false;
                    $header_data[$count]['markers'] = false;
                    $header_data[$count]['standard_id'] = false;
                    $header_data[$count]['avg_performance_levels'] = true;
                    $header_data[$count]['detail'] = false;
                    $count++;
                
            }
            
            if($row->name == 'Custom Fields (User-level data)' && $row->status)
            {
                $custom_fields_array = array();
                if(isset($account_user_custom_fields['user_summary']['custom_fields']))
                {
                    $custom_fields_array = $account_user_custom_fields['user_summary']['custom_fields'];
                    foreach($custom_fields_array as $cf)
                    {
                        $header_data[$count]['name'] = $cf->field_label;
                        $header_data[$count]['key'] = '';
                        $header_data[$count]['standard'] = false;
                        $header_data[$count]['performance'] = false;
                        $header_data[$count]['markers'] = false;
                        $header_data[$count]['standard_id'] = false;
                        $header_data[$count]['custom_fields'] = true;
                        $header_data[$count]['detail'] = false;
                        $count++;
                    }
                }
                if(isset($account_user_custom_fields['user_summary']['parent_custom_fields']))
                {
                    $custom_fields_array = $account_user_custom_fields['user_summary']['parent_custom_fields'];
                    foreach($custom_fields_array as $cf)
                    {
                        $header_data[$count]['name'] = $cf->field_label;
                        $header_data[$count]['key'] = '';
                        $header_data[$count]['standard'] = false;
                        $header_data[$count]['performance'] = false;
                        $header_data[$count]['markers'] = false;
                        $header_data[$count]['standard_id'] = false;
                        $header_data[$count]['custom_fields'] = true;
                        $header_data[$count]['detail'] = false;
                        $count++;
                    }
                }
            }
            
        }
        $result = array();
        $result['attributes_header'] = $header_data;
        $result['attributes_data'] = $final_array;
        $result['total_huddles'] = $huddle_counter;
        $result['sort_by'] = ($request->has('sort_by')) ? $request->get('sort_by') : '';
        $result['sort_by_order'] = ($request->has('sort_by_order')) ? $request->get('sort_by_order') : '';
        $result['page'] = $page+1;
        $result['limit'] = $limit;
        return response()->json($result, 200, [], JSON_PRETTY_PRINT);
    }

    function huddle_report_csv(Request $request){
        if($request->has('async') && $request->async == '1')
        {
            ini_set('memory_limit','5000M');
            ini_set('max_execution_time', 5000); //3 minutes
            set_time_limit(0);

            $response = $this->huddle_report($request);
            $response = $response->getData();
            $account_id = $request->account_id;
            $huddle_report_summary = json_decode(json_encode($response),true);
            $result = array();
            $count = 0;
            $header_data_array = array();
            $huddle_name = array();
            $user_name_email = array("User Name", "Email");
            $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
            $account_name = implode('_',$account_name);

            foreach($huddle_report_summary['attributes_header'] as $row){

                $header_data_array[] = $row['name'];
            }

            $header_data_array =  array_merge($user_name_email,$header_data_array);

            foreach($huddle_report_summary['attributes_data'] as $row){

                $huddle_name[0] = $row['huddle_name'];
                $huddle_name[1] = $row['huddle_type'];
                $result[$count] = $huddle_name;
                $huddle_name = [];
                $count++;
                if(isset($row['huddle_users_data']))
                {
                    foreach($row['huddle_users_data'] as $huddle_users_data)
                    {
                        unset($huddle_users_data['image']);
                        unset($huddle_users_data['account_id']);
                        unset($huddle_users_data['user_id']);
                        array_unshift($huddle_users_data,$huddle_users_data['email']);
                        array_unshift($huddle_users_data,$huddle_users_data['user_name']);
                        unset($huddle_users_data['email']);
                        unset($huddle_users_data['user_name']);
                        unset($huddle_users_data['huddle_ids']);
                        unset($huddle_users_data['huddle_id']);
                        unset($huddle_users_data['document_ids']);
                        $result[$count] = $huddle_users_data;
                        $count++;
                    }
                }  
                
            }
            
            $exportHuddleReportsToCSV = new \App\Services\Exports\HuddleReport\ExportToCSV;
            $exportHuddleReportsToCSV->export($header_data_array,$result,$account_name,$request->file_name,$request->user_id);

        }
        
        else
        {
            
            $input = $request->all();
            $account_id = $request->account_id;
            $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
            $account_name = implode('_',$account_name);
            $account_name = $account_name . "_huddle_report_";
            $file_name = $account_name .date('YmdHis').'.csv';
            $input['file_name'] = $file_name;
            $input['async'] = 1;
            $input['paginate'] = false;
            //$query_string = http_build_query($input);
            //print_r($query_string);
            //exec('wget -O - "http://local.sibme.local/huddle_report_csv?'.$query_string.'" > /dev/null 2>&1 &');
            exec("php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "' --report=huddle_report_csv > /dev/null 2>&1 &" );
            $final_result = array();
            $final_result['filename'] = $file_name;
            //$final_result['cron'] = 'wget -O - "http://local.sibme.local/huddle_report_csv?'.$query_string;
            return response()->json($final_result);
        }
    }

    function huddle_report_excel(Request $request){
        if($request->has('async') && $request->async == '1')
        {
            ini_set('memory_limit','5000M');
            ini_set('max_execution_time', 1000); //3 minutes
            set_time_limit(0);
        $response = $this->huddle_report($request);
        $response = $response->getData();
        $account_id = $request->account_id;
        $huddle_report_summary = json_decode(json_encode($response),true);
        $result = array();
        $count = 0;
        $header_data_array = array();
        $user_name_email = array("User Name", "Email");
        $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
        $account_name = implode('_',$account_name);
        foreach($huddle_report_summary['attributes_header'] as $row){
            $header_data_array[] = $row['name'];
        }
    
        $header_data_array =  array_merge($user_name_email,$header_data_array);
        foreach($huddle_report_summary['attributes_data'] as $row){
            $huddle_name[0] = $row['huddle_name'];
            $huddle_name[1] = $row['huddle_type'];
            $result[$count] = $huddle_name;
            $huddle_name = [];
            $count++;
            if(isset($row['huddle_users_data']))
                {
                    foreach($row['huddle_users_data'] as $huddle_users_data)
                    {
                        unset($huddle_users_data['image']);
                        unset($huddle_users_data['account_id']);
                        unset($huddle_users_data['user_id']);
                        array_unshift($huddle_users_data,$huddle_users_data['email']);
                        array_unshift($huddle_users_data,$huddle_users_data['user_name']);
                        unset($huddle_users_data['email']);
                        unset($huddle_users_data['user_name']);
                        unset($row['huddle_name']);
                        unset($row['huddle_type']);
                        unset($row['huddle_ids']);
                        unset($row['document_ids']);
                        unset($huddle_users_data['huddle_ids']);
                        unset($huddle_users_data['huddle_id']);
                        unset($huddle_users_data['document_ids']);
                        $result[$count] = $huddle_users_data;
                        $count++;
                    }
                }  
        }
        $exportHuddleReportsToExcel = new \App\Services\Exports\HuddleReport\ExportToExcel;
        return $exportHuddleReportsToExcel->export($header_data_array,$result,$account_name,$request->file_name,$request->user_id);
        }
        else{

            $input = $request->all();
            $account_id = $request->account_id;
            $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
            $account_name = implode('_',$account_name);
            $account_name = $account_name . "_huddle_report_";
            $file_name = $account_name .date('YmdHis').'.xlsx';
            $input['file_name'] = $file_name;
            $input['async'] = 1;
            $input['paginate'] = false;
            //$query_string = http_build_query($input);
            //exec('wget -O - "http://local.sibme.local/huddle_report_excel?'.$query_string.'" > /dev/null 2>&1 &');
            exec("php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "' --report=huddle_report_excel > /dev/null 2>&1 &" );
            $final_result = array();
            $final_result['filename'] = $file_name;
            return response()->json($final_result);
        }
    }
    
    function huddle_report_pdf(Request $request){

        if($request->has('async') && $request->async == '1')
        {
            ini_set('memory_limit','5000M');
            ini_set('max_execution_time', 1000); //3 minutes
            set_time_limit(0);
            $response = $this->huddle_report($request);
            $response = $response->getData();
            $account_id = $request->account_id;
            $huddle_report_summary = json_decode(json_encode($response),true);
            $count = 0;
            $header_data_array = array();
            $data = array();
            $user_name_email = array("Huddle Name", "Huddle Type" ,"User Name", "Email");
            $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
            $account_name = implode('_',$account_name);
            foreach($huddle_report_summary['attributes_header'] as $row){
                $header_data_array[] = $row['name'];
            }
        
            $header_data_array =  array_merge($user_name_email,$header_data_array);
            foreach($huddle_report_summary['attributes_data'] as $row){
                if(isset($row['huddle_users_data']))
                    {
                        foreach($row['huddle_users_data'] as $huddle_users_data)
                        {
                            unset($huddle_users_data['image']);
                            unset($huddle_users_data['account_id']);
                            unset($huddle_users_data['user_id']);
                            array_unshift($huddle_users_data,$huddle_users_data['email']);
                            array_unshift($huddle_users_data,$huddle_users_data['user_name']);
                            array_unshift($huddle_users_data,$row['huddle_type']);
                            array_unshift($huddle_users_data,$row['huddle_name']);
                            unset($huddle_users_data['email']);
                            unset($huddle_users_data['huddle_id']);
                            unset($huddle_users_data['user_name']);
                            $data[$count] = $huddle_users_data;
                            $count++;
                        }
                    }  
            }
            $account_name = $account_name . "_huddle_report_";
            $this->export_pdf($header_data_array,$data,$account_name,$request->file_name);
        
        }else{

        $input = $request->all();
        $account_id = $request->account_id;
        $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
        $account_name = implode('_',$account_name);
        $account_name = $account_name . "_huddle_report_";
        $file_name = $account_name .date('YmdHis').'.pdf';
        $input['file_name'] = $file_name;
        $input['async'] = 1;
        $input['paginate'] = false;
        //$query_string = http_build_query($input);
        //exec('wget -O - "http://local.sibme.local/huddle_report_pdf?'.$query_string.'" > /dev/null 2>&1 &');
        exec("php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "' --report=huddle_report_pdf > /dev/null 2>&1 &" );
        $final_result = array();
        $final_result['filename'] = $file_name;
        //$final_result['console'] = "php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "'";
        return response()->json($final_result);
        }
    }
    
    public function sanitize_filename($string, $force_lowercase = true, $alnum = false) {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
                    "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
                    "â€�?", "â€“", ",", "<", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($alnum) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
        return ($force_lowercase) ? (function_exists('mb_strtolower')) ? mb_strtolower($clean, 'UTF-8') :strtolower($clean) :$clean;
    }
    function assessment_report(Request $request){
        $input = $request->all();
        $paginate = isset($input['paginate']) && $input['paginate'] == false ? false : true;
        $account_id = $input['account_id'];
        if(!is_array($input['account_ids']))
        {
            $input['account_ids'] = json_decode($input['account_ids']);
        }
        if(isset($input['standard_ids']) && !is_array($input['standard_ids']))
        {
            $input['standard_ids'] = json_decode($input['standard_ids']);
        }
        $account_ids = $input['account_ids'];
        if(empty($account_ids))
        {
            $account_ids = array($input['account_id']);
        }
        $user_id = $input['user_id'];
        $user_role = $input['user_role'];
        $start_date = $input['start_date'];
        $end_date = $input['end_date'];
        $stDate = date_create($start_date);
        $start_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        $endDate = date_create($end_date);
        $end_date = date_format($endDate, 'Y-m-d'). ' 23:59:59';
        $account_user_custom_fields = CustomField::get_custom_fields($account_id,'1');
        $account_assessment_custom_fields = CustomField::get_custom_fields($account_id,'2');
        $page = (int)$input['page'];
        $page = $page - 1;
        $limit = $input['limit'];
        $custom_marker_data = array();
        $framework_data = array();
        $offset = $page*$limit;
        $active = $input['active'];
        if(isset($input['report_id']))
        {
            $report_config_id = $input['report_id'];
        }
        else 
        {
            $default_report_config = ReportConfiguration::where(array('report_type' => 'assessment_report','is_default' => '1'))->first();
            $report_config_id = $default_report_config->id;
        }
        $report_data = ReportConfiguration::where(array("id" => $report_config_id))->first();
        $report_attributes = json_decode($report_data->report_attributes);
        $report_attributes_array = array();
        foreach($report_attributes as $row)
        {
            if($row->status)
            {
                $report_attributes_array[] = $row->name; 
            }
        }
        $final_array = array();
        $query_array = array();
        $query_array['account_ids'] = $account_ids;
        $query_builder = AccountFolder::
                        select('AccountFolder.*')
                        ->where(array('AccountFolder.active' => '1','AccountFolder.folder_type' => '1' , 'afmd.meta_data_name' => 'folder_type' , 'afmd.meta_data_value' => '3' ))
                        ->join('account_folders_meta_data as afmd','afmd.account_folder_id','=','AccountFolder.account_folder_id')
                        ->whereIn('AccountFolder.account_id' , $account_ids);

                        if($active != 'true')
                        {
                            $query_builder= $query_builder->whereRaw('AccountFolder.created_date >= "'.$start_date. '" AND AccountFolder.created_date <= "' .$end_date . '"' );
                        }
        
        if(!empty($request->get('search')))
        {
            $search = $this->escape_string($request->get('search'));
            $query_builder = $query_builder->join('account_folder_users as afu','AccountFolder.account_folder_id','=','afu.account_folder_id')->join('users as u','afu.user_id','=','u.id');
            $query_builder = $query_builder
                                  ->where(function($query) use ($search) {
                                      $query->orWhere('name','like','%'.$search.'%')
                                             ->orWhere('u.username','like','%'.$search.'%')
                                             ->orWhere('u.email','like','%'.$search.'%')
                                             ->orWhereIn('u.first_name',explode(' ',$search))
                                             ->orWhereIn('u.last_name',explode(' ',$search));
                                  })->where(array('afu.role_id' => '210'));
        }
        $query_builder_clone = clone $query_builder;
        if($active == 'true')
        {
            $assessment_huddles = $query_builder->groupBy('AccountFolder.account_folder_id')->orderBy('AccountFolder.created_date','DESC')->get()->toArray();
        }
        else
        {
            if($paginate){
                $assessment_huddles = $query_builder->groupBy('AccountFolder.account_folder_id')->orderBy('AccountFolder.created_date','DESC')->offset($offset)->limit($limit)->get()->toArray();
            }else{
                $assessment_huddles = $query_builder->groupBy('AccountFolder.account_folder_id')->orderBy('AccountFolder.created_date','DESC')->get()->toArray();    
            }
            
        }
        $assessment_huddles_count = $query_builder_clone->distinct('AccountFolder.account_folder_id')->count('AccountFolder.account_folder_id');
        foreach ($assessment_huddles as $key => $huddle)
        {

            $_data = $this->assessment_report_assessee($request,$huddle['account_folder_id'],false);
            $updated_attribute_data = $_data['updated_attribute_data'];
            $huddle_detail = $_data['huddle_detail'];
            $account_user_custom_fields = $_data['account_user_custom_fields'];
            $custom_markers_ids = $_data['custom_markers_ids'];
            $avg_pl_level = $_data['avg_pl_level'];
            $custom_marker_data = $_data['custom_marker_data'];
            $account_assessment_custom_fields = $_data['account_assessment_custom_fields'];
            $framework_data = $_data['framework_data'];


           // $attribute_data_count = $_date['attribute_data_count'];
            // $huddle_detail = AccountFolder::where(array('account_folder_id' => $huddle['account_folder_id']))->first()->toArray();
            // $huddle_participants = array();
            // $user_ids = array();
            // $huddle_participants = AccountFolderUser::getAssesseesList($this->site_id, $huddle['account_folder_id'], $user_id, $user_role,false,'en');
            // $huddle_detail['huddle_assessors'] = AccountFolder::getHuddleUsersIncludingGroups($this->site_id, $huddle['account_folder_id'], $user_id, $user_role,true, 200)['participants'];
            // $submission_deadline_time = AccountFolderMetaData::where(array(
            //             'account_folder_id' => $huddle['account_folder_id'],
            //             'site_id' => $this->site_id,
            //             'meta_data_name' => 'submission_deadline_time'
            //         ))->get()->toArray();


            // $submission_deadline_time = isset($submission_deadline_time[0]['meta_data_value']) ? $submission_deadline_time[0]['meta_data_value'] : date('H:i');

            // $huddle_detail['submission_deadline_time'] = $submission_deadline_time;

            // $submission_deadline_date = AccountFolderMetaData::where(array(
            //             'account_folder_id' => $huddle['account_folder_id'],
            //             'site_id' => $this->site_id,
            //             'meta_data_name' => 'submission_deadline_date'
            //         ))->get()->toArray();

            // $submission_deadline_date = isset($submission_deadline_date[0]['meta_data_value']) ? $submission_deadline_date[0]['meta_data_value'] : date('m-d-Y');

            // $huddle_detail['submission_deadline_date'] = $submission_deadline_date;

            // $submission_allowed = AccountFolderMetaData::where(array(
            //             'account_folder_id' => $huddle['account_folder_id'],
            //             'site_id' => $this->site_id,
            //             'meta_data_name' => 'submission_allowed'
            //         ))->get()->toArray();

            // $submission_allowed = isset($submission_allowed[0]['meta_data_value']) ? $submission_allowed[0]['meta_data_value'] : 1;

            // $huddle_detail['submission_allowed'] = $submission_allowed;

            // $huddle_detail['resource_submission_allowed'] = app('App\Http\Controllers\ApiController')->count_resource_submission_allowed($huddle['account_folder_id']);
            // $huddle_message = AccountFolderMetaData::where(array(
            //         'account_folder_id' => $huddle['account_folder_id'],
            //         'site_id' => $this->site_id,
            //         'meta_data_name' => 'message'
            //     ))->first()->toArray();

            // $huddle_detail['huddle_message'] = $huddle_message;
            // $user_name = array_column($huddle_participants, 'user_name');
            // $user_name = array_map('strtolower', $user_name);
            // array_multisort($user_name, SORT_ASC, SORT_STRING , $huddle_participants);
            // $updated_attribute_data = array();
            // foreach($huddle_participants as $participant)
            // {
            //     $user_ids[] = $participant['user_id'];
            // }


            //call functoin here
            //$request->request->add(['huddle_id', $huddle['account_folder_id']]);
            //$request->huddle_id =
            //$updated_attribute_data = $this->assessment_huddle_users_pagination($request,$huddle['account_folder_id']);

            //$attribute_data = $this->attributes_query($account_id,array($huddle['account_id']),$user_ids,array($huddle['account_folder_id']),$report_config_id,$start_date,$end_date,false,'','',$active);
            //$attribute_data = json_decode(json_encode($attribute_data), True);
			// foreach($attribute_data as $value => $data)
            // {
            //     $participant_data = array();
            //     $huddle_role = HelperFunctions::get_huddle_roles($huddle['account_folder_id'],$data['user_id']);
            //     foreach($huddle_participants as $participant)
            //     {
            //         if($participant['user_id'] == $data['user_id'])
            //         {
            //             $participant_data = $participant;
            //         }
            //     }
            //     $documents_ids = AccountFolderDocument::select('account_folder_documents.document_id')->join('documents as d','account_folder_documents.document_id','=','d.id')->where(array('d.created_by' => $data['user_id'] ))->whereIn('account_folder_id',array($huddle['account_folder_id']))->get()->toArray();
            //     if(isset($input['standard_ids']))
            //     {
            //         $frameworks = $this->get_framework_tags_data($documents_ids,$input['standard_ids'],true);
            //         $framework_data = $frameworks['standard_data'];
            //     }
            //     else
            //     {
            //         $framework_data = array();
            //     }
            //     $custom_markers_ids = AccountTag::select('account_tag_id')->where(array('tag_type' => '1' , 'account_id' => $account_id))->get()->toArray();
            //     $avg_pl_level = $this->get_avg_performance_level($documents_ids,$input['framework_id']);
            //     $custom_marker_data = $this->get_custom_marker_data($documents_ids,$custom_markers_ids,true,$start_date,$end_date);
            //      foreach($report_attributes as $row)
            //         {
            //             if(!empty($row->value) && $row->status)
            //             {
            //                 $updated_attribute_data[$value][] = $data[$row->value];
            //             }

            //             if($row->name == 'Last Modified' && $row->status)
            //             {
            //                 $updated_attribute_data[$value][] = isset($participant_data['last_modified'])? $participant_data['last_modified'] : '';
            //             }
            //             if($row->name == 'Last Submission' && $row->status)
            //             {
            //                 $updated_attribute_data[$value][] = isset($participant_data['last_submission'])? $participant_data['last_submission'] : '';
            //             }
            //             if($row->name == 'Videos Uploaded to Huddles' && $row->status)
            //             {
            //                 $updated_attribute_data[$value][] = isset($participant_data['videos_count'])? $participant_data['videos_count'].','.'videos' : '';
            //             }
            //             if($row->name == 'Huddle Resources Uploaded' && $row->status)
            //             {
            //                 $updated_attribute_data[$value][] = isset($participant_data['resources_count'])? $participant_data['resources_count'].','.'resources' : '';
            //             }
            //             if($row->name == 'Assessed' && $row->status)
            //             {
            //                 if(isset($participant_data['assessed']))
            //                 {
            //                     if($participant_data['assessed'])
            //                     {
            //                         $updated_attribute_data[$value][] = 'true,assessed';
            //                     }
            //                     else
            //                     {
            //                         $updated_attribute_data[$value][] = 'false,assessed';
            //                     }
            //                 }
            //             }
            //             if($row->name == 'Is Active' && $row->status)
            //             {
            //                 $user_active_detail = UserAccount::where(array('user_id' => $data['user_id'] , 'account_id' => $data['account_id']))->first();
            //                 if($user_active_detail->is_active == '1' && $user_active_detail->status_type == 'Active' )
            //                 {
            //                     $updated_attribute_data[$value][] = 'Yes';
            //                 }
            //                 else
            //                 {
            //                     $updated_attribute_data[$value][] = 'No';
            //                 }
            //             }
            //             if($row->name == 'Huddle Role' && $row->status)
            //             {
            //                $updated_attribute_data[$value][] = $this->huddle_role_name($huddle_role,'3');
            //             }
            //             if($row->name == 'Avg Performance Levels' && $row->status)
            //             {
            //                 $updated_attribute_data[$value][] = isset($participant_data['avg_performance_level_rating'])? round($participant_data['avg_performance_level_rating']) : '';
            //             }
            //             if($row->name == 'Huddle Video Comments' && $row->status)
            //             {
            //                 $updated_attribute_data[$value][] = isset($participant_data['comments_count'])? $participant_data['comments_count'].','.'comments' : '';
            //             }
            //             if($row->name == 'Custom Fields (User-level data)' && $row->status)
            //             {

            //                 if(isset($account_user_custom_fields['user_summary']['custom_fields']))
            //                 {
            //                     $custom_fields_array = $account_user_custom_fields['user_summary']['custom_fields'];
            //                     foreach($custom_fields_array as $cf)
            //                     {
            //                         $user_custom_values = UserCustomField::get_users_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,'');
            //                         if(!empty($user_custom_values))
            //                         {
            //                             if($user_custom_values[0]->custom_field_value == 'true')
            //                             {
            //                                $user_custom_values[0]->custom_field_value = 'Yes';
            //                             }
            //                             if($user_custom_values[0]->custom_field_value == 'false')
            //                             {
            //                                $user_custom_values[0]->custom_field_value = 'No';
            //                             }
            //                             $updated_attribute_data[$value][] = $user_custom_values[0]->custom_field_value;
            //                         }
            //                         else
            //                         {
            //                             $updated_attribute_data[$value][] = '';
            //                         }
            //                     }
            //                 }

                        // }
                        // if($row->name == 'Custom Fields (Assessment-level data)' && $row->status)
                        // {
                        //    if(isset($account_assessment_custom_fields['assessment']['custom_fields']))
                        //    {
                        //        $custom_fields_array = $account_assessment_custom_fields['assessment']['custom_fields'];
                        //         foreach($custom_fields_array as $cf)
                        //         {
                        //             $user_custom_values = AssessmentCustomField::get_assessment_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,'');
                        //             if(!empty($user_custom_values))
                        //             {
                        //                 if($user_custom_values[0]->assessment_custom_field_value == 'true')
                        //                 {
                        //                    $user_custom_values[0]->assessment_custom_field_value = 'Yes';
                        //                 }
                        //                 if($user_custom_values[0]->assessment_custom_field_value == 'false')
                        //                 {
                        //                    $user_custom_values[0]->assessment_custom_field_value = 'No';
                        //                 }
                        //                 $updated_attribute_data[$value][] = $user_custom_values[0]->assessment_custom_field_value;
                        //             }
                        //             else
                        //             {
                        //                 $updated_attribute_data[$value][] = '';
                        //             }
                        //         }
                        //    }

                        //    if(isset($account_assessment_custom_fields['assessment']['parent_custom_fields']))
                        //    {
                        //        $custom_fields_array = $account_assessment_custom_fields['assessment']['parent_custom_fields'];
                        //         foreach($custom_fields_array as $cf)
                        //         {
                        //             $user_custom_values = AssessmentCustomField::get_assessment_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,'');
                        //             if(!empty($user_custom_values))
                        //             {
                        //                 if($user_custom_values[0]->assessment_custom_field_value == 'true')
                        //                 {
                        //                    $user_custom_values[0]->assessment_custom_field_value = 'Yes';
                        //                 }
                        //                 if($user_custom_values[0]->assessment_custom_field_value == 'false')
                        //                 {
                        //                    $user_custom_values[0]->assessment_custom_field_value = 'No';
                        //                 }
                        //                 $updated_attribute_data[$value][] = $user_custom_values[0]->assessment_custom_field_value;
                        //             }
                        //             else
                        //             {
                        //                 $updated_attribute_data[$value][] = '';
                        //             }
                        //         }
                        //    }

                        // }

                        // if($row->name == 'Framework Tags' && $row->status && !in_array('Standard Performance Levels',$report_attributes_array))
                        // {
                        //     foreach ($framework_data as $standard_data)
                        //     {
                        //         $updated_attribute_data[$value][] = $standard_data['count'];
                        //     }
                        // }
                        
                        // if($row->name == 'Standard Performance Levels' && $row->status && !in_array('Framework Tags',$report_attributes_array))
                        // {
                        //    foreach ($framework_data as $standard_data)
                        //     {
                        //        $updated_attribute_data[$value][] = $standard_data['pl_average'];
                        //     }
                        // }
                        // if($row->name == 'Framework Tags' && $row->status && in_array('Standard Performance Levels',$report_attributes_array ))
                        // {

                        //     foreach ($framework_data as $standard_data)
                        //     {
                        //        $updated_attribute_data[$value][] = $standard_data['count'];
                        //        $updated_attribute_data[$value][] = $standard_data['pl_average'];
                        //     }
                        // }
                        
            //             if($row->name == 'Standard Performance Levels' && $row->status && !in_array('Framework Tags',$report_attributes_array))
            //             {
            //                foreach ($framework_data as $standard_data)
            //                 {
            //                    $updated_attribute_data[$value][] = $standard_data['pl_average'];
            //                 }
            //             }
            //             if($row->name == 'Framework Tags' && $row->status && in_array('Standard Performance Levels',$report_attributes_array ))
            //             {

            //                 foreach ($framework_data as $standard_data)
            //                 {
            //                    $updated_attribute_data[$value][] = $standard_data['count'];
            //                    $updated_attribute_data[$value][] = $standard_data['pl_average'];
            //                 }
            //             }

                //     }

                // $updated_attribute_data[$value]['image'] = $data['image'];
                // $updated_attribute_data[$value]['account_id'] =  $data['account_id'];
                // $updated_attribute_data[$value]['user_id'] = $data['user_id'];
                // $updated_attribute_data[$value]['email'] = $data['email'];
                // $updated_attribute_data[$value]['user_name'] = $data['user_name'];
                // $updated_attribute_data[$value]['huddle_id'] = $huddle['account_folder_id'];
                // $updated_attribute_data[$value]['is_submitted'] = isset($participant_data['is_submitted'])? $participant_data['is_submitted'] : '';

            // }
            //$final_array[$key]['assessee'] = $updated_attribute_data;
            $final_array[$key] = $huddle_detail;
            $final_array[$key]['assessee'] = $updated_attribute_data;
           // $final_array[$key]['assessee_count'] = $attribute_data_count;
            $final_array[$key]['assessee_count'] = $_data['huddle_participants_count'];
        }
        
        $header_data = array();
        $count = 0;
        $translations =  TranslationsManager::get_page_lang_based_content("Api/analytics", $request->header("site_id"), '', $request->header("current-lang"));

        foreach($report_attributes as $key => $row)
        {
            if(!empty($row->value) && $row->status)
            {
                $key = str_replace(' ','-',strtolower($row->name));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : $row->name;
                $header_data[$count]['key'] = str_replace(' ','_',strtolower($row->name));
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                if($row->name == 'Account Name' || $row->name == 'User Role' || $row->name == 'User Type'  )
                {
                    $header_data[$count]['detail'] = false;
                }
                $count++; 
            }
            
            if($row->name == 'Is Active' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Is Active'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Is Active';
                //$header_data[$count]['name'] = 'Is Active';
                $header_data[$count]['key'] = str_replace(' ','_',strtolower('Is Active'));
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['detail'] = false;
                $count++;            
            }
            
            if($row->name == 'Huddle Role' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Huddle Role'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Huddle Role';
                //$header_data[$count]['name'] = 'Is Active';
                $header_data[$count]['key'] = str_replace(' ','_',strtolower('Huddle Role'));
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['detail'] = false;
                $count++;            
            }
            
            if($row->name == 'Last Modified' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Last Modified'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Last Modified';
                //$header_data[$count]['name'] = 'Last Modified';
                $header_data[$count]['key'] = 'last_modified';
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['detail'] = false;
                $count++; 
            }
            if($row->name == 'Last Submission' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Last Submission'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Last Submission';
                //$header_data[$count]['name'] = 'Last Submission';
                $header_data[$count]['key'] = 'last_submission';
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['detail'] = false;
                $count++;
            }
            if($row->name == 'Videos Uploaded to Huddles' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Videos Uploaded to Huddles'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Videos Uploaded to Huddles';
                //$header_data[$count]['name'] = 'Videos Uploaded';
                $header_data[$count]['key'] = 'videos_uploaded';
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $count++;
            }
            if($row->name == 'Huddle Resources Uploaded' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Huddle Resources Uploaded'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Huddle Resources Uploaded';
                //$header_data[$count]['name'] = 'Resources Uploaded';
                $header_data[$count]['key'] = 'resources_uploaded';
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $count++;
            }
            if($row->name == 'Assessed' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Assessed'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Assessed';
                //$header_data[$count]['name'] = 'Assessed';
                $header_data[$count]['key'] = 'assessed';
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['detail'] = false;
                $count++;
            }
            if($row->name == 'Avg Performance Levels' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Avg Performance Levels'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Avg Performance Levels';
                //$header_data[$count]['name'] = 'Avg Performance Levels';
                $header_data[$count]['key'] = 'avg_performance_levels';
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['avg_performance_levels'] = true;
                $header_data[$count]['detail'] = false;
                $count++;
            }
            if($row->name == 'Huddle Video Comments' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Huddle Video Comments'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Huddle Video Comments';
                //$header_data[$count]['name'] = 'Comments';
                $header_data[$count]['key'] = 'comments';
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $count++;
            }
            if($row->name == 'Assessment Summary' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Assessment Summary'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Assessment Summary';
                //$header_data[$count]['name'] = 'Comments';
                $header_data[$count]['key'] = 'comments';
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['assessment_summary'] = true;
                $count++;
            }



            if($row->name == 'Custom Fields (User-level data)' && $row->status)
            {
                $custom_fields_array = array();
                if(isset($account_user_custom_fields['user_summary']['custom_fields']))
                {
                    $custom_fields_array = $account_user_custom_fields['user_summary']['custom_fields'];
                    foreach($custom_fields_array as $cf)
                    {
                        $header_data[$count]['name'] = $cf->field_label;
                        $header_data[$count]['key'] = '';
                        $header_data[$count]['field_type'] = $cf->field_type;
                        $header_data[$count]['standard'] = false;
                        $header_data[$count]['performance'] = false;
                        $header_data[$count]['markers'] = false;
                        $header_data[$count]['standard_id'] = false;
                        $header_data[$count]['custom_fields'] = true;
                        $header_data[$count]['detail'] = false;
                        $count++;
                    }
                }
                if(isset($account_user_custom_fields['user_summary']['parent_custom_fields']))
                {
                    $custom_fields_array = $account_user_custom_fields['user_summary']['parent_custom_fields'];
                    foreach($custom_fields_array as $cf)
                    {
                        $header_data[$count]['name'] = $cf->field_label;
                        $header_data[$count]['key'] = '';
                        $header_data[$count]['field_type'] = $cf->field_type;
                        $header_data[$count]['standard'] = false;
                        $header_data[$count]['performance'] = false;
                        $header_data[$count]['markers'] = false;
                        $header_data[$count]['standard_id'] = false;
                        $header_data[$count]['custom_fields'] = true;
                        $header_data[$count]['detail'] = false;
                        $count++;
                    }
                }
            }
            if($row->name == 'Custom Fields (Assessment-level data)' && $row->status)
            {
                $custom_fields_array = array();
                if(isset($account_assessment_custom_fields['assessment']['custom_fields']))
                {
                    $custom_fields_array = $account_assessment_custom_fields['assessment']['custom_fields'];
                    foreach($custom_fields_array as $cf)
                    {
                        $header_data[$count]['name'] = $cf->field_label;
                        $header_data[$count]['key'] = '';
                        $header_data[$count]['field_type'] = $cf->field_type;
                        $header_data[$count]['standard'] = false;
                        $header_data[$count]['performance'] = false;
                        $header_data[$count]['markers'] = false;
                        $header_data[$count]['standard_id'] = false;
                        $header_data[$count]['custom_fields'] = true;
                        $header_data[$count]['detail'] = false;
                        $count++;
                    }
                }
                if(isset($account_assessment_custom_fields['assessment']['parent_custom_fields']))
                {
                    $custom_fields_array = $account_assessment_custom_fields['assessment']['parent_custom_fields'];
                    foreach($custom_fields_array as $cf)
                    {
                        $header_data[$count]['name'] = $cf->field_label;
                        $header_data[$count]['key'] = '';
                        $header_data[$count]['field_type'] = $cf->field_type;
                        $header_data[$count]['standard'] = false;
                        $header_data[$count]['performance'] = false;
                        $header_data[$count]['markers'] = false;
                        $header_data[$count]['standard_id'] = false;
                        $header_data[$count]['custom_fields'] = true;
                        $header_data[$count]['detail'] = false;
                        $count++;
                    }
                }
            }
            if($row->name == 'Framework Tags' && $row->status && !in_array('Standard Performance Levels',$report_attributes_array))
            {
                foreach ($framework_data as $standard_data)
                {
                    $header_data[$count]['name'] = $standard_data['label'];
                    $header_data[$count]['key'] = '';
                    $header_data[$count]['standard'] = true;
                    $header_data[$count]['performance'] = false;
                    $header_data[$count]['markers'] = false;
                    $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                    $count++;
                }
            }
            if($row->name == 'Standard Performance Levels' && $row->status && !in_array('Framework Tags',$report_attributes_array))
            {
                foreach ($framework_data as $standard_data)
                {
                    $header_data[$count]['name'] = $standard_data['label'];
                    $header_data[$count]['key'] = '';
                    $header_data[$count]['standard'] = false;
                    $header_data[$count]['performance'] = true;
                    $header_data[$count]['markers'] = false;
                    $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                    $count++;
                }
            }
            if($row->name == 'Framework Tags' && $row->status && in_array('Standard Performance Levels',$report_attributes_array))
            {
                foreach ($framework_data as $standard_data)
                {
                    $header_data[$count]['name'] = $standard_data['label'];
                    $header_data[$count]['key'] = '';
                    $header_data[$count]['standard'] = true;
                    $header_data[$count]['performance'] = false;
                    $header_data[$count]['markers'] = false;
                    $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                    $count++;
                    $header_data[$count]['name'] = $standard_data['label'];
                    $header_data[$count]['key'] = '';
                    $header_data[$count]['standard'] = false;
                    $header_data[$count]['performance'] = true;
                    $header_data[$count]['markers'] = false;
                    $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                    $count++;
                }
            }
            if($row->name == 'Custom Video Marker Tags' && $row->status)
            {
                foreach ($custom_marker_data as $marker_data)
                {
                    $header_data[$count]['name'] = $marker_data['label'];
                    $header_data[$count]['key'] = 'markers';
                    $header_data[$count]['standard'] = false;
                    $header_data[$count]['performance'] = false;
                    $header_data[$count]['markers'] = true;
                    $header_data[$count]['standard_id'] = $marker_data['account_tag_id'];
                    $count++;
                }
            }
        }
        $result = array();
        $result['attributes_header'] = $header_data;
        $result['assessment_huddles'] = $final_array;
        $result['assessment_huddles_count'] = $assessment_huddles_count;
        $result['page'] = $page+1;
        $result['limit'] = $limit;

        return response()->json($result, 200, [], JSON_PRETTY_PRINT);
    }
    
    function assessment_report_csv(Request $request){
        if($request->has('async') && $request->async == '1')
        {
        $response = $this->assessment_report($request);
        $response = $response->getData();
        $account_id = $request->account_id;
        $assessment_report_summary = json_decode(json_encode($response),true);
        $result = array();
        $count = 0;
        $header_data_array = array();
        $data_array = array();
        $assessee_name = array();
        $user_name_email = array("User Name", "Email");
        $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
        $account_name = implode('_',$account_name);
        foreach($assessment_report_summary['attributes_header'] as $row){
            $header_data_array[] = $row['name'];
        }
    
        $header_data_array =  array_merge($user_name_email,$header_data_array);
        foreach ($assessment_report_summary['assessment_huddles'] as $row)
        {
            $data_array[] =  $row;
                 
        }
        foreach($data_array as $row){
            $assessee_name[] = $row['name'];
            $result[$count] = $assessee_name;
            $assessee_name = [];
            $count++;
            if(isset($row['assessee']))
                {
                    foreach($row['assessee'] as  $assessee)
                    {
                        // if (in_array("videos", $assessee)){
                            
                        // }
                        $assessee = str_replace(',', ' ', $assessee);
                        $assessee = str_replace('videos', ' ', $assessee);
                        $assessee = str_replace('resources', ' ', $assessee);
                        $assessee = str_replace('comments', ' ', $assessee);
                        $assessee = str_replace('assessed', ' ', $assessee);
                        unset($assessee['image']);
                        unset($assessee['account_id']);
                        unset($assessee['user_id']);
                        array_unshift($assessee,$assessee['email']);
                        array_unshift($assessee,$assessee['user_name']);
                        unset($assessee['email']);
                        unset($assessee['user_name']);
                        unset($assessee['huddle_ids']);
                        unset($assessee['huddle_id']);
                        unset($assessee['document_ids']);
                        $result[$count] = $assessee;
                        $count++;
                    }
                }  
        }
        $exportAssessmentReportsToCSV = new \App\Services\Exports\AssessmentReport\ExportToCSV;
        return $exportAssessmentReportsToCSV->export($header_data_array,$result,$account_name,$request->file_name,$request->user_id);
        }
        else 
        {
          $input = $request->all();
          $account_id = $request->account_id;
          $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
          $account_name = implode('_',$account_name);
          $account_name = $account_name . "_assessment_report_";
          $file_name ='Assessment Report - '.date('Y-m-d').'.csv';
          $input['file_name'] = 'Assessment Report - '.date('Y-m-d').'.csv';
          $input['async'] = 1;
          $input['paginate'] = false;
          //$query_string = http_build_query($input);
          //exec('wget -O - "http://local.sibme.local/assessment_report_csv?'.$query_string.'" > /dev/null 2>&1 &');
          exec("php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "' --report=assessment_report_csv > /dev/null 2>&1 &" );
          $final_result = array();
          $final_result['filename'] = $file_name;
          return response()->json($final_result);
            
        }
    }
    
    function assessment_report_excel(Request $request){
       if($request->has('async') && $request->async == '1')
        {
        $response = $this->assessment_report($request);
        $response = $response->getData();
        $account_id = $request->account_id;
        $assessment_report_summary = json_decode(json_encode($response),true);
        $result = array();
        $count = 0;
        $header_data_array = array();
        $data_array = array();
        $assessee_name = array();
        $user_name_email = array("User Name", "Email");
        $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
        $account_name = implode('_',$account_name);
        foreach($assessment_report_summary['attributes_header'] as $row){
            $header_data_array[] = $row['name'];
        }
    
        $header_data_array =  array_merge($user_name_email,$header_data_array);
        foreach ($assessment_report_summary['assessment_huddles'] as $row)
        {
            $data_array[] =  $row;
                 
        }
        foreach($data_array as $row){
            $assessee_name[] = $row['name'];
            $result[$count] = $assessee_name;
            $assessee_name = [];
            $count++;
            if(isset($row['assessee']))
                {
                    foreach($row['assessee'] as $key => $assessee)
                    {
                        $assessee = str_replace(',', ' ', $assessee);
                        $assessee = str_replace('videos', ' ', $assessee);
                        $assessee = str_replace('resources', ' ', $assessee);
                        $assessee = str_replace('comments', ' ',$assessee);
                        $assessee = str_replace('assessed', ' ', $assessee);
                        unset($assessee['image']);
                        unset($assessee['account_id']);
                        unset($assessee['user_id']);
                        array_unshift($assessee,$assessee['email']);
                        array_unshift($assessee,$assessee['user_name']);
                        unset($assessee['email']);
                        unset($assessee['user_name']);
                        unset($assessee['huddle_ids']);
                        unset($assessee['huddle_id']);
                        unset($assessee['document_ids']);
                        $result[$count] = $assessee;
                        $count++;
                    }
                }  
        }
        $exportAssessmentReportsToExcel = new \App\Services\Exports\AssessmentReport\ExportToExcel;
        return $exportAssessmentReportsToExcel->export($header_data_array,$result,$account_name,$request->file_name,$request->user_id);
        }
        else 
        {
          $input = $request->all();
          $account_id = $request->account_id;
          $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
          $account_name = implode('_',$account_name);
          $account_name = $account_name . "_assessment_report_";
          $file_name = 'Assessment Report - '.date('Y-m-d').'.xlsx';
          $input['file_name'] ='Assessment Report - '.date('Y-m-d').'.xlsx';
          $input['async'] = 1;
          $input['paginate'] = false;
          //$query_string = http_build_query($input);
          //exec('wget -O - "http://local.sibme.local/assessment_report_excel?'.$query_string.'" > /dev/null 2>&1 &');
          exec("php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "' --report=assessment_report_excel > /dev/null 2>&1 &" );
          $final_result = array();
          $final_result['filename'] = $file_name;
          return response()->json($final_result);
            
        }
    }
    
    function assessment_report_pdf(Request $request){

        if($request->has('async') && $request->async == '1')
        {
        ini_set('memory_limit','5000M');
        ini_set('max_execution_time', 1000); //3 minutes
        set_time_limit(0);
        $response = $this->assessment_report($request);
        $response = $response->getData();
        $account_id = $request->account_id;
        $assessment_report_summary = json_decode(json_encode($response),true);
        $result = array();
        $count = 0;
        $header_data_array = array();
        $data_array = array();
        $user_name_email = array("Assessee Name","User Name", "Email");
        $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
        $account_name = implode('_',$account_name);
        foreach($assessment_report_summary['attributes_header'] as $row){
            $header_data_array[] = $row['name'];
        }
    
        $header_data_array =  array_merge($user_name_email,$header_data_array);
        foreach ($assessment_report_summary['assessment_huddles'] as $row)
        {
            $data_array[] =  $row;
                 
        }
        foreach($data_array as $row){
            // $result[$count] = $row['name'];
            // $count++;
            if(isset($row['assessee']))
                {
                    foreach($row['assessee'] as $key => $assessee)
                    {
                        $assessee = str_replace(',', ' ', $assessee);
                        $assessee = str_replace('videos', ' ', $assessee);
                        $assessee = str_replace('resources', ' ', $assessee);
                        $assessee = str_replace('comments', ' ', $assessee);
                        $assessee = str_replace('assessed', ' ', $assessee);
                        unset($assessee['image']);
                        unset($assessee['account_id']);
                        unset($assessee['user_id']);
                        array_unshift($assessee,$assessee['email']);
                        array_unshift($assessee,$assessee['user_name']);
                        array_unshift($assessee,$row['name']);
                        unset($assessee['email']);
                        unset($assessee['user_name']);
                        $result[$count] = $assessee;
                        $count++;
                    }
                }  
        }
        $account_name = $account_name . "_assessment_report_";
        $this->export_pdf($header_data_array,$result,$account_name,$request->file_name);
        // $exportAssessmentReportsToPDF = new \App\Services\Exports\AssessmentReport\ExportToPDF;
        // return $exportAssessmentReportsToPDF->export($header_data_array,$result,$account_name);
        }
        else 
        {
          $input = $request->all();
          $account_id = $request->account_id;
          $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
          $account_name = implode('_',$account_name);
          $account_name = $account_name . "_assessment_report_";
          $file_name = 'Assessment Report - '.date('Y-m-d').'.pdf';
          $input['file_name'] = 'Assessment Report - '.date('Y-m-d').'.pdf';
          $input['async'] = 1;
          $input['paginate'] = false;
          //$query_string = http_build_query($input);
          //exec('wget -O - "http://local.sibme.local/assessment_report_pdf?'.$query_string.'" > /dev/null 2>&1 &');
          exec("php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "' --report=assessment_report_pdf > /dev/null 2>&1 &" );
          $final_result = array();
          $final_result['filename'] = $file_name;
          return response()->json($final_result);
            
        }
        
    }

    function session_summary_count($document_ids,$user_id)
    {
        $summary_count = Comment::whereIn('ref_id',$document_ids)->where(array('ref_type' => '4','user_id' => $user_id))->count();
        return $summary_count;
    }
    
    function user_summary_report(Request $request) {
        ini_set('memory_limit','5000M');
        ini_set('max_execution_time', 1000); //3 minutes
        set_time_limit(0);
        $input = $request->all();

        
        if (isset($input['huddle_ids']) && $input['huddle_ids'] ) {
            $huddle_ids_for_participating = $input['huddle_ids'];
        }else{
            $huddle_ids_for_participating = [1,2,3,4,5];
        }

        $paginate = isset($input['paginate']) && $input['paginate'] == false ? false : true;
        if(!is_array($input['account_ids']))
        {
            $input['account_ids'] = json_decode($input['account_ids']);
        }
        if(isset($input['standard_ids']) && !is_array($input['standard_ids']))
        {
            $input['standard_ids'] = json_decode($input['standard_ids']);
        }
        if(isset($input['huddle_ids']) && !is_array($input['huddle_ids']))
        {
            $input['huddle_ids'] = json_decode($input['huddle_ids']);
        }
        if(count($input['huddle_ids']) == 7 || in_array('7',$input['huddle_ids']))
        {
            $input['huddle_ids'] = [7];
        }
        if($request->has('sort_by'))
        {
            $order_by = $request->get('sort_by');
            $sorting_order = $request->get('sort_by_order');
        }
        else
        {
            $order_by = false;
            $sorting_order = 'ASC';
        }
        //return $input; 
        $account_id = $input['account_id'];
        $account_ids = $input['account_ids'];
        $account_user_custom_fields = CustomField::get_custom_fields($account_id,'1');
        if(empty($account_ids))
        {
            $account_ids = array($input['account_id']);
        }
        $user_id = $input['user_id'];
        $start_date = $input['start_date'];
        $end_date = $input['end_date'];
        $page = $input['page'];
        $rows = $input['limit'];
        $active = $input['active'];
        $end_limit = $page * $rows;
        $start_limit = $end_limit - $rows;
        $offset = ($page-1) * $rows;
        //$limit = " LIMIT $start_limit , $end_limit";
        $limit = "LIMIT $rows OFFSET $offset";
        //$limit = "LIMIT 500 ";
       // dd($limit);
        $search = '';
        $custom_marker_data = array();
        $framework_data = array();
        if(!empty($request->get('search')))
        {
            $search = $this->escape_string($request->get('search'));
            $search = "WHERE (t0.full_name like '%".$search."%' or t0.email like '%".$search."%')";
        }
        if(isset($input['type']) && $input['type'] == 'playercard-user-summary')
        {
            $report_type =  'playercard-user-summary';
            $account_ids = array($input['account_id']);
        }
        else
        {
            $report_type =  'user_summary_report';
        }
        if(isset($input['report_id']))
        {
            $report_config_id = $input['report_id'];
        }
        else 
        {
            $default_report_config = ReportConfiguration::where(array('report_type' => $report_type,'is_default' => '1'))->first();
            $report_config_id = $default_report_config->id;
        }
        $report_data = ReportConfiguration::where(array("id" => $report_config_id))->first();
        $report_attributes = json_decode($report_data->report_attributes);
        $report_attributes_array = array();
        $updated_attribute_data = array();
        foreach($report_attributes as $row)
        {
            if($row->status)
            {
                $report_attributes_array[] = $row->name; 
            }
        }

        $obj_user_accounts = UserAccount::where("account_id", $account_id)->where("user_id", $user_id)->first();

        if(!$obj_user_accounts){
            return response()->json(['success'=>false, 'message'=>'No Record Found for Account ID = '.$account_id.' and User ID = '.$user_id]);
        }

        $user_role_id = $obj_user_accounts->role_id;
        $view_user_analytics = $obj_user_accounts->permission_view_analytics;
        if($user_role_id>115 && $input['type'] != 'playercard-user-summary'){
            return response()->json(['success'=>false, 'message'=>"This account doesn't have permission to access analytics."]);
        }
        $stDate = date_create($start_date);
        $start_date = date_format($stDate, 'Y-m-d');

        $endDate = date_create($end_date);
        $end_date = date_format($endDate, 'Y-m-d');
        if($user_role_id == 115 && $view_user_analytics == 0)
        {
            $user_ids = app('App\Http\Controllers\ApiController')->get_users_of_admin_circle($account_id, $user_id);
            $user_ids_imploded = implode(',', $user_ids);
            $all_huddle_ids = app('App\Http\Controllers\ApiController')->get_user_huddle_ids($account_id, $user_id, 0, $user_ids_imploded);
            
            $attribute_data = $this->attributes_query($account_id,$account_ids,$user_ids,$all_huddle_ids,$report_config_id,$start_date,$end_date,true,$limit,$search,$active,125,$order_by,$sorting_order,$paginate);
            $attribute_data = json_decode(json_encode($attribute_data), True);
            $attribute_data_wo_limit = $this->attributes_query($account_id,$account_ids,$user_ids,$all_huddle_ids,$report_config_id,$start_date,$end_date,true,'',$search,$active,125,$order_by,$sorting_order,$paginate);
            $attribute_data_count = count(json_decode(json_encode($attribute_data_wo_limit), True));
        }
        else {
           // DB::enableQueryLog();
            $attribute_data = $this->attributes_query($account_id,$account_ids,[],[],$report_config_id,$start_date,$end_date,true,$limit,$search,$active,125,$order_by,$sorting_order,$paginate);
           // dd(DB::getQueryLog());
            $attribute_data = json_decode(json_encode($attribute_data), True);
            $attribute_data_wo_limit = $this->attributes_query($account_id,$account_ids,[],[],$report_config_id,$start_date,$end_date,true,'',$search,$active,125,$order_by,$sorting_order,$paginate);
           // dd($attribute_data);
            $attribute_data_count = count(json_decode(json_encode($attribute_data_wo_limit), True));

        }
        
        if(isset($input['type']) && $input['type'] == 'playercard-user-summary')
        {
            
            $user_ids_imploded = implode(',', array($user_id));
            $all_huddle_ids = app('App\Http\Controllers\ApiController')->get_user_huddle_ids($account_id, $user_id, 0, $user_ids_imploded);
            
            $attribute_data = $this->attributes_query($account_id,$account_ids,array($user_id),[],$report_config_id,$start_date,$end_date,true,$limit,$search,$active,125);
            $attribute_data = json_decode(json_encode($attribute_data), True);
            $attribute_data_wo_limit = $this->attributes_query($account_id,$account_ids,array($user_id),[],$report_config_id,$start_date,$end_date,true,'',$search,$active,125);
            $attribute_data_count = count(json_decode(json_encode($attribute_data_wo_limit), True));
        }
        
        $folder_type = [];
        $account_folder_type = [];
        $huddle_ids_arr_type_1 = [];
        $huddle_ids_arr_type_2 = [];
        $huddle_ids_arr_type_3 = [];
        $huddle_ids_arr_type_4 = [];
        $huddle_ids_arr_type_5 = [];
        $huddle_ids_arr_type_6 = [];
        $merged_array = [];
        foreach($attribute_data as $value => $data)
        {
            $merged_array = [];
            if(in_array('7',$input['huddle_ids']))
            {
                $condition_array = array('afmd.meta_data_name' => 'folder_type'
                    );
                $folder_type = [1,2,3,5];
                $account_folder_type = [1,2,3];
            }
            if(in_array('6',$input['huddle_ids']))
            {
                 $condition_array = array('AccountFolder.created_by' => $data['user_id']
                    );
                $folder_type = [5];
                $account_folder_type = [3];
                $huddle_ids_arr_type_6 = $this->get_huddle_ids_query($data['account_id'],$account_folder_type,$condition_array,$folder_type,true);
                $merged_array = array_merge($huddle_ids_arr_type_6, $merged_array);
            }
            if(in_array('5',$input['huddle_ids']))
            {
                $condition_array = array('afmd.meta_data_name' => 'folder_type','huddle_users.user_id' => $data['user_id']
                    );
                $folder_type = [1];
                $account_folder_type = [1];
                $huddle_ids_arr_type_1 = $this->get_huddle_ids_query($data['account_id'],$account_folder_type,$condition_array,$folder_type,false);
                $merged_array = array_merge($huddle_ids_arr_type_1, $merged_array);
            }
            if(in_array('1',$input['huddle_ids']))
            {
                $condition_array = array('afmd.meta_data_name' => 'folder_type','huddle_users.user_id' => $data['user_id'],'huddle_users.role_id' => 200
                    );
                $folder_type = [2];
                $account_folder_type = [1];
                $huddle_ids_arr_type_2 = $this->get_huddle_ids_query($data['account_id'],$account_folder_type,$condition_array,$folder_type,false);
                $merged_array = array_merge($huddle_ids_arr_type_2, $merged_array);
            }
            if(in_array('2',$input['huddle_ids']))
            {
                $condition_array = array('afmd.meta_data_name' => 'folder_type','huddle_users.user_id' => $data['user_id'],'huddle_users.role_id' => 210
                    );
                $folder_type = [2];
                $account_folder_type = [1];
                $huddle_ids_arr_type_3 = $this->get_huddle_ids_query($data['account_id'],$account_folder_type,$condition_array,$folder_type,false);
                $merged_array = array_merge($huddle_ids_arr_type_3, $merged_array);
            }
            if(in_array('3',$input['huddle_ids']))
            {
                $condition_array = array('afmd.meta_data_name' => 'folder_type','huddle_users.user_id' => $data['user_id'],'huddle_users.role_id' => 200
                    );
                $folder_type = [3];
                $account_folder_type = [1];
                $huddle_ids_arr_type_4 = $this->get_huddle_ids_query($data['account_id'],$account_folder_type,$condition_array,$folder_type,false);
                $merged_array = array_merge($huddle_ids_arr_type_4, $merged_array);
            }
            if(in_array('4',$input['huddle_ids']))
            {
                $condition_array = array('afmd.meta_data_name' => 'folder_type','huddle_users.user_id' => $data['user_id'],'huddle_users.role_id' => 210
                    );
                $folder_type = [3];
                $account_folder_type[] = [1];
                $huddle_ids_arr_type_5 = $this->get_huddle_ids_query($data['account_id'],$account_folder_type,$condition_array,$folder_type,false);
                $merged_array = array_merge($huddle_ids_arr_type_5, $merged_array);
            }
                if(in_array('7',$input['huddle_ids']))
                {
                    $query = AccountFolder::select('AccountFolder.account_folder_id')
                            ->leftJoin('account_folders_meta_data as afmd','AccountFolder.account_folder_id','=','afmd.account_folder_id')
                            ->leftJoin('account_folder_users as huddle_users','AccountFolder.account_folder_id','=','huddle_users.account_folder_id')
                            ->leftJoin('users as User','User.id','=','huddle_users.user_id')
                            ->where($condition_array
                            )->whereIn('AccountFolder.folder_type',$account_folder_type)
                            ->whereIn('AccountFolder.account_id',array($data['account_id']))
                            ->whereRaw('(AccountFolder.created_by = ' . $data['user_id']. ' OR huddle_users.user_id = ' . $data['user_id']. ')' );

                    $account_coach_huddles = $query->whereIn('afmd.meta_data_value',$folder_type)->groupBy('AccountFolder.account_folder_id')->get()->toArray();
                }
                else 
                {
                    $account_coach_huddles = $merged_array;
                }
               
                $coach_hud = array();
                $documents_ids = array();
                $account_folder_users = array();
                foreach ($account_coach_huddles as $coach_huddles) {
                    $coach_hud[] = $coach_huddles['account_folder_id'];
                }
                if(!empty($coach_hud))
                {
                    $documents_ids = AccountFolderDocument::select('document_id')->whereIn('account_folder_id',$coach_hud)->get()->toArray();
                }
                else 
                {
                    $documents_ids = array();
                }
                if(isset($input['standard_ids']))
                {
                    $frameworks = $this->get_framework_tags_data($documents_ids,$input['standard_ids'],true);
                    $framework_data = $frameworks['standard_data'];
                }
                else 
                {
                    $framework_data = array();
                }
                $custom_markers_ids = AccountTag::select('account_tag_id')->where(array('tag_type' => '1' , 'account_id' => $account_id))->get()->toArray();
                $custom_marker_data = $this->get_custom_marker_data($documents_ids,$custom_markers_ids,true,$start_date,$end_date);
                $avg_pl_level = $this->get_avg_performance_level($documents_ids,$input['framework_id']);
                if(in_array('1',$input['huddle_ids']) || in_array('2',$input['huddle_ids']) || in_array('3',$input['huddle_ids']) || in_array('4',$input['huddle_ids']) || in_array('5',$input['huddle_ids']) || in_array('6',$input['huddle_ids']))
                {
                    $data = $this->attributes_query($account_id,array($data['account_id']),array($data['user_id']),$coach_hud,$report_config_id,$start_date,$end_date,true,'','',$active)[0];
                    $data = json_decode(json_encode($data), True);
                }
                    $leader_board_count = 0;
                    if(isset($data['video_upload_counts']))
                    {
                        $leader_board_count = $leader_board_count + $data['video_upload_counts'];
                    }
                    if(isset($data['library_upload_counts']))
                    {
                        $leader_board_count = $leader_board_count + $data['library_upload_counts'];
                    }
                    if(isset($data['workspace_upload_counts']))
                    {
                        $leader_board_count = $leader_board_count + $data['workspace_upload_counts'];
                    }
                    if(isset($data['total_hours_viewed']))
                    {
                        $leader_board_count = $leader_board_count + round($data['total_hours_viewed']*60);
                    }
                    if(isset($data['comments_initiated_count']))
                    {
                        $leader_board_count = $leader_board_count + $data['comments_initiated_count'];
                    }
                    if(isset($data['workspace_comments_initiated_count']))
                    {
                        $leader_board_count = $leader_board_count + $data['workspace_comments_initiated_count'];
                    }
                    if(isset($data['replies_initiated_count']))
                    {
                        $leader_board_count = $leader_board_count + $data['replies_initiated_count'];
                    }
                    if(isset($data['huddle_discussion_posts']))
                    {
                        $leader_board_count = $leader_board_count + $data['huddle_discussion_posts'];
                    }
                    if(isset($data['workspace_resources_viewed']))
                    {
                        $leader_board_count = $leader_board_count + $data['workspace_resources_viewed'];
                    }
                    if(isset($data['workspace_resources_uploaded']))
                    {
                        $leader_board_count = $leader_board_count + $data['workspace_resources_uploaded'];
                    }
                    if(isset($data['documents_viewed_count']))
                    {
                        $leader_board_count = $leader_board_count + $data['documents_viewed_count'];
                    }
                    if(isset($data['documents_uploaded_count']))
                    {
                        $leader_board_count = $leader_board_count + $data['documents_uploaded_count'];
                    }
            foreach($report_attributes as $row)
            {
                if(!empty($row->value) && $row->status)
                {
                    $updated_attribute_data[$value][] = $data[$row->value]; 
                }
                if($row->name == 'Framework Tags' && $row->status && !in_array('Standard Performance Levels',$report_attributes_array))
                {
                    foreach ($framework_data as $standard_data)
                    {
                       $updated_attribute_data[$value][] = $standard_data['count'];
                    }
                    if(empty($framework_data) && isset($input['standard_ids']))
                    {
                        foreach($input['standard_ids'] as $st)
                        {
                            $updated_attribute_data[$value][] =  '0';
                        }
                    }
                }
                if($row->name == 'Standard Performance Levels' && $row->status && !in_array('Framework Tags',$report_attributes_array))
                {
                    foreach ($framework_data as $standard_data)
                    {
                       $updated_attribute_data[$value][] = $standard_data['pl_average'];
                    }
                    if(empty($framework_data) && isset($input['standard_ids']))
                    {
                        foreach($input['standard_ids'] as $st)
                        {
                            $updated_attribute_data[$value][] =  '0';
                        }
                    }
                }
                if($row->name == 'Framework Tags' && $row->status && in_array('Standard Performance Levels',$report_attributes_array ))
                {
                    foreach ($framework_data as $standard_data)
                    {
                       $updated_attribute_data[$value][] = $standard_data['count']; 
                       $updated_attribute_data[$value][] = $standard_data['pl_average'];
                    }
                    if(empty($framework_data) && isset($input['standard_ids']))
                    {
                        foreach($input['standard_ids'] as $st)
                        {
                            $updated_attribute_data[$value][] =  '0';
                            $updated_attribute_data[$value][] =  '0';
                        }
                    }
                }
                if($row->name == 'Custom Video Marker Tags' && $row->status)
                {
                    foreach ($custom_marker_data as $marker_data)
                    {
                       $updated_attribute_data[$value][] = $marker_data['count'];
                    }
                    if(empty($custom_marker_data))
                    {
                        foreach($custom_markers_ids as $cm)
                        {
                            $updated_attribute_data[$value][] =  '0';
                            $updated_attribute_data[$value][] =  '0';
                        }
                    }
                }
                if($row->name == 'Avg Performance Levels' && $row->status)
                {
                    $updated_attribute_data[$value][] = $avg_pl_level;
                }
                
                if($row->name == 'Activity' && $row->status)
                {
                   $updated_attribute_data[$value][] = $leader_board_count;
                }
                
                if($row->name == 'Is Active' && $row->status)
                {
                    $user_active_detail = UserAccount::where(array('user_id' => $data['user_id'] , 'account_id' => $data['account_id']))->first();
                    if($user_active_detail->is_active == '1' && $user_active_detail->status_type == 'Active' )
                    {
                        $updated_attribute_data[$value][] = 'Yes';
                    }
                    else 
                    {
                        $updated_attribute_data[$value][] = 'No';
                    }
                }
                
                if($row->name == 'Custom Fields' && $row->status)
                {
                            
                            if(isset($account_user_custom_fields['user_summary']['custom_fields']))
                            {
                                $custom_fields_array = $account_user_custom_fields['user_summary']['custom_fields'];
                                foreach($custom_fields_array as $cf)
                                {
                                    $user_custom_values = UserCustomField::get_users_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,'');
                                    if(!empty($user_custom_values))
                                    {
                                        if($user_custom_values[0]->custom_field_value == 'true')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'Yes'; 
                                        }
                                        if($user_custom_values[0]->custom_field_value == 'false')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'No'; 
                                        }
                                        $updated_attribute_data[$value][] = $user_custom_values[0]->custom_field_value;
                                    }
                                    else 
                                    {
                                        $updated_attribute_data[$value][] = '';
                                    }
                                }
                            }

                            if(isset($account_user_custom_fields['user_summary']['parent_custom_fields']))
                            {
                                $custom_fields_array = $account_user_custom_fields['user_summary']['parent_custom_fields'];
                                foreach($custom_fields_array as $cf)
                                {
                                    $user_custom_values = UserCustomField::get_users_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,'');
                                    if(!empty($user_custom_values))
                                    {
                                        if($user_custom_values[0]->custom_field_value == 'true')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'Yes'; 
                                        }
                                        if($user_custom_values[0]->custom_field_value == 'false')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'No'; 
                                        }
                                        $updated_attribute_data[$value][] = $user_custom_values[0]->custom_field_value;
                                    }
                                    else 
                                    {
                                        $updated_attribute_data[$value][] = '';
                                    }
                                }

                            }

                }
                if($row->name == 'Huddles Participating In' && $row->status){
                                        
                    $updated_attribute_data[$value][] = $this->get_huddles_participating_in($data['user_id'],$data['account_id'],$input['start_date'],$input['end_date'],$huddle_ids_for_participating);

                }
            }
            $updated_attribute_data[$value]['image'] = $data['image'];
            $updated_attribute_data[$value]['account_id'] =  $data['account_id'];
            $updated_attribute_data[$value]['user_id'] = $data['user_id'];
            $updated_attribute_data[$value]['email'] = $data['email'];
            $updated_attribute_data[$value]['user_name'] = $data['user_name'];
            
        }
        
         $header_data = array();
         $count = 0;
         $translations =  TranslationsManager::get_page_lang_based_content("Api/analytics", $request->header("site_id"), '', $request->header("current-lang"));

                
        foreach($report_attributes as $key => $row)
        {
            if(!empty($row->value) && $row->status)
            {
                $key = str_replace(' ','-',strtolower($row->name));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : $row->name;
                //$header_data[$count]['name'] = $row->name;
                $header_data[$count]['key'] = str_replace(' ','_',strtolower($row->name));
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['sorting'] = true;
                $header_data[$count]['sorting_key'] = $row->value;
                if($row->name == 'Account Name' || $row->name == 'User Role' || $row->name == 'User Type'  )
                {
                    $header_data[$count]['detail'] = false;
                }
                $count++; 
            }
            if($row->name == 'Framework Tags' && $row->status && !in_array('Standard Performance Levels',$report_attributes_array))
            {
                foreach ($framework_data as $standard_data)
                {
                    $header_data[$count]['name'] = $standard_data['label'];
                    $header_data[$count]['key'] = 'standard';
                    $header_data[$count]['standard'] = true;
                    $header_data[$count]['performance'] = false;
                    $header_data[$count]['markers'] = false;
                    $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                    $count++;
                }
            }
            if($row->name == 'Standard Performance Levels' && $row->status && !in_array('Framework Tags',$report_attributes_array))
            {
                foreach ($framework_data as $standard_data)
                {
                    $header_data[$count]['name'] = $standard_data['label'];
                    $header_data[$count]['key'] = 'performance';
                    $header_data[$count]['standard'] = false;
                    $header_data[$count]['performance'] = true;
                    $header_data[$count]['markers'] = false;
                    $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                    $count++;
                }
            }
            if($row->name == 'Framework Tags' && $row->status && in_array('Standard Performance Levels',$report_attributes_array))
            {
                foreach ($framework_data as $standard_data)
                {
                    $header_data[$count]['name'] = $standard_data['label'];
                    $header_data[$count]['key'] = 'standard';
                    $header_data[$count]['standard'] = true;
                    $header_data[$count]['performance'] = false;
                    $header_data[$count]['markers'] = false;
                    $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                    $count++;
                    $header_data[$count]['name'] = $standard_data['label'];
                    $header_data[$count]['key'] = 'performance';
                    $header_data[$count]['standard'] = false;
                    $header_data[$count]['performance'] = true;
                    $header_data[$count]['markers'] = false;
                    $header_data[$count]['standard_id'] = $standard_data['account_tag_id'];
                    $count++;
                }
            }
            if($row->name == 'Custom Video Marker Tags' && $row->status)
            {
                foreach ($custom_marker_data as $marker_data)
                {
                    $header_data[$count]['name'] = $marker_data['label'];
                    $header_data[$count]['key'] = 'markers';
                    $header_data[$count]['standard'] = false;
                    $header_data[$count]['performance'] = false;
                    $header_data[$count]['markers'] = true;
                    $header_data[$count]['standard_id'] = $marker_data['account_tag_id'];
                    $count++;
                }
            }
            if($row->name == 'Avg Performance Levels' && $row->status)
            {
                    $key = str_replace(' ','-',strtolower('Avg Performance Levels'));
                    $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Avg Performance Levels';
                    //$header_data[$count]['name'] = 'Avg Performance Levels';
                    $header_data[$count]['key'] = 'avg_performance_levels';
                    $header_data[$count]['standard'] = false;
                    $header_data[$count]['performance'] = false;
                    $header_data[$count]['markers'] = false;
                    $header_data[$count]['standard_id'] = false;
                    $header_data[$count]['avg_performance_levels'] = true;
                    $header_data[$count]['detail'] = false;
                    $count++;
            }
            
            if($row->name == 'Is Active' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Is Active'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Is Active';
                //$header_data[$count]['name'] = 'Is Active';
                $header_data[$count]['key'] = str_replace(' ','_',strtolower('Is Active'));
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['detail'] = false;
                $count++;            
            }
            
            if($row->name == 'Activity' && $row->status)
            {
                $key = str_replace(' ','-',strtolower('Activity'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Activity';
                //$header_data[$count]['name'] = 'Activity';
                $header_data[$count]['key'] = str_replace(' ','_',strtolower('Activity'));
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['detail'] = false;
                $count++;            
            }
            
            if($row->name == 'Custom Fields' && $row->status)
            {
                $custom_fields_array = array();
                if(isset($account_user_custom_fields['user_summary']['custom_fields']))
                {
                    $custom_fields_array = $account_user_custom_fields['user_summary']['custom_fields'];
                    foreach($custom_fields_array as $cf)
                    {
                        $header_data[$count]['name'] = $cf->field_label;
                        $header_data[$count]['key'] = '';
                        $header_data[$count]['standard'] = false;
                        $header_data[$count]['performance'] = false;
                        $header_data[$count]['markers'] = false;
                        $header_data[$count]['standard_id'] = false;
                        $header_data[$count]['custom_fields'] = true;
                        $header_data[$count]['detail'] = false;
                        $count++;
                    }
                }
                if(isset($account_user_custom_fields['user_summary']['parent_custom_fields']))
                {
                    $custom_fields_array = $account_user_custom_fields['user_summary']['parent_custom_fields'];
                    foreach($custom_fields_array as $cf)
                    {
                        $header_data[$count]['name'] = $cf->field_label;
                        $header_data[$count]['key'] = '';
                        $header_data[$count]['standard'] = false;
                        $header_data[$count]['performance'] = false;
                        $header_data[$count]['markers'] = false;
                        $header_data[$count]['standard_id'] = false;
                        $header_data[$count]['custom_fields'] = true;
                        $header_data[$count]['detail'] = false;
                        $count++;
                    }
                }

            }

            if($row->name == 'Huddles Participating In' && $row->status){
                    
                $key = str_replace(' ','-',strtolower('Huddles Participating In'));
                $header_data[$count]['name'] = isset($translations[$key]) ? $translations[$key] : 'Huddles Participating In';
                //$header_data[$count]['name'] = 'Activity';
                $header_data[$count]['key'] = str_replace(' ','_',strtolower('Huddles Participating In'));
                $header_data[$count]['standard'] = false;
                $header_data[$count]['performance'] = false;
                $header_data[$count]['markers'] = false;
                $header_data[$count]['standard_id'] = false;
                $header_data[$count]['detail'] = false;
                $count++;       

            }
        }
        $result = array();
        $result['attributes_data'] = $updated_attribute_data;
        $result['attributes_header'] = $header_data;
        $result['sort_by'] = ($request->has('sort_by')) ? $request->get('sort_by') : '';
        $result['sort_by_order'] = ($request->has('sort_by_order')) ? $request->get('sort_by_order') : '';
        $result['total_users'] = $attribute_data_count;
        $result['page'] = $input['page'];
        $result['limit'] = $input['limit'];
        return response()->json($result);
    }

    function user_summary_report_csv(Request $request){
        if($request->has('async') && $request->async == '1')
        {
            ini_set('memory_limit','5000M');
            ini_set('max_execution_time', 1000); //3 minutes
            set_time_limit(0);
        $response = $this->user_summary_report($request);
        $response = $response->getData();
        $account_id = $request->account_id;
        $report_user_summary = json_decode(json_encode($response),true);
        $result = array();
        $count = 0;
        $header_data_array = array();
        $user_name_email = array("User Name", "Email");
        $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
        $account_name = implode('_',$account_name);
        foreach($report_user_summary['attributes_header'] as $row){
            $header_data_array[] = $row['name'];
        }
    
        $header_data_array =  array_merge($user_name_email,$header_data_array);
        foreach($report_user_summary['attributes_data'] as $row){
            unset($row['image']);
            unset($row['account_id']);
            unset($row['user_id']);
            array_unshift($row,$row['email']);
            array_unshift($row,$row['user_name']);
            unset($row['email']);
            unset($row['user_name']); 
            unset($row['huddle_ids']);
            unset($row['document_ids']);
            $row = array_map('trim', $row);
            $result[$count] = $row;
            $count++; 
    }
    $exportUserSummaryToCSV = new \App\Services\Exports\UserSummary\ExportToCSV;
    return $exportUserSummaryToCSV->export($header_data_array,$result,$account_name,$request->file_name,$request->user_id);
        }
        else 
        {
          $input = $request->all();
          $account_id = $request->account_id;
          $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
          $account_name = implode('_',$account_name);
          $account_name = $account_name . "_user_summary_report_";
          $file_name = $account_name .date('YmdHis').'.csv';
          $input['file_name'] = $file_name;
          $input['async'] = 1;
          $input['paginate'] = false;
          //$query_string = http_build_query($input);
          //exec('wget -O - "http://local.sibme.local/user_summary_report_csv?'.$query_string.'" > /dev/null 2>&1 &');
          exec("php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "' --report=user_summary_report_csv > /dev/null 2>&1 &" );
          $final_result = array();
          $final_result['filename'] = $file_name;
          //$final_result['cmd'] = "php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "' --report=user_summary_report_csv > /dev/null 2>&1 &";
          return response()->json($final_result);

        }
    }

    function user_summary_report_excel(Request $request){
        if($request->has('async') && $request->async == '1')
        {
            ini_set('memory_limit','5000M');
            ini_set('max_execution_time', 1000); //3 minutes
            set_time_limit(0);    
        $response = $this->user_summary_report($request);
        $response = $response->getData();
        $account_id = $request->account_id;
        $report_user_summary = json_decode(json_encode($response),true);
        $result = array();
        $count = 0;
        $header_data_array = array();
        $user_name_email = array("User Name", "Email");
        $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
        $account_name = implode('_',$account_name);
        foreach($report_user_summary['attributes_header'] as $row){
            $header_data_array[] = $row['name'];
        }
    
        $header_data_array =  array_merge($user_name_email,$header_data_array);
        foreach($report_user_summary['attributes_data'] as $row){ 
            unset($row['image']);
            unset($row['account_id']);
            unset($row['user_id']);
            array_unshift($row,$row['email']);
            array_unshift($row,$row['user_name']);
            unset($row['email']);
            unset($row['user_name']);
            unset($row['huddle_ids']);
            unset($row['document_ids']);
            $result[$count] = $row;
            $count++; 
    }
    $exportUserSummaryToExcel = new \App\Services\Exports\UserSummary\ExportToExcel;
    return $exportUserSummaryToExcel->export($header_data_array,$result,$account_name,$request->file_name,$request->user_id);
        }
        else 
        {
          $input = $request->all();
          $account_id = $request->account_id;
          $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
          $account_name = implode('_',$account_name);
          $account_name = $account_name . "_user_summary_report_";
          $file_name = $account_name .date('YmdHis').'.xlsx';
          $input['file_name'] = $file_name;
          $input['async'] = 1;
          $input['paginate'] = false;
          //$query_string = http_build_query($input);
          //exec('wget -O - "http://local.sibme.local/user_summary_report_excel?'.$query_string.'" > /dev/null 2>&1 &');
          exec("php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "' --report=user_summary_report_excel > /dev/null 2>&1 &" );
          $final_result = array();
          $final_result['filename'] = $file_name;
          return response()->json($final_result);

        }
    }

    function user_summary_report_pdf(Request $request){
       // return $request->active;
        if($request->has('async') && $request->async == '1')
        {
         ini_set('memory_limit','5000M');
         ini_set('max_execution_time', 1000); //3 minutes
         set_time_limit(0);    
        $response = $this->user_summary_report($request);
       // return $response;
        $response = $response->getData();
        $account_id = $request->account_id;
        $report_user_summary = json_decode(json_encode($response),true);
        $result = array();
        $count = 0;
        $header_data_array = array();
        $user_name_email = array("User Name", "Email");
        $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
        $account_name = implode('_',$account_name);
        foreach($report_user_summary['attributes_header'] as $row){
            $header_data_array[] = $row['name'];
        }
    
        $header_data_array =  array_merge($user_name_email,$header_data_array);
        foreach($report_user_summary['attributes_data'] as $row){ 
            unset($row['image']);
            unset($row['account_id']);
            unset($row['user_id']);
            array_unshift($row,$row['email']);
            array_unshift($row,$row['user_name']);
            unset($row['email']);
            unset($row['user_name']);
            $result[$count] = $row;
            $count++; 
    }
    $account_name = $account_name . "_user_summary_";
    $this->export_pdf($header_data_array,$result,$account_name,$request->file_name);
    }
    else 
     {
       $input = $request->all();
       $account_id = $request->account_id;
       $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
       $account_name = implode('_',$account_name);
       $account_name = $account_name . "_user_summary_report_";
       $file_name = $account_name .date('YmdHis').'.pdf';
       $input['file_name'] = $file_name;
       $input['async'] = 1;
       $input['paginate'] = false;
       //$query_string = http_build_query($input);
       //exec('wget -O - "http://local.sibme.local/user_summary_report_pdf?'.$query_string.'" > /dev/null 2>&1 &');
       exec("php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "' --report=user_summary_report_pdf > /dev/null 2>&1 &" );
       $final_result = array();
       $final_result['filename'] = $file_name;
       $final_result['console'] = "php7 ".base_path()."/artisan process:ReportExport '" .json_encode($input) . "' --report=user_summary_report_pdf > /dev/null 2>&1 &";
       //$final_result['cron'] = 'wget -O - "http://local.sibme.local/user_summary_report_pdf?'.$query_string;
       return response()->json($final_result);

     }
    }
    function detail_view(Request $request)
    {
        $input = $request->all();
        $translations = TranslationsManager::get_page_lang_based_content("Api/analytics", $request->header("site_id"), '', $request->header("current-lang"));
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];   
        $start_date = $input['start_date'];
        $end_date = $input['end_date'];
        $stDate = date_create($start_date);
        $start_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
        
        
        $endDate = date_create($end_date);
        $end_date = date_format($endDate, 'Y-m-d') . ' 23:59:59';
        $huddle_document_ids = $this->get_huddle_document_ids($account_id,$user_id,$input['coach_id'],$input['report_type'],$input['huddle_type'],$input['huddle_id']);
        $all_huddle_ids = implode(',', $huddle_document_ids['huddle_ids']);
        $all_document_ids = implode(',', $huddle_document_ids['document_ids']);
        if(empty($all_huddle_ids))
        {
            $all_huddle_ids = '0';
        }
         if(empty($all_document_ids))
        {
            $all_document_ids = '0';
        }
        
        if(isset($input['report_id']))
        {
            $report_config_id = $input['report_id'];
        }
        else 
        {
            $default_report_config = ReportConfiguration::where(array('report_type' => $input['report_type'],'is_default' => '1'))->first();
            $report_config_id = $default_report_config->id;
        }
        
        $report_data = ReportConfiguration::where(array("id" => $report_config_id))->first();
        $report_attributes = json_decode($report_data->report_attributes);
        $report_attributes_array = array();
        $updated_attribute_data = array();
        foreach($report_attributes as $row)
        {
            if($row->status)
            {
                $report_attributes_array[] = $row->name; 
            }
        }
        
        $dropdown_data = array();
        foreach($report_attributes as $key => $row)
        {
            if(!empty($row->value) && $row->status)
            {
                $dropdown_data[] = $row->name; 
            }
        }
        
        $huddles_check = " AND ual.account_folder_id IN (".$all_huddle_ids.")";
        $huddles_check_afd = " AND afd.account_folder_id IN (".$all_huddle_ids.")";
        
        $mAUCndSLog = ' AND date_added >="' . $start_date . '"' ;
        $mAUCndELog = ' AND date_added <="' . $end_date . '"' ;
        $curr_user = User::select('id', 'username', 'first_name', 'last_name')
                        ->where(array('id' => $user_id, 'site_id' => $this->site_id))
                        ->first()->toArray();
        if(isset($input['header']) && !is_array($input['header']))
        {
            $input['header'] = json_decode($input['header'],true);
        }
        $types = $input['header'];
        $header_data = [];
        $header_data[0] = 'Action';
        $header_data[1] = 'User Name';
        $header_data[2] = 'Activity Date';
        $all_users = array();
        foreach ($types as $type_in) 
        {
            if($type_in['key'] == 'standard' && $type_in['standard'])
            {
                if(!in_array('Huddles',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] :  'Huddles'; 
                }
                if(!in_array('Workspace',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace'; 
                }
                if(!in_array('Video Name',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                }
                $account_comment_tags = DB::table('account_comment_tags')->whereIn('ref_id',$huddle_document_ids['document_ids'])->whereIn('account_tag_id' , array($type_in['standard_id']))->get()->toArray();
                $account_comment_tags_new = array();
                $array_count = 0;
                foreach ($account_comment_tags as $user) {
                        $account_comment_tags_new[$array_count] = (array)$user;
                        $account_comment_tags_new[$array_count]['type'] = 'standard';
                        $account_comment_tags_new[$array_count]['date_added'] = $user->created_date;
                        $account_comment_tags_new[$array_count]['desc'] = $user->tag_title;
                        $array_count++;
                    }
                $all_users = array_merge($account_comment_tags_new, $all_users);
                
                
            }
            if($type_in['key'] == 'performance' && $type_in['performance'])
            {
                if(!in_array('Huddles',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                }
                if(!in_array('Workspace',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace'; 
                }
                if(!in_array('Video Name',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                }
                if(!in_array('Rating',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['rating']) ? $translations['rating'] : 'Rating'; 
                }
                $document_standard_ratings = DB::table('document_standard_ratings')->whereIn('document_id',$huddle_document_ids['document_ids'])->whereIn('standard_id' , array($type_in['standard_id']))->get()->toArray();
                $document_standard_ratings_new = array();
                $array_count = 0;
                foreach ($document_standard_ratings as $user) {
                        $document_standard_ratings_new[$array_count] = (array)$user;
                        $document_standard_ratings_new[$array_count]['type'] = 'performance';
                        $document_standard_ratings_new[$array_count]['date_added'] = $user->created_at;
                        $document_standard_ratings_new[$array_count]['rating'] = $user->rating_value;
                        $array_count++;
                    }
                $all_users = array_merge($document_standard_ratings_new, $all_users);
                
                
            }
            if($type_in['key'] == 'markers' && $type_in['markers'])
            {
                if(!in_array('Huddles',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                }
                if(!in_array('Workspace',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace'; 
                }
                if(!in_array('Video Name',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                }
                if(!in_array('Custom Marker Name',$header_data))
                {
                   $header_data[count($header_data)] = 'Custom Marker Name';
                }
                $account_comment_tags = DB::table('account_comment_tags')->whereIn('ref_id',$huddle_document_ids['document_ids'])->whereIn('account_tag_id' , array($type_in['standard_id']))->whereRaw('account_comment_tags.created_date >= "'.$start_date. '" AND account_comment_tags.created_date <= "' .$end_date . '"' )->get()->toArray();
                $account_comment_tags_new = array();
                $array_count = 0;
                foreach ($account_comment_tags as $user) {
                        $account_comment_tags_new[$array_count] = (array)$user;
                        $account_comment_tags_new[$array_count]['type'] = 'markers';
                        $account_comment_tags_new[$array_count]['date_added'] = $user->created_date;
                        $account_comment_tags_new[$array_count]['desc'] = $user->tag_title;
                        $array_count++;
                    }
                $all_users = array_merge($account_comment_tags_new, $all_users);
                
                
            }
            if ($type_in['key'] == 'videos_uploaded_to_huddles') {
                if(!in_array('Huddles',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                }
                if(!in_array('Video Name',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                }
                $types = '(ual.type = 2 OR ual.type = 4)';
                $huddles_video_uploaded = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type` FROM user_activity_logs ual LEFT JOIN account_folders AS af
                ON af.`account_folder_id` = ual.`account_folder_id` WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND af.`folder_type` = 1 AND ual.user_id = " . $user_id . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                $huddles_video_uploaded = json_decode(json_encode((array) $huddles_video_uploaded), true);
                $all_users = array_merge($huddles_video_uploaded, $all_users);
            }
            
            if ($type_in['key'] == 'videos_uploaded_to_library') {
                if(!in_array('Library',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['library']) ? $translations['library'] : 'Library'; 
                }
                $types = '(ual.type = 2 OR ual.type = 4)';
                $library_video_uploaded = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type`  FROM user_activity_logs ual LEFT JOIN account_folders AS af
                ON af.`account_folder_id` = ual.`account_folder_id` WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND af.`folder_type` = 2 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                $library_video_uploaded = json_decode(json_encode((array) $library_video_uploaded), true);
                $all_users = array_merge($library_video_uploaded, $all_users);
            }
            
            if ($type_in['key'] == 'videos_uploaded_to_workspace') {
                if(!in_array('Workspace',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace'; 
                }
                $types = '(ual.type = 2 OR ual.type = 4)';
                $workspace_video_uploaded = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type`  FROM user_activity_logs ual LEFT JOIN account_folders AS af
                ON af.`account_folder_id` = ual.`account_folder_id` WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND af.`folder_type` = 3 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                $workspace_video_uploaded = json_decode(json_encode((array) $workspace_video_uploaded), true);
                $all_users = array_merge($workspace_video_uploaded, $all_users);
            }
            
            if ($type_in['key'] == 'huddle_video_comments' || $type_in['key'] == 'comments' ) {
                if(!in_array('Huddles',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                }
                if(!in_array('Comment',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['comment']) ? $translations['comment'] : 'Comment'; 
                }
                if(!in_array('Video Name',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                }
                $types = '(ual.type = 5)';

                //$huddles_comment_uploaded = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type` , comments.`comment`  FROM user_activity_logs ual left join comments on ual.ref_id = comments.id left join account_folders on ual.account_folder_id = account_folders.account_folder_id WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . "  AND comments.ref_type not in (6,7)   AND ual.user_id = " . $user_id . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                //$huddles_comment_uploaded = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type` , comments.`comment`  FROM user_activity_logs ual join comments on ual.ref_id = comments.id join account_folders on ual.account_folder_id = account_folders.account_folder_id WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND account_folders.`folder_type` = 1 AND ual.user_id = " . $user_id . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                $huddles_comment_uploaded = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type` , comments.`comment`  FROM user_activity_logs ual join comments on ual.ref_id = comments.id join account_folders on ual.account_folder_id = account_folders.account_folder_id WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND account_folders.`folder_type` = 1 AND ual.user_id = " . $user_id . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                $huddles_comment_uploaded = json_decode(json_encode((array) $huddles_comment_uploaded), true);
                $all_users = array_merge($huddles_comment_uploaded, $all_users);
            }
            
            if ($type_in['key'] == 'workspace_video_notes') 
            {
                    if(!in_array('Workspace',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace'; 
                    }
                    if(!in_array('Video Name',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                    }
                    if(!in_array('Comment',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['comment']) ? $translations['comment'] : 'Comment'; 
                    }
                    $types = '(ual.type = 5)';
                    $workspace_comment_uploaded = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type` , comments.`comment`  FROM user_activity_logs ual join comments on ual.ref_id = comments.id join account_folders on ual.account_folder_id = account_folders.account_folder_id WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND account_folders.`folder_type` = 3 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $workspace_comment_uploaded = json_decode(json_encode((array) $workspace_comment_uploaded), true);
                    $all_users = array_merge($workspace_comment_uploaded, $all_users);
            }
            
            if($type_in['key'] == 'huddle_videos_viewed') {
                    if(!in_array('Huddles',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                    }
                    if(!in_array('Video Name',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                    }
                    $types = '(ual.type = 11)';
                    $huddles_video_viewed = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual  LEFT JOIN account_folder_documents AS afd
                        ON afd.document_id = ual.ref_id
                        LEFT JOIN account_folders AS af
                        ON af.`account_folder_id` = afd.`account_folder_id` WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND af.`folder_type` = 1 AND ual.user_id = " . $user_id . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $huddles_video_viewed = json_decode(json_encode((array) $huddles_video_viewed), true);
                    $all_users = array_merge($huddles_video_viewed, $all_users);
                }
            if($type_in['key'] == 'library_videos_viewed') {
                    if(!in_array('Library',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['library']) ? $translations['library'] : 'Library'; 
                    }
                    if(!in_array('Video Name',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                    }
                    $types = '(ual.type = 11)';
                    $library_video_viewed = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type` FROM user_activity_logs ual  LEFT JOIN account_folder_documents AS afd
                    ON afd.document_id = ual.ref_id
                    LEFT JOIN account_folders AS af
                    ON af.`account_folder_id` = afd.`account_folder_id` WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND af.`folder_type` = 2 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $library_video_viewed = json_decode(json_encode((array) $library_video_viewed), true);
                    $all_users = array_merge($library_video_viewed, $all_users);
                }
                
            if($type_in['key'] == 'videos_viewed_in_workspace') {
                    if(!in_array('Workspace',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace'; 
                    }
                    if(!in_array('Video Name',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                    }
                    $types = '(ual.type = 11)';
                    $workspace_video_viewed = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type` FROM user_activity_logs ual  LEFT JOIN account_folder_documents AS afd
                    ON afd.document_id = ual.ref_id
                    LEFT JOIN account_folders AS af
                    ON af.`account_folder_id` = afd.`account_folder_id` WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND af.`folder_type` = 3 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $workspace_video_viewed = json_decode(json_encode((array) $workspace_video_viewed), true);
                    $all_users = array_merge($workspace_video_viewed, $all_users);
                }
                
            if ($type_in['key'] == 'videos_shared_to_huddles' || $type_in['key'] == 'videos_uploaded') {
                    if(!in_array('Huddles',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                    }
                    if(!in_array('Video Name',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                    }
                    $types = '(ual.type = 22)';
                    $huddles_video_shared = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual  LEFT JOIN account_folder_documents AS afd
                    ON afd.document_id = ual.ref_id
                    LEFT JOIN account_folders AS af
                    ON af.`account_folder_id` = afd.`account_folder_id` WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . "  AND ual.user_id = " . $user_id . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $huddles_video_shared = json_decode(json_encode((array) $huddles_video_shared), true);
                    $all_users = array_merge($huddles_video_shared, $all_users);
                    //query before fixing
                    //ON af.`account_folder_id` = afd.`account_folder_id` WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND af.`folder_type` = 1 AND ual.user_id = " . $user_id . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");

                }
            if ($type_in['key'] == 'videos_shared_to_library') {
                    if(!in_array('Library',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['library']) ? $translations['library'] : 'Library'; 
                    }
                    if(!in_array('Video Name',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                    }
                    $types = '(ual.type = 22)';
                    $library_video_shared = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual  LEFT JOIN account_folder_documents AS afd
                    ON afd.document_id = ual.ref_id
                    LEFT JOIN account_folders AS af
                    ON af.`account_folder_id` = afd.`account_folder_id` WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND af.`folder_type` = 2 AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $library_video_shared = json_decode(json_encode((array) $library_video_shared), true);
                    $all_users = array_merge($library_video_shared, $all_users);
                }
            if ($type_in['key'] == 'huddle_video_replies') 
                {
                    if(!in_array('Huddles',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                    }
                    if(!in_array('Workspace',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace'; 
                    }
                    if(!in_array('Comment',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['comment']) ? $translations['comment'] : 'Comment'; 
                    }
                    $types = '(ual.type = 8)';
                    $replies_posted = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` , comments.`comment` FROM user_activity_logs ual join comments on ual.ref_id = comments.id  join account_folder_documents afd ON afd.`document_id` = comments.ref_id  WHERE ual.site_id = " . $this->site_id . " AND comments.ref_type = 3 AND ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id  . $huddles_check_afd . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $replies_posted = json_decode(json_encode((array) $replies_posted), true);
                    $all_users = array_merge($replies_posted, $all_users);
                }
            if ($type_in['key'] == 'huddle_discussions_posts') 
            {
                if(!in_array('Huddles',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                }
                if(!in_array('Workspace',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace'; 
                }
                if(!in_array('Comment',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['comment']) ? $translations['comment'] : 'Comment'; 
                }
                $types = '(ual.type = 8)';
                $replies_posted = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` , comments.`comment` FROM user_activity_logs ual join comments on ual.ref_id = comments.id  WHERE ual.site_id = " . $this->site_id . " AND comments.ref_type = 1 AND ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id  . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                $replies_posted = json_decode(json_encode((array) $replies_posted), true);
                $array_count = 0;
                $replies_posted_new = array();
                foreach ($replies_posted as $user) {
                        $replies_posted_new[] = $user;
                        $replies_posted_new[$array_count]['type'] = 'huddle_discussions_posts';
                        $array_count++;
                }
                $all_users = array_merge($replies_posted_new, $all_users);
            }
               if ($type_in['key'] == 'huddle_discussion_posts') {
                    if(!in_array('Huddles',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                    }
                    if(!in_array('Comment',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['comment']) ? $translations['comment'] : 'Comment'; 
                    }
                    $types = '(ual.type = 8)';
                    $huddle_discussion_posts = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` , comments.`comment` FROM user_activity_logs ual join comments on ual.ref_id = comments.id  WHERE ual.site_id = " . $this->site_id . " AND comments.ref_type = 1 AND ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id  . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $huddle_discussion_posts = json_decode(json_encode((array) $huddle_discussion_posts), true);
                    $all_users = array_merge($huddle_discussion_posts, $all_users);
                }
               if ($type_in['key'] == 'workspace_scripted_notes') {
                    $types = '(ual.type = 23)';
                    $scripted_observations = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual LEFT JOIN account_folder_documents afd
                                                        ON ual.`ref_id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN documents d
                                                        ON (d.id = ual.ref_id)  WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND af.`folder_type` = 3 AND d.is_associated = 1  AND d.current_duration is NULL AND d.published = 1 AND d.doc_type = 3  AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $scripted_observations = json_decode(json_encode((array) $scripted_observations), true);
                    $all_users = array_merge($scripted_observations, $all_users);

                }
                
               if ($type_in['key'] == 'scripted_video_observations') {
                    $types = '(ual.type = 20)';
                    $scripted_video_observations = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual LEFT JOIN account_folder_documents afd
                                                        ON ual.`ref_id`= afd.`document_id`
                                                        LEFT JOIN account_folders af
                                                        ON afd.`account_folder_id`= af.`account_folder_id`
                                                        LEFT JOIN documents d
                                                        ON (d.id = ual.ref_id)  WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND af.`folder_type` = 1 AND d.is_associated > 0  AND d.current_duration is NOT NULL AND d.published = 1 AND d.doc_type = 3  AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                    $scripted_video_observations = json_decode(json_encode((array) $scripted_video_observations), true);
                    $all_users = array_merge($scripted_video_observations, $all_users);
                }
                
               if ($type_in['key'] == 'hours_uploaded') {
                    if(!in_array('Huddles',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                    }
                    if(!in_array('Workspace',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace'; 
                    }
                    if(!in_array('Minutes',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['minutes']) ? $translations['minutes'] : 'Minutes'; 
                    }
                    if(!in_array('Video Name',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                    }
                    $total_video_hours_uploaded = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` , df.`duration`  FROM user_activity_logs ual INNER JOIN documents d
                                                        ON ual.`ref_id`= d.`id`
                                                        INNER JOIN document_files df
                                                        ON d.`id` = df.`document_id`
                                                      WHERE ual.site_id = " . $this->site_id . " AND d.account_id = " . $account_id . " AND d.created_by = " . $user_id . $huddles_check . " AND ual.type IN (2,4) AND d.recorded_date >= '$start_date' and d.recorded_date <= '$end_date' ");
                    $total_video_hours_uploaded = json_decode(json_encode((array) $total_video_hours_uploaded), true);
                    $array_count = 0;
                    $total_video_hours_uploaded_new = array();
                    foreach ($total_video_hours_uploaded as $user) {
                        $total_video_hours_uploaded_new[] = $user;
                        $total_video_hours_uploaded_new[$array_count]['type'] = 24;
                        $array_count++;
                    }
                    $all_users = array_merge($total_video_hours_uploaded_new, $all_users);
                }
                
                if ($type_in['key'] == 'live_hours_uploaded') {
                    if(!in_array('Huddles',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                    }
                    if(!in_array('Workspace',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace'; 
                    }
                    if(!in_array('Minutes',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['minutes']) ? $translations['minutes'] : 'Minutes'; 
                    }
                    if(!in_array('Video Name',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                    }
                    $total_video_hours_uploaded = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` , df.`duration`  FROM user_activity_logs ual INNER JOIN documents d
                                                        ON ual.`ref_id`= d.`id`
                                                        INNER JOIN document_files df
                                                        ON d.`id` = df.`document_id`
                                                      WHERE ual.site_id = " . $this->site_id . " AND d.account_id = " . $account_id . " AND d.created_by = " . $user_id . $huddles_check . " AND ual.type IN (24) AND d.recorded_date >= '$start_date' and d.recorded_date <= '$end_date' ");
                    $total_video_hours_uploaded = json_decode(json_encode((array) $total_video_hours_uploaded), true);
                    $array_count = 0;
                    $total_video_hours_uploaded_new = array();
                    foreach ($total_video_hours_uploaded as $user) {
                        $total_video_hours_uploaded_new[] = $user;
                        $total_video_hours_uploaded_new[$array_count]['type'] = 'live_hours_uploaded';
                        $array_count++;
                    }
                    $all_users = array_merge($total_video_hours_uploaded_new, $all_users);
                }
              
                if ($type_in['key'] == 'hours_viewed') {
                    if(!in_array('Huddles',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                    }
                    if(!in_array('Workspace',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace'; 
                    }
                    if(!in_array('Minutes',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['minutes']) ? $translations['minutes'] : 'Minutes'; 
                    }
                    if(!in_array('Video Name',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                    }
                    $total_video_hours_viewed = App('db')->select("SELECT dvh.created_date, dvh.document_id , MAX(dvh.minutes_watched) AS minutes_watched  FROM users us LEFT JOIN document_viewer_histories dvh
                                                        ON dvh.`user_id`= us.`id`
                                                        INNER JOIN account_folder_documents afd
                                                        ON afd.`document_id` = dvh.`document_id`
                                                        WHERE dvh.site_id = " . $this->site_id . " ".$huddles_check_afd." AND dvh.account_id = " . $account_id . " AND dvh.user_id = " . $user_id . "  AND dvh.created_date >= '$start_date' and dvh.created_date <= '$end_date' GROUP BY  afd.`document_id`");
                    $total_video_hours_viewed = json_decode(json_encode((array) $total_video_hours_viewed), true);
                    $array_count = 0;
                    $total_video_hours_viewed_new = array();
                    foreach ($total_video_hours_viewed as $user) {
                        $total_video_hours_viewed_new[] = $user;
                        $total_video_hours_viewed_new[$array_count]['type'] = 25;
                        $array_count++;
                    }
                    $all_users = array_merge($total_video_hours_viewed_new, $all_users);
                }
                
            if ($type_in['key'] == 'live_hours_viewed') {
                    if(!in_array('Huddles',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                    }
                    if(!in_array('Workspace',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace'; 
                    }
                    if(!in_array('Minutes',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['minutes']) ? $translations['minutes'] : 'Minutes'; 
                    }
                    if(!in_array('Video Name',$header_data))
                    {
                       $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                    }
                    $total_video_hours_viewed = App('db')->select("SELECT dvh.document_id , MAX(dvh.minutes_watched) AS minutes_watched  FROM users us LEFT JOIN document_viewer_histories dvh
                                                        ON dvh.`user_id`= us.`id`
                                                        INNER JOIN account_folder_documents afd
                                                        ON afd.`document_id` = dvh.`document_id`
                                                        INNER JOIN user_activity_logs ual
                                                        ON ual.`ref_id` = dvh.`document_id`
                                                        WHERE dvh.site_id = " . $this->site_id . " AND ual.type IN (24) AND dvh.account_id = " . $account_id . " AND dvh.user_id = " . $user_id . $huddles_check . "  AND dvh.created_date >= '$start_date' and dvh.created_date <= '$end_date' GROUP BY  afd.`document_id`");
                    $total_video_hours_viewed = json_decode(json_encode((array) $total_video_hours_viewed), true);
                    $array_count = 0;
                    $total_video_hours_viewed_new = array();
                    foreach ($total_video_hours_viewed as $user) {
                        $total_video_hours_viewed_new[] = $user;
                        $total_video_hours_viewed_new[$array_count]['type'] = 'live_hours_viewed';
                        $array_count++;
                    }
                    $all_users = array_merge($total_video_hours_viewed_new, $all_users);
                }
                
            if ($type_in['key'] == 'notes_shared_to_huddles') {
                $types = '(ual.type = 26)';
                $scripted_notes_shared_upload_counts = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` , ual.`type`  FROM user_activity_logs ual WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                $scripted_notes_shared_upload_counts = json_decode(json_encode((array) $scripted_notes_shared_upload_counts), true);
                $all_users = array_merge($scripted_notes_shared_upload_counts, $all_users);

            }
            
            if ($type_in['key'] == 'huddle_discussions_created') {
                if(!in_array('Huddles',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                }
                $types = '(ual.type = 6)';
                $huddle_discussion_created = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`,ual.`type`  FROM user_activity_logs ual WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                $huddle_discussion_created = json_decode(json_encode((array) $huddle_discussion_created), true);
                $all_users = array_merge($huddle_discussion_created, $all_users);
            }
            
            if ($type_in['key'] == 'huddle_resources_uploaded' || $type_in['key'] == 'resources_uploaded')
            {
                if(!in_array('Huddles',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                }
                if(!in_array('Resource Name',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['resource-name']) ? $translations['resource-name'] : 'Resource Name'; 
                }
                $types = '(ual.type = 3)';
                $huddle_resources_uploaded = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual join account_folders af ON ual.account_folder_id = af.account_folder_id WHERE af.folder_type = 1 AND ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                $huddle_resources_uploaded = json_decode(json_encode((array) $huddle_resources_uploaded), true);
                $all_users = array_merge($huddle_resources_uploaded, $all_users);

            }
                
           if ($type_in['key'] == 'workspace_resources_uploaded')
            {
                if(!in_array('Workspace',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace'; 
                }
                if(!in_array('Resource Name',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['resource-name']) ? $translations['resource-name'] : 'Resource Name'; 
                }
                $types = '(ual.type = 3)';
                $workspace_resources_uploaded = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual join account_folders af ON ual.account_folder_id = af.account_folder_id WHERE af.folder_type = 3 AND ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                $workspace_resources_uploaded = json_decode(json_encode((array) $workspace_resources_uploaded), true);
                $all_users = array_merge($workspace_resources_uploaded, $all_users);

            }
            
           if ($type_in['key'] == 'huddle_resources_viewed')
            {
                if(!in_array('Huddles',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                }
                if(!in_array('Resource Name',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['resource-name']) ? $translations['resource-name'] : 'Resource Name'; 
                }
                $types = '(ual.type = 13)';
                $huddle_resources_viewed = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual join account_folders af ON ual.account_folder_id = af.account_folder_id WHERE af.folder_type = 1 AND ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                $huddle_resources_viewed = json_decode(json_encode((array) $huddle_resources_viewed), true);
                $all_users = array_merge($huddle_resources_viewed, $all_users);

            }
           if ($type_in['key'] == 'workspace_resources_viewed')
            {
                if(!in_array('Workspace',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace'; 
                }
                if(!in_array('Resource Name',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['resource-name']) ? $translations['resource-name'] : 'Resource Name'; 
                }
                $types = '(ual.type = 13)';
                $workspace_resources_viewed = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual join account_folders af ON ual.account_folder_id = af.account_folder_id WHERE af.folder_type = 3 AND ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                $workspace_resources_viewed = json_decode(json_encode((array) $workspace_resources_viewed), true);
                $all_users = array_merge($workspace_resources_viewed, $all_users);

            }
            
            if ($type_in['key'] == 'session_summary')
            {
                if(!in_array('Huddles',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                }
                if(!in_array('Comment',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['comment']) ? $translations['comment'] : 'Comment'; 
                }
                $summaries = Comment::join('account_folder_documents as afd', 'afd.document_id', '=', 'Comment.ref_id')->join('account_folders as af', 'af.account_folder_id', '=', 'afd.account_folder_id')->whereIn('ref_id',$huddle_document_ids['document_ids'])->where(array('ref_type' => '4','user_id' => $user_id))->whereIn('af.account_folder_id',$huddle_document_ids['huddle_ids'])->get()->toArray();
                $final_summary_array = [];
                foreach ($summaries as $summary_index => $summary)
                {
                  
                    $final_summary_array[$summary_index]['comment'] = $summary['comment'];
                    $final_summary_array[$summary_index]['video'] = isset($summary['title']) ? $summary['title'] : '';
                    $final_summary_array[$summary_index]['huddles'] = isset($summary['name']) ? $summary['name'] : '';
                    $final_summary_array[$summary_index]['date_added'] = isset($summary['created_date']) ? $summary['created_date'] : '';
                    $final_summary_array[$summary_index]['type'] =  '';
                    $final_summary_array[$summary_index]['action_type'] =  'Session Summary';
                }
                $all_users = array_merge($final_summary_array, $all_users);

            }
            
            if ($type_in['key'] == 'live_sessions')
            {
                if(!in_array('Huddles',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                }
                if(!in_array('Video Name',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name'; 
                }
                $types = '(ual.type = 24)';
                $live_sessions = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual join account_folders af ON ual.account_folder_id = af.account_folder_id WHERE af.folder_type = 1 AND ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                $live_sessions = json_decode(json_encode((array) $live_sessions), true);
                $array_count = 0;
                $live_sessions_new = array();
                foreach ($live_sessions as $user) {
                        $live_sessions_new[] = $user;
                        $live_sessions_new[$array_count]['type'] = 'live_sessions';
                        $array_count++;
                }
                $all_users = array_merge($live_sessions_new, $all_users);
            }
            
           if ($type_in['key'] == 'scripted_notes_shared_to_huddles') {
                $types = '(ual.type = 26)';
                $scripted_notes_shared_to_huddles = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id`  FROM user_activity_logs ual WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . $huddles_check . " AND " . $types . $mAUCndSLog . $mAUCndELog . "");
                $scripted_notes_shared_to_huddles_new = array();
                $scripted_notes_shared_to_huddles = json_decode(json_encode((array) $scripted_notes_shared_to_huddles), true);
                $array_count = 0;
                    foreach ($scripted_notes_shared_to_huddles as $user) {
                        $scripted_notes_shared_to_huddles_new[] = $user;
                        $scripted_notes_shared_to_huddles_new[$array_count]['type'] = 26;
                        $array_count++;
                    }
                $all_users = array_merge($scripted_notes_shared_to_huddles_new, $all_users);

            }
            
            $type_string = [];
            if($type_in['key'] == 'huddles_created')
            {
                if(!in_array('Huddles',$header_data))
                {
                   $header_data[count($header_data)] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles'; 
                }
                $type_string[]  = 1;
            }
            
            if($type_in['key'] == 'total_logins')
            {
                $types = '(ual.type = 9)';
                $total_logins = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . " AND ". $types . $mAUCndSLog . $mAUCndELog . "");
                $total_logins = json_decode(json_encode((array) $total_logins), true);
                $all_users = array_merge($total_logins, $all_users);
            }
            
            $type_string = implode(',', $type_string);
            if (!empty($type_string)) {
                $rest_of_them = App('db')->select("SELECT ual.`desc`,ual.`url`,ual.`date_added`,ual.`account_folder_id`,ual.`ref_id` ,ual.`type` FROM user_activity_logs ual WHERE ual.site_id = " . $this->site_id . " AND ual.account_id = " . $account_id . " AND ual.user_id = " . $user_id . $huddles_check . " AND ual.type IN (" . $type_string . ") " . $mAUCndSLog . $mAUCndELog . "");
                $rest_of_them = json_decode(json_encode((array) $rest_of_them), true);
                $all_users = array_merge($rest_of_them, $all_users);
            }
            
            
        }
        $all_user_detail = array();
        foreach ($all_users as $allusr) {
                if ($allusr['type'] == 9) {
                    $allusr['action_type'] = $translations["analytics_logged_in"];
                    $allusr['url'] = '';
                    $allusr['desc'] = '';
                    $allusr['video'] = '';
                    $allusr['coach'] = '';
                    $allusr['coachee'] = '';
                    $allusr['session_date'] = '';
                }
                if ($allusr['type'] == 1) {
                    $allusr['action_type'] = $translations["analytics_huddle_created"];
                    $doc_info = App('db')->select("SELECT af.* FROM account_folders af WHERE af.site_id = " . $this->site_id . " AND af.account_folder_id = " . $allusr['ref_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['url'] = '';
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['desc'] = '';
                            $allusr['video'] = '';
                            $allusr['session_date'] = '';
                        }
                        if ($doc_info[0]['folder_type'] == 2) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                            $allusr['video'] = '';
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = '';
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['video'] = '';
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = '';
                        }
                    } else {
                        $allusr['url'] = '';
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 2) {
                    $allusr['action_type'] = isset($translations['video-uploaded']) ? $translations['video-uploaded'] : 'Videos Uploaded';
                    $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND afd.document_id = " . $allusr['ref_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            //$allusr['coach'] = $coach;
                            //$allusr['coachee'] = $coachee;
                            $allusr['url'] = '';
                            $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['desc'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                        if ($doc_info[0]['folder_type'] == 2) {
                            $allusr['url'] = '';
                            $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                            //$allusr['coach'] = '';
                            //$allusr['coachee'] = '';
                            $allusr['huddles'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            //$allusr['coach'] = '';
                            //$allusr['coachee'] = '';
                            $allusr['huddles'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                    } else {
                        $allusr['url'] = '';
                        $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                        $allusr['desc'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                        $allusr['huddles'] = '';
                    }
                }
                if ($allusr['type'] == 4) {
                    $allusr['action_type'] = $translations["analytics_video_uploaded"];
                    $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND afd.account_folder_id = " . $allusr['account_folder_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['url'] = '';
                            $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['desc'] = $doc_info[0]['name'];
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                        if ($doc_info[0]['folder_type'] == 2) {
                            $allusr['url'] = '';
                            $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                           // $allusr['coach'] = '';
                           // $allusr['coachee'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['url'] = '';
                            $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                    } else {
                        $allusr['url'] = '';
                        $allusr['video'] = '';
                        $allusr['desc'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 5) {
                    $allusr['action_type'] = $translations["analytics_comment_added"];
                    $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date as session_date FROM comments c LEFT JOIN account_folder_documents afd ON c.ref_id = afd.document_id LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE af.site_id = " . $this->site_id . " AND af.account_folder_id =" . $allusr['account_folder_id']);
                    /* $doc_info = App('db')->select("SELECT af.* FROM account_folders af WHERE af.account_folder_id = ".$allusr['account_folder_id']); */
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['url'] = $allusr['comment'];
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['desc'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                        if ($doc_info[0]['folder_type'] == 2) {
                            $allusr['url'] = $allusr['comment'];
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                    } else {
                        $allusr['url'] = '';
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 11) {
                    $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND afd.document_id = " . $allusr['ref_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    $allusr['action_type'] = $translations["analytics_video_viewed"];
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                        if ($doc_info[0]['folder_type'] == 2) {
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                            $allusr['url'] = '';
                            $allusr['video'] = $doc_info[0]['name'];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['url'] = '';
                            $allusr['video'] = $doc_info[0]['name'];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                    } else {
                        $allusr['url'] = '';
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 22) {
                    $allusr['action_type'] = $translations["analytics_video_shared"];
                    $allusr['url'] = isset($allusr['desc']) ? $allusr['desc'] : '';

                    $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND afd.account_folder_id = " . $allusr['account_folder_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND  afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if ($invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['coach'] = $coach;
//                            $allusr['coachee'] = $coachee;
//                            $allusr['url'] = '';
//                            $allusr['desc'] = $doc_info[0]['name'];
//                            $allusr['video'] = $doc_info[0]['title'];
//                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));

                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['url'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['desc'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }

                        else if ($doc_info[0]['folder_type'] == 2) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                            $allusr['coach'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }

                        else
                        {
                            $allusr['action_type'] = $translations["analytics_video_shared"];
                            $allusr['url'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['desc'] = '';
                            $allusr['video'] = '';
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = '';
                        }

                    } else {
                            $allusr['action_type'] = $translations["analytics_video_shared"];
                            $allusr['url'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['desc'] = '';
                            $allusr['video'] = '';
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 8) {
                    $allusr['action_type'] = $translations["analytics_reply_to_comment"];
                    /* $doc_info = App('db')->select("SELECT af.* FROM account_folders af WHERE af.account_folder_id = ".$allusr['account_folder_id']); */
                    //$doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date as session_date FROM comments c LEFT JOIN account_folder_documents afd ON c.ref_id = afd.document_id LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE af.account_folder_id =" . $allusr['account_folder_id']);

                    $comment_details = Comment::where(array(
                                "id" => $allusr['ref_id'],
                                "site_id" => $this->site_id
                            ))->get()->toArray();
                    $account_folder_id_data = AccountFolderDocument::where(array(
                                "document_id" => $comment_details[0]['ref_id'],
                                "site_id" => $this->site_id
                            ))->get()->toArray();


                    if (!empty($account_folder_id_data[0]['account_folder_id'])) {
                        $doc_info = App('db')->select("SELECT af.* FROM account_folders af WHERE af.site_id = " . $this->site_id . " AND af.account_folder_id = " . $account_folder_id_data[0]['account_folder_id']);
                    } else {
                        $doc_info = '';
                    }
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if (isset($doc_info[0]['folder_type']) && $doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['comment'] = $allusr['comment'];
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['desc'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                        if (isset($doc_info[0]['folder_type']) && $doc_info[0]['folder_type'] == 2) {
                            $allusr['url'] = $allusr['comment'];
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                        if (isset($doc_info[0]['folder_type']) && $doc_info[0]['folder_type'] == 3) {
                            $allusr['comment'] = $allusr['comment'];
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                    } else {
                        $allusr['url'] = $allusr['comment'];
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 6) {
                    $allusr['action_type'] = isset($translations['huddle-discussion-created']) ? $translations['huddle-discussion-created'] : 'Huddle Discussion Created';
                    $doc_info = App('db')->select("SELECT * FROM account_folders af WHERE af.site_id = " . $this->site_id . " AND  af.account_folder_id =" . $allusr['account_folder_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    /* $doc_info = App('db')->select("SELECT af.* FROM account_folders af WHERE af.account_folder_id = ".$allusr['account_folder_id']); */


                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND  meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND  afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if ($invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if ($invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['url'] = strip_tags($allusr['desc']);
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['desc'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                    } else {
                        $allusr['url'] = '';
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 3) {
                    $doc_info = App('db')->select("SELECT af.*,afd.title FROM account_folder_documents afd LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND af.folder_type IN (1,2,3) AND afd.document_id = " . $allusr['ref_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    $allusr['action_type'] = $translations["analytics_document_uploaded"];
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['desc'] = '';
                            $allusr['url'] = '';
                            $allusr['session_date'] = '';
                        }
                        if ($doc_info[0]['folder_type'] == 2) {
                            $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                            $allusr['url'] = '';
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = '';
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['url'] = '';
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = '';
                        }
                    } else {
                        continue;
                        $allusr['url'] = '';
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 13) {
                    $allusr['action_type'] = $translations["analytics_document_viewed"];
                    $doc_info = App('db')->select("SELECT af.*,afd.title FROM account_folder_documents afd LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND af.folder_type IN (1,2,3) AND afd.document_id = " . $allusr['ref_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['desc'] = '';
                            $allusr['url'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = '';
                        }
                        if ($doc_info[0]['folder_type'] == 2) {
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                            $allusr['url'] = '';
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = '';
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['url'] = '';
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = '';
                        }
                    } else {
                        $doc_info = App('db')->select("SELECT af.*,d.original_file_name FROM comment_attachments ca LEFT JOIN comments c ON ca.comment_id = c.id LEFT JOIN documents d ON ca.document_id = d.id LEFT JOIN account_folders af ON c.ref_id = af.account_folder_id WHERE ca.site_id = " . $this->site_id . " AND ca.document_id = " . $allusr['ref_id']);
                        $doc_info = json_decode(json_encode((array) $doc_info), true);
                        if (isset($doc_info[0])) {
                            if ($doc_info[0]['folder_type'] == 1) {
                                $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                                $coach_arr = array();
                                $coachee_arr = array();
                                $coach = '';
                                $coachee = '';
                                if (isset($huddle_info[0])) {
                                    $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                    $usr_info = json_decode(json_encode((array) $usr_info), true);
                                    foreach ($usr_info as $invitedUsers) {
                                        if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                            $coach_arr[] = $invitedUsers['name'];
                                        }
                                        if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                            $coachee_arr[] = $invitedUsers['name'];
                                        }
                                    }
                                    $coach = implode(',', $coach_arr);
                                    $coachee = implode(',', $coachee_arr);
                                }
                                $allusr['coach'] = $coach;
                                $allusr['coachee'] = $coachee;
                                $allusr['video'] = $doc_info[0]['original_file_name'];
                                $allusr['desc'] = $doc_info[0]['name'];
                                $allusr['url'] = '';
                                $allusr['session_date'] = '';
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }
                }
                if ($allusr['type'] == 24) {
                    $allusr['action_type'] = $translations["analytics_video_hours_uploaded"];
                    // $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ref_id']);
                    //  $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ref_id']);
                    $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND afd.document_id = " . $allusr['ref_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['coach'] = $coach;
//                            $allusr['coachee'] = $coachee;
//                            $allusr['url'] = '';
//                            $allusr['desc'] = $doc_info[0]['name'];
//                            $allusr['video'] = $doc_info[0]['title'];
//                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));

                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['url'] = '';
                            $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['desc'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['duration'] = isset($allusr['duration']) ? $allusr['duration'] : 0;
                            $allusr['minutes'] = round($allusr['duration'] / 60, 2);
                        }
                        if ($doc_info[0]['folder_type'] == 2) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['duration'] = isset($allusr['duration']) ? $allusr['duration'] : 0;
                            $allusr['minutes'] = round($allusr['duration'] / 60, 2);
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['duration'] = isset($allusr['duration']) ? $allusr['duration'] : 0;
                            $allusr['minutes'] = round($allusr['duration'] / 60, 2);
                        }
                    } else {
                        $allusr['url'] = '';
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 25) {
                    $allusr['action_type'] = $translations["analytics_video_hours_viewed"];
                    // $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ref_id']);
                    //  $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ref_id']);
                    if(isset($allusr['document_id'])){
                        $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND afd.document_id = " . $allusr['document_id']);
                        $doc_info = json_decode(json_encode((array) $doc_info), true);
                    }
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['coach'] = $coach;
//                            $allusr['coachee'] = $coachee;
//                            $allusr['url'] = '';
//                            $allusr['desc'] = $doc_info[0]['name'];
//                            $allusr['video'] = $doc_info[0]['title'];
//                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['date_added'] = @date('Y-m-d h:i A', strtotime($allusr['created_date']));
                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['url'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['desc'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['minutes'] = round($allusr['minutes_watched'] / 60, 2);
                        }
                        if ($doc_info[0]['folder_type'] == 2) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['date_added'] = @date('Y-m-d h:i A', strtotime($allusr['created_date']));
                            $allusr['minutes'] = round($allusr['minutes_watched'] / 60, 2);
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['date_added'] = @date('Y-m-d h:i A', strtotime($allusr['created_date']));
                            $allusr['minutes'] = round($allusr['minutes_watched'] / 60, 2);
                        }
                    } else {
                        $allusr['url'] = '';
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 23) {


                    $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND afd.document_id = " . $allusr['ref_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 3) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);

                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }


                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['action_type'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['desc'] = '';


                            $allusr['url'] = $allusr['url'];
                            $allusr['video'] = '';
                            $allusr['session_date'] = '';
                        }
                    }
                }
                if ($allusr['type'] == 26) {

                    $allusr['action_type'] = isset($translations['scripted-note-copied']) ? $translations['scripted-note-copied'] :'Scripted Note Copied';
                    $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND afd.document_id = " . $allusr['ref_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);

                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }


                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                           // $allusr['action_type'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['desc'] = $doc_info[0]['name'];


                            $allusr['url'] = $allusr['url'];
                            $allusr['video'] = '';
                            $allusr['session_date'] = '';
                        }
                    }
                }
                if ($allusr['type'] == 'live_sessions') {
                    $allusr['action_type'] = isset($translations['live-sessions']) ? $translations['live-sessions'] :'Live Sessions';
                    // $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ref_id']);
                    //  $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ref_id']);
                    $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND afd.document_id = " . $allusr['ref_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['coach'] = $coach;
//                            $allusr['coachee'] = $coachee;
//                            $allusr['url'] = '';
//                            $allusr['desc'] = $doc_info[0]['name'];
//                            $allusr['video'] = $doc_info[0]['title'];
//                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));

                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['url'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['desc'] = $doc_info[0]['name'];
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                        if ($doc_info[0]['folder_type'] == 2) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['duration'] = isset($allusr['duration']) ? $allusr['duration'] : 0;
                            $allusr['minutes'] = round($allusr['duration'] / 60, 2);
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['duration'] = isset($allusr['duration']) ? $allusr['duration'] : 0;
                            $allusr['minutes'] = round($allusr['duration'] / 60, 2);
                        }
                    } else {
                        $allusr['url'] = '';
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 'live_hours_uploaded') {
                    $allusr['action_type'] = isset($translations['live-video-hours-uploaded']) ? $translations['live-video-hours-uploaded'] :'Live Video Hours Uploaded';
                    // $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ref_id']);
                    //  $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ref_id']);
                    $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND afd.document_id = " . $allusr['ref_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['coach'] = $coach;
//                            $allusr['coachee'] = $coachee;
//                            $allusr['url'] = '';
//                            $allusr['desc'] = $doc_info[0]['name'];
//                            $allusr['video'] = $doc_info[0]['title'];
//                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));

                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['url'] = '';
                            $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['desc'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['duration'] = isset($allusr['duration']) ? $allusr['duration'] : 0;
                            $allusr['minutes'] = round($allusr['duration'] / 60, 2);
                        }
                        if ($doc_info[0]['folder_type'] == 2) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['duration'] = isset($allusr['duration']) ? $allusr['duration'] : 0;
                            $allusr['minutes'] = round($allusr['duration'] / 60, 2);
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['duration'] = isset($allusr['duration']) ? $allusr['duration'] : 0;
                            $allusr['minutes'] = round($allusr['duration'] / 60, 2);
                        }
                    } else {
                        $allusr['url'] = '';
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 'live_hours_viewed') {
                    $allusr['action_type'] = isset($translations['live-video-hours-viewed']) ? $translations['live-video-hours-viewed'] :'Live Video Hours Viewed';
                    // $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ref_id']);
                    //  $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ref_id']);
                    if(isset($allusr['document_id'])){
                        $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND afd.document_id = " . $allusr['document_id']);
                        $doc_info = json_decode(json_encode((array) $doc_info), true);
                    }
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['coach'] = $coach;
//                            $allusr['coachee'] = $coachee;
//                            $allusr['url'] = '';
//                            $allusr['desc'] = $doc_info[0]['name'];
//                            $allusr['video'] = $doc_info[0]['title'];
//                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['date_added'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['url'] = '';
                            $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['desc'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['minutes'] = round($allusr['minutes_watched'] / 60, 2);
                        }
                        if ($doc_info[0]['folder_type'] == 2) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['date_added'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['minutes'] = round($allusr['minutes_watched'] / 60, 2);
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['date_added'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                            $allusr['minutes'] = round($allusr['minutes_watched'] / 60, 2);
                        }
                    } else {
                        $allusr['url'] = '';
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 'standard') {
                    $allusr['action_type'] = isset($translations['framework-tag']) ? $translations['framework-tag'] :'Framework Tag';
                    // $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ref_id']);
                    //  $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ref_id']);
                    $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND afd.document_id = " . $allusr['ref_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['coach'] = $coach;
//                            $allusr['coachee'] = $coachee;
//                            $allusr['url'] = '';
//                            $allusr['desc'] = $doc_info[0]['name'];
//                            $allusr['video'] = $doc_info[0]['title'];
//                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));

                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['url'] = '';
                            $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['url'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                    } else {
                        $allusr['url'] = '';
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 'markers') {
                    $allusr['action_type'] = isset($translations['custom-marker']) ? $translations['custom-marker'] :'Custom Marker';
                    // $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ref_id']);
                    //  $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ref_id']);
                    $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND afd.document_id = " . $allusr['ref_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['coach'] = $coach;
//                            $allusr['coachee'] = $coachee;
//                            $allusr['url'] = '';
//                            $allusr['desc'] = $doc_info[0]['name'];
//                            $allusr['video'] = $doc_info[0]['title'];
//                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));

                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['url'] = '';
                            $allusr['desc'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['url'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['desc'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                    } else {
                        $allusr['url'] = '';
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 'performance') {
                    $allusr['action_type'] = isset($translations['performance-level-ratings']) ? $translations['performance-level-ratings'] :'Performance Level Ratings';
                    // $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date as session_date FROM account_folder_documents afd LEFT JOIN documents d ON afd.document_id = d.id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.document_id = " . $allusr['ref_id']);
                    //  $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.account_folder_id = " . $allusr['ref_id']);
                    $doc_info = App('db')->select("SELECT af.*,afd.title,d.created_date AS session_date FROM documents d LEFT JOIN account_folder_documents afd ON d.id = afd.document_id LEFT JOIN account_folders af ON afd.account_folder_id = af.account_folder_id WHERE afd.site_id = " . $this->site_id . " AND afd.document_id = " . $allusr['document_id']);
                    $doc_info = json_decode(json_encode((array) $doc_info), true);
                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $huddle_info = App('db')->select("SELECT * FROM account_folders_meta_data WHERE site_id = " . $this->site_id . " AND meta_data_name = 'folder_type' AND account_folder_id = " . $doc_info[0]['account_folder_id']);
                            $huddle_info = json_decode(json_encode((array) $huddle_info), true);
                            $coach_arr = array();
                            $coachee_arr = array();
                            $coach = '';
                            $coachee = '';
                            if (isset($huddle_info[0])) {
                                $usr_info = App('db')->select("SELECT CONCAT(u.first_name,' ',u.last_name) AS name,afu.role_id FROM users u LEFT JOIN account_folder_users afu ON u.id = afu.user_id WHERE afu.site_id = " . $this->site_id . " AND afu.account_folder_id = " . $doc_info[0]['account_folder_id']);
                                $usr_info = json_decode(json_encode((array) $usr_info), true);
                                foreach ($usr_info as $invitedUsers) {
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 200) {
                                        $coach_arr[] = $invitedUsers['name'];
                                    }
                                    if (isset($invitedUsers['role_id']) && $invitedUsers['role_id'] == 210) {
                                        $coachee_arr[] = $invitedUsers['name'];
                                    }
                                }
                                $coach = implode(',', $coach_arr);
                                $coachee = implode(',', $coachee_arr);
                            }
//                            $allusr['coach'] = $coach;
//                            $allusr['coachee'] = $coachee;
//                            $allusr['url'] = '';
//                            $allusr['desc'] = $doc_info[0]['name'];
//                            $allusr['video'] = $doc_info[0]['title'];
//                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));

                            $allusr['coach'] = $coach;
                            $allusr['coachee'] = $coachee;
                            $allusr['url'] = '';
                            $allusr['video'] = isset($allusr['desc']) ? $allusr['desc'] : '';
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['desc'] = '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['url'] = '';
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['video'] = isset($doc_info[0]['title']) ? $doc_info[0]['title'] : '';
                            $allusr['session_date'] = @date('Y-m-d h:i A', strtotime($doc_info[0]['session_date']));
                        }
                    } else {
                        $allusr['url'] = '';
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
                if ($allusr['type'] == 'huddle_discussions_posts') {

                    $allusr['action_type'] = isset($translations['huddle-discussion-posts']) ? $translations['huddle-discussion-posts'] :'Huddle Discussion Posts';


                        $doc_info = App('db')->select("SELECT af.* FROM account_folders af WHERE af.site_id = " . $this->site_id . " AND  af.account_folder_id = " . $allusr['account_folder_id']);
                        $doc_info = json_decode(json_encode((array) $doc_info), true);



                    if (isset($doc_info[0])) {
                        if ($doc_info[0]['folder_type'] == 1) {
                            $allusr['comment'] = strip_tags($allusr['comment']);
                            $allusr['huddles'] = $doc_info[0]['name'];
                            $allusr['video'] = '';
                            $allusr['coach'] = '';
                            $allusr['desc'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = '';
                        }
                        if ($doc_info[0]['folder_type'] == 2) {
                            $allusr['comment'] = $allusr['comment'];
                            $allusr['desc'] = '';
                            $allusr['library'] = $translations["analytics_video_library"];
                            $allusr['video'] = '';
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = '';
                        }
                        if ($doc_info[0]['folder_type'] == 3) {
                            $allusr['comment'] = $allusr['comment'];
                            $allusr['desc'] = '';
                            $allusr['workspace'] = $translations["analytics_my_workspace"];
                            $allusr['video'] = '';
                            $allusr['coach'] = '';
                            $allusr['coachee'] = '';
                            $allusr['session_date'] = '';
                        }
                    } else {
                        $allusr['url'] = strip_tags($allusr['comment']);
                        $allusr['desc'] = '';
                        $allusr['video'] = '';
                        $allusr['coach'] = '';
                        $allusr['coachee'] = '';
                        $allusr['session_date'] = '';
                    }
                }
           $allusr['username'] = $curr_user['first_name'] . ' ' . $curr_user['last_name'];
           $allusr['date_added'] = @date('Y-m-d h:i A', strtotime($allusr['date_added']));  
           if(isset($allusr['comment'])){
               $allusr['comment'] = strip_tags($allusr['comment']);
           }   
           $all_user_detail[] = $allusr;     
        }
        $final_header = [];
        foreach($header_data as $index => $header)
        {
            if($header == 'Action')
            {
                $final_header[$index]['header_name'] = isset($translations['action']) ? $translations['action'] : 'Action';
                $final_header[$index]['get_header_value'] = 'action_type';
            }
            if($header == 'User Name')
            {
                $final_header[$index]['header_name'] = isset($translations['user-name']) ? $translations['user-name'] : 'User Name';
                $final_header[$index]['get_header_value'] = 'username';
            }
            if($header == 'Activity Date')
            {
                $final_header[$index]['header_name'] = isset($translations['activity-date']) ? $translations['activity-date'] : 'Activity Date';
                $final_header[$index]['get_header_value'] = 'date_added';
            }
            if($header == 'Huddles')
            {
                $final_header[$index]['header_name'] = isset($translations['huddles']) ? $translations['huddles'] : 'Huddles';
                $final_header[$index]['get_header_value'] = 'huddles';
            }
            if($header == 'Workspace')
            {
                $final_header[$index]['header_name'] = isset($translations['workspace']) ? $translations['workspace'] : 'Workspace';
                $final_header[$index]['get_header_value'] = 'workspace';
            }
            if($header == 'Library')
            {
                $final_header[$index]['header_name'] = isset($translations['library']) ? $translations['library'] : 'Library';
                $final_header[$index]['get_header_value'] = 'library';
            }
            if($header == 'Description')
            {
                $final_header[$index]['header_name'] = isset($translations['description']) ? $translations['description'] : 'Description';
                $final_header[$index]['get_header_value'] = 'desc';
            }
            if($header == 'Video Name')
            {
                $final_header[$index]['header_name'] = isset($translations['video-name']) ? $translations['video-name'] : 'Video Name';
                $final_header[$index]['get_header_value'] = 'video';
            }
            if($header == 'Resource Name')
            {
                $final_header[$index]['header_name'] = isset($translations['resource-name']) ? $translations['resource-name'] : 'Resource Name';
                $final_header[$index]['get_header_value'] = 'video';
            }
            if($header == 'Minutes')
            {
                $final_header[$index]['header_name'] = isset($translations['minutes']) ? $translations['minutes'] : 'Minutes';
                $final_header[$index]['get_header_value'] = 'minutes';
            }
            if($header == 'Comment')
            {
                $final_header[$index]['header_name'] = isset($translations['comment']) ? $translations['comment'] : 'Comment';
                $final_header[$index]['get_header_value'] = 'comment';
            }
            if($header == 'Rating')
            {
                $final_header[$index]['header_name'] = isset($translations['rating']) ? $translations['rating'] : 'Rating';
                $final_header[$index]['get_header_value'] = 'rating';
            }
            if($header == 'Custom Marker Name')
            {
                $final_header[$index]['header_name'] = 'Custom Marker Name';
                $final_header[$index]['get_header_value'] = 'desc';
            }
            
            
        }
        
        $result = array();
        $result['header'] = $final_header;
        $result['activity_detail'] = $all_user_detail;
        $result['dropdown_data'] = $dropdown_data;
        return response()->json($result);
        
    }
    function detail_view_csv(Request $request){
        $response = $this->detail_view($request);
        $response = $response->getData();
        $account_id = $request->account_id;
        $detail_view = json_decode(json_encode($response),true);
        $count = 0;
        $result = array();
        $count1 = 0;
        $data = array();
        $header_value = array();
        $count2 = 0;
        $header_data_array =  array();
        $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
        $account_name = implode('_',$account_name);
        foreach($detail_view['header'] as $row)
        {
            $header_data_array[$count1] = $row['header_name'];
            $count1++;
            $header_value[$count2] = $row['get_header_value'];
            $count2++;
        }
        foreach($detail_view['activity_detail'] as $row)
        {
                foreach($row as $key => $val){
                    if(!in_array($key,$header_value)){
                    unset($row[$key]);
                    }
                }
                for($i = 0; $i<sizeof($header_value);$i++){
                foreach($row as $key => $val){
                    if($header_value[$i] == $key){
                        $row[$header_value[$i]] = $row[$key];
                        $data[] = ($row[$header_value[$i]]);
                    }
                }
                
            }  
                $result[$count] = $data;
                $data = []; 
                $count++;
        }
            // print_r($row[$header_value[$i]]);
        $exportDetailViewToCSV = new \App\Services\Exports\DetailView\ExportToCSV;
        return $exportDetailViewToCSV->export($header_data_array,$result,$account_name);
    }

    function detail_view_excel(Request $request){
        $response = $this->detail_view($request);
        $response = $response->getData();
        $account_id = $request->account_id;
        $detail_view = json_decode(json_encode($response),true);
        $count = 0;
        $result = array();
        $count1 = 0;
        $data = array();
        $header_value = array();
        $count2 = 0;
        $header_data_array =  array();
        $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
        $account_name = implode('_',$account_name);
        foreach($detail_view['header'] as $row)
        {
            $header_data_array[$count1] = $row['header_name'];
            $count1++;
            $header_value[$count2] = $row['get_header_value'];
            $count2++;
        }
        foreach($detail_view['activity_detail'] as $row)
        {
                foreach($row as $key => $val){
                    if(!in_array($key,$header_value)){
                    unset($row[$key]);
                    }
                }
                for($i = 0; $i<sizeof($header_value);$i++){
                foreach($row as $key => $val){
                    if($header_value[$i] == $key){
                        $row[$header_value[$i]] = $row[$key];
                        $data[] = ($row[$header_value[$i]]);
                    }
                }
                
            }  
                $result[$count] = $data;
                $data = []; 
                $count++;
        }
        $exportDetailViewToExcel = new \App\Services\Exports\DetailView\ExportToExcel;
        return $exportDetailViewToExcel->export($header_data_array,$result,$account_name);
    }

    function detail_view_pdf(Request $request){
        $response = $this->detail_view($request);
        $response = $response->getData();
        $account_id = $request->account_id;
        $detail_view = json_decode(json_encode($response),true);
        //dd($detail_view);
        
        $data = $detail_view['activity_detail'];
        
        $header_data_array =  array();
        $account_name = Account::where('id',$account_id)->pluck('company_name')->toArray();
        $account_name = implode('_',$account_name);

        foreach($detail_view['header'] as  $row)
        {
            $header_data_array[$row['get_header_value']] = $row['header_name'];
        
        }
       
        $account_name = $account_name . "_detail_view_";
        $this->export_pdf($header_data_array,$data,$account_name);

    }
    
    function get_huddle_document_ids($account_id,$user_id,$coach_id,$report_type,$huddle_type,$huddle_id)
    {
        if (! is_array($huddle_type)){
            $huddle_type = explode(",",$huddle_type); 
        }
        if($report_type == 'coaching_report')
        {
                $account_coach_huddles = AccountFolder::select('huddle_users.account_folder_id', 'huddle_users.user_id')
                    ->leftJoin('account_folders_meta_data as afmd','AccountFolder.account_folder_id','=','afmd.account_folder_id')
                    ->leftJoin('account_folder_users as huddle_users','AccountFolder.account_folder_id','=','huddle_users.account_folder_id')
                    ->leftJoin('users as User','User.id','=','huddle_users.user_id')
                    ->where(array(
                        'AccountFolder.folder_type' => 1,
                        'afmd.meta_data_name' => 'folder_type',
                        'afmd.meta_data_value' => 2,
                        'huddle_users.role_id' => 200,
                        'huddle_users.user_id' => $coach_id
                    ))
                    ->whereIn('AccountFolder.account_id',array($account_id))
                    ->where('AccountFolder.is_sample', '<>','1')
                    ->whereRaw('AccountFolder.active = IF((AccountFolder.`archive`=1)AND(AccountFolder.`active`=0),0,1)')
                    ->groupBy('huddle_users.account_folder_id')
                    ->get()->toArray();
                $coach_hud = array();
                foreach ($account_coach_huddles as $coach_huddles) {
                    $coach_hud[] = $coach_huddles['account_folder_id'];
                }
                
                    if($coach_id == $user_id)
                    {
                        $documents_ids = AccountFolderDocument::select('document_id')->whereIn('account_folder_id',$coach_hud)->get()->toArray();
                   
                    }
                    else 
                    {
                          $documents_ids = Document::select('documents.id as document_id')
                              ->leftJoin('account_folder_documents as afd','documents.id','=','afd.document_id')
                              ->leftJoin('account_folder_users as afu','afd.account_folder_id','=','afu.account_folder_id')
                              ->whereIn('afd.account_folder_id',$coach_hud)
                              ->whereIn('documents.doc_type' , array(1, 3) )
                              ->where('afu.user_id' , $user_id)
                              ->where('afu.role_id',210)
                              ->groupBy('documents.id')->get()->toArray();
 
                    }
                    $final_document_ids = array();
                    foreach($documents_ids as $document_id)
                    {
                        $final_document_ids[] = $document_id['document_id'];
                    }
                $result = array();
                $result['huddle_ids'] = $coach_hud;
                $result['document_ids'] = $final_document_ids;
                return $result;
        }
        
        if($report_type == 'user_summary_report')
        {
            $huddle_ids_arr_type_1 = [];
            $huddle_ids_arr_type_2 = [];
            $huddle_ids_arr_type_3 = [];
            $huddle_ids_arr_type_4 = [];
            $huddle_ids_arr_type_5 = [];
            $huddle_ids_arr_type_6 = [];
            $merged_array = [];
             if(in_array('7',$huddle_type))
            {
                $condition_array = array('afmd.meta_data_name' => 'folder_type'
                    );
                $folder_type = [1,2,3,5];
                $account_folder_type = [1,2,3];
            }
            if(in_array('6',$huddle_type))
            {
                 $condition_array = array('AccountFolder.created_by' => $user_id
                    );
                $folder_type = [5];
                $account_folder_type = [3];
                $huddle_ids_arr_type_6 = $this->get_huddle_ids_query($account_id,$account_folder_type,$condition_array,$folder_type,true);
                $merged_array = array_merge($huddle_ids_arr_type_6, $merged_array);
            }
            if(in_array('5',$huddle_type))
            {
                $condition_array = array('afmd.meta_data_name' => 'folder_type','huddle_users.user_id' => $user_id
                    );
                $folder_type = [1];
                $account_folder_type = [1];
                $huddle_ids_arr_type_1 = $this->get_huddle_ids_query($account_id,$account_folder_type,$condition_array,$folder_type,false);
                $merged_array = array_merge($huddle_ids_arr_type_1, $merged_array);
            }
            if(in_array('1',$huddle_type))
            {
                $condition_array = array('afmd.meta_data_name' => 'folder_type','huddle_users.user_id' => $user_id,'huddle_users.role_id' => 200
                    );
                $folder_type = [2];
                $account_folder_type = [1];
                $huddle_ids_arr_type_2 = $this->get_huddle_ids_query($account_id,$account_folder_type,$condition_array,$folder_type,false);
                $merged_array = array_merge($huddle_ids_arr_type_2, $merged_array);
            }
            if(in_array('2',$huddle_type))
            {
                $condition_array = array('afmd.meta_data_name' => 'folder_type','huddle_users.user_id' => $user_id,'huddle_users.role_id' => 210
                    );
                $folder_type = [2];
                $account_folder_type = [1];
                $huddle_ids_arr_type_3 = $this->get_huddle_ids_query($account_id,$account_folder_type,$condition_array,$folder_type,false);
                $merged_array = array_merge($huddle_ids_arr_type_3, $merged_array);
            }
            if(in_array('3',$huddle_type))
            {
                $condition_array = array('afmd.meta_data_name' => 'folder_type','huddle_users.user_id' => $user_id,'huddle_users.role_id' => 200
                    );
                $folder_type = [3];
                $account_folder_type = [1];
                $huddle_ids_arr_type_4 = $this->get_huddle_ids_query($account_id,$account_folder_type,$condition_array,$folder_type,false);
                $merged_array = array_merge($huddle_ids_arr_type_4, $merged_array);
            }
            if(in_array('4',$huddle_type))
            {
                $condition_array = array('afmd.meta_data_name' => 'folder_type','huddle_users.user_id' => $user_id,'huddle_users.role_id' => 210
                    );
                $folder_type = [3];
                $account_folder_type[] = [1];
                $huddle_ids_arr_type_5 = $this->get_huddle_ids_query($account_id,$account_folder_type,$condition_array,$folder_type,false);
                $merged_array = array_merge($huddle_ids_arr_type_5, $merged_array);
            }
            if(in_array('7',$huddle_type))
            {
                $query = AccountFolder::select('AccountFolder.account_folder_id')
                        ->leftJoin('account_folders_meta_data as afmd','AccountFolder.account_folder_id','=','afmd.account_folder_id')
                        ->leftJoin('account_folder_users as huddle_users','AccountFolder.account_folder_id','=','huddle_users.account_folder_id')
                        ->leftJoin('users as User','User.id','=','huddle_users.user_id')
                        ->where($condition_array
                        )->whereIn('AccountFolder.folder_type',$account_folder_type)
                        ->whereIn('AccountFolder.account_id',array($account_id))
                        ->whereRaw('(AccountFolder.created_by = ' . $user_id. ' OR huddle_users.user_id = ' . $user_id. ')' );

                $account_coach_huddles = $query->whereIn('afmd.meta_data_value',$folder_type)->groupBy('huddle_users.account_folder_id')->get()->toArray();
            }
            else 
            {
                $account_coach_huddles = $merged_array;
            }
                $coach_hud = array();
                $documents_ids = array();
                foreach ($account_coach_huddles as $coach_huddles) {
                    $coach_hud[] = $coach_huddles['account_folder_id'];
                }
                if(!empty($coach_hud))
                {
                    $documents_ids = AccountFolderDocument::select('document_id')->whereIn('account_folder_id',$coach_hud)->get()->toArray();
                }
                else 
                {
                    $documents_ids = array();
                }
            $final_document_ids = array();
            foreach($documents_ids as $document_id)
            {
                $final_document_ids[] = $document_id['document_id'];
            }    
            $result = array();
            $result['huddle_ids'] = $coach_hud;
            $result['document_ids'] = $final_document_ids;
            return $result;    
        }
        
        if($report_type == 'assessment_report' || $report_type == 'huddle_report' )
        {
            $documents_ids = AccountFolderDocument::select('document_id')->whereIn('account_folder_id',array($huddle_id))->get()->toArray();
            $final_document_ids = array();
            foreach($documents_ids as $document_id)
            {
                $final_document_ids[] = $document_id['document_id'];
            }    
            $result = array();
            $result['huddle_ids'] = array($huddle_id);
            $result['document_ids'] = $final_document_ids;
            return $result;
        }
        
    }
    
    function get_huddle_ids_query($account_id,$account_folder_type ,$condition_array,$folder_type,$workspace = false)
    {
        $query = '';
        $query = AccountFolder::select('AccountFolder.account_folder_id')
                    ->leftJoin('account_folders_meta_data as afmd','AccountFolder.account_folder_id','=','afmd.account_folder_id')
                    ->leftJoin('account_folder_users as huddle_users','AccountFolder.account_folder_id','=','huddle_users.account_folder_id')
                    ->leftJoin('users as User','User.id','=','huddle_users.user_id')
                    ->where($condition_array
                    )->whereIn('AccountFolder.folder_type',$account_folder_type)
                    ->whereIn('AccountFolder.account_id',array($account_id));
       
        if($workspace)
        {
            $account_coach_huddles = $query->whereIn('afmd.meta_data_value',$folder_type)->get()->toArray();
        }
        else
        {
            $account_coach_huddles = $query->whereIn('afmd.meta_data_value',$folder_type)->groupBy('huddle_users.account_folder_id')->get()->toArray();
        }
        
        return $account_coach_huddles;
    }

    function export_pdf($header_data_array,$data,$account_name,$file_name=false){

       // dd($header_data_array,$data);
        $report_name_break = strpos($account_name,"_");
        
        $report_name = substr($account_name,$report_name_break,strlen($account_name));
        $act_name = substr($account_name,0,$report_name_break );
        $report_name = str_replace("_"," ",$report_name); 
        $report_name = ucwords($report_name);
        $userName = "-";
        if($this->httpreq->has('user_id')){
            $userName = User::select('first_name','last_name')->where('id', $this->httpreq->input('user_id'))->get()->first();
            if($userName){
                $userName = $userName->first_name . " " . $userName->last_name ;
            }else{
                $userName = "-";    
            }
        }else{
            $userName = "-";
            
        }
        //$user_id = $request->input('user_id');

        $pdf = new Fpdf();
        $pdf->AddPage();

        //$pdf->SetAutoPageBreak(false);



        $pdf->Image(config('s3.sibme_base_url')."/img/pdf_logo.png", 10, 10);
        $pdf->SetFont('Arial');
        $pdf->cell(125,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Account Name:',0,0);
        $pdf->SetFont('Arial','',8);
        // $pdf->SetTextColor(6,69,173);
        $pdf->cell(22,5,$act_name,0,1);
        // $pdf->SetTextColor(0, 0, 0);
        $pdf->cell(125,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Report Name:',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->cell(22,5,$report_name,0,1);
        $pdf->cell(125,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Exported By:',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->cell(22,5,$userName,0,1);
        $pdf->cell(125,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Exported Date:',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->cell(22,5,date("Y-m-d h:i:sa"),0,1);
        $pdf->cell(0,5,'','B',1);
        $pdf->cell(0,5,'','0',1);



	    $pdf->SetFont('Arial','B',9);
        $rec = 1;
    
        foreach ($data as $huddle_report){
                
            $cell_data = "";

            foreach($header_data_array as $key => $header_dataa){
                //if(isset($huddle_report[$key]) && !empty($huddle_report[$key]) ){
                    $assessment_report = $huddle_report[$key];
                    if(!is_array($assessment_report)){
                        $cell_data .= chr(149) . "\t\t\t" . $header_dataa . " : " .strip_tags($assessment_report)  . "\n";
                    }
            }

            $pdf->MultiCell(0,6, $cell_data
                    ,1,1);
                $pdf->Ln(2);

                // if(trim($report_name) == trim("Detail View")){
                //         $pb = $rec % 6;
                // }
                // else
                // {
                //         $pb = $rec % 2;
                // }
                    
                // if($pb == 0){
                //     $pdf->AddPage();
                // }
            
                // $rec = $rec+1;      
        }
        //$today = date("m-d-y");
        $dir = config('general.export_analytics_path');
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        if(!$file_name){
            return $pdf->Output($account_name . ".pdf",'D');    
        }
        $file_path = $dir . $file_name;

        $pdf->Output($file_path,'F');
        $destination = $this->save_to_s3($file_path);
        $this->send_reports_export_email($file_name);
    }
    
    
    public function save_to_s3($file){
        $path_parts = pathinfo($file);
        $new_destination =  config('s3.export_analytics_path') . $path_parts['filename'] . '.' . $path_parts['extension'];
 
        // Create an Amazon S3 client object
        $client = new S3Client([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);

        $client->putObject(array(
            'Bucket' => config('s3.bucket_name'),
            'Key' => $new_destination,
            'SourceFile' => $file
        ));

        return $new_destination;
    }
    
      function escape_string($param) {
        if(is_array($param))
            return array_map(__METHOD__, $param);

            if(!empty($param) && is_string($param)) {
                return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $param);
            }

            return $param;
        } 
     function huddle_role_name($huddle_role,$huddle_type)
     {
        $role_name = ''; 
        if($huddle_role == '200' && $huddle_type == '1')
        {
            $role_name = 'Owner';
        }
        if($huddle_role == '210' && $huddle_type == '1')
        {
            $role_name = 'Member';
        }
        if($huddle_role == '220' && $huddle_type == '1')
        {
            $role_name = 'Viewer';
        }
        if($huddle_role == '200' && $huddle_type == '2')
        {
            $role_name = 'Coach';
        }
        if($huddle_role == '210' && $huddle_type == '2')
        {
            $role_name = 'Coachee';
        }
        if($huddle_role == '200' && $huddle_type == '3')
        {
            $role_name = 'Assessor';
        }
        if($huddle_role == '210' && $huddle_type == '3')
        {
            $role_name = 'Assessee';
        }
        return $role_name;
     }
     
     function send_reports_export_email($file_name,$user_id = false)
     {
         $subject = 'Export File';
         if(strpos($file_name,'huddle_report'))
         {
             $subject = 'Huddle Report Export File';
         }
         if(strpos($file_name,'coaching_report'))
         {
             $subject = 'Coaching Report Export File';
         }
         if(strpos($file_name,'assessment_report'))
         {
             $subject = 'Assessment Report Export File';
         }
         if(strpos($file_name,'user_summary_report'))
         {
             $subject = 'User Summary Report Export File';
         }
        if(!$user_id)
        {
            $user_id = $this->httpreq->user_id;
        }
        $user_detail = User::where(array('id' => $user_id))->first();
        $final_file = \App\Services\Exports\Analytics\ExportStandardsToExcel::retrieve_from_s3($file_name);
        $html = "Please Click the link to download your exported file : " . $final_file ;
          $emailData = [
                'from' => Sites::get_site_settings('static_emails', '1')['noreply'],
                'from_name' => Sites::get_site_settings('site_title', '1'),
                'to' => $user_detail->email,
                'subject' => $subject,
                'template' => $html,
            ];
       $email = Email::sendCustomEmail($emailData);
        //$this->add_job_queue(1, $user_detail->email , '', $html, true);
        
     }
     
    function add_job_queue($jobid, $to, $subject, $html, $is_html = false, $reply_to = '') {
        if ($is_html)
            $ihtml = 'Y';
        else
            $ihtml = 'N';

        if ($reply_to != '') {
            $reply_to = "<replyto>$reply_to</replyto>";
        }
        $html = '<![CDATA[' . $html . ']]>';
        $xml = "<mail><to>$to</to>$reply_to <from>" . Sites::get_site_settings('static_emails', $this->site_id)['noreply'] . "</from><fromname>" . Sites::get_site_settings('site_title', $this->site_id) . "</fromname><bcc /><cc /><subject><![CDATA[$subject]]></subject><body>$html</body><html>$ihtml</html></mail>";

        $data = array(
            'JobId' => $jobid,
            'CreateDate' => date("Y-m-d H:i:s"),
            'RequestXml' => $xml,
            'JobQueueStatusId' => 1,
            'site_id' => $this->site_id,
            'CurrentRetry' => 0,
            'JobSource' => Sites::get_base_url($this->site_id)
        );



        DB::table('JobQueue')->insert($data);
        return TRUE;
    }

    function get_huddles_participating_in($user_id,$account_id,$start_date,$end_date,$huddle_ids){

        // Log::info($huddle_ids);
        $huddles = AccountFolder::select("AccountFolder.is_published","AccountFolder.account_folder_id","AccountFolder.name",'account_folders_meta_data.meta_data_value as huddle_type')//,'account_folder_users.user_id','account_folder_users.role_id','AccountFolder.last_edit_date')//,'account_folder_groups.role_id as group_user_role ')//,'user_groups.group_id as group_user')//,'user_groups.group_id as group_user','account_folder_groups.role_id as group_user_role')
        ->join("account_folders_meta_data","AccountFolder.account_folder_id","account_folders_meta_data.account_folder_id")
        ->where("account_folders_meta_data.meta_data_name","folder_type")
        ->join("account_folder_users","AccountFolder.account_folder_id","account_folder_users.account_folder_id")
        ->where("account_folder_users.user_id",$user_id)
        ->where('AccountFolder.account_id',$account_id)
        ->where('AccountFolder.folder_type',1)
        ->where('AccountFolder.active',1)
        ->where('AccountFolder.site_id',$this->site_id)
        ->whereDate('AccountFolder.created_date','>=', $start_date)
        ->whereDate('AccountFolder.created_date', '<=', $end_date)
        ->where(function($query) use($huddle_ids){
            $query->when(in_array(1,$huddle_ids),function($q){
                $q->orWhere(function($qq){
                    $qq->where("account_folders_meta_data.meta_data_value",2)
                    ->where("account_folder_users.role_id",200);
                });
            })->when(in_array(2,$huddle_ids),function($q){
                $q->orWhere(function($qq){
                    $qq->where("account_folders_meta_data.meta_data_value",2)
                    ->where("account_folder_users.role_id",210);
                });
            })->when(in_array(3,$huddle_ids) || in_array(8,$huddle_ids) ,function($q){
                $q->orWhere(function($qq){
                    $qq->where("account_folders_meta_data.meta_data_value",3)
                    ->where("AccountFolder.is_published",1)
                    ->where("account_folder_users.role_id",200);
                });
            })->when(in_array(4,$huddle_ids) || in_array(8,$huddle_ids),function($q){
                $q->orWhere(function($qq){
                    $qq->where("account_folders_meta_data.meta_data_value",3)
                    ->where("AccountFolder.is_published",1)
                    ->where("account_folder_users.role_id",210);
                });
            })->when(in_array(5,$huddle_ids),function($q){
                $q->orWhere(function($qq){
                    $qq->where("account_folders_meta_data.meta_data_value",1);
                });
            })
            ->when(in_array(6,$huddle_ids),function($q){
                $q->orWhere(function($qq){
                    $qq->where("account_folders_meta_data.meta_data_value",5);
                });
            });
           
        })
        ->distinct("AccountFolder.account_folder_id")->get();


       // return $huddles;

        $huddles_from_group = AccountFolder::select("AccountFolder.is_published","AccountFolder.account_folder_id","AccountFolder.name",'account_folders_meta_data.meta_data_value as huddle_type')//,'user_groups.user_id','account_folder_groups.role_id','AccountFolder.last_edit_date')
        ->join("account_folders_meta_data","AccountFolder.account_folder_id","account_folders_meta_data.account_folder_id")
        ->where("account_folders_meta_data.meta_data_name","folder_type")
        ->join('account_folder_groups',"AccountFolder.account_folder_id","account_folder_groups.account_folder_id")
        ->join('user_groups', 'account_folder_groups.group_id','user_groups.group_id')
        ->where('user_groups.user_id',$user_id)
        ->where('AccountFolder.account_id',$account_id)
        ->where('AccountFolder.folder_type',1)
        ->where('AccountFolder.active',1)
        ->where('AccountFolder.site_id',$this->site_id)
        ->whereDate('AccountFolder.created_date','>=', $start_date)
        ->whereDate('AccountFolder.created_date', '<=', $end_date)
       ->groupBy('account_folders_meta_data.meta_data_value')
       ->where(function($query) use($huddle_ids){
            $query->when(in_array(1,$huddle_ids),function($q){
                $q->orWhere(function($qq){
                    $qq->where("account_folders_meta_data.meta_data_value",2)
                    ->where("account_folder_groups.role_id",200);
                });
            })->when(in_array(2,$huddle_ids),function($q){
                $q->orWhere(function($qq){
                    $qq->where("account_folders_meta_data.meta_data_value",2)
                    ->where("account_folder_groups.role_id",210);
                });
            })->when(in_array(3,$huddle_ids) || in_array(8,$huddle_ids) ,function($q){
                $q->orWhere(function($qq){
                    $qq->where("account_folders_meta_data.meta_data_value",3)
                    ->where("AccountFolder.is_published",1)
                    ->where("account_folder_groups.role_id",200);
                });
            })->when(in_array(4,$huddle_ids) || in_array(8,$huddle_ids),function($q){
                $q->orWhere(function($qq){
                    $qq->where("account_folders_meta_data.meta_data_value",3)
                    ->where("AccountFolder.is_published",1)
                    ->where("account_folder_groups.role_id",210);
                });
            })->when(in_array(5,$huddle_ids),function($q){
                $q->orWhere(function($qq){
                    $qq->where("account_folders_meta_data.meta_data_value",1);
                });
            })
            ->when(in_array(6,$huddle_ids),function($q){
                $q->orWhere(function($qq){
                    $qq->where("account_folders_meta_data.meta_data_value",5);
                });
            });
           
        })
        ->distinct("AccountFolder.account_folder_id")->get();

        
           // return $huddles_from_group;            
            $user_huddle_participation = array();
    
            foreach($huddles as $huddle){
               if(!in_array($huddle->account_folder_id,$user_huddle_participation)){
                    $user_huddle_participation[] = $huddle->account_folder_id;
               }  
            }
    
            foreach($huddles_from_group as $huddle_from_group){
                
                if(!in_array($huddle_from_group->account_folder_id,$user_huddle_participation))
                {
                    $user_huddle_participation[] = $huddle_from_group->account_folder_id;
                }
            }

            return count($user_huddle_participation);

    }
    
    function assessment_report_assessee(Request $request,$huddle_id=false,$api=true)
    {
            $input = $request->all();
            $huddle_id = $huddle_id ? $huddle_id : $input['huddle_id'];
            $subPage = isset($input['sub_page'])? $input['sub_page']  : 1;
            $limit = 5;
            if(isset($input['paginate']) && $input['paginate'] == false){
                $subPage=false;
                $limit = false;
            }
            $is_web = isset($input['is_web'])?$input['is_web']:false;
            $account_id = $input['account_id'];
            $user_role = $input['user_role'];
            $user_id = $input['user_id'];
            $active = $input['active'];
            $start_date = $input['start_date'];
            $end_date = $input['end_date'];
            $stDate = date_create($start_date);
            $start_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
            $endDate = date_create($end_date);
            $end_date = date_format($endDate, 'Y-m-d'). ' 23:59:59';
            $account_user_custom_fields = CustomField::get_custom_fields($account_id,'1');
            $account_assessment_custom_fields = CustomField::get_custom_fields($account_id,'2');
            if(isset($input['report_id']))
            {
                $report_config_id = $input['report_id'];
            }
            else
            {
                $default_report_config = ReportConfiguration::where(array('report_type' => 'assessment_report','is_default' => '1'))->first();
                $report_config_id = $default_report_config->id;
            }
            $report_data = ReportConfiguration::where(array("id" => $report_config_id))->first();
        $report_attributes = json_decode($report_data->report_attributes);
        $report_attributes_array = array();
        foreach($report_attributes as $row)
        {
            if($row->status)
            {
                $report_attributes_array[] = $row->name;
            }
        }
            $huddle_detail = AccountFolder::where(array('account_folder_id' => $huddle_id))->first()->toArray();
            $huddle_participants = array();
            $user_ids = array();
            $huddle_participants = AccountFolderUser::getAssesseesList($this->site_id, $huddle_id, $user_id, $user_role,false,'en',$is_web,$subPage,null,null,$limit);
            $huddle_participants_count = AccountFolder::getHuddleUsersIncludingGroups($this->site_id, $huddle_id, $user_id, $user_role, true, 210)['participants'];
            $huddle_detail['huddle_assessors'] = AccountFolder::getHuddleUsersIncludingGroups($this->site_id, $huddle_id, $user_id, $user_role,true, 200)['participants'];
            $submission_deadline_time = AccountFolderMetaData::where(array(
                        'account_folder_id' => $huddle_id,
                        'site_id' => $this->site_id,
                        'meta_data_name' => 'submission_deadline_time'
                    ))->get()->toArray();


            $submission_deadline_time = isset($submission_deadline_time[0]['meta_data_value']) ? $submission_deadline_time[0]['meta_data_value'] : date('H:i');

            $huddle_detail['submission_deadline_time'] = $submission_deadline_time;

            $submission_deadline_date = AccountFolderMetaData::where(array(
                        'account_folder_id' => $huddle_id,
                        'site_id' => $this->site_id,
                        'meta_data_name' => 'submission_deadline_date'
                    ))->get()->toArray();

            $submission_deadline_date = isset($submission_deadline_date[0]['meta_data_value']) ? $submission_deadline_date[0]['meta_data_value'] : date('m-d-Y');

            $huddle_detail['submission_deadline_date'] = $submission_deadline_date;

            $submission_allowed = AccountFolderMetaData::where(array(
                        'account_folder_id' => $huddle_id,
                        'site_id' => $this->site_id,
                        'meta_data_name' => 'submission_allowed'
                    ))->get()->toArray();

            $submission_allowed = isset($submission_allowed[0]['meta_data_value']) ? $submission_allowed[0]['meta_data_value'] : 1;

            $huddle_detail['submission_allowed'] = $submission_allowed;

            $huddle_detail['resource_submission_allowed'] = app('App\Http\Controllers\ApiController')->count_resource_submission_allowed($huddle_id);
            $huddle_message = AccountFolderMetaData::where(array(
                    'account_folder_id' => $huddle_id,
                    'site_id' => $this->site_id,
                    'meta_data_name' => 'message'
                ))->first()->toArray();

            $huddle_detail['huddle_message'] = $huddle_message;
            $user_name = array_column($huddle_participants, 'user_name');
            $user_name = array_map('strtolower', $user_name);
            array_multisort($user_name, SORT_ASC, SORT_STRING , $huddle_participants);
            $updated_attribute_data = array();
            foreach($huddle_participants as $participant)
            {
                $user_ids[] = $participant['user_id'];
            }
            $attribute_data = $this->attributes_query($account_id,array($account_id),$user_ids,array($huddle_id),$report_config_id,$start_date,$end_date,false,'','',$active);
            //$attribute_data = $this->attributes_query($account_id,array($account_id),$user_ids,array($huddle_id),$report_config_id,$start_date,$end_date,false,'LIMIT 10 OFFSET 0','',$active,130,$request->get('sort_by'),$request->get('sort_by_order'),true);
            $attribute_data = json_decode(json_encode($attribute_data), True);
            //$attribute_data_count = $this->attributes_query($account_id,array($account_id),$user_ids,array($huddle_id),$report_config_id,$start_date,$end_date,false,'','',$active,130,$request->get('sort_by'),$request->get('sort_by_order'),true);
            //$attribute_data_count = count($attribute_data_count);
            foreach($attribute_data as $value => $data)
            {
                $participant_data = array();
                $huddle_role = HelperFunctions::get_huddle_roles($huddle_id,$data['user_id']);
                foreach($huddle_participants as $participant)
                {
                    if($participant['user_id'] == $data['user_id'])
                    {
                        $participant_data = $participant;
                    }
                }
                $documents_ids = AccountFolderDocument::select('account_folder_documents.document_id')->join('documents as d','account_folder_documents.document_id','=','d.id')->where(array('d.created_by' => $data['user_id'] ))->whereIn('account_folder_id',array($huddle_id))->get()->toArray();
                if(isset($input['standard_ids']))
                {
                    $frameworks = $this->get_framework_tags_data($documents_ids,$input['standard_ids'],true);
                    $framework_data = $frameworks['standard_data'];
                }
                else
                {
                    $framework_data = array();
                }
                $custom_markers_ids = AccountTag::select('account_tag_id')->where(array('tag_type' => '1' , 'account_id' => $account_id))->get()->toArray();
                $avg_pl_level = $this->get_avg_performance_level($documents_ids,$input['framework_id']);
                $custom_marker_data = $this->get_custom_marker_data($documents_ids,$custom_markers_ids,true,$start_date,$end_date);
                 foreach($report_attributes as $row)
                    {
                        if(!empty($row->value) && $row->status)
                        {
                            $updated_attribute_data[$value][] = $data[$row->value];
                        }

                        if($row->name == 'Last Modified' && $row->status)
                        {
                            $updated_attribute_data[$value][] = isset($participant_data['last_modified'])? $participant_data['last_modified'] : '';
                        }
                        if($row->name == 'Last Submission' && $row->status)
                        {
                            $updated_attribute_data[$value][] = isset($participant_data['last_submission'])? $participant_data['last_submission'] : '';
                        }
                        if($row->name == 'Videos Uploaded to Huddles' && $row->status)
                        {
                            $updated_attribute_data[$value][] = isset($participant_data['videos_count'])? $participant_data['videos_count'].','.'videos' : '';
                        }
                        if($row->name == 'Huddle Resources Uploaded' && $row->status)
                        {
                            $updated_attribute_data[$value][] = isset($participant_data['resources_count'])? $participant_data['resources_count'].','.'resources' : '';
                        }
                        if($row->name == 'Assessed' && $row->status)
                        {
                            if(isset($participant_data['assessed']))
                            {
                                if($participant_data['assessed'])
                                {
                                    $updated_attribute_data[$value][] = 'true,assessed';
                                }
                                else
                                {
                                    $updated_attribute_data[$value][] = 'false,assessed';
                                }
                            }
                        }
                        if($row->name == 'Is Active' && $row->status)
                        {
                            $user_active_detail = UserAccount::where(array('user_id' => $data['user_id'] , 'account_id' => $data['account_id']))->first();
                            if($user_active_detail->is_active == '1' && $user_active_detail->status_type == 'Active' )
                            {
                                $updated_attribute_data[$value][] = 'Yes';
                            }
                            else
                            {
                                $updated_attribute_data[$value][] = 'No';
                            }
                        }
                        if($row->name == 'Huddle Role' && $row->status)
                        {
                           $updated_attribute_data[$value][] = $this->huddle_role_name($huddle_role,'3');
                        }
                        if($row->name == 'Avg Performance Levels' && $row->status)
                        {
                            $updated_attribute_data[$value][] = isset($participant_data['avg_performance_level_rating'])? round($participant_data['avg_performance_level_rating']) : '';
                        }
                        if($row->name == 'Huddle Video Comments' && $row->status)
                        {
                            $updated_attribute_data[$value][] = isset($participant_data['comments_count'])? $participant_data['comments_count'].','.'comments' : '';
                        }
                        if($row->name == 'Custom Fields (User-level data)' && $row->status)
                        {

                            if(isset($account_user_custom_fields['user_summary']['custom_fields']))
                            {
                                $custom_fields_array = $account_user_custom_fields['user_summary']['custom_fields'];
                                foreach($custom_fields_array as $cf)
                                {
                                    $user_custom_values = UserCustomField::get_users_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,'');
                                    if(!empty($user_custom_values))
                                    {
                                        if($user_custom_values[0]->custom_field_value == 'true')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'Yes';
                                        }
                                        if($user_custom_values[0]->custom_field_value == 'false')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'No';
                                        }
                                        $updated_attribute_data[$value][] = $user_custom_values[0]->custom_field_value;
                                    }
                                    else
                                    {
                                        $updated_attribute_data[$value][] = '';
                                    }
                                }
                            }

                            if(isset($account_user_custom_fields['user_summary']['parent_custom_fields']))
                            {
                                $custom_fields_array = $account_user_custom_fields['user_summary']['parent_custom_fields'];
                                foreach($custom_fields_array as $cf)
                                {
                                    $user_custom_values = UserCustomField::get_users_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,'');
                                    if(!empty($user_custom_values))
                                    {
                                        if($user_custom_values[0]->custom_field_value == 'true')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'Yes';
                                        }
                                        if($user_custom_values[0]->custom_field_value == 'false')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'No';
                                        }
                                        $updated_attribute_data[$value][] = $user_custom_values[0]->custom_field_value;
                                    }
                                    else
                                    {
                                        $updated_attribute_data[$value][] = '';
                                    }
                                }

                            }

                        }
                        if($row->name == 'Custom Fields (Assessment-level data)' && $row->status)
                        {
                           if(isset($account_assessment_custom_fields['assessment']['custom_fields']))
                           {
                               $custom_fields_array = $account_assessment_custom_fields['assessment']['custom_fields'];
                                foreach($custom_fields_array as $cf)
                                {
                                    $user_custom_values = AssessmentCustomField::get_assessment_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,$huddle_id);
                                    if(!empty($user_custom_values))
                                    {
                                        if($user_custom_values[0]->assessment_custom_field_value == 'true')
                                        {
                                           $user_custom_values[0]->assessment_custom_field_value = 'Yes';
                                        }
                                        if($user_custom_values[0]->assessment_custom_field_value == 'false')
                                        {
                                           $user_custom_values[0]->assessment_custom_field_value = 'No';
                                        }
                                        $updated_attribute_data[$value][] = $user_custom_values[0]->assessment_custom_field_value;
                                    }
                                    else
                                    {
                                        $updated_attribute_data[$value][] = '';
                                    }
                                }
                           }

                           if(isset($account_assessment_custom_fields['assessment']['parent_custom_fields']))
                           {
                               $custom_fields_array = $account_assessment_custom_fields['assessment']['parent_custom_fields'];
                                foreach($custom_fields_array as $cf)
                                {
                                    $user_custom_values = AssessmentCustomField::get_assessment_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,$huddle_id);
                                    if(!empty($user_custom_values))
                                    {
                                        if($user_custom_values[0]->assessment_custom_field_value == 'true')
                                        {
                                           $user_custom_values[0]->assessment_custom_field_value = 'Yes';
                                        }
                                        if($user_custom_values[0]->assessment_custom_field_value == 'false')
                                        {
                                           $user_custom_values[0]->assessment_custom_field_value = 'No';
                                        }
                                        $updated_attribute_data[$value][] = $user_custom_values[0]->assessment_custom_field_value;
                                    }
                                    else
                                    {
                                        $updated_attribute_data[$value][] = '';
                                    }
                                }
                           }

                        }

                        if($row->name == 'Framework Tags' && $row->status && !in_array('Standard Performance Levels',$report_attributes_array))
                        {
                            foreach ($framework_data as $standard_data)
                            {
                                $updated_attribute_data[$value][] = $standard_data['count'];
                            }
                        }

                        if($row->name == 'Standard Performance Levels' && $row->status && !in_array('Framework Tags',$report_attributes_array))
                        {
                           foreach ($framework_data as $standard_data)
                            {
                               $updated_attribute_data[$value][] = $standard_data['pl_average'];
                            }
                        }
                        if($row->name == 'Framework Tags' && $row->status && in_array('Standard Performance Levels',$report_attributes_array ))
                        {

                            foreach ($framework_data as $standard_data)
                            {
                               $updated_attribute_data[$value][] = $standard_data['count'];
                               $updated_attribute_data[$value][] = $standard_data['pl_average'];
                            }
                        }

                        if($row->name == 'Custom Video Marker Tags' && $row->status)
                        {
                            foreach ($custom_marker_data as $marker_data)
                            {
                                $updated_attribute_data[$value][] = $marker_data['count'];
                            }
                        }
                        if($row->name == 'Assessment Summary' && $row->status)
                        {
                            $updated_attribute_data[$value][] = AccountFolderUser::getAssessmentSummaryTop($huddle_id, $data['user_id']);
                        }

                    }

                $updated_attribute_data[$value]['image'] = $data['image'];
                $updated_attribute_data[$value]['account_id'] =  $data['account_id'];
                $updated_attribute_data[$value]['user_id'] = $data['user_id'];
                $updated_attribute_data[$value]['email'] = $data['email'];
                $updated_attribute_data[$value]['user_name'] = $data['user_name'];
                $updated_attribute_data[$value]['huddle_id'] = $huddle_id;
                $updated_attribute_data[$value]['is_submitted'] = isset($participant_data['is_submitted'])? $participant_data['is_submitted'] : '';
                $updated_attribute_data[$value]['assessment_summary'] = AccountFolderUser::getAssessmentSummary($huddle_id, $data['user_id']);



            }
            //$final_array[$key]['assessee'] = $updated_attribute_data;
//            $final_array[$key] = $huddle_detail;
//            $final_array[$key]['assessee'] = $updated_attribute_data;
//            return $final_array;

            if($api){
                return $updated_attribute_data;
            }else{
                $_data = [  'huddle_detail' => $huddle_detail,
                            'updated_attribute_data' => $updated_attribute_data,
                      //'attribute_data_count' => $attribute_data_count,
                            'huddle_participants_count' => count($huddle_participants_count),
                            'account_user_custom_fields' => $account_user_custom_fields,
                            'custom_markers_ids' => $custom_markers_ids,
                            'avg_pl_level' => $avg_pl_level,
                            'custom_marker_data' => $custom_marker_data,
                            'account_assessment_custom_fields' => $account_assessment_custom_fields,
                            'framework_data' => $framework_data,
                    ];
            return $_data;
            }


    }

    function huddle_report_users(Request $request,$huddle_id=false,$api = true)
    {

            $input = $request->all();
            $paginate = isset($input['paginate']) && $input['paginate'] == false ? false : true;
            $subPage = isset($input['sub_page'])? $input['sub_page']  : 1;
            $subPageOffset = ($subPage-1) * 5;
            $subLimit = 'LIMIT 5 OFFSET ' . $subPageOffset;
            if(isset($input['paginate']) && $input['paginate'] == false){
                $subLimit="";
            }
            $huddle_id = $huddle_id ? $huddle_id : $input['huddle_id'];
            if(!is_array($input['account_ids']))
            {
                $input['account_ids'] = json_decode($input['account_ids']);
            }
            if(isset($input['standard_ids']) && !is_array($input['standard_ids']))
            {
                $input['standard_ids'] = explode(',',$input['standard_ids']);
            }
            if(isset($input['huddle_ids']) && !is_array($input['huddle_ids']))
            {
                $input['huddle_ids'] = json_decode($input['huddle_ids']);
            }
            $account_id = $input['account_id'];
            $account_ids = $input['account_ids'];
            if(empty($account_ids))
            {
                $account_ids = array($input['account_id']);
            }
            $user_id = $input['user_id'];
            $start_date = $input['start_date'];
            $end_date = $input['end_date'];
            $stDate = date_create($start_date);
            $start_date = date_format($stDate, 'Y-m-d') . ' 00:00:00';
            $endDate = date_create($end_date);
            $end_date = date_format($endDate, 'Y-m-d'). ' 23:59:59';
            $page = (int)$input['page'];
            $page = $page - 1;
            $limit = $input['limit'];
            $offset = $page*$limit;
            $coach_bool = false;
            $coachee_bool = false;
            $framework_data = array();
            $custom_marker_data = array();
            $active = $input['active'];
            $huddle_include = [];
            if(in_array('1', $input['huddle_ids']) || in_array('3', $input['huddle_ids']))
            {
                $coach_bool = true;
            }
            if(in_array('2', $input['huddle_ids']) || in_array('4', $input['huddle_ids']))
            {
                $coachee_bool = true;
            }

            if(in_array('1', $input['huddle_ids']) || in_array('2', $input['huddle_ids']))
            {
                $huddle_include[] = 2;
            }

            if(in_array('3', $input['huddle_ids']) || in_array('4', $input['huddle_ids']) || in_array('8', $input['huddle_ids']) )
            {
                $huddle_include[] = 3;
            }

            if(in_array('5', $input['huddle_ids']))
            {
                $huddle_include[] = 1;
            }

            if(isset($input['report_id']))
            {
                $report_config_id = $input['report_id'];
            }
            else
            {
                $default_report_config = ReportConfiguration::where(array('report_type' => 'huddle_report','is_default' => '1'))->first();
                $report_config_id = $default_report_config->id;
            }
            $report_data = ReportConfiguration::where(array("id" => $report_config_id))->first();
            $report_attributes = json_decode($report_data->report_attributes);
            $report_attributes_array = array();
            foreach($report_attributes as $row)
            {
                if($row->status)
                {
                    $report_attributes_array[] = $row->name;
                }
            }
            $huddle = AccountFolder::where('account_folder_id' , $huddle_id)->first()->toArray();
            $participants = HelperFunctions::get_huddle_participant_ids($huddle['account_folder_id'],$this->site_id);
            //$attribute_data = $this->attributes_query($account_id,array($huddle['account_id']),$participants,array($huddle['account_folder_id']),$report_config_id,$start_date,$end_date,false,'','',$active);
            $attribute_data = $this->attributes_query($account_id,array($huddle['account_id']),$participants,array($huddle['account_folder_id']),$report_config_id,$start_date,$end_date,false,$subLimit,'',$active,130,$request->get('sort_by'),$request->get('sort_by_order'),true);
            //dd($attribute_data);
            $attribute_data_count = $this->attributes_query($account_id,array($huddle['account_id']),$participants,array($huddle['account_folder_id']),$report_config_id,$start_date,$end_date,false,'','',$active,130,$request->get('sort_by'),$request->get('sort_by_order'),true);

            $attribute_data_count = count($attribute_data_count);

            $updated_attribute_data = array();
            $attribute_data = json_decode(json_encode($attribute_data), True);
            $huddle_type = HelperFunctions::get_huddle_type($huddle['account_folder_id'],$this->site_id);
            $documents_ids = AccountFolderDocument::select('document_id')->whereIn('account_folder_id',array($huddle['account_folder_id']))->get()->toArray();
            $custom_markers_ids = AccountTag::select('account_tag_id')->where(array('tag_type' => '1' , 'account_id' => $account_id))->get()->toArray();
            $avg_pl_level = $this->get_avg_performance_level($documents_ids,$input['framework_id'],true);
            $custom_marker_data = $this->get_custom_marker_data($documents_ids,$custom_markers_ids,true,$start_date,$end_date);
            $account_user_custom_fields = CustomField::get_custom_fields($account_id,'1');
            if(isset($input['standard_ids']))
            {
                $frameworks = $this->get_framework_tags_data($documents_ids,$input['standard_ids'],true,array($huddle['account_folder_id']));
                $framework_data = $frameworks['standard_data'];
            }
            else
            {
                $framework_data = array();
            }
            $huddle_sum_array = array();
            foreach($attribute_data as $value => $data)
            {

                $huddle_role = HelperFunctions::get_huddle_roles($huddle['account_folder_id'],$data['user_id']);

                $bool = false;
                if($huddle_role == 200 && $huddle_type != '1' )
                {
                    $bool = $coach_bool;
                }
                else
                {
                    $bool = $coachee_bool;
                }
                $new_cm_arr = array();
                $new_fd_arr = array();
                if(!$bool)
                {
                    foreach($custom_marker_data as $ind => $cm)
                    {
                        $new_cm_arr[$ind] = $cm;
                        $new_cm_arr[$ind]['count'] = 0;
                    }
                    foreach($framework_data as $fd_num => $fd)
                    {
                        $new_fd_arr[$fd_num] = $fd;
                        $new_fd_arr[$fd_num]['count'] = 0;
                        $new_fd_arr[$fd_num]['pl_average'] = 0;
                    }
                    $avg_pl_level = 0;
                    $framework_data = $new_fd_arr;
                    $custom_marker_data = $new_cm_arr;
                }

                if($huddle_type == '1')
                {
                    $bool = true;
                }
                if($huddle_type == '2' && in_array('3', $input['huddle_ids']) && !in_array('1', $input['huddle_ids'])  )
                {
                    $bool = false;
                }
                if($huddle_type == '2' && in_array('4', $input['huddle_ids']) && !in_array('2', $input['huddle_ids'])  )
                {
                    $bool = false;
                }
                if($huddle_type == '3' && in_array('1', $input['huddle_ids']) && !in_array('3', $input['huddle_ids'])  )
                {
                    $bool = false;
                }
                if($huddle_type == '3' && in_array('2', $input['huddle_ids']) && !in_array('4', $input['huddle_ids'])  )
                {
                    $bool = false;
                }

                $count = 0;

                foreach($report_attributes as $row)
                    {
                        if(!isset($huddle_sum_array[$count]) && $row->status && !empty($row->value) && is_numeric($data[$row->value]))
                        {
                            $huddle_sum_array[$count] = 0;
                        }
                        else if($row->status && !empty($row->value) && !is_numeric($data[$row->value]))
                        {
                            $huddle_sum_array[$count] = '';
                        }
                        if(!empty($row->value) && $row->status)
                        {
                            $updated_attribute_data[$value][] = $data[$row->value];
                            if(is_numeric($data[$row->value]))
                            {
                                $huddle_sum_array[$count] = round($huddle_sum_array[$count] + $data[$row->value],2);
                            }
                        }

                        if($row->status && !empty($row->value) )
                        {
                            $count++;
                        }

                        if($row->name == 'Framework Tags' && $row->status && !in_array('Standard Performance Levels',$report_attributes_array))
                        {
                            foreach ($framework_data as $standard_data)
                            {
                                if(!isset($huddle_sum_array[$count]))
                                 {
                                     $huddle_sum_array[$count] = 0;
                                 }
                               $updated_attribute_data[$value][] = $standard_data['count'];
                               $huddle_sum_array[$count] = $huddle_sum_array[$count] + $standard_data['count'];
                               $count++;
                            }
                        }

                        if($row->name == 'Standard Performance Levels' && $row->status && !in_array('Framework Tags',$report_attributes_array))
                        {
                           foreach ($framework_data as $standard_data)
                            {
                                if(!isset($huddle_sum_array[$count]))
                                {
                                    $huddle_sum_array[$count] = $standard_data['pl_average_huddle'];
                                }
                               $updated_attribute_data[$value][] = $standard_data['pl_average'];
                               //$huddle_sum_array[$count] = $huddle_sum_array[$count] + $standard_data['pl_average'];
                               $count++;
                            }
                        }
                        if($row->name == 'Framework Tags' && $row->status && in_array('Standard Performance Levels',$report_attributes_array ))
                        {

                            foreach ($framework_data as $standard_data)
                            {
                                if(!isset($huddle_sum_array[$count]))
                                {
                                    $huddle_sum_array[$count] = 0;
                                }
                               $updated_attribute_data[$value][] = $standard_data['count'];
                               $huddle_sum_array[$count] = $huddle_sum_array[$count] + $standard_data['count'];
                               $count++;
                                if(!isset($huddle_sum_array[$count]))
                                {
                                    $huddle_sum_array[$count] = $standard_data['pl_average_huddle'];
                                }
                               $updated_attribute_data[$value][] = $standard_data['pl_average'];
                              // $huddle_sum_array[$count] = $huddle_sum_array[$count] + $standard_data['pl_average'];
                               $count++;
                            }
                        }

                        if($row->name == 'Custom Video Marker Tags' && $row->status)
                        {
                            foreach ($custom_marker_data as $marker_data)
                            {
                                if(!isset($huddle_sum_array[$count]))
                                {
                                    $huddle_sum_array[$count] = 0;
                                }
                               $huddle_sum_array[$count] = $huddle_sum_array[$count] + $marker_data['count'];
                               $updated_attribute_data[$value][] = $marker_data['count'];
                               $count++;
                            }
                        }

                        if($row->name == 'Avg Performance Levels' && $row->status)
                        {
                            if(!isset($huddle_sum_array[$count]))
                                {
                                    $huddle_sum_array[$count] = '';
                                }
                                $count++;
                                $updated_attribute_data[$value][] = $avg_pl_level;
                        }
                        if($row->name == 'Is Active' && $row->status)
                        {
                            $user_active_detail = UserAccount::where(array('user_id' => $data['user_id'] , 'account_id' => $data['account_id']))->first();
                            if($user_active_detail->is_active == '1' && $user_active_detail->status_type == 'Active' )
                            {
                                $updated_attribute_data[$value][] = 'Yes';
                            }
                            else
                            {
                                $updated_attribute_data[$value][] = 'No';
                            }
                            if(!isset($huddle_sum_array[$count]))
                            {
                                $huddle_sum_array[$count] = '';
                            }
                            $count++;
                        }
                        if($row->name == 'Huddle Role' && $row->status)
                        {
                            if($data['user_id'] == $huddle['created_by']){
                                $updated_attribute_data[$value][] = 'Creator';  
                            }
                            else{
                                $updated_attribute_data[$value][] = $this->huddle_role_name($huddle_role,$huddle_type);
                            
                           }
                            if(!isset($huddle_sum_array[$count]))
                            {
                                $huddle_sum_array[$count] = '';
                            }
                            $count++;
                        }
                        if($row->name == 'Last Modified' && $row->status)
                        {
                            $updated_attribute_data[$value][] = isset($huddle['last_edit_date'])? self::getCurrentLangDate(strtotime($huddle['last_edit_date']),'archive_module') : '';
                            if(!isset($huddle_sum_array[$count]))
                            {
                                $huddle_sum_array[$count] = '';
                            }
                            $count++;
                        }

                    if($row->name == 'Custom Fields (User-level data)' && $row->status)
                    {

                        if(isset($account_user_custom_fields['user_summary']['custom_fields']))
                        {
                            $custom_fields_array = $account_user_custom_fields['user_summary']['custom_fields'];
                            foreach($custom_fields_array as $cf)
                            {
                                if(!isset($huddle_sum_array[$count]))
                                {
                                    $huddle_sum_array[$count] = '';
                                }
                                $count++;
                                $user_custom_values = UserCustomField::get_users_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,'');
                                if(!empty($user_custom_values))
                                {
                                        if($user_custom_values[0]->custom_field_value == 'true')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'Yes';
                                        }
                                        if($user_custom_values[0]->custom_field_value == 'false')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'No';
                                        }
                                    $updated_attribute_data[$value][] = $user_custom_values[0]->custom_field_value;
                                }
                                else
                                {
                                    $updated_attribute_data[$value][] = '';
                                }
                            }
                        }

                        if(isset($account_user_custom_fields['user_summary']['parent_custom_fields']))
                        {
                            $custom_fields_array = $account_user_custom_fields['user_summary']['parent_custom_fields'];
                            foreach($custom_fields_array as $cf)
                            {
                                if(!isset($huddle_sum_array[$count]))
                                {
                                    $huddle_sum_array[$count] = '';
                                }
                                $count++;
                                $user_custom_values = UserCustomField::get_users_custom_fields_of_account($data['user_id'],$account_id, $cf->id ,'');
                                if(!empty($user_custom_values))
                                {
                                        if($user_custom_values[0]->custom_field_value == 'true')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'Yes';
                                        }
                                        if($user_custom_values[0]->custom_field_value == 'false')
                                        {
                                           $user_custom_values[0]->custom_field_value = 'No';
                                        }
                                    $updated_attribute_data[$value][] = $user_custom_values[0]->custom_field_value;
                                }
                                else
                                {
                                    $updated_attribute_data[$value][] = '';
                                }
                            }

                        }

                    }
                }

                        $updated_attribute_data[$value]['image'] = $data['image'];
                        $updated_attribute_data[$value]['account_id'] =  $data['account_id'];
                        $updated_attribute_data[$value]['user_id'] = $data['user_id'];
                        $updated_attribute_data[$value]['email'] = $data['email'];
                        $updated_attribute_data[$value]['user_name'] = $data['user_name'];
                        $updated_attribute_data[$value]['huddle_id'] = $huddle['account_folder_id'];

            }


            if($api){
                return $updated_attribute_data;
            }else{

                $_data = [ 'huddle_sum_array' => $huddle_sum_array,
                'updated_attribute_data' => $updated_attribute_data,
                'attribute_data_count' => $attribute_data_count,
                'huddle_type' => $huddle_type,
                'account_user_custom_fields' => $account_user_custom_fields,
                'custom_markers_ids' => $custom_markers_ids,
                'avg_pl_level' => $avg_pl_level,
                'custom_marker_data' => $custom_marker_data,
                'framework_data' => $framework_data,
                ];

                return $_data;
            }


        // $_data = array('update_attribute_data' => $updated_attribute_data , 'huddle_sum_array' => $huddle_sum_array );
        // return $_data;
    }

}