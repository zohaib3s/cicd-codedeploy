<?php

namespace App\Http\Controllers;

use App\Api\Activities\GetActivities;
use App\Models\AccountFolderDocument;
use App\Models\AccountFolderGroup;
use App\Models\UserActivityLog;
use Illuminate\Http\Request;
use App\Models\AccountFolderUser;
use App\Models\UserGroup;
use App\Models\AccountFolder;
use App\Models\AccountFolderMetaData;
use App\Models\DocumentStandardRating;
use App\Models\Document;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\AccountTag;
use App\Models\Comment;
use App\Models\AccountFrameworkSettingPerformanceLevel;
use App\Models\AccountFrameworkSetting;
use Illuminate\Support\Facades\DB;
use Datetime;
use App\Services\TranslationsManager;
use App\Models\AccountMetaData;
use App\Models\DocumentMetaData;

use App\Services\HelperFunctions;

class TrackerController extends Controller {

    protected $httpreq;
    protected $site_id;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->base = config('s3.sibme_base_url');
        $this->module_url = 'Api/huddle_list';
        //
    }
    
  
    public function get_assessment_tracker_huddles(Request $request) {
        $input = $request->all();
        $account_id = $input['account_id'];
        $user_id = $input['user_id'];
        $role_id = $input['role_id'];
        
        $tracker_enable = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'enable_tracker'))->first();
        if($tracker_enable)
        {
            $enable_coaching_tracker = isset($tracker_enable['meta_data_value']) ? $tracker_enable['meta_data_value'] : "0";
        }
        else {
            
            $enable_coaching_tracker = "0";
        }
        
        $tracker_enable = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'enable_matric'))->first();
        if($tracker_enable)
        {
            $enable_assessment_tracker = isset($tracker_enable['meta_data_value']) ? $tracker_enable['meta_data_value'] : "0";
        }
        else {
            
            $enable_assessment_tracker = "0";
        }
        
        $tracker_enable = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'enabletracking'))->first();
        if($tracker_enable)
        {
            $enable_tracking = isset($tracker_enable['meta_data_value']) ? $tracker_enable['meta_data_value'] : "0";
        }
        else {
            
            $enable_tracking = "0";
        }
        
        if(($enable_coaching_tracker == "1" || $enable_assessment_tracker == "1" ) && ($role_id == 100 || $role_id == 110 || $role_id == 115 ) )
        {
            $tracker_permission = true;
        }
        else
        {
            $tracker_permission = false;
        }
        
        $page = $input['page'];
        $user_current_account = $input['user_current_account'];
        $folder_id = (isset($input['folder_id']) && !empty($input['folder_id'])) ? $input['folder_id'] : false;
        $title = (isset($input['title']) && !empty($input['title'])) ? $input['title'] : "";
        $huddle_sort = (isset($input['huddle_sort']) && !empty($input['huddle_sort'])) ? $input['huddle_sort'] : '0';
        $huddle_type = (isset($input['huddle_type']) && !empty($input['huddle_type'])) ? $input['huddle_type'] : '0';
        $lang = (isset($input['lang']) && !empty($input['lang'])) ? $input['lang'] : 'en';
        
        $limit = 10;
        
        $accountFolderUsers = AccountFolderUser::where('user_id', $user_id)->where('site_id', $this->site_id)->get();
        $accountFolderGroups = UserGroup::get_user_group($user_id, $this->site_id);
        
        if ($accountFolderUsers && count($accountFolderUsers) > 0) {
            foreach ($accountFolderUsers as $row) {
                $accountFoldereIds [] = $row->account_folder_id;
            }
        } else {
            $accountFoldereIds = '0';
        }
        if ($accountFolderGroups && count($accountFolderGroups) > 0) {
            foreach ($accountFolderGroups as $row) {
                $accountFolderGroupsIds[] = $row['account_folder_id'];
            }
        } else {
            $accountFolderGroupsIds = '0';
        }
       
        if (!empty($title)) {
            $search_bool = 1;
        } else {
            $search_bool = 0;
        }
        
      $final_array = array();  
      $huddles = AccountFolder::getAllAssessmentTrackerHuddles($this->site_id, $account_id, '', FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id, $search_bool, $limit, $page, false, $title, $huddle_sort, $huddle_type, $role_id);
      $huddles_count = AccountFolder::getAllAssessmentTrackerHuddles($this->site_id, $account_id, '', FALSE, $accountFoldereIds, $accountFolderGroupsIds, $folder_id, $search_bool, $limit, $page, true, $title, $huddle_sort, $huddle_type, $role_id);
      
        if ($huddles) {
            $huddles_temp = array();
            foreach ($huddles as $key => $huddle) {
                // dd($huddle->v_total);
                // echo $huddle->folderType . "<br>";
           



                $huddle_users = AccountFolder::getHuddleUsers($huddle->account_folder_id, $this->site_id);
                $huddle_group_users = AccountFolder::getHuddleGroupsUsers($huddle->account_folder_id, $this->site_id);

                $participants_arranged = array();
                $participants = array();
                $huddle_name_type = '';

                if (!empty($huddle_groups)) {

                    foreach ($huddle_groups as $huddle_group) {

                        $participants_arranged[] = array(
                            'user_id' => $huddle_group['id'],
                            'role_id' => $huddle_group['role_id'],
                            'user_name' => $huddle_group['first_name'] . ' ' . $huddle_group['last_name'],
                            'image' => $huddle_group['image'],
                            'user_email' => $huddle_group['email']
                        );
                    }
                }

                if (!empty($huddle_users)) {

                    foreach ($huddle_users as $huddle_user) {
                        $participants_arranged[] = array(
                            'user_id' => $huddle_user['user_id'],
                            'role_id' => $huddle_user['role_id'],
                            'user_name' => $huddle_user['first_name'] . ' ' . $huddle_user['last_name'],
                            'image' => $huddle_user['image'],
                            'user_email' => $huddle_user['email']
                        );
                    }

                    $participants = array();
                    $huddle_name_type = TranslationsManager::get_translation('huddle_list_collaboration', $this->module_url, $this->site_id, $user_id);
                    $huddle_name = 'collaboration';
                    if ($huddle->folderType == 'assessment') {
                        $role_200 = 'assessor';
                        $role_210 = 'assessed_participants';
                        $role_220 = '';
                        $huddle_name_type = TranslationsManager::get_translation('huddle_list_assessment', $this->module_url, $this->site_id, $user_id);
                        $huddle_name = 'assessment';
                    } elseif ($huddle->folderType == 'coaching') {

                        $role_200 = 'coach';
                        $role_210 = 'coachee';
                        $role_220 = '';
                        $huddle_name_type = TranslationsManager::get_translation('coaching_list_coaching', $this->module_url, $this->site_id, $user_id);
                        $huddle_name = 'coaching';
                    } else {

                        $role_200 = 'collaboration_participants';
                        $role_210 = 'collaboration_participants';
                        $role_220 = 'collaboration_participants';
                        $huddle_name_type = TranslationsManager::get_translation('huddle_list_collaboration', $this->module_url, $this->site_id, $user_id);
                        $huddle_name = 'collaboration';
                    }
                    
                    

                    foreach ($participants_arranged as $row) {
                        if ($row['role_id'] == '200') {
                            $row['role_name'] = 'Admin';
                            $participants[$role_200][] = $row;
                        }
                    }
                }
                
                $assessors_name = '';
                $loop_counter = 0;
                $assessors_names_array = array();
              if($participants)
              {
                foreach ($participants['assessor'] as $assessor)
                {
                  $assessors_names_array[] = $assessor['user_name'];  
                }
               $user_name = array_column($participants['assessor'], 'user_name');
               $user_name = array_map('strtolower', $user_name);
               array_multisort($user_name, SORT_ASC, SORT_STRING , $participants['assessor']);
              }
               $assessors_name = $this->assessor_names_arrangement($assessors_names_array,$loop_counter);
               
              $assessment_huddle_essentials = $this->assessment_huddle_essentials($huddles[$key]['account_folder_id'],$huddles[$key]['published_date'],$huddles[$key]['due_date'],$lang);
                
                $huddle->assessor_count =  count($assessors_names_array);
                $huddle->assessors_name =  $assessors_name;
                $huddle->participants = $participants; //$huddle_participants['participants'];
                $huddle->huddle_type = $huddle_name_type; //$huddle_participants['huddle_name_type'];
                $huddle->title = $huddles[$key]['name'];
                $huddle->assessment_huddle_essentials = $assessment_huddle_essentials;

                $huddles_temp[] = $huddle->toArray();
            }

            $final_array['huddles'] = $huddles_temp;
            $final_array['total_huddles'] = $huddles_count;
            $final_array['tracker_permission'] = $tracker_permission;
            $final_array['enable_coaching_tracker'] = $enable_coaching_tracker;
            $final_array['enable_assessment_tracker'] = $enable_assessment_tracker;
            
            
            
      
      return response()->json($final_array, 200, [], JSON_PRETTY_PRINT);
        

        }
    }

    public function get_assessment_tracker_huddle_assessees(Request $request) {
        $input = $request->all();
        $final_data = array();
        $huddle_id = $input['huddle_id'];
        $user_current_account = $input['user_current_account'];
        if(isset($input['lang']))
        {
            $lang = $input['lang'];
        }
        else
        {
            $lang = 'en';
        }
        
        $current_user_id = $user_current_account['User']['id'];
        $current_user_role = $user_current_account['roles']['role_id'];
        
        $final_data['huddle_detail'] = AccountFolderUser::getAssesseesList($this->site_id, $huddle_id, $current_user_id, $current_user_role,true,$lang);
        
        $user_name = array_column($final_data['huddle_detail'], 'user_name');
        $user_name = array_map('strtolower', $user_name);
        array_multisort($user_name, SORT_ASC, SORT_STRING , $final_data['huddle_detail']);
        
        return response()->json($final_data, 200, [], JSON_PRETTY_PRINT);

    }
    
    function assessor_names_arrangement($assessors_names_array,$loop_counter)
    {
    
        usort($assessors_names_array, 'strnatcasecmp');
        $assessors_name = '';
        foreach($assessors_names_array as $name)
        { 
            if(count($assessors_names_array) >= 3)
            {
                if($loop_counter < 3)
                { 

                    if($loop_counter < 2)
                        $assessors_name .=  $name . ', ';
                    else
                        $assessors_name .=  $name ; 
                }
            }

            if(count($assessors_names_array) == 2)
            {
                if($loop_counter < 2)
                { 

                    if($loop_counter < 1)
                        $assessors_name .=  $name . ', ';
                    else
                        $assessors_name .=  $name ; 
                }
            }

            if(count($assessors_names_array) == 1)
            {
                $assessors_name .=  $name ; 
            }

            $loop_counter++;
        }
        return $assessors_name;
                    
    }
    
    
    function assessment_huddle_essentials($huddle_id,$published_date,$submission_deadline_date,$lang = 'en')
    {
        
        if($lang == 'es')
        {
            $unpublihsed_trans = 'No publicada';
        }
        else {
            
            $unpublihsed_trans = 'Unpublished';
        }
            $resource_submission_allowed = AccountFolderMetaData
                ::where(array(
                    "account_folder_id" => $huddle_id,
                    "site_id" => $this->site_id,
                    "meta_data_name" => "resource_submission_allowed"
                ))->first();
            
            $final_array = array();
            
            if(!empty($published_date))
            {
                if($lang == 'es')
                {
                 $published_date = $this->SpanishDate(strtotime($published_date),'most_common');  
                }
                else
                {
                $time_date = strtotime($published_date);
                            $ano = date('Y',$time_date);
                            $mes = date('M',$time_date);
                            $dia = date('d',$time_date);
                $published_date =   $mes.', '.$dia .', ' .$ano;
                }
              $final_array['published_date'] =  $published_date; 
            }
            else {
               $final_array['published_date'] =  $unpublihsed_trans;  
            }
            
            if($resource_submission_allowed)
            {
                $resource_submission_allowed = $resource_submission_allowed->toArray();
                $final_array['resource_submission_allowed'] = $resource_submission_allowed['meta_data_value'];
            }
            else{
                $resource_submission_allowed['meta_data_value'] = '';
                $final_array['resource_submission_allowed'] = $resource_submission_allowed['meta_data_value'];
            }
             
             
            if(!empty($submission_deadline_date))
            {
                if($lang == 'es')
                {
                    $submission_deadline_date =   $this->SpanishDate(strtotime($submission_deadline_date),'most_common');
                }
                else {
                    
                    $time_date = strtotime($submission_deadline_date);
                    $ano = date('Y',$time_date);
                    $mes = date('M',$time_date);
                    $dia = date('d',$time_date);
                    $submission_deadline_date =   $mes.', '.$dia .', ' .$ano;
                    
                }

                $final_array['submission_deadline_date'] = $submission_deadline_date;
            }
            else{
                $final_array['submission_deadline_date'] = '';
            }
             
             $submission_deadline_time = AccountFolderMetaData
                ::where(array(
                    "account_folder_id" => $huddle_id,
                    "site_id" => $this->site_id,
                    "meta_data_name" => "submission_deadline_time"
                ))->first();
             
             if($submission_deadline_time)
            {
                $submission_deadline_time = $submission_deadline_time->toArray();
                $final_array['submission_deadline_time'] = $submission_deadline_time['meta_data_value'];
            }
            else{
                $submission_deadline_time['meta_data_value'] = '';
                $final_array['submission_deadline_time'] = $submission_deadline_time['meta_data_value'];
            }
             
             $submission_allowed = AccountFolderMetaData
                ::where(array(
                    "account_folder_id" => $huddle_id,
                    "site_id" => $this->site_id,
                    "meta_data_name" => "submission_allowed"
                ))->first();
             
             
            if($submission_allowed)
            {
                $submission_allowed = $submission_allowed->toArray();
                $final_array['submission_allowed'] = $submission_allowed['meta_data_value'];
            }
            else{
                $submission_allowed['meta_data_value'] = '';
                $final_array['submission_allowed'] = $submission_allowed['meta_data_value'];
            }
            
          return $final_array;   
        
    }
    
    function SpanishDate($FechaStamp,$location = '')
        {
           $ano = date('Y',$FechaStamp);
           $mes = date('n',$FechaStamp);
           $dia = date('d',$FechaStamp);
           $time = date('h:i A',$FechaStamp);
           
           $diasemana = date('w',$FechaStamp);
           $diassemanaN= array("Domingo","Lunes","Martes","Mi�rcoles",
                          "Jueves","Viernes","S�bado");
           $mesesN=array(1=>"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio",
                     "Agosto","Septiembre","Octubre","Noviembre","Diciembre");
           if($location == 'dashboard')
           {
              $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic"); 
              return $mesesN[$mes]. ' '  .$dia ;
           }
           elseif($location == 'trackers' )
           {
            $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $dia . '-'.$mesesN[$mes] ; 
           }
           elseif($location == 'trackers_filter' )
           {  
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $mesesN[$mes] ; 
           }
           elseif($location == 'archive_module' )
           {  
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $mesesN[$mes].' '.$dia.', '.$ano ; 
           }
            elseif($location == 'discussion_time' )
            {  
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $time ; 
            }
            
            elseif($location == 'discussion_date' )
            {  
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $mesesN[$mes].' '.$dia.', '.$ano . ' '.$time ; 
            }
            
            elseif($location == 'most_common' )
            {  
             $mesesN=array(1=>"Ene","Feb","March","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $mesesN[$mes].' '.$dia.', '.$ano ; 
            }
            
           //return $diassemanaN[$diasemana].", $dia de ". $mesesN[$mes] ." de $ano";
        }
        
         public function isHuddleAllowed(Request $request) {
             
             $input = $request->all();
             $huddle_id = $input['huddle_id'];
             $user_id = $input['user_id'];
             $final_array = array();
             
             $result = AccountFolderUser
                ::where(array(
                    "user_id" => $user_id,
                    "account_folder_id" => $huddle_id
                ))->first();
             
             
             if($result)
             {
                $final_array['allowed'] = true; 
                return response()->json($final_array, 200, [], JSON_PRETTY_PRINT);
             }
             else
             {
                $final_array['allowed'] = false; 
                return response()->json($final_array, 200, [], JSON_PRETTY_PRINT);
                 
             }
             
             
             
         }
         
         
    public function coaching_tracker_video_detail(Request $request) {

        $user = $request->get('user_current_account');
        $account_id = $user['accounts']['account_id'];
        $user_role_id = $user['roles']['role_id'];
        $user_id = $user['User']['id'];
        $document_id = $request->get('document_id');
        $metric_old = AccountMetaData::where(array('account_id' => $account_id))->whereRaw('meta_data_name like "metric_value_%"')->orderBy('meta_data_value')->get()->toArray();
        $metric_new = array();
        foreach ($metric_old as $metric) {
            $metric_new [] = array('name' => strtolower(substr($metric['meta_data_name'], 13)), 'value' => $metric['meta_data_value']);
        }
        
        $coaching_perfomance_level = AccountMetaData::where(array('account_id' => $account_id, 'meta_data_name' => 'coaching_perfomance_level', 'site_id' => $this->site_id))->first();
            if ($coaching_perfomance_level) {
                $coaching_perfomance_level = $coaching_perfomance_level->toArray();
            }
        $coaching_perfomance_level = isset($coaching_perfomance_level['meta_data_value']) ? $coaching_perfomance_level['meta_data_value'] : "0";

        
       $video_tags =  AccountTag::select('AccountTag.tag_title','AccountTag.tads_code','AccountTag.account_tag_id')
                ->selectRaw('act.created_date AS AccountTag__tags_date')
                ->selectRaw('COUNT(act.account_comment_tag_id) AccountTag__total_tags')
                ->join('account_comment_tags as act' , 'AccountTag.account_tag_id' , '=' , 'act.account_tag_id')
                ->where(array('AccountTag.tag_type' => 0,'act.ref_id' => $document_id))
                ->groupBy('AccountTag.account_tag_id')
                ->orderBy('AccountTag__total_tags',"DESC")
                ->get()
                ->toArray();
                

        $account_coach_tags_analytics = $video_tags;

        $standards_settings = false;
        $performace_level_switch = false;

        $mt_arr = array();
        foreach ($metric_old as $mt_old) {
            $mt_arr[] = substr($mt_old['meta_data_name'], 13);
        }

        $assessments = array(0 => 'No Rating');
        $arr_all = array_merge($assessments, $mt_arr);



        foreach ($account_coach_tags_analytics as $key => $row) {


            $framework_details = AccountTag::where(array('account_tag_id' => $row['account_tag_id'], 'account_id' => $account_id))->first();
            if($framework_details)
            {
                $framework_details = $framework_details->toArray();
            }

            if (!empty($framework_details['framework_id'])) {
                $framework_id = $framework_details['framework_id'];
                $standards_settings = AccountFrameworkSetting::where(array(
                        "account_id" => $account_id,
                        "account_tag_id" => $framework_id
                ))->first()->toArray();
                if (isset($standards_settings['enable_performance_level'])) {
                    $performace_level_switch = $standards_settings['enable_performance_level'];
                } else {
                    $performace_level_switch = false;
                }
            }


            if ($performace_level_switch) {
                $mt_arr = array();
                $account_framework_settings_performance_levels = AccountFrameworkSettingPerformanceLevel::where(array(
                        'AccountFrameworkSettingPerformanceLevel.account_framework_setting_id' => $standards_settings['id']
                    ))->orderBy('performance_level_rating')->get()->toArray();

                foreach ($account_framework_settings_performance_levels as $pl) {
                    $mt_arr[] = $pl['performance_level'];
                }

                $assessments = array(0 => 'No Rating');
                $arr_all = array_merge($assessments, $mt_arr);
            }



            $ratings_array = DocumentStandardRating::where(array('document_id' => $document_id, 'standard_id' => $row['account_tag_id'], 'account_id' => $account_id  ))->get()->toArray();
            $count = 0;
            $avg_sum = 0;
            $avg = 0;
            foreach ($ratings_array as $values) {



                $avg = $avg + (int) $values['rating_value'];
                $count++;
            }
            if ($count > 0) {

                $final_average = round($avg / $count);
            } else {
                $final_average = 0;
            }
            if ($final_average == 0) {
                $account_coach_tags_analytics[$key]['label'] = 'No Rating';
            }

            if ($final_average > 0) {
                $final_average_name = '';

                if ($performace_level_switch) {
                    foreach ($account_framework_settings_performance_levels as $pl) {
                        if ($final_average == $pl['performance_level_rating']) {
                            $final_average_name = $pl['performance_level'];
                        }
                    }
                } else {

                    foreach ($metric_new as $metric) {
                        if ($final_average == $metric['value']) {
                            $final_average_name = $metric['name'];
                        }
                    }
                }
                
                    if ($coaching_perfomance_level == '0') {
                        $final_average = 0;
                        $final_average_name = 'No Ratings';
                    }

                $account_coach_tags_analytics[$key]['label'] = $final_average_name;
                $account_coach_tags_analytics[$key]['average_rating'] = $final_average;
                $account_coach_tags_analytics[$key]['color_rating'] = '#000';
            }
        } 



        
       $doc_count = Document::select('documents.*', 'afd.title', 'afd.desc', 'afd.account_folder_id')
                ->join('account_folder_documents as afd','documents.id' ,'=','afd.document_id' )
                ->join('account_folderdocument_attachments as afda','afda.account_folder_document_id','=','afd.id')
                ->where(array('doc_type' => '2'))->whereRaw('afda.attach_id=' . $document_id)->count();
       
        
        $query = Document::select('documents.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'afd.account_folder_id')
                ->join('account_folder_documents as afd','documents.id','=','afd.document_id')
                ->leftJoin('users as User','documents.created_by','=','afd.document_id');
        
        if($user_role_id == 115)
        {
            $query = $query->join('account_folder_users as afu','afd.account_folder_id','=','afd.account_folder_id');
            $query = $query->where(array('afu.role_id' => 200,'afu.user_id' => $user_id));
        }
        $query = $query->where(array('documents.id' => $document_id))->whereIn('doc_type',array(1,3));
        
        $video_detail = $query->get()->toArray();
     
        $h_id = AccountFolderDocument::select('afu.user_id')
                ->leftJoin('account_folder_users as afu','afu.account_folder_id','=','account_folder_documents.account_folder_id')
                ->where(array('account_folder_documents.document_id' => $document_id, 'afu.role_id' => 200))->get()->toArray();
                

        $coach_users = array();
        foreach ($h_id as $coach_huddles) {
            $coach_users[] = $coach_huddles['user_id'];
        }

        $coach_feedback = Comment::where(array(
                'ref_id' => $document_id,
                'ref_type' => 4
            ))->whereIn('user_id',$coach_users)->orderBy('created_date',"DESC")->first();
        
        if($coach_feedback)
        {
            $coach_feedback = $coach_feedback->toArray();
        }
        else {
           $coach_feedback = array(); 
        }
        
         $document = Document::where(array('id' => $document_id))->first()->toArray();
         $account_folder_document = AccountFolderDocument::where(array('document_id' => $document_id))->first()->toArray();
         
         $video_data = app('App\Http\Controllers\VideoController')->get_document_url($document);          
         $document['thumbnail_url'] = $video_data['thumbnail'];
         $document['huddle_id'] = $account_folder_document['account_folder_id'];
        
        $final_array = array();
        $final_array = array(
            'coach_feedback' => $coach_feedback,
            'video_detail' => $video_detail,
            'video_tags' => $account_coach_tags_analytics,
            'document' => $document,
            'rating_names' => $arr_all,
            'coaching_perfomance_level' => $coaching_perfomance_level
            );
        
        return response()->json($final_array, 200, [], JSON_PRETTY_PRINT);
    
        
    }
    
    
    public function coaching_tracker_note(Request $request) {
        $users = $request->get('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];

        if (empty($request->get('coach_feedback_id'))) {
            $data = array(
                'title' => '',
                'comment' => $request->get('coach_response'),
                'ref_id' => $request->get('document_id'),
                'ref_type' => '4',
                'created_date' => date('y-m-d H:i:s', time()),
                'created_by' => $user_id,
                'last_edit_date' => date('y-m-d H:i:s', time()),
                'last_edit_by' => $user_id,
                'user_id' => $user_id,
                'site_id' => $this->site_id,
            );
            DB::table('comments')->insert($data);
            return response()->json(['status' => true], 200, [], JSON_PRETTY_PRINT);
            
        } else {
            
            DB::table("comments")->where("id", $request->get('coach_feedback_id'))->update(["comment" => $request->get('coach_response')]);
            return response()->json(['status' => true], 200, [], JSON_PRETTY_PRINT);
        }
    }
    
    
    public function remove_tracking(Request $request) {

        $users = $request->get('user_current_account');
        $account_id = $users['accounts']['account_id'];
        $user_id = $users['User']['id'];
        
        $data = array(
            'meta_data_name' => 'remove_coaching_tracking',
            'meta_data_value' => 1,
            'document_id' => $request->get('document_id'),
            'created_date' => date('y-m-d H:i:s', time()),
            'created_by' => $user_id,
            'last_edit_date' => date('y-m-d H:i:s', time()),
            'last_edited_by' => $user_id,
            'site_id' => $this->site_id
        );
        
        

        if (DB::table('document_meta_data')->insert($data)) {
            return response()->json(['status' => true], 200, [], JSON_PRETTY_PRINT);
        }
        else
        {
            return response()->json(['status' => false], 200, [], JSON_PRETTY_PRINT);
        }
    }
    
    
    
public function video_standard_tags_with_performance_rating($account_id,$document_id)
    {
    
        $sum = 0;
        $metric_old = AccountMetaData::where(array('account_id' => $account_id))->whereRaw('meta_data_name like "metric_value_%"')->orderBy('meta_data_value')->get()->toArray();
        $metric_new = array();
        foreach ($metric_old as $metric) {
            $metric_new [] = array('name' => strtolower(substr($metric['meta_data_name'], 13)), 'value' => $metric['meta_data_value']);
        }
    
        $video_tags =  AccountTag::select('AccountTag.tag_title','AccountTag.tads_code','AccountTag.account_tag_id')
                ->selectRaw('act.created_date AS AccountTag__tags_date')
                ->selectRaw('COUNT(act.account_comment_tag_id) AccountTag__total_tags')
                ->join('account_comment_tags as act' , 'AccountTag.account_tag_id' , '=' , 'act.account_tag_id')
                ->where(array('AccountTag.tag_type' => 0,'act.ref_id' => $document_id))
                ->groupBy('AccountTag.account_tag_id')
                ->orderBy('AccountTag__total_tags',"DESC")
                ->get()
                ->toArray();
                

        $account_coach_tags_analytics = $video_tags;

        $standards_settings = false;
        $performace_level_switch = false;

        $mt_arr = array();
        foreach ($metric_old as $mt_old) {
            $mt_arr[] = substr($mt_old['meta_data_name'], 13);
        }

        $assessments = array(0 => 'No Rating');
        $arr_all = array_merge($assessments, $mt_arr);



        foreach ($account_coach_tags_analytics as $key => $row) {


            $framework_details = AccountTag::where(array('account_tag_id' => $row['account_tag_id'], 'account_id' => $account_id))->first();
            if($framework_details)
            {
                $framework_details = $framework_details->toArray();
            }

            if (!empty($framework_details['framework_id'])) {
                $framework_id = $framework_details['framework_id'];
                $standards_settings = AccountFrameworkSetting::where(array(
                        "account_id" => $account_id,
                        "account_tag_id" => $framework_id
                ))->first()->toArray();
                if (isset($standards_settings['enable_performance_level'])) {
                    $performace_level_switch = $standards_settings['enable_performance_level'];
                } else {
                    $performace_level_switch = false;
                }
            }


            if ($performace_level_switch) {
                $mt_arr = array();
                $account_framework_settings_performance_levels = AccountFrameworkSettingPerformanceLevel::where(array(
                        'AccountFrameworkSettingPerformanceLevel.account_framework_setting_id' => $standards_settings['id']
                    ))->orderBy('performance_level_rating')->get()->toArray();

                foreach ($account_framework_settings_performance_levels as $pl) {
                    $mt_arr[] = $pl['performance_level'];
                }

                $assessments = array(0 => 'No Rating');
                $arr_all = array_merge($assessments, $mt_arr);
            }



            $ratings_array = DocumentStandardRating::where(array('document_id' => $document_id, 'standard_id' => $row['account_tag_id'], 'account_id' => $account_id))->get()->toArray();
            $count = 0;
            $avg_sum = 0;
            $avg = 0;
            foreach ($ratings_array as $values) {



                $avg = $avg + (int) $values['rating_value'];
                $count++;
            }
            if ($count > 0) {

                $final_average = round($avg / $count);
            } else {
                $final_average = 0;
            }
            if ($final_average == 0) {
                $account_coach_tags_analytics[$key]['label'] = 'No Rating';
            }

            if ($final_average > 0) {
                $final_average_name = '';

                if ($performace_level_switch) {
                    foreach ($account_framework_settings_performance_levels as $pl) {
                        if ($final_average == $pl['performance_level_rating']) {
                            $final_average_name = $pl['performance_level'];
                        }
                    }
                } else {

                    foreach ($metric_new as $metric) {
                        if ($final_average == $metric['value']) {
                            $final_average_name = $metric['name'];
                        }
                    }
                }

                $account_coach_tags_analytics[$key]['label'] = $final_average_name;
                $account_coach_tags_analytics[$key]['average_rating'] = $final_average;
                $account_coach_tags_analytics[$key]['color_rating'] = '#000';
            }
            
            $sum = $sum + $final_average;
        }
        $rating_name = 'No Rating';
        if(count($account_coach_tags_analytics) > 0)
        {
            $total_avg = $sum/count($account_coach_tags_analytics);
        }
        else {
            $total_avg = 0;
        }
        
        foreach($arr_all as $key => $rating)
        {
            if(round($total_avg) == $key)
            {
                $rating_name = $rating;
            }
        }
        
        return $rating_name;
        
    }
         


}
