<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Api\TrialSignUp\TrialSignUp;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\UserAccount;
use App\Models\Account;
use App\Services\TranslationsManager;
use Validator;
use DB;

class TrialSignUpController extends Controller {

    protected $client;
    protected $api_key;
    protected $contact_url;
    protected $company_url;
    protected $association_url;
    protected $contact_list_url;
    protected $sample_huddle;
    protected $salt;
    protected $tsignup;
    protected $baseurl;
    protected $httpreq;
    protected $site_id;
    protected $module_url;

    public function __construct(Request $request, TrialSignUp $tsignup) {
        $this->client = new \GuzzleHttp\Client(['http_errors' => false]);
        $this->api_key = config('apikeys.hubspot_api_key');
        $this->contact_url = config('hubspot.contact_endpoint');
        $this->company_url = config('hubspot.company_endpoint');
        $this->association_url = config('hubspot.association_url');
        $this->contact_list_url = config('hubspot.contact_lists');
        $this->sample_huddle = config('apikeys.sample_huddle');
        $this->salt = config('apikeys.salt');
        $this->tsignup = $tsignup;
        $this->baseurl = env('APP_URL');
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
        $this->module_url = 'Api/online_trial';
    }

    public function trial_signup(Request $request) {
        $data = $request->all();
        $data['trial_start_date'] = date('Y-m-d');
        $data['trial_end_date'] = date('Y-m-d', strtotime("+30 days"));
        $validator = Validator::make(
                        [
                    'full_name' => @$data['full_name'],
                    'username' => @$data['email'],
                    'password' => @$data['password'],
                    'password_confirmation' => @$data['password_confirmation'],
                    'email' => @$data['email'],
                    'company' => @$data['company'],
                    'current_role' => @$data['current_role']
                        ], [
                    'full_name' => 'required',
                    'username' => 'required',
                    'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
                    'password_confirmation' => 'min:8',
                    'email' => 'required|email|max:255',
                    'company' => 'required',
                    'current_role' => 'required'
                        ]
        );
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $verification_code = uniqid();
        $account_id = $this->tsignup->save_accounts($data, $this->site_id);
        $user_id = $this->tsignup->save_users($data, $this->salt, $verification_code, $this->site_id);
        app('App\Http\Controllers\AccountsController')->update_account_folders_meta_data_params($account_id,$user_id,'1','enable_tags');
        app('App\Http\Controllers\AccountsController')->update_account_folders_meta_data_params($account_id,$user_id,'1','enable_tags_ws');
        $this->tsignup->update_auth_userid($user_id, $this->site_id);
        $uaccid = $this->tsignup->save_users_accounts($user_id, $account_id, $this->site_id);
        $tag_id = $this->tsignup->save_account_tags($user_id, $account_id, $this->site_id);
        //Commented due to wrong comments. SW-3426
        //$this->tsignup->copyHuddleForSample($this->sample_huddle, $account_id, $user_id, $this->site_id);
        $this->tsignup->send_welcome_email_template($data, $account_id, $this->site_id);
        $data['verification_code'] = $verification_code;
        $data['user_id'] = $user_id;
        $data['account_id'] = $account_id;
        $this->tsignup->activation_email($data, $account_id, $this->site_id);
        $reqdata = $this->tsignup->transformers($data, $account_id, $uaccid);
        if (!empty($data['colleague_email'])) {

            $new_signup_user_session = array();
            $new_signup_user_session['accounts']['company_name'] = @$data['company'];
            $new_signup_user_session['User']['id'] = $user_id;
            $split_name = $this->split_name(@$data['full_name']);
            $fname = $split_name[0];
            $lname = $split_name[1] ? $split_name[1] : '';
            $new_signup_user_session['User']['first_name'] = $fname;
            $new_signup_user_session['User']['email'] = trim($data['email']);
            $new_signup_user_session['User']['last_name'] = $lname;
            $new_signup_user_session['accounts']['account_id'] = $account_id;


            $colleguename = @$data['colleague_name'] ? @$data['colleague_name'] : 'No Name';
            $this->_addUser($colleguename, $data['colleague_email'], '', $data, $new_signup_user_session);
        }
        if ($this->site_id == 2) {
            $this->tsignup->upGradePlantoUnlimited($account_id);
        }


        if ($data['current_lang'] == 'es') {
            return response()->json(['success' => true, 'response' => [], 'resend_link' => $this->baseurl . 'resend_activation_code/' . $account_id . '/' . $user_id]);
        } else {
            $getarray = json_decode($reqdata, true);
            $properties = !empty($getarray['properties']) ? array_column($getarray['properties'], 'property') : false;
            if($properties==false){
                return response()->json(['success' => true, 'response' => [], 'resend_link' => $this->baseurl . 'resend_activation_code/' . $account_id . '/' . $user_id]);
            }
            return $this->hubspot_integration($data, $reqdata, $account_id, $user_id);
        }
    }

    public function hubspot_integration($data, $reqdata, $account_id, $user_id) {


        $getarray = json_decode($reqdata, true);
        $properties = !empty($getarray['properties']) ? array_column($getarray['properties'], 'property') : false;       
        $key = array_search('email', $properties);
        $splitdata = explode("@", $getarray['properties'][$key]['value']);
        $get_company_domain = $splitdata[1];
        $result = $this->company_exists($get_company_domain);
        $signup_online_trial_value = TranslationsManager::get_translation('signup_online_trial_value', $this->module_url, $this->site_id, '');
        $index = array_search('company', $properties);
        if (isset($index)) {
            unset($getarray['properties'][$index]);
            $newindex = sizeof($getarray['properties']);
        }
        if (isset($result[0]) && $this->check_if_parent_company($result[1])) {
            $getarray['properties'][$newindex]['property'] = 'company';
            $getarray['properties'][$newindex]['value'] = $result[0];
        } else {
            $getarray['properties'][$newindex]['property'] = 'company';
            $getarray['properties'][$newindex]['value'] = $signup_online_trial_value;
        }
        $reqdata = json_encode($getarray);
        $insert_url = $this->contact_url . '?hapikey=' . $this->api_key;
        $response = $this->client->post($insert_url, [
            'debug' => false,
            'body' => $reqdata,
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        $resp = json_decode($response->getBody()->getContents(), true);
        $statuscode = $response->getStatusCode();
        if ($response->getStatusCode() == 409 && $resp['error'] == 'CONTACT_EXISTS') {
            $email = $resp['identityProfile']['identity'][0]['value'];
            $statuscode = $this->contact_update_by_email($email, $reqdata);
            if ($statuscode == 204) {
                $vid = $resp['identityProfile']['vid'];
                $this->add_to_contact_list($vid, trim($data['email']));
                return response()->json(['success' => true, 'message' => TranslationsManager::get_translation('signup_contact_alreay_exist', $this->module_url, $this->site_id, ''), 'vid' => $vid, 'resend_link' => $this->baseurl . 'resend_activation_code/' . $account_id . '/' . $user_id]);
            }
        }
        if ($statuscode == 200) {
            $vid = $resp['vid'];
            $this->add_to_contact_list($vid, trim($data['email']));
            return response()->json(['success' => true, 'response' => $resp, 'resend_link' => $this->baseurl . 'resend_activation_code/' . $account_id . '/' . $user_id]);
        } else {
            return response()->json(['success' => false, 'response' => $resp]);
        }
    }

    public function contact_update_by_email($email, $reqdata) {
        $update_url = $this->contact_url . 'email/' . $email . '/profile?hapikey=' . $this->api_key;
        $response = $this->client->post($update_url, [
            'debug' => false,
            'body' => $reqdata,
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        $resp = json_decode($response->getBody()->getContents(), true);
        $statuscode = $response->getStatusCode();
        return $statuscode;
    }

    public function company_exists($domain) {
        $company_url = $this->company_url . 'domains/' . $domain . '/companies?hapikey=' . $this->api_key;
        $reqdata = '{
          "limit": 2,
          "requestOptions": {
            "properties": [
              "domain",
              "createdate",
              "name",
              "hs_lastmodifieddate"
            ]
          },
          "offset": {
            "isPrimary": true,
            "companyId": 0
          }
        }';
        $response = $this->client->post($company_url, [
            'debug' => false,
            'body' => $reqdata,
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        $resp = json_decode($response->getBody()->getContents(), true);
        if (!isset($resp['results'][0]['properties']['name']['value'])) {
            return [];
        }
        return [$resp['results'][0]['properties']['name']['value'],
            $resp['results'][0]['companyId']];
    }

    public function create_new_company() {
        $company_url = $this->company_url . 'companies?hapikey=' . $this->api_key;
        $signup_online_trial_value = TranslationsManager::get_translation('signup_online_trial_value', $this->module_url, $this->site_id, '');
        $signup_create_company_description = TranslationsManager::get_translation('signup_create_company_description', $this->module_url, $this->site_id, '');

        $reqdata = '{
                    "properties": [
                      {
                        "name": "name",
                        "value": $signup_online_trial_value
                      },
                      {
                        "name": "description",
                        "value": ' . $signup_create_company_description . '
                      }
                    ]
                  }';
        $response = $this->client->post($company_url, [
            'debug' => false,
            'body' => $reqdata,
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        $resp = json_decode($response->getBody()->getContents(), true);
        if (empty($resp['companyId'])) {
            return false;
        }
        return true;
    }

    public function add_to_contact_list($vid, $email) {
        $contact_list_url = $this->contact_list_url . '/67/add?hapikey=' . $this->api_key;
        $reqdata = '{
          "emails": [
            "' . $email . '"
          ]
        }';
        $response = $this->client->post($contact_list_url, [
            'debug' => false,
            'body' => $reqdata,
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        $resp = json_decode($response->getBody()->getContents(), true);
        if (!empty($resp['updated'])) {
            return [];
        }
        return $resp['updated'];
    }

    public function check_if_parent_company($companyid) {
        //$pc_company = $this->company_url . 'families/' . $companyid . '/children/with-total?hapikey=' . $this->api_key;
        $pc_company = $this->association_url.$companyid.'/HUBSPOT_DEFINED/13?hapikey=' . $this->api_key;
        $response = $this->client->get($pc_company, ['debug' => false]);
        $resp = json_decode($response->getBody()->getContents(), true);
        if (empty($resp['results'])) {
            return true;
        }
        return false;
    }

    public function checkbox_options(request $request) {
        $data = $request->all();
        $lang = $data['current_lang'];
        $data = $this->tsignup->generic_options($lang);
        return response()->json($data);
    }

    public function email_existance(Request $request) {
        $data = $request->all();
        $resp = $this->tsignup->check_email($data, $this->site_id);
        if (empty($resp)) {
            return response()->json(['status' => true, 'message' => TranslationsManager::get_translation('signup_email_not_exist', $this->module_url, $this->site_id, '')]);
        }


        return response()->json(['status' => false, 'message' => TranslationsManager::get_translation('signup_already_exist_msg', $this->module_url, $this->site_id, '')]);
    }

    public function resend_activation_code($account_id, $user_id) {
        $data = $this->tsignup->get_user_account_data($account_id, $user_id, $this->site_id);

        $this->tsignup->activation_email($data, $account_id, $this->site_id);

        return response()->json(['status' => true, 'message' => TranslationsManager::get_translation('signup_resend_activation_code', $this->module_url, $this->site_id, '')]);
    }

    public function _addUser($full_name, $email, $account_folder_id = '', $user_current_account, $new_signup_user_session) {
        $users = $user_current_account;
        $account_id = $users['account_id'];
        $arr = array();
        $data = array();
        if (isset($full_name)) {
            $name = explode(' ', $full_name);
            $arr['first_name'] = isset($name[0]) ? $name[0] : '';
            $arr['last_name'] = '';
            if (count($name) > 1) {
                array_shift($name);
                $arr['last_name'] = implode(" ", $name);
            }
        }
        if (isset($email)) {

            $arr['created_by'] = $users['user_id'];
            $arr['created_date'] = date('Y-m-d H:i:s');
            $arr['last_edit_by'] = $users['user_id'];
            $arr['site_id'] = $this->site_id;
            $arr['last_edit_date'] = date('Y-m-d H:i:s');
            $arr['email'] = $email;
            $arr['username'] = $email;
            $arr['authentication_token'] = app('App\Http\Controllers\ApiController')->digitalKey(20);
            $arr['is_active'] = false;
            $arr['type'] = 'Invite_Sent';
            if ($arr['first_name'] != '' && $arr['email'] != '') {
                $data[] = $arr;
            }
        }

        $new_user_id = -1;
        $roleData = array();
        foreach ($data as $user) {
            if (isset($user['first_name']) && isset($user['email']) && $user['email']) {

                $exUser = User::where(array(
                            'email' => $user['email'],
                            'site_id' => $this->site_id
                        ))->get()->toArray();

                $userAccountCount = 0;
                if (count($exUser) == 0 && DB::table('users')->insert($user)) {
                    $user_id = DB::getPdo()->lastInsertId();

                    DB::table('users')->where('id', $user_id)->where('site_id', $this->site_id)->update(['authentication_token' => md5($user_id), 'site_id' => $this->site_id]);

                    $user['authentication_token'] = md5($user_id);
                } else {
                    $user_id = $exUser[0]['id'];
                }

                if (!empty($user_id)) {

                    $userAccountCount = UserAccount::where(array(
                                'user_id' => $user_id,
                                'account_id' => $account_id,
                                'site_id' => $this->site_id
                            ))->count();

                    //Checking default account is exist or not
                    $defaultAccount = UserAccount::where(array('user_id' => $user_id, 'is_default' => 1))->count();

                    if ($userAccountCount == 0) {


                        $userAccount = array(
                            'account_id' => $account_id,
                            'user_id' => $user_id,
                            'site_id' => $this->site_id,
                            'role_id' => '110',
                            'is_default' => ($defaultAccount > 0) ? 0 : 1,
                            'created_by' => $users['user_id'],
                            'created_date' => date("Y-m-d H:i:s"),
                            'last_edit_date' => date("Y-m-d H:i:s"),
                            'last_edit_by' => $users['user_id']
                        );

                        DB::table('users_accounts')->insert($userAccount);
                        
                        $this->create_churnzero_event('User+Added', $account_id, $users['email']);

                        if ($user_id) {
                            $user['user_id'] = $user_id;
                            //  $user['message'] = $this->data['message'];
                            $user['role_title'] = 'User';
                            $user['account_id'] = $account_id;
                            $account_details = Account::where(array('id' => $account_id))->first();
                            $user['company_name'] = $account_details->company_name;
                            if (count($exUser) == 0) {
                                app('App\Http\Controllers\ApiController')->sendInvitation($user, $account_folder_id, $new_signup_user_session,$this->site_id);
                            } else {
                                app('App\Http\Controllers\ApiController')->sendConfirmation($user, $new_signup_user_session,$this->site_id);
                            }

                            $desc_user_name = $user['first_name'] . " " . isset($user['last_name']) ? $user['last_name'] : '';

                            $user_activity_logs = array(
                                'ref_id' => $account_folder_id,
                                'desc' => $desc_user_name . " registered",
                                'url' => 'Huddles',
                                'type' => '31',
                                'site_id' => $this->site_id,
                                'account_folder_id' => $account_folder_id,
                                'date_added' => date("Y-m-d H:i:s")
                            );
                            app('App\Http\Controllers\ApiController')->user_activity_logs($user_activity_logs, $users['account_id'], $users['user_id']);

                            $new_user_id = $user_id;
                        }
                    }
                }
            }
        }

        return $new_user_id;
    }

    public function test() {
        echo app('App\Http\Controllers\ApiController')->digitalKey(20);
        exit;
    }

    public function checkSpecialChar($array) {
        $pattern = '/[\'^£$%&*()}{@#~?><>,|=_+¬]/';
        foreach ($array as $k => $val) {
            if (preg_match($pattern, $val)) {
                return true;
            }
        }
        return false;
    }

    public function split_name($name) {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim(preg_replace('#' . $last_name . '#', '', $name));
        return array($first_name, $last_name);
    }
    
     function create_churnzero_event($event_name,$account_id,$user_email)
    {
           if(strpos($user_email,"@sibme.com") || strpos($user_email,"@jjtestsite.us")   )
           {
              return true;
           }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://analytics.churnzero.net/i?appKey=invw-q7Ivjwby8NI1F6qQcH1Gix0811ja7-Li4_1xWg&accountExternalId='.$account_id.'&contactExternalId='.$user_email.'&action=trackEvent&eventName='.$event_name.'&description='.$event_name.'&quantity=1' );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
        
    }
    
    public function add_to_contact_list_for_cake($vid, $list_id) {
        $contact_list_url = $this->contact_list_url . '/'.$list_id.'/add?hapikey=' . $this->api_key;
        $reqdata = '{
          "vids": [
            "' . $vid . '"
          ]
        }';
        $response = $this->client->post($contact_list_url, [
            'debug' => false,
            'body' => $reqdata,
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        $resp = json_decode($response->getBody()->getContents(), true);
        
        return $resp;
    }
    
    public function remove_from_contact_list_for_cake($vid, $list_id) {
        $contact_list_url = $this->contact_list_url . '/'.$list_id.'/remove?hapikey=' . $this->api_key;
        $reqdata = '{
          "vids": [
            "' . $vid . '"
          ]
        }';
        $response = $this->client->post($contact_list_url, [
            'debug' => false,
            'body' => $reqdata,
            'headers' => [
                'Content-Type' => 'application/json',
            ]
        ]);
        $resp = json_decode($response->getBody()->getContents(), true);
        
        return $resp;
    }
    
    
    

}
