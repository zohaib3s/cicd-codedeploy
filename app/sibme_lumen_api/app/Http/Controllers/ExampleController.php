<?php

namespace App\Http\Controllers;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use App\Services\Emails\Email;

use Carbon\Carbon;
use App\Services\S3Services;


use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use FFMpeg\FFProbe\DataMapping\StreamCollection;
use FFMpeg\FFProbe\DataMapping\Stream;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Filters\Video\ResizeFilter;

use App\Services\CodeProfiler;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    function test()
    {
        $s3 = new S3Services(
            config('s3.amazon_base_url'), config('s3.bucket_name'), config('s3.access_key_id'), config('s3.secret_access_key')
        );
        $s3->processElementalJobs();

        dd(config('s3.sibme_base_url'));
        
        $results = app('db')->select("SELECT * FROM users where email = 'saadziaps4@gmail.com' ");

        dd($results);
    }

    function test_email(){

        // Email::sendEmail($site_id, $view, $view_params_array, $to_email, $to_name, $subject);

        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = 2;                                       // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host       = env('MAIL_HOST');                       // Specify main and backup SMTP servers
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = env('MAIL_USERNAME');                                  // SMTP username
            $mail->Password   = env('MAIL_PASSWORD');                               // SMTP password
            $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = env('MAIL_PORT');                                    // TCP port to connect to
        
            //Recipients
            $mail->setFrom('waqasn@sibme.com', 'Mailer Dev Test');
            $mail->addAddress('waqasn@sibme.com', 'Joe User');     // Add a recipient
            // $mail->addAddress('ellen@example.com');               // Name is optional
            // $mail->addReplyTo('info@example.com', 'Information');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');
        
            // Attachments
            // $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        
            // Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'The subject - Mailer Dev Test';
            $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
            // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
        
            $mail->send();
            echo 'Message has been sent';
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }

    function testWaqas(){
        $prof = new CodeProfiler("test waqas");
        $prof->start();
        $custom_fields = '
                            [
                                {
                                    "custom_field_id": 11,
                                    "field_label": "sdvs",
                                    "field_type": 4,
                                    "assessment_custom_field_id": null
                                },
                                {
                                    "custom_field_id": 12,
                                    "field_label": "new ",
                                    "field_type": 2,
                                    "assessment_custom_field_id": null
                                },
                                {
                                    "custom_field_id": 15,
                                    "field_label": "new 1",
                                    "field_type": 2,
                                    "assessment_custom_field_id": null
                                },
                                {
                                    "custom_field_id": 16,
                                    "field_label": "new 2",
                                    "field_type": 4,
                                    "assessment_custom_field_id": null
                                },
                                {
                                    "custom_field_id": 17,
                                    "field_label": "new 3",
                                    "field_type": 1,
                                    "assessment_custom_field_id": null
                                }
                            ]
                        ';
        $custom_fields = '
                            [
                                {
                                    "custom_field_id": 12,
                                    "field_label": "new ",
                                    "field_type": 2,
                                    "assessment_custom_field_id": null
                                },
                                {
                                    "custom_field_id": 16,
                                    "field_label": "new 2",
                                    "field_type": 4,
                                    "assessment_custom_field_id": null
                                }
                            ]
                        ';
        $custom_fields = json_decode(json_encode($custom_fields), True);
var_dump($custom_fields);
$res = $prof->end();
dd($res);
        // dd(\App\Models\AssessmentCustomField::save_assessment_custom_fields(398162, 18737, $custom_fields));

        // \App\Services\HmhLicense::send_new_order_emails();
exit();
        $use_ffmpeg_path = config('core.use_ffmpeg_path');
        $ffmpeg_binaries = config('core.ffmpeg_binaries');
        $ffprobe_binaries = config('core.ffprobe_binaries');
        
        $lvideo_path = "https://file-examples.com/wp-content/uploads/2017/04/file_example_MP4_480_1_5MG.mp4";

        // $lvideo_path = "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4";

        /*
        $ffmpeg = FFMpeg::create(array(
            'ffmpeg.binaries' => $ffmpeg_binaries,
            'ffprobe.binaries' => $ffprobe_binaries,
        ));
*/
try{
        $ffprobe = FFProbe::create(array(
            'ffmpeg.binaries' => $ffmpeg_binaries,
            'ffprobe.binaries' => $ffprobe_binaries,
        ));
        $duration = $ffprobe->format($lvideo_path) // extracts file informations
                            ->get('duration');             // returns the duration property
        dd($duration);
} catch (\Exception $ex){
    echo $ex->getMessage();
}

        // $analyticsHuddlesSummary = new \App\Models\AnalyticsHuddlesSummary;
        // $analyticsHuddlesSummary->saveRecord(181, 8595, 18737, "workspace_videos_viewed_count");

        // $loginSummary = new \App\Models\AnalyticsLoginSummary;
        // dd($loginSummary->saveRecord(181, 621));
        
/*
        $huddlesSummary = new \App\Services\Dynamo\HuddlesSummary;
        
        dd($huddlesSummary->getHuddleSummaryForAnalytics(181, [8595], "2015-01-01", "2016-12-31"));
        // dd($huddlesSummary->saveRecord(181, 8595, 18737, "workspace_videos_viewed_count"));
        // dd($huddlesSummary->getTotalCount());
        // dd($huddlesSummary->importData());
        //    dd($huddlesSummary->createTable());


        $userLoginSummary = new \App\Services\Dynamo\UserLoginSummary;
        
        dd($userLoginSummary->getLoginCountsForAnalytics(181, [], "2016-01-12", "2016-09-12"));
    //   dd($userLoginSummary->saveRecord(181, 621));
    //   dd($userLoginSummary->getTotalCount());
    //   dd($userLoginSummary->importData());
    //    dd($userLoginSummary->createTable());
//        dd($userLoginSummary->saveLoginCount(2936, 621));
*/

        dd(Carbon::now());
        // Get all active users of active accounts:
        $users = UserAccount::get_all_users([100, 110, 115]);
        foreach ($users as $user) {
            $user = UserAccount::get($user->account_id, $user->user_id);
            dd($user);
        }
        $zencoder = new \App\Services\Zencoder();
        $zencoder->createJob($zencoder);
        exit;
        $account_id = 181 ; 
        $user_id = 621 ; 
        $document_id = 110271; 
        $coach_id = 621; 
        $coachee_id = 12329;
        $huddle_id = 51897;

        // $account_id = 181;
        // $user_id = 621;
        // $document_id = 88106;
        // $coachee_id = ;

        $details = \App\Services\AssessmentDetail::getDetails($huddle_id ,$account_id, $user_id, $document_id, $coachee_id);

        return response()->json($users);
    }

    function receive_notification(){
        $zencoder = new \App\Services\Zencoder();
        $response = $zencoder->processJobNotification();
        return response()->json($response);
    }

    function test_update_sites_entry(){
        $data = 'a:1:{s:2:"en";a:62:{s:10:"login_logo";s:15:"/img/logo_2.svg";s:11:"login_label";s:24:"Login to Coaching Studio";s:16:"signin_btn_color";s:7:"#848688";s:19:"reset_password_text";s:16:"Forgot Password?";s:25:"rest_password_descritpion";s:85:"To reset your password, enter the email address you use to sign in to {redirect_url}.";s:23:"send_me_rest_link_color";s:7:"#848688";s:14:"launchpad_logo";s:15:"/img/logo_3.svg";s:21:"launchpad_header_logo";s:15:"/img/logo_2.svg";s:22:"launchpad_search_title";s:19:"HMH Coaching Studio";s:20:"launchpad_tiles_logo";s:10:"Logo Title";s:17:"globel_video_logo";s:15:"/img/logo_3.svg";s:16:"site_header_logo";s:65:"https://s3.amazonaws.com/sibme.com/app/img/home/photo-default.png";s:15:"site_body_color";s:7:"#F3F2EA";s:17:"site_header_color";s:7:"#004061";s:20:"workspace_video_logo";s:62:"https://s3.amazonaws.com/sibme.com/app/img/video-thumbnail.png";s:25:"workspace_video_btn_color";s:7:"#ee9828";s:31:"workspace_video_btn_boder_color";s:7:"#da8b25";s:30:"workspace_recsources_btn_color";s:7:"#3dab28";s:37:"workspace_recsources_btn_border_color";s:7:"#349221";s:18:"site_support_email";s:21:"suports.hmh.sibme.com";s:17:"huddle_video_logo";s:62:"https://s3.amazonaws.com/sibme.com/app/img/video-thumbnail.png";s:18:"library_video_logo";s:62:"https://s3.amazonaws.com/sibme.com/app/img/video-thumbnail.png";s:20:"add_hudle_title_text";s:6:"huddle";s:22:"add_huddle_description";s:18:"huddle_descritpion";s:38:"permission_resend_activation_link_text";s:5:"Sibme";s:22:"video_detail_info_text";s:9:"Text info";s:28:"discussion_email_option_text";s:14:"test@gmail.com";s:8:"site_url";s:25:"https://www.hmh.sibme.com";s:10:"site_title";s:6:"HMH-CS";s:10:"login_text";s:24:"Login to Coaching Studio";s:8:"faveIcon";s:11:"favicon.ico";s:9:"g_color_1";s:7:"#7b7e81";s:9:"g_color_2";s:7:"#4c4c4e";s:9:"g_color_3";s:7:"#fcb813";s:9:"g_color_4";s:7:"#5091cd";s:26:"launchpad_uper_logo_config";a:2:{s:5:"width";s:5:"375px";s:6:"height";s:5:"109px";}s:15:"lunchpad_border";s:7:"#fcb813";s:16:"create_btn_color";s:7:"#349221";s:23:"create_btn_border_color";s:7:"#349221";s:18:"create_foloder_btn";s:7:"#da8b25";s:26:"create_folder_border_color";s:7:"#da8b25";s:16:"rubric_save_exit";s:7:"#FCB813";s:11:"rubric_next";s:7:"#5daf46";s:14:"rubric_previus";s:7:"#5091cd";s:16:"cancel_btn_color";s:7:"#6c757d";s:12:"ok_btn_color";s:7:"#5ab75c";s:16:"primary_bg_color";s:7:"#3dab28";s:20:"primary_border_color";s:7:"#349221";s:18:"secondary_bg_color";s:7:"#da8b25";s:22:"secondary_border_color";s:7:"#da8b25";s:12:"smtp_details";a:6:{s:11:"smtp_server";s:17:"smtp.sendgrid.net";s:9:"smtp_port";s:4:"2525";s:13:"smtp_username";s:15:"davew@sibme.com";s:13:"smtp_password";s:22:"5lFmwoT!qWkjkkkk123___";s:12:"from_address";s:12:"info@hmh.com";s:9:"from_name";s:3:"HMH";}s:11:"static_urls";a:5:{s:4:"help";s:29:"https://www.hmhco.com/support";s:14:"privacy_policy";s:36:"https://www.hmhco.com/privacy-policy";s:5:"terms";s:38:"https://www.hmhco.com/web-terms-of-use";s:8:"help-ios";s:29:"https://www.hmhco.com/support";s:12:"help-android";s:29:"https://www.hmhco.com/support";}s:13:"static_emails";a:5:{s:7:"support";s:15:"support@hmh.com";s:7:"contact";s:15:"contact@hmh.com";s:7:"noreply";s:22:"do-not-reply@sibme.com";s:5:"sales";s:13:"sales@hmh.com";s:4:"info";s:12:"info@hmh.com";}s:14:"header_logo_bg";s:15:"/img/logo_3.svg";s:15:"login_btn_color";s:7:"#848688";s:18:"login_border_color";s:7:"#848688";s:15:"login_text_text";s:15:"Coaching Studio";s:16:"static_apps_urls";a:2:{s:7:"android";s:63:"https://play.google.com/store/apps/details?id=com.hmh.mobileapp";s:3:"ios";s:69:"https://itunes.apple.com/us/app/hmh-coaching-studio/id1447107504?mt=8";}s:13:"email_subject";s:3:"HMH";s:18:"switching_app_urls";a:2:{s:7:"android";s:78:"https://play.google.com/store/apps/details?id=com.virblue.mystudylife&hl=en_US";s:3:"ios";s:57:"https://itunes.apple.com/pk/app/u-study/id1233654982?mt=8";}s:15:"app_store_image";s:33:"/app/img/dbimages/apple_store.png";s:16:"play_store_image";s:34:"/app/img/dbimages/google_store.png";}}';

        $arr = unserialize($data);

        print_r($arr);
        exit;

        $arr['en']['static_emails']['noreply'] = "do-not-reply@sibme.com";

        print_r(serialize($arr));
    }

    function get_caller_function(){
        // $dbt=debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,2);
        // $caller = isset($dbt[1]['function']) ? $dbt[1]['function'] : null;
        // \Log::info("---------------------- Time: ".date("H:i:m")." File: ".__FILE__." Line: ".__LINE__." -----------------", [php_sapi_name()]);
        // \Log::info("---------------------- BackTrace -----------------", [$caller]);
    }

    function getQueueStats(){
        // Redis Available functions: https://github.com/phpredis/phpredis#connection
        $queue = 'notification';
        $d = 'queues:'.$queue.':delayed';
        $r = 'queues:'.$queue.':reserved';
        // $delayed_jobs = app('redis')->lRange($d, '-inf', '+inf');
        // $reserved_jobs = app('redis')->lRange($r,'-inf', '+inf');
        $delayed_jobs = app('redis')->lRange($d, 0, -1);
        $reserved_jobs = app('redis')->lRange($r,0, -1);
        $size = app('redis')->client('list');//client('getname');//dbSize();
        dd(["Queue"=>$queue, "size"=>$size, "delayed_jobs"=>$delayed_jobs, "reserved_jobs"=>$reserved_jobs]);
    }

}
