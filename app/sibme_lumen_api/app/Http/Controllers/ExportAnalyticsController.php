<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class ExportAnalyticsController extends Controller
{
    protected $httpreq;
    protected $site_id;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        //
        $this->httpreq = $request;
        $this->site_id = $this->httpreq->header('site_id');
    }


    function ExportStandardsToExcel(Request $request)
    {        
        $input = $request->all();
        $account_id = $request->account_id ;
        $user_id = $request->user_id ;
        //$subAccount = $request->subAccount ;
        $subAccount = implode(",", $request->account_ids);
        $start_date = $request->start_date ;
        $end_date = $request->end_date ;
        $standards = isset($input['standards']) ? $request->standards : '' ;
        $framework_id = $request->framework_id ;
        $huddle_type = $request->huddle_type;
        // Returns Filename
        return response()->json(['file_name' => \App\Services\Exports\Analytics\ExportStandardsToExcel::create_request($this->site_id, $account_id, $user_id, $subAccount, $start_date, $end_date, $standards, $framework_id, $huddle_type)]);
        // return response()->json(['file_name' => 'my_test.xlsx']);

        // return \App\Services\Exports\Analytics\ExportStandardsToExcel::export($account_id, $user_id, $subAccount, $start_date, $end_date, $standards, $framework_id, $huddle_type);
    }

    function ExportStandardsToExcelAsync(Request $request)
    {
        ini_set('max_execution_time', 1000); //3 minutes
        set_time_limit(0);

        $account_id = $request->account_id ;
        $user_id = $request->user_id ;
        $subAccount = explode(",", $request->subAccount);
        $start_date = $request->start_date ;
        $end_date = $request->end_date ;
        $standards = $request->standards ;
        $framework_id = $request->framework_id;
        $huddle_type = $request->huddle_type;
        $huddle_type = $request->huddle_type;
        $file_name = $request->file_name;
        $site_id = isset($request->site_id) ? $request->site_id : $this->site_id;

        return \App\Services\Exports\Analytics\ExportStandardsToExcel::export($site_id, $account_id, $user_id, $subAccount, $start_date, $end_date, $standards, $framework_id, $huddle_type, $file_name);

    }

    function is_download_ready(Request $request){
        $file_name = $request->file_name;
        $response = [];
        $download_status = \App\Services\Exports\Analytics\ExportStandardsToExcel::is_s3_download_ready($file_name);
        $response ['download_status'] = $download_status;
        if($download_status){
            $final_file = \App\Services\Exports\Analytics\ExportStandardsToExcel::retrieve_from_s3($file_name);
            $response ['download_url'] = $final_file;
        }
        return response()->json($response);
    }

    function testWaqas(){
        $account_id = 181 ; 
        $user_id = 621 ; 
        $document_id = 110271; 
        $coach_id = 621; 
        $coachee_id = 12329;
        $huddle_id = 51897;

        // $account_id = 181;
        // $user_id = 621;
        // $document_id = 88106;
        // $coachee_id = ;

        $details = \App\Services\AssessmentDetail::getDetails($huddle_id ,$account_id, $user_id, $document_id, $coachee_id);

        return response()->json($details);
    }

}
