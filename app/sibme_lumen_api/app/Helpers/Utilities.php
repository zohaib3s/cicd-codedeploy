<?php
if (!function_exists('formatSeconds')) {
    function formatSeconds($seconds) {
        $hours = 0;
        $milliseconds = str_replace("0.", '', $seconds - floor($seconds));

        if ($seconds > 3600) {
            $hours = floor($seconds / 3600);
        }
        $seconds = $seconds % 3600;


        return str_pad($hours, 2, '0', STR_PAD_LEFT)
                . gmdate(':i:s', $seconds)
                . ($milliseconds ? ".$milliseconds" : '');
    }
}
if(!function_exists('is_local')){
    function is_local(){
        return env('APP_ENV')=='local';
    }
}
function strip_tags_deep($value){
      return is_array($value) ?array_map('strip_tags_deep', $value) : str_replace('&nbsp;',' ',strip_tags($value));
}