<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Services\CodeProfiler;

class AppServiceProvider extends ServiceProvider
{

    /**
     * All of the container singletons that should be registered.
     *
     * @var array
     */
    public $singletons = [
        // CodeProfiler::class => CodeProfiler::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
		$this->app->singleton('Illuminate\Contracts\Routing\ResponseFactory', function ($app) {
            return new \Illuminate\Routing\ResponseFactory(
                $app['Illuminate\Contracts\View\Factory'],
                $app['Illuminate\Routing\Redirector']
            );
        });

        $this->app->singleton('App\Services\CodeProfiler');
    }
}
