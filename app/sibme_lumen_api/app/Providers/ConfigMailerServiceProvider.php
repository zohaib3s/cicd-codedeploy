<?php

namespace App\Providers;

use App\Models\Sites;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Config;

class ConfigMailerServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $site_id = $this->get_site_id();
        $siteinfo = Sites::where(array( "site_id" => $site_id ))->get()->toArray();
        $smtpdata = $this->get_values($siteinfo, 'smtp_details');
        if (!empty($smtpdata)) {
            $config = array(
                'driver'     => 'smtp',
                'host'       => $smtpdata['smtp_server'],
                'port'       => $smtpdata['smtp_port'],
                'from'       => array('address' => $smtpdata['from_address'], 'name' => $smtpdata['from_name']),
                'username'   => $smtpdata['smtp_username'],
                'password'   => $smtpdata['smtp_password']
            );
            config('mail', $config);
        }
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.
    }

    public function get_values($site_metadata, $key){
        $site_data = unserialize($site_metadata[0]['site_metadata']);
        $site_data = $site_data['en'];
        if (!empty(array_key_exists($key, $site_data))) {
            return $site_data[$key];
        } else {
            return 'Sibme';
        }
    }

    public function get_site_id(){
        $site_id = 1;
        if(php_sapi_name()!='cli'){
            $url = $_SERVER['HTTP_HOST'];
            if(strpos($url, 'hmhapi') !== false){
                $site_id = 2;
            }
        }
        return $site_id;
    }
}
