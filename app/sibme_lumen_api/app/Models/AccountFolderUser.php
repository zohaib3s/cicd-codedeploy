<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountFolderUser extends Model
{

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_edit_date';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $casts = [
        'created_date' => 'string',
        'last_edit_date' => 'string',
    ];
    protected $table = 'account_folder_users as AccountFolderUser';

    public static function getCount($user_id) {
        return AccountFolderUser::where('user_id', (int) $user_id)->count();
    }
    public static function getUserHuddleRoles($account_folder_id,$user_id) {
        $data = AccountFolderUser::where('user_id', (int) $user_id)->where('account_folder_id',(int) $account_folder_id)->first();
        if($data)
        {
            return $data->toArray();
        }
        return [];
    }
    public static function getUserHuddleMaxRoles($account_folder_id,$user_id) {
        $data = AccountFolderUser::selectRaw("MAX(role_id) as role_id")
        ->where('user_id', (int) $user_id)->where('account_folder_id',(int) $account_folder_id)->first();
        
        if($data)
        {
            $result =  $data->toArray();
        }
        
        if(!empty($result['role_id'])){
            return $result;
        }
        $data = self::getUserRoleInHuddleGroup($account_folder_id, $user_id);
        if($data){
            return $data;
        }
        return [];
    }
    public static function getUserRoleInHuddleGroup($account_folder_id, $user_id) {
        $account_folder_id = (int) $account_folder_id;
        $query = AccountFolder
                        ::join('account_folder_groups as huddle_groups', 'huddle_groups.account_folder_id', '=', 'AccountFolder.account_folder_id')
                        ->join('groups as Group', 'Group.id', '=', 'huddle_groups.group_id')
                        ->join('user_groups as UserGroup', 'UserGroup.group_id', '=', 'Group.id')
                        ->select('huddle_groups.role_id')
                        ->where('AccountFolder.account_folder_id', $account_folder_id)
                        ->where('UserGroup.user_id', $user_id);
        $result = $query->first();
        if($result){
            return $result->toArray();
        } else {
            return [];
        }
    }

    public static function getUserNameCoaches($account_folder_id, $site_id) {
    	$coaches_users = AccountFolderUser::join('users', 'AccountFolderUser.user_id', '=', 'users.id')
		                ->selectRaw("CONCAT_WS(' ',users.`first_name`, users.`last_name`) AS user_name , users.email")
                        ->where('is_coach',1)
		                ->where('AccountFolderUser.site_id', $site_id)
		                ->where('account_folder_id',$account_folder_id)->get();
        return $coaches_users->toArray();
    }

    public static function getUserNameMente($account_folder_id, $site_id) {
    	$coaches_users = AccountFolderUser::join('users', 'AccountFolderUser.user_id', '=', 'users.id')
		                ->selectRaw("CONCAT_WS(' ',users.`first_name`, users.`last_name`) AS user_name, users.id,users.email")
                        ->where('is_mentee',1)
		                ->where('AccountFolderUser.site_id',$site_id)
		                ->where('account_folder_id',$account_folder_id)->get();
        return $coaches_users->toArray();
    }

    public static function getUsernameEvaluator($site_id, $account_folder_id, $evaluator_id = '') {
    	$coaches_sql = AccountFolderUser::join('users', 'AccountFolderUser.user_id', '=', 'users.id')
		                ->selectRaw("CONCAT_WS(' ',users.`first_name`, users.`last_name`) AS user_name, users.id,users.email")
                        ->where('is_coach',1)
		                ->where('AccountFolderUser.site_id',$site_id)
		                ->where('account_folder_id',$account_folder_id);

        if ($evaluator_id != '') {
        	$coaches_sql->where('user_id',$evaluator_id);
        }

		$coaches_users = $coaches_sql->get();
        return $coaches_users->toArray();
    }

    public static function getParticipants($site_id, $account_folder_id , $user_id = '') {
        
        if(!empty($user_id))
        {
            return AccountFolderUser::join('users', 'AccountFolderUser.user_id', '=', 'users.id')
		                ->selectRaw("CONCAT_WS(' ',users.`first_name`, users.`last_name`) AS user_name, users.id,users.email")
                        ->where('is_mentee',1)
		                ->where('AccountFolderUser.site_id', $site_id)
		                ->where('account_folder_id',$account_folder_id)
                                ->where('AccountFolderUser.user_id',$user_id)
		                ->get()->toArray();
            
        }
        else
        {
    	return AccountFolderUser::join('users', 'AccountFolderUser.user_id', '=', 'users.id')
		                ->selectRaw("CONCAT_WS(' ',users.`first_name`, users.`last_name`) AS user_name, users.id,users.email")
		                ->where('is_mentee',1)
                        ->where('AccountFolderUser.site_id', $site_id)
		                ->where('account_folder_id',$account_folder_id)
		                ->get()->toArray();
        }
    }

    public static function check_if_evalutor($site_id, $huddle_id, $user_id) {
        return AccountFolderUser::where('user_id', $user_id)
                                   ->where('account_folder_id', $huddle_id)
                                   ->where('role_id', 200)
                                   ->where('site_id', $site_id)
                                   ->exists();

        $group_evaluation = AccountFolderGroup::join('user_groups as ug', 'ug.group_id', '=', 'account_folder_groups.group_id')
                                              ->where('ug.user_id', $user_id)
                                              ->where('account_folder_groups.account_folder_id', $huddle_id)
                                              ->where('account_folder_groups.role_id', 200)
                                              ->exists();

        return $user_evaluation || $group_evaluation; 
    }

    public static function check_if_evaluated_participant($huddle_id, $user_id, $site_id) {
        return AccountFolderUser::where('user_id', $user_id)
                                   ->where('account_folder_id', $huddle_id)
                                   ->where('role_id', 210)
                                   ->where('site_id', $site_id)
                                   ->exists();
    }
    
    public static function get($user_id, $site_id) {
        
       return AccountFolderUser::where(array('user_id' => (int) $user_id, 'site_id' => $site_id))->get()->toArray();
    }

    public static function get_participants_ids($huddle_id, $role_id, $site_id) {
    	return AccountFolderUser::where('role_id', $role_id)
                                ->where('account_folder_id', $huddle_id)
                                ->where('site_id', $site_id)
                                ->select('user_id')
                                ->get()->toArray();
    }
     public static function has_participant($huddle_id,$user_id, $site_id) {
        $data = AccountFolderUser::where('account_folder_id', $huddle_id)
                                ->where('site_id', $site_id)
                                ->where('user_id', $user_id)
                                ->select('user_id')
                                ->first();
        if($data)
        {
            return $data->toArray();
        }
        return [];
    }

    function get_evaluator_ids($huddle_id) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $result = $db->find('all', array(
            'conditions' => array(
                'role_id' => 200,
                'account_folder_id' => $huddle_id
            )
        ));
        $evaluator = '';
        if ($result) {
            foreach ($result as $row) {
                $evaluator[] = $row['AccountFolderUser']['user_id'];
            }
            return $evaluator;
        } else {
            return FALSE;
        }
    }
    
    public static function user_present_huddle($user_id, $huddle_id) {        
        $result = AccountFolderUser
                        ::leftJoin('account_folders as AccountFolder', 'AccountFolder.account_folder_id', '=', 'AccountFolderUser.account_folder_id')
                        ->leftJoin('account_folder_groups as afg', 'AccountFolder.account_folder_id', '=', 'afg.account_folder_id')
                        ->leftJoin('user_groups as ug', 'ug.group_id', '=', 'afg.group_id')
                        ->where('AccountFolder.account_folder_id',$huddle_id)
                        ->whereRaw('( (AccountFolderUser.user_id = '.$user_id.') OR (ug.user_id = '.$user_id.') )')->get()->toArray();
            
                        
        if (count($result) > 0) {
            return 1;
        } else {
            return 0;
        }
        
    }
    public static function user_present_huddle_count($user_id, $huddle_id) {
        $goal_evidence = (app("Illuminate\Http\Request")->get("goal_evidence") == 1);
        if($goal_evidence)
        {
            return true;
        }
        $result = AccountFolderUser
                        ::leftJoin('account_folders as AccountFolder', 'AccountFolder.account_folder_id', '=', 'AccountFolderUser.account_folder_id')
                        ->leftJoin('account_folder_groups as afg', 'AccountFolder.account_folder_id', '=', 'afg.account_folder_id')
                        ->leftJoin('user_groups as ug', 'ug.group_id', '=', 'afg.group_id')
                        ->where('AccountFolder.account_folder_id',$huddle_id)
                        ->whereRaw('( (AccountFolderUser.user_id = '.$user_id.') OR (ug.user_id = '.$user_id.') )')->count();
                        
        if ($result > 0) {
            return true;
        } else {
            return false;
        }
        
    }

    public static function get_all_account_folder_user($user_id, $sort = '') {
        $user_id = (int) $user_id;
        $fields = array(
            'AccountFolder.*',
            'AccountFolderUser.*'
        );

        $result = AccountFolderUser::leftJoin('account_folders as AccountFolder','AccountFolder.account_folder_id','=','AccountFolderUser.account_folder_id')
            ->select($fields)->where(array(
            'AccountFolderUser.user_id' => $user_id,
            'AccountFolder.folder_type' => 1,
            'AccountFolder.active' => 1
        ))->get();

        if ($result) {
            return $result;
        } else {
            return [];
        }
    }


    public static function getAssesseesList($site_id, $huddle_id, $user_id, $user_role_id, $active_comments = false, $lang = 'en', $is_web = false, $page = false, $search = null, $sort = null,$limit=false){
        $assessees_list = [];
        $results = AccountFolder::getHuddleUsersIncludingGroups($site_id, $huddle_id, $user_id, $user_role_id, true, 210, $page, $search, $limit)['participants'];
                                                                

        $assessor_ids = self::get_participants_ids($huddle_id, 200, $site_id);
        $ids = [];
        foreach($assessor_ids as $assessor){
            $ids [] = $assessor['user_id'];
        }
        $assessor_ids = $ids;
        foreach($results as $assessee){
            $stats = self::getAssesseeStats($site_id, $huddle_id, $assessee['user_id'], $assessor_ids,true,$active_comments);
            // dd($stats);
            $last_submission = '';
            if(!empty($assessee ['last_submission']))
            {
              $last_submission = $assessee ['last_submission'];
               if($lang == 'es')
               {
                   $assessee['last_submission'] = self::SpanishDate(strtotime($assessee['last_submission']),'most_common'); 
               }
                else {
                    $time_date = strtotime($assessee ['last_submission']);
                                $ano = date('Y',$time_date);
                                $mes = date('M',$time_date);
                                $dia = date('d',$time_date);
                   $assessee['last_submission'] =   $mes.', '.$dia .', ' .$ano;

                }
       
            }
            else
            {
               $assessee['last_submission'] = '';
            }

            /*
            if($assessee['role_id']!=210)
                continue;
            */
            // $assessee ['last_modified'] = '2019-03-'.$count;
            //$assessee ['last_submission'] = '2019-04-'.$count;
            $assessee ['last_modified'] = $assessee ['last_submission'];
            $assessee ['videos_count'] = $stats['videos_count'];            
            $assessee ['resources_count'] = $stats['resources_count']+$stats['urls_count'];
            $assessee ['comments_count'] = $stats['comments_count'];
            if($is_web){
                $assessee ['assessment_summary'] = self::getLatestAssessmentSummary($huddle_id, $assessee['user_id']);
            }else{
                $assessee ['assessment_summary'] = self::getAssessmentSummary($huddle_id, $assessee['user_id']);
            }
            
            $assessee ['active_comments_count'] = $stats['active_comments_count'];
            $assessee ['assessed'] = $stats['assessed'];
            $assessee ['avg_performance_level_rating'] = $stats['avg_performance_level_rating'];
            $assessee ['custom_fields_data'] = $stats['custom_fields_data'];
            $assessee ['last_submission_with_time'] = $last_submission;
            $assessee ['is_submitted'] = $stats['is_submitted'];
            $assessees_list [] = $assessee;
        }
        return $assessees_list;
    }

    public static function getAssesseeStats($site_id, $huddle_id, $assessee_id, $assessor_ids,$assessed_stat = true,$active_comments = false){
        $results = Document::leftJoin('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
        ->where('afd.account_folder_id',$huddle_id)
        ->where('documents.created_by',$assessee_id)
        ->whereIn('documents.doc_type',[1,2,5])
        // ->selectRaw("IF(doc_type='1','videos_count','resources_count') AS doc_type, COUNT(*) AS docs_count")
        ->selectRaw("(CASE WHEN doc_type='1' THEN 'videos_count' WHEN doc_type='2' THEN 'resources_count' WHEN doc_type='5' THEN 'urls_count' END) AS doc_type, COUNT(*) AS docs_count")
        ->groupBy('doc_type')
        ->get();

        $response = ["videos_count"=>0, "resources_count"=>0, "comments_count"=>0, "assessed"=>0, "urls_count"=>0];
        foreach($results as $res){
            $response[$res->doc_type] = $res->docs_count;
           
        }
        
        if($active_comments)
        {
           $response ["comments_count"] = self::getUsersVideoActiveCommentsCount($huddle_id, $assessee_id);
           $response["active_comments_count"] = $response ["comments_count"];
        }
        else {
           $response ["comments_count"] = self::getUsersVideoCommentsCount($huddle_id, $assessee_id); 
           $response["active_comments_count"] = self::getUsersVideoActiveCommentsCount($huddle_id, $assessee_id);
        }

        
        $is_submitted = self::isSubmitted($huddle_id, $assessee_id);
        $response ["is_submitted"] = $is_submitted;
        if($assessed_stat)
        {
            // If the assignment is not Submitted. Consider it as Not Assessed by default. 
            // If the assignment is submitted. Then check if it is assessed or not.
            $response ["custom_fields_data"] = UserAssessmentCustomField::get_user_assessment_custom_fields($assessee_id, $huddle_id);
            if($is_submitted){
                $response ["assessed"] = self::isAssessed($huddle_id, $assessee_id, $assessor_ids);
            } else {
                $response ["assessed"] = false;
                // $response ["custom_fields_data"] = false;
            }
        }
        $response ['avg_performance_level_rating'] = DocumentStandardRating::getAveragePerformanceLevelRating($huddle_id, $assessee_id);
        return $response;
    }

    public static function getUsersVideoCommentsCount($huddle_id, $user_id, $video_id = 0){
        $query = Document::
                    join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                    ->join('comments as c', 'c.ref_id', '=', 'documents.id')
                    ->where('afd.account_folder_id',$huddle_id)
                    ->where('documents.created_by',$user_id)
                    ->where('documents.doc_type',"1")
                    ->whereRaw('c.ref_type != 5');
        if($video_id)
        {
            $query->where('documents.id',$video_id);
        }
        $result = $query->selectRaw("COUNT(c.id) AS comments_count")
                    ->first();

        return $result->comments_count;
    }
    
    public static function getAssessmentSummary($huddle_id, $user_id){
        $query = Document::
                    join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                    ->join('comments as c', 'c.ref_id', '=', 'documents.id')
                    ->where('afd.account_folder_id',$huddle_id)
                    ->where('documents.created_by',$user_id)
                    ->where('documents.doc_type',"1")
                    ->whereRaw('c.ref_type = 5');
        return $query->select("afd.title","c.comment")->get();
    }
    public static function getAssessmentSummaryTop($huddle_id, $user_id){
        $query = Document::
                    join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                    ->join('comments as c', 'c.ref_id', '=', 'documents.id')
                    ->where('afd.account_folder_id',$huddle_id)
                    ->where('documents.created_by',$user_id)
                    ->where('documents.doc_type',"1")
                    ->whereRaw('c.ref_type = 5');
        $result =  $query->select("afd.title","c.comment","afd.document_id")->get();
        $comments = array();
        if( $result){
            foreach($result as $row){
                $video_id = $row->document_id;
                $comments[$video_id]   = array(
                    'title'=>$row->title,
                    'comment'=>$row->comment
                );
            }
        }
        $all_result = array();
        if(count( $comments) > 0){
            foreach($comments as $row){ 
                $all_result[] = $row;
            }
        }

        return $all_result;
    }
    public static function getLatestAssessmentSummary($huddle_id, $user_id){
        $all_summary = array();
        $result = Document::
                    join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                    //->join('comments as c', 'c.ref_id', '=', 'documents.id')
                    ->where('afd.account_folder_id',$huddle_id)
                    ->where('documents.created_by',$user_id)
                    ->where('documents.doc_type',"1");
                    //->whereRaw('c.ref_type = 5')                    
                   // ->get();       
        $all_vidoes = $result->select("afd.title","afd.document_id")->get();        
        if($all_vidoes){
            foreach($all_vidoes as $row){
                $video_id = $row->document_id;
                $query = Document::                    
                      join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                    ->join('comments as c', 'c.ref_id', '=', 'documents.id')                    
                    ->where('documents.id',$video_id)
                    ->where('documents.doc_type',"1")
                    ->whereRaw('c.ref_type = 5')      
                    ->orderBy('c.id','DESC');
                    $all_summary[$video_id] = $query->select("afd.title","c.comment")->get();
            }            
           return $all_summary;
        }
    }
      public static function getUsersVideoActiveCommentsCount($huddle_id, $user_id,$video_id = 0){
          $query = Document::
                    join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                    ->join('comments as c', 'c.ref_id', '=', 'documents.id')
                    ->where('afd.account_folder_id',$huddle_id)
                    ->where('documents.created_by',$user_id)
                    ->where('documents.doc_type',"1")
                    ->where('c.active','1')
                    ->whereRaw('c.ref_type != 5');
            if($video_id)
            {
                $query->where('documents.id',$video_id);
            }
        $result = $query->selectRaw("COUNT(c.id) AS comments_count")
                    ->first();

        return $result->comments_count;
    }

    public static function isAssessed($huddle_id, $assessee_id, $assessor_ids=[]){
        $assessed = true;
        // Get assessee's videos list of given huddle id
        $assessee_videos = Document::leftJoin('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
        ->where('afd.account_folder_id',$huddle_id)
        ->where('documents.created_by',$assessee_id)
        ->where('documents.doc_type','1')
        ->select("afd.document_id")->get();

        if(!$assessee_videos->isEmpty()){
            foreach($assessee_videos as $video){
                // Loop through the videos and check if all videos have at least one comment from any of the assessors.
                $assessor_comments = Comment::where('ref_id', $video->document_id)
                                    ->whereIn('created_by',$assessor_ids)
                                    ->where('active', '1')
                                    ->whereRaw('ref_type != 5')
                                    ->count();
                if($assessor_comments<=0){
                    $assessed = false;
                    break;
                }
            }
        } else {
            $assessed = false;
        }
        return $assessed;
    }
    public static function isAssessedByVideo($huddle_id, $assessee_id, $assessor_ids=[],$video_id){
        $assessed = true;
        // Get assessee's videos list of given huddle id
        $assessee_videos = Document::leftJoin('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
        ->where('afd.account_folder_id',$huddle_id)
        ->where('documents.created_by',$assessee_id)
        ->where('documents.id',$video_id)
        ->where('documents.doc_type','1')
        ->select("afd.document_id")->get();

        if(!$assessee_videos->isEmpty()){
            foreach($assessee_videos as $video){
                // Loop through the videos and check if all videos have at least one comment from any of the assessors.
                $assessor_comments = Comment::where('ref_id', $video->document_id)
                                    ->whereIn('created_by',$assessor_ids)
                                    ->where('active', '1')
                                    ->whereRaw('ref_type != 5')
                                    ->count();
                if($assessor_comments<=0){
                    $assessed = false;
                    break;
                }
            }
        } else {
            $assessed = false;
        }
        return $assessed;
    }

    public static function isSubmitted($huddle_id, $assessee_id){
        $submission = AccountFolderUser::where('account_folder_id',$huddle_id)
                                       ->where('user_id',$assessee_id)
                                       ->where('is_submitted','1')
                                       ->first();
        return $submission ? true : false;
    }

    public static function delete_assessee($huddle_id, $assessee_id){
        UserAssessmentCustomField::remove_user_assessment_custom_fields($assessee_id, $huddle_id);
        return \DB::table('account_folder_users')
            ->where('account_folder_id', $huddle_id)
            ->where('user_id', $assessee_id)
            ->where('role_id','210')
            ->delete();
    }
    
    public static function SpanishDate($FechaStamp,$location = '')
        {
           $ano = date('Y',$FechaStamp);
           $mes = date('n',$FechaStamp);
           $dia = date('d',$FechaStamp);
           $time = date('h:i A',$FechaStamp);
           
           $diasemana = date('w',$FechaStamp);
           $diassemanaN= array("Domingo","Lunes","Martes","Mi�rcoles",
                          "Jueves","Viernes","S�bado");
           $mesesN=array(1=>"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio",
                     "Agosto","Septiembre","Octubre","Noviembre","Diciembre");
           if($location == 'dashboard')
           {
              $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic"); 
              return $mesesN[$mes]. ' '  .$dia ;
           }
           elseif($location == 'trackers' )
           {
            $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $dia . '-'.$mesesN[$mes] ; 
           }
           elseif($location == 'trackers_filter' )
           {  
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $mesesN[$mes] ; 
           }
           elseif($location == 'archive_module' )
           {  
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $mesesN[$mes].' '.$dia.', '.$ano ; 
           }
            elseif($location == 'discussion_time' )
            {  
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $time ; 
            }
            
            elseif($location == 'discussion_date' )
            {  
             $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $mesesN[$mes].' '.$dia.', '.$ano . ' '.$time ; 
            }
            
            elseif($location == 'most_common' )
            {  
             $mesesN=array(1=>"Ene","Feb","March","Abr","May","Jun","Jul",
                     "Ago","Sep","Oct","Nov","Dic");   
             return $mesesN[$mes].' '.$dia.', '.$ano ; 
            }
            
           //return $diassemanaN[$diasemana].", $dia de ". $mesesN[$mes] ." de $ano";
        }

}