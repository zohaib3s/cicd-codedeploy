<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sites extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sites as Sites';
    
    public static function get_site_settings($key, $site_id){
        $siteinfo = Sites::where(array( "site_id" => $site_id ))->get()->toArray();
        $site_data = unserialize($siteinfo[0]['site_metadata']);
        $site_data = $site_data['en'];
        if (!empty(array_key_exists($key, $site_data))) {
            return $site_data[$key];
        } else {
            return 'Sibme';
        }
    }

    public static function get_base_url($site_id=null){
        if(!empty($site_id) && php_sapi_name() == 'cli'){
            $site = self::where("id",$site_id)->value('site_url');
            $base_url = "https://".$site."/";
        } elseif(php_sapi_name() != 'cli') {
            $base_url = config('s3.sibme_base_url');
        } else {
            // This block executes if $site_id is empty && php_sapi_name() == 'cli'
            $base_url = 'https://app.sibme.com/';
        }
        return $base_url;
    }

    public static function get_site_title($site_id=null){
        return (!empty($site_id) && $site_id == 2) ? 'Coaching Studio' : 'Sibme';
    }
}
