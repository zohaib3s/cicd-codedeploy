<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class DocumentSubtitle extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $timestamps = false;
//    protected $table = 'document_subtitles';
}