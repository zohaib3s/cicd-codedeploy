<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LiveStreamStatus extends Model
{
    protected $table = 'live_stream_status';
    public $timestamps = false;
}