<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountFrameworkSettingPerformanceLevel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'account_framework_setting_performance_levels as AccountFrameworkSettingPerformanceLevel';
}
