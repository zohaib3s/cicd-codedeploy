<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DocumentMetaData extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'document_meta_data';
}