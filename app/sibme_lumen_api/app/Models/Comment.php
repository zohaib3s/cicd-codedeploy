<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Comment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'comments as Comment';
    protected $fillable = ['parent_comment_id','title','comment','ref_type','ref_id','user_id','time','restrict_to_users','created_by','created_date','last_edit_by','last_edit_date','active','audio_duration','published_by','created_at_gmt'];
    public $timestamps = false;
    
            
    public static function getParentDiscussion($comment_id, $site_id){
    	return Comment::leftJoin('users as User', 'Comment.user_id', '=', 'User.id')
    					->leftJoin('comment_attachments as CommentAttachment','Comment.id','=','CommentAttachment.comment_id')
    					->leftJoin('documents as Document','CommentAttachment.document_id','=','Document.id')
		                ->select(['Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.email' , 'User.image', 'Document.url', 'Document.original_file_name', 'Document.id as document_id'])
		                ->where('Comment.id',$comment_id)
                        ->where('Comment.site_id',$site_id)
                        ->groupBy('Comment.id')
		                ->orderBy('Comment.created_date','DESC')
		                ->first()->toArray();
    }

    public static function getVideoComments($site_id, $video_id, $changType = '', $huddel_id = '', $searchCmt = '', $tags = '', $support_audio_annotation = 0, $get_all_comments = 0, $include_replys = false, $active=false) {

        $video_id = (int) $video_id;
        if ($huddel_id != '') {
            $huddel_id = (int) $huddel_id;
        }

        $query = Comment
                    ::leftJoin('users as User', 'Comment.user_id', '=', 'User.id')
                    ->leftJoin('account_comment_tags AS act','Comment.id','=','act.comment_id')
                    ->select('Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image', \DB::raw('(SELECT GROUP_CONCAT(original_file_name SEPARATOR ", ") FROM `documents` 
INNER JOIN `comment_attachments` ON documents.`id` = `comment_attachments`.`document_id` 
WHERE comment_attachments.`comment_id` = `Comment`.`id`) AS attachments'));

        if ($support_audio_annotation == 1) {
            $cmt_type = ($get_all_comments == 1) ? ['2', '6', '7'] : ['2', '6'];
        }else {
            $cmt_type = ['2'];
        }

        if ($include_replys) {
            array_push($cmt_type, '3');
        } else {
            $query->whereNull('Comment.parent_comment_id');
        }

        $query->where('Comment.ref_id',$video_id)
              ->whereIn('Comment.ref_type',$cmt_type);

        if($active){
            $query->where('Comment.active', 1);
        }

        if (!empty($tags)) {
            $query->whereIn('act.account_tag_id', $tags);
        }

        if (!empty($searchCmt)) {
                  $query->orWhere('Comment.comment','LIKE', '%'.$searchCmt.'%')
                        ->orWhere('act.tag_title','LIKE', '%'.$searchCmt.'%');
        }
        $query->where('Comment.site_id', $site_id);
        if ($changType == 'oldest') {
            $query->orderBy('Comment.id', 'ASC');
        } elseif ($changType == 'timestamp') {
            $query->orderByRaw('Comment.time=0 ASC'); // This is to keep "All Video" comments on bottom.
            $query->orderBy('Comment.time', 'ASC');
        } else {
            $query->orderBy('Comment.id', 'DESC');
        }

/*
        if ($changType == '1') {
            $query->orderBy('Comment.id', 'ASC');
        } elseif ($changType == '2') {
            $query->selectRaw('CASE WHEN time IS NULL THEN (SELECT MAX(id) + 1 FROM comments) ELSE time  END time_modified');
            $query->orderBy('time_modified', 'ASC');
        } elseif ($changType == '3') {
            $query->orderBy('User.first_name', 'ASC')
                  ->orderBy('Comment.time', 'ASC');
        } elseif ($changType == '4') {
            $query->orderBy('Comment.id', 'DESC');
        } elseif ($changType == '5') {
            $query->orderBy('Comment.id', 'DESC');
        } elseif ($changType == '6') {
            $query->orderBy('Comment.time', 'ASC');
        } else {
            $query->orderBy('Comment.time', 'DESC');
        }
*/
        return $query->groupBy('Comment.id')
                     ->get()
                     ->toArray();
    }

    public static function getAssessmentComment($document_id, $site_id)
    {
        $user_assessment = '';
        $assessment = Comment::where('ref_id', $document_id)
                             ->where('ref_type', 5)
                             ->where('site_id', $site_id)
                             ->first();
        if($assessment){
            $user_assessment = $assessment->comment;
        }
        return $user_assessment;
    }

    public static function getAssessmentFeedback($document_id, $coach_users, $site_id)
    {
        $coach_users = (array)$coach_users;
        return Comment::where('ref_id', $document_id)
                      ->where('ref_type', 5)
                    //   ->where('user_id', $coach_users) // SW-4897 This was creating problem if there are multiple coaches. The Query Builder takes array's second element as Site Id that produces logical error. Means, we never get feedback.
                      ->whereIn('user_id', $coach_users)
                      ->where('site_id', $site_id)
                      ->orderBy('created_date', 'DESC')
                      ->first();
    }
    public static function getCoachCommentsCount($document_id, $coach_users, $site_id)
    {
        return Comment::where('ref_id', $document_id)
                      ->where('site_id', $site_id)
                      ->whereIn('ref_type', [2,3])
                      ->whereIn('user_id', $coach_users)
                      ->count();
    }
    public static function getCoacheeCommentsCount($document_id, $coachee_id, $site_id)
    {
        return Comment::where('ref_id', $document_id)
                      ->where('site_id', $site_id)
                      ->whereIn('ref_type', [2,3])
                      ->where('user_id', $coachee_id)
                      ->count();
    }
    public static function getCommentReplies($comment_id)
    {
      return \DB::select("SELECT `comments`.`comment`, 
                              (SELECT CONCAT(first_name,' ',last_name) AS user_name FROM `users` WHERE id=`comments`.`created_by`) AS user_name
                              FROM (SELECT * FROM `comments` ORDER BY parent_comment_id, `comments`.id) `comments`,
                                   (SELECT @pv := ?) initialisation
                              WHERE FIND_IN_SET(parent_comment_id, @pv) > 0
                              AND @pv := CONCAT(@pv, ',', `comments`.id)", [$comment_id]);
    }
    public static function comments_count($ref_id, $site_id)
    {
        $ref_type = array(2, 3, 6, 7);
      return Comment::where('ref_id', $ref_id)
                      ->where('site_id', $site_id)
                      ->whereIn("ref_type", $ref_type)
                      ->count();
    }
    public static function get_parent_discussion($comment_id, $site_id){
        $data = Comment::leftJoin('users as User', 'Comment.user_id', '=', 'User.id')
            ->leftJoin('comment_attachments as CommentAttachment','Comment.id','=','CommentAttachment.comment_id')
            ->leftJoin('documents as Document','CommentAttachment.document_id','=','Document.id')
            ->select(['Comment.*', 'User.id as user_id', 'User.first_name', 'User.last_name', 'User.image', 'Document.url', 'Document.original_file_name', 'Document.id as document_id'])
            ->where('Comment.id',$comment_id)
            ->where('Comment.site_id',$site_id)
            ->groupBy('Comment.id')
            ->orderBy('Comment.created_date','DESC')
            ->first();
        if($data)
        {
            $data = $data->toArray();
        }
        else
        {
            $data = [];
        }
    	return $data;
    }
    public static function get($ref_id, $site_id){

      $result = app('db')->select("SELECT 
                  Comment.*,
                  User.id AS user_id,
                  User.first_name,
                  User.last_name,
                  User.image 
                FROM
                  `comments` AS `Comment` 
                  INNER JOIN users AS `User` 
                    ON User.id = Comment.created_by 
                WHERE Comment.id = $ref_id AND Comment.active = 1 AND Comment.site_id = $site_id
                GROUP BY Comment.id 
                ORDER BY Comment.created_date DESC ");

        $result = Comment::convert_array($result);

        return $result ? $result[0] : [];
    }
    public static function get_all($ref_id, $site_id){

        $result = app('db')->select("SELECT 
                    Comment.*,
                    User.id AS user_id,
                    User.first_name,
                    User.last_name,
                    User.image 
                  FROM
                    `comments` AS `Comment` 
                    INNER JOIN users AS `User` 
                      ON User.id = Comment.created_by 
                  WHERE Comment.id = $ref_id  AND Comment.site_id = $site_id
                  GROUP BY Comment.id 
                  ORDER BY Comment.created_date DESC ");
  
          $result = Comment::convert_array($result);
  
          return $result ? $result[0] : [];
      }
    public static function getCommentsSearch($account_folder_id, $site_id, $title = '', $sort = '',$page=1,$limit=2,$discussion_bool = false,$user_id,$is_mobile_request=false){

      $account_folder_id = (int) $account_folder_id;
        if (empty($sort)) {
            $orderBy = "Comment.title ASC";
        } else {
            if ($sort=='topic'){
                $orderBy = "Comment.title ASC";
            }
            elseif ($sort=='date_created'){
                $orderBy = "Comment.created_date DESC";
            }
            elseif ($sort=='last_modified'){
                $orderBy = "Comment.last_edit_date DESC";
            }
            elseif ($sort=='created_by'){
                $orderBy = "Comment.created_by ASC";
            }
            elseif ($sort=='unread'){
                /*WILL NEED TO DISCUSS-NEW FUNCTIONALITY*/
                $orderBy = "unread_replies DESC";
            }else{
                $orderBy = "Comment.title ASC";
            }
        }

        if ($page==1){
            $offset=0;
        }else{
            $offset = $page * $limit;
            $offset = $offset - $limit;
        }

        if (!empty($title)) {
              if($discussion_bool)
              {
              $title =  " AND ( (`Comment`.`title` LIKE '%$title%') OR (`Comment`.`comment` LIKE '%$title%') OR (  (SELECT CONCAT(first_name,' ',last_name)  FROM users WHERE id = `Comment`.`created_by`  ) LIKE '%$title%'   ) ) ";
              }
              else
              {
                 $title =  " AND (`Comment`.`title` LIKE '%$title%') "; 
              }
        }
        if($is_mobile_request)
        {
            $order_by = "";
        }
        else {
            $order_by = "ORDER BY $orderBy limit $limit offset $offset";
        }
        
      $result = app('db')->select("SELECT 
                      `Comment`.*,
                      `User`.`id` AS `user_id`,
                      `User`.`first_name`,
                      `User`.`last_name`,
                      `User`.`image`,
                      `Document`.`url`,
                      `Document`.`original_file_name`,
                      `Document`.`id` AS `document_id`,
                      ( (SELECT COUNT(*) AS AGGREGATE FROM comments AS c WHERE c.parent_comment_id = Comment.id AND `site_id` = $site_id AND `active` = 1)
                        -
                      (SELECT COUNT(*) AS AGGREGATE FROM `user_read_comments` WHERE `discussion_id` = Comment.id AND `user_id` = $user_id) ) AS unread_replies
                    FROM
                      `comments` AS `Comment` 
                      LEFT JOIN `users` AS `User` 
                        ON (
                          `User`.`id` = `Comment`.`user_id`
                        ) 
                      LEFT JOIN `comment_attachments` AS `CommentAttachment` 
                        ON (
                          `Comment`.`id` = `CommentAttachment`.`comment_id`
                        ) 
                      LEFT JOIN `documents` AS `Document` 
                        ON (
                          `CommentAttachment`.`document_id` = `Document`.`id`
                        ) 
                    WHERE `Comment`.`ref_type` = 1 
                      AND `Comment`.`active` = 1 
                      AND (`Comment`.`parent_comment_id` IS NULL OR `Comment`.`parent_comment_id` = 0  OR `Comment`.`parent_comment_id` = '')
                      AND `Comment`.`ref_id` = $account_folder_id
                      AND `Comment`.`site_id` = $site_id
                      AND user_id != ''  
                      $title
                    GROUP BY `Comment`.`id`".$order_by);

        $result = Comment::convert_array($result);
        if(count($result))
        {
            foreach ($result as $key => &$value) {
                $value["created_date_str"] = Controller::dateToString($value["created_date"]);
            }
        }
        return $result ? $result : [];
    }
    public static function getsingleDiscussion($account_folder_id, $site_id,$detail_id,$user_id = null,$is_mobile_request=false){

      $account_folder_id = (int) $account_folder_id;
      $result = app('db')->select("SELECT 
                      `Comment`.*,
                      `User`.`id` AS `user_id`,
                      `User`.`first_name`,
                      `User`.`last_name`,
                      `User`.`image`,
                      `Document`.`url`,
                      `Document`.`original_file_name`,
                      `Document`.`id` AS `document_id`,
                      `Document`.`stack_url`
                    FROM
                      `comments` AS `Comment` 
                      LEFT JOIN `users` AS `User` 
                        ON (
                          `User`.`id` = `Comment`.`user_id`
                        ) 
                      LEFT JOIN `comment_attachments` AS `CommentAttachment` 
                        ON (
                          `Comment`.`id` = `CommentAttachment`.`comment_id`
                        ) 
                      LEFT JOIN `documents` AS `Document` 
                        ON (
                          `CommentAttachment`.`document_id` = `Document`.`id`
                        ) 
                    WHERE `Comment`.`ref_type` = 1 
                      AND `Comment`.`active` = 1
                      AND `Comment`.`ref_id` = $account_folder_id
                      AND `Comment`.`site_id` = $site_id
                      AND user_id != ''  
                      AND `Comment`.`id` = $detail_id  
                    GROUP BY `Comment`.`id`");
//AND (`Comment`.`parent_comment_id` IS NULL OR `Comment`.`parent_comment_id` = 0  OR `Comment`.`parent_comment_id` = '')
        $result = Comment::convert_array($result);
        if(count($result))
        {
            foreach ($result as &$value) {
                $value["created_date_str"] = Controller::dateToString($value["created_date"]);
                $temp_attachments = self::getattachments($detail_id);
                $attachments = array();
                $base = config('s3.sibme_base_url');
                foreach ($temp_attachments as $key => $attachment)
                {
                        $attachments[$key] = $attachment;
                        
                        if(!empty($attachment['stack_url']))
                        { 
                           if($is_mobile_request)
                           {
                                $attachments[$key]['stack_url'] = $attachment['stack_url']; 
                           }
                           else
                           {
                                $attachments[$key]['stack_url'] = $base . 'app/view_document/'.$attachment['stack_url'];
                           }
                        }
                        else {
                           $attachments[$key]['stack_url'] = ''; 
                        }
                        
                        $ext = pathinfo($attachment['original_file_name'], PATHINFO_EXTENSION);
                        if ($ext == 'xls' || $ext == 'xlsx') {
                            $attachments[$key]['thubnail_url'] = $base . 'img/excel_gry_icon.svg';
                            $attachments[$key]['file_type'] = $ext;
                        } elseif ($ext == 'pdf') {
                            $attachments[$key]['thubnail_url'] = $base . 'img/pdf_gry_icon.svg';
                            $attachments[$key]['file_type'] = $ext;
                        } elseif ($ext == "pptx" || $ext == "ppt") {
                            $attachments[$key]['thubnail_url'] = $base . 'img/power_point.png';
                            $attachments[$key]['file_type'] = $ext;
                        } elseif ($ext == "docx" || $ext == "doc") {
                            $attachments[$key]['thubnail_url'] = $base . 'img/word_icon.png';
                            $attachments[$key]['file_type'] = $ext;
                        } elseif ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                            $attachments[$key]['thubnail_url'] = $base . 'img/image_icon.png';
                            $attachments[$key]['file_type'] = $ext;
                        } else {
                            $attachments[$key]['thubnail_url'] = $base . 'img/file_gry_icon.svg';
                            $attachments[$key]['file_type'] = $ext;
                        }
                }
                $value["attachments"] = $attachments;
                $value['attachment_count'] = Comment::getTotalAttachments($value['id']);
                $reply_counts = self::getRepliesCountWithSubReplies($value['id'],$user_id, $site_id);
                $value['reply_count'] = $reply_counts;
                if($user_id != null)
                {
                    $value['unread_comments'] = self::get_unread_counts($value['id'], $site_id, $user_id,$reply_counts);
                }

                $replyDiscussions = self::commentReplys($detail_id);
                $newreplyDiscussions = array();
                if($replyDiscussions)
                {
                    $replyDiscussions = $replyDiscussions->toArray();
                    foreach ($replyDiscussions as $key => $discussion)
                    {
                        $newreplyDiscussions[$key] = $discussion;
                        $replyreplyObject = self::commentReplys($discussion->id);
                        if($replyreplyObject)
                        {
                            $replyreplyObject = $replyreplyObject->toArray();
                            $newreplyDiscussions[$key]->replies = $replyreplyObject;
                        }
                        else
                        {
                            $newreplyDiscussions[$key]->replies = [];
                        }
                    }
                }

                $replyDiscussions = $newreplyDiscussions;
                $value["replies"] = $replyDiscussions;
            }
        }
        return $result ? $result : [];
    }
    public static function getattachments($detail_id){
      $result = app('db')->select("SELECT 
                      `Document`.`url`,
                      `Document`.`original_file_name`,
                      `Document`.`id` AS `document_id`,
                      `Document`.`stack_url`,
                      `Document`.`created_date`,
                      `comment_attachments`.id As comment_attachments_id
                    FROM
                      `comment_attachments` 
                      LEFT JOIN `documents` AS `Document` 
                        ON (
                          `comment_attachments`.`document_id` = `Document`.`id`
                        ) 
                    WHERE `comment_attachments`.`comment_id`=$detail_id");

        $result = Comment::convert_array($result);

        return $result ? $result : [];
    }

    public static function getTotalAttachments($detail_id){

        $ids = Comment::where("parent_comment_id", $detail_id)->pluck('id');
        $child_ids = Comment::whereIn("parent_comment_id", $ids)->pluck('id');
        $sub_childs = Comment::whereIn("parent_comment_id", $child_ids)->pluck('id');
        $ids->push($detail_id);
        $ids = $ids->merge($child_ids);
        $ids = $ids->merge($sub_childs);

        $result = app('db')->select("SELECT 
                        count(`Document`.`url`) as total
                      FROM
                        `comment_attachments` 
                        LEFT JOIN `documents` AS `Document` 
                          ON (
                            `comment_attachments`.`document_id` = `Document`.`id`
                          ) 
                      WHERE `comment_attachments`.`comment_id` IN ("  . $ids->implode(",") . ")");
  
          //$result = Comment::convert_array($result);
  
          return $result ? $result[0]->total : 0;
      
 }

    public static function countDiscussion($account_folder_id, $site_id,$title = '',$discussion_bool =false)
    {
        if (!empty($title)) {
            if($discussion_bool)
              {
              $title =  " AND ( (`Comment`.`title` LIKE '%$title%') OR (`Comment`.`comment` LIKE '%$title%') OR (  (SELECT CONCAT(first_name,' ',last_name)  FROM users WHERE id = `Comment`.`created_by`  ) LIKE '%$title%'   ) ) ";
              }
              else
              {
                 $title =  " AND (`Comment`.`title` LIKE '%$title%') "; 
              }
            
        }
        $result = app('db')->select("SELECT *  FROM
                            `comments` AS `Comment` 
                            LEFT JOIN `users` AS `User` 
                            ON (
                            `User`.`id` = `Comment`.`user_id`
                            ) 
                            LEFT JOIN `comment_attachments` AS `CommentAttachment` 
                            ON (
                            `Comment`.`id` = `CommentAttachment`.`comment_id`
                            ) 
                            LEFT JOIN `documents` AS `Document` 
                            ON (
                            `CommentAttachment`.`document_id` = `Document`.`id`
                            ) 
                            WHERE `Comment`.`ref_type` = 1 
                            AND `Comment`.`active` = 1 
                             AND (`Comment`.`parent_comment_id` IS NULL OR `Comment`.`parent_comment_id` = 0  OR `Comment`.`parent_comment_id` = '')
                            AND `Comment`.`ref_id` = $account_folder_id
                            AND `Comment`.`site_id` = $site_id
                            AND user_id != ''  
                            $title GROUP BY `Comment`.`id`");

        return (isset($result)?count($result):'');
    }

    public static function getRepliesCount($parent_comment_id, $site_id)
    {
        return Comment::where('parent_comment_id', $parent_comment_id)
                      ->where('site_id', $site_id)
                      ->where('active', 1)
                      ->count();
    }

    public static function getRepliesCountWithSubReplies($parent_comment_id,$user_id, $site_id)
    {
        $total_comments = 0;
        $total_replies = 0;
        $total_replies_count =0;
        $result =  Comment::where('parent_comment_id', $parent_comment_id)
                      ->where('site_id', $site_id)
                      ->where('active', 1)
                      ->get();
                      if($result){
                        foreach($result as $row){
                            $parent_id = $row->id;
                            $total_replies =   Comment::where('parent_comment_id', $parent_id)
                            ->where('site_id', $site_id)
                            ->where('active', 1)
                            //->where('user_id',$user_id)
                            ->count();
                         $total_replies_count = $total_replies_count +$total_replies;
                        }
                }
        $total_comments = count($result) +$total_replies_count;
        return  $total_comments;
    }
    public static function deleteDiscussion($comment_id, $site_id)
    {
        $resp = Comment::where('id', $comment_id)
                      ->where('site_id', $site_id)
                      ->update(['active'=> 0]);
                      
        UserReadComments::where('comment_id', $comment_id)
                        ->delete();

        return ['success'=>true];
    }
    public static function convert_array($data){
        $data = collect($data)->map(function($x){ 
                return (array) $x; 
            })->toArray();
        return $data;
    }

    public static function commentReplys($comment_id, $get_all_comments = 0,$orderBy = 'ASC',$is_mobile_request=false) {
        $comment_id = (int) $comment_id;

        if ($get_all_comments == 1) {
            $whereactive = 'comments.active=1 OR comments.active=0';
        } else {
            $whereactive = 'comments.active=1';
        }

        $reply = DB::table('comments')
            ->select('comments.*', 'users.id as user_id', 'users.first_name', 'users.last_name', 'users.image', 'documents.url', 'documents.original_file_name', 'documents.id as document_id','documents.stack_url')
            ->leftJoin('users','comments.user_id','=','users.id')
            ->leftJoin('comment_attachments','comments.id','=','comment_attachments.comment_id')
            ->leftJoin('documents','comment_attachments.document_id','=','documents.id')
            ->where('comments.parent_comment_id','=',$comment_id)
            ->where('user_id','!=','')
            ->whereRaw($whereactive)
            ->groupBy('comments.id')
            ->orderBy('comments.created_date',$orderBy)
            ->get();
        if(count($reply))
        {
            foreach ($reply as $key => &$value) {
                $value->created_date_str = Controller::dateToString($value->created_date);
                $temp_attachments = self::getattachments($value->id);
                $attachments = array();
                $base = config('s3.sibme_base_url');
                foreach ($temp_attachments as $index => $attachment)
                {
                        $attachments[$index] = $attachment;
                        
                        
                        if(!empty($attachment['stack_url']))
                        {
                            if($is_mobile_request)
                            {
                                $attachments[$index]['stack_url'] = $attachment['stack_url'];
                            }
                            else {
                                $attachments[$index]['stack_url'] = $base . 'app/view_document/'.$attachment['stack_url'];   
                            }
                          
                        }
                        else {
                           $attachments[$index]['stack_url'] = ''; 
                        }
                        
                        
                        $ext = pathinfo($attachment['original_file_name'], PATHINFO_EXTENSION);
                        if ($ext == 'xls' || $ext == 'xlsx') {
                            $attachments[$index]['thubnail_url'] = $base . 'img/excel_gry_icon.svg';
                            $attachments[$index]['file_type'] = $ext;
                        } elseif ($ext == 'pdf') {
                            $attachments[$index]['thubnail_url'] = $base . 'img/pdf_gry_icon.svg';
                            $attachments[$index]['file_type'] = $ext;
                        } elseif ($ext == "pptx" || $ext == "ppt") {
                            $attachments[$index]['thubnail_url'] = $base . 'img/power_point.png';
                            $attachments[$index]['file_type'] = $ext;
                        } elseif ($ext == "docx" || $ext == "doc") {
                            $attachments[$index]['thubnail_url'] = $base . 'img/word_icon.png';
                            $attachments[$index]['file_type'] = $ext;
                        } elseif ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
                            $attachments[$index]['thubnail_url'] = $base . 'img/image_icon.png';
                            $attachments[$index]['file_type'] = $ext;
                        } else {
                            $attachments[$index]['thubnail_url'] = $base . 'img/file_gry_icon.svg';
                            $attachments[$index]['file_type'] = $ext;
                        }
                }
                $value->attachments = $attachments;
            }
        }
        return $reply;

    }

    public static function get_unread_counts($discussion_id,$site_id,$user_id,$reply_counts = null)
    {
               
        if($reply_counts == null)
        {
            $reply_counts = self::getRepliesCountWithSubReplies($discussion_id,$user_id,$site_id);
        }
        
        $already_read_count = UserReadComments::where("discussion_id",$discussion_id)->where("user_id",$user_id)->count();
    
        //if($discussion_id == 902407){
        //    echo $reply_counts ."<br/>";
        //   echo $already_read_count."<br/>"; 
        //   die;
        //}
                      
        $unread_counts = $reply_counts - $already_read_count;
        if($unread_counts < 0)
        {
            $unread_counts = 0;
        }
        
        return $unread_counts;
    }

    public function replies()
    {
        //$this->table = "comments";
        return $this->hasMany('App\Models\Komment','parent_comment_id','id')->with('replies');
    }
}