<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditError extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'audit_errors as AuditError';
}