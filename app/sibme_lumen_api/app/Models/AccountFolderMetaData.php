<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountFolderMetaData extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'account_folders_meta_data as AccountFolderMetaData';

    public static function getMetaDataValue($huddle_id, $site_id){
    	$type = AccountFolderMetaData::where('account_folder_id',$huddle_id)
                                      ->where('meta_data_name','folder_type')
                                      ->where('site_id',$site_id)
		                 			 ->first();
                if(!$type){
                    return 1;
		}
                                                         
                if($type['meta_data_value'] == '5')
                {
                    return 1;
                }
                
		return empty($type->meta_data_value) ? 1 : $type->meta_data_value;
    }

    public static function getMetaDataValueByName($huddle_id, $site_id, $name){
    	$type = AccountFolderMetaData::where('account_folder_id',$huddle_id)
                                      ->where('meta_data_name',$name)
                                      ->where('site_id',$site_id)
		                 			 ->first();
        return !empty($type['meta_data_value']) ? $type['meta_data_value'] : 0;
    }

    public static function check_if_eval_huddle($huddle_id, $site_id) {
        $exist = AccountFolderMetaData::where('meta_data_value', '3')
                                    ->where('meta_data_name', 'folder_type')
                                    ->where('site_id', $site_id)
                                    ->where('account_folder_id', $huddle_id)
                                    ->first();
        return ($exist ? 1 : 0);
    }

    public static function is_enabled_coach_feedback($account_folder_id, $site_id) {
        return AccountFolderMetaData::where('account_folder_id', $account_folder_id)
                                    ->where('meta_data_name', 'coach_hud_feedback')
                                    ->where('meta_data_value','1')
                                    ->exists();
    }

}