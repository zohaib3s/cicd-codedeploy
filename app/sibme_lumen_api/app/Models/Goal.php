<?php

namespace App\Models;

use App\Http\Controllers\GoalsController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Goal extends Model
{
    const userTypeCollaborator = 1;
    const userTypeOwner = 2;

    const goalTypeTemplate = 1;
    const goalTypeAccount = 2;
    const goalTypeGroup = 3;
    const goalTypeIndividual = 4;
    const activityTypes = [
        "created_for_self"=>32,
        "created_for_someone"=>33,
        "created_for_multiple"=>34,
        "created_group_goal"=>35,
        "created_account_goal"=>36,
        "created_template_goal"=>37,
        "added_action_item"=>38,
        "added_collaborator"=>39,
        "added_evidence"=>40,
        "mark_complete"=>41,
        "added_feedback"=>42,
        "mark_incomplete"=>43,
        "action_item_start_today"=>44,
        "action_item_deadline_tomorrow"=>45,
        "action_item_deadline_passed"=>46,
        "goal_deadline_tomorrow"=>47,
        "goal_deadline_passed"=>48,
        "action_item_deleted"=>49,
        "goal_deleted"=>50,
        "goal_start_date_edited"=>51,
        "goal_deadline_edited"=>52,
        "action_item_start_date_edited"=>53,
        "action_item_deadline_edited"=>54,
        "action_item_title_edited"=>55,
        "goal_title_edited"=>56,
        "action_item_deadline_7_days"=>57,
        "goal_deadline_7_days"=>58
        ];
    const translations = [
        32=>["s_en"=>"Created Personal Goal","s_es"=>"Creó Meta Personal","en"=>"created the personal goal {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}", "es"=>"Creaste la meta personal {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}"],
        33=>["s_en"=>"Created Individual Goal","s_es"=>"Creó Meta Individual","en"=>"created an individual goal for {OTHER_USER_NAME} called {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}", "es"=>"creó la meta individual para {OTHER_USER_NAME} llamada {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}"],
        34=>["s_en"=>"Created Individual Goal","s_es"=>"Creó Meta Individual","en"=>"created an individual goal for multiple people called {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}", "es"=>"creó la meta individual para varias personas llamada {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}"],
        35=>["s_en"=>"Created Group Goal","s_es"=>"Creó Meta Grupal","en"=>"created a Group Goal called {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}", "es"=>"creó una meta grupal llamada {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}"],
        36=>["s_en"=>"Created Account Goal","s_es"=>"Creó Meta de Cuenta","en"=>"created a Account Goal called {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}", "es"=>"creó una meta de cuenta {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}"],
        37=>["s_en"=>"Created Template Goal","s_es"=>"Creó Plantilla de Meta","en"=>"created a Goal Template called {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}", "es"=>"creó una plantilla de meta llamada {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}"],
        38=>["s_en"=>"Added Action Item","s_es"=>"Agregó Acción","en"=>"added the Action Item {SPECIAL} to {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}", "es"=>"agregó la acción {SPECIAL} a {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}"],
        39=>["s_en"=>"Added Collaborator","s_es"=>"Agregó Colaborador","en"=>"added {OTHER_USER_NAME} to {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} as a Collaborator with {SPECIAL}", "es"=>"agregó {OTHER_USER_NAME} a {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} como colaborador con derechos para {SPECIAL}"],
        40=>["s_en"=>"Added Evidence","s_es"=>"Agregó Evidencia","en"=>"added {SPECIAL} to {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}", "es"=>"agregó {SPECIAL} a {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}"],
        41=>["s_en"=>"Action Item Complete","s_es"=>"Acción Completada","en"=>"marked {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} complete", "es"=>"marcó {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} como realizado"],
        42=>["s_en"=>"Provided Feedback","s_es"=>"Entregó Retroalimentación","en"=>"provided feedback on {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}", "es"=>"entregó retroalimentación en {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}"],
        43=>["s_en"=>"Action Item Incomplete","s_es"=>"Acción Incompleta","en"=>"marked {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} incomplete", "es"=>"marcó {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} como incompleta"],
        44=>["en"=>"", "es"=>""],//not implemented
        45=>["s_en"=>"Deadline Tomorrow","s_es"=>"Entrega Mañana","en"=>"{Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} deadline is tomorrow {SPECIAL}", "es"=>"Fecha de entrega de {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} es mañana {SPECIAL}"],
        46=>["s_en"=>"Deadline Passed","s_es"=>"Fecha Entrega Pasó","en"=>"The deadline for {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} has passed on {SPECIAL}", "es"=>"Fecha de entrega de {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} venció su plazo el {SPECIAL}"],
        47=>["s_en"=>"Goal Deadline Tomorrow","s_es"=>"Meta Vence Mañana","en"=>"{Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} deadline is tomorrow {SPECIAL}", "es"=>"Fecha de entrega de {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} es mañana {SPECIAL}"],
        48=>["s_en"=>"Goal Deadline Passed","s_es"=>"Plazo Meta Venció","en"=>"The deadline for {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} has passed on {SPECIAL}", "es"=>"Fecha de entrega de {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} venció su plazo el {SPECIAL}"],
        49=>["s_en"=>"Action Item Deleted","s_es"=>"Acción Eliminada","en"=>"deleted the Action Item {SPECIAL} from {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}", "es"=>"eliminó la Acción {SPECIAL} de {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}"],
        50=>["s_en"=>"Goal Deleted","s_es"=>"Meta Eliminada","en"=>"deleted the Goal {ITEM_NAME}", "es"=>"eliminó la Meta {ITEM_NAME}"],
        51=>["s_en"=>"Goal Start Date","s_es"=>"Meta Fecha de Inicio","en"=>"edited the Goal Start Date for {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} to {SPECIAL}", "es"=>"editó la fecha de inicio de la meta {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} a {SPECIAL}"],
        52=>["s_en"=>"Goal Deadline","s_es"=>"Meta Entrega","en"=>"edited the Goal Deadline for {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} to {SPECIAL}", "es"=>"editó la fecha de entrega de la meta {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} a {SPECIAL}"],
        53=>["en"=>"", "es"=>""],//not implemented
        54=>["s_en"=>"Action Item Deadline","s_es"=>"Acción Entrega","en"=>"edited the Action Item Deadline for {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} to {SPECIAL}", "es"=>"editó la fecha de entrega de la acción {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} a {SPECIAL}"],
        55=>["s_en"=>"Action Item Title Edited","s_es"=>"Nombre Acción Editado","en"=>"edited the title for the Action Item titled {SPECIAL} in {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}", "es"=>"editó el nombre de la Acción {SPECIAL} en {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}"],
        56=>["s_en"=>"Goal Title Edited","s_es"=>"Nombre Meta Editado","en"=>"edited the Goal title for {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}", "es"=>"editó el nombre de la meta {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE}"],
        57=>["s_en"=>"Action Item Deadline in 7 Days","s_es"=>"Fecha entrega acción en 7 días","en"=>"{Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} deadline is in 7 days on {SPECIAL}", "es"=>"Fecha de entrega de {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} es en 7 días el {SPECIAL}"],
        58=>["s_en"=>"Goal Deadline in 7 Days","s_es"=>"Fecha entrega meta en 7 días","en"=>"{Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} deadline is on {SPECIAL}", "es"=>"Fecha de entrega de {Link_SEPRATE}{ITEM_NAME}{Link_SEPRATE} es en 7 días el {SPECIAL}"],
    ];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_edit_date';
    public function actionItems()
    {
        return $this->hasMany("App\Models\GoalItem", "goal_id", "id");
    }

    public function categories()
    {
        return $this->hasMany("App\Models\GoalCategory", "goal_id", "id");
    }

    public function owners()
    {
        return $this->hasMany("App\Models\GoalUser", "goal_id", "id")->join("users","users.id","goal_users.user_id")->where("user_type", self::userTypeOwner);
    }

    public function collaborators()
    {
        return $this->hasMany("App\Models\GoalUser", "goal_id", "id")
            ->join("users","users.id","goal_users.user_id")
            ->leftJoin("goal_users_settings",function($join){
                $join->on('goal_users.user_id', '=', 'goal_users_settings.user_id');
                $join->on('goal_users.account_id','=','goal_users_settings.account_id');
            })
            ->where("user_type", self::userTypeCollaborator);
    }

    public static function getGoalRelations($withOwners = 1, $withCategories = 1, $withActionItems = 1, $withCollaborators = 1, $from_listing = 0)
    {
        $request = app("Illuminate\Http\Request");
        $owner_id = $request->has("owner_id") ? $request->get("owner_id") : 0;

        $collaborator_id = $request->has("collaborator_id") ? $request->get("collaborator_id") : 0;
        $goal_type = $request->has("goal_type") ? $request->get("goal_type") : 1;
        $relations = [];
        if($withOwners)
        {
            $relations[] = "owners:goal_id,user_id,email,first_name,last_name,image,user_type,is_archived";

            $relations["actionItems.submittedEvidence"] = function ($query) use ($owner_id, $goal_type) {
            $query->selectRaw("goal_item_submitted_evidence.*, 
                   IF(goal_item_submitted_evidence.evidence_type IN (1,2,5) , (select CONCAT(afd.title, '___', afd.account_folder_id, '___', af.folder_type) from account_folder_documents afd join account_folders af on af.account_folder_id = afd.account_folder_id where document_id = goal_item_submitted_evidence.evidence_ref_id limit 1)
                   , IF(goal_item_submitted_evidence.evidence_type = 3, (select af.name from account_folders af where account_folder_id = goal_item_submitted_evidence.evidence_ref_id limit 1)
                   ,  IF(goal_item_submitted_evidence.evidence_type = 4, (select at.tag_title from account_tags at where account_tag_id = goal_item_submitted_evidence.evidence_ref_id limit 1), null )))  
                   as title, IF(goal_item_submitted_evidence.evidence_type = 2, (select IF(d.doc_type = 5, url,stack_url) as stack_url  from documents d where d.id = goal_item_submitted_evidence.evidence_ref_id limit 1), NULL) as stack_url");
                if($goal_type != self::goalTypeGroup)
                {
                    $query->where("owner_id", $owner_id);
                }
            };

        }
        if($withCategories)
        {
            $relations[] = "categories";
        }
        if($withActionItems)
        {
            $more_fields = DB::raw("goal_items_feedback.id as feedback_id, goal_items_feedback.feedback, goal_items_feedback.reflection");
            if($from_listing)
            {
                $more_fields = DB::raw("goal_items_feedback.id as feedback_id");
            }
            $relations['actionItems'] = function ($query) use ($owner_id, $goal_type, $more_fields){
                $query->leftJoin("goal_items_feedback", function ($join) use ($owner_id, $goal_type){
                    $join->on("goal_items.id", "=", "goal_items_feedback.goal_item_id");
                    if($goal_type != self::goalTypeGroup)
                    {
                        $join->on("goal_items_feedback.owner_id", "=", DB::raw("'".$owner_id."'"));
                    }
                })->select("goal_items.*", $more_fields, "goal_items_feedback.is_done", "goal_items_feedback.owner_id", DB::raw("IF(goal_items_feedback.current_value IS NOT NULL, goal_items_feedback.current_value, goal_items.start_value) AS current_value"), "goal_items_feedback.created_date as feedback_created_at", "goal_items_feedback.last_edit_date as feedback_updated_at", "goal_items_feedback.completed_at as completed_at", "goal_items_feedback.is_completed_on_time");
                $query->orderBy("goal_items.item_order", "ASC");
            };
            $relations['actionItems.itemFeedbacks'] = function ($query) use ($owner_id, $goal_type, $more_fields){
                $query->select($more_fields, "goal_items_feedback.id","goal_items_feedback.goal_item_id","goal_items_feedback.goal_id","goal_items_feedback.is_done", "goal_items_feedback.owner_id", "goal_items_feedback.current_value", "goal_items_feedback.created_date as feedback_created_at", "goal_items_feedback.last_edit_date as feedback_updated_at", "goal_items_feedback.completed_at as completed_at", "goal_items_feedback.is_completed_on_time");
                if($goal_type != self::goalTypeGroup)
                {
                    $query->where("goal_items_feedback.owner_id", "<=>", ($owner_id ? $owner_id : DB::raw("(select if(goals.goal_type = 3, null, user_id) as user_id from goal_users join goals on goals.id = goal_users.goal_id where user_type = 2 and goal_id = goal_items_feedback.goal_id limit 1)")));
                }
            };
            $relations[] = "actionItems.allowedEvidence";
        }
        if($withCollaborators)
        {
            if($collaborator_id)
            {
                $relations["collaborators"] = function ($query) use ($collaborator_id)
                {
                    $query->select(DB::raw("goal_id,goal_users.user_id,email,first_name,last_name,image,permission,user_type,goal_users_settings.*"))
                        ->where("goal_users.user_id", $collaborator_id);
                };
            }
            else
            {
                $relations[] = "collaborators:goal_id,goal_users.user_id,email,first_name,last_name,image,permission,user_type,goal_users_settings.*";
            }
        }
        return $relations;

    }
    public static function setAccountOwnerForThisUser($user_id, $account_id)
    {
        //This function get called when a user made viewer or deactivated or deleted.
        // it will remove current user from goals and set account owner as owner of goals
        $account_owner_id = UserAccount::where('account_id', $account_id)->where('role_id', 100)->first()->user_id;
        //Following query set AO as owner of goal where this user was an owner
        GoalUser::where('user_id', $user_id)->where('user_type', Goal::userTypeOwner)->update(['user_id'=>$account_owner_id, 'is_archived'=>1]);
        //Following query delete where this user was collaborator
        GoalUser::where('user_id', $user_id)->delete();
        //Following query will remove duplicates that get created due to first query (e.i AO becomes owner 2 times in same goal)
        DB::select('DELETE gu2 FROM goal_users gu1, goal_users gu2 WHERE gu1.id > gu2.id AND gu1.is_archived = 0  AND gu1.goal_id = gu2.goal_id AND gu1.user_id = gu2.user_id AND gu1.user_type = gu2.user_type');

        GoalItemsFeedback::where('owner_id', $user_id)->update(['owner_id'=>$account_owner_id]);
        GoalItemSubmittedEvidence::where('owner_id', $user_id)->update(['owner_id'=>$account_owner_id]);
        Goal::where('created_by', $user_id)->update(['created_by'=>$account_owner_id]);
        return true;
    }

    public static function updateGoalSettingsForNewUser($user_id, $account_id, $role_id)
    {
        GoalsController::getGoalsSettings($account_id);//Calling this to update goal settings for new user
        $goal_user_settings_count = GoalUserSettings::where('account_id', $account_id)->where('can_archive_goals', 1)->where('role_id','>', 110)->count();
        $total_users_count = UserAccount::where('account_id', $account_id)->where('role_id','>', 110)->count();
        if($goal_user_settings_count >= ($total_users_count/2))//if 50% users have setting on then turn it on for new users as well
        {
            GoalUserSettings::where('account_id', $account_id)->where('user_id', $user_id)->where('role_id', $role_id)->update(['can_archive_goals'=>1]);
        }
    }
}