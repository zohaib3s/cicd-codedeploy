<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SNSElementalUpdates extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $timestamps = false;
    protected $table = 'sns_elemental_job_updates';
    protected $connection = 'production_global';
}