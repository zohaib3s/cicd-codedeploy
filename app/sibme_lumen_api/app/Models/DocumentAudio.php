<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class DocumentAudio extends Model
{
    public $timestamps = false;
    protected $table = 'document_audio';
    /**
     * The table associated with the model.
     *
     * @var string
     */

}
