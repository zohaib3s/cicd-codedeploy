<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountTag extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_edit_date';
    protected $table = 'account_tags as AccountTag';
    protected $casts = [
        'created_date' => 'string',
        'last_edit_date' => 'string',
    ];

    public static function getCustomMarkersByAccount($account_ids , $site_id){
      $account_ids = is_array($account_ids) ? $account_ids : [$account_ids];
      return AccountTag::whereIn('account_id', $account_ids)->where('tag_type',1)->where('site_id',$site_id)->get();
    }

    public static function getStandards($account_ids, $framework_id, $standards=[], $site_id){
      $account_ids = is_array($account_ids) ? $account_ids : [$account_ids];  
      $qry = AccountTag::whereIn('account_id', $account_ids)
                       ->where('tag_type',0)
                       ->where('site_id',$site_id);
      if(!empty($framework_id)){
        $qry->where("framework_id", $framework_id);
      }

      if(!empty($standards)){
        $qry->whereIn("account_tag_id", $standards);
      }
      return $qry->get();
    }

    public static function getFrameworkId($account_id, $huddle_id, $site_id){
      $framework_id = 0;
      $objAccountFolderMetaData = AccountFolderMetaData::where('account_folder_id', $huddle_id)
                               ->where('meta_data_name', 'framework_id')
						        					 ->where('site_id', $site_id)
						        					 ->first();
  		if($objAccountFolderMetaData && !empty($objAccountFolderMetaData->meta_data_value) ){
  			$framework_id = $objAccountFolderMetaData->meta_data_value;
  		}

      if ($framework_id == -1 || $framework_id == 0) {
        $framework_id = 0;
        $objAccountMetaData = AccountMetaData::where('account_id', $account_id)
                           ->where('meta_data_name', 'default_framework')
				        					 ->where('site_id', $site_id)
				        					 ->first();
  			if($objAccountMetaData && !empty($objAccountMetaData->meta_data_value) ){
  				$framework_id = $objAccountMetaData->meta_data_value;
  			}
  		}

		  return $framework_id;
    }

    public static function getStandardByCodeWeb($site_id, $standard_code, $account_id,$framework_id = 0 ){
        if(!empty($framework_id))
        {
             $account_tag_info  = AccountTag::where('account_tag_id', $standard_code)->where('site_id',$site_id)->first();
             if($account_tag_info)
             {
               $account_tag_info = $account_tag_info->toArray();
             }

             $account_id = $account_tag_info['account_id'];
        }

        $qry = AccountTag::where('account_id', $account_id)
                 ->where('account_tag_id', $standard_code)
                 ->where('site_id',$site_id);
        if(empty($framework_id)) {
        	$qry->whereNull('framework_id');
        } else {
        	$qry->where('framework_id', $framework_id);
        }
        return $qry->first();
    }

    public static function getVideoCommentTags($document_id, $site_id){

        return AccountTag::join('account_comment_tags as act','AccountTag.account_tag_id','=','act.account_tag_id')
                         ->where('AccountTag.tag_type', 0)
                         ->where('AccountTag.site_id', $site_id)
                         ->where('act.ref_id', $document_id)
                         ->select(['AccountTag.tag_title', 'AccountTag.tads_code', 'AccountTag.account_tag_id', 'AccountTag.standard_analytics_label' ,'act.created_date AS tags_date'])
                         ->selectRaw('COUNT(act.account_comment_tag_id) AccountTag__total_tags')
                         ->groupBy('AccountTag.account_tag_id')
                         ->orderBy('AccountTag__total_tags','DESC')
                         ->get();

/*
        return AccountTag
                       ::join('account_comment_tags as act', 'AccountTag.account_tag_id', '=', 'act.account_tag_id')
                       ->select('AccountTag.tag_title',
                                'AccountTag.tads_code',
                                'AccountTag.account_tag_id')
                       ->selectRaw('COUNT(act.account_comment_tag_id) total_tags')
                       ->selectRaw('act.created_date AS tags_date')
                       ->where(array('AccountTag.tag_type' => 0,'act.ref_id' => $document_id))
                       ->orderByRaw('total_tags DESC')
                       ->groupBy('AccountTag.account_tag_id')
                       ->limit(5)
                       ->get()
                       ->toArray();
*/
    }


    public static function getVideoCommentTagsWithoutComments($document_id,$framework_id, $site_id){
      
      return AccountTag::Leftjoin('account_comment_tags as act','AccountTag.account_tag_id','=','act.account_tag_id')
      ->Leftjoin('document_standard_ratings as dsr','dsr.standard_id','=','AccountTag.account_tag_id')
      ->where('AccountTag.tag_type', 0)
      ->where('AccountTag.site_id', $site_id)     
      ->where('framework_id' , $framework_id)
      ->where(function($query) use ($document_id){
                   $query->orWhere('act.ref_id',$document_id)
                          ->orWhere('dsr.document_id',$document_id);
               })
      ->select(['AccountTag.tag_title', 'AccountTag.tads_code', 'AccountTag.account_tag_id', 'AccountTag.standard_analytics_label' ,'act.created_date AS tags_date'])
      ->selectRaw('COUNT(act.account_comment_tag_id) AccountTag__total_tags')
      ->groupBy('AccountTag.account_tag_id')
      ->orderBy('AccountTag__total_tags','DESC')
      ->get();
  }
}