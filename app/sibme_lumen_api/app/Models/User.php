<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class User extends Model
{

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_edit_date';
    const roleAccountOwner = 100;
    const roleSuperAdmin = 110;
    const roleAdmin = 115;
    const roleUser = 120;
    const roleViewer = 125;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $casts = [
        'created_date' => 'string',
        'last_edit_date' => 'string',
    ];
    protected $table = 'users';

    public static function get_user_accounts($id, $site_id, $account_id=null)
    {
        $id = (int)$id;

        $query = User::join('users_accounts', 'users.id', '=', 'users_accounts.user_id')
            ->join('accounts', 'users_accounts.account_id', '=', 'accounts.id')
            ->join('roles', 'users_accounts.role_id', '=', 'roles.role_id')
            ->select('users.*', 'users_accounts.*', 'roles.name', 'roles.role_id', 'accounts.in_trial', 'accounts.is_suspended', 'accounts.storage_used', 'accounts.allow_launchpad', 'accounts.id as account_id', 'accounts.company_name', 'accounts.image_logo', 'accounts.header_background_color', 'accounts.nav_bg_color', 'accounts.created_at', 'accounts.usernav_bg_color', 'accounts.enable_live_rec')
            ->where(array('users.id' => $id, 'users_accounts.is_active' => '1', 'users.site_id' => $site_id));

        if(!empty($account_id)){
            $query->where('accounts.id',$account_id);
        }
        $result = $query->get()->toArray();
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    public static function get_user_type($account_id, $user_id, $site_id)
    {
        $eval_active = User::check_if_eval_huddle_active($account_id);

        $assessment_huddles_details = User::single_query($account_id, $user_id, 3);

        $coaching_huddles_details = User::single_query($account_id, $user_id, 2);

        $collaboration_huddles_details = User::single_query($account_id, $user_id, 1);

        $assessment_huddles = (!empty($assessment_huddles_details)) ? 1 : 0;
        $coaching_huddles = (!empty($coaching_huddles_details)) ? 1 : 0;
        $collaboration_huddles = (!empty($collaboration_huddles_details)) ? 1 : 0;

        $user_role_detail = UserAccount::where(array(

            'account_id' => $account_id,
            'site_id' => $site_id,
            'user_id' => $user_id,

        ))->get()->toArray();

        $user_role_details = !empty($user_role_detail[0]) ? $user_role_detail[0] : false;

        $assessment_permission = isset($user_role_details['manage_evaluation_huddles']) ? $user_role_details['manage_evaluation_huddles'] : false;

        $coaching_permission = isset($user_role_details['manage_coach_huddles']) ? $user_role_details['manage_coach_huddles'] : false;

        $collaboration_permission = isset($user_role_details['manage_collab_huddles']) ? $user_role_details['manage_collab_huddles'] : false;

        $is_user_coachee = (!empty($is_user_coachee_details)) ? 1 : 0;

        $user_type = 'Participant';
        if ($collaboration_permission && !$coaching_permission && !$assessment_permission && ($user_role_details['role_id'] == 120 || $user_role_details['role_id'] == 115)) {
            $user_type = 'Collaborator';
        }
        if ($coaching_permission && !$assessment_permission && ($user_role_details['role_id'] == 120 || $user_role_details['role_id'] == 115)) {
            $user_type = 'Coach';
        }
        if (!$coaching_permission && $assessment_permission && ($user_role_details['role_id'] == 120 || $user_role_details['role_id'] == 115)) {
            $user_type = 'Assessor';
        }
        if ($coaching_permission && $assessment_permission && ($user_role_details['role_id'] == 120 || $user_role_details['role_id'] == 115)) {
            $user_type = 'Coach/Assessor';
        }
        if (!$collaboration_permission && !$coaching_permission && !$assessment_permission && ($user_role_details['role_id'] == 120 || $user_role_details['role_id'] == 115)) {
            $user_type = 'Participant';
        }
        if ($coaching_huddles && !$collaboration_permission && !$coaching_permission && !$assessment_permission && ($user_role_details['role_id'] == 120 || $user_role_details['role_id'] == 115)) {
            $user_type = 'Coachee';
        }
        if ($assessment_huddles && !$collaboration_permission && !$coaching_permission && !$assessment_permission && ($user_role_details['role_id'] == 120 || $user_role_details['role_id'] == 115)) {
            $user_type = 'Coachee/Assessed Participant';
        }
        if ($eval_active && ($user_role_details['role_id'] == 100 || $user_role_details['role_id'] == 110)) {
            $user_type = 'Coach/Assessor';
        }
        if (!$eval_active && ($user_role_details['role_id'] == 100 || $user_role_details['role_id'] == 110)) {
            $user_type = 'Coach';
        }
        return $user_type;

    }

    public static function check_if_eval_huddle_active($account_id)
    {
        $result = Account::where(array(
            "id" => $account_id,
        ))->get()->toArray();

        if ($result[0]['is_evaluation_activated'] == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function getUsersByAccount($account_ids = [], $site_id, $role_id = null, $limit = '')
    {
        $account_ids = is_array($account_ids) ? $account_ids : [$account_ids];
        $query = User::join('users_accounts', 'users.id', '=', 'users_accounts.user_id')
        ->join('accounts', 'users_accounts.account_id', '=', 'accounts.id')
        ->join('roles', 'users_accounts.role_id', '=', 'roles.role_id')
        ->select(\DB::raw('CONCAT(users.first_name," ",users.last_name) AS `name`'), 'users.email', 'accounts.company_name AS account_name', 'roles.name AS user_role', 'users.id AS user_id','accounts.id AS account_id')
        ->where("users.is_active", 1)
        ->where("users.type", 'Active')
        ->where("users.site_id", $site_id)
        ->whereIn("users_accounts.account_id", $account_ids)
        ->whereNotIn("users.id", [2423, 2422, 2427])
        ->whereNotIn("users_accounts.role_id", [125]);
        if ($role_id) {
            $query->where("users_accounts.role_id", $role_id);
        }
        if ($limit) {
            $query->limit($limit);
        }
        $result = $query->get();
        return $result;
    }

    public static function getUsersByAccount2($account_id, $role = null, $limit = '', $start = 0, $keywords = '', $deactivated = 0,$site_id)
    {
        $account_id = (int) $account_id;

        $conditions = null;
        if ($keywords != '') {
            $keywords = '%' . $keywords . '%';
            $conditions = '(users.first_name LIKE "' . $keywords . '" OR users.last_name LIKE "' . $keywords .
                '" OR  CONCAT(users.first_name, \' \',users.last_name)  LIKE "' . $keywords . '")';
        }
        $query = User::select('users.*',\DB::raw('CONCAT(users.first_name," ",users.last_name) AS `full_name`'), 'accounts.company_name', 'users_accounts.account_id', 'users_accounts.user_id', 'users_accounts.role_id as role')
            ->leftJoin("users_accounts", 'users.id', '=', 'users_accounts.user_id')
            ->leftJoin("accounts", 'users_accounts.account_id', '=', 'accounts.id')
            ->where("users_accounts.account_id", $account_id)
            ->where("users_accounts.site_id",$site_id);
        if ($role) {
            $query->where("users_accounts.role_id", $role);
        }

        $query->where("users.is_deleted", '0');

        if ($deactivated) {
            $query->where("users_accounts.status_type","=","Inactive");
        }
        else
        {
            //$query->where("users_accounts.status_type","<>","Inactive");
            $query->where(function ($q){
                $q->where("users_accounts.status_type","<>","Inactive")->orWhereNull("users_accounts.status_type");
            });
        }
        if ($conditions) {
            $query->whereRaw($conditions);
        }
        $query->groupBy('users.id')->orderBy('users.first_name', 'ASC');
        if ($limit) {
            $query->limit($limit)->offset($start);
        }
        $result = $query->get();
        return $result;
    }

    public static function single_query($account_id, $user_id, $metadeta)
    {

        $cnt = app('db')->select("SELECT
              afu.`user_id`
            FROM
              account_folders AS af 
              JOIN `account_folders_meta_data` AS afmd 
                ON afmd.`account_folder_id` = af.`account_folder_id` 
              JOIN `account_folder_users` AS afu 
                ON afu.`account_folder_id` = af.`account_folder_id` 
            WHERE afmd.`meta_data_name` = 'folder_type' 
              AND afmd.`meta_data_value` = " . $metadeta . "
              AND af.`account_id` = " . $account_id . " 
              AND af.active IN (
                IF(
                  (af.`archive` = 1)
                  AND (af.`active` = 0),
                  0,
                  1
                )
              )
              AND afu.`user_id` = " . $user_id);
        return $cnt;
    }
    public static function isUserEmailExists($email, $user_id = 0,$site_id)
    {
        return User::where(array('users.email' => $email,'users.site_id'=>$site_id))->whereRaw('users.id <> '.$user_id)->count();
    }

    public static function isEmailExistInAccount($account_id, $emails)
    {
        $query = User::join("users_accounts", "users.id", "=", "users_accounts.user_id");
        if(!is_array($emails)){
            $emails = (array)$emails;
        }
        return $query->whereIn("users.email",$emails)
              ->where('users_accounts.account_id', $account_id)
              ->count();
    }

    public static function getUserInformation($account_id, $id)
    {
        $account_id = (int) $account_id;
        $id = (int) $id;

        $result = User::select('users.*', 'users_accounts.*', 'roles.name', 'roles.role_id', 'roles.role_id as role', 'accounts.in_trial', 'accounts.id as account_id', 'accounts.braintree_customer_id', 'accounts.braintree_subscription_id', 'accounts.company_name', 'accounts.image_logo', 'accounts.header_background_color', 'accounts.nav_bg_color', 'accounts.created_at', 'users.id as id')
            ->join("users_accounts", 'users.id', '=', 'users_accounts.user_id')
            ->join("accounts", 'users_accounts.account_id', '=', 'accounts.id')
            ->join("roles", 'users_accounts.role_id', '=', 'roles.role_id')
            ->where('users_accounts.user_id', $id)
            ->where('users_accounts.account_id', $account_id)->first();

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
    public static function getTotalUsers($account_id)
    {
        $account_id = (int) $account_id;

        // $joins = array(
        //     array(
        //         'table' => 'users_accounts',
        //         'type' => 'inner',
        //         'conditions' => 'users.id = users_accounts.user_id',
        //     ),
        // );
        // $fields = array('users.*');
        // $result = $this->find('count', array(
        //     'joins' => $joins,
        //     'conditions' => array('users_accounts.account_id' => $account_id, 'users_accounts.role_id NOT IN ("125") AND users.id NOT IN ("2423","2422","2427","4681")'),
        //     'fields' => $fields,
        // ));

        return User::join('users_accounts', 'users.id', '=', 'users_accounts.user_id')
            ->where('users_accounts.account_id', $account_id)
            ->whereRaw('users_accounts.is_active <> 0')
            ->whereRaw('users_accounts.role_id NOT IN ("125") AND users.id NOT IN ("2423","2422","2427","4681")')
            ->count();

    }

    /* GET USER BY ROLE*/
    public static function getUsersByRole($account_id, $role_ids, $user_id = FALSE)
    {
        $account_id = (int)$account_id;
        //$role_id = (int) $role_id;

        $users = DB::table('users')
            ->select('users.*', 'accounts.company_name', 'users_accounts.user_id', 'users_accounts.role_id', 'roles.name')
            ->join('users_accounts', 'users.id', '=', 'users_accounts.user_id')
            ->join('accounts', 'users_accounts.account_id', '=', 'accounts.id')
            ->join('roles', 'users_accounts.role_id', '=', 'roles.role_id')
            ->where(['users_accounts.account_id' => $account_id])
            ->whereIn( 'roles.role_id', $role_ids)
            ->where('users.id','!=',$user_id)
            ->whereRaw('users.id NOT IN ("2423","2422","2427","4681")')
            ->groupBy('users.id')
            ->orderBy('users.first_name', 'ASC')
            ->get();

        return $users;
    }
    
    public static function get($user_id) {
    
        return User::select('users.*', 'users_accounts.*', 'roles.name', 'roles.role_id', 'accounts.in_trial', 'accounts.is_suspended', 'accounts.storage_used', 'accounts.allow_launchpad', 'accounts.id as account_id', 'accounts.company_name', 'accounts.image_logo', 'accounts.header_background_color', 'accounts.nav_bg_color', 'accounts.created_at', 'accounts.usernav_bg_color', 'accounts.enable_live_rec')
        ->leftJoin('users_accounts','users.id','=','users_accounts.user_id')
        ->leftJoin('accounts','users_accounts.account_id','=','accounts.id')
        ->leftJoin('roles','users_accounts.role_id','=','roles.role_id')
        ->where('users_accounts.user_id', $user_id)
        ->first();
    }
    
    public static function isUserExists($user_name, $user_id = 0,$site_id) {
       return User::where(array('users.username' => $user_name,'users.site_id'=>$site_id))->whereRaw('users.id <> '.$user_id)->count();
    }
    
    /**
     * Check if user is associated with the given account
     */
    public static function check_if_user_associated_with_account($site_id, $account_id, $user_id){
        return User::join('users_accounts', 'users.id', '=', 'users_accounts.user_id')
                        ->where(array('users.id' => $user_id, 'users_accounts.account_id' => $account_id, 'users.site_id' => $site_id))
                        ->exists();
    }
}
