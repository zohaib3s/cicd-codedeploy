<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_groups';
    
    
    public static function get_user_group($user_id, $site_id) {
        $user_id = (int) $user_id;
        
        $result = UserGroup::join('account_folder_groups', 'user_groups.group_id', '=', 'account_folder_groups.group_id')
		                ->select('account_folder_groups.*', 'user_groups.*')
		                ->where(array('user_groups.user_id' => $user_id, 'user_groups.site_id'=> $site_id))->get()->toArray();
        
        
        
        
        return $result;
    }
    
    public static function get_group_users($group_ids = []){
        settype($group_ids, "array");
        return UserGroup::whereIn('group_id',$group_ids)->distinct()->select('user_id')->pluck('user_id')->toArray();
    }
    
    
    
}