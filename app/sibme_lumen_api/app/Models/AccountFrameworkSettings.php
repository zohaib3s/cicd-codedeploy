<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountFrameworkSettings extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'account_framework_settings as AccountFrameworkSettings';
}