<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentsFilestackData extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $timestamps = false;
    public $table = "documents_filestack_data";
}