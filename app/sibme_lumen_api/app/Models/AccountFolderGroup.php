<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountFolderGroup extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'account_folder_groups';
    
    
    public static function getHuddleGroups($account_folder_id, $site_id) {
        $account_folder_id = (int) $account_folder_id;
      
        $result = AccountFolderGroup::join('user_groups', 'user_groups.group_id', '=', 'account_folder_groups.group_id')
                        ->select(
                                'user_groups.user_id',
                                'account_folder_groups.*'
                                )->where(array(
                                    'account_folder_groups.account_folder_id' => $account_folder_id,
                                    'account_folder_groups.site_id' => $site_id                   
                                ))->get()->toArray();
        
        return !empty($result) ? $result : false;
    }
    
    
    
}

