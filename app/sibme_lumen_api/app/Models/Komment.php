<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Komment extends Model
{
    //This is made beacuse the comment model is not optimized as per laravel standard
    //even it has confilicts with model class working.
    //This is for comments table
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'comments';
    protected $fillable = ['parent_comment_id','title','comment','ref_type','ref_id','user_id','time','restrict_to_users','created_by','created_date','last_edit_by','last_edit_date','active','audio_duration','published_by','created_at_gmt'];
    public $timestamps = false;
    
    public function replies()
    {
        return $this->hasMany(Komment::class,'parent_comment_id','id')->with('replies');
    }
   
}