<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class AnalyticsLoginSummary extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */

    /**
     * Read Operations:
     * 1. To update/insert, select 1 item by account id, huddle_id and created_at using hash. 
     * 2. For analytics, select all records by account id and created_at between START and END date.
     * - - After selection, sum records by users.
     * Write Operations:
     * 1. Insert new record on every first activity in a day.
     * 2. Update record based on account id, huddle_id and created_at using hash.
     * 3. Update/Insert a particular User record in above selected main record with increment 1 in the given metric.
     */

    /**
     * Notes:
     * Use SaveRecord function to log entries on each related activity.
     * Create a Hash with md5(AccountID+HuddleID+CreatedAt) and save that hash, mark it as indexed key.
     * On Every Upsert operation use Hash to search for the Huddle record.
     * create another index on account id + created_at.
     */

    protected $table = 'analytics_login_summary';
    protected $fillable = ['hash_key', 'created_at', 'account_id', 'user_id', 'user_type', 'web_login_counts', 'is_user_active'];

    protected function getHashKey($account_id, $user_id, $date){
        if(!Carbon::createFromFormat('Y-m-d', $date)){
            return false;
        }
        return md5($account_id.$user_id.$date);
    }

    public function saveRecord($account_id, $user_id, $site_id, $date=null, $activity_value=1){
        if(empty($date) || $date=="0000-00-00"){
            $date = date("Y-m-d");
        }
        $hash_key = $this->getHashKey($account_id, $user_id, $date);

        $item = $this->where("hash_key",$hash_key)->first();

        if(!empty($item)){
            // Update
            $item->web_login_counts += $activity_value;
        } else {
            $item = new self();
            // Insert
            $item->hash_key = $hash_key;
            $item->user_id = $user_id;
            $item->is_user_active = 1;
            $item->account_id = $account_id;
            $item->created_at = $date;
            $item->site_id = $site_id;
            $item->web_login_counts = $activity_value;
        }
        return $item->save();
    }

}