<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerformanceLevelDescription extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'performance_level_descriptions as PerformanceLevelDescription';
}
