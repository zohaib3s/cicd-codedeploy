<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AssessmentCustomField extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'assessment_custom_fields';
    protected $fillable = ['account_folder_id', 'custom_field_id', 'created_by', 'created_at', 'updated_by', 'updated_at', 'site_id'];

    public static function get_assessment_custom_fields($account_id, $account_folder_id=null, $strict = false){
        $account_folder_qry = empty($account_folder_id) ? "1=0" : " acf.`account_folder_id` = ".$account_folder_id;
        $join_type = $strict ? " INNER " : " LEFT OUTER "; 
        return DB::select("SELECT cf.id as custom_field_id, cf.`field_label`, cf.`field_type`, acf.`id` AS assessment_custom_field_id
                            FROM (
                                SELECT id, field_label, field_type FROM `custom_fields` WHERE `account_id` = (SELECT `parent_account_id` FROM accounts WHERE id = ".$account_id.") AND is_shared = 1 AND ref_type = 2 
                                UNION
                                SELECT id, field_label, field_type FROM `custom_fields` WHERE `account_id` = ".$account_id." AND ref_type = 2 
                            ) AS cf 
                            ".$join_type." JOIN `assessment_custom_fields` AS acf ON cf.id=acf.`custom_field_id` AND ".$account_folder_qry." ORDER BY cf.id ASC
                        ");
    }
    
    public static function get_assessment_custom_fields_of_account($user_id , $account_id , $cf_id , $account_folder_id = '' , $strict = false){
        $account_folder_qry = empty($account_folder_id) ? "1=1" : " acf.`account_folder_id` = ".$account_folder_id;
        $join_type = $strict ? " INNER " : " LEFT OUTER "; 
        return DB::select("SELECT uacf.* , cf.id as custom_field_id, cf.`field_label`, cf.`field_type`, acf.`id` AS assessment_custom_field_id
                            FROM (
                                SELECT id, field_label, field_type FROM `custom_fields` WHERE `account_id` = (SELECT `parent_account_id` FROM accounts WHERE id = ".$account_id.") AND is_shared = 1 AND ref_type = 2 AND id = ".$cf_id." 
                                UNION
                                SELECT id, field_label, field_type FROM `custom_fields` WHERE `account_id` = ".$account_id." AND ref_type = 2 AND id = ".$cf_id."
                            ) AS cf 
                            LEFT JOIN `assessment_custom_fields` AS acf ON cf.id=acf.`custom_field_id` AND ".$account_folder_qry." 
                            LEFT JOIN `user_assessment_custom_fields` AS uacf ON uacf.assessment_custom_field_id = acf.id AND uacf.`assessee_id`=".$user_id." ORDER BY cf.id ASC    
                        ");
    }

    public static function save_assessment_custom_fields($account_folder_id, $current_user_id, $assessment_custom_fields){
        // Only send objects which are checked. 
        // Get ids of existing assessment_custom_fields of this assessment. 
        // Loop on existing assessment_custom_fields. 
        // Check if the field does not exist in new assessment_custom_fields. 
        //      -> delete this assessment_custom_field + delete related user_assessment_custom_field records.

        // Loop on new assessment_custom_fields. 
        // Check if the field does not exists in existing assessment_custom_fields. 
        //      -> if yes then add new record.
        $new_checked_ids = [];
        foreach($assessment_custom_fields as $field){
            if(!empty($field['assessment_custom_field_id'])){
                $new_checked_ids [] = $field['assessment_custom_field_id'];
            }
        }

        $existing_assessment_custom_field_ids = self::where('account_folder_id', $account_folder_id)->pluck('id')->toArray();
        foreach ($existing_assessment_custom_field_ids as $existing_id) {
            if(!in_array($existing_id, $new_checked_ids)){
                UserAssessmentCustomField::where('assessment_custom_field_id', $existing_id)->delete();
                self::where('id', $existing_id)->delete();
            }
        }

        foreach($assessment_custom_fields as $field){
            if(!in_array($field['assessment_custom_field_id'], $existing_assessment_custom_field_ids)){
                $acf = new self();
                $acf->account_folder_id = $account_folder_id;
                $acf->custom_field_id = $field['custom_field_id'];
                $acf->created_by = $current_user_id;
                $acf->updated_by = $current_user_id;
                $acf->save();
            }
        }

    }

    public static function delete_assessment_custom_fields($custom_field_id){
        UserAssessmentCustomField::whereIn("assessment_custom_field_id", function ($query) use ($custom_field_id) {
            $query->select('id')
                ->from('assessment_custom_fields')
                ->where('custom_field_id', $custom_field_id);
        })->delete();
        self::where('custom_field_id', $custom_field_id)->delete();
    }

    public static function get_account_folder_info($assessment_custom_field_id){
        return self::join('account_folders', 'assessment_custom_fields.account_folder_id', '=', 'account_folders.account_folder_id')
                   ->where('assessment_custom_fields.id',$assessment_custom_field_id)
                   ->select('assessment_custom_fields.account_folder_id', 'account_folders.account_id')
                   ->first();
    }
}