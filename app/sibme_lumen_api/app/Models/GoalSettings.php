<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoalSettings extends Model
{
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_edit_date';
}