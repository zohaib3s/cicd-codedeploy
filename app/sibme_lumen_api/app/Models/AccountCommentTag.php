<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountCommentTag extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'account_comment_tags as AccountCommentTag';

    public static function gettagsbycommentid($site_id, $comment_id,$tag_type){
       return AccountCommentTag::leftJoin('account_tags', 'AccountCommentTag.account_tag_id', '=', 'account_tags.account_tag_id')
                ->select('AccountCommentTag.*','tag_code')
                ->where('comment_id', $comment_id)
                ->where('AccountCommentTag.site_id', $site_id)
                ->whereIn('ref_type', $tag_type)
                ->get()
                ->toArray();
    }

    public static function getUserParticipationCountByTag($account_tag_id, $user_id, $start_date, $end_date, $site_id){
        return AccountCommentTag::where('account_tag_id', $account_tag_id)
                                ->where('created_by', $user_id)
                                ->where('site_id', $site_id)
                                ->whereBetween('created_date',[date("Y-m-d",strtotime($start_date)), date("Y-m-d",strtotime($end_date))])
                                ->count();
    }
    
    public static function getUserParticipationCountByTagCoachCoachee($account_tag_id, $user_id, $start_date, $end_date, $site_id, $role_id){
        return AccountCommentTag::join('account_folder_documents as afd', 'afd.document_id', '=', 'AccountCommentTag.ref_id')
                                ->join('account_folder_users as afu', 'afu.account_folder_id', '=', 'afd.account_folder_id')
                                ->join('account_folders_meta_data as afmd', 'afmd.account_folder_id', '=', 'afd.account_folder_id')
                                ->where('AccountCommentTag.account_tag_id', $account_tag_id)
                                //->where('AccountCommentTag.created_by', $user_id)
                                ->where('AccountCommentTag.site_id', $site_id)
                                ->where('afu.role_id',$role_id)
                                ->where('afu.user_id',$user_id)
                                ->where('afmd.meta_data_name','folder_type')
                                ->where('afmd.meta_data_value','2')
                                ->whereBetween('AccountCommentTag.created_date',[date("Y-m-d",strtotime($start_date)) . " 00:00:00" , date("Y-m-d",strtotime($end_date)) . " 23:59:59" ])
                                ->count();
    }

}