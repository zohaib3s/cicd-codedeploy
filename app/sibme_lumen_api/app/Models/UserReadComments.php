<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class UserReadComments extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_read_comments';
    public $timestamps = false;

    public static function checkIfCommentAlreadyExists($comment_id, $user_id){
        $comment = self::where('comment_id', $comment_id)->where('user_id',$user_id)->count();
        if($comment > 0){
            return false;
        } else {           
            return true;
        }
    }

    /**
     * @see https://stackoverflow.com/a/25472319/470749
     * 
     * @param array $arrayOfArrays
     * @return bool
     */
    public static function insertIgnore($arrayOfArrays) {
        $static = new static();
        $table = with(new static)->getTable(); //https://github.com/laravel/framework/issues/1436#issuecomment-28985630
        // $table = $this->table;
        $questionMarks = '';
        $values = [];
        foreach ($arrayOfArrays as $k => $array) {
            if ($k > 0) {
                $questionMarks .= ',';
            }
            $questionMarks .= '(?' . str_repeat(',?', count($array) - 1) . ')';
            $values = array_merge($values, array_values($array));//TODO
        }
        $query = 'INSERT IGNORE INTO ' . $table . ' (' . implode(',', array_keys($array)) . ') VALUES ' . $questionMarks;

        // Following is to retry in case of 1213 Deadlock.
        $attempts = 0;
        do {
            try
            {
                return \DB::insert($query, $values);
            } catch (\Exception $e) {
                Log::info($e->getMessage());
                $attempts++;
                sleep(1);
                continue;
            }
            break;
        } while($attempts < 2);
        return false;
    }    
}
