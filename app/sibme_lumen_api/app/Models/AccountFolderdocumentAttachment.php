<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountFolderdocumentAttachment extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'account_folderdocument_attachments';
    public $timestamps = false;
}