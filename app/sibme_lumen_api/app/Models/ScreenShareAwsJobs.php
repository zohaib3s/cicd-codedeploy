<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ScreenShareAwsJobs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'screen_share_aws_jobs';
}