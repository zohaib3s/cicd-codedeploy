<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoalUserSettings extends Model
{
    protected $table = 'goal_users_settings';
    public $timestamps = false;

}