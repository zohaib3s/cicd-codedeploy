<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CustomField extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'custom_fields';
    protected $fillable = ['account_id', 'ref_type', 'field_label', 'field_type', 'is_shared', 'created_by', 'created_at', 'updated_by', 'updated_at', 'site_id'];

    public static function get_custom_fields($account_id, $ref_type=null){
        $ref_type_query = "";
        if(!empty($ref_type)){
            $ref_type_query = " AND ref_type = ".$ref_type;
        }
        $custom_fields = DB::select("
                                        SELECT * FROM `custom_fields` WHERE `account_id` = (SELECT `parent_account_id` FROM accounts WHERE id = ".$account_id.") AND is_shared = 1 ".$ref_type_query." 
                                        UNION
                                        SELECT * FROM `custom_fields` WHERE `account_id` = ".$account_id." ".$ref_type_query
                                    );
        return self::formulate_custom_fields_array($custom_fields, $account_id);
    }

    public static function formulate_custom_fields_array($custom_fields, $account_id){
        $array = [];
        $array['user_summary']['parent_custom_fields'] = [];
        $array['assessment']['parent_custom_fields'] = [];
        $array['user_summary']['custom_fields'] = [];
        $array['assessment']['custom_fields'] = [];

        foreach ($custom_fields as $field) {
            if($field->account_id != $account_id){
                if($field->ref_type == '1'){
                    $array['user_summary']['parent_custom_fields'][] = $field;
                } else {
                    $array['assessment']['parent_custom_fields'][] = $field;
                }
            } else {
                if($field->ref_type == '1'){
                    $array['user_summary']['custom_fields'][] = $field;
                } else {
                    $array['assessment']['custom_fields'][] = $field;
                }
            }
        }
        return $array;
    }

    public static function can_user_edit_custom_fields($user_role_id, $account_id){
        $allowed = 1;
        $account = Account::find($account_id);
        if($account){
            if($user_role_id=='115'){
                $allowed = $account->admins_can_edit_cf;
            }
            if($user_role_id=='120'){
                $allowed = $account->users_can_edit_cf;
            }
            if($user_role_id=='125'){
                $allowed = 0;
            }
        }
        return $allowed;
    }

    public static function delete_children($custom_field){
        if($custom_field->ref_type=='1'){
            UserCustomField::where('custom_field_id',$custom_field->id)->delete();
        }elseif($custom_field->ref_type=='2'){
            AssessmentCustomField::delete_assessment_custom_fields($custom_field->id);
        }
    }

    public static function unshare($custom_field_id, $is_shared){
        if(!empty($is_shared)){
            return $is_shared;
        }
        $custom_field = self::find($custom_field_id);
        if(!$custom_field){
            return false;
        }
        $old_is_shared = $custom_field->is_shared;
        if(empty($is_shared) && $is_shared!==$old_is_shared){
            self::delete_children($custom_field);
        }
    }

    public static function delete_carefully($custom_field){
        self::delete_children($custom_field);
        $custom_field->delete();
    }

}