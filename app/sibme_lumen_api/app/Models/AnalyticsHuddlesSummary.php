<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class AnalyticsHuddlesSummary extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */

    /**
     * Read Operations:
     * 1. To update/insert, select 1 item by account id, huddle_id and created_at using hash. 
     * 2. For analytics, select all records by account id and created_at between START and END date.
     * - - After selection, sum records by users.
     * Write Operations:
     * 1. Insert new record on every first activity in a day.
     * 2. Update record based on account id, huddle_id and created_at using hash.
     * 3. Update/Insert a particular User record in above selected main record with increment 1 in the given metric.
     */

    /**
     * Notes:
     * Use SaveRecord function to log entries on each related activity.
     * Create a Hash with md5(AccountID+HuddleID+CreatedAt) and save that hash, mark it as indexed key.
     * On Every Upsert operation use Hash to search for the Huddle record.
     * create another index on account id + created_at.
     */

    protected $table = 'analytics_huddles_summary';
    protected $fillable = ['hash_key', 'created_at', 'account_id', 'parent_account_id', 'is_account_active', 'huddle_id', 'is_huddle_active', 'created_by', 'folder_type', 'user_id', 'company_name', 'username', 'email', 'is_user_active', 'user_role_id', 'video_upload_counts', 'workspace_upload_counts', 'library_upload_counts', 'shared_upload_counts', 'library_shared_upload_counts', 'videos_viewed_count', 'workspace_videos_viewed_count', 'library_videos_viewed_count', 'huddle_created_count', 'comments_initiated_count', 'workspace_comments_initiated_count', 'replies_initiated_count', 'documents_uploaded_count', 'documents_viewed_count', 'scripted_observations', 'scripted_video_observations', 'total_hours_uploaded', 'total_hours_viewed', 'workspace_resources_uploaded', 'workspace_resources_viewed', 'huddle_discussion_created', 'huddle_discussion_posts', 'scripted_notes_shared_upload_counts', 'live_sessions_count'];
    protected $activities = [
                                '2-4-1' => 'video_upload_counts', 
                                '2-4-3' => 'workspace_upload_counts', 
                                '2-4-2' => 'library_upload_counts', 
                                '22-1' => 'shared_upload_counts', 
                                '22-2' => 'library_shared_upload_counts', 
                                '11-1' => 'videos_viewed_count', 
                                '11-3' => 'workspace_videos_viewed_count', 
                                '11-2' => 'library_videos_viewed_count', 
                                '1' => 'huddle_created_count', 
                                '5-1' => 'comments_initiated_count', 
                                '5-3' => 'workspace_comments_initiated_count', 
                                '8' => 'replies_initiated_count', 
                                '3-29-1' => 'documents_uploaded_count', 
                                '13-1' => 'documents_viewed_count', 
                                '23' => 'scripted_observations', 
                                '20' => 'scripted_video_observations', 
                                'total_hours_uploaded' => 'total_hours_uploaded', 
                                'total_hours_viewed' => 'total_hours_viewed', 
                                '3-29-3' => 'workspace_resources_uploaded', 
                                '13-3' => 'workspace_resources_viewed', 
                                '6' => 'huddle_discussion_created', 
                                '8-1' => 'huddle_discussion_posts', 
                                '26' => 'scripted_notes_shared_upload_counts',
                                '24' => 'live_sessions_count',
                                '66' => 'documents_comments_count',
                            ];

    protected function getHashKey($account_id, $huddle_id, $user_id, $date){
        if(!Carbon::createFromFormat('Y-m-d', $date)){
            return false;
        }
        return md5($account_id.$huddle_id.$user_id.$date);
    }

    public function saveRecord($account_id, $huddle_id, $user_id, $site_id, $activity_type, $date=null, $activity_value=1, $decrement=false){
        if(empty($date) || $date=="0000-00-00"){
            $date = date("Y-m-d");
        }

        if( empty($account_id) || empty($huddle_id) || empty($user_id) || !in_array($activity_type,[2,4,22,11,1,5,8,3,29,13,23,20,6,24,26,'8-1','total_hours_uploaded','total_hours_viewed','66']) || !Carbon::createFromFormat('Y-m-d', $date)){
            return false;
        }
        
        $hash_key = $this->getHashKey($account_id, $huddle_id, $user_id, $date);
        $activity = $this->getActivityName($activity_type, $huddle_id);
        if(!$activity){
            return false;
        }

        $attempts = 0;
        do {
            try
            {
                $item = $this->where("hash_key",$hash_key)->first();
                if(!empty($item)){
                    // Update
                    if($decrement){
                        if(($item->$activity-$activity_value) >= 0  ){

                            $item->$activity -= $activity_value;
                        }
                    } else {
                        $item->$activity += $activity_value;
                    }
                } else {
                    // Insert
                    $item = $this->newItem($hash_key, $account_id, $huddle_id, $user_id, $site_id, $activity, $activity_value, $date);
                }
                return $item->save();
            } catch (\Exception $e) {
                $attempts++;
                sleep(1);
                continue;
            }        
            break;
        } while($attempts < 2);



/*
        $item = $this->where("hash_key",$hash_key)->first();
        if(!empty($item)){
            // Update
            if($decrement){
                $item->$activity -= $activity_value;
            } else {
                $item->$activity += $activity_value;
            }
        } else {
            // Insert
            $item = $this->newItem($hash_key, $account_id, $huddle_id, $user_id, $activity, $activity_value, $date);
        }
        return $item->save();
*/

    }

    protected function newItem($hash_key, $account_id, $huddle_id, $user_id, $site_id, $activity, $activity_value, $date){
        $item = new self();
        $account = Account::find($account_id);
        $huddle = AccountFolder::where("account_folder_id", $huddle_id)->first();
        $user = User::find($user_id);
        $item->hash_key = $hash_key;
        $item->created_at = $date;
        $item->account_id = $account_id;
        $item->parent_account_id = $account->parent_account_id; 
        $item->company_name = $account->company_name; 
        $item->is_account_active = 1;
        $item->huddle_id = $huddle_id;
        $item->is_huddle_active = 1;
        $item->created_by = $huddle->created_by;
        $item->folder_type = $huddle->folder_type;
        $item->user_id = $user_id; 
        $item->username = $user->username; 
        $item->email = $user->email; 
        $item->is_user_active = 1; 
        $item->site_id = $site_id;
        $item->user_role_id = UserAccount::where("account_id", $account_id)->where("user_id", $user_id)->value("role_id");
        $this->addUserDefaultMetrics($item, $activity, $activity_value);
        return $item;
    }

    protected function addUserDefaultMetrics(&$item, $for_activity, $for_activity_value){
        foreach ($this->activities as $activity) {
            if($activity==$for_activity){
                $item->$activity = $for_activity_value;
            } else {
                $item->$activity = 0;
            }
/*
            if($activity==$for_activity && ($activity=="total_hours_uploaded" || $activity=="total_hours_viewed")){
                $item->$activity = $for_activity_value;
            } else if($activity==$for_activity && $for_activity_value<0 && !is_null($for_activity_value)){
                $item->$activity = $for_activity_value;
            } else if($activity==$for_activity){
                $item->$activity = 1;
            } else {
                $item->$activity = 0;
            }
*/
        }
    }

    protected function getActivityName($activity_type_id, $huddle_id){
        $key = $activity_type_id;
        if(in_array($activity_type_id, [2,4,22,11,5,3,29,13])){
            $folder_type = AccountFolder::where('account_folder_id' , $huddle_id)->value('folder_type');
            if($activity_type_id==2 || $activity_type_id==4){
                $key = "2-4-".$folder_type;
            } elseif($activity_type_id==3 || $activity_type_id==29){
                $key = "3-29-".$folder_type;
            } else {
                $key = $activity_type_id ."-". $folder_type; 
            }
        }
        return isset($this->activities[$key]) ? $this->activities[$key] : false;
    }

}