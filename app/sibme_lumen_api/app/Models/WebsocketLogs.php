<?php

namespace App\Models;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class WebsocketLogs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $table = 'websocket_push_notification_logs';
    public $timestamps = false;
}