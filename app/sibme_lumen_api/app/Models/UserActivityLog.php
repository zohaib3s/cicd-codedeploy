<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserActivityLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_activity_logs as UserActivityLog';

    public static function user_activity_logs($site_id, $data, $l_account_id = '', $l_user_id = '', $ref_type = '') {
        $data['account_id'] = $l_account_id;
        $data['user_id'] = $l_user_id;
        $data['site_id'] = $site_id;

        $data['date_added'] = date('Y-m-d H:i:s');
        $data['app_name'] = self::get_app_name();

        $result = \DB::table('user_activity_logs')->insert($data);

        if(!empty($ref_type) && $data['type'] == '8'){
            $data['type'] = $data['type'] . "-" . $ref_type;
        }
        $analyticsHuddlesSummary = new \App\Models\AnalyticsHuddlesSummary;
        $analyticsHuddlesSummary->saveRecord($data['account_id'], $data['account_folder_id'], $data['user_id'], $data['site_id'], $data['type']);

        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public static function get_huddle_activity($site_id, $account_id, $huddle){
        $account_id = (int) $account_id;

        $nowDate = date("Y-m-d", time());
        $nowDate = strtotime($nowDate);
        $nowDate = strtotime("-7 day", $nowDate);

        $nextDate = date('Y-m-d', $nowDate);
        $results = app('db')->select("SELECT 
                `UserActivityLog`.*,
                `User`.`id` AS `user_id`,
                `User`.`first_name`,
                `User`.`last_name`,
                `User`.`image` 
            FROM
                `user_activity_logs` AS `UserActivityLog` 
                INNER JOIN `users` AS `User` 
                ON (
                    `User`.`id` = `UserActivityLog`.`user_id`
                ) 
            WHERE `UserActivityLog`.`site_id` = $site_id 
                AND `UserActivityLog`.`date_added` >= $nextDate 
                AND `UserActivityLog`.`account_id` = $account_id 
                AND `UserActivityLog`.`account_folder_id` = $huddle 
                AND `UserActivityLog`.`type` IN (1, 2, 3, 5, 6, 4, 8, 7, 17, 18, 19, 20, 21, 22, 29, 30) 
            ORDER BY `UserActivityLog`.`date_added` DESC 
        LIMIT 15");
        return $results;
    }

    public static function get_app_name(){
        if(isset($_GET['app_name']) && !empty($_GET['app_name'])){
            return $_GET['app_name'];
        }
        if(isset($_POST['app_name']) && !empty($_POST['app_name'])){
            return $_POST['app_name'];
        }
        $app_name = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '';
        if(!empty($app_name)){
            return $app_name;
        } else {
            return "NA";
        }
    }

}