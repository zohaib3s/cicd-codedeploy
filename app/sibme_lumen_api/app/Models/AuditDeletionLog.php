<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditDeletionLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'audit_deletion_log as AuditDeletionLog';
}
?>