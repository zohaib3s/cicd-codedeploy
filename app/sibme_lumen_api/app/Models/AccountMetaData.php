<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccountMetaData extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'account_meta_data';

    public static function getRatingName($average, $account_id) {
    	$average_rating_name_details = AccountMetaData::where('account_id', $account_id)
						    						  ->where('meta_data_value', $average)
						    						  ->where('meta_data_name', 'like', 'metric_value_%')
						    						  ->first();

		$average_rating_name = 'No Rating';
		if($average_rating_name_details){
			$average_rating_name = substr($average_rating_name_details->meta_data_name, 13);
		}

        return $average_rating_name;
    }

}