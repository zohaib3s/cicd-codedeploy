<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentStandardRating extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
   protected $table = 'document_standard_ratings as DocumentStandardRating';
   public static function getRatings($document_id, $standard_id, $account_id, $site_id)
   {
    	return DocumentStandardRating::where('document_id', $document_id)
                                     ->where('standard_id', $standard_id)
    								 ->where('site_id', $site_id)
    								 ->where('account_id', $account_id)
    								 ->get();
   }

   public static function getAveragePerformanceLevelRating($account_folder_id, $document_created_by=null, $document_id=null){
      $avg_pl =  DocumentStandardRating::join('documents', 'documents.id', '=', 'DocumentStandardRating.document_id')
                                       ->where('DocumentStandardRating.account_folder_id', $account_folder_id)
                                       ->when(!empty($document_created_by), function($q) use ($document_created_by){
                                          $q->where('documents.created_by', $document_created_by);
                                       })
                                       ->when(!empty($document_id), function($q) use ($document_id){
                                          $q->where('documents.id', $document_id);
                                       })
                                       ->avg('DocumentStandardRating.rating_value');
      return round($avg_pl);
   }
}