<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAccount extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_accounts as UserAccount';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_edit_date';
    protected $casts = [
        'created_date' => 'string',
        'last_edit_date' => 'string',
    ];
    public static function get($account_id, $user_id) {
        $account_id = (int) $account_id;
        $user_id = (int) $user_id;

        return UserAccount::select('UserAccount.*', 'accounts.*', 'users.*', 'roles.*','UserAccount.id as user_account_id')
        ->leftJoin('accounts','UserAccount.account_id','=','accounts.id')
        ->leftJoin('users','UserAccount.user_id','=','users.id')
        ->leftJoin('roles','UserAccount.role_id','=','roles.role_id')
        ->where('UserAccount.account_id', $account_id)
        ->where('UserAccount.user_id', $user_id)
        ->first();
    }
    public static function get_all_user_ids($roles=[], $account_id=null, $check_no_permission_view_analytics=false, $only_active_accounts=true, $only_active_users=true) {
        $query = UserAccount::select('UserAccount.account_id', 'UserAccount.user_id', 'users.site_id')
                            ->leftJoin('accounts','UserAccount.account_id','=','accounts.id')
                            ->leftJoin('users','UserAccount.user_id','=','users.id')
                            ->where('UserAccount.user_id', '!=', '0');
        if(!empty($roles) && is_array($roles)){
            $query->whereIn('UserAccount.role_id', $roles);
        }
        if(!empty($roles) && !is_array($roles)){
            $query->where('UserAccount.role_id', $roles);
        }
        if(!empty($check_no_permission_view_analytics)){
            $query->where('UserAccount.permission_view_analytics', '0');
        }
        if(!empty($account_id)){
            $query->where('accounts.id',$account_id);
        }
        if(!empty($only_active_accounts)){
            $query->where('accounts.is_suspended',0);
            $query->where('accounts.is_active',1);
        }
        if(!empty($only_active_users)){
            $query->where('users.is_active',1);
            $query->where('UserAccount.is_active',1);
        }
        return $query->get();
    }

    public static function setDefaultAccountForUser($user_id, $account_id=null){
        UserAccount::where('user_id', $user_id)->update(['is_default' => 0]);
        if (!empty($account_id)) {
            $user_account = \DB::table('users_accounts')->where('user_id', $user_id)->where('account_id',$account_id)->limit(1)->update(['is_default' => 1]);
        } else {
            // Set first active found account as default for this user automatically.
            $user_account = \DB::table('users_accounts')->where('user_id', $user_id)->where('is_active', '1')->where('status_type', 'Active')->limit(1)->update(['is_default' => 1]);
        }
    }
}