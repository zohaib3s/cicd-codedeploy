<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SNSAuditLogs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $timestamps = false;
    protected $table = 'audit_logs';
    protected $connection = 'production_global';
}