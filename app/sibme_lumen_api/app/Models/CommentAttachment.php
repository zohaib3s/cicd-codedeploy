<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommentAttachment extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'comment_attachments';
}
