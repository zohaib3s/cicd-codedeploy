<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\TranslationsManager;

class AccountFolderDocument extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'account_folder_documents';
    protected $fillable = ['account_folder_id'];
    public $timestamps = false;
    
    
    public static function get_row($conditions) {
        
       $result = AccountFolderDocument::where($conditions)->get()->toArray();
       if(!empty($result)){
            if(count($result) > 0) {
                return $result;
            } else {
                return FALSE;
            }
       }
        else {
            return FALSE;   
           }
    }
    
    public static function getCoachUserIDs($document_id, $site_id)
    {
        return AccountFolderDocument::join('account_folder_users as afu', 'afu.account_folder_id', '=', 'account_folder_documents.account_folder_id')
                             ->select('afu.user_id')
                             ->where('account_folder_documents.document_id', $document_id)
                             ->where('afu.role_id', 200)
                             ->where('afu.site_id', $site_id)
                             ->pluck('afu.user_id')
                             ->toArray();
    }

    public static function get_document_name($ref_id, $site_id) {
        $ref_id = (int) $ref_id;
        $result = AccountFolderDocument::where('document_id', $ref_id)
                    ->where('site_id', $site_id)
                    ->select(['title'])
                    ->get()->toArray();
        if (count($result) > 0) {
            return $result[0]['title'];
        } else {
            return false;
        }
    }

    public static function update_video_title($title, $document_id, $site_id,$user_id) {
        $title = htmlspecialchars($title);
        $record = AccountFolderDocument::where('id', $document_id)
        ->where('site_id', $site_id)
        ->select(['title','document_id'])
        ->get()->toArray();
        if(empty($record)){
            return false;
        }
        $updates_array = ['title' => $title];
        if($record && isset($record[0]) && $record[0]['title'] != $title)
        {
            $updates_array = ['title' => $title, "no_of_copies"=>1];
        }
        $result = AccountFolderDocument::where('id', $document_id)
                    ->where('site_id', $site_id)
                    ->update($updates_array);
        Document::where('id', $record[0]['document_id'])->update(["last_edit_by"=>$user_id, "last_edit_date"=>date('Y-m-d H:i:s')]);
        if ($result) {
            return ['success'=>true, 'title' => $title,'message'=>TranslationsManager::get_translation('discussion_title_changed_success', 'Api/huddle_list', $site_id, $user_id)];
        } else {
            return ['success'=>true, 'message'=>TranslationsManager::get_translation('discussion_title_changed_success', 'Api/huddle_list', $site_id, $user_id)];
        }
    }

    public static function get_document_folder_type($document_id){
        $result = AccountFolderDocument::join('account_folders as af', 'account_folder_documents.account_folder_id', '=', 'af.account_folder_id')
                                    ->select(['af.account_id','af.account_folder_id','af.folder_type'])
                                    ->where('account_folder_documents.document_id', $document_id)
                                    ->first();
        if($result){
            return $result->toArray();
        }
        return false;
    }

    public static function get_total_assessments($user_id,$account_folder_id, $site_id)
    {
        return AccountFolderDocument::join('account_folder_users as afu', 'afu.account_folder_id', '=', 'account_folder_documents.account_folder_id')
                             ->join('documents as d', 'd.id', '=', 'account_folder_documents.document_id')
                             ->select('afu.user_id')
                             ->where('account_folder_documents.account_folder_id',$account_folder_id )
                             ->where('d.created_by',$user_id)
                             ->where('afu.site_id', $site_id)                             
                             ->count();
                            
    }
}