<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoalItemEvidence extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    const evidenceTypeVideo = 1;
    const evidenceTypeResource = 2;
    const evidenceTypeHuddle = 3;
    const evidenceTypeFrameWork = 4;
    const evidenceTypeScriptedNote = 5;

    public $table = "goal_item_allowed_evidence";
    public $timestamps = false;
}