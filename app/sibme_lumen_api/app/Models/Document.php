<?php

namespace App\Models;

use App\Http\Controllers\HuddleController;
use App\Services\HelperFunctions;
use Illuminate\Database\Eloquent\Model;

class Document extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'documents';
    protected $fillable = ['account_id', 'doc_type', 'url', 'original_file_name', 'file_size', 'view_count', 'content_type', 'published', 'job_id', 'zencoder_output_id', 'active', 'created_date', 'created_by', 'last_edit_date', 'last_edit_by', 'zencoder_output', 'encoder_provider', 'encoder_status', 'thumbnail_number', 'request_dump', 'recorded_date', 'video_duration', 'is_associated', 'is_processed', 'current_duration', 'stack_url', 'upload_progress', 'upload_status', 'scripted_current_duration', 'not_observed', 'video_is_saved','allow_transcribe'];
    public $timestamps = false;
    public static $audioExt = ["aac", "aiff", "opus", "flac", "m4a", "ogg", "wav", "wma", "mp3"];

    public static function get_video_details($video_id, $site_id) {
        return Document::where('id', $video_id)->where('site_id', $site_id)->first();
    }

    public static function getVideoDetails($site_id, $document_id, $doc_type = 1) {
        $document_id = (int) $document_id;
        $result = Document::leftJoin('users as User', 'documents.created_by', '=', 'User.id')
                ->join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                ->select('documents.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'afd.account_folder_id')
                ->where('documents.id', $document_id)
                ->where('documents.site_id', $site_id)
                ->whereIn('doc_type', [1, 3])
                ->first();
        if ($result) {
            return $result->toArray();
        }
    }

    public static function getResourcesDetails($site_id, $document_id) {
        if(empty($document_id)) return false;
        $document_id = (int) $document_id;
        $result = Document::leftJoin('users as User', 'documents.created_by', '=', 'User.id')
                ->join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                ->select('documents.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'afd.account_folder_id')
                ->where('documents.id', $document_id)
                ->where('documents.site_id', $site_id)
                //->where('doc_type', $doc_type)
                ->first();
        if ($result) {
            return $result->toArray();
        } else {
            return false;
        }
    }

    public static function getAllowedResource($site_id, $document_id, $user_id){
        $document = Document::getResourcesDetails($site_id, $document_id);
        if(empty($document)){
            return ['success'=>false, 'message'=>'resource not found!'];
        }
        $huddle_id = $document['account_folder_id'];
        $folder_type = AccountFolder::where(array('account_folder_id' => $huddle_id, 'site_id' => $site_id))->value("folder_type");
        
        if($folder_type=='1') {
            // Check Huddle Participation
            $is_allowed = AccountFolderUser::user_present_huddle($user_id, $huddle_id);
        } elseif($folder_type=='2') {
            // Check Video Library Permissions
            $is_allowed = User::check_if_user_associated_with_account($site_id, $document['account_id'], $user_id);
            // Also check if user has permission for library. Missing for now because of limitation time.
        } elseif($folder_type=='3') {
            // For Workspace, check if resource is created by the given user
            $is_allowed = ($document['created_by'] == $user_id) ? true : false;
        } else {
            return ['success'=>false, 'message'=>'huddle type not found'];
        }
        if(!$is_allowed) {
            return ["success"=>false, "message"=>"Permission denied!"];
        }
        return ["success"=>true, "document" => $document];
    }

    public static function getVideoAttachmentTitles($comment_id, $site_id) {
        return Document::join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                        ->join('comment_attachments as ca', 'documents.id', '=', 'ca.document_id')
                        ->select('documents.*', 'afd.title', 'afd.desc', 'afd.account_folder_id')
                        ->where('ca.comment_id', $comment_id)
                        ->where('doc_type', '2')
                        ->where('documents.site_id', $site_id)
                        ->get()->toArray();
    }

    public static function getVideoAttachmentTotalCount($comment_id, $site_id) {
        return Document::join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                        ->join('comment_attachments as ca', 'documents.id', '=', 'ca.document_id')
                        ->where('ca.comment_id', $comment_id)
                        ->where('doc_type', '2')
                        ->where('documents.site_id', $site_id)
                        ->count();
    }

    public static function getVideoAttachmentNumbers($document_id, $site_id) {
        return Document::join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                        ->join('account_folderdocument_attachments as afda', 'afda.account_folder_document_id', '=', 'afd.id')
                        ->where('afda.attach_id', $document_id)
                        ->where('doc_type', '2')
                        ->where('documents.site_id', $site_id)
                        ->count();
    }

    public static function getVideoAttachmentCount($document_id, $time, $site_id) {
        return Document::join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                        ->join('account_folderdocument_attachments as afda', 'afda.account_folder_document_id', '=', 'afd.id')
                        ->where('afda.attach_id', $document_id)
                        ->where('doc_type', '2')
                        ->where('documents.scripted_current_duration', $time)
                        ->where('documents.site_id', $site_id)
                        ->count();
    }

    public static function getVideoAttachmentNames($document_id, $time, $site_id) {
        return Document::join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                        ->join('account_folderdocument_attachments as afda', 'afda.account_folder_document_id', '=', 'afd.id')
                        ->select('documents.*', 'afd.title', 'afd.desc', 'afd.account_folder_id')
                        ->where('afda.attach_id', $document_id)
                        ->where('doc_type', '2')
                        ->where('documents.scripted_current_duration', $time)
                        ->where('documents.site_id', $site_id)
                        ->get()->toArray();
    }

    public static function deleteVideoDocument($document_id, $video_id, $account_folder_id, $site_id, $user_id) {
        $account_folder_documents = AccountFolderDocument::where('account_folder_id', $account_folder_id)
                ->where('document_id', $document_id)
                ->where('site_id', $site_id)
                ->first();

        $flag = 0;
        $deleted = 0;
        if ($account_folder_documents) {
            $attachmentCount = AccountFolderdocumentAttachment::where('account_folder_document_id', $account_folder_documents->id)->where('site_id', $site_id)->count();

            if ($attachmentCount > 1) {
                $flag = AccountFolderdocumentAttachment::where('account_folder_document_id', $account_folder_documents->id)->where('attach_id', (int) $video_id)->where('site_id', $site_id)->delete();
                $deleted = 1;
            } else {
                AccountFolderdocumentAttachment::where('account_folder_document_id', $account_folder_documents->id)->where('site_id', $site_id)->delete();

                AccountFolderDocument::where('account_folder_id', $account_folder_id)
                        ->where('document_id', $document_id)
                        ->where('site_id', $site_id)
                        ->delete();

                $flag = Document::where('id', $document_id)->where('site_id', $site_id)->delete();
                $deleted = 1;
            }
        }
        $flag_outside = Document::where('id', $document_id)->where('site_id', $site_id)->delete();
        if($flag_outside)
        {
            $deleted = 1;
        }
        if($deleted)
        {
            $folder_type = \DB::select("select * from account_folders where account_folder_id = $account_folder_id");
            $is_workspace = 0;
            $is_library = 0;
            if(isset($folder_type[0]) && $folder_type[0]->folder_type == '3')
            {
                $is_workspace = 1;
            }
            else if(isset($folder_type[0]) && $folder_type[0]->folder_type == '2')
            {
                $is_library = 1;
            }

            $res = Document::get_single_video_data($site_id, $video_id, $folder_type[0]->folder_type, 1, $account_folder_id, $user_id);
            $event = [
                'channel' => ($is_workspace ? 'workspace-'.$folder_type[0]->account_id."-". $user_id:($is_library ? 'library-'.$folder_type[0]->account_id : 'huddle-details-' . $account_folder_id)),
                'event' => "resource_renamed",
                'data' => $res,
                'video_file_name' => $res["title"],
                'is_dummy' => 1
            ];
            HelperFunctions::broadcastEvent($event,"broadcast_event",false);//not sending push to prevent multiple refresh

            $event = [
                'channel' => 'video-details-'.$video_id,
                'event' => "attachment_deleted",
                'data' => $document_id,
                'item_id' => $document_id,
                'huddle_id' => $account_folder_id,
                'document_id' => $document_id,
                'reference_id' => $video_id,
                'is_attachment' => 1,
                'is_workspace' => $is_workspace,
                'is_library' => $is_library,
            ];
            HelperFunctions::broadcastEvent($event);
        }
        if($flag)
        {
           return $flag; 
        }
        elseif($flag_outside)
        {
            return $flag_outside;
        }
        else
        {
            return false;
        }
        
    }

    public static function getVideoDocumentsByVideo($document_id, $account_folder_id, $user_id, $h_type, $if_evaluator, $is_coach_enable, $site_id, $is_library = 0) {
        $document_id = (int) $document_id;
        $account_folder_id = (int) $account_folder_id;


//        $result = Document::join('account_folder_documents1 as afd', 'documents.id', '=', 'afd.document_id')
//                          ->join('account_folderdocument_attachments as afda', 'afda.account_folder_document_id', '=', 'afd.id')
//                          ->join('comment_attachments as ca' , 'ca.document_id' , '=' , 'documents.id' )
//                          ->join('comments as cmt' , 'cmt.id' , '=' , 'ca.comments_id' )
//                          ->select('documents.*', 'afd.title', 'afd.desc', 'afd.account_folder_id')
//                          ->where('afda.attach_id', $document_id)
//                          ->where('doc_type', '2')
//                          ->orderBy('documents.scripted_current_duration', 'ASC')
//                          ->get()->toArray();
        $query = "";
        if($is_library)
        {
            $query = " 
            
            UNION
             
               SELECT 
                  `d`.*,
                  `afd`.`title`,
                  `afd`.`desc`,
                  `afd`.`account_folder_id`,
                   NULL AS `comment_id`,
                   IF(dfd.is_processed = 1 OR dfd.is_processed IS NULL, 1, 0) AS ready_for_download 
               FROM documents d 
               JOIN users u 
                ON u.id = d.created_by 
               JOIN account_folder_documents afd 
                ON d.id = afd.document_id 
               LEFT OUTER JOIN documents_filestack_data as dfd 
                ON dfd.document_id = d.id 
               WHERE afd.account_folder_id = $account_folder_id 
                AND doc_type = 2 ";
        }
        if ((!$if_evaluator) && (($h_type == '2' && $is_coach_enable) || $h_type == '3')) {
            $result = app('db')->select("SELECT
                                  `documents`.*,
                                  `afd`.`title`,
                                  `afd`.`desc`,
                                  `afd`.`account_folder_id`,
                                  NULL AS `comment_id`,
                                  IF(dfd.is_processed = 1 OR dfd.is_processed IS NULL, 1, 0) AS ready_for_download 

                                FROM
                                  `documents`
                                  INNER JOIN `account_folder_documents` AS `afd`
                                    ON `documents`.`id` = `afd`.`document_id`
                                  INNER JOIN `account_folderdocument_attachments` AS `afda`
                                    ON `afda`.`account_folder_document_id` = `afd`.`id`
                                  LEFT OUTER JOIN documents_filestack_data as dfd 
                                    ON dfd.document_id = documents.id 
                                WHERE `afda`.`attach_id` = " . $document_id . "
                                  AND `doc_type` = 2
                                 AND scripted_current_duration IS NULL


                                UNION

                                SELECT
                                  `documents`.*,
                                  `afd`.`title`,
                                  `afd`.`desc`,
                                  `afd`.`account_folder_id`,
                                  `ca`.`comment_id`,
                                  IF(dfd.is_processed = 1 OR dfd.is_processed IS NULL, 1, 0) AS ready_for_download                
                                FROM
                                  `documents`
                                  INNER JOIN `account_folder_documents` AS `afd`
                                    ON `documents`.`id` = `afd`.`document_id`
                                  LEFT OUTER JOIN `account_folderdocument_attachments` AS `afda`
                                    ON `afda`.`account_folder_document_id` = `afd`.`id`
                                  LEFT OUTER JOIN `comment_attachments` AS `ca`
                                    ON `ca`.`document_id` = `documents`.`id`
                                  LEFT OUTER JOIN `comments` AS `cmt`
                                    ON `cmt`.`id` = `ca`.`comment_id`
                                  LEFT OUTER JOIN documents_filestack_data as dfd 
                                    ON dfd.document_id = documents.id 
                                WHERE `afda`.`attach_id` = " . $document_id . "
                                  AND `doc_type` = 2
                                  AND `documents`.`site_id` = " . $site_id . "
                                  AND (cmt.active IS NULL OR cmt.active = 1)
                                  AND scripted_current_duration IS NOT NULL " . $query );
        } else {
            $result = app('db')->select("SELECT
                                  `documents`.*,
                                  `afd`.`title`,
                                  `afd`.`desc`,
                                  `afd`.`account_folder_id`,
                                  NULL AS `comment_id`,
                                  IF(dfd.is_processed = 1 OR dfd.is_processed IS NULL, 1, 0) AS ready_for_download 
                                FROM
                                  `documents`
                                  INNER JOIN `account_folder_documents` AS `afd`
                                    ON `documents`.`id` = `afd`.`document_id`
                                  INNER JOIN `account_folderdocument_attachments` AS `afda`
                                    ON `afda`.`account_folder_document_id` = `afd`.`id`
                                  LEFT OUTER JOIN documents_filestack_data as dfd 
                                    ON dfd.document_id = documents.id 
                                WHERE `afda`.`attach_id` = " . $document_id . "
                                  AND `doc_type` = 2
                                 AND scripted_current_duration IS NULL


                                UNION

                                SELECT
                                  `documents`.*,
                                  `afd`.`title`,
                                  `afd`.`desc`,
                                  `afd`.`account_folder_id`,
                                  `ca`.`comment_id`,
                                  IF(dfd.is_processed = 1 OR dfd.is_processed IS NULL, 1, 0) AS ready_for_download                
                                FROM
                                  `documents`
                                  INNER JOIN `account_folder_documents` AS `afd`
                                    ON `documents`.`id` = `afd`.`document_id`
                                  LEFT OUTER JOIN `account_folderdocument_attachments` AS `afda`
                                    ON `afda`.`account_folder_document_id` = `afd`.`id`
                                  LEFT OUTER JOIN `comment_attachments` AS `ca`
                                    ON `ca`.`document_id` = `documents`.`id`
                                  LEFT OUTER JOIN `comments` AS `cmt`
                                    ON `cmt`.`id` = `ca`.`comment_id`
                                  LEFT OUTER JOIN documents_filestack_data as dfd 
                                    ON dfd.document_id = documents.id 
                                WHERE `afda`.`attach_id` = " . $document_id . "
                                  AND `doc_type` = 2
                                  AND `documents`.`site_id` = " . $site_id . "
                                  AND scripted_current_duration IS NOT NULL " . $query );
        }

        return !empty($result) ? $result : array();
    }

    public static function getCoacheeDocuments($site_id, $account_folder_id, $title = '', $sort = '', $doc_type = '') {
        $account_folder_id = (int) $account_folder_id;
        $title_condition = "";

        if (empty($sort)) {
            $orderBy = "order by afd.title ASC";
        } else {
            $orderBy = "order by " . $sort;
        }


        if (!empty($title)) {
            $title_condition = "AND afd.title like '%$title%'";
        }

        $result = app('db')->select("(SELECT
                                  `Document`.`id`
                                FROM
                                  `documents` AS `Document`
                                  INNER JOIN `users` AS `User`
                                    ON (
                                      `Document`.`created_by` = `User`.`id`
                                    )
                                  INNER JOIN `account_folder_documents` AS `afd`
                                    ON (
                                      `Document`.`id` = `afd`.`document_id`
                                    )
                                  INNER JOIN `account_folderdocument_attachments` AS `afda`
                                    ON (
                                      `afda`.`account_folder_document_id` = `afd`.`id`
                                    )
                                  INNER JOIN `comment_attachments` AS `ca`
                                    ON Document.`id` = ca.`document_id`
                                  INNER JOIN comments AS cmt
                                    ON cmt.`id` = ca.`comment_id`
                                WHERE `afd`.`account_folder_id` = " . $account_folder_id . "
                                  AND `doc_type` = 2
                                  AND cmt.`active` = '1'
                                  AND Document.site_id = " . $site_id . "
                              " . $title_condition . "
                                " . $orderBy . " )

                                UNION

                                (
                                SELECT
                                  `Document`.`id`
                                FROM
                                  `documents` AS `Document`
                                  INNER JOIN `users` AS `User`
                                    ON (
                                      `Document`.`created_by` = `User`.`id`
                                    )
                                  INNER JOIN `account_folder_documents` AS `afd`
                                    ON (
                                      `Document`.`id` = `afd`.`document_id`
                                    )
                                  LEFT JOIN `account_folderdocument_attachments` AS `afda`
                                    ON (
                                      `afda`.`account_folder_document_id` = `afd`.`id`
                                    )
                                  LEFT JOIN `comment_attachments` AS `ca`
                                    ON Document.`id` = ca.`document_id`
                                  LEFT JOIN comments AS cmt
                                    ON cmt.`id` = ca.`comment_id`
                                WHERE `afd`.`account_folder_id` = " . $account_folder_id . "
                                  AND `doc_type` = 2
                                  AND  cmt.`active` IS NULL
                                  AND Document.site_id = " . $site_id . "
                                 " . $title_condition . "
                                " . $orderBy . "
                                )");

        foreach ($result as &$item) {
            $item = $item->id;
        }
        return !empty($result) ? $result : array();
    }

    public static function get_eval_participant_videos($huddle_id, $user_id, $site_id, $doc_type = 1) {

        $videos = Document::getVideos($site_id, $huddle_id, '', '', '', '', $user_id, $doc_type);
        if (count($videos) >= 1) {
            return count($videos);
        } else {
            return 0;
        }
    }

    public static function get_submission_allowed($huddle_id, $site_id, $doc_type = 1) {
        $submission_allowed_count = 1;
        $column = "submission_allowed";
        if($doc_type == 2 || $doc_type == 5)
        {
            $column = "resource_submission_allowed";
        }
        $submission_allowed = AccountFolderMetaData::where('account_folder_id', $huddle_id)->where('site_id', $site_id)->where('meta_data_name', $column)->first();

        if ($submission_allowed) {
            $submission_allowed_count = $submission_allowed->meta_data_value;
        }

        return $submission_allowed_count;
    }

    public static function getVideos($site_id, $account_folder_id, $title = '', $sort = '', $limit = 0, $page = 0, $user_id = 0, $doc_type = 1) {
        $account_folder_id = (int) $account_folder_id;
        $feedbac_sort = '';
        if (empty($sort)) {
            $orderBy = "documents.created_date DESC";
        } elseif ($sort == 'published_feedback') {
            $orderBy = 'cmt.active DESC';
        } elseif ($sort == 'unpublished_feedback') {
            $orderBy = 'cmt.active ASC';
        } else {
            $orderBy = $sort;
        }

        $conditions = array();
        $keyword = '';
        $conditions['afd.account_folder_id'] = $account_folder_id;
        $conditions['documents.doc_type'] = $doc_type;
        $conditions['documents.active'] = '1';
        $conditions['documents.site_id'] = $site_id;


        if ($user_id) {
            $conditions['documents.created_by'] = $user_id;
        }

        if (!empty($title)) {
            $keyword = " (afd.title like '%" . $title . "%' OR (User.first_name like '%" . $title . "%' OR User.last_name like '%" . $title . "%') OR documents.id IN (select comment.ref_id from comments as comment WHERE comment.comment like '%" . $title . "%') OR documents.id IN (select act.ref_id from account_comment_tags as act WHERE act.tag_title like '%" . $title . "%') ) ";
        }


        if (!empty($keyword)) {

            $query = Document::join('users as User', 'documents.created_by', '=', 'User.id')
                    ->join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                    ->leftJoin('document_files as df', 'documents.id', '=', 'df.document_id')
                    ->leftJoin('comments as cmt', 'cmt.ref_id', '=', 'documents.id')
                    ->select('documents.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'afd.id', 'afd.account_folder_id', \DB::raw("IF(df.duration = 0 OR df.duration = NULL OR df.duration = '', documents.video_duration, df.duration) AS video_duration"),'documents.id as doc_id')
                    ->selectRaw('(SELECT "autocreated" FROM account_folders_meta_data WHERE account_folder_id = afd.account_folder_id AND meta_data_name = "autocreated" AND meta_data_value = 1) AS AutoCreated')
                    ->selectRaw('(SELECT meta_data_value FROM account_folders_meta_data WHERE account_folder_id = afd.document_id AND meta_data_name = "web_uploader_name") AS WebUploaderName')
                    ->selectRaw('(SELECT meta_data_value FROM account_folders_meta_data WHERE account_folder_id = afd.document_id AND meta_data_name = "web_uploader_email") AS WebUploaderEmail')
                    ->where($conditions)
                    ->whereRaw($keyword)
                    ->orderByRaw($orderBy)
                    ->groupBy('documents.id');
        } else {
            $query = Document::join('users as User', 'documents.created_by', '=', 'User.id')
                    ->join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                    ->leftJoin('document_files as df', 'documents.id', '=', 'df.document_id')
                    ->leftJoin('comments as cmt', 'cmt.ref_id', '=', 'documents.id')
                    ->select('documents.*', 'User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'afd.id', 'afd.account_folder_id', \DB::raw("IF(df.duration = 0 OR df.duration = NULL OR df.duration = '', documents.video_duration, df.duration) AS video_duration"),'documents.id as doc_id')
                    ->selectRaw('(SELECT "autocreated" FROM account_folders_meta_data WHERE account_folder_id = afd.account_folder_id AND meta_data_name = "autocreated" AND meta_data_value = 1) AS AutoCreated')
                    ->selectRaw('(SELECT meta_data_value FROM account_folders_meta_data WHERE account_folder_id = afd.document_id AND meta_data_name = "web_uploader_name") AS WebUploaderName')
                    ->selectRaw('(SELECT meta_data_value FROM account_folders_meta_data WHERE account_folder_id = afd.document_id AND meta_data_name = "web_uploader_email") AS WebUploaderEmail')
                    ->where($conditions)
                    ->orderByRaw($orderBy)
                    ->groupBy('documents.id');
        }

        if (0 < (int) $limit) {
            $offset = (int) $page * (int) $limit;
            $query = $query->limit($limit)->offset($offset);
        }

        return $query->get()->toArray();
    }

    public static function getArtifects($site_id, $account_folder_id, $title = '', $sort = '', $limit = 0, $page = 0, $user_id = 0, $doc_type = array(), $document_ids = array(), $htype, $request_type = false,$document_id ='', $parent_folder_id = 0, $apply_parent_check = 1, $request_doc_type = 0, $artifact_moved_count = 0, $apply_inner_search = 1) {
        if(!is_array($account_folder_id))
        {
            $account_folder_id = (int) $account_folder_id;
        }

        $feedbac_sort = '';
        if (empty($sort) || $sort == "uploaded_date") {
            $orderBy = "documents.created_date DESC";
        } elseif ($sort == 'published_feedback') {
            $orderBy = 'cmt.active DESC';
        } elseif ($sort == 'unpublished_feedback') {
            $orderBy = 'cmt.active ASC';
        } elseif ($sort == 'video_title') {
            //$orderBy = 'documents.original_file_name ASC';
            $orderBy = 'afd.title ASC';
        } elseif ($sort == 'uploaded_by') {
            $orderBy = 'User.first_name ASC';
        } elseif ($sort == 'last_modified') {
            $orderBy = 'documents.last_edit_date DESC';
        } else {
            $orderBy = $sort;
        }

        $conditions = array();
        $keyword = '';
        if(!is_array($account_folder_id) && $account_folder_id)
        {
            $conditions['afd.account_folder_id'] = $account_folder_id;
        }
        //$conditions['documents.doc_type'] = $doc_type;
        $conditions['documents.active'] = '1';
        $conditions['documents.site_id'] = $site_id;


//        if ($user_id) {
//            $conditions['documents.created_by'] = $user_id;
//        }

        if (!empty($title)) {
            $title =   Document::mysql_escape($title);
            $keyword = " (afd.title like '%" . $title . "%' OR (User.first_name like '%" . $title . "%' OR User.last_name like '%" . $title . "%') OR documents.id IN (select comment.ref_id from comments as comment WHERE comment.comment like '%" . $title . "%') OR documents.id IN (select act.ref_id from account_comment_tags as act WHERE act.tag_title like '%" . $title . "%') ) ";
        }


        if (!empty($keyword)) {

            $query = Document::join('users as User', 'documents.created_by', '=', 'User.id')
                    ->join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                    ->leftJoin('document_files as df', 'documents.id', '=', 'df.document_id')
                    ->leftJoin('comments as cmt', function ($join){
                        $join->on('cmt.ref_id', '=', 'documents.id');
                        $join->on('cmt.ref_type', '!=', \DB::raw('5'));
                    })
                    ->leftJoin('documents_filestack_data as dfd', 'dfd.document_id', '=', 'documents.id')
                    ->select('documents.*','df.transcoding_status','User.first_name', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'documents.id AS doc_id', 'afd.id', 'afd.account_folder_id', 'afd.assessment_sample', 'afd.slot_index', \DB::raw("IF(df.duration = 0 OR df.duration = NULL OR df.duration = '' OR count(df.duration) > 1 , ROUND(documents.video_duration), df.duration) AS video_duration"), \DB::raw("IF(dfd.is_processed = 1 OR dfd.is_processed IS NULL, 1, 0) AS ready_for_download"))
                    ->selectRaw('(SELECT "autocreated" FROM account_folders_meta_data WHERE account_folder_id = afd.account_folder_id AND meta_data_name = "autocreated" AND meta_data_value = 1) AS AutoCreated')
                    ->selectRaw('(SELECT meta_data_value FROM account_folders_meta_data WHERE account_folder_id = afd.document_id AND meta_data_name = "web_uploader_name") AS WebUploaderName')
                    ->selectRaw('(SELECT meta_data_value FROM account_folders_meta_data WHERE account_folder_id = afd.document_id AND meta_data_name = "web_uploader_email") AS WebUploaderEmail')
                    ->whereRaw('documents.id NOT IN (select afd.document_id  from account_folderdocument_attachments afda join account_folder_documents afd ON afda.account_folder_document_id = afd.id)')
                    ->where($conditions)
                    ->where('documents.is_scripted_note_start', '1')
                    ->whereRaw($keyword)
                    ->whereIn('documents.doc_type', $doc_type);
            if(is_array($account_folder_id))
            {
                $query->whereIn('afd.account_folder_id', $account_folder_id);
            }
            if ($user_id && is_array($user_id)) {
                $query->whereIn('documents.created_by', $user_id);
            } elseif ($user_id) {
                $query->where('documents.created_by', '=', $user_id);
            }

            if ($htype == 2 && !empty($document_ids)) {
                 //$query->whereIn('documents.id', $document_ids);   //ToDo : Some Discussion Required
            }

            if(!empty($document_id)){
                $query->where('documents.id', $document_id);
            }
            $query->orderByRaw($orderBy)
                    ->groupBy('documents.id');
        } else {
            $query = Document::join('users as User', 'documents.created_by', '=', 'User.id')
                    ->join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                    ->leftJoin('document_files as df', 'documents.id', '=', 'df.document_id')
                    ->leftJoin('comments as cmt', 'cmt.ref_id', '=', 'documents.id')
                    ->leftJoin('documents_filestack_data as dfd', 'dfd.document_id', '=', 'documents.id')
                    ->select('documents.*', 'User.first_name','df.transcoding_status', 'User.last_name', 'User.email', 'afd.title', 'afd.desc', 'afd.id', 'documents.id AS doc_id', 'afd.account_folder_id', 'afd.assessment_sample', 'afd.slot_index', \DB::raw("IF(df.duration = 0 OR df.duration = NULL OR df.duration = '' OR count(df.duration) > 1, ROUND(documents.video_duration), df.duration) AS video_duration"), \DB::raw("IF(dfd.is_processed = 1 OR dfd.is_processed IS NULL, 1, 0) AS ready_for_download"))
                    ->selectRaw('(SELECT "autocreated" FROM account_folders_meta_data WHERE account_folder_id = afd.account_folder_id AND meta_data_name = "autocreated" AND meta_data_value = 1) AS AutoCreated')
                    ->selectRaw('(SELECT meta_data_value FROM account_folders_meta_data WHERE account_folder_id = afd.document_id AND meta_data_name = "web_uploader_name") AS WebUploaderName')
                    ->selectRaw('(SELECT meta_data_value FROM account_folders_meta_data WHERE account_folder_id = afd.document_id AND meta_data_name = "web_uploader_email") AS WebUploaderEmail')
                    ->whereRaw('documents.id NOT IN (select afd.document_id  from account_folderdocument_attachments afda join account_folder_documents afd ON afda.account_folder_document_id = afd.id)')
                    ->where($conditions)
                    ->where('documents.is_scripted_note_start', '1')
                    ->whereIn('documents.doc_type', $doc_type);
            if(is_array($account_folder_id))
            {
                $query->whereIn('afd.account_folder_id', $account_folder_id);
            }
            if ($user_id && is_array($user_id)) {
                $query->whereIn('documents.created_by', $user_id);
            } elseif ($user_id) {
                $query->where('documents.created_by', '=', $user_id);
            }
            if ($htype == 2 && !empty($document_ids)) {
                //$query->whereIn('documents.id', $document_ids);  //ToDo : Discussion Required
            }
            if(!empty($document_id)){
                $query->where('documents.id', $document_id);
            }
            $query->orderByRaw($orderBy)
                    ->groupBy('documents.id');
        }
        if($apply_parent_check)
        {
            if($parent_folder_id)
            {
                if(!empty($request_doc_type) && $apply_inner_search)
                {
                    $parent_folder_ids = DocumentFolder::getNLevelChilds($parent_folder_id, 1, 1);
                    $query->whereIn('documents.parent_folder_id',$parent_folder_ids);
                }
                else
                {
                    $query->where('documents.parent_folder_id',$parent_folder_id);
                }
            }
            else if(empty($request_doc_type) || $request_doc_type == 0 || $apply_inner_search == 0)
            {
                $query->where(function ($q){
                    $q->where('documents.parent_folder_id', 0)->orWhereNull('documents.parent_folder_id');
                });
            }
        }

        $total_records = 0;
        if ($page == 1) {
            $total_records = count($query->get());
        }
        if ($limit > 0 && $request_type == false) {
            $offset = ($page == 1) ? 0 : ( ((int) $page - 1) * (int) $limit);
            if($offset && $artifact_moved_count)
            {
                $offset = ($offset - $artifact_moved_count);
                if($offset < 0)
                {
                    $offset = 0;
                }
            }
            $query = $query->limit($limit)->offset($offset);
            return ["data" => $query->get()->toArray(), "total_records" => $total_records];
        } else {
            return ["data" => $query->get()->toArray(), "total_records" => $total_records];
        }
    }

    public static function get_document_row($document_id, $site_id) {


        $result = Document::where(array('id' => (int) $document_id, 'site_id' => $site_id))->first();

        if ($result) {
            return $result->toArray();
        } else {
            return false;
        }
    }

    public static function countVideosEvaluator($site_id, $account_folder_id, $user_id = 0, $doc_type = 1, $participants_ids = false) {
        $account_folder_id = (int) $account_folder_id;

        $query = Document::join('users as User', 'documents.created_by', '=', 'User.id')
                ->join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                ->leftJoin('account_folderdocument_attachments as afda', 'afda.account_folder_document_id', '=', 'afd.id')
                ->where('afd.account_folder_id', $account_folder_id)
                ->where('afd.assessment_sample', "0")
                ->where('documents.doc_type', $doc_type)
                ->where('documents.site_id', $site_id)
                ->where('documents.active', 1);

        if (!empty($participants_ids)) {
            array_push($participants_ids, $user_id);
            $query->whereIn('documents.created_by', $participants_ids);
        } else {
            $query->where('documents.created_by', $user_id);
        }




        return $query->count();
    }

    public static function countAsseseeDocuments($afid, $site_id) {
        $result = app('db')->select("(SELECT
                Document.id
            FROM
                `documents` AS `Document`
                INNER JOIN `users` AS `User`
                ON (
                    `Document`.`created_by` = `User`.`id`
                )
                INNER JOIN `account_folder_documents` AS `afd`
                ON (
                    `Document`.`id` = `afd`.`document_id`
                )
                INNER JOIN `account_folderdocument_attachments` AS `afda`
                ON (
                    `afda`.`account_folder_document_id` = `afd`.`id`
                )
                INNER JOIN `comment_attachments` AS `ca`
                ON Document.`id` = ca.`document_id`
                INNER JOIN comments AS cmt
                ON cmt.`id` = ca.`comment_id`
            WHERE `afd`.`account_folder_id` = $afid
                AND `afd`.`assessment_sample`='0' 
                AND `afd`.`site_id` = $site_id
                AND `Document`.`doc_type` = '2'
                AND cmt.`active` = '1' )

                UNION

            (  SELECT
                Document.id
            FROM
                `documents` AS `Document`
                INNER JOIN `users` AS `User`
                ON (
                    `Document`.`created_by` = `User`.`id`
                )
                INNER JOIN `account_folder_documents` AS `afd`
                ON (
                    `Document`.`id` = `afd`.`document_id`
                )
                LEFT JOIN `account_folderdocument_attachments` AS `afda`
                ON (
                    `afda`.`account_folder_document_id` = `afd`.`id`
                )
                LEFT JOIN `comment_attachments` AS `ca`
                ON Document.`id` = ca.`document_id`
                LEFT JOIN comments AS cmt
                ON cmt.`id` = ca.`comment_id`
            WHERE `afd`.`account_folder_id` = $afid
                AND `afd`.`assessment_sample`='0' 
                AND `afd`.`site_id` = $site_id
                AND `Document`.`doc_type` = '2'
                AND (cmt.`active` IS NULL))
            ");
        return count($result);
    }

    public static function getDocuments($site_id, $account_folder_id, $title = '', $sort = '', $doc_type = '') {
        if (empty($sort)) {
            $orderBy = "afd.title ASC";
        } else {
            $orderBy = $sort;
        }

        $conditions = array();
        $account_folder_id;
        if ($doc_type != '') {
            $doc_type = $doc_type;
        } else {
            $doc_type = '2';
        }
        if (!empty($title)) {
            $title = " AND afd.title like '%$title%'";
        }

        $result = app('db')->select("SELECT
                      Document.*,
                      User.first_name,
                      User.last_name,
                      User.email,
                      afd.id,
                      afd.title,
                      afd.desc
                    FROM
                      `documents` AS Document
                      INNER JOIN users AS `User`
                        ON Document.created_by = User.id
                      INNER JOIN account_folder_documents AS afd
                        ON Document.id = afd.document_id
                    WHERE afd.account_folder_id = $account_folder_id
                      AND afd.site_id = $site_id
                      AND doc_type = $doc_type
                      $title");

        return $result;
    }
    public static function getHuddleArtifactsCount($account_folder_id, $site_id,$doc_type =1) {  
    $result = app('db')->select("SELECT 
        COUNT(*) AS `count` 
      FROM
        `documents` AS `Document` 
        INNER JOIN `users` AS `User` 
          ON (
            `Document`.`created_by` = `User`.`id`
          ) 
        INNER JOIN `account_folder_documents` AS `afd` 
          ON (
            `Document`.`id` = `afd`.`document_id`
          ) 
        INNER JOIN `documents` AS `doc` 
          ON (`afd`.`document_id` = `doc`.`id`) 
      WHERE `afd`.`account_folder_id` = $account_folder_id 
        AND `afd`.`assessment_sample` = '0' 
        AND `Document`.`doc_type` = $doc_type
        AND `Document`.`site_id` = $site_id 
        AND `Document`.`active` = '1' ");
        return $result[0]->count;         
    }


    public static function getDocumentsCount($document_id, $site_id, $is_published = 0) {

        if ($is_published == 1) {
            return Document::get_assessor_count($document_id, $site_id);
        }

        $result = app('db')->select("SELECT
              `Document`.*,
              `afd`.`title`,
              `afd`.`desc`,
              `afd`.`account_folder_id`
            FROM
              `documents` AS `Document`
              INNER JOIN `account_folder_documents` AS `afd`
                ON (
                  `Document`.`id` = `afd`.`document_id`
                )
              INNER JOIN `account_folderdocument_attachments` AS `afda`
                ON (
                  `afda`.`account_folder_document_id` = `afd`.`id`
                  AND `afda`.`attach_id` = $document_id
                )
            WHERE `Document`.`site_id` = $site_id
              AND `doc_type` = 2");

        return count($result); 
        
    }

    public static function getWorkSpaceVideos($site_id, $account_id, $user_id, $title = '', $sort = '', $limit = 10, $page = 0, $doc_type = array(), $document_id = null,$workspaceORhuddle = 3, $parent_folder_id = 0, $apply_parent_check = 1, $request_doc_type = 0, $artifact_moved_count = 0, $apply_inner_search = 1) {

        if (empty($sort) || $sort == "uploaded_date") {
            $orderBy = "documents.created_date DESC";
        } elseif ($sort == 'video_title') {
            $orderBy = 'afd.title ASC';
        } elseif ($sort == 'last_modified' || $sort == 'last_edit_date') {
            $orderBy = 'documents.last_edit_date DESC';
        } elseif ($sort == 'uploaded_by') {
            $orderBy = 'User.first_name ASC';
        } else {
            $orderBy = $sort;
        }
        $keyword = '';
        if (!empty($title)) {
            $title =   Document::mysql_escape($title);
            $keyword = " (afd.title like '%" . $title . "%' OR documents.id IN (select comment.ref_id from comments as comment WHERE comment.comment like '%" . $title . "%') OR documents.id IN (select act.ref_id from account_comment_tags as act WHERE act.tag_title like '%" . $title . "%') ) ";
        }

        $query = Document::join('users as User', 'documents.created_by', '=', 'User.id')
                ->join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                ->join('account_folders as AccountFolder', 'afd.account_folder_id', '=', 'AccountFolder.account_folder_id')
                ->leftJoin('document_files as df', 'documents.id', '=', 'df.document_id')
                ->leftJoin('user_activity_logs as ual', function($join) {
                    $join->on('ual.user_id', '=', 'documents.created_by');
                    $join->on('ual.ref_id', '=', 'documents.id');
                    $join->on('ual.type', '=', \DB::raw("'20'"));
                    $join->on('ual.account_id', '=', 'documents.account_id');
                    $join->on('ual.date_added', '<=', 'documents.created_date');
                })
                ->leftJoin('documents_filestack_data as dfd', 'dfd.document_id', '=', 'documents.id')
                ->select('documents.*','df.transcoding_status','documents.id as doc_id', 'User.first_name', 'User.last_name', 'User.email', 'afd.*', 'AccountFolder.*', 'ual.environment_type', 'documents.created_date as created_date', \DB::raw("IF(df.duration = 0 OR df.duration IS NULL OR df.duration = '' OR count(df.duration) > 1, ROUND(documents.video_duration), df.duration) AS video_duration"), \DB::raw("IF(dfd.is_processed = 1 OR dfd.is_processed IS NULL, 1, 0) AS ready_for_download"), 'documents.parent_folder_id')
                ->whereRaw('documents.id NOT IN (select afd.document_id  from account_folderdocument_attachments afda join account_folder_documents afd ON afda.account_folder_document_id = afd.id)')
                ->where('documents.site_id', $site_id)
                ->where('documents.account_id', $account_id)
                ->where('documents.active', 1)
                ->where('AccountFolder.account_id', $account_id)
                ->where('AccountFolder.folder_type', $workspaceORhuddle)
                ->where('AccountFolder.active', 1)
                ->where('documents.is_scripted_note_start', '1');
        $goalEvidence = app("Illuminate\Http\Request")->get('goalEvidence');
        if(!$goalEvidence)
        {
            $query->where('AccountFolder.created_by', $user_id);
        }
        if (count($doc_type) == 1) {
            if ($doc_type[0] == "1") {
                // Include Synced Notes with Video List.
                $query->where(function($mq) {
                    $mq->where('documents.doc_type', "1");
                    $mq->orWhere(function($q1) {
                        $q1->where("documents.doc_type", "3");
                        $q1->where(function($q2) {
                            $q2->where("documents.is_processed", "4")
                                    ->orWhere("documents.is_processed", "5");
                        });
                    });
                });
            } elseif ($doc_type[0] == "3") {
                $query->where('documents.doc_type', "3");
                $query->where("documents.is_processed", "<", "4");
            } elseif ($doc_type[0] == "2") {
                $query->where('documents.doc_type', "2");
            } elseif ($doc_type[0] == "5") {
                $query->where('documents.doc_type', "5");
            }
        } else {
            $query->whereIn('documents.doc_type', $doc_type);
        }
        if(!empty($document_id)){
            $query->where('documents.id', $document_id);
        }
        if ($keyword != '') {
            $query->whereRaw($keyword);
        }
        if($apply_parent_check)
        {
            if($parent_folder_id)
            {
                if(!empty($request_doc_type) && $apply_inner_search)
                {
                    $parent_folder_ids = DocumentFolder::getNLevelChilds($parent_folder_id, 1, 1);
                    $query->whereIn('documents.parent_folder_id',$parent_folder_ids);
                }
                else
                {
                    $query->where('documents.parent_folder_id',$parent_folder_id);
                }
            }
            else if (empty($request_doc_type) || $request_doc_type == 0 || $apply_inner_search == 0)
            {
                $query->where(function ($q){
                    $q->where('documents.parent_folder_id', 0)->orWhereNull('documents.parent_folder_id');
                });
            }
        }

        $query->orderByRaw($orderBy)->groupBy('documents.id');

        $total_records = 0;
        if ($page == 1) {
            $total_records = count($query->get());//don't use $query->count() it is buggy
        }

        $offset = ($page == 1) ? 0 : ((int) ($page - 1) * (int) $limit);
        if($offset && $artifact_moved_count)
        {
            $offset = ($offset - $artifact_moved_count);
            if($offset < 0)
            {
                $offset = 0;
            }
        }
        if (!empty($limit)) {
            $query = $query->limit($limit)->offset($offset);
        }

        return ["data" => $query->get()->toArray(), "total_records" => $total_records];
    }

    public static function get_single_video_data($site_id, $video_id, $folder_type = 3, $withData = 0, $huddle_id = 0, $user_id = 0) {

        $data = Document::join('users as User', 'documents.created_by', '=', 'User.id')
                        ->join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                        ->join('account_folders as AccountFolder', 'afd.account_folder_id', '=', 'AccountFolder.account_folder_id')
                        ->leftJoin('document_files as df', 'documents.id', '=', 'df.document_id')
                        ->leftJoin('user_activity_logs as ual', function($join) {
                            $join->on('ual.user_id', '=', 'documents.created_by');
                            $join->on('ual.ref_id', '=', 'documents.id');
                            $join->on('ual.type', '=', \DB::raw("'20'"));
                            $join->on('ual.account_id', '=', 'documents.account_id');
                            $join->on('ual.date_added', '<=', 'documents.created_date');
                        })
                        ->select('documents.*', 'df.transcoding_status' ,'documents.id as doc_id', 'User.first_name', 'User.last_name', 'User.email', 'afd.*', 'AccountFolder.*', 'ual.environment_type', 'documents.created_date as created_date', 'documents.last_edit_date as last_edit_date', 'documents.created_by as created_by', \DB::raw("IF(df.duration = 0 OR df.duration = NULL OR df.duration = '', documents.video_duration, df.duration) AS video_duration"), 'afd.id as id', 'documents.parent_folder_id')
                        ->where('documents.site_id', $site_id)
                        ->where('documents.id', $video_id)
                        ->whereIn('documents.doc_type', [1, 2, 3, 4, 5,6])
                        ->where('documents.active', 1)
                        ->where('AccountFolder.folder_type', $folder_type)
                        ->where('AccountFolder.active', 1)
                        ->get()->toArray();
        if (count($data) && $withData) {
            if(empty($data[0]['video_duration']))
            {
                $result = HuddleController::getVideoDuration($video_id);
                $duration = $result["status"] ? round($result["data"]) : 0;
                $data[0]['duration'] = $duration;
                $data[0]['video_duration'] = $duration;
            }
            else
            {
                $data[0]['duration'] = $data[0]['video_duration'];
            }
            
            $data[0]['total_comments'] = HelperFunctions::get_video_comment_numbers($video_id, $huddle_id, $user_id);
            $if_evaluator = HelperFunctions::check_if_evalutor($huddle_id, $user_id);
            $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $site_id);
            $is_coach_enable = HelperFunctions::is_enabled_coach_feedback($huddle_id);
            $data[0]['total_attachment'] = count(Document::getVideoDocumentsByVideo($video_id, $huddle_id, $user_id, $h_type, $if_evaluator, $is_coach_enable, $site_id));
            $from_workspace = 0;
            if ($folder_type == 3) {
                $from_workspace = 1;
            }
            $data = app('App\Http\Controllers\WorkSpaceController')->conversion_to_thmubs($data[0], 1, $from_workspace);
        }
        if($data)
        {
            $data[0]['updated_by'] = $user_id;
        }
        return $data;
    }

    public static function get_assessor_count($document_id, $site_id) {

        $results = app('db')->select("(SELECT
                    `Document`.*,
                    `afd`.`title`,
                    `afd`.`desc`,
                    `afd`.`account_folder_id`
                FROM
                    `documents` AS `Document`
                    INNER JOIN `account_folder_documents` AS `afd`
                    ON (
                        `Document`.`id` = `afd`.`document_id`
                    )
                    INNER JOIN `account_folderdocument_attachments` AS `afda`
                    ON (
                        `afda`.`account_folder_document_id` = `afd`.`id`
                        AND `afda`.`attach_id` = $document_id
                    )
                    LEFT JOIN `comment_attachments` AS `ca`
                    ON Document.`id` = ca.`document_id`
                    LEFT JOIN comments AS cmt
                    ON cmt.`id` = ca.`comment_id`
                WHERE `doc_type` = 2
                    AND Document.site_id= " . $site_id . "
                    AND cmt.active = 1)
                UNION
                (SELECT
                    `Document`.*,
                    `afd`.`title`,
                    `afd`.`desc`,
                    `afd`.`account_folder_id`
                FROM
                    `documents` AS `Document`
                    INNER JOIN `account_folder_documents` AS `afd`
                    ON (
                        `Document`.`id` = `afd`.`document_id`
                    )
                    INNER JOIN `account_folderdocument_attachments` AS `afda`
                    ON (
                        `afda`.`account_folder_document_id` = `afd`.`id`
                        AND `afda`.`attach_id` = $document_id
                    )
                    LEFT JOIN `comment_attachments` AS `ca`
                    ON Document.`id` = ca.`document_id`
                    LEFT JOIN comments AS cmt
                    ON cmt.`id` = ca.`comment_id`
                WHERE `doc_type` = 2
                    AND Document.site_id= " . $site_id . "
                    AND (cmt.active IS NULL))");
        return count($results);
    }

    public static function getSampleData($huddle_id, $site_id, $user_id) {
        $data = Document::join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                ->select('documents.*')
                ->where("account_folder_id", $huddle_id)
                ->where("assessment_sample", "1")
                ->where('documents.site_id', $site_id)
                ->get();

        $sample_data = [];
        if ($data) {
            foreach ($data as $row) {
                $sample_data [] = self::get_single_video_data($site_id, $row->id, 1, 1, $huddle_id, $user_id);
            }
        }
        return $sample_data;
    }


    public static function getSampleData_opt($site_id,  $folder_type = 3, $withData = 0, $huddle_id = 0, $user_id = 0){        
        $sample_data = [];
        $data = Document::join('users as User', 'documents.created_by', '=', 'User.id')
                        ->join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                        ->join('account_folders as AccountFolder', 'afd.account_folder_id', '=', 'AccountFolder.account_folder_id')
                        ->leftJoin('document_files as df', 'documents.id', '=', 'df.document_id')
                        ->select('documents.*', 'df.transcoding_status' ,'documents.id as doc_id', 'User.first_name', 'User.last_name', 'User.email', 'afd.*', 'AccountFolder.*', 'documents.created_date as created_date', 'documents.last_edit_date as last_edit_date', 'documents.created_by as created_by', \DB::raw("IF(df.duration = 0 OR df.duration = NULL OR df.duration = '', documents.video_duration, df.duration) AS video_duration"), 'afd.id as id')
                        ->where('documents.site_id', $site_id)
                        ->where('afd.account_folder_id',$huddle_id)
                        ->where("afd.assessment_sample", "1")
                        ->whereIn('documents.doc_type', [1, 2, 3, 4, 5])
                        ->where('documents.active', 1)
                        ->where('AccountFolder.folder_type', $folder_type)
                        ->where('AccountFolder.active', 1)
                        ->get()->toArray();

        if (count($data) && $withData) {
            foreach($data as $key=>$row){
                $video_id = $row['doc_id'];
                if(empty($row['video_duration']))
                {
                    $result = HuddleController::getVideoDuration($video_id);
                    $duration = $result["status"] ? round($result["data"]) : 0;
                    $row['duration'] = $duration;
                    $row['video_duration'] = $duration;
                }
                else
                {
                    $row['duration'] = $row['video_duration'];
                }
                
                $row['total_comments'] = HelperFunctions::get_video_comment_numbers($video_id, $huddle_id, $user_id);
                $if_evaluator = HelperFunctions::check_if_evalutor($huddle_id, $user_id);
                $h_type = AccountFolderMetaData::getMetaDataValue($huddle_id, $site_id);
                $is_coach_enable = HelperFunctions::is_enabled_coach_feedback($huddle_id);
                $row['total_attachment'] = count(Document::getVideoDocumentsByVideo($video_id, $huddle_id, $user_id, $h_type, $if_evaluator, $is_coach_enable, $site_id));
                $from_workspace = 0;
                if ($folder_type == 3) {
                    $from_workspace = 1;
                }
                $row['updated_by'] = $user_id;
                $data = app('App\Http\Controllers\WorkSpaceController')->conversion_to_thmubs($row, 1, $from_workspace);
                $sample_data[$key] = $data;
            }
        }
        // if($data)
        // {
        //     $data[0]['updated_by'] = $user_id;
        // }
        return $sample_data;
    } 
    
    public static function deleteDiscussionAttachment($document_id,$site_id) {
      
        $flag = 0;
        CommentAttachment::where('document_id', $document_id)->delete();
       
        $flag = Document::where('id', $document_id)->where('site_id', $site_id)->delete();
        
        
        return $flag;
    }
    
    public static function renameDiscussionAttachment($title, $document_id, $site_id) {
        $title = htmlspecialchars($title);
        $updates_array = ['original_file_name' => $title];
        
        $result = Document::where('id', $document_id)
                    ->where('site_id', $site_id)
                    ->update($updates_array);
        if ($result) {
            return ['success'=>true, 'title' => $title];
        } else {
            return ['success'=>true, 'message'=>'Title changed successfully'];
        }
    }
 
    public static function mysql_escape($inp)
        { 
            if(is_array($inp)) return array_map(__METHOD__, $inp);

            if(!empty($inp) && is_string($inp)) { 
                return str_replace(array('\\', "\0", "\n", "\r", "'", '"', "\x1a"), array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'), $inp); 
            } 

            return $inp; 
        }

    public static function get_document_filename_with_extension($document_id) {
        $result = Document::leftJoin('account_folder_documents as afd', 'afd.document_id', '=', 'documents.id')
                          ->where('documents.id', $document_id)
                          ->select(['afd.title','documents.original_file_name','documents.url'])
                          ->first();
        if ($result) {
            if (!empty($result->title)) {
                $title = $result->title;
            } else {
                $title = $result->original_file_name;
            }

            $path_info = pathinfo($title);
            $extension= isset($path_info['extension'])?$path_info['extension']:'';
            if (!empty($extension)) {
                return $title;
            } else {
                $path_info = pathinfo($result->url);
                $extension = isset($path_info['extension'])?$path_info['extension']:'';
                
                return $title.".".$extension;
            }
        } else {
            return false;
        }
    }
    

}