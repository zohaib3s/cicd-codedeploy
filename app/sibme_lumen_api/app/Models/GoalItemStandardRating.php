<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoalItemStandardRating extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    public $timestamps = false;
    public $table = "goal_item_standard_ratings";
}