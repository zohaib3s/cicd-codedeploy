<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Services\HelperFunctions;

class UserCustomField extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_custom_fields';
    protected $fillable = ['user_id', 'custom_field_id', 'custom_field_value', 'created_by', 'created_at', 'updated_by', 'updated_at', 'site_id'];

    public static function get_user_custom_fields($user_id, $account_id, $can_user_edit_custom_fields){
        $join = !empty($can_user_edit_custom_fields) ? " LEFT OUTER " : " INNER ";
        $custom_fields = DB::select("SELECT cf.id as custom_field_id, cf.`field_label`, cf.`field_type`, ucf.`id` AS user_custom_field_id, ucf.`custom_field_value`
                                        FROM (
                                        SELECT id, field_label, field_type FROM `custom_fields` WHERE `account_id` = (SELECT `parent_account_id` FROM accounts WHERE id = ".$account_id.") AND is_shared = 1 AND ref_type = 1
                                        UNION
                                        SELECT id, field_label, field_type FROM `custom_fields` WHERE `account_id` = ".$account_id." AND ref_type = 1
                                        ) AS cf 
                                        ".$join." JOIN `user_custom_fields` AS ucf ON cf.id=ucf.`custom_field_id` AND ucf.`user_id`=".$user_id." ORDER BY cf.id ASC
                                    ");
        return $custom_fields;
    }
    
     public static function get_users_custom_fields_of_account($user_id , $account_id , $cf_id , $can_user_edit_custom_fields){
        $join = !empty($can_user_edit_custom_fields) ? " LEFT OUTER " : " INNER ";
        $custom_fields = DB::select("SELECT cf.id as custom_field_id, cf.`field_label`, cf.`field_type` , ucf.`user_id`, ucf.`custom_field_value`
                                        FROM (
                                        SELECT id, field_label, field_type FROM `custom_fields` WHERE `account_id` = (SELECT `parent_account_id` FROM accounts WHERE id = ".$account_id.") AND is_shared = 1 AND ref_type = 1 AND id = ".$cf_id."
                                        UNION
                                        SELECT id, field_label, field_type FROM `custom_fields` WHERE `account_id` = ".$account_id." AND ref_type = 1 AND id = ".$cf_id."
                                        ) AS cf 
                                        ".$join." JOIN `user_custom_fields` AS ucf ON cf.id=ucf.`custom_field_id` AND ucf.`user_id`=".$user_id." ORDER BY cf.id ASC
                                    ");
        return $custom_fields;
    }

    public static function save_user_custom_fields($user_id, $current_user_id, $account_id, $current_user_role_id, $user_custom_fields){
        $flag = false;
        foreach($user_custom_fields as $field){
            if(!empty($field['user_custom_field_id'])){
                self::where("id",$field['user_custom_field_id'])->where("custom_field_id", $field['custom_field_id'])->update(['custom_field_value'=>$field['custom_field_value'], 'updated_by'=>$current_user_id, 'updated_at'=>date('Y-m-d H:i:s') ]);
                $flag = true;
            }elseif(!empty($field['custom_field_value'])){
                $ucf = new self();
                $ucf->user_id = $user_id;
                $ucf->custom_field_id = $field['custom_field_id'];
                $ucf->custom_field_value = $field['custom_field_value'];
                $ucf->created_by = $current_user_id;
                $ucf->updated_by = $current_user_id;
                $ucf->save();
                $flag = true;
            }
        }
        if($flag){
            self::broadcast_websocket($user_id, $account_id, $current_user_role_id);
        }
   }

    public static function broadcast_websocket($user_id, $account_id, $current_user_role_id){
        $can_user_edit_custom_fields = CustomField::can_user_edit_custom_fields($current_user_role_id, $account_id);
        $user_custom_fields = self::get_user_custom_fields($user_id, $account_id, $can_user_edit_custom_fields);
        $event = [
            'channel' => "user-settings-".$account_id."-".$user_id,
            'event' => "user_custom_field_updated",
            'data' => ['user_custom_fields'=>$user_custom_fields]
        ];

        $is_push_notification_allowed = HelperFunctions::is_push_notification_allowed($account_id);
        if(!$is_push_notification_allowed){
            HelperFunctions::broadcastEvent($event, 'broadcast_event', false);
        } else {
            HelperFunctions::broadcastEvent($event);
        }
    }

}