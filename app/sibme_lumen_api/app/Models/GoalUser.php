<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoalUser extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    const permissionView = 1;
    const permissionEdit = 2;
    const permissionEvidence = 3;
    const permissionReview = 4;
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_edit_date';
}