<?php

namespace App\Models;
use App\Models\Sites;

use Illuminate\Database\Eloquent\Model;

class JobQueue extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'JobQueue';
    protected $fillable = ['JobId', 'CreateDate', 'RequestXml', 'JobQueueStatusId', 'CurrentRetry', 'site_id', 'JobSource'];
    public $timestamps = false;

    public static function job_queue_storage_function($site_id, $account_id, $jobid) {
        $xml = "<StorageCalculationJob><accounts_id>$account_id</accounts_id></StorageCalculationJob>";
        $data = [
            'JobId' => $jobid,
            'CreateDate' => date("Y-m-d H:i:s"),
            'RequestXml' => $xml,
            'JobQueueStatusId' => 1,
            'site_id' => $site_id,
            'CurrentRetry' => 0,
            'JobSource' => Sites::get_base_url($site_id)
        ];
    	return JobQueue::create($data);
    }

    public static function add_job_queue($site_id, $jobid, $to, $subject, $html, $is_html = false, $reply_to = '', $cc = '', $bcc = '', $returnInsertAble = false) {
        if ($is_html)
            $ihtml = 'Y';
        else
            $ihtml = 'N';

        if ($reply_to != '') {
            $reply_to = "<replyto>$reply_to</replyto>";
        }

        if ($cc != '') {
            $cc = "<cc>$cc</cc>";
        }

        if ($bcc != '') {
            $bcc = "<bcc>$bcc</bcc>";
        }
        $html = '<![CDATA[' . $html . ']]>';
        $xml = "<mail><to>$to</to>$reply_to <from>".Sites::get_site_settings('static_emails', $site_id)['noreply']."</from><fromname>".Sites::get_site_settings('site_title', $site_id)."</fromname>". $cc . $bcc ."<subject><![CDATA[$subject]]></subject><body>$html</body><html>$ihtml</html></mail>";
        $data = array(
            'JobId' => $jobid,
            'site_id' => $site_id,
            'CreateDate' => date("Y-m-d H:i:s"),
            'RequestXml' => $xml,
            'JobQueueStatusId' => 1,
            'CurrentRetry' => 0,
            'JobSource' => Sites::get_base_url($site_id)
        );
        if($returnInsertAble)
        {
            return $data;
        }
        return \DB::table('JobQueue')->insert($data);
    }

}