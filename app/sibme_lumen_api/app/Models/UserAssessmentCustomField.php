<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Services\HelperFunctions;

class UserAssessmentCustomField extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_assessment_custom_fields';
    protected $fillable = ['assessee_id', 'assessment_custom_field_id', 'assessment_custom_field_value', 'created_by', 'created_at', 'updated_by', 'updated_at'];

    public static function get_user_assessment_custom_fields($assessee_id, $account_folder_id){
        $custom_fields = DB::select("SELECT acf.id AS assessment_custom_field_id, cf.field_label, cf.field_type, uacf.id AS user_assessment_custom_field_id, uacf.assessment_custom_field_value
                                        FROM
                                            custom_fields AS cf
                                        INNER JOIN assessment_custom_fields AS acf ON (cf.id = acf.custom_field_id)
                                        LEFT OUTER JOIN user_assessment_custom_fields AS uacf ON (acf.id = uacf.assessment_custom_field_id AND uacf.assessee_id=".$assessee_id.")
                                        WHERE (acf.account_folder_id =".$account_folder_id.") ORDER BY cf.id ASC");
        return $custom_fields;
    }

    public static function save_user_assessment_custom_fields($account_folder_id, $assessee_id, $current_user_id, $assessment_custom_fields){
        $flag = false;
        foreach($assessment_custom_fields as $field){
            if(!empty($field['user_assessment_custom_field_id'])){
                self::where("id",$field['user_assessment_custom_field_id'])->where("assessment_custom_field_id", $field['assessment_custom_field_id'])->update(['assessment_custom_field_value'=>$field['assessment_custom_field_value'], 'updated_by'=>$current_user_id, 'updated_at'=>date('Y-m-d H:i:s') ]);
                $flag = true;
            }elseif(!empty($field['assessment_custom_field_value'])){
                $ucf = new self();
                $ucf->assessee_id = $assessee_id;
                $ucf->assessment_custom_field_id = $field['assessment_custom_field_id'];
                $ucf->assessment_custom_field_value = $field['assessment_custom_field_value'];
                $ucf->created_by = $current_user_id;
                $ucf->updated_by = $current_user_id;
                $ucf->save();
                $flag = true;
            }
        }
        if($flag){
            self::broadcast_websocket($assessee_id, $account_folder_id);
        }
    }

    public static function remove_user_assessment_custom_fields($assessee_id, $account_folder_id, $delete_all=false, $notIn = ""){
        $assessee_qry = "";
        if(!empty($assessee_id) && empty($delete_all)){
            $assessee_qry = " uacf.assessee_id $notIn IN (".$assessee_id.") AND ";
        }
        $q = 'DELETE uacf 
              FROM user_assessment_custom_fields AS uacf 
              INNER JOIN assessment_custom_fields AS acf ON uacf.assessment_custom_field_id = acf.id
              WHERE '.$assessee_qry.' acf.account_folder_id = '.$account_folder_id;
        $affected_rows = \DB::delete($q);
        if(!empty($affected_rows)){
            self::broadcast_websocket($assessee_id, $account_folder_id);
        }
    }

    public static function broadcast_websocket($assessee_id, $account_folder_id){
        $account_id = AccountFolder::where('account_folder_id', $account_folder_id)->value('account_id');
        if(empty($account_id)){
            return false;
        }
        if(!empty($assessee_id)){
            self::broadcast_assessee_websocket($assessee_id, $account_folder_id, $account_id);
        } else {
            $assessees = \App\Models\AccountFolderUser::where('account_folder_id', $account_folder_id)->pluck('user_id');
            foreach ($assessees as $assessee_id) {
                self::broadcast_assessee_websocket($assessee_id, $account_folder_id, $account_id);
            }
        }
    }

    public static function broadcast_assessee_websocket($assessee_id, $account_folder_id, $account_id){
        $user_assessment_custom_fields = UserAssessmentCustomField::get_user_assessment_custom_fields($assessee_id, $account_folder_id);
        $event_for_assessment_huddle = [
            'channel' => "huddle-details-".$account_folder_id,
            'event' => "user_assessment_custom_field_updated",
            'data' => ['user_assessment_custom_fields'=>$user_assessment_custom_fields],
            'assessee_id' => $assessee_id
        ];
        $event = [
            'channel' => "huddle-details-".$account_folder_id."-".$assessee_id,
            'event' => "user_assessment_custom_field_updated",
            'data' => ['user_assessment_custom_fields'=>$user_assessment_custom_fields]
        ];

        $is_push_notification_allowed = HelperFunctions::is_push_notification_allowed($account_id);
        if(!$is_push_notification_allowed){
            HelperFunctions::broadcastEvent($event_for_assessment_huddle, 'broadcast_event', false);
            HelperFunctions::broadcastEvent($event, 'broadcast_event', false);
        } else {
            HelperFunctions::broadcastEvent($event_for_assessment_huddle);
            HelperFunctions::broadcastEvent($event);
        }
    }


}