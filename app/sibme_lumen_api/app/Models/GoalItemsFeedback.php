<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoalItemsFeedback extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_edit_date';
    public $table = "goal_items_feedback";
}