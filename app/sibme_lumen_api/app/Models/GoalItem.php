<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GoalItem extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_edit_date';

    public function allowedEvidence()
    {
        return $this->hasMany("App\Models\GoalItemEvidence", "goal_item_id", "id");
    }

    public function submittedEvidence()
    {
        return $this->hasMany("App\Models\GoalItemSubmittedEvidence", "goal_item_id", "id");
    }

    public function itemFeedback()
    {
        return $this->hasMany("App\Models\GoalItemsFeedback", "owner_id", "user_id");
    }
    public function itemFeedbacks()
    {
        return $this->hasOne("App\Models\GoalItemsFeedback", "goal_item_id", "id");
    }
}