<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LiveStreamViewers extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'live_stream_viewers';
}