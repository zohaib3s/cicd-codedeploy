<?php

namespace App\Models;

use App\Http\Controllers\HuddleController;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DocumentFolder extends Model {


    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_edit_date';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $casts = [
        'created_date' => 'string',
        'last_edit_date' => 'string',
    ];
    protected $table = 'document_folders';
    public function setCreatedDateAttribute($date)
    {
        $this->attributes['created_date'] = date("Y-m-d H:i:s",strtotime($date));
    }
    public function setLastEditDateAttribute($date)
    {
        $this->attributes['last_edit_date'] = date("Y-m-d H:i:s",strtotime($date));
    }

    public static function count_folders_in_folder($folder_id, $site_id) {
        $count_folder = DocumentFolder::where("parent_folder_id", $folder_id)
            ->where("active", 1)
            ->where("site_id", $site_id)
            ->count();
        return $count_folder;
    }



    public static function getFolders($account_folder_id, $account_id, $site_id, $user_id = 0, $parent_folder_id = 0, $sort = '', $title = '', $apply_parent_check = 1)
    {
        $query = DocumentFolder::join('users',"users.id", "=", "document_folders.created_by")->leftJoin('documents as d',"d.parent_folder_id", "=", "document_folders.id")
            ->select('document_folders.*',DB::raw("CONCAT(users.first_name,' ',users.last_name) as created_by_name")
                ,DB::raw("(SELECT 
                                COUNT(*) FROM document_folders AS df  WHERE df.active = 1   AND df.site_id = ".$site_id."  AND df.parent_folder_id = document_folders.id) AS folders,
                               COUNT(IF((d.site_id = ".$site_id."  AND ( d.doc_type = 1 OR ( d.doc_type = 3 AND ( d.is_processed = 4  OR d.is_processed = 5 ) )
                                )   AND d.parent_folder_id = document_folders.id), 1, NULL)) 'videos',
                                  COUNT(IF((d.site_id = ".$site_id." 
                               AND ( d.doc_type = 3  AND d.is_processed < 4 )  ), 1, NULL)) 'scripted_notes',
                                   COUNT(IF((d.site_id = ".$site_id."  AND d.doc_type = 2 ), 1, NULL)) 'resources',
                                    COUNT(IF((d.site_id = ".$site_id."  AND d.doc_type = 5  ), 1, NULL)) 'urls'"))
            ->where("document_folders.account_folder_id", $account_folder_id)
            ->where('document_folders.account_id',$account_id)
            ->where('document_folders.active', 1)
            ->where('document_folders.site_id', $site_id);
        if($user_id)
        {
            $query->where('document_folders.created_by', $user_id);
        }
        if($apply_parent_check)
        {
            if($parent_folder_id)
            {
                $query->where('document_folders.parent_folder_id', $parent_folder_id);
            }
            else
            {
                $query->where(function ($q){
                    $q->where('document_folders.parent_folder_id', 0)->orWhereNull('document_folders.parent_folder_id');
                });
            }
        }
        if (!empty($title))
        {
            $title =   Document::mysql_escape($title);
            $query->where('document_folders.name','like', "%$title%");
        }

        if ($sort == 'uploaded_by') {
            $orderBy = 'users.first_name ASC';
        } elseif ($sort == 'uploaded_date') {
            $orderBy = 'document_folders.created_date DESC';
        } elseif ($sort == 'last_modified' || $sort == 'last_edit_date') {
            $orderBy = 'document_folders.last_edit_date DESC';
        } else {
            $orderBy = 'document_folders.name ASC';
        }

        $results = $query->groupBy('document_folders.id')->orderByRaw($orderBy)->get();
        $folders = [];
        foreach ($results as $folder)
        {
            $folders[] = self::formatFolder($folder);
        }
        return $folders;
    }

    public static function count_artifacts_in_folder($folder_id, $doc_type)
    {
        $query = Document::where('parent_folder_id', $folder_id);
        if($doc_type == 1)
        {
            $query->where(function ($mq) use ($doc_type)
            {
                $mq->where('doc_type', $doc_type)
                    ->orWhere(function($q1) {
                        $q1->where("documents.doc_type", "3");
                        $q1->where(function($q2) {
                            $q2->where("documents.is_processed", "4")
                                ->orWhere("documents.is_processed", "5");
                        });
                    });
            });
        }
        else if($doc_type == 3)
        {
            $query->where('doc_type', $doc_type)->where("documents.is_processed", "<", "4");
        }
        else
        {
            $query->where('doc_type', $doc_type);
        }
        return $query->count();
    }

    public static function formatFolder($folder)
    {
        $arranged_folder_object = $folder;
        $stats = [];
        if(isset($folder->folders))
        {
            $stats ['folders'] = $folder->folders;
        }
        else
        {
            $stats ['folders'] = self::count_folders_in_folder($folder->id, $folder->site_id);
        }
        if(isset($folder->videos))
        {
            $stats ['videos'] = $folder->videos;
        }
        else
        {
            $stats ['videos'] = self::count_artifacts_in_folder($folder->id, 1);
        }
        if(isset($folder->resources))
        {
            $stats ['resources'] = $folder->resources;
        }
        else
        {
            $stats ['resources'] = self::count_artifacts_in_folder($folder->id, 2);
        }
        if(isset($folder->scripted_notes))
        {
            $stats ['scripted_notes'] = $folder->scripted_notes;
        }
        else
        {
            $stats ['scripted_notes'] = self::count_artifacts_in_folder($folder->id, 3);
        }
        if(isset($folder->urls))
        {
            $stats ['urls'] = $folder->urls;
        }
        else
        {
            $stats ['urls'] = self::count_artifacts_in_folder($folder->id, 5);
        }

        $arranged_folder_object->created_on = HuddleController::getCurrentLangDate(strtotime($folder->created_date), "archive_module");
        $arranged_folder_object->stats = $stats;
        $arranged_folder_object->huddle_id = $folder->account_folder_id;
        $arranged_folder_object->folder_id = $folder->id;
        $arranged_folder_object->title = $folder->name;
        if(!isset($arranged_folder_object->created_by_name) || (isset($arranged_folder_object->created_by_name) && empty($arranged_folder_object->created_by_name)))
        {
            $arranged_folder_object->created_by_name = HuddleController::get_user_name_from_id($folder->created_by);
        }
        return $arranged_folder_object;
    }

    public static function getNLevelChilds($folder_id, $include_current_folder = 0, $only_ids = 0)
    {
        $results = [];
        if($folder_id && $folder_id !='undefined'){
            $query = "SELECT * 
            FROM (SELECT * FROM document_folders ORDER BY parent_folder_id, id) folders_sorted,
            (SELECT @pv := $folder_id) initialisation
            WHERE   FIND_IN_SET(parent_folder_id, @pv) > 0
            AND @pv := CONCAT(@pv, ',', id);";
            if($include_current_folder)
            {
                $query = "SELECT *,@pv = $folder_id FROM document_folders WHERE id = $folder_id UNION ". $query;
            }
            $results = DB::select($query);
            if($only_ids)
            {
                $folder_ids = [];
                foreach ($results as $folder)
                {
                    $folder_ids[] = $folder->id;
                }
                return $folder_ids;
            }
        }
        
        return $results;
    }

    public static function getNLevelParents($folder_id)
    {
        $results = [];
        if($folder_id && $folder_id !='undefined')
        {
            $folder = DocumentFolder::find($folder_id);
            if($folder)
            {
                $query = "SELECT T2.*
                    FROM (
                        SELECT
                            @r AS _id,
                            (SELECT @r := `parent_folder_id` FROM document_folders WHERE id = _id) AS parent_id,
                            @l := @l + 1 AS lvl
                        FROM
                            (SELECT @r := $folder_id, @l := 0) vars,
                            document_folders m
                        WHERE @r <> 0) T1
                    JOIN document_folders T2
                    ON T1._id = T2.id
                    ORDER BY T1.lvl DESC;";
                $results = DB::select($query);
                foreach ($results as $folder)//for breadcumbs
                {
                    $folder->folder_id = $folder->id;
                    $folder->folder_name = $folder->name;
                    $folder->is_document = true;
                }
            }
        }
        return $results;
    }

    public function children()
    {
        return $this->hasMany('DocumentFolder', 'parent_folder_id');
    }

// recursive, loads all descendants
    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
        // which is equivalent to:
        // return $this->hasMany('Survey', 'parent')->with('childrenRecursive);
    }

// parent
    public function parent()
    {
        return $this->belongsTo('DocumentFolder','parent_folder_id');
    }

// all ascendants
    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }
}
