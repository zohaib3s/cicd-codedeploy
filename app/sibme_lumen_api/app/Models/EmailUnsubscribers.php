<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailUnsubscribers extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'email_unsubscribers';
    
    public static function check_subscription($user_id, $email_id, $account_id, $site_id) {       
        return (!EmailUnsubscribers::join('users_accounts as ua','ua.user_id','=','email_unsubscribers.user_id')->where(array(
           'email_unsubscribers.user_id' => $user_id, 'email_unsubscribers.email_format_id' => $email_id, 'email_unsubscribers.account_id' => $account_id, 'email_unsubscribers.site_id' => $site_id,'ua.account_id'=> $account_id
        ))->exists());
    }

}