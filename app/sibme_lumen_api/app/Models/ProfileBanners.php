<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class ProfileBanners extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'profile_banners';

     public static function get_banners( $site_id)
    {
        return ProfileBanners::where('site_id',  $site_id) ->first();
    }
}
