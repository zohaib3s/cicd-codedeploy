<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AnalyticsFilter extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'analytics_filters';
    
    public static function get_my_analytics_filters($current_user_id, $current_account_id, $site_id){
        return AnalyticsFilter::leftJoin('accounts', 'analytics_filters.subAccount', '=', 'accounts.id')
                                  ->select('analytics_filters.*',DB::raw('(CASE WHEN accounts.company_name IS NULL THEN "All Accounts" ELSE accounts.company_name END) AS account_name'))
                                  ->where('current_user_id', $current_user_id)
                                  ->where('current_account_id', $current_account_id)
                                  ->where('analytics_filters.site_id', $site_id)
                                  ->get();
    }
}
