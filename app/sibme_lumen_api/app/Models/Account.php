<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Account extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'accounts';

    public static function getMyAccounts($account_id) {
        return Account::select('plans.*', 'accounts.*')->leftJoin("plans","plans.id", "accounts.plan_id")->where("accounts.id", (int) $account_id)->first();
    }

    public static function haveParent($account_id) {
        $account_id = (int) $account_id;

        $result = Account::select('accounts.parent_account_id')
            ->where('id', $account_id)
            ->where('parent_account_id', '!=', 0)
            ->first();
        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }

    public static function child_accounts_exist($account_id) {
        $account_id = (int) $account_id;
        return Account::where("parent_account_id", $account_id)->exists();
    }

    public static function getRelatedAccounts($account_id, $site_id, $have_parent = false) {
        if ($have_parent) {
            $query = 'SELECT
                      Account.*,
                      `AccountMeta`.`account_meta_data_id`,
                      `AccountMeta`.`meta_data_name`,
                      `AccountMeta`.`meta_data_value`
                    FROM
                      (SELECT
                        @r AS _id,
                        (SELECT
                          @r := parent_account_id
                        FROM
                          accounts
                        WHERE id = _id) AS parent_account_id,
                        @l := @l + 1 AS lvl
                      FROM
                        (SELECT
                          @r := ' . $account_id . ',
                          @l := 0) vars,
                        accounts h
                      WHERE @r <> 0) T1
                      JOIN accounts Account
                        ON T1._id = Account.id
                    
                      INNER JOIN `account_meta_data` AS `AccountMeta`
                        ON (
                          `AccountMeta`.`account_id` = Account.id
                          AND `AccountMeta`.`meta_data_name` = "enable_video_library"
                          AND `AccountMeta`.`meta_data_value` = 1
                        )
                    WHERE Account.site_id = "' . $site_id . '"
                    GROUP BY Account.id
                    ORDER BY T1.lvl ASC';
            $result = \DB::select($query);
            if(!isset($result[0]))
            {
                $result = array();
            }
            else if ($result[0]->id != $account_id) {
                $result = array();
            }
        } else {
            $result = array();
        }

        return $result;
    }

    public static function getPreviousStorageUsed($account_id, $site_id)
    {
        return Account::where('id', $account_id)->where('site_id', $site_id)->select(['storage_used'])->first();
    }

    public static function getAccountByUserID($account_id, $user_id, $site_id)
    {
        return Account::leftJoin('users_accounts', 'accounts.id', '=', 'users_accounts.account_id')
            ->select('company_name')
            ->where('users_accounts.site_id', $site_id)
            ->where('users_accounts.account_id', $account_id)
            ->where('users_accounts.user_id', $user_id)
            ->first();
    }
    public static function get_allowed_users($account_id)
    {
        $trial_users = 40;
        $unlimited_users = 100000;

        $result = Account::where('id', $account_id)
            ->first()->toArray();
        if ($result['deactive_plan']) {
            if ($result['custom_users'] == -1) {
                return $unlimited_users;
            } else {
                return $result['custom_users'];
            }
        } elseif ($result['in_trial']) {
            return $trial_users;
        } elseif (!empty($result['plan_id']) && $result['plan_id'] < 9) {
           $plan_id =  $result['plan_id'];
           $plans = app('db')->select("select * from plans where id =$plan_id");
           $plans = json_decode(json_encode($plans), True);
            return $plans[0]['users'];
        } else {
            return $result['plan_qty'];
        }
    }
}
