<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GlobalSettings extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'global_settings';
    protected $connection = 'production_global';

    public static function getSettingByName($name, $default = false)
    {
        $setting = self::where('name', $name)->first();
        if($setting)
        {
            return $setting->value;
        }
        return $default;
    }
}