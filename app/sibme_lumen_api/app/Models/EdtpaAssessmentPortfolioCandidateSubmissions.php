<?php 

namespace App\Models;

use App\Http\Controllers\HuddleController;
use App\Services\HelperFunctions;
use Illuminate\Database\Eloquent\Model;
class EdtpaAssessmentPortfolioCandidateSubmissions extends Model{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'edtpa_assessment_portfolio_candidate_submissions';
}
?>