<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentFiles extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'document_files';
    protected $fillable = ['document_id','url','resolution','duration','file_size','default_web','transcoding_job_id','transcoding_status','transcoding_status_details'];

    public static function get_document_row($document_id, $site_id) {
      
        $result = DocumentFiles::where(array(
                        'document_id' => (int)$document_id, 'site_id' => $site_id))->first();
        
        if ($result) {
            return $result->toArray();
        } else {
            return false;
        }
    }
}