<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Services\TranslationsManager;
use Illuminate\Support\Facades\DB;

class AccountFolder extends Model {


    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_edit_date';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $casts = [
        'created_date' => 'string',
        'last_edit_date' => 'string',
    ];
    protected $table = 'account_folders as AccountFolder';
    //protected $primaryKey = 'account_folder_id';
    public function setCreatedDateAttribute($date)
    {
        $this->attributes['created_date'] = date("Y-m-d H:i:s",strtotime($date));
    }
    public function setLastEditDateAttribute($date)
    {
        $this->attributes['last_edit_date'] = date("Y-m-d H:i:s",strtotime($date));
    }
    public static function get_folder_type($account_folder_id, $site_id) {
        $huddle = AccountFolder::where('account_folder_id', $account_folder_id)
                ->where('site_id', $site_id)
                ->select(['folder_type'])
                ->first();
        $account_folder_type = '';
        if ($huddle) {
            $account_folder_type = $huddle->folder_type;
        }
        return $account_folder_type;
    }

    public static function getHuddle($site_id, $account_folder_id, $account_id = 0) {
        $account_folder_id = (int) $account_folder_id;
        $account_id = (int) $account_id;

        $query = AccountFolder
                        ::join('users as User', 'User.id', '=', 'AccountFolder.created_by')
                        ->leftJoin('account_folders_meta_data', 'account_folders_meta_data.account_folder_id', '=', 'AccountFolder.account_folder_id')
                        ->select('AccountFolder.*', 'User.username', 'User.first_name', 'User.last_name', 'User.email')
                        ->selectRaw('account_folders_meta_data.meta_data_value as message')
                        ->selectRaw('account_folders_meta_data.account_folder_meta_data_id as message_id')
                        ->selectRaw('account_folders_meta_data.meta_data_name as meta_data_name')
                        ->where('AccountFolder.account_folder_id', $account_folder_id)
                        ->where('AccountFolder.active', 1)
                        ->where('AccountFolder.site_id', $site_id)
                        ->whereIn('AccountFolder.folder_type', [1,2, 3])
                        ->get();
        $result = [];
        if($query){
            $result = $query->toArray();
        }

        return $result;
    }

    public static function getHuddleUsers($account_folder_id, $site_id, $role_id = [], $page = false, $search = null,$limit = false,$user_id ='',$user_role_id='') {
        $account_folder_id = (int) $account_folder_id;
        $query = self::getHuddleUsersQuery($account_folder_id, $site_id, $role_id, $search,$user_id,$user_role_id);
        
        if($limit == false){
            $limit = 50;
        }
        //$limit = 50;
        if ($page!==false) {
            // $page must be starting from 1 (NOT 0)
            $query->forPage($page, $limit);
        }
        $result = $query->get();
        if($result){
            return $result->toArray();
        } else {
            return [];
        }
    }

    public static function getHuddleUsersCount($account_folder_id, $site_id, $role_id = [], $search = null) {
        $account_folder_id = (int) $account_folder_id;
        $query = self::getHuddleUsersQuery($account_folder_id, $site_id, $role_id, $search);
        return $query->get()->count();
    }

    public static function getHuddleUsersQuery($account_folder_id, $site_id, $role_id = [], $search = null,$user_id='',$user_role_id='') {
        $account_folder_id = (int) $account_folder_id;
        $order_by = DB::raw('User.first_name, User.last_name');

        $query = AccountFolder
                        ::join('account_folder_users as huddle_users', 'huddle_users.account_folder_id', '=', 'AccountFolder.account_folder_id')
                        ->join('users as User', 'User.id', '=', 'huddle_users.user_id')
                        ->select(
                                'huddle_users.*', 'User.id', 'User.first_name', 'User.last_name', 'User.image', 'User.email','User.is_active'
                        )->where(array(
                            'AccountFolder.account_folder_id' => $account_folder_id,
                            'AccountFolder.site_id' => $site_id
                        ));
        if(!empty($role_id)){
            settype($role_id, "array");
            $query->whereIn('huddle_users.role_id', $role_id);
        }
        if($user_role_id ==210 && $user_id !=''){
            $query->where('huddle_users.user_id', $user_id);
        }
        if (!empty($search)) {
            $search = trim($search);
            $query->where(function ($sub_query) use ($search) {
                $sub_query->where(DB::raw('CONCAT(User.first_name, " ", User.last_name)'), 'like', '%'.$search.'%');
                $sub_query->orWhere('User.email', 'like', '%'.$search.'%');
            });
        }
        $query->groupBy("huddle_users.user_id")->orderBy($order_by);
        return $query;
    }

    public static function getAccountFolderType($account_folder_id) {
        $accountFolderMetaData = AccountFolderMetaData::where("account_folder_id", $account_folder_id)->where("meta_data_name", "folder_type")->first();
        if($accountFolderMetaData){
            return !empty($accountFolderMetaData->meta_data_value)?$accountFolderMetaData->meta_data_value:false;
        }else{
            return false;
        }
        
    }

    public static function get_all_huddle_users($account_id, $sort = '') {
        $account_id = (int) $account_id;

        $fields = array(
            'User.*',
            'AFU.*',
            'AccountFolder.*'
        );



        $result = AccountFolder::leftJoin('users as User','User.id','=','AccountFolder.created_by')
            ->join('account_folder_users as AFU','AFU.account_folder_id','=','AccountFolder.account_folder_id')
            ->select($fields)
            ->where(array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type' => 1,
                'AccountFolder.active' => 1
            ))
            ->groupBy('AccountFolder.account_folder_id')
            ->orderBy('AccountFolder.name','ASC')
            ->orderBy('User.first_name', 'ASC')
            ->get();


        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public static function getUserHuddles($user_id, $account_id,$huddle_type_id = null) {
        $user_id = (int) $user_id;

        $query = AccountFolder
                        ::join('account_folder_users as huddle_users', 'huddle_users.account_folder_id', '=', 'AccountFolder.account_folder_id')
                        ->join('users as User', 'User.id', '=', 'huddle_users.user_id');
        if($huddle_type_id)
        {
            $query->join('account_folders_meta_data as AFMD', function($join) {
                $join->on('AFMD.account_folder_id', '=', 'huddle_users.account_folder_id');
                $join->whereRaw('meta_data_name = "folder_type"');
            });
        }
        $query->select('huddle_users.*', 'User.id')->where(array('huddle_users.user_id' => $user_id, 'AccountFolder.account_id' => $account_id));
        if($huddle_type_id)
        {
            $query->where("AFMD.meta_data_value",$huddle_type_id);
        }
        $result = $query->get()->toArray();

        return !empty($result) ? $result : [];
    }

    public static function isUserParticipatingInHuddle($folder_id, $account_id, $user_id, $site_id) {

        $folder_ids = self::get_folder_childs($folder_id, $site_id);
        $flag = false;
        foreach ($folder_ids as $fold_id) {
            $has_huddle = self::isFolderContainsHuddles($account_id, $user_id, $fold_id, $site_id);
            if ($has_huddle == true) {
                $flag = true;
            }
        }

        if ($flag) {
            return true;
        } else {
            return false;
        }
    }

    public static function get_folder_childs($folder_id, $site_id) {
        $results = app('db')->select("SELECT
         af.`account_folder_id`
          , af1.`account_folder_id` as af1_account_folder_id
           , af2.`account_folder_id` as af2_account_folder_id
           , af3.`account_folder_id` as af3_account_folder_id
            , af4.`account_folder_id` as af4_account_folder_id
             , af5.`account_folder_id` as af5_account_folder_id
             , af6.`account_folder_id` as af6_account_folder_id
        FROM
          `account_folders` af
          LEFT JOIN `account_folders` af1
            ON (af.`account_folder_id` = af1.`parent_folder_id` AND af1.`folder_type` = 5 AND af1.`active` =1  AND af.`account_id` = af1.`account_id`)
            LEFT JOIN `account_folders` af2
            ON (af1.`account_folder_id` = af2.`parent_folder_id`  AND af2.`folder_type` = 5 AND af2.`active` =1  AND af1.`account_id` = af2.`account_id` )
                LEFT JOIN `account_folders` af3
            ON (af2.`account_folder_id` = af3.`parent_folder_id` AND af3.`folder_type` = 5 AND af3.`active` =1  AND af2.`account_id` = af3.`account_id` )
                LEFT JOIN `account_folders` af4
            ON (af3.`account_folder_id` = af4.`parent_folder_id` AND af4.`folder_type` = 5 AND af4.`active` =1   AND af3.`account_id` = af4.`account_id`)
                LEFT JOIN `account_folders` af5
            ON (af4.`account_folder_id` = af5.`parent_folder_id` AND af5.`folder_type` = 5 AND af5.`active` =1  AND af4.`account_id` = af5.`account_id`)
                LEFT JOIN `account_folders` af6
            ON (af5.`account_folder_id` = af6.`parent_folder_id` AND af5.`folder_type` = 5 AND af5.`active` =1  AND af5.`account_id` = af6.`account_id`)

            WHERE af.`account_folder_id` = " . $folder_id . " AND af.`active` = 1 AND af.`site_id` = " . $site_id . " AND af.`folder_type` = 5");


        $account_folder_ids = array();

        foreach ($results as $result) {

            if (!empty($result->account_folder_id)) {
                $account_folder_ids[$result->account_folder_id] = $result->account_folder_id;
            }
            if (!empty($result->af1_account_folder_id)) {
                $account_folder_ids[$result->af1_account_folder_id] = $result->af1_account_folder_id;
            }
            if (!empty($result->af2_account_folder_id)) {
                $account_folder_ids[$result->af2_account_folder_id] = $result->af2_account_folder_id;
            }
            if (!empty($result->af3_account_folder_id)) {
                $account_folder_ids[$result->af3_account_folder_id] = $result->af3_account_folder_id;
            }
            if (!empty($result->af4_account_folder_id)) {
                $account_folder_ids[$result->af4_account_folder_id] = $result->af4_account_folder_id;
            }
            if (!empty($result->af5_account_folder_id)) {
                $account_folder_ids[$result->af5_account_folder_id] = $result->af5_account_folder_id;
            }
            if (!empty($result->af6_account_folder_id)) {
                $account_folder_ids[$result->af6_account_folder_id] = $result->af6_account_folder_id;
            }
        }

        return $account_folder_ids;
    }

    public static function isFolderContainsHuddles($account_id, $user_id, $folder_id, $site_id) {
        $account_id = (int) $account_id;
        $query = AccountFolder::where('account_id', $account_id)
                ->where('folder_type', 1)
                ->where('active', 1)
                ->where('site_id', $site_id)
                ->whereRaw('( ( account_folder_id IN( select  account_folder_id from account_folder_users where user_id=' . $user_id . ' ) ) OR (account_folder_id IN (select  account_folder_id from account_folder_groups afg join user_groups ug on afg.group_id = ug.group_id where ug.user_id = ' . $user_id . '  ) ) ) ');

        if ($folder_id != false) {
            $query->where('parent_folder_id', $folder_id);
        } else {
            $query->whereNull('parent_folder_id');
        }

        $result = $query->exists();

        return $result;
    }

    public static function getHuddleGroups($account_folder_id) {
        $account_folder_id = (int) $account_folder_id;

        $result = AccountFolderGroup::join('user_groups', 'user_groups.group_id', '=', 'account_folder_groups.group_id')
                        ->join('groups', 'groups.id', '=', 'account_folder_groups.group_id')
                        ->select(
                                'user_groups.user_id', 'account_folder_groups.*','groups.name'
                        )->where(array(
                    'account_folder_groups.account_folder_id' => $account_folder_id

                ))->get()->toArray();

        return !empty($result) ? $result : false;
    }

    public static function copyAllHuddles($site_id, $account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false,$without_assessment=0) {
        $account_id = (int) $account_id;
        $user_id = app("Illuminate\Http\Request")->get('user_id');
        $huddle_array = $without_assessment ? [1, 2] : [1, 2, 3];
        $account_folder_users = array(
            array(
                'table' => 'account_folder_users',
                'alias' => 'huddle_users',
                'type' => 'left',
                'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
            ),
            array(
                'table' => 'users as User',
                'type' => 'left',
                'conditions' => array('User.id=huddle_users.user_id')
            ),
            array(
                'table' => 'account_folders_meta_data as AFMD',
                'type' => 'left',
                'conditions' => array('AFMD.account_folder_id=huddle_users.account_folder_id AND meta_data_name = "folder_type"')
            )
        );
        $fields = array(
            'User.*',
            '(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType',
            'AccountFolder.*',
            'huddle_users.user_id',
            'huddle_users.role_id',
            'huddle_users.account_folder_user_id',
            'AFMD.meta_data_value'
        );

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'TRIM(AccountFolder.name) ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }
        $groups = array('AccountFolder.account_folder_id');
        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }
        /* if ($account_folder_ids != false) {
          $where_in .= " AFMD.meta_data_name = 'folder_type'";
          } */

        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        $result = AccountFolder
                        ::leftJoin('account_folder_users as huddle_users', function($join) use ($user_id) {
                            $join->on('huddle_users.account_folder_id', '=', 'AccountFolder.account_folder_id');
                            $join->whereRaw("huddle_users.user_id = '$user_id'");
                        })
                        ->leftJoin('users as User', 'User.id', '=', 'huddle_users.user_id')
                        //   ->leftJoin('account_folders_meta_data as AFMD', 'AFMD.account_folder_id', '=', 'huddle_users.account_folder_id AND meta_data_name = "folder_type"')
                        ->leftJoin('account_folders_meta_data as AFMD', function($join) use($huddle_array) {
                            $join->on('AFMD.account_folder_id', '=', 'huddle_users.account_folder_id');
                            $join->whereRaw('meta_data_name = "folder_type"');
                            $join->whereIn('AFMD.meta_data_value', $huddle_array);
                        })
                        ->select('AccountFolder.*', 'huddle_users.user_id', 'huddle_users.role_id', 'huddle_users.account_folder_user_id', 'AFMD.meta_data_value')
                        ->selectRaw('(SELECT "coaching" FROM account_folders_meta_data WHERE account_folder_id = AccountFolder.`account_folder_id` AND meta_data_name = "folder_type" AND meta_data_value = 2) AS folderType')
                        ->selectRaw('(SELECT Count(account_folder_id) FROM document_folders WHERE account_folder_id = AccountFolder.`account_folder_id` AND active = 1) AS folder_count')
                        ->whereRaw($where_in)
                        ->whereRaw('AccountFolder.folder_type IN(1)')        
                        ->where(array(
                            'AccountFolder.account_id' => $account_id,
                            'AccountFolder.active' => 1,
                            'AccountFolder.site_id' => $site_id,
                        ))->orderByRaw($order_by)
                        ->groupBy('AccountFolder.account_folder_id')
                        ->get()->toArray();

//       echo $this->getLastQuery();
//        exit;
        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = app('db')->select('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['account_folder_id'] . ' AND meta_data_name = "folder_type" ');

                $result[$i]['meta_data_value'] = isset($res[0]->meta_data_value) ? $res[0]->meta_data_value : '';
                if($result[$i]['meta_data_value'] == 1)
                {
                    $result[$i]['icon_path'] = "./assets/img/collaboration_icon.svg";
                }
                else if($result[$i]['meta_data_value'] == 2)
                {
                    $result[$i]['icon_path'] = "./assets/img/coaching_icon.svg";
                }
                else if($result[$i]['meta_data_value'] == 3)
                {
                    $result[$i]['icon_path'] = "./assets/img/assessment_icon.svg";
                }
                else
                {
                    $result[$i]['icon_path'] = "./assets/img/folder_gry.svg";
                }
            }
            //echo '<pre>';
            $output = array();
            //$result = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] == '') {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
                //$output[] = $item;
            }
            $result = $output;
            //print_r($result);
            //die;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['created_date']);
                    $updated_at = new DateTime($item['last_edit_date']);
                    $item['created_at'] = $created_at->format(DateTime::W3C);
                    $item['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['description'] = $item['AccountFolder']['desc'];
                    $output[] = $item;
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getHuddleGroupsWithUsers($account_folder_id, $site_id) {
        $account_folder_id = (int) $account_folder_id;
        $result = AccountFolder
                        ::leftJoin('account_folder_groups as huddle_groups', 'huddle_groups.account_folder_id', '=', 'AccountFolder.account_folder_id')
                        ->leftJoin('groups as Group', 'Group.id', '=', 'huddle_groups.group_id')
                        ->leftJoin('user_groups as UserGroup', 'UserGroup.group_id', '=', 'Group.id')
                        ->leftJoin('users as User', 'UserGroup.user_id', '=', 'User.id')
                        ->select(
                                'User.*', 'huddle_groups.role_id', 'Group.name', 'Group.id as group_id'
                        )->where(array(
                    'AccountFolder.account_folder_id' => $account_folder_id,
                    'AccountFolder.site_id' => $site_id
                ))->get()->toArray();


        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public static function getHuddleGroupsUsers($account_folder_id, $site_id, $role_id=[]) {
        $account_folder_id = (int) $account_folder_id;
        $query = AccountFolder
                        ::join('account_folder_groups as huddle_groups', 'huddle_groups.account_folder_id', '=', 'AccountFolder.account_folder_id')
                        ->join('groups as Group', 'Group.id', '=', 'huddle_groups.group_id')
                        ->join('user_groups as UserGroup', 'UserGroup.group_id', '=', 'Group.id')
                        ->join('users as User', 'UserGroup.user_id', '=', 'User.id')
                        ->select(
                                'User.*', 'huddle_groups.role_id', 'Group.name', 'Group.id as group_id'
                        )->where(array(
                                    'AccountFolder.account_folder_id' => $account_folder_id,
                                    'AccountFolder.site_id' => $site_id
                                ));

        if(!empty($role_id)){
            settype($role_id, "array");
            $query->whereIn('huddle_groups.role_id', $role_id);
        }
        $result = $query->get();
        if($result){
            return $result->toArray();
        } else {
            return [];
        }
    }

    public static function getAllAccountFolders($site_id, $account_id, $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $folder_path = '/root/', $bool = 0, $title, $huddle_sort) {
        $account_id = (int) $account_id;

        if ($huddle_sort == '0') {
            $order_by = 'TRIM(AccountFolder.name) ASC';
        } elseif ($huddle_sort == '1') {
            $order_by = 'created_by_name ASC';
        } elseif ($huddle_sort == '2') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif ($huddle_sort == '3') {
            $order_by = 'AccountFolder.last_edit_date DESC';
        }elseif($huddle_sort == '4' && !empty($title)){
            $order_by = 'AccountFolder.parent_folder_id,TRIM(AccountFolder.name) ASC ';
        }
        $query = AccountFolder
                ::leftJoin('account_folder_users as huddle_users', 'AccountFolder.account_folder_id', '=', 'huddle_users.account_folder_id')
                ->leftJoin('users as User', 'huddle_users.user_id', '=', 'User.id')
                ->leftJoin('account_folder_documents as afd', 'AccountFolder.account_folder_id','=','afd.account_folder_id')
                ->select(
                // 'User.*',
                'AccountFolder.*', 'huddle_users.user_id', 'huddle_users.role_id', 'huddle_users.account_folder_user_id', \DB::raw("(SELECT concat(first_name,' ',last_name) FROM users WHERE id = AccountFolder.created_by) AS created_by_name"), \DB::raw("concat('$folder_path', AccountFolder.name) as SibmePath")
        );
        $query->where('AccountFolder.site_id', $site_id);
        if ($folder_id !== false) {
            if (!$bool) {
            $query->where("AccountFolder.parent_folder_id", $folder_id);
            }
            else
            {
            $query->whereRaw("AccountFolder.account_folder_id != " . $folder_id);  
            }
        } else {
            if (!$bool) {
                $query->whereNull("AccountFolder.parent_folder_id");
            }
        }

        if ($bool) {
            $query->where('account_id', $account_id)
                    ->where('folder_type', 5)
                    ->where('active', 1)
                    ->whereRaw('(AccountFolder.name like "%' . $title . '%" || User.first_name like "%'.$title.'%" || User.last_name like "%'.$title.'%" || afd.title like "%'.$title.'%")')
                    ->orderByRaw($order_by)
                    ->groupBy('AccountFolder.account_folder_id');
        } else {
            $query->where('account_id', $account_id)
                    ->where('folder_type', 5)
                    ->where('active', 1)
                    ->orderByRaw($order_by)
                    ->groupBy('AccountFolder.account_folder_id');
        }

        $results = $query->get();
        if ($results) {
            foreach ($results as $key => $row) {
                $results[$key]['meta_data_value'] = AccountFolderMetaData::getMetaDataValue($row->account_folder_id, $site_id);
            }
            // Missing $array_format block
            return $results;
        } else {
            return FALSE;
        }
    }
    
        public static function getAllAccountUnArchiveFolders($site_id, $account_id, $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $folder_path = '/root/', $bool = 0, $title, $huddle_sort,$switch,$start_date,$end_date) {
        $account_id = (int) $account_id;

        if ($huddle_sort == '0') {
            $order_by = 'TRIM(AccountFolder.name) ASC';
        } elseif ($huddle_sort == '1') {
            $order_by = 'created_by_name ASC';
        } elseif ($huddle_sort == '2') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif ($huddle_sort == '3') {
            $order_by = 'AccountFolder.last_edit_date DESC';
        }
        $query = AccountFolder
                ::leftJoin('account_folder_users as huddle_users', 'AccountFolder.account_folder_id', '=', 'huddle_users.account_folder_id')
                ->leftJoin('users as User', 'huddle_users.user_id', '=', 'User.id')
                ->select(
                // 'User.*',
                'AccountFolder.*', 'huddle_users.user_id', 'huddle_users.role_id', 'huddle_users.account_folder_user_id', \DB::raw("(SELECT concat(first_name,' ',last_name) FROM users WHERE id = AccountFolder.created_by) AS created_by_name"), \DB::raw("concat('$folder_path', AccountFolder.name) as SibmePath")
        );
        $query->where('AccountFolder.site_id', $site_id);
        if ($folder_id !== false) {
            if (!$bool) {
            $query->where("AccountFolder.parent_folder_id", $folder_id);
            }
            else
            {
            $query->whereRaw("AccountFolder.account_folder_id != " . $folder_id);  
            }
        } else {
            if (!$bool) {
               // $query->whereNull("AccountFolder.parent_folder_id");
            }
        }
        
        if($switch == 'archived')
        {
            $active = 0 ;
            $archive = 1;
        }
        
        else
        {
           $active = 1 ;
           $archive = 0; 
        }

        if ($bool) {
            $query->where('account_id', $account_id)
                    ->where('folder_type', 5)
                    ->where('active', $active)
                    ->where('archive', $archive)
                    ->whereRaw('AccountFolder.name like "%' . $title . '%"')
                    ->orderByRaw($order_by)
                    ->groupBy('AccountFolder.account_folder_id');
        } else {
            $query->where('account_id', $account_id)
                    ->where('folder_type', 5)
                    ->where('active', $active)
                    ->where('archive', $archive)
                    ->orderByRaw($order_by)
                    ->groupBy('AccountFolder.account_folder_id');
        }
        
        if(!empty($start_date) && !empty($end_date))
        {
           $query->whereRaw("AccountFolder.created_date >= '" . $start_date . "'" )->whereRaw("AccountFolder.created_date <= '" . $end_date . "'" ); 
        }

        $results = $query->get();
        if ($results) {
            foreach ($results as $key => $row) {
                $results[$key]['meta_data_value'] = AccountFolderMetaData::getMetaDataValue($row->account_folder_id, $site_id);
            }
            // Missing $array_format block
            return $results;
        } else {
            return FALSE;
        }
    }

    public static function getAllAccountHuddles($site_id, $account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $bool = 0, $limit = 10, $page = 1, $counter = false, $title = '', $huddle_sort = '0', $huddle_type = '0', $role_id = null, $huddle_id = null, $all_huddles = 0, $without_assessment = 0, $current_huddle_id = 0,$goal_evidence=0) {
        $account_id = (int) $account_id;

        if(!empty($title))
        {
            $order_by = DB::connection()->getPdo()->quote("AccountFolder.name <> '" . $title . "' , TRIM(AccountFolder.name) ASC");
        }else
        {

            if ($huddle_sort == '0') {
                $order_by = 'TRIM(AccountFolder.name) ASC';
            } elseif ($huddle_sort == '1') {
                $order_by = 'created_by_name ASC';
            } elseif ($huddle_sort == '2') {
                $order_by = 'AccountFolder.created_date DESC';
            } elseif ($huddle_sort == '3') {
                $order_by = 'AccountFolder.last_edit_date DESC';
            }
        }

        


        if ($huddle_type == '0') {
            $huddle_array = $without_assessment ? [1, 2] : [1, 2, 3];
        } elseif ($huddle_type == '1') {
            $huddle_array = [1];
        } elseif ($huddle_type == '2') {
            $huddle_array = [2];
        } elseif ($huddle_type == '3') {
            $huddle_array = [3];
        }




        $query = AccountFolder
                ::leftJoin('account_folder_users as huddle_users', 'AccountFolder.account_folder_id', '=', 'huddle_users.account_folder_id')
                ->leftJoin('users as User', 'huddle_users.user_id', '=', 'User.id')
                ->leftJoin('account_folder_documents as afd', 'AccountFolder.account_folder_id','=','afd.account_folder_id')
                ->leftJoin('account_folders_meta_data as afmd', 'AccountFolder.account_folder_id', '=', 'afmd.account_folder_id')
                ->select(
//                            'User.*',
                'AccountFolder.*', 'huddle_users.user_id', 'huddle_users.role_id', 'huddle_users.account_folder_user_id', \DB::raw("(SELECT concat(first_name,' ',last_name) FROM users WHERE id = AccountFolder.created_by limit 1) AS created_by_name"), self::qry_v_total($site_id), self::qry_doc_total($site_id), self::qryfolderType($site_id)
        );
        $query->where('AccountFolder.site_id', $site_id);
        if ($account_folder_ids !== false && is_array($account_folder_ids) && !($accountFolderGroupsIds !== false && is_array($accountFolderGroupsIds))) {
            $query->whereIn("AccountFolder.account_folder_id", $account_folder_ids);
        }
        if ($accountFolderGroupsIds !== false && is_array($accountFolderGroupsIds)) {
            if ($account_folder_ids !== false && is_array($account_folder_ids)) {
                $accountFolderGroupsIds = implode(',', $accountFolderGroupsIds);
                $account_folder_ids = implode(',', $account_folder_ids);

                $query->whereRaw("( (AccountFolder.account_folder_id IN ( " . $account_folder_ids . ")) OR (AccountFolder.account_folder_id IN ( " . $accountFolderGroupsIds . ")) ) ");
            } else {
                $query->whereIn("AccountFolder.account_folder_id", $accountFolderGroupsIds);
            }
        }

        if (!$account_folder_ids && !$accountFolderGroupsIds) {
            $query->where("AccountFolder.account_folder_id", '0');
        }

        if ($folder_id !== false) {
            if(!$bool /*&& ($huddle_type != '1' && $huddle_type != '2' && $huddle_type != '3' )*/)
            {
            $query->where("AccountFolder.parent_folder_id", $folder_id);
            }
        } else {
            if (!$bool && ($huddle_type != '1' && $huddle_type != '2' && $huddle_type != '3' ) && !$all_huddles && !$goal_evidence) {
                $query->whereNull("AccountFolder.parent_folder_id");
            }
        }
        

        if ($bool) {
            $query->where('AccountFolder.account_id', $account_id)
                    ->where('AccountFolder.folder_type', 1)
                    ->where('AccountFolder.active', 1)
                    ->where('afmd.meta_data_name', 'folder_type')
                    ->whereIn('afmd.meta_data_value', $huddle_array)
                    ->whereRaw('(AccountFolder.name like "%' . $title . '%" OR User.first_name like "%'.$title.'%" OR User.last_name like "%'.$title.'%" OR afd.title like "%'.$title.'%")')
                    //->whereRaw('(AccountFolder.name like "%' . $title . '%"  OR afd.title like "%'.$title.'%")')
                    ->orderByRaw($order_by)
                    ->groupBy('AccountFolder.account_folder_id');
            ;
        } else {
            $query->where('account_id', $account_id)
                    ->where('folder_type', 1)
                    ->where('active', 1)
                    ->where('afmd.meta_data_name', 'folder_type')
                    ->whereIn('afmd.meta_data_value', $huddle_array)
                    ->orderByRaw($order_by)
                    ->groupBy('AccountFolder.account_folder_id');
        }

        if(!empty($role_id)&& $role_id == 210)//Assessee
        {
            $query->where("AccountFolder.is_published",1);
        }

        if(!empty($huddle_id)){
            $query->where("AccountFolder.account_folder_id", $huddle_id);
        }

        if($current_huddle_id)
        {
            $query->where("AccountFolder.account_folder_id", '!=', $current_huddle_id);
        }

        $from_goals = app('Illuminate\Http\Request')->get('from_goals');
        $user_id = app('Illuminate\Http\Request')->get('user_id');
        if($from_goals == 1 && !empty($user_id))
        {
            //this will exclude all huddles where current user is viewer(this will also resolve huddle count issue)
            $query->whereRaw("
            NOT Exists (select 1 from account_folder_users as afu where afu.user_id = $user_id and afu.site_id = $site_id and afu.role_id = 220 and afu.account_folder_id = AccountFolder.account_folder_id)
            AND NOT Exists (select 1 from account_folder_groups afg join user_groups ug on afg.group_id = ug.group_id and ug.user_id = $user_id where afg.site_id = $site_id and afg.role_id = 220 and afg.account_folder_id = AccountFolder.account_folder_id )
            ");
        }

        if ($counter) {
            return $query->get()->count();
        }

        if($all_huddles && $limit == 0)
        {
            $results = $query->get();
            $counts = $results->count();
            $results = self::addMetaDataValue($results, $site_id);
            return ["huddles" => $results, "counts" => $counts];
        }
        else
        {
            $tmp_results = $query->get();
            $counts = $tmp_results->count();
            $results = $tmp_results->forPage($page, $limit);
            if($all_huddles)
            {
                $results = self::addMetaDataValue($results, $site_id);
                return ["huddles" => $results, "counts" => $counts];
            }
        }

        if ($results) {
            $results = self::addMetaDataValue($results, $site_id);
            // Missing $array_format block
            return $results;
        } else {
            return FALSE;
        }
    }
    public static function addMetaDataValue($results, $site_id)
    {
        foreach ($results as $key => $row) {
            $results[$key]['meta_data_value'] = AccountFolderMetaData::getMetaDataValue($row->account_folder_id, $site_id);
        }
        return $results;
    }
    
    
        public static function getAllAccountUnArchiveHuddles($site_id, $account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $bool = 0, $limit = 10, $page = 1, $counter = false, $title = '', $huddle_sort = '0', $huddle_type = '0', $role_id = null,$switch,$start_date,$end_date) {
        $account_id = (int) $account_id;

        if ($huddle_sort == '0') {
            $order_by = 'AccountFolder.name ASC';
        } elseif ($huddle_sort == '1') {
            $order_by = 'created_by_name ASC';
        } elseif ($huddle_sort == '2') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif ($huddle_sort == '3') {
            $order_by = 'AccountFolder.last_edit_date DESC';
        }


        if ($huddle_type == '0') {
            $huddle_array = [1, 2, 3];
        } elseif ($huddle_type == '1') {
            $huddle_array = [1];
        } elseif ($huddle_type == '2') {
            $huddle_array = [2];
        } elseif ($huddle_type == '3') {
            $huddle_array = [3];
        }




        $query = AccountFolder
                ::leftJoin('account_folder_users as huddle_users', 'AccountFolder.account_folder_id', '=', 'huddle_users.account_folder_id')
                ->leftJoin('users as User', 'huddle_users.user_id', '=', 'User.id')
                ->leftJoin('account_folders_meta_data as afmd', 'AccountFolder.account_folder_id', '=', 'afmd.account_folder_id')
                ->select(
//                            'User.*',
                'AccountFolder.*', 'huddle_users.user_id', 'huddle_users.role_id', 'huddle_users.account_folder_user_id', \DB::raw("(SELECT concat(first_name,' ',last_name) FROM users WHERE id = AccountFolder.created_by) AS created_by_name"), self::qry_v_total($site_id), self::qry_doc_total($site_id), self::qryfolderType($site_id)
        );
        $query->where('AccountFolder.site_id', $site_id);
       
     

       

        if ($folder_id !== false) {
            if(!$bool && ($huddle_type != '1' && $huddle_type != '2' && $huddle_type != '3' ))
            {
            $query->where("AccountFolder.parent_folder_id", $folder_id);
            }
        } else {
            if (!$bool && ($huddle_type != '1' && $huddle_type != '2' && $huddle_type != '3' )) {
               if($counter)
               {
                    $query->whereNull("AccountFolder.parent_folder_id");
               }
            }
        }
        
        if($switch == 'archived')
        {
            $active = 0 ;
            $archive = 1;
        }
        
        else
        {
           $active = 1 ;
           $archive = 0; 
        }

        if ($bool) {
            $query->where('AccountFolder.account_id', $account_id)
                    ->where('AccountFolder.folder_type', 1)
                    ->where('AccountFolder.active', $active)
                    ->where('AccountFolder.archive', $archive)
                    ->where('afmd.meta_data_name', 'folder_type')
                    ->whereIn('afmd.meta_data_value', $huddle_array)
                    ->whereRaw('AccountFolder.name like "%' . $title . '%"')
                    ->orderByRaw($order_by)
                    ->groupBy('AccountFolder.account_folder_id');
            ;
        } else {
            $query->where('account_id', $account_id)
                    ->where('folder_type', 1)
                    ->where('active', $active)
                    ->where('archive', $archive)
                    ->where('afmd.meta_data_name', 'folder_type')
                    ->whereIn('afmd.meta_data_value', $huddle_array)
                    ->orderByRaw($order_by)
                    ->groupBy('AccountFolder.account_folder_id');
        }
        
        if(!empty($start_date) && !empty($end_date))
        {
           $query->whereRaw("AccountFolder.created_date >= '" . $start_date . "'" )->whereRaw("AccountFolder.created_date <= '" . $end_date . "'" ); 
        }

        if(!empty($role_id)&& $role_id == 210)//Assessee
        {
            $query->where("AccountFolder.is_published",1);
        }

        if ($counter) {
            return $query->get()->count();
        }
        
        if ($folder_id !== false) {
           $results = $query->get(); 
        }
        else
        {
            $results = $query->get()->forPage($page, $limit);
        }
        

        if ($results) {
            foreach ($results as $key => $row) {
                $results[$key]['meta_data_value'] = AccountFolderMetaData::getMetaDataValue($row->account_folder_id, $site_id);
            }
            // Missing $array_format block
            return $results;
        } else {
            return FALSE;
        }
    }
    
    
        public static function getAllAssessmentTrackerHuddles($site_id, $account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false, $bool = 0, $limit = 10, $page = 1, $counter = false, $title = '', $huddle_sort = '0', $huddle_type = '0', $role_id = null) {
        $account_id = (int) $account_id;

        if ($huddle_sort == '0') {
            $order_by = 'AccountFolder.name ASC';
        } elseif ($huddle_sort == '1') {
            $order_by = 'AccountFolder.published_date DESC';
        } elseif ($huddle_sort == '2') {
            $order_by = 'due_date DESC';
        } elseif ($huddle_sort == '3') {
            $order_by = 'AccountFolder.published_date DESC';
        }


        if ($huddle_type == '0') {
            $huddle_array = [1, 2, 3];
        } elseif ($huddle_type == '1') {
            $huddle_array = [1];
        } elseif ($huddle_type == '2') {
            $huddle_array = [2];
        } elseif ($huddle_type == '3') {
            $huddle_array = [3];
        }




        $query = AccountFolder
                ::leftJoin('account_folder_users as huddle_users', 'AccountFolder.account_folder_id', '=', 'huddle_users.account_folder_id')
                ->leftJoin('users as User', 'huddle_users.user_id', '=', 'User.id')
                ->leftJoin('account_folders_meta_data as afmd', 'AccountFolder.account_folder_id', '=', 'afmd.account_folder_id')
                ->select(
//                            'User.*',
                'AccountFolder.*', 'huddle_users.user_id', 'huddle_users.role_id', 'huddle_users.account_folder_user_id', \DB::raw("STR_TO_DATE((SELECT meta_data_value FROM `account_folders_meta_data` WHERE account_folder_id = AccountFolder.account_folder_id AND meta_data_name = 'submission_deadline_date' LIMIT 1 ) ,  '%m-%d-%Y' ) AS due_date") , \DB::raw("(SELECT concat(first_name,' ',last_name) FROM users WHERE id = AccountFolder.created_by) AS created_by_name"), self::qry_v_total($site_id), self::qry_doc_total($site_id), self::qryfolderType($site_id)
        );
        $query->where('AccountFolder.site_id', $site_id);
        
        if($role_id == 115 || $huddle_sort == '3' )
        {
        
        if ($account_folder_ids !== false && is_array($account_folder_ids) && !($accountFolderGroupsIds !== false && is_array($accountFolderGroupsIds))) {
            $query->whereIn("AccountFolder.account_folder_id", $account_folder_ids);
        }
        if ($accountFolderGroupsIds !== false && is_array($accountFolderGroupsIds)) {
            if ($account_folder_ids !== false && is_array($account_folder_ids)) {
                $accountFolderGroupsIds = implode(',', $accountFolderGroupsIds);
                $account_folder_ids = implode(',', $account_folder_ids);

                $query->whereRaw("( (AccountFolder.account_folder_id IN ( " . $account_folder_ids . ")) OR (AccountFolder.account_folder_id IN ( " . $accountFolderGroupsIds . ")) ) ");
            } else {
                $query->whereIn("AccountFolder.account_folder_id", $accountFolderGroupsIds);
            }
        }

        if (!$account_folder_ids && !$accountFolderGroupsIds) {
            $query->where("AccountFolder.account_folder_id", '0');
        }
        
        
        }

       /* if ($folder_id !== false) {
            $query->where("AccountFolder.parent_folder_id", $folder_id);
        } else {
            if (!$bool) {
                $query->whereNull("AccountFolder.parent_folder_id");
            }
        } */

        if ($bool) {
            $query->where('AccountFolder.account_id', $account_id)
                    ->where('AccountFolder.folder_type', 1)
                    ->where('AccountFolder.active', 1)
                    ->where('afmd.meta_data_name', 'folder_type')
                    ->whereIn('afmd.meta_data_value', $huddle_array)
                    ->whereRaw('((AccountFolder.name like "%' . $title . '%") OR (CONCAT(User.first_name," ",User.last_name) like "%' . $title . '%") )')
                    ->where('huddle_users.role_id',200)
                    ->orderByRaw($order_by)
                    ->orderBy('AccountFolder.created_date','DESC')
                    ->groupBy('AccountFolder.account_folder_id');
        } else {
            $query->where('account_id', $account_id)
                    ->where('folder_type', 1)
                    ->where('active', 1)
                    ->where('afmd.meta_data_name', 'folder_type')
                    ->whereIn('afmd.meta_data_value', $huddle_array)
                    ->orderByRaw($order_by)
                    ->orderBy('AccountFolder.created_date','DESC')
                    ->groupBy('AccountFolder.account_folder_id');
        }

        if(!empty($role_id)&& $role_id == 210)//Assessee
        {
            $query->where("AccountFolder.is_published",1);
        }

        if ($counter) {
            return $query->get()->count();
        }

        $results = $query->get()->forPage($page, $limit);

        if ($results) {
            foreach ($results as $key => $row) {
                $results[$key]['meta_data_value'] = AccountFolderMetaData::getMetaDataValue($row->account_folder_id, $site_id);
            }
            // Missing $array_format block
            return $results;
        } else {
            return FALSE;
        }
    }

    public static function qry_v_total($site_id) {
        return \DB::raw("(SELECT
                                COUNT(*) AS `count`
                            FROM
                                `documents` AS `Document`
                                INNER JOIN `users` AS `User`
                                ON (
                                `Document`.`created_by` = `User`.`id`
                                )
                                INNER JOIN `account_folder_documents` AS `afd`
                                ON (
                                `Document`.`id` = `afd`.`document_id`
                                )
                                INNER JOIN `documents` AS `doc`
                                ON (`afd`.`document_id` = `doc`.`id`)
                            WHERE `afd`.`account_folder_id` = `AccountFolder`.`account_folder_id`
                                AND `afd`.`assessment_sample`='0'                                 
                                AND `Document`.`doc_type` = 1
                                AND `Document`.`site_id` = $site_id
                                AND `Document`.`active` = '1' )  AS v_total");
    }

    public static function qry_doc_total($site_id) {
        return \DB::raw("(SELECT
                                COUNT(*) AS `count`
                            FROM
                                `documents` AS `Document`
                                INNER JOIN `users` AS `User`
                                ON (
                                    `Document`.`created_by` = `User`.`id`
                                )
                                INNER JOIN `account_folder_documents` AS `afd`
                                ON (
                                    `Document`.`id` = `afd`.`document_id`
                                )
                                INNER JOIN `documents` AS `doc`
                                ON (`afd`.`document_id` = `doc`.`id`)
                            WHERE `afd`.`account_folder_id` = `AccountFolder`.`account_folder_id`
                                AND `afd`.`assessment_sample`='0' 
                                AND `Document`.`doc_type` IN(2,5)
                                AND `Document`.`site_id` = $site_id
                                AND `Document`.`active` = '1' )  AS doc_total");
    }

    public static function qryfolderType($site_id) {
        return \DB::raw("(SELECT
                            CASE
                                WHEN meta_data_value  = '2' THEN 'coaching'
                                WHEN meta_data_value = '3' THEN 'assessment'
                                ELSE NULL
                            END
                            FROM
                            account_folders_meta_data
                            WHERE account_folder_id = AccountFolder.`account_folder_id`
                            AND meta_data_name = 'folder_type'
                            AND site_id = $site_id
                            AND (meta_data_value = 3 OR meta_data_value = 2 ) limit 1) AS folderType");
    }

    public static function count_coaching_colab_huddles_in_folder($account_folder_id, $user_id, $bool, $site_id) {
        $account_id = AccountFolder::where("account_folder_id", $account_folder_id)->select("account_id")->value("account_id");

        $huddle_ids = AccountFolder::where("parent_folder_id", $account_folder_id)
                        ->where("folder_type", 1)
                        ->where("active", 1)
                        ->where("account_id", $account_id)
                        ->where("site_id", $site_id)
                        ->whereRaw("((account_folder_id in (select account_folder_id from account_folder_users where user_id=$user_id) ) OR (account_folder_id IN (select  account_folder_id from account_folder_groups afg join user_groups ug on afg.group_id = ug.group_id where ug.user_id = $user_id  ) ) )")
                        ->select("account_folder_id")
                        ->pluck("account_folder_id")->toArray();

        $db2 = new AccountFolderMetaData();
        $count_coaching_huddles = 0;
        $count_colab_huddles = 0;
        $count_eval_huddles = 0;

        foreach ($huddle_ids as $huddle_id) {
            if ($bool == 2) {

                $coaching_huddles = AccountFolderMetaData::where("meta_data_value", 2)
                        ->where("meta_data_name", "folder_type")
                        ->where("site_id", $site_id)
                        ->where("account_folder_id", $huddle_id)
                        ->exists();

                if ($coaching_huddles) {
                    $count_coaching_huddles++;
                }
            } elseif ($bool == 1) {

                $coaching_assessment_huddles = AccountFolderMetaData::whereIn("meta_data_value", [2, 3])
                        ->where("meta_data_name", "folder_type")
                        ->where("site_id", $site_id)
                        ->where("account_folder_id", $huddle_id)
                        ->exists();
                if (!$coaching_assessment_huddles) {
                    $count_colab_huddles++;
                }
            } elseif ($bool == 3) {

                $assessment_huddles = AccountFolderMetaData::where("meta_data_value", 3)
                        ->where("meta_data_name", "folder_type")
                        ->where("site_id", $site_id)
                        ->where("account_folder_id", $huddle_id)
                        ->exists();

                if ($assessment_huddles) {
                    $count_eval_huddles++;
                }
            }
        }
        if ($bool == 2) {
            return $count_coaching_huddles;
        } elseif ($bool == 3) {
            return $count_eval_huddles;
        } elseif ($bool == 1) {
            return $count_colab_huddles;
        }
    }

    public static function count_folders_in_folder($folder_id, $user_id, $account_id, $site_id) {
        $folders = AccountFolder::where("parent_folder_id", $folder_id)
                ->where("folder_type", 5)
                ->where("active", 1)
                ->where("site_id", $site_id)
                ->get();

        $count_folder = 0;
        foreach ($folders as $folder) {
            if (self::folder_has_huddle_participating($folder->account_folder_id, $user_id, $account_id, $site_id) || $folder->created_by == $user_id) {
                $count_folder++;
            }
        }
        return $count_folder;
    }

    public static function folder_has_huddle_participating($folder_id, $user_id, $account_id, $site_id) {

        $folder_ids = self::get_folder_childs($folder_id, $site_id);
        $flag = false;
        foreach ($folder_ids as $fold_id) {
            $has_huddle = self::hasAccountHuddlesInFolder($account_id, $user_id, $fold_id, $site_id);
            if ($has_huddle == true) {
                $flag = true;
            }
        }

        if ($flag) {
            return true;
        } else {
            return false;
        }
    }

    public static function hasAccountHuddlesInFolder($account_id, $user_id, $folder_id, $site_id) {
        $account_id = (int) $account_id;

        $query = AccountFolder::where("AccountFolder.account_id", $account_id)
                ->where("AccountFolder.folder_type", 1)
                ->where("AccountFolder.active", 1)
                ->where("site_id", $site_id)
                ->whereRaw("( ( account_folder_id IN( select  account_folder_id from account_folder_users where user_id=$user_id ) ) OR (account_folder_id IN (select  account_folder_id from account_folder_groups afg join user_groups ug on afg.group_id = ug.group_id where ug.user_id = $user_id  ) ) ) ");

        if ($folder_id != false) {
            $query->where("AccountFolder.parent_folder_id", $folder_id);
        } else {
            $query->whereNull("AccountFolder.parent_folder_id");
        }

        return $query->exists();
    }
    
    
    public static function count_folders_in_folder_archive($folder_id, $user_id, $account_id, $site_id , $switch = 'unarchive') {
        if($switch == 'unarchive')
        {
            $active = 1;
            $archive = 0;
        }
        else 
        {
            $active = 0;
            $archive = 1;
        }
        
        $folders = AccountFolder::where("parent_folder_id", $folder_id)
                ->where("folder_type", 5)
                ->where("active", $active)
                ->where("archive" ,$archive)
                ->where("site_id", $site_id)
                ->get();

        $count_folder = 0;
        foreach ($folders as $folder) {
            if (self::folder_has_huddle_participating_archive($folder->account_folder_id, $user_id, $account_id, $site_id, $switch) || $folder->created_by == $user_id) {
                $count_folder++;
            }
        }
        return $count_folder;
    }

    public static function folder_has_huddle_participating_archive($folder_id, $user_id, $account_id, $site_id , $switch = 'unarchive') {

        $folder_ids = self::get_folder_childs($folder_id, $site_id);
        $flag = false;
        foreach ($folder_ids as $fold_id) {
            $has_huddle = self::hasAccountHuddlesInFolder_archive($account_id, $user_id, $fold_id, $site_id,$switch);
            if ($has_huddle == true) {
                $flag = true;
            }
        }

        if ($flag) {
            return true;
        } else {
            return false;
        }
    }

    public static function hasAccountHuddlesInFolder_archive($account_id, $user_id, $folder_id, $site_id ,$switch = 'unarchive') {
        if($switch == 'unarchive')
        {
            $active = 1;
            $archive = 0;
        }
        else 
        {
            $active = 0;
            $archive = 1;
        }
        $account_id = (int) $account_id;

        $query = AccountFolder::where("AccountFolder.account_id", $account_id)
                ->where("AccountFolder.folder_type", 1)
                ->where("AccountFolder.active", $active)
                ->where("AccountFolder.archive", $archive)
                ->where("site_id", $site_id)
                ->whereRaw("( ( account_folder_id IN( select  account_folder_id from account_folder_users where user_id=$user_id ) ) OR (account_folder_id IN (select  account_folder_id from account_folder_groups afg join user_groups ug on afg.group_id = ug.group_id where ug.user_id = $user_id  ) ) ) ");

        if ($folder_id != false) {
            $query->where("AccountFolder.parent_folder_id", $folder_id);
        } else {
            $query->whereNull("AccountFolder.parent_folder_id");
        }

        return $query->exists();
    }
    
    

    public static function getAllHuddlesDashboard($site_id, $account_id, $sort = '', $array_format = false, $account_folder_ids = false, $accountFolderGroupsIds = false, $folder_id = false) {

        $account_id = (int) $account_id;

        if (!empty($sort) && $sort == 'name') {
            $order_by = 'AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'date') {
            $order_by = 'AccountFolder.created_date DESC';
        } elseif (!empty($sort) && $sort == 'coaching') {
            $order_by = 'folderType DESC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'collaboration') {
            $order_by = 'folderType ASC,AccountFolder.name ASC';
        } elseif (!empty($sort) && $sort == 'folder_type') {
            $order_by = 'folder_type DESC';
        } else {
            $order_by = 'AccountFolder.created_date DESC';
        }

        $where_in = '(';

        if ($account_folder_ids != false) {
            $where_in .= " AccountFolder.account_folder_id IN($account_folder_ids) ";
        }


        if ($accountFolderGroupsIds != false) {
            if ($account_folder_ids != false) {
                $where_in .= " OR ";
            }
            $where_in .= " AccountFolder.account_folder_id IN($accountFolderGroupsIds) ";
        }
        if ($where_in != '(')
            $where_in .= ' ) ';
        else
            $where_in = '(AccountFolder.account_folder_id IN(0))';

        if ($folder_id != false) {
            $where_in .= " AND AccountFolder.parent_folder_id = '$folder_id' ";
        } else {
            $where_in .= " AND AccountFolder.parent_folder_id IS NULL ";
        }

        $result = app('db')->select("SELECT
                      `User`.*,
                      (SELECT
                        'coaching'
                      FROM
                        account_folders_meta_data
                      WHERE account_folder_id = AccountFolder.`account_folder_id`
                        AND meta_data_name = 'folder_type'
                        AND meta_data_value = 2) AS folderType,
                      `AccountFolder`.*,
                      `huddle_users`.`user_id`,
                      `huddle_users`.`role_id`,
                      `huddle_users`.`account_folder_user_id`
                    FROM
                      `account_folders` AS `AccountFolder`
                      LEFT JOIN `account_folder_users` AS `huddle_users`
                        ON (
                          `AccountFolder`.`account_folder_id` = `huddle_users`.`account_folder_id`
                        )
                      LEFT JOIN `users` AS `User`
                        ON (
                          `huddle_users`.`user_id` = `User`.`id`
                        )
                    WHERE `AccountFolder`.`site_id` = $site_id
                      AND `AccountFolder`.`account_id` = $account_id
                      AND `AccountFolder`.`folder_type` IN (1, 5)
                      AND `AccountFolder`.`active` = 1
                      AND (
                        $where_in
                      )
                      AND `AccountFolder`.`parent_folder_id` IS NULL
                    GROUP BY `AccountFolder`.`account_folder_id`
                    ORDER BY $order_by ");

        $result = AccountFolder::convert_array($result);

        if (is_array($result) && count($result) > 0) {

            $output1 = array();
            for ($i = 0; $i < count($result); $i++) {
                $item = $result[$i];
                $res = app('db')->select('select meta_data_value from account_folders_meta_data where account_folder_id = ' . $item['account_folder_id'] . ' AND meta_data_name = "folder_type" and site_id ="' . $site_id . '"');

                $result[$i]['meta_data_value'] = isset($res[0]['account_folders_meta_data']['meta_data_value']) ? $res[0]['account_folders_meta_data']['meta_data_value'] : '';
            }
            //echo '<pre>';
            $output = array();
            //$result = array();
            for ($i = 0; $i < count($result); $i++) {
                if (!empty($sort) && $sort == 'coaching') {
                    if ($result[$i][0]['folderType'] == 'coaching') {
                        $output[] = $result[$i];
                    }
                } elseif (!empty($sort) && $sort == 'collaboration') {
                    if ($result[$i][0]['folderType'] == '' && $result[$i]['folder_type'] != 5) {
                        $output[] = $result[$i];
                    }
                } else {
                    $output[] = $result[$i];
                }
                //$output[] = $item;
            }
            $result = $output;
            //print_r($result);
            //die;
            if ($array_format) {
                $output = array();
                for ($i = 0; $i < count($result); $i++) {
                    $item = $result[$i];
                    $created_at = new DateTime($item['created_date']);
                    $updated_at = new DateTime($item['last_edit_date']);
                    $item['created_at'] = $created_at->format(DateTime::W3C);
                    $item['updated_at'] = $updated_at->format(DateTime::W3C);
                    $item['description'] = $item['desc'];
                    $output[] = $item;
                }
                $result = $output;
            }

            return $result;
        } else {
            return FALSE;
        }
    }

    

    public static function getHuddleName($account_folder_id, $site_id) {
        $result = AccountFolder::where('account_folder_id', $account_folder_id)
                        ->where('site_id', $site_id)
                        ->select(['name'])
                        ->get()->toArray();
        $account_folder_name = '';
        if ($result && count($result) > 0) {
            $account_folder_name = $result[0]['name'];
        }
        return $account_folder_name;
    }

    public static function convert_array($data) {
        $data = collect($data)->map(function($x) {
                    return (array) $x;
                })->toArray();
        return $data;
    }

    public static function get($account_folder_id) {
        $result = AccountFolder::where('account_folder_id', (int) $account_folder_id)->first();
        if ($result && count($result) > 0) {
            return $result;
        } else {
            return FALSE;
        }
    }

    public static function getHuddleUsersIncludingGroupsActive($site_id, $huddle_id, $user_id, $user_role_id, $with_group_users=false, $for_role_ids=[]){
        $huddle_users = AccountFolder::getHuddleUsers($huddle_id, $site_id, $for_role_ids);
        if($with_group_users){
            $huddle_groups = AccountFolder::getHuddleGroupsUsers($huddle_id, $site_id, $for_role_ids);
        }else{
            $huddle_groups =  AccountFolder::getHuddleGroups($huddle_id);
        }
        $participants_arranged = array();
        $participants = array();
        $huddle_name_type = null;
        $huddle_name = null;

        if (!empty($huddle_groups)) {
            if($with_group_users){
                foreach ($huddle_groups as $huddle_group) {    
                    $participants_arranged[] = array(
                        'user_id' => $huddle_group['id'],
                        'role_id' => $huddle_group['role_id'],
                        'user_name' => $huddle_group['first_name'] . ' ' . $huddle_group['last_name'],
                        'image' => $huddle_group['image'],
                        'user_email' => $huddle_group['email'],
                        'group_id' => $huddle_group['group_id'],
                        'is_submitted' => '1',
                        'last_submission' => null,
                    );
                }
            } else {
                foreach($huddle_groups as $row){
                    $participants_arranged[] = array(
                        'user_id' => $row['user_id'],
                        'role_id' => $row['role_id'],
                        'user_name' => $row['name'],
                        'image' =>'groups',
                        'user_email' => 'N/A',
                        'group_id' => $row['group_id'],
                        'is_submitted' => null,
                        'last_submission' => null,
                    );
                }
            }
        }

        if (!empty($huddle_users)) {

            foreach ($huddle_users as $huddle_user) {
                if($huddle_user['is_active'] ==0){
                    continue;
                }
                $participants_arranged[] = array(
                    'user_id' => $huddle_user['user_id'],
                    'role_id' => $huddle_user['role_id'],
                    'user_name' => $huddle_user['first_name'] . ' ' . $huddle_user['last_name'],
                    'image' => $huddle_user['image'],
                    'user_email' => $huddle_user['email'],
                    'group_id' => null,
                    'is_submitted' => $huddle_user['is_submitted'],
                    'last_submission' => $huddle_user['last_submission'],
                );
            }

            $folder_type = AccountFolder::getAccountFolderType($huddle_id);
            if ($folder_type == 1) {
                $folder_type = "collaboration";
            } else if ($folder_type == 2) {
                $folder_type = "coaching";
            } else {
                $folder_type = "assessment";
            }    

            $huddle_name_type = TranslationsManager::get_translation('huddle_list_collaboration', 'Api/huddle_list', $site_id, $user_id);
            $huddle_name = 'collaboration';
            if($folder_type == 'assessment')
            {
                $role_200 = 'assessor';
                $role_210 = 'assessed_participants';
                $role_220 = '';
                $huddle_name_type = TranslationsManager::get_translation('huddle_list_assessment', 'Api/huddle_list', $site_id, $user_id);
                $huddle_name = 'assessment';
            }
            elseif($folder_type == 'coaching'){
                $role_200 = 'coach';
                $role_210 = 'coachee';
                $role_220 = '';
                $huddle_name_type = TranslationsManager::get_translation('coaching_list_coaching', 'Api/huddle_list', $site_id, $user_id);
                $huddle_name = 'coaching';
            } else {
                $role_200 = 'collaboration_participants';
                $role_210 = 'collaboration_participants';
                $role_220 = 'collaboration_participants';
                $huddle_name_type = TranslationsManager::get_translation('huddle_list_collaboration', 'Api/huddle_list', $site_id, $user_id);
                $huddle_name = 'collaboration';
            }
            
            foreach ($participants_arranged as $row) {
                if ($row['role_id'] == '200') {
                    $row['role_name'] = TranslationsManager::get_translation('gen_admin', 'Api/video_details', $site_id, $user_id);
                    if ($folder_type == 'assessment') {
                        //$row['role_name'] = "Assessor";
                        $row['role_name'] = TranslationsManager::get_translation('artifact_assessor', 'Api/huddle_list', $site_id, $user_id);
                    }
                    if ($folder_type == 'coaching') {
                       // $row['role_name'] = "Coach";
                        $row['role_name'] = TranslationsManager::get_translation('artifact_coach', 'Api/huddle_list', $site_id, $user_id);
                    }
                    $row['role'] = $role_200;
                } elseif ($row['role_id'] == '210') {
                    $row['role_name'] = TranslationsManager::get_translation('gen_member', 'Api/video_details', $site_id, $user_id);
                    if ($folder_type == 'assessment') {
                       // $row['role_name'] = "Assessee";
                        $row['role_name'] = TranslationsManager::get_translation('artifact_assessee', 'Api/huddle_list', $site_id, $user_id);
                    }
                    if ($folder_type == 'coaching') {
                        //$row['role_name'] = "Coachee";
                        $row['role_name'] = TranslationsManager::get_translation('artifact_coachee', 'Api/huddle_list', $site_id, $user_id);
                    }
                    $row['role'] = $role_210;
                } else {
                    $row['role_name'] = TranslationsManager::get_translation('gen_viewer', 'Api/video_details', $site_id, $user_id);
                    if ($folder_type == 'coaching') {
                        $row['role_name'] = TranslationsManager::get_translation('artifact_coachee', 'Api/huddle_list', $site_id, $user_id);
                    }
                    $row['role'] = $role_220;
                }
                if(!($folder_type == "assessment" && $user_role_id == "210" && $row['role_id'] == "210" && $row["user_id"] != $user_id))
                {
                    $participants[] = $row;
                }
            }
    
        }
       
        return ['participants'=>$participants, 'huddle_name_type'=>$huddle_name_type, 'huddle_name'=>$huddle_name];
    }

    public static function getHuddleUsersIncludingGroups($site_id, $huddle_id, $user_id, $user_role_id, $with_group_users=false, $for_role_ids=[], $page = false, $search = null,$limit=false){


        $huddle_users = AccountFolder::getHuddleUsers($huddle_id, $site_id, $for_role_ids, $page, $search,$limit,$user_id,$user_role_id);
        
        if($with_group_users){
            $huddle_groups = AccountFolder::getHuddleGroupsUsers($huddle_id, $site_id, $for_role_ids);
        }else{
            $huddle_groups =  AccountFolder::getHuddleGroups($huddle_id);
        }
        $participants_arranged = array();
        $participants = array();
        $huddle_name_type = null;
        $huddle_name = null;
        if (!empty($huddle_groups)) {
            if($with_group_users){
                foreach ($huddle_groups as $huddle_group) {    
                    $participants_arranged[ $huddle_group['id']] = array(
                        'user_id' => $huddle_group['id'],
                        'role_id' => $huddle_group['role_id'],
                        'user_name' => $huddle_group['first_name'] . ' ' . $huddle_group['last_name'],
                        'image' => $huddle_group['image'],
                        'user_email' => $huddle_group['email'],
                        'group_id' => $huddle_group['group_id'],
                        'is_submitted' => '1',
                        'last_submission' => null,
                    );
                }
            } else {
                foreach($huddle_groups as $row){
                    $participants_arranged[$row['user_id']] = array(
                        'user_id' => $row['user_id'],
                        'role_id' => $row['role_id'],
                        'user_name' => $row['name'],
                        'image' =>'groups',
                        'user_email' => 'N/A',
                        'group_id' => $row['group_id'],
                        'is_submitted' => null,
                        'last_submission' => null,
                    );
                }
            }
        }

        if (!empty($huddle_users)) {

            foreach ($huddle_users as $huddle_user) {
                
                $participants_arranged[$huddle_user['user_id']] = array(
                    'user_id' => $huddle_user['user_id'],
                    'role_id' => $huddle_user['role_id'],
                    'user_name' => $huddle_user['first_name'] . ' ' . $huddle_user['last_name'],
                    'image' => $huddle_user['image'],
                    'user_email' => $huddle_user['email'],
                    'group_id' => null,
                    'is_submitted' => $huddle_user['is_submitted'],
                    'last_submission' => $huddle_user['last_submission'],
                );
            }

            $folder_type = AccountFolder::getAccountFolderType($huddle_id);
            if ($folder_type == 1) {
                $folder_type = "collaboration";
            } else if ($folder_type == 2) {
                $folder_type = "coaching";
            } else {
                $folder_type = "assessment";
            }    

            $huddle_name_type = TranslationsManager::get_translation('huddle_list_collaboration', 'Api/huddle_list', $site_id, $user_id);
            $huddle_name = 'collaboration';
            if($folder_type == 'assessment')
            {
                $role_200 = 'assessor';
                $role_210 = 'assessed_participants';
                $role_220 = '';
                $huddle_name_type = TranslationsManager::get_translation('huddle_list_assessment', 'Api/huddle_list', $site_id, $user_id);
                $huddle_name = 'assessment';
            }
            elseif($folder_type == 'coaching'){
                $role_200 = 'coach';
                $role_210 = 'coachee';
                $role_220 = '';
                $huddle_name_type = TranslationsManager::get_translation('coaching_list_coaching', 'Api/huddle_list', $site_id, $user_id);
                $huddle_name = 'coaching';
            } else {
                $role_200 = 'collaboration_participants';
                $role_210 = 'collaboration_participants';
                $role_220 = 'collaboration_participants';
                $huddle_name_type = TranslationsManager::get_translation('huddle_list_collaboration', 'Api/huddle_list', $site_id, $user_id);
                $huddle_name = 'collaboration';
            }
            $role_name = [
                '200' => [
                    'default' => TranslationsManager::get_translation('gen_admin', 'Api/video_details', $site_id, $user_id),
                    'assessment' => TranslationsManager::get_translation('artifact_assessor', 'Api/huddle_list', $site_id, $user_id),
                    'coaching' => TranslationsManager::get_translation('artifact_coach', 'Api/huddle_list', $site_id, $user_id),
                ],
                '210' => [
                    'default' => TranslationsManager::get_translation('gen_member', 'Api/video_details', $site_id, $user_id),
                    'assessment' => TranslationsManager::get_translation('artifact_assessee', 'Api/huddle_list', $site_id, $user_id),
                    'coaching' => TranslationsManager::get_translation('artifact_coachee', 'Api/huddle_list', $site_id, $user_id),
                ],
                '220' => [
                    'default' => TranslationsManager::get_translation('gen_viewer', 'Api/video_details', $site_id, $user_id),
                    'coaching' => TranslationsManager::get_translation('artifact_coachee', 'Api/huddle_list', $site_id, $user_id),
                ],
            ];
            foreach ($participants_arranged as $row) {
                if ($row['role_id'] == '200') {
                    // $row['role_name'] = TranslationsManager::get_translation('gen_admin', 'Api/video_details', $site_id, $user_id);
                    $row['role_name'] = $role_name['200']['default'];
                    if ($folder_type == 'assessment') {
                        //$row['role_name'] = "Assessor";
                        // $row['role_name'] = TranslationsManager::get_translation('artifact_assessor', 'Api/huddle_list', $site_id, $user_id);
                        $row['role_name'] = $role_name['200']['assessment'];
                    }
                    if ($folder_type == 'coaching') {
                       // $row['role_name'] = "Coach";
                        // $row['role_name'] = TranslationsManager::get_translation('artifact_coach', 'Api/huddle_list', $site_id, $user_id);
                        $row['role_name'] = $role_name['200']['coaching'];
                    }
                    $row['role'] = $role_200;
                } elseif ($row['role_id'] == '210') {
                    // $row['role_name'] = TranslationsManager::get_translation('gen_member', 'Api/video_details', $site_id, $user_id);
                    $row['role_name'] = $role_name['210']['default'];
                    if ($folder_type == 'assessment') {
                       // $row['role_name'] = "Assessee";
                        // $row['role_name'] = TranslationsManager::get_translation('artifact_assessee', 'Api/huddle_list', $site_id, $user_id);
                        $row['role_name'] = $role_name['210']['assessment'];
                    }
                    if ($folder_type == 'coaching') {
                        //$row['role_name'] = "Coachee";
                        // $row['role_name'] = TranslationsManager::get_translation('artifact_coachee', 'Api/huddle_list', $site_id, $user_id);
                        $row['role_name'] = $role_name['210']['coaching'];
                    }
                    $row['role'] = $role_210;
                } else {
                    // $row['role_name'] = TranslationsManager::get_translation('gen_viewer', 'Api/video_details', $site_id, $user_id);
                    $row['role_name'] = $role_name['220']['default'];
                    if ($folder_type == 'coaching') {
                        // $row['role_name'] = TranslationsManager::get_translation('artifact_coachee', 'Api/huddle_list', $site_id, $user_id);
                        $row['role_name'] = $role_name['220']['coaching'];
                    }
                    $row['role'] = $role_220;
                }
                if(!($folder_type == "assessment" && $user_role_id == "210" && $row['role_id'] == "210" && $row["user_id"] != $user_id))
                {
                    $participants[] = $row;
                }
            }
    
        }

        return ['participants'=>$participants, 'huddle_name_type'=>$huddle_name_type, 'huddle_name'=>$huddle_name];
    }

    public static function getHuddleUsersIncludingGroupsNew($site_id, $huddle_id, $user_id, $user_role_id, $with_group_users=false, $for_role_ids=[]){

        $huddle_users = AccountFolder::getHuddleUsers($huddle_id, $site_id, $for_role_ids);
        if($with_group_users){
            $huddle_groups = AccountFolder::getHuddleGroupsUsers($huddle_id, $site_id, $for_role_ids);
        }else{
            $huddle_groups =  AccountFolder::getHuddleGroups($huddle_id);
        }
        $participants_arranged = array();
        $participants = array();
        $huddle_name_type = null;
        $huddle_name = null;

        if (!empty($huddle_groups)) {
            if($with_group_users){
                foreach ($huddle_groups as $huddle_group) {    
                    $participants_arranged[] = array(
                        'user_id' => $huddle_group['id'],
                        'role_id' => $huddle_group['role_id'],
                        'user_name' => $huddle_group['first_name'] . ' ' . $huddle_group['last_name'],
                        'image' => $huddle_group['image'],
                        'user_email' => $huddle_group['email'],
                        'group_id' => $huddle_group['group_id'],
                        'is_submitted' => '1',
                        'last_submission' => null,
                    );
                }
            } else {
                foreach($huddle_groups as $row){
                    $participants_arranged[] = array(
                        'user_id' => $row['user_id'],
                        'role_id' => $row['role_id'],
                        'user_name' => $row['name'],
                        'image' =>'groups',
                        'user_email' => 'N/A',
                        'group_id' => $row['group_id'],
                        'is_submitted' => null,
                        'last_submission' => null,
                    );
                }
            }
        }

        if (!empty($huddle_users)) {

            foreach ($huddle_users as $huddle_user) {
                
                $participants_arranged[] = array(
                    'user_id' => $huddle_user['user_id'],
                    'role_id' => $huddle_user['role_id'],
                    'user_name' => $huddle_user['first_name'] . ' ' . $huddle_user['last_name'],
                    'image' => $huddle_user['image'],
                    'user_email' => $huddle_user['email'],
                    'group_id' => null,
                    'is_submitted' => $huddle_user['is_submitted'],
                    'last_submission' => $huddle_user['last_submission'],
                );
            }

            $folder_type = AccountFolder::getAccountFolderType($huddle_id);
            if ($folder_type == 1) {
                $folder_type = "collaboration";
            } else if ($folder_type == 2) {
                $folder_type = "coaching";
            } else {
                $folder_type = "assessment";
            }    

            $huddle_name_type = TranslationsManager::get_translation('huddle_list_collaboration', 'Api/huddle_list', $site_id, $user_id);
            $huddle_name = 'collaboration';
            if($folder_type == 'assessment')
            {
                $role_200 = 'assessor';
                $role_210 = 'assessed_participants';
                $role_220 = '';
                $huddle_name_type = TranslationsManager::get_translation('huddle_list_assessment', 'Api/huddle_list', $site_id, $user_id);
                $huddle_name = 'assessment';
            }
            elseif($folder_type == 'coaching'){
                $role_200 = 'coach';
                $role_210 = 'coachee';
                $role_220 = '';
                $huddle_name_type = TranslationsManager::get_translation('coaching_list_coaching', 'Api/huddle_list', $site_id, $user_id);
                $huddle_name = 'coaching';
            } else {
                $role_200 = 'collaboration_participants';
                $role_210 = 'collaboration_participants';
                $role_220 = 'collaboration_participants';
                $huddle_name_type = TranslationsManager::get_translation('huddle_list_collaboration', 'Api/huddle_list', $site_id, $user_id);
                $huddle_name = 'collaboration';
            }
            
            foreach ($participants_arranged as $row) {                
                if( $row['user_id'] == $user_id){                    
                    $participants[] = $row;
                }   
              
            }
    
        }

        return ['participants'=>$participants, 'huddle_name_type'=>$huddle_name_type, 'huddle_name'=>$huddle_name];
    }

    
    public static function getVideoLibraryCount($domain, $accId, $site_id, $subject_ids = null, $withuncat = false, $except_categories = "", $vidTitle = "") {
        $accId = (int) $accId;
        $whereAll = "";
        if(!empty($vidTitle))
        {
            $vidTitle =   Document::mysql_escape($vidTitle);

            $whereAll = " AND ( ";
            $whereAll .= " af.name like '%$vidTitle%' ";
            //tags
            $whereAll .= " OR af.account_folder_id IN  ( select afmd.account_folder_id from account_folders_meta_data afmd join account_folders af on afmd.account_folder_id = af.account_folder_id where meta_data_name='tag' and meta_data_value like '%$vidTitle%' and af.account_id = $accId  ) ";
            //topics
            $whereAll .= " OR af.account_folder_id in (SELECT account_folder_id from account_folder_topics join topics on account_folder_topics.topic_id = topics.id where topics.name like '%$vidTitle%') ";
            //subjects
            $whereAll .= " OR af.account_folder_id in (SELECT account_folder_id from account_folder_subjects join subjects on account_folder_subjects.subject_id = subjects.id where subjects.name like '%$vidTitle%') ";
            //document
            $whereAll .= " OR afd.title like '%$vidTitle%' ";
            $whereAll .= " ) ";
        }

        if ($domain == "subjects") {
            /* $query = "select count(*) as count
              from subjects s inner join account_folder_subjects sv on s.id=sv.subject_id
              inner join account_folders v on sv.account_folder_id=v.account_folder_id
              where v.active=1 and v.account_id=$accId and v.folder_type=2"; */
            /* $query = "SELECT count(*) as count from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id JOIN account_folder_subjects afs ON afd.account_folder_id = afs.account_folder_id where 1=1 and account_folders.active=1 and account_folders.account_id=$accId and folder_type=2 and d.doc_type=1 order by account_folders.created_date desc";
             */
            $join = "JOIN";
            if(empty($subject_ids))
            {
                $whereS = " AND sf.id IS NOT NULL ";
            }
            else
            {
                if(!$withuncat)
                {
                    $whereS = " AND sf.subject_id IN ($subject_ids) ";
                }
                else
                {
                    $join = "LEFT JOIN";
                    $whereS = " AND (sf.subject_id IN ($subject_ids) OR sf.subject_id IS NULL) ";
                }
            }
            if(!empty($except_categories))
            {
                $whereS .= " AND af.account_folder_id NOT IN ( SELECT account_folder_id from account_folder_subjects where subject_id IN ($except_categories)) ";
            }
            $query = "SELECT COUNT(DISTINCT(af.account_folder_id)) as count FROM account_folders af $join account_folder_subjects sf ON af.`account_folder_id` = sf.account_folder_id JOIN account_folder_documents afd ON af.account_folder_id = afd.account_folder_id JOIN documents d ON afd.document_id = d.id WHERE af.account_id = $accId AND af.folder_type =2 AND af.active = 1 $whereAll AND d.doc_type = 1 $whereS AND af.site_id=" . $site_id;
            $result = \DB::select($query);
            if (is_array($result) && isset($result[0]->count))
                return $result[0]->count;
        } else if ($domain == "topics") {
            /* $query = "select count(*) as count
              from topics t inner join account_folder_topics tv on t.id=tv.topic_id
              inner join account_folders v on tv.accaccIdount_folder_id=v.account_folder_id
              where  v.active=1 and v.account_id=$ and v.folder_type=2"; */
            /* $query = "SELECT count(*) as count from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id JOIN account_folder_topics aft ON afd.account_folder_id = aft.account_folder_id where 1=1 and account_folders.active=1 and account_folders.account_id=$accId and folder_type=2 and d.doc_type=1 order by account_folders.created_date desc"; */
            $query = "SELECT COUNT(DISTINCT(af.account_folder_id)) as count FROM account_folders af LEFT JOIN account_folder_topics tf ON af.`account_folder_id` = tf.account_folder_id JOIN account_folder_documents afd ON af.account_folder_id = afd.account_folder_id JOIN documents d ON afd.document_id = d.id WHERE af.account_id = $accId AND af.folder_type =2 AND af.active = 1 $whereAll AND d.doc_type = 1 AND tf.id IS NOT NULL AND af.site_id = " . $site_id;
            $result =\DB::select($query);
            if (is_array($result) && isset($result[0]->count))
                return $result[0]->count;
        } else if ($domain == "unique_subjects") {
            $query = "select count(*) count from (
                select distinct v.account_folder_id as id
                from subjects s inner join account_folder_subjects sv on s.id=sv.subject_id
                inner join account_folders v on sv.account_folder_id=v.account_folder_id
                where  v.active=1 and v.account_id=$accId and v.folder_type=2  and v.site_id = " . $site_id . ") as UniqueSubjects";
            $result =\DB::select($query);
            if (is_array($result) && isset($result[0]->count))
                return $result[0]->count;
        } else if ($domain == "unique_topics") {
            $query = "select count(*) count from (
                select distinct v.account_folder_id as id
                from topics t inner join account_folder_topics tv on t.id=tv.topic_id
                inner join account_folders v on tv.account_folder_id=v.account_folder_id
                where  v.active=1 and v.account_id=$accId and v.folder_type=2 and v.site_id = " . $site_id . ") as UniqueTopics";
            $result =\DB::select($query);
            if (is_array($result) && isset($result[0]->count))
                return $result[0]->count;
        } else if ($domain == "all") {
            //$tot = $this->find('count', array('conditions' => array('account_id' => $accId, 'folder_type' => 2, 'active' => '1')));
            $query = "SELECT COUNT(DISTINCT(af.account_folder_id)) AS count FROM account_folders af LEFT JOIN account_folder_subjects sf ON af.`account_folder_id` = sf.account_folder_id JOIN account_folder_documents afd ON af.account_folder_id = afd.account_folder_id JOIN documents d ON afd.document_id = d.id WHERE af.account_id = $accId AND af.folder_type =2 AND af.active = 1 $whereAll AND d.doc_type = 1 AND af.site_id=" . $site_id;
            $result =\DB::select($query);
            if (is_array($result) && isset($result[0]->count))
                return $result[0]->count;
            //return $tot;
        }else if ($domain == "uncat") {
            //$tot = $this->find('count', array('conditions' => array('account_id' => $accId, 'folder_type' => 2, 'active' => '1')));
            $query = "SELECT COUNT(DISTINCT(af.account_folder_id)) AS count FROM account_folders af LEFT JOIN account_folder_subjects sf ON af.`account_folder_id` = sf.account_folder_id JOIN account_folder_documents afd ON af.account_folder_id = afd.account_folder_id JOIN documents d ON afd.document_id = d.id WHERE af.account_id = $accId AND af.folder_type =2 AND af.active = 1 $whereAll AND d.doc_type = 1 AND sf.id IS NULL AND af.site_id=" . $site_id;
            $result =\DB::select($query);
            if (is_array($result) && isset($result[0]->count))
                return $result[0]->count;
        }
    }
    
    public static function getSubjectTopicVideos($accId, $subjectId, $domain, $order, $start, $rows, $topicId,$site_id, $withuncat = false, $except_categories = "", $vidTitle = "") {
        $accId = (int) $accId;
        $topicId = (int) $topicId;
        $start = $start * $rows;
        $orderby = "";
        if($order == "updated_date")
        {
            $orderby = " order by account_folders.last_edit_date desc";
        }
        else if($order == "views")
        {
            $orderby = " order by d.view_count desc";
        }elseif($order == "title"){
            $orderby = " order by afd.title ASC";
        }
        else {//created date
            $orderby = " order by account_folders.created_date desc";
        }
        $whereAll = '';
        if(!empty($vidTitle))
        {
            $vidTitle =   Document::mysql_escape($vidTitle);
            $whereAll = " AND ( ";
            $whereAll .= " name like '%$vidTitle%' ";
            //tags
            $whereAll .= " OR account_folders.account_folder_id IN(select afmd.account_folder_id from account_folders_meta_data afmd join account_folders af on afmd.account_folder_id = af.account_folder_id where meta_data_name='tag' and meta_data_value like '%$vidTitle%' and af.account_id = $accId  ) ";
            //topics
            $whereAll .= " OR account_folders.account_folder_id in (SELECT account_folder_id from account_folder_topics join topics on account_folder_topics.topic_id = topics.id where topics.name like '%$vidTitle%') ";
            //subjects
            $whereAll .= " OR account_folders.account_folder_id in (SELECT account_folder_id from account_folder_subjects join subjects on account_folder_subjects.subject_id = subjects.id where subjects.name like '%$vidTitle%') ";
            //documents
            $whereAll .= " OR afd.title like '%$vidTitle%' ";
            $whereAll .= " ) ";
        }
        if(!empty($except_categories))
        {
            $whereAll .= " AND account_folders.account_folder_id NOT IN ( SELECT account_folder_id from account_folder_subjects where subject_id IN ($except_categories)) ";
        }
        if ($domain == 'subject') {
            $extraWhere = '';
            $join = "JOIN";
            if (!empty($subjectId)) {
                if(!$withuncat)
                {
                    $extraWhere .= " AND afs.subject_id IN ($subjectId) ";
                }
                else
                {
                    $join = "LEFT JOIN";
                    $extraWhere .= " AND (afs.subject_id IN ($subjectId) OR afs.subject_id IS NULL) ";
                }
            }

            /* if ($topicId > 0) {
              $extraWhere .= " AND aft.topic_id=$topicId ";
              } */

            //$query1 = "SELECT COUNT(DISTINCT(account_folders.account_folder_id)) as total from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id JOIN account_folder_subjects afs ON afd.account_folder_id = afs.account_folder_id where 1=1 $extraWhere and account_folders.active=1 and account_folders.account_id=$accId and folder_type=2 and d.doc_type=1" . $orderby;
            $query1 = "SELECT COUNT(DISTINCT(account_folders.account_folder_id)) AS total FROM account_folders LEFT JOIN account_folder_subjects afs ON account_folders.`account_folder_id` = afs.account_folder_id JOIN account_folder_documents afd ON account_folders.account_folder_id = afd.account_folder_id JOIN documents d ON afd.document_id = d.id WHERE account_folders.account_id = $accId AND account_folders.folder_type =2 AND account_folders.active = 1 AND d.doc_type = 1 AND account_folders.site_id= " . $site_id . " $extraWhere AND afs.id IS NOT NULL" . $orderby;
            $query2 = "SELECT afs.subject_id, account_folders.*, d.*, afd.title, afd.desc, d.id as doc_id, d.id as document_id,afd.id, u.first_name, u.last_name, IF(df.duration = 0 OR df.duration = NULL OR df.duration = '', d.video_duration, df.duration) AS video_duration from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id LEFT JOIN document_files df ON d.id = df.document_id JOIN users u on u.id = d.created_by $join account_folder_subjects afs ON account_folders.account_folder_id = afs.account_folder_id where 1=1 $extraWhere and account_folders.active=1 and account_folders.account_id=$accId and account_folders.site_id =" . $site_id . " and account_folders.folder_type=2 and d.doc_type=1 $whereAll group by account_folders.account_folder_id" . $orderby . " limit $start,$rows";
        } else if ($domain == 'topic') {

            $extraWhere = '';

            if ($topicId > 0) {
                $extraWhere .= " AND afs.subject_id IN ($subjectId) ";
            }

            if ($subjectId > 0) {
                $extraWhere .= " AND aft.topic_id=$topicId ";
            }

            //$query1 = "SELECT COUNT(DISTINCT(account_folders.account_folder_id)) as total from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id JOIN account_folder_topics aft ON afd.account_folder_id = aft.account_folder_id where 1=1 $extraWhere and account_folders.active=1 and account_folders.account_id=$accId and folder_type=2 and d.doc_type=1" . $orderby;
            $query1 = "SELECT COUNT(DISTINCT(account_folders.account_folder_id)) AS total FROM account_folders LEFT JOIN account_folder_topics aft ON account_folders.`account_folder_id` = aft.account_folder_id JOIN account_folder_documents afd ON account_folders.account_folder_id = afd.account_folder_id JOIN documents d ON afd.document_id = d.id WHERE account_folders.account_id = $accId AND account_folders.folder_type =2 AND account_folders.active = 1 AND account_folders.site_id =" . $site_id . " AND d.doc_type = 1 $extraWhere AND aft.id IS NOT NULL" . $orderby;
            $query2 = "SELECT afs.subject_id, account_folders.*, d.*, afd.title, afd.desc, d.id as doc_id, d.id as document_id,afd.id, u.first_name, u.last_name, IF(df.duration = 0 OR df.duration = NULL OR df.duration = '', d.video_duration, df.duration) AS video_duration from account_folders left join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id left join documents d on afd.document_id = d.id LEFT JOIN document_files df ON d.id = df.document_id JOIN users u on u.id = d.created_by LEFT JOIN account_folder_topics aft ON account_folders.account_folder_id = aft.account_folder_id where 1=1 $extraWhere and account_folders.active=1 and account_folders.account_id=$accId and account_folders.folder_type=2 AND account_folders.site_id =" . $site_id . " $whereAll group by account_folders.account_folder_id" . $orderby . " limit $start,$rows";
        } else if ($domain == 'all') {
            $query1 = "SELECT count(*) as total from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id  where  account_folders.active=1 and account_folders.account_id=$accId and folder_type=2 and account_folders.site_id =" . $site_id . "  and d.doc_type=1 $orderby ";
            $query2 = "SELECT afs.subject_id, account_folders.*, d.*, afd.title, afd.desc, d.id as doc_id, d.id as document_id,afd.id, u.first_name, u.last_name, IF(df.duration = 0 OR df.duration = NULL OR df.duration = '', d.video_duration, df.duration) AS video_duration  from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id LEFT JOIN document_files df ON d.id = df.document_id JOIN users u on u.id = d.created_by LEFT JOIN account_folder_subjects afs ON account_folders.account_folder_id = afs.account_folder_id where account_folders.active=1 and account_folders.account_id=$accId AND account_folders.site_id =" . $site_id . " and account_folders.folder_type=2 and d.doc_type=1 $whereAll group by account_folders.account_folder_id" . $orderby . " limit $start,$rows";
        } else if ($domain == 'uncat') {
            $query1 = "SELECT COUNT(DISTINCT(account_folders.account_folder_id)) AS total FROM account_folders LEFT JOIN account_folder_subjects afs ON account_folders.`account_folder_id` = afs.account_folder_id JOIN account_folder_documents afd ON account_folders.account_folder_id = afd.account_folder_id JOIN documents d ON afd.document_id = d.id WHERE account_folders.account_id = $accId AND account_folders.folder_type =2 AND account_folders.active = 1 AND d.doc_type = 1 AND afs.id IS NULL" . $orderby;
            $query2 = "SELECT afs.subject_id, account_folders.*, d.*, afd.title, afd.desc, d.id as doc_id, d.id as document_id,afd.id, u.first_name, u.last_name, IF(df.duration = 0 OR df.duration = NULL OR df.duration = '', d.video_duration, df.duration) AS video_duration from account_folders join account_folder_documents afd on account_folders.account_folder_id = afd.account_folder_id join documents d on afd.document_id = d.id LEFT JOIN document_files df ON d.id = df.document_id JOIN users u on u.id = d.created_by LEFT JOIN account_folder_subjects afs ON account_folders.account_folder_id = afs.account_folder_id where 1=1 and account_folders.active=1 and account_folders.account_id=$accId AND account_folders.site_id =" . $site_id . " and account_folders.folder_type=2 AND d.doc_type = 1 AND afs.id IS NULL $whereAll group by account_folders.account_folder_id" . $orderby . " limit $start,$rows";
        }


        //$result1 = \DB::select($query1);

        $result2 = \DB::select($query2);


        $arr2 = array();

        foreach ($result2 as $res) {
            $arr = (array)$res;
            //$viewCount = $this->getViewCount($res->account_folder_id);
            /*$arr['id'] = $res['account_folder_id'];
            $arr['video_title'] = $res->name;
            $arr['document_id'] = $res->id;
            $arr['video_url'] = $res->url;
            $arr['video_desc'] = $res->desc;
            $arr['view_count'] = $res->view_count;
            $arr['published'] = $res->published;
            $arr['encoder_provider'] = $res->encoder_provider;
            $arr['encoder_status'] = $res->encoder_status;
            $arr['created_date'] = $res->created_date;
            $arr['original_file_name'] = $res->original_file_name;
            $arr['created_by'] = $res->created_by;
            $arr['thumbnail_number'] = $res->thumbnail_number;*/
            $arr2[] = $arr;
        }
        $total_videos = 0;

        if (isset($result1) && isset($result1[0]->total)) {
            //$arr['total_rec'] = $result1[0][0]['total'];
            $total_videos = $result1[0]->total;
        }

        return ["total_videos" => $total_videos, "videos"=> $arr2];
    }
    

    public static function getSubjects($accId, $site_id, $account_folder_id = null) {
        $accId = (int) $accId;
        $sub_query2 = "";
        if($account_folder_id)
        {
            $sub_query2 = " AND sv.account_folder_id = '$account_folder_id' ";
        }
        $query = "select s.id as subject_id,s.name,count(v.account_folder_id) as count
                from subjects s left join account_folder_subjects sv on s.id=sv.subject_id
                left join account_folders v on sv.account_folder_id=v.account_folder_id and v.active=1 and v.account_id=$accId  and v.folder_type=2 and v.site_id = " . $site_id . "
                where s.account_id = $accId and s.site_id = $site_id
                AND (s.parent_id is NULL OR s.parent_id = '' OR s.parent_id = 0)
                $sub_query2
                group by s.id
                order by s.name asc";

        $result = \DB::select($query);
        $arr = array();
        $i = 0;

        foreach ($result as $res) {
            $query2 = "select s.id as subject_id,s.name,count(v.account_folder_id) as count
                 from subjects s left join account_folder_subjects sv on s.id=sv.subject_id
                left join account_folders v on sv.account_folder_id=v.account_folder_id and v.active=1 and v.account_id=$accId  and v.folder_type=2 and v.site_id = " . $site_id . "
                where s.account_id = $accId and s.site_id = $site_id
                AND s.parent_id = ".$res->subject_id."
                $sub_query2
                group by s.id
                order by s.name asc";
            $result2 = \DB::select($query2);
            $arr2 = array();
            $j = 0;
            foreach ($result2 as $res2) {

                $arr2[$j]['subject_id'] = $res2->subject_id;
                $arr2[$j]['name'] = $res2->name;
                $arr2[$j]['count'] = $res2->count;
                $j++;
            }
            $arr[$i]['subject_id'] = $res->subject_id;
            $arr[$i]['name'] = $res->name;
            $arr[$i]['count'] = $res->count;
            $arr[$i]['childs'] = $arr2;
            $arr[$i]['no_category_count'] = self::calculate_no_subcategories_videos($res->subject_id);
            $i++;
        }
        return $arr;
    }

    public static function getTopics($accId, $site_id) {
        $accId = (int) $accId;

        $query = "select t.id as topic_id,t.name,count(*) as count
                from topics t inner join account_folder_topics tv on t.id=tv.topic_id
                inner join account_folders v on tv.account_folder_id=v.account_folder_id
                where  v.active=1 and v.account_id=$accId and v.folder_type=2 and v.site_id = " . $site_id . "
                group by t.id
                order by t.id asc";

        $result = \DB::select($query);
        $arr = array();
        $i = 0;

        foreach ($result as $res) {
            $arr[$i]['topic_id'] = $res->topic_id;
            $arr[$i]['name'] = $res->name;
            $arr[$i]['count'] = $res->count;
            $i++;
        }
        return $arr;
    }
    
    
    public static function calculate_no_subcategories_videos($subject_id)
    {
           $query = "SELECT account_folder_subjects.account_folder_id FROM account_folder_subjects JOIN account_folders AF ON AF.`account_folder_id` = account_folder_subjects.`account_folder_id` WHERE subject_id IN (".$subject_id.") AND AF.`active` = 1";
           $results = \DB::select($query);
           $count = 0;
           $subject_childs_query = "SELECT id FROM subjects WHERE parent_id = ".$subject_id;
           $subject_childs_query_results = \DB::select($subject_childs_query);
           $childs_array = array();
           foreach($subject_childs_query_results as $row)
           {
               $childs_array[] = $row->id;
           }
           
           if(!empty($childs_array))
           {
               $childs_array_string = implode(',', $childs_array);
           }
           else
           {
               $childs_array_string = '0';
           }
           
           foreach($results as $result)
           {
               $sub_category_query = "SELECT account_folder_id FROM `account_folder_subjects` WHERE subject_id IN (".$childs_array_string.") AND account_folder_id = " .$result->account_folder_id ;
               $sub_category_result = \DB::select($sub_category_query);
               if(!$sub_category_result)
               {
                   $count++;
               }
               
           }
           
           return $count;
        
    }

    public static function get_title_for_header($parent_folder_id, $huddle_id){
        $title = "";
        if(!empty($parent_folder_id)) {
            $title = DocumentFolder::where("id", $parent_folder_id)->value("name");
        } elseif(!empty($huddle_id)) {
            $title = self::where("account_folder_id", $huddle_id)->value("name");
        }
        return $title;
    }

}
