<?php

namespace App\Jobs;
use Illuminate\Support\Facades\Log;

class RegisterJobForQueue extends Job
{
    private $class;
    private $function;
    private $args;

    public $timeout = 600;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($class, $function, $args)
    {
        $this->class = $class; 
        $this->function = $function; 
        $this->args = $args;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            \DB::reconnect('mysql');
            \Log::info(">>>> Executing Queued functions from RegisterJobForQueue");
            \Log::info($this->class." -> ".$this->function);
            call_user_func_array([app($this->class), $this->function], $this->args);
            \DB::disconnect();
        } catch (\Exception $e) {
            Log::error("----------- An error in queues occurred ------------");
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
        // app('App\Http\Controllers\VideoController')->processSendCommentEmail($this->account_folder_id,$this->account_id,$this->user_id,$this->video_id,$this->comment_id,$this->comment,$this->first_name,$this->last_name,$this->company_name,$this->image, $this->site_id);
    }
}
