<?php

namespace App\Services\Exports\Reporting;

class ExportToPDF {
     public function export($headings,$reports,$account_name) { 
        
        $contents = view('export/reports_pdf', compact('headings', 'reports'))->render();
 
        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadHTML($contents);
        $today = date("m-d-y");
        return $pdf->download($account_name."_coaching_report_".$today .'.pdf');

    }
}

?>
