<?php

namespace App\Services\Exports\Reporting;
use Aws\S3\S3Client;
// use Maatwebsite\Excel\Facades\Excel;

/**
 * composer require phpoffice/phpspreadsheet
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;

class ExportToCSV {

    public function export($heading,$data,$account_name,$filename,$user_id) {
 
        $csv = new ExportHandler($heading, $data);
        $today = date("m-d-y");
        $csv->download($filename,$user_id);
    }
}

class ExportHandler{

    private $headings;
    private $reporting;
    private $title = "Reporting";
    public function __construct($headings, $reporting)
    {
        $this->headings = $headings;
        $this->reporting = $reporting;
    }

    public function download($filename,$user_id){
        // CREATE A NEW SPREADSHEET + SET METADATA
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
        ->setCreator('Sibme')
        ->setLastModifiedBy('Sibme')
        ->setTitle($this->title)
        ->setSubject($this->title)
        ->setDescription($this->title)
        ->setKeywords($this->title)
        ->setCategory($this->title);

        // NEW WORKSHEET
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle($this->title);

        // HEADER ROW
        // $headings = $this->headings($translations);
        $headings = $this->headings;
        $sheet->fromArray([$headings], NULL, 'A1');

        $this->formatAssessees($sheet);

        // OUTPUT
        $writer = new Csv($spreadsheet);
        $writer->setUseBOM(true);
        $writer->setDelimiter(',');
        $writer->setEnclosure('');
        $writer->setLineEnding("\r\n");
        $writer->setSheetIndex(0);
        
        // FORCE DOWNLOAD
//        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//        header('Content-Disposition: attachment;filename="'.$filename.'"');
//        header('Cache-Control: max-age=0');
//        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
//        header('Cache-Control: cache, must-revalidate');
//        header('Pragma: public');
//        $writer->save('php://output');
        $dir = config('general.export_analytics_path');
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        $file_path = $dir . $filename;
        $writer->save($file_path);
        $destination = $this->save_to_s3($file_path);
        app('App\Http\Controllers\ReportingController')->send_reports_export_email($filename,$user_id);
    }
    public function save_to_s3($file){
        $path_parts = pathinfo($file);
        $new_destination =  config('s3.export_analytics_path') . $path_parts['filename'] . '.' . $path_parts['extension'];
 
        // Create an Amazon S3 client object
        $client = new S3Client([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);

        $client->putObject(array(
            'Bucket' => config('s3.bucket_name'),
            'Key' => $new_destination,
            'SourceFile' => $file
        ));

        return $new_destination;
    }
    public function formatAssessees(&$sheet){
        $xl_count = 1;
        foreach ($this->reporting as  $report) {
            $xl_count++;
            $sheet->fromArray($report, NULL, 'A'.$xl_count, true);
        }
    }

}

?>
