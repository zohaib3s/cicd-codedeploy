<?php

namespace App\Services\Exports\HuddleReport;

class ExportToPDF {
     public function export($headings,$huddle_reports,$account_name) { 
        ini_set('max_execution_time', '0');
        ini_set('memory_limit', '3000MB');
        $new_count = 0;
        if(count($huddle_reports) > 50)
        {
            $zip = new \ZipArchive;
            $today = date("m-d-y");
            $zip_file_path = public_path("pdf/".$account_name."_huddle_report_".$today.".zip");
            $res = $zip->open($zip_file_path, \ZipArchive::CREATE);
            $counter = 0;
            $new_huddle_reports = $huddle_reports;
            while($counter < count($new_huddle_reports))
            {
              if($counter+50 < count($new_huddle_reports))
              {
                  $end_limit = $counter+50;
              }
              else
              {
                  $end_limit = count($new_huddle_reports);
              }
            $huddle_reports = [];  
            $huddle_reports = array_slice($new_huddle_reports,$counter ,$end_limit);
            $counter = $end_limit + 1;
            $contents = view('export/huddle_reports_pdf', compact('headings', 'huddle_reports'))->render();
            $pdf = app()->make('dompdf.wrapper');
            $pdf->loadHTML($contents);
            $today = date("m-d-y");
            //$pdf->stream($account_name."_huddle_report_".$today .'.pdf');
            
            $output = $pdf->output();
            $name = public_path("$new_count.pdf");
            file_put_contents($name,$output);
            $zip->addFile($name);
            unset($pdf);
            }
            $zip->close();
           return response()->download($zip_file_path)->deleteFileAfterSend();
        }
        else 
        {
            $contents = view('export/huddle_reports_pdf', compact('headings', 'huddle_reports'))->render();
            $pdf = app()->make('dompdf.wrapper');
            $pdf->loadHTML($contents);
            $today = date("m-d-y");
            return $pdf->download($account_name."_huddle_report_".$today .'.pdf');
        }

    }
}

?>
