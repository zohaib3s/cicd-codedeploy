<?php

namespace App\Services\Exports\Goals\Detail;

/**
 * composer require phpoffice/phpspreadsheet
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use App\Services\HelperFunctions;

class ExportToExcel {

    public function export($result, $translations) {
        $headingsToExport = $this->getHeadingsToExport($translations);
        // $goalsToExport = $this->getGoalsToExport($goals);
        $excel = new ExportHandler($headingsToExport, $result);
        $excel->download('goals_detail.xlsx');
    }

    public function getHeadingsToExport($translations){
        $headings = [
            $translations['analytics_user'],
            $translations['last_update'],
            $translations['status_all'],
        ];

        return $headings;
    }

}

class ExportHandler{

    private $headings;
    private $goal;
    private $users;
    private $title = "Goal Detail";
    public function __construct($headings, $result)
    {
        $this->headings = $headings;
        $this->goal = $result['goal_data'];
        $this->users = $result['user_data'];
    }

    public function download($filename){
        // CREATE A NEW SPREADSHEET + SET METADATA
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
        ->setCreator('Sibme')
        ->setLastModifiedBy('Sibme')
        ->setTitle($this->title)
        ->setSubject($this->title)
        ->setDescription($this->title)
        ->setKeywords($this->title)
        ->setCategory($this->title);

        // NEW WORKSHEET
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle($this->title);

        $sheet->fromArray(['Title: '.$this->goal->title], NULL, 'A1');
        $sheet->fromArray(['Description: '.strip_tags($this->goal->desc)], NULL, 'A2');
        $sheet->fromArray(['Start date: '.HelperFunctions::formatReadableDate($this->goal->start_date), 'Due date: '.HelperFunctions::formatReadableDate($this->goal->end_date)], NULL, 'A3');
        $sheet->fromArray(['Exported date: '.date("M d, Y")], NULL, 'A4');
        // HEADER ROW
        // $headings = $this->headings($translations);
        $headings = $this->headings;
        $sheet->fromArray([$headings], NULL, 'A6');

        $this->formatUsers($sheet);

        // OUTPUT
        $writer = new Xlsx($spreadsheet);
        // FORCE DOWNLOAD
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer->save('php://output');
    }

    public function formatUsers(&$sheet){
        $xl_count = 6;
        foreach ($this->users as $user) {
            $xl_count++;
            $sheet->fromArray([$user], NULL, 'A'.$xl_count);
        }
    }

}

?>