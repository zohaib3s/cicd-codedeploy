<?php

namespace App\Services\Exports\Goals\Detail;

use App\Services\HelperFunctions;

class ExportToPDF {
    
    public function export($result, $translations,$header_data) {
        $goal = $result['goal_data'];
        $headings = $this->getHeadingsToExport($translations);
        $users = $this->formatUsers($result['user_data'], $goal);
        $contents = view('export/Goals/detail', compact('headings', 'goal', 'users','header_data'))->render();
        // Following output-xhtml option is creating problems for CSS in PDF.
        /*
        $tidy_config = array(
            //'clean' => true,
            'output-xhtml' => false,
            'output-html' => true,
            'show-body-only' => true,
            'wrap' => 0,
            //'fix-style-tags'=>false
        );
        
        $tidy = tidy_parse_string( $contents, $tidy_config, 'UTF8');
        $tidy->cleanRepair();
        
        $contents = $tidy->value;
        $contents = str_replace('<s>', '<p>', $contents);
        $contents = str_replace('</s>', '</p>', $contents);
        */
        @ini_set("memory_limit", "1000M");
        set_time_limit(100);
        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadHTML($contents);
        return $pdf->download('goal_details.pdf');
        // return $pdf->stream();
    }

    public function getHeadingsToExport($translations){
        $headings = [
            $translations['analytics_user'],
            $translations['last_update'],
            $translations['status_all'],
        ];

        return $headings;
    }

    public function formatUsers($usersToExport, $goal){
        $users = [];
        foreach ($usersToExport as $user) {
            // $goal_row = [];
            // $goal_row ['title'] = $goal->title;
            // $goal_row ['edtpa_type'] = $goal->goal_type;
            // $goal_row ['last_update'] = $goal->last_edit_date;
            // $goal_row ['due_date_all'] = $goal->end_date;
            // $goal_row ['goal_published'] = $goal->publish_date;
            // $goal_row ['goals_owner'] = $goals_owners;
            // $items_count = !empty($goal->total_items) ? $goal->total_items : $goal->action_items_count;
            // $goal_row ['status_all'] = !empty($items_count) ? round(($goal->completed_items / $items_count) * 100). "%" : "0%";
            $user_row = [];
            $user_row ['title'] = $user->first_name ." ".$user->last_name . " (" .$user->email. ")";
            $user_row ['last_update'] = HelperFunctions::formatReadableDate($user->last_edit_date);
            $items_count = !empty($goal->total_items) ? $goal->total_items : $goal->action_items_count;
            $user_row ['status_all'] = !empty($items_count) ? round(($user->completed_items / $items_count) * 100). "%" : "0%";

            // $user_row ['status_all'] = "0%";

            $users [] = $user_row;
        }

        return $users;
    }
}

?>
