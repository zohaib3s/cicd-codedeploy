<?php

namespace App\Services\Exports\Goals\ActionItems;

/**
 * composer require phpoffice/phpspreadsheet
 */

use App\Http\Controllers\GoalsController;
use Codedge\Fpdf\Fpdf\Fpdf;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Carbon\Carbon;

use App\Services\HelperFunctions;

class Export {

    public static function ActionItems($item){
        return new ActionItem($item);
    }

    public function export($actionItemsToExport, $translations,$username) {
        $headingsToExport = $this->getHeadingsToExport($translations);
        
        $excel = new ExportHandler($headingsToExport, $actionItemsToExport, $translations);
        $excel->download($username);
    }

    public function getGoalStatus($goal){

        $progress = $goal['data']['progress'];

        $totalItems = count($goal['data']['action_items']);
        $share =  floor(1/$totalItems*100);

        foreach($goal['data']['action_items'] as $item)
        {
            if($item["is_done"] !=1){
                $actionItem = new ActionItem($item);
                $itemProgress = $actionItem->getProgress(true);
                if($itemProgress > 0){
                    $progress += ($share * $itemProgress/100);
                }
            }  
        }
        return floor($progress);
    }

    public function exportToPdf($actionItems, $translations,$username,$account_name){

        $act_name = $account_name;
        $report_name = "Goal's Action Items";
        $userName = $username;

        $headingsToExport = $this->getHeadingsToExport($translations,true);

        $pdf = new Fpdf();
        $pdf->AddPage();

        $pdf->Image(config('s3.sibme_base_url')."/img/pdf_logo.png", 10, 10);
        $pdf->SetFont('Arial');
        $pdf->cell(125,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Account Name:',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->cell(22,5,$act_name,0,1);
        $pdf->cell(125,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Report Name:',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->cell(22,5,$report_name,0,1);
        $pdf->cell(125,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Exported By:',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->cell(22,5,$userName,0,1);
        $pdf->cell(125,5,'',0,0);
        $pdf->SetFont('Arial','B',8);
        $pdf->cell(22,5,'Exported Date:',0,0);
        $pdf->SetFont('Arial','',8);
        $pdf->cell(22,5,date("Y-m-d h:i:sa"),0,1);
        $pdf->cell(0,5,'','B',1);
        $pdf->cell(0,5,'','0',1);

	    $pdf->SetFont('Arial','B',10);
        $rec = 1;

        $pdf->MultiCell(0,6, $actionItems["data"]["title"]);
        $pdf->MultiCell(0,6, "Description". " : " . "\t" . trim(strip_tags($actionItems["data"]["desc"])) );
        $pdf->MultiCell(0,6, "Start Date" ." : " . "\t" . Carbon::parse($actionItems["data"]["start_date"])->toFormattedDateString());
        $pdf->MultiCell(0,6, "End Date" . " : " . "\t" . Carbon::parse($actionItems["data"]["end_date"])->toFormattedDateString());
        $pdf->MultiCell(0,6, "Owner/s" . " : " . "\t" . $this->getOwners($actionItems["data"]["owners"]));
        $pdf->MultiCell(0,6, "Collaborators" . " : " . "\t" . $this->getOwners($actionItems["data"]["collaborators"]));
        $pdf->MultiCell(0,6, "Goal Status" . " : " . "\t" . $this->getGoalStatus($actionItems) . "%");
      
        $pdf->SetFont('Arial','B',9);
        $pdf->Cell(0,5,"",0,1);

        foreach ($actionItems["data"]["action_items"] as $actionItem) {
                           
            $cell_data = "";
            $actionItem = new ActionItem($actionItem);
            $actionItem_row = [];

            $actionItem_row['goals_title'] = $actionItem->getTitle();
            $actionItem_row['goals_due_date'] = $actionItem->getDueDate();
            $actionItem_row['last_update'] = $actionItem->getLastUpdate();
            $actionItem_row['start'] = $actionItem->getStart();
            $actionItem_row['end'] = $actionItem->getEnd();
            $actionItem_row['goal_current']  = $actionItem->getCurrent();
            $actionItem_row['progress'] = $actionItem->getProgress();
            $actionItem_row['status'] = $actionItem->getStatus();
            $actionItem_row['action_item_done_date'] = $actionItem->getDoneDate();
            $actionItem_row['goal_reflection'] = $actionItem->getReflection();
            $actionItem_row['feedback'] = $actionItem->getFeedback();

            foreach($headingsToExport as $key => $header_dataa){
                if(isset($actionItem_row[$key]) ){    
                    $cell_data .= chr(149) . "\t\t\t" . $header_dataa . " : " . $actionItem_row[$key]  . "\n"; 
                }
            }

            $pdf->MultiCell(0,6, $cell_data
                    ,1,1);
                $pdf->Ln(2);

        }
        
        $pdf->Output('D', str_replace(" ", "_", $userName) . "_Action_Items_Export_" . date('Y_d_m') . ".pdf");
      
    }


    public function getHeadingsToExport($translations,$keys=false){
        if($keys){
            $headings = [
                'goals_title' => $translations['goals_title'],
                'goals_due_date' =>$translations['goals_due_date'] ,
                'last_update' =>$translations['last_update'],
                'start' =>$translations['start'],
                'end' =>$translations['end'],
                'goal_current' =>$translations['goal_current'],
                'progress' =>$translations['progress'],
                'status' =>$translations['status'],
                'action_item_done_date' =>$translations['action_item_done_date'],
                'goal_reflection' =>$translations['goal_reflection'],
                'feedback' =>$translations['feedback'],
            ];
        }else{
            $headings = [
                $translations['goals_title'],
                $translations['goals_due_date'] ,
                $translations['last_update'],
                $translations['start'],
                $translations['end'],
                $translations['goal_current'],
                $translations['progress'],
                $translations['status'],
                $translations['action_item_done_date'],
                $translations['goal_reflection'],
                $translations['feedback'],
            ];
        }
        

        return $headings;
    }

    public function getOwners($owners){
        $final_owners = [];
        foreach ($owners as $owner) {
            $final_owners [] = $owner["first_name"] ." ". $owner["last_name"];
        }
        return implode(", ", $final_owners);
    }

    public function getGoalsType($type_id){
        $trans_key = $this->goals_type_translation_key[$type_id];
        return $this->translations[$trans_key];
    }

}

class ExportHandler{

    private $headings;
    private $actionItems;
    private $title = "Action Items";
    public $goals_type_translation_key = ['1'=>'template', '2'=>'vd_account', '3'=>'group', '4'=>'individual'];
    public $translations;

    public function __construct($headings, $actionItems, $translations)
    {
        $this->headings = $headings;
        $this->actionItems = $actionItems;
        $this->translations = $translations;
    }
    public function getGoalStatus($goal){

        $progress = $goal['data']['progress'];

        $totalItems = count($goal['data']['action_items']);
        $share =  floor(1/$totalItems*100);

        foreach($goal['data']['action_items'] as $item)
        {
            if($item["is_done"] !=1){
                $actionItem = new ActionItem($item);
                $itemProgress = $actionItem->getProgress(true);
                if($itemProgress > 0){
                    $progress += ($share * $itemProgress/100);
                }
            }
        }
        return floor($progress);
    }

    public function download($filename){
        // CREATE A NEW SPREADSHEET + SET METADATA
        $username = $filename;
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
        ->setCreator('Sibme')
        ->setLastModifiedBy('Sibme')
        ->setTitle($this->title)
        ->setSubject($this->title)
        ->setDescription($this->title)
        ->setKeywords($this->title)
        ->setCategory($this->title);

        // NEW WORKSHEET
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle($this->title);

        // HEADER ROW
        // $headings = $this->headings($translations);
        $headings = $this->headings;


        $sheet->setCellValue('A1', $this->actionItems["data"]["title"]);

        $sheet->setCellValue('A2', "Description");
        $sheet->setCellValue('A3', "Start Date");
        $sheet->setCellValue('A4', "End Date");
        $sheet->setCellValue('A5', "Owner/s");
        $sheet->setCellValue('A6', "Collaborators");
        $sheet->setCellValue('A7', "Goal Status");

        $sheet->setCellValue('B2', strip_tags($this->actionItems["data"]["desc"]));
        $sheet->setCellValue('B3', Carbon::parse($this->actionItems["data"]["start_date"])->toFormattedDateString());
        $sheet->setCellValue('B4', Carbon::parse($this->actionItems["data"]["end_date"])->toFormattedDateString());
        $sheet->setCellValue('B5', $this->getOwners($this->actionItems["data"]["owners"]));
        $sheet->setCellValue('B6', $this->getOwners($this->actionItems["data"]["collaborators"]));
        $sheet->setCellValue('B7', $this->getGoalStatus($this->actionItems) . "%");
        



        $sheet->fromArray([$headings], NULL, 'A8');

        $this->formatGoals($sheet);

        // OUTPUT
        $writer = new Xlsx($spreadsheet);
        // FORCE DOWNLOAD
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.str_replace(" ", "_", $username) . "_Action_Items_Export_" . date('Y_d_m').  ".xlsx" .'"');
        header('Cache-Control: max-age=0');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer->save('php://output');
    }

    public function formatGoals(&$sheet){
        $xl_count = 8;
        foreach ($this->actionItems["data"]["action_items"] as $actionItem) {
            $actionItem = new ActionItem($actionItem);
            $actionItem_row = [];

            $actionItem_row['goals_title'] = $actionItem->getTitle();
            $actionItem_row['goals_due_date'] = $actionItem->getDueDate();
            $actionItem_row['last_update'] = $actionItem->getLastUpdate();
            $actionItem_row['start'] = $actionItem->getStart();
            $actionItem_row['end'] = $actionItem->getEnd();
            $actionItem_row['goal_current']  = $actionItem->getCurrent();
            $actionItem_row['progress'] = $actionItem->getProgress() ;
            $actionItem_row['status'] = $actionItem->getStatus();
            $actionItem_row['action_item_done_date'] = $actionItem->getDoneDate();
            $actionItem_row['goal_reflection'] = $actionItem->getReflection();
            $actionItem_row['feedback'] = $actionItem->getFeedback();
            
            $xl_count++;
            $sheet->fromArray([$actionItem_row], NULL, 'A'.$xl_count,true);
        }
    }

    public function getOwners($owners){
        $final_owners = [];
        foreach ($owners as $owner) {
            $final_owners [] = $owner["first_name"] ." ". $owner["last_name"];
        }
        return implode(", ", $final_owners);
    }

    public function getGoalsType($type_id){
        $trans_key = $this->goals_type_translation_key[$type_id];
        return $this->translations[$trans_key];
    }

}

class ActionItem {

    public $item ;

    public function __construct($item){
        $this->item = $item;
    }

    public function getTitle(){
        return $this->item["title"];
    }

    public function getDueDate(){
        $date = Carbon::parse($this->item["deadline"]); 
        return $date->toFormattedDateString();
    }

    public function getLastUpdate(){
        $date = Carbon::parse($this->item["last_edit_date"]); 
        return $date->toFormattedDateString();
    }

    public function getStart(){
       
        return $this->item["start_value"];
    }

    public function getEnd(){
       
        return $this->item["end_value"];
    }

    public function getCurrent(){
       
        return $this->item["current_value"];
    }

    public function getProgress($number=false){
        $item = (object)$this->item;
        GoalsController::checkItemProgress($item);
        $this->item = (array)$item;
        if($number)
        {
            return $item->percentage;
        }
        else
        {
            return ($item->percentage > 0 ? $item->percentage.'%' : '-');
        }
       $start = $this->getStart();
       $end = $this->getEnd();
       $current = $this->getCurrent();
       $total = $end - $start;
       $current = $current - $start;

       if($end > 0){
        
            if($current > $total){
                if($number){
                    return 100;
                }else{
                    return "100%";
                }
                
            }
            if($total > 0)
            {
                $percentage = $current/$total*100;
            }
            else
            {
                $percentage = 0;
            }
            if($number){
                return floor($percentage);
            }else{
                return floor($percentage) . "%";
            }
            
       }else{
           if($number){
               return 0;
           }else{
               return "-";
           }
           
       }

    }
    public function getStatus(){

        return $this->item["is_completed_on_time"] != 1 ? (Carbon::parse($this->item["deadline"]) < Carbon::now() ? "Incomplete" : (Carbon::parse($this->item["deadline"]) >= Carbon::now() ? "Pending" : "Lets See")) : "Completed";
    }

    public function getDoneDate(){
        return  $this->item["completed_at"];
    }

    public function getReflection(){
        return isset($this->item["reflection"]) ? strip_tags($this->item["reflection"]) : '';
    }

    public function getFeedback(){
        return   isset($this->item["feedback"]) ? strip_tags($this->item["feedback"]) : '';
    }



}

?>