<?php

namespace App\Services\Exports\Goals\GoalsList;

/**
 * composer require phpoffice/phpspreadsheet
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use App\Services\HelperFunctions;

class ExportToExcel {
    public function export($goalsToExport, $translations) {
        $headingsToExport = $this->getHeadingsToExport($translations);
        // $goalsToExport = $this->getGoalsToExport($goals);
        $excel = new ExportHandler($headingsToExport, $goalsToExport, $translations);
        $excel->download('goals.xlsx');
    }

    public function getHeadingsToExport($translations){
        $headings = [
            $translations['title'],
            $translations['edtpa_type'],
            $translations['last_update'],
            $translations['due_date_all'],
            $translations['goal_published'],
            $translations['goals_owner'],
            $translations['status_all'],
        ];

        return $headings;
    }

}

class ExportHandler{

    private $headings;
    private $goals;
    private $title = "Goals List";
    public $goals_type_translation_key = ['1'=>'template', '2'=>'vd_account', '3'=>'group', '4'=>'individual'];
    public $translations;

    public function __construct($headings, $goals, $translations)
    {
        $this->headings = $headings;
        $this->goals = $goals;
        $this->translations = $translations;
    }

    public function download($filename){
        // CREATE A NEW SPREADSHEET + SET METADATA
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
        ->setCreator('Sibme')
        ->setLastModifiedBy('Sibme')
        ->setTitle($this->title)
        ->setSubject($this->title)
        ->setDescription($this->title)
        ->setKeywords($this->title)
        ->setCategory($this->title);

        // NEW WORKSHEET
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle($this->title);

        // HEADER ROW
        // $headings = $this->headings($translations);
        $headings = $this->headings;
        $sheet->fromArray([$headings], NULL, 'A1');

        $this->formatGoals($sheet);

        // OUTPUT
        $writer = new Xlsx($spreadsheet);
        // FORCE DOWNLOAD
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer->save('php://output');
    }

    public function formatGoals(&$sheet){
        $xl_count = 1;
        foreach ($this->goals as $goal) {
            $xl_count++;
            $sheet->fromArray([$goal], NULL, 'A'.$xl_count);
        }
    }

    public function getOwners($owners){
        $final_owners = [];
        foreach ($owners as $owner) {
            $final_owners [] = $owner->first_name ." ". $owner->last_name;
        }
        return implode(", ", $final_owners);
    }

    public function getGoalsType($type_id){
        $trans_key = $this->goals_type_translation_key[$type_id];
        return $this->translations[$trans_key];
    }

}

?>