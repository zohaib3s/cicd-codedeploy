<?php

namespace App\Services\Exports\Goals\GoalsList;

use App\Services\HelperFunctions;

class ExportToPDF {

    public $goals_type_translation_key = ['1'=>'template', '2'=>'vd_account', '3'=>'group', '4'=>'individual'];
    public $translations;
    public function export($goalsToExport, $translations,$header_data = array()) {
        $this->translations = $translations;
        $headings = $this->getHeadingsToExport();
        $goals = $this->formatGoals($goalsToExport);       
        $contents = view('export/Goals/list', compact('headings', 'goals','header_data'))->render();
        // Following output-xhtml option is creating problems for CSS in PDF.
        /*
        $tidy_config = array(
            //'clean' => true,
            'output-xhtml' => false,
            'output-html' => true,
            'show-body-only' => true,
            'wrap' => 0,
            //'fix-style-tags'=>false
        );
        
        $tidy = tidy_parse_string( $contents, $tidy_config, 'UTF8');
        $tidy->cleanRepair();
        
        $contents = $tidy->value;
        $contents = str_replace('<s>', '<p>', $contents);
        $contents = str_replace('</s>', '</p>', $contents);
        */
       @ini_set("memory_limit", "1000M");
        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadHTML($contents);
        return $pdf->download('goals.pdf');
        // return $pdf->stream();
    }

    public function getHeadingsToExport(){
        $translations = $this->translations;
        $headings = [
            $translations['title'],
            $translations['edtpa_type'],
            $translations['last_update'],
            $translations['due_date_all'],
            $translations['goal_published'],
            $translations['goals_owner'],
            $translations['status_all'],
        ];

        return $headings;
    }

    public function formatGoals($goalsToExport){
        $xl_count = 1;
        $count= 0;
        $goals = [];
        foreach ($goalsToExport as $goal) {
            $goals_owners = $this->getOwners($goal->owners);
            
            $goal_row = [];
            $goal_row ['title'] = $goal->title;
            $goal_row ['edtpa_type'] = $this->getGoalsType($goal->goal_type);
            $goal_row ['last_update'] = HelperFunctions::formatReadableDate($goal->last_edit_date);
            $goal_row ['due_date_all'] = HelperFunctions::formatReadableDate($goal->end_date);
            $goal_row ['goal_published'] = empty($goal->is_published) ? "Unpublished" : HelperFunctions::formatReadableDate($goal->publish_date);
            $goal_row ['goals_owner'] = $goals_owners;
            $goal_row ['status_all'] = '(Not Updated)';
            if($goal->goal_type==3 || ($goal->goal_type==4 && count($goal->owners)==1)){
                $items_count = !empty($goal->total_items) ? $goal->total_items : $goal->action_items_count;
                if($goal->total_items == $goal->completed_items && $items_count != 0){
                    $goal_row['status_all'] = floor(($goal->completed_items / $items_count) * 100). "% (Completed)";
                }
                if($goal->total_items != $goal->completed_items && $goal->end_date < date('Y-m-d'))
                {
                    $goal_row ['status_all'] = !empty($items_count) ? floor(($goal->completed_items / $items_count) * 100). "% (In Complete)" : "0% (In Complete)";
                }
                if($goal->total_items != $goal->completed_items  && $goal->end_date > date('Y-m-d')){ 
                    $goal_row ['status_all'] = !empty($items_count) ? floor(($goal->completed_items / $items_count) * 100). "% (In Progress)" : "0% (In Progress)";
                }
            }

            $xl_count++;
            $goals [] = $goal_row;
        }
        return $goals;
    }

    public function getOwners($owners){
        $final_owners = [];
        foreach ($owners as $owner) {
            $final_owners [] = $owner->first_name ." ". $owner->last_name;
        }
        return implode(", ", $final_owners);
    }

    public function getGoalsType($type_id){
        $trans_key = $this->goals_type_translation_key[$type_id];
        return $this->translations[$trans_key];
    }

}

?>
