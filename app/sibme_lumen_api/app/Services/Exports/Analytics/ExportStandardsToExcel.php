<?php

namespace App\Services\Exports\Analytics;
// use Maatwebsite\Excel\Facades\Excel;

use GuzzleHttp\Client;
use Aws\S3\S3Client;

/**
 * composer require phpoffice/phpspreadsheet
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExportStandardsToExcel {

    public static function export($site_id, $account_id, $user_id, $subAccount, $start_date, $end_date, $standards, $framework_id, $huddle_type, $file_name) {

        $analytics = [];
        $account_name = \App\Models\Account::getAccountByUserID($account_id, $user_id, $site_id)->company_name;
        // return Excel::download(new ExportHandler($subAccount, $start_date, $end_date, $standards, $framework_id), $account_name . ' - Rubric Analytics.xlsx');
        $excel = new ExportHandler($subAccount, $start_date, $end_date, $standards, $framework_id, $site_id, $huddle_type);
        $excel->download($file_name);
    }

    public static function create_request($site_id, $account_id, $user_id, $subAccount, $start_date, $end_date, $standards, $framework_id, $huddle_type) {
        $account_name = \App\Models\Account::getAccountByUserID($account_id, $user_id, $site_id)->company_name;
        $file_name = self::sanitize_filename($account_name . '-RubricAnalytics-'.date('YmdHis').'.xlsx');
        $params = [
            'account_id' => $account_id,
            'user_id' => $user_id,
            'subAccount' => $subAccount,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'standards' => $standards,
            'framework_id' => $framework_id,
            'huddle_type' => $huddle_type,
            'file_name' => $file_name,
            'site_id' => $site_id
        ];

        $query_string = http_build_query($params);

        exec('wget -O - "http://local.sibme.local/export_analytics_standards_to_excel_async?'.$query_string.'" > /dev/null 2>&1 &');
        //exec('wget -O - "'.  str_replace('http','https',config('s3.sibme_api_url')).'export_analytics_standards_to_excel_async?'.$query_string.'" > /dev/null 2>&1 &');
        return $file_name;
    }

    /**
     * Function: sanitize_filename
     * Returns a sanitized string, typically for URLs.
     *
     * Parameters:
     *     $string - The string to sanitize.
     *     $force_lowercase - Force the string to lowercase?
     *     $alnum - If set to *true*, will remove all non-alphanumeric characters.
     */
    public static function sanitize_filename($string, $force_lowercase = true, $alnum = false) {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
                    "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
                    "â€�?", "â€“", ",", "<", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($alnum) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }

    public static function is_s3_download_ready($file_name){
        $key =  config('s3.export_analytics_path') . $file_name;
 
        // Create an Amazon S3 client object
        $client = new S3Client([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);

        return $client->doesObjectExist(config('s3.bucket_name'), $key) ;
    }

    public static function retrieve_from_s3($file_name){
        $key = config('s3.export_analytics_path') . $file_name;
        $expires = '+240 minutes';

        $client = new S3Client([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);

        try {
            $cmd = $client->getCommand('GetObject', [
                'Bucket' => config('s3.bucket_name'),
                'Key' => $key,
                'ResponseCacheControl' => 'No-cache',
                'ResponseContentDisposition' => 'attachment; filename=' . $file_name
            ]);
            $url = $client->createPresignedRequest($cmd, $expires)->getUri()->__toString();
            return $url;
        } catch (\Aws\S3\Exception\S3Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }
}

class ExportHandler
{
    private $analytics, $account_id;

    public function __construct($subAccount, $start_date, $end_date, $standards, $framework_id, $site_id, $huddle_type)

    {
        $this->account_id = $subAccount;
        $this->framework_id = $framework_id;
        $this->standards = !empty($standards) && $standards!='undefined' ? explode(",", $standards) : [];
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->site_id = $site_id;
        $this->huddle_type = $huddle_type;
    }

    public function download($filename){
/*
        $myfile = fopen(base_path() . '/public/export_analytics/' . "Download2Async.txt", "w") or die("Unable to open file!");
        $txt = "John Doe\n";
        fwrite($myfile, $txt);
        fclose($myfile);

        return true;
*/
        // CREATE A NEW SPREADSHEET + SET METADATA
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
        ->setCreator('Sibme')
        ->setLastModifiedBy('Sibme')
        ->setTitle('Rubric Analytics')
        ->setSubject('Rubric Analytics')
        ->setDescription('Rubric Analytics')
        ->setKeywords('Rubric Analytics')
        ->setCategory('Rubric Analytics');

        // NEW WORKSHEET
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Rubric Analytics');

        // HEADER ROW
        $headings = $this->headings($this->site_id);
        $sheet->fromArray([$headings], NULL, 'A1');

        $this->formatUserStats($sheet, $this->site_id);

        // OUTPUT
        $writer = new Xlsx($spreadsheet);
        /*
        // FORCE DOWNLOAD
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer->save('php://output');
        */

        $dir = config('general.export_analytics_path');
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
        }

        $file_path = $dir . $filename;
        $writer->save($file_path);

        $destination = $this->save_to_s3($file_path);
    }

    public function formatUserStats(&$sheet, $site_id){
        // 1. Get All Users for this Account and Loop through it.
        // 2. Fill user data in sub array.
        //    2.1: get user type of the user.
        //    2.2: get custom markers stats of the user and fill in appropriate column.
        //    2.3: get standards stats of the user and fill in appropriate column.

        $xl_count = 1;

        $users = \App\Models\User::getUsersByAccount($this->account_id, $site_id);
        $customMarkers = \App\Models\AccountTag::getCustomMarkersByAccount($this->account_id, $site_id);
        $standards = \App\Models\AccountTag::getStandards($this->account_id, $this->framework_id, $this->standards, $site_id);

        foreach ($users as $user) {
            $user_row = [];
            $user_row ['name'] = $user->name;
            $user_row ['email'] = $user->email;
            $user_row ['account_name'] = $user->account_name;
            $user_row ['user_role'] = $user->user_role;
            //    2.1: get user type of the user.
            $user_row ['user_type'] = \App\Models\User::get_user_type($user->account_id, $user->user_id, $site_id);

            //    2.2: get custom markers stats of the user and fill in appropriate column.
            foreach ($customMarkers as $customMarker) {
                $account_tag_id = $customMarker->account_tag_id;
                 if(isset($this->huddle_type) && ($this->huddle_type == '2' || $this->huddle_type == '4' ))
                {
                        if($this->huddle_type == '2')
                        {
                            $role_id = 200;
                        }
                        else
                        {
                            $role_id = 210;
                        }
                        
                      $user_account_tags =  \App\Models\AccountCommentTag::where(array('account_tag_id' => $account_tag_id , 'created_by' => $user->user_id ))->get()->toArray();
                        if(!empty($user_account_tags) || $role_id == 210){

                        $count = \App\Models\AccountCommentTag::getUserParticipationCountByTagCoachCoachee($account_tag_id, $user->user_id, $this->start_date, $this->end_date, $site_id, $role_id);
                        
                        }
                        
                        else
                        {
                            $count = 0;
                        }
                }
                        else
                        {
                        $count = \App\Models\AccountCommentTag::getUserParticipationCountByTag($account_tag_id, $user->user_id, $this->start_date, $this->end_date, $site_id);
                        }
                $user_row ['cm_'.$account_tag_id] = empty($count) ? '0' : $count;
            }

            //    2.3: get standards stats of the user and fill in appropriate column.
            foreach ($standards as $standard) {
                $account_tag_id = $standard->account_tag_id;
                
                if(isset($this->huddle_type) && ($this->huddle_type == '2' || $this->huddle_type == '4' ))
                {
                        if($this->huddle_type == '2')
                        {
                            $role_id = 200;
                        }
                        else
                        {
                            $role_id = 210;
                        }
                        
                      $user_account_tags =  \App\Models\AccountCommentTag::where(array('account_tag_id' => $account_tag_id , 'created_by' => $user->user_id ))->get()->toArray();
                        if(!empty($user_account_tags) || $role_id == 210){

                        $count = \App\Models\AccountCommentTag::getUserParticipationCountByTagCoachCoachee($account_tag_id, $user->user_id, $this->start_date, $this->end_date, $site_id, $role_id);
                        
                        }
                        
                        else
                        {
                            $count = 0;
                        }
                }
                
                else{
                    
                        $count = \App\Models\AccountCommentTag::getUserParticipationCountByTag($account_tag_id, $user->user_id, $this->start_date, $this->end_date, $site_id);
                
                
                }

                $user_row ['st_'.$account_tag_id] = empty($count) ? '0' : $count;
            }

            $xl_count++;
            $sheet->fromArray([$user_row], NULL, 'A'.$xl_count);
        }

    }

    public function getTags($tags){
        $custom_framework_tags = [];
        $custom_marker_tags = [];

        foreach ($tags as $tag) {
            if($tag['ref_type'] === 0){
                $custom_framework_tags [] = $tag['tag_title'];
            }
            if($tag['ref_type'] === 1){
                $custom_marker_tags [] = $tag['tag_title'];
            }
        }

        return ['custom_framework_tags'=>implode(", ", $custom_framework_tags), 
                'custom_marker_tags'=> implode(", ", $custom_marker_tags)];
    }

    public function getCustomMarkersHeadings($site_id){
        $customMarkers = \App\Models\AccountTag::getCustomMarkersByAccount($this->account_id, $site_id);
        $headings = [];
        foreach ($customMarkers as $customMarker) {
            $headings [] = $customMarker->tag_title;
        }
        return $headings;
    }
    
    public function getStandardsHeadings($site_id){
        $standards = \App\Models\AccountTag::getStandards($this->account_id, $this->framework_id, $this->standards, $site_id);
        $headings = [];
        foreach ($standards as $standard) {
            $headings [] = !empty($standard->standard_analytics_label) ? $standard->standard_analytics_label : mb_strimwidth(trim($standard->tag_code ." ". $standard->tag_title), 0, 252, "...");
        }
        return $headings;
    }
    
    public function headings($site_id)
    {
        $userHeadings = [
            'Name',
            'Email',
            'Account Name',
            'User Role',
            'User Type',
        ];
        // Adding Custom Marker Headings at the end of the user headings.
        $customMarkerHeadings = $this->getCustomMarkersHeadings($site_id);
        // Adding Standard Headings at the end of the Custom Marker Headings.
        $standardsHeadings = $this->getStandardsHeadings($site_id);

        return array_merge($userHeadings, $customMarkerHeadings, $standardsHeadings);
    }

    public function save_to_s3($file){
        $path_parts = pathinfo($file);
        $new_destination =  config('s3.export_analytics_path') . $path_parts['filename'] . '.' . $path_parts['extension'];
 
        // Create an Amazon S3 client object
        $client = new S3Client([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);

        $client->putObject(array(
            'Bucket' => config('s3.bucket_name'),
            'Key' => $new_destination,
            'SourceFile' => $file
        ));

        return $new_destination;
    }


}

?>
