<?php

namespace App\Services\Exports\Assessees;
use Aws\S3\S3Client;
class ExportToPDF {
    public function export($site_id, $account_id, $huddle_id, $current_user_id, $current_user_role, $translations,$header_details=array()) {
        $assesseeList = new AssesseesList($site_id, $account_id, $huddle_id, $current_user_id, $current_user_role, $translations);
        $headings = $assesseeList->getHeadingsToExport();
        $assessees = $assesseeList->getAssesseesToExport();
        $assessees = $this->formatAssessees($assessees, $translations);

        $contents = view('export/assessees_pdf', compact('headings', 'assessees','header_details'))->render();
/*
        $tidy_config = array(
            //'clean' => true,
            'output-xhtml' => true,
            'show-body-only' => true,
            'wrap' => 0,
            //'fix-style-tags'=>false
        );
        
        $tidy = tidy_parse_string( $contents, $tidy_config, 'UTF8');
        $tidy->cleanRepair();
        
        $contents = $tidy->value;
        $contents = str_replace('<s>', '<p>', $contents);
        $contents = str_replace('</s>', '</p>', $contents);
 */
        $pdf = app()->make('dompdf.wrapper');

        $pdf->loadHTML($contents);
        $file_name =  isset($translations['file_name'])?$translations['file_name']:'document.pdf';
        //return $pdf->download($file_name);


       $dir = config('general.export_analytics_path');
       if (!file_exists($dir)) {
           mkdir($dir, 0777, true);
       }

       $file_path = $dir . $file_name;
       $pdf->save($file_path);
       //$writer->save($file_path);
       $destination = $this->save_to_s3($file_path);
       //return $pdf->download($file_name);
       //app('App\Http\Controllers\ReportingController')->send_reports_export_email($filename,$user_id);

    }

    public function formatAssessees($assessees_list, $translations){
        $assessees = [];
        foreach ($assessees_list as $assessee) {
            $assessee_row = [];
            $assessee_row ['artifact_assessee'] = $assessee['user_name'] ." ( ".  $assessee['user_email'] ." )";
            $assessee_row ['artifacts_last_modified'] = $assessee['last_modified'];
            $assessee_row ['artifacts_last_submission'] = $assessee['last_submission_with_time'];
            $assessee_row ['huddles_details_videos'] = $assessee['videos_count'];
            $assessee_row ['artifact_recources'] = $assessee['resources_count'];
            $assessee_row ['artifact_comments'] = $assessee['comments_count'];
            $assessee_row ['cf_assessed_status'] = empty($assessee['assessed']) ? $translations['cf_not_assessed'] : $translations['assessed_tracker_ng'];
            $assessee_row ['avg_performance_level'] = $assessee['avg_performance_level_rating'];
            $customFieldsData = array_column($assessee['custom_fields_data'], 'assessment_custom_field_value');

            $assessees [] = array_merge($assessee_row, $customFieldsData);
        }

        return $assessees;
    }

    public function save_to_s3($file){
        $path_parts = pathinfo($file);
        $new_destination =  config('s3.export_analytics_path') . $path_parts['filename'] . '.' . $path_parts['extension'];
 
        // Create an Amazon S3 client object
        $client = new S3Client([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);

        $client->putObject(array(
            'Bucket' => config('s3.bucket_name'),
            'Key' => $new_destination,
            'SourceFile' => $file
        ));

        return $new_destination;
    }

    

    
}

?>
