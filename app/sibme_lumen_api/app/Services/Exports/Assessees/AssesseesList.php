<?php

namespace App\Services\Exports\Assessees;

use App\Models\AccountFolderUser;
use App\Models\AssessmentCustomField;

class AssesseesList {

    public $site_id; 
    public $huddle_id; 
    public $current_user_id; 
    public $current_user_role; 
    protected $assesseesToExport = [];
    public function __construct($site_id, $account_id, $huddle_id, $current_user_id, $current_user_role, $translations){
        $this->site_id = $site_id; 
        $this->account_id = $account_id; 
        $this->huddle_id = $huddle_id; 
        $this->current_user_id = $current_user_id; 
        $this->current_user_role = $current_user_role; 
        $this->translations = $translations; 
    }

    public function getHeadingsToExport(){
        $translations = $this->translations;
        $headings = [
            $translations['artifact_assessee'],
            $translations['artifacts_last_modified'],
            $translations['artifacts_last_submission'],
            $translations['huddles_details_videos'],
            $translations['artifact_recources'],
            $translations['artifact_comments'],
            $translations['cf_assessed_status'],
            $translations['cf_avg_performance_level']
        ];
        // Adding Custom Fields at the end of the headings.
        $custom_fields_headings = $this->getCustomFieldsHeadings();
        return array_merge($headings, $custom_fields_headings);
    }

    public function getCustomFieldsHeadings(){
        $headings = AssessmentCustomField::get_assessment_custom_fields($this->account_id, $this->huddle_id, true);
        return array_column($headings, 'field_label');
    }

    public function getAssesseesToExport() {
        $assessees = AccountFolderUser::getAssesseesList($this->site_id, $this->huddle_id, $this->current_user_id, $this->current_user_role);
         foreach ($assessees as $key => $assessee) {
             foreach ($assessee['custom_fields_data'] as $key2 => $datum)
             {
                 if($datum->field_type == 4)//Yes/No
                 {
                     $value = $datum->assessment_custom_field_value;
                     if($datum->assessment_custom_field_value === "true" || $datum->assessment_custom_field_value === true)
                     {
                         $value = "Yes";
                     }
                     else if($datum->assessment_custom_field_value === "false" || $datum->assessment_custom_field_value === false)
                     {
                         $value = "No";
                     }
                     $assessees[$key]['custom_fields_data'][$key2]->assessment_custom_field_value = $value;
                 }
             }
         }
        return $assessees;
    }
}

?>
