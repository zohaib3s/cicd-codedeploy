<?php

namespace App\Services\Exports\Assessees;
// use Maatwebsite\Excel\Facades\Excel;

/**
 * composer require phpoffice/phpspreadsheet
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Aws\S3\S3Client;
class ExportToExcel {

    public function export($site_id, $account_id, $huddle_id, $current_user_id, $current_user_role, $translations) {
        $assesseeList = new AssesseesList($site_id, $account_id, $huddle_id, $current_user_id, $current_user_role, $translations);
        $headingsToExport = $assesseeList->getHeadingsToExport();
        $assesseesToExport = $assesseeList->getAssesseesToExport();
        $excel = new ExportHandler($headingsToExport, $assesseesToExport, $translations);
        $file_name =  isset($translations['file_name'])?$translations['file_name']:'assessees.xlsx';
        $excel->download($file_name);
    }
}

class ExportHandler{

    private $headings;
    private $assessees;
    private $translations;
    private $title = "Assessees";
    public function __construct($headings, $assessees, $translations)
    {
        $this->headings = $headings;
        $this->assessees = $assessees;
        $this->translations = $translations;
    }

    public function download($filename){
        // CREATE A NEW SPREADSHEET + SET METADATA
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
        ->setCreator('Sibme')
        ->setLastModifiedBy('Sibme')
        ->setTitle($this->title)
        ->setSubject($this->title)
        ->setDescription($this->title)
        ->setKeywords($this->title)
        ->setCategory($this->title);

        // NEW WORKSHEET
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle($this->title);

        // HEADER ROW
        // $headings = $this->headings($translations);
        $headings = $this->headings;
        $sheet->fromArray([$headings], NULL, 'A1');

        $this->formatAssessees($sheet);

        // OUTPUT
        $writer = new Xlsx($spreadsheet);


        $file_name =  $filename;

        //$file_name =  isset($translations['file_name'])?$translations['file_name']:'document.pdf';
        //return $pdf->download($file_name);


       $dir = config('general.export_analytics_path');
       if (!file_exists($dir)) {
           mkdir($dir, 0777, true);
       }

       $file_path = $dir . $file_name;
       $writer->save($file_path);
       $destination = $this->save_to_s3($file_path);


        // FORCE DOWNLOAD
        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header('Content-Disposition: attachment;filename="'.$filename.'"');
        // header('Cache-Control: max-age=0');
        // header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        // header('Cache-Control: cache, must-revalidate');
        // header('Pragma: public');
        // $writer->save('php://output');
    }

    public function formatAssessees(&$sheet){
        $xl_count = 1;
        foreach ($this->assessees as $assessee) {
            $assessee_row = [];
            $assessee_row ['artifact_assessee'] = $assessee['user_name'] ." ". $assessee['user_email'];
            $assessee_row ['artifacts_last_modified'] = $assessee['last_modified'];
            $assessee_row ['artifacts_last_submission'] = $assessee['last_submission_with_time'];
            $assessee_row ['huddles_details_videos'] = $assessee['videos_count'];
            $assessee_row ['artifact_recources'] = $assessee['resources_count'];
            $assessee_row ['artifact_comments'] = $assessee['comments_count'];
            $assessee_row ['cf_assessed_status'] = empty($assessee['assessed']) ? $this->translations['cf_not_assessed'] : $this->translations['assessed_tracker_ng'];
            $assessee_row ['avg_performance_level'] = $assessee['avg_performance_level_rating'];
            $customFieldsData = array_column($assessee['custom_fields_data'], 'assessment_custom_field_value');
            $customFieldsData = array_map(function($v){
                return str_replace('&nbsp;', ' ',strip_tags($v));
            }, $customFieldsData);
            $assessee_row = array_merge($assessee_row, $customFieldsData);
            $xl_count++;
            $sheet->fromArray([$assessee_row], NULL, 'A'.$xl_count);
        }
    }

    public function save_to_s3($file){
        $path_parts = pathinfo($file);
        $new_destination =  config('s3.export_analytics_path') . $path_parts['filename'] . '.' . $path_parts['extension'];
 
        // Create an Amazon S3 client object
        $client = new S3Client([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);

        $client->putObject(array(
            'Bucket' => config('s3.bucket_name'),
            'Key' => $new_destination,
            'SourceFile' => $file
        ));

        return $new_destination;
    }

}

?>
