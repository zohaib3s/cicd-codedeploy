<?php

namespace App\Services\Exports\AssessmentReport;
class ExportToPDF {
     public function export($headings,$assessment_reports,$account_name) { 
        $contents = view('export/assessment_reports_pdf', compact('headings','assessment_reports'))->render();
        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadHTML($contents);
        $today = date("m-d-y");
        return $pdf->download($account_name."_assessment_reports_".$today .'.pdf');
    }
}

?>
