<?php

namespace App\Services\Exports\UserSummary;

class ExportToPDF {
     public function export($headings,$user_summary,$account_name) { 

        $contents = view('export/user_summary_pdf', compact('headings', 'user_summary'))->render();
 
        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadHTML($contents);
        $today = date("m-d-y");
        return $pdf->download($account_name."_user_summary_".$today .'.pdf');
    }
}

?>
