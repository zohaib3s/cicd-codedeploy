<?php

namespace App\Services\Exports\DetailView;

class ExportToPDF {
     public function export($headings,$detail_views,$account_name) { 
        
        $contents = view('export/detail_view_pdf', compact('headings', 'detail_views'))->render();
 
        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadHTML($contents);
        $today = date("m-d-y");
        return $pdf->download($account_name."_detail_view_".$today .'.pdf');
    }
}

?>
