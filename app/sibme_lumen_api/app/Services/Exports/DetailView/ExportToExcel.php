<?php

namespace App\Services\Exports\DetailView;
// use Maatwebsite\Excel\Facades\Excel;

/**
 * composer require phpoffice/phpspreadsheet
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExportToExcel {

    public function export($headings,$data,$account_name) {
        $excel = new ExportHandler($headings, $data);
        $today = date("m-d-y");
        $excel->download($account_name."_detail_view_".$today .'.xlsx');
    }
}

class ExportHandler{

    private $headings;
    private $detail_views;
    private $title = "detail_view_report";
    public function __construct($headings, $detail_views)
    {
        $this->headings = $headings;
        $this->detail_views = $detail_views;
    }

    public function download($filename){
        // CREATE A NEW SPREADSHEET + SET METADATA
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
        ->setCreator('Sibme')
        ->setLastModifiedBy('Sibme')
        ->setTitle($this->title)
        ->setSubject($this->title)
        ->setDescription($this->title)
        ->setKeywords($this->title)
        ->setCategory($this->title);

        // NEW WORKSHEET
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle($this->title);

        // HEADER ROW
        // $headings = $this->headings($translations);
        $headings = $this->headings;
        $sheet->fromArray([$headings], NULL, 'A1');

        $this->formatAssessees($sheet);

        // OUTPUT
        $writer = new Xlsx($spreadsheet);
        // FORCE DOWNLOAD
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer->save('php://output');
    }

    public function formatAssessees(&$sheet){
        $xl_count = 1;
        foreach ($this->detail_views as  $detail_view) {
               $xl_count++;
            $sheet->fromArray($detail_view, NULL, 'A'.$xl_count,true);
        }
    }

}

?>
