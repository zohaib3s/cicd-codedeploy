<?php

namespace App\Services\Exports\Transcriptions;

class ExportPDFTranscriptions {

    public function export($site_id, $subtitles,$header_details = array()) {
        $contents = view('export/transcription_pdf', compact('subtitles','header_details'))->render();        
        $tidy_config = array(
            //'clean' => true,
            'output-xhtml' => true,
            'show-body-only' => true,
            'wrap' => 0,
            //'fix-style-tags'=>false
        );
        
        $tidy = tidy_parse_string( $contents, $tidy_config, 'UTF8');
        $tidy->cleanRepair();
        
        $contents = $tidy->value;
        $contents = str_replace('<s>', '<p>', $contents);
        $contents = str_replace('</s>', '</p>', $contents);
 
        @ini_set("memory_limit", "1000M");
        @ini_set('max_execution_time',120);
        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadHTML($contents);
        return $pdf->download();
        // return $pdf->stream();
    }
}

?>
