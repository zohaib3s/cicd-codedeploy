<?php

namespace App\Services\Exports\Transcriptions;

// use Maatwebsite\Excel\Facades\Excel;

/**
 * composer require phpoffice/phpspreadsheet
 */
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExportExcelTranscriptions {

    public function export($site_id, $subtitles) {
        $excel = new ExportHandler($subtitles, $site_id);
        $excel->download('transcriptions.xlsx');
    }

}

class ExportHandler {

    private $subtitles;
    private $site_id;

    public function __construct($subtitles, $site_id) {
        $this->subtitles = $subtitles;
        $this->site_id = $site_id;
    }

    public function download($filename) {
        // CREATE A NEW SPREADSHEET + SET METADATA
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
                ->setCreator('Sibme')
                ->setLastModifiedBy('Sibme')
                ->setTitle('Transcriptions')
                ->setSubject('Transcriptions')
                ->setDescription('Transcriptions')
                ->setKeywords('Transcriptions')
                ->setCategory('Transcriptions');

        // NEW WORKSHEET
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Transcriptions');

        // HEADER ROW
        $headings = [
            'time_from',
            'time_to',
            'subtitles',
        ];
        ;
        $sheet->fromArray([$headings], NULL, 'A1');

        $this->formatTranscriptions($sheet, $this->site_id);

        // OUTPUT
        $writer = new Xlsx($spreadsheet);
        // FORCE DOWNLOAD
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer->save('php://output');
    }

    public function formatTranscriptions(&$sheet, $site_id) {
        $xl_count = 1;
        foreach ($this->subtitles as $subtitle) {

            $subtitle_row = [];

            $subtitle_row ['time_from'] = substr($subtitle ['time_range'], 0, 12);
            $subtitle_row ['time_to'] = substr($subtitle['time_range'],strpos($subtitle['time_range'], "-> ")+2);
            $subtitle_row ['subtitles'] = $subtitle ['subtitles'];
            $xl_count++;
            $sheet->fromArray([$subtitle_row], NULL, 'A' . $xl_count);
        }
    }

    public function headings($translations) {
        $headings = [
            $translations['time_from'],
            $translations['time_to'],
            $translations['subtitles'],
        ];
        // return $headings;
        return $headings;
    }

}

?>
