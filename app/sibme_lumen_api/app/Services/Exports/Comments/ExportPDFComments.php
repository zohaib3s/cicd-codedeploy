<?php

namespace App\Services\Exports\Comments;
use Illuminate\Support\Facades\Storage;
class ExportPDFComments extends ExportComments {

    public function export($site_id, $video_id, $sort_by, $huddle_id, $account_id, $user_id,$translations,$header_details=array()) { 
        $this->getCommentsToExport($site_id, $video_id, $sort_by,  $huddle_id, $account_id, $user_id,$translations,$header_details);        
        $contents = view('export/comments_pdf', $this->commentsToExport)->render();        
        $tidy_config = array(
            //'clean' => true,
            'output-xhtml' => true,
            'show-body-only' => true,
            'wrap' => 0,
            //'fix-style-tags'=>false
        );
        
        
        $tidy = tidy_parse_string( $contents, $tidy_config, 'UTF8');
        $tidy->cleanRepair();
        
        $contents = $tidy->value;
        $contents = str_replace('<s>', '<p>', $contents);
        $contents = str_replace('</s>', '</p>', $contents);
        //echo $contents;die;
        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadHTML($contents);
        return $pdf->download();
        // return $pdf->stream();
    }

    function export_attachment($site_id, $video_id, $sort_by, $huddle_id, $account_id, $user_id,$translations,$header_details=array(),$file_name){
        $this->getCommentsToExport($site_id, $video_id, $sort_by,  $huddle_id, $account_id, $user_id,$translations,$header_details);        
        $contents = view('export/comments_pdf', $this->commentsToExport)->render();        
        $tidy_config = array(
            //'clean' => true,
            'output-xhtml' => true,
            'show-body-only' => true,
            'wrap' => 0,
            //'fix-style-tags'=>false
        );       
        
        $tidy = tidy_parse_string( $contents, $tidy_config, 'UTF8');
        $tidy->cleanRepair();        
        $contents = $tidy->value;
        $contents = str_replace('<s>', '<p>', $contents);
        $contents = str_replace('</s>', '</p>', $contents);        
        $pdf = app()->make('dompdf.wrapper');
        $path = base_path() . "/public/attachments/$file_name".'_'.time().'.pdf';
        $pdf->loadHTML($contents)->save( $path);
        return $path;
    }
}

?>
