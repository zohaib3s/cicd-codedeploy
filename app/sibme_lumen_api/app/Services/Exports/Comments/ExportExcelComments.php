<?php

namespace App\Services\Exports\Comments;
// use Maatwebsite\Excel\Facades\Excel;

/**
 * composer require phpoffice/phpspreadsheet
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExportExcelComments extends ExportComments {

    public function export($site_id, $video_id, $sort_by, $huddle_id, $account_id, $user_id,$translations) {
        $this->getCommentsToExport($site_id, $video_id, $sort_by, $huddle_id, $account_id, $user_id,$translations);
        $excel = new ExportHandler($this->commentsToExport, $site_id);
        $excel->download('comments.xlsx',$translations);
    }
}

class ExportHandler{

    private $comments;
    private $site_id;
    public function __construct($comments, $site_id)
    {
        $this->comments = $comments['videoComments'];
        $this->site_id = $site_id;
    }

    public function download($filename,$translations){
        // CREATE A NEW SPREADSHEET + SET METADATA
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()
        ->setCreator('Sibme')
        ->setLastModifiedBy('Sibme')
        ->setTitle('Comments')
        ->setSubject('Comments')
        ->setDescription('Comments')
        ->setKeywords('Comments')
        ->setCategory('Comments');

        // NEW WORKSHEET
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Comments');

        // HEADER ROW
        $headings = $this->headings($this->site_id,$translations);
        $sheet->fromArray([$headings], NULL, 'A1');

        $this->formatComments($sheet, $this->site_id);

        // OUTPUT
        $writer = new Xlsx($spreadsheet);
        // FORCE DOWNLOAD
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$filename.'"');
        header('Cache-Control: max-age=0');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer->save('php://output');
    }

    public function formatComments(&$sheet, $site_id){
        $xl_count = 1;
        foreach ($this->comments as $comment) {
            if(is_null($comment['parent_comment_id'])){
                $comment_row = [];

                $post_time = '';
                if(!empty($comment ['time'])){
                    $post_time = gmdate("H:i:s", $comment ['time']) ;
                    if(!empty($comment ['end_time'])){
                        $post_time .= " - ".gmdate("H:i:s", $comment ['end_time']) ;
                    }
                }else{
                    $post_time = 'All';
                }
                $comment_row ['created_date'] = date('m/d/Y',strtotime($comment ['created_date']));
                //$comment_row ['time'] = !empty($comment ['time']) ? gmdate("H:i:s", $comment ['time']) : 'ALL' .!empty($comment ['end_time']) ? " - ".gmdate("H:i:s", $comment ['end_time']):'';
                $comment_row ['time'] =  $post_time ;
                $comment_row ['username'] = $comment ['first_name'] .' '. $comment ['last_name'];
                $comment_row ['comment'] = $comment ['comment'];

                $tags = $this->getTags($comment ['tags']);
                $comment_row ['custom_framework_tags'] = $tags ['custom_framework_tags'];
                $comment_row ['custom_marker_tags'] = $tags ['custom_marker_tags'];

                $comment_row ['attachments'] = $comment ['attachments'];

                $replies = $this->getReplies($comment);

                $comment_row = array_merge($comment_row, $replies);
                $xl_count++;
                $sheet->fromArray([$comment_row], NULL, 'A'.$xl_count);
            }
        }
    }

    public function getTags($tags){
        $custom_framework_tags = [];
        $custom_marker_tags = [];

        foreach ($tags as $tag) {
            if($tag['ref_type'] === 0){
                $custom_framework_tags [] = $tag['tag_title'];
            }
            if($tag['ref_type'] === 1){
                $custom_marker_tags [] = $tag['tag_title'];
            }
        }

        return ['custom_framework_tags'=>implode(", ", $custom_framework_tags), 
                'custom_marker_tags'=> implode(", ", $custom_marker_tags)];
    }

    public function getReplies($comment){

/*
        $replies = \App\Models\Comment::getCommentReplies($comment_id);
        $comment_replies = [];
        foreach ($replies as $key=>$reply) {
            $i = $key+1;
            $comment_replies ['reply_'.$i] = $reply->user_name .': '.$reply->comment;
        }

        return $comment_replies;
*/
        // Getting Replies
        if(isset($comment['replies']))
        {
        $replies_array = $comment['replies'];
        }
        else
        {
         $replies_array = array();   
        } 

        $replies = [];
        foreach ($replies_array as $reply) {
            
                $size = sizeof($replies)+1;
                $replies ['reply_'.$size] = $reply ['first_name'] .' '. $reply ['last_name'] .': '.$reply ['comment'];

                // Getting Replies to Reply

                if(isset($reply['replyofreply']))
                {
                $replies_of_replies_array = $reply['replyofreply'];
                }
                else
                {
                 $replies_of_replies_array = array();   
                }


                foreach ($replies_of_replies_array as $reply_of_reply) {
                   
                        $size = sizeof($replies)+1;
                        $replies ['reply_'.$size] = $reply_of_reply ['first_name'] .' '. $reply_of_reply ['last_name'] .': '.$reply_of_reply ['comment'];
                    
                }

            
        }
        return $replies;

    }

    public function getRepliesHeadings($translations){
        /*
        $parents = [];
        foreach ($this->comments as $comment) {
            if(!is_null($comment['parent_comment_id'])){
                if(isset($parents [ $comment['parent_comment_id'] ])){
                    $parents [ $comment['parent_comment_id'] ] += 1;
                } else {
                    $parents [ $comment['parent_comment_id'] ] = 1;
                }                
            }
        }
        return empty($parents) ? 0 : max($parents);
        */

        $replies = $replies_headings = [];
        foreach ($this->comments as $comment) {
            $replies [] = count($this->getReplies($comment));
        }
        $max = empty($replies) ? 0 : max($replies);
        if($max>0){
            for ($col=1; $col <= $max; $col++) { 
                $replies_headings [] = $translations['reply'].' '.$col;
            }
        }

        return $replies_headings;
    }

    public function headings($site_id,$translations)
    {
        // Adding Replies at the end of the headings.
        $replies_headings = $this->getRepliesHeadings($translations);
        
        $headings = [
            $translations['date_stamp_export'],
            $translations['time_stamp_export'],
            $translations['username_export'],
            $translations['comment_export'],
            $translations['custom_framework_tags'],
            $translations['custom_marker_tags'],
            $translations['attachment_name']
        ];
        // return $headings;
        return array_merge($headings, $replies_headings);
    }

}

?>
