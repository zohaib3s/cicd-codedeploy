<?php

namespace App\Services\Exports\Comments;
use App\Models\AccountCommentTag;
use App\Models\AccountFolder;
use App\Models\AccountFolderMetaData;
use App\Models\AccountFolderUser;
use App\Models\Comment;

class ExportComments {

    protected $commentsToExport = [];
    public function getCommentsToExport($site_id, $video_id, $sort_by, $huddle_id, $account_id, $user_id,$translations,$header_details=array()) {

        $huddle = AccountFolder::getHuddle($huddle_id, $account_id);

        $h_type = AccountFolderMetaData::where('account_folder_id', $huddle_id)
                                       ->where('meta_data_name', 'folder_type')
                                       ->where('site_id', $site_id)
                                       ->first();
        $h_type = isset($h_type->meta_data_value) ? $h_type->meta_data_value : "1";

        if ((!AccountFolderUser::check_if_evalutor($site_id, $huddle_id, $user_id)) && (($h_type == '2' && AccountFolderMetaData::is_enabled_coach_feedback($huddle_id, $site_id)) || $h_type == '3')) {
            // Getting Active Comments Here
            $active = true;
            $result = Comment::getVideoComments($site_id, $video_id, $sort_by, '', '', '', 0, 0, true, $active);
        } else {
            $result = Comment::getVideoComments($site_id, $video_id, $sort_by, '', '', '', 0, 0, true);
        }

        $videoComments = '';
        if (is_array($result) && count($result) > 0) {
            $videoComments = $result;
        }
        $params = array(
            'user_id' => $user_id,
            'video_id' => $video_id,
            'videoComments' => $videoComments,
            'header_details'=>$header_details,
            'huddle' => $huddle
        );

        $comments = array();
        if (!empty($videoComments)) {
            foreach ($videoComments as $comment) {
                $get_standard = AccountCommentTag::gettagsbycommentid($site_id, $comment['id'], ['0']); //get standards
                $get_tags = AccountCommentTag::gettagsbycommentid($site_id, $comment['id'], [1, 2]); //get tags
                $comments[] = array_merge($comment, array("tags" => array_merge($get_standard, $get_tags)));
            }
        }
        $parentcomments = array();
        if(!empty($comments)){
            foreach ($comments as $key => $value) {
                if(trim($value["parent_comment_id"]) == ''){
                    $parentcomments[$value["id"]] = $value;
                }
            }
        }
        $copyparentcomments = array();
        if (!empty($parentcomments)) {
            $copyparentcomments = $parentcomments;
            foreach ($parentcomments as $key => $value) {
                foreach ($comments as $k => $val) {
                    if($key == $val["parent_comment_id"]){
                        $copyparentcomments[$key]['replies'][] = $val;
                    }
                }
            }
        }
        if (!empty($copyparentcomments)) {
            foreach ($copyparentcomments as $key => $value) {
                if(isset($value['replies'])){
                    foreach ($value['replies'] as $k => $val) {
                        foreach ($comments as $index => $data) {
                            if($val['id'] == $data['parent_comment_id']){
                                $copyparentcomments[$key]['replies'][$k]['replyofreply'][] = $data;
                            }
                        }
                    }
                }
            }
        }
        
        $this->commentsToExport = [
                                        'video_id' => $video_id,
                                        'user_id' => $user_id,
                                        'videoComments' => $copyparentcomments,
                                        'huddle' => $huddle,
                                        'header_details'=>$header_details,
                                        'translations' => $translations
                                  ];
    }
}

?>
