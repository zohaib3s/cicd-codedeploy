<?php
namespace App\Services;

use Illuminate\Support\Facades\DB;

/**
 * Usage:
 *         $cp = new \App\Services\CodeProfiler_Old;
 *         $cp->p_open('flag-1');
 *         // some code.
 *         $cp->p_close('flag-1');
 *         $cp->p_open('flag-2');
 *         // some code.
 *         $cp->p_close('flag-2');
 *         \Illuminate\Support\Facades\Log::info("- Profile -", $cp->p_dump());
 */

class CodeProfiler_Old {
    private $p_times = [];
    public function p_open($flag) {
        if (null === $this->p_times)
            $this->p_times = [];
        if (! array_key_exists($flag, $this->p_times))
            $this->p_times[$flag] = [ 'total' => 0, 'open' => 0 ];
        $this->p_times[$flag]['open'] = microtime(true);
    }
    
    public function p_close($flag)
    {
        if (isset($this->p_times[$flag]['open'])) {
            $this->p_times[$flag]['total'] += (microtime(true) - $this->p_times[$flag]['open']);
            unset($this->p_times[$flag]['open']);
        }
    }
    
    public function p_dump()
    {
        $dump = [];
        $sum  = 0;
        $total_elapsed = 0;
        foreach ($this->p_times as $flag => $info) {
            $info['total'] = $info['total']/2;
            $dump[$flag]['elapsed'] = $info['total'];
            $total_elapsed += $dump[$flag]['elapsed'];
            $sum += $info['total'];
        }
        foreach ($dump as $flag => $info) {
            $dump[$flag]['percent'] = $dump[$flag]['elapsed']/$sum;
        }
        $dump['total_elapsed']['original'] = $total_elapsed;
        $dump['total_elapsed']['seconds'] = number_format($total_elapsed,3);
        return $dump;
    }

}