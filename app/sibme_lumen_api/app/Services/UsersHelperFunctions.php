<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Models\Account;
use App\Models\AccountFolderGroup;
use App\Models\AccountFolderMetaData;
use App\Models\AccountFolderUser;
use App\Models\AccountFolder;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UsersHelperFunctions {

    function __construct() {

    }
    public static function _library_access($function, $role_id, $permissions) {
        if ($function == 'index' || $function == 'view' || $function == 'update_view_count' || $function == 'getAllVideos' || $function == 'getVideosByTitle' || $function == 'getSubjectVideos' || $function == 'getTopicVideos' || $function == 'addSubject' || $function == 'addTopic' || $function == 'delete') {
            if ($permissions["permission_access_video_library"] == TRUE) {
                return TRUE;
            } else {
                return self::no_permissions();
            }
        } elseif ($function == 'add' || $function == 'edit') {

            if ($role_id == '110' || $role_id == '100' || $permissions["permission_video_library_upload"] == '1') {
                return TRUE;
            } else {
                return self::no_permissions();
            }
        }
    }

    public static function _people_access($function, $permissions) {

        if ($function == 'administrators_groups' || $function == 'addUsers') {
            if ($permissions['permission_administrator_user_new_role'] == '1') {
                return TRUE;
            } else {
                return self::no_permissions();
            }
        }
    }

    public static function _permission_access($function, $permissions) {

        if ($function == 'all' && $permissions) {
            if ($permissions['permission_administrator_user_new_role'] == '1') {
                return TRUE;
            } else {
                return self::no_permissions();
            }
        }
    }

    public static function _permissions_on_huddle($function, $user, $current_user_role, $created_by_id, $current_user_id) {

        if ($function == 'add' && $user) {
            if ($user->role_id != '120' || $user->folders_check == '1' || $user->manage_collab_huddles == '1' || $user->manage_coach_huddles == '1' || $user->manage_evaluation_huddles == '1') {
                return TRUE;
            } else {
                return self::no_permissions();
            }
        } elseif ($function == 'edit' || $function == 'download' || $function == 'downloadZip' || $function == 'upload' || $function == 'delete' || $function == 'changeTitle' || $function == 'deleteHuddleVideo' || $function == 'deleteComment' || $function == 'editComment' || $current_user_role || $created_by_id || $current_user_id) {

            if ($current_user_role == 200 || (self::is_creator($current_user_id, $created_by_id) && $user['users_accounts']['permission_maintain_folders'] == '1')) {
                return TRUE;
            } else if ($current_user_role == 210 && self::is_creator($current_user_id, $created_by_id)) {
                return TRUE;
            } else {
                return self::no_permissions();
            }
        } elseif ($function == 'comments' && $user || $current_user_role || $created_by_id || $current_user_id) {
            if ($current_user_role == 200 || $current_user_role == 210 || (self::is_creator($current_user_id, $created_by_id) && $user->permission_maintain_folders == '1')) {
                return TRUE;
            } else {
                return self::no_permissions();
            }
        }
    }

    public static function is_creator($created_by_id, $user_id) {
        if ($created_by_id == $user_id) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public static function no_permissions() {
        return FALSE;
        //$this->redirect('/NoPermissions/no_permission');
    }

    public static function huddle_check_on_permission_page($huddle_id, $user_id, $role_id) {

        $result = AccountFolderMetaData::where(array(
            'meta_data_name' => 'folder_type',
            'account_folder_id' => $huddle_id
        ))->first();

        $huddle_details = AccountFolder::where('account_folder_id', $huddle_id)->first();

        if ($result->meta_data_value == '2') {

            $current_coachee = AccountFolderUser::where(array(
                'user_id' => $user_id,
                'account_folder_id' => $huddle_id,
                'role_id' => '210'
            ))->first();

            $any_other_coachee = AccountFolderUser::where(array(
                    'account_folder_id' => $huddle_id,
                    'role_id' => '210'
            ))->count();

            if (empty($current_coachee) && $any_other_coachee > 0 && $role_id == 210) {
                return array(
                    'success' => true,
                    'message' => 'Huddle "' . $huddle_details->name . '" cannot have more than 1 Coachee'
                );
            }

            if (!empty($current_coachee) && $any_other_coachee == 1 && $role_id == 200) {
                return array(
                    'success' => true,
                    'message' => 'Huddle "' . $huddle_details->name . '" should have atleast one Coachee.'
                );
            }

            $coach_number =AccountFolderUser::where(array(
                    'account_folder_id' => $huddle_id,
                    'role_id' => '200'
                ))->count();

            $current_coach = AccountFolderUser::where(array(
                    'account_folder_id' => $huddle_id,
                    'role_id' => '200',
                    'user_id' => $user_id
                ))->first();


            if ($coach_number == 1 && !empty($current_coach) && $role_id == 210) {
                return array(
                    'success' => true,
                    'message' => 'Huddle "' . $huddle_details->name . '" should have at least one Coach'
                );
            }
        } else {
            return false;
        }
    }

    public static function set_permissions($logged_in_role,$not_logged_in_role) {
        $permission = '';
        if ($logged_in_role == $not_logged_in_role) {
            $permission = 'allow';
        } elseif ($logged_in_role < $not_logged_in_role) {
            $permission = "allow";
        } elseif ($logged_in_role > $not_logged_in_role) {
            $permission = "allow";
        }
        return $permission;
    }

    public static function set_promotions($logged_in_role,$not_logged_in_role) {
        $permission = '';
        if ($logged_in_role == $not_logged_in_role) {
            $permission = 'allow';
        } elseif ($logged_in_role < $not_logged_in_role) {
            $permission = "allow";
        } elseif ($logged_in_role > $not_logged_in_role) {
            $permission = "not_allow";
        }
        return $permission;
    }
    
    public static function coaching_huddle_user_remove_check_on_permission_page($huddle_id, $user_id) {

        $result = AccountFolderMetaData::where(array(
            'meta_data_name' => 'folder_type',
            'account_folder_id' => $huddle_id
        ))->first();

        $huddle_details = AccountFolder::where('account_folder_id', $huddle_id)->first();

        if (isset($result) && $result->meta_data_value == '2') {

            $current_coachee = AccountFolderUser::where(array(
                'user_id' => $user_id,
                'account_folder_id' => $huddle_id,
                'role_id' => '210'
            ))->first();

            $any_other_coachee = AccountFolderUser::where(array(
                    'account_folder_id' => $huddle_id,
                    'role_id' => '210'
            ))->count();


            if (!empty($current_coachee) && $any_other_coachee == 1) {
                return array(
                    'success' => true,
                    'message' => 'Huddle "' . $huddle_details->name . '" should have atleast one Coachee.',
                    'role_id' => 210
                );
            }

            $coach_number =AccountFolderUser::where(array(
                    'account_folder_id' => $huddle_id,
                    'role_id' => '200'
                ))->count();

            $current_coach = AccountFolderUser::where(array(
                    'account_folder_id' => $huddle_id,
                    'role_id' => '200',
                    'user_id' => $user_id
                ))->first();


            if ($coach_number == 1 && !empty($current_coach)) {
                return array(
                    'success' => true,
                    'message' => 'Huddle "' . $huddle_details->name . '" should have at least one Coach',
                    'role_id' => 200
                );
            }
        } else {
            return false;
        }
    }
    
    
}

?>
