<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Models\Account;

use App\Models\Document;
use App\Models\Comment;
use App\Models\AccountFolderGroup;
use App\Models\AccountFolderUser;
use App\Models\UserAccount;
use App\Models\AccountFolder;
use App\Models\Plans;
use App\Models\AccountFolderMetaData;
use App\Models\AccountMetaData;

use App\Models\JobQueue;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Intercom\IntercomClient;
use App\PushNotifications;
use App\Models\User;
use App\Models\UserDeviceLog;
use Braintree;
use Braintree\TransactionSearch;
use Braintree\Subscription;
use Datetime;


class BraintreeSubscription {
    var $envoirment = 'sandbox';
    var $gateway;

    function __construct() {
        $this->gateway = $this->integrationMerchantConfig();
        
    }
    
    function integrationMerchantConfig() {
//        if ($this->envoirment == 'sandbox') {
//            $config = [
//                'environment' => 'sandbox',
//                'merchantId' => '2zhny5z63p96qnpv',
//                'publicKey' => '7m8srqdvts26gc4n',
//                'privateKey' => 'a5cc6489fd6230be94f3fd1787ccbbe0'
//            ];
//            $conf = new Braintree\Gateway($config);
//            return $conf;
//        }else{
//            $config = [
//                'environment' => 'production',
//                'merchantId' => 'tk7ph5dyz33hccq8',
//                'publicKey' => '44g8y8krfcrdyygn',
//                'privateKey' => '9d9c0a674920bc6e4462280cd17d55a3'
//            ];
//            $conf = new Braintree\Gateway($config);
//            return $conf;
//        }
         $config = [
                'environment' => config('general.environment'),
                'merchantId' => config('general.merchantId'),
                'publicKey' => config('general.publicKey'),
                'privateKey' => config('general.privateKey')
            ];
            $conf = new Braintree\Gateway($config);
            return $conf;
        
    }
    
    function find_transaction_detail($transaction_id) {
        try {
            $collection = $this->gateway->transaction()->find($transaction_id);
            return $collection ;
        }catch(Braintree_Exception_NotFound $e) {
            return array();
        }

    }
    
    function subscriptions_detail($subscription_id) {
        try {
            $collection = $this->gateway->subscription()->find($subscription_id);
            return $collection;
        }catch(\Exception $e) {
            return array();
        }

    }
    
    function get_customer_information($customer_id) {
        try {
            $customer = $this->gateway->customer()->find($customer_id);
            return $customer;
        }catch(\Exception $e) {
            return array();
        }
    }
    
    function transactions_detail($customer_id) {
        try {
            $collection = $this->gateway->transaction()->search([
                TransactionSearch::customerId()->is($customer_id),
            ]);
            return $collection;
        }catch(Braintree_Exception_NotFound $e) {
            return array();
        }
    }
    
    function create_subscription_with_existing_customer($planId, $customerData,$price,$monthly_bool,$subscription_id,$customer_id) {
        /*
         * Step 1 . First Create Cutomer and get Customer Id.
         * Step 2 . Create Credit Card and return Token.
         * Step 3 . Create Subscriptions return subscription id.
         */
       //  Braintree_Subscription::cancel($subscription_id);
         $customer_data = array();
        $result = $this->gateway->customer()->update($customer_id,$customerData);
        if ($result->success) {
            $result1 = $this->gateway->subscription()->create(array(
                        'paymentMethodToken' => $result->customer->creditCards[0]->token,
                        'planId' => $planId,
                        'trialDuration' => 60,
                        'trialDurationUnit' => 'month',
                        'price' => $price
            ));
            
            $subscription_type = 0 ;
            
            if($monthly_bool == 1)
            {
                $subscription_type = 0;
                
                
                $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 month", $time));
                
            }
            else
            {
                
                $subscription_type = 1;
                $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 year", $time));
              
              
                
            }
            
            
            
            $customer_data = array(
                'success' => TRUE,
                'braintree_customer_id' => $result->customer->id,
                'braintree_subscription_id' => $result1->subscription->id,
                'braintree_plan_id' => $result1->subscription->planId,
                'subscription_type' => $subscription_type,
                'subscription_status' => $result1->subscription->status,
                'payment_status' =>  (isset($result1->subscription->transactions[0]->status))? $result1->subscription->transactions[0]->status : '',
                'start_date' => $start_date ,
                'end_date' => $end_date,
                'db_check' => 'new'
            );
            return $customer_data;
        } else {
            return $result;
        }
    }
    
    function create_subscription($current_plan_id,$current_plan_users,$subscription_operation,$planId, $customerData, $price, $monthly_bool, $subscription_id,$customer_id = '',$description = '') 
     {
        /*
         * Step 1 . First Create Cutomer and get Customer Id.
         * Step 2 . Create Credit Card and return Token.
         * Step 3 . Create Subscriptions return subscription id.
         */
         //$this->gateway->subscription()->cancel($subscription_id);
         $customer_data = array();
         $CreditCardToker = '';
        //$result = Braintree_Customer::create($customerData);
         
         if(empty($customerData) && !empty($customer_id))
         {
               $result = $this->get_customer_information($customer_id);
               $CreditCardToken = $result->creditCards[0]->token;
         }
         else
         {
            if(empty($customer_id))
            {
               $result = $this->gateway->customer()->create($customerData);
               if($result->success)
               {
                    $CreditCardToken = $result->customer->creditCards[0]->token;
                    $customer_id = $result->customer->id;
               }
               else {
                    $CreditCardToken = '';
               }
            }
            else
            {
               $result = $this->gateway->customer()->update($customer_id,$customerData);
               if($result->success)
               {
                    $CreditCardToken = $result->customer->creditCards[0]->token;
                    $customer_id = $result->customer->id;
               }
               else {
                    $CreditCardToken = '';
               }
            }
         }
        if (!empty($CreditCardToken)) {
            
         if($subscription_operation == 'update')
         {
              $result1 = $this->gateway->subscription()->update($subscription_id,[
                        'paymentMethodToken' => $CreditCardToken,
                        'planId' => $planId,
                        'price' => $price
            ]); 
         }
         elseif ($subscription_operation == 'monthlytoyearly') {
             $subscriptions_detail = $this->subscriptions_detail($subscription_id);
             $subscriptions_detail = json_decode(json_encode($subscriptions_detail), true);
             $nextBillingDate = $subscriptions_detail['nextBillingDate']['date'];
             $days_difference = $this->date_difference(new DateTime($nextBillingDate), new DateTime(date("Y-m-d H:i:s")));
             $plan_detail = Plans::where(array("id" => $current_plan_id))->first();
             $price_per_day = ($plan_detail['price'] * $current_plan_users )/(30);
             $discount_price = round($days_difference * $price_per_day);
            if(!empty($subscription_id))
            {
                $this->gateway->subscription()->cancel($subscription_id);
            }
             
             $result1 = $this->gateway->subscription()->create(array(
                        'paymentMethodToken' => $CreditCardToken,
                        'planId' => $planId,
                        'trialDuration' => 60,
                        'trialDurationUnit' => 'month',
                        'price' => $price ,
                        'discounts' => [
                        'add' => [
                            [
                                'inheritedFromId' => '745g',
                                'amount' => $discount_price,
                                'numberOfBillingCycles' => 1
                            ]
                        ],
                        ]));
             
         }
         else
         {
            if(!empty($subscription_id))
            {
                $this->gateway->subscription()->cancel($subscription_id);
            }
            $result1 = $this->gateway->subscription()->create([
                        'paymentMethodToken' => $CreditCardToken,
                        'planId' => $planId,
                        'trialDuration' => 60,
                        'trialDurationUnit' => 'month',
                        'price' => $price
            ]);
         }
            if($result1->success == false)
            {
               return ['success' => false , 'error_message' => $result1->message]; 
            }
            
            if($result1->subscription->transactions[0]->status == 'processor_declined')
            {
               return ['success' => false , 'error_message' => $result1->subscription->transactions[0]->status]; 
            }
            
            $transaction = $result1->subscription->transactions[0];
            $transaction_date = (array) $transaction->createdAt;
            $trans_date = strtotime($transaction_date['date']);
            $trans_date = date('Y-m-d H:i:s', $trans_date);
            $transaction_data = array(
                        'transaction_id' => $transaction->id,
                        'transaction_type' => $transaction->type,
                        'status' => $transaction->status,
                        'student_payment_id'=> '0',
                        'created_date' => $trans_date,
                        'description' => $description
                        
            );
            DB::table('transactions')->insertGetId($transaction_data);
            
            $subscription_type = 0 ;
            
            if($monthly_bool == 1)
            {
                $subscription_type = 0;
                
                
                  $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 month", $time));
                
            }
            else
            {
                
                $subscription_type = 1;
                $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 year", $time));
              
              
                
            }
            
            if($result1->success)
            {
            
            
                    $customer_data = array(
                        'success' => TRUE,
                        'braintree_customer_id' => $customer_id,
                        'braintree_subscription_id' => $result1->subscription->id,
                        'braintree_plan_id' => $result1->subscription->planId,
                        'subscription_type' => $subscription_type,
                        'subscription_status' => $result1->subscription->status,
                        'payment_status' =>  (isset($result1->subscription->transactions[0]->status))? $result1->subscription->transactions[0]->status : '',
                        'start_date' => $start_date ,
                        'end_date' => $end_date,
                        'transaction_id' => $transaction->id ,
                        'db_check' => 'new',
                        'success' => true
                    );
                    return $customer_data;
            }
            
            else
            {
                    return ['success' => false , 'error_message' => $result1->message];
            }
            
        } else {
            return ['success' => false , 'error_message' => $result->message];
        }
    }
    
        function update_customer_credit_card_info($customerData,$customer_id )
        {
              $result = $this->gateway->customer()->update($customer_id,$customerData);
               if($result->success)
               {
                    return $result;
                    
               }
               else
               {
                   $result->success = false;
                   return $result;
               }

        }
        
        function date_difference($date1,$date2)
        {
            $days = $date1->diff($date2)->format("%a");
            return $days;
        }

    
}

?>