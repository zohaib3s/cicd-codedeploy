<?php
namespace App\Services;
use App\Http\Controllers\ApiController;
use App\Models\Account;
use App\Models\AccountFolderMetaData;
use App\Models\JobQueue;
use App\Models\Sites;
use App\Models\User;
use App\Models\UserAccount;
use \App\Models\UserActivityLog;
use App\Models\Document;
use App\Services\HelperFunctions;
use App\Services\TranslationsManager;
use App\Services\QueuesManager;
use DB;

class URLUploader {

    public function __construct()
    {
    }
    
    function addURL($site_id, $account_folder_id, $account_id, $user_id, $workspace = false, $url, $title, $current_user_role_id, $current_user_email, $is_sample='0', $slot_index = 0, $environment_type = 2, $parent_folder_id = 0) {

        $url = HelperFunctions::addHttpWithLinks($url, true);
        
        $data = array(
            'account_id' => $account_id,
            'doc_type' => '5',
            'url' => $url,
            'original_file_name' => $title,
            'active' => 1,
            'created_date' => date("Y-m-d H:i:s"),
            'created_by' => $user_id,
            'last_edit_date' => date("Y-m-d H:i:s"),
            'last_edit_by' => $user_id,
            'published' => '1',
            'site_id' => $site_id,
            'stack_url' => '',
            'post_rubric_per_video' => '1',
            'parent_folder_id' => $parent_folder_id,
            'scripted_current_duration' => '0'
        );

        $document_id =  \DB::table('documents')->insertGetId($data);
        
        if ($document_id) {

            $folder_type = \App\Models\AccountFolder::get_folder_type($account_folder_id, $site_id);
            if ($folder_type == '1') {
                $huddle_url = 'Huddles/view/' . $account_folder_id;
            } elseif ($folder_type == '3') {
                $huddle_url = 'home/workspace/';
            } else {
                $huddle_url = '/videoLibrary/view/' . $account_folder_id;
            }

            $user_activity_logs = array(
                'ref_id' => $document_id,
                'desc' => "Added URL Artifact - ".$title,
                'url' => $huddle_url,
                'type' => '29',
                'site_id' => $site_id,
                'account_folder_id' => $account_folder_id,
                'environment_type' => $environment_type
            );

            UserActivityLog::user_activity_logs($site_id, $user_activity_logs, $account_id, $user_id);

            $data = array(
                'account_folder_id' => $account_folder_id,
                'document_id' => $document_id,
                'title' => $title,
                'site_id' => $site_id,
                'desc' => "Added URL Artifact - ".$title,
                'assessment_sample' => $is_sample,
                'slot_index' => $slot_index,
            );
            $account_folder_document_id = \DB::table('account_folder_documents')->insertGetId($data);

            $huddle = \App\Models\AccountFolder::getHuddle($site_id, $account_folder_id, $account_id);
            if(!$huddle) return response()->json([__FILE__.':@:'.__LINE__.' Huddle Not Found: '.$account_folder_id]);
            $huddleUsers = \App\Models\AccountFolder::getHuddleUsers($account_folder_id, $site_id);
            $documentData = array(
                'account_folder_id' => $account_folder_id,
                'huddle_name' => $huddle[0]['name'],
                'huddle_url' => $huddle_url,
                'participating_users' => $huddleUsers,
                'site_id' => $site_id
            );
            $all_participants = array();
            if (\App\Models\AccountFolderMetaData::check_if_eval_huddle($account_folder_id, $site_id) == 1) {
                if ($huddleUsers) {
                    foreach ($huddleUsers as $usre) {
                        if ($usre['role_id'] == 210) {
                            $all_participants[] = $usre['user_id'];
                        }
                    }
                }
            }
            $resp = Document::get_single_video_data($site_id, $document_id, $folder_type);
            if ($huddleUsers && HelperFunctions::is_email_notification_allowed($account_id) ) {
                QueuesManager::sendToEmailQueue(__CLASS__,"sendAddURLEmailToParticipants",$huddleUsers,$account_folder_id,$site_id,$user_id,$all_participants,$documentData,$account_id);
            }
        
            $data_ws = null;
            if($resp)
            {
                $data_ws = $resp[0];
            }
            
            $allowed_participants = [];
            $h_type = AccountFolderMetaData::where('account_folder_id', $account_folder_id)->where('meta_data_name', 'folder_type')->first();
            $htype = isset($h_type->meta_data_value) ? $h_type->meta_data_value : "1";
            if($current_user_role_id)
            {
                $allowed_participants = HelperFunctions::get_security_permissions($account_folder_id);
                if($htype == 3 && $current_user_role_id == "210")
                {
                    foreach ($allowed_participants as $key => $allowed_participant)
                    {
                        if($allowed_participant["user_id"] != $user_id && $allowed_participant["role_id"] == "210")
                        {
                            unset($allowed_participants[$key]);
                        }
                    }
                }
            }
            if($workspace)
            {
                $channel = 'workspace-'.$account_id."-".$user_id;
            }
            else
            {
                $channel = 'huddle-details-'.$account_folder_id;
            }
            $reference_id = $document_id;
            /*
            if(!empty($video_id))
            {
                $reference_id = $video_id;
                $res = Document::get_single_video_data($site_id, $reference_id, $folder_type, 1, $account_folder_id, $user_id);
                $event = [
                    'channel' => $channel,
                    'event' => "resource_renamed",
                    'data' => $res,
                    'video_file_name' => $res["title"],
                    'is_dummy' => 1
                ];
                HelperFunctions::broadcastEvent($event,"broadcast_event",false);//not sending push to prevent multiple refresh
            }
            $data_ws["comment_id"] = $comment_id;
            */
            $res = array(
                'is_comment_resource'=>false,
                'document_id' => $document_id,
                'reference_id' => $reference_id,
                'data' => $data_ws,
                'url' => $url,
                'huddle_id' => $account_folder_id,
                'channel' => $channel,
                'event' => 'resource_added',
                'allowed_participants' => $allowed_participants,
                'assessment_sample' => $is_sample
            );
           if(HelperFunctions::is_push_notification_allowed($account_id))
           {
                HelperFunctions::broadcastEvent($res);
           }
          else 
           {
                HelperFunctions::broadcastEvent($res,'broadcast_event',false);
           }

    //        return response()->json($res);
            return $res;
        } else {
            $document_id = 0;
            $s_url = '';
            return response()->json(["success"=>false,"message"=>"Url Not Added due to some error."]);
        }
    }
    function sendAddURLEmailToParticipants($huddleUsers,$account_folder_id,$site_id,$user_id,$all_participants,$documentData,$account_id){
        foreach ($huddleUsers as $row) {
            if($row['is_active'] ==0){
                continue;
            }

            if (\App\Models\AccountFolderMetaData::check_if_eval_huddle($account_folder_id, $site_id) > 0) {
                if (\App\Models\AccountFolderUser::check_if_evaluated_participant($account_folder_id, $user_id, $site_id)) {
                    if (isset($all_participants) && is_array($all_participants) && in_array($row['id'], $all_participants)) {
                        continue;
                    }
                } else {

                    if ($user_id != $row['id']) {
                        continue;
                    }
                }
                $documentData['huddle_type'] = 3;
            } else {
                $documentData['huddle_type'] = '';
            }
            $documentData['email'] = $row['email'];
            $documentData['user_id'] = $row['id'];
            if ($documentData['huddle_type']!='3' && \App\Models\EmailUnsubscribers::check_subscription($row['id'], '1', $account_id, $site_id)) {
                
                    $this->sendUrlEmail($documentData,$user_id,$account_id,$site_id,"added");
                
            }
        }
    }
    function sendUrlEmail($data,$user_id,$account_id,$site_id,$action) {
        if (!($data['email']))
            return FALSE;



        $current_user = User::find($user_id);
        $current_account = Account::where("id",$account_id)->where("site_id",$site_id)->first();

        $email_from = $current_user->first_name . " " . $current_user->last_name . '  ' . $current_account->company_name . '<' . Sites::get_site_settings('static_emails',$site_id)['noreply'] . '>';
        $email_to = $data['email'];

        $lang = $current_user->lang;
        if ($lang == 'en') {
            $email_subject = Sites::get_site_settings('email_subject', $site_id) . " - URL Resource Added";
        } else {
            $email_subject = Sites::get_site_settings('email_subject', $site_id) . " - URL Recurso Añadido";
        }

        $sender = $current_user->first_name . "  " . $current_user->last_name;
        if($user_id == $data['user_id']){
            // This is Uploader of this URL
            $sender = TranslationsManager::get_single_translation_by_key('you_global', $lang);
        }
        if($action=="added"){
            $action = strtolower(TranslationsManager::get_single_translation_by_key('artifacts_video_added', $lang));
        }elseif($action=="shared"){
            $action = strtolower(TranslationsManager::get_single_translation_by_key('artifacts_video_shared', $lang));
        }

        $sibme_base_url = Sites::get_base_url($site_id); 
        $unsubscribe = $sibme_base_url . 'subscription/unsubscirbe_now/' . $data['user_id'] . '/1';
        $key = "url_added_" . $site_id . "_" . $lang;
        $result = SendGridEmailManager::get_send_grid_contents($key);
        if(!empty($result->versions) && !empty($result->versions[0]->html_content)){
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $sender, $html);
            $html = str_replace('{action}', $action, $html);
            $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
            $html = str_replace('{redirect}', $sibme_base_url . $data['huddle_url'], $html);
            $html = str_replace('{unsubscribe}', $unsubscribe, $html);
            $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
            $auditEmail = array(
                'account_id' => $data['account_folder_id'],
                'email_from' => $email_from,
                'email_to' => $data['email'],
                'email_subject' => $email_subject,
                'email_body' => $html,
                'is_html' => true,
                'sent_date' => date("Y-m-d H:i:s")
            );
    
            if (!empty($data['email'])) {
                DB::table('audit_emails')->insert($auditEmail);
                $result = false;
                try {
                    $use_job_queue = config('s3.use_job_queue');
                    if ($use_job_queue) {
                        $result = JobQueue::add_job_queue($site_id,1, $data['email'], $email_subject, $html, true);
                    } else {
                        /*
                        $emailData = [
                            'from' => Sites::get_site_settings('static_emails', $site_id)['noreply'],
                            'from_name' => Sites::get_site_settings('site_title', $site_id),
                            'to' => $data['email'],
                            'subject' => $email_subject,
                            'template' => $html
                        ];
                        Email::sendCustomEmail($emailData);
                        */
                    }
                } catch (\Exception $ex) {
    
                }
                return $result;
            }
        }      
        

        return FALSE;
    }

    public function validate_url($url){
        if(empty($url)) return false;

        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_TIMEOUT,10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_exec($ch);

        //Did an error occur? If so, dump it out.
        $is_valid = empty(curl_errno($ch));
        curl_close($ch);
        return $is_valid;
    }

}