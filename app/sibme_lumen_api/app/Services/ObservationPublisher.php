<?php

namespace App\Services;

use \App\Models\Document;
use \App\Models\AccountFolderDocument;
use \App\Models\DocumentFiles;
use \App\Models\Comment;
use \App\Models\AccountCommentTag;
use \App\Models\AccountFolder;
use \App\Models\User;
use \App\Models\Account;
use \App\Models\AccountFolderMetaData;
use \App\Models\EmailUnsubscribers;
use \App\Models\AuditEmail;
use \App\Models\JobQueue;
use App\Models\Sites;
use \App\Services\Emails\Email;
use Illuminate\Support\Facades\DB;

class ObservationPublisher {

    public function __construct() {

    }

    function publish_observation($site_id, $video_id, $account_id, $huddle_id, $user_current_account, $site_title) {
        $document = Document::getVideoDetails($site_id, $video_id, 3);
        $account_folder_document = AccountFolderDocument::where('document_id', $video_id)->where('site_id', $site_id)->first()->toArray();
        $document_data_files = DocumentFiles::where("document_id", $video_id)->where("site_id", $site_id)->where("default_web", true)->first();
        if ($document_data_files) {
            $document_data_files = $document_data_files->toArray();
        }
        if (empty($document) || empty($account_folder_document) || empty($document_data_files)) {
            return response()->json(['status' => 'false']);
        }
        Document::where('id', $video_id)->where('site_id', $site_id)->update(['is_associated' => 1]);

        $comments = Comment::getVideoComments($site_id, $video_id, '', '', '', '', 1);

        $data = array(
            'account_id' => $account_id,
            'site_id' => $site_id,
            'doc_type' => '1',
            'url' => $document['url'],
            'original_file_name' => $document['original_file_name'],
            'file_size' => $document['file_size'],
            'view_count' => $document['view_count'],
            'content_type' => $document['content_type'],
            'published' => 1,
            'is_associated' => 1,
            'job_id' => $document['job_id'],
            'zencoder_output_id' => $document['zencoder_output_id'],
            'active' => $document['active'],
            'created_date' => date('Y-m-d H:i:s', time()),
            'created_by' => $document['created_by'],
            'last_edit_date' => date('Y-m-d H:i:s', time()),
            'last_edit_by' => $document['last_edit_by'],
            'zencoder_output' => $document['zencoder_output'],
            'encoder_provider' => $document['encoder_provider'],
            'encoder_status' => $document['encoder_status'],
            'thumbnail_number' => $document['thumbnail_number'],
            'request_dump' => $document['request_dump'],
            'recorded_date' => date('Y-m-d H:i:s', time()),
            'post_rubric_per_video' => '1'
        );

        $doc = DB::table('documents')->insert($data);
        if ($doc) {
            $document_id = DB::getPdo()->lastInsertId();
            Document::where('id', $video_id)->where('site_id', $site_id)->update(['is_associated' => $document_id, 'site_id' => $site_id]);
            $data2 = array(
                'document_id' => $document_id,
                'site_id' => $site_id,
                'url' => $document_data_files['url'],
                'resolution' => $document_data_files['resolution'],
                'duration' => $document_data_files['duration'],
                'file_size' => $document_data_files['file_size'],
                'default_web' => $document_data_files['default_web'],
                'transcoding_job_id' => $document_data_files['transcoding_job_id'],
                'transcoding_status' => $document_data_files['transcoding_status'],
                'created_at'=>date('Y-m-d H:i:s'),
                "debug_logs" => "Lumen::ObservationPublisher::publish_observation::line=83::document_id=".$document_id."::status=".$document_data_files['transcoding_status']."::created"
            );
            DB::table('document_files')->insert($data2);

            $data3 = array(
                'account_folder_id' => $huddle_id,
                'site_id' => $site_id,
                'document_id' => $document_id,
                'title' => $account_folder_document['title'],
                'zencoder_output_id' => $account_folder_document['zencoder_output_id'],
                'desc' => $account_folder_document['desc'],
                'is_viewed' => $account_folder_document['is_viewed'],
                'video_framework_id' => $account_folder_document['video_framework_id']
            );
            DB::table('account_folder_documents')->insert($data3);

            if (count($comments) > 0) {
                foreach ($comments as $row) {
                    $data4 = array(
                        'parent_comment_id' => $row['parent_comment_id'],
                        'site_id' => $site_id,
                        'title' => htmlspecialchars($row['title']),
                        'comment' => $row['comment'],
                        'ref_type' => $row['ref_type'],
                        'ref_id' => $document_id,
                        'user_id' => $row['user_id'],
                        'time' => $row['time'],
                        'restrict_to_users' => $row['restrict_to_users'],
                        'created_date' => date('Y-m-d H:i:s', time()),
                        'last_edit_by' => $row['last_edit_by'],
                        'created_by' => $row['created_by'],
                        'last_edit_date' => date('Y-m-d H:i:s', time()),
                        'active' => $row['active'],
                    );
                    $comm = DB::table('comments')->insert($data4);
                    $comment_id = DB::getPdo()->lastInsertId();
                    $get_standard = AccountCommentTag::gettagsbycommentid($site_id, $row['id'], array('0')); //get standards
                    $get_tags = AccountCommentTag::gettagsbycommentid($site_id, $row['id'], array('1', '2')); //get tags
//                    echo "<pre>";
//                    print_r($get_tags);
                    // $standards = isset($observations_standards) ? $observations_standards : '';
                    if (!empty($get_standard)) {
                        foreach ($get_standard as $row) {
                            $data = array(
                                // 'account_comment_tag_id1' => $row['AccountCommentTag']['account_comment_tag_id'],
                                'comment_id' => $comment_id,
                                'site_id' => $site_id,
                                'ref_id' => $document_id,
                                'ref_type' => $row['ref_type'],
                                'tag_title' => $row['tag_title'],
                                'account_tag_id' => $row['account_tag_id'],
                                'created_by' => $row['created_by'],
                                'created_date' => $row['created_date'],
                            );
                            DB::table('account_comment_tags')->insert($data);
                        }
                    }
                    //$tags = isset($observations_tags) ? $observations_tags : '';
                    if (!empty($get_tags)) {
                        foreach ($get_tags as $row) {
                            $data = array(
                                // 'account_comment_tag_id' => $row['AccountCommentTag']['account_comment_tag_id'],
                                'comment_id' => $comment_id,
                                'site_id' => $site_id,
                                'ref_id' => $document_id,
                                'ref_type' => $row['ref_type'],
                                'tag_title' => $row['tag_title'],
                                'account_tag_id' => $row['account_tag_id'],
                                'created_by' => $row['created_by'],
                                'created_date' => $row['created_date'],
                            );
                            DB::table('account_comment_tags')->insert($data);
                        }
                    }
                }
            }
            Document::where('id', $document_id)->where('site_id', $site_id)->update(['is_associated' => 1, 'site_id' => $site_id]);
            $this->after_publish_observation_email($site_id, $document_id, $account_id, $user_current_account, $site_title);
        }

        return array(
            'status' => 'true', 'document_id' => $document_id
        );
    }

    function after_publish_observation_email($site_id, $document_id, $account_id, $user_current_account, $site_title) {

        $is_video_published_now = false;

        $video = Document::find($document_id)->toArray();

        if (isset($video) && !empty($video)) {
            $is_video_published_now = true;

            $account_folder_document = AccountFolderDocument::where('document_id', $document_id)->where('site_id', $site_id)->first()->toArray();
            $account_folder = AccountFolder::where('account_folder_id', $account_folder_document['account_folder_id'])->where('site_id', $site_id)->first()->toArray();
            $creator = User::where('id', $video['created_by'])->where('site_id', $site_id)->first()->toArray();
            $huddleUsers = AccountFolder::getHuddleUsers($account_folder_document['account_folder_id'], $site_id);

            if ($account_folder['folder_type'] != 1) {
                if (EmailUnsubscribers::check_subscription($creator['id'], '7', $account_id, $site_id)) {

                    $video_name = $account_folder_document['title'];

                    if (empty($video_name))
                        $video_name = $account_folder['name'];

                    //var_dump($creator);
                    $this->sendVideoPublishedNotification_AfterPublish(
                            $site_id
                            , $video_name
                            , $account_folder['account_folder_id']
                            , $video['id']
                            , $creator
                            , $account_folder['account_id']
                            , $creator, $creator['email']
                            , $creator['id']
                            , $account_folder['folder_type']
                            , $user_current_account
                            , '7', '', $site_title
                    );
                }
            } else {

                if (count($huddleUsers) > 0) {
                    for ($i = 0; $i < count($huddleUsers); $i++) {

                        $huddleUser = $huddleUsers[$i];

                        if ($is_video_published_now && $huddleUser['role_id'] == 210) {
                            if (EmailUnsubscribers::check_subscription($huddleUser['id'], '8', $account_id, $site_id)) {
                                $this->sendVideoPublishedNotification_AfterPublish(
                                        $site_id
                                        , $account_folder['name']
                                        , $account_folder['account_folder_id']
                                        , $video['id']
                                        , $creator
                                        , $account_folder['account_id']
                                        , $huddleUsers
                                        , $huddleUser['email']
                                        , $huddleUser['id']
                                        , $account_folder['folder_type']
                                        , $user_current_account
                                        , '8', '', $site_title);
                            }
                        }
                    }
                }
            }
        }
    }

    function sendVideoPublishedNotification_AfterPublish($site_id, $huddle_name, $huddle_id, $video_id, $creator, $account_id, $huddleUsers, $recipient_email, $huddle_user_id, $huddle_type, $user_current_account, $email_type = '', $url_scripted = '', $site_title) {
        if (!filter_var($recipient_email, FILTER_VALIDATE_EMAIL)) {
            return ['error' => 1, 'message' => 'Invalid Email!'];
        }

        $user = $user_current_account;
        $first_name = $user['User']['first_name'];
        $last_name = $user['User']['last_name'];
        $company_name = $user['accounts']['company_name'];

        $account_info = Account::where('id', $account_id)->where('site_id', $site_id)->first()->toArray();

        $auth_token = md5($creator['id']);
        $from = $first_name . " " . $last_name . " " . $company_name . " <" . Sites::get_site_settings('static_emails', $site_id)['noreply'] . ">";
        $to = $recipient_email;

        if ($huddle_type == '2') {
            $subject = $account_info['company_name'] . " Video Library.";
        } else {
            $subject = "Re: " . $huddle_name . " You've got a new video to view. ";
        }

        $h_type = AccountFolderMetaData::where('account_folder_id', $huddle_id)->where('site_id', $site_id)->where('meta_data_name', 'folder_type')->first()->toArray();
        $htype = isset($h_type['meta_data_value']) ? $h_type['meta_data_value'] : "1";

        if ($huddle_type == '1') {
            $huddle_name = 'Huddle: ' . $huddle_name;
            $url = "huddles/view/" . $huddle_id . "/1/" . $video_id;
        } elseif ($huddle_type == '2') {
            $huddle_name = $huddle_name;
            $url = "VideoLibrary/view/" . $huddle_id;
        } elseif ($huddle_type == '3') {
            $huddle_name = '' . $huddle_name;
            $url = "MyFiles/view/1/" . $huddle_id;
        } else {
            $huddle_type = '1';
            $huddle_name = 'Huddle: ' . $huddle_name;
            $url = "huddles/view/" . $huddle_id . "/1/" . $video_id;
        }

        if (!empty($url_scripted)) {
            $url = $url_scripted;
            $subject = "Re: " . $huddle_name . " You've got a new scripted observation to view. ";
        }

        $params = array(
            'huddle_name' => $huddle_name,
            'site_id' => $site_id,
            'account_name' => $account_info['company_name'],
            'huddle_id' => $huddle_id,
            'video_id' => $video_id,
            'creator' => $creator,
            'huddle_type' => $huddle_type,
            'participated_user' => $huddleUsers,
            'authentication_token' => $auth_token,
            'url' => $url,
            'htype' => $htype,
            'huddle_user_id' => $huddle_user_id,
            'email_type' => $email_type,
            'site_title' => $site_title
        );

        if (!empty($url_scripted)) {
            $email_view = 'emails.observation.' . $site_id . '.after_scriptedob_publish_email';
        } else {
            $email_view = 'emails.observation.' . $site_id . '.after_video_publish_email';
        }

        $html = view($email_view, $params)->render();

        $auditEmail = array(
            'account_id' => $account_id,
            'site_id' => $site_id,
            'email_from' => $from,
            'email_to' => $creator['email'],
            'email_subject' => $subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        $use_job_queue = config('s3.use_job_queue');
        if ($use_job_queue) {
            \DB::table('audit_emails')->insert($auditEmail);
            JobQueue::add_job_queue($site_id, 1, $to, $subject, $html, true);
            return ['error' => 0];
        } else {
            $response = Email::sendEmail($site_id, $email_view, $params, $to, '', $subject);
            if ($response !== true) {
                $auditEmail['error_msg'] = $response;
                \DB::table('audit_emails')->insert($auditEmail);
                return ['error' => 1,
                    'message' => 'Sorry your email could not be sent right now, please try again later.',
                    'exception' => $response];
            } else {
                \DB::table('audit_emails')->insert($auditEmail);
                return ['error' => 0];
            }
        }
    }

}
