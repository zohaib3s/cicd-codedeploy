<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use \App\Models\Sites;
use Illuminate\Support\Facades\DB;

class SendGridEmailManager {

    function __construct() {

    }

    public static function get_send_grid_contents($key) {
        $result = DB::table('sendgrid_templates')->where('slug', $key)->get();
        if ($result) {           
            $temp_content = (object) array(
                'versions' => array(
                    0 => (object) array(
                        'html_content' => !empty($result[0]->contents) ? $result[0]->contents : '',
                    )
                )
            );           
            return $temp_content;
            // $use_template = $result[0]->send_grid_id;
            // $curl = curl_init();
            // $trimmed_url = trim("https://api.sendgrid.com/v3/templates/$use_template");
            // curl_setopt_array($curl, array(
            //     CURLOPT_URL => $trimmed_url,
            //     CURLOPT_RETURNTRANSFER => true,
            //     CURLOPT_SSL_VERIFYPEER => false,
            //     CURLOPT_ENCODING => "",
            //     CURLOPT_MAXREDIRS => 10,
            //     CURLOPT_TIMEOUT => 1800,
            //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            //     CURLOPT_CUSTOMREQUEST => "GET",
            //     CURLOPT_HTTPHEADER => array(
            //         "authorization: Basic ZGF2ZXdAc2libWUuY29tOmM5ODhZWXdzIUA=",
            //         "content-type: application/json"
            //     ),
            // ));
            // $response = curl_exec($curl);
            // $err = curl_error($curl);
            // curl_close($curl);
            // if ($err) {
            //     return "cURL Error #:" . $err;
            // } else {
            //     return json_decode($response);
            // }
        } else {
            return false;
        }
    }

    public static function get_site_url() {
        return 'www.sibme.com';
    }

}

?>
