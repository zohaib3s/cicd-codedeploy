<?php
namespace App\Services\Workspace;

class Videos {
    public static function getVideoDetail($video_id, $huddle_id, $account_id, $user_id){
        $final_array = array();

        $video_creater_detail = Document::where(array('id' => $video_id))->first();
        
        if (!$video_creater_detail) {
            return array('message' => 'You don\'t have access to this video.',
                'success' => false);
        }

        $user_pause_settings = UserAccount::where(array(
                    "user_id" => $user_id, "account_id" => $account_id
                ))->first()->toArray();

        $comments = $this->get_video_comments_with_replies($video_id, 0, $huddle_id, $user_id);

        $final_array['comments'] = $comments;
/// Check
        $huddle_detail = AccountFolder::where(array('account_folder_id' => $huddle_id))->first();

        if ($huddle_detail) {
            $huddle_detail = $huddle_detail->toArray();
        }
            
        $video_creater_detail = Document::where(array('id' => $video_id))->first();

        if ($video_creater_detail) {
            $video_creater_detail = $video_creater_detail->toArray();
        }

        $final_array['huddle_info'] = $huddle_detail;

        $video_detail = $this->getVideoDetails($video_id);

        $video_url = $this->get_document_url($video_detail);

        $final_array['video_detail'] = $video_detail;

        $custom_markers = $this->getmarkertags($account_id, '1');

        $final_array['custom_markers'] = $custom_markers;

        $final_array['static_url'] = $video_url['url'];

        $comments_counts = $this->get_comments_count($video_id, $user_id);
        $attached_document_numbers = Document::getVideoAttachmentNumbers($video_id);
        
        $final_array['comments_counts'] = $comments_counts;
        $final_array['user_pause_settings'] = $user_pause_settings['type_pause'];
        $final_array['attached_document_numbers'] = $attached_document_numbers;
        $final_array['success'] = true;

        return $final_array;
    }
}