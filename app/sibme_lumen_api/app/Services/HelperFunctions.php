<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Models\Account;

use App\Models\Document;
use App\Models\Comment;
use App\Models\AccountFolderGroup;
use App\Models\AccountFolderUser;
use App\Models\GoalSettings;
use App\Models\DocumentMetaData;
use App\Models\DocumentFiles;
use App\Models\UserAccount;
use App\Models\AccountFolder;
use App\Models\Plans;
use App\Models\AccountFolderMetaData;
use App\Models\AccountMetaData;

use App\Models\JobQueue;
use App\Models\WebsocketLogs;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Intercom\IntercomClient;
use App\PushNotifications;
use App\Models\User;
use App\Models\Goal;
use App\Models\GoalUser;
use App\Models\Sites;
use App\Models\UserDeviceLog;
use App\Services\S3Services;

class HelperFunctions {

    function __construct() {

    }

    public static function get_user_role_name($role_id,$lang = 'en') {

        $user_role = '';
        if($lang == 'en')
        {
            $account_owner = 'Account Owner';
            $super_admin = 'Super Admin' ;
            $admin = 'Admin' ;
            $user = 'User' ;
            $viewer = 'Viewer' ;
            
        }
        
        else {
            
            $account_owner = 'Titular de Cuenta';
            $super_admin = 'Super Administrador' ;
            $admin = 'Administrador' ;
            $user = 'Usuario' ;
            $viewer = 'Solo ver' ;
            
        }
        
        if ($role_id == '100') {
            $user_role = $account_owner;
        } else if ($role_id == '110') {
            $user_role = $super_admin;
        } else if ($role_id == '115') {
            $user_role = $admin;
        } else if ($role_id == '120') {
            $user_role = $user;
        } else if ($role_id == '125') {
            $user_role = $viewer;
        }

        return $user_role;
    }

    public static function check_if_account_in_trial_intercom($account_id) {

        $result = Account::find($account_id);

        $value = "";

        if ($result && $result->in_trial == '1') {
            $value = "True";
        } else {
            $value = "False";
        }

        return $value;
    }

    public static function create_intercom_event($event_name, $meta_data, $loggedInUserEmail) {
        try {
            $site_id = app("Illuminate\Http\Request")->header("site_id");
            if($site_id == '2')
            {
              $client = new IntercomClient(config('general.intercom_access_token_hmh'), null);  
            }
            else {
              $client = new IntercomClient(config('general.intercom_access_token'), null);  
            }
            

            $client->events->create([
                "event_name" => $event_name,
                "created_at" => time(),
                "email" => $loggedInUserEmail,
                "metadata" => $meta_data
            ]);
        } catch (ClientException $e) {
            $response = $e->getResponse();
            $status_code = $response->getStatusCode();
            return $status_code;
        }
    }

    public static function check_if_evalutorNew($huddle_id, $user_id) {
        $result = AccountFolderUser::where('user_id', $user_id)->where('account_folder_id', $huddle_id)->first();
        $result_groups = AccountFolderGroup::join("user_groups", "user_groups.group_id", "account_folder_groups.group_id")->where("user_groups.user_id", $user_id)->where("account_folder_groups.account_folder_id", $huddle_id)->first();
        if ($result && $result_groups ){
            if($result->role_id == 200){
                return true;
            }else{
                return false;
            }
        }elseif(!$result &&  $result_groups){
            if ($result_groups && $result_groups->role_id == 200) {
                return true;
            } else {
                return false;
            }
        }elseif($result &&  !$result_groups){
            if($result->role_id == 200){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }

        // if (($result && $result->role_id == 200) || ($result_groups && $result_groups->role_id == 200)) {
        //     return true;
        // } else {
        //     return false;
        // }
    }

    public static function check_if_evalutor($huddle_id, $user_id) {
        $result = AccountFolderUser::where('user_id', $user_id)->where('account_folder_id', $huddle_id)->first();

        $result_groups = AccountFolderGroup::join("user_groups", "user_groups.group_id", "account_folder_groups.group_id")->where("user_groups.user_id", $user_id)->where("account_folder_groups.account_folder_id", $huddle_id)->first();

        if (($result && $result->role_id == 200) || ($result_groups && $result_groups->role_id == 200)) {
            return true;
        } else {
            return false;
        }
    }

    public static function cleanFileName($string) {
        $string = str_replace(" ", "-", $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

    public static function CreateJobQueueTranscodingJob($document_id, $temp_upload_path, $suppress_success_message = false) {
        if ($suppress_success_message == false) {
            $jobQueueString = "<TranscodeVideoJob><document_id>$document_id</document_id><source_file><![CDATA[$temp_upload_path]]></source_file></TranscodeVideoJob>";
        } else {
            $jobQueueString = "<TranscodeVideoJob><document_id>$document_id</document_id><source_file><![CDATA[$temp_upload_path]]></source_file><send_success_message>0</send_success_message></TranscodeVideoJob>";
        }

        $site_id = Document::where("id",$document_id)->value('site_id');
        $jobQueue = new JobQueue();
        $jobQueue->JobId = '2';
        $jobQueue->CreateDate = date("Y-m-d H:i:s");
        $jobQueue->RequestXml = $jobQueueString;
        $jobQueue->JobQueueStatusId = 1;
        $jobQueue->CurrentRetry = 0;
        $jobQueue->AutoRetryCount = 3;
        $jobQueue->site_id = $site_id;
        $jobQueue->JobSource = Sites::get_base_url($site_id);
        $jobQueue->save();

        if ($jobQueue) {

            return $jobQueue->id;
        } else {
            return false;
        }
    }

    public static function broadcastEvent($response_object, $url = "broadcast_event",$send_push = true, $use_queue = true) {
        // $use_queue = false;
        if(is_local()){
            $event_url = "https://q2echo.sibme.com/" . $url;
        } else {
            $event_url = "https://echo.sibme.com/" . $url;
        }

        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $client->post($event_url, [
            'json' => $response_object,
            'headers' => [
                'Content-Type' => 'application/json',
            ],
            'debug' => false
        ]);
        $log_added = 0;
        if($send_push) //Saad Zia disabled this because push notifications were slowing down comments
        {
            try
            {
                if($url == "broadcast_event" || (isset($response_object['from_goals']) && $response_object['from_goals'] && $response_object["event"] == 'refresh-header-settings'))
                {
                    if(strpos($response_object["channel"], 'discussion-comment-changes-') === false)//mobiles don't need this notification
                    {
                        $log_added = self::sendPushNotification($response_object, $use_queue);
                    }
                }
                else//url = broadcast_multiple_event
                {
                    foreach ($response_object["multiple_huddle_ids"] as $huddle_id)
                    {
                        $data_array = [];
                        $data_array = $response_object["data_array"];
                        $main_channel = $data_array["channel"];
                        $data_array["channel"] = $main_channel."-".$huddle_id;
                        $data_array["huddle_id"] = $huddle_id;
                        $log_added = self::sendPushNotification($data_array, $use_queue);
                    }
                }
            }
            catch (\Exception $e)
            {
                Log::error("----------- Error: websocket processing ------------");
                Log::error($e->getMessage());
                Log::error($e->getTraceAsString());
            }
        }
        if(!$log_added)
        {
            self::insertWebsocketLogs($response_object, []);
        }

    }

/*
    public static function skipDataForPushNotification($response_object){
        if (isset($response_object["event"]) && $response_object["event"]=="feedback_published") {
            $response_object["assessees_list"] = [];
            \Log::info(["File: ".__FILE__, "Line: ".__LINE__, $response_object]);
        }
        return $response_object;
    }
*/
    public static function insertWebsocketLogs($response_object, $token_ids = [], $from_cake = 0)
    {
        $log = new WebsocketLogs();
        $log->account_id = self::findInData($response_object, 'account_id');
        $log->account_folder_id = self::findInData($response_object, 'account_folder_id');
        $log->ref_id = self::findInData($response_object, 'ref_id'); //sometimes ref_id and video id are different
        $log->document_id = self::findInData($response_object, 'video_id');
        $log->data = json_encode($response_object);
        if(!empty($token_ids))
        {
            $log->device_ids = json_encode($token_ids);
            $log->is_push_sent = 1;
        }
        else
        {
            $log->is_push_sent = 0;
        }
        $log->created_by = self::findInData($response_object, 'user_id');
        $log->created_date = date('Y-m-d H:i:s');
        $log->from_cake = $from_cake;
        $log->event = isset($response_object['event']) ? $response_object['event'] : (isset($response_object['data_array']['event']) ? $response_object['data_array']['event'] : '');
        $log->save();
        return 1;
    }
    public static function get_security_permissions($huddle_id, $resquest_type = 'video_details',  $vidoes = []) {
        $result = array();
        if ($resquest_type == 'video_details') {
            try {
                $users = self::get_evaluator_ids($huddle_id, [200,210,220]);
                return $users;
            } catch (\Exception $e) {
                dd($e->getMessage());
            }
        }
        return false;
    }

    public static function get_evaluator_ids($huddle_id, $role_id) {

        $query = AccountFolderUser::where('account_folder_id', $huddle_id);
        if (is_array($role_id)) {
            $query->whereIn("role_id", $role_id);
        } else {
            $query->where('role_id', $role_id);
        }
        $results = $query->get()->toArray();
        $evaluator = array();
        if ($results) {
            if (is_array($role_id)) {
                foreach ($results as $row) {
                    $evaluator[] = ["user_id" => $row['user_id'], "role_id" => $row["role_id"]];
                }
            } else {
                foreach ($results as $row) {
                    $evaluator[] = $row['user_id'];
                }
            }
            return $evaluator;
        } else {
            return [];
        }
    }

    public static function get_evaluator_emails($huddle_id, $role_id) {

        $query = AccountFolderUser::join("users", "users.id", "AccountFolderUser.user_id")->select(['users.id','users.email','users.lang'])->where('account_folder_id', $huddle_id);
        if (is_array($role_id)) {
            $query->whereIn("role_id", $role_id);
        } else {
            $query->where('role_id', $role_id);
        }
        $results = $query->get()->toArray();
        $evaluator = array();
        if ($results) {
            if (is_array($role_id)) {
                foreach ($results as $row) {
                    $evaluator[] = ["user_id" => $row['user_id'], "role_id" => $row["role_id"]];
                }
            } else {
                foreach ($results as $key => $row) {
                    $evaluator[$key]['email'] = $row['email'];
                    $evaluator[$key]['id'] = $row['id'];
                    $evaluator[$key]['lang'] = $row['lang'];
                }
            }
            return $evaluator;
        } else {
            return [];
        }
    }

    public static function check_if_evaluated_participant($huddle_id, $user_id) {

        $site_id = app("Illuminate\Http\Request")->header("site_id");
        $result = AccountFolderUser
                ::where(array(
                    'user_id' => $user_id,
                    'account_folder_id' => $huddle_id,
                    'site_id' => $site_id
                ))->get()->toArray();



        $result_groups = AccountFolderGroup::join('user_groups as ug', 'ug.group_id', '=', 'account_folder_groups.group_id')->where(array(
                    'ug.user_id' => $user_id,
                    'account_folder_groups.account_folder_id' => $huddle_id,
                    'account_folder_groups.site_id' => $site_id
                ))->get()->toArray();


        if ($result && $result[0]['role_id'] == 200) {
            return false;
        }
        if (($result && $result[0]['role_id'] == 210) || ($result_groups && $result_groups[0]['role_id'] == 210)) {
            return true;
        } else {
            return false;
        }
    }

    public static function get_submission_date($huddle_id, $return = false, $site_id = false, $get_valid = false) {
        if ($site_id == false) {
            $site_id = app("Illuminate\Http\Request")->header("site_id");
        }

        $submission_deadline_date = AccountFolderMetaData::where(array(
                    'site_id' => $site_id,
                    "meta_data_name" => "submission_deadline_date",
                    "account_folder_id" => $huddle_id
                ))->first();
        if ($submission_deadline_date) {
            $submission_deadline_date = $submission_deadline_date->toArray();
        }

        $submission_deadline_time = AccountFolderMetaData::where(array(
                    'site_id' => $site_id,
                    "meta_data_name" => "submission_deadline_time",
                    "account_folder_id" => $huddle_id
                ))->first();

        if ($submission_deadline_time) {
            $submission_deadline_time = $submission_deadline_time->toArray();
        } else {
            $submission_deadline_time = "00:00:00";
        }


        if ($submission_deadline_date && $submission_deadline_date['meta_data_value'] != '') {
            $date_modified = explode('-', $submission_deadline_date['meta_data_value']);
            if(isset($date_modified[2]))
            {
                $date = $date_modified[2] . '-' . $date_modified[0] . '-' . $date_modified[1];
                /*
                if(!$get_valid)
                {
                    $date = $date_modified[2] . '-' . $date_modified[0] . '-' . $date_modified[1];
                }
                else
                {
                    $date = $date_modified[2] . '-' . $date_modified[1] . '-' . $date_modified[0];
                }
                */
            }
            else
            {
                $date = $submission_deadline_date['meta_data_value'];
            }
            //$date = date('Y-m-d', strtotime($submission_deadline_date['AccountFolderMetaData']['meta_data_value']));
            // $time = $submission_deadline_time['AccountFolderMetaData']['meta_data_value'] . ':00';
            $time = date("h:i:s a", strtotime($submission_deadline_time['meta_data_value']));
            return $date . ' ' . $time;
        } else {
            return '';
        }
    }

    public static function get_eval_participant_videos($huddle_id, $user_id, $site_id) {

        $videos = Document::getVideos($site_id, $huddle_id, '', '', '', '', $user_id);
        if (count($videos) >= 1) {
            return count($videos);
        } else {
            return 0;
        }
    }

    public static function get_huddle_roles($huddle_id, $user_id) {

        $site_id = app("Illuminate\Http\Request")->header("site_id");
        $result = AccountFolderUser
                ::where(array(
                    'user_id' => $user_id,
                    'account_folder_id' => $huddle_id,
                    'site_id' => $site_id
                ))->get()->toArray();


        $result_groups = AccountFolderGroup::join('user_groups as ug', 'ug.group_id', '=', 'account_folder_groups.group_id')->where(array(
                    'ug.user_id' => $user_id,
                    'account_folder_groups.account_folder_id' => $huddle_id,
                    'account_folder_groups.site_id' => $site_id
                ))->get()->toArray();

        if ($result && !empty($result[0]['role_id'])) {
            return $result[0]['role_id'];
        } elseif ($result_groups && !empty($result_groups[0]['role_id'])) {
            return $result_groups[0]['role_id'];
        } else {
            return false;
        }
    }

    public static function get_special_permission($user_id, $account_id) {

        $site_id = app("Illuminate\Http\Request")->header("site_id");
        $user_permissions = UserAccount::where(array('user_id' => $user_id, 'account_id' => $account_id, 'site_id' => $site_id))->first();
        if(!$user_permissions){
            return false;
        }
        return $user_permissions->toArray();
    }

    public static function check_if_submission_date_passed($huddleId, $user_id) {
        $site_id = app("Illuminate\Http\Request")->header("site_id");
        if (self::check_if_evaluated_participant($huddleId, $user_id)) {
            $submission_date = self::get_submission_date($huddleId);
            if ($submission_date != '') {
                $submission_string = strtotime($submission_date);
                $current_time = strtotime(date('Y-m-d h:i:s a', time()));
                //echo $current_time.'  '.$submission_date;die;
                if ($submission_string < $current_time) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public static function sort_array_of_array(&$array, $subfield) {
        $sortarray = array();
        foreach ($array as $key => $row) {
            $sortarray[$key] = $row[$subfield];
        }

        array_multisort($sortarray, SORT_DESC, $array);
    }

    public static function dis_mem_del_video($account_id, $logged_user_role, $huddle_type) {
        $site_id = app("Illuminate\Http\Request")->header("site_id");
        $check_data = AccountMetaData::where(array(
                    'site_id' => $site_id,
                    "meta_data_name" => "dis_mem_del_video",
                    "account_id" => $account_id
                ))->get()->first();

        if (!empty($check_data)) {
            if ($check_data['meta_data_value'] && $logged_user_role == 210 && $huddle_type == 1) {
                return 0;
            } else {
                return 1;
            }
        } else {

            return 1;
        }
    }

    public static function coachee_permissions($huddle_id) {
        $site_id = app("Illuminate\Http\Request")->header("site_id");
         $result = AccountFolderMetaData::where(array(
                    'site_id' => $site_id,
                    "meta_data_name" => "coachee_permission",
                    "account_folder_id" => $huddle_id
                ))->first();

        if ($result) {
            $result = $result->toArray();
        }

        $coachee_permission = isset($result['meta_data_value']) ?$result['meta_data_value'] : "0";
        return  $coachee_permission;
    }

    public static function get_video_comment_numbers($video_id, $huddle_id = '', $user_id = '') {
        $site_id = app("Illuminate\Http\Request")->header("site_id");
            $check_data = AccountFolderMetaData::where(array(
                'site_id' => $site_id,
                "meta_data_name" => "folder_type",
                "account_folder_id" => $huddle_id
            ))->first();

        if ($check_data) {
            $check_data = $check_data->toArray();
        }
        $h_type = isset($check_data['meta_data_value']) ? $check_data['meta_data_value'] : "1";
        $ref_type = array(2, 3, 6, 7);
        $check_cmnt_data = '';
        if ((!self::check_if_evalutor($huddle_id, $user_id)) && (($h_type == '2' && self::is_enabled_coach_feedback($huddle_id)) || $h_type == '3')) {
            $check_cmnt_data = Comment::where(array(
                'ref_id' => $video_id,
                "active" => 1
            ))->whereIn("ref_type", $ref_type)->count();
/*
            if ($check_cmnt_data) {
                $check_cmnt_data = $check_cmnt_data->toArray();
            }
*/
        } else {

            $check_cmnt_data = Comment::where(array(
                'ref_id' => $video_id,
                //"active" => 1,
            ))->whereIn("ref_type", $ref_type)->count();
/*
            if ($check_cmnt_data) {
                $check_cmnt_data = $check_cmnt_data->toArray();
            }
*/
        }
       return $check_cmnt_data;
        if($check_cmnt_data){
            return count($check_cmnt_data);
        }else{
            return 0;
        }

    }

    public static function is_enabled_coach_feedback($account_folder_id) {
        $site_id = app("Illuminate\Http\Request")->header("site_id");
        $result = AccountFolderMetaData::where(array(
            'site_id' => $site_id,
            "meta_data_name" => "coach_hud_feedback",
            "account_folder_id" => $account_folder_id
        ))->first();

        if ($result) {
            $result = $result->toArray();
        }
        if (isset($result['meta_data_value'])) {
            if ($result['meta_data_value'] == '1') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function customPathInfo($path_with_filename)
    {
        if(!empty($path_with_filename))
        {
            $temp = explode("/",$path_with_filename);
            $basename = $temp[count($temp)-1];
            $c = explode(".",$basename);
            $filename = isset($c[0])?$c[0].".":"";
            if(count($c) == 2)
            {
                $filename = isset($c[0])?$c[0].".":"";
            }
            if(count($c) > 2)
            {
                $filename = '';
                for ($i=0 ; $i < count($c)-1 ; $i++ )
                {
                    $filename.= isset($c[$i])?$c[$i]."." :"";
                }
            }
            $extension = isset($c[1])?$c[1]:"";
            $dirname = rtrim(str_replace($basename,"",$path_with_filename),"/");
            return ["dirname"=>$dirname,"basename"=>$basename,"filename"=>$filename,"extension"=>$extension];
        }
        return [];
    }

    public static function addHttpWithLinks($text, $plain=false) {
        if($plain){
            // Return plain URL with no added HTML treatment.
            if (!self::isURLContainsHttp($text)) {
                $text = "https://" . $text;
            }
            return $text;
        }
        $tidy_config = array(
            //'clean' => true,
            'output-xhtml' => true,
            'show-body-only' => true,
            'wrap' => 0,
            //'fix-style-tags'=>false
        );
        
        $tidy = tidy_parse_string( $text, $tidy_config, 'UTF8');
        $tidy->cleanRepair();
        $dom = new \DOMdocument();
        $dom->loadHTML( (string) $tidy);

        /*$dom = new \DomDocument();
        $dom->loadHTML($text, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);*/

        foreach ($dom->getElementsByTagName('a') as $item) {
            $href = $item->getAttribute('href');
            if (!self::isURLContainsHttp($href)) {
                $href = "https://" . $href;
                $item->setAttribute('href', $href);
                $dom->saveHTML($item);
            }
        }
        // echo "Ddd";
        // var_dump($dom->saveHTML());
        // die;
        return $dom->saveHTML();
    }

    public static function isURLContainsHttp($url) {
        $url = parse_url($url);
        return isset($url['scheme']) && in_array($url['scheme'], ['http', 'https']);
    }

    public static function is_mobile_request($app_name) {
        return ( !empty($app_name) && ($app_name == 'com.sibme.mobileapp' || $app_name == 'com.sibme.sibme' || $app_name == 'HMH' || $app_name == 'com.hmh.mobileapp' || $app_name == 'com.hmh.com') );
    }
    
    public static function get_allowed_users($account_id) {
        $trial_users = 40;
        $unlimited_users = 100000;

        $result = Account::where(array(
                'id' => $account_id,))->first();
        
        if($result)
        {
            $result = $result->toArray();
        }
        else
        {
            return 0;
        }

//        print_r($result);die;

        if ($result['deactive_plan']) {
            if ($result['custom_users'] == -1) {
                return $unlimited_users;
            } else {
                return $result['custom_users'];
            }
        } elseif ($result['in_trial']) {
            return $trial_users;
        } elseif ($result['plan_id'] < 9) {

            $plans = Plans::where(array(
                    'id' => $result['plan_id'],
                ))->first();
            if($plans){
                $plans = $plans->toArray();
            }    
            return !empty($plans['users'])?$plans['users']:'';
        } else {
            return $result['plan_qty'];
        }
    }
    
    public static function parse_translation_params($string, $replacement_array = []){
        $processed_string = preg_replace_callback(
            '~\{\$(.*?)\}~si',
            function($match) use ($replacement_array)
            {
                return str_replace($match[0], isset($replacement_array[$match[1]]) ? $replacement_array[$match[1]] : $match[0], $match[0]);
            },
            $string);

        return $processed_string;
    }

    public static function get_users_of_admin_circle($account_id, $user_id,$site_id) {

        $user_distinct_ids = app('db')->select("SELECT user_id FROM `account_folder_users` as afu2 WHERE account_folder_id IN
        (SELECT afu.`account_folder_id`
        FROM `account_folders` AS af
        INNER JOIN `account_folder_users` AS afu ON (afu.`account_folder_id` = af.`account_folder_id`)
        WHERE af.`active`=1 AND
        af.`account_id`=" . $account_id . " AND
        af.`folder_type`=1 AND
        afu.`role_id`=200 AND
        af.site_id = " . $site_id . " AND
        afu.`user_id`=" . $user_id . ") AND user_id!=" . $user_id . " GROUP BY user_id;");


        $final_users_ids = array();
        foreach ($user_distinct_ids as $user_distinct_id) {
            $final_users_ids[] = $user_distinct_id->user_id;
        }

        if (!empty($final_users_ids)) {
            return $final_users_ids;
        } else {
            return array($user_id);
        }
    }
    public static function SpanishDate($FechaStamp,$location = '')
    {
        $ano = date('Y',$FechaStamp);
        $mes = date('n',$FechaStamp);
        $dia = date('d',$FechaStamp);
        $time = date('h:i A',$FechaStamp);

        $diasemana = date('w',$FechaStamp);
        $diassemanaN= array("Domingo","Lunes","Martes","Mi�rcoles",
            "Jueves","Viernes","S�bado");
        $mesesN=array(1=>"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio",
            "Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        if($location == 'dashboard')
        {
            $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                "Ago","Sep","Oct","Nov","Dic");
            return $mesesN[$mes]. ' '  .$dia ;
        }
        elseif($location == 'trackers' )
        {
            $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                "Ago","Sep","Oct","Nov","Dic");
            return $dia . '-'.$mesesN[$mes] ;
        }
        elseif($location == 'trackers_filter' )
        {
            $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                "Ago","Sep","Oct","Nov","Dic");
            return $mesesN[$mes] ;
        }
        elseif($location == 'archive_module' )
        {
            $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                "Ago","Sep","Oct","Nov","Dic");
            return $mesesN[$mes].' '.$dia.', '.$ano ;
        }
        elseif($location == 'discussion_time' )
        {
            $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                "Ago","Sep","Oct","Nov","Dic");
            return $time ;
        }

        elseif($location == 'discussion_date' )
        {
            $mesesN=array(1=>"Ene","Feb","Mar","Abr","May","Jun","Jul",
                "Ago","Sep","Oct","Nov","Dic");
            return $mesesN[$mes].' '.$dia.', '.$ano . ' '.$time ;
        }

        elseif($location == 'most_common' )
        {
            $mesesN=array(1=>"Ene","Feb","March","Abr","May","Jun","Jul",
                "Ago","Sep","Oct","Nov","Dic");
            return $mesesN[$mes].' '.$dia.', '.$ano ;
        }

        //return $diassemanaN[$diasemana].", $dia de ". $mesesN[$mes] ." de $ano";
    }

    public static function get_all_participant_ids($huddle_id, $user_id) {

        $result = AccountFolderUser::where('account_folder_id', $huddle_id)->whereIn('role_id', array(200, 210, 220))->get()->toArray();
        $evaluator = [];
        if ($result) {
            foreach ($result as $row) {
                $evaluator[] = $row['user_id'];
            }
            return $evaluator;
        } else {
            return [];
        }
    }

    public static function findInData($data, $find = 'video_id')
    {
        $value = self::findInDataInner($data, $find);
        if(is_array($value))
        {
            $value = implode(',', $value);
        }
        return $value;
    }

    public static function findInDataInner($data, $find = 'video_id')
    {
        $request =  app("Illuminate\Http\Request");
        if(isset($data["goal_id"])){
            $goal_id = $data["goal_id"];
        }
        if(empty($goal_id) && isset($data["data"]["goal_id"])){
            $goal_id= $data["data"]["goal_id"];
        }
        if(isset($data["huddle_id"]))
        {
            $account_folder_id = $data["huddle_id"];
        }

        if(empty($account_folder_id))
        {
            $account_folder_id = $request->has("huddle_id")?$request->get("huddle_id"):$request->get("account_folder_id");;
        }

        $video_id = isset($data["document_id"])?$data["document_id"]:(isset($data["item_id"])?$data["item_id"]:0);
        if($data && isset($data["data"]["doc_id"]))
        {
            $video_id = $data["data"]["doc_id"];
        }
        else if(isset($data["data"]["id"]))
        {
            $video_id = $data["data"]["id"];
        }

        if(isset($data['reference_id']))
        {
            $ref_id = $data['reference_id'];
        }
        else
        {
            $ref_id = $video_id;
        }
        if(empty($account_folder_id))
        {
            if(isset($data["data"]["account_folder_id"]))
            {
                $account_folder_id = $data["data"]["account_folder_id"];
            }
            else
            {
                $account_folder_id = 0;
            }
        }
        if(isset($data["user_id"]))
        {
            $user_id = $data["user_id"];
        }
        else
        {
            $user_id = $request->get("user_id");
        }
        if(empty($user_id) && isset($data["data"]["created_by"])){
            $user_id = $data["data"]["created_by"];
        }

        if(isset($data["site_id"]))
        {
            $site_id = $data["site_id"];
        }
        else
        {
            $site_id = $request->header('site_id');
        }

        if(isset($data["account_id"]))
        {
            $account_id = $data["account_id"];
        }
        else
        {
            $account_id = $request->get('account_id');
        }
        if(empty($account_id) && isset($data['data']['account_id']))
        {
            $account_id = $data['data']['account_id'];
        }
        if($find == 'site_id')
        {
            $return = $site_id;
        }
        else if($find == 'user_id')
        {
            $return = $user_id;
        }
        else if($find == 'account_folder_id')
        {
            if(is_array($account_folder_id))
            {
                $account_folder_id = implode(',', $account_folder_id);
            }
            $return = $account_folder_id;
        }
        else if($find == 'video_id')
        {
            $return = $video_id;
        }
        else if($find == 'ref_id')
        {
            $return = $ref_id;
        }
        else if($find == 'account_id')
        {
            $return = $account_id;
        }
        else if($find == 'goal_id'){
            $return = isset($goal_id) ? $goal_id : false;
        }
        else
        {
            $return = 0;
        }
        return $return;
    }

    public static function sendPushNotification($data, $use_queue) {
        \Log::info(">>> Executing sendPushNotification <<<", [$use_queue]);
        if ($use_queue && false) { // Forcefully disabling push notifications queue.
            // Please don't remove following code. It works only for Queues. Because, request object is not working while queue workers execute this function. So we have to get parameters before sending code to queues.
            $request =  app("Illuminate\Http\Request");
            if ($request->has("deviceToken")) {
                $data['deviceToken'] = $request->get("deviceToken");
            }
            $data['account_folder_id'] = $data['huddle_id'] = self::findInData($data, 'account_folder_id');
            $data['user_id'] = self::findInData($data, 'user_id');
            $data['site_id'] = self::findInData($data, 'site_id');
            $data['video_id'] = $data['document_id'] = self::findInData($data, 'video_id');
            $data['account_id'] = self::findInData($data, 'account_id');
            QueuesManager::sendToNotificationQueue(__CLASS__, "processPushNotification", $data);
        } else {
            self::processPushNotification($data);
        }
    }

    public static function processPushNotification($data) {
        \Log::info(">>> Executing processPushNotification <<<");
        $request =  app("Illuminate\Http\Request");
        $video_file_name = "";
        if($data && isset($data["data"]["name"]))
        {
            $video_file_name = $data["data"]["name"];
        }
        $from_goals = isset($data['from_goals']) ? $data['from_goals'] : false;
        $account_folder_id = self::findInData($data, 'account_folder_id');
        $user_id = self::findInData($data, 'user_id');
        $site_id = self::findInData($data, 'site_id');
        $video_id = self::findInData($data, 'video_id');
        $account_id = self::findInData($data, 'account_id');
        if (empty($site_id)) {
            $site_id = self::getSiteID($account_folder_id, $user_id, $video_id);
        }

        if($from_goals){
            $goal_id = self::findInData($data, 'goal_id');
            if(!$goal_id || empty($goal_id)){
                $from_goals = false;
            }else{
                $account_folder_id =$goal_id;
            }
        }

        $token_ids = self::get_device_token_ids($account_folder_id, $user_id, $site_id, $account_id, $from_goals);
        if (empty($token_ids)) {
            \Log::info(">>>> Empty token_ids ");
            \Log::info([$token_ids, ['account_folder_id' => $account_folder_id, 'user_id' => $user_id, 'site_id' => $site_id]]);
        }
        /*$user_data = User::find($user_id);*/
        $push_sent = [];
        if (isset($data['deviceToken'])) {
            $current_device_token = $data['deviceToken'];
        } else {
            $current_device_token = $request->get("deviceToken");
        }
        
                $title = '';
                $desc = '';
                $data_1 = array(
                    /*'event_data' => $data,*/
                    'huddle_id' => $account_folder_id,
                    'notification_type' => 10,
                    'video_id' => $video_id,
                    'video_file_name' => $video_file_name,
                    /*'user' => $user_data,*/
                );
                if(isset($data['notification_type']))
                {
                    $data_1['notification_type'] = $data['notification_type'];
                }
                $data_1 = array_merge($data_1,$data);
                if(!isset($data_1["reference_id"]))
                {
                    $data_1["reference_id"] = $video_id;
                }
                $full_data = $data_1;

                if(isset($data_1["data"]))
                {
                    unset($data_1["data"]);//removing data key because it some times get too large data and push give error.
                }
                if((isset($data_1["event"]) && $data_1["event"]=="subtitles_available") || (isset($data_1["channel"]) && strpos($data_1["channel"], "library")))
                {
                    // keep only following keys if it's subtitle_available
                    $keep_only_keys = ['huddle_id', 'notification_type', 'video_id', 'video_file_name', 'channel', 'event', 'reference_id', 'document_id', 'user_id', 'account_folder_id', 'site_id', 'account_id'];
                    $data_1 = array_intersect_key($data_1, array_flip($keep_only_keys));
                }

                // $exclude_keys = ["data", "subtitles_data", "subtitle_paths", "assessees_list", "user"];
                // $data_1 = array_diff_key($data_1,array_flip($exclude_keys));

                // $keep_only_keys = ['huddle_id', 'notification_type', 'video_id', 'video_file_name', 'channel', 'event', 'reference_id', 'document_id', 'user_id', 'account_folder_id', 'site_id', 'account_id'];
                // $data_1 = array_intersect_key($data_1, array_flip($keep_only_keys));

                $data_array = $data_1;
                $new_data_array = $data_1;
                
                $data2 = [
                    'mtitle'=>$title,
                    'mdesc'=>$desc,
                    'badge'=>1,
                    'required_data'=> $data_array
                ];

        foreach ($token_ids as $token_id) {
            if(!in_array($token_id['one_signal_device_id'],$push_sent) && $current_device_token != $token_id['apns_token'])
            {
                $push_sent[] = $token_id['one_signal_device_id'];
            }
        }

//        if($fromJobQueue)
//        {
//          $documentfiles =  DocumentFiles::where(array('document_id' => $video_id))->first();
//          $document_local_id = DocumentMetaData::where(array('document_id' => $video_id,'meta_data_name' => 'document_local_id' ))->first();
//          $document_duration = DocumentMetaData::where(array('document_id' => $video_id,'meta_data_name' => 'document_duration' ))->first();
//          if($documentfiles)
//          {
//              $new_data_array['transcoded_duration'] = $documentfiles->duration;
//          }
//          if($document_local_id)
//          {
//              $new_data_array['document_local_id'] = $document_local_id->meta_data_value;
//          }
//          if($document_duration)
//          {
//              $new_data_array['document_duration'] = $document_duration->meta_data_value;
//          }
//          $new_data_array['notification_type'] = 13;
//          $data3 = [
//                     'mtitle'=>$title,
//                     'mdesc'=>$desc,
//                     'badge'=>1,
//                     'required_data'=> $new_data_array
//                 ];
//          self::ios_and_android($data3,$push_sent,$site_id);
//          
//        }
        
        self::ios_and_android($data2,$push_sent,$site_id);
        return self::insertWebsocketLogs($full_data, $token_ids);
    }
    public static function get_device_token_ids($huddle_id, $user_id,$site_id, $account_id = false,$from_goals = false) {
      //In case of from_goals = true, $huddle_id = goal_id
        $token_ids = array();
        if($from_goals){
            $participant_ids = self::getGoalParticipants($huddle_id,$account_id);
        }else{
        if($account_id)
        {
            $participant_ids = self::get_all_account_user_ids($account_id);
        }
        else
        {
            $participant_ids = self::get_all_participant_ids($huddle_id, $user_id);
        }
        }
        $count = 0;
        foreach ($participant_ids as $participant_id) {
            if($user_id != $participant_id)
            {
                $user_device_logs = UserDeviceLog::where(array("user_id" => $participant_id , 'push_notifications_on' => '1', 'site_id' => $site_id))->get();
                foreach ($user_device_logs as $user_device_log) {
                    if (!empty($user_device_log)) {
                        $token_ids[$count]['apns_token'] = $user_device_log->apns_token;
                        $token_ids[$count]['device_type'] = $user_device_log->device_type;
                        $token_ids[$count]['one_signal_device_id'] = $user_device_log->one_signal_device_id;
                        $count++;
                    }
                }
            }
        }
        $user_device_logs = UserDeviceLog::where(array("user_id" => $user_id , 'push_notifications_on' => '1' , 'site_id' => $site_id))->get();
        foreach ($user_device_logs as $user_device_log) {
            if (!empty($user_device_log)) {
                $token_ids[$count]['apns_token'] = $user_device_log->apns_token;
                $token_ids[$count]['device_type'] = $user_device_log->device_type;
                $token_ids[$count]['one_signal_device_id'] = $user_device_log->one_signal_device_id;
                $count++;
            }
        }
        return $token_ids;
    }

    public static function get_all_account_user_ids($account_id)
    {
        return UserAccount::where('account_id', $account_id)->pluck('user_id');
    }
    
    
    public static function is_enabled_workspace_framework($account_id,$site_id) {
        $check_data = AccountFolderMetaData
                ::where(array(
                    "account_folder_id" => $account_id,
                    "site_id" => $site_id,
                    "meta_data_name" => "enable_framework_standard_ws"
                ))->first();
                

        if (!$check_data) {
            return false;
        }

        if ($check_data->meta_data_value == '1') {
            return true;
        } else {
            return false;
        }
    }
    public static function is_enabled_workspace_custom_marker($account_id,$site_id) {
        $check_data = AccountFolderMetaData
                ::where(array(
                    "account_folder_id" => $account_id,
                    "site_id" => $site_id,
                    "meta_data_name" => "enable_tags_ws"
                ))->first();
                

        if (!$check_data) {
            return false;
        }

        if ($check_data->meta_data_value == '1') {
            return true;
        } else {
            return false;
        }
    }
    
    public static function get_huddle_type($huddle_id,$site_id = false) {
        if(!$site_id)
        {
         $site_id = app("Illuminate\Http\Request")->header("site_id");
        }
        $htype = AccountFolderMetaData::where('account_folder_id', $huddle_id)
                ->where('meta_data_name', 'folder_type')
                ->where('site_id', $site_id)
                ->first();
        if ($htype) {
            $htype->toArray();
            return $htype['meta_data_value'];
        } else {
            return 1;
        }
    }

    public static function getUserIP() {
        // Get real visitor IP behind CloudFlare network
        if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
            $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
            $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
        }
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }

        return $ip;
    }

   public static function get_allowed_storage($account_id) {

        $trial_storage = 5;
        $unlimited_storage = 100000;


        $result = Account::where(array(
                'id' => $account_id,
            ))->first();

//        print_r($result);die;

        if ($result->deactive_plan) {
            if ($result->custom_storage == -1) {
                return $unlimited_storage;
            } else {
                return $result->custom_storage;
            }
        } elseif ($result->in_trial) {
            return $trial_storage;
        } elseif ($result->plan_id < 9) {
            

            $plans = Plans::where(array(
                    'id' => $result->plan_id,
                ))->first();
            if($plans)
            {
                return $plans->storage;
            }
            else
            {
                return 0;
            }
        } else {

            $plans = Plans::where(array(
                    'id' => $result->plan_id,
                ))->first();

            return $result->plan_qty * $plans->storage;
        }
    }
    
    
    public static function user_name_from_id($user_id, $site_id) {
        $result = User::where(array(
            'id' => $user_id, 'site_id' => $site_id))
            ->first();

        if ($result) {
            return $result->first_name . ' ' . $result->last_name;
        } else {
            $empty_name = '';
            return $empty_name;
        }
    }

    public static function getSecureSibmecdnImageUrl($file, $name = null) {

        $aws_access_key_id = config('s3.access_key_id');
        $aws_secret_key = config('s3.secret_access_key');
        $aws_bucket = config('s3.bucket_name_cdn');
        $expires = '+240 minutes';

        $url = "https://s3.amazonaws.com/" . $aws_bucket . "/" . $file;

        return $url;
    }
    
    public static function ios_and_android($data,$one_signal_ids,$site_id) {
        return self::send_onesignal_notifications($data['mtitle'], $data['mdesc']  , $data['required_data'] ,$one_signal_ids,$site_id);
    }
    
    public static function send_onesignal_notifications($title, $desc, $data  ,$one_signal_ids,$site_id)
    {

            if($site_id == 2)
            {
               $title_on_base_site_id = 'HMH';
               $desc_on_base_site_id = 'HMH Notifications'; 
               $app_id = "6522eb42-9763-4e96-8da6-ad02d7126378";  
            }
            
            else
            {
               $title_on_base_site_id = 'Sibme';
               $desc_on_base_site_id = 'Sibme Notifications'; 
               $app_id = "695ea104-69f8-4dde-a309-6e3e3906a21b"; 
            }
        
        
//            if(empty($title))
//            {
//                $title = $title_on_base_site_id;
//            }
//            
//            if(empty($desc))
//            {
//                $desc = $desc_on_base_site_id;
//            }
            $non_empty_ids = [];
            foreach ($one_signal_ids as $id)
            {
                if(!empty($id))
                {
                    $non_empty_ids[] = $id;
                }
            }
            $fields = array( 
            'app_id' => $app_id, 
            'include_player_ids' => $non_empty_ids,
            'headings' => ["en" => $title],
            'contents' => ["en" => $desc],
            'data' => $data,
            'content_available' => true    
            ); 
            Log::info("Line: ".__LINE__." Sending Push to: ",[$non_empty_ids]);

            $fields = json_encode($fields);

            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications"); 
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
            curl_setopt($ch, CURLOPT_HEADER, FALSE); 
            curl_setopt($ch, CURLOPT_POST, TRUE); 
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields); 
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 

            $response = curl_exec($ch);
            $response_json = json_encode($response);
            if (strpos($response_json, "error")>-1 && !strpos($response_json, "invalid_player_ids")) {
                // Trace only if there are errors but don't trace for invalid_player_ids
                Log::info("Error: Sending push lumen details : \n".$fields);
                Log::info("OneSignal Response : \n".$response_json);
                $trace = self::getCallingFunction();
                Log::info("--- Calling Function of ".__FUNCTION__." ---");
                Log::info($trace);
            }
            if(strpos($response_json, "error")===false){
                Log::info("Success: Sent Push to: ",[$non_empty_ids]);
            }
            Log::info("OneSignal Response : \n".$response_json);
            curl_close($ch); 
            
            return $response;
	
    }

    public static function sendCurlRequest($url,$authorization_token, $method = false, $is_post = false, $post_fields = [])
    {
        $headers = [];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if(!empty($authorization_token))
        {
            $headers = array("Authorization: Bearer $authorization_token");
        }

        if($method && !$is_post)
        {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }
        if($is_post)
        {
            if(is_array($post_fields))
            {
                $post_fields = json_encode($post_fields);
            }
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);

            $headers['Content-Type'] = 'application/json';
        }
        if(!empty($headers))
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }
        $server_output = curl_exec($ch);
        curl_close($ch);
        return json_decode($server_output);
    }
    public static function discussion_delete_perm_in_coaching($huddle_id,$user_id,$creator_id)
    {
        $huddle_role = self::get_huddle_roles($huddle_id, $user_id);
        $huddle_type = self::get_huddle_type($huddle_id);
        $coachee_permissions = self::coachee_permissions($huddle_id);

        if($huddle_type == '2')
        {
            if($huddle_role == '200')
            {
                return '1';
            }
            else if($user_id == $creator_id  && $coachee_permissions == '1')
            {
                return '1';
            }
            else
            {
                return '0';
            }

        }
        else
        {
            return '1';
        }


    }


    public static function get_show_parent_video_library($account_id)
    {
        $check_data = AccountMetaData::where(array(
            "account_id" => $account_id,
            "meta_data_name" => "show_parent_video_library"
        ))->first();

        if (!empty($check_data)) {

            return $check_data->meta_data_value;
        } else {

            return 0;
        }
    }
    
    public static function is_push_notification_allowed($account_id)
    {
        $account_details = Account::where(array('id' => $account_id))->first();
        
        if(isset($account_details->enable_push_notifications) && $account_details->enable_push_notifications == '1')
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    
    public static function is_email_notification_allowed($account_id, $check_permission_for=null)
    {
        $account_details = Account::where(array('id' => $account_id))->first();

        // We need to prefer sub email permissions over the main enable_all_emails permission as per Khurram's instructions (Notification Phase 2 - Plan B) 
        $allowed_params = ['enable_emails_for_upload_video', 'enable_emails_for_add_comment', 'enable_emails_for_create_huddle', 'enable_emails_for_goals'];
        if (!empty($check_permission_for) && in_array($check_permission_for, $allowed_params) && isset($account_details->$check_permission_for)) {
            return $account_details->$check_permission_for=='1' ? true : false;
        }

        if(isset($account_details->enable_all_emails) && $account_details->enable_all_emails == '1')
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    
    public static function getTranscoderType($account_id,$site_id){
        $transcoder = Account::select('id','active_transcoder_type')
                ->where(array('id' => $account_id, 'site_id' => $site_id))->first();
        return $transcoder->active_transcoder_type;
        
    }

    public static function searchArrayByValue($id, $array, $search_key = "user_id")
    {
        foreach ($array as $key => $val)
        {
            if(!is_array($val))
            {
                $val = (array)$val;
            }
            if ($val[$search_key] == $id)
            {
                return $key;
            }
        }
        return null;
    }

    /**
     * Redirect with POST data.
     *
     * @param string $url URL.
     * @param array $params POST data. Example: array('foo' => 'var', 'id' => 123)
     */
    public static function redirect_post($url, array $data)
    {
        $html = '
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <script type="text/javascript">
                function closethisasap() {
                    document.forms["redirectpost"].submit();
                }
            </script>
        </head>
        <body onload="closethisasap();">
        <form name="redirectpost" method="post" action="'. $url . '">
        ';

            if ( !is_null($data) ) {
                foreach ($data as $k => $v) {
                    $html .= '<input type="hidden" name="' . $k . '" value="' . $v . '"> ';
                }
            }
        $html .= '
        <noscript><input type="submit" value="Please click here if you are not redirected."/></noscript>
        </form>
        </body>
        </html>
        ';
        echo $html;
        exit;
    }

    public static function getGoalsAltName($account_id,$site_id)
    {
        $goal_settings = GoalSettings::where("account_id", $account_id)->where("site_id", $site_id)->first();
        $altNames = [];
        $translations = [];
        $translations['goals'] = TranslationsManager::get_translation('goals');
        $translations['action_item'] = TranslationsManager::get_translation('action_item');
        $translations['goals_evidence'] = TranslationsManager::get_translation('goals_evidence');
        $translations['goals_measurement'] = TranslationsManager::get_translation('goals_measurement');
        $altNames['goals'] = ($goal_settings && !empty($goal_settings->goal_alt_name)) ? $goal_settings->goal_alt_name : $translations['goals'];
        $altNames['action_item'] = ($goal_settings && !empty($goal_settings->item_alt_name)) ? $goal_settings->item_alt_name :  $translations['action_item'];
        $altNames['goals_evidence'] = ($goal_settings && !empty($goal_settings->evidence_alt_name)) ? $goal_settings->evidence_alt_name :  $translations['goals_evidence'];
        $altNames['goals_measurement'] = ($goal_settings && !empty($goal_settings->measurement_alt_name)) ? $goal_settings->measurement_alt_name :  $translations['goals_measurement'];
        return ["translations"=>$translations, "altNames"=>$altNames];
    }

    /**
     * Replaces goal, goals, Goal and Goals with goal terminology alternate name
     * Replaces action item, action items, Action item and Action Items etc with action item terminology alternate name
     * Replaces evidence and Evidence with evidence terminology alternate name
     * Replaces measurement and Measurement with measurement terminology alternate name
     * @param $translation
     * @param $account_id
     * @param $site_id
     * @param null $goals_Name
     * @return string|string[]|null
     */

    public static function getGoalAltTranslation($translation, $account_id, $site_id, $goals_Name = null)
    {
        if(!$goals_Name)
        {
            $goals_Name = self::getGoalsAltName($account_id, $site_id);
        }
        $translations = $goals_Name["translations"];
        $altNames = $goals_Name["altNames"];
        $goalAltName = $altNames['goals'];
        $actionItemAltName = $altNames['action_item'];
        $evidenceItemAltName = $altNames['goals_evidence'];
        $measurementItemAltName = $altNames['goals_measurement'];
        // replace word goal with goal alternate name
        if ($goalAltName !== $translations['goals']) {
            $lRegex = "/\b(?:goal|goals|meta|metas)\b/";
              $uRegex = "/\b(?:Goal|Goals|Meta|Metas)\b/";
              $translation = preg_replace($lRegex, strtolower($goalAltName), $translation);
              $translation = preg_replace($uRegex, ($goalAltName), $translation);
            }
        // replace word action item with action item alternate name
        if ($actionItemAltName !== $translations['action_item']) {
            $lRegex = "/\b(?:action item|action items|acción|acciónes)\b/";
              $uRegex = "/\b(?:Action Item|Action Items|Acción|Acciónes)\b/";
              $cRegex = "/\b(?:Action item|Action items|Acción|Acciónes)\b/";
              $translation = preg_replace($lRegex, strtolower($actionItemAltName), $translation);
              $translation = preg_replace($uRegex, ($actionItemAltName), $translation);
              $translation = preg_replace($cRegex, ($actionItemAltName), $translation);
            }
        // replace word evidence with evidence alternate name
        if ($evidenceItemAltName !== $translations['goals_evidence']) {
            $lRegex = "/\b(?:evidence|evidencia)\b/";
              $uRegex = "/\b(?:Evidence|Evidencia)\b/";
              $translation = preg_replace($lRegex, strtolower($evidenceItemAltName), $translation);
              $translation = preg_replace($uRegex, ($evidenceItemAltName), $translation);
            }
        // replace word measurement with measurement alternate name
        if ($measurementItemAltName !== $translations['goals_measurement']) {
            $lRegex = "/\b(?:measurement|indicador)\b/";
              $uRegex = "/\b(?:Measurement|Indicador)\b/";
              $translation = preg_replace($lRegex, strtolower($measurementItemAltName), $translation);
              $translation = preg_replace($uRegex, ($measurementItemAltName), $translation);
            }
        return $translation;
    }

    public static function getS3ServiceObject()
    {
        $s3_service = new S3Services(
            config('s3.amazon_base_url'), config('s3.bucket_name'), config('s3.access_key_id'), config('s3.secret_access_key')
        );
        return $s3_service;
    }

    public static function formatReadableDate($date=null)
    {
        if(empty($date)) return $date;
        return date_format(date_create($date),"M d, Y");
    }
    
    public static function get_huddle_participant_ids($huddle_id,$site_id = false) {
        if(!$site_id)
        {
            $site_id = app("Illuminate\Http\Request")->header("site_id");
        }
        $user_ids = array();
        $result = AccountFolderUser
                ::where(array(
                    'account_folder_id' => $huddle_id,
                    'site_id' => $site_id
                ))->get()->toArray();



        $result_groups = AccountFolderGroup::join('user_groups as ug', 'ug.group_id', '=', 'account_folder_groups.group_id')->where(array(
                    'account_folder_groups.account_folder_id' => $huddle_id,
                    'account_folder_groups.site_id' => $site_id
                ))->get()->toArray();


        foreach ($result as $row)
        {
            $user_ids[] = $row['user_id'];
        }
        foreach ($result_groups as $row)
        {
            $user_ids[] = $row['user_id'];
        }
        return $user_ids;
        
    }
    /*GET GOAL PARTICIPANTS FOR PUSH*/
    public static function getGoalParticipants($goal_id,$account_id = false){

        $goal_type = Goal::where("id",$goal_id)->first()->goal_type;

        if($goal_type == Goal::goalTypeAccount && $account_id)
        {
           return UserAccount::where('account_id', $account_id)->pluck('user_id');
        }else{
            $data =  GoalUser::join("users", "users.id", "=", "goal_users.user_id")
                    ->where("goal_users.goal_id", $goal_id)->get()->toArray();
        }
         $evaluator = [];
        if ($data) {
            foreach ($data as $row) {
                $evaluator[] = $row['user_id'];
            }
            return $evaluator;
        } else {
            return [];
        }
    }

    public static function addParamToURL( $key, $value, $url) {
        $url .= (parse_url($url, PHP_URL_QUERY) ? '&' : '?') . $key . '=' . $value;
        return $url;
    }

    public static function getParamFromUrl($url,$paramName){
        parse_str(parse_url($url,PHP_URL_QUERY),$op);// fetch querystring parameters from string and convert to associative array
        return array_key_exists($paramName,$op) ? $op[$paramName] : null; // check key is exist in this array
    }

    public static function getSiteID($account_folder_id, $user_id, $document_id){
        $site_id = null;
        if (empty($site_id) && !empty($account_folder_id)) {
            $site_id = AccountFolder::where('account_folder_id', $account_folder_id)->value('site_id');
        }
        if (empty($site_id) && !empty($user_id)) {
            $site_id = User::where('id', $user_id)->value('site_id');
        }
        if (empty($site_id) && !empty($document_id)) {
            $site_id = Document::where('id', $document_id)->value('site_id');
        }
        return $site_id;
    }

    /**
     * Function to get a trace of calling function list.
     * e.g,         $trace = self::getCallingFunction(true, true);
     *              \Log::info("--- Calling Function of ".__FUNCTION__." ---");
     *              \Log::info($trace);
     */
    public static function getCallingFunction($full_trace=false, $with_args=false) {
        // Create an exception
        $ex = new \Exception();

        // Call getTrace() function
        $trace = $ex->getTrace();

        // Position 0 would be the line that called this function
        $final_call = $trace[1];

        if ($full_trace) {
            $full = [];
            foreach ($trace as $key => $t) {
                if (strpos($t['function'], "Illuminate")>-1) {
                    break;
                }
                $current = ['function'=>'', 'file'=>'', 'line'=>''];
                if (isset($t['function'])) {
                    $current['function'] = $t['function'];
                }
                if ($with_args && isset($t['args'])) {
                    $current['args'] = $t['args'];
                }
                if (isset($t['file'])) {
                    $current['file'] = $t['file'];
                }
                if (isset($t['line'])) {
                    $current['line'] = $t['line'];
                }
                if (!$with_args) {
                    $current = $current['function'] ."() in ".$current['file']." @ Line: ".$current['line'];
                }
                $full [] = $current;
            }
            return $full;
        } else {
            return $final_call;
        }
    }
}

?>