<?php
namespace App\Services;

use App\Models\AccountCommentTag;
use App\Models\AccountFolderDocument;
use App\Models\AccountFolder;
use App\Models\AccountMetaData;
use App\Models\AccountTag;
use App\Models\Comment;
use App\Models\Document;
use App\Models\DocumentStandardRating;
use App\Models\User;
use App\Models\AccountFrameworkSettingPerformanceLevel;
use App\Models\AccountFolderMetaData;
use App\Models\AccountFrameworkSetting;

class AssessmentDetail {

    public function __construct()
    {
    }
    
    public static function getDetails($site_id, $huddle_id,$account_id, $user_id, $document_id, $coachee_id, $from_video = 0, $class = 0) {
        $responseArray = [];
        $metric_old = AccountMetaData::where('account_id', $account_id)
                                     ->where('site_id', $site_id)
                                     ->where('meta_data_name','like','metric_value_%')
                                     ->orderBy('meta_data_value','ASC')
                                     ->get();

        $responseArray ['metrics_name'] = $metric_old;

        $metric_new = array();
        foreach ($metric_old as $metric) {
            $metric_new [] = array('name' => strtolower(substr($metric->meta_data_name, 13)), 'value' => $metric->meta_data_value);
        }

        $coach_users = AccountFolderDocument::getCoachUserIDs($document_id, $site_id);
        $coach_hud = array();
        $new_array = array();
        $account_coachees_videos  = array();
        if($coach_users){
            $coach_hud = self::getCoachHuddleIDsForAssessment($account_id, $coach_users, $site_id);
            $account_coachees_videos = self::getCoacheesVideos($coach_hud, $coachee_id, $document_id, $account_id, $site_id);
          
            foreach ($account_coachees_videos as $videos) {
    
                foreach ($metric_new as $metric) {
                    if ($videos->Assessment == $metric['value']) {
                        $videos->Assessment = $metric['name'];
                    }
                }
                $new_array[] = $videos;
            }
        }    
        
        $account_coachees_videos = $new_array;
        $output = array();
        foreach ($account_coachees_videos as $videos) {
            if ($videos->RemoveTracking != '1') {
                if (empty($videos->Assessment)){ 
                    $videos->Assessment = 'No Assessment';
                }
                $output[] = $videos;
            }
        }
        $account_coachees_videos = $output;
        
        
         $id_framework = AccountFolderMetaData::where(array(
              'account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id', 'site_id' => $site_id
            ))->first();
        
        if($id_framework)
        {
           $id_framework->toArray();
            
        }
        
        $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";


        if ($framework_id == -1 || $framework_id == 0) {
           
            $id_framework = AccountMetaData::where(array(
              'account_id' => $account_id, 'meta_data_name' => 'default_framework', 'site_id' => $site_id
            ))->first();
             
             
            if($id_framework)
            {
               $id_framework->toArray();

            }
            
            $framework_id = isset($id_framework['meta_data_value']) ? $id_framework['meta_data_value'] : "0";
        }
        
        $framework_detail = AccountFolderDocument::where(array('document_id'=>$document_id))->first();
        
        if(!empty($framework_detail->video_framework_id)){
            $framework_id = $framework_detail->video_framework_id;
        }else{
            $framework_id = "0";
        }
        
       $standards_settings = AccountFrameworkSetting::where(array(
                "account_id" => $account_id,
                "site_id" => $site_id,
                "account_tag_id" => $framework_id
            ))->get()->toArray();
       
       if(isset($standards_settings[0]['enable_performance_level']))
       {
            $performace_level_switch = $standards_settings[0]['enable_performance_level'];
       }
       else
       {
           $performace_level_switch = false;
       }
        

        $mt_arr = array();
        
        if($performace_level_switch)
        {
            $account_framework_settings_performance_levels = AccountFrameworkSettingPerformanceLevel::select('AccountFrameworkSettingPerformanceLevel.*')->where(array(
              'AccountFrameworkSettingPerformanceLevel.account_framework_setting_id' => $standards_settings[0]['id'],
              'AccountFrameworkSettingPerformanceLevel.site_id' => $site_id
            ))->orderBy('performance_level_rating')->get()->toArray();
            
            foreach ($account_framework_settings_performance_levels as $pl) {
                
            $mt_arr[] = $pl['performance_level'] ;
            
            } 
            
            
        }
        else
        {
            
        foreach ($metric_old as $mt_old) {
            $mt_arr[] = substr($mt_old->meta_data_name, 13);
        }
        
        }

        $assessments = array(0 => 'No Assessment');
        $arr_all = array_merge($assessments, $mt_arr);
        $video_assessments = array();
        foreach ($account_coachees_videos as $acta) {
            foreach ($arr_all as $key => $value) {
                $acta->assessment_point = array();
                if (strtolower($acta->Assessment) == strtolower($value)) {
                    $acta->assessment_point = $key;
                }
                $video_assessments[$key] = $acta;
            }
        }
        $results = \DB::select( \DB::raw(
            "SELECT
                account_meta_data.`meta_data_value` AS assessment_point,
                account_comment_tags.`tag_title` AS Assessment,
                comments.`id` AS id,
                comments.`time` AS created_date
                FROM
                  account_meta_data,
                  account_comment_tags ,
                  account_folder_documents,
                  comments
                WHERE LOWER(
                    account_comment_tags.`tag_title`
                  ) = LOWER(
                    SUBSTRING(
                      account_meta_data.`meta_data_name`,
                      14
                    )
                  )
                  AND account_comment_tags.`ref_id` = account_folder_documents.`document_id`
                  AND comments.`active` = 1
                  AND comments.`site_id` = ". $site_id. "
                  AND account_comment_tags.`ref_type` = 2
                  AND account_id = " . $account_id . "
                  AND account_folder_documents.`document_id` =  " . $document_id . "
                  AND account_comment_tags.`comment_id` = comments.`id`"
        ) );

        $account_coach_tags_analytics = array();
        foreach ($results as $key => $result) {
            $account_coach_tags_analytics[$key]['assessment_point'] = $result->assessment_point;
            $comment_standard = AccountCommentTag::where('comment_id', $result->id)
                                                 ->where('site_id', $site_id)
                                                 ->where('ref_type', 0)
                                                 ->first();

            if ($comment_standard) {
                $account_coach_tags_analytics[$key]['Assessment'] = $result->Assessment . '<br>Standard: ' . $comment_standard->tag_title;
            } else {
                $account_coach_tags_analytics[$key]['Assessment'] = $result->Assessment;
            }
            $account_coach_tags_analytics[$key]['id'] = $result->id;
            if ($result->created_date == 0) {
                $account_coach_tags_analytics[$key]['created_date'] = 'All';
            } else {
                $account_coach_tags_analytics[$key]['created_date'] = gmdate("H:i:s", $result->created_date);
            }
        }

        // $this->set('assessment_graph', json_encode($account_coach_tags_analytics));

        $responseArray ['assessment_graph'] = $account_coach_tags_analytics;
        $responseArray ['assessment_array'] = $arr_all;
       
        $video_tags = AccountTag::getVideoCommentTagsWithoutComments($document_id, $framework_id,$site_id);
        
        $new_video_tags = array();
        
        foreach($video_tags as $video_tag)
        {
          
            if(!empty($video_tag['standard_analytics_label']))
            {
                $video_tag['tag_title'] = $video_tag['standard_analytics_label'];
                
            }
            
            $new_video_tags[] = $video_tag;
            
        }
        
        $video_tags = $new_video_tags;

        $account_coach_tags_analytics = $video_tags;

        $count_all = 0;
        $total_standards = 0;
        $total_avg_rattings = 0;
        $count_t_avg = 0;
        foreach ($account_coach_tags_analytics as $key => $row) {
            $ratings_array = DocumentStandardRating::getRatings($document_id, $row['account_tag_id'], $account_id, $site_id);

            $count = 0;
            $avg_sum = 0;
            $avg = 0;

            $total_standards = $total_standards + $row['total_tags'];
            foreach ($ratings_array as $values) {
                $avg = $avg + (int) $values->rating_value;
                $count++;
            }
            if ($count > 0) {
                $final_average = round($avg / $count);
            } else {
                $final_average = 0;
            }
            if ($final_average == 0) {
                $account_coach_tags_analytics[$key]['label'] = 'No Rating';
            }
            $total_avg_rattings = $total_avg_rattings + $final_average;
            if ($final_average != 0) {
                $count_t_avg ++;
            }
            if ($final_average > 0) {
                $final_average_name = '';
                
                if($performace_level_switch)
                {
                     foreach ($account_framework_settings_performance_levels as $pl) {
                          if ($final_average == $pl['performance_level_rating']) {
                              $final_average_name = $pl['performance_level'];
                          }
                    
            
                    } 
                     
                }
                else
                {
                foreach ($metric_new as $metric) {
                    if ($final_average == $metric['value']) {
                        $final_average_name = $metric['name'];
                    }
                }
                }

                $account_coach_tags_analytics[$key]['label'] = $final_average_name;
                $account_coach_tags_analytics[$key]['average_rating'] = $final_average;
                $account_coach_tags_analytics[$key]['color_rating'] = '#000';
            }
            
            if($count_t_avg == 0)
            {
                $total_ratting_avg = 0;
            }
            else { 
              $total_ratting_avg = round($total_avg_rattings / $count_t_avg);  
            }
            
            $total_ratting_name = '';
            if ($total_ratting_avg == 0) {
               $total_ratting_name =  'No Rating';
            }
            
            if($total_ratting_avg > 0)
            {
                   if($performace_level_switch)
                {
                     foreach ($account_framework_settings_performance_levels as $pl) {
                          if ($total_ratting_avg == $pl['performance_level_rating']) {
                              $total_ratting_name = $pl['performance_level'];
                          }
                    
            
                    } 
                     
                }
                else
                {
                foreach ($metric_new as $metric) {
                    if ($total_ratting_avg == $metric['value']) {
                        $total_ratting_name = $metric['name'];
                    }
                }
                
                }
                
            }
            
            

            $total_ratting_avg = $total_ratting_avg . ' - ' . $total_ratting_name;
            $responseArray ['ratting_title'] = 'Average Performance Level';
            $responseArray ['standard_title'] = 'Total tagged standards';
            $responseArray ['total_ratting'] = $total_ratting_avg;
            $responseArray ['total_standards'] = $total_standards;
            $responseArray ['standard_color'] = '#85c4e3';
            $responseArray ['ratting_color'] = '#000';

            $count_all ++;
        }

        // $this->set('video_tags', json_encode($account_coach_tags_analytics));
        $responseArray ['video_tags'] = $account_coach_tags_analytics;

        $doc_count = Document::getVideoAttachmentNumbers($document_id, $site_id);
        $responseArray ['resources'] = $doc_count;

        $video_detail = Document::getVideoDetails($site_id, $document_id, 1);
        $responseArray ['video_detail'] = $video_detail;

        $user_assessment = Comment::getAssessmentComment($document_id, $site_id);
        $responseArray ['doc_assessment'] = $user_assessment;

        $coachee_name = User::where('id', $coachee_id)->where('site_id', $site_id)->first();
        $responseArray ['coachee_name'] = $coachee_name;

        $assessment_feedback = Comment::getAssessmentFeedback($document_id, $coach_users, $site_id);
        $responseArray ['assessment_feedback'] = $assessment_feedback;

        $coach_comments = Comment::getCoachCommentsCount($document_id, $coach_users, $site_id);
        $responseArray ['coach_comments'] = $coach_comments;

        $coachee_comments = Comment::getCoacheeCommentsCount($document_id, $coachee_id, $site_id);
        $responseArray ['coachee_comments'] = $coachee_comments;

        $document = Document::where('id', $document_id)->where('site_id', $site_id)->first();
        $responseArray ['document'] = $document;
        $responseArray ['load_back'] = $from_video;
        $responseArray ['url'] = '/dashboard/assessment_tracker';
        $responseArray ['class'] = $class;

        return $responseArray;
    }

    public static function getCoachHuddleIDsForAssessment($account_id, $coach_users, $site_id){
        return AccountFolder::leftJoin('account_folders_meta_data as afmd', 'AccountFolder.account_folder_id', '=', 'afmd.account_folder_id')
                            ->leftJoin('account_folder_users as huddle_users', 'huddle_users.account_folder_id', '=', 'AccountFolder.account_folder_id')
                            ->leftJoin('users as User', 'User.id', '=', 'huddle_users.user_id')
                            ->where('AccountFolder.account_id', $account_id)
                            ->where('AccountFolder.folder_type', 1)
                            ->where('AccountFolder.site_id', $site_id)
                            ->where('afmd.meta_data_name', 'folder_type')
                            ->where('afmd.meta_data_value', 3)
                            ->where('huddle_users.role_id', 200)
                            ->where('huddle_users.user_id', $coach_users)
                            ->whereRaw('AccountFolder.active = IF((AccountFolder.`archive`=1) AND (AccountFolder.`active`=0),0,1)')
                            ->select('huddle_users.account_folder_id')
                            ->groupBy('huddle_users.account_folder_id')
                            ->pluck('huddle_users.account_folder_id')
                            ->toArray();
    }

    public static function getCoacheesVideos($account_folder_id, $coachee_id, $document_id, $account_id, $site_id)
    {
        return Document::leftJoin('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                       ->leftJoin('account_folder_users as afu', 'afd.account_folder_id', '=', 'afu.account_folder_id')
                       ->where('afd.account_folder_id', $account_folder_id)
                       ->where('documents.doc_type', 1)
                       ->where('documents.site_id', $site_id)
                       ->where('afu.user_id', $coachee_id)
                       ->where('documents.id', $document_id)
                       ->select(['documents.id','documents.created_date'])
                       ->selectRaw('(SELECT "1" FROM document_meta_data WHERE document_id = `afd`.`document_id` AND meta_data_name = "remove_assessment_tracking" AND meta_data_value = 1) AS RemoveTracking')
                       ->selectRaw('( SELECT ROUND(AVG(meta_data_value)) FROM account_meta_data , account_comment_tags WHERE LOWER(account_comment_tags.`tag_title`)= LOWER(SUBSTRING(account_meta_data.`meta_data_name` , 14)) AND
                        account_comment_tags.`ref_id` = `afd`.`document_id` AND account_comment_tags.`ref_type` = 2 AND account_id = ' . $account_id . ' ) AS Assessment')
                       ->groupBy('documents.id')
                       ->orderBy('documents.recorded_date', 'ASC')
                       ->get();
    }


}