<?php
namespace App\Services\Emails;
use App\Models\Sites;

use Mail;

class Email {
    public function __construct(){}

    public static function sendEmail($site_id, $view, $view_params_array, $to_email, $to_name, $subject, $filesWithPath=[]){
		try{
	        Mail::send($view, $view_params_array, function ($m) use ($site_id, $to_email, $to_name, $subject, $filesWithPath) {
                $from_email = Sites::get_site_settings('static_emails', $site_id)['noreply'];
                $site_title = Sites::get_site_settings('site_title', $site_id);
	            $m->from($from_email, $site_title);
	            $m->to($to_email, $to_name)->subject($subject);
		        foreach ($filesWithPath as $file) {
            		if(!empty($file)){
			            $m->attach($file);
	            	}
	            }
	        });
	        return true;
		}
		catch(\Exception $e){
            $html = view($view, $view_params_array);
            self::on_error($site_id, $to_email, $subject, $html);
		    return $e->getMessage();
		}
    }

    public static function sendCustomEmail($data){
        try{
            Mail::send([], [], function($message) use ($data) {
                $from_name = isset($data['from_name']) ? $data['from_name'] : "";
                $message->from($data['from'],$from_name);
                $message->to($data['to']);
                if(isset($data['cc']) && !empty($data['cc'])){
                    $message->cc($data['cc']);
                }
                if(isset($data['bcc']) && !empty($data['bcc'])){
                    $message->bcc($data['bcc']);
                }
                $message->subject($data['subject']);
                $message->setBody($data['template'], 'text/html');
            });
            return true;
        }catch(\Exception $e){
            $site_id = isset($data['site_id']) ? $data['site_id'] : 1;
            self::on_error($site_id, $data['to'], $data['subject'], $data['template']);
            return $e->getMessage();
        }
    }

    public static function on_error($site_id, $email_to, $email_subject, $html){
        \App\Models\JobQueue::add_job_queue($site_id, 1, $email_to, $email_subject, $html, true);
    }
}