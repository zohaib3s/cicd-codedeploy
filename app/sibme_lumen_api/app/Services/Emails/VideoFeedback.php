<?php
namespace App\Services\Emails;

class VideoFeedback{
    public function __construct(){}

    public static function sendEmail($site_id, $huddle_id, $video_id, $account_id, $emails, $subject, $message, $file,$comments_attachment=''){
/*
$file = $_FILES['additional_attachemnt']
*/

        $attachment_1 = '';
        $errors = array();
        $maxsize_mb = 2;
        $acceptable = array(
            'application/pdf',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'application/msword',
            'application/vnd.ms-excel',
            'application/xls',
            'image/jpeg',
            'image/jpg',
            'image/gif',
            'image/png'
        );

        if (isset($file['name']) && $file['name'] != '') {
            $additional_attachment = $file['tmp_name'];
            $file_size = $file['size'] / 1048576;
            if (($file_size > $maxsize_mb) || empty($file['size'])) {
                $errors[] = 'File too large. File must be less than 2 megabytes.';
            }
            if (!in_array($file['type'], $acceptable) && (!empty($file['type']))) {
                $errors[] = 'Invalid file type. Only PDF,XLXS,XLS, JPG, GIF and PNG types are accepted.';
            }

            if (count($errors) === 0) {
                $path = base_path() . '/public/attachments/' . $file['name'];
                move_uploaded_file($additional_attachment, $path);
                $attachment_1 = $path;
            } else {
                $error_message = '';
                if (isset($errors[0])) {
                    $error_message .= $errors[0] . ' ';
                } elseif (isset($errors[1])) {
                    $error_message .= $errors[0];
                }

                return ['error'=>1, 'message'=>$error_message];
            }
        }

        $attachments = array($attachment_1);
        $attachments[] = $comments_attachment;
        $response = '';
        foreach ($emails as $email_single) {
            if (!filter_var($email_single, FILTER_VALIDATE_EMAIL)) {
              return ['error'=>1, 'message'=>'Invalid Email Address == '.$email_single]; 
            }
            $auditEmail = array(
                'account_id' => $account_id,
                'site_id' => $site_id,
                'email_from' => config('mail.from.address'),
                'email_to' => $email_single,
                'email_subject' => $subject,
                'email_body' => $message,
                'is_html' => 1,
                'sent_date' => date("Y-m-d H:i:s")
            );
            if (!empty($email_single)) {
                $response = Email::sendEmail($site_id, 'emails.feedback', ['email_message'=>$message], $email_single, '', $subject, $attachments);
                if($response!==true){
                    $auditEmail['error_msg'] = $response;
                    \DB::table('audit_emails')->insert($auditEmail);
                    return ['error'=>1, 
                            'message'=>'Sorry your email could not be sent right now, please try again later.', 
                            'exception'=>$response];
                } else {
                    \DB::table('audit_emails')->insert($auditEmail);
                }
            }
        }
        if($response===true){
            self::unLinkAttachments($attachments);
            return ['error'=>0, 'message'=>'Email sent successfully.'];
        }
    }

    public static function unLinkAttachments($attachments)
    {
        foreach ($attachments as $attachment) {
            if (trim($attachment) != '') {
                unlink($attachment);
            }
        }
    }
}