<?php
namespace App\Services\Emails;
use DB;

class TrialSignUp{
    public function __construct(){

    }

    public static function sendEmail($site_id, $data, $account_id, $emailtemplate, $from){
        $html = str_replace('<%body%>', '', $emailtemplate);
        $subject = 'Welcome to Sibme!';
        $user_details = self::convert_array(DB::table('users')
                    ->where('email','=',$data['email'])
                    ->where('site_id','=',$site_id)
                    ->get());
        $html = str_replace('[Insert the User Name Here]', $user_details['first_name'] . ' ' . $user_details['last_name'], $html);

        $auditEmail = array(
            'account_id' => $account_id,
            'site_id' => $site_id,
            'email_from' => $from,
            'email_to' => $data['email'],
            'email_subject' => $subject,
            'email_body' => $html,
            'is_html' => 1,
            'sent_date' => date("Y-m-d H:i:s")
        );
        $emaildata['to'] = $data['email'];
        $emaildata['from'] = $from;
        $emaildata['template'] = $html;
        $emaildata['subject'] = $subject;
        $response = Email::sendCustomEmail($emaildata);
        if($response!==true){
            $auditEmail['error_msg'] = $response;
            \DB::table('audit_emails')->insert($auditEmail);
            return ['error'=>1, 
                    'message'=>'Sorry your email could not be sent right now, please try again later.', 
                    'exception'=>$response];
        } else {
            \DB::table('audit_emails')->insert($auditEmail);
        }
    }
    public static function activationEmail($data, $account_id, $site_id){
        $subject = "Sibme Activation Email!";
        if($site_id == 2){
            $subject = "HMH Activation Email!";
        }
        $auditEmail = array(
            'account_id' => $account_id,
            'site_id' => $site_id,
            'email_from' => config('mail.from.address'),
            'email_to' => $data['email'],
            'email_subject' => $subject,
            'email_body' => 'Activation Email',
            'is_html' => 1,
            'sent_date' => date("Y-m-d H:i:s")
        );
        $response = Email::sendEmail($site_id, 'emails.activation', ['name'=>$data['full_name'], 'account_name'=>$data['company'], 'user_id'=>$data['user_id']], $data['email'], '', $subject);
                if($response!==true){
                    $auditEmail['error_msg'] = $response;
                    \DB::table('audit_emails')->insert($auditEmail);
                    return ['error'=>1, 
                            'message'=>'Sorry your email could not be sent right now, please try again later.', 
                            'exception'=>$response];
                } else {
                    \DB::table('audit_emails')->insert($auditEmail);
                }
    }
    public static function convert_array($data){
        $data = collect($data)->map(function($x){ 
                return (array) $x; 
            })->toArray();
        return $data ? $data[0] : $data;
    }
}