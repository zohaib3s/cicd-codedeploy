<?php

namespace App\Services;

use \App\Models\Translation;
use Illuminate\Support\Facades\DB;

class TranslationsManager {

    function __construct() {

    }

    public static function get_page_lang_based_content($url, $site_id, $user_id = '', $lang = "en") {        
        if($lang =='es'){
            $lang = 'es';
        }else{
            $lang = 'en';
        }
        if(empty($lang))
        {
            if (empty($user_id))
            {
                $lang = TranslationsManager::getPreferredLanguage();
            }
            else
            {
                $result = DB::table('users')->where('id', $user_id)->get();
                $lang = $result[0]->lang;
            }
        }
        if (empty($lang)) {
            die('language not found!');
        }
        $language_contents = TranslationsManager::get_page_lang_based_data($url, $site_id);

        $language_array = array();
        foreach ($language_contents as $row)
        {
            if(isset($row->key) && $row->key !=''){               
                $language_array[$row->key] = $row->$lang;
            }
           
        }
        
        return $language_array;
    }

    public static function get_page_lang_based_data($url, $site_id) {
        $result = DB::table('translations')->where('url', $url)->orWhere("url","global")->get()->toArray();
        return $result;
    }

    public static function getPreferredLanguage() {

        $langs = array();
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            // break up string into pieces (languages and q factors)
            preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);
            if (count($lang_parse[1])) {
                // create a list like "en" => 0.8
                $langs = array_combine($lang_parse[1], $lang_parse[4]);
                // set default to 1 for any without q factor
                foreach ($langs as $lang => $val) {
                    if ($val === '')
                        $langs[$lang] = 1;
                }
                // sort list based on value
                arsort($langs, SORT_NUMERIC);
            }
        }
        $lang = "en"; // If no language found the set "en" as default.
        if(!empty($langs)){
            //extract most important (first)
            foreach ($langs as $lang => $val) {
                break;
            }
            //if complex language simplify it
            if (stristr($lang, "-")) {
                $tmp = explode("-", $lang);
                $lang = $tmp[0];
            }
        }
        return $lang;
    }

    public static function parse_translation_params($string, $replacement_array = []){
        $processed_string = preg_replace_callback(
            '~\{\$(.*?)\}~si',
            function($match) use ($replacement_array)
            {
                return str_replace($match[0], isset($replacement_array[$match[1]]) ? $replacement_array[$match[1]] : $match[0], $match[0]);
            },
            $string);

        return $processed_string;
    }

    public static function get_translation($key, $url = "", $site_id = 1, $user_id = '', $lang = false) {
        $result = '';
        if(!$lang)
        {
            $lang = app("Illuminate\Http\Request")->header('current-lang');
            if($lang != 'es' && $lang != 'en')
            {
                $lang = 'en';
            }
        }
        $result = TranslationsManager::get_single_translation_by_key($key);
        return !empty($result)?$result->$lang:'';
    }

    public static function get_single_translation_by_key($key, $lang=null)
    {
        $result = DB::table('translations')->where("translations.key",$key)->first();
        return !empty($lang) && !empty($result) ? $result->$lang : $result;
    }

}

?>
