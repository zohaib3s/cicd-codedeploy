<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Models\Document;
use App\Models\DocumentFiles;
use App\Models\ScreenShareAwsJobs;
use \App\Models\Sites;
use Coconut\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Coconut {

    private $transcodingType;

    function __construct() {
        $this->transcodingType = 'mp4:480p';//mp4,mp4:720p,mp4:1080p etc check coconut for more options
    }

    function createJob($s3_path, $file_url, $document_id){
        if(empty($file_url))
        {
            return false;
        }
        $path_parts = pathinfo($s3_path);
        if(!isset($path_parts["dirname"]) ||!isset($path_parts["filename"]) ||!isset($path_parts["extension"]) )
        {
            return false;
        }
        $name_modifier = "_zencoded";
        $s3 = 's3://'.config('s3.access_key_id').':'.config('s3.secret_access_key').'@'.config('s3.bucket_name');
        $output_file = $s3.''. $path_parts["dirname"] ."/". $path_parts["filename"] . $name_modifier .".". $path_parts["extension"];
        try {

            $job = Job::create(array(
                'api_key' => env('COCONUT_API_KEY'),
                'source' => $file_url,
                'metadata' => true,
                'webhook' => config("s3.sibme_api_url")."receive_notification_coconut?code=".$document_id.", metadata=true",
                'outputs' => array(
                    $this->transcodingType => $output_file,
                    //'webm' => $s3 . '/videos/video_' . $vid . '.webm',
                    //'jpg:300x' => $s3 . '/previews/thumbs_#num#.jpg, number=3'
                )
            ));
            print_r($job);
            if(empty(((array)$job->errors))) {
                $job_id = $job->id;
            } else {
                echo json_encode($job->errors);
            }

        } catch (\Exception $e) {
            // If were here, an error occurred
            echo($e->getMessage());
            return false;
        }
        echo "\nAll Job Attributes:\n";
        return isset($job_id) ? $job_id : false;
    }

    function processJobNotification($response){
        Log::info("Response from coconut \n "); Log::info(json_encode($response));  Log::info( "\n".$response['id']);
        try {
            // Catch notification
            $notification = $response;
            $job_id = $response['id'];
            // If you're encoding to multiple outputs and only care when all of the outputs are finished
            // you can check if the entire job is finished.
            $id = null;
            $document_id = null;
            $document = Document::where("zencoder_output_id", $job_id)->first();
            $success = false;
            if($document)
            {
                if($response['event'] == "job.completed") {
                    if(!empty($response['errors']))
                    {
                        $document->published = 0;
                        $document->encoder_status = "Error";
                        $document->last_edit_date = date('Y-m-d H:i:s');
                        $document->save();
                        DocumentFiles::where("document_id",$document->id)->update(["transcoding_status"=>5,"transcoding_status_details"=>$response["errors"]['source'], "updated_at"=>date('Y-m-d H:i:s'), "debug_logs" => "Lumen::Coconut::processJobNotification::line=91::document_id=".$document->id."::status=5::updated"]);
                        S3Services::execute_thumbnail_websocket($document ,1);
                        return ['status'=>false, "document_id"=>$document_id, "success"=>$success];
                    }
                    $url = stripslashes($response['output_urls'][$this->transcodingType]);
                    $duration = $response['metadata'][$this->transcodingType]['format']['duration'];
                    $coconut_path = trim(parse_url($url)['path'],"/");
                    $document->url = $coconut_path;
                    $document->encoder_status = "complete";
                    $screenShareClips = app('App\Http\Controllers\LiveStreamController')->getClippingData($document->id);
                    if(empty($screenShareClips) || true)//directly publishing document without screen merging, it will be done in ffmpeg
                    {
                        $document->published = 1;
                    }
                    else
                    {
                        $document->published = 0;//Dont publish until we merge screen share videos and cam videos
                        $s3Service = HelperFunctions::getS3ServiceObject();
                        $job_id = $s3Service->submitElementalClipsMergingJob($document, $screenShareClips);
                        $job_log = new ScreenShareAwsJobs();
                        $job_log->document_id = $document->id;
                        $job_log->clipping_job_id = $job_id;
                        $job_log->clipping_job_status = "submitted";
                        $job_log->site_id = $document->site_id;
                        $job_log->save();
                        app('App\Console\Commands\CraeteAudioAwsForTranscribe')->createJobForAudio($coconut_path,$document->id);
                    }
                    $document->video_duration = $duration;
                    $document->file_size = $response['metadata'][$this->transcodingType]['format']['size'];
                    $document->save();
                    $document_id = $document->id;
                    $document_files_data = [
                        "duration" => $duration,
                        "file_size" => $document->file_size,
                        "transcoding_status" => 3,
                        "url" => $coconut_path,
                        "updated_at"=>date('Y-m-d H:i:s'),
                        "debug_logs" => "Lumen::Coconut::processJobNotification::line=128::document_id=".$document->id."::status=3::updated"
                    ];
                    DocumentFiles::where("document_id",$document->id)->update($document_files_data);
                    S3Services::execute_thumbnail_websocket($document ,1);
                    $success = true;
                }
                else if($response['event'] == "job.error")
                {
                        $document->published = 0;
                        $document->encoder_status = "Error";
                        $document->last_edit_date = date('Y-m-d H:i:s');
                        $document->save();
                        DocumentFiles::where("document_id",$document->id)->update(["transcoding_status"=>5,"transcoding_status_details"=>$response["errors"]['source'], "updated_at"=>date('Y-m-d H:i:s'), "debug_logs" => "Lumen::Coconut::processJobNotification::line=140::document_id=".$document->id."::status=5::updated"]);
                }
                else
                {
                    Log::info("Unrecognized Coconut response:\n".json_encode($response));
                }
            }

            return ['status'=>true, 'coconut_notifications_log_id' => $id, "document_id"=>$document_id, "success"=>$success];

        } catch (\Exception $e) {
            // If were here, an error occurred
            echo($e->getMessage());
        }
    }

}

?>
