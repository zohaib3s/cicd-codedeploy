<?php
namespace App\Services;

use App\Services\SendGridEmailManager;

use App\Models\Sites;
use App\Models\JobQueue;
use App\Models\Hmh_order;
use App\Models\Hmh_order_license;

use App\Services\Emails\Email;
use App\Services\CodeProfiler;

use Illuminate\Support\Facades\DB;

class HmhLicense {
    private static $required_fields = ['Sales_Doc','Line','SoldTo_Cust_Number','SoldTo_Cust','ShipTo_Cust','ShipTo_City','ShipTo_State','Is_ShipTo_the_District_Y_N','Admin_Email','ISBN13','Material','Qty','Subscription_Start','Subscription_End'];
    private static $missing_required_fields = [];
    private static $invalid_orders = [];
    private static $already_exist_orders = [];
    private $cp;
    public static function process_orders($orders){
        /**
         * 1. Loop through all orders.
         * 2. Validate the fields in each order.
         * 3. Get Account ID from given Material.
         * 4. MD5 the email for admin_key (to access the client admin).
         * 5. Get the number of required licenses.
         * 6. Insert the order in Hmh_order along with the admin_key.
         * 7. Insert the required number of licenses in Hmh_order_license (order_id, is_used=0 for open/used licenses, is_invited=0 for invite/reinvite).
         * 8. Generate Client Licensing Admin Link with the above sha1 hash. e.g, https://app.sibme.com/licensing/root/a2rEr13tgYa
         * 9. Send licensing email with Admin Link to the email found in order.
         * 9. 
         */
        $success_count = 0;
        $message = "";
        $sibme_base_url = config('s3.sibme_base_url');
        foreach($orders as $order){
            if(!self::validate_fields($order) || self::order_already_exist($order) ){
                continue;
            }
            // self::$missing_required_fields [] = $order;
            $order['account_id'] = self::get_account_id($order['Material']);
            if(!$order['account_id']){
                self::$invalid_orders [] = $order;
                continue;
            }
            $success_count++;
            $data = self::getData($order);
            $data['admin_key'] = md5(strtolower(trim($data['Admin_Email'])));
            $data['admin_link'] = $sibme_base_url .'licensing/root/'. $data['admin_key'];
            $data['Order_Date'] = date("Y-m-d H:i:s");
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['account_id'] = $order['account_id'];
            $required_licenses_count = $data['Qty'];
            $order_id = DB::table('hmh_orders')->insertGetId($data);

            if(!empty($required_licenses_count) && !empty($order_id)) {
                for($i=0;$i<$required_licenses_count;$i++){
                    $license = [
                       'order_id' =>  $order_id,
                       'created_at' => date("Y-m-d H:i:s"),
                       'updated_at' => date("Y-m-d H:i:s")
                    ];
                    DB::table('hmh_order_licenses')->insert($license);
                }
            }

            // self::send_new_order_email($data);
            // self::schedule_new_order_email($data);
        }
        if($success_count>0){
            $message .= "Successfully created ".$success_count." order(s). ";
            $success = true;
        }
        if(!empty(self::$invalid_orders)) {
            $message .= count(self::$invalid_orders) . " order(s) failed to process because Material Code is wrong.";
            $success = false;
        }
        if(!empty(self::$missing_required_fields)) {
            $message .= count(self::$missing_required_fields) . " order(s) failed to process because all required fields are not provided.";
            $success = false;
        }
        if(!empty(self::$already_exist_orders)) {
            $message .= "Order(s) with Sales_Doc+Line= " . implode(", ",self::$already_exist_orders) . " already exist. ";
            $success = false;
        }
        return ["success"=>$success, "message"=>$message];
    }

    public static function validate_fields($order){
        /**
         * Loop through self::$required_fields and check if all the fields exist in current order.
         */
        $valid = true;
        foreach(self::$required_fields as $val){
            if(!array_key_exists($val, $order)){
                self::$missing_required_fields [] = $order;
                $valid = false;
                break;
            }
        }
        return $valid;
    }

    public static function getData($order){
        $valid = true;
        $data = [];
        foreach(self::$required_fields as $val){
            if(array_key_exists($val, $order)){
                $data [$val] = $order[$val];
            }
        }
        return $data;
    }

    public static function order_already_exist($order){
        /**
         * Check if order already exist for SalesDoc and Line.
         */
        if(!isset($order['Sales_Doc']) || empty($order['Sales_Doc']) || !isset($order['Line']) || empty($order['Line'])){
            return true;
        }
        $exist = false;
        // Check SalesDoc and Line from DB if that record already exists, return false.
        $result = Hmh_order::where('Sales_Doc',$order['Sales_Doc'])->where('Line',$order['Line'])->first();
        if($result){
            self::$already_exist_orders [] = $order['Sales_Doc'] . " " . $order['Line'];
            $exist = true;
        }
        return $exist;
    }

    public static function get_account_id($material){
        return DB::table('hmh_accounts_lookup')->where('material', $material)->value("account_id");
    }

    public static function send_new_order_emails(){
        $orders = Hmh_order::where('is_email_sent', '0')->get();
        $arranged_orders = [];
        foreach ($orders as $order) {
            $Sales_Doc = $order->Sales_Doc;
            $Admin_Email = $order->Admin_Email;
            $arranged_orders[$Sales_Doc.$Admin_Email][] = $order;
        }
        foreach ($arranged_orders as $orders) {
            $order_detail_template = "";
            foreach ($orders as $order) {
                $order_detail_template .= self::order_detail_template($order);                
            }
            $Sales_Doc = $order->Sales_Doc;
            $Admin_Email = $order->Admin_Email;
            Hmh_order::where('Sales_Doc', $Sales_Doc)->where('Admin_Email', $Admin_Email)->where('is_email_sent', '0')->update(['is_email_sent' => '1']);
            $order['Order_Detail'] = $order_detail_template;
            self::send_new_order_email_2($order);
        }
    }

    public static function order_detail_template($order){
        return '<p><a href="'.$order['admin_link'].'">Client Licensing Admin</a></p>
                    <p>
                    - Customer Email Address: '.$order['Admin_Email'].'<br>
                    - Order Material: '.$order['Material'].'<br>
                    </p>
                    <table width="100%" style="border-collapse: collapse;">
                    <tbody><tr>
                        <th style="background:#efefef;border: 1px solid black;font-weight: normal;text-align: left;padding:5px;">Product</th>
                        <th style="background:#efefef;border: 1px solid black;font-weight: normal;text-align: left;padding:5px;">ISBN</th>
                        <th style="background:#efefef;border: 1px solid black;font-weight: normal;text-align: left;padding:5px;">Number of Users</th>
                    </tr>
                    <tr>
                        <td style="border: 1px solid black;font-weight: normal;text-align: left;padding:5px;">'.$order['Material'].'</td>
                        <td style="border: 1px solid black;font-weight: normal;text-align: left;padding:5px;">'.$order['ISBN13'].'</td>
                        <td style="border: 1px solid black;font-weight: normal;text-align: left;padding:5px;">'.$order['Qty'].'</td>
                    </tr>
                    </tbody></table>
                ';
    }

    public static function send_new_order_email_2($order){
        // Create Template in SEND GrID and then fill values in here accordingly
        $site_id = app("Illuminate\Http\Request")->header("site_id");
        $site_id = !empty($site_id) ? $site_id : 2;
        $site_email_subject = Sites::get_site_settings('email_subject', $site_id);
        $from =  'Sibme HMH <' . Sites::get_site_settings('static_emails', $site_id)['noreply'] . '>';
        $customer_email = $order['Admin_Email'];

        $subject = "Your Blended Coaching with HMH’s Coaching Studio Order";

        $key = "hmh_order_confirmation_email_en";
        $result = SendGridEmailManager::get_send_grid_contents($key);
        if (!$result || !isset($result->versions[0]) && empty($result->versions[0]->html_content)) {
            return false;
        }

        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{SoldTo_Cust}', $order['SoldTo_Cust'], $html);
        $html = str_replace('{Sales_Doc}', $order['Sales_Doc'], $html);
/*        $html = str_replace('{Admin_Email}', $order['Admin_Email'], $html);
            $html = str_replace('{Material}', $order['Material'], $html);
            $html = str_replace('{ISBN13}', $order['ISBN13'], $html);
            $html = str_replace('{Qty}', $order['Qty'], $html);
            // $html = str_replace('{}', $order[''], $html);
            $html = str_replace('{client_licensing_admin}', $order['admin_link'], $html);
*/
        $html = str_replace('{Order_Detail}', $order['Order_Detail'], $html);

        $auditEmail = array(
            'account_id' => $order['account_id'],
            'site_id' => $site_id,
            'email_from' => $from,
            'email_to' => $customer_email,
            'email_subject' => $subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($customer_email)) {
            DB::table('audit_emails')->insert($auditEmail);
            $use_job_queue = config('s3.use_job_queue');
            $cc = "digent@objo7dblbqhblrtwathf4sfo.3-21neaa.na15.apex.salesforce.com";
            // $cc = "sibme29@gmail.com";
            if (!empty($use_job_queue)) {
                JobQueue::add_job_queue($site_id, 1, $customer_email, $subject, $html, true, '', $cc);
            } else {
                $emailData = [
                    'from' => Sites::get_site_settings('static_emails', $site_id)['noreply'],
                    'from_name' => Sites::get_site_settings('site_title', $site_id),
                    'to' => $customer_email,
                    'subject' => $subject,
                    'template' => $html,
                    'cc' => $cc,
                ];
                Email::sendCustomEmail($emailData);
            }
            return TRUE;
        }

        return FALSE;

    }

/*
// We can remove this commented block after testing.
    public static function schedule_new_order_email($order){
        if(empty($order)) return false;
        $required_fields_for_order_email = ['Admin_Email', 'SoldTo_Cust', 'Sales_Doc', 'Material', 'ISBN13', 'Qty', 'admin_link', 'account_id'];
        $order = array_intersect_key($order, array_flip( $required_fields_for_order_email ) );
        // $data = [
        //     'sales_doc' => $order['Sales_Doc'],
        //     'admin_email' => $order['Admin_Email'],
        //     'params' =>  json_encode($order)
        // ];
        DB::table('hmh_order_emails_queue')->insert($order);
    }

    public static function process_new_order_email(){
        // First combine multiple same orders in one email.
        self::combine_same_orders_and_send_email();
        // Then process other unique orders.
        $query = "SELECT * FROM `hmh_orders` WHERE is_email_sent=0";
        $orders = DB::select($query);
        foreach ($orders as $order) {
            DB::statement("UPDATE `hmh_orders` SET is_email_sent=1 WHERE id=?",[$order->id]);
            self::send_new_order_email_2($order);
        }
    }

    public static function combine_same_orders_and_send_email(){
        $query = "SELECT * FROM `hmh_orders` WHERE is_email_sent=0 GROUP BY `Sales_Doc`, `Admin_Email` HAVING COUNT(*)>1;";
        $orders = DB::select($query);
        foreach ($orders as $order) {
            $Sales_Doc = $order->Sales_Doc;
            $Admin_Email = $order->Admin_Email;
            $order_emails = Hmh_order::where('Sales_Doc', $Sales_Doc)->where('Admin_Email', $Admin_Email)->where('is_email_sent', '0')->get();
            $order_detail_template = "";
            foreach ($order_emails as $order_email) {
                $order_detail_template .= order_detail_template($order_email);
                // Concatinate orders
            }
            $order['Order_Detail'] = $order_detail_template;
            self::send_new_order_email_2($order);
        }
    }
*/

    public static function send_new_order_email($order){
        // Create Template in SEND GrID and then fill values in here accordingly
        $site_id = app("Illuminate\Http\Request")->header("site_id");
        $site_id = !empty($site_id) ? $site_id : 2;
        $site_email_subject = Sites::get_site_settings('email_subject', $site_id);
        $from =  'Sibme HMH <' . Sites::get_site_settings('static_emails', $site_id)['noreply'] . '>';
        $customer_email = $order['Admin_Email'];

        $subject = "Your Blended Coaching with HMH’s Coaching Studio Order";

        $key = "hmh_order_confirmation_email_en";
        $result = SendGridEmailManager::get_send_grid_contents($key);
        if (!$result || !isset($result->versions[0]) && empty($result->versions[0]->html_content)) {
            return false;
        }

        $html = $result->versions[0]->html_content;
        $html = str_replace('<%body%>', '', $html);
        $html = str_replace('{SoldTo_Cust}', $order['SoldTo_Cust'], $html);
        $html = str_replace('{Sales_Doc}', $order['Sales_Doc'], $html);
        $html = str_replace('{Admin_Email}', $order['Admin_Email'], $html);
        $html = str_replace('{Material}', $order['Material'], $html);
        $html = str_replace('{ISBN13}', $order['ISBN13'], $html);
        $html = str_replace('{Qty}', $order['Qty'], $html);
        // $html = str_replace('{}', $order[''], $html);
        $html = str_replace('{client_licensing_admin}', $order['admin_link'], $html);

        $auditEmail = array(
            'account_id' => $order['account_id'],
            'site_id' => $site_id,
            'email_from' => $from,
            'email_to' => $customer_email,
            'email_subject' => $subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($customer_email)) {
            DB::table('audit_emails')->insert($auditEmail);
            $use_job_queue = config('s3.use_job_queue');
            $cc = "digent@objo7dblbqhblrtwathf4sfo.3-21neaa.na15.apex.salesforce.com";
            // $cc = "sibme29@gmail.com";
            if (!empty($use_job_queue)) {
                JobQueue::add_job_queue($site_id, 1, $customer_email, $subject, $html, true, '', $cc);
            } else {
                $emailData = [
                    'from' => Sites::get_site_settings('static_emails', $site_id)['noreply'],
                    'from_name' => Sites::get_site_settings('site_title', $site_id),
                    'to' => $customer_email,
                    'subject' => $subject,
                    'template' => $html,
                    'cc' => $cc,
                ];
                Email::sendCustomEmail($emailData);
            }
            return TRUE;
        }

        return FALSE;

    }

    public static function get_orders($admin_key){
        // $cp = new CodeProfiler;
        // $cp = App(\App\Services\CodeProfiler::class);
        // $cp = app()->singleton('App\Services\CodeProfiler');
        // $cp->p_open('process_orders');
        $orders = Hmh_order::where('admin_key', $admin_key)->get();
        // if(empty($orders)){
        //     return false;
        // }
        foreach ($orders as $key => $order) {
            // $orders[$key]['licenses'] = Hmh_order_license::where('order_id', $order['id'])->get();
            $order->licenses = '';
            $order->licenses = Hmh_order_license::where('order_id', $order['id'])->get();
        }
        // $cp->p_close('process_orders');
        return $orders;
    }

    public static function validate_admin($admin_key){
        return Hmh_order::where('admin_key', $admin_key)->exists();
    }

}