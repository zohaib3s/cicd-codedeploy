<?php
namespace App\Services;

use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
use Aws\ElasticTranscoder\ElasticTranscoderClient;

use \App\Models\Document;
use \App\Models\User;
use \App\Models\DocumentFiles;
use \App\Models\UserActivityLog;

class DocumentDownloader {

    public function __construct()
    {
    }
    
    function getDocument($id, $account_id, $user_id, $site_id) {
        $file = Document::find($id);
        if(!$file){
            return false;
        }

        if ($file->doc_type == 1) {
            $document_files_array = $this->get_document_url($file, $site_id);
            if (empty($document_files_array['url'])) {
                $file->published = 0;
                $document_files_array['url'] = $file->original_file_name;
                $document_files_array['file_path'] = $file->url;
                $file->encoder_status = $document_files_array['encoder_status'];
            } else {
                $file->encoder_status = $document_files_array['encoder_status'];
            }

            $document_url = $document_files_array['relative_url'];

            $aws_access_key_id = config('s3.access_key_id');
            $aws_secret_key = config('s3.secret_access_key');
            $aws_bucket = config('s3.bucket_name');
            $expires = '+20 minutes';

            // Create an Amazon S3 client object
            $client = new S3Client([
                'region' => 'us-east-1',
                'version' => 'latest',
                'credentials' => array(
                    'key' => config('s3.access_key_id'),
                    'secret' => config('s3.secret_access_key'),
                )
            ]);    

            if (!empty($document_url)) {
                $response = $client->doesObjectExist($aws_bucket, $document_url);
                if ($response) {
                    
                } else {
                    if (!empty($file->url) && isset($file->url)) {
                        $document_url = $file->url;
                        $response = $client->doesObjectExist($aws_bucket, $document_url);
                        if ($response) {
                            
                        } else {
                            return ['error'=>1, 'message'=>'This video is not available for Download.'];
                        }
                    } else {
                        return ['error'=>1, 'message'=>'This video is not available for Download.'];
                    }
                }
            } else {

                if (!empty($file->url) && isset($file->url)) {
                    $document_url = $file->url;
                    $response = $client->doesObjectExist($aws_bucket, $document_url);
                    if ($response) {
                        
                    } else {
                        return ['error'=>1, 'message'=>'This video is not available for Download.'];
                    }
                } else {
                    return ['error'=>1, 'message'=>'This video is not available for Download.'];
                }
            }

            //$extension = explode(".",$file->original_file_name);

            $path_info = pathinfo($file->original_file_name);

            $extension= isset($path_info['extension'])?$path_info['extension']:'';
            $account_folder_document = \DB::table("account_folder_documents")->where("document_id",$file->id)->first();
            $account_folder_document->title = str_replace(',', '', $account_folder_document->title) . (isset($extension)?".".$extension:"");
            $account_folder_document->title = str_replace("&","",str_replace(";","",/*str_replace("amp","",*/strip_tags($account_folder_document->title)/*)*/));
/*
            $result = $client->getObjectUrl(
                    $aws_bucket, $document_url, $expires, array(
                'ResponseContentType' => 'video/mp4',
                'ResponseCacheControl' => 'No-cache',
                'ResponseContentDisposition' => 'attachment; filename=' . utf8_encode($account_folder_document->title)
                    )
            );
*/

            try {
                $cmd = $client->getCommand('GetObject', [
                    'Bucket' => $aws_bucket,
                    'Key' => $document_url,
                    'ResponseCacheControl' => 'No-cache',
                    'ResponseContentDisposition' => 'attachment; filename=' . utf8_encode($account_folder_document->title)
                ]);
                $result = $client->createPresignedRequest($cmd, $expires)->getUri()->__toString();
            } catch (\Aws\S3\Exception\S3Exception $e) {
                echo $e->getMessage() . "\n";
            }

            $user_activity_logs = array(
                'ref_id' => $id,
                'site_id' => $site_id,
                'desc' => 'Download Resource',
                'url' => '/download_document',
                'type' => '13',
                'account_folder_id' => '',
                'environment_type' => '2',
            );
            UserActivityLog::user_activity_logs($site_id, $user_activity_logs, $account_id, $user_id);
            $user_detail = User::where(array('id' => $user_id))->first();

            if ($user_detail) {
                $user_detail = $user_detail->toArray();
                $this->create_churnzero_event('Resource+Viewed', $account_id, $user_detail['email']);
            }

            return $result;

        } else {
            // $document_url = "uploads/" . $file->url;

            $document_files_array = $this->get_document_url($file, $site_id);

            //$document_url = $document_files_array['relative_url'];
            // $document_url = "uploads/" . $file->url;    //BEFORE
            $document_url = $file->url;

            if ($file->doc_type == 2) {
                $document_url = "uploads/" . $file->url;
            }

            $aws_access_key_id = config('s3.access_key_id');
            $aws_secret_key = config('s3.secret_access_key');
            $aws_bucket = config('s3.bucket_name');
            $expires = '+20 minutes';

            // Create an Amazon S3 client object
            $client = new S3Client([
                'region' => 'us-east-1',
                'version' => 'latest',
                'credentials' => array(
                    'key' => config('s3.access_key_id'),
                    'secret' => config('s3.secret_access_key'),
                )
            ]);

            $path_info = pathinfo($file->original_file_name);

            $extension= isset($path_info['extension'])?$path_info['extension']:"";

            //$extension =  explode(".",$file->original_file_name);
            $account_folder_document = \DB::table("account_folder_documents")->where("document_id",$file->id)->first();

            $path_info2 = pathinfo($account_folder_document->title);
            
            $extension2 = isset($path_info2['extension']) ? $path_info2['extension'] : "" ;


            //$extension2 = explode(".",$account_folder_document->title);
            if(!empty($extension2))
            {
                //$account_folder_document->title = str_replace(',', '', $account_folder_document->title) ;
                $account_folder_document->title = str_replace(',', '', $account_folder_document->title);
            }
            else
            {
                $account_folder_document->title = str_replace(',', '', $account_folder_document->title) . (isset($extension)?".".$extension:"");
            }
            $account_folder_document->title = str_replace("&","",str_replace(";","",/*str_replace("amp","",*/strip_tags($account_folder_document->title)/*)*/));
            try {
                $cmd = $client->getCommand('GetObject', [
                    'Bucket' => $aws_bucket,
                    'Key' => $document_url,
                    'ResponseCacheControl' => 'No-cache',
                    'ResponseContentDisposition' => 'attachment; filename=' . utf8_encode($account_folder_document->title)
                ]);
                $result = $client->createPresignedRequest($cmd, $expires)->getUri()->__toString();
            } catch (\Aws\S3\Exception\S3Exception $e) {
                echo $e->getMessage() . "\n";
            }
            $user_activity_logs = array(
                'ref_id' => $id,
                'site_id' => $site_id,
                'desc' => 'Download Resource',
                'url' => '/download_document',
                'type' => '13',
                'account_folder_id' => '',
                'environment_type' => '2',
            );
            UserActivityLog::user_activity_logs($site_id, $user_activity_logs, $account_id, $user_id);
            $user_detail = User::where(array('id' => $user_id))->first();

            if ($user_detail) {
                $user_detail = $user_detail->toArray();
                $this->create_churnzero_event('Resource+Viewed', $account_id, $user_detail['email']);
            }
            

            return $result;
        }
    }

    function getDiscussionDocument($id, $account_id, $user_id, $site_id) {
        $file = Document::find($id);
        if(!$file){
            return false;
        }       
            // $document_url = "uploads/" . $file->url;

            $document_files_array = $this->get_document_url($file, $site_id);

            //$document_url = $document_files_array['relative_url'];
            // $document_url = "uploads/" . $file->url;    //BEFORE
            $document_url = $file->url;

            if ($file->doc_type == 2) {
                $document_url = "uploads/" . $file->url;
            }

            $aws_access_key_id = config('s3.access_key_id');
            $aws_secret_key = config('s3.secret_access_key');
            $aws_bucket = config('s3.bucket_name');
            $expires = '+20 minutes';

            // Create an Amazon S3 client object
            $client = new S3Client([
                'region' => 'us-east-1',
                'version' => 'latest',
                'credentials' => array(
                    'key' => config('s3.access_key_id'),
                    'secret' => config('s3.secret_access_key'),
                )
            ]);

            /*
            $extension =  explode(".",$file->original_file_name);
            
            $account_folder_document = \DB::table("account_folder_documents")->where("document_id",$file->id)->first();
            $extension2 = explode(".",$account_folder_document->title);
            if(!empty($extension2[1]))
            {
                $account_folder_document->title = str_replace(',', '', $account_folder_document->title) ;
            }
            else
            {
                $account_folder_document->title = str_replace(',', '', $account_folder_document->title) . (isset($extension[1])?".".$extension[1]:"");
            }
            */

            //$renamed_title = $file->original_file_name;
          
            $filename = $file->original_file_name;
            


            $filename = str_replace("&","",str_replace(";","",str_replace("amp","",strip_tags($filename))));
            try {
                $cmd = $client->getCommand('GetObject', [
                    'Bucket' => $aws_bucket,
                    'Key' => $document_url,
                    'ResponseCacheControl' => 'No-cache',
                    'ResponseContentDisposition' => 'attachment; filename=' . utf8_encode($filename)
                ]);
                $result = $client->createPresignedRequest($cmd, $expires)->getUri()->__toString();
            } catch (\Aws\S3\Exception\S3Exception $e) {
                echo $e->getMessage() . "\n";
            }
            $user_activity_logs = array(
                'ref_id' => $id,
                'site_id' => $site_id,
                'desc' => 'Download Resource',
                'url' => '/download_document',
                'type' => '13',
                'account_folder_id' => '',
                'environment_type' => '2',
            );
            UserActivityLog::user_activity_logs($site_id, $user_activity_logs, $account_id, $user_id);
            $user_detail = User::where(array('id' => $user_id))->first();

            if ($user_detail) {
                $user_detail = $user_detail->toArray();
                $this->create_churnzero_event('Resource+Viewed', $account_id, $user_detail['email']);
            }
            

            return $result;
        
    }

    function get_document_url($document_video, $site_id) {
        $videoFilePath = pathinfo($document_video->url);
        $videoFileExtension = isset($videoFilePath['extension'])?$videoFilePath['extension']:"";
        if ($document_video->published == 0) {
            return array(
                'url' => $document_video->url,
                'thumbnail' => '',
                'encoder_status' => $document_video->encoder_status
            );
        }

        $check_data = DocumentFiles::where("document_id", $document_video->id)
                                   ->where("default_web", true)
                                   ->where("site_id", $site_id)
                                   ->first();

        if ($check_data && !empty($check_data->url)) {

            $url = $check_data->url;
            $duration = $check_data->duration;
            $thumbnail = '';
            $videoFilePath = pathinfo($url);
            $videoFileName = $videoFilePath['filename'];
            //these are old files before JobQueue
            if ($check_data->transcoding_status == '-1') {

                $videoFilePathThumb = pathinfo($document_video->url);
                $videoFileNameThumb = $videoFilePathThumb['filename'];
            } else {
                $videoFileNameThumb = $videoFileName;
            }


            $poster_end_extension = "_thumb.png";
            if ($document_video->encoder_provider == '2') {
                $poster_end_extension = empty($document_video->thumbnail_number) ?
                        "_thumb_00001.png" :
                        '_thumb_' . sprintf('%05d', $document_video->thumbnail_number) . '.png';
            }/*
            elseif ($document_video->encoder_provider == '4') {
                $poster_end_extension = "_thumbnail.0000000.png";
            }
            */
            $thumbnail_image_path = '';
            if (config('s3.use_cloudfront') == true) {
                if ($check_data->transcoding_status == '2') {
                    $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/video-thumbnail.png',null,1);
                } else {
                    if ($videoFileExtension == 'mp3' || $videoFileExtension == 'm4a' || $videoFileExtension == 'wav') {
                        $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/audio-thumbnail.png');
                    } else {
                        if ($videoFilePath['dirname'] == ".")
                            $thumbnail_image_path = $this->getSecureAmazonUrl(($videoFileNameThumb) . "$poster_end_extension");
                        else
                            $thumbnail_image_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileNameThumb) . "$poster_end_extension");
                    }
                }
            } else {
                if ($videoFileExtension == 'mp3' || $videoFileExtension == 'm4a' || $videoFileExtension == 'wav') {
                    $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/audio-thumbnail.png');
                } else {
                    if ($videoFilePath['dirname'] == ".")
                        $thumbnail_image_path = $this->getSecureAmazonUrl(($videoFileNameThumb) . "$poster_end_extension");
                    else
                        $thumbnail_image_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileNameThumb) . "$poster_end_extension");
                }
            }

            if ($thumbnail_image_path == '')
                $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/video-thumbnail.png',null,1);

            $video_path = '';
            $relative_video_path = '';

            if (config('s3.use_cloudfront') == true) {

                if ($videoFilePath['dirname'] == ".") {

                    $video_path = $this->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) . ".mp4");
                    $relative_video_path = $videoFileName . ".mp4";
                } else {

                    $video_path = $this->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . ".mp4");
                    $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".mp4";
                }
            } else {

                if ($videoFilePath['dirname'] == ".") {

                    $video_path = $this->getSecureAmazonUrl(($videoFileName) . ".mp4");
                    $relative_video_path = $videoFileName . ".mp4";
                } else {

                    $video_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileName) . ".mp4");
                    $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".mp4";
                }
            }

            return array(
                'url' => $video_path,
                'relative_url' => $relative_video_path,
                'thumbnail' => $thumbnail_image_path,
                'duration' => $duration,
                'encoder_status' => (isset($document_video->encoder_status) ? $document_video->encoder_status : '')
            );
        } else {
            //todo
            //through the exception here
            return array(
                'url' => '',
                'relative_url' => '',
                'thumbnail' => '',
                'duration' => '',
                'encoder_status' => 'Error'
            );
        }

    }

    function getSecureAmazonUrl($file, $name = null) {
        $aws_access_key_id = config('s3.access_key_id');
        $aws_secret_key = config('s3.secret_access_key');
        $aws_bucket = config('s3.bucket_name');
        $expires = '+240 minutes';

        $client = new S3Client([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);

        try {
            $cmd = $client->getCommand('GetObject', [
                'Bucket' => $aws_bucket,
                'Key' => $file,
                'ResponseCacheControl' => 'No-cache',
                'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : basename($file))
            ]);
            $url = $client->createPresignedRequest($cmd, $expires)->getUri()->__toString();
            return $url;
        } catch (\Aws\S3\Exception\S3Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

    function getSecureAmazonSibmeUrl($file, $name = null) {
        $aws_access_key_id = config('s3.access_key_id');
        $aws_secret_key = config('s3.secret_access_key');
        $aws_bucket = config('s3.bucket_name_cdn');
        $expires = '+240 minutes';

        $client = new S3Client([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);

        try {
            $cmd = $client->getCommand('GetObject', [
                'Bucket' => $aws_bucket,
                'Key' => $file,
                'ResponseCacheControl' => 'No-cache',
                'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : basename($file))
            ]);
            $url = $client->createPresignedRequest($cmd, $expires)->getUri()->__toString();
            return $url;
        } catch (\Aws\S3\Exception\S3Exception $e) {
            echo $e->getMessage() . "\n";
        }

    }

    function getSecureAmazonCloudFrontUrl($resource, $name = '', $timeout = 600) {


        //This comes from key pair you generated for cloudfront
        $keyPairId = config('s3.cloudfront_keypair');

        //Read Cloudfront Private Key Pair
        $cloudFront = new CloudFrontClient([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);

        $streamHostUrl = config('s3.cloudfront_host_url');
        $resourceKey = $resource;
        $expires = time() + 21600;

        //Construct the URL
        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
            'private_key' => realpath(dirname(__FILE__) . '/..') . "/Config/cloudfront_key.pem",
            'key_pair_id' => config('s3.cloudfront_keypair'),
        ));

        return $signedUrlCannedPolicy;
    }
    
     function create_churnzero_event($event_name,$account_id,$user_email,$value = 1)
    {
           if(strpos($user_email,"@sibme.com") || strpos($user_email,"@jjtestsite.us")   )
           {
              return true;
           }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://analytics.churnzero.net/i?appKey=invw-q7Ivjwby8NI1F6qQcH1Gix0811ja7-Li4_1xWg&accountExternalId='.$account_id.'&contactExternalId='.$user_email.'&action=trackEvent&eventName='.$event_name.'&description='.$event_name.'&quantity='.$value );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
        
    }

}