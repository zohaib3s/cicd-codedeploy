<?php

namespace App\Services;

use App\Http\Controllers\VideoController;
use App\Models\AccountFolderDocument;
use App\Models\AccountFolderMetaData;
use App\Models\Comment;
use App\Models\GlobalSettings;
use App\Models\JobQueue;
use App\Models\Sites;
use App\Models\AccountFolder;
use App\Models\AccountFolderUser;
use App\Models\Document;
use App\Models\DocumentFiles;
use App\Models\SNSElementalUpdates;
use App\Models\User;
use Aws\ElasticTranscoder\ElasticTranscoderClient;
use App\Services\S3;
use App\Services\HelperFunctions;
use Aws\MediaConvert\MediaConvertClient;
use Aws\Exception\AwsException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class S3Services {

    public $amazon_base_url = '';
    public $bucket_name = '';
    public $access_key_id = '';
    public $secret_access_key = '';

    private $document_id = '';
    private $encoding_error_flag = false;

    function __construct($p_amazon_base_url, $p_bucket_name, $p_access_key_id, $p_secret_access_key) {

        $this->amazon_base_url = $p_amazon_base_url;
        $this->bucket_name = $p_bucket_name;
        $this->access_key_id = $p_access_key_id;
        $this->secret_access_key = $p_secret_access_key;
    }

    function publish_to_s3($file_path, $s3_folder_path, $del_file = true) {
        //AWS access info
        if (!defined('awsAccessKey'))
            define('awsAccessKey', $this->access_key_id);
        if (!defined('awsSecretKey'))
            define('awsSecretKey', $this->secret_access_key);

        //instantiate the class
        $s3 = new S3($this->access_key_id, $this->secret_access_key);

        $fileName = basename($file_path);

        //move the file
        if ($s3->putObjectFile($file_path, $this->bucket_name, $s3_folder_path . $fileName, S3::ACL_AUTHENTICATED_READ)) {
            if ($del_file)
                $this->deleteDir($file_path);
            return $this->amazon_base_url . "$s3_folder_path" . $fileName;
        } else {
            return "";
        }
    }

    function get_object_info_s3($bucket, $uri) {
        //AWS access info
        if (!defined('awsAccessKey'))
            define('awsAccessKey', $this->access_key_id);
        if (!defined('awsSecretKey'))
            define('awsSecretKey', $this->secret_access_key);

        //instantiate the class
        $s3 = new S3($this->access_key_id, $this->secret_access_key);

        return $s3->getObjectInfo($this->bucket_name, $uri);
    }

    function copy_from_s3($src_folder_path, $dest_folder_path) {
        //AWS access info
        if (!defined('awsAccessKey'))
            define('awsAccessKey', $this->access_key_id);
        if (!defined('awsSecretKey'))
            define('awsSecretKey', $this->secret_access_key);

        //instantiate the class
        $s3 = new S3($this->access_key_id, $this->secret_access_key);

        //move the file
        if ($s3->copyObject($this->bucket_name, $src_folder_path, $this->bucket_name, $dest_folder_path, S3::ACL_AUTHENTICATED_READ)) {
            //$s3->deleteObject($this->bucket_name, $src_folder_path);
            return $this->amazon_base_url . "$dest_folder_path";
        } else {
            return "";
        }
    }

    function copy_from_s3_wcontent($src_folder_path, $dest_folder_path, $content_type) {
        //AWS access info
        if (!defined('awsAccessKey'))
            define('awsAccessKey', $this->access_key_id);
        if (!defined('awsSecretKey'))
            define('awsSecretKey', $this->secret_access_key);

        //instantiate the class
        $s3 = new S3($this->access_key_id, $this->secret_access_key);

        //move the file
        if ($s3->copyObject($this->bucket_name, $src_folder_path, $this->bucket_name, $dest_folder_path, S3::ACL_AUTHENTICATED_READ, array(), array("Content-Type" => $content_type))) {
            //$s3->deleteObject($this->bucket_name, $src_folder_path);
            return $this->amazon_base_url . "$dest_folder_path";
        } else {
            return "";
        }
    }

    function set_acl_permission($src_folder_path) {
        //AWS access info
        if (!defined('awsAccessKey'))
            define('awsAccessKey', $this->access_key_id);
        if (!defined('awsSecretKey'))
            define('awsSecretKey', $this->secret_access_key);

        //instantiate the class

        $s3 = new S3($this->access_key_id, $this->secret_access_key);

        $acp = $s3->getAccessControlPolicy($this->bucket_name, $src_folder_path);
        $acp['acl'][] = array(
            'type' => 'Group'
            ,'uri' => 'http://acs.amazonaws.com/groups/global/AuthenticatedUsers'
            , 'permission' => 'FULL_CONTROL'
        );


        return $s3->setAccessControlPolicy($this->bucket_name, $src_folder_path, $acp);

    }

    function deleteDir($dirPath) {
        if (!is_dir($dirPath)) {
            $dirPath = dirname($dirPath);
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                $this->deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    function get_job_id() {
        $client = new ElasticTranscoderClient([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => $this->access_key_id,
                'secret' => $this->secret_access_key,
            )
        ]);

        $result = $client->listPipelines(array());

        if ($this->bucket_name == 'sibme-production') $pipelineId = $result['Pipelines'][1]['Id'];
        else $pipelineId = $result['Pipelines'][0]['Id'];

    }

    function encode_videos($s_url, $s3_relative_path, $file_name, $transcoder_type = 1, $name_modifier=null, $startTimecode=null, $endTimecode=null,$aws_transcoder_type=1, $trim_url="", $final_ext = '') {
        $path_parts = pathinfo($file_name);
        $extension = $path_parts['extension'];
        

        if ($transcoder_type == 1 && $aws_transcoder_type != 2) {
            // amazon
            try {

                $path_parts = pathinfo($file_name);
                $base_file_name = $path_parts['filename'];
                require_once('Services/Zencoder.php');

                $s_url = str_replace("#", urlencode("#"), $s_url);
                $s_url = str_replace("?", urlencode("?"), $s_url);

                $zencoder = new Services_Zencoder('39bdc265c56dc8e09b62b8fd38f475f9');
               
                $encoding_job = $zencoder->jobs->create(
                        array(
                            "input" => $s_url,
                            "outputs" => array(
                                array(
                                    "label" => "$extension",
                                    "base_url" => $this->amazon_base_url . "$s3_relative_path",
                                    "filename" => $base_file_name . "_enc.$extension",
                                    "public" => false,
                                    "thumbnails" => array(
                                        "base_url" => $this->amazon_base_url . "$s3_relative_path",
                                        "size" => "320x180",
                                        "filename" => $base_file_name . "_thumb"
                                    ),
                                    "notifications" => array("https://app.sibme.com/ZencoderNotification/received_notification")
                                )
                            )
                        )
                );

                // Success if we got here
                return $encoding_job->id;
            } catch (ErrorException $e) {
                return FALSE;
            }

        } elseif ($transcoder_type == 2 && $aws_transcoder_type != 2) {

            //elastic transcoder

            // $src_relative_path = str_replace($this->amazon_base_url, "", $s_url);
            $src_relative_path = ltrim(parse_url($s_url, PHP_URL_PATH),"/");

            $path_parts = pathinfo($file_name);
            $base_file_name = $path_parts['filename'];
            if(empty($final_ext))
            {
                $final_ext = $path_parts['extension'];
            }
            if(!empty($trim_url) && strtolower($final_ext) == "mp3"){
                $output_key = $trim_url;
            } else {
                $output_key = "$s3_relative_path/$base_file_name"."_720_enc.$extension";
            }
            $client = new ElasticTranscoderClient([
                'region' => 'us-east-1',
                'version' => 'latest',
                'credentials' => array(
                    'key' => $this->access_key_id,
                    'secret' => $this->secret_access_key,
                )
            ]);
    
            $result = $client->listPipelines(array());

            if ($this->bucket_name == 'sibme-production') $pipelineId = $result['Pipelines'][1]['Id'];
            else $pipelineId = '1444071830576-8h6mzh';
//'PresetId' => '1425215270054-j95h0f'
            $job_setting = array(
                'PipelineId' => $pipelineId,
                'Input' => array(
                    'Key' => "$src_relative_path",
                    'FrameRate' => 'auto',
                    'Resolution' => 'auto',
                    'AspectRatio' => 'auto',
                    'Interlaced' => 'auto',
                    'Container' => 'auto',
                ),
                'Output' => array(
                    'Key' => $output_key,
                    'ThumbnailPattern' => "$s3_relative_path/$base_file_name" ."_720_enc" . '_thumb_{count}',
                    'Rotate' => 'auto',
                    /*'Composition' => array(
                      array(
                          'TimeSpan' => array(
                              'StartTime' => '00:00:03.000',
                               'Duration' => '00:00:01.000'
                          )
                      )
                    ),*/
                    'PresetId' => '1443730387055-5y6awl'
                )

            );
            if(!empty($startTimecode) && !empty($endTimecode))
            {
                $job_setting['Output']['Composition'] = array(
                    array(
                        'TimeSpan' => array(
                            'StartTime' => $startTimecode.'.000',
                            'Duration' => $endTimecode.'.000'
                        )
                    )
                );
            }
            $job = $client->createJob($job_setting);

            return $job['Job']['Id'];

        } elseif ($aws_transcoder_type==2) {
            // $src_relative_path = str_replace($this->amazon_base_url, "", $s_url);
            $input_file_w_bucket = "s3://".$this->bucket_name."/".$file_name; 
            $job_id = $this->encode_with_elemental_mediaconvert($input_file_w_bucket, $name_modifier, $startTimecode, $endTimecode);
            return $job_id;
        }

    }

    public function elastic_clip_merging_job($path, $chunks, $s3_dest_folder_path, $name_modifier, $with_thumbnail = 0, $rotate_180 = 0, $base_file_name = "output")
    {
        $client = new ElasticTranscoderClient([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => $this->access_key_id,
                'secret' => $this->secret_access_key,
            )
        ]);

        $result = $client->listPipelines(array());

        if ($this->bucket_name == 'sibme-production') $pipelineId = $result['Pipelines'][1]['Id'];
        else $pipelineId = '1444071830576-8h6mzh';

        $inputs = [];
        foreach ($chunks as $chunk) {
            $input =  array(
                'Key' => $path.'/'.$chunk,
                'FrameRate' => 'auto',
                'Resolution' => 'auto',
                'AspectRatio' => 'auto',
                'Interlaced' => 'auto',
                'Container' => 'auto',
            );
            $inputs[] = $input;
        }

        $job_setting = array(
            'PipelineId' => $pipelineId,
            'Inputs' => $inputs,
            'Output' => array(
                'Key' => $s3_dest_folder_path.'/'."$base_file_name".$name_modifier."_720_enc.mp4",
                'ThumbnailPattern' => $s3_dest_folder_path.'/'."$base_file_name".$name_modifier ."_720_enc" . '_thumb_{count}',
                'Rotate' => $rotate_180 ? '180' : 'auto',
                'PresetId' => '1443730387055-5y6awl'
            )
        );
        $job = $client->createJob($job_setting);

        return $job['Job']['Id'];
    }

    public function encode_with_elemental_mediaconvert($input_file_w_path, $name_modifier=null, $startTimecode=null, $endTimecode=null, $millicast_preset = false) {
        $file_parts = pathinfo($input_file_w_path);
        $dir_name = $file_parts['dirname'] . "/";
        if(!strpos($dir_name,"transcoded")){
            $dir_name .= "transcoded/";
        }
        if($millicast_preset)
        {
            /*$preset = "Live Stream Millicast Preset"; // This is creating very poor quality video with no audio
            $preset_thumbnail = "Custom_Thumbnail_Millicast";*/ // This preset is Wrong need to delete.
            $preset = "Custom_elemental"; // This preset create good video but no audio.
            $preset_thumbnail = "Custom Thumbnail";//For live streams it is always creating a black thumbnail
        }
        else
        {
            $preset = "Custom_elemental";
            $preset_thumbnail = "Custom Thumbnail";
        }
        $jobSetting = [
            "OutputGroups" => [
                [
                    "CustomName" => "Video",
                    "Name" => "File Group",
                    "Outputs" => [
                        [
                            "Preset" => $preset,
                            // "Preset" => "Custom-Generic_Sd_Mp4_Avc_Aac_16x9_Sdr_1280x720p_24Hz_0.15Mbps_Qvbr_Vq4",
                        ]
                    ],
                    "OutputGroupSettings" => [
                        "Type" => "FILE_GROUP_SETTINGS",
                        "FileGroupSettings" => [
                            "Destination" => $dir_name
                            // "Destination" => "s3://sibme-production/elemental_mediaconvert/"
                        ]
                    ],
                ],
                [
                    "CustomName" => "Thumbnail",
                    "Name" => "File Group",
                    "Outputs" => [
                        [
                            "Preset" => $preset_thumbnail,
                            "Extension" => "png",
                            "NameModifier" => "_thumbnail"
                        ]
                    ],
                    "OutputGroupSettings" => [
                        "Type" => "FILE_GROUP_SETTINGS",
                        "FileGroupSettings" => [
                            "Destination" => $dir_name 
                            // "Destination" => "s3://sibme-production/elemental_mediaconvert/"
                        ]
                    ],
                ],
            ],
            "AdAvailOffset" => 0,
            "Inputs" => [
                [
                    "AudioSelectors" => [
                        "Audio Selector 1" => [
                            "Offset" => 0,
                            "DefaultSelection" => "DEFAULT",
                            "ProgramSelection" => 1,
                        ]
                    ],
                    "VideoSelector" => [
                        "ColorSpace" => "FOLLOW",
                        "Rotate"=> "AUTO"
                    ],
                    "FilterEnable" => "AUTO",
                    "PsiControl" => "USE_PSI",
                    "FilterStrength" => 0,
                    "DeblockFilter" => "DISABLED",
                    "DenoiseFilter" => "DISABLED",
                    "TimecodeSource" => "ZEROBASED",
                    // "FileInput" => "s3://sibme-production/ksaleem_recent.mov"
                    "FileInput" => $input_file_w_path
                ]
            ],
            "TimecodeConfig" => [
                "Source" => "EMBEDDED"
            ]
        ];

        if(!empty($name_modifier)){
            $jobSetting['OutputGroups'][0]['Outputs'][0]["NameModifier"] = $name_modifier;
            $jobSetting['OutputGroups'][1]['Outputs'][0]["NameModifier"] = $name_modifier."_thumbnail";
        }

        if(!empty($startTimecode) && !empty($endTimecode)){
            $jobSetting['OutputGroups'][0]['OutputGroupSettings']["FileGroupSettings"]["Destination"] = $dir_name;
            $jobSetting['OutputGroups'][1]['OutputGroupSettings']["FileGroupSettings"]["Destination"] = $dir_name;
            $frame = ":05";
            $jobSetting['Inputs'][0]['InputClippings'] = [
                                                            [
                                                                'StartTimecode' => $startTimecode . $frame,
                                                                'EndTimecode' => $endTimecode . $frame,
                                                            ]
                                                         ];
        }
        return $this->submitElementalJob($jobSetting);

    }

    public function getMediaConvertClient()
    {
        $mediaConvertClient = new MediaConvertClient( [
            'version' => '2017-08-29',
            'region' => 'us-east-1',
            'endpoint' => 'https://q25wbt2lc.mediaconvert.us-east-1.amazonaws.com',
            'credentials' => [
                // 'key'    => 'AKIAIXPDDXXGZVUBMUFA',
                // 'secret' => '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',
                'key'    => 'AKIAJ4ZWDR5X5JKB7CZQ',
                'secret' => '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',
            ]
        ]);
        return $mediaConvertClient;
    }

    public function submitElementalJob($jobSetting, $job_template = 'Custom-Generic_Mp4_Hev1_Avc_Aac_Sdr_Qvbr')
    {
        $attempts = 0;
        do {
            try {
                $mediaConvertClient = $this->getMediaConvertClient();

                $free_queue = $this->qetAvailableQueueName();
                //Log::info("free_queue :" . $free_queue);
                $job = [
                    "Role" => "arn:aws:iam::614596103109:role/MediaConvert_Transcoder_Default_Role",
                    "Settings" => $jobSetting, //JobSettings structure
                    // "JobTemplate" => "arn:aws:mediaconvert:us-east-1:614596103109:jobTemplates/Custom-Generic_Mp4_Hev1_Avc_Aac_Sdr_Qvbr",
                    "Queue" => $free_queue,
                    "StatusUpdateInterval" => "SECONDS_30",
                    "Priority" => 0
                ];
                if($job_template !== false)
                {
                    $job['JobTemplate'] = $job_template;
                }
                $result = $mediaConvertClient->createJob($job);
                if(!empty($this->document_id) && $this->encoding_error_flag){
                    Document::where("id", $this->document_id)->update(["encoder_status_detail" => "Elemental Job Created in attempt # ".$attempts]);
                    $this->encoding_error_flag = false;
                    $this->document_id = '';
                }
                return $result['Job']['Id'];
            } catch (AwsException $e) {
                // output error message if fails
                $error = $e->getMessage();
                echo $e->getMessage();
                echo "\n";
                if(!empty($this->document_id)){
                    Document::where("id", $this->document_id)->update(["encoder_status_detail"=> "Error in Elemental Job Creation: ".$e->getMessage() ]);
                    $this->encoding_error_flag = true;
                    Log::info("Error in Document ID = " . $this->document_id . " Detail: " . $e->getMessage());
                } else {
                    Log::info("Error Encoding Video: " . $e->getMessage());
                }
                $attempts++;
                sleep(3);
                continue;
            }
            break;
        } while($attempts < 2);
    }

    public function processElementalJobs(){
        $documents = Document::where("doc_type", 1)->where("aws_transcoder_type", 2)->whereNotNull('zencoder_output_id')->where("encoder_status", "!=", "complete")->where("encoder_status", "!=", "error")->get();
        foreach($documents as $document){
            if(strpos($document->zencoder_output_id, '-')===false){
                continue;
            }
            $job = $this->getElementalJobDetail($document);
            if(empty($job)){
                continue;
            }
            $status = $job['status'];
            // \Log::info("^^^ ",[$status, $document->id, $document->encoder_status]);
            if($status=="complete") {
                $document->published = 1;
                $document->encoder_status = "Complete";
                $document->video_duration = $job['duration'];
            }
            else if($status=="error" || $status=="Error")
            {
                $document->video_duration = 0;
                $document->is_processed = 0;
                $document->published = 1;
                $document->encoder_status = "Error";
                $document->save();
            }
            $document->encoder_status = $status;
            if($job['status']=="complete") {
                $document_file = DocumentFiles::where("document_id", $document->id)->first();
                if(!$document_file){
                    $document_file = new DocumentFiles();
                    $document_file->document_id = $document->id;
                }
                $file_parts = pathinfo($document->url);
                $transcoded_url = $file_parts['dirname'] . "/" . $file_parts['basename'];
                if(!strpos($file_parts['dirname'],"transcoded")){
                    $transcoded_url = $file_parts['dirname'] . "/transcoded" . "/" . $file_parts['basename'];
                }
        
                $document_file->url = $transcoded_url;
                $document->url = $transcoded_url;
                $document_file->resolution = $job['resolution'];
                $document_file->duration = $job['duration'];
                $document_file->transcoding_status = 3;
                $document_file->transcoding_status_details = $status;
                $document_file->debug_logs = "Lumen::S3Services::processElementalJobs::line=555::document_id=".$document->id."::status=3::".(!$document_file ? 'created':'updated');
                $document_file->save();

                $document->save();

                $this->execute_thumbnail_websocket($document);

                // If Huddle
                $account_folder_document = AccountFolderDocument::where('document_id', $document->id)->first();
                $account_folder = AccountFolder::where('account_folder_id', $account_folder_document->account_folder_id)->first();
                if($account_folder->folder_type=="1"){
                    $creator = AccountFolderUser::join("users as u", "AccountFolderUser.user_id", "=", "u.id")->where("user_id",$document->created_by)->where("account_folder_id",$account_folder_document->account_folder_id)->select(["user_id","role_id","first_name","last_name","email"])->first();
                } else {
                    $creator = User::where("id",$document->created_by)->select(["id as user_id","first_name","last_name","email"])->first();
                }

                self::sendVideoPublishedEmailToHuddleParticipants($document->site_id, $account_folder->name, $account_folder->account_folder_id, $document->id, $creator, $account_folder->account_id, $account_folder->folder_type);
                //SW-4741 generate thummbnail for transcoder
                $file_parts = pathinfo($transcoded_url);
                $thumb_url = $file_parts['dirname'] . "/" . $file_parts['filename'];
                $s_url = $this->copy_from_s3($thumb_url.'_thumbnail.0000000.png', $thumb_url.'_thumb_00001.png');
            }

            if(isset($account_folder) && isset($document))
            {
                self::fireElementalWebSocket($document->id,$account_folder->folder_type,$account_folder->account_id,$account_folder->account_folder_id,$document->created_by,$document->site_id, $status);
            }

        }

    }
/*
    public function sendVideoPublishedNotification($document){
        $account_folder_document = AccountFolderDocument::where('document_id', $document->document_id)->first();
        $account_folder = AccountFolder::where('account_folder_id', $account_folder_document->account_folder_id)->first();
        $creator = User::find($document->created_by);
        $huddleUsers = AccountFolder::getHuddleUsers($account_folder_document->account_folder_id, $document->site_id);
        $account_id = $account_folder->account_id;
        
    }
*/
    public function checkJobStatusInTable($job_id, $document)
    {
        $record = SNSElementalUpdates::where('job_id', $job_id)->first();
        $hours = GlobalSettings::getSettingByName('manual_check_elemental_job_status_after_hours', 4);
        $manually_check_time = time()-(60*60*$hours);
        $check_manually = 0;
        if(!$record)
        {

            if(strtotime($document->created_date) <= $manually_check_time)
            {
                $check_manually = 1;
            }
            return ['status'=>false,'success'=>false, 'message'=>"No Record Found", 'check_manually'=>$check_manually];
        }
        if($record->job_status !== 'COMPLETE' && strtotime($record->last_edit_date) <= $manually_check_time )
        {
            return ['status'=>false,'success'=>false, 'message'=>"Record found but not updated since $hours Hours", 'check_manually'=>1];
        }
        return ['success'=>true,'status'=>strtolower($record->job_status), 'duration'=>$record->duration, 'resolution'=> $record->width."x".$record->height];
    }

    public function getElementalJobDetail($document, $support_checking = 0){
        if (empty($document) || empty($document->zencoder_output_id)) {
            return false;
        }
        $job_id = $document->zencoder_output_id;

        $result = $this->checkJobStatusInTable($job_id, $document);
        if($result['status'])
        {
            return $result;
        }
        else if($result['check_manually'] != 1 && !$support_checking)
        {
            return false;
        }

        $mediaConvertClient = $this->getMediaConvertClient();
        $attempts = 0;
        do {
            try {
                $result = $mediaConvertClient->getJob([
                    'Id' => $job_id,
                ]);
                $seconds = 0;
                if(isset($result['Job']['OutputGroupDetails'][0]['OutputDetails'][0]['DurationInMs'])){
                    $milli_sec = $result['Job']['OutputGroupDetails'][0]['OutputDetails'][0]['DurationInMs'];
                    $seconds = floor($milli_sec/1000);
                }
                $resolution = isset($result['Job']['OutputGroupDetails'][0]['OutputDetails'][0]['VideoDetails']['WidthInPx'])
                                ? $result['Job']['OutputGroupDetails'][0]['OutputDetails'][0]['VideoDetails']['WidthInPx'] ."x". $result['Job']['OutputGroupDetails'][0]['OutputDetails'][0]['VideoDetails']['HeightInPx']
                                : "";
                $job = [
                        'status' => strtolower($result['Job']['Status']),
                        'duration' => $seconds,
                        'resolution' => $resolution,
                        'success' => true
                        ];
                if($this->encoding_error_flag){
                    Document::where("id", $document->id)->update(["encoder_status_detail"=> "Elemental Job Processed in attempt # ".$attempts ]);
                    $this->encoding_error_flag = false;
                }
                return $job;
            } catch (AwsException $e) {
                // output error message if fails
                $error = $e->getMessage();
                // echo $error;
                // echo "\n";
                $attempts++;
                $data = ["encoder_status_detail"=> "Error in Processing Elemental Job: ".$error];
                if ($attempts==2) {
                    // if (strpos($error, 'TooManyRequestsException') ) {
                        $data['encoder_status'] = "Error";
                        // If we open this block, on Error the video would get published. So, we have to check if this is good or not. We are disabling this temporarily.
                        /* 
                        $data['video_duration'] = 0;
                        $data['is_processed'] = 0;
                        $data['published'] = 1;
                        */
                    // }
                }
                Document::where("id", $document->id)->update($data);
                $this->encoding_error_flag = true;
                Log::info("Error in Document ID = " . $document->id . " Detail: " . $error);
                sleep(3);
                continue;
            }
            break;
        } while($attempts < 2);
        if($this->encoding_error_flag && $support_checking)
        {
            return ['success'=>false, "message"=>$data['encoder_status_detail']];
        }
    }

    public function getElasticJobDetail($zencoder_output_id)
    {
        $client = new ElasticTranscoderClient([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => $this->access_key_id,
                'secret' => $this->secret_access_key,
            )
        ]);
        $error = "";
        try {

            if (preg_match("/^\d{13}-\w{6}$/", $zencoder_output_id)) {

                sleep(1);
                $result = $client->readJob(array(
                    'Id' => $zencoder_output_id
                ));
            }
        } catch (\Exception $e) {

            $error = $e->getMessage();
        }


        if (isset($result) && isset($result['Job']) && !empty($result['Job']['Status'])) {
            $job = [
                'status' => strtolower($result['Job']['Status']),
                'duration' => 0,
                'resolution' => 1,
                'full' => $result['Job'],
                'success' => true
            ];
        }
        else
        {
            $job = ['status'=>false, 'success'=>false, 'message'=>$error];
        }
        return $job;
    }

    public static function execute_thumbnail_websocket($document, $live_recording_event = 0){
        $account_folder = AccountFolderDocument::get_document_folder_type($document->id);
        if(!$account_folder){
            return false;
        }
        $doc_id = $document->id;
        $site_id = $document->site_id;
        $user_id = $document->created_by;
        $account_id = $account_folder['account_id'];
        $huddle_id = $account_folder['account_folder_id'];
        $folder_type = $account_folder['folder_type'];

        $res = Document::get_single_video_data($document->site_id, $document->id, $folder_type, 1, $huddle_id, $document->created_by);
        $res['updated_by'] = $document->created_by;
        $channel_name = 'huddle-details-' . $huddle_id;
        if ($folder_type=='3') {
            $channel_name = "workspace-" . $account_id . "-" . $document->created_by;
        }
        if(empty($res["title"])){
            $res["title"] = [];
        }
        $event = [
            'channel' => $channel_name,
            'event' => "resource_renamed",
            'data' => $res,
            'is_dummy' => 1,
            'video_file_name' => $res["title"],
            'document_id' => $doc_id,
            'video_id' => $doc_id,
            'reference_id' => $doc_id,
            'item_id' => $doc_id,
            'huddle_id' => $huddle_id,
            'user_id' => $user_id,
            'account_folder_id' => $huddle_id,
            'site_id' => $site_id,
            'live_video_event' => $live_recording_event
        ];
        HelperFunctions::broadcastEvent($event);
    }

    public static function sendVideoPublishedNotification($huddle_name, $huddle_id, $video_id, $creator_first_name, $creator_last_name, $account_id, $recipient_email, $huddle_user_id, $huddle_type, $email_type = '') {
        $is_email_notification_allowed = HelperFunctions::is_email_notification_allowed($account_id);
        if(!$is_email_notification_allowed){
            return false;
        }

        if (empty($recipient_email))
            return FALSE;

        // $users = \DB::table("users")->where('id', $huddle_user_id)->first();
        $users = User::find($huddle_user_id);
        $site_id = $users->site_id;

        $account_info = \DB::table("accounts")->where('id', $account_id)->first();

        $sibme_base_url = Sites::get_base_url($site_id);

        $from = $users->first_name . " " . $users->last_name . " " . $account_info->company_name . " <" . Sites::get_site_settings('static_emails',$site_id)['noreply'] . ">";
        $to = $recipient_email;

        $lang = $users->lang;
        $key = "video_publish_huddle_" . $site_id . "_" . $lang;

        $template = 'default';
        $sendAs = 'html';
        $lang_id = $users->lang;
        if ($lang_id == 'en') {
            $subject = Sites::get_site_settings('email_subject',$site_id) . ' - Video Added';
        } else {
            $subject = Sites::get_site_settings('email_subject',$site_id) . ' - Video Añadido';
        }

        //$htype = AccountFolderMetaData::getMetaDataValue($huddle_id, $site_id);

        if ($huddle_type == '1') {
            $url = $sibme_base_url . "huddles/view/" . $huddle_id . "/1/" . $video_id.'/3';
        } elseif ($huddle_type == '2') {
            $url = $sibme_base_url . "VideoLibrary/view/" . $huddle_id.'/4';
        } elseif ($huddle_type == '3') {
            $url = $sibme_base_url . "MyFiles/view/1/" . $huddle_id.'/5';
        } else {
            $huddle_type = '1';
            $url = $sibme_base_url . "huddles/view/" . $huddle_id . "/1/" . $video_id.'/6';
        }

        /*$params = array(
            'huddle_name' => $huddle_name,
            'account_name' => $account_info->company_name,
            'huddle_id' => $huddle_id,
            'video_id' => $video_id,
            'creator' => $creator,
            'huddle_type' => $huddle_type,
            'participated_user' => $huddleUsers,
            'authentication_token' => $auth_token,
            'url' => $url,
            'htype' => $htype,
            'huddle_user_id' => $huddle_user_id,
            'email_type' => $email_type
        );*/

        $html = '';
        $key = '';

        if ($huddle_type == 3) {
            $lang = $users->lang;
            $key = "video_publish_workspace_" . $site_id . "_" . $lang;
            $result = SendGridEmailManager::get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{redirect}', $url, $html);
            $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
            $html = str_replace('{unsubscribe}', $sibme_base_url . '/subscription/unsubscirbe_now/' . $huddle_user_id . "/" . $email_type, $html);
        } elseif ($huddle_type == 2) {
            $lang = $users->lang;
            $key = "video_publish_lib_" . $site_id . "_" . $lang;
            $result = SendGridEmailManager::get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{account_name}', $account_info->company_name, $html);
            $html = str_replace('{huddle_name}', $huddle_name, $html);
            $html = str_replace('{redirect}', $url, $html);
            $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
            $html = str_replace('{unsubscribe}', $sibme_base_url . '/subscription/unsubscirbe_now/' . $huddle_user_id . "/" . $email_type, $html);
        } else {
            $lang = $users->lang;
            $key = "video_publish_huddle_" . $site_id . "_" . $lang;
            $result = SendGridEmailManager::get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $creator_first_name . " " . $creator_last_name, $html);
            $html = str_replace('{huddle_name}', $huddle_name, $html);
            $html = str_replace('{redirect}', $url, $html);
            $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
            $html = str_replace('{unsubscribe}', $sibme_base_url . '/subscription/unsubscirbe_now/' . $huddle_user_id . "/" . $email_type, $html);
        }

        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $from,
            'email_to' => $to,
            'email_subject' => $subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );

        if (!empty($to)) {

            \DB::table('audit_emails')->insert($auditEmail);
            $use_job_queue = config('s3.use_job_queue');

            if ($use_job_queue) {
                JobQueue::add_job_queue($site_id, 1, $to, $subject, $html, true);
                return TRUE;
            }
        }
        return FALSE;
    }

    public static function fireElementalWebSocket($doc_id,$folder_type,$account_id,$huddle_id,$user_id,$site_id,$encoder_status)
    {
        $res = Document::get_single_video_data($site_id, $doc_id, $folder_type, 1, $huddle_id, $user_id);
        if($res)
        {
            $res['updated_by'] = $user_id;
            $channel_name = 'huddle-details-' . $huddle_id;
            if ($folder_type == 3) {
                $channel_name = "workspace-" . $account_id . "-" . $user_id;
            }

            $event = [
                'channel' => $channel_name,
                'event' => "resource_renamed",
                'data' => $res,
                'video_file_name' => $res["title"],
                'document_id' => $doc_id,
                'video_id' => $doc_id,
                'reference_id' => $doc_id,
                'item_id' => $doc_id,
                'huddle_id' => $huddle_id,
                'user_id' => $user_id,
                'account_folder_id' => $huddle_id,
                'site_id' => $site_id,
                'encoder_status' => $encoder_status
            ];
            HelperFunctions::broadcastEvent($event);
        }
    }

    public static function sendVideoPublishedEmailToHuddleParticipants($site_id, $current_account_folder_name, $account_folder_id, $video_id, $creator, $account_id, $current_account_folder_folder_type) {
        if($current_account_folder_folder_type=="1"){
            $is_email_notification_allowed = HelperFunctions::is_email_notification_allowed($account_id, 'enable_emails_for_upload_video');
            if(!$is_email_notification_allowed){
                return false;
            }
    
            // Send Email to Huddle Participants
            $participants = AccountFolder::getHuddleUsersIncludingGroups($site_id, $account_folder_id, $creator->user_id, $creator->role_id, true, [200,210,220]);
            if(!isset($participants['participants']) && empty($participants['participants'])){
                return false;
            }
            $participants = $participants['participants'];
            foreach ($participants as $participant) {
                if($creator->user_id==$participant['user_id']){
                    continue; // Skip current user. Because he is uploader of this video.
                }
                S3Services::sendVideoPublishedNotification($current_account_folder_name, $account_folder_id, $video_id, $creator->first_name, $creator->last_name, $account_id, $participant['user_email'], $participant['user_id'], $current_account_folder_folder_type, '8');
            }
        } else {
            // Workspace -> Send Email to Creator == Self
            S3Services::sendVideoPublishedNotification($current_account_folder_name, $account_folder_id, $video_id, $creator->first_name, $creator->last_name, $account_id, $creator->email, $creator->user_id, $current_account_folder_folder_type, '8');
        }
    }

    function trim_videos($s_url, $s3_relative_path, $file_name, $startTime, $endTime) {



            //amazon

            $src_relative_path = str_replace($this->amazon_base_url, "", $s_url);

            $path_parts = pathinfo($file_name);
            $base_file_name = $path_parts['filename'];

            $timestamp = "_".strtotime('now');

            $base_file_name_output = $base_file_name.$timestamp;

            $src_relative_path_output = str_replace("_enc.mp4" , $timestamp."_enc.mp4", $src_relative_path);

            $client = new ElasticTranscoderClient([
                'region' => 'us-east-1',
                'version' => 'latest',
                'credentials' => array(
                    'key' => $this->access_key_id,
                    'secret' => $this->secret_access_key,
                )
            ]);

            $result = $client->listPipelines(array());
            if ($this->bucket_name == 'sibme-production') $pipelineId = $result['Pipelines'][1]['Id'];
            else $pipelineId = $result['Pipelines'][0]['Id'];

            $thumnail_pattern_path = pathinfo($src_relative_path_output);

            $job = $client->createJob(array(
                'PipelineId' => $pipelineId,
                'Input' => array(
                    'Key' => "$src_relative_path",
                    'FrameRate' => 'auto',
                    'Resolution' => 'auto',
                    'AspectRatio' => 'auto',
                    'Interlaced' => 'auto',
                    'Container' => 'auto',
                ),
                'Output' => array(
                      'Key' => "$src_relative_path_output",
                      'ThumbnailPattern' => $thumnail_pattern_path['dirname']. "/" . $thumnail_pattern_path['filename'] . '_thumb_{count}',
                      //'Rotate' => 'auto',
                      'Composition' => array(
                        array(
                            'TimeSpan' => array(
                                'StartTime' => $startTime,
                                 'Duration' => $endTime
                            )
                        )
                      ),
                      //'PresetId' => '1425215270054-j95h0f'
                      'PresetId' => '1443730387055-5y6awl'
                )

            ));

            return $job['Job']['Id']."||".str_replace('uploads/' , '' ,$src_relative_path_output);



    }

    public function qetAvailableQueueName($mediaConvertClient = "")
    {

        $mediaConvertClient = $this->getMediaConvertClient();

        $results = $mediaConvertClient->listQueues([]);


        //$final_queue = "";
        if(!empty($results["Queues"]))
        {
            foreach ($results["Queues"] as $queue)
            {
                if($queue["PricingPlan"] == "RESERVED" && ($queue["SubmittedJobsCount"] + $queue["ProgressingJobsCount"]) <=  env('ELEMENTAL_JOB_WAITING', 5) && $queue["Status"] == "ACTIVE")
                {
                    return $queue["Arn"];
                    //as soon as we get a free Reserved Queue loop should exit.
                }
            }
        }
        return "arn:aws:mediaconvert:us-east-1:614596103109:queues/Default";

    }
    
    public function encode_audios($s_url, $s3_relative_path, $file_name, $transcoder_type = 1){
        $src_relative_path = str_replace($this->amazon_base_url, "", $s_url);
        $path_parts = pathinfo($file_name);
        $base_file_name = $path_parts['filename'];
        $client = new ElasticTranscoderClient([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => $this->access_key_id,
                'secret' => $this->secret_access_key,
            )
        ]);
        $result = $client->listPipelines(array());
        if ($this->bucket_name == 'sibme-production') $pipelineId = $result['Pipelines'][1]['Id'];
        else $pipelineId = '1444071830576-8h6mzh';
//'PresetId' => '1425215270054-j95h0f'
        $job = $client->createJob(array(
            'PipelineId' => $pipelineId,
            'Input' => array(
                'Key' => "$src_relative_path",
                'FrameRate' => 'auto',
                'Resolution' => 'auto',
                'AspectRatio' => 'auto',
                'Interlaced' => 'auto',
                'Container' => 'auto',
            ),
            'Output' => array(
                'Key' => "$s3_relative_path/$base_file_name"."_enc.mp3",
                'PresetId' => '1579708033698-7u97hy'
            )
        ));
        return $job['Job']['Id'];
    }

    public function processAudioJobs()
    {
        $client = new ElasticTranscoderClient([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => $this->access_key_id,
                'secret' => $this->secret_access_key,
            )
        ]);

        $amazon_audios = Comment::where('ref_type', 6)->where('encoder_status', 'Processing')->get();


        for ($counter = 0; $counter < count($amazon_audios); $counter++) {

            $amazon_audio = $amazon_audios[$counter];

            if (empty($amazon_audio->zencoder_output_id))
                continue;
            if ((int)$amazon_audio->zencoder_output_id < -2)
                continue;

            $amazon_zencoder_output_id_array = explode($amazon_audio->zencoder_output_id, '-');


            //its jobQueue task
            if (count($amazon_zencoder_output_id_array) == 0)
                continue;

            echo $amazon_audio->id . "-" . $amazon_audio->zencoder_output_id;

            try {

                if (preg_match("/^\d{13}-\w{6}$/", $amazon_audio->zencoder_output_id)) {

                    //sleep(1);
                    $result = $client->readJob(array(
                        'Id' => $amazon_audio->zencoder_output_id
                    ));
                }
            } catch (\Exception $e) {

                echo $e->getMessage();
                continue;
            }


            if (isset($result) && isset($result['Job']) && !empty($result['Job']['Status'])) {

                if ($result['Job']['Status'] == "Complete" || $result['Job']['Status'] == "Error") {
                    $videoFilePath = pathinfo($amazon_audio->comment);
                    $new_path = $videoFilePath['dirname']."/".$videoFilePath['filename']."_enc.mp3";
                    Comment::where('id',$amazon_audio->id)->update(["comment"=>$new_path,"encoder_status"=>$result['Job']['Status']]);
                    $request_temp = new Request([]);
                    $request_temp->headers->set('current-lang', "en");
                    $request_temp->headers->set('site_id', $amazon_audio->site_id);
                    $videoController = new VideoController($request_temp);
                    $afd = AccountFolderDocument::where("document_id",$amazon_audio->ref_id)->first();
                    if($afd)
                    {
                        $input = [
                            'comment_id' => $amazon_audio->id,
                            'user_id' => $amazon_audio->created_by,
                            'video_id' => $amazon_audio->ref_id,
                            'videoId' => $amazon_audio->ref_id,
                            'huddle_id' => $afd->account_folder_id,
                        ];
                        $request = new Request($input);
                        $request->headers->set('current-lang', "en");
                        $request->headers->set('site_id', $amazon_audio->site_id);
                        $latest_comment = $videoController->getSingleCommentDetails($request);
                        $result['updated_comment'] = $latest_comment;
                        $result['status'] = "success";
                        if ($latest_comment['ref_type'] == 6) {
                            $latest_comment['comment'] = $videoController->get_cloudfront_url($latest_comment['comment']);
                        }
                        $event = [
                            'channel' => 'video-details-' . $latest_comment['ref_id'],
                            'event' => "comment_edited",
                            'data' => $latest_comment,
                            'comment' => $amazon_audio->comment,
                            'item_id' => $amazon_audio->id,
                            'is_workspace_comment' => $latest_comment['is_workspace_comment'],
                            'reference_id' => $latest_comment['ref_id'],
                            'video_id' => $latest_comment['ref_id'],
                            'document_id' => $latest_comment['ref_id'],
                            'site_id' => $amazon_audio->site_id,
                            'user_id' => $amazon_audio->created_by,
                            'huddle_id' => $afd->account_folder_id,
                        ];
                        HelperFunctions::broadcastEvent($event);
                        Log::info("Comment with id =  ".$amazon_audio->id. " encoder_status changed to = ".$result['Job']['Status']);
                    }
                    else
                    {
                        Log::error("Comment referenced video not found with id = ".$amazon_audio->ref_id. " , comment id is ".$amazon_audio->id);
                    }
                }

            }
        }
    }

    public function submitElementalClipsMergingJob($document, $screenShareClips)
    {
        $inputs = [];
        $main_url = "s3://".config('s3.bucket_name')."/". substr($document->url, 0, strrpos( $document->url, '/'));
        $file_name = substr($document->url, strrpos($document->url, '/') + 1);
        $file_name_without_ext = preg_replace('/\.[^.\s]{3,4}$/', '', $file_name);
        foreach ($screenShareClips as $screenShareClip) {
            $input = [
                'VideoSelector' =>
                    array (
                        'ColorSpace' => 'FOLLOW',
                        'Rotate' => 'AUTO',
                        'AlphaBehavior' => 'DISCARD',
                    ),
                'FilterEnable' => 'AUTO',
                'PsiControl' => 'USE_PSI',
                'FilterStrength' => 0,
                'DeblockFilter' => 'DISABLED',
                'DenoiseFilter' => 'DISABLED',
                'TimecodeSource' => 'ZEROBASED',
            ];

            if(isset($screenShareClip['start']))
            {
                $input['InputClippings'][0]['StartTimecode'] = $screenShareClip['start'];
                if($screenShareClip['end'] != "00:00:00:00")
                {
                    $input['InputClippings'][0]['EndTimecode'] = $screenShareClip['end'];
                }
                $input['FileInput'] = "s3://".config('s3.bucket_name')."/".$document->url;//cam video url
            }
            else
            {
                $url = "s3://".config('s3.bucket_name')."/".$screenShareClip['s3_path'];
                $input['FileInput'] = $url;//Screen Share File Url
            }
            $inputs[] = $input;
        }
        $jobSetting = array (
            'TimecodeConfig' =>
                [
                    'Source' => 'ZEROBASED',
                ],
            'OutputGroups' =>
                [
                    [
                        'CustomName' => 'Screen_Share_Clipping',
                        'Name' => 'File Group',
                        'Outputs' =>
                            [
                                [
                                    "Preset" => 'only_video_no_audio',
                                    "NameModifier"=> '_merged_clips',
                                ]
                            ],
                        'OutputGroupSettings' =>
                            [
                                'Type' => 'FILE_GROUP_SETTINGS',
                                'FileGroupSettings' =>
                                    [
                                        'Destination' => $main_url.'/Merged/',
                                    ]
                            ]
                    ]
                ],
            'AdAvailOffset' => 0,
            'Inputs' => $inputs,
        );
        //$job_template = "video_without_audio_and_thumbnail";
        return $this->submitElementalJob($jobSetting, false);
    }

    public function submitElementalChunksMergingJob($path, $chunks, $s3_dest_folder_path, $name_modifier, $with_thumbnail = 0, $rotate_180 = 0)
    {
        $inputs = [];
        $main_url = "s3://".config('s3.bucket_name')."/". $path;
        $destination = "s3://".config('s3.bucket_name')."/".$s3_dest_folder_path.'/';
        $preset_thumbnail = "Custom Thumbnail";
        foreach ($chunks as $chunk) {
            $input = [
                "AudioSelectors" => [
                        "Audio Selector 1" => [
                        "Offset"=> 0,
                        "DefaultSelection"=> "DEFAULT",
                        "ProgramSelection"=> 1
                    ]
                ],
                'VideoSelector' =>
                    array (
                        'ColorSpace' => 'FOLLOW',
                        'Rotate' => $rotate_180 ? 'DEGREES_180' : 'AUTO',
                        'AlphaBehavior' => 'DISCARD',
                    ),
                'FilterEnable' => 'AUTO',
                'PsiControl' => 'USE_PSI',
                'FilterStrength' => 0,
                'DeblockFilter' => 'DISABLED',
                'DenoiseFilter' => 'DISABLED',
                'TimecodeSource' => 'ZEROBASED',
                'FileInput' => $main_url.'/'.$chunk
            ];
            $inputs[] = $input;
        }
        $jobSetting = array (
            'TimecodeConfig' =>
                [
                    'Source' => 'ZEROBASED',
                ],
            'OutputGroups' =>
                [
                    [
                        'CustomName' => 'Offline_Video_Clipping',
                        'Name' => 'File Group',
                        'Outputs' =>
                            [
                                [
                                    "Preset" => 'Custom_elemental',
                                    /*"NameModifier"=> '_merged',*/
                                ]
                            ],
                        'OutputGroupSettings' =>
                            [
                                'Type' => 'FILE_GROUP_SETTINGS',
                                'FileGroupSettings' =>
                                    [
                                        'Destination' => $destination,
                                    ]
                            ]
                    ]
                ],
            'AdAvailOffset' => 0,
            'Inputs' => $inputs,
        );
        if($with_thumbnail)
        {
            $jobSetting['OutputGroups'][1] = [
                "CustomName" => "Thumbnail",
                "Name" => "File Group",
                "Outputs" => [
                    [
                        "Preset" => $preset_thumbnail,
                        "Extension" => "png",
                        "NameModifier" => "_thumbnail"
                    ]
                ],
                "OutputGroupSettings" => [
                    "Type" => "FILE_GROUP_SETTINGS",
                    "FileGroupSettings" => [
                        "Destination" => $destination
                    ]
                ],
            ];
        }
        if(!empty($name_modifier))
        {
            $jobSetting['OutputGroups'][0]['Outputs'][0]["NameModifier"] = $name_modifier;
        }
        //$job_template = "video_without_audio_and_thumbnail";
        return $this->submitElementalJob($jobSetting, false);
    }

    public function set_document_id($document_id){
        $this->document_id = $document_id;
    }
}

?>
