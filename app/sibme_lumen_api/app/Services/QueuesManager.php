<?php

namespace App\Services;

class QueuesManager {
/**
 * Redis Available functions: https://github.com/phpredis/phpredis#connection
 * To View Available Keys in redis:
 * redis-cli keys "*"
 * redis-cli LLEN "key"
 * Remove Queue Entries:
 * app('redis')->del('queues:operation');
 * https://www.cyberciti.biz/faq/how-to-flush-redis-cache-and-delete-everything-using-the-cli/
 * redis-cli -h {host-name} -p {port-name} FLUSHDB
 * redis-cli -h {host-name} -p {port-name} FLUSHALL
 */
    private static $enable_log=true;

    public static function sendToEmailQueue($class, $function, ...$args){
        if (config('core.use_queues')) {
            $queue = 'email';
            self::dispatch_job($queue, $class, $function, $args);
        } else {
            self::skip_queue($class, $function, $args);
        }
    }

    public static function sendToNotificationQueue($class, $function, ...$args){
        if (config('core.use_queues')) {
            $queue = 'notification';
            self::dispatch_job($queue, $class, $function, $args);
        } else {
            self::skip_queue($class, $function, $args);
        }
    }

    public static function sendToOperationQueue($class, $function, ...$args){
        if (config('core.use_queues')) {
            $queue = 'operation';
            self::dispatch_job($queue, $class, $function, $args);
        } else {
            self::skip_queue($class, $function, $args);
        }
    }

    private static function dispatch_job($queue, $class, $function, $args){
        self::create_log($queue, $class, $function, $args);
        $job = (new \App\Jobs\RegisterJobForQueue($class, $function, $args))->onQueue($queue);
        dispatch($job);
    }

    private static function skip_queue($class, $function, $args){
        // Skip Queue and execute the function now.
        call_user_func_array([app($class), $function], $args);
    }

    private static function create_log($queue, $class, $function, $args){
        if (self::$enable_log) {
            \Log::info('--------- Job Created in '.$queue.' Queue ----------');
            \Log::info([
                'class' => $class,
                'function' => $function,
                // 'Args' => $args
            ]);
        }
    }

}

?>