<?php
namespace App\Services\Dynamo;

use BaoPham\DynamoDb\DynamoDbModel;
use BaoPham\DynamoDb\Facades\DynamoDb;

use Aws\DynamoDb\Exception\DynamoDbException;

class Base extends DynamoDbModel{
    protected $dynamodb;
    public function __construct(){
        // With DynamoDB client you can call all functions provided by AWS DynamoDB SDK
        $this->dynamodb = DynamoDb::client();
    }
    protected function createBaseTable($params){
        try {
            $result = $this->dynamodb->createTable($params);
            return $result['TableDescription']['TableStatus'];
        } catch (DynamoDbException $e) {
            echo "Unable to create table:\n";
            echo $e->getMessage() . "\n";
            exit;
        }
    }

    protected function debugQuery($rawQuery){
        // Example Param: $this->where('id', $id)
        // $rawQuery = $this->where('id', $id)->toDynamoDbQuery();
        $rawQuery = $rawQuery->toDynamoDbQuery();
        // $op is either "Scan" or "Query"
        $op = $rawQuery->op;
        // The query body being sent to AWS
        $query = $rawQuery->query;
        echo "<pre>";       
        dd($op, $query);
    }

}