<?php
namespace App\Services\Dynamo;

use Carbon\Carbon;

use BaoPham\DynamoDb\Facades\DynamoDb;

class UserLoginSummary extends Base {

    /**
     * Read Operations:
     * 1. To update/insert, select 1 record by account id, user id and created_at using hash - Done. 
     * 2. For analytics, select all records by account id and created_at between START and END date - Done.
     * - - After selection, sum records by users - Done.
     * Write Operations:
     * 1. insert new record on every first login in a day - Done.
     * 2. update record based on account id, user id and created_at. increment 1 in login count - Done.
     */

    /**
     * Notes:
     * Lumen side: Move Login Activity on Launchpad (log entry against a particular account when user clicks on a Launchpad)
     * Dynamo Side: Create a Hash with md5(AccountID+UserID+CreatedAt) and save that hash as ID into DynamoDB, mark it as partition key - Done.
     * On Every Upsert operation use Hash to search for the user - Done.
     * Why are we using Unix Timestamp?
     * Change Created_at type to String and save date in ISO 8601 standard i.e, 2019-08-16 - Done
     * Mark Created_at as sort key - Done.
     * create secondary global index on account id (partition key) + created_at (sort key) - Done.
     * https://medium.com/cloud-native-the-gathering/querying-dynamodb-by-date-range-899b751a6ef2
     * Another approach to get Items on date range: see answer from "bsd": https://stackoverflow.com/questions/35963243/how-to-query-dynamodb-by-date-range-key-with-no-obvious-hash-key?noredirect=1&lq=1
     * Read "Warren P" answer: https://stackoverflow.com/questions/14836600/querying-dynamodb-by-date
     * 
     * Bulk Insert:
     * https://aws.amazon.com/blogs/database/implementing-bulk-csv-ingestion-to-amazon-dynamodb/
     */

    protected $table = 'user_login_summary_dev';
    protected $fillable = ['id', 'account_id', 'user_id', 'web_login_counts', 'is_active', 'created_at'];

    public function createTable(){
        $params = [
            'TableName' => $this->table,
            'KeySchema' => [
                [
                    'AttributeName' => 'id',
                    'KeyType' => 'HASH'  //Partition key
                ]
            ],
            'AttributeDefinitions' => [
                [
                    'AttributeName' => 'id',
                    'AttributeType' => 'S'
                ],
                [
                    'AttributeName' => 'created_at',
                    'AttributeType' => 'S'
                ],
                [
                    'AttributeName' => 'account_id',
                    'AttributeType' => 'N'
                ],
            ],
            'ProvisionedThroughput' => [
                'ReadCapacityUnits' => 10,
                'WriteCapacityUnits' => 10
            ],
            'GlobalSecondaryIndexes' => [
                [
                    'IndexName' => 'usersGSI',
                    'KeySchema' => [
                        [ 'AttributeName' => "account_id", 'KeyType' => "HASH" ],
                        [ 'AttributeName' => "created_at", 'KeyType' => "RANGE" ],
                    ],
                    'Projection' => [
                        'ProjectionType' => 'ALL'
                    ],
                    'ProvisionedThroughput' => [
                        'ReadCapacityUnits' => 10,
                        'WriteCapacityUnits' => 10
                    ]
                ]
            ]
    
        ];
        $result = $this->createBaseTable($params);
        return strtolower($result)=="active";
    }

/*
    public function createTable(){
        $params = [
            'TableName' => $this->table,
            'KeySchema' => [
                [
                    'AttributeName' => 'id',
                            'KeyType' => 'HASH'  //Partition key
                ],
                [
                    'AttributeName' => 'created_at',
                    'KeyType' => 'RANGE'  //Sort key
                ]
            ],
            'AttributeDefinitions' => [
                [
                    'AttributeName' => 'id',
                    'AttributeType' => 'S'
                ],
                [
                    'AttributeName' => 'created_at',
                    'AttributeType' => 'S'
                ],
                [
                    'AttributeName' => 'account_id',
                    'AttributeType' => 'N'
                ],
                // [
                //     'AttributeName' => 'user_id',
                //     'AttributeType' => 'N'
                // ],
                // [
                //     'AttributeName' => 'web_login_counts',
                //     'AttributeType' => 'N'
                // ],
                // [
                //     'AttributeName' => 'is_active',
                //     'AttributeType' => 'N'
                // ],       
            ],
            'ProvisionedThroughput' => [
                'ReadCapacityUnits' => 10,
                'WriteCapacityUnits' => 10
            ],
            'GlobalSecondaryIndexes' => [
                [
                    'IndexName' => 'usersGSI',
                    'KeySchema' => [
                        [ 'AttributeName' => "account_id", 'KeyType' => "HASH" ],
                        [ 'AttributeName' => "created_at", 'KeyType' => "RANGE" ],
                    ],
                    'Projection' => [
                        'ProjectionType' => 'ALL'
                    ],
                    'ProvisionedThroughput' => [
                        'ReadCapacityUnits' => 10,
                        'WriteCapacityUnits' => 10
                    ]
                ]
            ]

        ];
        $result = $this->createBaseTable($params);
        return strtolower($result)=="active";
    }
*/
    public function importData(){
        $file = public_path("login_counts.json");
        $time_start = microtime(true);

        $string = file_get_contents($file);
        $json_a=json_decode($string,true);
        $config = [
            'table' => $this->table,
            'error' => function($e) { if ($e instanceof \Exception) { echo $e->getMessage(); } }
           ];
        
        $putBatch = new \Aws\DynamoDb\WriteRequestBatch($this->dynamodb, $config);

        foreach($json_a as $item){
            $putBatch->put( DynamoDb::marshalItem($item) );
        }
        $putBatch->flush();

        $time_end = microtime(true);
        //dividing with 60 will give the execution time in minutes otherwise seconds
        //$execution_time = ($time_end - $time_start)/60;
        $execution_time = ($time_end - $time_start);
        
        //execution time of the script
        echo '<br><b>Total Execution Time:</b> '.$execution_time.' Seconds';
        
    }

    public function getTotalCount(){
        return $this->all()->count();
    }

    public function getPartitionKey($account_id, $user_id, $date){
        if(!Carbon::createFromFormat('Y-m-d', $date)){
            return false;
        }
        return md5($account_id.$user_id.$date);
    }

    public function saveRecord($account_id, $user_id){
        $id = $this->getPartitionKey($account_id, $user_id, date("Y-m-d"));
        // $id = $this->getPartitionKey($account_id, $user_id, "2015-09-12");

        // $this->debugQuery($this->where(['id' => '5aeb0c33-96c4-11ea-83e4-120924c29f6b']));
        // $this->debugQuery($this->where(['account_id' => $account_id, 'user_id' => $user_id, 'created_at' => "2015-09-12" ])->withIndex('usersGSI'));

        // $item = $this->where(['account_id' => $account_id, 'user_id' => $user_id, 'created_at' => "2015-09-12" ])->get();
        // dd(current($item->toArray()));

        $item = $this->find($id);
        if(!empty($item)){
            $item->web_login_counts += 1;
        } else {
            $item->id = $id;
            $item->user_id = $user_id;
            $item->is_active = 1;
            $item->account_id = $account_id;
            $item->created_at = $item->updated_at = date("Y-m-d");
            $item->web_login_counts = 1;
        }
        return $item->save();
    }

    public function getLoginCountsForAnalytics($account_id, $user_id_array, $from_date, $to_date){
        $items = $this->where('account_id', $account_id)
                      ->where('created_at', '>=', $from_date)
                      ->where('created_at', '<=', $to_date)
                      ->withIndex('usersGSI');
        if(!empty($user_id_array)){
            $items->whereIn('user_id',$user_id_array);
        }
        $items = $items->get();
        // $this->debugQuery($items);
        if(empty($items)) {
            return false;
        }

        $sum_per_user = [];
        foreach ($items as $key => $item) {
            $item = $item->toArray();
            $sum_per_user [$item['user_id']]['web_login_counts'][] = $item['web_login_counts'];
        }

        $total_per_user = [];
        foreach($sum_per_user as $user_id=>$metric){
            $total_per_user [$user_id]['web_login_counts'] = array_sum($metric['web_login_counts']);
        }

        return $total_per_user;
    }

}