<?php
namespace App\Services\Dynamo;

use Carbon\Carbon;

use BaoPham\DynamoDb\Facades\DynamoDb;

class HuddlesSummary extends Base {

    /**
     * Read Operations:
     * 1. To update/insert, select 1 item by account id, huddle_id and created_at using hash. 
     * 2. For analytics, select all records by account id and created_at between START and END date.
     * - - After selection, sum records by users.
     * Write Operations:
     * 1. Insert new record on every first activity in a day.
     * 2. Update record based on account id, huddle_id and created_at using hash.
     * 3. Update/Insert a particular User record in above selected main record with increment 1 in the given metric.
     */

    /**
     * Notes:
     * Lumen side: Use DynamoDB functions to log entries on each related activity.
     * Dynamo Side: Create a Hash with md5(AccountID+HuddleID+CreatedAt) and save that hash as ID into DynamoDB, mark it as partition key.
     * On Every Upsert operation use Hash to search for the Huddle record.
     * Mark Created_at as sort key.
     * create secondary global index on account id (partition key) + created_at (sort key).
     * 
     * Bulk Insert:
     * https://aws.amazon.com/blogs/database/implementing-bulk-csv-ingestion-to-amazon-dynamodb/
     */

    protected $table = 'huddles_summary_dev';
    protected $fillable = ['id', 'account_id', 'created_at', 'created_by', 'folder_type', 'huddle_id', 'is_account_active', 'is_huddle_active', 'parent_account_id', 'updated_at','users'];
    protected $activities = ['huddle_created_count', 'scripted_video_observations', 'shared_upload_counts', 'video_upload_counts', 'workspace_upload_counts', 'comments_initiated_count', 'documents_uploaded_count', 'library_upload_counts', 'workspace_comments_initiated_count', 'huddle_discussion_posts', 'workspace_videos_viewed_count', 'total_hours_uploaded', 'replies_initiated_count', 'workspace_resources_uploaded', 'library_shared_upload_counts', 'huddle_discussion_created', 'scripted_notes_shared_upload_counts', 'scripted_observations', 'library_videos_viewed_count', 'videos_viewed_count', 'workspace_resources_viewed', 'total_hours_viewed', 'documents_viewed_count' ];

    public function createTable(){
        $params = [
            'TableName' => $this->table,
            'KeySchema' => [
                [
                    'AttributeName' => 'id',
                    'KeyType' => 'HASH'  //Partition key
                ]
            ],
            'AttributeDefinitions' => [
                [
                    'AttributeName' => 'id',
                    'AttributeType' => 'S'
                ],
                [
                    'AttributeName' => 'created_at',
                    'AttributeType' => 'S'
                ],
                [
                    'AttributeName' => 'account_id',
                    'AttributeType' => 'N'
                ],
            ],
            'ProvisionedThroughput' => [
                'ReadCapacityUnits' => 10,
                'WriteCapacityUnits' => 10
            ],
            'GlobalSecondaryIndexes' => [
                [
                    'IndexName' => 'huddlesGSI',
                    'KeySchema' => [
                        [ 'AttributeName' => "account_id", 'KeyType' => "HASH" ],
                        [ 'AttributeName' => "created_at", 'KeyType' => "RANGE" ],
                    ],
                    'Projection' => [
                        'ProjectionType' => 'ALL'
                    ],
                    'ProvisionedThroughput' => [
                        'ReadCapacityUnits' => 10,
                        'WriteCapacityUnits' => 10
                    ]
                ]
            ]
    
        ];
        $result = $this->createBaseTable($params);
        return strtolower($result)=="active";
    }

    public function importData(){
        $file = public_path("analytics_huddle_summary.json");
        $time_start = microtime(true);

        $string = file_get_contents($file);
        $json_a=json_decode($string,true);
        $config = [
            'table' => $this->table,
            'error' => function($e) { if ($e instanceof \Exception) { echo $e->getMessage(); } }
           ];
        
        $putBatch = new \Aws\DynamoDb\WriteRequestBatch($this->dynamodb, $config);

        foreach($json_a as $item){
            $putBatch->put( DynamoDb::marshalItem($item) );
        }
        $putBatch->flush();

        $time_end = microtime(true);
        //dividing with 60 will give the execution time in minutes otherwise seconds
        //$execution_time = ($time_end - $time_start)/60;
        $execution_time = ($time_end - $time_start);
        
        //execution time of the script
        echo '<br><b>Total Execution Time:</b> '.$execution_time.' Seconds';
        
    }

    public function getTotalCount(){
        return $this->all()->count();
    }

    public function getPartitionKey($account_id, $huddle_id, $date){
        if(!Carbon::createFromFormat('Y-m-d', $date)){
            return false;
        }
        return md5($account_id.$huddle_id.$date);
    }

    public function saveRecord($account_id, $huddle_id, $user_id, $activity, $activity_value=1, $decrement=false){
        $id = $this->getPartitionKey($account_id, $huddle_id, date("Y-m-d"));
        // $id = $this->getPartitionKey($account_id, $huddle_id, "2015-09-14");

        // $this->debugQuery($this->where(['id' => '5aeb0c33-96c4-11ea-83e4-120924c29f6b']));
        // $this->debugQuery($this->where(['account_id' => $account_id, 'user_id' => $user_id, 'created_at' => "2015-09-12" ])->withIndex('usersGSI'));

        // $item = $this->where(['account_id' => $account_id, 'user_id' => $user_id, 'created_at' => "2015-09-12" ])->get();
        // dd(current($item->toArray()));

        $item = $this->find($id);
        // dd($item->toArray());
        // dd($item->users['user_'.$user_id]);
        if(!empty($item)){
            if(!isset($item->users)){
                $item->users = [];
            }
            $users = $item->users;
            if(!isset($item->users['user_'.$user_id])){
                $users['user_'.$user_id] = $this->saveUserDefaultMetrics($user_id, $activity, $activity_value);
            } else {
                if(isset($users['user_'.$user_id][$activity])){
                    $users['user_'.$user_id][$activity] += 1;
                } else {
                    $users['user_'.$user_id][$activity] = 1;
                }
            }
        } else {
            $item->id = $id;
            $item->account_id = $account_id;
            $item->created_at = $item->updated_at = date("Y-m-d");
            $item->created_by = $created_by; // Get from Huddle Info
            $item->folder_type = $folder_type; // Get from Huddle Info
            $item->huddle_id = $huddle_id;
            $item->is_account_active = 1;
            $item->is_huddle_active = 1;
            $item->parent_account_id = $parent_account_id; // Get Parent of given Account
            $users['user_'.$user_id] = $this->saveUserDefaultMetrics($user_id, $activity, $activity_value);
        }
        $item->users = $users;
        return $item->save();
    }

    public function getHuddleSummaryForAnalytics($account_id, $huddle_id_array, $from_date, $to_date){
        $items = $this->where('account_id', $account_id)
                      ->where('created_at', '>=', $from_date)
                      ->where('created_at', '<=', $to_date)
                      ->withIndex('huddlesGSI');
        if(!empty($huddle_id_array)){
            $items->whereIn('huddle_id',$huddle_id_array);
        }
        $items = $items->get();
        // $this->debugQuery($items);
        if(empty($items)) {
            return false;
        }

        $sum_per_user = [];
        foreach ($items as $key => $item) {
            foreach ($item->users as $key => $user) {
                foreach ($this->activities as $activity) {
                    $sum_per_user [$user["user_id"]][$activity][] = $user[$activity];
                }
            }
        }

        $total_per_user = [];
        foreach($sum_per_user as $user_id=>$metric){
            foreach ($this->activities as $activity) {
                $total_per_user [$user_id][$activity] = array_sum($metric[$activity]);
            }
        }

        return $total_per_user;
    }

    public function saveUserDefaultMetrics($user_id, $for_activity, $for_activity_value=1){
        $user ['user_id'] = $user_id;
        $user ['is_user_active'] = 1;

        foreach ($this->activities as $activity) {
            if($activity==$for_activity && ($activity=="total_hours_uploaded" || $activity=="total_hours_viewed")){
                $user [$activity] = $for_activity_value;
            } else if($activity==$for_activity){
                $user [$activity] = 1;
            } else {
                $user [$activity] = 0;
            }
        }
        return $user;
    }

}