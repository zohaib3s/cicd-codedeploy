<?php
namespace App\Services\Dynamo;

use Aws3\DynamoDb\Exception\DynamoDbException;

class HhuddlesSummary extends Base {
    private $tableName = 'huddles_summary_dev';
    public function getTotalCount(){
        $params = [
            'TableName' => $this->tableName,
            'Select' => 'COUNT'
        ];

        try {
            $result = $this->client->scan($params);
            return current($result)['Count'];
        } catch (DynamoDbException $e) {
            echo "Unable to query:\n";
            echo $e->getMessage() . "\n";
        }        
    }
}