<?php
namespace App\Services;

use Illuminate\Support\Str;

/**
 * Usage:
 *  1.       $cp = new \App\Services\CodeProfiler();
 *  2.       $cp->start("Set Title Here");
 *         // some code.
 *         // some code.
 *         // some code.
 *  3.      $result_link = $cp->end();
 *         dd($result_link)
 *         $cp = new \App\Services\CodeProfiler();
 *         $cp->start("Another Title");
 *         // some code.
 *         // some code.
 *         // some code.
 *         $result_link = $cp->end();
 *         dd($result_link)
 *         \Illuminate\Support\Facades\Log::info("- Code Profiler Link -", $result_link);
 *         \Illuminate\Support\Facades\Log::info("- Code Profiler Dump -", $cp->get_result_dump());
 * 
 * View all Logs:
 *          http://wlapi.sibme.com/xhprof_html/index.php
 * 
 * Remove Logs:
 *          sudo rm -fr \tmp\<file_name>.xhprof
 * Remove all Logs:
 *          sudo find /tmp -type f -name "*.xhprof" -delete
 */

class CodeProfiler {
    private $run_id;
    private $title;
    private $started = false;
    private $data = [];

    public function __construct(){
    }

    function start($title){
        $this->run_id = uniqid();
        $this->title = Str::slug($title);
        $this->started = true;
        tideways_xhprof_enable();
    } 

    function end(){
        if (!$this->started) {
            throw new \Exception("End is called before Start. Please make sure function calls are matching with the given example in ".__CLASS__." Service.", 1120);
        }
        $this->data = tideways_xhprof_disable();
        $this->save();
        return $this->get_result_link();
    } 

    private function save(){
        if (empty($this->data)) {
            throw new \Exception("Data is empty. Please make sure function calls matching with the given example in ".__CLASS__." Service.", 1122);
        }
        file_put_contents(
            sys_get_temp_dir() . "/" . $this->run_id . ".".$this->title.".xhprof",
            serialize($this->data)
        );
    }

    private function get_result_link(){
        if (!$this->started || empty($this->data)) {
            throw new \Exception("Result not found. Please make sure function calls are matching with the given example in ".__CLASS__." Service.", 1121);
        }
        return config("s3.sibme_api_url")."xhprof_html/index.php?run=".$this->run_id."&source=".$this->title;
    }

    public function get_result_dump(){
        if (!$this->started || empty($this->data)) {
            throw new \Exception("Result not found. Please make sure function calls are matching with the given example in ".__CLASS__." Service.", 1121);
        }
        return $this->data;
    }
}