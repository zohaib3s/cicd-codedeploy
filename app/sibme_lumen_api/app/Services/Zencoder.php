<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Models\Document;
use App\Models\DocumentFiles;
use App\Models\ScreenShareAwsJobs;
use \App\Models\Sites;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Services_Zencoder;
use Services_Zencoder_Exception;

class Zencoder {
    private $zencoder;
    function __construct() {
        $this->zencoder = new Services_Zencoder( env('ZENCODER_API_KEY'));
    }

    function createJob($file){
        if(empty($file))
        {
            return false;
        }
        $path_parts = pathinfo($file);
        if(!isset($path_parts["dirname"]) ||!isset($path_parts["filename"]) ||!isset($path_parts["extension"]) )
        {
            return false;
        }
        $name_modifier = "_zencoded";
        $output_file = $path_parts["dirname"] ."/". $path_parts["filename"] . $name_modifier .".". $path_parts["extension"];
        try {
            $job = [
                // "input" => "s3://zencoder-customer-ingest/uploads/2020-03-13/1081154/256846/1ad28450-6529-11ea-b3f6-f99d0523ec33.mp4",
                "input" => $file,
                "outputs" => [
                    "url" => $output_file,
                    "notifications" => [
                        "url" => config("s3.sibme_api_url")."receive_notification"
                    ]
                ]
            ];

            $response = $this->zencoder->jobs->create($job);
        } catch (Services_Zencoder_Exception $e) {
            // If were here, an error occurred
            echo($e->getMessage());
            return false;
        }
        echo "\nAll Job Attributes:\n";
        return isset($response->id) ? $response->id : false;
    }

    function processJobNotification(){
        try {
            // Catch notification
            $notification = $this->zencoder->notifications->parseIncoming();
            $job_id = $notification->job->id;
            // If you're encoding to multiple outputs and only care when all of the outputs are finished
            // you can check if the entire job is finished.
            $outputVideo = $notification->job->outputs[0];
            $id = null;
            $document_id = null;
            $document = Document::where("zencoder_output_id", $job_id)->first();
            if($document)
            {
                if($outputVideo->state == "finished") {
                    $url = stripslashes($outputVideo->url);
                    $duration = $outputVideo->duration_in_ms/1000;
                    $zencoder_path = trim(parse_url($url)['path'],"/");
                    $document->url = $zencoder_path;
                    $document->encoder_status = "complete";
                    $screenShareClips = app('App\Http\Controllers\LiveStreamController')->getClippingData($document->id);
                    if(empty($screenShareClips))
                    {
                        $document->published = 1;
                    }
                    else
                    {
                        $document->published = 0;//Dont publish until we merge screen share videos and cam videos
                        $s3Service = HelperFunctions::getS3ServiceObject();
                        $job_id = $s3Service->submitElementalClipsMergingJob($document, $screenShareClips);
                        $job_log = new ScreenShareAwsJobs();
                        $job_log->document_id = $document->id;
                        $job_log->clipping_job_id = $job_id;
                        $job_log->clipping_job_status = "submitted";
                        $job_log->site_id = $document->site_id;
                        $job_log->save();
                        app('App\Console\Commands\CraeteAudioAwsForTranscribe')->createJobForAudio($zencoder_path,$document->id);
                    }
                    $document->video_duration = $duration;
                    $document->last_edit_date = date('Y-m-d H:i:s');
                    $document->file_size = $outputVideo->file_size_in_bytes;
                    $document->save();
                    $document_id = $document->id;
                    $document_files_data = [
                        "duration" => $duration,
                        "file_size" => $outputVideo->file_size_in_bytes,
                        "transcoding_status" => 3,
                        "url" => $zencoder_path,
                        "updated_at"=>date('Y-m-d H:i:s'),
                        "debug_logs" => "Lumen::Zencoder::processJobNotification::line=109::document_id=".$document->id."::status=3::updated"
                    ];
                    DocumentFiles::where("document_id",$document->id)->update($document_files_data);
                    S3Services::execute_thumbnail_websocket($document ,1);
                    foreach($notification->job->outputs as $job){
                        $data = [
                            'response' => json_encode($job),
                            'state' => $job->state,
                            'url' => $job->url,
                            'format' => $job->format,
                            'created_at' => date("Y-m-d H:i:s")
                        ];
                        $id = DB::table('zencoder_notifications_log')->insertGetId($data);
                    }
                }
                else
                {
                    if($document->video_duration > 1 && $document->file_size > 1)
                    {
                        //Millicast file will be played because for zencoder it is a corrupt file
                        $document->published = 1;
                        $document->encoder_status = "failed";
                        $document->save();
                        DocumentFiles::where("document_id",$document->id)->update(["transcoding_status"=>3,"transcoding_status_details"=>$outputVideo->error_message, "updated_at"=>date('Y-m-d H:i:s'),"debug_logs" => "Lumen::Zencoder::processJobNotification::line=132::document_id=".$document->id."::status=3::updated"]);

                    }
                    else
                    {
                        $document->published = 0;
                        $document->encoder_status = "Error";
                        $document->save();
                        DocumentFiles::where("document_id",$document->id)->update(["transcoding_status"=>5,"transcoding_status_details"=>$outputVideo->error_message, "updated_at"=>date('Y-m-d H:i:s'),"debug_logs" => "Lumen::Zencoder::processJobNotification::line=140::document_id=".$document->id."::status=5::updated"]);
                    }
                }
            }

            return ['status'=>true, 'zencoder_notifications_log_id' => $id, "document_id"=>$document_id];
/*
            // Check output/job state
            if($notification->job->outputs[0]->state == "finished") {
                echo "w00t!\n<pre>";
                // If you're encoding to multiple outputs and only care when all of the outputs are finished
                // you can check if the entire job is finished.
                if($notification->job->state == "finished") {
                    echo "Dubble w00t!";
                }
            } elseif ($notification->job->outputs[0]->state == "cancelled") {
                echo "Cancelled!\n";
            } else {
                echo "Fail!\n";
                echo $notification->job->outputs[0]->error_message."\n";
                echo $notification->job->outputs[0]->error_link;
            }
*/
        } catch (Services_Zencoder_Exception $e) {
            // If were here, an error occurred
            echo($e->getMessage());
        }
    }

}

?>
