<?php
namespace App\Services;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\ResourceController;
use App\Models\Account;
use App\Models\AccountFolder;
use App\Models\AccountFolderMetaData;
use App\Models\DocumentFolder;
use App\Models\JobQueue;
use App\Models\Sites;
use App\Models\User;
use App\Models\UserAccount;
use \App\Models\UserActivityLog;
use App\Models\Document;
use App\Services\HelperFunctions;
use DB;

class DocumentUploader {

    public function __construct()
    {
    }
    
    function uploadDocuments($site_id, $account_folder_id, $account_id, $user_id, $video_id = '', $url_stack = '', $workspace = false, $url, $video_url, $video_file_name, $video_file_size, $video_desc, $current_user_role_id, $current_user_email,$time = '',$comment_id = '',$is_sample='0',$slot_index = 0, $filestack_handle=null, $filestack_url=null, $parent_folder_id = 0) {
/////
/*
$req =>
    $url
    $video_url
    $video_file_name
    $video_file_size
    $video_desc

$current_user_role_id
$current_user_email

*/
/////

            if ($url_stack == 1) {
                $url_stack = $url;
            }
            $previous_storage_used = \App\Models\Account::getPreviousStorageUsed($account_id, $site_id);
            $s3_dest_folder_path_only = "$account_id/$account_folder_id/" . date('Y') . "/" . date('m') . "/" . date('d');
            $url = !empty($video_url) ? $s3_dest_folder_path_only . "/" . $video_url : $s3_dest_folder_path_only;
            //added new code to extract title
            $last_dot_index = strrpos($video_file_name, ".");
            $file_title = substr($video_file_name, 0, $last_dot_index);

            
            $path_parts = pathinfo($video_file_name);
            $extension = isset($path_parts['extension']) && !empty($path_parts['extension']) ? $path_parts['extension'] : "";
            // Note : I have changed because pathinfo function removed spanish characters that's why i am changing. 
            $video_file_name = $file_title . "." . $extension;
            

            $path_parts_orig = pathinfo($video_file_name);
            $request_dump = addslashes(print_r($_POST, true));
            //$url_stack = explode('/',$url_stack );
            $data = array(
                'account_id' => $account_id,
                'doc_type' => '2',
                'url' => $url,
                'original_file_name' => $video_file_name,
                'active' => 1,
                'created_date' => date("Y-m-d H:i:s"),
                'created_by' => $user_id,
                'last_edit_date' => date("Y-m-d H:i:s"),
                'last_edit_by' => $user_id,
                'published' => '1',
                'site_id' => $site_id,
                'stack_url' => $url_stack,
                'post_rubric_per_video' => '1',
                'parent_folder_id' => $parent_folder_id
            );
            if(!empty($time))
            {
                $data['scripted_current_duration'] = $time;
            }
            else
            {
                $data['scripted_current_duration'] = '0';
            }
            $document_id =  \DB::table('documents')->insertGetId($data);
            
            if ($document_id) {
                if(!empty($parent_folder_id))
                {
                    DocumentFolder::where('id', $parent_folder_id)->update(['last_edit_date'=>date('Y-m-d H:i:s'), 'last_edit_by'=> $user_id]);
                }
                // $document_id = \DB::getPdo()->lastInsertId();
                
                if(!empty($comment_id))
                {
                    $file_attachment_data = array('comment_id' => $comment_id,
                                                  'document_id' =>   $document_id,
                                                  'site_id' => $site_id
                                                );
                    \DB::table('comment_attachments')->insert($file_attachment_data);
                }

                $folder_type = \App\Models\AccountFolder::get_folder_type($account_folder_id, $site_id);
                if ($folder_type == '1') {
                    $url = '/Huddles/view/' . $account_folder_id . '/2';
                } elseif ($folder_type == '3') {
                    $url = '/MyFiles/view/2';
                } else {
                    $url = '/videoLibrary/view/' . $account_folder_id;
                }
                if (\App\Models\AccountFolderMetaData::check_if_eval_huddle($account_folder_id, $site_id)) {
                    $url = '/Huddles/view/' . $account_folder_id . '/1/' . $video_id;
                    $user_activity_logs = array(
                        'ref_id' => $document_id,
                        'desc' => $video_file_name,
                        'url' => $url,
                        'type' => '3',
                        'site_id' => $site_id,
                        'account_folder_id' => $account_folder_id,
                        'environment_type' => 2
                    );
                } else {
                    $user_activity_logs = array(
                        'ref_id' => $document_id,
                        'desc' => $video_file_name,
                        'url' => $url,
                        'type' => '3',
                        'site_id' => $site_id,
                        // 'account_folder_id' => ($video_id != '' && $video_id != 'undefined') ? $video_id : $account_folder_id,
                        'account_folder_id' => $account_folder_id,
                        'environment_type' => 2
                    );
                }

                UserActivityLog::user_activity_logs($site_id, $user_activity_logs, $account_id, $user_id);
                if (empty($filestack_handle)) {
                    $stack_array = explode('/',$url_stack );
                    $filestack_handle = end($stack_array);
                }

                $s_url = $filestack_url;
                if(!empty($video_url))
                {
                    $s_url = $this->uploadResourcePendingProcess($document_id, $s3_dest_folder_path_only, $video_file_name, $video_url);
                    $ready_for_download = 1;
                } else {
                    //Document is uploaded to filestack server but not transfered to s3 so a cron(processFilestackPendingItems) will process it once its available on s3.
                    $filestack = new \App\Models\DocumentsFilestackData();
                    $filestack->document_id = $document_id;
                    $filestack->user_id = $user_id;
                    $filestack->suppress_success_email = 0;
                    $filestack->s3_dest_folder_path_only = $s3_dest_folder_path_only;
                    $filestack->site_id = !empty($this->site_id) ? $this->site_id : $site_id;
                    $filestack->filestack_handle = $filestack_handle;
                    $filestack->folder_type = $folder_type;
                    $filestack->save();
                    $ready_for_download = 0;
                }

/*
                $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name";
                $s3_src_folder_path = $video_url;

                if(empty(config('s3.amazon_base_url'))){
                    throw new \Exception("S3 Config File (\config\s3.php) is Not Loaded", 1);
                }
                $s3_service = new \App\Services\S3Services(
                       config('s3.amazon_base_url'), config('s3.bucket_name'), config('s3.access_key_id'), config('s3.secret_access_key')
                );

                $s_url = $s3_service->copy_from_s3($s3_src_folder_path, "uploads/" . $s3_dest_folder_path);
*/
                $s3_dest_folder_path = $s3_dest_folder_path_only . "/" .$video_file_name;
                \App\Models\Document::where('id', $document_id)->where('site_id', $site_id)
                        ->update([
                                    'zencoder_output_id' => 0,
                                    'url' => addslashes($s3_dest_folder_path) ,
                                    'file_size' => (int) $video_file_size,
                                    'site_id' => $site_id,
                                    'request_dump' =>  $request_dump 
                                ]);

                if ($previous_storage_used && !empty($previous_storage_used->storage_used)) {
                    $storage_in_used = $video_file_size + $previous_storage_used->storage_used;
                } else {
                    $storage_in_used = $video_file_size;
                }

                \App\Models\Account::where('id', $account_id)->where('site_id', $site_id)->update(['storage_used' => $storage_in_used, 'site_id' => $site_id]);
                \App\Models\JobQueue::job_queue_storage_function($site_id, $account_id, '4');

                $data = array(
                    'account_folder_id' => $account_folder_id,
                    'document_id' => $document_id,
                    'title' => $video_file_name,
                    'site_id' => $site_id,
                    'desc' => $video_desc,
                    'assessment_sample' => $is_sample,
                    'slot_index' => $slot_index,
                );
                $account_folder_document_id = \DB::table('account_folder_documents')->insertGetId($data);
//                $accountFolderDocument = \App\Models\AccountFolderDocument::insert($data);

                if (!empty($video_id) && $video_id != "undefined") {
                    // $account_folder_document_id = \DB::getPdo()->lastInsertId();
                    $data = [
                        'account_folder_document_id' => $account_folder_document_id,
                        'attach_id' => $video_id,
                        'site_id' => $site_id
                    ];
                   
                    \DB::table('account_folderdocument_attachments')->insert($data);
                    //sw-5005
                    Document::where('id',$video_id)->update(['last_edit_date'=>date("Y-m-d H:i:s"), 'last_edit_by'=>$user_id]);
                }

                $huddle = \App\Models\AccountFolder::getHuddle($site_id, $account_folder_id, $account_id);
                if(!$huddle) return response()->json([__FILE__.':@:'.__LINE__.' Huddle Not Found: '.$account_folder_id]);
                if(!empty($account_folder_id))//sw-5004
                {
                    AccountFolder::where('account_folder_id', $account_folder_id)->update(['last_edit_date'=>date("Y-m-d H:i:s"), 'last_edit_by'=>$user_id]);
                }
                $huddleUsers = \App\Models\AccountFolder::getHuddleUsers($account_folder_id, $site_id);
                if (\App\Models\AccountFolderMetaData::check_if_eval_huddle($account_folder_id, $site_id)) {
                    $documentData = array(
                        'account_folder_id' => $account_folder_id,
                        'huddle_name' => $huddle[0]['name'],
                        'video_link' => '/Huddles/view/' . $account_folder_id . '/1/' . $video_id.'/7',
                        'participating_users' => $huddleUsers,
                        'site_id' => $site_id
                    );
                } else {
                    $documentData = array(
                        'account_folder_id' => $account_folder_id,
                        'huddle_name' => $huddle[0]['name'],
                        'video_link' => '/Huddles/view/' . $account_folder_id . '/2/8',
                        'participating_users' => $huddleUsers,
                        'site_id' => $site_id
                    );
                }

                $all_participants = array();
                if (\App\Models\AccountFolderMetaData::check_if_eval_huddle($account_folder_id, $site_id) == 1) {
                    if ($huddleUsers) {
                        foreach ($huddleUsers as $usre) {
                            if ($usre['role_id'] == 210) {
                                $all_participants[] = $usre['user_id'];
                            }
                        }
                    }
                }

                $video_details = \App\Models\Document::get_video_details($video_id, $site_id);
                if($video_details)
                {
                    $vide_created_by = $video_details->created_by;
                }
                else
                {
                    $vide_created_by = null;
                }

                $is_email_notification_allowed = HelperFunctions::is_email_notification_allowed($account_id);

                if ($huddleUsers && $is_email_notification_allowed) {
                    // Email Queued Here
                    QueuesManager::sendToEmailQueue(__CLASS__, "send_emails_to_queue", $huddleUsers, $user_id, $account_id, $account_folder_id, $site_id, $all_participants, $vide_created_by, $documentData);
                }
            } else {
                $document_id = 0;
                $s_url = '';
            }
            $resourceCtrl = new ResourceController();
            $s3_path = $resourceCtrl->generateResourceThumbnail(2, $filestack_handle, $extension, "uploads/" .$s3_dest_folder_path_only);
            if($s3_path)
            {
                Document::where('id',$document_id)->update(['s3_thumbnail_url'=> "uploads/" .$s3_dest_folder_path_only.'/'.$filestack_handle.'.jpg']);
            }
            $resp = Document::get_single_video_data($site_id, $document_id, $folder_type);
            $data_ws = null;
            if($resp)
            {
                $data_ws = app('App\Http\Controllers\WorkSpaceController')->conversion_to_thmubs($resp[0]);
            }
            $allowed_participants = [];
            $h_type = AccountFolderMetaData::where('account_folder_id', $account_folder_id)->where('meta_data_name', 'folder_type')->first();
            $htype = isset($h_type->meta_data_value) ? $h_type->meta_data_value : "1";
            if($current_user_role_id)
            {
                $allowed_participants = HelperFunctions::get_security_permissions($account_folder_id);
                if($htype == 3 && $current_user_role_id == "210")
                {
                    foreach ($allowed_participants as $key => $allowed_participant)
                    {
                        if($allowed_participant["user_id"] != $user_id && $allowed_participant["role_id"] == "210")
                        {
                            unset($allowed_participants[$key]);
                        }
                    }
                }
            }
            if($workspace)
            {
                $channel = 'workspace-'.$account_id."-".$user_id;
            }
            else
            {
                $channel = 'huddle-details-'.$account_folder_id;
            }
            $reference_id = $document_id;
            $reference_id = $video_id;
            $res = Document::get_single_video_data($site_id, $reference_id, $folder_type, 1, $account_folder_id, $user_id);
            if(!empty($video_id))
            {
                
                $res['ready_for_download'] = $ready_for_download;
                $event = [
                    'channel' => ($folder_type == 2) ?'library-'.$account_id : $channel,
                    'event' => "resource_renamed",
                    'data' => $res,
                    'video_file_name' => $res["title"],
                    'is_dummy' => 1
                ];
                HelperFunctions::broadcastEvent($event,"broadcast_event",false);//not sending push to prevent multiple refresh
            }
            $data_ws["comment_id"] = $comment_id;
            $data_ws["ready_for_download"] = $ready_for_download;
            if($data_ws)
            {
                $data_ws[0]['updated_by'] = $user_id;
            }
            //$data_ws = Document::get_single_video_data($site_id, $document_id, 1, 1, $account_folder_id, $user_id); commenting because this was causing issue in document upload socket by Saad Zia
            $is_comment_attachment = false;
            if(!empty($comment_id)){
                $is_comment_attachment = true;
            }
            if($data_ws['doc_type'] == 2){
                $data_ws['title'] = pathinfo($data_ws['title'], PATHINFO_FILENAME);
            }           
            $res = array(
                'is_comment_resource'=>$is_comment_attachment,
                'document_id' => $document_id,
                'reference_id' => $reference_id,
                'data' => $data_ws,
                'url' => $s_url,
                'huddle_id' => $account_folder_id,
                'channel' => $channel,
                'event' => 'resource_added',
                'allowed_participants' => $allowed_participants,
                'assessment_sample' => $is_sample
            );

            $is_push_notification_allowed = HelperFunctions::is_push_notification_allowed($account_id);
            if(!$is_push_notification_allowed){
                HelperFunctions::broadcastEvent($res, 'broadcast_event', false);
            } else {
                HelperFunctions::broadcastEvent($res);
            }

//        return response()->json($res);
        return $res;
    }

    function uploadResourcePendingProcess($document_id, $s3_dest_folder_path_only, $video_file_name, $video_url){
        $s3_dest_folder_path = $s3_dest_folder_path_only. "/" .$video_file_name;
        $s3_src_folder_path = $video_url;

        if(empty(config('s3.amazon_base_url'))){
            throw new \Exception("S3 Config File (\config\s3.php) is Not Loaded", 1);
        }
        $s3_service = new \App\Services\S3Services(
               config('s3.amazon_base_url'), config('s3.bucket_name'), config('s3.access_key_id'), config('s3.secret_access_key')
        );

        $s_url = $s3_service->copy_from_s3($s3_src_folder_path, "uploads/" . $s3_dest_folder_path);

        \App\Models\Document::where('id', $document_id)->update(['url' => addslashes($s3_dest_folder_path)]);

        return $s_url;
    }


    function cake_send_emails_to_queue($user_id, $account_id, $account_folder_id, $video_id, $site_id){

        $huddle = \App\Models\AccountFolder::getHuddle($site_id, $account_folder_id, $account_id);
        if(!$huddle) return response()->json([__FILE__.':@:'.__LINE__.' Huddle Not Found: '.$account_folder_id]);
        // if(!empty($account_folder_id))//sw-5004
        // {
        //     AccountFolder::where('account_folder_id', $account_folder_id)->update(['last_edit_date'=>date("Y-m-d H:i:s"), 'last_edit_by'=>$user_id]);
        // }
        $huddleUsers = \App\Models\AccountFolder::getHuddleUsers($account_folder_id, $site_id);
        if (\App\Models\AccountFolderMetaData::check_if_eval_huddle($account_folder_id, $site_id)) {
            $documentData = array(
                'account_folder_id' => $account_folder_id,
                'huddle_name' => $huddle[0]['name'],
                'video_link' => '/Huddles/view/' . $account_folder_id . '/1/' . $video_id.'/7',
                'participating_users' => $huddleUsers,
                'site_id' => $site_id
            );
        } else {
            $documentData = array(
                'account_folder_id' => $account_folder_id,
                'huddle_name' => $huddle[0]['name'],
                'video_link' => '/Huddles/view/' . $account_folder_id . '/2/8',
                'participating_users' => $huddleUsers,
                'site_id' => $site_id
            );
        }

        $all_participants = array();
        if (\App\Models\AccountFolderMetaData::check_if_eval_huddle($account_folder_id, $site_id) == 1) {
            if ($huddleUsers) {
                foreach ($huddleUsers as $usre) {
                    if ($usre['role_id'] == 210) {
                        $all_participants[] = $usre['user_id'];
                    }
                }
            }
        }

        $video_details = \App\Models\Document::get_video_details($video_id, $site_id);
        if($video_details)
        {
            $vide_created_by = $video_details->created_by;
        }
        else
        {
            $vide_created_by = null;
        }
        //return [$huddleUsers, $user_id, $account_id, $account_folder_id, $site_id, $all_participants, $vide_created_by, $documentData];
        $is_email_notification_allowed = HelperFunctions::is_email_notification_allowed($account_id);
        if ($huddleUsers && $is_email_notification_allowed) {
            QueuesManager::sendToEmailQueue(__CLASS__, "send_emails_to_queue", $huddleUsers, $user_id, $account_id, $account_folder_id, $site_id, $all_participants, $vide_created_by, $documentData);
        }

        
    }


    function send_emails_to_queue($huddleUsers, $user_id, $account_id, $account_folder_id, $site_id, $all_participants, $vide_created_by, $documentData){
        foreach ($huddleUsers as $row) {
            if($row['is_active'] ==0){
                continue;
            }
            if ($row['id'] == $user_id) {
                continue;
            }
            if (\App\Models\AccountFolderMetaData::check_if_eval_huddle($account_folder_id, $site_id) > 0) {
                if (\App\Models\AccountFolderUser::check_if_evaluated_participant($account_folder_id, $user_id, $site_id)) {
                    if (isset($all_participants) && is_array($all_participants) && in_array($row['id'], $all_participants)) {
                        continue;
                    }
                } else {

                    if ($vide_created_by != $row['id']) {
                        continue;
                    }
                }
                $documentData['huddle_type'] = 3;
            } else {
                $documentData['huddle_type'] = '';
            }
            $documentData['email'] = $row['email'];
            $documentData['user_id'] = $row['id'];
            if ($documentData['huddle_type']!='3' && \App\Models\EmailUnsubscribers::check_subscription($row['id'], '1', $account_id, $site_id)) {
                $this->sendDocumentEmail($documentData,$user_id,$account_id,$site_id);
            }
        }
    }

    function sendDocumentEmail($data,$user_id,$account_id,$site_id) {
        if (!($data['email']))
            return FALSE;



        $current_user = User::find($user_id);
        $current_account = Account::where("id",$account_id)->where("site_id",$site_id)->first();

        $email_delivery = 'smtp';
        $email_from = $current_user->first_name . " " . $current_user->last_name . '  ' . $current_account->company_name . '<' . Sites::get_site_settings('static_emails',$site_id)['noreply'] . '>';
        $email_to = $data['email'];

        $email_template = 'default';
        $email_sendAs = 'html';
        //$view = new View($this, true);
        $params = array(
            'data' => $data
        );
//$html = $view->element('emails/' . $this->site_id . '/document_uploaded', $params);
        $lang = $current_user->lang;
        if ($lang == 'en') {
            $email_subject = Sites::get_site_settings('email_subject', $site_id) . " - Resource Added";
        } else {
            $email_subject = Sites::get_site_settings('email_subject', $site_id) . " - Recurso Añadido";
        }
        $sibme_base_url = config('s3.sibme_base_url');
        $unsubscribe = $sibme_base_url . 'subscription/unsubscirbe_now/' . $data['user_id'] . '/1';
        $key = "document_uploaded_" . $site_id . "_" . $lang;
        $result = SendGridEmailManager::get_send_grid_contents($key);
        if(!empty($result->versions) && !empty($result->versions[0]->html_content)){
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $current_user->first_name . "  " . $current_user->last_name, $html);
            $html = str_replace('{huddle_name}', $data['huddle_name'], $html);
            $html = str_replace('{redirect}', $sibme_base_url . $data['video_link'], $html);
            $html = str_replace('{unsubscribe}', $unsubscribe, $html);
            $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
            $auditEmail = array(
                'account_id' => $data['account_folder_id'],
                'email_from' => $email_from,
                'email_to' => $data['email'],
                'email_subject' => $email_subject,
                'email_body' => $html,
                'is_html' => true,
                'sent_date' => date("Y-m-d H:i:s")
            );
    
            if (!empty($data['email'])) {
                DB::table('audit_emails')->insert($auditEmail);
                $result = false;
                try {
                    $use_job_queue = config('s3.use_job_queue');
                    if ($use_job_queue) {
                        $result = JobQueue::add_job_queue($site_id,1, $data['email'], $email_subject, $html, true);
                    } else {
                        /*
                        $emailData = [
                            'from' => Sites::get_site_settings('static_emails', $site_id)['noreply'],
                            'from_name' => Sites::get_site_settings('site_title', $site_id),
                            'to' => $data['email'],
                            'subject' => $email_subject,
                            'template' => $html
                        ];
                        Email::sendCustomEmail($emailData);
                        */
                    }
                } catch (\Exception $ex) {
    
                }
                return $result;
            }
        }      
        

        return FALSE;
    }

}