<?php

namespace App\Services\Cache;

use Illuminate\Support\Facades\DB;

use App\Models\AnalyticsFilter;
use App\Models\UserAccount;
use App\Models\User;

class AnalyticsCache {

    public $users_fields;
    public $users_accounts_fields;
    public $accounts_fields;
    public $roles_fields;
    public $global_payload;
    
    function __construct() {
/**
 * 1. loop through all accounts.
 * 2. get users from users_accounts table where role_id in (100, 110, 115) of current selected account.
 * 3. get current_user_accounts object from cake Api/get_user()
 * 4. Convert get_user() to lumen.
 * 5. Create Cache with default filters for the current user.
 * 6. Create Cache with all saved filters of the current user.
 */

        $this->users_fields = \Schema::getColumnListing("users");
        $this->users_accounts_fields = \Schema::getColumnListing("users_accounts");
        $this->accounts_fields = \Schema::getColumnListing("accounts");
        $this->roles_fields = \Schema::getColumnListing("roles");

        $this->global_payload = [
            'duration'=>"-1",
            'start_date'=>date("m/d/Y", strtotime("-1 year")),
            'end_date'=>date("m/d/Y"),
            'filter_type'=>'custom',
            'subAccount'=>'',
            'forceRefresh'=>'1'
        ];

    }

    public function create_user_summary_cache($site_id, $payload) {
        $additional_payload = [
            'page'=>'',
            'rows'=>'',
            // 'page'=>'1',
            // 'rows'=>'10',
            'sidx'=>'account_name',
            'sord'=>'asc',
            'search_text'=>'',
        ];
        $payload = array_merge($payload, $additional_payload);
        $request = new \Illuminate\Http\Request($payload);
        $request->headers->set('site_id', $site_id);
        $api = new \App\Http\Controllers\ApiController($request);
        $api->accountUsersAjax($request);
    }

    public function create_account_overview_cache($site_id, $payload) {
        $request = new \Illuminate\Http\Request($payload);
        $request->headers->set('site_id', $site_id);
        $api = new \App\Http\Controllers\ApiController($request);
        $api->account_overview($request);
    }

    public function process_cache(){
        // Get All Account Owners with Account IDs
        // Create Cache of All these users with account_id. analytics_account_overview_<account id>
        // This will be used for Account Owners/Super Admins/Admins(with full analytics access) - Do in API Controller
        // Get All the Admins (with limited analytics access) of the current Account.
        // Create Cache of all those admins with key =  analytics_account_overview_<account id>_<admin_id>.

        // Execution fails on big accounts like 826. It gives Memory Exhausted error. That's why we set memory_limit to -1 temporarily.
        ini_set("memory_limit","-1");

        // Get all owners of active accounts:
        $users = UserAccount::get_all_user_ids([100]);

        foreach ($users as $user) {
            // if(!in_array($user->account_id, ['2936'])) continue;
            // if(!in_array($user->account_id, ['826'])) continue; // Big Account

            $start = \Carbon\Carbon::now();
            echo "\nAccount: ".$user->account_id." User: ".$user->user_id. " ";
            
            // Create Cache for Owner. So, other Super Admins and Privileged Admins can share the same analytics
            $this->createCacheForUser($this->global_payload, $user->site_id, $user->user_id);
            // Create Cache for Non Privileged Admins in the same account.
            $this->createCacheForNonPrivilegedAdmins($user->account_id, $this->global_payload);
            $end = \Carbon\Carbon::now();
            echo "\nExecution Time for ".$user->account_id." : " . $start->diffInHours($end) . ':' . $start->diff($end)->format('%I:%S') . "\n";            
        }

    }

    public function process_filters_cache(){
        // Execution fails on big accounts like 826. It gives Memory Exhausted error. That's why we set memory_limit to -1 temporarily.
        ini_set("memory_limit","-1");

        $filters = AnalyticsFilter::all();
        foreach($filters as $filter){
            // if(!in_array($filter->current_account_id, ['2936'])) continue;
            // if(!in_array($filter->current_account_id, ['826'])) continue; // Big Account

            // $this->createCacheForSavedFilters($filter->site_id, $filter->current_account_id, $filter->current_user_id, $this->global_payload);
            $filter_payload = $this->preparePayloadForFilter($filter, $this->global_payload);
        
            $this->createCacheForUser($filter_payload, $filter->site_id, $filter->current_user_id, $filter->current_account_id);
            // $this->create_user_summary_cache($filter->site_id, $filter_payload);
            // $this->create_account_overview_cache($filter->site_id, $filter_payload);
        }
    }

    public function formulate_user_accounts_object($user_accounts){
        if(empty($user_accounts)) return false;
        unset($this->users_accounts_fields['id']);
        $final = [];
        $User = [];
        $users_accounts = [];
        $roles = [];
        $accounts = [];
        if(empty($user_accounts)){
            return false;
        } 
        $arr_account = $user_accounts[0];
        foreach($arr_account as $key=>$val){
            if(in_array($key, $this->users_fields)){
                $User[$key] = $val;
            }elseif(in_array($key, $this->users_accounts_fields)){
                $users_accounts[$key] = $val;
            }elseif(in_array($key, $this->accounts_fields)){
                $accounts[$key] = $val;
            }elseif(in_array($key, $this->roles_fields)){
                $roles[$key] = $val;
            }
        }
        $accounts['account_id'] = $arr_account['account_id'];
        $roles['role_id'] = $arr_account['role_id'];
        $User['id'] = $arr_account['user_id'];

        $final['User'] = $User;
        $final['users_accounts'] = $users_accounts;
        $final['roles'] = $roles;
        $final['accounts'] = $accounts;

        return $final;
    }

    public function create_key($salt, $payload){
        if(empty($payload)){
            return false;
        } 
        $payload = !is_array($payload) ? json_decode($payload) : $payload;
        if(!isset($payload['user_current_account']) || empty($payload['user_current_account'])){
            return false;
        }
        $required_keys = ['account_id','end_date','start_date','subAccount','user_id'];
        if($payload['subAccount']=='-1'){
            $payload['subAccount'] = '';
        }

        $payload['user_id'] = (string) $payload['user_current_account']['User']['id'];
        $payload['account_id'] = (string) $payload['user_current_account']['users_accounts']['account_id'];
        $role_id = (string) $payload['user_current_account']['roles']['role_id'];

        if(isset($payload['filter_id']) && !empty($payload['filter_id']) && $payload['filter_id']>0){
            // 1. For Load Filters get a Param from frontend to generate appropriate filter key.
            $key = $payload['account_id'] . "_" . $payload['user_id'] . "_" . $payload['filter_id'];
        } else {
            // 2. If user role = 100 or 110 or 115 with view_analytics permission = 1 then use default key analytics_account_overview_<account_id>
            if(in_array($role_id, ['100','110']) ){
                $key = $payload['account_id'];
            }elseif($role_id=='115'){
                // Get permission_view_analytics settings of user.
                $permission_view_analytics = UserAccount::where("account_id", $payload['account_id'])->where("user_id", $payload['user_id'])->pluck('permission_view_analytics');
                if(!empty($permission_view_analytics) && $permission_view_analytics=='1'){
                    $key = $payload['account_id'];
                } else {
                    // 3. If user role = 115 and view_analytics permission = 0 then use key analytics_account_overview_<account_id>_<user_id>
                    $key = $payload['account_id'] . "_" . $payload['user_id'];
                }
            }
        }
        return !isset($key) ? false : $salt . $key;

        /*
        // We don't need following. Instead just use keys from filter table (for filters cache) or use default params for default cache.

        if(isset($payload['search_text']) && !empty($payload['search_text'])){
            $required_keys[] = 'search_text';
        }
        if(isset($payload['page']) && !empty($payload['page'])){
            $required_keys[] = 'page';
        }
        if( isset($payload['standards']) && !empty($payload['standards']) ){
            $required_keys[] = 'standards';
        }
        if( isset($payload['framework_id']) && !empty($payload['framework_id']) ){
            $required_keys[] = 'framework_id';
        }

        $cached_vals = [];
        foreach ($required_keys as $key) {
            if(isset($payload[$key])){
                $cached_vals [$key] = (string) $payload[$key];
            }
        }
        
        /**
         * Create Keys for Following:
         * 1. default filter
         * 2. saved filters
         * 3. search/paging
         */

        // ksort($cached_vals);
        // return $salt . serialize($cached_vals);
    }

    public function cachedResult($result,$cache_key){
        $result = json_decode($result);
        $result->is_cached = 1;
        $result->cached_datetime = $this->getCreation($cache_key);
        return json_encode($result);
    }

    public function getCreation($cache_key){
        $ttl = app('redis')->ttl($cache_key); //current ttl
        $diff = config('core.cache_expire_seconds') - $ttl; //difference is the time passed after key was created
        $creation = time() - $diff; //this is creation time in seconds since unix epoch
        return date("m/d/Y H:i:s",$creation);
    }

    public function createCacheForSavedFilters($filter, $payload){
        /*
        $savedFilters = AnalyticsFilter::where("current_user_id", $user_id)
                                        ->where('current_account_id', $account_id)
                                        ->where('site_id', $site_id)
                                        ->get();
        if(!$savedFilters) return false;
        foreach ($savedFilters as $filter) {
            $filter_payload = $this->preparePayloadForFilter($filter, $payload);
            
            $this->create_user_summary_cache($site_id, $filter_payload);
            $this->create_account_overview_cache($site_id, $filter_payload);
            unset($filter_payload);
        }
        */
    }
    
    public function preparePayloadForFilter($filter, $payload){
        switch ($filter->search_preference) {
            case 'date':
                $payload['start_date'] = $filter->start_date;
                $payload['end_date'] = $filter->end_date;
                break;
            
            case 'month':
                $months = $filter->months;
                $payload['start_date'] = date("m/d/Y",strtotime("-".$months." Months")); // eg.) -3 Months
                $payload['end_date'] = date("m/d/Y");
                break;
        }
        $payload['subAccount'] = $filter->subAccount=='-1' ? '' : $filter->subAccount;
        $payload['filter_id'] = $filter->id;
        return $payload;
    }

    public function createCacheForUser($payload, $site_id, $user_id, $account_id=null){
        $user_accounts = User::get_user_accounts($user_id, $site_id, $account_id);
        $user_current_account = $this->formulate_user_accounts_object($user_accounts);
        // $payload['user_current_account'] = json_encode($user_current_account);
        $payload['user_current_account'] = $user_current_account;
        // $this->create_cache(json_encode($payload));
        $this->create_user_summary_cache($site_id, $payload);
        $this->create_account_overview_cache($site_id, $payload);
    }

    public function createCacheForNonPrivilegedAdmins($account_id, $payload){
        // Get all Analytics Non Privileged Admins of active accounts:
        $users = UserAccount::get_all_user_ids([115], $account_id, true);
        foreach ($users as $user) {
            $this->createCacheForUser($payload, $user->site_id, $user->user_id);
        }
    }

}

?>
