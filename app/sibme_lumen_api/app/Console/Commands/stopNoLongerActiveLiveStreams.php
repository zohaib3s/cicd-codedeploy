<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Http\Controllers\VideoController;
use App\Models\Document;
use App\Models\LiveStreamStatus;
use App\Services\HelperFunctions;
use Exception;
use Illuminate\Console\Command;

use App\Services\S3Services;
use Illuminate\Support\Facades\Log;


/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class stopNoLongerActiveLiveStreams extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:inactive_live_streams";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will automatically stop those live streams that are not active. As per this API updateLiveStreamStatus";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
			$inactive_live_streams = LiveStreamStatus::join("documents", "documents.id", "live_stream_status.document_id")->join("account_folder_documents", "documents.id", "account_folder_documents.document_id")->select("account_folder_documents.*","documents.*")->where("doc_type", 4)->where("last_live_at", "<=", date('Y-m-d H:i:s', strtotime('-1 minutes')))->get();
			foreach ($inactive_live_streams as $live_stream)
            {
                Log::info("Found InActive Live Stream now disabling it.");
                $update_array = [
                    "doc_type" => 1,
                    "video_is_saved" => 0,
                    "published" => 1
                ];
                Document::where("id", $live_stream->document_id)->update($update_array);
                $document = Document::get_single_video_data($live_stream->site_id, $live_stream->document_id, 1, 1, $live_stream->account_folder_id, $live_stream->created_by);
                $channel_data = array(
                    'item_id' => $live_stream->document_id,
                    'video_id' => $live_stream->document_id,
                    'data' => $document,
                    'channel' => 'huddle-details-' . $live_stream->account_folder_id,
                    'huddle_id' => $live_stream->account_folder_id,
                    'notification_type' => '4',
                    'event' => "resource_renamed",
                    'is_dummy' => "1",
                    'live_video_event' => "1",
                );
                HelperFunctions::broadcastEvent($channel_data);
            }
            \Log::info("All jobs are processed.");
            /**/
        } catch (Exception $e) {
            $this->error("An error occurred");
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }
}