<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;

use App\Services\HmhLicense;

use Illuminate\Support\Facades\Log;

/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class processHmhOrderEmails extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:hmh_order_emails";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will automatically process and send HMH order emails as described in SW-4305";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
			HmhLicense::send_new_order_emails();
            Log::info("All HMH order emails are sent.");
            /**/
        } catch (Exception $e) {
            Log::info("An error occurred in ".__FILE__);
            Log::info($e->getMessage());
        }
    }
}