<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Models\Document;
use App\Models\DocumentFiles;
use App\Models\ScreenShareAwsJobs;
use App\Services\HelperFunctions;
use Exception;
use Illuminate\Console\Command;

use App\Services\S3Services;
use Illuminate\Support\Facades\Log;


/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class processScreenMergingJobs extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:screen_merging_final";
    protected $s3_service = null;
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will check status of Final merging job for Audio and cam, screens clipped video submitted in this cron -> ProcessScreenShareFiles";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->s3_service = HelperFunctions::getS3ServiceObject();
            $documents = $this->getDocuments();
            if (!empty($documents))
            {
                foreach ($documents as $document)
                {
                    $clip_job = $this->s3_service->getElementalJobDetail($document->audio_merge_job_id);
                    Log::info("Live Audio Merging Job :\n".json_encode($clip_job));
                    $status = $clip_job['status'];
                    if ($status == 'complete')
                    {
                        $this->updateDocument($document);
                    }
                    else if($status == 'error')
                    {
                        print_r($clip_job);
                        Document::where("id", $document->document_id)->update(["published" => 0, "encoder_status" => "Error", "last_edit_date"=>date('Y-m-d H:i:s')]);
                        DocumentFiles::where("document_id",$document->document_id)->update(["transcoding_status"=>5,"transcoding_status_details"=>"Audio merging job failed","updated_at"=>date('Y-m-d H:i:s'),'debug_logs'=>"Lumen::processScreenMergingJobs::handle::line=71::document_id=".$document->id."::status=5::updated"]);
                        ScreenShareAwsJobs::where("document_id", $document->document_id)->update(['audio_merge_job_status'=>'error']);
                        S3Services::execute_thumbnail_websocket($document ,1);
                    }
                }
            }
        } catch (Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }

    public function getDocuments() {
        try {
            //Only get those documents that have Audio file available
            $documents = Document::join('screen_share_aws_jobs as d_s', 'documents.id', '=', 'd_s.document_id')
                ->select("documents.*","d_s.*", "documents.id as id")
                ->where("doc_type", 1)
                ->where("published", 0)
                ->where("encoder_status", "complete")
                ->where("d_s.audio_merge_job_status", "submitted")
                ->whereNotNull("d_s.audio_merge_job_id")
                ->get();
            return $documents;
        } catch (Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }

    public function updateDocument($document)
    {
        DocumentFiles::where("document_id", $document->document_id)->update([ "url" => $document->audio_merged_video_path, "updated_at"=>date('Y-m-d H:i:s'),'debug_logs'=>"Lumen::processScreenMergingJobs::updateDocument::line=103::document_id=".$document->id."::status=5::updated"]);
        Document::where("id", $document->document_id)->update(["published" => 1, "last_edit_date"=>date('Y-m-d H:i:s')]);
        ScreenShareAwsJobs::where("document_id", $document->document_id)->update(['audio_merge_job_status'=>'complete']);
    }
}