<?php

/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\AccountFolder;
use App\Models\CirqliveConfigs;
use App\Models\CirqliveCronLogs;
use Illuminate\Support\Facades\File;
use Aws\S3\S3Client;
use \Firebase\JWT\JWT;
use GuzzleHttp\Client;
use Exception;

/**
 * Class CirqliveRecordings
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class CirqliveRecordings extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "download:cirq-recordings";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will download the all recordings from zoom,webex etc using cirqlive api's and upload to aws bucket";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $cirqConfigs = $this->getCirqLiveData();
        $cirqCronLogs = $this->getCirqCronLogs();

        if (!empty($cirqConfigs)) {
            if (!empty($cirqCronLogs)) {
                $cirqCronLogs = $cirqCronLogs->toArray();
                $from_string = $cirqCronLogs['to_datetime'];
                $from = strtotime($from_string);
                $to_string = date('Y-m-d H:i:s', strtotime('+30 minutes', $from));
                $to = strtotime($to_string);
                $log_id = $this->saveCronLogs($from_string, $to_string);
            } else {
                $from = strtotime('-30 minutes');
                $from_string = date('Y-m-d H:i:s', $from);
                $to_string = date('Y-m-d H:i:s', strtotime('+30 minutes', $from));
                $to = strtotime($to_string);
                $log_id = $this->saveCronLogs($from_string, $to_string);
            }
            // $yesterday = strtotime("yesterday midnight");
            foreach ($cirqConfigs as $cirqC) {
                $account_id = $cirqC['account_id'];
                $huddle_data = AccountFolder::
                                join('account_folders_meta_data as meta_data', 'meta_data.account_folder_id', '=', 'AccountFolder.account_folder_id')
                                ->where("meta_data.meta_data_name", "allow_download_recordings")
                                ->where("meta_data.meta_data_value", "1")
                                ->where("AccountFolder.account_id", $account_id)
                                ->get()->toArray();
                if (!empty($huddle_data)) {
                    foreach ($huddle_data as $huddle) {
                        $course_url = $cirqC['api_root_url'] . 'from_lti_id_context?meetsIdConnection=' . $cirqC['lti_zoom_key'] . '&ltiIdContext=' . $huddle['account_folder_id'];
                        $course_response = app('App\Http\Controllers\CirQliveController')->request_signature($cirqC['auth_key'], $cirqC['auth_secret'], $course_url);
                        if ($course_response['code'] == 200) {
                            $course_data = json_decode($course_response['data']);
                            $list_rec_url = $cirqC['api_root_url'] . 'list_conference_recordings?meetsIdCourses=' . $course_data->meetsIdCourse . '&timeAddedAfter=' . $from . '&timeAddedBefore=' . $to;
                            $list_recording_res = app('App\Http\Controllers\CirQliveController')->request_signature($cirqC['auth_key'], $cirqC['auth_secret'], $list_rec_url);
                            if ($list_recording_res['code'] == 200) {
                                $recording_data = json_decode($list_recording_res['data']);
                                foreach ($recording_data->conferenceRecordings as $rec_data) {


                                    if ($rec_data->mimeType == 'video/mp4') {
//                                    if ($rec_data->mimeType == 'wwwserver/redirection') {
                                        $rec_url = $cirqC['api_root_url'] . 'download_conference_recording?meetsIdConferenceRecording=' . $rec_data->meetsIdConferenceRecording;
                                        $rec_res = app('App\Http\Controllers\CirQliveController')->request_signature($cirqC['auth_key'], $cirqC['auth_secret'], $rec_url, null, 'get_header');
                                        if ($rec_res['code'] == 303) {
                                            /* Get host id from who created event */
                                            $host_id = null;
                                            $videoDate = null;
                                            foreach ($recording_data->conferencingEvents as $eventHost) {
                                                if ($rec_data->meetsIdConferencingEvent == $eventHost->meetsIdConferencingEvent) {
                                                    $host_id = $eventHost->meetsIdUser_host;
                                                    $videoDate = $eventHost->timeBegin;
                                                    $videoDate = date('d-M-Y H:i', $videoDate);
                                                    $service = $eventHost->conferencingService;
                                                    break;
                                                }
                                            }
                                            /* Get host(user) id  of our database */
                                            $user_id = null;
                                            foreach ($recording_data->users as $users) {
                                                if ($host_id == $users->meetsIdUser) {
                                                    $user_id = $users->ltiIdUser;
                                                    break;
                                                }
                                            }

                                            $user_obj = $this->getUserObj($user_id, $account_id); //get user object from cake
                                            if (!empty($user_obj) && $user_obj->status == true) {
//                                                preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $rec_res['data'], $match);
                                                 preg_match_all('@((https?://)?([-\\w]+\\.[-\\w\\.]+)+\\w(:\\d+)?(/([-\\w/_\\.]*(\\?\\S+)?)?)*)@', $rec_res['data'], $match);
                                              if (filter_var($match[0][0], FILTER_VALIDATE_URL)) {
//                                                    $videoDate = $this->getRecordingSDateTime($match[0][0]);
                                                    $now = $service . '-' . strtotime("now");
//                                                    $title = !empty($videoDate) ? 'Zoom-' . $videoDate : $now;
                                                    $title = !empty($videoDate) ? $service . '-' . $videoDate : $now;
                                                    $fileName = $now . '.' . substr($rec_data->mimeType, strrpos($rec_data->mimeType, '/') + 1);
                                                    // $contents = file_get_contents($match[0][0]);
                                                    // file_put_contents(public_path($fileName), $contents);
//                                                    $this->downloadFile($match[0][0], public_path($fileName));
                                                    $recording_url = $match[0][0];
                                                    if ($service == "webexmeetingcenter") {
                                                        $webexUrl = $this->getWebexUrl($match[0][0]);
                                                        if (filter_var($webexUrl, FILTER_VALIDATE_URL)) {
                                                            $recording_url = $webexUrl;
                                                        } else {
                                                            continue;
                                                        }
                                                    }
                                                    else if($service == "zoom" && !empty($cirqC['zoom_oauth_key']) && !empty($cirqC['zoom_oauth_secret'])){
                                                        $JWT = $this->CreateJwtToken($cirqC['zoom_oauth_key'],$cirqC['zoom_oauth_secret']);
                                                        $recording_url= $recording_url.'?access_token='.$JWT;
                                                    } 

                                                    $this->saveRecordingToServer($recording_url,$fileName);

                                                    $file_size = filesize(public_path($fileName));

                                                    $s3client = new S3Client([
                                                        'region' => 'us-east-1',
                                                        'version' => 'latest',
                                                        'credentials' => array(
                                                            'key' => config('s3.access_key_id'),
                                                            'secret' => config('s3.secret_access_key'),
                                                        )
                                                    ]);
                                        
                                                    $path = $s3client->putObject(array(
                                                        'Bucket' => config('s3.bucket_name'),
                                                        'Key' => 'tempupload/' . $account_id . '/' . date('Y') . '/' . date('m') . '/' . date('d') . '/' . $fileName,
                                                        'SourceFile' => public_path($fileName)
                                                    ));
                                                    if ($path && isset($path['ObjectURL'])) {
                                                        $url = parse_url($path['ObjectURL'], PHP_URL_PATH);
                                                        $video_url = ltrim($url, '/');

                                                        $myRequest = new \Illuminate\Http\Request();
                                                        $myRequest->setMethod('POST');
                                                        $myRequest->request->add(['account_id' => $account_id, 'user_id' => $user_id, 'video_file_name' => $fileName, 'video_title' => $title, 'video_desc' => 'Recording from cirq', 'video_url' => $video_url, 'video_file_size' => $file_size, 'account_folder_id' => $huddle['account_folder_id'],'direct_publish'=>1, 'user_current_account' => json_encode($user_obj->result[0])]);
                                                        $myRequest->headers->set('site_id', $cirqC['site_id']);
                                                        $upload_status = app('App\Http\Controllers\HuddleController')->uploadVideos($myRequest);
                                                        if (File::exists(public_path($fileName))) {
                                                            unlink(public_path($fileName));
                                                        }
                                                        /* Increment in total_recordings column of logs for total uploaded */
                                                        try {
                                                            CirqliveCronLogs::find($log_id)->increment('uploaded_recordings');
//                                                ->update(['uploaded_recordings' => 1]);
                                                        } catch (\Illuminate\Database\QueryException $e) {
                                                            echo $e->getMessage();
                                                            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
                                                            return;
                                                        }
                                                    } else {
                                                        echo "error in uploading temp folder s3";
                                                    }
                                                }
                                            }
                                        } else {
                                            echo "not 303: please modify functionality now for this status";
                                            print_r($rec_res);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        \Log::info("End processing Cirqlive Recordings");
    }

    /* Get All Accounts CirQlive Configuration */

    public function getCirqLiveData() {
        try {
            $result = CirqliveConfigs::where('status', '1')->get()->toArray();
            return $result;
        } catch (Exception $e) {
            \Log::error("An error occurred while getting cirq_configs");
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }

    public function getUserObj($user_id, $account_id) {
        $ch = curl_init();
        $event_url = config('s3.sibme_base_url') . 'Api/get_user';
        curl_setopt($ch, CURLOPT_URL, $event_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['user_id' => $user_id, 'account_id' => $account_id]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        return json_decode($server_output);
    }

    /*  public function getRecordingSDateTime($url) {
      try {

      $file = fopen($url, 'r');
      $meta_data = stream_get_meta_data($file);
      $modified_Date = substr($meta_data['wrapper_data'][23], strrpos($meta_data['wrapper_data'][23], 'Last-Modified:') + 15);
      fclose($file);
      return $modified_Date;
      } catch (Exception $e) {
      $this->error("An error occurred while stream meta data for date time");
      $this->error($e->getMessage());
      return null;
      }
      } */

    public function getCirqCronLogs() {
        try {
            $result = CirqliveCronLogs::orderBy('id', 'desc')->first();
            return $result;
        } catch (Exception $e) {
            \Log::error("An error occurred while getting cirq_cron_logs");
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }

    public function saveCronLogs($from, $to) {
        try {
            $cron_logs = new CirqliveCronLogs();
            $cron_logs->from_datetime = $from;
            $cron_logs->to_datetime = $to;
            $cron_logs->save();
            return $cron_logs->id;
        } catch (Exception $e) {
            \Log::error("An error occurred while saving cirq_cron_logs");
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }

    /*  private function downloadFile($url, $path) {
      $newfname = $path;
      $file = fopen($url, 'rb');
      if ($file) {
      $newf = fopen($newfname, 'wb');
      if ($newf) {
      while (!feof($file)) {
      fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
      }
      }
      }
      if ($file) {
      fclose($file);
      }
      if ($newf) {
      fclose($newf);
      }
      }
     */
    /* Get webex download url by hitting api of node (puppeteer) */

    public function getWebexUrl($w_url) {
        try {
            $client = new Client();
            $url = "http://ec2-3-213-131-51.compute-1.amazonaws.com/sibme-webex/get_url";

            $myBody = array(
                'json' => array(
                    'webex_url' => $w_url,
            ));

            $request = $client->post($url, $myBody);
            $actual_url = json_decode($request->getBody());
            return $actual_url;
        } catch (Exception $e) {
            var_dump($e->getCode());
            var_dump($e->getMessage());
            return false;
        }
    }

    public function saveRecordingToServer($recording_url, $fileName) {
        try {
            $guzzle_client = new Client(); //GuzzleHttp\Client
            $guzzle_client->request('GET', $recording_url, [
                'sink' => public_path($fileName)
            ]);
            return true;
        } catch (Exception $e) {
            var_dump($e->getMessage());
            return false;
        }
    }
    
   public function CreateJwtToken($key,$secret){
    $payload = array(
        "iss" => $key,
        "exp" => time() + (5 * 60) //5 minutes,
    );

        return JWT::encode($payload, $secret);
    }

}
