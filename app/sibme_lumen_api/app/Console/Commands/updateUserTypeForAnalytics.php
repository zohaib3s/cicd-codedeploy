<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;

/**
 * Class updateUserTypeForAnalytics
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class updateUserTypeForAnalytics extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "update:user_type_for_analytics";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will automatically process (direct published) elemental jobs to check the transcoding status";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        echo "\nStart Time:" . date("Y-m-d H:i:s") . "\n";
        $start = \Carbon\Carbon::now();

        $myRequest = new \Illuminate\Http\Request();
        $myRequest->setMethod('GET');
        $myRequest->headers->set('site_id', 1);
        $apiController = new \App\Http\Controllers\ApiController($myRequest);

        $apiController->update_user_type();

        echo "\nEnd Time:" . date("Y-m-d H:i:s") . "\n";
        $end = \Carbon\Carbon::now();
        echo "Total Execution Time: " . $start->diffInHours($end) . ':' . $start->diff($end)->format('%I:%S') . '\n'; 

        /*
        try {
            app('App\Http\Controllers\ApiController')->update_user_type();
            echo "\nCompleted.....\n";
        } catch (Exception $e) {
            $this->error("An error occurred");
            $this->error($e->getMessage());
        }
        */
    }
}