<?php

/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Models\AccountFolderDocument;
use App\Models\Document;
use App\Models\DocumentFiles;
use App\Models\ScreenShareLogs;
use App\Models\User;
use App\Services\HelperFunctions;
use App\Services\S3Services;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Format\Video\X264;
use Illuminate\Console\Command;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;

/**
 * Class CirqliveRecordings
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class MillicastRecordings extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:millicast_recordings";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will download the all recordings from millicast and upload to aws s3 bucket";

    private $authDetails = false;
    private $s3Client;
    private $ffmpeg;
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->customLogs("Start processing Millicast Recordings");
        $this->getMillicastRcordings();
        $this->customLogs("End processing Millicast Recordings");
        $videoController = app('App\Http\Controllers\VideoController');	
        $videoController->onlineUserCheck();
    }

    /* Get All Accounts CirQlive Configuration */

    public function setFFMPEGInstance()
    {
        $use_ffmpeg_path = config('core.use_ffmpeg_path');
        $ffmpeg_binaries = config('core.ffmpeg_binaries');
        $ffprobe_binaries = config('core.ffprobe_binaries');

        if ($use_ffmpeg_path == 0) {

            $ffmpeg = FFMpeg::create();
        } else {

            $ffmpeg = FFMpeg::create(array(
                'ffmpeg.binaries' => $ffmpeg_binaries,
                'ffprobe.binaries' => $ffprobe_binaries,
            ));
        }
        $this->ffmpeg = $ffmpeg;
    }
    public function getMillicastRcordings($page = 1,$items = 25) {
        $use_coconut = env("USE_COCONUT_TRANSCODING");
        $use_ffmpeg = env("USE_FFMPEG_TRANSCODING");
        if($use_ffmpeg)
        {
            $this->setFFMPEGInstance();
            $use_coconut = 0;
        }
        $this->s3Client = new S3Services(
            config('s3.amazon_base_url'), config('s3.bucket_name'), config('s3.access_key_id'), config('s3.secret_access_key')
        );
        $huddleController = app('App\Http\Controllers\HuddleController');
        $this->authDetails = $huddleController->getStreamingPlatformDetails();

        $url = "https://api.millicast.com/api/record_files/list?page=$page&itemsOnPage=$items&isDescending=true";
        $result = HelperFunctions::sendCurlRequest($url,$this->authDetails["api_token"]);
        $need_recall = true;
        $total_items = count($result->data);
        $this->customLogs("Currently page = ".$page.", total items found = ". $total_items);
        if($total_items < $items)
        {
            $need_recall = false;
        }
        if($result->status == "success" && $total_items)
        {
            $recording_data = $result->data;
            foreach ($recording_data as $rec_data) {
                $single_rec_url = "https://api.millicast.com/api/record_files/".$rec_data->id;
                $this->customLogs("Current Recording status : ". $rec_data->status);
                if ($rec_data->status == "Complete") {

                    $single_recording = HelperFunctions::sendCurlRequest($single_rec_url,$this->authDetails["api_token"]);
                    if($single_recording->status == "success" && !empty($single_recording->data) && $single_recording->data->status == "Complete")
                    {
                        $single_recording = $single_recording->data;
                        $file_size =$single_recording->metadata->size;
                        $file_duration =$single_recording->metadata->duration;
                        $fileName = $stream_name = $single_recording->streamName;
                        $fileName .= ".mp4";
                        $stream_name_parts = explode("_",$stream_name);
                        $downloaded_video_file = public_path("tempTranscribeFiles/".$fileName);
                        if(isset($stream_name_parts[4]))
                        {
                            $document_id = $stream_name_parts[4];
                            if(strpos($stream_name,'_screen_sharing_') !== false)
                            {
                                $document = Document::join("account_folder_documents AS AFD","documents.id","AFD.document_id")
                                    ->join("screen_share_logs AS ssl","documents.id","ssl.document_id")
                                    ->select("documents.*", "AFD.account_folder_id", "ssl.id as log_id")
                                    ->where('documents.id', $document_id)
                                    ->where('ssl.file_name_recorded_video', $stream_name)
                                    ->where('ssl.uploaded_to_s3', 0)
                                    ->first();
                                if($document)
                                {
                                    $s3_dest_folder_path_only = $document->account_id."/". $document->account_folder_id ."/" . date('Y') . "/" . date('m') . "/" . date('d');
                                    $s3_dest_folder_path = "$s3_dest_folder_path_only/$fileName";

                                    $this->customLogs("Started Downloading of screen stream file : ". $fileName);
                                    $this->downloadFile($single_recording->download->downloadUrl, $downloaded_video_file,3);
                                    $this->customLogs("Downloading Complete of screen stream file : ". $fileName);
                                    $s_url = $this->s3Client->publish_to_s3($downloaded_video_file, "uploads/" .$s3_dest_folder_path_only."/", false);
                                    if ($s_url)
                                    {
                                        $s3_dest_folder_path = "uploads/" . $s3_dest_folder_path;
                                        ScreenShareLogs::where('id', $document->log_id)->update(["uploaded_to_s3"=>1, "s3_path"=>$s3_dest_folder_path]);
                                    }
                                }
                                continue;
                            }
                            $this->customLogs("Current recording document id : ". $document_id);
                            $dynamic_column = "documents.id";
                            if(isset($stream_name_parts[5]) && $stream_name_parts[5] == 'safari')
                            {
                                $dynamic_column = "documents.millicast_rec_id";
                            }
                            $document = Document::join("account_folder_documents AS AFD","documents.id","AFD.document_id")
                                ->select("documents.*", "AFD.account_folder_id")
                                ->where($dynamic_column,$document_id)
                                ->where("documents.doc_type",1)
                                ->where("documents.encoder_provider",5)
                                ->where("documents.aws_transcoder_type",0)
                                ->where("documents.published",0)
                                ->where("documents.original_file_name",$stream_name)
                                ->first();

                                if($document && empty($document->zencoder_output_id)  && $document->encoder_status != "Complete" && $document->encoder_status != "complete" && $document->encoder_status != "Submitted" && $document->encoder_status != "submitted" && $document->encoder_status != "Processing"){
                                    $user_email = User::where('id',$document->created_by)->pluck('email')->first();
                                    $this->create_churnzero_event('Live+Hours+Streamed', $document->account_id, $user_email,$single_recording->metadata->duration);
                                }
                            if($document && empty($document->zencoder_output_id) && $document->video_is_saved == 1 && $document->encoder_status != "Complete" && $document->encoder_status != "complete" && $document->encoder_status != "Submitted" && $document->encoder_status != "submitted" && $document->encoder_status != "Processing")
                            {
                                $document_id = $document->id;
                                $this->customLogs("Current recording document record found successfully");
                                $user_id = $document->created_by;
                                $account_id = $document->account_id;
                                $video_file_name = (strpos($document->original_file_name, '.mp4') !== false) ? $document->original_file_name : $document->original_file_name.".mp4";
                                $s3_dest_folder_path_only = "$account_id/". $document->account_folder_id ."/" . date('Y') . "/" . date('m') . "/" . date('d');
                                $s3_dest_folder_path = "$s3_dest_folder_path_only/$video_file_name";

                                $stream_logs = DB::select("SELECT COUNT(*) AS total, COUNT(uploaded_to_s3) AS available FROM screen_share_logs WHERE document_id = $document_id");
                                if($stream_logs[0]->total >= 1)//there are screen share files
                                {
                                    if($stream_logs[0]->available == $stream_logs[0]->total)// and they are available locally
                                    {
                                        $temp_data = $this->mergingMultipleVideos($document_id, "uploads/" .$s3_dest_folder_path_only, $stream_name, $account_id, $user_id, $document->site_id, $document->account_folder_id);
                                    }
                                    else //all screen share files not downloaded yet so not processing this document for now.
                                    {
                                        continue;
                                    }
                                }

                                if(!$use_coconut)
                                {
                                    $this->customLogs("Started Downloading of stream file : ". $video_file_name);
                                    $this->downloadFile($single_recording->download->downloadUrl, $downloaded_video_file,3);
                                    $this->customLogs("Downloading Complete of stream file : ". $video_file_name);
                                }
                                $downloaded_video_thumbnail = public_path("tempTranscribeFiles/".$stream_name."_zencoded_thumb.png");

                                if(isset($single_recording->metadata->thumbnails[0]))
                                {
                                    $this->customLogs("Started Downloading of stream thumbnail : ". $video_file_name);
                                    $this->downloadFile($single_recording->metadata->thumbnails[0], $downloaded_video_thumbnail,3);
                                    $this->customLogs("Downloading Complete of stream thumbnail : ". $video_file_name);
                                    $this->s3Client->publish_to_s3($downloaded_video_thumbnail, "uploads/" .$s3_dest_folder_path_only."/", false);
                                    $this->s3Client->copy_from_s3("uploads/" .$s3_dest_folder_path_only."/".$stream_name."_zencoded_thumb.png", "uploads/" .$s3_dest_folder_path_only."/".$stream_name."_thumb.png");
                                }
                                else
                                {
                                    $this->customLogs("No thumbnail found on Millicast for this video");
                                }
                                if(!$use_coconut && !$use_ffmpeg)//Zencoder
                                {
                                    $s_url = $this->s3Client->publish_to_s3($downloaded_video_file, "uploads/" .$s3_dest_folder_path_only."/", false);
                                    $this->customLogs("Current recording S3 path : ".$s_url);
                                }
                                else
                                {
                                    $s_url = true;
                                    $this->customLogs("Current recording will be published to S3 by coconut or ffmpeg");
                                }
                                if ($s_url) {

                                    if($use_coconut)
                                    {
                                        $s3_url = "/uploads/" . $s3_dest_folder_path;
                                        $s3_zencoder_id = $this->submitCoconutJob($s3_url, $single_recording->download->downloadUrl, $document_id);
                                        $this->customLogs("Current recording coconut id = ".$s3_zencoder_id);
                                    }
                                    else if($use_ffmpeg)
                                    {
                                        $s3_zencoder_id = -1;
                                        $temp_data = $this->transcodeWithFFMPEG($downloaded_video_file);
                                        if($temp_data['status'])
                                        {
                                            $s3_url = $this->s3Client->publish_to_s3($temp_data['data'], "uploads/" .$s3_dest_folder_path_only."/", false);
                                            $s3_dest_folder_path = $s3_dest_folder_path_only."/". pathinfo($temp_data['data'])['filename']. ".mp4";
                                            $this->customLogs("Current recording submitted to ffmpeg.\n output s3 path : $s3_url. \n response:\n".json_encode($temp_data));
                                        }
                                        else
                                        {
                                            $this->customLogs('Moving to next live_stream as ffmpeg returned '. $temp_data['message']);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        $s3_url = "s3://".config('s3.bucket_name')."/uploads/" . $s3_dest_folder_path;
                                        $s3_zencoder_id = $this->submitZencoderJob($s3_url);
                                        $this->customLogs("Current recording Zencoder id = ".$s3_zencoder_id);
                                    }
                                    $encoder_provider = 5;
                                    $encoder_status = "Processing";
                                    $transcoding_status = 2;
                                    if(!$s3_zencoder_id)
                                    {
                                        $s3_zencoder_id = "";
                                        $encoder_provider = "";
                                        $encoder_status = "error";
                                        $transcoding_status = 5;
                                    }
                                    $s3_dest_folder_path = "uploads/" . $s3_dest_folder_path;

                                    $updates = array(
                                        'zencoder_output_id' => $s3_zencoder_id,
                                        'url' => $s3_dest_folder_path,
                                        'encoder_provider' => $encoder_provider,
                                        'aws_transcoder_type' => 0,
                                        'file_size' => $file_size,
                                        'video_duration' => $file_duration,
                                        'published' => 0,
                                        'video_is_saved' => '1',
                                        'encoder_status' => $encoder_status,
                                        'doc_type' => 1,
                                    );echo "Updates to documents : \n";print_r($updates);
                                    Document::where("id",$document_id)->update($updates);
                                    $video_data = array(
                                        'document_id' => $document_id,
                                        'url' => $s3_dest_folder_path,
                                        'duration' => $file_duration,
                                        'file_size' => $file_size,
                                        'transcoding_job_id' => $s3_zencoder_id,
                                        'transcoding_status' => $transcoding_status,
                                        'default_web' => true,
                                        'site_id' => $document->site_id,
                                        'created_at' => date('Y-m-d H:i:s'),
                                        'debug_logs'=>"Lumen::MillicastRecordigs::getMillicastRecordings::line=295::document_id=".$document_id."::status=".$transcoding_status."::deleted/created"
                                    );
                                    DocumentFiles::where("document_id",$document_id)->delete();
                                    DocumentFiles::insert($video_data);
                                    //Call Encode Video function and update document table fields
                                    if (file_exists($downloaded_video_file)) {
                                        @unlink($downloaded_video_file);
                                    }
                                    if (file_exists($downloaded_video_thumbnail)) {
                                        @unlink($downloaded_video_thumbnail);
                                    }
                                    /*$delete_response = HelperFunctions::sendCurlRequest($single_rec_url,$this->authDetails["api_token"],"DELETE");
                                    $this->customLogs("Delete API Response : \n".json_encode($delete_response));*/
                                } else {
                                    echo "error in uploading temp folder s3";
                                }
                            }
                            else
                            {
                                $this->customLogs($document? "Current Document is already submitted for or completed transcoding".$document->encoder_status:"Document record not found in database with id = ".$document_id);
                            }

                        }
                    }

                }
                else if($rec_data->status == "Error")
                {
                    //Delete Recording from Millicast if it has Error because it will not be of any use.
                    HelperFunctions::sendCurlRequest($single_rec_url,$this->authDetails["api_token"],"DELETE");
                }
            }
        }

        $this->processCorruptVideos();

        if($need_recall)
        {
            //$this->getMillicastRcordings(($page+1));//Unlock this if more then 25 live videos being generated in a minute
        }
    }

    public function processCorruptVideos()
    {
        $document_ids = Document::whereNotIn("encoder_status",["new","complete","failed","Complete","Error", "error", "Submitted", "submitted", "Processing", "processing"])->where("video_is_saved", 1)->where("published", 0)->where("aws_transcoder_type",0)->where("video_duration",0)->where("is_processed",4)->where("zencoder_output_id","")->where("doc_type",1)->pluck("id")->toArray();
        if(count($document_ids))
        {
            $this->customLogs("Corrupt Video ids = ". implode(", ",$document_ids));
            Document::whereIn("id",$document_ids)->update(["encoder_status"=>"Error"]);
            DocumentFiles::whereIn("document_id",$document_ids)->update(["transcoding_status"=>5, "updated_at"=>date('Y-m-d H:i:s'),'debug_logs'=>"Lumen::MillicastRecordigs::processCorruptVideos::line=344::document_id=".implode(",",$document_ids)."::status=5::updated"]);
        }
        else
        {
            $this->customLogs("No Corrupt video found!");
        }
    }

    private function downloadFile($url, $path, $strategy = 1)
    {
        try
        {
            if($strategy == 2)
            {
                $contents = file_get_contents($url);
                file_put_contents($path, $contents);
            }
            else if($strategy == 3)
            {
                $client = new Client(); //GuzzleHttp\Client
                $client->request('GET', $url, [
                    'sink' => $path
                ]);
            }
            else
            {
                $newfname = $path;
                $file = fopen ($url, 'rb');
                if ($file) {
                    $newf = fopen ($newfname, 'wb');
                    if ($newf) {
                        while(!feof($file)) {
                            fwrite($newf, fread($file, 1024 * 50), 1024 * 50);
                        }
                    }
                }
                if ($file) {
                    fclose($file);
                }
                if ($newf) {
                    fclose($newf);
                }
            }
        }
        catch (\Exception $e)
        {
            $this->customLogs("Error while downloading : \n".$e->getMessage());
        }

    }
    
    public function customLogs($info,$should_echo = false)
    {
        if($should_echo)
        {
            echo $info."\n";
        }
        else
        {
            Log::info($info);
        }
    }

    public function submitZencoderJob($s3_url)
    {
        $zencoder = new \App\Services\Zencoder();
        return $zencoder->createJob($s3_url);
    }

    public function submitCoconutJob($s3_url, $file_url, $document_id)
    {
        $coconut = new \App\Services\Coconut();
        return $coconut->createJob($s3_url, $file_url, $document_id);
    }

    public function transcodeWithFFMPEG($url) {
        $this->customLogs("Transcoding start of $url with FFMPEG!");
        try {
            if (!empty($url)) {
                $videoFilePath = pathinfo($url);
                $videoFileName = $videoFilePath['filename'];

                $video = $this->ffmpeg->open($url);
                $format = new X264();
                $format->setAudioCodec("aac");
                $format->on('progress', function ($video, $format, $percentage) {
                    echo "$percentage % transcoded";
                });

                //uncomment following code to set bitrate of video
                /*$format
                    ->setKiloBitrate(1000)
                    ->setAudioChannels(2)
                    ->setAudioKiloBitrate(128);*/

                //uncomment following code to set video dimension
                /*$video
                    ->filters()
                    ->resize(new Dimension(320, 240))
                    ->synchronize();*/

                //Uncomment following code to get thumbnail
                /* $video
                     ->frame(TimeCode::fromSeconds(10))
                     ->save('frame.jpg');*/
                $outputPath = public_path("tempTranscribeFiles/".$videoFileName).'_ffmpeg.mp4';
                $video
                    ->save($format, $outputPath);
                $this->customLogs("Transcoding END with success FFMPEG!");
                return ["status" => true, "message" => "duration found.", "data" => $outputPath];
            } else {
                $this->customLogs("Not Transcoding with FFMPEG url is empty!");
                return ["status" => false, "message" => "Unable to find duration.", "data" => -1];
            }
        } catch (\Exception $ex) {
            $this->customLogs("Error while Transcoding with FFMPEG!\n");
            $this->customLogs($ex->getMessage());
            return ["status" => false, "message" => "Error: ".$ex->getMessage(), "data" => -1];
        }

    }

    public function ffmpegClipMerging($document_id, $main_file_name, $temp_dir = "")
    {
        $stream_logs = ScreenShareLogs::where("document_id",$document_id)->get();
        $filters = "";
        $overlays = "";
        $inputs = " -i $main_file_name ";
        $counter = 1;
        $old_overlay = "0:v";
        foreach ($stream_logs as $log)
        {
            $inputs .= " -i ".$temp_dir."".$log->file_name_recorded_video.".mp4";
            $filters .= "[$counter:v]setpts=PTS+".($log->started_at+1)."/TB[v_$log->id];";
            $overlays  .= "[$old_overlay][v_$log->id]overlay=enable='between(t,".($log->started_at+1).",$log->stopped_at)'[o_$log->id];";
            $old_overlay = "o_$log->id";
        }
        $overlays = rtrim($overlays,";");
        $overlays .= '"';
        $command = "ffmpeg ". $inputs. ' -filter_complex "'.$filters.' '.$overlays.' -map ['.$old_overlay.'] -map 0:a -c:v libx264 -c:a aac '.$temp_dir."ffmpeg_$document_id.mp4";
        $this->customLogs("FFMPEG command for id= $document_id : \n". $command);
        $output = system($command);
        $this->customLogs("Output from ffmpeg clip merging command for id = $document_id : \n".$output);
/*
The above code will generate a command like following
system('ffmpeg -i cam.mp4 -i screen1.mp4 -i screen2.mp4 \
-filter_complex '[1:v]setpts=PTS+28/TB[a];[2:v]setpts=PTS+73/TB[b]; \
                 [0:v][a]overlay=enable='between(t,28,47)'[out1];[out1][b]overlay=enable='between(t,73,84)':shortest=0[out]" \
         -map [out] -map 0:a \
         -c:v libx264 -c:a aac \
         output.mp4')*/
        return $temp_dir."ffmpeg_$document_id.mp4";
    }

    public function mergingMultipleVideos($document_id, $folder_path, $filename, $account_id, $user_id, $site_id, $account_folder_id)
    {
        $document = new Document();
        $document->account_id = $account_id;
        $document->doc_type = 1;
        $document->url = $folder_path;
        $document->original_file_name = $filename;
        $document->active = 1;
        $document->created_date = date("Y-m-d H:i:s");
        $document->created_by = $user_id;
        $document->last_edit_date = date("Y-m-d H:i:s");
        $document->recorded_date = date("Y-m-d H:i:s");
        $document->file_size = 0;
        $document->last_edit_by = $user_id;
        $document->published = '0';
        $document->post_rubric_per_video = '1';
        $document->site_id = $site_id;
        $document->save();

        $account_folder_doc = new AccountFolderDocument();
        $account_folder_doc->account_folder_id = $account_folder_id;
        $account_folder_doc->document_id = $document->id;
        $account_folder_doc->title = $filename;
        $account_folder_doc->zencoder_output_id = "";
        $account_folder_doc->site_id = $site_id;
        $account_folder_doc->desc = "This is auto generated file from system.";
        $account_folder_doc->save();
        $clips = [];
        $logs = ScreenShareLogs::where("document_id", $document_id)->where('uploaded_to_s3',1)->get();
        foreach ($logs as $log)
        {
            $clips[] = $log->file_name_recorded_video.'.mp4';
        }
        $request = new Request(['document_id'=>$document->id, 'folder_path'=>$folder_path, 's3_dest_folder_path'=>$folder_path, 'clips'=>implode(',',$clips), 'file_name'=>$filename]);
        app('App\Http\Controllers\LiveStreamController')->submit_clip_merging_job($request);
    }
    function create_churnzero_event($event_name,$account_id,$user_email,$quantity=1,$custom_fields='')
    {
           if(strpos($user_email,"@sibme.com") || strpos($user_email,"@jjtestsite.us")   )
            {
              return true;
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://analytics.churnzero.net/i?appKey=invw-q7Ivjwby8NI1F6qQcH1Gix0811ja7-Li4_1xWg&accountExternalId='.$account_id.'&contactExternalId='.$user_email.'&action=trackEvent&eventName='.$event_name.'&description='.$event_name.'&quantity='.$quantity.$custom_fields );
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $result = curl_exec($ch);
            curl_close($ch);
            return true;
        
    }


}
