<?php

/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Http\Controllers\VideoController;
use App\Models\Document;
use App\Services\HelperFunctions;
use Aws\S3\S3Client;
use Exception;
use Illuminate\Console\Command;
use App\Models\AccountFolder;
use App\Models\User;
use App\Models\Sites;
use App\Models\EmailUnsubscribers;
use App\Models\DocumentSubtitle;
use Illuminate\Support\Facades\DB;
use App\Models\JobQueue;
use App\Services\SendGridEmailManager;
use App\Services\S3Services;
use Aws\TranscribeService\TranscribeServiceClient;
use AwsTranscribeToWebVTT\Transcriber;
use Illuminate\Support\Facades\Log;

/*
 * Transcribe column "subtitle_available" values explained
 *
 * 0 => Subtitles not available (Not yet requested)
 * 1 => Subtitles Available
 * 2 => Subtitles Job currently in progress
 * 3 => Error while Submitting Transcribe Job
 * */

/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class processTranscribeJobs extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:transcribe_jobs";
    protected $createSRT = true;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will automatically process transcribe jobs to check the video transcribe/subtitles status";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        try {
            Log::info("Running Transcribe Job");
            $transcribeClient = new TranscribeServiceClient(array(
                        'region' => 'us-east-1',
                        'version' => 'latest',
                        'credentials' => array(
                            'key' => config('s3.access_key_id'),
                            'secret' => config('s3.secret_access_key'),
                        )
            ));
            $documents = Document::leftJoin('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                    ->select("documents.*", "afd.account_folder_id", "afd.title")
                    ->where("documents.subtitle_available", 2)
                    ->where("documents.allow_transcribe", 1)
                    ->orderBy('documents.id', 'desc')->get();
            $update_ids = [];
            foreach ($documents as $document) {

                $document_id = $document->id;

                Log::info("Running Transcribe Job for document id : " . $document_id);
                try {
                    $result = $transcribeClient->getTranscriptionJob(["TranscriptionJobName" => $document_id . "_transcribe"]);
                    
                    } catch (\Exception $e) {
                    Log::error("Error reading Transcribe Job : \n" . $e->getMessage());
                    continue;
                }
                $job = $result->get("TranscriptionJob");
                if ($job["TranscriptionJobStatus"] == "COMPLETED") {
                    $filesToUpload = $this->createVttFromJson($job["Transcript"]["TranscriptFileUri"], $document_id);
                   
                    if (!empty($filesToUpload)) {
                        if ($this->createSRT) {
                            $srtFilePath = $this->createSrtFromVtt($filesToUpload[0], $document_id,$document->created_by);
                            if (!empty($srtFilePath)) {
                                $filesToUpload[] = $srtFilePath;
                            }
                        }
                    }
                    if (!empty($filesToUpload)) {
                        $subtitle_paths = $this->uploadToS3($filesToUpload, $document->url);
                        $update_ids[] = $document_id;
                        $DocumentSubtitle= $this->getSubtitlesFromDb($document_id);
                        $event = [
                            'channel' => 'video-details-' . $document_id,
                            'event' => "subtitles_available",
                            'data' => $document,
                            'subtitle_paths' => $subtitle_paths,
                            'subtitles_data' => $DocumentSubtitle,
                            'reference_id' => $document_id,
                            'document_id' => $document_id,
                            'user_id' => $document->created_by,
                            'account_folder_id' => $document->account_folder_id,
                            'video_file_name' => $document->title,
                            'site_id' => $document->site_id
                        ];
                        HelperFunctions::broadcastEvent($event);
                        foreach ($filesToUpload as $file) {
                            unlink($file);
                        }
                        Document::where("id", $document_id)->update(["subtitle_available" => 1]);
                        //Send Email to user after Transcribe video
                            if($document->account_folder_id && (strtotime($document->created_date) >= strtotime('2021-05-27 00:00:00')) && EmailUnsubscribers::check_subscription($document->created_by, '13', $document->account_id, $document->site_id))//we have disabled crons on 27 may 2021 so i added check for those videos if they are newly created videos then email should go
                            {
                                $huddle_data = $this->getHuddleData($document->account_folder_id); //Get HUddle Data For Email
                                $user_result = User::where(array('id' => $document->created_by ))->first();
                                if($huddle_data)
                                {
                                    $this->sendVideoTranscribedNotification($huddle_data['name'], $document->account_folder_id, $document_id, $user_result, $document->account_id, $user_result->email, $document->created_by, $huddle_data['folder_type'], '13');
                                }
                            }
                        }
                } else if ($job["TranscriptionJobStatus"] == "error" || $job["TranscriptionJobStatus"] == "ERROR") {
                    Log::info("Error Running Transcribe Job for document id : " . $document_id . ". Job Status = " . $job["TranscriptionJobStatus"]);
                }
                sleep(3);
            }
            \Log::info("All transcribe jobs are processed.");
            Log::info("End Running Transcribe Job");
            /**/
        } catch (Exception $e) {
            $this->error("An error occurred while transcribe job processing");
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }

    public function saveFileLocally($content, $document_id, $ext) {
        $dir = public_path("tempTranscribeFiles");
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        }
        $filePath = public_path("tempTranscribeFiles/" . $document_id . '_transcribe.' . $ext);
        $content = str_replace("- ", "", $content);
        file_put_contents($filePath, $content);
        /* $fh = fopen($filePath, 'w');
          fputs($fh, $content);
          fclose($fh); */
        return $filePath;
    }

    public function createVttFromJson($jsonUrl, $document_id) {
        $vttFile = "";
        $jsonFile = "";
        try {
            $transcriber = new Transcriber();
            $jsonData = file_get_contents($jsonUrl);
            $transcriber->setAwsTranscription($jsonData);
            $result = $transcriber->getOutputAsString();

            $jsonFile = $this->saveFileLocally($jsonData, $document_id, "json");
            $vttFile = $this->saveFileLocally($result, $document_id, "vtt");
        } catch (\Exception $e) {
            $this->error("An error occurred while converting json to vtt file.");
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }

        return [$vttFile, $jsonFile];
    }

    function createSrtFromVtt($webVttFile, $document_id,$user_id) {
        $srtFile = "";
        try {
            // Read the WebVTT file content into an array of lines
            $lines = file($webVttFile);
            $this->saveDocumentSubtitles($document_id,$user_id,$lines);
            // Convert all timestamp lines
            // The first timestamp line is 3
            $length = count($lines);

            for ($index = 3; $index < $length; $index++) {
                // A line is a timestamp line if the second line above it is an empty line
                if (trim($lines[$index - 2]) === '') {
                    $lines[$index] = str_replace('.', ',', $lines[$index]);
                }
            }

            // Remove 2 first lines of WebVTT format
            unset($lines[0]);
            unset($lines[1]);
            
            // Concatenate all other lines into the result file
            $srtFile = $this->saveFileLocally(implode('', $lines), $document_id, "srt");
            

        } catch (\Exception $e) {
            $this->error("An error occurred while converting vtt to srt file.");
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }

        return $srtFile;
    }
    /*Save docuemnt subtitles into db*/
    public function saveDocumentSubtitles($doc_id,$user_id, $transcribe_data){
        $arr = array_values(array_filter($transcribe_data));
        $arr = array_slice($arr,3);
       
       // $allow_range = count($arr) > 2 ? true : false;
        for($i=0; $i<=count($arr); $i=$i+3){
         try {
             if(isset($arr[$i]) && $i<=count($arr) -1)
             {
                 $subtitle_added =  DocumentSubtitle::where(
                     array(
                         'document_id' => $doc_id ,
                         'subtitles' => ltrim($arr[$i+1],'- '),
                         'created_by' => $user_id,
                         'time_range' => str_replace(',', '.', $arr[$i])
                     ))->first();
                 if(!$subtitle_added)
                 {
                     $subtitle = new DocumentSubtitle();
                     $subtitle->document_id = $doc_id;
                     $subtitle->time_range = str_replace(',', '.', $arr[$i]);
                     $subtitle->subtitles = ltrim($arr[$i+1],'- ');
                     $subtitle->created_by = $user_id;
                     $subtitle->created_date = date("Y-m-d H:i:s");
                     $subtitle->last_edit_by = $user_id;
                     $subtitle->last_edit_date = date("Y-m-d H:i:s");
                     $subtitle->save();
                 }
                // if(!$allow_range) break;
             }

         } catch (Exception $e) {
            $this->error("An error occurred while saving subtitles into database\n");
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
         }
	
        }
    }

    public function uploadToS3($localFiles, $doc_url) {
        $client = new S3Client([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);

        $pathInfo = pathinfo($doc_url);
        $new_paths = [];
        foreach ($localFiles as $localFile) {
            $localPath = pathinfo($localFile);
            $path = $client->putObject(array(
                'Bucket' => config('s3.bucket_name'),
                'Key' => $pathInfo["dirname"] . '/' . $localPath["filename"] . "." . $localPath["extension"],
                'SourceFile' => $localFile
            ));
            $new_paths[$localPath["extension"]] = VideoController::getSecureAmazonUrl($pathInfo["dirname"] . '/' . $localPath["filename"] . "." . $localPath["extension"]);
        }

        return $new_paths;
    }

    public function sendVideoTranscribedNotification($huddle_name, $huddle_id, $video_id, $creator, $account_id, $recipient_email, $huddle_user_id, $huddle_type, $email_type = '') {

        if (empty($recipient_email))
            return FALSE;


        $users = \DB::table("users")->where('id', $huddle_user_id)->first();
        $site_id = $users->site_id;
        $account_info = \DB::table("accounts")->where('id', $account_id)->first();
        
        $site_info = Sites::where(array('id' => $account_info->site_id ))->first();
        
        $sibme_base_url = 'https://' . $site_info->site_url . '/'; 

        $from = $users->first_name . " " . $users->last_name . " " . $account_info->company_name . " <" . Sites::get_site_settings('static_emails', $site_id)['noreply'] . ">";
        $to = $recipient_email;
        $lang = $creator->lang;
        $key = "video_transcribe_" . $site_id . "_" . $lang;

        $template = 'default';
        $sendAs = 'html';
        $lang_id = $users->lang;
        if ($lang_id == 'en') {
            $subject = Sites::get_site_settings('email_subject', $site_id) . ' - Video Transcribed';
        } else {
            $subject = Sites::get_site_settings('email_subject', $site_id) . ' - Video Transcrita';
        }
        if ($huddle_type == '1') {
            $url = $sibme_base_url . "huddles/view/" . $huddle_id . "/1/" . $video_id . '/3';
        } elseif ($huddle_type == '2') {
            $url = $sibme_base_url . "home/library_video/home/" . $huddle_id . '/' . $video_id ;
        } elseif ($huddle_type == '3') {
            $url = $sibme_base_url . "MyFiles/view/1/" . $huddle_id . '/5';
        } else {
            $huddle_type = '1';
            $url = $sibme_base_url . "huddles/view/" . $huddle_id . "/1/" . $video_id . '/6';
        }
        $html = '';
        $key = '';

        if ($huddle_type == 3) {
            $lang = $users->lang;
            $key = "video_transcribe_workspace_" . $site_id . "_" . $lang;
            $result = SendGridEmailManager::get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{redirect}', $url, $html);
            $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
            $html = str_replace('{unsubscribe}', $sibme_base_url . '/subscription/unsubscirbe_now/' . $huddle_user_id . "/" . $email_type, $html);
        } elseif ($huddle_type == 2) {
            $lang = $users->lang;
            $key = "video_transcribe_lib_" . $site_id . "_" . $lang;
            $result = SendGridEmailManager::get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{account_name}', $account_info->company_name, $html);
            $html = str_replace('{huddle_name}', $huddle_name, $html);
            $html = str_replace('{redirect}', $url, $html);
            $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
            $html = str_replace('{unsubscribe}', $sibme_base_url . '/subscription/unsubscirbe_now/' . $huddle_user_id . "/" . $email_type, $html);
        } else {
            $lang = $users->lang;
            $key = "video_transcribe_" . $site_id . "_" . $lang;
            $result = SendGridEmailManager::get_send_grid_contents($key);
            $html = $result->versions[0]->html_content;
            $html = str_replace('<%body%>', '', $html);
            $html = str_replace('{sender}', $creator->first_name . " " . $creator->last_name, $html);
            $html = str_replace('{huddle_name}', $huddle_name, $html);
            $html = str_replace('{redirect}', $url, $html);
            $html = str_replace('{site_url}', SendGridEmailManager::get_site_url(), $html);
            $html = str_replace('{unsubscribe}', $sibme_base_url . '/subscription/unsubscirbe_now/' . $huddle_user_id . "/" . $email_type, $html);
        }


        $auditEmail = array(
            'account_id' => $account_id,
            'email_from' => $from,
            'email_to' => $creator->email,
            'email_subject' => $subject,
            'email_body' => $html,
            'is_html' => true,
            'sent_date' => date("Y-m-d H:i:s")
        );


        if (!empty($to)) {

            \DB::table('audit_emails')->insert($auditEmail);
            $use_job_queue = config('s3.use_job_queue');

            if ($use_job_queue) {
                JobQueue::add_job_queue($site_id, 1, $to, $subject, $html, true);
                return TRUE;
            }
        }
        return 'Done';
    }

    public function getHuddleData($account_folder_id) {
        $huddle_data = AccountFolder::where(array(
                    'account_folder_id' => $account_folder_id,
                ))->first();
        if($huddle_data)
        {
            $huddle_data = $huddle_data->toArray();
        }
        return $huddle_data;
    }

    public function getUserObj($user_id, $account_id) {
        $ch = curl_init();
        $event_url = config('s3.sibme_base_url') . 'Api/get_user';
        curl_setopt($ch, CURLOPT_URL, $event_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ['user_id' => $user_id, 'account_id' => $account_id]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        curl_close($ch);
        return json_decode($server_output);
    }
    
    public function getSubtitlesFromDb($doc_id){
          try {
              $DocumentSubtitle= DocumentSubtitle::where('document_id', $doc_id)->get()->toArray();;
              return $DocumentSubtitle;
          } catch (Exception $e) {
              $this->error("An error occurred while getting subtitles from database\n");
              \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
              return false;
          }
    }

}
