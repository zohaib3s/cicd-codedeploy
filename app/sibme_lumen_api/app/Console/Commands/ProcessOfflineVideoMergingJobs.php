<?php

/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Document;
use App\Models\AccountMetaData;
use App\Models\DocumentFiles;
use App\Models\ScreenShareAwsJobs;
use App\Services\HelperFunctions;
use App\Services\S3Services;
use Aws\S3\S3Client;
use Exception;
use Illuminate\Console\Command;
use App\Models\DocumentAudio;
use Aws\ElasticTranscoder\ElasticTranscoderClient;

require_once base_path('aws3/autoload.php');
use Illuminate\Support\Facades\Log;


class ProcessOfflineVideoMergingJobs extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:offline_video_clip_merging_jobs";
    protected $s3_service = null;
//    protected $createSRT = true;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will check status of Offline videos Clip Merging jobs and update document status on success!";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->s3_service = HelperFunctions::getS3ServiceObject();
        $documents = $this->getDocuments();
        if (!empty($documents))
        {
            foreach ($documents as $document)
            {
                $clip_job = $this->s3_service->getElementalJobDetail($document);
                Log::info("Offline Video Clip Merging Job :\n".json_encode($clip_job));
                $status = $clip_job['status'];
                if ($status == 'complete')
                {
                    Document::where("id", $document->id)->update(["published" => 1, "video_duration"=>$clip_job['duration']]);
                    $document_file = DocumentFiles::where("document_id", $document->id)->first();
                    if(!$document_file)
                    {
                        $document_file = new DocumentFiles();
                        $document_file->document_id = $document->id;
                    }
                    $document_file->resolution = $clip_job['resolution'];
                    $document_file->duration = $clip_job['duration'];
                    $document_file->url = $document->url;
                    $document_file->default_web = 1;
                    $document_file->transcoding_status = 3;
                    $document_file->transcoding_status_details = $status;
                    $document_file->site_id = $document->site_id;
                    $document_file->debug_logs = "Lumen::ProcessOfflineVideoMergingJobs::handle::line=80::document_id=".$document->id."::status=3::".(!$document_file ? 'created' : 'updated');
                    $document_file->save();
                }
                else if($status == 'error')
                {
                    Document::where("id", $document->id)->update(["published" => 0, "encoder_status" => "Error", "last_edit_date"=>date('Y-m-d H:i:s')]);
                    DocumentFiles::where("document_id",$document->id)->update(["transcoding_status"=>5,"transcoding_status_details"=>"Clips merging job failed", "updated_at"=>date('Y-m-d H:i:s'),'debug_logs'=>"Lumen::ProcessOfflineVideoMergingJobs::handle::line=86::document_id=".$document->id."::status=5::updated"]);
                }
                S3Services::execute_thumbnail_websocket($document);
            }
        }
    }

    /* Get Documents and document file data if transcoded */

    public function getDocuments() {
        try {
            //Only get those documents that have Audio file available
            $documents = Document::where("documents.published", 0)
                            ->where("documents.encoder_status", 'complete')
                            ->where("documents.encoder_provider", 2)
                            ->where("documents.aws_transcoder_type", 2)
                            ->whereNotNull("documents.zencoder_output_id")
                            ->get();
            return $documents;
        } catch (Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
            return false;
        }
    }

}
