<?php

/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Models\Account;
use App\Models\Document;
use App\Models\AccountMetaData;
use App\Models\DocumentFiles;
use App\Models\ScreenShareAwsJobs;
use App\Services\HelperFunctions;
use App\Services\S3Services;
use Aws\S3\S3Client;
use Exception;
use Illuminate\Console\Command;
use App\Models\DocumentAudio;
use Aws\ElasticTranscoder\ElasticTranscoderClient;

require_once base_path('aws3/autoload.php');
use Illuminate\Support\Facades\Log;


class ProcessScreenShareFiles extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:screen_share_jobs";
    protected $s3_service = null;
//    protected $createSRT = true;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will check status of Screen Share Clipping jobs and also checks if audio is available for corresponding document and then submit a new elemental job to merge Audio with clipped video";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->s3_service = HelperFunctions::getS3ServiceObject();
        $documents = $this->getDocuments();
        if (!empty($documents))
        {
            foreach ($documents as $document)
            {
                $clip_job = $this->s3_service->getElementalJobDetail($document->clipping_job_id);
                Log::info("Cam & screens Merging Job :\n".json_encode($clip_job));
                $status = $clip_job['status'];
                if ($status == 'complete')
                {
                    $this->createJobForAudioMerge($document);
                }
                else if($status == 'error')
                {
                    print_r($clip_job);
                    Document::where("id", $document->document_id)->update(["published" => 0, "encoder_status" => "Error", "last_edit_date"=>date('Y-m-d H:i:s')]);
                    DocumentFiles::where("document_id",$document->document_id)->update(["transcoding_status"=>5,"transcoding_status_details"=>"Cam & screen share job failed", "updated_at"=>date('Y-m-d H:i:s'),'debug_logs'=>"Lumen::ProcessScreenShareFiles::handle::line=72::document_id=".$document->id."::status=5::updated"]);
                    S3Services::execute_thumbnail_websocket($document ,1);
                    ScreenShareAwsJobs::where("document_id", $document->document_id)->update(['clipping_job_status'=>'error']);
                }
            }
        }
    }

    /* Get Documents and document file data if transcoded */

    public function getDocuments() {
        try {
            //Only get those documents that have Audio file available
            $documents = Document::join('screen_share_aws_jobs as d_s', 'documents.id', '=', 'd_s.document_id')
                            ->join('document_audio as d_a', 'documents.id', '=', 'd_a.document_id')
                            ->select("documents.*","d_s.*","d_a.url as audio_url", "documents.id as id")
                            ->where("documents.published", 0)
                            ->where("d_a.transcoding_status", 'Complete')
                            ->where("documents.encoder_status", 'complete')
                            ->where("d_s.clipping_job_status", 'submitted')
                            ->get();
            return $documents;
        } catch (Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }
    
    /*Create amazon job from video to audio*/
    public function createJobForAudioMerge($document) {
        $main_url = substr($document->url, 0, strrpos( $document->url, '/'));
        $bucket = "s3://".config('s3.bucket_name');
        $file_name = substr($document->url, strrpos($document->url, '/') + 1);
        $file_name_without_ext = preg_replace('/\.[^.\s]{3,4}$/', '', $file_name);
        try {
            $preset = "Custom_elemental";
            $jobSettings = array (
                'TimecodeConfig' =>
                    array (
                        'Source' => 'ZEROBASED',
                    ),
                'OutputGroups' =>
                    array (
                        0 =>
                            array (
                                'CustomName' => 'screen_share_audio_merge',
                                'Name' => 'File Group',
                                'Outputs' =>
                                    [
                                        [
                                            "Preset" => $preset,
                                            "NameModifier"=> '_final',
                                        ]
                                    ],
                                'OutputGroupSettings' =>
                                    array (
                                        'Type' => 'FILE_GROUP_SETTINGS',
                                        'FileGroupSettings' =>
                                            array (
                                                'Destination' => $bucket.'/'.$main_url.'/Merged/',
                                            )
                                    )
                            )
                    ),
                'AdAvailOffset' => 0,
                'Inputs' =>
                    array (
                        0 =>
                            array (
                                'AudioSelectors' =>
                                    array (
                                        'Audio Selector 1' =>
                                            array (
                                                'Offset' => 0,
                                                'DefaultSelection' => 'DEFAULT',
                                                'SelectorType' => 'TRACK',
                                                'ExternalAudioFileInput' => $bucket."/".$document->audio_url,
                                                'ProgramSelection' => 1,
                                            )
                                    ),
                                'VideoSelector' =>
                                    array (
                                        'ColorSpace' => 'FOLLOW',
                                        'Rotate' => 'DEGREE_0',
                                        'AlphaBehavior' => 'DISCARD',
                                    ),
                                'FilterEnable' => 'AUTO',
                                'PsiControl' => 'USE_PSI',
                                'FilterStrength' => 0,
                                'DeblockFilter' => 'DISABLED',
                                'DenoiseFilter' => 'DISABLED',
                                'TimecodeSource' => 'ZEROBASED',
                                'FileInput' => $bucket.'/'.$main_url.'/Merged/'.$file_name_without_ext.'_merged_clips.mp4',
                            )
                    )
            );
            $jobId = $this->s3_service->submitElementalJob($jobSettings, false);
            $this->updateScreenShareJob($jobId,'submitted',$main_url.'/Merged/'.$file_name_without_ext.'_merged_clips_final.mp4',$document->id, $main_url.'/Merged/'.$file_name_without_ext.'_merged_clips.mp4');
        } catch (Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
             return false;
        }
    }
    public function updateScreenShareJob($jobId,$job_status,$url,$document_id,$clip_path){
        try {
            $s_document = ScreenShareAwsJobs::where("document_id", $document_id)->first();
            $s_document->clipping_job_status = 'complete';
            $s_document->clipped_video_path = $clip_path;
            $s_document->audio_merge_job_id = $jobId;
            $s_document->audio_merge_job_status = $job_status;
            $s_document->audio_merged_video_path = $url;
            $s_document->save();
            Log::info("Running audio aws Job for document id : " . $document_id. 'Job Id:'.$jobId);
        } catch (Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }

}
