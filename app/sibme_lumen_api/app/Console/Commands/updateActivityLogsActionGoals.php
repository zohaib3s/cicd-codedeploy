<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Api\Activities\GetActivities;
use Exception;
use App\Models\User;
use App\Models\Goal;
use Illuminate\Console\Command;

/**
 * Class updateUserTypeForAnalytics
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class updateActivityLogsActionGoals extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "update:activity_logs_action_goals";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will update activity logs table according to goal expiry";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $deadlineType =array("nextWeek","tommorow","yesterday");
         foreach ($deadlineType as $deadline){
            $goals = $this->getGoals($deadline);
          if(!empty($goals)) {
              foreach($goals as $goal){
                  $send_notification = true;
                  $send_email = true;
                   $user_result = User::select('lang')->where(array('id' => $goal['created_by'] ))->first();
                   $deadlineDate = $goal['end_date'];
                  if($deadline == 'nextWeek'){
                      $desc = "{ITEM_NAME} deadline is in on ".date("M d,Y", strtotime($deadlineDate));
                      $activity_type = Goal::activityTypes["goal_deadline_7_days"];
                      $send_notification = false;
                  }else if ($deadline == 'tommorow'){
                      $desc = "{ITEM_NAME} deadline is tomorrow ".date("M d,Y", strtotime($deadlineDate));
                      $activity_type = Goal::activityTypes["goal_deadline_tomorrow"];
                  }else if ($deadline == 'yesterday'){
                   $desc = "The deadline for {ITEM_NAME} has passed on ".date("M d,Y", strtotime($deadlineDate));
                   $activity_type = Goal::activityTypes["goal_deadline_passed"];
                    $send_email = false;
                  }
                  $special = date("M d,Y", strtotime($deadlineDate)). "||" . GetActivities::_SpanishDate(strtotime($deadlineDate), 'spanish_day_month_year');
                  $activity_data = ["current_lang"=>$user_result->lang,"item_name"=>$goal['title'],"goal_id"=>$goal['id'],"site_id"=>$goal['site_id'], "desc"=>$desc, "account_folder_id"=>$goal['account_folder_id'], "account_id"=>$goal['account_id'], "user_id"=>$goal['created_by'], "type"=>$activity_type,"send_notifications"=>$send_notification,"send_email"=>$send_email, "special"=>$special];
                  
                  app('App\Http\Controllers\GoalsController')->addRecentActivity($activity_data);
              }
          }
         }
         
       
        
    }
    /*Get all goals according to expiry date*/
     public function getGoals($deadlineType) {
        try {
            $result=array();
            if($deadlineType == 'nextWeek'){
            $today =     date('Y-m-d');
            $next_date = date('Y-m-d', strtotime('+7 day'));
            $result = Goal::whereDate('end_date', '=', date('Y-m-d', strtotime('+7 day')))->where("is_active",1)->where("is_published",1)->get();
            }else if($deadlineType == 'tommorow'){
                 $result = Goal::whereDate('end_date', '=', date('Y-m-d', strtotime('+1 day')))->where("is_active",1)->where("is_published",1)->get();
            }else{
                $result = Goal::whereDate('end_date', '=', date('Y-m-d', strtotime('-1 day')))->where("is_active",1)->where("is_published",1)->get();
            }
            return $result;
        } catch (Exception $e) {
            $this->error("An error occurred while getting goal items.".$deadlineType);
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }
}