<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Services\Cache\AnalyticsCache;
use Exception;
use Illuminate\Console\Command;

/**
 * Class CreateAnalyticsFiltersCache
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class CreateAnalyticsFiltersCache extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "create:analytics-filters-cache";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will automatically Creates Analytics Cache for all users";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            echo "\nStart Time:" . date("Y-m-d H:i:s") . "\n";
            $start = \Carbon\Carbon::now();
            $cache = new AnalyticsCache();
            $cache->process_filters_cache();
            echo "\nEnd Time:" . date("Y-m-d H:i:s") . "\n";
            $end = \Carbon\Carbon::now();
            echo "Total Execution Time: " . $start->diffInHours($end) . ':' . $start->diff($end)->format('%I:%S') . '\n'; 
            \Log::info("\nFilters Cache for all users is refreshed successfully.\n");
            /**/
        } catch (Exception $e) {
            $this->error("An error occurred");
            $this->error($e->getMessage());
        }
    }
}