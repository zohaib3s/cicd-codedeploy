<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Models\AccountFolderDocument;
use App\Models\Document;
use App\Models\DocumentsFilestackData;
use App\Services\HelperFunctions;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class processFilestackPendingItems extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:filestack_items";

    private $current_document_id = 0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will automatically process filestack pending items to check the if they are available on s3";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $client = new \GuzzleHttp\Client(['http_errors' => false]);
            $pending_items = DocumentsFilestackData::where("is_processed", 0)->get();
            $huddleController = app('App\Http\Controllers\HuddleController');
            foreach ($pending_items as $item)
            {
                $this->current_document_id = $item->document_id;
                $url = "https://cdn.filestackcontent.com/".$item->filestack_handle."/metadata";
                $response = $client->get($url);
                $body = $response->getBody();
                $data = json_decode($body);
                if(!empty($data) && isset($data->key) && isset($data->path))
                {
                    $size = 0;
                    if(isset($data->size))
                    {
                        $size = $data->size;
                    }
                    $s3_dest_folder_path_only = $item->s3_dest_folder_path_only;
                    $path_parts = explode('/', $s3_dest_folder_path_only);
                    $document = Document::where("id", $item->document_id)->first();
                    if(!$document){
                        continue;
                    }
                    if($document->doc_type=="1"){
                        $huddleController->site_id = $document->site_id;
                        $zencoder_output_id = $huddleController->uploadPendingProcess($path_parts[0], $path_parts[1], $item->document_id, $data->path, $item->suppress_success_email, $item->user_id, $item->site_id, $s3_dest_folder_path_only, $size, false, 1, $data->key);
                        $account_folder_doc = AccountFolderDocument::where("document_id", $item->document_id)->where('account_folder_id', $path_parts[1])->first();
                        $account_folder_doc->zencoder_output_id = $zencoder_output_id;
                        $account_folder_doc->save();
                    }
                    if($document->doc_type=="2"){
                        $documentUploader = new \App\Services\DocumentUploader();
                        $documentUploader->uploadResourcePendingProcess($document->id, $s3_dest_folder_path_only, $document->original_file_name, $data->path);
                    }
                    $huddleController->sendResourceUpdateSocket($item->site_id, $item->document_id, $item->folder_type, 1, $path_parts[1], $item->user_id, $path_parts[0]);
                    DocumentsFilestackData::where("id", $item->id)->update(['is_processed' => 1]);
                    $this->current_document_id = 0;
                }
                else
                {
                    Log::info("Key is still not available for document_id = $item->document_id.");
                }
            }
            Log::info("All Filestack Pending jobs are processed.");

        } catch (Exception $e) {
            if($this->current_document_id)
            {
                DocumentsFilestackData::where("document_id", $this->current_document_id)->update(['is_processed' => 2, 'error_message' => $e->getMessage()]);
            }

            Log::error("----------- An error occurred ------------");
            Log::error($e->getMessage());
            Log::error($e->getTraceAsString());
        }
    }
}