<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

use App\Services\S3Services;


/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class processReportExport extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:ReportExport {params} {--report=}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This process will export the specific report.";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //try {
            $params = $this->argument('params');
            $report = $this->option('report');
            $params = json_decode($params,true);
            $myRequest = new Request();
            $myRequest->setMethod('POST');
            $myRequest->request->add($params);
            $myRequest->headers->set('site_id', '1');
            $myRequest->headers->set('current-lang', 'en');
            $reportingController = new \App\Http\Controllers\ReportingController($myRequest);
            $reportingController->$report($myRequest);
//        } catch (Exception $e) {
//            $this->error("An error occurred");
//            $this->error($e->getMessage());
//        }
    }
}