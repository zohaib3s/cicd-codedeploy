<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;

use App\Services\S3Services;


/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class processCardFailureJobs extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:CardFailure";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will automatically process (direct published) elemental jobs to check the transcoding status";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $paymentController = app('App\Http\Controllers\PaymentController');	
            $paymentController->cron_for_checking_card_failures();
        } catch (Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }
}