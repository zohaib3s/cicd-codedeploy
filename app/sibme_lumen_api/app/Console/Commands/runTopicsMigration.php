<?php

/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Models\Account;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/*
 * Transcribe column "subtitle_available" values explained
 *
 * 0 => Subtitles not available (Not yet requested)
 * 1 => Subtitles Available
 * 2 => Subtitles Job currently in progress
 * 3 => Error while Submitting Transcribe Job
 * */

/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class runTopicsMigration extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "run:topicsMigration {next_step=0}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will automatically process transcribe jobs to check the video transcribe/subtitles status";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        try {
            Log::info("Running topicsMigration Job");
            $next_step = $this->argument('next_step');
            if($next_step == 0)
            {
                $this->runTopicsMigration();
            }
            else if($next_step == 1)
            {
                $this->makeSubjectMainCategory();
            }
            else if($next_step == 2)
            {
                $this->correctAccountAnomalies();
            }
            Log::info("End Running topicsMigration Job");
            /**/
        } catch (Exception $e) {
            $this->error("An error occurred while topicsMigration job processing");
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }

    public function runTopicsMigration()
    {
        $topics = DB::table("topics")->select("topics.*")
            ->join("accounts", "topics.account_id", "=", "accounts.id")
            ->join("account_folder_topics", "topics.id", "=", "account_folder_topics.topic_id")
            ->groupBy("topics.id")
            ->get();
        foreach ($topics as $topic)
        {
            $new_category = [
                "name"=> $topic->name,
                "account_id"=> $topic->account_id,
                "site_id"=>$topic->site_id,
                "created_by" => $topic->created_by,
                "created_date" => $topic->created_date,
                "last_edit_date" => $topic->last_edit_date,
                "last_edit_by" => $topic->last_edit_by,
                "is_topic" => 1
            ];
            $category_id = DB::table('subjects')->insertGetId($new_category);

            $topic_videos = DB::table("account_folder_topics")
                ->join("account_folders", "account_folders.account_folder_id", "=", "account_folder_topics.account_folder_id")
                ->where("topic_id", $topic->id)
                ->get();
            $new_category_relation = [];
            foreach ($topic_videos as $video)
            {
                $new_category_relation[] = [
                    "account_folder_id" => $video->account_folder_id,
                    "subject_id" => $category_id,
                    "site_id"=>$video->site_id,
                    "created_by" => $video->created_by,
                    "created_date" => $video->created_date,
                    "last_edit_date" => $video->last_edit_date,
                    "last_edit_by" => $video->last_edit_by,
                ];

            }
            DB::table('account_folder_subjects')->insert($new_category_relation);
        }

        return response()->json(["status"=> true, "message" => "Successfully migrated all topics!"]);
    }

    public function makeSubjectMainCategory()
    {
        DB::table('subjects')->whereNotNull("parent_id")->update(["parent_id"=>null]);
        $accounts = Account::all();
        foreach ($accounts as $account)
        {
            $subjectIds = DB::table('subjects')->where("is_topic", 0)->where("account_id", $account->id)->pluck("id");
            if($subjectIds)
            {
                $new_category = [
                    "name"=> "Subject",
                    "account_id"=> $account->id,
                    "site_id"=>$account->site_id,
                    "created_by" => $account->created_by,
                    "created_date" => date("Y-m-d H:i:s"),
                    "last_edit_date" => date("Y-m-d H:i:s"),
                    "last_edit_by" => $account->created_by
                ];
                $subject_id = DB::table('subjects')->insertGetId($new_category);
                $account_folder_ids = DB::table("account_folder_subjects")->whereIn("subject_id", $subjectIds)->groupBy("account_folder_id")->pluck("account_folder_id");
                $parent_relations = [];
                foreach ($account_folder_ids as $account_folder_id)
                {
                    $parent_relations[] = [
                        "account_folder_id" => $account_folder_id,
                        "subject_id" => $subject_id,
                        "site_id"=>$account->site_id,
                        "created_by" => $account->created_by,
                        "created_date" => date("Y-m-d H:i:s"),
                        "last_edit_date" => date("Y-m-d H:i:s"),
                        "last_edit_by" => $account->last_edit_by,
                    ];
                }
                DB::table('account_folder_subjects')->insert($parent_relations);
                DB::table('subjects')->where("is_topic", 0)->where("id", "!=", $subject_id)->where("account_id", $account->id)->update(["parent_id"=>$subject_id]);
            }

            //Topics

            $subjectIds = DB::table('subjects')->where("is_topic", 1)->where("account_id", $account->id)->pluck("id");
            if($subjectIds)
            {
                $new_category = [
                    "name"=> "Topic",
                    "account_id"=> $account->id,
                    "site_id"=>$account->site_id,
                    "created_by" => $account->created_by,
                    "created_date" => date("Y-m-d H:i:s"),
                    "last_edit_date" => date("Y-m-d H:i:s"),
                    "last_edit_by" => $account->created_by,
                    "is_topic" => 1
                ];
                $subject_id = DB::table('subjects')->insertGetId($new_category);
                $account_folder_ids = DB::table("account_folder_subjects")->whereIn("subject_id", $subjectIds)->groupBy("account_folder_id")->pluck("account_folder_id");
                $parent_relations = [];
                foreach ($account_folder_ids as $account_folder_id)
                {
                    $parent_relations[] = [
                        "account_folder_id" => $account_folder_id,
                        "subject_id" => $subject_id,
                        "site_id"=>$account->site_id,
                        "created_by" => $account->created_by,
                        "created_date" => date("Y-m-d H:i:s"),
                        "last_edit_date" => date("Y-m-d H:i:s"),
                        "last_edit_by" => $account->last_edit_by,
                    ];
                }
                DB::table('account_folder_subjects')->insert($parent_relations);
                DB::table('subjects')->where("is_topic", 1)->where("id", "!=", $subject_id)->where("account_id", $account->id)->update(["parent_id"=>$subject_id]);
            }
        }
        return response()->json(["status"=> true, "message" => "Successfully Added Parent relations!"]);
    }

    public function runQueryManuallyAfterMigration()
    {
        // $ids = SELECT s.id FROM subjects s WHERE (SELECT COUNT(*) FROM subjects WHERE parent_id = s.id) <=0 AND s.parent_id IS NULL AND (s.`name` = "Subject" OR s.`name` = 'Topic')
        // delete from subjects where id in ($ids)


        // $ids2 = SELECT af.account_folder_id FROM account_folders af JOIN account_folder_subjects afs ON afs.account_folder_id = af.account_folder_id LEFT JOIN account_folder_documents afd ON afd.account_folder_id = af.account_folder_id WHERE af.folder_type = 2 AND af.active = 1 AND afd.id IS NULL
        // Update account folders set active = 0 where account_folder_id in ($ids2)


        // DELETE FROM subjects WHERE `name` IN ("Subject", "Topic") AND parent_id IS NOT NULL AND parent_id != 0 AND parent_id != ''
    }

    public function correctAccountAnomalies()
    {
        $accounts = Account::all();
        foreach ($accounts as $account)
        {
            $account_id = $account->id;
            $invalidRecords = DB::Select("SELECT s.*, GROUP_CONCAT(sv.account_folder_id) AS account_folder_ids
                FROM subjects s INNER JOIN account_folder_subjects sv ON s.id=sv.subject_id
                INNER JOIN account_folders v ON sv.account_folder_id=v.account_folder_id
                WHERE  v.active=1 AND v.account_id=$account_id AND v.folder_type=2 AND v.site_id = 1
                AND s.account_id != $account_id
                GROUP BY s.id
                ORDER BY s.id ASC");
            $parents = DB::table('subjects')->whereNull("parent_id")->where("account_id", $account->id)->whereIn("name", ["Subject", "Topic"])->orderBy("created_date")->limit(2)->pluck("id","is_topic");
            $parent_topic_id = 0;
            $parent_subject_id = 0;
            foreach ($parents as $is_topic => $parent_id)
            {
                if($is_topic)
                {
                    $parent_topic_id = $parent_id;
                }
                else
                {
                    $parent_subject_id = $parent_id;
                }
            }
            foreach ($invalidRecords as $record)
            {
                $new_category = [
                    "name"=> $record->name,
                    "account_id"=> $account->id,
                    "site_id"=>$account->site_id,
                    "created_by" => $account->created_by,
                    "created_date" => date("Y-m-d H:i:s"),
                    "last_edit_date" => date("Y-m-d H:i:s"),
                    "last_edit_by" => $account->created_by,
                    "is_topic" => $record->is_topic,
                    "parent_id" => ($record->is_topic ? $parent_topic_id : $parent_subject_id)
                ];
                $subject_id = DB::table('subjects')->insertGetId($new_category);
                $account_folder_ids = explode(",", $record->account_folder_ids);
                $parent_relations = [];
                foreach ($account_folder_ids as $account_folder_id)
                {
                    $parent_relations[] = [
                        "account_folder_id" => $account_folder_id,
                        "subject_id" => $subject_id,
                        "site_id"=>$account->site_id,
                        "created_by" => $account->created_by,
                        "created_date" => date("Y-m-d H:i:s"),
                        "last_edit_date" => date("Y-m-d H:i:s"),
                        "last_edit_by" => $account->last_edit_by,
                    ];

                    $parent_relations[] = [
                        "account_folder_id" => $account_folder_id,
                        "subject_id" => $record->is_topic ? $parent_topic_id : $parent_subject_id,
                        "site_id"=>$account->site_id,
                        "created_by" => $account->created_by,
                        "created_date" => date("Y-m-d H:i:s"),
                        "last_edit_date" => date("Y-m-d H:i:s"),
                        "last_edit_by" => $account->last_edit_by,
                    ];
                }
                DB::table('account_folder_subjects')->insert($parent_relations);
            }
        }

    }
}
