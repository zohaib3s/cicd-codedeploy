<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class setDefaultAccounts extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "set:default_accounts";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will automatically set default accounts where missing";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $pdo = DB::connection()->getPdo();
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $pdo->query('CALL set_default_account()');
            // DB::select("CALL set_default_account()"); // Calling this was throwing mysql error: sending 4 packets instead of 1.  
            Log::info("set_default_account executed successfully");
            /**/
        } catch (Exception $e) {
            Log::info("An error occurred in ".__FILE__);
            Log::info($e->getMessage());
        }
    }
}