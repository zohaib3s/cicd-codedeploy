<?php

/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Models\Document;
use App\Models\AccountMetaData;
use Aws\S3\S3Client;
use Exception;
use Illuminate\Console\Command;
use App\Models\DocumentAudio;
use Aws\ElasticTranscoder\ElasticTranscoderClient;

require_once base_path('aws3/autoload.php');
use Illuminate\Support\Facades\Log;


class CraeteAudioAwsForTranscribe extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "create:audio_for_transcribe";
//    protected $createSRT = true;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will create mp3 files of saved videos and later we will transcribe them";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $accountMetaData = $this->getAccountMetaData();
        if (!empty($accountMetaData)) {
            foreach ($accountMetaData as $metaData) {
                $account_id = $metaData->account_id;
                $documents = $this->getDocuments($account_id);
                if (!empty($documents)) {
                    foreach ($documents as $document) {
                        $audioAvailable = $this->audioAvailable($document['id']);
                        if (!$audioAvailable) {
                            $this->createJobForAudio($document['url'],$document['id']);
                            sleep(5);
                        }
                    }
                }
            }
        }
    }

    /* Get Account metadata where transcribe is enable */

    public function getAccountMetaData() {
        try {
            $result = \DB::select("select accounts.id as account_id from accounts where transcribe_workspace_videos = 1 OR transcribe_huddle_videos = 1 OR transcribe_library_videos = 1");
            return $result;
        } catch (Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
            return false;
        }
    }

    /* Get Documents and document file data if transcoded */

    public function getDocuments($account_id) {
        try {
            $documents = Document::join('document_files as d_f', 'documents.id', '=', 'd_f.document_id')
                            ->select("documents.*", "d_f.document_id", "d_f.transcoding_status")
                            ->where("documents.subtitle_available", 0)
                            ->where("documents.allow_transcribe", 1)
                            ->where("documents.published", 1)
                            ->where("documents.account_id", $account_id)
                            ->where("documents.created_date", '>', '2021-01-01 00:00:00')//added safe check so old videos dont get submitted for transcription due to some error
                            ->where(function($q) {
                                $q->where(function($query){
                                        $query->where('d_f.transcoding_Status', 3);
                                    })
                                  ->orWhere(function($query) {
                                        $query->where('d_f.transcoding_Status', -1);
                                    });
                                })
                            ->orderBy('documents.id', 'desc')->get()->toArray();
            return $documents;
        } catch (Exception $e) {
            Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
            return false;
        }
    }

    public function audioAvailable($document_id) {
        try {
            $document_available = DocumentAudio::where('document_id', $document_id)->first();
            return $document_available;
        } catch (Exception $e) {
            Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
            return true;
        }
    }
    
    /*Create amazon job from video to audio*/
    public function createJobForAudio($url,$document_id) {
        $URLwithoutExt = preg_replace('/\.[^.\s]{3,4}$/', '', $url);
        try {
            $client = new ElasticTranscoderClient([
                'region' => 'us-east-1',
                'version' => 'latest',
                'credentials' => array(
                    'key' => config('s3.access_key_id'),
                    'secret' => config('s3.secret_access_key'),
                )
            ]);
            $result = $client->listPipelines(array());
            if (config('s3.bucket_name') == 'sibme-production')$pipelineId = $result['Pipelines'][1]['Id'];
            else $pipelineId = $result['Pipelines'][0]['Id'];
            $job = $client->createJob(array(
                'PipelineId' => $pipelineId,
                'Input' => array(
                    'Key' => $url,
                ),
                'Outputs' => array(
                    array(
                        'Key' => $URLwithoutExt.'.mp3',
                        'PresetId' => '1351620000001-300040',
                    ),
                ),
            ));
            $jobData = $job->get('Job');
            $jobId = $jobData['Id'];
            $this->saveDocumentAudio($jobId,$jobData['Status'],$URLwithoutExt,$document_id);
        } catch (Exception $e) {
             Log::error("Error while creating job for audio: " . $e->getMessage());
             return false;
        }
    }
    public function saveDocumentAudio($jobId,$job_status,$url,$document_id){
        try {
            $document_audio = new DocumentAudio();
            $document_audio->document_id = $document_id;
            $document_audio->url = $url.'.mp3';
            $document_audio->transcoding_status = $job_status;
            $document_audio->transcoding_job_id = $jobId;
            $document_audio->save();
            Log::info("Running audio aws Job for document id : " . $document_id. 'Job Id:'.$jobId);
        } catch (Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }

}
