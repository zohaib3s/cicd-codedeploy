<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;

use App\Services\S3Services;


/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class processAudioTranscoding extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:audio_transcoding";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will automatically process (direct published) elemental jobs to check the audio transcoding status";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $s3 = new S3Services(
                config('s3.amazon_base_url'), config('s3.bucket_name'), config('s3.access_key_id'), config('s3.secret_access_key')
            );
            $s3->processAudioJobs();
            \Log::info("All audio jobs are processed.");
            /**/
        } catch (Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }
}