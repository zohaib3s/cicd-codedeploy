<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;

use App\Services\S3Services;
use App\Models\AccountStudentPaymentsLogs;
use App\Models\AccountStudentPayments;
use App\Models\AccountUserCode;
use App\Models\Account;
use Illuminate\Support\Facades\DB;


/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class processDeactivateUsersJobs extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:DeactivateUser";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will automatically deactivate Users in Sibme and HMH who have expired Subscription Dates";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->deactivate_sibme_users();
            $this->deactivate_hmh_users();
        } catch (Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }
    
    function deactivate_sibme_users()
    {
        $accounts = Account::where(array('site_id' => '1'))->get(); 
        foreach($accounts as $account)
        {
           $account_user_codes = AccountUserCode::where(array('account_id' => $account->id))->get();
           foreach ($account_user_codes as $code)
           {
               if(!empty($code->user_id) && !empty($code->expiry_date) )
               {
                   if(date('Y-m-d') >= $code->expiry_date)
                   {
                      $this->deactivate_user_in_account($code->account_id,$code->user_id); 
                   }
               }
           }
           $account_users = AccountStudentPaymentsLogs::where(array('account_id' => $account->id))->get();
           foreach($account_users as $user)
           {
              $payment_duration = AccountStudentPayments::where(array('id' => $user->payment_id))->first();
            if($payment_duration)
            {
              if($payment_duration->type == 'Semester')
              {
                  $resulted_date = date('Y-m-d', strtotime($user->created_date. ' + 183 days'));
                  if(date('Y-m-d') >= $resulted_date )
                  {
                      $this->deactivate_user_in_account($account->id,$user->user_id);
                  }
              }
              else if($payment_duration->type == 'Year')
              {
                  $resulted_date = date('Y-m-d', strtotime($user->created_date. ' + 365 days'));
                  if(date('Y-m-d') >= $resulted_date)
                  {
                      $this->deactivate_user_in_account($account->id,$user->user_id);
                  }
              }
              
              else if($payment_duration->type == 'Two Year')
              {
                  $resulted_date = date('Y-m-d', strtotime($user->created_date. ' + 730 days'));
                  if(date('Y-m-d') >= $resulted_date)
                  {
                      $this->deactivate_user_in_account($account->id,$user->user_id);
                  }
              }
              
              else if($payment_duration->type == 'Month')
              {
                  $resulted_date = date('Y-m-d', strtotime($user->created_date. ' + 32 days'));
                  if(date('Y-m-d') >= $resulted_date)
                  {
                      $this->deactivate_user_in_account($account->id,$user->user_id);
                  }
              }
              
              else if($payment_duration->type == '18 Month')
              {
                  $resulted_date = date('Y-m-d', strtotime($user->created_date. ' + 558 days'));
                  if(date('Y-m-d') >= $resulted_date)
                  {
                      $this->deactivate_user_in_account($account->id,$user->user_id);
                  }
              }
              
              else if($payment_duration->type == '36 Month')
              {
                  $resulted_date = date('Y-m-d', strtotime($user->created_date. ' + 1116 days'));
                  if(date('Y-m-d') >= $resulted_date)
                  {
                      $this->deactivate_user_in_account($account->id,$user->user_id);
                  }
              }
              
            }
              
           }
        }
    }
    
    function deactivate_hmh_users()
    {
        $accounts = Account::where(array('site_id' => '2'))->get();
        foreach($accounts as $account)
        {
           $account_orders = DB::table('hmh_orders')->where(array('account_id' => $account->id))->get();
           foreach($account_orders as $order)
           {
              $order_users = DB::table('hmh_order_licenses')->where(['order_id' => $order->id ])->get();
              foreach ($order_users as $user)
              {
                 if(date('Y-m-d') >= date('Y-m-d', strtotime($order->Subscription_End)))
                 {
                     $this->deactivate_user_in_account($account->id,$user->user_id);
                 }
              }
              
           }
        }
    }
    
    function deactivate_user_in_account($account_id,$user_id)
    {
        DB::table('users_accounts')->where(['account_id' => $account_id , 'user_id' => $user_id])->update(['is_active' => '0' , 'status_type' => 'Inactive']);
    }
}