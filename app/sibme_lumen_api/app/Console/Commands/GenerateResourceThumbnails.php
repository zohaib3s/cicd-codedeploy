<?php

/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Document;
use App\Http\Controllers\ResourceController;
use Exception;
use Illuminate\Support\Facades\Log;


class GenerateResourceThumbnails extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "generate:thumbnails";
//    protected $createSRT = true;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will create the thumbails of resources using filestack api's";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $count5=0;
        $count2=0;
        $max = 100;
        $total = 112277;
        $pages = ceil($total / $max);
        for ($i = 1; $i < ($pages + 1); $i++) {
        $offset = (($i - 1)  * $max);
        $start = ($offset == 0 ? 0 : ($offset + 1));
        $documents = $this->getDocumnetResources($start,$max);
        if (!empty($documents)) {
            
            foreach ($documents as $document) {
                 $s3_dest_folder_path_only = $document['account_id']."/".$document['account_folder_id']."/" . date('Y') . "/" . date('m') . "/" . date('d');
               if($document['doc_type'] == 2){
                  $path_parts = pathinfo($document['original_file_name']);
                  $extension = isset($path_parts['extension']) && !empty($path_parts['extension']) ? $path_parts['extension'] : "";
                  $stack_array = explode('/',$document['stack_url']);
                  $filestack_id = end($stack_array);
                  $resourceCtrl = new ResourceController();
                  $s3_path = $resourceCtrl->generateResourceThumbnail(2, $filestack_id, $extension, "uploads/" .$s3_dest_folder_path_only);
                    if($s3_path)
                    {
                        Document::where('id',$document['document_id'])->update(['s3_thumbnail_url'=> "uploads/" .$s3_dest_folder_path_only.'/'.$filestack_id.'.jpg']);
                        echo "doc id: ".$document['document_id']. '</br>';
                        $count2++;
                    }
               }else if($document['doc_type'] == 5){
                  $url_title = $document['title'].'-'.time();
                  $resourceCtrl = new ResourceController();
                   $url = $document['url'];
                   if (!$this->isURLContainsHttp($document['url'])) {
                       $url = "https://" . $document['url'];
                    }
                  $s3_path = $resourceCtrl->generateResourceThumbnail(5, $url_title, null, "uploads/" .$s3_dest_folder_path_only,$url);
                    if($s3_path)
                    {
                        Document::where('id',$document['document_id'])->update(['s3_thumbnail_url'=> "uploads/" .$s3_dest_folder_path_only.'/'.$url_title.'.jpg']);
                        echo "doc id: ".$document['document_id']. '</br>'; 
                         $count5++;
                    }
               }
            }
        }
    }
            echo "count of type 5: ".$count5;
            echo "  count of type 2: ".$count2;
    }

    /* Get data from documents table for resources and url's*/

    public function getDocumnetResources($start,$max) {
        try {
            $result = Document::join('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                             ->whereNull('documents.s3_thumbnail_url')
//                             ->where('documents.doc_type',2)
//                             ->whereNotNull('documents.stack_url')
                             ->skip($start)
                             ->take($max)
                             ->where(function($q) {
                                $q->where(function($query){
                                        $query->where('documents.doc_type',2);
                                    })
                                  ->orWhere(function($query) {
                                        $query->where('documents.doc_type',5);
                                    });
                                })
                               ->where(function($q) {
                                $q->where(function($query){
                                        $query->where('documents.doc_type',5);
                                    })
                                  ->orWhere(function($query) {
                                        $query->whereNotNull('documents.stack_url');
                                    });
                                })
//                                ->orderBy('documents.id')
//                                        ->chunk(100, function($data) {
//                                    foreach ($data as $document) {
//                                        $url_title = $document['title'].'-'.time();
//                                        $s3_dest_folder_path_only = $document['account_id']."/".$document['account_folder_id']."/" . date('Y') . "/" . date('m') . "/" . date('d');
//                                      if($document['doc_type'] == 2){
//                                         $resourceCtrl = new ResourceController();
//                                         $s3_path = $resourceCtrl->generateResourceThumbnail(2, $url_title, null, "uploads/" .$s3_dest_folder_path_only);
//                                           if($s3_path)
//                                           {
//                                               Document::where('id',$document['document_id'])->update(['s3_thumbnail_url'=> "uploads/" .$s3_dest_folder_path_only.'/'.$url_title.'.jpg']);
//                                               echo "doc id type2 : ".$document['document_id']. '</br>';
//                                           }
//                                      }else if($document['doc_type'] == 5){
//                                          $resourceCtrl = new ResourceController();
//                                         $s3_path = $resourceCtrl->generateResourceThumbnail(5, $url_title, null, "uploads/" .$s3_dest_folder_path_only,$document['url']);
//                                           if($s3_path)
//                                           {
//                                               Document::where('id',$document['document_id'])->update(['s3_thumbnail_url'=> "uploads/" .$s3_dest_folder_path_only.'/'.$url_title.'.jpg']);
//                                               echo "doc id 5: ".$document['document_id']. '</br>'; 
//                                           }
//                                      }
//                                    }
//                                });
//                                ->limit(200)
                            ->get()->toArray();
            return $result;
        } catch (Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
            return false;
        }
    }
    
    public static function isURLContainsHttp($url) {
        $url = parse_url($url);
        return isset($url['scheme']) && in_array($url['scheme'], ['http', 'https']);
    }
 
    
}
