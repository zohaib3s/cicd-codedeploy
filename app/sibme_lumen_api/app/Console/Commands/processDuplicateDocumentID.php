<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Models\WebsocketLogs;
use Exception;
use Illuminate\Console\Command;

use App\Services\S3Services;


/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class processDuplicateDocumentID extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:DuplicateDocumentID";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will get documents ids that are duplicated in document files and will send them to given email daily.";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $videoController = app('App\Http\Controllers\VideoController');	
            $videoController->duplicateDocumentsIDsTracker();
            $this->deleteOldWebsocketLogs();
        } catch (Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }

    public function deleteOldWebsocketLogs()
    {
        //This function will delete websockets older then 15 days
        $date = new \DateTime();
        $date->modify('-15 days');
        $formatted = $date->format('Y-m-d H:i:s');
        WebsocketLogs::where('created_date', '<=', $formatted)->delete();
    }
}