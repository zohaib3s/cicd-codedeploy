<?php

/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Http\Controllers\VideoController;
use App\Models\Document;
use App\Services\HelperFunctions;
use Aws\S3\S3Client;
use Exception;
use Illuminate\Console\Command;
use App\Models\AccountFolder;
use App\Models\User;
use App\Models\Sites;
use App\Models\EmailUnsubscribers;
use App\Models\DocumentSubtitle;
use Illuminate\Support\Facades\DB;
use App\Models\JobQueue;
use App\Services\SendGridEmailManager;
use App\Services\S3Services;
use Aws\TranscribeService\TranscribeServiceClient;
use AwsTranscribeToWebVTT\Transcriber;
use Illuminate\Support\Facades\Log;

/*
 * Transcribe column "subtitle_available" values explained
 *
 * 0 => Subtitles not available (Not yet requested)
 * 1 => Subtitles Available
 * 2 => Subtitles Job currently in progress
 * 3 => Error while Submitting Transcribe Job
 * */

/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class processTranscribeJobsNew extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:transcribe_jobs_new";
    protected $createSRT = true;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will automatically process transcribe jobs to check the video transcribe/subtitles status";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        try {
            Log::info("Running Transcribe Job");            
            $documents = Document::leftJoin('account_folder_documents as afd', 'documents.id', '=', 'afd.document_id')
                    ->select("documents.*", "afd.account_folder_id", "afd.title")
                    ->where("documents.subtitle_available", 2)
                     ->where("documents.allow_transcribe", 1)                     
                    ->orderBy('documents.id', 'desc')->get();            
            foreach ($documents as $document) {
                $document_id = $document->id;
                Log::info("Running Transcribe Job for document id : " . $document_id);
                try {                    
                    $videoFilePath = $document ? pathinfo($document->url) : '';            
                        if ( $document &&  $document->subtitle_available == 2) {                            
                            $subtitle_vtt = self::getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($document->id) . "_transcribe.vtt");
                            $lines = file($subtitle_vtt);
                            $this->saveDocumentSubtitles($document_id,$document->created_by,$lines);
                            sleep(3);
                        }
                    } catch (\Exception $e) {
                    Log::info("Running Transcribe Job for document id : $document_id the traget vtt file is not exist.\n");
                    Document::where("id", $document->id)->update(["subtitle_available" => 0,"allow_transcribe" => 0]);
                    Log::error("Error reading Transcribe Job : \n" . $e->getMessage());
                    continue;
               }
                
            }
            $this->info("All transcribe jobs are processed.");
            Log::info("End Running Transcribe Job");
            /**/
        } catch (Exception $e) {
            $this->error("An error occurred while transcribe job processing");
            Log::error("Transcribe Job error: \n" . $e->getMessage());
            $this->error($e->getMessage());
        }
    }
            
    /*Save docuemnt subtitles into db*/
    public function saveDocumentSubtitles($doc_id,$user_id, $transcribe_data){
        $arr = array_values(array_filter($transcribe_data));
        $arr = array_slice($arr,3);
       
       // $allow_range = count($arr) > 2 ? true : false;
        for($i=0; $i<=count($arr); $i=$i+3){
         try {
             if(isset($arr[$i]) && $i<=count($arr) -1)
             {
                 $subtitle_added =  DocumentSubtitle::where(
                     array(
                         'document_id' => $doc_id ,
                         'subtitles' => ltrim($arr[$i+1],'- '),
                         'created_by' => $user_id,
                         'time_range' => str_replace(',', '.', $arr[$i])
                     ))->first();
                 if(!$subtitle_added)
                 {
                     $subtitle = new DocumentSubtitle();
                     $subtitle->document_id = $doc_id;
                     $subtitle->time_range = str_replace(',', '.', $arr[$i]);
                     $subtitle->subtitles = ltrim($arr[$i+1],'- ');
                     $subtitle->created_by = $user_id;
                     $subtitle->created_date = date("Y-m-d H:i:s");
                     $subtitle->last_edit_by = $user_id;
                     $subtitle->last_edit_date = date("Y-m-d H:i:s");
                     $subtitle->save();
                 }
                // if(!$allow_range) break;
             }

        } catch (Exception $e) {
            Log::error("An error occurred while saving subtitles into database: \n" . $e->getMessage());
            Log::error($e->getTraceAsString());
            $this->error("An error occurred while saving subtitles into database\n".$e->getMessage());
        }
	
        }
        Document::where("id", $doc_id)->update(["subtitle_available" => 1]);
        
       
    }

    public static function getSecureAmazonUrl($file, $name = null) {


        $aws_access_key_id = 'AKIAJ4ZWDR5X5JKB7CZQ';
        $aws_secret_key = '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';
        $aws_bucket = config('s3.bucket_name');
        $expires = '+240 minutes';

        // Create an Amazon S3 client object
        $client = new S3Client([
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret' => config('s3.secret_access_key'),
            )
        ]);

        try {
            $cmd = $client->getCommand('GetObject', [
                'Bucket' => $aws_bucket,
                'Key' => $file,
                'ResponseCacheControl' => 'No-cache',
                'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : basename($file))
            ]);
            $url = $client->createPresignedRequest($cmd, $expires)->getUri()->__toString();
            return $url;
        } catch (\Aws\S3\Exception\S3Exception $e) {
            echo $e->getMessage() . "\n";
        }
    }

}