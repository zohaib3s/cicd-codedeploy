<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Models\AccountFolder;
use App\Models\AccountFolderUser;
use App\Models\AccountFolderDocument;
use App\Services\HelperFunctions;
use Exception;
use Illuminate\Console\Command;



/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class AutoSubmitAssignments extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "submit:assignments";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will automatically submits not submitted assignments when their submission date passed";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $huddles = \DB::table("account_folders_meta_data")
                ->where("account_folders_meta_data.meta_data_name","folder_type")
                ->where("account_folders_meta_data.meta_data_value","3")->get();
            if(empty($huddles))
            {
                \Log::info("No huddle found");
            }
            $huddle_ids = [];
            foreach ($huddles as $huddle)
            {
                $submission_date = HelperFunctions::get_submission_date($huddle->account_folder_id, false, $huddle->site_id,true);
                if(strtotime($submission_date) < time())
                {
                    $assignments=AccountFolderDocument::where('account_folder_id',$huddle->account_folder_id)->first();
                    // var_dump($huddle->account_folder_id, $submission_date);
                    if(!empty($assignments)){
                        $huddle_ids[] = $huddle->account_folder_id;
                    }
                }
            }
            if(!empty($huddle_ids))
            {
                // var_dump(count($huddle_ids));
                AccountFolderUser::whereIn("account_folder_id",$huddle_ids)->update(["is_submitted"=>1,"last_submission"=>date("Y-m-d H:i:s")]);
            }
            \Log::info("All assignments with deadline passed marked as submitted.");
            /**/
        } catch (Exception $e) {
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }
}