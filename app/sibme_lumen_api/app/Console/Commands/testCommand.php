<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Exception;
use Illuminate\Console\Command;

/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class testCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "test_command";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron is for testing console";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            \Log::info("Test Command Ran Successfully.");
            exit;
            $cp = new \App\Services\CodeProfiler();
            $cp->start("getAssesseesList");
            $ass = \App\Models\AccountFolderUser::getAssesseesList(1, 482061, 66688, 200);
            $result_link = $cp->end();
            dd($result_link);
            \Log::info("Test Command Ran Successfully.");
            /**/
        } catch (Exception $e) {
            \Log::error("----------- An error occurred in ".__FILE__." ------------");
            \Log::error($e->getMessage());
            \Log::error($e->getTraceAsString());
        }
    }
}