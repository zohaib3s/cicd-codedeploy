<?php

/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Document;
use App\Models\AccountMetaData;
use Aws\S3\S3Client;
use Exception;
use App\Models\DocumentAudio;
use Aws\ElasticTranscoder\ElasticTranscoderClient;
use Illuminate\Support\Facades\Log;
use Aws\TranscribeService\TranscribeServiceClient;

class CheckAudioJobStatus extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "check:audio_job_status";
//    protected $createSRT = true;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will check and save the aws job status for audio to video and submit request for transcribe";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $documentAudio = $this->getDocumnetAudioData();

        if (!empty($documentAudio)) {
            foreach ($documentAudio as $audio) {
                $this->readJobForAudio($audio['id'],$audio['document_id'], $audio['transcoding_job_id']);
                sleep(5);
            }
        }
    }

    /* Get data from document_audio table data */

    public function getDocumnetAudioData() {
        try {
            $result = Document::join('document_audio as d_a', 'documents.id', '=', 'd_a.document_id')
                            ->select("d_a.id", "d_a.document_id", "documents.subtitle_available", "documents.allow_transcribe", "d_a.transcoding_job_id")
                            ->where("d_a.transcoding_status", "Submitted")
                            ->orWhere("d_a.transcoding_status", "Progressing")
                            ->orderBy('documents.id', 'desc')->get()->toArray();
            return $result;
        } catch (Exception $e) {
            Log::error("An error occurred while getting document audio data\n");
            Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
            return false;
        }
    }

    /* Read amazon transcoding job for audio */

    public function readJobForAudio($id,$doc_id, $jobId) {
        try {
            $client = new ElasticTranscoderClient([
                'region' => 'us-east-1',
                'version' => 'latest',
                'credentials' => array(
                    'key' => config('s3.access_key_id'),
                    'secret' => config('s3.secret_access_key'),
                )
            ]);
    
            $resposne = $client->readJob(array('Id' => $jobId));
            $jobData = $resposne->get('Job');
            if ($jobData['Status'] !== 'Progressing' && $jobData['Status'] !== 'Submitted') {
                if ($jobData['Status'] == 'Complete') {
                    $this->updateDocumentFile($id, $jobData['Status'], $jobData['Output']['Duration'], $jobData['Output']['FileSize']);
                    $this->submitTranscribeJob($doc_id,$jobData['Output']['Key']);
                } else {
                    $this->updateDocumentFile($id, $jobData['Status']);
                }
                Log::info("Reading audio job id : " . $jobId . 'Job status:' . $jobData['Status']);
            }
        } catch (Exception $e) {
            Log::error("An error occurred while reading a job status for mp3\n");
            Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
            return false;
        }
    }

    public function updateDocumentFile($id, $job_status, $duration = null, $size = null) {
        try {
            DocumentAudio::where("id", $id)->update([
                "transcoding_status" => $job_status,
                "duration" => $duration,
                "file_size" => $size
            ]);
            Log::info("saving audio Job data after reading, document_files.id : " . $id . 'Job status:' . $job_status);
        } catch (Exception $e) {
            Log::error("An error occurred while updating documents audio data\n");
            Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }
    
    /*Send request to amazon for transcribe audio*/
    public function submitTranscribeJob($doc_id,$url){
     try
        {
            $transcribeClient = new TranscribeServiceClient(array(
                'region' => 'us-east-1',
                'version' => 'latest',
                'credentials' => array(
                    'key' => config('s3.access_key_id'),
                    'secret'  => config('s3.secret_access_key'),
                )
            ));
           $res= $transcribeClient->startTranscriptionJob([
                "TranscriptionJobName" => $doc_id."_transcribe",
                "LanguageCode" => "en-US",
                /*"MediaSampleRateHertz" => 44100,*/ //Not Rquired
                "MediaFormat" => "mp3",
                "Media" => [
                    "MediaFileUri" => "s3://".config('s3.bucket_name')."/".$url
                ]]);
                var_dump($res);
            Document::where("id",$doc_id)->update(["subtitle_available"=>2]);
            Log::info("Transcribe job submitted successfully. document_id: ".$doc_id);
         }
        catch (\Exception $e)
        {
            Document::where("id",$doc_id)->where("subtitle_available","!=",2)->update(["subtitle_available"=>3]);
            Log::error("Error submitting video transcribe request for document = $doc_id\n");
            Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }   
    }

}
