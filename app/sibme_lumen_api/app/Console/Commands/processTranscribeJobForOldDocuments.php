<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Http\Controllers\VideoController;
use App\Models\Document;
use App\Services\HelperFunctions;
use Aws\S3\S3Client;
use Exception;
use Illuminate\Console\Command;

use App\Services\S3Services;
require_once base_path('aws3/autoload.php');
use Aws3\TranscribeService\TranscribeServiceClient;
use AwsTranscribeToWebVTT\Transcriber;
use Illuminate\Support\Facades\Log;

/**
 * Class autoSubmitAssignments
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class processTranscribeJobForOldDocuments extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:transcribe_job_all";

    protected $createSRT = true;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will submit transcribe job for all old documents";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info("Running Submit Transcribe Job for old documents");
        $documents = Document::where("subtitle_available",0)->where("doc_type",1)->where("published",1)->get();
        $update_ids = [];
        $update_ids2 = [];
        $transcribeClient = TranscribeServiceClient::factory(array(
            'region' => 'us-east-1',
            'version' => 'latest',
            'credentials' => array(
                'key' => config('s3.access_key_id'),
                'secret'  => config('s3.secret_access_key'),
            )
        ));
        foreach ($documents as $document)
        {
            try
            {
                $document_id = $document->id;
                $transcribeClient->startTranscriptionJob([
                    "TranscriptionJobName" => $document_id."_transcribe",
                    "LanguageCode" => "en-US",
                    "MediaSampleRateHertz" => 44100,
                    "MediaFormat" => "mp4",
                    "Media" => [
                        "MediaFileUri" => "s3://".config('s3.bucket_name')."/".$document->url
                    ]]);
                $update_ids[] = $document_id;
            } catch (Exception $e) {
                $update_ids2[] = $document_id;
                Log::error("An error occurred while submitting transcribe job for $document_id.\n");
                \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
            }
            if(!empty($update_ids))
            {
                Document::whereIn("id",$update_ids)->update(["subtitle_available"=>2]);
            }
            if(!empty($update_ids2))
            {
                Document::whereIn("id",$update_ids2)->update(["subtitle_available"=>3]);
            }
        }

        Log::info("End Running Submit Transcribe Job for old documents");
    }

}