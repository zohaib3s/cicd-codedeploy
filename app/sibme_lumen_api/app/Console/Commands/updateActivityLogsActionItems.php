<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;

use App\Api\Activities\GetActivities;
use Exception;
use App\Models\User;
use App\Models\Goal;
use App\Models\GoalItem;
use Illuminate\Console\Command;

/**
 * Class updateUserTypeForAnalytics
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class updateActivityLogsActionItems extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "update:activity_logs_action_items";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "This cron will update activity logs table according to action item expiry";


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $deadlineType =array("nextWeek","tommorow","yesterday");
         foreach ($deadlineType as $deadline){
             
            $actionItems = $this->getActionItems($deadline);
          if(!empty($actionItems)) {
              foreach($actionItems as $item){
                  $send_notification = true;
                  $send_email = true;
                   $user_result = User::select('lang')->where(array('id' => $item['created_by'] ))->first();
                  $deadlineDate = $item['deadline'];
                  if($deadline == 'nextWeek'){
                      $desc = "{ITEM_NAME} deadline is in 7 days on ".date("M d,Y", strtotime($deadlineDate));
                      $activity_type = Goal::activityTypes["action_item_deadline_7_days"];
                      $send_notification = false;
                  }else if ($deadline == 'tommorow'){
                      $desc = "{ITEM_NAME} deadline is tomorrow ".date("M d,Y", strtotime($deadlineDate));
                      $activity_type = Goal::activityTypes["action_item_deadline_tomorrow"];
                  }else if ($deadline == 'yesterday'){
                   $desc = "The deadline for {ITEM_NAME} has passed on ".date("M d,Y", strtotime($deadlineDate));
                   $activity_type = Goal::activityTypes["action_item_deadline_passed"];
                   $send_email = false;
                  }
                  $special = date("M d,Y", strtotime($deadlineDate)). "||" . GetActivities::_SpanishDate(strtotime($deadlineDate), 'spanish_day_month_year');
                  $activity_data = ["current_lang"=>$user_result->lang,"item_name"=>$item['title'],"goal_id"=>$item['goal_id'],"site_id"=>$item['site_id'], "desc"=>$desc, "account_folder_id"=>$item['account_folder_id'], "account_id"=>$item['account_id'], "user_id"=>$item['created_by'], "type"=>$activity_type,"send_notifications"=>$send_notification,"send_email"=>$send_email, "special"=>$special];
                  
                  app('App\Http\Controllers\GoalsController')->addRecentActivity($activity_data);
              }
          }
         }
         
       
        
    }
    /*Get all action items according to expiry date*/
     public function getActionItems($deadlineType) {
        try {
            $result=array();
            if($deadlineType == 'nextWeek'){
            $today =     date('Y-m-d');
            $next_date = date('Y-m-d', strtotime('+7 day'));
            $result = GoalItem::select('goal_items.*','g.account_folder_id')
                    ->join('goals as g', 'g.id', '=', 'goal_items.goal_id')
                    ->whereDate('deadline', '=', date('Y-m-d', strtotime('+7 day')))->where("g.is_active",1)->where("g.is_published",1)->get();
            }else if($deadlineType == 'tommorow'){
                 $result = GoalItem::select('goal_items.*','g.account_folder_id')
                         ->join('goals as g', 'g.id', '=', 'goal_items.goal_id')
                         ->whereDate('deadline', '=', date('Y-m-d', strtotime('+1 day')))->where("g.is_active",1)->where("g.is_published",1)->get();
            }else{
                $result = GoalItem::select('goal_items.*','g.account_folder_id')
                        ->join('goals as g', 'g.id', '=', 'goal_items.goal_id')
                         ->whereDate('deadline', '=', date('Y-m-d', strtotime('-1 day')))->where("g.is_active",1)->where("g.is_published",1)->get();
            }
            return $result;
        } catch (Exception $e) {
            $this->error("An error occurred while getting goal items.".$deadlineType);
            \Log::error("Stack Trace : " . $e->getMessage() . " \n " . $e->getTraceAsString());
        }
    }
}