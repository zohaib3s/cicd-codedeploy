<?php

namespace App\Console;

use App\Console\Commands\stopNoLongerActiveLiveStreams;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Laravelista\LumenVendorPublish\VendorPublishCommand::class,
        Commands\autoSubmitAssignments::class,
        Commands\processElementalJobs::class,
        Commands\CirqliveRecordings::class,
        Commands\processAudioTranscoding::class,
        // Commands\CreateAnalyticsCache::class,
        Commands\CreateAnalyticsFiltersCache::class,
        Commands\MillicastRecordings::class,
        Commands\processCardFailureJobs::class,
        Commands\processTranscribeJobs::class,
        Commands\processTranscribeJobsNew::class,
        Commands\CraeteAudioAwsForTranscribe::class,
        Commands\CheckAudioJobStatus::class,
        Commands\runTopicsMigration::class,
        Commands\updateUserTypeForAnalytics::class,
        Commands\updateActivityLogsActionItems::class,
        Commands\updateActivityLogsActionGoals::class,
        Commands\processDeactivateUsersJobs::class,
        Commands\ProcessScreenShareFiles::class,
        Commands\processScreenMergingJobs::class,
        Commands\processHmhOrderEmails::class,
        Commands\ProcessOfflineVideoMergingJobs::class,
        Commands\processFilestackPendingItems::class,
        stopNoLongerActiveLiveStreams::class,
        Commands\ProcessOfflineVideoMergingJobs::class,
        Commands\processReportExport::class,
        Commands\processAssesmentExport::class,
        Commands\GenerateResourceThumbnails::class,
        Commands\processDuplicateDocumentID::class,
        Commands\setDefaultAccounts::class,
        Commands\testCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command("submit:assignments")->everyMinute();
        $schedule->command("process:elemental_jobs")->everyMinute()->withoutOverlapping();
        $schedule->command("process:audio_transcoding")->everyMinute()->withoutOverlapping();
        $schedule->command("process:millicast_recordings")->everyMinute();
        $schedule->command("process:inactive_live_streams")->everyMinute();
        $schedule->command("process:screen_share_jobs")->everyMinute()->withoutOverlapping();
        $schedule->command("process:screen_merging_final")->everyMinute()->withoutOverlapping();
        $schedule->command("create:audio_for_transcribe")->cron('*/2 * * * *')->withoutOverlapping();
        $schedule->command("check:audio_job_status")->cron('*/2 * * * *')->withoutOverlapping();
        $schedule->command("process:filestack_items")->everyMinute()->withoutOverlapping();
        $schedule->command("process:offline_video_clip_merging_jobs")->everyMinute()->withoutOverlapping();

        $schedule->command("download:cirq-recordings")->everyThirtyMinutes();
        // $schedule->command("create:analytics-cache")->daily();
        // $schedule->command("create:analytics-filters-cache")->daily();
        $schedule->command("process:CardFailure")->daily();
        $schedule->command("process:transcribe_jobs")->cron('*/2 * * * *')->withoutOverlapping();
        //$schedule->command("process:transcribe_jobs_new")->daily();
        $schedule->command("update:activity_logs_action_items")->daily();
        $schedule->command("update:activity_logs_action_goals")->daily();
        $schedule->command("process:DeactivateUser")->daily();
        $schedule->command("process:DuplicateDocumentID")->daily();
        // $schedule->command("process:hmh_order_emails")->dailyAt('23:59');
        $schedule->command("process:hmh_order_emails")->daily();
        /*$schedule->command("generate:thumbnails")->daily();*/
        $schedule->command("set:default_accounts")->everyThirtyMinutes();
        // $schedule->command("set:default_accounts")->cron('*/2 * * * *'); // Run Every 2 Minutes.
        // $schedule->command("test_command")->everyMinute();
    }
}
