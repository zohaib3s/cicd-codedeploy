<?php
namespace Aws3\MediaConnect;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AWS MediaConnect** service.
 * @method \Aws3\Result addFlowOutputs(array $args = [])
 * @method \GuzzleHttp\Promise\Promise addFlowOutputsAsync(array $args = [])
 * @method \Aws3\Result createFlow(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createFlowAsync(array $args = [])
 * @method \Aws3\Result deleteFlow(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteFlowAsync(array $args = [])
 * @method \Aws3\Result describeFlow(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeFlowAsync(array $args = [])
 * @method \Aws3\Result grantFlowEntitlements(array $args = [])
 * @method \GuzzleHttp\Promise\Promise grantFlowEntitlementsAsync(array $args = [])
 * @method \Aws3\Result listEntitlements(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listEntitlementsAsync(array $args = [])
 * @method \Aws3\Result listFlows(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listFlowsAsync(array $args = [])
 * @method \Aws3\Result listTagsForResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listTagsForResourceAsync(array $args = [])
 * @method \Aws3\Result removeFlowOutput(array $args = [])
 * @method \GuzzleHttp\Promise\Promise removeFlowOutputAsync(array $args = [])
 * @method \Aws3\Result revokeFlowEntitlement(array $args = [])
 * @method \GuzzleHttp\Promise\Promise revokeFlowEntitlementAsync(array $args = [])
 * @method \Aws3\Result startFlow(array $args = [])
 * @method \GuzzleHttp\Promise\Promise startFlowAsync(array $args = [])
 * @method \Aws3\Result stopFlow(array $args = [])
 * @method \GuzzleHttp\Promise\Promise stopFlowAsync(array $args = [])
 * @method \Aws3\Result tagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise tagResourceAsync(array $args = [])
 * @method \Aws3\Result untagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise untagResourceAsync(array $args = [])
 * @method \Aws3\Result updateFlowEntitlement(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateFlowEntitlementAsync(array $args = [])
 * @method \Aws3\Result updateFlowOutput(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateFlowOutputAsync(array $args = [])
 * @method \Aws3\Result updateFlowSource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateFlowSourceAsync(array $args = [])
 */
class MediaConnectClient extends AwsClient {}
