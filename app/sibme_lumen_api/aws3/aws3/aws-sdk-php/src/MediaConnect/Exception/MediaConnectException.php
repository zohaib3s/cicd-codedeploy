<?php
namespace Aws3\MediaConnect\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS MediaConnect** service.
 */
class MediaConnectException extends AwsException {}
