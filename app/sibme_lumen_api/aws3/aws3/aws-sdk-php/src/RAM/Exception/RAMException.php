<?php
namespace Aws3\RAM\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Resource Access Manager** service.
 */
class RAMException extends AwsException {}
