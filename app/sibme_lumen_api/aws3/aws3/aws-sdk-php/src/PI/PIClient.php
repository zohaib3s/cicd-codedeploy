<?php
namespace Aws3\PI;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AWS Performance Insights** service.
 * @method \Aws3\Result describeDimensionKeys(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeDimensionKeysAsync(array $args = [])
 * @method \Aws3\Result getResourceMetrics(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getResourceMetricsAsync(array $args = [])
 */
class PIClient extends AwsClient {}
