<?php
namespace Aws3\PI\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Performance Insights** service.
 */
class PIException extends AwsException {}
