<?php
namespace Aws3\SageMaker\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon SageMaker Service** service.
 */
class SageMakerException extends AwsException {}
