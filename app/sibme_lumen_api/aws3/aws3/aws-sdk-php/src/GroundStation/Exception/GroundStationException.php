<?php
namespace Aws3\GroundStation\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Ground Station** service.
 */
class GroundStationException extends AwsException {}
