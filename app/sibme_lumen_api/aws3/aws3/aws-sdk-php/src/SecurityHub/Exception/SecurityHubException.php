<?php
namespace Aws3\SecurityHub\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS SecurityHub** service.
 */
class SecurityHubException extends AwsException {}
