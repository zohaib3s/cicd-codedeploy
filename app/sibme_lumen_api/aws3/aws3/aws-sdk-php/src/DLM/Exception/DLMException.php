<?php
namespace Aws3\DLM\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Data Lifecycle Manager** service.
 */
class DLMException extends AwsException {}
