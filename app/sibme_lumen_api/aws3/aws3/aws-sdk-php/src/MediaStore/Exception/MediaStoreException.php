<?php
namespace Aws3\MediaStore\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Elemental MediaStore** service.
 */
class MediaStoreException extends AwsException {}
