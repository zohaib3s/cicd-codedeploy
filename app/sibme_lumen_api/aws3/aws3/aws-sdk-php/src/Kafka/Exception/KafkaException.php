<?php
namespace Aws3\Kafka\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Managed Streaming for Kafka** service.
 */
class KafkaException extends AwsException {}
