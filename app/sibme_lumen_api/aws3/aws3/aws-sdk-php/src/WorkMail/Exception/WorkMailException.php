<?php
namespace Aws3\WorkMail\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon WorkMail** service.
 */
class WorkMailException extends AwsException {}
