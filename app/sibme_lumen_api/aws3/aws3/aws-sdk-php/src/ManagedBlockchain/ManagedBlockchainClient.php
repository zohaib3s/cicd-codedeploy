<?php
namespace Aws3\ManagedBlockchain;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon Managed Blockchain** service.
 * @method \Aws3\Result createMember(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createMemberAsync(array $args = [])
 * @method \Aws3\Result createNetwork(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createNetworkAsync(array $args = [])
 * @method \Aws3\Result createNode(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createNodeAsync(array $args = [])
 * @method \Aws3\Result createProposal(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createProposalAsync(array $args = [])
 * @method \Aws3\Result deleteMember(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteMemberAsync(array $args = [])
 * @method \Aws3\Result deleteNode(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteNodeAsync(array $args = [])
 * @method \Aws3\Result getMember(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getMemberAsync(array $args = [])
 * @method \Aws3\Result getNetwork(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getNetworkAsync(array $args = [])
 * @method \Aws3\Result getNode(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getNodeAsync(array $args = [])
 * @method \Aws3\Result getProposal(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getProposalAsync(array $args = [])
 * @method \Aws3\Result listInvitations(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listInvitationsAsync(array $args = [])
 * @method \Aws3\Result listMembers(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listMembersAsync(array $args = [])
 * @method \Aws3\Result listNetworks(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listNetworksAsync(array $args = [])
 * @method \Aws3\Result listNodes(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listNodesAsync(array $args = [])
 * @method \Aws3\Result listProposalVotes(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listProposalVotesAsync(array $args = [])
 * @method \Aws3\Result listProposals(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listProposalsAsync(array $args = [])
 * @method \Aws3\Result rejectInvitation(array $args = [])
 * @method \GuzzleHttp\Promise\Promise rejectInvitationAsync(array $args = [])
 * @method \Aws3\Result voteOnProposal(array $args = [])
 * @method \GuzzleHttp\Promise\Promise voteOnProposalAsync(array $args = [])
 */
class ManagedBlockchainClient extends AwsClient {}
