<?php
namespace Aws3\MediaPackageVod;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AWS Elemental MediaPackage VOD** service.
 * @method \Aws3\Result createAsset(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createAssetAsync(array $args = [])
 * @method \Aws3\Result createPackagingConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createPackagingConfigurationAsync(array $args = [])
 * @method \Aws3\Result createPackagingGroup(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createPackagingGroupAsync(array $args = [])
 * @method \Aws3\Result deleteAsset(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteAssetAsync(array $args = [])
 * @method \Aws3\Result deletePackagingConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deletePackagingConfigurationAsync(array $args = [])
 * @method \Aws3\Result deletePackagingGroup(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deletePackagingGroupAsync(array $args = [])
 * @method \Aws3\Result describeAsset(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeAssetAsync(array $args = [])
 * @method \Aws3\Result describePackagingConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describePackagingConfigurationAsync(array $args = [])
 * @method \Aws3\Result describePackagingGroup(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describePackagingGroupAsync(array $args = [])
 * @method \Aws3\Result listAssets(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listAssetsAsync(array $args = [])
 * @method \Aws3\Result listPackagingConfigurations(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listPackagingConfigurationsAsync(array $args = [])
 * @method \Aws3\Result listPackagingGroups(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listPackagingGroupsAsync(array $args = [])
 */
class MediaPackageVodClient extends AwsClient {}
