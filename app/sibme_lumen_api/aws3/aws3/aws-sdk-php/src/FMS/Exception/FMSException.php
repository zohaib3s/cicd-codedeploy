<?php
namespace Aws3\FMS\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Firewall Management Service** service.
 */
class FMSException extends AwsException {}
