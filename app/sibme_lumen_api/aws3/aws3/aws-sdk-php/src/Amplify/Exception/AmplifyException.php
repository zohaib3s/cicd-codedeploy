<?php
namespace Aws3\Amplify\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Amplify** service.
 */
class AmplifyException extends AwsException {}
