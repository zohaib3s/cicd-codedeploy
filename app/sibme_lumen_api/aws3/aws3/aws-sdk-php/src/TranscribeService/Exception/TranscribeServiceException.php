<?php
namespace Aws3\TranscribeService\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Transcribe Service** service.
 */
class TranscribeServiceException extends AwsException {}
