<?php
namespace Aws3\TranscribeService;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon Transcribe Service** service.
 * @method \Aws3\Result createVocabulary(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createVocabularyAsync(array $args = [])
 * @method \Aws3\Result deleteTranscriptionJob(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteTranscriptionJobAsync(array $args = [])
 * @method \Aws3\Result deleteVocabulary(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteVocabularyAsync(array $args = [])
 * @method \Aws3\Result getTranscriptionJob(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getTranscriptionJobAsync(array $args = [])
 * @method \Aws3\Result getVocabulary(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getVocabularyAsync(array $args = [])
 * @method \Aws3\Result listTranscriptionJobs(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listTranscriptionJobsAsync(array $args = [])
 * @method \Aws3\Result listVocabularies(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listVocabulariesAsync(array $args = [])
 * @method \Aws3\Result startTranscriptionJob(array $args = [])
 * @method \GuzzleHttp\Promise\Promise startTranscriptionJobAsync(array $args = [])
 * @method \Aws3\Result updateVocabulary(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateVocabularyAsync(array $args = [])
 */
class TranscribeServiceClient extends AwsClient {}
