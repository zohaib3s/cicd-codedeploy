<?php
namespace Aws3\ApplicationInsights\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon CloudWatch Application Insights** service.
 */
class ApplicationInsightsException extends AwsException {}
