<?php
namespace Aws3\ApplicationInsights;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon CloudWatch Application Insights** service.
 * @method \Aws3\Result createApplication(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createApplicationAsync(array $args = [])
 * @method \Aws3\Result createComponent(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createComponentAsync(array $args = [])
 * @method \Aws3\Result deleteApplication(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteApplicationAsync(array $args = [])
 * @method \Aws3\Result deleteComponent(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteComponentAsync(array $args = [])
 * @method \Aws3\Result describeApplication(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeApplicationAsync(array $args = [])
 * @method \Aws3\Result describeComponent(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeComponentAsync(array $args = [])
 * @method \Aws3\Result describeComponentConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeComponentConfigurationAsync(array $args = [])
 * @method \Aws3\Result describeComponentConfigurationRecommendation(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeComponentConfigurationRecommendationAsync(array $args = [])
 * @method \Aws3\Result describeObservation(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeObservationAsync(array $args = [])
 * @method \Aws3\Result describeProblem(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeProblemAsync(array $args = [])
 * @method \Aws3\Result describeProblemObservations(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeProblemObservationsAsync(array $args = [])
 * @method \Aws3\Result listApplications(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listApplicationsAsync(array $args = [])
 * @method \Aws3\Result listComponents(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listComponentsAsync(array $args = [])
 * @method \Aws3\Result listProblems(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listProblemsAsync(array $args = [])
 * @method \Aws3\Result updateApplication(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateApplicationAsync(array $args = [])
 * @method \Aws3\Result updateComponent(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateComponentAsync(array $args = [])
 * @method \Aws3\Result updateComponentConfiguration(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateComponentConfigurationAsync(array $args = [])
 */
class ApplicationInsightsClient extends AwsClient {}
