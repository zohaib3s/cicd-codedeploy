<?php
namespace Aws3\KinesisVideoArchivedMedia\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Kinesis Video Streams Archived Media** service.
 */
class KinesisVideoArchivedMediaException extends AwsException {}
