<?php
namespace Aws3\ServiceQuotas\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Service Quotas** service.
 */
class ServiceQuotasException extends AwsException {}
