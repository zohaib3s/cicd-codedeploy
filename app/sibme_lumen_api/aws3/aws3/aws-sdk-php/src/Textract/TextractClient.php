<?php
namespace Aws3\Textract;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon Textract** service.
 * @method \Aws3\Result analyzeDocument(array $args = [])
 * @method \GuzzleHttp\Promise\Promise analyzeDocumentAsync(array $args = [])
 * @method \Aws3\Result detectDocumentText(array $args = [])
 * @method \GuzzleHttp\Promise\Promise detectDocumentTextAsync(array $args = [])
 * @method \Aws3\Result getDocumentAnalysis(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getDocumentAnalysisAsync(array $args = [])
 * @method \Aws3\Result getDocumentTextDetection(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getDocumentTextDetectionAsync(array $args = [])
 * @method \Aws3\Result startDocumentAnalysis(array $args = [])
 * @method \GuzzleHttp\Promise\Promise startDocumentAnalysisAsync(array $args = [])
 * @method \Aws3\Result startDocumentTextDetection(array $args = [])
 * @method \GuzzleHttp\Promise\Promise startDocumentTextDetectionAsync(array $args = [])
 */
class TextractClient extends AwsClient {}
