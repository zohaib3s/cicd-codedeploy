<?php
namespace Aws3\ApiGatewayV2\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AmazonApiGatewayV2** service.
 */
class ApiGatewayV2Exception extends AwsException {}
