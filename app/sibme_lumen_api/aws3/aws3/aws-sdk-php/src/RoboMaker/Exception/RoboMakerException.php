<?php
namespace Aws3\RoboMaker\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS RoboMaker** service.
 */
class RoboMakerException extends AwsException {}
