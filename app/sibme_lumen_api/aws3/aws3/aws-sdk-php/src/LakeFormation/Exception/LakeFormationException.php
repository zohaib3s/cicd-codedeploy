<?php
namespace Aws3\LakeFormation\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Lake Formation** service.
 */
class LakeFormationException extends AwsException {}
