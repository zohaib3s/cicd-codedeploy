<?php
namespace Aws3\Greengrass\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Greengrass** service.
 */
class GreengrassException extends AwsException {}
