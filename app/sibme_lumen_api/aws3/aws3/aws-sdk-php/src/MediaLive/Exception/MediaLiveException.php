<?php
namespace Aws3\MediaLive\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Elemental MediaLive** service.
 */
class MediaLiveException extends AwsException {}
