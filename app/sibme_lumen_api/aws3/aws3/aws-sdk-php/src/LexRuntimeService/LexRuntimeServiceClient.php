<?php
namespace Aws3\LexRuntimeService;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon Lex Runtime Service** service.
 * @method \Aws3\Result deleteSession(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteSessionAsync(array $args = [])
 * @method \Aws3\Result getSession(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getSessionAsync(array $args = [])
 * @method \Aws3\Result postContent(array $args = [])
 * @method \GuzzleHttp\Promise\Promise postContentAsync(array $args = [])
 * @method \Aws3\Result postText(array $args = [])
 * @method \GuzzleHttp\Promise\Promise postTextAsync(array $args = [])
 * @method \Aws3\Result putSession(array $args = [])
 * @method \GuzzleHttp\Promise\Promise putSessionAsync(array $args = [])
 */
class LexRuntimeServiceClient extends AwsClient {}
