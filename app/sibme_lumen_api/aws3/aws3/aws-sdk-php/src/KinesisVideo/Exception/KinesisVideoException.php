<?php
namespace Aws3\KinesisVideo\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Kinesis Video Streams** service.
 */
class KinesisVideoException extends AwsException {}
