<?php
namespace Aws3\SecretsManager;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AWS Secrets Manager** service.
 * @method \Aws3\Result cancelRotateSecret(array $args = [])
 * @method \GuzzleHttp\Promise\Promise cancelRotateSecretAsync(array $args = [])
 * @method \Aws3\Result createSecret(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createSecretAsync(array $args = [])
 * @method \Aws3\Result deleteResourcePolicy(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteResourcePolicyAsync(array $args = [])
 * @method \Aws3\Result deleteSecret(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteSecretAsync(array $args = [])
 * @method \Aws3\Result describeSecret(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeSecretAsync(array $args = [])
 * @method \Aws3\Result getRandomPassword(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getRandomPasswordAsync(array $args = [])
 * @method \Aws3\Result getResourcePolicy(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getResourcePolicyAsync(array $args = [])
 * @method \Aws3\Result getSecretValue(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getSecretValueAsync(array $args = [])
 * @method \Aws3\Result listSecretVersionIds(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listSecretVersionIdsAsync(array $args = [])
 * @method \Aws3\Result listSecrets(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listSecretsAsync(array $args = [])
 * @method \Aws3\Result putResourcePolicy(array $args = [])
 * @method \GuzzleHttp\Promise\Promise putResourcePolicyAsync(array $args = [])
 * @method \Aws3\Result putSecretValue(array $args = [])
 * @method \GuzzleHttp\Promise\Promise putSecretValueAsync(array $args = [])
 * @method \Aws3\Result restoreSecret(array $args = [])
 * @method \GuzzleHttp\Promise\Promise restoreSecretAsync(array $args = [])
 * @method \Aws3\Result rotateSecret(array $args = [])
 * @method \GuzzleHttp\Promise\Promise rotateSecretAsync(array $args = [])
 * @method \Aws3\Result tagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise tagResourceAsync(array $args = [])
 * @method \Aws3\Result untagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise untagResourceAsync(array $args = [])
 * @method \Aws3\Result updateSecret(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateSecretAsync(array $args = [])
 * @method \Aws3\Result updateSecretVersionStage(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateSecretVersionStageAsync(array $args = [])
 */
class SecretsManagerClient extends AwsClient {}
