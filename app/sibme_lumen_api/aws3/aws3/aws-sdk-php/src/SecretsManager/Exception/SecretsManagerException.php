<?php
namespace Aws3\SecretsManager\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Secrets Manager** service.
 */
class SecretsManagerException extends AwsException {}
