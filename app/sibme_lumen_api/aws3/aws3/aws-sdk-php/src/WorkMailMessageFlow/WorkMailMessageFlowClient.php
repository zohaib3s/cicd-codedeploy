<?php
namespace Aws3\WorkMailMessageFlow;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon WorkMail Message Flow** service.
 * @method \Aws3\Result getRawMessageContent(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getRawMessageContentAsync(array $args = [])
 */
class WorkMailMessageFlowClient extends AwsClient {}
