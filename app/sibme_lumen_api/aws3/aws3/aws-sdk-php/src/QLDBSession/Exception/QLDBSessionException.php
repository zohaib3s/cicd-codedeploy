<?php
namespace Aws3\QLDBSession\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon QLDB Session** service.
 */
class QLDBSessionException extends AwsException {}
