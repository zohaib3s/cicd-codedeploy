<?php
namespace Aws3\QLDBSession;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon QLDB Session** service.
 * @method \Aws3\Result sendCommand(array $args = [])
 * @method \GuzzleHttp\Promise\Promise sendCommandAsync(array $args = [])
 */
class QLDBSessionClient extends AwsClient {}
