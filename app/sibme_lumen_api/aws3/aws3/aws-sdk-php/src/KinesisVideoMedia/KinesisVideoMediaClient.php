<?php
namespace Aws3\KinesisVideoMedia;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon Kinesis Video Streams Media** service.
 * @method \Aws3\Result getMedia(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getMediaAsync(array $args = [])
 */
class KinesisVideoMediaClient extends AwsClient {}
