<?php
namespace Aws3\KinesisVideoMedia\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Kinesis Video Streams Media** service.
 */
class KinesisVideoMediaException extends AwsException {}
