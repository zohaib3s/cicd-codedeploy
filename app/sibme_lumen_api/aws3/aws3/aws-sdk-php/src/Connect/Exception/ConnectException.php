<?php
namespace Aws3\Connect\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Connect Service** service.
 */
class ConnectException extends AwsException {}
