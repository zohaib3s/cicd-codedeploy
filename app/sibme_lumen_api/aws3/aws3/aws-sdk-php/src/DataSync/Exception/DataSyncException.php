<?php
namespace Aws3\DataSync\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS DataSync** service.
 */
class DataSyncException extends AwsException {}
