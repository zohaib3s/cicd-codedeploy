<?php
namespace Aws3\CodeBuild;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **AWS CodeBuild** service.
 * @method \Aws3\Result batchDeleteBuilds(array $args = [])
 * @method \GuzzleHttp\Promise\Promise batchDeleteBuildsAsync(array $args = [])
 * @method \Aws3\Result batchGetBuilds(array $args = [])
 * @method \GuzzleHttp\Promise\Promise batchGetBuildsAsync(array $args = [])
 * @method \Aws3\Result batchGetProjects(array $args = [])
 * @method \GuzzleHttp\Promise\Promise batchGetProjectsAsync(array $args = [])
 * @method \Aws3\Result createProject(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createProjectAsync(array $args = [])
 * @method \Aws3\Result createWebhook(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createWebhookAsync(array $args = [])
 * @method \Aws3\Result deleteProject(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteProjectAsync(array $args = [])
 * @method \Aws3\Result deleteSourceCredentials(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteSourceCredentialsAsync(array $args = [])
 * @method \Aws3\Result deleteWebhook(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteWebhookAsync(array $args = [])
 * @method \Aws3\Result importSourceCredentials(array $args = [])
 * @method \GuzzleHttp\Promise\Promise importSourceCredentialsAsync(array $args = [])
 * @method \Aws3\Result invalidateProjectCache(array $args = [])
 * @method \GuzzleHttp\Promise\Promise invalidateProjectCacheAsync(array $args = [])
 * @method \Aws3\Result listBuilds(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listBuildsAsync(array $args = [])
 * @method \Aws3\Result listBuildsForProject(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listBuildsForProjectAsync(array $args = [])
 * @method \Aws3\Result listCuratedEnvironmentImages(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listCuratedEnvironmentImagesAsync(array $args = [])
 * @method \Aws3\Result listProjects(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listProjectsAsync(array $args = [])
 * @method \Aws3\Result listSourceCredentials(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listSourceCredentialsAsync(array $args = [])
 * @method \Aws3\Result startBuild(array $args = [])
 * @method \GuzzleHttp\Promise\Promise startBuildAsync(array $args = [])
 * @method \Aws3\Result stopBuild(array $args = [])
 * @method \GuzzleHttp\Promise\Promise stopBuildAsync(array $args = [])
 * @method \Aws3\Result updateProject(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateProjectAsync(array $args = [])
 * @method \Aws3\Result updateWebhook(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateWebhookAsync(array $args = [])
 */
class CodeBuildClient extends AwsClient {}
