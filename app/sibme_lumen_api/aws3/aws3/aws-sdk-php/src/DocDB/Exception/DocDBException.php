<?php
namespace Aws3\DocDB\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon DocumentDB with MongoDB compatibility** service.
 */
class DocDBException extends AwsException {}
