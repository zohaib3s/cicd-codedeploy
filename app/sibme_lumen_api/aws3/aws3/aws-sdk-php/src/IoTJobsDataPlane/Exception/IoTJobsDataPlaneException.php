<?php
namespace Aws3\IoTJobsDataPlane\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS IoT Jobs Data Plane** service.
 */
class IoTJobsDataPlaneException extends AwsException {}
