<?php
namespace Aws3\Backup\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Backup** service.
 */
class BackupException extends AwsException {}
