<?php
namespace Aws3\AppSync\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS AppSync** service.
 */
class AppSyncException extends AwsException {}
