<?php
namespace Aws3\EndpointDiscovery\Exception;

use Aws3\HasMonitoringEventsTrait;
use Aws3\MonitoringEventsInterface;

/**
 * Represents an error interacting with configuration for endpoint discovery
 */
class ConfigurationException extends \RuntimeException implements
    MonitoringEventsInterface
{
    use HasMonitoringEventsTrait;
}
