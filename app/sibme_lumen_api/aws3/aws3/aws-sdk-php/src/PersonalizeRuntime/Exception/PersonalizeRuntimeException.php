<?php
namespace Aws3\PersonalizeRuntime\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Personalize Runtime** service.
 */
class PersonalizeRuntimeException extends AwsException {}
