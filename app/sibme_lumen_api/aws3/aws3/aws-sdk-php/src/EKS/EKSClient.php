<?php
namespace Aws3\EKS;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon Elastic Container Service for Kubernetes** service.
 * @method \Aws3\Result createCluster(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createClusterAsync(array $args = [])
 * @method \Aws3\Result deleteCluster(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteClusterAsync(array $args = [])
 * @method \Aws3\Result describeCluster(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeClusterAsync(array $args = [])
 * @method \Aws3\Result describeUpdate(array $args = [])
 * @method \GuzzleHttp\Promise\Promise describeUpdateAsync(array $args = [])
 * @method \Aws3\Result listClusters(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listClustersAsync(array $args = [])
 * @method \Aws3\Result listTagsForResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listTagsForResourceAsync(array $args = [])
 * @method \Aws3\Result listUpdates(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listUpdatesAsync(array $args = [])
 * @method \Aws3\Result tagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise tagResourceAsync(array $args = [])
 * @method \Aws3\Result untagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise untagResourceAsync(array $args = [])
 * @method \Aws3\Result updateClusterConfig(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateClusterConfigAsync(array $args = [])
 * @method \Aws3\Result updateClusterVersion(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateClusterVersionAsync(array $args = [])
 */
class EKSClient extends AwsClient {}
