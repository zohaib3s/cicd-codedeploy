<?php
namespace Aws3\EKS\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Elastic Container Service for Kubernetes** service.
 */
class EKSException extends AwsException {}
