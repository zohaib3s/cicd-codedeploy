<?php
namespace Aws3\GuardDuty;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon GuardDuty** service.
 * @method \Aws3\Result acceptInvitation(array $args = [])
 * @method \GuzzleHttp\Promise\Promise acceptInvitationAsync(array $args = [])
 * @method \Aws3\Result archiveFindings(array $args = [])
 * @method \GuzzleHttp\Promise\Promise archiveFindingsAsync(array $args = [])
 * @method \Aws3\Result createDetector(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createDetectorAsync(array $args = [])
 * @method \Aws3\Result createFilter(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createFilterAsync(array $args = [])
 * @method \Aws3\Result createIPSet(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createIPSetAsync(array $args = [])
 * @method \Aws3\Result createMembers(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createMembersAsync(array $args = [])
 * @method \Aws3\Result createSampleFindings(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createSampleFindingsAsync(array $args = [])
 * @method \Aws3\Result createThreatIntelSet(array $args = [])
 * @method \GuzzleHttp\Promise\Promise createThreatIntelSetAsync(array $args = [])
 * @method \Aws3\Result declineInvitations(array $args = [])
 * @method \GuzzleHttp\Promise\Promise declineInvitationsAsync(array $args = [])
 * @method \Aws3\Result deleteDetector(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteDetectorAsync(array $args = [])
 * @method \Aws3\Result deleteFilter(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteFilterAsync(array $args = [])
 * @method \Aws3\Result deleteIPSet(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteIPSetAsync(array $args = [])
 * @method \Aws3\Result deleteInvitations(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteInvitationsAsync(array $args = [])
 * @method \Aws3\Result deleteMembers(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteMembersAsync(array $args = [])
 * @method \Aws3\Result deleteThreatIntelSet(array $args = [])
 * @method \GuzzleHttp\Promise\Promise deleteThreatIntelSetAsync(array $args = [])
 * @method \Aws3\Result disassociateFromMasterAccount(array $args = [])
 * @method \GuzzleHttp\Promise\Promise disassociateFromMasterAccountAsync(array $args = [])
 * @method \Aws3\Result disassociateMembers(array $args = [])
 * @method \GuzzleHttp\Promise\Promise disassociateMembersAsync(array $args = [])
 * @method \Aws3\Result getDetector(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getDetectorAsync(array $args = [])
 * @method \Aws3\Result getFilter(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getFilterAsync(array $args = [])
 * @method \Aws3\Result getFindings(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getFindingsAsync(array $args = [])
 * @method \Aws3\Result getFindingsStatistics(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getFindingsStatisticsAsync(array $args = [])
 * @method \Aws3\Result getIPSet(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getIPSetAsync(array $args = [])
 * @method \Aws3\Result getInvitationsCount(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getInvitationsCountAsync(array $args = [])
 * @method \Aws3\Result getMasterAccount(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getMasterAccountAsync(array $args = [])
 * @method \Aws3\Result getMembers(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getMembersAsync(array $args = [])
 * @method \Aws3\Result getThreatIntelSet(array $args = [])
 * @method \GuzzleHttp\Promise\Promise getThreatIntelSetAsync(array $args = [])
 * @method \Aws3\Result inviteMembers(array $args = [])
 * @method \GuzzleHttp\Promise\Promise inviteMembersAsync(array $args = [])
 * @method \Aws3\Result listDetectors(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listDetectorsAsync(array $args = [])
 * @method \Aws3\Result listFilters(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listFiltersAsync(array $args = [])
 * @method \Aws3\Result listFindings(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listFindingsAsync(array $args = [])
 * @method \Aws3\Result listIPSets(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listIPSetsAsync(array $args = [])
 * @method \Aws3\Result listInvitations(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listInvitationsAsync(array $args = [])
 * @method \Aws3\Result listMembers(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listMembersAsync(array $args = [])
 * @method \Aws3\Result listTagsForResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listTagsForResourceAsync(array $args = [])
 * @method \Aws3\Result listThreatIntelSets(array $args = [])
 * @method \GuzzleHttp\Promise\Promise listThreatIntelSetsAsync(array $args = [])
 * @method \Aws3\Result startMonitoringMembers(array $args = [])
 * @method \GuzzleHttp\Promise\Promise startMonitoringMembersAsync(array $args = [])
 * @method \Aws3\Result stopMonitoringMembers(array $args = [])
 * @method \GuzzleHttp\Promise\Promise stopMonitoringMembersAsync(array $args = [])
 * @method \Aws3\Result tagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise tagResourceAsync(array $args = [])
 * @method \Aws3\Result unarchiveFindings(array $args = [])
 * @method \GuzzleHttp\Promise\Promise unarchiveFindingsAsync(array $args = [])
 * @method \Aws3\Result untagResource(array $args = [])
 * @method \GuzzleHttp\Promise\Promise untagResourceAsync(array $args = [])
 * @method \Aws3\Result updateDetector(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateDetectorAsync(array $args = [])
 * @method \Aws3\Result updateFilter(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateFilterAsync(array $args = [])
 * @method \Aws3\Result updateFindingsFeedback(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateFindingsFeedbackAsync(array $args = [])
 * @method \Aws3\Result updateIPSet(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateIPSetAsync(array $args = [])
 * @method \Aws3\Result updateThreatIntelSet(array $args = [])
 * @method \GuzzleHttp\Promise\Promise updateThreatIntelSetAsync(array $args = [])
 */
class GuardDutyClient extends AwsClient {}
