<?php
namespace Aws3\RDSDataService\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS RDS DataService** service.
 */
class RDSDataServiceException extends AwsException {}
