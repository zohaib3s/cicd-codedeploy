<?php
namespace Aws3\IoTThingsGraph\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS IoT Things Graph** service.
 */
class IoTThingsGraphException extends AwsException {}
