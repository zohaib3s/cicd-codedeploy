<?php
namespace Aws3\QuickSight\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon QuickSight** service.
 */
class QuickSightException extends AwsException {}
