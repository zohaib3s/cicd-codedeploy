<?php
namespace Aws3\Transfer\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **AWS Transfer for SFTP** service.
 */
class TransferException extends AwsException {}
