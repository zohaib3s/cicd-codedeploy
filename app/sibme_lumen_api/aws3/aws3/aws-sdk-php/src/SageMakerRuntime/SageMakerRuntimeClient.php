<?php
namespace Aws3\SageMakerRuntime;

use Aws3\AwsClient;

/**
 * This client is used to interact with the **Amazon SageMaker Runtime** service.
 * @method \Aws3\Result invokeEndpoint(array $args = [])
 * @method \GuzzleHttp\Promise\Promise invokeEndpointAsync(array $args = [])
 */
class SageMakerRuntimeClient extends AwsClient {}
