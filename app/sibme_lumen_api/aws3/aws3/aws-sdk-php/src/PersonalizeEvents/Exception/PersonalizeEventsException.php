<?php
namespace Aws3\PersonalizeEvents\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Amazon Personalize Events** service.
 */
class PersonalizeEventsException extends AwsException {}
