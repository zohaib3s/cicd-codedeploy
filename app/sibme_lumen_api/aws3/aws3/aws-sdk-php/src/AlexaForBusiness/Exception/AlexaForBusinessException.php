<?php
namespace Aws3\AlexaForBusiness\Exception;

use Aws3\Exception\AwsException;

/**
 * Represents an error interacting with the **Alexa For Business** service.
 */
class AlexaForBusinessException extends AwsException {}
