<?php return [
    'contact_endpoint' => 'https://api.hubapi.com/contacts/v1/contact/',
    'company_endpoint' => 'https://api.hubapi.com/companies/v2/',
    'contact_lists' => 'https://api.hubapi.com/contacts/v1/lists',
    'association_url' => 'https://api.hubapi.com/crm-associations/v1/associations/'
];