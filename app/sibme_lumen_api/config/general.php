<?php
$common = [
    'default_user_ids' => [2421, 2422, 2423, 2427],
    'export_analytics_path' => base_path() . '/public/export_analytics/',
    'trial_duration' => 30
];
if(is_local())
{
    $restricted = [
        'sibme_base_url' => env('BASE_URL','http://staging.sibme.com/'),
        'intercom_access_token' => 'dG9rOjg4MjgyOGJmX2Q2MWRfNDM3OF9hNWNjXzQzODU2ZjliMDZlYjoxOjA=',
        'intercom_access_token_hmh' => 'dG9rOjAwOTQ4ZGZmXzJlN2JfNDcxM185ZmY1X2U4NjgyOTQ2MDA3ZjoxOjA=',
        'environment' => 'sandbox',
        'merchantId' => '2zhny5z63p96qnpv',
        'publicKey' => '7m8srqdvts26gc4n',
        'privateKey' => 'a5cc6489fd6230be94f3fd1787ccbbe0'
        // '' => '',
    ];
}
else
{
    $restricted = [
        'sibme_base_url' => env('BASE_URL','http://app.sibme.com/'),
        'intercom_access_token' => 'dG9rOjJmMGIwZTU5XzViMzhfNDExZF85MjAwXzE0ODI5Yjk3ZGU2NToxOjA=',
        'intercom_access_token_hmh' => 'dG9rOjAwOTQ4ZGZmXzJlN2JfNDcxM185ZmY1X2U4NjgyOTQ2MDA3ZjoxOjA=',
        'environment' => 'production',
        'merchantId' => 'tk7ph5dyz33hccq8',
        'publicKey' => '44g8y8krfcrdyygn',
        'privateKey' => '9d9c0a674920bc6e4462280cd17d55a3'
        // '' => '',
    ];    
}

return array_merge($common, $restricted);
