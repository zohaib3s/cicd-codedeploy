<?php

// If you choose to use ENV vars to define these values, give this IdP its own env var names
// so you can define different values for each IdP, all starting with 'SAML2_'.$this_idp_env_id
$this_idp_env_id = 'mytestidp1';

//This is variable is for simplesaml example only.
// For real IdP, you must set the url values in the 'idp' config to conform to the IdP's real urls.
$idp_host = env('SAML2_'.$this_idp_env_id.'_IDP_HOST', config('s3.sibme_api_url'));

return $settings = array(

    /*****
     * One Login Settings
     */

    // If 'strict' is True, then the PHP Toolkit will reject unsigned
    // or unencrypted messages if it expects them signed or encrypted
    // Also will reject the messages if not strictly follow the SAML
    // standard: Destination, NameId, Conditions ... are validated too.
    'strict' => true, //@todo: make this depend on laravel config

    // Enable debug mode (to print errors)
    'debug' => env('APP_DEBUG', false),

    // Service Provider Data that we are deploying
    'sp' => array(

        // Specifies constraints on the name identifier to be used to
        // represent the requested subject.
        // Take a look on lib/Saml2/Constants.php to see the NameIdFormat supported
//        'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:emailAddress',
        'NameIDFormat' => 'urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress',

        // Usually x509cert and privateKey of the SP are provided by files placed at
        // the certs folder. But we can also provide them with the following parameters
        'x509cert' => env('SAML2_'.$this_idp_env_id.'_SP_x509',''),
        'privateKey' => env('SAML2_'.$this_idp_env_id.'_SP_PRIVATEKEY',''),

        // Identifier (URI) of the SP entity.
        // Leave blank to use the '{idpName}_metadata' route, e.g. 'test_metadata'.
        'entityId' => env('SAML2_'.$this_idp_env_id.'_SP_ENTITYID',''),

        // Specifies info about where and how the <AuthnResponse> message MUST be
        // returned to the requester, in this case our SP.
        'assertionConsumerService' => array(
            // URL Location where the <Response> from the IdP will be returned,
            // using HTTP-POST binding.
            // Leave blank to use the '{idpName}_acs' route, e.g. 'test_acs'
            'url' => config('s3.sibme_api_url').'acs',
        ),
        // Specifies info about where and how the <Logout Response> message MUST be
        // returned to the requester, in this case our SP.
        // Remove this part to not include any URL Location in the metadata.
        'singleLogoutService' => array(
            // URL Location where the <Response> from the IdP will be returned,
            // using HTTP-Redirect binding.
            // Leave blank to use the '{idpName}_sls' route, e.g. 'test_sls'
            'url' => '',
        ),
    ),

    // Identity Provider Data that we want connect with our SP
    'idp' => array(
        // Identifier of the IdP entity  (must be a URI)
//        'entityId' => env('SAML2_'.$this_idp_env_id.'_IDP_ENTITYID', $idp_host . '/saml2/idp/metadata.php'),
        'entityId' => env('SAML2_'.$this_idp_env_id.'_IDP_ENTITYID', 'https://app.onelogin.com/saml/metadata/d1d74952-3695-4bb2-9e6c-ad5fd0c87992'),
//        'entityId' => env('SAML2_'.$this_idp_env_id.'_IDP_ENTITYID', 'https://dev-903576.okta.com/app/exkjtthmbp3xa5bfj4x6/sso/saml/metadata'),
        // SSO endpoint info of the IdP. (Authentication Request protocol)
        'singleSignOnService' => array(
            // URL Target of the IdP where the SP will send the Authentication Request Message,
            // using HTTP-Redirect binding.
//            'url' => env('SAML2_'.$this_idp_env_id.'_IDP_SSO_URL', $idp_host . '/saml2/idp/SSOService.php'),
            'url' => env('SAML2_'.$this_idp_env_id.'_IDP_SSO_URL', 'https://3s-solutions-dev.onelogin.com/trust/saml2/http-post/sso/d1d74952-3695-4bb2-9e6c-ad5fd0c87992'),
//            'url' => env('SAML2_'.$this_idp_env_id.'_IDP_SSO_URL', 'https://dev-903576.okta.com/app/3ssolutionsdev903576_ssowithokta_1/exkjtptsd1OmuOJVn4x6/sso/saml'),
        ),
        // SLO endpoint info of the IdP.
        'singleLogoutService' => array(
            // URL Location of the IdP where the SP will send the SLO Request,
            // using HTTP-Redirect binding.
//            'url' => env('SAML2_'.$this_idp_env_id.'_IDP_SL_URL', $idp_host . '/saml2/idp/SingleLogoutService.php'),
            'url' => env('SAML2_'.$this_idp_env_id.'_IDP_SL_URL', 'https://3s-solutions-dev.onelogin.com/trust/saml2/http-redirect/slo/1177915'),
        ),
        // Public x509 certificate of the IdP
        'x509cert' => env('SAML2_'.$this_idp_env_id.'_IDP_x509', '-----BEGIN CERTIFICATE-----
                          MIID6zCCAtOgAwIBAgIUYOJDurc/xEr/0recX+mD7wwANggwDQYJKoZIhvcNAQEF
                          BQAwSjEVMBMGA1UECgwMM1MgU29sdXRpb25zMRUwEwYDVQQLDAxPbmVMb2dpbiBJ
                          ZFAxGjAYBgNVBAMMEU9uZUxvZ2luIEFjY291bnQgMB4XDTIwMDYzMDEzMzQyMVoX
                          DTI1MDYzMDEzMzQyMVowSjEVMBMGA1UECgwMM1MgU29sdXRpb25zMRUwEwYDVQQL
                          DAxPbmVMb2dpbiBJZFAxGjAYBgNVBAMMEU9uZUxvZ2luIEFjY291bnQgMIIBIjAN
                          BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAxDpJtCvP123id21fOeqHMotBBbNM
                          l8nRUjqQgC9D1E3xx8u9bv2Nzkc5igLTtmH4nLec78FxTXKdQkTGFkd6plcOVwE2
                          R2z49k4602EohHzjA4Z1vkJ/gHWFk9SEVZ6HXbvJtJOw/07Y+Y1beECFrRi+PaW2
                          2pplKzbxpcoBOeFRGLVfeK6/cXkYzFPwv+6fQWCrCeNVps2a6Uk6cWC7Gzw70gWD
                          QEzu0KQukyXBk37pLRJfgIP51dbOgk9GCbQjdIE1bURdxugkoyaNfQfHXnUfrIOs
                          Bh9OEVfagkw1jwtpTRT546hTBqO+6hOs9K9i9AHx3655wE67iFeJcc28RwIDAQAB
                          o4HIMIHFMAwGA1UdEwEB/wQCMAAwHQYDVR0OBBYEFPyyHvF89InLgwsTKIVSbTv8
                          ydC2MIGFBgNVHSMEfjB8gBT8sh7xfPSJy4MLEyiFUm07/MnQtqFOpEwwSjEVMBMG
                          A1UECgwMM1MgU29sdXRpb25zMRUwEwYDVQQLDAxPbmVMb2dpbiBJZFAxGjAYBgNV
                          BAMMEU9uZUxvZ2luIEFjY291bnQgghRg4kO6tz/ESv/St5xf6YPvDAA2CDAOBgNV
                          HQ8BAf8EBAMCB4AwDQYJKoZIhvcNAQEFBQADggEBALOSYoI8jgnoZoP0GcVBUrpy
                          8AheHmVhPtEaKZL/CS/aUs/qxhG6oICP0S0BFUC48Mr7u5HIQJT7QrQKiPUQHu6L
                          o0+lAAbk3cJVqXRdQSyXkrwHOHha9D3a/Eet02eTte3WbSxc+xAR1IjjrvtThF6p
                          iTYYz1hi/2MiIE/6tqzFDiDyVOJJ1lQ2B728Yb97V9uPNerdwfWGTMZPLRfw1l6L
                          R/wex2b6/16GWNCsD47ejPtMQm3cjxDPuC3UShniI6Ip3rbc4spa5QFYadejsD82
                          u84o7GrWv1QB4ip55aK2kGgR1OJFYzPBmDBLCUasT7TipeWX5LWzzQPSwgp3YnY=
                          -----END CERTIFICATE-----'),
        /*
         *  Instead of use the whole x509cert you can use a fingerprint
         *  (openssl x509 -noout -fingerprint -in "idp.crt" to generate it)
         */
        // 'certFingerprint' => '',
    ),



    /***
     *
     *  OneLogin advanced settings
     *
     *
     */
    // Security settings
    'security' => array(

        /** signatures and encryptions offered */

        // Indicates that the nameID of the <samlp:logoutRequest> sent by this SP
        // will be encrypted.
        'nameIdEncrypted' => false,

        // Indicates whether the <samlp:AuthnRequest> messages sent by this SP
        // will be signed.              [The Metadata of the SP will offer this info]
        'authnRequestsSigned' => false,

        // Indicates whether the <samlp:logoutRequest> messages sent by this SP
        // will be signed.
        'logoutRequestSigned' => false,

        // Indicates whether the <samlp:logoutResponse> messages sent by this SP
        // will be signed.
        'logoutResponseSigned' => false,

        /* Sign the Metadata
         False || True (use sp certs) || array (
                                                    keyFileName => 'metadata.key',
                                                    certFileName => 'metadata.crt'
                                                )
        */
        'signMetadata' => false,


        /** signatures and encryptions required **/

        // Indicates a requirement for the <samlp:Response>, <samlp:LogoutRequest> and
        // <samlp:LogoutResponse> elements received by this SP to be signed.
        'wantMessagesSigned' => false,

        // Indicates a requirement for the <saml:Assertion> elements received by
        // this SP to be signed.        [The Metadata of the SP will offer this info]
        'wantAssertionsSigned' => false,

        // Indicates a requirement for the NameID received by
        // this SP to be encrypted.
        'wantNameIdEncrypted' => false,

        // Authentication context.
        // Set to false and no AuthContext will be sent in the AuthNRequest,
        // Set true or don't present thi parameter and you will get an AuthContext 'exact' 'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport'
        // Set an array with the possible auth context values: array ('urn:oasis:names:tc:SAML:2.0:ac:classes:Password', 'urn:oasis:names:tc:SAML:2.0:ac:classes:X509'),
        'requestedAuthnContext' => true,
    ),

    // Contact information template, it is recommended to suply a technical and support contacts
    'contactPerson' => array(
        'technical' => array(
            'givenName' => 'name',
            'emailAddress' => 'no@reply.com'
        ),
        'support' => array(
            'givenName' => 'Support',
            'emailAddress' => 'no@reply.com'
        ),
    ),

    // Organization information template, the info in en_US lang is recomended, add more if required
    'organization' => array(
        'en-US' => array(
            'name' => 'Name',
            'displayname' => 'Display Name',
            'url' => 'http://url'
        ),
    ),

/* Interoperable SAML 2.0 Web Browser SSO Profile [saml2int]   http://saml2int.org/profile/current

   'authnRequestsSigned' => false,    // SP SHOULD NOT sign the <samlp:AuthnRequest>,
                                      // MUST NOT assume that the IdP validates the sign
   'wantAssertionsSigned' => true,
   'wantAssertionsEncrypted' => true, // MUST be enabled if SSL/HTTPs is disabled
   'wantNameIdEncrypted' => false,
*/

);
