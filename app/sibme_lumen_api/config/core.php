<?php
return [
    'encoder_provider' => '2', 
    /**
     * // 1 is Zencoder, 2 is Amazon, 4 is Elemental Mediaconvert
     * to prevent from any ripple effects we are NOT using encoder_provider => 4 here. Instead we will forcefully use Elemental Mediaconvert to
     * videos from web by adding a separate column aws_transcoder_type in documents table (decided in the meeting on 09 Oct 19).
     * So, if aws_transcoder_type == 2 then use Elemental Mediaconvert and supress the encoder_provider setting.
     */
    'aws_transcoder_type' => '1',
    /**
     * 2 for Elemental Mediaconvert, 1 for default
     * If aws_transcoder_type == 2 then use Elemental Mediaconvert and supress the encoder_provider setting.
     */
    'use_cloudfront'=> true, // enable/disable cloud front
    'sibmecdn_host_url'=> 'https://d1c4hn49irt1g3.cloudfront.net', 

    'use_ffmpeg_path'=> '1', //ffmpeg settings
    'ffmpeg_binaries'=> '/usr/bin/ffmpeg', //ffmpeg settings
    'ffprobe_binaries'=> '/usr/bin/ffprobe', //ffprobe settings

    'cache_expire_seconds'=>129600, // seconds = 36 HOURS
    'use_queues' => env('ENABLE_LUMEN_QUEUES', true),
];
?>