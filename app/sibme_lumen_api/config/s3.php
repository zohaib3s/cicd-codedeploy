<?php

    if (php_sapi_name() == 'cli') {
        $url = env('API_URL', 'https://api.sibme.com/');
    }else{        
        $url = getSiteURL();
    }

    if (is_local()) {
        if (strpos($url, 'q2hmhapi') !== false) {
            $base_url = 'https://q2hmh.sibme.com/';
        } elseif(strpos($url, 'qahmhapi') !== false) {
            $base_url = 'https://qahmh.sibme.com/';
        } elseif(strpos($url, 'hmhapi') !== false) {
            $base_url = 'https://hmh.sibme.com/';
        } elseif(strpos($url, 'clshmhapi') !== false) {
            $base_url = 'https://clshmh.sibme.com/';
        } elseif(strpos($url, 'stagingapi') !== false) {
            $base_url = 'https://staging.sibme.com/';
        } elseif(strpos($url, 'qaapi') !== false) {
            $base_url = 'https://qa.sibme.com/';
        } elseif(strpos($url, 'q2api') !== false) {
            $base_url = 'https://q2.sibme.com/';
        } elseif(strpos($url, 'q21api') !== false) {
            $base_url = 'https://q21.sibme.com/';
        } elseif(strpos($url, 'q22api') !== false) {
            $base_url = 'https://q22.sibme.com/';
        } elseif(strpos($url, 'q3api') !== false) {
            $base_url = 'https://q3.sibme.com/';
        } elseif(strpos($url, 'q31api') !== false) {
            $base_url = 'https://q31.sibme.com/';
        } elseif(strpos($url, 'q32api') !== false) {
            $base_url = 'https://q32.sibme.com/';
        } elseif(strpos($url, 'q21api') !== false) {
            $base_url = 'https://q21.sibme.com/';
        } elseif(strpos($url, 'clsapi') !== false) {
            $base_url = 'https://cls.sibme.com/';
        } elseif(strpos($url, 'devteam-api') !== false) {
            $base_url = 'https://devteam-app.sibme.com/';
        } elseif(strpos($url, 'doapi') !== false) {
            $base_url = 'https://doapp.sibme.com/';
        } elseif(strpos($url, 'devopsapi') !== false) {
            $base_url = 'https://devops.sibme.com/';
        } else {
            $base_url = 'https://wl.sibme.com/';
        }
    } else {
        if (strpos($url, 'csapi') !== false) {
            $base_url = 'https://coachingstudio.hmhco.com/';
        } else {
            $base_url = 'https://app.sibme.com/';
        }
    }

    if(is_local()){
        $api_url = rtrim($url, '/') . '/';
    } else {
        if(strpos($url, 'csapi') !== false){
            $api_url = "https://csapi.sibme.com/";
        }else{
            $api_url = "https://api.sibme.com/";
        }
    }
    
return [
    'amazon_base_url' => 'http://sibme-production.s3.amazonaws.com/',
    'bucket_name' => 'sibme-production',
    'bucket_name_cdn' => 'sibme.com',
    'access_key_id' => 'AKIAJ4ZWDR5X5JKB7CZQ',
    'secret_access_key' => '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',
    'region' => 'us-east-1',
    'use_cloudfront' => true,
    'cloudfront_keypair' => 'APKAJ64SUURDPDPPLA5Q',
    'cloudfront_host_url' => 'https://d1ebp8oyqi4ffr.cloudfront.net',
    'cloudfront_distribution_id'=> 'E2Y2BO1ZAWQUFG',
    'reply_to_email' => '@sibme.com',
    'use_job_queue' => true,
    'sibme_base_url' => $base_url,
    'sibme_api_url' => $api_url,
    'export_analytics_path' => 'uploads/export_analytics/',
    'use_local_file_store'=>false,
    'amazon_base_url'=>'https://s3.amazonaws.com/'
];

function getSiteURL() {
    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ||
      $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'];
    return $protocol.$domainName;
  }
  