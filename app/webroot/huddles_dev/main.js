(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./list/list.module": [
		"./src/app/list/list.module.ts",
		"list-list-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return __webpack_require__.e(ids[1]).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/account-information/account-information.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/account-information/account-information.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/*.account_info{\r\n\tdisplay: inline-block;\r\n\tcolor: #FFF;\r\n\tfont-size: 16px;\r\n\tmargin-top: 30px;\r\n\tposition: absolute;\r\n\r\n\twhite-space: nowrap;\r\n    overflow: hidden;\r\n    text-overflow: ellipsis;\r\n    max-width: 35ch;\r\n}*/\r\n.sibme_coaching {\r\n    margin-left: 186px;\r\n    display: inline-block;\r\n    font-size: 15px;\r\n    color: #fff;\r\n    top: 3px;\r\n    position: relative;\r\n}\r\n.sibme_coaching a{\r\n    color: #ccc;\r\n    text-decoration: none;\r\n}\r\n.sibme_coaching a:hover{\r\n    color: #fff;\r\n}\r\n.sibme_coaching a .non_gry{\r\n    display: none;\r\n}\r\n.sibme_coaching a:hover .non_gry{\r\n    display: inline-block;\r\n}\r\n.sibme_coaching a:hover .gry{\r\n    display: none;\r\n}\r\n.tooltip {\r\n  width: 7em;\r\n  white-space: normal;\r\n}\r\np{\r\n    margin:0px;\r\n}\r\n.sibme_coaching p{\r\n    font-size: 12px;\r\n    line-height: 23px}\r\n.sibme_coaching img{\r\n    max-width: 15px;\r\n    margin-left: 3px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWNjb3VudC1pbmZvcm1hdGlvbi9hY2NvdW50LWluZm9ybWF0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7O0dBV0c7QUFDSDtJQUNJLG1CQUFtQjtJQUNuQixzQkFBc0I7SUFDdEIsZ0JBQWdCO0lBQ2hCLFlBQVk7SUFDWixTQUFTO0lBQ1QsbUJBQW1CO0NBQ3RCO0FBRUQ7SUFDSSxZQUFZO0lBQ1osc0JBQXNCO0NBQ3pCO0FBQ0Q7SUFDSSxZQUFZO0NBQ2Y7QUFFRDtJQUNJLGNBQWM7Q0FDakI7QUFFRDtJQUNJLHNCQUFzQjtDQUN6QjtBQUVEO0lBQ0ksY0FBYztDQUNqQjtBQUVEO0VBQ0UsV0FBVztFQUNYLG9CQUFvQjtDQUNyQjtBQUNEO0lBQ0ksV0FBVztDQUNkO0FBQ0Q7SUFDSSxnQkFBZ0I7SUFDaEIsaUJBQWlCLENBQUM7QUFFdEI7SUFDSSxnQkFBZ0I7SUFDaEIsaUJBQWlCO0NBQ3BCIiwiZmlsZSI6InNyYy9hcHAvYWNjb3VudC1pbmZvcm1hdGlvbi9hY2NvdW50LWluZm9ybWF0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKi5hY2NvdW50X2luZm97XHJcblx0ZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG5cdGNvbG9yOiAjRkZGO1xyXG5cdGZvbnQtc2l6ZTogMTZweDtcclxuXHRtYXJnaW4tdG9wOiAzMHB4O1xyXG5cdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHJcblx0d2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcclxuICAgIG1heC13aWR0aDogMzVjaDtcclxufSovXHJcbi5zaWJtZV9jb2FjaGluZyB7XHJcbiAgICBtYXJnaW4tbGVmdDogMTg2cHg7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIHRvcDogM3B4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG4uc2libWVfY29hY2hpbmcgYXtcclxuICAgIGNvbG9yOiAjY2NjO1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG59XHJcbi5zaWJtZV9jb2FjaGluZyBhOmhvdmVye1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbn1cclxuXHJcbi5zaWJtZV9jb2FjaGluZyBhIC5ub25fZ3J5e1xyXG4gICAgZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuLnNpYm1lX2NvYWNoaW5nIGE6aG92ZXIgLm5vbl9ncnl7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbi5zaWJtZV9jb2FjaGluZyBhOmhvdmVyIC5ncnl7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4udG9vbHRpcCB7XHJcbiAgd2lkdGg6IDdlbTtcclxuICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xyXG59XHJcbnB7XHJcbiAgICBtYXJnaW46MHB4O1xyXG59XHJcbi5zaWJtZV9jb2FjaGluZyBwe1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgbGluZS1oZWlnaHQ6IDIzcHh9XHJcblxyXG4uc2libWVfY29hY2hpbmcgaW1ne1xyXG4gICAgbWF4LXdpZHRoOiAxNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDNweDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/account-information/account-information.component.html":
/*!************************************************************************!*\
  !*** ./src/app/account-information/account-information.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sibme_coaching\">\r\n\t<p>\r\n\t\t\r\n\t\t<a *ngIf=\"details?.is_change_account_enable==1\" [attr.href]=\"details?.base_url+'/launchpad'\" tooltip=\"change account\" placement=\"bottom\">\r\n\t\t\t{{details?.user_current_account?.accounts?.company_name}}\r\n\t\t\t<img width=\"19\" src=\"./assets/img/new/icon-switch-account.svg\" class=\"non_gry\" />\r\n\t\t\t<img width=\"19\" src=\"./assets/img/new/icon-switch-account_gry.svg\" class=\"gry\" />\r\n\r\n\t\t</a>\r\n\t\t<a *ngIf=\"details?.is_change_account_enable!=1 && !details?.user_current_account?.accounts?.in_trial\" tooltip=\"change account\" placement=\"bottom\">\r\n\t\t\t{{details?.user_current_account?.accounts?.company_name}}\r\n\r\n\t\t</a>\r\n\r\n\t\t<a *ngIf=\"details?.is_change_account_enable!=1 && details?.user_current_account?.accounts?.in_trial\" tooltip=\"change account\" placement=\"bottom\">\r\n\t\t\t{{details?.user_current_account?.accounts?.company_name}}\r\n\r\n\t\t</a>\r\n\t</p>\r\n\t<trial-message [details]=\"details\"> </trial-message>\r\n</div>"

/***/ }),

/***/ "./src/app/account-information/account-information.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/account-information/account-information.component.ts ***!
  \**********************************************************************/
/*! exports provided: AccountInformationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountInformationComponent", function() { return AccountInformationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AccountInformationComponent = /** @class */ (function () {
    function AccountInformationComponent() {
    }
    AccountInformationComponent.prototype.ngOnInit = function () {
    };
    AccountInformationComponent.prototype.change_account = function () {
        console.log("Change triggered");
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AccountInformationComponent.prototype, "details", void 0);
    AccountInformationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'account-information',
            template: __webpack_require__(/*! ./account-information.component.html */ "./src/app/account-information/account-information.component.html"),
            styles: [__webpack_require__(/*! ./account-information.component.css */ "./src/app/account-information/account-information.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AccountInformationComponent);
    return AccountInformationComponent;
}());



/***/ }),

/***/ "./src/app/app-header/app-header.component.css":
/*!*****************************************************!*\
  !*** ./src/app/app-header/app-header.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header_container{\r\n\tbackground: #3D6A98;\r\n    width: 100%;\r\n    color: #FFF;\r\n    height: 64px;\r\n}\r\nbody{\r\n\tcolor: #757575;\r\n    font-size: 90%;\r\n    font-weight: normal;\r\n    font-family: \"Segoe UI\",Helvetica,Arial,sans-serif;\r\n}\r\n.container{\r\n\tmax-width: 1053px !important;\r\n}\r\np{\r\n\tmargin-bottom: 0; \r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLWhlYWRlci9hcHAtaGVhZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Q0FDQyxvQkFBb0I7SUFDakIsWUFBWTtJQUNaLFlBQVk7SUFDWixhQUFhO0NBQ2hCO0FBQ0Q7Q0FDQyxlQUFlO0lBQ1osZUFBZTtJQUNmLG9CQUFvQjtJQUNwQixtREFBbUQ7Q0FDdEQ7QUFFRDtDQUNDLDZCQUE2QjtDQUM3QjtBQUVEO0NBQ0MsaUJBQWlCO0NBQ2pCIiwiZmlsZSI6InNyYy9hcHAvYXBwLWhlYWRlci9hcHAtaGVhZGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyX2NvbnRhaW5lcntcclxuXHRiYWNrZ3JvdW5kOiAjM0Q2QTk4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBjb2xvcjogI0ZGRjtcclxuICAgIGhlaWdodDogNjRweDtcclxufVxyXG5ib2R5e1xyXG5cdGNvbG9yOiAjNzU3NTc1O1xyXG4gICAgZm9udC1zaXplOiA5MCU7XHJcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgZm9udC1mYW1pbHk6IFwiU2Vnb2UgVUlcIixIZWx2ZXRpY2EsQXJpYWwsc2Fucy1zZXJpZjtcclxufVxyXG5cclxuLmNvbnRhaW5lcntcclxuXHRtYXgtd2lkdGg6IDEwNTNweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5we1xyXG5cdG1hcmdpbi1ib3R0b206IDA7IFxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/app-header/app-header.component.html":
/*!******************************************************!*\
  !*** ./src/app/app-header/app-header.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"headerdetails?.status\" class=\"sibme_header\">\r\n<impersonation [details]=\"headerdetails\"></impersonation>\r\n<header id=\"header\" class=\"header_container\" [style.backgroundColor]=\"get_header_bg(headerdetails)\">\r\n\t<div class=\"container\">\r\n\t<logo-area [details]=\"headerdetails\"></logo-area>\r\n\t<account-information [details]=\"headerdetails\"> </account-information>\r\n\t\r\n\r\n\t<user [details]=\"headerdetails\"></user>\r\n\r\n\t<navigation [details]=\"headerdetails\"></navigation>\r\n\t</div>\r\n\r\n</header>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app-header/app-header.component.ts":
/*!****************************************************!*\
  !*** ./src/app/app-header/app-header.component.ts ***!
  \****************************************************/
/*! exports provided: AppHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppHeaderComponent", function() { return AppHeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _header_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../header.service */ "./src/app/header.service.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");




//"/angular_header/"
var AppHeaderComponent = /** @class */ (function () {
    //base_url = "http://www.sibme.test/api";
    function AppHeaderComponent(headerService) {
        this.headerService = headerService;
    }
    AppHeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.headerdetails = {};
        this.headerdetails.status = false;
        var base_url = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].baseHeaderUrl + "/angular_header_api";
        this.headerService.getHeaderData(base_url).subscribe(function (header_details) { _this.handleResponse(header_details); });
    };
    AppHeaderComponent.prototype.handleError = function () {
        console.log("Error occured.");
        setTimeout(function () {
            this.handleResponse({ status: false });
        });
    };
    AppHeaderComponent.prototype.handleResponse = function (args) {
        this.headerdetails = args;
        if (!args.status) {
            //alert("You need to login to access this page.");
            var getUrl = window.location;
            var baseUrl = getUrl.protocol + "//" + getUrl.host;
            location.href = baseUrl;
        }
        else {
            // console.log(this.headerdetails);
        }
    };
    AppHeaderComponent.prototype.get_header_bg = function (args) {
        if (args && args.status) {
            return args.header_background_color;
        }
    };
    AppHeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./app-header.component.html */ "./src/app/app-header/app-header.component.html"),
            styles: [__webpack_require__(/*! ./app-header.component.css */ "./src/app/app-header/app-header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_header_service__WEBPACK_IMPORTED_MODULE_2__["HeaderService"]])
    ], AppHeaderComponent);
    return AppHeaderComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [
    {
        path: 'list',
        loadChildren: './list/list.module#ListModule'
    },
    {
        path: 'list/:folder_id',
        loadChildren: './list/list.module#ListModule'
    },
    {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: 'list'
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n<app-header></app-header>\r\n<app-breadcrumbs></app-breadcrumbs>\r\n<div *ngIf=\"isLoggedIn\" class=\"container box_outer\">\r\n\r\n<router-outlet></router-outlet>\r\n</div>\r\n\r\n<ngx-loading-bar></ngx-loading-bar>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _header_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./header.service */ "./src/app/header.service.ts");



var AppComponent = /** @class */ (function () {
    function AppComponent(headerService) {
        var _this = this;
        this.headerService = headerService;
        this.title = 'huddle-landing-page';
        var env = window.location.hostname.substring(0, window.location.hostname.indexOf(".")).toUpperCase();
        if (location.protocol != 'https:' && env != "") {
            location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
        }
        this.headerService.LoggedIn.subscribe(function (data) {
            _this.isLoggedIn = data;
            if (data) {
                _this.writeToHead();
            }
        });
    }
    AppComponent.prototype.writeToHead = function () {
        var sessionData = this.headerService.getStaticHeaderData();
        var user_role = sessionData.user_role;
        var is_in_trial = sessionData.is_in_trial;
        var full_name = sessionData.user_current_account.User.first_name + ' ' + sessionData.user_current_account.User.last_name;
        var intercomSettings = {
            app_id: "uin0il18",
            name: full_name,
            email: sessionData.user_current_account.User.email,
            created_at: Math.floor(new Date().getTime() / 1000),
            user_role: user_role,
            is_in_trial: is_in_trial
        };
        window.Intercom('update', intercomSettings);
        window.HS.beacon.ready(function () {
            window.HS.beacon.identify({
                name: full_name,
                email: sessionData.user_current_account.User.email,
            });
        });
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_header_service__WEBPACK_IMPORTED_MODULE_2__["HeaderService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _ngx_loading_bar_http_client__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ngx-loading-bar/http-client */ "./node_modules/@ngx-loading-bar/http-client/fesm5/ngx-loading-bar-http-client.js");
/* harmony import */ var ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-bootstrap/modal */ "./node_modules/ngx-bootstrap/modal/fesm5/ngx-bootstrap-modal.js");
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_toastr__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-toastr */ "./node_modules/ngx-toastr/fesm5/ngx-toastr.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");
/* harmony import */ var _app_header_app_header_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./app-header/app-header.component */ "./src/app/app-header/app-header.component.ts");
/* harmony import */ var _logo_area_logo_area_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./logo-area/logo-area.component */ "./src/app/logo-area/logo-area.component.ts");
/* harmony import */ var _account_information_account_information_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./account-information/account-information.component */ "./src/app/account-information/account-information.component.ts");
/* harmony import */ var _trial_message_trial_message_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./trial-message/trial-message.component */ "./src/app/trial-message/trial-message.component.ts");
/* harmony import */ var _user_user_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./user/user.component */ "./src/app/user/user.component.ts");
/* harmony import */ var _navigation_navigation_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./navigation/navigation.component */ "./src/app/navigation/navigation.component.ts");
/* harmony import */ var _impersonation_impersonation_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./impersonation/impersonation.component */ "./src/app/impersonation/impersonation.component.ts");
/* harmony import */ var _breadcrumbs_breadcrumbs_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./breadcrumbs/breadcrumbs.component */ "./src/app/breadcrumbs/breadcrumbs.component.ts");
/* harmony import */ var _header_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./header.service */ "./src/app/header.service.ts");
/* harmony import */ var _list_services_home_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./list/services/home.service */ "./src/app/list/services/home.service.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");

// AngularServices






// 3rdParty





// Custom Components








// Services


// Routing

// BootstrapableApp

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_23__["AppComponent"],
                _app_header_app_header_component__WEBPACK_IMPORTED_MODULE_12__["AppHeaderComponent"],
                _logo_area_logo_area_component__WEBPACK_IMPORTED_MODULE_13__["LogoAreaComponent"],
                _account_information_account_information_component__WEBPACK_IMPORTED_MODULE_14__["AccountInformationComponent"],
                _trial_message_trial_message_component__WEBPACK_IMPORTED_MODULE_15__["TrialMessageComponent"],
                _user_user_component__WEBPACK_IMPORTED_MODULE_16__["UserComponent"],
                _navigation_navigation_component__WEBPACK_IMPORTED_MODULE_17__["NavigationComponent"],
                _impersonation_impersonation_component__WEBPACK_IMPORTED_MODULE_18__["ImpersonationComponent"],
                _breadcrumbs_breadcrumbs_component__WEBPACK_IMPORTED_MODULE_19__["BreadcrumbsComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_22__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                _ngx_loading_bar_http_client__WEBPACK_IMPORTED_MODULE_7__["LoadingBarHttpClientModule"],
                ngx_bootstrap_modal__WEBPACK_IMPORTED_MODULE_8__["ModalModule"].forRoot(),
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_9__["BsDropdownModule"].forRoot(),
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
                ngx_toastr__WEBPACK_IMPORTED_MODULE_10__["ToastrModule"].forRoot(),
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_11__["TooltipModule"].forRoot()
            ],
            providers: [
                _header_service__WEBPACK_IMPORTED_MODULE_20__["HeaderService"],
                _list_services_home_service__WEBPACK_IMPORTED_MODULE_21__["HomeService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_23__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/breadcrumbs/breadcrumbs.component.css":
/*!*******************************************************!*\
  !*** ./src/app/breadcrumbs/breadcrumbs.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bred_an {\r\n    position: relative;\r\n    top: 24px;\r\n    color: #4d504d;    \r\n    padding-bottom: 25px;\r\n    font-family: Roboto;\r\n}\r\n.bred_an img{ margin:0 15px;}\r\n.bred_an a{ \r\n    color: #838383 !important;\r\n    font-weight: 400;\r\n    font-family: Roboto;\r\n}\r\n/*.bred_an {\r\n    position: relative;\r\n    top: 24px;\r\n    color: #4d504d;\r\n    font-family: 'Roboto';\r\n}*/\r\na.disabled {\r\n   pointer-events: none;\r\n   cursor: default;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYnJlYWRjcnVtYnMvYnJlYWRjcnVtYnMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLG1CQUFtQjtJQUNuQixVQUFVO0lBQ1YsZUFBZTtJQUNmLHFCQUFxQjtJQUNyQixvQkFBb0I7Q0FDdkI7QUFDRCxjQUFjLGNBQWMsQ0FBQztBQUU3QjtJQUNJLDBCQUEwQjtJQUMxQixpQkFBaUI7SUFDakIsb0JBQW9CO0NBQ3ZCO0FBQ0Q7Ozs7O0dBS0c7QUFFSDtHQUNHLHFCQUFxQjtHQUNyQixnQkFBZ0I7Q0FDbEIiLCJmaWxlIjoic3JjL2FwcC9icmVhZGNydW1icy9icmVhZGNydW1icy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJyZWRfYW4ge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgdG9wOiAyNHB4O1xyXG4gICAgY29sb3I6ICM0ZDUwNGQ7ICAgIFxyXG4gICAgcGFkZGluZy1ib3R0b206IDI1cHg7XHJcbiAgICBmb250LWZhbWlseTogUm9ib3RvO1xyXG59XHJcbi5icmVkX2FuIGltZ3sgbWFyZ2luOjAgMTVweDt9XHJcblxyXG4uYnJlZF9hbiBheyBcclxuICAgIGNvbG9yOiAjODM4MzgzICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgZm9udC1mYW1pbHk6IFJvYm90bztcclxufVxyXG4vKi5icmVkX2FuIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRvcDogMjRweDtcclxuICAgIGNvbG9yOiAjNGQ1MDRkO1xyXG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8nO1xyXG59Ki9cclxuXHJcbmEuZGlzYWJsZWQge1xyXG4gICBwb2ludGVyLWV2ZW50czogbm9uZTtcclxuICAgY3Vyc29yOiBkZWZhdWx0O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/breadcrumbs/breadcrumbs.component.html":
/*!********************************************************!*\
  !*** ./src/app/breadcrumbs/breadcrumbs.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"clear\"></div>\r\n<div class=\"container bred_an\" *ngIf=\"breadcrubms\">\r\n <a [attr.href]=\"urls?.homeUrl\">Home</a>\r\n\r\n <ng-container *ngIf=\"breadcrubms && breadcrubms?.length>0\">\r\n \t<img src=\"assets/img/breadcrumarrow.svg\">\r\n\t<a [routerLink]=\"['/list']\">\r\n\t\tHuddles\r\n\t</a>\r\n <ng-container *ngFor=\"let bc of breadcrubms; let last=last\">\r\n\t<img src=\"assets/img/breadcrumarrow.svg\">\r\n\t<a [routerLink]=\"['/list', bc.folder_id]\" [class.disabled]=\"last\">\r\n\t\t{{bc.folder_name}}\r\n\t</a>\r\n </ng-container>\r\n</ng-container>\r\n\r\n<ng-container *ngIf=\"breadcrubms?.length==0\">\r\n\t<img src=\"assets/img/breadcrumarrow.svg\"> Huddles\r\n</ng-container>\r\n  \r\n</div>\r\n<div class=\"clear\"></div>"

/***/ }),

/***/ "./src/app/breadcrumbs/breadcrumbs.component.ts":
/*!******************************************************!*\
  !*** ./src/app/breadcrumbs/breadcrumbs.component.ts ***!
  \******************************************************/
/*! exports provided: BreadcrumbsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BreadcrumbsComponent", function() { return BreadcrumbsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _list_services_home_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../list/services/home.service */ "./src/app/list/services/home.service.ts");





var BreadcrumbsComponent = /** @class */ (function () {
    function BreadcrumbsComponent(router, homeService) {
        this.router = router;
        this.homeService = homeService;
    }
    BreadcrumbsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.urls = {};
        // this.breadcrubms = [];
        this.urls.homeUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].baseUrl;
        // this.router.params.subscribe((p)=>{
        // if(p.folder_id){
        this.homeService.Breadcrumbs.subscribe(function (data) {
            _this.breadcrubms = data;
            console.log(_this.breadcrubms);
        });
        // }
        // });
    };
    BreadcrumbsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-breadcrumbs',
            template: __webpack_require__(/*! ./breadcrumbs.component.html */ "./src/app/breadcrumbs/breadcrumbs.component.html"),
            styles: [__webpack_require__(/*! ./breadcrumbs.component.css */ "./src/app/breadcrumbs/breadcrumbs.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _list_services_home_service__WEBPACK_IMPORTED_MODULE_4__["HomeService"]])
    ], BreadcrumbsComponent);
    return BreadcrumbsComponent;
}());



/***/ }),

/***/ "./src/app/header.service.ts":
/*!***********************************!*\
  !*** ./src/app/header.service.ts ***!
  \***********************************/
/*! exports provided: HeaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderService", function() { return HeaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");





var HeaderService = /** @class */ (function () {
    function HeaderService(http) {
        this.http = http;
        this.LoggedIn = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    HeaderService.prototype.loadData = function (path) {
        var _this = this;
        return this.http.get(path)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (data) { _this.data = data; _this.UpdateLoggedInStatus(data); }));
    };
    HeaderService.prototype.UpdateLoggedInStatus = function (data) {
        this.LoggedIn.emit(data.status);
    };
    HeaderService.prototype.getStaticHeaderData = function () {
        return this.data;
    };
    HeaderService.prototype.getHeaderData = function (path) {
        // let data: headerData;
        // this.data = (<any>window).header_data;
        // return of(this.data);
        //if(!this.data){
        //	return this.loadData();
        return this.loadData(path); //.subscribe((data) => {this.data=data;this.getHeaderData()})
        //	}else{
        //return of(this.data);
        //		console.log("presaved");
        //		return of(this.data);
        //	}
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], HeaderService.prototype, "LoggedIn", void 0);
    HeaderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            providers: [
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
            ]
        }),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], HeaderService);
    return HeaderService;
}());



/***/ }),

/***/ "./src/app/impersonation/impersonation.component.css":
/*!***********************************************************!*\
  !*** ./src/app/impersonation/impersonation.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".impersonation {\r\n    padding: 5px;\r\n    background: #FF7800;\r\n    color: #fff;\r\n    font-size: 13px;\r\n    font-weight: bold;\r\n    text-align: center;\r\n    z-index: 9;\r\n    border-bottom: solid 1px #4A3A2E;\r\n    position: relative;\r\n}\r\n\r\n.impersonation a {\r\n    color: white;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW1wZXJzb25hdGlvbi9pbXBlcnNvbmF0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxhQUFhO0lBQ2Isb0JBQW9CO0lBQ3BCLFlBQVk7SUFDWixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLG1CQUFtQjtJQUNuQixXQUFXO0lBQ1gsaUNBQWlDO0lBQ2pDLG1CQUFtQjtDQUN0Qjs7QUFFRDtJQUNJLGFBQWE7Q0FDaEIiLCJmaWxlIjoic3JjL2FwcC9pbXBlcnNvbmF0aW9uL2ltcGVyc29uYXRpb24uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbXBlcnNvbmF0aW9uIHtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGJhY2tncm91bmQ6ICNGRjc4MDA7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgei1pbmRleDogOTtcclxuICAgIGJvcmRlci1ib3R0b206IHNvbGlkIDFweCAjNEEzQTJFO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG4uaW1wZXJzb25hdGlvbiBhIHtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/impersonation/impersonation.component.html":
/*!************************************************************!*\
  !*** ./src/app/impersonation/impersonation.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"details?.is_impersonate==1\" class=\"impersonation\" (click)=\"exitImpersonation()\">\r\n\t<a [attr.href]=\"details.base_url+'/app/impersonate_back/'+details?.last_login_id\"> \r\n\t\t<!-- href=\"/app/impersonate_back/\" -->\r\n\texit impersonation\r\n\t</a>\r\n</div>"

/***/ }),

/***/ "./src/app/impersonation/impersonation.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/impersonation/impersonation.component.ts ***!
  \**********************************************************/
/*! exports provided: ImpersonationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ImpersonationComponent", function() { return ImpersonationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ImpersonationComponent = /** @class */ (function () {
    function ImpersonationComponent() {
    }
    ImpersonationComponent.prototype.ngOnInit = function () {
        this.is_visible = true;
    };
    ImpersonationComponent.prototype.exitImpersonation = function () {
        debugger;
        this.is_visible = !this.is_visible;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ImpersonationComponent.prototype, "details", void 0);
    ImpersonationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'impersonation',
            template: __webpack_require__(/*! ./impersonation.component.html */ "./src/app/impersonation/impersonation.component.html"),
            styles: [__webpack_require__(/*! ./impersonation.component.css */ "./src/app/impersonation/impersonation.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ImpersonationComponent);
    return ImpersonationComponent;
}());



/***/ }),

/***/ "./src/app/list/services/home.service.ts":
/*!***********************************************!*\
  !*** ./src/app/list/services/home.service.ts ***!
  \***********************************************/
/*! exports provided: HomeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeService", function() { return HomeService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");




var HomeService = /** @class */ (function () {
    function HomeService(http) {
        // console.log("I Came here");
        this.http = http;
        this.Breadcrumbs = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    HomeService.prototype.UpdateFilter = function () {
    };
    HomeService.prototype.GetHuddles = function (data) {
        // page = page || 1;
        var path = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].APIbaseUrl + "/get_huddles";
        return this.http.post(path, data);
    };
    HomeService.prototype.Move = function (obj) {
        var path = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].APIbaseUrl + "/move_huddle_folder";
        return this.http.post(path, obj);
    };
    HomeService.prototype.CreateFolder = function (obj) {
        var path = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].APIbaseUrl + "/create_folder";
        return this.http.post(path, obj);
    };
    HomeService.prototype.GetFolderList = function (obj) {
        var path = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].APIbaseUrl + "/treeview_detail";
        return this.http.post(path, obj);
    };
    HomeService.prototype.GetBreadcrumbs = function (obj) {
        var path = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].APIbaseUrl + "/get_bread_crumb";
        return this.http.post(path, obj);
    };
    HomeService.prototype.EditFolder = function (obj) {
        var path = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].APIbaseUrl + "/edit_folder";
        return this.http.post(path, obj);
    };
    HomeService.prototype.DeleteItem = function (obj, isFolder) {
        var path = (isFolder) ? _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].APIbaseUrl + "/delete_folder" : _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].APIbaseUrl + "/delete_huddle";
        return this.http.post(path, obj);
    };
    HomeService.prototype.getAvatarPath = function (user) {
        if (user.image) {
            return _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].imageBaseUrl + "/" + user.user_id + "/" + user.image;
        }
        else {
            return _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].baseUrl + "/img/home/photo-default.png";
        }
        // if(details.user_current_account.User.image){
        //   return details.avatar_path;
        // } else{
        // }
        //
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], HomeService.prototype, "Breadcrumbs", void 0);
    HomeService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], HomeService);
    return HomeService;
}());



/***/ }),

/***/ "./src/app/logo-area/logo-area.component.css":
/*!***************************************************!*\
  !*** ./src/app/logo-area/logo-area.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#logo{\r\n\tdisplay: block;\r\n    position: absolute;\r\n    top: 0;\r\n    text-indent: 100%;\r\n    overflow: hidden;\r\n    font-size: 0;\r\n    width: 175px;\r\n    height: 64px;\r\n    background: #fff;\r\n    box-shadow: 0px 2px 7px rgba(0,0,0,0.3);\r\n}\r\n\r\n#logo a {\r\n    width: 150px;\r\n    height: 63px;\r\n    margin: 0 auto;\r\n    /*background: url(/assets/img/new/logo-3.png) no-repeat center center;*/\r\n    background-repeat: no-repeat;\r\n    background-position: center center;\r\n    display: block;\r\n    top: 0;\r\n    background-size: 107px;\r\n    position: relative;\r\n    left: -1px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9nby1hcmVhL2xvZ28tYXJlYS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0NBQ0MsZUFBZTtJQUNaLG1CQUFtQjtJQUNuQixPQUFPO0lBQ1Asa0JBQWtCO0lBQ2xCLGlCQUFpQjtJQUNqQixhQUFhO0lBQ2IsYUFBYTtJQUNiLGFBQWE7SUFDYixpQkFBaUI7SUFDakIsd0NBQXdDO0NBQzNDOztBQUVEO0lBQ0ksYUFBYTtJQUNiLGFBQWE7SUFDYixlQUFlO0lBQ2Ysd0VBQXdFO0lBQ3hFLDZCQUE2QjtJQUM3QixtQ0FBbUM7SUFDbkMsZUFBZTtJQUNmLE9BQU87SUFDUCx1QkFBdUI7SUFDdkIsbUJBQW1CO0lBQ25CLFdBQVc7Q0FDZCIsImZpbGUiOiJzcmMvYXBwL2xvZ28tYXJlYS9sb2dvLWFyZWEuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiNsb2dve1xyXG5cdGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgdGV4dC1pbmRlbnQ6IDEwMCU7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgZm9udC1zaXplOiAwO1xyXG4gICAgd2lkdGg6IDE3NXB4O1xyXG4gICAgaGVpZ2h0OiA2NHB4O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIGJveC1zaGFkb3c6IDBweCAycHggN3B4IHJnYmEoMCwwLDAsMC4zKTtcclxufVxyXG5cclxuI2xvZ28gYSB7XHJcbiAgICB3aWR0aDogMTUwcHg7XHJcbiAgICBoZWlnaHQ6IDYzcHg7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIC8qYmFja2dyb3VuZDogdXJsKC9hc3NldHMvaW1nL25ldy9sb2dvLTMucG5nKSBuby1yZXBlYXQgY2VudGVyIGNlbnRlcjsqL1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlciBjZW50ZXI7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHRvcDogMDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogMTA3cHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBsZWZ0OiAtMXB4O1xyXG59Il19 */"

/***/ }),

/***/ "./src/app/logo-area/logo-area.component.html":
/*!****************************************************!*\
  !*** ./src/app/logo-area/logo-area.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"logo\">\r\n\t\r\n\t<a [attr.href]=\"details.base_url+'/dashboard/home'\" [style.backgroundImage]=\"'url('+ load_bg(details) +')'\" [style.backgroundColor]=\"details?.logo_background_color\">{{details.base_url}}</a>\r\n\r\n</div>"

/***/ }),

/***/ "./src/app/logo-area/logo-area.component.ts":
/*!**************************************************!*\
  !*** ./src/app/logo-area/logo-area.component.ts ***!
  \**************************************************/
/*! exports provided: LogoAreaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoAreaComponent", function() { return LogoAreaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LogoAreaComponent = /** @class */ (function () {
    function LogoAreaComponent() {
    }
    LogoAreaComponent.prototype.ngOnInit = function () {
    };
    LogoAreaComponent.prototype.load_bg = function (details) {
        if (details && details.base_url && !details.user_current_account.accounts.image_logo) {
            return encodeURI(details.base_url + details.logo_image);
        }
        else {
            return encodeURI(details.logo_image);
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], LogoAreaComponent.prototype, "details", void 0);
    LogoAreaComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'logo-area',
            template: __webpack_require__(/*! ./logo-area.component.html */ "./src/app/logo-area/logo-area.component.html"),
            styles: [__webpack_require__(/*! ./logo-area.component.css */ "./src/app/logo-area/logo-area.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LogoAreaComponent);
    return LogoAreaComponent;
}());



/***/ }),

/***/ "./src/app/navigation/navigation.component.css":
/*!*****************************************************!*\
  !*** ./src/app/navigation/navigation.component.css ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".nav-header {\r\n    float: right;\r\n    clear: right;\r\n    margin-top: 22px;\r\n    /* font-family: Conv_Roboto-Light, Roboto, Roboto-Light, \"Segoe UI\",Frutiger,\"Frutiger Linotype\"; */\r\n    line-height: 32px;\r\n    clear: inherit;\r\n    margin-bottom: 5px;\r\n    position: relative;\r\n    top: -3px;\r\n}\r\n\r\n\r\nnav ul, nav ol {\r\n    margin: 0;\r\n    padding: 0;\r\n    list-style: none;\r\n    list-style-image: none;\r\n}\r\n\r\n\r\n.nav li {\r\n    float: left;\r\n    margin-left: 3px;\r\n    position: relative;\r\n}\r\n\r\n\r\n.nav-header a {\r\n    display: block;\r\n    padding: 8px 11px;\r\n    background: none;\r\n    color: #cccccc;\r\n    text-shadow: none;\r\n    font-weight: normal;\r\n    font-size: 15px;\r\n    margin-top: -9px;\r\n    top: -2px;\r\n    position: relative;\r\n}\r\n\r\n\r\na{\r\n\ttransition: all .1s linear;\r\n}\r\n\r\n\r\n.nav-header a:hover {\r\n    color: #fff;\r\n    text-decoration: none;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxhQUFhO0lBQ2IsYUFBYTtJQUNiLGlCQUFpQjtJQUNqQixvR0FBb0c7SUFDcEcsa0JBQWtCO0lBQ2xCLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLFVBQVU7Q0FDYjs7O0FBR0Q7SUFDSSxVQUFVO0lBQ1YsV0FBVztJQUNYLGlCQUFpQjtJQUNqQix1QkFBdUI7Q0FDMUI7OztBQUVEO0lBQ0ksWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixtQkFBbUI7Q0FDdEI7OztBQUVEO0lBQ0ksZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixpQkFBaUI7SUFDakIsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixvQkFBb0I7SUFDcEIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixVQUFVO0lBQ1YsbUJBQW1CO0NBQ3RCOzs7QUFFRDtDQUNDLDJCQUEyQjtDQUMzQjs7O0FBQ0Q7SUFDSSxZQUFZO0lBQ1osc0JBQXNCO0NBQ3pCIiwiZmlsZSI6InNyYy9hcHAvbmF2aWdhdGlvbi9uYXZpZ2F0aW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmF2LWhlYWRlciB7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICBjbGVhcjogcmlnaHQ7XHJcbiAgICBtYXJnaW4tdG9wOiAyMnB4O1xyXG4gICAgLyogZm9udC1mYW1pbHk6IENvbnZfUm9ib3RvLUxpZ2h0LCBSb2JvdG8sIFJvYm90by1MaWdodCwgXCJTZWdvZSBVSVwiLEZydXRpZ2VyLFwiRnJ1dGlnZXIgTGlub3R5cGVcIjsgKi9cclxuICAgIGxpbmUtaGVpZ2h0OiAzMnB4O1xyXG4gICAgY2xlYXI6IGluaGVyaXQ7XHJcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IC0zcHg7XHJcbn1cclxuXHJcblxyXG5uYXYgdWwsIG5hdiBvbCB7XHJcbiAgICBtYXJnaW46IDA7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgIGxpc3Qtc3R5bGUtaW1hZ2U6IG5vbmU7XHJcbn1cclxuXHJcbi5uYXYgbGkge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBtYXJnaW4tbGVmdDogM3B4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG4ubmF2LWhlYWRlciBhIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgcGFkZGluZzogOHB4IDExcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiBub25lO1xyXG4gICAgY29sb3I6ICNjY2NjY2M7XHJcbiAgICB0ZXh0LXNoYWRvdzogbm9uZTtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICBtYXJnaW4tdG9wOiAtOXB4O1xyXG4gICAgdG9wOiAtMnB4O1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG5he1xyXG5cdHRyYW5zaXRpb246IGFsbCAuMXMgbGluZWFyO1xyXG59XHJcbi5uYXYtaGVhZGVyIGE6aG92ZXIge1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/navigation/navigation.component.html":
/*!******************************************************!*\
  !*** ./src/app/navigation/navigation.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav class=\"nav nav-header\">\r\n    <ul>\r\n        <!-- <li class=\"active\"><a [attr.href]=\"details?.base_url+'/Dashboard/home'\">Dashboard</a></li> -->\r\n         <li>           \r\n            <a [attr.href]=\"details?.base_url+'/MyFiles'\">Workspace</a>\r\n        </li>\r\n                                    \r\n        <li [class.active]=\"currentProject=='huddle'\"><a [attr.href]=\"details?.base_url+'/Huddles'\">Huddles</a></li>\r\n                                    \r\n        <li *ngIf=\"details?.library_permission==1\"><a [attr.href]=\"details?.base_url+'/VideoLibrary/'\">Library</a></li>\r\n\r\n        <li [class.active]=\"currentProject=='analytics'\" *ngIf=\"(details?.user_permissions?.UserAccount?.permission_view_analytics==1  || details?.user_current_account?.roles?.role_id==115) && !details?.user_current_account?.accounts?.in_trial\"><a [attr.href]=\"details?.base_url+'/analytics_angular/'\">Analytics</a></li>\r\n\r\n        <li *ngIf=\"isAllowedPeople()\">\r\n            <a [attr.href]=\"details?.base_url+'/users/administrators_groups'\">People</a>\r\n        </li>\r\n                                 \r\n        <li *ngIf=\"details?.user_permissions?.accounts?.enable_edtpa=='1'\"><a href=\"/Edtpa/edtpa\">edTPA</a>\r\n        </li>\r\n        </ul>\r\n</nav>"

/***/ }),

/***/ "./src/app/navigation/navigation.component.ts":
/*!****************************************************!*\
  !*** ./src/app/navigation/navigation.component.ts ***!
  \****************************************************/
/*! exports provided: NavigationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationComponent", function() { return NavigationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NavigationComponent = /** @class */ (function () {
    function NavigationComponent() {
        var that = this;
        setTimeout(function () {
            // console.log(localStorage);
            if (localStorage.current_page) {
                that.currentProject = localStorage.current_page;
            }
        }, 3000);
    }
    NavigationComponent.prototype.isAllowedPeople = function () {
        if (!this.details) {
            return false;
        }
        else {
            var roleId = this.GetRoleId();
            if (roleId == 100 || roleId == 110) {
                return true;
            }
            else if (roleId == 120 && this.details.user_current_account.users_accounts.permission_administrator_user_new_role == "1") {
                return true;
            }
            else if (roleId == 115 && this.details.user_current_account.users_accounts.permission_administrator_user_new_role == "1") {
                return true;
            }
            else {
                return false;
            }
        }
    };
    NavigationComponent.prototype.GetRoleId = function () {
        //details?.user_current_account?.roles?.role_id!=120
        return this.details.user_current_account.roles.role_id;
    };
    NavigationComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], NavigationComponent.prototype, "details", void 0);
    NavigationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'navigation',
            template: __webpack_require__(/*! ./navigation.component.html */ "./src/app/navigation/navigation.component.html"),
            styles: [__webpack_require__(/*! ./navigation.component.css */ "./src/app/navigation/navigation.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NavigationComponent);
    return NavigationComponent;
}());



/***/ }),

/***/ "./src/app/trial-message/trial-message.component.css":
/*!***********************************************************!*\
  !*** ./src/app/trial-message/trial-message.component.css ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.upgrade_link{\r\n\ttext-decoration: underline;\r\n\tcolor: #FFF;\r\n\r\n}\r\n\r\n.trial_message_quote{\r\n\tcolor: #ccc;\r\n}\r\n\r\n.trial_message{\r\n    font-size:13px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdHJpYWwtbWVzc2FnZS90cmlhbC1tZXNzYWdlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0NBQ0MsMkJBQTJCO0NBQzNCLFlBQVk7O0NBRVo7O0FBRUQ7Q0FDQyxZQUFZO0NBQ1o7O0FBQ0Q7SUFDSSxlQUFlO0NBQ2xCIiwiZmlsZSI6InNyYy9hcHAvdHJpYWwtbWVzc2FnZS90cmlhbC1tZXNzYWdlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLnVwZ3JhZGVfbGlua3tcclxuXHR0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcclxuXHRjb2xvcjogI0ZGRjtcclxuXHJcbn1cclxuXHJcbi50cmlhbF9tZXNzYWdlX3F1b3Rle1xyXG5cdGNvbG9yOiAjY2NjO1xyXG59XHJcbi50cmlhbF9tZXNzYWdle1xyXG4gICAgZm9udC1zaXplOjEzcHg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/trial-message/trial-message.component.html":
/*!************************************************************!*\
  !*** ./src/app/trial-message/trial-message.component.html ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"trial_message\" *ngIf=\"details?.user_current_account?.accounts\r\n?.in_trial\">\r\n\t<span class=\"trial_message_quote\">Trial expires {{details?.expiry_date}} </span> <a href=\"\" [attr.href]=\"details.base_url+'/accounts/account_settings_all/'+details?.user_current_account?.accounts?.account_id+'/4'\" class=\"upgrade_link\"> UPGRADE </a>\r\n</div>\r\n  \r\n"

/***/ }),

/***/ "./src/app/trial-message/trial-message.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/trial-message/trial-message.component.ts ***!
  \**********************************************************/
/*! exports provided: TrialMessageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TrialMessageComponent", function() { return TrialMessageComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var TrialMessageComponent = /** @class */ (function () {
    function TrialMessageComponent() {
    }
    TrialMessageComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TrialMessageComponent.prototype, "details", void 0);
    TrialMessageComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'trial-message',
            template: __webpack_require__(/*! ./trial-message.component.html */ "./src/app/trial-message/trial-message.component.html"),
            styles: [__webpack_require__(/*! ./trial-message.component.css */ "./src/app/trial-message/trial-message.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TrialMessageComponent);
    return TrialMessageComponent;
}());



/***/ }),

/***/ "./src/app/user/user.component.css":
/*!*****************************************!*\
  !*** ./src/app/user/user.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#user.dropdown {\r\n    position: relative;\r\n    height: 34px;\r\n}\r\n\r\n#user {\r\n    width: auto;\r\n    display: inline-block;\r\n    vertical-align: top;\r\n    zoom: 1;\r\n    float: right;\r\n}\r\n\r\n#user {\r\n    width: auto;\r\n    display: inline-block;\r\n    vertical-align: top;\r\n    zoom: 1;\r\n    float: right;\r\n}\r\n\r\n#user {\r\n    width: auto;\r\n    display: inline-block;\r\n    vertical-align: top;\r\n    zoom: 1;\r\n    float: right;\r\n}\r\n\r\n.dropdown {\r\n    position: relative;\r\n    width: 182px;\r\n    height: 34px;\r\n}\r\n\r\n.dropdown {\r\n    position: relative;\r\n    width: 182px;\r\n    height: 34px;\r\n}\r\n\r\n#user .photo {\r\n    margin-right: 10px;\r\n    padding: 2px;\r\n    box-shadow: 0 1px rgba(0,0,0,0.4);\r\n    display: inline-block;\r\n}\r\n\r\n.photo {\r\n    padding: 4px;\r\n    background-color: #fff;\r\n    box-shadow: 0 1px 3px rgba(0,0,0,0.2);\r\n    font-size: 8px;\r\n    overflow: hidden;\r\n}\r\n\r\n.ph {\r\n    float: left;\r\n}\r\n\r\n#user span.wrap {\r\n    position: relative;\r\n    top: 1px;\r\n}\r\n\r\n.span-wrap {\r\n    text-align: left;\r\n    margin-left: 0px;\r\n}\r\n\r\n#user.dropdown .dropdown-toggle {\r\n    float: right;\r\n    padding: 21px 0 20px 12px;\r\n    margin-top: 0px;\r\n    background: none;\r\n    border-radius: 3px;\r\n    box-shadow: none;\r\n    color: #f3f1ec;\r\n    font-weight: 400;\r\n    font-size: 13px;\r\n    cursor: pointer;\r\n    white-space: nowrap;\r\n    width: 100%;\r\n}\r\n\r\n#user .dropdown-toggle {\r\n    float: right;\r\n    padding: 5px 8px 5px 8px;\r\n    background: url('/huddles_dev/app/img/new/blank.png');\r\n    box-shadow: 0 1px 0 rgba(255,255,255,0.1);\r\n    color: #f3f1ec;\r\n    font-weight: bold;\r\n    font-size: 12px;\r\n    cursor: pointer;\r\n    white-space: nowrap;\r\n}\r\n\r\n#user .dropdown-toggle {\r\n    float: right;\r\n    padding: 5px 8px 5px 8px;\r\n    background: url('/huddles_dev/app/img/blank.png');\r\n    box-shadow: 0 1px 0 rgba(255,255,255,0.1);\r\n    color: #f3f1ec;\r\n    font-weight: bold;\r\n    font-size: 12px;\r\n    cursor: pointer;\r\n    white-space: nowrap;\r\n    width: 100%;\r\n}\r\n\r\n.user_newDropDown {\r\n       background: #fff;\r\n    padding: 18px 22px;\r\n    width: 356px;\r\n    position: absolute;\r\n    right: 0px;\r\n   top: 71px !important;\r\n    z-index: 20;\r\n    box-shadow: 0px 0 2px #c7c7c7;\r\n}\r\n\r\n.user_newDropDown :before {\r\n    content: '';\r\n    right: 7%;\r\n    margin-left: -9px;\r\n    bottom: 100%;\r\n    width: 0px;\r\n    height: 0px;\r\n    border-style: solid;\r\n    border-width: 0 7px 7px 7px;\r\n    border-color: transparent transparent #fff transparent;\r\n    position: absolute;\r\n    z-index: 1;\r\n}\r\n\r\n.user_dropDown_info {\r\n   float: left;\r\n    width: 155px;\r\n    word-wrap: break-word;\r\n    font-size: 13px;\r\n}\r\n\r\n.user_dropDown_info p {\r\n    display: block;\r\n    color: #424242;\r\n    font-size: 16px;\r\n    font-weight: 600;\r\n    line-height: 11px;\r\n    margin-bottom: 4px;\r\n}\r\n\r\n.user_dropDown_info span{\r\n    color: #757575;\r\n    font-size: 13px;\r\n}\r\n\r\n.account_info {\r\n        margin-top: 15px;\r\n    background: #eaeaea;\r\n    color: #5b5b5b;\r\n    padding: 6px 16px 7px 16px;\r\n    border-radius: 3px;\r\n    text-align: center;\r\n    display: block;\r\n}\r\n\r\nul, ol {\r\n    margin: 1em 0;\r\n    padding: 0 0 0 40px;\r\n}\r\n\r\n.user_newDropDown ol {\r\n    float: right;\r\n    border-left: solid 1px #eaeaea;\r\n    list-style: none;\r\n    margin: 0px;\r\n    padding-bottom: 0px;\r\n    width: 128px;\r\n    min-height: 88px;\r\n    padding-left: 18px;\r\n    margin-left: 22px !important;\r\n}\r\n\r\nol {\r\n    display: block;\r\n    list-style-type: decimal;\r\n    -webkit-margin-before: 1em;\r\n    -webkit-margin-after: 1em;\r\n    -webkit-margin-start: 0px;\r\n    -webkit-margin-end: 0px;\r\n    -webkit-padding-start: 40px;\r\n}\r\n\r\n#user .user_newDropDown ol li {\r\n    padding: 0px;\r\n}\r\n\r\n#user .user_newDropDown ol li a {\r\n    display: block;\r\n    padding: 0px 0px;\r\n    color: #515151;\r\n    font-size: 12px;\r\n    font-weight: normal;\r\n    line-height: 27px;\r\n}\r\n\r\n#user .user_newDropDown ol li a:hover {\r\n    color: #2074bd;\r\n    background: none;\r\n    text-decoration: none;\r\n}\r\n\r\n.account_info {\r\n    display: block;\r\n}\r\n\r\n.user_info_bottom {\r\n       background: #eaeaea;\r\n    color: #5b5b5b;\r\n    margin: 19px -22px -20px -22px;\r\n    cursor: pointer;\r\n   padding: 7px 4px 9px 9px;\r\n}\r\n\r\n.user_newDropDown a:hover{\r\n    text-decoration: none;\r\n}\r\n\r\n.user_info_bottom span{\r\n    font-size: 13px;\r\n}\r\n\r\n.user_info_bottom img{\r\n    width: 15px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNlci91c2VyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxtQkFBbUI7SUFDbkIsYUFBYTtDQUNoQjs7QUFFRDtJQUNJLFlBQVk7SUFDWixzQkFBc0I7SUFDdEIsb0JBQW9CO0lBQ3BCLFFBQVE7SUFDUixhQUFhO0NBQ2hCOztBQUNEO0lBQ0ksWUFBWTtJQUNaLHNCQUFzQjtJQUN0QixvQkFBb0I7SUFDcEIsUUFBUTtJQUNSLGFBQWE7Q0FDaEI7O0FBQ0Q7SUFDSSxZQUFZO0lBQ1osc0JBQXNCO0lBQ3RCLG9CQUFvQjtJQUNwQixRQUFRO0lBQ1IsYUFBYTtDQUNoQjs7QUFDRDtJQUNJLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsYUFBYTtDQUNoQjs7QUFDRDtJQUNJLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2IsYUFBYTtDQUNoQjs7QUFDRDtJQUNJLG1CQUFtQjtJQUNuQixhQUFhO0lBQ2Isa0NBQWtDO0lBQ2xDLHNCQUFzQjtDQUN6Qjs7QUFDRDtJQUNJLGFBQWE7SUFDYix1QkFBdUI7SUFDdkIsc0NBQXNDO0lBQ3RDLGVBQWU7SUFDZixpQkFBaUI7Q0FDcEI7O0FBQ0Q7SUFDSSxZQUFZO0NBQ2Y7O0FBRUQ7SUFDSSxtQkFBbUI7SUFDbkIsU0FBUztDQUNaOztBQUNEO0lBQ0ksaUJBQWlCO0lBQ2pCLGlCQUFpQjtDQUNwQjs7QUFDRDtJQUNJLGFBQWE7SUFDYiwwQkFBMEI7SUFDMUIsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixtQkFBbUI7SUFDbkIsaUJBQWlCO0lBQ2pCLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixvQkFBb0I7SUFDcEIsWUFBWTtDQUNmOztBQUVEO0lBQ0ksYUFBYTtJQUNiLHlCQUF5QjtJQUN6QixzREFBd0M7SUFDeEMsMENBQTBDO0lBQzFDLGVBQWU7SUFDZixrQkFBa0I7SUFDbEIsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtJQUNoQixvQkFBb0I7Q0FDdkI7O0FBQ0Q7SUFDSSxhQUFhO0lBQ2IseUJBQXlCO0lBQ3pCLGtEQUFvQztJQUNwQywwQ0FBMEM7SUFDMUMsZUFBZTtJQUNmLGtCQUFrQjtJQUNsQixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0lBQ2hCLG9CQUFvQjtJQUNwQixZQUFZO0NBQ2Y7O0FBRUQ7T0FDTyxpQkFBaUI7SUFDcEIsbUJBQW1CO0lBQ25CLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsV0FBVztHQUNaLHFCQUFxQjtJQUNwQixZQUFZO0lBQ1osOEJBQThCO0NBQ2pDOztBQUNEO0lBQ0ksWUFBWTtJQUNaLFVBQVU7SUFDVixrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLFdBQVc7SUFDWCxZQUFZO0lBQ1osb0JBQW9CO0lBQ3BCLDRCQUE0QjtJQUM1Qix1REFBdUQ7SUFDdkQsbUJBQW1CO0lBQ25CLFdBQVc7Q0FDZDs7QUFFRDtHQUNHLFlBQVk7SUFDWCxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLGdCQUFnQjtDQUNuQjs7QUFFRDtJQUNJLGVBQWU7SUFDZixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsbUJBQW1CO0NBQ3RCOztBQUNEO0lBQ0ksZUFBZTtJQUNmLGdCQUFnQjtDQUNuQjs7QUFDRDtRQUNRLGlCQUFpQjtJQUNyQixvQkFBb0I7SUFDcEIsZUFBZTtJQUNmLDJCQUEyQjtJQUMzQixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGVBQWU7Q0FDbEI7O0FBRUQ7SUFDSSxjQUFjO0lBQ2Qsb0JBQW9CO0NBQ3ZCOztBQUVEO0lBQ0ksYUFBYTtJQUNiLCtCQUErQjtJQUMvQixpQkFBaUI7SUFDakIsWUFBWTtJQUNaLG9CQUFvQjtJQUNwQixhQUFhO0lBQ2IsaUJBQWlCO0lBQ2pCLG1CQUFtQjtJQUNuQiw2QkFBNkI7Q0FDaEM7O0FBRUQ7SUFDSSxlQUFlO0lBQ2YseUJBQXlCO0lBQ3pCLDJCQUEyQjtJQUMzQiwwQkFBMEI7SUFDMUIsMEJBQTBCO0lBQzFCLHdCQUF3QjtJQUN4Qiw0QkFBNEI7Q0FDL0I7O0FBRUQ7SUFDSSxhQUFhO0NBQ2hCOztBQUNEO0lBQ0ksZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLG9CQUFvQjtJQUNwQixrQkFBa0I7Q0FDckI7O0FBQ0Q7SUFDSSxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLHNCQUFzQjtDQUN6Qjs7QUFFRDtJQUNJLGVBQWU7Q0FDbEI7O0FBQ0Q7T0FDTyxvQkFBb0I7SUFDdkIsZUFBZTtJQUNmLCtCQUErQjtJQUMvQixnQkFBZ0I7R0FDakIseUJBQXlCO0NBQzNCOztBQUVEO0lBQ0ksc0JBQXNCO0NBQ3pCOztBQUVEO0lBQ0ksZ0JBQWdCO0NBQ25COztBQUVEO0lBQ0ksWUFBWTtDQUNmIiwiZmlsZSI6InNyYy9hcHAvdXNlci91c2VyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjdXNlci5kcm9wZG93biB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBoZWlnaHQ6IDM0cHg7XHJcbn1cclxuXHJcbiN1c2VyIHtcclxuICAgIHdpZHRoOiBhdXRvO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcclxuICAgIHpvb206IDE7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbn1cclxuI3VzZXIge1xyXG4gICAgd2lkdGg6IGF1dG87XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG4gICAgem9vbTogMTtcclxuICAgIGZsb2F0OiByaWdodDtcclxufVxyXG4jdXNlciB7XHJcbiAgICB3aWR0aDogYXV0bztcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHZlcnRpY2FsLWFsaWduOiB0b3A7XHJcbiAgICB6b29tOiAxO1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcbi5kcm9wZG93biB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB3aWR0aDogMTgycHg7XHJcbiAgICBoZWlnaHQ6IDM0cHg7XHJcbn1cclxuLmRyb3Bkb3duIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHdpZHRoOiAxODJweDtcclxuICAgIGhlaWdodDogMzRweDtcclxufVxyXG4jdXNlciAucGhvdG8ge1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgcGFkZGluZzogMnB4O1xyXG4gICAgYm94LXNoYWRvdzogMCAxcHggcmdiYSgwLDAsMCwwLjQpO1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcbi5waG90byB7XHJcbiAgICBwYWRkaW5nOiA0cHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG4gICAgYm94LXNoYWRvdzogMCAxcHggM3B4IHJnYmEoMCwwLDAsMC4yKTtcclxuICAgIGZvbnQtc2l6ZTogOHB4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG4ucGgge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbn1cclxuXHJcbiN1c2VyIHNwYW4ud3JhcCB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDFweDtcclxufVxyXG4uc3Bhbi13cmFwIHtcclxuICAgIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG59XHJcbiN1c2VyLmRyb3Bkb3duIC5kcm9wZG93bi10b2dnbGUge1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgcGFkZGluZzogMjFweCAwIDIwcHggMTJweDtcclxuICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbiAgICBib3gtc2hhZG93OiBub25lO1xyXG4gICAgY29sb3I6ICNmM2YxZWM7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4jdXNlciAuZHJvcGRvd24tdG9nZ2xlIHtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIHBhZGRpbmc6IDVweCA4cHggNXB4IDhweDtcclxuICAgIGJhY2tncm91bmQ6IHVybCgvYXBwL2ltZy9uZXcvYmxhbmsucG5nKTtcclxuICAgIGJveC1zaGFkb3c6IDAgMXB4IDAgcmdiYSgyNTUsMjU1LDI1NSwwLjEpO1xyXG4gICAgY29sb3I6ICNmM2YxZWM7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbn1cclxuI3VzZXIgLmRyb3Bkb3duLXRvZ2dsZSB7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICBwYWRkaW5nOiA1cHggOHB4IDVweCA4cHg7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoL2FwcC9pbWcvYmxhbmsucG5nKTtcclxuICAgIGJveC1zaGFkb3c6IDAgMXB4IDAgcmdiYSgyNTUsMjU1LDI1NSwwLjEpO1xyXG4gICAgY29sb3I6ICNmM2YxZWM7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLnVzZXJfbmV3RHJvcERvd24ge1xyXG4gICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIHBhZGRpbmc6IDE4cHggMjJweDtcclxuICAgIHdpZHRoOiAzNTZweDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAwcHg7XHJcbiAgIHRvcDogNzFweCAhaW1wb3J0YW50O1xyXG4gICAgei1pbmRleDogMjA7XHJcbiAgICBib3gtc2hhZG93OiAwcHggMCAycHggI2M3YzdjNztcclxufSAgXHJcbi51c2VyX25ld0Ryb3BEb3duIDpiZWZvcmUge1xyXG4gICAgY29udGVudDogJyc7XHJcbiAgICByaWdodDogNyU7XHJcbiAgICBtYXJnaW4tbGVmdDogLTlweDtcclxuICAgIGJvdHRvbTogMTAwJTtcclxuICAgIHdpZHRoOiAwcHg7XHJcbiAgICBoZWlnaHQ6IDBweDtcclxuICAgIGJvcmRlci1zdHlsZTogc29saWQ7XHJcbiAgICBib3JkZXItd2lkdGg6IDAgN3B4IDdweCA3cHg7XHJcbiAgICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50IHRyYW5zcGFyZW50ICNmZmYgdHJhbnNwYXJlbnQ7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB6LWluZGV4OiAxO1xyXG59IFxyXG5cclxuLnVzZXJfZHJvcERvd25faW5mbyB7XHJcbiAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgd2lkdGg6IDE1NXB4O1xyXG4gICAgd29yZC13cmFwOiBicmVhay13b3JkO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcblxyXG4udXNlcl9kcm9wRG93bl9pbmZvIHAge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBjb2xvcjogIzQyNDI0MjtcclxuICAgIGZvbnQtc2l6ZTogMTZweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBsaW5lLWhlaWdodDogMTFweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDRweDtcclxufVxyXG4udXNlcl9kcm9wRG93bl9pbmZvIHNwYW57XHJcbiAgICBjb2xvcjogIzc1NzU3NTtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxufVxyXG4uYWNjb3VudF9pbmZvIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gICAgYmFja2dyb3VuZDogI2VhZWFlYTtcclxuICAgIGNvbG9yOiAjNWI1YjViO1xyXG4gICAgcGFkZGluZzogNnB4IDE2cHggN3B4IDE2cHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAzcHg7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxudWwsIG9sIHtcclxuICAgIG1hcmdpbjogMWVtIDA7XHJcbiAgICBwYWRkaW5nOiAwIDAgMCA0MHB4O1xyXG59XHJcblxyXG4udXNlcl9uZXdEcm9wRG93biBvbCB7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICBib3JkZXItbGVmdDogc29saWQgMXB4ICNlYWVhZWE7XHJcbiAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgbWFyZ2luOiAwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4gICAgd2lkdGg6IDEyOHB4O1xyXG4gICAgbWluLWhlaWdodDogODhweDtcclxuICAgIHBhZGRpbmctbGVmdDogMThweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAyMnB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbm9sIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgbGlzdC1zdHlsZS10eXBlOiBkZWNpbWFsO1xyXG4gICAgLXdlYmtpdC1tYXJnaW4tYmVmb3JlOiAxZW07XHJcbiAgICAtd2Via2l0LW1hcmdpbi1hZnRlcjogMWVtO1xyXG4gICAgLXdlYmtpdC1tYXJnaW4tc3RhcnQ6IDBweDtcclxuICAgIC13ZWJraXQtbWFyZ2luLWVuZDogMHB4O1xyXG4gICAgLXdlYmtpdC1wYWRkaW5nLXN0YXJ0OiA0MHB4O1xyXG59XHJcblxyXG4jdXNlciAudXNlcl9uZXdEcm9wRG93biBvbCBsaSB7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuI3VzZXIgLnVzZXJfbmV3RHJvcERvd24gb2wgbGkgYSB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHBhZGRpbmc6IDBweCAwcHg7XHJcbiAgICBjb2xvcjogIzUxNTE1MTtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICBsaW5lLWhlaWdodDogMjdweDtcclxufVxyXG4jdXNlciAudXNlcl9uZXdEcm9wRG93biBvbCBsaSBhOmhvdmVyIHtcclxuICAgIGNvbG9yOiAjMjA3NGJkO1xyXG4gICAgYmFja2dyb3VuZDogbm9uZTtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxufVxyXG5cclxuLmFjY291bnRfaW5mbyB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufVxyXG4udXNlcl9pbmZvX2JvdHRvbSB7XHJcbiAgICAgICBiYWNrZ3JvdW5kOiAjZWFlYWVhO1xyXG4gICAgY29sb3I6ICM1YjViNWI7XHJcbiAgICBtYXJnaW46IDE5cHggLTIycHggLTIwcHggLTIycHg7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgIHBhZGRpbmc6IDdweCA0cHggOXB4IDlweDtcclxufVxyXG5cclxuLnVzZXJfbmV3RHJvcERvd24gYTpob3ZlcntcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxufVxyXG5cclxuLnVzZXJfaW5mb19ib3R0b20gc3BhbntcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxufVxyXG5cclxuLnVzZXJfaW5mb19ib3R0b20gaW1ne1xyXG4gICAgd2lkdGg6IDE1cHg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/user/user.component.html":
/*!******************************************!*\
  !*** ./src/app/user/user.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"user\" class=\"dropdown\" dropdown>\r\n  <div dropdownToggle class=\"dropdown-toggle\">\r\n\r\n    <img [attr.src]=\"getAvatarPath(details)\" id=\"user_top_photo\" class=\"photo ph\" height=\"25\" rel=\"tooltip\" width=\"25\" data-original-title=\"\" title=\"\">\r\n    <span class=\"span-wrap wrap\" style=\"text-overflow: ellipsis;overflow: hidden;white-space: nowrap;height: 18px;float:left;\">{{details?.user_current_account?.User?.first_name}}</span>\r\n\r\n  </div>\r\n  <div *dropdownMenu class=\"dropdown-menu dropdown-menu-right user_newDropDown\" role=\"menu\">\r\n    <div class=\"user_dropDown_info\">\r\n    \t<p>{{details?.user_current_account?.User?.first_name+\" \"+details?.user_current_account?.User?.last_name}}</p>\r\n    \t<span>{{details?.user_current_account?.User?.email}}</span>\r\n    \t<span class=\"account_info\">{{details?.user_current_account?.roles?.name}}</span>\r\n\r\n    \t\r\n    </div>\r\n    <ol>\r\n        <li><a href=\"\" [attr.href]=\"details?.base_url+'/Users/editUser/'+details?.user_current_account\r\n?.User?.id\">User Settings</a></li>\r\n       <li *ngIf=\"SettingsAllowed(details)\"><a [attr.href]=\"details?.base_url+'/accounts/account_settings_main/'+details?.user_current_account\r\n        ?.accounts?.account_id\">Account Settings</a></li>\r\n                 \r\n     \t<!-- <li *ngIf=\"details?.is_change_account_enable==1\"><a [attr.href]=\"details?.base_url+'/launchpad'\">Launchpad</a></li> -->\r\n                 \r\n        <li><a [attr.href]=\"details?.base_url+'/users/logout'\">Logout</a></li>\r\n        </ol>\r\n\r\n        <div class=\"clear\"></div>\r\n\r\n        <a href=\"/launchpad\" *ngIf=\"details?.is_change_account_enable==1\">\r\n\t\t\t<div class=\"user_info_bottom\">\r\n\t\t\t\t<span>{{details?.user_current_account?.accounts?.company_name}}</span>\r\n\t\t\t\t<img width=\"19\" src=\"assets/img/new/icon-switch-account_grey.svg\">\r\n\t\t\t</div>\r\n        </a>\r\n\r\n        <a href=\"#\" *ngIf=\"details?.is_change_account_enable!=1\">\r\n\t\t\t<div class=\"user_info_bottom\">\r\n\t\t\t\t<span>{{details?.user_current_account?.accounts?.company_name}}</span>\r\n\t\t\t</div>\r\n        </a>\r\n        \r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/user/user.component.ts":
/*!****************************************!*\
  !*** ./src/app/user/user.component.ts ***!
  \****************************************/
/*! exports provided: UserComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserComponent", function() { return UserComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var UserComponent = /** @class */ (function () {
    function UserComponent() {
    }
    UserComponent.prototype.ngOnInit = function () {
    };
    UserComponent.prototype.getAvatarPath = function (details) {
        if (details.user_current_account.User.image) {
            return details.avatar_path;
        }
        else {
            return details.base_url + "/img/home/photo-default.png";
        }
        //
    };
    UserComponent.prototype.SettingsAllowed = function (details) {
        return details.user_permissions.roles.role_id == 100;
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], UserComponent.prototype, "details", void 0);
    UserComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'user',
            template: __webpack_require__(/*! ./user.component.html */ "./src/app/user/user.component.html"),
            styles: [__webpack_require__(/*! ./user.component.css */ "./src/app/user/user.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], UserComponent);
    return UserComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment;
var env = window.location.hostname.substring(0, window.location.hostname.indexOf(".")).toUpperCase();
env = env || "QA"; //in case of localhost
var relativePaths = {
    "APP": {
        production: true,
        APIbaseUrl: "https://api.sibme.com",
        baseHeaderUrl: "https://app.sibme.com/api",
        deploymentPath: 'app/webroot/video_details',
        baseUrl: "https://app.sibme.com",
        fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
        container: "sibme-production",
        appPath: "https://app.sibme.com",
        appUrl: "http://sibme.com",
        imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users"
    },
    "QA": {
        production: false,
        APIbaseUrl: "https://qaapi.sibme.com",
        baseHeaderUrl: "https://qa.sibme.com/api",
        deploymentPath: 'app/webroot/video_details',
        baseUrl: "https://qa.sibme.com",
        fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
        container: "sibme-production",
        appPath: "https://qa.sibme.com",
        appUrl: "http://sibme.com",
        imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users"
    },
    "STAGING": {
        production: false,
        APIbaseUrl: "https://stagingapi.sibme.com",
        baseHeaderUrl: "https://staging.sibme.com/api",
        deploymentPath: 'app/webroot/video_details',
        baseUrl: "https://staging.sibme.com",
        fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
        container: "sibme-production",
        appPath: "https://staging.sibme.com",
        appUrl: "http://sibme.com",
        imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users"
    }
};
environment = relativePaths[env];
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! Z:\sibme_prod_optimization\webroot\angular_projects\huddle-landing-page\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map