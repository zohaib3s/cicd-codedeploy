$(document).ready(function () {
    $('#observation-date-picker').datepicker({
        inline: true
    });
    $("#frm_new_observation").submit(function (event) {
        if ($('#txthuddles').val() == '') {
            $('#txthuddles').parent().addClass('error');
            $('#txthuddles').addClass('error');
            event.preventDefault();
        } else {
            $('#txthuddles').parent().removeClass('error');
            $('#txthuddles').removeClass('error');
        }
    });
    $("#frm_new_observation_fromhuddle").submit(function (event) {
        if ($('#txthuddles').val() == '') {
            $('#txthuddles').parent().addClass('error');
            $('#txthuddles').addClass('error');
            event.preventDefault();
            return false;
        } else {
            $('#txthuddles').parent().removeClass('error');
            $('#txthuddles').removeClass('error');
        }
    });
    var problemElement;
    $("#frm_new_observation").validate({
        errorPlacement: function (error, element) {
        }
    });
    $("#frm_new_observation_fromhuddle").validate({
        errorPlacement: function (error, element) {
        }
    });
    $('#btnAddToAccount2').click(function () {
        addToAccountObservation();
    });
    $('#backurl').on('click', function () {
        $('tabbox5').show();
    });

    $('#observation-time-picker').timepicker();

    $('#btnSaveHuddle').on('click', function (e) {
        var selected = [''];
        $('#list-containers input[type=checkbox]').each(function () {
            if ($(this).is(":checked")) {
                selected.push($(this).attr('name'));
            }
        });
        if (selected.length == 1) {
            $('.observee-table-header-main').addClass('error');
            e.preventDefault();
        } else {
            $('.observee-table-header-main').removeClass('error');
        }

    });

});
/*
 *  Search through name 
 *  show records on the dropdown list at huddless textfield
 *  start here
 */
$(document).on('keyup', '#txthuddles', function () {
    var txtname = $(this).val();
    var account_id = $('#account_id').val();
    $(this).parent().removeClass('error');
    $(this).removeClass('error');
    if (txtname != '') {
        $.ajax({
            type: 'POST',
            data: {
                mode: 'list',
                title: txtname,
                observationAjax: 'true'
            },
            url: home_url + '/Huddles/search',
            success: function (response) {
                //console.log(response);
                if (response != '') {
                    var results = jQuery.parseJSON(response);
                    var htmlapp = '';
//                    console.dir(results);
                    $.each(results.video_huddles, function (index, values) {
                        htmlapp += "<li>";
                        htmlapp += "<a name='" + values.AccountFolder.name + "' ac_fl_id='" + values.AccountFolder.account_folder_id + "'><b>" + values.AccountFolder.name + "</b><p>" + values.AccountFolder.desc + "</p></a>";
                        htmlapp += "</li>";
                    });
                    $('#ddlhuddles').html(htmlapp);
                    $('#shhuddles').addClass('active');
                }
            }
        });
    }

});
/*
 * end here
 */
/*
 * onclick of search values 
 * send the name in huddle box and call for observee and obserers
 * start here 
 */
$(document).on('click', '#shhuddles li a', function () {
    var name = $(this).attr("name");
    var ac_fl_id = $(this).attr("ac_fl_id");
    $('#accflid').val(ac_fl_id);
    $('#txthuddles').val(name);
    $('#shhuddles').removeClass('active');

    $.ajax({
        type: 'POST',
        data: {ac_fl_id: ac_fl_id},
        url: home_url + '/Observe/gethuddleuser',
        success: function (response) {
            var results = jQuery.parseJSON(response);
            var option_observee = '';
            var option_observer = '';
            if (results[0] != 'false') {
                option_observee += "<option value='' selected='selected'>";
                option_observee += "Select Observee";
                option_observee += "</option>";
                $.each(results, function (index, values) {
                    option_observee += "<option value='" + values.User.id + "'>";
                    option_observee += values.User.first_name + ' ' + values.User.last_name;
                    option_observee += "</option>";
                });
                $.each(results, function (index, values) {
                    option_observer += "<li id='" + values.User.id + "'>";
                    if (current_user_id == values.User.id) {
                        option_observer += '<input class="viewer-user" onclick="return false;" type="checkbox" value="' + values.User.id + '" name="group_ids[]" id="thing' + values.User.id + '" style="float: left;" checked=""><label for="thing' + values.User.id + '"><a style="color: #757575; font-weight: normal;">' + values.User.first_name + ' ' + values.User.last_name + '</a></label>';
                    } else {
                        option_observer += '<input class="viewer-user chkbox" type="checkbox" value="' + values.User.id + '" name="group_ids[]" id="thing' + values.User.id + '" style="float: left;"> <label for="thing' + values.User.id + '"><a style="color: #757575; font-weight: normal;">' + values.User.first_name + ' ' + values.User.last_name + '</a></label>';
                    }
                    var role_id = '';
                    if (values.huddle_users.role_id == '200') {
                        role_id = 'Admin';
                    }
                    else if (values.huddle_users.role_id == '210') {
                        role_id = 'Member';
                    }
                    else {
                        role_id = 'Viewer';
                    }
                    option_observer += '<div class="permissions"><label for="group_role_76_200"> ' + role_id + '</label>';
                    option_observer += '</li>';
                });
            }
            else {
                option_observee += "<option value=''>";
                option_observee += "No Observee";
                option_observee += "</option>";
                option_observer += "<li>";
                option_observer += '<label><a style="color: #757575; font-weight: normal;">No Observer Avaliable.</a></label>';
                option_observer += '</li>';
            }
            $('#sel_observee').html(option_observee);
            $('#list-containers').html(option_observer);
            $(".widget-scrollable").tinyscrollbar();
        }
    });


});

/*
 * end here
 */
/*
 * selected observee hidden in observer list
 * start here
 */
$(document).on('change', '#sel_observee', function () {
    var user_id = $(this).val();
    $('#list-containers li').removeClass('remove');
    $('li#' + user_id).addClass('remove');
    $('li#' + user_id + ' input').attr('checked', false);
    $('.scrollbar').removeClass('disable');
});

/*
 * end here
 */

$(document).ready(function () {
    $('.select-all-none').on('click', function () {
        if ($('#selectAll').is(':checked')) {
            $('#selectAll').each(function () {
                this.checked = false;
            });
            $('.chkbox').each(function () {
                this.checked = false;
            });
        } else {
            $('#selectAll').each(function () {
                this.checked = true;
            });
            $('.chkbox').each(function () {
                this.checked = true;
            });
        }
    });
});

function addToAccountObservation() {
    var lposted_data = [];
    var userNames = document.getElementsByName('users[][name]');
    var userEmails = document.getElementsByName('users[][email]');
    var i = 0;
    for (i = 0; i < userNames.length; i++) {
        var userEmail = $(userEmails[i]);
        if (is_email_exists(userEmail.val())) {
            alert('A new user with this email is already added.');
            return false;
        }
    }

    for (i = 0; i < userNames.length; i++) {
        var userName = $(userNames[i]);
        var userEmail = $(userEmails[i]);
        if (userName.val() != '' || userEmail.val() != '') {
            if (userName.val() == '') {
                alert('Please enter a valid Full Name.');
                userName.focus();
                return false;
            }
            if (userEmail.val() == '') {
                alert('Please enter a valid Email.');
                userEmail.focus();
                return false;
            }
            if (is_email_exists(userEmail.val())) {
                alert('A new user with this email is already added.');
                return false;
            }
            var newUser = [];
            newUser[newUser.length] = '';
            newUser[newUser.length] = userName.val();
            newUser[newUser.length] = userEmail.val();
            lposted_data[lposted_data.length] = newUser;
        }
    }

    if (lposted_data.length == 0) {
        alert('Please enter atleast one Full name or email.');
        return;
    }
    var account_folder_id = $('#accflid').val();
    if (account_folder_id.length == '') {
        alert('Please select the Huddle First.');
        return false;
    }

    $.ajax({
        type: 'POST',
        data: {
            user_data: lposted_data, account_folder_id: account_folder_id

        },
        url: home_url + '/Observe/checkuserinhuddle',
        success: function (response) {
            if (response != '') {
                $('#flashMessage2')
                        .css('display', 'block')
                        .html(response);
            } else {
                $('#flashMessage2').css('display', 'none');

                //new_ids -= 1;
                for (var i = 0; i < lposted_data.length; i++) {
                    var data = lposted_data[i];
                    var html = '';
                    html += '<li>'
                            + '<input class="viewer-user chkbox"  type="checkbox" value="' + data.join('::') + '" name="group_ids[]" id="thing' + data.join('::') + '" style="float: left;" checked="">' +
                            '<label for="thing' + data.join('::') + '"><a style="color: #757575; font-weight: normal;">' + data[1] + '</a></label>' +
                            '<div class="permissions"><label for="group_role_76_200">Member</label>' +
                            '</li>';
                    $('#list-containers').append(html);
                    $(".widget-scrollable").tinyscrollbar();
                }
                $('#addSuperAdminModal').modal('hide');
            }
        },
        errors: function (response) {
            alert(response.contents);
        }

    });
}

function is_email_exists(email) {

    for (var i = 0; i < posted_data.length; i++) {

        var posted_user = posted_data[i];
        if (posted_user[1] == email)
            return true;
    }

    return false;
}
var posted_data = [];

