$(document).ready(function ()
{
    var $scrollbar1 = $("#scrollbar1");
    $scrollbar1.tinyscrollbar();

    var $scrollbar2 = $("#scrollbar2");
    $scrollbar2.tinyscrollbar();

    var $scrollbar3 = $("#scrollbar3");
    $scrollbar3.tinyscrollbar();

    //FOR TEXT AREA #TAG OVER LAY
    $('#comment_comment').overlay([
            {
                    match: /\B#\w+\S+/g,
                    css: {
                            'background-color': '#d8dfea'
                    }
            }
    ]);
    $('.input-group').find('textarea').overlay([
            {
                    match: /\B#\w+\S+/g,
                    css: {
                            'background-color': '#d8dfea'
                    }
            }
    ]);
    // FOR FRAME WORK TAG LIST
    $('#expList').find('li:has(ul)')
            .click(function (event) {
                if (this == event.target) {
                    $(this).toggleClass('expanded');
                    $(this).children('ul').toggle('medium');
                }
                $scrollbar3.tinyscrollbar();
                return false;
            })
            .addClass('collapsed')
            .children('ul').hide();

    //FOR TAG SEARCH
    $('#txtSearchTags').on('keyup', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            if ($(this).val() != '') {
                $("#listContainer li").unhighlight();
                $("#listContainer li").highlight($(this).val());
                $('#expList').find('li:has(ul)').each(function () {
                    if (!($(this).hasClass('expanded'))) {
                        $(this).trigger('click');
                    }
                });
//                $("#listContainer li .highlight").parent().each(function (index, element) {
//                    if (!($(this).hasClass('expanded'))) {
//                        $(this).trigger('click');
//                    }
//                });
            } else {
                $("#listContainer li").unhighlight();
            }
            $scrollbar3.tinyscrollbar();
        }
    });
    $('#clearSearchTags').on('click', function () {
        $('#txtSearchTags').val('');
        $("#listContainer li .highlight").parent().each(function (index, element) {
            if ($(this).hasClass('expanded')) {
                $(this).trigger('click');
            }
        });
        $("#listContainer li").unhighlight();
        $scrollbar3.tinyscrollbar();
    });
    $('#clearComment').on('click',function(){
        $('#comment_comment').text('');
        $('#comment_comment').val('');
    });
});
function filterComments(filterTagCode) {
    //ADD THE TAG TO THE FILTER CLOUD
    var tagFilterHTML = '<div id="tagFilterContainer" class="btnwraper">'
            + '<button class="btn btn-lilghtgray" id="tagLine">'
            + filterTagCode
            + '</button>'
            + '<a href="javascript:void(0)" onClick="removeCommentsFilter($(this))">'
            + '<img alt="" src="/app/img/crossb.png" class="crossBtn">'
            + '</a>'
            + '</div>';
    var count = 0;
    $("#tagFilterList .btnwraper").each(function (index, element) {
        count++;
    });
    if (count > 0) {
        var $isInList = false;
        var $foundIn;
        $("#tagFilterList .btnwraper").each(function (index, element) {
            if ($(this).text() == filterTagCode) {
                $isInList = true;
                $foundIn = $(this)
            }
        });
        if ($isInList) {
            $foundIn.find('#tagLine').stop().css("border-color", "red")
                    .animate({'border-color': "#b4b4b4"}, 1500);
        } else {
            $('#tagFilterList').append(tagFilterHTML);
        }
    } else {
        $('#tagFilterList').append(tagFilterHTML);
    }

    //MAKE THE FILTER ARRAY FOR AJAX CALL
    var tagsArray = makeTagArray();

    //UPDATE THE COMMENTS WITH FILTERED RESULTS
    updateComments(tagsArray)
}
function removeCommentsFilter(filterButton) {
    filterButton.parent().remove();
    tagsArray = makeTagArray();
    updateComments(tagsArray)
}

function updateComments($tagArray) {
    ob_filtered_getVideoComments($tagArray);
}

function makeTagArray() {
    var $tagArray = [];
    $("#tagFilterList .btnwraper").each(function (index, element) {
        $tagArray.push($(this).text());
    });
    return $tagArray;
}

function openTagsDialog($tagText) {
    $('#pop-up-btn').trigger('click');
    $('#txtSearchTags').val($tagText);
    $("#listContainer li").unhighlight();
    $("#listContainer li").highlight($tagText);
    $('#expList').find('li:has(ul)').each(function () {
        if (!($(this).hasClass('expanded'))) {
            $(this).trigger('click');
        }
    });
}
function updateTagCloud() {
    $('#scrollbar2').find('#tagCloudContainer').html('');
    $('#vidComments_obs').find('.hashTagClickAble').each(function () {
        var tagText = $(this).text();
        var $isInList = false;
        var $foundIn;
        $('#scrollbar2').find('#tagCloudContainer a').each(function (index, element) {
            if ($(this).text() == tagText) {
                $isInList = true;
            }
        });
        if (!($isInList)) {
            $('#scrollbar2').find('#tagCloudContainer').append('<div class="tagsselect"><a href="javascript:filterComments(\'' + $(this).text() + '\')">' + $(this).text() + '</a></div>')
        }
    });
}
function hideAddEvidenceVideoBtn(){
    $('#obVideoUpload').hide();
}