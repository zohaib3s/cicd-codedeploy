
$(function() {
    var Huddles = {};

    Huddles.updateComment = function(button, container, id, comment) {
        var title = $('#comment_title').val(),
                parent_id = $('#discussion_parent_id').val(),
                data
                ;
        if (title == '') {
            alert('Title Field Required.');
            return false;
        }
        if (window.FormData) {
            data = new FormData();
            data.append('comment', $.trim(comment));
            data.append('title', title);
            data.append('discussion_parent_id', parent_id);
            data.append('remove_attachment', container.find('.js-remove-attachment').val());

            var files = container.find('input[type=file]');
            files.each(function(idx, el) {
                if (el.files.length == 0)
                    return;
                for (var i = 0; i < el.files.length; i++)
                    data.append('attachment[]', el.files[i]);
            });
        } else {
            data = {
                comment: jQuery.trim(comment),
                title: title,
                discussion_parent_id: parent_id
            };
        }
        button.html('Updating <span class="loading"><span></span><span></span><span></span></span>');
        $.ajax({
            type: 'POST',
            url: home_url + '/Huddles/editComment/' + id,
            data: data,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function(res) {
                if (res.ok) {
                    $('#discussion-title-box').html(title).show();
                    $('#discussion-title-input').hide();

                    Huddles.updateDocumentList(container, res.documents,id);

                    container.find('.discussion-editor-row-content').html(res.content);
                    container.find('.edit-controls').hide();
                    container.find('.view-controls').show();

                    button.html('Update');
                } else {
                    alert('Update discussion fail');
                }
            },
            errors: function(response) {
                alert(response.contents);
            }
        });
    };

    Huddles.updateDocumentList = function(container, documents,comment_id) {
        var list = container.find('.js-attachment-list');
        var download_button_img_path = $('#download_button_img_path').val();
        list.html('');
        for (var i = 0; i < documents.length; i++) {
            var li = $('<li></li>');
            var link = home_url + '/Huddles/download/' + documents[i].id;
            var icon = $('<img class="icon24" src="' + Huddles.getDocTypeIconLink(documents[i].name) + '" />')
            var a = $('<a id="url_generator_'+ documents[i].id +'" href="' + link + '"> </a>');
            var remove = $('<i class="remove edit-controls" style="display:none;">remove</i>');
            var id = $('<input type="hidden" name="document_id" value="' + documents[i].id + '" />');
            var cross_button = $('<span del_document_id="' + documents[i].id + '" style="display:none;cursor: pointer;font-weight: 400;text-decoration: underline;color: red;position: relative;width:auto;" class = "delete_doc'+ comment_id +'" id="delete_discussion_file_' + documents[i].id + '">Remove</span>');
            var download_button = $('<span style="width:auto;"><a style ="width:auto; cursor: pointer;" download href="'+ link +'"><img alt="Download" class="right smargin-right" style="height: 24px;" rel="tooltip" src="'+ download_button_img_path +'" title="download document"  /></a></span>');
            a.append(icon);
            a.append('<span style="width:auto;">' + documents[i].name + '</span>');
            li.append(a);
            li.append(remove);
            li.append(id);
            li.append(cross_button);
            li.append(download_button);

            remove.click(Huddles.removeFile);

            list.append(li);
        }
    };

    Huddles.getDocTypeIconLink = function(filename) {
        var ext = filename.split('.').pop();
        switch (ext) {
            case 'doc':
            case 'docx':
                $link = 'img/icons/wd48.png';
                break;
            case 'xls':
            case 'xlsx':
                $link = 'img/icons/excel48.png';
                break;
            case 'pdf':
                $link = 'img/icons/pdf48.png';
                break;
            case 'png':
            case 'gif':
            case 'jpg':
            case 'jpeg':
                $link = 'img/icons/jpeg48.png';
                break;
            case 'ppt':
            case 'pptx':
                $link = 'img/icons/ms-powerpoint.png';
                break;
            default:
                $link = 'img/icons/file48.png';
        }
        return home_url + '/' + $link;
    };

    Huddles.removeFile = function() {
        var self = $(this);
        var row = self.closest('.discussion-editor-row');
        var document_id = self.closest('li').find('input[name=document_id]').val();
        var removeAttachedFiles = row.find('.js-remove-attachment');

        // check if document_id input isn't exists
        if (document_id) {
            if (removeAttachedFiles.val().length == 0) {
                removeAttachedFiles.val(document_id);
            } else {
                removeAttachedFiles.val(removeAttachedFiles.val() + ',' + document_id);
            }
        }

        self.parent().remove();
    };

    Huddles.fileChange = function() {
        var self = $(this),
                parent = self.parent(),
                file;
        if (this.files.length > 0) {
            /* move file input to li and replace by cloned a file input */
            var fileClone = self.clone();
            fileClone.change(Huddles.fileChange);
            fileClone.val('');

            self.removeClass('edit-controls').hide();
            parent.append(fileClone);

            file = this.files[0];
            var remove = $('<i class="remove edit-controls">remove</i>');
            var li = $('<li><a><span>' + file.name + '</span></a></li>');
            li.append(remove);
            remove.click(function() {
                li.remove();
            });

            var link = Huddles.getDocTypeIconLink(file.name);
            var icon = $('<img class="icon24" src="' + link + '" alt="doc type icon" />');
            li.find('a').prepend(icon);

            li.append(self);
            parent.find('ul.js-attachment-list').append(li);
        }
    };

    $('.discussion-editor-row input[type=file]').change(Huddles.fileChange).val('');
    $('#new_commentdd input[type=file], #frmAddDiscussion input[type=file]').change(Huddles.fileChange).val('');

    $('.discussion-editor-row .js-attachment-list i.remove').click(Huddles.removeFile);

    $('.discussion-editor-row .controls a.edit').click(function(event) {
        event.preventDefault();

        if ($(this).attr('id') == 'parentDiscussionEdit') {
            $('#discussion-title-box').hide();
            $('#discussion-title-input').show();
        }

        var row = $(this).closest('div.discussion-editor-row'),
                attachmentList = row.find('.js-attachment-list'),
                attachmentBackup
                ;
        row.find('.js-attachment-backup').remove();

        attachmentBackup = attachmentList.clone();
        attachmentBackup.addClass('js-attachment-backup').removeClass('js-attachment-list').hide();
        attachmentBackup.insertAfter(attachmentList);

        row.find('.view-controls').hide();
        row.find('.edit-controls').show();

        return false;
    });

    $('.discussion-editor-row a.js-cancel-btn').click(function(event) {
        event.preventDefault();
        $('#discussion-title-box').show();
        $('#discussion-title-input').hide();

//        var row = $(this).closest('div.discussion-editor-row'),
//                attachmentBackup = row.find('.js-attachment-backup'),
//                attachmentList = row.find('.js-attachment-list'),
//                removeAttachedFiles = row.find('.js-remove-attachment')
//                ;
//
//        attachmentList.html(attachmentBackup.html());
//        attachmentList.find('i.remove').click(Huddles.removeFile);
//        attachmentBackup.remove();
//
//        removeAttachedFiles.val('');
        var row = $(this).closest('div.discussion-editor-row');
        row.find('.view-controls').show();
        row.find('.edit-controls').hide();

        return false;
    });

    $('.discussion-editor-row .controls a.delete-comment').click(function(event) {
        event.preventDefault();
        if (confirm('Do you want to delete this comment ?') == true) {
            var selected_del = $(this);
            var comment_id = $(this).attr('id').split('-')[1];
            var huddle_id = $('#huddle_id').val();
            var parent_comment_id = $('.hid_id').eq(0).val();

            $.ajax({
                type: 'POST',
                url: home_url + '/Huddles/deleteComment/' + comment_id,
                success: function() {
                    if (selected_del && selected_del.hasClass('main')) {
                        location.href = home_url + '/Huddles/view/' + huddle_id + '/3';
                    } else {
                     //   location.href = home_url + '/Huddles/view/' + huddle_id + '/3/' + parent_comment_id;
                     $('#discussion-reply-'+comment_id).hide();
                    }
                },
                errors: function(response) {
                    alert(response.contents);
                }
            });
        }
    });

    $('.discussion-editor-row button.js-update-btn').click(function() {
        var row = $(this).closest('div.discussion-editor-row'),
                comment_id = row.find('input.hid_id[type=hidden]').val(),
                editor = false
                ;
        for (var i = 0; i < gApp.editorItems.length; i++) {
            var txtElement = $(gApp.editorItems[i].textareaElement);
            if (txtElement.attr('id').replace('editor-', '') == comment_id) {
                editor = gApp.editorItems[i];
                break;
            }
        }

        if (editor)
            Huddles.updateComment($(this), row, comment_id, editor.getValue());
    });
});

$(function() {
    var tab = $("#tabIndex").val();
    if (tab == 1) {
        $('.tab1').css('display', 'none');
        $('.tab2').css('display', 'block');
        $('#gVideoUpload').show();
    }
    else if (tab == 2) {
        $('.tab1').css('display', 'block');
        $('.tab2').css('display', 'none');
        $('#gDocumentUpload').show();
    }
    else if (tab == 3) {
        $('.tab1').css('display', 'none');
        $('.tab2').css('display', 'none');
        $('#gDiscussion').show();
    }
    else if (tab == 5) {
        $('.tab1').css('display', 'none');
        $('.tab2').css('display', 'none');
        $('#gObservation').show();
    }




    $('#tab1').click(function() {
        $('#tabbox1').css('display', 'block !important');
    });
    $('#tab2').click(function() {
        $('#tabbox2').css('display', 'block !important');
    });
    $('#tab3').click(function() {
        $('#tabbox3').css('display', 'block !important');
    });
    $('#tab4').click(function() {
        $('#tabbox4').css('display', 'block !important');
    });
    $('#tab5').click(function() {
        $('#tabbox5').addClass('tab-active');
        $('#tabbox5Area').css('display', 'block !important');
    });
    $('#tab7').click(function() {
        $('#tabbox7').css('display', 'block !important');
    });

    var gVideoUpload = $('#gVideoUpload');
    var gDocumentUpload = $('#gDocumentUpload');
    var gDiscussion = $('#gDiscussion');
    var gObservation = $('#gObservation');
    var obVideoUpload = $('#obVideoUpload');
    var gObservationStart = $('#gObservationStart');
    $('ul.tabset li a').on('click', function(e) {
        var href = $(this).attr('href');

        if (href == "#commentsTab" || href == "#attachmentTab" || href == "#frameWorksTab") {
            return;
        }
        if (href === '#tabbox1') {
            gVideoUpload.show();
            gDocumentUpload.hide();
            gDiscussion.hide();
            gObservation.hide();
            obVideoUpload.hide();
            gObservationStart.hide();
        }
        else if (href === '#tabbox2') {
            gDocumentUpload.show();
            gVideoUpload.hide();
            gDiscussion.hide();
            gObservation.hide();
            obVideoUpload.hide();
            gObservationStart.hide();
        }
        else if (href === '#tabbox3') {
            gDocumentUpload.hide();
            gVideoUpload.hide();
            gObservation.hide();
            obVideoUpload.hide();
            gObservationStart.hide();
            gDiscussion.show();
        }
        else if (href === '#tabbox5') {
            gDiscussion.hide();
            gDocumentUpload.hide();
            gVideoUpload.hide();
            gObservation.hide();
            obVideoUpload.hide();
            gObservationStart.show();
        }
        else if (href === '#tabbox5Area') {
            gDocumentUpload.hide();
            gVideoUpload.hide();
            gDiscussion.hide();
            gObservationStart.hide();
            gObservation.show();
        }
        else if (href === '#tabbox7'){
            gVideoUpload.hide();
            gDocumentUpload.hide();
            gDiscussion.hide();
            gObservation.hide();
            obVideoUpload.hide();
            gObservationStart.hide();
        }
        else {
            gDocumentUpload.hide();
            gVideoUpload.hide();
            gDiscussion.hide();
            gObservation.hide();
            gObservationStart.hide();
        }
        e.preventDefault();
        return false;
    });
    $('ul.tabset li').removeClass('hidden');

    $("#attachment-file").click(function() {
        $("#browse-attachment").show();
        $('#attachment_cancel').show();
        $(this).hide();
        return false;
    });

    $("#attachment_cancel").click(function() {
        /*For IE*/
        $("#comment_attachment").replaceWith($("#comment_attachment").clone(true));
        /*For other browsers*/
        $("#comment_attachment").val("");
        $("#browse-attachment").hide();
        $("#attachment-file").show();
        $(this).hide();
    });

    $('input[name="send_email"]').click(function() {
        if ($('input[name=send_email]:checked').val() == '2') {
            $('#users-list').fadeIn(1500);
        } else {
            $('#users-list').fadeOut(1500);
        }
    });

//    $('#gVideoUpload').click(function () {
//        OpenFilePicker('video');
//    });
    $('#obVideoUpload').click(function() {
        ob_OpenFilePicker('video');
    });
//    $('#gDocumentUpload').click(function () {
//        OpenFilePicker('doc');
//    });

    $('#docs-container').on('submit', 'form', function(event) {
        var $form = $(this);
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function() {
                $form.closest('li').remove();
                if ($('ul.video-docs li').length == 0) {
                    $('ul.video-docs').html('<li style="clear:left;">No Attachments have been added.</li>');
                }
                doDocumentSearchAjax();
            }
        });
        event.preventDefault();
    });
    $('.docs-container').on('submit', 'form', function(event) {
        var $form = $(this);
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function() {
                $form.closest('li').remove();
                if ($('ul.video-docs li').length == 0) {
                    $('ul.video-docs').html('<li style="clear:left;">No Attachments have been added.</li>');
                }
                doDocumentSearchAjax();
            }
        });
        event.preventDefault();
    });

    refreshAttachedDocumentList();
    ob_refreshAttachedDocumentList();
    /* video */
    $('#copy-huddle-btn').on('click', function(e) {
        e.preventDefault();
        $('#copy-huddle-btn').val('Copying...');
        $.ajax({
            url: home_url + "/huddles/copy",
            data: $('#huddle-copy-form').serialize(),
            type: 'POST',
            dataType: 'json',
            success: function(response) {
                $('#copy-huddle-btn').val('Copy');
                $('#notification').css('display', 'block');
                if (response.status == true) {
                    $('#notification').html('<div class="message success" style="cursor:pointer">' + response.message + '</div>');
                } else {
                    $('#notification').html('<div class="message error"  style="cursor:pointer">' + response.message + '</div>');
                }

                $('#moveFiles').modal('hide');
            },
            error: function() {
                alert("Network Error Occured");
            }
        });
    });

    $('#notification').hover(function() {
        $('#notification').fadeOut();
    });

    $('.vjs-play-progress').css('padding-right', '8px');


    /* init document */
    $('.ul-docs').on('submit', '.delete-document', function(event) {
        var form = $(this);
        var li = form.closest('li');
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            success: function() {
                li.remove();
                if ($('ul.ul-docs li.padd').length == 0) {
                    $('ul.ul-docs').html('<li>No Attachments have been added.</li>');
                }
            }
        });
        event.preventDefault();
        return false;
    });
    $('#tabbox2 .head').on('click', '.head-checkbox', getToogleCheckAllDocuments);

    initDocuments();
    initVideoDetail();
    doDocumentSearchAjaxInit();
    initVideoComments();

    var show_ff_message = $.cookie('show_ff_message');
    if (isFirefox() && !show_ff_message) {
        $.cookie('show_ff_message', 'displayed', {expires: new Date((new Date).getTime() + 14 * 86400 * 1000)});
        if ($('#flashMessage').length == 0) {
            $('#main').prepend('<div class="notice" id="flashMessage" style="cursor: pointer;">At this time, the video trimmer does not work with FireFox, please use Google Chrome, Internet Explorer 9+, or Safari.</div>');
            $('#flashMessage').click(function() {
                $(this).css('display', 'none')
            })
        } else {
            var html = $('#flashMessage').html();
            $('#flashMessage').html(html + '<br/>At this time, the video trimmer does not work with FireFox, please use Google Chrome, Internet Explorer 9+, or Safari.');
        }
    }
});

function initVideoDetail() {
    /* don't execute script if there is video detail page */
    if ($('.video-detail').length == 0)
        return;

    var yTrigger = 325;
    var viz = false;
    var doc = document.documentElement;
    var body = document.body;
    var minHeight = 640;
    var minWidth = 950;
    var top;
    var position;
    var rightbox = $('.right-box');
    var leftbox = $('.left-box');

    var small_scroll = false;

    var currentWinH = $(window).height();
    var checkH = (currentWinH <= minHeight) ? addScroll() : $.noop();
    var currentWinW, checkW;
    if (!small_scroll) {
        currentWinW = $(window).width();
        checkW = (currentWinW <= minWidth) ? addScroll() : removeScroll();
    }

    window.onscroll = function() {
        where();
    };

    $(window).resize(function() {
        //console.log("resized");
        currentWinH = $(window).height();
        checkH = (currentWinH <= minHeight) ? addScroll() : removeScroll();
        if (!small_scroll) {
            currentWinW = $(window).width();
            checkW = (currentWinW <= minWidth) ? addScroll() : removeScroll();
        }
    });

    function where() {
        if (leftbox.outerHeight() > rightbox.outerHeight())
            return;

        position = $(document).scrollTop();
        if (position >= yTrigger && viz == false && !small_scroll) {
            $(".video-outer").addClass("videoPosition");
            $("#comment_add_form_html").addClass("docsPosition");
            viz = true;
        } else if ((position < yTrigger && viz == true) || small_scroll) {
            $(".video-outer").removeClass("videoPosition");
            $("#comment_add_form_html").removeClass("docsPosition");
            viz = false;
        }
    }

    function addScroll() {
        $("#docs-container ul").addClass("scroll-small");
        small_scroll = true;
        where();
    }

    function removeScroll() {
        $("#docs-container ul").removeClass("scroll-small");
        small_scroll = false;
        where();
    }
}

function OpenFilePicker(docType) {
    if (docType == 'doc' && window.isCurrentVideoPlaying == true) {
        if (!confirm('Uploading a document while your video is playing will restart the video.  Are you sure you want to upload a document?'))
            return;
    }

    //filepicker.setKey(filepicker_access_key);
    var client = filestack.init(filepicker_access_key);
    var uploadPath = $("#uploadPath").val();
    $('#txtUploadedFilePath').val("");
    $('#txtUploadedFileName').val("");
    $('#txtUploadedFileMimeType').val("");
    $('#txtUploadedFileSize').val("");
    $('#txtUploadedDocType').val(docType);

    var filepickerOptions;

    if (docType == 'video') {
        filepickerOptions = {
            multiple: true,
            services: ['COMPUTER', 'DROPBOX', 'GOOGLE_DRIVE', 'SKYDRIVE', 'BOX', 'VIDEO', 'webcam'],
            extensions: ['3gp', '3gpp', 'avi', 'divx', 'dv', 'flv', 'm4v', 'mjpeg', 'mkv', 'mod', 'mov', 'mp4', 'mpeg', 'mpg', 'm2ts', 'mts', 'mxf', 'ogv', 'wmv', 'aif', 'mp3', 'm4a', 'ogg', 'wav', 'wma']
        }
    } else {
        filepickerOptions = {
            multiple: true,
            extensions: ['bmp', 'gif', 'jpeg', 'jpg', 'png', 'tif', 'tiff', 'swf', 'pdf', 'txt', 'docx', 'ppt', 'pptx', 'potx', 'xls', 'xlsx', 'xlsm', 'rtf', 'odt', 'doc', 'mp3', 'm4a'],
            services: ['COMPUTER', 'DROPBOX', 'GOOGLE_DRIVE', 'SKYDRIVE', 'BOX', 'URL']
        }
    }

    // fix for iOS 7
    if (isIOS()) {
        filepickerOptions.multiple = false;
    }

    // new window for iPhone
    if (!!navigator.userAgent.match(/iPhone/i)) {
        filepickerOptions.container = 'window';
    }

    if (docType == 'video') {

    client.pick({
            maxFiles: 1,
            fromSources: ['local_file_system' ,'dropbox', 'googledrive', 'box','onedrive', 'video'], 
            accept: ['video/*','3gp', '3gpp', 'avi', 'divx', 'dv', 'flv', 'm4v', 'mjpeg', 'mkv', 'mod', 'mov', 'mp4', 'mpeg', 'mpg', 'm2ts', 'mts', 'mxf', 'ogv', 'wmv', 'aif', 'mp3', 'm4a', 'ogg', 'wav', 'wma'],
         storeTo:   {
                                        location: "s3",
                                        path: uploadPath,
                                        access: 'public',
                                        container: bucket_name,
                                        region: 'us-east-1'
            }
        }).then(
    function(inkBlob) {
        inkBlob = inkBlob.filesUploaded;
        if (inkBlob && inkBlob.length > 0) {
            for (var i = 0; i < inkBlob.length; i++) {
                var blob = inkBlob[i];
                var fileExt = getFileExtension(blob.filename).toLowerCase();

                $('#txtUploadedFilePath').val(blob.key);
                $('#txtUploadedFileName').val(blob.filename);
                $('#txtUploadedFileMimeType').val(blob.mimetype);
                $('#txtUploadedFileSize').val(blob.size);
                if (docType == 'video') {
                    $('#temp-video-title').html(blob.filename);
                    $('#videos-list li.videos-list__item_noitem').text('');
                    var total_videos = parseInt($('#videos_count').val());
                    $('ul.videos-list').prepend($('#temp-list').html());
                    if (total_videos != '' && total_videos >= 12) {
                        $('ul.videos-list li:last').remove();
                    }
                    PostHuddleVideo(docType, (fileExt == 'mp4' ? true : false));
                } else {
                    $('#txtUploadedUrl').val(blob.url);
                    $('#doc-title').html(blob.filename);
                    $('#doc-type').html(blob.mimetype);
                    $('#add-document-row').html($('#extra-row-li').html());
                    PostHuddleDocument();
                }
            }
        }
    }),
            function(FPError) {
                var error_desc = 'Unkown Error';
                //as per filepicker documentation these are possible two errors
                if (FPError.code == 101) {
                    error_desc = 'The user closed the dialog without picking a file';
                } else if (FPError.code = 151) {
                    error_desc = 'The file store couldnt be reached';
                }

                $.ajax({
                    type: 'POST',
                    data: {
                        type: 'Huddles',
                        id: $('#huddle_id').val(),
                        error_id: FPError.code,
                        error_desc: error_desc,
                        docType: docType,
                        current_user_id: current_user_id
                    },
                    url: home_url + '/Huddles/logFilePickerError/',
                    success: function(response) {
                        //Do nothing.
                    },
                    errors: function(response) {
                        alert('Error occured while logging, please report the error code to the system administrator: ' + FPError.code);
                    }
                });
            }
            
            
    }
    
    else
    {
            client.pick({
          maxFiles: 1,
            accept: ['application/*','.bmp', '.gif', '.jpeg', '.jpg', '.png', '.tif', '.tiff', '.swf', '.pdf', '.txt', '.docx', '.ppt', '.pptx', '.potx', '.xls', '.xlsx', '.xlsm', '.rtf', '.odt', '.doc'],
            fromSources: ['local_file_system' ,'dropbox', 'googledrive', 'box','onedrive'], 
         storeTo:   {
                                        location: "s3",
                                        path: uploadPath,
                                        access: 'public',
                                        container: bucket_name,
                                        region: 'us-east-1'
            }
        }).then(
    function(inkBlob) {
        inkBlob = inkBlob.filesUploaded;
        if (inkBlob && inkBlob.length > 0) {
            for (var i = 0; i < inkBlob.length; i++) {
                var blob = inkBlob[i];
                var fileExt = getFileExtension(blob.filename).toLowerCase();

                $('#txtUploadedFilePath').val(blob.key);
                $('#txtUploadedFileName').val(blob.filename);
                $('#txtUploadedFileMimeType').val(blob.mimetype);
                $('#txtUploadedFileSize').val(blob.size);
                if (docType == 'video') {
                    $('#temp-video-title').html(blob.filename);
                    $('#videos-list li.videos-list__item_noitem').text('');
                    var total_videos = parseInt($('#videos_count').val());
                    $('ul.videos-list').prepend($('#temp-list').html());
                    if (total_videos != '' && total_videos >= 12) {
                        $('ul.videos-list li:last').remove();
                    }
                    PostHuddleVideo(docType, (fileExt == 'mp4' ? true : false));
                } else {
                    $('#txtUploadedUrl').val(blob.url);
                    $('#doc-title').html(blob.filename);
                    $('#doc-type').html(blob.mimetype);
                    $('#add-document-row').html($('#extra-row-li').html());
                    var attachment_no = $("#attachment").text();
                    var attach_new = attachment_no.substring(13,14);
                    attachment_no = parseInt(attach_new) + 1;
                    $("#attachment").text('Attachments (' + attachment_no + ')');
                    PostHuddleDocument();
                }
            }
        }
    }),
            function(FPError) {
                var error_desc = 'Unkown Error';
                //as per filepicker documentation these are possible two errors
                if (FPError.code == 101) {
                    error_desc = 'The user closed the dialog without picking a file';
                } else if (FPError.code = 151) {
                    error_desc = 'The file store couldnt be reached';
                }

                $.ajax({
                    type: 'POST',
                    data: {
                        type: 'Huddles',
                        id: $('#huddle_id').val(),
                        error_id: FPError.code,
                        error_desc: error_desc,
                        docType: docType,
                        current_user_id: current_user_id
                    },
                    url: home_url + '/Huddles/logFilePickerError/',
                    success: function(response) {
                        //Do nothing.
                    },
                    errors: function(response) {
                        alert('Error occured while logging, please report the error code to the system administrator: ' + FPError.code);
                    }
                });
            }
        
        
        
        
    }

    
}

function getFileExtension(filename) {

    return filename.substr(filename.lastIndexOf('.') + 1)

}

function ob_OpenFilePicker(docType) {
    if (docType == 'doc' && window.isCurrentVideoPlaying == true) {
        if (!confirm('Uploading a document while your video is playing will restart the video.  Are you sure you want to upload a document?'))
            return;
    }

    filepicker.setKey(filepicker_access_key);
    var uploadPath = $("#uploadPath").val();
    $('#txtUploadedFilePath').val("");
    $('#txtUploadedFileName').val("");
    $('#txtUploadedFileMimeType').val("");
    $('#txtUploadedFileSize').val("");
    $('#txtUploadedDocType').val(docType);

    var filepickerOptions;

    if (docType == 'video') {
        filepickerOptions = {
            multiple: true,
            services: ['COMPUTER', 'DROPBOX', 'GOOGLE_DRIVE', 'SKYDRIVE', 'BOX', 'URL'],
            extensions: ['3gp', '3gpp', 'avi', 'divx', 'dv', 'flv', 'm4v', 'mjpeg', 'mkv', 'mod', 'mov', 'mp4', 'mpeg', 'mpg', 'm2ts', 'mts', 'mxf', 'ogv', 'wmv', 'aif', 'mp3', 'm4a', 'ogg', 'wav', 'wma']
        }
    } else {
        filepickerOptions = {
            multiple: true,
            extensions: ['bmp', 'gif', 'jpeg', 'jpg', 'png', 'tif', 'tiff', 'swf', 'pdf', 'txt', 'docx', 'ppt', 'pptx', 'potx', 'xls', 'xlsx', 'xlsm', 'rtf', 'odt', 'doc', 'mp3', 'm4a'],
            services: ['COMPUTER', 'DROPBOX', 'GOOGLE_DRIVE', 'SKYDRIVE', 'BOX', 'URL']
        }
    }

    // fix for iOS 7
    if (isIOS()) {
        filepickerOptions.multiple = false;
    }

    // new window for iPhone
    if (!!navigator.userAgent.match(/iPhone/i)) {
        filepickerOptions.container = 'window';
    }

    filepicker.pickAndStore(
            filepickerOptions, {
        location: "S3",
        path: uploadPath,
        container: bucket_name
    },
    function(inkBlob) {
        if (inkBlob && inkBlob.length > 0) {
            for (var i = 0; i < inkBlob.length; i++) {
                var blob = inkBlob[i];
                var fileExt = getFileExtension(blob.filename).toLowerCase();

                $('#txtUploadedFilePath').val(blob.key);
                $('#txtUploadedFileName').val(blob.filename);
                $('#txtUploadedFileMimeType').val(blob.mimetype);
                $('#txtUploadedFileSize').val(blob.size);
                if (docType == 'video') {
                    $('#temp-video-title').html(blob.filename);
                    $('ul.videos-list').prepend($('#temp-list').html());
                    ob_PostHuddleVideo(docType, (fileExt == 'mp4' ? true : false));
                } else {
                    $('#doc-title').html(blob.filename);
                    $('#doc-type').html(blob.mimetype);
                    $('#add-document-row').html($('#extra-row-li').html());
                    ob_PostHuddleDocument();
                }
            }
        }
    },
            function(FPError) {
                var error_desc = 'Unkown Error';
                //as per filepicker documentation these are possible two errors
                if (FPError.code == 101) {
                    error_desc = 'The user closed the dialog without picking a file';
                } else if (FPError.code = 151) {
                    error_desc = 'The file store couldnt be reached';
                }

                $.ajax({
                    type: 'POST',
                    data: {
                        type: 'Huddles',
                        id: $('#ob_account_folder_id').val(),
                        error_id: FPError.code,
                        error_desc: error_desc,
                        docType: docType,
                        current_user_id: current_user_id
                    },
                    url: home_url + '/Huddles/logFilePickerError/',
                    success: function(response) {
                        //Do nothing.
                    },
                    errors: function(response) {
                        alert('Error occured while logging, please report the error code to the system administrator: ' + FPError.code);
                    }
                });
            }

    );
}
function ob_PostHuddleVideo(doc_type, direct_publish) {
    var video_id_url = '';
    if (doc_type == 'doc' && $('ul.tabset li a.active').length > 0 && $('ul.tabset li a.active').html() == 'Videos' && $('#txtCurrentVideoID').length > 0) {
        video_id_url = "/" + $('#txtCurrentVideoID').val();
    }

    $.ajax({
        url: home_url + (doc_type == 'video' ? '/Huddles/uploadVideos/' : '/Huddles/uploadDocuments/') +
                $('#ob_account_folder_id').val() + '/' + $('#account_id').val() + '/' + $('#user_id').val() + video_id_url,
        method: 'POST',
        dataType: 'json',
        success: function(data) {
            console.log(data);
            if (data['user_session_expired']) {
                location.href = home_url + '/users/login';
                return;
            }
            doVideoSearchAjax();
            if ($('#flashMessage').length == 0) {
                $('#main').prepend('<div class="success" id="flashMessage" style="cursor: pointer;">Sit tight, your video is currently processing.  You will receive an email when your video is ready to be viewed.</div>');
                $('#flashMessage').click(function() {
                    $(this).css('display', 'none')
                })
            } else {
                var html = $('#flashMessage').html();
                $('#flashMessage').html(html + '<br/>Sit tight, your video is currently processing.  You will receive an email when your video is ready to be viewed.');
            }
        },
        data: {
            video_title: $('#txtUploadedFileName').val(),
            video_desc: '',
            video_url: $('#txtUploadedFilePath').val(),
            video_file_name: $('#txtUploadedFileName').val(),
            video_mime_type: $('#txtUploadedFileMimeType').val(),
            video_file_size: $('#txtUploadedFileSize').val(),
            rand_folder: $('#txtVideoPopupRandomNumber').val(),
            direct_publish: (direct_publish == true ? 1 : 0)
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (textStatus == 'error') {
//            alert('An error just occurred. The site will be refreshed.');
            location.reload(true);
        }
    });
}

function ob_PostHuddleDocument() {
    showProcessOverlay($('#obs-docs-container'));
    var video_id_url = "/" + $('#ob_account_folder_id').val();
    $.ajax({
        url: home_url + '/Huddles/uploadDocuments/' + $('#ob_account_folder_id').val() + '/' + $('#account_id').val() + '/' + video_id_url,
        method: 'POST',
        dataType: 'json',
        success: function(data) {
            console.log(data);
            if (data['user_session_expired']) {
                location.href = home_url + '/users/login';
            } else if (data['document_id'] == undefined || data['document_id'] == 0) {
                alert('Unable to save data, please try again');
            } else {
                refreshAttachedDocumentList();
                ob_refreshAttachedDocumentList();
                doObservationDocumentSearchAjax(function() {
                    hideProcessOverlay($('#obs-docs-container'))
                });
            }
        },
        data: {
            video_title: $('#txtUploadedFileName').val(),
            video_desc: "",
            video_url: $('#txtUploadedFilePath').val(),
            video_file_name: $('#txtUploadedFileName').val(),
            video_mime_type: $('#txtUploadedFileMimeType').val(),
            video_file_size: $('#txtUploadedFileSize').val(),
            rand_folder: "0"
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (textStatus == 'error') {
//            alert('An error just occurred. The site will be refreshed.');
            location.reload(true);
        }
    });
}
function PostHuddleVideo(doc_type, direct_publish) {
    var video_id_url = '';
    if (doc_type == 'doc' && $('ul.tabset li a.active').length > 0 && $('ul.tabset li a.active').html() == 'Videos' && $('#txtCurrentVideoID').length > 0) {
        video_id_url = "/" + $('#txtCurrentVideoID').val();
    }

    $.ajax({
        url: home_url + (doc_type == 'video' ? '/Huddles/uploadVideos/' : '/Huddles/uploadDocuments/') +
                $('#huddle_id').val() + '/' + $('#account_id').val() + '/' + $('#user_id').val() + video_id_url,
        method: 'POST',
        dataType: 'json',
        success: function(data) {
            console.log(data);
            if (data['user_session_expired']) {
                location.href = home_url + '/users/login';
                return;
            }
            doVideoSearchAjax();
            if ($('#flashMessage').length == 0) {
                $('#main').prepend('<div class="success" id="flashMessage" style="cursor: pointer;">Sit tight, your video is currently processing.  You will receive an email when your video is ready to be viewed.</div>');
                $('#flashMessage').click(function() {
                    $(this).css('display', 'none')
                })
            } else {
                var html = $('#flashMessage').html();
                $('#flashMessage').html(html + '<br/>Sit tight, your video is currently processing.  You will receive an email when your video is ready to be viewed.');
            }
        },
        data: {
            video_title: $('#txtUploadedFileName').val(),
            video_desc: '',
            video_url: $('#txtUploadedFilePath').val(),
            video_file_name: $('#txtUploadedFileName').val(),
            video_mime_type: $('#txtUploadedFileMimeType').val(),
            video_file_size: $('#txtUploadedFileSize').val(),
            rand_folder: $('#txtVideoPopupRandomNumber').val(),
            direct_publish: (direct_publish == true ? 1 : 0)
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (textStatus == 'error') {
//            alert('An error just occurred. The site will be refreshed.');
            location.reload(true);
        }
    });
}

function PostHuddleDocument() {
    showProcessOverlay($('#docs-container'));
    showProcessOverlay($('#tabbox2'));
    var video_id_url = "/" + $('#txtCurrentVideoID').val();
    $.ajax({
        url: home_url + '/Huddles/uploadDocuments/' + $('#huddle_id').val() + '/' + $('#account_id').val() + '/' + $('#user_id').val() + video_id_url + '/1',
        method: 'POST',
        dataType: 'json',
        success: function(data) {
            console.log(data);
            if (data['user_session_expired']) {
                location.href = home_url + '/users/login';
            } else if (data['document_id'] == undefined || data['document_id'] == 0) {
                alert('Unable to save data, please try again');
            } else {
                CloseDocumentUpload();
            }
        },
        data: {
            video_title: $('#txtUploadedFileName').val(),
            video_desc: "",
            video_url: $('#txtUploadedFilePath').val(),
            video_file_name: $('#txtUploadedFileName').val(),
            video_mime_type: $('#txtUploadedFileMimeType').val(),
            video_file_size: $('#txtUploadedFileSize').val(),
            url: $('#txtUploadedUrl').val(),
            rand_folder: "0"
        }
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (textStatus == 'error') {
//            alert('An error just occurred. The site will be refreshed.');
            location.reload(true);
        }
    });
}

function CloseDocumentUpload() {
    refreshAttachedDocumentList();
    ob_refreshAttachedDocumentList();
    doDocumentSearchAjax(function() {
        hideProcessOverlay($('#tabbox2'));
    });
}

function refreshAttachedDocumentList() {

    var videoInp = $('#txtCurrentVideoID');
    var huddle_id = $('#huddle_id');
    if (videoInp.length == 0)
        return;
    var url = home_url + '/MyFiles/getAttachedDocuments/' + videoInp.val() + '/' + huddle_id.val() + '?t=' + (new Date()).getTime();
    $.getJSON(url).done(function(data) {
        var container = $('#docs-container');
        container.html(data.html);
        hideProcessOverlay(container);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (textStatus == 'error') {
//            alert('An error just occurred. The site will be refreshed.');
            location.reload(true);
        }
    });
}
function ob_refreshAttachedDocumentList() {

    var videoInp = $('#ob_account_folder_id');
    if (videoInp.length == 0)
        return;
    var url = home_url + '/MyFiles/ob_getAttachedDocuments/' + videoInp.val() + '?t=' + (new Date()).getTime();
    $.getJSON(url).done(function(data) {
        var container = $('#obs-docs-container');
        container.html(data.html);
        hideProcessOverlay(container);
    }).fail(function(jqXHR, textStatus, errorThrown) {
        if (textStatus == 'error') {
//            alert('An error just occurred. The site will be refreshed.');
            location.reload(true);
        }
    });
}
function checkAllUsers(users) {
    $(users).each(function(index, el) {
        el = $(el);
        if (el.prop('checked') == false) {
            el.prop('checked', true).trigger('change');
        }
    });
}

function uncheckAllUsers(users) {
    $(users).each(function(index, el) {
        el = $(el);
        if (el.prop('checked') == true) {
            el.prop('checked', false).trigger('change');
        }
    });
}

function getCheckedUsers() {
    var users_id = '';
    $.each($('.user_checkbox[name="chk_notification_user_ids"]'), function(index, el) {
        if ($(this).is(':checked')) {
            if ($(this).val() != '') {
                if (users_id != '') {
                    users_id += ',' + $(this).val()
                }
                else {
                    users_id += $(this).val()
                }
            }
        }
    });
    if (users_id == '') {
        return '';
    } else {
        return users_id;
    }

}

function getCheckedReplyUsers() {
    var user_ids = [];
    $('.user_checkbox_reply').each(function(index, el) {
        el = $(el);
        if (el.prop('checked') == true) {
            user_ids.push(el.attr('value'));
        }
    });
    return user_ids.join(',');
}

function postDiscussionReply() {

    var editor = false;
    for (var i = 0; i < gApp.editorItems.length; i++) {
        var txtElement = $(gApp.editorItems[i].textareaElement);
        if (txtElement.attr('id') == "editor-2") {
            editor = gApp.editorItems[i];
            break;
        }
    }

    if (editor) {
        var html = editor.getValue();
        $('#comment_html_2').val(html);
        $('#txtNotifications').val(getCheckedReplyUsers());
        if ($('#rdoPreDefined').prop('checked') == true) {
            $('#txtNotifications').val($('#txtPreDefinedNotifiers').val());
        } else if ($('#rdoLetMeChoose').prop('checked') == true) {
            $('#txtNotifications').val(getCheckedUsers());
            if ($('#txtNotifications').val() == '') {
                alert('Please select Users to send notification');
                return false;
            }
        } else {
            $('#txtNotifications').val('');
        }
        $('#new_comment_reply').submit();

    }

}

function postDiscussion() {

    var editor = false;
    for (var i = 0; i < gApp.editorItems.length; i++) {
        var txtElement = $(gApp.editorItems[i].textareaElement);
        if (txtElement.attr('id') == "editor-2") {
            editor = gApp.editorItems[i];
            break;
        }
    }

    if (editor) {

        var html = editor.getValue();
        $('#comment_html_2').val(html);

        $("#frmAddDiscussion").valid();
        if ($("#frmAddDiscussion").valid()) {
            if ($('#rdoPreDefined').prop('checked') == true) {
                $('#txtNotifications').val($('#txtPreDefinedNotifiers').val());
            } else if ($('#rdoLetMeChoose').prop('checked') == true) {
                $('#txtNotifications').val(getCheckedUsers());
                if ($('#txtNotifications').val() == '') {
                    alert('Please select Users to send notification');
                    return false;
                }
            } else {
                $('#txtNotifications').val('');
            }

            $('#frmAddDiscussion').submit();

        } else {
            return false;
        }

    }

    return false;
}

function getCheckedDocuments() {
    var users_id = '';
    $.each($('.download_doc_checkbox'), function(index, value) {
        if ($(this).prop('checked') == true) {
            if (users_id != '')
                users_id += ',';
            users_id += $(this).attr('value');
        }
    });

    return users_id;
}

function getToogleCheckAllDocuments() {
    if ($(this).prop('checked') == true) {
        $.each($('.download_doc_checkbox'), function(index, value) {
            $(this).prop('checked', true).trigger('change');
        });
    } else {
        $.each($('.download_doc_checkbox'), function(index, value) {
            $(this).prop('checked', false).trigger('change');
        });
    }
}

function downloadItemsAsZip() {
    var checked_items = getCheckedDocuments();
    if (checked_items == '') {
        alert('Please select at least one document');
        return false;
    }
    $('#download_as_zip_docs').val(checked_items);
    $('#huddle-download-form').submit();
    return true;
}

function deleteDoc() {
    var checked_items = getCheckedDocuments();
    if (checked_items == '') {
        alert('Please select at least one document');
        return false;
    }
    if (!confirm('Are you sure you want to delete this resource?')) {
        return false;
    }
    $('#document_ids').val(checked_items);
    $('#account-folder-id').val($('#huddle_id').val());
    $('#huddle-delete-form').submit();
    return true;
}

function initDocuments() {
    $('.ul-docs')
            .on('click', '.video-dropdown', function() {
        if ($(this).hasClass('video-dropdown-selected')) {
            $(this).removeClass('video-dropdown-selected');
            $($(this).siblings('.video-dropdown-container')[0]).css('display', 'none');
        } else {
            CloseAllDropDown();
            $(this).addClass('video-dropdown-selected');
            $($(this).siblings('.video-dropdown-container')[0]).css('display', 'block');
        }
    })
            .on('click', '.btnDoneVideoLibrary', CloseDropDown)
            .on('click', '.btnCancelVideoLibrary', CloseAllDropDown)
            ;
}

function getCheckedVideos(document_id) {
    var checked_videos = [];
    $('.doc_checkbox_' + document_id).each(function(idx, el) {
        el = $(el);
        if (el.prop('checked')) {
            checked_videos.push(el.attr('value'));
        }
    });
    return checked_videos;
}

function CloseDropDown() {
    var document_id = this.id.split('-')[1];
    var checked_videos = getCheckedVideos(document_id);
    $.ajax({
        type: 'POST',
        data: {
            associated_videos: checked_videos.join(',')
        },
        url: home_url + '/Huddles/associateVideoDocuments/' + document_id + '/' + $('#huddle_id').val(),
        success: function() {
            CloseAllDropDown();
        },
        errors: function(response) {
            alert(response.contents);
            location.href = home_url + '/Huddles/view/' + $('#huddle_id').val() + '/2/';
        }
    });
}

function CloseAllDropDown() {
    var btnDones = $('.btnDoneVideoLibrary');
    for (var i = 0; i < btnDones.length; i++) {
        var btnDone = $(btnDones[i]);
        var current_dropdwn = $(btnDone.parent().parent().prev()[0]);
        if (current_dropdwn.hasClass('video-dropdown-selected')) {
            current_dropdwn.removeClass('video-dropdown-selected');
            $(current_dropdwn.siblings('.video-dropdown-container')[0]).css('display', 'none');
        }
    }
}
