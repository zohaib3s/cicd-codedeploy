var comment_radio_selected_by_user = false;

$(document).ready(function () {

    $(document).on("change", "#list-containers li", function () {

        var userchk = $(this).find('label [type="checkbox"]').is(":checked");
        if (userchk) {
            if ($(this).find("span .chk_is_coachee").is(':checked')) {
                $("#chk_is_coachee").val("1");
            }
            else {
                $("#chk_is_coachee").val("0");
                $("#list-containers li").each(function (value) {
                    if ($(this).find('label [type="checkbox"]').is(":checked")) {
                        if ($(this).find("span .chk_is_coachee").is(':checked')) {
                            $("#chk_is_coachee").val("1");
                        }
                    }
                });
            }
        }
        else {
            $("#chk_is_coachee").val("0");
        }
    });
    $("#txtVideostandard_tagsinput span a").on("click", function () {
        var key_value = $(this).parent().text();
        var key_without_space = key_value.split(" ");

    });
    $(".desk-cta").click(function () {
        $("body").scrollTop(0);
        $('#desk-support-box').show();
        $('#desk-darkness').show();
        return false;
    });

    $("#desk-support-box-close").click(function () {
        $('#desk-support-box').hide();
        $('#desk-darkness').hide();
        return false;
    });

    $("#desk-darkness").click(function () {
        $('#desk-support-box').hide();
        $('#desk-darkness').hide();
    });

    $('#flashMessage')
            .css('cursor', 'pointer')
            .click(function () {
                $('#flashMessage').fadeOut();
            });

    appendix($(".appendix"));

    //appendix($(".appendix2"));

    $('body').on('click', 'a[data-confirm]', function (event) {
        console.log('data-confirm');
        event.preventDefault();
        event.stopPropagation();
        event.stopImmediatePropagation();

        if (confirm($(this).attr('data-confirm'))) {
            try {
                eval($(this).attr('href'));
            }
            catch (e) {
                location.href = $(this).attr('href');
            }
        }
        return false;
    });

    if ($('#txtSearchVideos') && $('#txtSearchVideos').length > 0) {

        $('#txtSearchVideos').bind('keypress', function (e) {
            $('#clearVideoButton').css('display', 'block');
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                doVideoSearchAjax();
            }
        });
    }
    if ($('#txtSearchObsInHuddle') && $('#txtSearchObsInHuddle').length > 0) {
        $('#txtSearchObsInHuddle').bind('keypress', function (e) {
            $('#clearSearchObsInHuddle').css('display', 'block');
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                doObsInHuddleSearchAjax();
            }
        });
    }

    if ($('#txtSearchDocuments') && $('#txtSearchDocuments').length > 0) {
        $('#txtSearchDocuments').bind('keypress', function (e) {
            $('#clearButton').css('display', 'block');
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) { //Enter keycode
                doDocumentSearchAjax();
                return false;
            }
        });
    }
    if ($('#txtSearchObVideos') && $('#txtSearchObVideos').length > 0) {
        $('#txtSearchObVideos').bind('keypress', function (e) {
            $('#clearObVideoButton').css('display', 'block');
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                doObVideoSearchAjax();
            }
        });
    }

    if ($('#cmbVideoSort').length > 0) {
        $('#cmbVideoSort').change(function () {
            doVideoSearchAjax();
        });
    }
    if ($('#cmbObVideoSort').length > 0) {
        $('#cmbObVideoSort').change(function () {
            doObVideoSearchAjax();
        });
    }
    if ($('#cmbSearchObsInHuddle').length > 0) {
        $('#cmbSearchObsInHuddle').change(function () {
            doObsInHuddleSearchAjax();
        });
    }

    if ($('#cmbDocumentSort').length > 0) {
        $('#cmbDocumentSort').change(doDocumentSearchAjax);
    }

    if ($('#btnSearchVideos').length > 0) {
        $('#btnSearchVideos').click(function () {
            doVideoSearchAjax();
        });
    }
    $('#btnSearchObsInHuddle').click(function () {
        if ($("#txtSearchObsInHuddle:input").val() != '') {
            doObsInHuddleSearchAjax();
        }
    });

    var txtSearchHuddles = $("#txtSearchHuddles");
    if (txtSearchHuddles.length > 0) {
        var clearBtn = $("#clearSearchHuddles");
        var searchBtn = $('#btnHuddleSearch');

        txtSearchHuddles.on("keypress", function (e) {
            clearBtn.show();
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                doHuddleSearch_Textbox();
                return false;
            }
        });

        clearBtn.click(function () {
            txtSearchHuddles.val("");
            clearBtn.hide();
            doHuddleSearch();
            return false;
        });
        searchBtn.click(function () {
            if (txtSearchHuddles.val().length > 0) {
                doHuddleSearch_Textbox();
            }
        });
    }


    $('#clearButton').click(function (e) {
        $("#txtSearchDocuments:input").val('');
        doDocumentSearchAjax();
        $('#clearButton').css('display', 'none');

        return false;
    });
    $('#clearObVideoButton').click(function (e) {
        $("#txtSearchObVideos:input").val('');
        doObVideoSearchAjax();
        $('#clearVideoButton').css('display', 'none');
        return false;
    });
    $('#clearVideoButton').click(function (e) {
        $("#txtSearchVideos:input").val('');
        doVideoSearchAjax();
        $('#clearVideoButton').css('display', 'none');
        return false;
    });
    $('#clearSearchObsInHuddle').click(function (e) {
        $("#txtSearchObsInHuddle:input").val('');
        doObsInHuddleSearchAjax();
        $('#clearSearchObsInHuddle').css('display', 'none');
        return false;
    });
    if ($('#btnSearchDocuments').length > 0) {
        $('#btnSearchDocuments').click(function () {
            doDocumentSearchAjax();
        });
    }

    $('#videos-list').on('click', '.pagination li', function () {
        doVideoSearchAjax($(this).data('page'));
    });

    //---------------------------Search Bars--------------------//

    $('#txtSearchPeople').bind('keypress', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) { //Enter keycode
            var keywords = $('#txtSearchPeople').val();
            var huddle_id = $("#huddle-id").val();
            doSearchPoeple(keywords, huddle_id);
            $('#clearSearchPeople').css('display', 'block');
            return false;
        }
    });

    $('#btnSearchPeople').click(function (e) {
        var keywords = $('#txtSearchPeople').val();
        var huddle_id = $("#huddle-id").val();
        doSearchPoeple(keywords, huddle_id);
        $('#clearSearchPeople').css('display', 'block');
    });

    $('#clearSearchPeople').click(function (e) {
        $("#txtSearchPeople:input").val('');
        var keywords = '';
        var huddle_id = $("#huddle-id").val();
        doSearchPoeple(keywords, huddle_id);
        $('#clearSearchPeople').css('display', 'none');
        return false;
    });

    //People Sections
    $('#txtSearchSuperUser').bind('keypress', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) { //Enter keycode
            var keywords = $('#txtSearchSuperUser').val();
            doSearchSuperUser(keywords);
            $('#clearSuperUser').css('display', 'block');
            return false;
        }
    });

    $('#btnSearchSuperUser').click(function (e) {
        var keywords = $('#txtSearchSuperUser').val();
        doSearchSuperUser(keywords);
        $('#clearSuperUser').css('display', 'block');
    });

    $('#clearSuperUser').click(function (e) {
        $("#txtSearchSuperUser:input").val('');
        var keywords = '';
        doSearchSuperUser(keywords);
        $('#clearSuperUser').css('display', 'none');
        return false;
    });

    //Admin Users Search
    $('#txtSearchAdminUser').bind('keypress', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) { //Enter keycode
            var keywords = $('#txtSearchAdminUser').val();
            doSearchAdminUser(keywords);
            $('#clearAdminUser').css('display', 'block');
            return false;
        }
    });

    $('#btnSearchAdminUser').click(function (e) {
        var keywords = $('#txtSearchAdminUser').val();
        doSearchAdminUser(keywords);
        $('#clearAdminUser').css('display', 'block');
    });

    $('#clearAdminUser').click(function (e) {
        $("#txtSearchAdminUser:input").val('');
        var keywords = '';
        doSearchAdminUser(keywords);
        $('#clearAdminUser').css('display', 'none');
        return false;
    });
    //Group Users
    $('#txtSearchGroups').bind('keypress', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) { //Enter keycode
            var keywords = $('#txtSearchGroups').val();
            doSearchGroup(keywords);
            $('#clearGroups').css('display', 'block');
            return false;
        }
    });

    $('#btnGroupSearch').click(function (e) {
        var keywords = $('#txtSearchGroups').val();
        doSearchGroup(keywords);
        $('#clearGroups').css('display', 'block');
    });

    $('#clearGroups').click(function (e) {
        $("#txtSearchGroups:input").val('');
        var keywords = '';
        doSearchGroup(keywords);
        $('#clearGroups').css('display', 'none');
        return false;
    });

    // ----------Search Discussions----------- //
    $('#txtSearchHuddleDiscussions').bind('keypress', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) { //Enter keycode
            doHuddleDiscussionSearchAjax();
        } else {
            $('#clearSearchHuddleDiscussions').css('display', 'block');
        }
    });

    $('#btnHuddleDiscussionSearch').click(function () {
        doHuddleDiscussionSearchAjax();
        $('#clearSearchHuddleDiscussions').css('display', 'block');
    });

    $('#clearSearchHuddleDiscussions').click(function (e) {
        $("#txtSearchHuddleDiscussions:input").val('');
        doHuddleDiscussionSearchAjax();
        $('#clearSearchHuddleDiscussions').css('display', 'none');
        return false;
    });

    if ($('#cmbDiscussionSort') && $('#cmbDiscussionSort').length > 0) {
        $('#cmbDiscussionSort').change(doHuddleDiscussionSearchAjax);
    }

    /* video details */
    $('#comment_form_main').on('submit', '#comments-form2', function (event) {
        this.submit.disabled = 1;
        event.preventDefault();
        $("#comments").trigger("click");
        var $form = $(this);
        $form[0].submit.disabled = 0;
    });
    $('#comment_form_main').on('submit', '#comments-form1', function (event) {

        this.submit.disabled = 1;
        event.preventDefault();
        $("#comments").trigger("click");
        var $form = $(this);
        $form[0].submit.disabled = 0;
        var form_post_data = $form.serialize();
        if ($('#comment_comment').length > 0) {
            //$('#comment_comment').val('');
            $('#txtVideoTags_tagsinput span').remove();

            $('#txtVideostandard_tagsinput span').remove();
            $('#txtVideostandard_vid_tagsinput span').remove();
            $('#txtVideostandard_tag').show();
            $('#txtVideostandard_vid_tag').show();

            var tagid = '';
            var quick_tag_counter = 0;
            $(".divblockwidth a").each(function (value) {
                $(this).attr('status_flag', '0');
                $(this).removeAttr('class');

                if (quick_tag_counter == 0) {
                    $(this).attr('class', 'default_tag tags_qucls');
                }

                if (quick_tag_counter == 1) {
                    $(this).attr('class', 'default_tag tags_sugcls');
                }

                if (quick_tag_counter == 2) {
                    $(this).attr('class', 'default_tag tags_notescls');
                }

                if (quick_tag_counter == 3) {
                    $(this).attr('class', 'default_tag tags_strangthcls');
                }

                quick_tag_counter++;
            });
            var tagid = '';
            var quick_tag_counter = 0;
            $(".divblockwidth2 a").each(function (value) {
                $(this).attr('status_flag', '0');
                $(this).removeAttr('class');

                if (quick_tag_counter == 0) {
                    $(this).attr('class', 'default_rating tags_qucls');
                }

                if (quick_tag_counter == 1) {
                    $(this).attr('class', 'default_rating tags_sugcls');
                }

                if (quick_tag_counter == 2) {
                    $(this).attr('class', 'default_rating tags_notescls');
                }

                if (quick_tag_counter == 3) {
                    $(this).attr('class', 'default_rating tags_strangthcls');
                }

                if (quick_tag_counter == 4) {
                    $(this).attr('class', 'default_rating tags_strangthcls');
                }

                quick_tag_counter++;
            });
            $("#expList").find('input:checkbox').removeAttr('checked');
            $('#txtSearchVideos').val('');
            $('#comment_comment').val('');
            $('#comment_comment').attr("placeholder", "Add a note...");
        }
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: form_post_data,
            success: function () {
                $('#comment_comment').val('');
                $('#synchro_time_class_tags').val('');
                getVideoComments(function () {
                    $('#txtVideoTags').removeAllTag();
                    $('#txtVideostandard_vid').removeAllTag();
                });
                var synchro_time_class = 'short_tag_0';
                if (typeof ($("#synchro_time_class").val()) != "undefined" && $("#synchro_time_class").val() !== null && $("#synchro_time_class").val().length > 0) {
                    synchro_time_class = $("#synchro_time_class").val();
                }
                VideoInstance.addMarker($('[name="synchro_time"]').val(), synchro_time_class);
                playPlayer();
            }
        });
    });

    $(document).on('change', '#cmt-sortings', function () {
        var srtType = $(this).val();
        var txtcomment = $('#txtSearchVideos').val();
        var tagid = '';
        $("#tagsDivcls2 a").each(function (value) {
            if ($(this).attr("status_flag") == "1") {
                tagid += $(this).attr("tag_id") + ',';
            }
        });
        commentsSorting(srtType, txtcomment, tagid, 'sorting');
    });
    $(document).on('keyup', '#txtSearchVideos', function (e) {

        var code = e.which;
        if (code == 13) {
            var srtType = $('#cmt-sortings').val();
            var txtcomment = $('#txtSearchVideos').val();
            var tagid = '';
            $("#tagsDivcls2 a").each(function (value) {
                if ($(this).attr("status_flag") == "1") {
                    tagid += $(this).attr("tag_id") + ',';
                }
            });
            commentsSorting(srtType, txtcomment, tagid, 'search');
        }
    });
    $('.clear_comment').on('click', function () {
        var srtType = $(this).val();
        var txtcomment = $('#txtSearchVideos').val();
        var tagid = '';
        $("#tagsDivcls2 a").each(function (value) {
            if ($(this).attr("status_flag") == "1") {
                tagid += $(this).attr("tag_id") + ',';
            }
        });
        commentsSorting(srtType, txtcomment, tagid, 'clear');
    });
    $('.clear_comment').on('click', function () {
        var srtType = $(this).val();
        var txtcomment = $('#txtSearchVideos').val();
        var tagid = '';
        $("#tagsDivcls2 a").each(function (value) {
            if ($(this).attr("status_flag") == "1") {
                tagid += $(this).attr("tag_id") + ',';
            }
        });
        commentsSorting(srtType, txtcomment, tagid, 'clear');
    });
    /*$(document).on("focus", ".std_tags", function () {
     console.log('test1');
     $("#frameWorkss").trigger("click");
     });*/
    $(document).on('keyup', '#txtSearchVideos', function (e) {

        var code = e.which;
        if (code == 13) {
            var srtType = $('#cmt-sortings').val();
            var txtcomment = $('#txtSearchVideos').val();
            var tagid = '';
            $("#tagsDivcls2 a").each(function (value) {
                if ($(this).attr("status_flag") == "1") {
                    tagid += $(this).attr("tag_id") + ',';
                }
            });
            commentsSorting(srtType, txtcomment, tagid, 'search');
        }
    });


    $('.comment_form').on('submit', '#comments-form1', function (event) {
        this.submit.disabled = 1;
        event.preventDefault();

        var $form = $(this);
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function () {
                getVideoComments(function () {
                    $form[0].submit.disabled = 0;
                });
                VideoInstance.addMarker($('[name="synchro_time"]').val());
                ob_getVideoComments();
            }
        });
    });

    //var pathnames = location.pathname.split('/');
    //while (pathnames.length > 0 && (pathnames[0] == '' || pathnames[0] == 'app')) {
    //    pathnames = pathnames.splice(1);
    //}
    //if (pathnames.length == 0 || !('users' == pathnames[0].toLowerCase() && 'login' == pathnames[1].toLowerCase())) {
    //    // consequently request to server to prevent session timeout
    //    setTimeout(executeQuery, 30000);
    //}
});
if (typeof ($('#videoLibrary').val()) == 'undefined') {
    (function poll() {

        setTimeout(function () {
            var processing_video_ids = [];
            $('.videos-list__item-thumb').each(function () {

                if (typeof ($('#txtHuddleID').val()) != 'undefined') {
                    var img = $(this).find('img').attr('src');
                    if (img == '/img/video-thumbnail.png')
                    {
                        var videoID = $(this).find('a').attr('href').split('/');
                        processing_video_ids.push(videoID['5']);
                    }
                } else if ($('#videoLibrary').val() != 'undefined' && $('#videoLibrary').val() == 1) {

                } else {

                    var img = $(this).find('img').attr('src');
                    if (img == '/img/video-thumbnail.png')
                    {
                        var videoID = $(this).find('a').attr('id');
                        videoID = parseInt(videoID.split('-')[2]);
                        processing_video_ids.push(videoID);
                    }

                }

            });
            $('[id^=processing-message] .video-unpublished').each(function (idx, el) {
                var parent = $(this).parent();
                var id = parent.attr('id');
                id = parseInt(id.split('-')[2]);
                processing_video_ids.push(id);
            });
            $.ajax({
                url: home_url + '/Huddles/sysRefreash',
                type: 'post',
                data: {
                    processing_video_ids: processing_video_ids,
                    created_date: $('#latest_video_created_date').val(),
                    video_envo: $('#video_envo').val(),
                },
                dataType: 'json'
            }).success(function (res) {
                //console.log( res );
                var bool = true;
                for (var id in res.data) {
                    if (res.data[id].indexOf("video-thumbnail.png") >= 0)
                    {
                        bool = false;
                    }
                }
                if (typeof ($('#txtHuddleID').val()) != 'undefined') {
                    if (res.latest_video_created_date && res.latest_video_created_date.length)
                    {
                        $('#latest_video_created_date').val(res.latest_video_created_date);
                        doVideoSearchAjax();
                    }
                }
                else
                {
                    if ($('#video_envo').val() == 3 && res.latest_video_created_date && res.latest_video_created_date.length)
                    {
                        $('#latest_video_created_date').val(res.latest_video_created_date);
                        doMyFileVideoSearchAjax();
                    }

                }

                if (res.ok && res.data != undefined) {
                    if (typeof ($('#txtHuddleID').val()) != 'undefined') {
                        if (bool)
                        {
                            doVideoSearchAjax();
                        }

                    } else if ($('#videoLibrary').val() != 'undefined' && $('#videoLibrary').val() == 1) {
                        if (res.ok && $(".vjs-control").hasClass("vjs-playing") != 'undefined' && $(".vjs-control").hasClass("vjs-playing") == true) {
                            //document_viewer_history();
                        }
                        doLibrarySearchAjax();
                    } else {
                        if (bool)
                        {
                            doMyFileVideoSearchAjax();
                        }
                    }
                    for (var id in res.data) {
                        var image = res.data[id];
                        $('#processing-message-' + id).html(image).append('<div class="play-icon" style="background-size: 50px;height: 50px;margin: -60px 0 0 -25px;width: 50px;"></div>');
                    }
                }
                if (res.ok && $(".vjs-control").hasClass("vjs-playing") != 'undefined' && $(".vjs-control").hasClass("vjs-playing") == true) {
                    //document_viewer_history();
                }

            }).always(function () {
                poll();
            });
        }, 30000);
    })();
}
/* Add Viewer History ---*/
/*function document_viewer_history() {
 $.ajax({
 url: home_url + '/app/add_viewer_history',
 method: 'POST',
 data: {
 document_id: $('#video-id').val(),
 user_id: $('#user_id').val(),
 minutes_watched: $('#synchro_time').val()
 },
 dataType: 'json',
 success: function (res) {
 console.log(res);
 }
 });
 }*/

/* begin comment function */

function commentsSorting(srtType, searchCmt, tags, event_type) {

    if ($('#video-id').length == 0)
        return;
    var parts = location.pathname.toLowerCase().split('/').slice(1);
    var videoId = $('#video-id').val();
    var url = home_url +
            (parts[0] == 'huddles' || (parts[0] == 'app' && parts[1] == 'huddles') ? '/Huddles/getVideoComments/' : '/MyFiles/getVideoComments/') +
            videoId;
    $.ajax({
        dataType: 'JSON',
        type: 'POST',
        data: {
            type: 'get_video_comments', srtType: srtType, searchCmt: searchCmt, tags: tags, event_type: event_type
        },
        url: url,
        success: function (response) {
            var href_split_pdf = $('#comment-acro').attr('href').split('/');
            href_split_pdf[6] = srtType != "" ? srtType : "";
            var href_new_pdf = href_split_pdf[0] + "//" + href_split_pdf[2] + "/" + href_split_pdf[3] + "/" + href_split_pdf[4] + "/" + href_split_pdf[5] + "/" + href_split_pdf[6] + "/" + href_split_pdf[7];
            $('#comment-acro').attr('href', href_new_pdf);

            var href_split_exl = $('#comment-excel').attr('href').split('/');
            href_split_exl[6] = srtType != "" ? srtType : "";
            var href_new_exl = href_split_exl[0] + "//" + href_split_exl[2] + "/" + href_split_exl[3] + "/" + href_split_exl[4] + "/" + href_split_exl[5] + "/" + href_split_exl[6];
            $('#comment-excel').attr('href', href_new_exl);

            $('#vidComments').html(response.contents);
            if (searchCmt === undefined || searchCmt === null) {
                $('#txtSearchVideos').val('');
            } else {
                $('#txtSearchVideos').val(searchCmt);
            }
            var tagid = '';
            if (tags === undefined || tags === null || tags === '') {

            }
            else {
                console.log(tags);
                console.log(tags);
                var tagids = tags.split(',');
                if (tagids.length === -1) {
                    $('#tagsDivcls2 a').each(function (value) {
                        positionid = $(this).attr("position_id");
                        $(this).attr("status_flag", '0');
//                        $(this).removeAttr('class');
//                        $(this).attr('class', 'tagsDivcls uncheck');
                        $(this).addClass(defaulttagsclasses1(positionid));
                        $(this).removeClass(defaulttagsclasses(positionid));
                    });

                } else {
                    $('#tagsDivcls2 a').each(function (value) {
                        tag_id = $(this).attr("tag_id");
                        positionid = $(this).attr("position_id");
                        if ($.inArray(tag_id, tagids) > -1) {
                            $(this).attr("status_flag", '1');
                            $(this).addClass(defaulttagsclasses(positionid));
                            $(this).removeClass(defaulttagsclasses1(positionid));
                        } else {
                            $(this).attr("status_flag", '0');
//                            $(this).removeAttr('class');
//                            $(this).attr('class', 'tagsDivcls uncheck');
                            $(this).addClass(defaulttagsclasses1(positionid));
                            $(this).removeClass(defaulttagsclasses(positionid));
                        }
                    });

                }
            }
            if (typeof initVideoComments == 'function') {
                initVideoComments();
            }
//            $(document).on('change', '#cmt-sortings', function () {
//                var srtType = $(this).val();
//                commentsSorting(srtType);
//            });
            var total_comments = 0;
            if (response.total_comment > 0) {
                total_comments = response.total_comment;
            }
            //var response_values = jQuery.parseJSON(response);
            if (location.href.indexOf('MyFiles') >= 0) {
                //$('#comments').html('Notes (' + response_values.total_comments + ')');
                $('#comments').text('Notes (' + total_comments + ')');
            } else {
                $('#comments').text('Comments (' + total_comments + ')');
            }
            //$('#comments').text('Comments (' + total_comments + ')');
            $.each(response.tag_count, function (key, value) {
                console.log('a.tagsDivcls[tag_id="' + value.account_tag_id + '"]');
                $('a.tagsDivcls[tag_id="' + value.account_tag_id + '"]').find('span').text(value.total);
            });


            $('.synchro-time').on('click', function () {
                setTimeout(function () {
                    $('[name="synchro_time"]').val($(this).data('time'));
                    if ($('#video_span').length && small_scroll) {
                        console.log('scroll to video');
                        $('html, body').animate({
                            scrollTop: $('#video_span').offset().top
                        }, 400);
                    }
                }, 1000);
            });
        },
        errors: function (response) {
            alert(response.contents);
        }
    });
}

function initVideoComments() {
    if ($('#for_synchro_time') && $('#for_synchro_time').length > 0) {
        $('#for_synchro_time').click(function () {
            comment_radio_selected_by_user = true;
        });
    }

    if ($('#for_entire_video') && $('#for_entire_video').length > 0) {
        $('#for_entire_video').click(function () {
            comment_radio_selected_by_user = true;
        });
    }

    if ($('#comment_comment') && $('#comment_comment').length > 0) {

        $('#comment_comment').keyup(function (e) {

            if (comment_radio_selected_by_user == false) {
                var value = $("#pause_while_type").is(':checked') ? 1 : 0;
                if (value && e.which != 13)
                {
                    pausePlayer();
                }
                if ($('#for_synchro_time') && $('#for_synchro_time').length > 0)
                    $('#for_synchro_time').prop('checked', true);
            }

        });
    }

    if ($("#add_comment_button") && $("#add_comment_button").length > 0) {
        $("#add_comment_button").click(function () {
            $(".divblockwidth").show("fast");
            $("#comment_form_main").show("fast", function () {
                comment_radio_selected_by_user = false;
                pausePlayer();
                if ($('#for_synchro_time') && $('#for_synchro_time').length > 0)
                    $('#for_synchro_time').prop('checked', true);
                $('#comment_comment').focus();
            });
            $("#add_comment_button").hide();
        });
    }

    if ($("#close_comment_form") && $("#close_comment_form").length > 0) {
        $("#close_comment_form").click(function () {
            //$("#comment_form_main").hide("fast");
            // $(".divblockwidth").hide("fast");
            //$("#add_comment_button").show();
            $('#comment_comment').val('');
            $('#txtVideoTags').removeAllTag();
            $('#txtVideostandard_vid').removeAllTag();
//            $('#txtVideoTags_tagsinput span').remove();
//            $('#txtVideostandard_tagsinput span').remove();
            $('#txtVideostandard_tag').show();
            var tagid = '';
            var count = 1;
            $(".divblockwidth a").each(function (value) {
                $(this).attr('status_flag', '0');
                $(this).removeAttr('class');
                if (count == 1) {
                    $(this).attr('class', 'default_tag tags_qucls');
                }
                else if (count == 2) {
                    $(this).attr('class', 'default_tag tags_sugcls');
                }
                else if (count == 3) {
                    $(this).attr('class', 'default_tag tags_notescls');
                }
                else if (count == 4) {
                    $(this).attr('class', 'default_tag tags_strangthcls');
                }
                count++;
            });
            $("#expList").find('input:checkbox').removeAttr('checked');
            $('#comment_comment').attr("placeholder", "Add a note...");
            playPlayer();
        });
    }
}


function getVideoComments(callback) {

    if ($('#video-id').length == 0)
        return;

    var parts = location.pathname.toLowerCase().split('/').slice(1);
    var videoId = $('#video-id').val();
    get_comment_count(videoId);
    var url = home_url +
            (parts[0] == 'huddles' || (parts[0] == 'app' && parts[1] == 'huddles') ? '/Huddles/getVideoComments/' : '/MyFiles/getVideoComments/') +
            videoId;
    var srtType = $('#cmt-sortings').val();
    var txtcomment = $('#txtSearchVideos').val();
    if (txtcomment === undefined || txtcomment === null) {
        txtcomment = '';
    }
    var check_tags = $('#tagsDivcls2').val();
    var tagid = '';
    if (check_tags === undefined || check_tags === null) {
        tagid = '';
    }
    else {
        $(this).parent().find('[status_flag="1"]').each(function (value) {
            tagid += $(this).attr("tag_id") + ',';
        });
    }
    $.ajax({
        dataType: 'JSON',
        type: 'POST',
        data: {type: 'get_video_comments', srtType: srtType, searchCmt: txtcomment, tags: tagid},
        url: url,
        success: function (response) {

            if (typeof callback == 'function') {
                callback();
            }
            if (typeof initVideoComments == 'function') {
                initVideoComments();
            }

            $('.synchro-time').on('click', function () {
                setTimeout(function () {
                    $('[name="synchro_time"]').val($(this).data('time'));
                    if ($('#video_span').length && small_scroll) {
                        $('html, body').animate({
                            scrollTop: $('#video_span').offset().top
                        }, 400);
                    }
                }, 1000);
            });
            $('.modal').modal('hide');
            $('#vidComments').html(response.contents);

        },
        errors: function (response) {
            alert(response.contents);
        }
    });
}
function get_comment_count(video_id) {

    $.ajax({
        url: home_url + "/Huddles/get_comments_count/" + video_id,
        data: {type: 'get_video_comments'},
        type: 'POST',
        success: function (response) {
            console.log('decent');
            var response_values = jQuery.parseJSON(response);

            if (location.href.indexOf('MyFiles') >= 0) {
                $('#comments').html('Notes (' + response_values.total_comments + ')');
                $('.copy').attr('data-total-comments',response_values.total_comments);
            } else {
                $('#comments').html('Comments (' + response_values.total_comments + ')');
                $('.copy').attr('data-total-comments',response_values.total_comments);
            }
            console.log(response_values);
            $.each(response_values.tag_count, function (key, value) {
                $('a.tagsDivcls[tag_id="' + value.account_tag_id + '"]').find('span').text(value.total);
            });
            if ($('#huddle_type').val() == 3) {
                if (response_values.is_feedback_published) {
                    console.log(response_values.total_comments);
                    $('#publish-feeback').hide();
                    $('#comment-excel').hide();
                    $('#comment-acro').hide();
                    $('#email_send').hide();
                } else {
                    console.log(response_values.total_comments);
                    $('#publish-feeback').show();
                    $('#comment-excel').show();
                    $('#comment-acro').show();
                    $('#email_send').show();
                }
            }

        },
        error: function () {
            alert("Network Error Occured");
        }
    });
}

function getVdoComments(callback) {

    if ($('#video-id').length == 0)
        return;

    var parts = location.pathname.toLowerCase().split('/').slice(1);
    var videoId = $('#video-id').val();
    get_comment_count(videoId);
    var url = home_url +
            (parts[0] == 'huddles' || (parts[0] == 'app' && parts[1] == 'huddles') ? '/Huddles/getVideoComments/' : '/MyFiles/getVideoComments/') +
            videoId;
    var srtType = 4;
    var txtcomment = $('#txtSearchVideos').val();
    if (txtcomment === undefined || txtcomment === null) {
        txtcomment = '';
    }
    var check_tags = $('#tagsDivcls2').val();
    var tagid = '';
    if (check_tags === undefined || check_tags === null) {
        tagid = '';
    }
    else {
        $(this).parent().find('[status_flag="1"]').each(function (value) {
            tagid += $(this).attr("tag_id") + ',';
        });
    }
    $.ajax({
        dataType: 'JSON',
        type: 'POST',
        data: {type: 'get_video_comments', srtType: srtType, searchCmt: txtcomment, tags: tagid},
        url: url,
        success: function (response) {

            if (typeof callback == 'function') {
                callback();
            }
            if (typeof initVideoComments == 'function') {
                initVideoComments();
            }

            $('.synchro-time').on('click', function () {
                setTimeout(function () {
                    $('[name="synchro_time"]').val($(this).data('time'));
                    if ($('#video_span').length && small_scroll) {
                        $('html, body').animate({
                            scrollTop: $('#video_span').offset().top
                        }, 400);
                    }
                }, 1000);
            });
            $('#vidComments').html(response.contents);

        },
        errors: function (response) {
            alert(response.contents);
        }
    });
}

function ob_getVideoComments(callback) {
    if ($('#video-id').length == 0)
        return;
    var parts = location.pathname.toLowerCase().split('/').slice(1);
    var videoId = $('#ob_account_folder_id').val();
    var url = home_url +
            (parts[0] == 'huddles' || (parts[0] == 'app' && parts[1] == 'huddles') ? '/Huddles/getVideoComments/' : '/MyFiles/getVideoComments/') +
            videoId;
    $.ajax({
        dataType: 'JSON',
        type: 'POST',
        data: {type: 'get_video_comments_obs'},
        url: url,
        success: function (response) {
            $('#vidComments_obs').html(response.contents);
            if (typeof initVideoComments == 'function') {
                initVideoComments();
            }
            if (typeof callback == 'function') {
                callback();
            }
            $('.synchro-time').on('click', function () {
                setTimeout(function () {
                    $('[name="synchro_time"]').val($(this).data('time'));
                    if ($('#video_span').length && small_scroll) {
                        console.log('scroll to video');
                        $('html, body').animate({
                            scrollTop: $('#video_span').offset().top
                        }, 400);
                    }
                }, 1000);
            });

            var $scrollbar1 = $("#scrollbar1");
            $scrollbar1.tinyscrollbar();
        },
        errors: function (response) {
            alert(response.contents);
        }
    })
}
function ob_filtered_getVideoComments($tagsArray) {
    if ($('#ob_account_folder_id').length == 0)
        return;
//    console.log($tagsArray);
    var parts = location.pathname.toLowerCase().split('/').slice(1);
    var videoId = $('#ob_account_folder_id').val();
    var url = home_url +
            (parts[0] == 'huddles' || (parts[0] == 'app' && parts[1] == 'huddles') ? '/Huddles/getVideoComments/' : '/MyFiles/getVideoComments/') +
            videoId;
    $.ajax({
        dataType: 'JSON',
        type: 'POST',
        data: {type: 'get_video_comments_obs', tagsArray: $tagsArray},
        url: url,
        success: function (response) {
            $('#vidComments_obs').html(response.contents);
            if (typeof initVideoComments == 'function') {
                initVideoComments();
            }
            if (typeof callback == 'function') {
                callback();
            }
            $('.synchro-time').on('click', function () {
                setTimeout(function () {
                    $('[name="synchro_time"]').val($(this).data('time'));
                    if ($('#video_span').length && small_scroll) {
                        console.log('scroll to video');
                        $('html, body').animate({
                            scrollTop: $('#video_span').offset().top
                        }, 400);
                    }
                }, 1000);
            });

            var $scrollbar1 = $("#scrollbar1");
            $scrollbar1.tinyscrollbar();
        },
        errors: function (response) {
            alert(response.contents);
        }
    })
}

/* end comment function */

//------------------------Search Bars ---------------------------//
function doSearchSuperUser(keywords) {
    $.ajax({
        url: home_url + "/Users/ajax_super_user/",
        data: {keywords: keywords},
        type: 'POST',
        success: function (response) {
            $('#super-user-list').html(response)
        },
        error: function () {
            alert("Network Error Occured");
        }
    });
}
function doSearchAdminUser(keywords) {
    $.ajax({
        url: home_url + "/Users/ajax_user/",
        data: {keywords: keywords},
        type: 'POST',
        success: function (response) {
            $('#user-list').html(response)
        },
        error: function () {
            alert("Network Error Occured");
        }


    });
}
function doSearchGroup(keywords) {
    $.ajax({
        url: home_url + "/Users/ajax_groups/",
        data: {keywords: keywords},
        type: 'POST',
        success: function (response) {
            $('#grous-list').html(response)
        },
        error: function () {
            alert("Network Error Occured");
        }


    });
}
function doSearchHuddles(keywords, account_id, user_id) {
    $.ajax({
        url: home_url + "/Permissions/ajax_filter/" + account_id + "/" + user_id,
        data: {keywords: keywords},
        type: 'POST',
        success: function (response) {
            $('#huddles-lists').html(response);
        },
        error: function () {
            alert("Network Error Occured");
        }


    });
}

function doSearchPoeple(keyword, huddle_id) {
    $.ajax({
        url: home_url + "/Huddles/ajax_people_filter/" + huddle_id,
        data: {keywords: keyword},
        type: 'POST',
        success: function (response) {
            $('#people-lists').html(response);
        },
        error: function () {
            alert("Network Error Occured");
        }
    });
}

//------------------------Huddle Search--------------------------//

function doHuddleSearch() {
    var $title = $('#txtSearchHuddles').val();
    var $mode = $('#search-mode').val();
    //$.blockUI({message: $('#domMessage').html()});
    $.ajax({
        url: home_url + '/Huddles/search',
        data: {title: $title, mode: $mode},
        dataType: 'JSON',
        type: 'POST',
        success: function (response) {
            //$(document).ajaxStop($.unblockUI);
            if (response.status == true) {
                $('#huddle-listings').html(response.contents);
                $('.page-title__counter').html($('#search-huddle-count').html());
            } else {
                $('.page-title__counter').html($('#search-huddle-count').html());
                $('#huddle-listings').html(response.contents);
            }
            dragndrop();
        },
        error: function () {

        }
    });
}

function doHuddleSearch_Textbox() {
    var $title = $('#txtSearchHuddles').val();
    var $mode = $('#search-mode').val();
    //$.blockUI({message: $('#domMessage').html()});
    $.ajax({
        url: home_url + '/Huddles/search_textbox',
        data: {title: $title, mode: $mode},
        dataType: 'JSON',
        type: 'POST',
        success: function (response) {
            //$(document).ajaxStop($.unblockUI);
            if (response.status == true) {
                $('#huddle-listings').html(response.contents);
                $('.page-title__counter').html($('#search-huddle-count').html());
            } else {
                $('.page-title__counter').html($('#search-huddle-count').html());
                $('#huddle-listings').html(response.contents);
            }
            dragndrop();
        },
        error: function () {

        }
    });
}



//------------------------Observation Search--------------------------//
function doObservationSearch() {
    $('#loader-gif-div').show();
    $("#observations-grids-main").html('');


    var $sortmodeText = $('#cmbSearchObsInHuddle').val();
    var $filterText = $('#filter-mode').find('li .active').parent().attr('id');
    var $title = $('#txtSearchObservations').val();


    $.ajax({
        url: home_url + '/Observe/getAjaxObservations',
        type: 'POST',
        data: {
            page: 1,
            sort: $sortmodeText,
            title: $title,
            filter: $filterText
        },
        success: function (msg) {
            $('#loader-gif-div').hide();
            $("#observations-grids-main").html(msg);
            $('.page-title__counter').html($('#search-huddle-count').html());
        }
    });
}
//------------------------Observation Search end --------------------------//

function doDocumentSearchAjax(callback) {
    var keywords = $('#txtSearchDocuments').val();
    if (keywords == 'Search Documents...') {
        keywords = '';
    }
    $.ajax({
        type: 'POST',
        data: {
            sort: $('#cmbDocumentSort').val()
        },
        url: home_url + '/Huddles/getDocumentSearch/' + $('#txtHuddleID').val() + '/' + keywords,
        success: function (response) {
            $('.ul-docs').html(response);
            gApp.updateCheckboxEvent($('.ul-docs .ui-checkbox input'));
            if (typeof callback == 'function') {
                callback();
            }
        },
        errors: function (response) {
            alert(response.contents);
        }
    });
}

function doDocumentSearchAjaxInit() {

    var keywords = $('#txtSearchDocuments').val();
    if (keywords == 'Search Documents...') {
        keywords = '';
    }
    $.ajax({
        type: 'POST',
        data: {
            sort: $('#cmbDocumentSort').val(),
            state: 'init'
        },
        url: home_url + '/Huddles/getDocumentSearch/' + $('#txtHuddleID').val() + '/' + keywords,
        success: function (response) {
            $('.ul-docs').html(response);
            gApp.updateCheckboxEvent($('.ul-docs .ui-checkbox input'));
            if (typeof callback == 'function') {
                callback();
            }
        },
        errors: function (response) {
            alert(response.contents);
        }
    });
}

function doObservationDocumentSearchAjax(callback) {
    var keywords = $('#txtSearchDocuments').val();
    if (keywords == 'Search Documents...') {
        keywords = '';
    }
    $.ajax({
        type: 'POST',
        data: {
            sort: $('#cmbDocumentSort').val()
        },
        url: home_url + '/Huddles/getDocumentSearch/' + $('#ob_account_folder_id').val() + '/' + keywords,
        success: function (response) {
            $('#obs-docs-container .ul-docs').html(response);
            gApp.updateCheckboxEvent($('#obs-docs-container.ul-docs #obs-docs-container.ui-checkbox input'));
            if (typeof callback == 'function') {
                callback();
            }
        },
        errors: function (response) {
            alert(response.contents);
        }
    });
}

function doHuddleDiscussionSearchAjax() {
    $.ajax({
        type: 'POST',
        data: {
            type: 'get_video_comments',
            sort: $('#cmbDiscussionSort').val()
        },
        url: home_url + '/Huddles/getDiscussionSearch/' + $('#txtHuddleID').val() + '/' + $('#txtSearchHuddleDiscussions').val(),
        success: function (response) {
            $('#ulDiscussions').html(response);
            $('div.tab-3 h1').html('Huddle Discussion (' + $('#ulDiscussions li.discussion_row').length + ')');
        },
        errors: function (response) {
            alert(response.contents);
        }
    });
}

function doObVideoSearchAjax(page) {
    if (!page)
        page = 1;

    var keywords = $('#txtSearchObVideos').val();
    if (keywords == 'Search Videos...') {
        keywords = '';
    }

    $.ajax({
        type: 'POST',
        data: {
            type: 'get_video_comments',
            sort: $('#cmbObVideoSort').val(),
            page: page
        },
        dataType: 'json',
        url: home_url + '/Huddles/getObVideoSearch/' + $('#txtHuddleID').val() + '/' + keywords,
        success: function (response) {
            $('#ob-videos-list').html(response.html);
            $('#huddle_ob_video_title_strong').html('Observations (' + response.count + ')');
            $('#ob-videos-list a[data-confirm]').click(function (event) {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
                if (confirm($(this).attr('data-confirm'))) {
                    try {
                        eval($(this).attr('href'));
                    }
                    catch (e) {
                        location.href = $(this).attr('href');
                    }
                }
                return false;
            });
        },
        errors: function (response) {
            alert(response.contents);
        }

    });
}

function doObVideoSearchAjax1(page) {
    if (!page)
        page = 1;



    $.ajax({
        type: 'POST',
        data: {
            type: 'get_video_comments',
            sort: $('#cmbObVideoSort').val(),
            page: page
        },
        dataType: 'json',
        url: home_url + '/Huddles/getObVideoSearch/' + $('#txtHuddleID').val(),
        success: function (response) {
            $('#ob-videos-list').html(response.html);
            // $('#huddle_ob_video_title_strong').html('Observations (' + $('#ob-videos-list li.videos-list__item').length + ')');
            $('#ob-videos-list a[data-confirm]').click(function (event) {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
                if (confirm($(this).attr('data-confirm'))) {
                    try {
                        eval($(this).attr('href'));
                    }
                    catch (e) {
                        location.href = $(this).attr('href');
                    }
                }
                return false;
            });
        },
        errors: function (response) {
            alert(response.contents);
        }

    });
}




function doVideoSearchAjax(page) {
    if (!page)
        page = 1;

    var keywords = $('#txtSearchVideos').val();
    if (keywords == 'Search Videos...') {
        keywords = '';
    }

    $.ajax({
        type: 'POST',
        data: {
            type: 'get_video_comments',
            sort: $('#cmbVideoSort').val(),
            page: page
        },
        url: home_url + '/Huddles/getVideoSearch/' + $('#txtHuddleID').val() + '/' + keywords,
        success: function (response) {
            $('#videos-list').html(response);
            $('#huddle_video_title_strong').html('Videos (' + $('#videos-list li.videos-list__item').length + ')');
            $('#videos-list a[data-confirm]').click(function (event) {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
                if (confirm($(this).attr('data-confirm'))) {
                    try {
                        eval($(this).attr('href'));
                    }
                    catch (e) {
                        location.href = $(this).attr('href');
                    }
                }
                return false;
            });
        },
        errors: function (response) {
            alert(response.contents);
        }

    });
}
function doObsInHuddleSearchAjax(page) {
    $('#loader-gif-div').show();
    $('#observation-list').html('');
    if (!page)
        page = 1;

    var keywords = $('#txtSearchObsInHuddle').val();
    if (keywords == 'Search Videos...') {
        keywords = '';
    }
    $.ajax({
        type: 'POST',
        data: {
            type: 'get_huddel_observations',
            huddle_id: $('#txtHuddleID:input').val(),
            sort: $('#cmbSearchObsInHuddle').val(),
            page: 2,
            title: keywords,
            filter: 'all',
        },
        url: home_url + '/Observe/getAjaxObservations',
        success: function (response) {
            page = 2;
            $('#loader-gif-div').hide();
            $('#observation-list').html(response);
            $('#loadMoreStartOver').val('1');
            showLoadMoreObservations();
            if ($('#txtobservationCount:input').val() != '')
            {
                console.log('txtobservationCount NOT EMTY');
                $('span#observationCountTxt').html('(' + $('#txtobservationCount:input').val() + ')');
                $('#txtObsCount:input').val($('#txtobservationCount:input').val());


                if ($('#txtobservationCount:input').val() <= $('#observationsPerPage:input').val()) {
                    console.log('txtobservationCount LESS TEN EQUAL TO txtobservationCount');
                    $('#load_more_observations').hide();
                } else {
                    console.log('ELSE');
                    $('#load_more_observations').show();
                }
            } else {
                console.log('ELSE Not Empty');
                $('#observationCountTxt:span').html('(0)');
                $('#txtObsCount:input').val('0');
                $('#load_more_observations').hide();
            }
            $('#observation-list a[data-confirm]').click(function (event) {
                event.preventDefault();
                event.stopPropagation();
                event.stopImmediatePropagation();
                if (confirm($(this).attr('data-confirm'))) {
                    try {
                        eval($(this).attr('href'));
                    }
                    catch (e) {
                        location.href = $(this).attr('href');
                    }
                }
                return false;
            });
        },
        errors: function (response) {
            alert(response.contents);
        }

    });
}

function appendix($el) {
    $el.each(function () {

        var $this = $(this),
                $content = $this.next('.appendix-content'),
                offset = null,
                position,
                parentWidth,
                leftAligned;

        $this.on('click', function (e) {
            position = $this.position();
            parentWidth = $this.offsetParent().width();
            leftAligned = position.left < parentWidth / 2;

            if (leftAligned)
                $content.addClass('left-aligned');

            if (offset == null) {
                if ($content.hasClass('down')) {
                    offset = {
                        top: position.top + 40,
                        left: position.left - ($this.width() / 2) +
                                (leftAligned ? 0 : -$content.outerWidth()
                                        + ($this.parent().outerWidth() - $this.parent().width()) + ($this.outerWidth() - $this.width())
                                        + parseInt($content.css('margin-left'))
                                        + 10)
                    };
                }
                else if ($content.hasClass('card')) {
                    offset = {
                        top: position.top - $content.outerHeight() - 20,
                        left: position.left - (leftAligned ? 0 : $content.outerWidth() + 40) - $this.width() / 2
                    };
                }
                else {
                    offset = {
                        top: position.top - $content.outerHeight() - 20,
                        left: position.left - $content.outerWidth() / 2 + 10
                    };
                }
                $content.offset(offset);
            }

            //$content.stop()[e.type == 'mouseout' ? 'fadeOut' : 'fadeIn']('fast');
            e.preventDefault();
            $(".appendix-content").not($content).css('display', 'none');
            $content.stop().fadeToggle('fast');

        });

    });
}

function showProcessOverlay(container) {
    var overlay = container.find('.process-overlay');
    if (overlay.length == 0) {
        overlay = $('<div class="process-overlay"></div>');
        container.append(overlay);
    }
    container.css('position', 'relative');
    overlay.show();
}

function hideProcessOverlay(container) {
    container.find('.process-overlay').hide();
    container.css('position', '');
}

function isIOS() {
    return !!(navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i));
}

function isFirefox() {
    var isFirefox = typeof InstallTrigger !== 'undefined';
    return isFirefox;
}
function pausePlayer() {
    var videoPlayer = vjs('example_video_' + $('#txtCurrentVideoID').val());
    if (videoPlayer != 'undefined')
        try {
            videoPlayer.pause();
        } catch (e) {

        }
}
function playPlayer() {
    var videoPlayer = vjs('example_video_' + $('#txtCurrentVideoID').val());
    if (videoPlayer != 'undefined')
        try {
            videoPlayer.play();
        } catch (e) {

        }
}

