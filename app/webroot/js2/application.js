/*! jQuery v1.7.2 jquery.com | jquery.org/license */
function remove_fields(e) {
    $(e).previous("input[type=hidden]").value = "1", $(e).up(".fields").hide()
}
function add_fields(e, t, n) {
    var r = (new Date).getTime(), i = new RegExp("new_" + t, "g");
    $(e).up().insert({before: n.replace(i, r)})
}
function remove_fields(e) {
    $(e).prev("input[type=hidden]").val("1"), $(e).closest(".fields").hide()
}
function add_fields(e, t, n) {
    var r = (new Date).getTime(), i = new RegExp("new_" + t, "g");
    $(e).parent().before(n.replace(i, r))
}
(function(e, t) {
    function n(e) {
        return H.isWindow(e) ? e : e.nodeType === 9 ? e.defaultView || e.parentWindow : !1
    }
    function r(e) {
        if (!vn[e]) {
            var t = _.body, n = H("<" + e + ">").appendTo(t), r = n.css("display");
            n.remove();
            if (r === "none" || r === "") {
                mn || (mn = _.createElement("iframe"), mn.frameBorder = mn.width = mn.height = 0), t.appendChild(mn);
                if (!gn || !mn.createElement)
                    gn = (mn.contentWindow || mn.contentDocument).document, gn.write((H.support.boxModel ? "<!doctype html>" : "") + "<html><body>"), gn.close();
                n = gn.createElement(e), gn.body.appendChild(n), r = H.css(n, "display"), t.removeChild(mn)
            }
            vn[e] = r
        }
        return vn[e]
    }
    function i(e, t) {
        var n = {};
        return H.each(En.concat.apply([], En.slice(0, t)), function() {
            n[this] = e
        }), n
    }
    function s() {
        Sn = t
    }
    function o() {
        return setTimeout(s, 0), Sn = H.now()
    }
    function u() {
        try {
            return new e.ActiveXObject("Microsoft.XMLHTTP")
        } catch (t) {
        }
    }
    function a() {
        try {
            return new e.XMLHttpRequest
        } catch (t) {
        }
    }
    function f(e, n) {
        e.dataFilter && (n = e.dataFilter(n, e.dataType));
        var r = e.dataTypes, i = {}, s, o, u = r.length, a, f = r[0], l, c, h, p, d;
        for (s = 1; s < u; s++) {
            if (s === 1)
                for (o in e.converters)
                    typeof o == "string" && (i[o.toLowerCase()] = e.converters[o]);
            l = f, f = r[s];
            if (f === "*")
                f = l;
            else if (l !== "*" && l !== f) {
                c = l + " " + f, h = i[c] || i["* " + f];
                if (!h) {
                    d = t;
                    for (p in i) {
                        a = p.split(" ");
                        if (a[0] === l || a[0] === "*") {
                            d = i[a[1] + " " + f];
                            if (d) {
                                p = i[p], p === !0 ? h = d : d === !0 && (h = p);
                                break
                            }
                        }
                    }
                }
                !h && !d && H.error("No conversion from " + c.replace(" ", " to ")), h !== !0 && (n = h ? h(n) : d(p(n)))
            }
        }
        return n
    }
    function l(e, n, r) {
        var i = e.contents, s = e.dataTypes, o = e.responseFields, u, a, f, l;
        for (a in o)
            a in r && (n[o[a]] = r[a]);
        while (s[0] === "*")
            s.shift(), u === t && (u = e.mimeType || n.getResponseHeader("content-type"));
        if (u)
            for (a in i)
                if (i[a] && i[a].test(u)) {
                    s.unshift(a);
                    break
                }
        if (s[0]in r)
            f = s[0];
        else {
            for (a in r) {
                if (!s[0] || e.converters[a + " " + s[0]]) {
                    f = a;
                    break
                }
                l || (l = a)
            }
            f = f || l
        }
        if (f)
            return f !== s[0] && s.unshift(f), r[f]
    }
    function c(e, t, n, r) {
        if (H.isArray(t))
            H.each(t, function(t, i) {
                n || Ut.test(e) ? r(e, i) : c(e + "[" + (typeof i == "object" ? t : "") + "]", i, n, r)
            });
        else if (!n && H.type(t) === "object")
            for (var i in t)
                c(e + "[" + i + "]", t[i], n, r);
        else
            r(e, t)
    }
    function h(e, n) {
        var r, i, s = H.ajaxSettings.flatOptions || {};
        for (r in n)
            n[r] !== t && ((s[r] ? e : i || (i = {}))[r] = n[r]);
        i && H.extend(!0, e, i)
    }
    function p(e, n, r, i, s, o) {
        s = s || n.dataTypes[0], o = o || {}, o[s] = !0;
        var u = e[s], a = 0, f = u ? u.length : 0, l = e === rn, c;
        for (; a < f && (l || !c); a++)
            c = u[a](n, r, i), typeof c == "string" && (!l || o[c] ? c = t : (n.dataTypes.unshift(c), c = p(e, n, r, i, c, o)));
        return(l || !c) && !o["*"] && (c = p(e, n, r, i, "*", o)), c
    }
    function d(e) {
        return function(t, n) {
            typeof t != "string" && (n = t, t = "*");
            if (H.isFunction(n)) {
                var r = t.toLowerCase().split(Zt), i = 0, s = r.length, o, u, a;
                for (; i < s; i++)
                    o = r[i], a = /^\+/.test(o), a && (o = o.substr(1) || "*"), u = e[o] = e[o] || [], u[a ? "unshift" : "push"](n)
            }
        }
    }
    function v(e, t, n) {
        var r = t === "width" ? e.offsetWidth : e.offsetHeight, i = t === "width" ? 1 : 0, s = 4;
        if (r > 0) {
            if (n !== "border")
                for (; i < s; i += 2)
                    n || (r -= parseFloat(H.css(e, "padding" + jt[i])) || 0), n === "margin" ? r += parseFloat(H.css(e, n + jt[i])) || 0 : r -= parseFloat(H.css(e, "border" + jt[i] + "Width")) || 0;
            return r + "px"
        }
        r = Ft(e, t);
        if (r < 0 || r == null)
            r = e.style[t];
        if (Dt.test(r))
            return r;
        r = parseFloat(r) || 0;
        if (n)
            for (; i < s; i += 2)
                r += parseFloat(H.css(e, "padding" + jt[i])) || 0, n !== "padding" && (r += parseFloat(H.css(e, "border" + jt[i] + "Width")) || 0), n === "margin" && (r += parseFloat(H.css(e, n + jt[i])) || 0);
        return r + "px"
    }
    function m(e) {
        var t = _.createElement("div");
        return Lt.appendChild(t), t.innerHTML = e.outerHTML, t.firstChild
    }
    function g(e) {
        var t = (e.nodeName || "").toLowerCase();
        t === "input" ? y(e) : t !== "script" && typeof e.getElementsByTagName != "undefined" && H.grep(e.getElementsByTagName("input"), y)
    }
    function y(e) {
        if (e.type === "checkbox" || e.type === "radio")
            e.defaultChecked = e.checked
    }
    function b(e) {
        return typeof e.getElementsByTagName != "undefined" ? e.getElementsByTagName("*") : typeof e.querySelectorAll != "undefined" ? e.querySelectorAll("*") : []
    }
    function w(e, t) {
        var n;
        t.nodeType === 1 && (t.clearAttributes && t.clearAttributes(), t.mergeAttributes && t.mergeAttributes(e), n = t.nodeName.toLowerCase(), n === "object" ? t.outerHTML = e.outerHTML : n !== "input" || e.type !== "checkbox" && e.type !== "radio" ? n === "option" ? t.selected = e.defaultSelected : n === "input" || n === "textarea" ? t.defaultValue = e.defaultValue : n === "script" && t.text !== e.text && (t.text = e.text) : (e.checked && (t.defaultChecked = t.checked = e.checked), t.value !== e.value && (t.value = e.value)), t.removeAttribute(H.expando), t.removeAttribute("_submit_attached"), t.removeAttribute("_change_attached"))
    }
    function E(e, t) {
        if (t.nodeType === 1 && !!H.hasData(e)) {
            var n, r, i, s = H._data(e), o = H._data(t, s), u = s.events;
            if (u) {
                delete o.handle, o.events = {};
                for (n in u)
                    for (r = 0, i = u[n].length; r < i; r++)
                        H.event.add(t, n, u[n][r])
            }
            o.data && (o.data = H.extend({}, o.data))
        }
    }
    function S(e, t) {
        return H.nodeName(e, "table") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
    }
    function x(e) {
        var t = dt.split("|"), n = e.createDocumentFragment();
        if (n.createElement)
            while (t.length)
                n.createElement(t.pop());
        return n
    }
    function T(e, t, n) {
        t = t || 0;
        if (H.isFunction(t))
            return H.grep(e, function(e, r) {
                var i = !!t.call(e, r, e);
                return i === n
            });
        if (t.nodeType)
            return H.grep(e, function(e, r) {
                return e === t === n
            });
        if (typeof t == "string") {
            var r = H.grep(e, function(e) {
                return e.nodeType === 1
            });
            if (lt.test(t))
                return H.filter(t, r, !n);
            t = H.filter(t, r)
        }
        return H.grep(e, function(e, r) {
            return H.inArray(e, t) >= 0 === n
        })
    }
    function N(e) {
        return!e || !e.parentNode || e.parentNode.nodeType === 11
    }
    function C() {
        return!0
    }
    function k() {
        return!1
    }
    function L(e, t, n) {
        var r = t + "defer", i = t + "queue", s = t + "mark", o = H._data(e, r);
        o && (n === "queue" || !H._data(e, i)) && (n === "mark" || !H._data(e, s)) && setTimeout(function() {
            !H._data(e, i) && !H._data(e, s) && (H.removeData(e, r, !0), o.fire())
        }, 0)
    }
    function A(e) {
        for (var t in e) {
            if (t === "data" && H.isEmptyObject(e[t]))
                continue;
            if (t !== "toJSON")
                return!1
        }
        return!0
    }
    function O(e, n, r) {
        if (r === t && e.nodeType === 1) {
            var i = "data-" + n.replace(I, "-$1").toLowerCase();
            r = e.getAttribute(i);
            if (typeof r == "string") {
                try {
                    r = r === "true" ? !0 : r === "false" ? !1 : r === "null" ? null : H.isNumeric(r) ? +r : F.test(r) ? H.parseJSON(r) : r
                } catch (s) {
                }
                H.data(e, n, r)
            } else
                r = t
        }
        return r
    }
    function M(e) {
        var t = B[e] = {}, n, r;
        e = e.split(/\s+/);
        for (n = 0, r = e.length; n < r; n++)
            t[e[n]] = !0;
        return t
    }
    var _ = e.document, D = e.navigator, P = e.location, H = function() {
        function n() {
            if (!r.isReady) {
                try {
                    _.documentElement.doScroll("left")
                } catch (e) {
                    setTimeout(n, 1);
                    return
                }
                r.ready()
            }
        }
        var r = function(e, t) {
            return new r.fn.init(e, t, o)
        }, i = e.jQuery, s = e.$, o, u = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/, a = /\S/, f = /^\s+/, l = /\s+$/, c = /^<(\w+)\s*\/?>(?:<\/\1>)?$/, h = /^[\],:{}\s]*$/, p = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, d = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, v = /(?:^|:|,)(?:\s*\[)+/g, m = /(webkit)[ \/]([\w.]+)/, g = /(opera)(?:.*version)?[ \/]([\w.]+)/, y = /(msie) ([\w.]+)/, b = /(mozilla)(?:.*? rv:([\w.]+))?/, w = /-([a-z]|[0-9])/ig, E = /^-ms-/, S = function(e, t) {
            return(t + "").toUpperCase()
        }, x = D.userAgent, T, N, C, k = Object.prototype.toString, L = Object.prototype.hasOwnProperty, A = Array.prototype.push, O = Array.prototype.slice, M = String.prototype.trim, P = Array.prototype.indexOf, H = {};
        return r.fn = r.prototype = {constructor: r, init: function(e, n, i) {
                var s, o, a, f;
                if (!e)
                    return this;
                if (e.nodeType)
                    return this.context = this[0] = e, this.length = 1, this;
                if (e === "body" && !n && _.body)
                    return this.context = _, this[0] = _.body, this.selector = e, this.length = 1, this;
                if (typeof e == "string") {
                    e.charAt(0) !== "<" || e.charAt(e.length - 1) !== ">" || e.length < 3 ? s = u.exec(e) : s = [null, e, null];
                    if (s && (s[1] || !n)) {
                        if (s[1])
                            return n = n instanceof r ? n[0] : n, f = n ? n.ownerDocument || n : _, a = c.exec(e), a ? r.isPlainObject(n) ? (e = [_.createElement(a[1])], r.fn.attr.call(e, n, !0)) : e = [f.createElement(a[1])] : (a = r.buildFragment([s[1]], [f]), e = (a.cacheable ? r.clone(a.fragment) : a.fragment).childNodes), r.merge(this, e);
                        o = _.getElementById(s[2]);
                        if (o && o.parentNode) {
                            if (o.id !== s[2])
                                return i.find(e);
                            this.length = 1, this[0] = o
                        }
                        return this.context = _, this.selector = e, this
                    }
                    return!n || n.jquery ? (n || i).find(e) : this.constructor(n).find(e)
                }
                return r.isFunction(e) ? i.ready(e) : (e.selector !== t && (this.selector = e.selector, this.context = e.context), r.makeArray(e, this))
            }, selector: "", jquery: "1.7.2", length: 0, size: function() {
                return this.length
            }, toArray: function() {
                return O.call(this, 0)
            }, get: function(e) {
                return e == null ? this.toArray() : e < 0 ? this[this.length + e] : this[e]
            }, pushStack: function(e, t, n) {
                var i = this.constructor();
                return r.isArray(e) ? A.apply(i, e) : r.merge(i, e), i.prevObject = this, i.context = this.context, t === "find" ? i.selector = this.selector + (this.selector ? " " : "") + n : t && (i.selector = this.selector + "." + t + "(" + n + ")"), i
            }, each: function(e, t) {
                return r.each(this, e, t)
            }, ready: function(e) {
                return r.bindReady(), N.add(e), this
            }, eq: function(e) {
                return e = +e, e === -1 ? this.slice(e) : this.slice(e, e + 1)
            }, first: function() {
                return this.eq(0)
            }, last: function() {
                return this.eq(-1)
            }, slice: function() {
                return this.pushStack(O.apply(this, arguments), "slice", O.call(arguments).join(","))
            }, map: function(e) {
                return this.pushStack(r.map(this, function(t, n) {
                    return e.call(t, n, t)
                }))
            }, end: function() {
                return this.prevObject || this.constructor(null)
            }, push: A, sort: [].sort, splice: [].splice}, r.fn.init.prototype = r.fn, r.extend = r.fn.extend = function() {
            var e, n, i, s, o, u, a = arguments[0] || {}, f = 1, l = arguments.length, c = !1;
            typeof a == "boolean" && (c = a, a = arguments[1] || {}, f = 2), typeof a != "object" && !r.isFunction(a) && (a = {}), l === f && (a = this, --f);
            for (; f < l; f++)
                if ((e = arguments[f]) != null)
                    for (n in e) {
                        i = a[n], s = e[n];
                        if (a === s)
                            continue;
                        c && s && (r.isPlainObject(s) || (o = r.isArray(s))) ? (o ? (o = !1, u = i && r.isArray(i) ? i : []) : u = i && r.isPlainObject(i) ? i : {}, a[n] = r.extend(c, u, s)) : s !== t && (a[n] = s)
                    }
            return a
        }, r.extend({noConflict: function(t) {
                return e.$ === r && (e.$ = s), t && e.jQuery === r && (e.jQuery = i), r
            }, isReady: !1, readyWait: 1, holdReady: function(e) {
                e ? r.readyWait++ : r.ready(!0)
            }, ready: function(e) {
                if (e === !0 && !--r.readyWait || e !== !0 && !r.isReady) {
                    if (!_.body)
                        return setTimeout(r.ready, 1);
                    r.isReady = !0;
                    if (e !== !0 && --r.readyWait > 0)
                        return;
                    N.fireWith(_, [r]), r.fn.trigger && r(_).trigger("ready").off("ready")
                }
            }, bindReady: function() {
                if (!N) {
                    N = r.Callbacks("once memory");
                    if (_.readyState === "complete")
                        return setTimeout(r.ready, 1);
                    if (_.addEventListener)
                        _.addEventListener("DOMContentLoaded", C, !1), e.addEventListener("load", r.ready, !1);
                    else if (_.attachEvent) {
                        _.attachEvent("onreadystatechange", C), e.attachEvent("onload", r.ready);
                        var t = !1;
                        try {
                            t = e.frameElement == null
                        } catch (i) {
                        }
                        _.documentElement.doScroll && t && n()
                    }
                }
            }, isFunction: function(e) {
                return r.type(e) === "function"
            }, isArray: Array.isArray || function(e) {
                return r.type(e) === "array"
            }, isWindow: function(e) {
                return e != null && e == e.window
            }, isNumeric: function(e) {
                return!isNaN(parseFloat(e)) && isFinite(e)
            }, type: function(e) {
                return e == null ? String(e) : H[k.call(e)] || "object"
            }, isPlainObject: function(e) {
                if (!e || r.type(e) !== "object" || e.nodeType || r.isWindow(e))
                    return!1;
                try {
                    if (e.constructor && !L.call(e, "constructor") && !L.call(e.constructor.prototype, "isPrototypeOf"))
                        return!1
                } catch (n) {
                    return!1
                }
                var i;
                for (i in e)
                    ;
                return i === t || L.call(e, i)
            }, isEmptyObject: function(e) {
                for (var t in e)
                    return!1;
                return!0
            }, error: function(e) {
                throw new Error(e)
            }, parseJSON: function(t) {
                if (typeof t != "string" || !t)
                    return null;
                t = r.trim(t);
                if (e.JSON && e.JSON.parse)
                    return e.JSON.parse(t);
                if (h.test(t.replace(p, "@").replace(d, "]").replace(v, "")))
                    return(new Function("return " + t))();
                r.error("Invalid JSON: " + t)
            }, parseXML: function(n) {
                if (typeof n != "string" || !n)
                    return null;
                var i, s;
                try {
                    e.DOMParser ? (s = new DOMParser, i = s.parseFromString(n, "text/xml")) : (i = new ActiveXObject("Microsoft.XMLDOM"), i.async = "false", i.loadXML(n))
                } catch (o) {
                    i = t
                }
                return(!i || !i.documentElement || i.getElementsByTagName("parsererror").length) && r.error("Invalid XML: " + n), i
            }, noop: function() {
            }, globalEval: function(t) {
                t && a.test(t) && (e.execScript || function(t) {
                    e.eval.call(e, t)
                })(t)
            }, camelCase: function(e) {
                return e.replace(E, "ms-").replace(w, S)
            }, nodeName: function(e, t) {
                return e.nodeName && e.nodeName.toUpperCase() === t.toUpperCase()
            }, each: function(e, n, i) {
                var s, o = 0, u = e.length, a = u === t || r.isFunction(e);
                if (i) {
                    if (a) {
                        for (s in e)
                            if (n.apply(e[s], i) === !1)
                                break
                    } else
                        for (; o < u; )
                            if (n.apply(e[o++], i) === !1)
                                break
                } else if (a) {
                    for (s in e)
                        if (n.call(e[s], s, e[s]) === !1)
                            break
                } else
                    for (; o < u; )
                        if (n.call(e[o], o, e[o++]) === !1)
                            break;
                return e
            }, trim: M ? function(e) {
                return e == null ? "" : M.call(e)
            } : function(e) {
                return e == null ? "" : (e + "").replace(f, "").replace(l, "")
            }, makeArray: function(e, t) {
                var n = t || [];
                if (e != null) {
                    var i = r.type(e);
                    e.length == null || i === "string" || i === "function" || i === "regexp" || r.isWindow(e) ? A.call(n, e) : r.merge(n, e)
                }
                return n
            }, inArray: function(e, t, n) {
                var r;
                if (t) {
                    if (P)
                        return P.call(t, e, n);
                    r = t.length, n = n ? n < 0 ? Math.max(0, r + n) : n : 0;
                    for (; n < r; n++)
                        if (n in t && t[n] === e)
                            return n
                }
                return-1
            }, merge: function(e, n) {
                var r = e.length, i = 0;
                if (typeof n.length == "number")
                    for (var s = n.length; i < s; i++)
                        e[r++] = n[i];
                else
                    while (n[i] !== t)
                        e[r++] = n[i++];
                return e.length = r, e
            }, grep: function(e, t, n) {
                var r = [], i;
                n = !!n;
                for (var s = 0, o = e.length; s < o; s++)
                    i = !!t(e[s], s), n !== i && r.push(e[s]);
                return r
            }, map: function(e, n, i) {
                var s, o, u = [], a = 0, f = e.length, l = e instanceof r || f !== t && typeof f == "number" && (f > 0 && e[0] && e[f - 1] || f === 0 || r.isArray(e));
                if (l)
                    for (; a < f; a++)
                        s = n(e[a], a, i), s != null && (u[u.length] = s);
                else
                    for (o in e)
                        s = n(e[o], o, i), s != null && (u[u.length] = s);
                return u.concat.apply([], u)
            }, guid: 1, proxy: function(e, n) {
                if (typeof n == "string") {
                    var i = e[n];
                    n = e, e = i
                }
                if (!r.isFunction(e))
                    return t;
                var s = O.call(arguments, 2), o = function() {
                    return e.apply(n, s.concat(O.call(arguments)))
                };
                return o.guid = e.guid = e.guid || o.guid || r.guid++, o
            }, access: function(e, n, i, s, o, u, a) {
                var f, l = i == null, c = 0, h = e.length;
                if (i && typeof i == "object") {
                    for (c in i)
                        r.access(e, n, c, i[c], 1, u, s);
                    o = 1
                } else if (s !== t) {
                    f = a === t && r.isFunction(s), l && (f ? (f = n, n = function(e, t, n) {
                        return f.call(r(e), n)
                    }) : (n.call(e, s), n = null));
                    if (n)
                        for (; c < h; c++)
                            n(e[c], i, f ? s.call(e[c], c, n(e[c], i)) : s, a);
                    o = 1
                }
                return o ? e : l ? n.call(e) : h ? n(e[0], i) : u
            }, now: function() {
                return(new Date).getTime()
            }, uaMatch: function(e) {
                e = e.toLowerCase();
                var t = m.exec(e) || g.exec(e) || y.exec(e) || e.indexOf("compatible") < 0 && b.exec(e) || [];
                return{browser: t[1] || "", version: t[2] || "0"}
            }, sub: function() {
                function e(t, n) {
                    return new e.fn.init(t, n)
                }
                r.extend(!0, e, this), e.superclass = this, e.fn = e.prototype = this(), e.fn.constructor = e, e.sub = this.sub, e.fn.init = function(n, i) {
                    return i && i instanceof r && !(i instanceof e) && (i = e(i)), r.fn.init.call(this, n, i, t)
                }, e.fn.init.prototype = e.fn;
                var t = e(_);
                return e
            }, browser: {}}), r.each("Boolean Number String Function Array Date RegExp Object".split(" "), function(e, t) {
            H["[object " + t + "]"] = t.toLowerCase()
        }), T = r.uaMatch(x), T.browser && (r.browser[T.browser] = !0, r.browser.version = T.version), r.browser.webkit && (r.browser.safari = !0), a.test(" ") && (f = /^[\s\xA0]+/, l = /[\s\xA0]+$/), o = r(_), _.addEventListener ? C = function() {
            _.removeEventListener("DOMContentLoaded", C, !1), r.ready()
        } : _.attachEvent && (C = function() {
            _.readyState === "complete" && (_.detachEvent("onreadystatechange", C), r.ready())
        }), r
    }(), B = {};
    H.Callbacks = function(e) {
        e = e ? B[e] || M(e) : {};
        var n = [], r = [], i, s, o, u, a, f, l = function(t) {
            var r, i, s, o, u;
            for (r = 0, i = t.length; r < i; r++)
                s = t[r], o = H.type(s), o === "array" ? l(s) : o === "function" && (!e.unique || !h.has(s)) && n.push(s)
        }, c = function(t, l) {
            l = l || [], i = !e.memory || [t, l], s = !0, o = !0, f = u || 0, u = 0, a = n.length;
            for (; n && f < a; f++)
                if (n[f].apply(t, l) === !1 && e.stopOnFalse) {
                    i = !0;
                    break
                }
            o = !1, n && (e.once ? i === !0 ? h.disable() : n = [] : r && r.length && (i = r.shift(), h.fireWith(i[0], i[1])))
        }, h = {add: function() {
                if (n) {
                    var e = n.length;
                    l(arguments), o ? a = n.length : i && i !== !0 && (u = e, c(i[0], i[1]))
                }
                return this
            }, remove: function() {
                if (n) {
                    var t = arguments, r = 0, i = t.length;
                    for (; r < i; r++)
                        for (var s = 0; s < n.length; s++)
                            if (t[r] === n[s]) {
                                o && s <= a && (a--, s <= f && f--), n.splice(s--, 1);
                                if (e.unique)
                                    break
                            }
                }
                return this
            }, has: function(e) {
                if (n) {
                    var t = 0, r = n.length;
                    for (; t < r; t++)
                        if (e === n[t])
                            return!0
                }
                return!1
            }, empty: function() {
                return n = [], this
            }, disable: function() {
                return n = r = i = t, this
            }, disabled: function() {
                return!n
            }, lock: function() {
                return r = t, (!i || i === !0) && h.disable(), this
            }, locked: function() {
                return!r
            }, fireWith: function(t, n) {
                return r && (o ? e.once || r.push([t, n]) : (!e.once || !i) && c(t, n)), this
            }, fire: function() {
                return h.fireWith(this, arguments), this
            }, fired: function() {
                return!!s
            }};
        return h
    };
    var j = [].slice;
    H.extend({Deferred: function(e) {
            var t = H.Callbacks("once memory"), n = H.Callbacks("once memory"), r = H.Callbacks("memory"), i = "pending", s = {resolve: t, reject: n, notify: r}, o = {done: t.add, fail: n.add, progress: r.add, state: function() {
                    return i
                }, isResolved: t.fired, isRejected: n.fired, then: function(e, t, n) {
                    return u.done(e).fail(t).progress(n), this
                }, always: function() {
                    return u.done.apply(u, arguments).fail.apply(u, arguments), this
                }, pipe: function(e, t, n) {
                    return H.Deferred(function(r) {
                        H.each({done: [e, "resolve"], fail: [t, "reject"], progress: [n, "notify"]}, function(e, t) {
                            var n = t[0], i = t[1], s;
                            H.isFunction(n) ? u[e](function() {
                                s = n.apply(this, arguments), s && H.isFunction(s.promise) ? s.promise().then(r.resolve, r.reject, r.notify) : r[i + "With"](this === u ? r : this, [s])
                            }) : u[e](r[i])
                        })
                    }).promise()
                }, promise: function(e) {
                    if (e == null)
                        e = o;
                    else
                        for (var t in o)
                            e[t] = o[t];
                    return e
                }}, u = o.promise({}), a;
            for (a in s)
                u[a] = s[a].fire, u[a + "With"] = s[a].fireWith;
            return u.done(function() {
                i = "resolved"
            }, n.disable, r.lock).fail(function() {
                i = "rejected"
            }, t.disable, r.lock), e && e.call(u, u), u
        }, when: function(e) {
            function t(e) {
                return function(t) {
                    o[e] = arguments.length > 1 ? j.call(arguments, 0) : t, f.notifyWith(l, o)
                }
            }
            function n(e) {
                return function(t) {
                    r[e] = arguments.length > 1 ? j.call(arguments, 0) : t, --u || f.resolveWith(f, r)
                }
            }
            var r = j.call(arguments, 0), i = 0, s = r.length, o = Array(s), u = s, a = s, f = s <= 1 && e && H.isFunction(e.promise) ? e : H.Deferred(), l = f.promise();
            if (s > 1) {
                for (; i < s; i++)
                    r[i] && r[i].promise && H.isFunction(r[i].promise) ? r[i].promise().then(n(i), f.reject, t(i)) : --u;
                u || f.resolveWith(f, r)
            } else
                f !== e && f.resolveWith(f, s ? [e] : []);
            return l
        }}), H.support = function() {
        var t, n, r, i, s, o, u, a, f, l, c, h, p = _.createElement("div"), d = _.documentElement;
        p.setAttribute("className", "t"), p.innerHTML = "   <link/><table></table><a href='/a' style='top:1px;float:left;opacity:.55;'>a</a><input type='checkbox'/>", n = p.getElementsByTagName("*"), r = p.getElementsByTagName("a")[0];
        if (!n || !n.length || !r)
            return{};
        i = _.createElement("select"), s = i.appendChild(_.createElement("option")), o = p.getElementsByTagName("input")[0], t = {leadingWhitespace: p.firstChild.nodeType === 3, tbody: !p.getElementsByTagName("tbody").length, htmlSerialize: !!p.getElementsByTagName("link").length, style: /top/.test(r.getAttribute("style")), hrefNormalized: r.getAttribute("href") === "/a", opacity: /^0.55/.test(r.style.opacity), cssFloat: !!r.style.cssFloat, checkOn: o.value === "on", optSelected: s.selected, getSetAttribute: p.className !== "t", enctype: !!_.createElement("form").enctype, html5Clone: _.createElement("nav").cloneNode(!0).outerHTML !== "<:nav></:nav>", submitBubbles: !0, changeBubbles: !0, focusinBubbles: !1, deleteExpando: !0, noCloneEvent: !0, inlineBlockNeedsLayout: !1, shrinkWrapBlocks: !1, reliableMarginRight: !0, pixelMargin: !0}, H.boxModel = t.boxModel = _.compatMode === "CSS1Compat", o.checked = !0, t.noCloneChecked = o.cloneNode(!0).checked, i.disabled = !0, t.optDisabled = !s.disabled;
        try {
            delete p.test
        } catch (v) {
            t.deleteExpando = !1
        }
        !p.addEventListener && p.attachEvent && p.fireEvent && (p.attachEvent("onclick", function() {
            t.noCloneEvent = !1
        }), p.cloneNode(!0).fireEvent("onclick")), o = _.createElement("input"), o.value = "t", o.setAttribute("type", "radio"), t.radioValue = o.value === "t", o.setAttribute("checked", "checked"), o.setAttribute("name", "t"), p.appendChild(o), u = _.createDocumentFragment(), u.appendChild(p.lastChild), t.checkClone = u.cloneNode(!0).cloneNode(!0).lastChild.checked, t.appendChecked = o.checked, u.removeChild(o), u.appendChild(p);
        if (p.attachEvent)
            for (c in{submit:1, change:1, focusin:1})
                l = "on" + c, h = l in p, h || (p.setAttribute(l, "return;"), h = typeof p[l] == "function"), t[c + "Bubbles"] = h;
        return u.removeChild(p), u = i = s = p = o = null, H(function() {
            var n, r, i, s, o, u, f, l, c, d, v, m, g, y = _.getElementsByTagName("body")[0];
            !y || (l = 1, g = "padding:0;margin:0;border:", v = "position:absolute;top:0;left:0;width:1px;height:1px;", m = g + "0;visibility:hidden;", c = "style='" + v + g + "5px solid #000;", d = "<div " + c + "display:block;'><div style='" + g + "0;display:block;overflow:hidden;'></div></div>" + "<table " + c + "' cellpadding='0' cellspacing='0'>" + "<tr><td></td></tr></table>", n = _.createElement("div"), n.style.cssText = m + "width:0;height:0;position:static;top:0;margin-top:" + l + "px", y.insertBefore(n, y.firstChild), p = _.createElement("div"), n.appendChild(p), p.innerHTML = "<table><tr><td style='" + g + "0;display:none'></td><td>t</td></tr></table>", a = p.getElementsByTagName("td"), h = a[0].offsetHeight === 0, a[0].style.display = "", a[1].style.display = "none", t.reliableHiddenOffsets = h && a[0].offsetHeight === 0, e.getComputedStyle && (p.innerHTML = "", f = _.createElement("div"), f.style.width = "0", f.style.marginRight = "0", p.style.width = "2px", p.appendChild(f), t.reliableMarginRight = (parseInt((e.getComputedStyle(f, null) || {marginRight: 0}).marginRight, 10) || 0) === 0), typeof p.style.zoom != "undefined" && (p.innerHTML = "", p.style.width = p.style.padding = "1px", p.style.border = 0, p.style.overflow = "hidden", p.style.display = "inline", p.style.zoom = 1, t.inlineBlockNeedsLayout = p.offsetWidth === 3, p.style.display = "block", p.style.overflow = "visible", p.innerHTML = "<div style='width:5px;'></div>", t.shrinkWrapBlocks = p.offsetWidth !== 3), p.style.cssText = v + m, p.innerHTML = d, r = p.firstChild, i = r.firstChild, o = r.nextSibling.firstChild.firstChild, u = {doesNotAddBorder: i.offsetTop !== 5, doesAddBorderForTableAndCells: o.offsetTop === 5}, i.style.position = "fixed", i.style.top = "20px", u.fixedPosition = i.offsetTop === 20 || i.offsetTop === 15, i.style.position = i.style.top = "", r.style.overflow = "hidden", r.style.position = "relative", u.subtractsBorderForOverflowNotVisible = i.offsetTop === -5, u.doesNotIncludeMarginInBodyOffset = y.offsetTop !== l, e.getComputedStyle && (p.style.marginTop = "1%", t.pixelMargin = (e.getComputedStyle(p, null) || {marginTop: 0}).marginTop !== "1%"), typeof n.style.zoom != "undefined" && (n.style.zoom = 1), y.removeChild(n), f = p = n = null, H.extend(t, u))
        }), t
    }();
    var F = /^(?:\{.*\}|\[.*\])$/, I = /([A-Z])/g;
    H.extend({cache: {}, uuid: 0, expando: "jQuery" + (H.fn.jquery + Math.random()).replace(/\D/g, ""), noData: {embed: !0, object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000", applet: !0}, hasData: function(e) {
            return e = e.nodeType ? H.cache[e[H.expando]] : e[H.expando], !!e && !A(e)
        }, data: function(e, n, r, i) {
            if (!!H.acceptData(e)) {
                var s, o, u, a = H.expando, f = typeof n == "string", l = e.nodeType, c = l ? H.cache : e, h = l ? e[a] : e[a] && a, p = n === "events";
                if ((!h || !c[h] || !p && !i && !c[h].data) && f && r === t)
                    return;
                h || (l ? e[a] = h = ++H.uuid : h = a), c[h] || (c[h] = {}, l || (c[h].toJSON = H.noop));
                if (typeof n == "object" || typeof n == "function")
                    i ? c[h] = H.extend(c[h], n) : c[h].data = H.extend(c[h].data, n);
                return s = o = c[h], i || (o.data || (o.data = {}), o = o.data), r !== t && (o[H.camelCase(n)] = r), p && !o[n] ? s.events : (f ? (u = o[n], u == null && (u = o[H.camelCase(n)])) : u = o, u)
            }
        }, removeData: function(e, t, n) {
            if (!!H.acceptData(e)) {
                var r, i, s, o = H.expando, u = e.nodeType, a = u ? H.cache : e, f = u ? e[o] : o;
                if (!a[f])
                    return;
                if (t) {
                    r = n ? a[f] : a[f].data;
                    if (r) {
                        H.isArray(t) || (t in r ? t = [t] : (t = H.camelCase(t), t in r ? t = [t] : t = t.split(" ")));
                        for (i = 0, s = t.length; i < s; i++)
                            delete r[t[i]];
                        if (!(n ? A : H.isEmptyObject)(r))
                            return
                    }
                }
                if (!n) {
                    delete a[f].data;
                    if (!A(a[f]))
                        return
                }
                H.support.deleteExpando || !a.setInterval ? delete a[f] : a[f] = null, u && (H.support.deleteExpando ? delete e[o] : e.removeAttribute ? e.removeAttribute(o) : e[o] = null)
            }
        }, _data: function(e, t, n) {
            return H.data(e, t, n, !0)
        }, acceptData: function(e) {
            if (e.nodeName) {
                var t = H.noData[e.nodeName.toLowerCase()];
                if (t)
                    return t !== !0 && e.getAttribute("classid") === t
            }
            return!0
        }}), H.fn.extend({data: function(e, n) {
            var r, i, s, o, u, a = this[0], f = 0, l = null;
            if (e === t) {
                if (this.length) {
                    l = H.data(a);
                    if (a.nodeType === 1 && !H._data(a, "parsedAttrs")) {
                        s = a.attributes;
                        for (u = s.length; f < u; f++)
                            o = s[f].name, o.indexOf("data-") === 0 && (o = H.camelCase(o.substring(5)), O(a, o, l[o]));
                        H._data(a, "parsedAttrs", !0)
                    }
                }
                return l
            }
            return typeof e == "object" ? this.each(function() {
                H.data(this, e)
            }) : (r = e.split(".", 2), r[1] = r[1] ? "." + r[1] : "", i = r[1] + "!", H.access(this, function(n) {
                if (n === t)
                    return l = this.triggerHandler("getData" + i, [r[0]]), l === t && a && (l = H.data(a, e), l = O(a, e, l)), l === t && r[1] ? this.data(r[0]) : l;
                r[1] = n, this.each(function() {
                    var t = H(this);
                    t.triggerHandler("setData" + i, r), H.data(this, e, n), t.triggerHandler("changeData" + i, r)
                })
            }, null, n, arguments.length > 1, null, !1))
        }, removeData: function(e) {
            return this.each(function() {
                H.removeData(this, e)
            })
        }}), H.extend({_mark: function(e, t) {
            e && (t = (t || "fx") + "mark", H._data(e, t, (H._data(e, t) || 0) + 1))
        }, _unmark: function(e, t, n) {
            e !== !0 && (n = t, t = e, e = !1);
            if (t) {
                n = n || "fx";
                var r = n + "mark", i = e ? 0 : (H._data(t, r) || 1) - 1;
                i ? H._data(t, r, i) : (H.removeData(t, r, !0), L(t, n, "mark"))
            }
        }, queue: function(e, t, n) {
            var r;
            if (e)
                return t = (t || "fx") + "queue", r = H._data(e, t), n && (!r || H.isArray(n) ? r = H._data(e, t, H.makeArray(n)) : r.push(n)), r || []
        }, dequeue: function(e, t) {
            t = t || "fx";
            var n = H.queue(e, t), r = n.shift(), i = {};
            r === "inprogress" && (r = n.shift()), r && (t === "fx" && n.unshift("inprogress"), H._data(e, t + ".run", i), r.call(e, function() {
                H.dequeue(e, t)
            }, i)), n.length || (H.removeData(e, t + "queue " + t + ".run", !0), L(e, t, "queue"))
        }}), H.fn.extend({queue: function(e, n) {
            var r = 2;
            return typeof e != "string" && (n = e, e = "fx", r--), arguments.length < r ? H.queue(this[0], e) : n === t ? this : this.each(function() {
                var t = H.queue(this, e, n);
                e === "fx" && t[0] !== "inprogress" && H.dequeue(this, e)
            })
        }, dequeue: function(e) {
            return this.each(function() {
                H.dequeue(this, e)
            })
        }, delay: function(e, t) {
            return e = H.fx ? H.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function(t, n) {
                var r = setTimeout(t, e);
                n.stop = function() {
                    clearTimeout(r)
                }
            })
        }, clearQueue: function(e) {
            return this.queue(e || "fx", [])
        }, promise: function(e, n) {
            function r() {
                --u || i.resolveWith(s, [s])
            }
            typeof e != "string" && (n = e, e = t), e = e || "fx";
            var i = H.Deferred(), s = this, o = s.length, u = 1, a = e + "defer", f = e + "queue", l = e + "mark", c;
            while (o--)
                if (c = H.data(s[o], a, t, !0) || (H.data(s[o], f, t, !0) || H.data(s[o], l, t, !0)) && H.data(s[o], a, H.Callbacks("once memory"), !0))
                    u++, c.add(r);
            return r(), i.promise(n)
        }});
    var q = /[\n\t\r]/g, R = /\s+/, U = /\r/g, z = /^(?:button|input)$/i, W = /^(?:button|input|object|select|textarea)$/i, X = /^a(?:rea)?$/i, V = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i, $ = H.support.getSetAttribute, J, K, Q;
    H.fn.extend({attr: function(e, t) {
            return H.access(this, H.attr, e, t, arguments.length > 1)
        }, removeAttr: function(e) {
            return this.each(function() {
                H.removeAttr(this, e)
            })
        }, prop: function(e, t) {
            return H.access(this, H.prop, e, t, arguments.length > 1)
        }, removeProp: function(e) {
            return e = H.propFix[e] || e, this.each(function() {
                try {
                    this[e] = t, delete this[e]
                } catch (n) {
                }
            })
        }, addClass: function(e) {
            var t, n, r, i, s, o, u;
            if (H.isFunction(e))
                return this.each(function(t) {
                    H(this).addClass(e.call(this, t, this.className))
                });
            if (e && typeof e == "string") {
                t = e.split(R);
                for (n = 0, r = this.length; n < r; n++) {
                    i = this[n];
                    if (i.nodeType === 1)
                        if (!i.className && t.length === 1)
                            i.className = e;
                        else {
                            s = " " + i.className + " ";
                            for (o = 0, u = t.length; o < u; o++)
                                ~s.indexOf(" " + t[o] + " ") || (s += t[o] + " ");
                            i.className = H.trim(s)
                        }
                }
            }
            return this
        }, removeClass: function(e) {
            var n, r, i, s, o, u, a;
            if (H.isFunction(e))
                return this.each(function(t) {
                    H(this).removeClass(e.call(this, t, this.className))
                });
            if (e && typeof e == "string" || e === t) {
                n = (e || "").split(R);
                for (r = 0, i = this.length; r < i; r++) {
                    s = this[r];
                    if (s.nodeType === 1 && s.className)
                        if (e) {
                            o = (" " + s.className + " ").replace(q, " ");
                            for (u = 0, a = n.length; u < a; u++)
                                o = o.replace(" " + n[u] + " ", " ");
                            s.className = H.trim(o)
                        } else
                            s.className = ""
                }
            }
            return this
        }, toggleClass: function(e, t) {
            var n = typeof e, r = typeof t == "boolean";
            return H.isFunction(e) ? this.each(function(n) {
                H(this).toggleClass(e.call(this, n, this.className, t), t)
            }) : this.each(function() {
                if (n === "string") {
                    var i, s = 0, o = H(this), u = t, a = e.split(R);
                    while (i = a[s++])
                        u = r ? u : !o.hasClass(i), o[u ? "addClass" : "removeClass"](i)
                } else if (n === "undefined" || n === "boolean")
                    this.className && H._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : H._data(this, "__className__") || ""
            })
        }, hasClass: function(e) {
            var t = " " + e + " ", n = 0, r = this.length;
            for (; n < r; n++)
                if (this[n].nodeType === 1 && (" " + this[n].className + " ").replace(q, " ").indexOf(t) > -1)
                    return!0;
            return!1
        }, val: function(e) {
            var n, r, i, s = this[0];
            if (!!arguments.length)
                return i = H.isFunction(e), this.each(function(r) {
                    var s = H(this), o;
                    if (this.nodeType === 1) {
                        i ? o = e.call(this, r, s.val()) : o = e, o == null ? o = "" : typeof o == "number" ? o += "" : H.isArray(o) && (o = H.map(o, function(e) {
                            return e == null ? "" : e + ""
                        })), n = H.valHooks[this.type] || H.valHooks[this.nodeName.toLowerCase()];
                        if (!n || !("set"in n) || n.set(this, o, "value") === t)
                            this.value = o
                    }
                });
            if (s)
                return n = H.valHooks[s.type] || H.valHooks[s.nodeName.toLowerCase()], n && "get"in n && (r = n.get(s, "value")) !== t ? r : (r = s.value, typeof r == "string" ? r.replace(U, "") : r == null ? "" : r)
        }}), H.extend({valHooks: {option: {get: function(e) {
                    var t = e.attributes.value;
                    return!t || t.specified ? e.value : e.text
                }}, select: {get: function(e) {
                    var t, n, r, i, s = e.selectedIndex, o = [], u = e.options, a = e.type === "select-one";
                    if (s < 0)
                        return null;
                    n = a ? s : 0, r = a ? s + 1 : u.length;
                    for (; n < r; n++) {
                        i = u[n];
                        if (i.selected && (H.support.optDisabled ? !i.disabled : i.getAttribute("disabled") === null) && (!i.parentNode.disabled || !H.nodeName(i.parentNode, "optgroup"))) {
                            t = H(i).val();
                            if (a)
                                return t;
                            o.push(t)
                        }
                    }
                    return a && !o.length && u.length ? H(u[s]).val() : o
                }, set: function(e, t) {
                    var n = H.makeArray(t);
                    return H(e).find("option").each(function() {
                        this.selected = H.inArray(H(this).val(), n) >= 0
                    }), n.length || (e.selectedIndex = -1), n
                }}}, attrFn: {val: !0, css: !0, html: !0, text: !0, data: !0, width: !0, height: !0, offset: !0}, attr: function(e, n, r, i) {
            var s, o, u, a = e.nodeType;
            if (!!e && a !== 3 && a !== 8 && a !== 2) {
                if (i && n in H.attrFn)
                    return H(e)[n](r);
                if (typeof e.getAttribute == "undefined")
                    return H.prop(e, n, r);
                u = a !== 1 || !H.isXMLDoc(e), u && (n = n.toLowerCase(), o = H.attrHooks[n] || (V.test(n) ? K : J));
                if (r !== t) {
                    if (r === null) {
                        H.removeAttr(e, n);
                        return
                    }
                    return o && "set"in o && u && (s = o.set(e, r, n)) !== t ? s : (e.setAttribute(n, "" + r), r)
                }
                return o && "get"in o && u && (s = o.get(e, n)) !== null ? s : (s = e.getAttribute(n), s === null ? t : s)
            }
        }, removeAttr: function(e, t) {
            var n, r, i, s, o, u = 0;
            if (t && e.nodeType === 1) {
                r = t.toLowerCase().split(R), s = r.length;
                for (; u < s; u++)
                    i = r[u], i && (n = H.propFix[i] || i, o = V.test(i), o || H.attr(e, i, ""), e.removeAttribute($ ? i : n), o && n in e && (e[n] = !1))
            }
        }, attrHooks: {type: {set: function(e, t) {
                    if (z.test(e.nodeName) && e.parentNode)
                        H.error("type property can't be changed");
                    else if (!H.support.radioValue && t === "radio" && H.nodeName(e, "input")) {
                        var n = e.value;
                        return e.setAttribute("type", t), n && (e.value = n), t
                    }
                }}, value: {get: function(e, t) {
                    return J && H.nodeName(e, "button") ? J.get(e, t) : t in e ? e.value : null
                }, set: function(e, t, n) {
                    if (J && H.nodeName(e, "button"))
                        return J.set(e, t, n);
                    e.value = t
                }}}, propFix: {tabindex: "tabIndex", readonly: "readOnly", "for": "htmlFor", "class": "className", maxlength: "maxLength", cellspacing: "cellSpacing", cellpadding: "cellPadding", rowspan: "rowSpan", colspan: "colSpan", usemap: "useMap", frameborder: "frameBorder", contenteditable: "contentEditable"}, prop: function(e, n, r) {
            var i, s, o, u = e.nodeType;
            if (!!e && u !== 3 && u !== 8 && u !== 2)
                return o = u !== 1 || !H.isXMLDoc(e), o && (n = H.propFix[n] || n, s = H.propHooks[n]), r !== t ? s && "set"in s && (i = s.set(e, r, n)) !== t ? i : e[n] = r : s && "get"in s && (i = s.get(e, n)) !== null ? i : e[n]
        }, propHooks: {tabIndex: {get: function(e) {
                    var n = e.getAttributeNode("tabindex");
                    return n && n.specified ? parseInt(n.value, 10) : W.test(e.nodeName) || X.test(e.nodeName) && e.href ? 0 : t
                }}}}), H.attrHooks.tabindex = H.propHooks.tabIndex, K = {get: function(e, n) {
            var r, i = H.prop(e, n);
            return i === !0 || typeof i != "boolean" && (r = e.getAttributeNode(n)) && r.nodeValue !== !1 ? n.toLowerCase() : t
        }, set: function(e, t, n) {
            var r;
            return t === !1 ? H.removeAttr(e, n) : (r = H.propFix[n] || n, r in e && (e[r] = !0), e.setAttribute(n, n.toLowerCase())), n
        }}, $ || (Q = {name: !0, id: !0, coords: !0}, J = H.valHooks.button = {get: function(e, n) {
            var r;
            return r = e.getAttributeNode(n), r && (Q[n] ? r.nodeValue !== "" : r.specified) ? r.nodeValue : t
        }, set: function(e, t, n) {
            var r = e.getAttributeNode(n);
            return r || (r = _.createAttribute(n), e.setAttributeNode(r)), r.nodeValue = t + ""
        }}, H.attrHooks.tabindex.set = J.set, H.each(["width", "height"], function(e, t) {
        H.attrHooks[t] = H.extend(H.attrHooks[t], {set: function(e, n) {
                if (n === "")
                    return e.setAttribute(t, "auto"), n
            }})
    }), H.attrHooks.contenteditable = {get: J.get, set: function(e, t, n) {
            t === "" && (t = "false"), J.set(e, t, n)
        }}), H.support.hrefNormalized || H.each(["href", "src", "width", "height"], function(e, n) {
        H.attrHooks[n] = H.extend(H.attrHooks[n], {get: function(e) {
                var r = e.getAttribute(n, 2);
                return r === null ? t : r
            }})
    }), H.support.style || (H.attrHooks.style = {get: function(e) {
            return e.style.cssText.toLowerCase() || t
        }, set: function(e, t) {
            return e.style.cssText = "" + t
        }}), H.support.optSelected || (H.propHooks.selected = H.extend(H.propHooks.selected, {get: function(e) {
            var t = e.parentNode;
            return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null
        }})), H.support.enctype || (H.propFix.enctype = "encoding"), H.support.checkOn || H.each(["radio", "checkbox"], function() {
        H.valHooks[this] = {get: function(e) {
                return e.getAttribute("value") === null ? "on" : e.value
            }}
    }), H.each(["radio", "checkbox"], function() {
        H.valHooks[this] = H.extend(H.valHooks[this], {set: function(e, t) {
                if (H.isArray(t))
                    return e.checked = H.inArray(H(e).val(), t) >= 0
            }})
    });
    var G = /^(?:textarea|input|select)$/i, Y = /^([^\.]*)?(?:\.(.+))?$/, Z = /(?:^|\s)hover(\.\S+)?\b/, et = /^key/, tt = /^(?:mouse|contextmenu)|click/, nt = /^(?:focusinfocus|focusoutblur)$/, rt = /^(\w*)(?:#([\w\-]+))?(?:\.([\w\-]+))?$/, it = function(
            e) {
        var t = rt.exec(e);
        return t && (t[1] = (t[1] || "").toLowerCase(), t[3] = t[3] && new RegExp("(?:^|\\s)" + t[3] + "(?:\\s|$)")), t
    }, st = function(e, t) {
        var n = e.attributes || {};
        return(!t[1] || e.nodeName.toLowerCase() === t[1]) && (!t[2] || (n.id || {}).value === t[2]) && (!t[3] || t[3].test((n["class"] || {}).value))
    }, ot = function(e) {
        return H.event.special.hover ? e : e.replace(Z, "mouseenter$1 mouseleave$1")
    };
    H.event = {add: function(e, n, r, i, s) {
            var o, u, a, f, l, c, h, p, d, v, m, g;
            if (!(e.nodeType === 3 || e.nodeType === 8 || !n || !r || !(o = H._data(e)))) {
                r.handler && (d = r, r = d.handler, s = d.selector), r.guid || (r.guid = H.guid++), a = o.events, a || (o.events = a = {}), u = o.handle, u || (o.handle = u = function(e) {
                    return typeof H == "undefined" || !!e && H.event.triggered === e.type ? t : H.event.dispatch.apply(u.elem, arguments)
                }, u.elem = e), n = H.trim(ot(n)).split(" ");
                for (f = 0; f < n.length; f++) {
                    l = Y.exec(n[f]) || [], c = l[1], h = (l[2] || "").split(".").sort(), g = H.event.special[c] || {}, c = (s ? g.delegateType : g.bindType) || c, g = H.event.special[c] || {}, p = H.extend({type: c, origType: l[1], data: i, handler: r, guid: r.guid, selector: s, quick: s && it(s), namespace: h.join(".")}, d), m = a[c];
                    if (!m) {
                        m = a[c] = [], m.delegateCount = 0;
                        if (!g.setup || g.setup.call(e, i, h, u) === !1)
                            e.addEventListener ? e.addEventListener(c, u, !1) : e.attachEvent && e.attachEvent("on" + c, u)
                    }
                    g.add && (g.add.call(e, p), p.handler.guid || (p.handler.guid = r.guid)), s ? m.splice(m.delegateCount++, 0, p) : m.push(p), H.event.global[c] = !0
                }
                e = null
            }
        }, global: {}, remove: function(e, t, n, r, i) {
            var s = H.hasData(e) && H._data(e), o, u, a, f, l, c, h, p, d, v, m, g;
            if (!!s && !!(p = s.events)) {
                t = H.trim(ot(t || "")).split(" ");
                for (o = 0; o < t.length; o++) {
                    u = Y.exec(t[o]) || [], a = f = u[1], l = u[2];
                    if (!a) {
                        for (a in p)
                            H.event.remove(e, a + t[o], n, r, !0);
                        continue
                    }
                    d = H.event.special[a] || {}, a = (r ? d.delegateType : d.bindType) || a, m = p[a] || [], c = m.length, l = l ? new RegExp("(^|\\.)" + l.split(".").sort().join("\\.(?:.*\\.)?") + "(\\.|$)") : null;
                    for (h = 0; h < m.length; h++)
                        g = m[h], (i || f === g.origType) && (!n || n.guid === g.guid) && (!l || l.test(g.namespace)) && (!r || r === g.selector || r === "**" && g.selector) && (m.splice(h--, 1), g.selector && m.delegateCount--, d.remove && d.remove.call(e, g));
                    m.length === 0 && c !== m.length && ((!d.teardown || d.teardown.call(e, l) === !1) && H.removeEvent(e, a, s.handle), delete p[a])
                }
                H.isEmptyObject(p) && (v = s.handle, v && (v.elem = null), H.removeData(e, ["events", "handle"], !0))
            }
        }, customEvent: {getData: !0, setData: !0, changeData: !0}, trigger: function(n, r, i, s) {
            if (!i || i.nodeType !== 3 && i.nodeType !== 8) {
                var o = n.type || n, u = [], a, f, l, c, h, p, d, v, m, g;
                if (nt.test(o + H.event.triggered))
                    return;
                o.indexOf("!") >= 0 && (o = o.slice(0, -1), f = !0), o.indexOf(".") >= 0 && (u = o.split("."), o = u.shift(), u.sort());
                if ((!i || H.event.customEvent[o]) && !H.event.global[o])
                    return;
                n = typeof n == "object" ? n[H.expando] ? n : new H.Event(o, n) : new H.Event(o), n.type = o, n.isTrigger = !0, n.exclusive = f, n.namespace = u.join("."), n.namespace_re = n.namespace ? new RegExp("(^|\\.)" + u.join("\\.(?:.*\\.)?") + "(\\.|$)") : null, p = o.indexOf(":") < 0 ? "on" + o : "";
                if (!i) {
                    a = H.cache;
                    for (l in a)
                        a[l].events && a[l].events[o] && H.event.trigger(n, r, a[l].handle.elem, !0);
                    return
                }
                n.result = t, n.target || (n.target = i), r = r != null ? H.makeArray(r) : [], r.unshift(n), d = H.event.special[o] || {};
                if (d.trigger && d.trigger.apply(i, r) === !1)
                    return;
                m = [[i, d.bindType || o]];
                if (!s && !d.noBubble && !H.isWindow(i)) {
                    g = d.delegateType || o, c = nt.test(g + o) ? i : i.parentNode, h = null;
                    for (; c; c = c.parentNode)
                        m.push([c, g]), h = c;
                    h && h === i.ownerDocument && m.push([h.defaultView || h.parentWindow || e, g])
                }
                for (l = 0; l < m.length && !n.isPropagationStopped(); l++)
                    c = m[l][0], n.type = m[l][1], v = (H._data(c, "events") || {})[n.type] && H._data(c, "handle"), v && v.apply(c, r), v = p && c[p], v && H.acceptData(c) && v.apply(c, r) === !1 && n.preventDefault();
                return n.type = o, !s && !n.isDefaultPrevented() && (!d._default || d._default.apply(i.ownerDocument, r) === !1) && (o !== "click" || !H.nodeName(i, "a")) && H.acceptData(i) && p && i[o] && (o !== "focus" && o !== "blur" || n.target.offsetWidth !== 0) && !H.isWindow(i) && (h = i[p], h && (i[p] = null), H.event.triggered = o, i[o](), H.event.triggered = t, h && (i[p] = h)), n.result
            }
        }, dispatch: function(n) {
            n = H.event.fix(n || e.event);
            var r = (H._data(this, "events") || {})[n.type] || [], i = r.delegateCount, s = [].slice.call(arguments, 0), o = !n.exclusive && !n.namespace, u = H.event.special[n.type] || {}, a = [], f, l, c, h, p, d, v, m, g, y, b;
            s[0] = n, n.delegateTarget = this;
            if (!u.preDispatch || u.preDispatch.call(this, n) !== !1) {
                if (i && (!n.button || n.type !== "click")) {
                    h = H(this), h.context = this.ownerDocument || this;
                    for (c = n.target; c != this; c = c.parentNode || this)
                        if (c.disabled !== !0) {
                            d = {}, m = [], h[0] = c;
                            for (f = 0; f < i; f++)
                                g = r[f], y = g.selector, d[y] === t && (d[y] = g.quick ? st(c, g.quick) : h.is(y)), d[y] && m.push(g);
                            m.length && a.push({elem: c, matches: m})
                        }
                }
                r.length > i && a.push({elem: this, matches: r.slice(i)});
                for (f = 0; f < a.length && !n.isPropagationStopped(); f++) {
                    v = a[f], n.currentTarget = v.elem;
                    for (l = 0; l < v.matches.length && !n.isImmediatePropagationStopped(); l++) {
                        g = v.matches[l];
                        if (o || !n.namespace && !g.namespace || n.namespace_re && n.namespace_re.test(g.namespace))
                            n.data = g.data, n.handleObj = g, p = ((H.event.special[g.origType] || {}).handle || g.handler).apply(v.elem, s), p !== t && (n.result = p, p === !1 && (n.preventDefault(), n.stopPropagation()))
                    }
                }
                return u.postDispatch && u.postDispatch.call(this, n), n.result
            }
        }, props: "attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "), fixHooks: {}, keyHooks: {props: "char charCode key keyCode".split(" "), filter: function(e, t) {
                return e.which == null && (e.which = t.charCode != null ? t.charCode : t.keyCode), e
            }}, mouseHooks: {props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "), filter: function(e, n) {
                var r, i, s, o = n.button, u = n.fromElement;
                return e.pageX == null && n.clientX != null && (r = e.target.ownerDocument || _, i = r.documentElement, s = r.body, e.pageX = n.clientX + (i && i.scrollLeft || s && s.scrollLeft || 0) - (i && i.clientLeft || s && s.clientLeft || 0), e.pageY = n.clientY + (i && i.scrollTop || s && s.scrollTop || 0) - (i && i.clientTop || s && s.clientTop || 0)), !e.relatedTarget && u && (e.relatedTarget = u === e.target ? n.toElement : u), !e.which && o !== t && (e.which = o & 1 ? 1 : o & 2 ? 3 : o & 4 ? 2 : 0), e
            }}, fix: function(e) {
            if (e[H.expando])
                return e;
            var n, r, i = e, s = H.event.fixHooks[e.type] || {}, o = s.props ? this.props.concat(s.props) : this.props;
            e = H.Event(i);
            for (n = o.length; n; )
                r = o[--n], e[r] = i[r];
            return e.target || (e.target = i.srcElement || _), e.target.nodeType === 3 && (e.target = e.target.parentNode), e.metaKey === t && (e.metaKey = e.ctrlKey), s.filter ? s.filter(e, i) : e
        }, special: {ready: {setup: H.bindReady}, load: {noBubble: !0}, focus: {delegateType: "focusin"}, blur: {delegateType: "focusout"}, beforeunload: {setup: function(e, t, n) {
                    H.isWindow(this) && (this.onbeforeunload = n)
                }, teardown: function(e, t) {
                    this.onbeforeunload === t && (this.onbeforeunload = null)
                }}}, simulate: function(e, t, n, r) {
            var i = H.extend(new H.Event, n, {type: e, isSimulated: !0, originalEvent: {}});
            r ? H.event.trigger(i, null, t) : H.event.dispatch.call(t, i), i.isDefaultPrevented() && n.preventDefault()
        }}, H.event.handle = H.event.dispatch, H.removeEvent = _.removeEventListener ? function(e, t, n) {
        e.removeEventListener && e.removeEventListener(t, n, !1)
    } : function(e, t, n) {
        e.detachEvent && e.detachEvent("on" + t, n)
    }, H.Event = function(e, t) {
        if (!(this instanceof H.Event))
            return new H.Event(e, t);
        e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || e.returnValue === !1 || e.getPreventDefault && e.getPreventDefault() ? C : k) : this.type = e, t && H.extend(this, t), this.timeStamp = e && e.timeStamp || H.now(), this[H.expando] = !0
    }, H.Event.prototype = {preventDefault: function() {
            this.isDefaultPrevented = C;
            var e = this.originalEvent;
            !e || (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
        }, stopPropagation: function() {
            this.isPropagationStopped = C;
            var e = this.originalEvent;
            !e || (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0)
        }, stopImmediatePropagation: function() {
            this.isImmediatePropagationStopped = C, this.stopPropagation()
        }, isDefaultPrevented: k, isPropagationStopped: k, isImmediatePropagationStopped: k}, H.each({mouseenter: "mouseover", mouseleave: "mouseout"}, function(e, t) {
        H.event.special[e] = {delegateType: t, bindType: t, handle: function(e) {
                var n = this, r = e.relatedTarget, i = e.handleObj, s = i.selector, o;
                if (!r || r !== n && !H.contains(n, r))
                    e.type = i.origType, o = i.handler.apply(this, arguments), e.type = t;
                return o
            }}
    }), H.support.submitBubbles || (H.event.special.submit = {setup: function() {
            if (H.nodeName(this, "form"))
                return!1;
            H.event.add(this, "click._submit keypress._submit", function(e) {
                var n = e.target, r = H.nodeName(n, "input") || H.nodeName(n, "button") ? n.form : t;
                r && !r._submit_attached && (H.event.add(r, "submit._submit", function(e) {
                    e._submit_bubble = !0
                }), r._submit_attached = !0)
            })
        }, postDispatch: function(e) {
            e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && H.event.simulate("submit", this.parentNode, e, !0))
        }, teardown: function() {
            if (H.nodeName(this, "form"))
                return!1;
            H.event.remove(this, "._submit")
        }}), H.support.changeBubbles || (H.event.special.change = {setup: function() {
            if (G.test(this.nodeName)) {
                if (this.type === "checkbox" || this.type === "radio")
                    H.event.add(this, "propertychange._change", function(e) {
                        e.originalEvent.propertyName === "checked" && (this._just_changed = !0)
                    }), H.event.add(this, "click._change", function(e) {
                        this._just_changed && !e.isTrigger && (this._just_changed = !1, H.event.simulate("change", this, e, !0))
                    });
                return!1
            }
            H.event.add(this, "beforeactivate._change", function(e) {
                var t = e.target;
                G.test(t.nodeName) && !t._change_attached && (H.event.add(t, "change._change", function(e) {
                    this.parentNode && !e.isSimulated && !e.isTrigger && H.event.simulate("change", this.parentNode, e, !0)
                }), t._change_attached = !0)
            })
        }, handle: function(e) {
            var t = e.target;
            if (this !== t || e.isSimulated || e.isTrigger || t.type !== "radio" && t.type !== "checkbox")
                return e.handleObj.handler.apply(this, arguments)
        }, teardown: function() {
            return H.event.remove(this, "._change"), G.test(this.nodeName)
        }}), H.support.focusinBubbles || H.each({focus: "focusin", blur: "focusout"}, function(e, t) {
        var n = 0, r = function(e) {
            H.event.simulate(t, e.target, H.event.fix(e), !0)
        };
        H.event.special[t] = {setup: function() {
                n++ === 0 && _.addEventListener(e, r, !0)
            }, teardown: function() {
                --n === 0 && _.removeEventListener(e, r, !0)
            }}
    }), H.fn.extend({on: function(e, n, r, i, s) {
            var o, u;
            if (typeof e == "object") {
                typeof n != "string" && (r = r || n, n = t);
                for (u in e)
                    this.on(u, n, r, e[u], s);
                return this
            }
            r == null && i == null ? (i = n, r = n = t) : i == null && (typeof n == "string" ? (i = r, r = t) : (i = r, r = n, n = t));
            if (i === !1)
                i = k;
            else if (!i)
                return this;
            return s === 1 && (o = i, i = function(e) {
                return H().off(e), o.apply(this, arguments)
            }, i.guid = o.guid || (o.guid = H.guid++)), this.each(function() {
                H.event.add(this, e, i, r, n)
            })
        }, one: function(e, t, n, r) {
            return this.on(e, t, n, r, 1)
        }, off: function(e, n, r) {
            if (e && e.preventDefault && e.handleObj) {
                var i = e.handleObj;
                return H(e.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler), this
            }
            if (typeof e == "object") {
                for (var s in e)
                    this.off(s, n, e[s]);
                return this
            }
            if (n === !1 || typeof n == "function")
                r = n, n = t;
            return r === !1 && (r = k), this.each(function() {
                H.event.remove(this, e, r, n)
            })
        }, bind: function(e, t, n) {
            return this.on(e, null, t, n)
        }, unbind: function(e, t) {
            return this.off(e, null, t)
        }, live: function(e, t, n) {
            return H(this.context).on(e, this.selector, t, n), this
        }, die: function(e, t) {
            return H(this.context).off(e, this.selector || "**", t), this
        }, delegate: function(e, t, n, r) {
            return this.on(t, e, n, r)
        }, undelegate: function(e, t, n) {
            return arguments.length == 1 ? this.off(e, "**") : this.off(t, e, n)
        }, trigger: function(e, t) {
            return this.each(function() {
                H.event.trigger(e, t, this)
            })
        }, triggerHandler: function(e, t) {
            if (this[0])
                return H.event.trigger(e, t, this[0], !0)
        }, toggle: function(e) {
            var t = arguments, n = e.guid || H.guid++, r = 0, i = function(n) {
                var i = (H._data(this, "lastToggle" + e.guid) || 0) % r;
                return H._data(this, "lastToggle" + e.guid, i + 1), n.preventDefault(), t[i].apply(this, arguments) || !1
            };
            i.guid = n;
            while (r < t.length)
                t[r++].guid = n;
            return this.click(i)
        }, hover: function(e, t) {
            return this.mouseenter(e).mouseleave(t || e)
        }}), H.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) {
        H.fn[t] = function(e, n) {
            return n == null && (n = e, e = null), arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
        }, H.attrFn && (H.attrFn[t] = !0), et.test(t) && (H.event.fixHooks[t] = H.event.keyHooks), tt.test(t) && (H.event.fixHooks[t] = H.event.mouseHooks)
    }), function() {
        function e(e, t, n, r, s, o) {
            for (var u = 0, a = r.length; u < a; u++) {
                var f = r[u];
                if (f) {
                    var l = !1;
                    f = f[e];
                    while (f) {
                        if (f[i] === n) {
                            l = r[f.sizset];
                            break
                        }
                        if (f.nodeType === 1) {
                            o || (f[i] = n, f.sizset = u);
                            if (typeof t != "string") {
                                if (f === t) {
                                    l = !0;
                                    break
                                }
                            } else if (h.filter(t, [f]).length > 0) {
                                l = f;
                                break
                            }
                        }
                        f = f[e]
                    }
                    r[u] = l
                }
            }
        }
        function n(e, t, n, r, s, o) {
            for (var u = 0, a = r.length; u < a; u++) {
                var f = r[u];
                if (f) {
                    var l = !1;
                    f = f[e];
                    while (f) {
                        if (f[i] === n) {
                            l = r[f.sizset];
                            break
                        }
                        f.nodeType === 1 && !o && (f[i] = n, f.sizset = u);
                        if (f.nodeName.toLowerCase() === t) {
                            l = f;
                            break
                        }
                        f = f[e]
                    }
                    r[u] = l
                }
            }
        }
        var r = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g, i = "sizcache" + (Math.random() + "").replace(".", ""), s = 0, o = Object.prototype.toString, u = !1, a = !0, f = /\\/g, l = /\r\n/g, c = /\W/;
        [0, 0].sort(function() {
            return a = !1, 0
        });
        var h = function(e, t, n, i) {
            n = n || [], t = t || _;
            var s = t;
            if (t.nodeType !== 1 && t.nodeType !== 9)
                return[];
            if (!e || typeof e != "string")
                return n;
            var u, a, f, l, c, p, m, g, b = !0, w = h.isXML(t), E = [], x = e;
            do {
                r.exec(""), u = r.exec(x);
                if (u) {
                    x = u[3], E.push(u[1]);
                    if (u[2]) {
                        l = u[3];
                        break
                    }
                }
            } while (u);
            if (E.length > 1 && v.exec(e))
                if (E.length === 2 && d.relative[E[0]])
                    a = S(E[0] + E[1], t, i);
                else {
                    a = d.relative[E[0]] ? [t] : h(E.shift(), t);
                    while (E.length)
                        e = E.shift(), d.relative[e] && (e += E.shift()), a = S(e, a, i)
                }
            else {
                !i && E.length > 1 && t.nodeType === 9 && !w && d.match.ID.test(E[0]) && !d.match.ID.test(E[E.length - 1]) && (c = h.find(E.shift(), t, w), t = c.expr ? h.filter(c.expr, c.set)[0] : c.set[0]);
                if (t) {
                    c = i ? {expr: E.pop(), set: y(i)} : h.find(E.pop(), E.length !== 1 || E[0] !== "~" && E[0] !== "+" || !t.parentNode ? t : t.parentNode, w), a = c.expr ? h.filter(c.expr, c.set) : c.set, E.length > 0 ? f = y(a) : b = !1;
                    while (E.length)
                        p = E.pop(), m = p, d.relative[p] ? m = E.pop() : p = "", m == null && (m = t), d.relative[p](f, m, w)
                } else
                    f = E = []
            }
            f || (f = a), f || h.error(p || e);
            if (o.call(f) === "[object Array]")
                if (!b)
                    n.push.apply(n, f);
                else if (t && t.nodeType === 1)
                    for (g = 0; f[g] != null; g++)
                        f[g] && (f[g] === !0 || f[g].nodeType === 1 && h.contains(t, f[g])) && n.push(a[g]);
                else
                    for (g = 0; f[g] != null; g++)
                        f[g] && f[g].nodeType === 1 && n.push(a[g]);
            else
                y(f, n);
            return l && (h(l, s, n, i), h.uniqueSort(n)), n
        };
        h.uniqueSort = function(e) {
            if (w) {
                u = a, e.sort(w);
                if (u)
                    for (var t = 1; t < e.length; t++)
                        e[t] === e[t - 1] && e.splice(t--, 1)
            }
            return e
        }, h.matches = function(e, t) {
            return h(e, null, null, t)
        }, h.matchesSelector = function(e, t) {
            return h(t, null, null, [e]).length > 0
        }, h.find = function(e, t, n) {
            var r, i, s, o, u, a;
            if (!e)
                return[];
            for (i = 0, s = d.order.length; i < s; i++) {
                u = d.order[i];
                if (o = d.leftMatch[u].exec(e)) {
                    a = o[1], o.splice(1, 1);
                    if (a.substr(a.length - 1) !== "\\") {
                        o[1] = (o[1] || "").replace(f, ""), r = d.find[u](o, t, n);
                        if (r != null) {
                            e = e.replace(d.match[u], "");
                            break
                        }
                    }
                }
            }
            return r || (r = typeof t.getElementsByTagName != "undefined" ? t.getElementsByTagName("*") : []), {set: r, expr: e}
        }, h.filter = function(e, n, r, i) {
            var s, o, u, a, f, l, c, p, v, m = e, g = [], y = n, b = n && n[0] && h.isXML(n[0]);
            while (e && n.length) {
                for (u in d.filter)
                    if ((s = d.leftMatch[u].exec(e)) != null && s[2]) {
                        l = d.filter[u], c = s[1], o = !1, s.splice(1, 1);
                        if (c.substr(c.length - 1) === "\\")
                            continue;
                        y === g && (g = []);
                        if (d.preFilter[u]) {
                            s = d.preFilter[u](s, y, r, g, i, b);
                            if (!s)
                                o = a = !0;
                            else if (s === !0)
                                continue
                        }
                        if (s)
                            for (p = 0; (f = y[p]) != null; p++)
                                f && (a = l(f, s, p, y), v = i ^ a, r && a != null ? v ? o = !0 : y[p] = !1 : v && (g.push(f), o = !0));
                        if (a !== t) {
                            r || (y = g), e = e.replace(d.match[u], "");
                            if (!o)
                                return[];
                            break
                        }
                    }
                if (e === m) {
                    if (o != null)
                        break;
                    h.error(e)
                }
                m = e
            }
            return y
        }, h.error = function(e) {
            throw new Error("Syntax error, unrecognized expression: " + e)
        };
        var p = h.getText = function(e) {
            var t, n, r = e.nodeType, i = "";
            if (r) {
                if (r === 1 || r === 9 || r === 11) {
                    if (typeof e.textContent == "string")
                        return e.textContent;
                    if (typeof e.innerText == "string")
                        return e.innerText.replace(l, "");
                    for (e = e.firstChild; e; e = e.nextSibling)
                        i += p(e)
                } else if (r === 3 || r === 4)
                    return e.nodeValue
            } else
                for (t = 0; n = e[t]; t++)
                    n.nodeType !== 8 && (i += p(n));
            return i
        }, d = h.selectors = {order: ["ID", "NAME", "TAG"], match: {ID: /#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/, CLASS: /\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/, NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/, ATTR: /\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/, TAG: /^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/, CHILD: /:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/, POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/, PSEUDO: /:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/}, leftMatch: {}, attrMap: {"class": "className", "for": "htmlFor"}, attrHandle: {href: function(e) {
                    return e.getAttribute("href")
                }, type: function(e) {
                    return e.getAttribute("type")
                }}, relative: {"+": function(e, t) {
                    var n = typeof t == "string", r = n && !c.test(t), i = n && !r;
                    r && (t = t.toLowerCase());
                    for (var s = 0, o = e.length, u; s < o; s++)
                        if (u = e[s]) {
                            while ((u = u.previousSibling) && u.nodeType !== 1)
                                ;
                            e[s] = i || u && u.nodeName.toLowerCase() === t ? u || !1 : u === t
                        }
                    i && h.filter(t, e, !0)
                }, ">": function(e, t) {
                    var n, r = typeof t == "string", i = 0, s = e.length;
                    if (r && !c.test(t)) {
                        t = t.toLowerCase();
                        for (; i < s; i++) {
                            n = e[i];
                            if (n) {
                                var o = n.parentNode;
                                e[i] = o.nodeName.toLowerCase() === t ? o : !1
                            }
                        }
                    } else {
                        for (; i < s; i++)
                            n = e[i], n && (e[i] = r ? n.parentNode : n.parentNode === t);
                        r && h.filter(t, e, !0)
                    }
                }, "": function(t, r, i) {
                    var o, u = s++, a = e;
                    typeof r == "string" && !c.test(r) && (r = r.toLowerCase(), o = r, a = n), a("parentNode", r, u, t, o, i)
                }, "~": function(t, r, i) {
                    var o, u = s++, a = e;
                    typeof r == "string" && !c.test(r) && (r = r.toLowerCase(), o = r, a = n), a("previousSibling", r, u, t, o, i)
                }}, find: {ID: function(e, t, n) {
                    if (typeof t.getElementById != "undefined" && !n) {
                        var r = t.getElementById(e[1]);
                        return r && r.parentNode ? [r] : []
                    }
                }, NAME: function(e, t) {
                    if (typeof t.getElementsByName != "undefined") {
                        var n = [], r = t.getElementsByName(e[1]);
                        for (var i = 0, s = r.length; i < s; i++)
                            r[i].getAttribute("name") === e[1] && n.push(r[i]);
                        return n.length === 0 ? null : n
                    }
                }, TAG: function(e, t) {
                    if (typeof t.getElementsByTagName != "undefined")
                        return t.getElementsByTagName(e[1])
                }}, preFilter: {CLASS: function(e, t, n, r, i, s) {
                    e = " " + e[1].replace(f, "") + " ";
                    if (s)
                        return e;
                    for (var o = 0, u; (u = t[o]) != null; o++)
                        u && (i ^ (u.className && (" " + u.className + " ").replace(/[\t\n\r]/g, " ").indexOf(e) >= 0) ? n || r.push(u) : n && (t[o] = !1));
                    return!1
                }, ID: function(e) {
                    return e[1].replace(f, "")
                }, TAG: function(e, t) {
                    return e[1].replace(f, "").toLowerCase()
                }, CHILD: function(e) {
                    if (e[1] === "nth") {
                        e[2] || h.error(e[0]), e[2] = e[2].replace(/^\+|\s*/g, "");
                        var t = /(-?)(\d*)(?:n([+\-]?\d*))?/.exec(e[2] === "even" && "2n" || e[2] === "odd" && "2n+1" || !/\D/.test(e[2]) && "0n+" + e[2] || e[2]);
                        e[2] = t[1] + (t[2] || 1) - 0, e[3] = t[3] - 0
                    } else
                        e[2] && h.error(e[0]);
                    return e[0] = s++, e
                }, ATTR: function(e, t, n, r, i, s) {
                    var o = e[1] = e[1].replace(f, "");
                    return!s && d.attrMap[o] && (e[1] = d.attrMap[o]), e[4] = (e[4] || e[5] || "").replace(f, ""), e[2] === "~=" && (e[4] = " " + e[4] + " "), e
                }, PSEUDO: function(e, t, n, i, s) {
                    if (e[1] === "not") {
                        if (!((r.exec(e[3]) || "").length > 1 || /^\w/.test(e[3]))) {
                            var o = h.filter(e[3], t, n, !0 ^ s);
                            return n || i.push.apply(i, o), !1
                        }
                        e[3] = h(e[3], null, null, t)
                    } else if (d.match.POS.test(e[0]) || d.match.CHILD.test(e[0]))
                        return!0;
                    return e
                }, POS: function(e) {
                    return e.unshift(!0), e
                }}, filters: {enabled: function(e) {
                    return e.disabled === !1 && e.type !== "hidden"
                }, disabled: function(e) {
                    return e.disabled === !0
                }, checked: function(e) {
                    return e.checked === !0
                }, selected: function(e) {
                    return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                }, parent: function(e) {
                    return!!e.firstChild
                }, empty: function(e) {
                    return!e.firstChild
                }, has: function(e, t, n) {
                    return!!h(n[3], e).length
                }, header: function(e) {
                    return/h\d/i.test(e.nodeName)
                }, text: function(e) {
                    var t = e.getAttribute("type"), n = e.type;
                    return e.nodeName.toLowerCase() === "input" && "text" === n && (t === n || t === null)
                }, radio: function(e) {
                    return e.nodeName.toLowerCase() === "input" && "radio" === e.type
                }, checkbox: function(e) {
                    return e.nodeName.toLowerCase() === "input" && "checkbox" === e.type
                }, file: function(e) {
                    return e.nodeName.toLowerCase() === "input" && "file" === e.type
                }, password: function(e) {
                    return e.nodeName.toLowerCase() === "input" && "password" === e.type
                }, submit: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return(t === "input" || t === "button") && "submit" === e.type
                }, image: function(e) {
                    return e.nodeName.toLowerCase() === "input" && "image" === e.type
                }, reset: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return(t === "input" || t === "button") && "reset" === e.type
                }, button: function(e) {
                    var t = e.nodeName.toLowerCase();
                    return t === "input" && "button" === e.type || t === "button"
                }, input: function(e) {
                    return/input|select|textarea|button/i.test(e.nodeName)
                }, focus: function(e) {
                    return e === e.ownerDocument.activeElement
                }}, setFilters: {first: function(e, t) {
                    return t === 0
                }, last: function(e, t, n, r) {
                    return t === r.length - 1
                }, even: function(e, t) {
                    return t % 2 === 0
                }, odd: function(e, t) {
                    return t % 2 === 1
                }, lt: function(e, t, n) {
                    return t < n[3] - 0
                }, gt: function(e, t, n) {
                    return t > n[3] - 0
                }, nth: function(e, t, n) {
                    return n[3] - 0 === t
                }, eq: function(e, t, n) {
                    return n[3] - 0 === t
                }}, filter: {PSEUDO: function(e, t, n, r) {
                    var i = t[1], s = d.filters[i];
                    if (s)
                        return s(e, n, t, r);
                    if (i === "contains")
                        return(e.textContent || e.innerText || p([e]) || "").indexOf(t[3]) >= 0;
                    if (i === "not") {
                        var o = t[3];
                        for (var u = 0, a = o.length; u < a; u++)
                            if (o[u] === e)
                                return!1;
                        return!0
                    }
                    h.error(i)
                }, CHILD: function(e, t) {
                    var n, r, s, o, u, a, f, l = t[1], c = e;
                    switch (l) {
                        case"only":
                        case"first":
                            while (c = c.previousSibling)
                                if (c.nodeType === 1)
                                    return!1;
                            if (l === "first")
                                return!0;
                            c = e;
                        case"last":
                            while (c = c.nextSibling)
                                if (c.nodeType === 1)
                                    return!1;
                            return!0;
                        case"nth":
                            n = t[2], r = t[3];
                            if (n === 1 && r === 0)
                                return!0;
                            s = t[0], o = e.parentNode;
                            if (o && (o[i] !== s || !e.nodeIndex)) {
                                a = 0;
                                for (c = o.firstChild; c; c = c.nextSibling)
                                    c.nodeType === 1 && (c.nodeIndex = ++a);
                                o[i] = s
                            }
                            return f = e.nodeIndex - r, n === 0 ? f === 0 : f % n === 0 && f / n >= 0
                    }
                }, ID: function(e, t) {
                    return e.nodeType === 1 && e.getAttribute("id") === t
                }, TAG: function(e, t) {
                    return t === "*" && e.nodeType === 1 || !!e.nodeName && e.nodeName.toLowerCase() === t
                }, CLASS: function(e, t) {
                    return(" " + (e.className || e.getAttribute("class")) + " ").indexOf(t) > -1
                }, ATTR: function(e, t) {
                    var n = t[1], r = h.attr ? h.attr(e, n) : d.attrHandle[n] ? d.attrHandle[n](e) : e[n] != null ? e[n] : e.getAttribute(n), i = r + "", s = t[2], o = t[4];
                    return r == null ? s === "!=" : !s && h.attr ? r != null : s === "=" ? i === o : s === "*=" ? i.indexOf(o) >= 0 : s === "~=" ? (" " + i + " ").indexOf(o) >= 0 : o ? s === "!=" ? i !== o : s === "^=" ? i.indexOf(o) === 0 : s === "$=" ? i.substr(i.length - o.length) === o : s === "|=" ? i === o || i.substr(0, o.length + 1) === o + "-" : !1 : i && r !== !1
                }, POS: function(e, t, n, r) {
                    var i = t[2], s = d.setFilters[i];
                    if (s)
                        return s(e, n, t, r)
                }}}, v = d.match.POS, m = function(e, t) {
            return"\\" + (t - 0 + 1)
        };
        for (var g in d.match)
            d.match[g] = new RegExp(d.match[g].source + /(?![^\[]*\])(?![^\(]*\))/.source), d.leftMatch[g] = new RegExp(/(^(?:.|\r|\n)*?)/.source + d.match[g].source.replace(/\\(\d+)/g, m));
        d.match.globalPOS = v;
        var y = function(e, t) {
            return e = Array.prototype.slice.call(e, 0), t ? (t.push.apply(t, e), t) : e
        };
        try {
            Array.prototype.slice.call(_.documentElement.childNodes, 0)[0].nodeType
        } catch (b) {
            y = function(e, t) {
                var n = 0, r = t || [];
                if (o.call(e) === "[object Array]")
                    Array.prototype.push.apply(r, e);
                else if (typeof e.length == "number")
                    for (var i = e.length; n < i; n++)
                        r.push(e[n]);
                else
                    for (; e[n]; n++)
                        r.push(e[n]);
                return r
            }
        }
        var w, E;
        _.documentElement.compareDocumentPosition ? w = function(e, t) {
            return e === t ? (u = !0, 0) : !e.compareDocumentPosition || !t.compareDocumentPosition ? e.compareDocumentPosition ? -1 : 1 : e.compareDocumentPosition(t) & 4 ? -1 : 1
        } : (w = function(e, t) {
            if (e === t)
                return u = !0, 0;
            if (e.sourceIndex && t.sourceIndex)
                return e.sourceIndex - t.sourceIndex;
            var n, r, i = [], s = [], o = e.parentNode, a = t.parentNode, f = o;
            if (o === a)
                return E(e, t);
            if (!o)
                return-1;
            if (!a)
                return 1;
            while (f)
                i.unshift(f), f = f.parentNode;
            f = a;
            while (f)
                s.unshift(f), f = f.parentNode;
            n = i.length, r = s.length;
            for (var l = 0; l < n && l < r; l++)
                if (i[l] !== s[l])
                    return E(i[l], s[l]);
            return l === n ? E(e, s[l], -1) : E(i[l], t, 1)
        }, E = function(e, t, n) {
            if (e === t)
                return n;
            var r = e.nextSibling;
            while (r) {
                if (r === t)
                    return-1;
                r = r.nextSibling
            }
            return 1
        }), function() {
            var e = _.createElement("div"), n = "script" + (new Date).getTime(), r = _.documentElement;
            e.innerHTML = "<a name='" + n + "'/>", r.insertBefore(e, r.firstChild), _.getElementById(n) && (d.find.ID = function(e, n, r) {
                if (typeof n.getElementById != "undefined" && !r) {
                    var i = n.getElementById(e[1]);
                    return i ? i.id === e[1] || typeof i.getAttributeNode != "undefined" && i.getAttributeNode("id").nodeValue === e[1] ? [i] : t : []
                }
            }, d.filter.ID = function(e, t) {
                var n = typeof e.getAttributeNode != "undefined" && e.getAttributeNode("id");
                return e.nodeType === 1 && n && n.nodeValue === t
            }), r.removeChild(e), r = e = null
        }(), function() {
            var e = _.createElement("div");
            e.appendChild(_.createComment("")), e.getElementsByTagName("*").length > 0 && (d.find.TAG = function(e, t) {
                var n = t.getElementsByTagName(e[1]);
                if (e[1] === "*") {
                    var r = [];
                    for (var i = 0; n[i]; i++)
                        n[i].nodeType === 1 && r.push(n[i]);
                    n = r
                }
                return n
            }), e.innerHTML = "<a href='#'></a>", e.firstChild && typeof e.firstChild.getAttribute != "undefined" && e.firstChild.getAttribute("href") !== "#" && (d.attrHandle.href = function(e) {
                return e.getAttribute("href", 2)
            }), e = null
        }(), _.querySelectorAll && function() {
            var e = h, t = _.createElement("div"), n = "__sizzle__";
            t.innerHTML = "<p class='TEST'></p>";
            if (!t.querySelectorAll || t.querySelectorAll(".TEST").length !== 0) {
                h = function(t, r, i, s) {
                    r = r || _;
                    if (!s && !h.isXML(r)) {
                        var o = /^(\w+$)|^\.([\w\-]+$)|^#([\w\-]+$)/.exec(t);
                        if (o && (r.nodeType === 1 || r.nodeType === 9)) {
                            if (o[1])
                                return y(r.getElementsByTagName(t), i);
                            if (o[2] && d.find.CLASS && r.getElementsByClassName)
                                return y(r.getElementsByClassName(o[2]), i)
                        }
                        if (r.nodeType === 9) {
                            if (t === "body" && r.body)
                                return y([r.body], i);
                            if (o && o[3]) {
                                var u = r.getElementById(o[3]);
                                if (!u || !u.parentNode)
                                    return y([], i);
                                if (u.id === o[3])
                                    return y([u], i)
                            }
                            try {
                                return y(r.querySelectorAll(t), i)
                            } catch (a) {
                            }
                        } else if (r.nodeType === 1 && r.nodeName.toLowerCase() !== "object") {
                            var f = r, l = r.getAttribute("id"), c = l || n, p = r.parentNode, v = /^\s*[+~]/.test(t);
                            l ? c = c.replace(/'/g, "\\$&") : r.setAttribute("id", c), v && p && (r = r.parentNode);
                            try {
                                if (!v || p)
                                    return y(r.querySelectorAll("[id='" + c + "'] " + t), i)
                            } catch (m) {
                            } finally {
                                l || f.removeAttribute("id")
                            }
                        }
                    }
                    return e(t, r, i, s)
                };
                for (var r in e)
                    h[r] = e[r];
                t = null
            }
        }(), function() {
            var e = _.documentElement, t = e.matchesSelector || e.mozMatchesSelector || e.webkitMatchesSelector || e.msMatchesSelector;
            if (t) {
                var n = !t.call(_.createElement("div"), "div"), r = !1;
                try {
                    t.call(_.documentElement, "[test!='']:sizzle")
                } catch (i) {
                    r = !0
                }
                h.matchesSelector = function(e, i) {
                    i = i.replace(/\=\s*([^'"\]]*)\s*\]/g, "='$1']");
                    if (!h.isXML(e))
                        try {
                            if (r || !d.match.PSEUDO.test(i) && !/!=/.test(i)) {
                                var s = t.call(e, i);
                                if (s || !n || e.document && e.document.nodeType !== 11)
                                    return s
                            }
                        } catch (o) {
                        }
                    return h(i, null, null, [e]).length > 0
                }
            }
        }(), function() {
            var e = _.createElement("div");
            e.innerHTML = "<div class='test e'></div><div class='test'></div>";
            if (!!e.getElementsByClassName && e.getElementsByClassName("e").length !== 0) {
                e.lastChild.className = "e";
                if (e.getElementsByClassName("e").length === 1)
                    return;
                d.order.splice(1, 0, "CLASS"), d.find.CLASS = function(e, t, n) {
                    if (typeof t.getElementsByClassName != "undefined" && !n)
                        return t.getElementsByClassName(e[1])
                }, e = null
            }
        }(), _.documentElement.contains ? h.contains = function(e, t) {
            return e !== t && (e.contains ? e.contains(t) : !0)
        } : _.documentElement.compareDocumentPosition ? h.contains = function(e, t) {
            return!!(e.compareDocumentPosition(t) & 16)
        } : h.contains = function() {
            return!1
        }, h.isXML = function(e) {
            var t = (e ? e.ownerDocument || e : 0).documentElement;
            return t ? t.nodeName !== "HTML" : !1
        };
        var S = function(e, t, n) {
            var r, i = [], s = "", o = t.nodeType ? [t] : t;
            while (r = d.match.PSEUDO.exec(e))
                s += r[0], e = e.replace(d.match.PSEUDO, "");
            e = d.relative[e] ? e + "*" : e;
            for (var u = 0, a = o.length; u < a; u++)
                h(e, o[u], i, n);
            return h.filter(s, i)
        };
        h.attr = H.attr, h.selectors.attrMap = {}, H.find = h, H.expr = h.selectors, H.expr[":"] = H.expr.filters, H.unique = h.uniqueSort, H.text = h.getText, H.isXMLDoc = h.isXML, H.contains = h.contains
    }();
    var ut = /Until$/, at = /^(?:parents|prevUntil|prevAll)/, ft = /,/, lt = /^.[^:#\[\.,]*$/, ct = Array.prototype.slice, ht = H.expr.match.globalPOS, pt = {children: !0, contents: !0, next: !0, prev: !0};
    H.fn.extend({find: function(e) {
            var t = this, n, r;
            if (typeof e != "string")
                return H(e).filter(function() {
                    for (n = 0, r = t.length; n < r; n++)
                        if (H.contains(t[n], this))
                            return!0
                });
            var i = this.pushStack("", "find", e), s, o, u;
            for (n = 0, r = this.length; n < r; n++) {
                s = i.length, H.find(e, this[n], i);
                if (n > 0)
                    for (o = s; o < i.length; o++)
                        for (u = 0; u < s; u++)
                            if (i[u] === i[o]) {
                                i.splice(o--, 1);
                                break
                            }
            }
            return i
        }, has: function(e) {
            var t = H(e);
            return this.filter(function() {
                for (var e = 0, n = t.length; e < n; e++)
                    if (H.contains(this, t[e]))
                        return!0
            })
        }, not: function(e) {
            return this.pushStack(T(this, e, !1), "not", e)
        }, filter: function(e) {
            return this.pushStack(T(this, e, !0), "filter", e)
        }, is: function(e) {
            return!!e && (typeof e == "string" ? ht.test(e) ? H(e, this.context).index(this[0]) >= 0 : H.filter(e, this).length > 0 : this.filter(e).length > 0)
        }, closest: function(e, t) {
            var n = [], r, i, s = this[0];
            if (H.isArray(e)) {
                var o = 1;
                while (s && s.ownerDocument && s !== t) {
                    for (r = 0; r < e.length; r++)
                        H(s).is(e[r]) && n.push({selector: e[r], elem: s, level: o});
                    s = s.parentNode, o++
                }
                return n
            }
            var u = ht.test(e) || typeof e != "string" ? H(e, t || this.context) : 0;
            for (r = 0, i = this.length; r < i; r++) {
                s = this[r];
                while (s) {
                    if (u ? u.index(s) > -1 : H.find.matchesSelector(s, e)) {
                        n.push(s);
                        break
                    }
                    s = s.parentNode;
                    if (!s || !s.ownerDocument || s === t || s.nodeType === 11)
                        break
                }
            }
            return n = n.length > 1 ? H.unique(n) : n, this.pushStack(n, "closest", e)
        }, index: function(e) {
            return e ? typeof e == "string" ? H.inArray(this[0], H(e)) : H.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.prevAll().length : -1
        }, add: function(e, t) {
            var n = typeof e == "string" ? H(e, t) : H.makeArray(e && e.nodeType ? [e] : e), r = H.merge(this.get(), n);
            return this.pushStack(N(n[0]) || N(r[0]) ? r : H.unique(r))
        }, andSelf: function() {
            return this.add(this.prevObject)
        }}), H.each({parent: function(e) {
            var t = e.parentNode;
            return t && t.nodeType !== 11 ? t : null
        }, parents: function(e) {
            return H.dir(e, "parentNode")
        }, parentsUntil: function(e, t, n) {
            return H.dir(e, "parentNode", n)
        }, next: function(e) {
            return H.nth(e, 2, "nextSibling")
        }, prev: function(e) {
            return H.nth(e, 2, "previousSibling")
        }, nextAll: function(e) {
            return H.dir(e, "nextSibling")
        }, prevAll: function(e) {
            return H.dir(e, "previousSibling")
        }, nextUntil: function(e, t, n) {
            return H.dir(e, "nextSibling", n)
        }, prevUntil: function(e, t, n) {
            return H.dir(e, "previousSibling", n)
        }, siblings: function(e) {
            return H.sibling((e.parentNode || {}).firstChild, e)
        }, children: function(e) {
            return H.sibling(e.firstChild)
        }, contents: function(e) {
            return H.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : H.makeArray(e.childNodes)
        }}, function(e, t) {
        H.fn[e] = function(n, r) {
            var i = H.map(this, t, n);
            return ut.test(e) || (r = n), r && typeof r == "string" && (i = H.filter(r, i)), i = this.length > 1 && !pt[e] ? H.unique(i) : i, (this.length > 1 || ft.test(r)) && at.test(e) && (i = i.reverse()), this.pushStack(i, e, ct.call(arguments).join(","))
        }
    }), H.extend({filter: function(e, t, n) {
            return n && (e = ":not(" + e + ")"), t.length === 1 ? H.find.matchesSelector(t[0], e) ? [t[0]] : [] : H.find.matches(e, t)
        }, dir: function(e, n, r) {
            var i = [], s = e[n];
            while (s && s.nodeType !== 9 && (r === t || s.nodeType !== 1 || !H(s).is(r)))
                s.nodeType === 1 && i.push(s), s = s[n];
            return i
        }, nth: function(e, t, n, r) {
            t = t || 1;
            var i = 0;
            for (; e; e = e[n])
                if (e.nodeType === 1 && ++i === t)
                    break;
            return e
        }, sibling: function(e, t) {
            var n = [];
            for (; e; e = e.nextSibling)
                e.nodeType === 1 && e !== t && n.push(e);
            return n
        }});
    var dt = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video", vt = / jQuery\d+="(?:\d+|null)"/g, mt = /^\s+/, gt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/ig, yt = /<([\w:]+)/, bt = /<tbody/i, wt = /<|&#?\w+;/, Et = /<(?:script|style)/i, St = /<(?:script|object|embed|option|style)/i, xt = new RegExp("<(?:" + dt + ")[\\s/>]", "i"), Tt = /checked\s*(?:[^=]|=\s*.checked.)/i, Nt = /\/(java|ecma)script/i, Ct = /^\s*<!(?:\[CDATA\[|\-\-)/, kt = {option: [1, "<select multiple='multiple'>", "</select>"], legend: [1, "<fieldset>", "</fieldset>"], thead: [1, "<table>", "</table>"], tr: [2, "<table><tbody>", "</tbody></table>"], td: [3, "<table><tbody><tr>", "</tr></tbody></table>"], col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"], area: [1, "<map>", "</map>"], _default: [0, "", ""]}, Lt = x(_);
    kt.optgroup = kt.option, kt.tbody = kt.tfoot = kt.colgroup = kt.caption = kt.thead, kt.th = kt.td, H.support.htmlSerialize || (kt._default = [1, "div<div>", "</div>"]), H.fn.extend({text: function(e) {
            return H.access(this, function(e) {
                return e === t ? H.text(this) : this.empty().append((this[0] && this[0].ownerDocument || _).createTextNode(e))
            }, null, e, arguments.length)
        }, wrapAll: function(e) {
            if (H.isFunction(e))
                return this.each(function(t) {
                    H(this).wrapAll(e.call(this, t))
                });
            if (this[0]) {
                var t = H(e, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                    var e = this;
                    while (e.firstChild && e.firstChild.nodeType === 1)
                        e = e.firstChild;
                    return e
                }).append(this)
            }
            return this
        }, wrapInner: function(e) {
            return H.isFunction(e) ? this.each(function(t) {
                H(this).wrapInner(e.call(this, t))
            }) : this.each(function() {
                var t = H(this), n = t.contents();
                n.length ? n.wrapAll(e) : t.append(e)
            })
        }, wrap: function(e) {
            var t = H.isFunction(e);
            return this.each(function(n) {
                H(this).wrapAll(t ? e.call(this, n) : e)
            })
        }, unwrap: function() {
            return this.parent().each(function() {
                H.nodeName(this, "body") || H(this).replaceWith(this.childNodes)
            }).end()
        }, append: function() {
            return this.domManip(arguments, !0, function(e) {
                this.nodeType === 1 && this.appendChild(e)
            })
        }, prepend: function() {
            return this.domManip(arguments, !0, function(e) {
                this.nodeType === 1 && this.insertBefore(e, this.firstChild)
            })
        }, before: function() {
            if (this[0] && this[0].parentNode)
                return this.domManip(arguments, !1, function(e) {
                    this.parentNode.insertBefore(e, this)
                });
            if (arguments.length) {
                var e = H.clean(arguments);
                return e.push.
                        apply(e, this.toArray()), this.pushStack(e, "before", arguments)
            }
        }, after: function() {
            if (this[0] && this[0].parentNode)
                return this.domManip(arguments, !1, function(e) {
                    this.parentNode.insertBefore(e, this.nextSibling)
                });
            if (arguments.length) {
                var e = this.pushStack(this, "after", arguments);
                return e.push.apply(e, H.clean(arguments)), e
            }
        }, remove: function(e, t) {
            for (var n = 0, r; (r = this[n]) != null; n++)
                if (!e || H.filter(e, [r]).length)
                    !t && r.nodeType === 1 && (H.cleanData(r.getElementsByTagName("*")), H.cleanData([r])), r.parentNode && r.parentNode.removeChild(r);
            return this
        }, empty: function() {
            for (var e = 0, t; (t = this[e]) != null; e++) {
                t.nodeType === 1 && H.cleanData(t.getElementsByTagName("*"));
                while (t.firstChild)
                    t.removeChild(t.firstChild)
            }
            return this
        }, clone: function(e, t) {
            return e = e == null ? !1 : e, t = t == null ? e : t, this.map(function() {
                return H.clone(this, e, t)
            })
        }, html: function(e) {
            return H.access(this, function(e) {
                var n = this[0] || {}, r = 0, i = this.length;
                if (e === t)
                    return n.nodeType === 1 ? n.innerHTML.replace(vt, "") : null;
                if (typeof e == "string" && !Et.test(e) && (H.support.leadingWhitespace || !mt.test(e)) && !kt[(yt.exec(e) || ["", ""])[1].toLowerCase()]) {
                    e = e.replace(gt, "<$1></$2>");
                    try {
                        for (; r < i; r++)
                            n = this[r] || {}, n.nodeType === 1 && (H.cleanData(n.getElementsByTagName("*")), n.innerHTML = e);
                        n = 0
                    } catch (s) {
                    }
                }
                n && this.empty().append(e)
            }, null, e, arguments.length)
        }, replaceWith: function(e) {
            return this[0] && this[0].parentNode ? H.isFunction(e) ? this.each(function(t) {
                var n = H(this), r = n.html();
                n.replaceWith(e.call(this, t, r))
            }) : (typeof e != "string" && (e = H(e).detach()), this.each(function() {
                var t = this.nextSibling, n = this.parentNode;
                H(this).remove(), t ? H(t).before(e) : H(n).append(e)
            })) : this.length ? this.pushStack(H(H.isFunction(e) ? e() : e), "replaceWith", e) : this
        }, detach: function(e) {
            return this.remove(e, !0)
        }, domManip: function(e, n, r) {
            var i, s, o, u, a = e[0], f = [];
            if (!H.support.checkClone && arguments.length === 3 && typeof a == "string" && Tt.test(a))
                return this.each(function() {
                    H(this).domManip(e, n, r, !0)
                });
            if (H.isFunction(a))
                return this.each(function(i) {
                    var s = H(this);
                    e[0] = a.call(this, i, n ? s.html() : t), s.domManip(e, n, r)
                });
            if (this[0]) {
                u = a && a.parentNode, H.support.parentNode && u && u.nodeType === 11 && u.childNodes.length === this.length ? i = {fragment: u} : i = H.buildFragment(e, this, f), o = i.fragment, o.childNodes.length === 1 ? s = o = o.firstChild : s = o.firstChild;
                if (s) {
                    n = n && H.nodeName(s, "tr");
                    for (var l = 0, c = this.length, h = c - 1; l < c; l++)
                        r.call(n ? S(this[l], s) : this[l], i.cacheable || c > 1 && l < h ? H.clone(o, !0, !0) : o)
                }
                f.length && H.each(f, function(e, t) {
                    t.src ? H.ajax({type: "GET", global: !1, url: t.src, async: !1, dataType: "script"}) : H.globalEval((t.text || t.textContent || t.innerHTML || "").replace(Ct, "/*$0*/")), t.parentNode && t.parentNode.removeChild(t)
                })
            }
            return this
        }}), H.buildFragment = function(e, t, n) {
        var r, i, s, o, u = e[0];
        return t && t[0] && (o = t[0].ownerDocument || t[0]), o.createDocumentFragment || (o = _), e.length === 1 && typeof u == "string" && u.length < 512 && o === _ && u.charAt(0) === "<" && !St.test(u) && (H.support.checkClone || !Tt.test(u)) && (H.support.html5Clone || !xt.test(u)) && (i = !0, s = H.fragments[u], s && s !== 1 && (r = s)), r || (r = o.createDocumentFragment(), H.clean(e, o, r, n)), i && (H.fragments[u] = s ? r : 1), {fragment: r, cacheable: i}
    }, H.fragments = {}, H.each({appendTo: "append", prependTo: "prepend", insertBefore: "before", insertAfter: "after", replaceAll: "replaceWith"}, function(e, t) {
        H.fn[e] = function(n) {
            var r = [], i = H(n), s = this.length === 1 && this[0].parentNode;
            if (s && s.nodeType === 11 && s.childNodes.length === 1 && i.length === 1)
                return i[t](this[0]), this;
            for (var o = 0, u = i.length; o < u; o++) {
                var a = (o > 0 ? this.clone(!0) : this).get();
                H(i[o])[t](a), r = r.concat(a)
            }
            return this.pushStack(r, e, i.selector)
        }
    }), H.extend({clone: function(e, t, n) {
            var r, i, s, o = H.support.html5Clone || H.isXMLDoc(e) || !xt.test("<" + e.nodeName + ">") ? e.cloneNode(!0) : m(e);
            if ((!H.support.noCloneEvent || !H.support.noCloneChecked) && (e.nodeType === 1 || e.nodeType === 11) && !H.isXMLDoc(e)) {
                w(e, o), r = b(e), i = b(o);
                for (s = 0; r[s]; ++s)
                    i[s] && w(r[s], i[s])
            }
            if (t) {
                E(e, o);
                if (n) {
                    r = b(e), i = b(o);
                    for (s = 0; r[s]; ++s)
                        E(r[s], i[s])
                }
            }
            return r = i = null, o
        }, clean: function(e, t, n, r) {
            var i, s, o, u = [];
            t = t || _, typeof t.createElement == "undefined" && (t = t.ownerDocument || t[0] && t[0].ownerDocument || _);
            for (var a = 0, f; (f = e[a]) != null; a++) {
                typeof f == "number" && (f += "");
                if (!f)
                    continue;
                if (typeof f == "string")
                    if (!wt.test(f))
                        f = t.createTextNode(f);
                    else {
                        f = f.replace(gt, "<$1></$2>");
                        var l = (yt.exec(f) || ["", ""])[1].toLowerCase(), c = kt[l] || kt._default, h = c[0], p = t.createElement("div"), d = Lt.childNodes, v;
                        t === _ ? Lt.appendChild(p) : x(t).appendChild(p), p.innerHTML = c[1] + f + c[2];
                        while (h--)
                            p = p.lastChild;
                        if (!H.support.tbody) {
                            var m = bt.test(f), y = l === "table" && !m ? p.firstChild && p.firstChild.childNodes : c[1] === "<table>" && !m ? p.childNodes : [];
                            for (o = y.length - 1; o >= 0; --o)
                                H.nodeName(y[o], "tbody") && !y[o].childNodes.length && y[o].parentNode.removeChild(y[o])
                        }
                        !H.support.leadingWhitespace && mt.test(f) && p.insertBefore(t.createTextNode(mt.exec(f)[0]), p.firstChild), f = p.childNodes, p && (p.parentNode.removeChild(p), d.length > 0 && (v = d[d.length - 1], v && v.parentNode && v.parentNode.removeChild(v)))
                    }
                var b;
                if (!H.support.appendChecked)
                    if (f[0] && typeof (b = f.length) == "number")
                        for (o = 0; o < b; o++)
                            g(f[o]);
                    else
                        g(f);
                f.nodeType ? u.push(f) : u = H.merge(u, f)
            }
            if (n) {
                i = function(e) {
                    return!e.type || Nt.test(e.type)
                };
                for (a = 0; u[a]; a++) {
                    s = u[a];
                    if (r && H.nodeName(s, "script") && (!s.type || Nt.test(s.type)))
                        r.push(s.parentNode ? s.parentNode.removeChild(s) : s);
                    else {
                        if (s.nodeType === 1) {
                            var w = H.grep(s.getElementsByTagName("script"), i);
                            u.splice.apply(u, [a + 1, 0].concat(w))
                        }
                        n.appendChild(s)
                    }
                }
            }
            return u
        }, cleanData: function(e) {
            var t, n, r = H.cache, i = H.event.special, s = H.support.deleteExpando;
            for (var o = 0, u; (u = e[o]) != null; o++) {
                if (u.nodeName && H.noData[u.nodeName.toLowerCase()])
                    continue;
                n = u[H.expando];
                if (n) {
                    t = r[n];
                    if (t && t.events) {
                        for (var a in t.events)
                            i[a] ? H.event.remove(u, a) : H.removeEvent(u, a, t.handle);
                        t.handle && (t.handle.elem = null)
                    }
                    s ? delete u[H.expando] : u.removeAttribute && u.removeAttribute(H.expando), delete r[n]
                }
            }
        }});
    var At = /alpha\([^)]*\)/i, Ot = /opacity=([^)]*)/, Mt = /([A-Z]|^ms)/g, _t = /^[\-+]?(?:\d*\.)?\d+$/i, Dt = /^-?(?:\d*\.)?\d+(?!px)[^\d\s]+$/i, Pt = /^([\-+])=([\-+.\de]+)/, Ht = /^margin/, Bt = {position: "absolute", visibility: "hidden", display: "block"}, jt = ["Top", "Right", "Bottom", "Left"], Ft, It, qt;
    H.fn.css = function(e, n) {
        return H.access(this, function(e, n, r) {
            return r !== t ? H.style(e, n, r) : H.css(e, n)
        }, e, n, arguments.length > 1)
    }, H.extend({cssHooks: {opacity: {get: function(e, t) {
                    if (t) {
                        var n = Ft(e, "opacity");
                        return n === "" ? "1" : n
                    }
                    return e.style.opacity
                }}}, cssNumber: {fillOpacity: !0, fontWeight: !0, lineHeight: !0, opacity: !0, orphans: !0, widows: !0, zIndex: !0, zoom: !0}, cssProps: {"float": H.support.cssFloat ? "cssFloat" : "styleFloat"}, style: function(e, n, r, i) {
            if (!!e && e.nodeType !== 3 && e.nodeType !== 8 && !!e.style) {
                var s, o, u = H.camelCase(n), a = e.style, f = H.cssHooks[u];
                n = H.cssProps[u] || u;
                if (r === t)
                    return f && "get"in f && (s = f.get(e, !1, i)) !== t ? s : a[n];
                o = typeof r, o === "string" && (s = Pt.exec(r)) && (r = +(s[1] + 1) * +s[2] + parseFloat(H.css(e, n)), o = "number");
                if (r == null || o === "number" && isNaN(r))
                    return;
                o === "number" && !H.cssNumber[u] && (r += "px");
                if (!f || !("set"in f) || (r = f.set(e, r)) !== t)
                    try {
                        a[n] = r
                    } catch (l) {
                    }
            }
        }, css: function(e, n, r) {
            var i, s;
            n = H.camelCase(n), s = H.cssHooks[n], n = H.cssProps[n] || n, n === "cssFloat" && (n = "float");
            if (s && "get"in s && (i = s.get(e, !0, r)) !== t)
                return i;
            if (Ft)
                return Ft(e, n)
        }, swap: function(e, t, n) {
            var r = {}, i, s;
            for (s in t)
                r[s] = e.style[s], e.style[s] = t[s];
            i = n.call(e);
            for (s in t)
                e.style[s] = r[s];
            return i
        }}), H.curCSS = H.css, _.defaultView && _.defaultView.getComputedStyle && (It = function(e, t) {
        var n, r, i, s, o = e.style;
        return t = t.replace(Mt, "-$1").toLowerCase(), (r = e.ownerDocument.defaultView) && (i = r.getComputedStyle(e, null)) && (n = i.getPropertyValue(t), n === "" && !H.contains(e.ownerDocument.documentElement, e) && (n = H.style(e, t))), !H.support.pixelMargin && i && Ht.test(t) && Dt.test(n) && (s = o.width, o.width = n, n = i.width, o.width = s), n
    }), _.documentElement.currentStyle && (qt = function(e, t) {
        var n, r, i, s = e.currentStyle && e.currentStyle[t], o = e.style;
        return s == null && o && (i = o[t]) && (s = i), Dt.test(s) && (n = o.left, r = e.runtimeStyle && e.runtimeStyle.left, r && (e.runtimeStyle.left = e.currentStyle.left), o.left = t === "fontSize" ? "1em" : s, s = o.pixelLeft + "px", o.left = n, r && (e.runtimeStyle.left = r)), s === "" ? "auto" : s
    }), Ft = It || qt, H.each(["height", "width"], function(e, t) {
        H.cssHooks[t] = {get: function(e, n, r) {
                if (n)
                    return e.offsetWidth !== 0 ? v(e, t, r) : H.swap(e, Bt, function() {
                        return v(e, t, r)
                    })
            }, set: function(e, t) {
                return _t.test(t) ? t + "px" : t
            }}
    }), H.support.opacity || (H.cssHooks.opacity = {get: function(e, t) {
            return Ot.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? parseFloat(RegExp.$1) / 100 + "" : t ? "1" : ""
        }, set: function(e, t) {
            var n = e.style, r = e.currentStyle, i = H.isNumeric(t) ? "alpha(opacity=" + t * 100 + ")" : "", s = r && r.filter || n.filter || "";
            n.zoom = 1;
            if (t >= 1 && H.trim(s.replace(At, "")) === "") {
                n.removeAttribute("filter");
                if (r && !r.filter)
                    return
            }
            n.filter = At.test(s) ? s.replace(At, i) : s + " " + i
        }}), H(function() {
        H.support.reliableMarginRight || (H.cssHooks.marginRight = {get: function(e, t) {
                return H.swap(e, {display: "inline-block"}, function() {
                    return t ? Ft(e, "margin-right") : e.style.marginRight
                })
            }})
    }), H.expr && H.expr.filters && (H.expr.filters.hidden = function(e) {
        var t = e.offsetWidth, n = e.offsetHeight;
        return t === 0 && n === 0 || !H.support.reliableHiddenOffsets && (e.style && e.style.display || H.css(e, "display")) === "none"
    }, H.expr.filters.visible = function(e) {
        return!H.expr.filters.hidden(e)
    }), H.each({margin: "", padding: "", border: "Width"}, function(e, t) {
        H.cssHooks[e + t] = {expand: function(n) {
                var r, i = typeof n == "string" ? n.split(" ") : [n], s = {};
                for (r = 0; r < 4; r++)
                    s[e + jt[r] + t] = i[r] || i[r - 2] || i[0];
                return s
            }}
    });
    var Rt = /%20/g, Ut = /\[\]$/, zt = /\r?\n/g, Wt = /#.*$/, Xt = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg, Vt = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i, $t = /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/, Jt = /^(?:GET|HEAD)$/, Kt = /^\/\//, Qt = /\?/, Gt = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi, Yt = /^(?:select|textarea)/i, Zt = /\s+/, en = /([?&])_=[^&]*/, tn = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+))?)?/, nn = H.fn.load, rn = {}, sn = {}, on, un, an = ["*/"] + ["*"];
    try {
        on = P.href
    } catch (fn) {
        on = _.createElement("a"), on.href = "", on = on.href
    }
    un = tn.exec(on.toLowerCase()) || [], H.fn.extend({load: function(e, n, r) {
            if (typeof e != "string" && nn)
                return nn.apply(this, arguments);
            if (!this.length)
                return this;
            var i = e.indexOf(" ");
            if (i >= 0) {
                var s = e.slice(i, e.length);
                e = e.slice(0, i)
            }
            var o = "GET";
            n && (H.isFunction(n) ? (r = n, n = t) : typeof n == "object" && (n = H.param(n, H.ajaxSettings.traditional), o = "POST"));
            var u = this;
            return H.ajax({url: e, type: o, dataType: "html", data: n, complete: function(e, t, n) {
                    n = e.responseText, e.isResolved() && (e.done(function(e) {
                        n = e
                    }), u.html(s ? H("<div>").append(n.replace(Gt, "")).find(s) : n)), r && u.each(r, [n, t, e])
                }}), this
        }, serialize: function() {
            return H.param(this.serializeArray())
        }, serializeArray: function() {
            return this.map(function() {
                return this.elements ? H.makeArray(this.elements) : this
            }).filter(function() {
                return this.name && !this.disabled && (this.checked || Yt.test(this.nodeName) || Vt.test(this.type))
            }).map(function(e, t) {
                var n = H(this).val();
                return n == null ? null : H.isArray(n) ? H.map(n, function(e, n) {
                    return{name: t.name, value: e.replace(zt, "\r\n")}
                }) : {name: t.name, value: n.replace(zt, "\r\n")}
            }).get()
        }}), H.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "), function(e, t) {
        H.fn[t] = function(e) {
            return this.on(t, e)
        }
    }), H.each(["get", "post"], function(e, n) {
        H[n] = function(e, r, i, s) {
            return H.isFunction(r) && (s = s || i, i = r, r = t), H.ajax({type: n, url: e, data: r, success: i, dataType: s})
        }
    }), H.extend({getScript: function(e, n) {
            return H.get(e, t, n, "script")
        }, getJSON: function(e, t, n) {
            return H.get(e, t, n, "json")
        }, ajaxSetup: function(e, t) {
            return t ? h(e, H.ajaxSettings) : (t = e, e = H.ajaxSettings), h(e, t), e
        }, ajaxSettings: {url: on, isLocal: $t.test(un[1]), global: !0, type: "GET", contentType: "application/x-www-form-urlencoded; charset=UTF-8", processData: !0, async: !0, accepts: {xml: "application/xml, text/xml", html: "text/html", text: "text/plain", json: "application/json, text/javascript", "*": an}, contents: {xml: /xml/, html: /html/, json: /json/}, responseFields: {xml: "responseXML", text: "responseText"}, converters: {"* text": e.String, "text html": !0, "text json": H.parseJSON, "text xml": H.parseXML}, flatOptions: {context: !0, url: !0}}, ajaxPrefilter: d(rn), ajaxTransport: d(sn), ajax: function(e, n) {
            function r(e, n, r, p) {
                if (E !== 2) {
                    E = 2, b && clearTimeout(b), y = t, m = p || "", T.readyState = e > 0 ? 4 : 0;
                    var d, v, g, w = n, x = r ? l(i, T, r) : t, N, C;
                    if (e >= 200 && e < 300 || e === 304) {
                        if (i.ifModified) {
                            if (N = T.getResponseHeader("Last-Modified"))
                                H.lastModified[h] = N;
                            if (C = T.getResponseHeader("Etag"))
                                H.etag[h] = C
                        }
                        if (e === 304)
                            w = "notmodified", d = !0;
                        else
                            try {
                                v = f(i, x), w = "success", d = !0
                            } catch (k) {
                                w = "parsererror", g = k
                            }
                    } else {
                        g = w;
                        if (!w || e)
                            w = "error", e < 0 && (e = 0)
                    }
                    T.status = e, T.statusText = "" + (n || w), d ? u.resolveWith(s, [v, w, T]) : u.rejectWith(s, [T, w, g]), T.statusCode(c), c = t, S && o.trigger("ajax" + (d ? "Success" : "Error"), [T, i, d ? v : g]), a.fireWith(s, [T, w]), S && (o.trigger("ajaxComplete", [T, i]), --H.active || H.event.trigger("ajaxStop"))
                }
            }
            typeof e == "object" && (n = e, e = t), n = n || {};
            var i = H.ajaxSetup({}, n), s = i.context || i, o = s !== i && (s.nodeType || s instanceof H) ? H(s) : H.event, u = H.Deferred(), a = H.Callbacks("once memory"), c = i.statusCode || {}, h, d = {}, v = {}, m, g, y, b, w, E = 0, S, x, T = {readyState: 0, setRequestHeader: function(e, t) {
                    if (!E) {
                        var n = e.toLowerCase();
                        e = v[n] = v[n] || e, d[e] = t
                    }
                    return this
                }, getAllResponseHeaders: function() {
                    return E === 2 ? m : null
                }, getResponseHeader: function(e) {
                    var n;
                    if (E === 2) {
                        if (!g) {
                            g = {};
                            while (n = Xt.exec(m))
                                g[n[1].toLowerCase()] = n[2]
                        }
                        n = g[e.toLowerCase()]
                    }
                    return n === t ? null : n
                }, overrideMimeType: function(e) {
                    return E || (i.mimeType = e), this
                }, abort: function(e) {
                    return e = e || "abort", y && y.abort(e), r(0, e), this
                }};
            u.promise(T), T.success = T.done, T.error = T.fail, T.complete = a.add, T.statusCode = function(e) {
                if (e) {
                    var t;
                    if (E < 2)
                        for (t in e)
                            c[t] = [c[t], e[t]];
                    else
                        t = e[T.status], T.then(t, t)
                }
                return this
            }, i.url = ((e || i.url) + "").replace(Wt, "").replace(Kt, un[1] + "//"), i.dataTypes = H.trim(i.dataType || "*").toLowerCase().split(Zt), i.crossDomain == null && (w = tn.exec(i.url.toLowerCase()), i.crossDomain = !(!w || w[1] == un[1] && w[2] == un[2] && (w[3] || (w[1] === "http:" ? 80 : 443)) == (un[3] || (un[1] === "http:" ? 80 : 443)))), i.data && i.processData && typeof i.data != "string" && (i.data = H.param(i.data, i.traditional)), p(rn, i, n, T);
            if (E === 2)
                return!1;
            S = i.global, i.type = i.type.toUpperCase(), i.hasContent = !Jt.test(i.type), S && H.active++ === 0 && H.event.trigger("ajaxStart");
            if (!i.hasContent) {
                i.data && (i.url += (Qt.test(i.url) ? "&" : "?") + i.data, delete i.data), h = i.url;
                if (i.cache === !1) {
                    var N = H.now(), C = i.url.replace(en, "$1_=" + N);
                    i.url = C + (C === i.url ? (Qt.test(i.url) ? "&" : "?") + "_=" + N : "")
                }
            }
            (i.data && i.hasContent && i.contentType !== !1 || n.contentType) && T.setRequestHeader("Content-Type", i.contentType), i.ifModified && (h = h || i.url, H.lastModified[h] && T.setRequestHeader("If-Modified-Since", H.lastModified[h]), H.etag[h] && T.setRequestHeader("If-None-Match", H.etag[h])), T.setRequestHeader("Accept", i.dataTypes[0] && i.accepts[i.dataTypes[0]] ? i.accepts[i.dataTypes[0]] + (i.dataTypes[0] !== "*" ? ", " + an + "; q=0.01" : "") : i.accepts["*"]);
            for (x in i.headers)
                T.setRequestHeader(x, i.headers[x]);
            if (!i.beforeSend || i.beforeSend.call(s, T, i) !== !1 && E !== 2) {
                for (x in{success:1, error:1, complete:1})
                    T[x](i[x]);
                y = p(sn, i, n, T);
                if (!y)
                    r(-1, "No Transport");
                else {
                    T.readyState = 1, S && o.trigger("ajaxSend", [T, i]), i.async && i.timeout > 0 && (b = setTimeout(function() {
                        T.abort("timeout")
                    }, i.timeout));
                    try {
                        E = 1, y.send(d, r)
                    } catch (k) {
                        if (!(E < 2))
                            throw k;
                        r(-1, k)
                    }
                }
                return T
            }
            return T.abort(), !1
        }, param: function(e, n) {
            var r = [], i = function(e, t) {
                t = H.isFunction(t) ? t() : t, r[r.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
            };
            n === t && (n = H.ajaxSettings.traditional);
            if (H.isArray(e) || e.jquery && !H.isPlainObject(e))
                H.each(e, function() {
                    i(this.name, this.value)
                });
            else
                for (var s in e)
                    c(s, e[s], n, i);
            return r.join("&").replace(Rt, "+")
        }}), H.extend({active: 0, lastModified: {}, etag: {}});
    var ln = H.now(), cn = /(\=)\?(&|$)|\?\?/i;
    H.ajaxSetup({jsonp: "callback", jsonpCallback: function() {
            return H.expando + "_" + ln++
        }}), H.ajaxPrefilter("json jsonp", function(t, n, r) {
        var i = typeof t.data == "string" && /^application\/x\-www\-form\-urlencoded/.test(t.contentType);
        if (t.dataTypes[0] === "jsonp" || t.jsonp !== !1 && (cn.test(t.url) || i && cn.test(t.data))) {
            var s, o = t.jsonpCallback = H.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, u = e[o], a = t.url, f = t.data, l = "$1" + o + "$2";
            return t.jsonp !== !1 && (a = a.replace(cn, l), t.url === a && (i && (f = f.replace(cn, l)), t.data === f && (a += (/\?/.test(a) ? "&" : "?") + t.jsonp + "=" + o))), t.url = a, t.data = f, e[o] = function(e) {
                s = [e]
            }, r.always(function() {
                e[o] = u, s && H.isFunction(u) && e[o](s[0])
            }), t.converters["script json"] = function() {
                return s || H.error(o + " was not called"), s[0]
            }, t.dataTypes[0] = "json", "script"
        }
    }), H.ajaxSetup({accepts: {script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"}, contents: {script: /javascript|ecmascript/}, converters: {"text script": function(e) {
                return H.globalEval(e), e
            }}}), H.ajaxPrefilter("script", function(e) {
        e.cache === t && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1)
    }), H.ajaxTransport("script", function(e) {
        if (e.crossDomain) {
            var n, r = _.head || _.getElementsByTagName("head")[0] || _.documentElement;
            return{send: function(i, s) {
                    n = _.createElement("script"), n.async = "async", e.scriptCharset && (n.charset = e.scriptCharset), n.src = e.url, n.onload = n.onreadystatechange = function(e, i) {
                        if (i || !n.readyState || /loaded|complete/.test(n.readyState))
                            n.onload = n.onreadystatechange = null, r && n.parentNode && r.removeChild(n), n = t, i || s(200, "success")
                    }, r.insertBefore(n, r.firstChild)
                }, abort: function() {
                    n && n.onload(0, 1)
                }}
        }
    });
    var hn = e.ActiveXObject ? function() {
        for (var e in dn)
            dn[e](0, 1)
    } : !1, pn = 0, dn;
    H.ajaxSettings.xhr = e.ActiveXObject ? function() {
        return!this.isLocal && a() || u()
    } : a, function(e) {
        H.extend(H.support, {ajax: !!e, cors: !!e && "withCredentials"in e})
    }(H.ajaxSettings.xhr()), H.support.ajax && H.ajaxTransport(function(n) {
        if (!n.crossDomain || H.support.cors) {
            var r;
            return{send: function(i, s) {
                    var o = n.xhr(), u, a;
                    n.username ? o.open(n.type, n.url, n.async, n.username, n.password) : o.open(n.type, n.url, n.async);
                    if (n.xhrFields)
                        for (a in n.xhrFields)
                            o[a] = n.xhrFields[a];
                    n.mimeType && o.overrideMimeType && o.overrideMimeType(n.mimeType), !n.crossDomain && !i["X-Requested-With"] && (i["X-Requested-With"] = "XMLHttpRequest");
                    try {
                        for (a in i)
                            o.setRequestHeader(a, i[a])
                    } catch (f) {
                    }
                    o.send(n.hasContent && n.data || null), r = function(e, i) {
                        var a, f, l, c, h;
                        try {
                            if (r && (i || o.readyState === 4)) {
                                r = t, u && (o.onreadystatechange = H.noop, hn && delete dn[u]);
                                if (i)
                                    o.readyState !== 4 && o.abort();
                                else {
                                    a = o.status, l = o.getAllResponseHeaders(), c = {}, h = o.responseXML, h && h.documentElement && (c.xml = h);
                                    try {
                                        c.text = o.responseText
                                    } catch (e) {
                                    }
                                    try {
                                        f = o.statusText
                                    } catch (p) {
                                        f = ""
                                    }
                                    !a && n.isLocal && !n.crossDomain ? a = c.text ? 200 : 404 : a === 1223 && (a = 204)
                                }
                            }
                        } catch (d) {
                            i || s(-1, d)
                        }
                        c && s(a, f, c, l)
                    }, !n.async || o.readyState === 4 ? r() : (u = ++pn, hn && (dn || (dn = {}, H(e).unload(hn)), dn[u] = r), o.onreadystatechange = r)
                }, abort: function() {
                    r && r(0, 1)
                }}
        }
    });
    var vn = {}, mn, gn, yn = /^(?:toggle|show|hide)$/, bn = /^([+\-]=)?([\d+.\-]+)([a-z%]*)$/i, wn, En = [["height", "marginTop", "marginBottom", "paddingTop", "paddingBottom"], ["width", "marginLeft", "marginRight", "paddingLeft", "paddingRight"], ["opacity"]], Sn;
    H.fn.extend({show: function(e, t, n) {
            var s, o;
            if (e || e === 0)
                return this.animate(i("show", 3), e, t, n);
            for (var u = 0, a = this.length; u < a; u++)
                s = this[u], s.style && (o = s.style.display, !H._data(s, "olddisplay") && o === "none" && (o = s.style.display = ""), (o === "" && H.css(s, "display") === "none" || !H.contains(s.ownerDocument.documentElement, s)) && H._data(s, "olddisplay", r(s.nodeName)));
            for (u = 0; u < a; u++) {
                s = this[u];
                if (s.style) {
                    o = s.style.display;
                    if (o === "" || o === "none")
                        s.style.display = H._data(s, "olddisplay") || ""
                }
            }
            return this
        }, hide: function(e, t, n) {
            if (e || e === 0)
                return this.animate(i("hide", 3), e, t, n);
            var r, s, o = 0, u = this.length;
            for (; o < u; o++)
                r = this[o], r.style && (s = H.css(r, "display"), s !== "none" && !H._data(r, "olddisplay") && H._data(r, "olddisplay", s));
            for (o = 0; o < u; o++)
                this[o].style && (this[o].style.display = "none");
            return this
        }, _toggle: H.fn.toggle, toggle: function(e, t, n) {
            var r = typeof e == "boolean";
            return H.isFunction(e) && H.isFunction(t) ? this._toggle.apply(this, arguments) : e == null || r ? this.each(function() {
                var t = r ? e : H(this).is(":hidden");
                H(this)[t ? "show" : "hide"]()
            }) : this.animate(i("toggle", 3), e, t, n), this
        }, fadeTo: function(e, t, n, r) {
            return this.filter(":hidden").css("opacity", 0).show().end().animate({opacity: t}, e, n, r)
        }, animate: function(e, t, n, i) {
            function s() {
                o.queue === !1 && H._mark(this);
                var t = H.extend({}, o), n = this.nodeType === 1, i = n && H(this).is(":hidden"), s, u, a, f, l, c, h, p, d, v, m;
                t.animatedProperties = {};
                for (a in e) {
                    s = H.camelCase(a), a !== s && (e[s] = e[a], delete e[a]);
                    if ((l = H.cssHooks[s]) && "expand"in l) {
                        c = l.expand(e[s]), delete e[s];
                        for (a in c)
                            a in e || (e[a] = c[a])
                    }
                }
                for (s in e) {
                    u = e[s], H.isArray(u) ? (t.animatedProperties[s] = u[1], u = e[s] = u[0]) : t.animatedProperties[s] = t.specialEasing && t.specialEasing[s] || t.easing || "swing";
                    if (u === "hide" && i || u === "show" && !i)
                        return t.complete.call(this);
                    n && (s === "height" || s === "width") && (t.overflow = [this.style.overflow, this.style.overflowX, this.style.overflowY], H.css(this, "display") === "inline" && H.css(this, "float") === "none" && (!H.support.inlineBlockNeedsLayout || r(this.nodeName) === "inline" ? this.style.display = "inline-block" : this.style.zoom = 1))
                }
                t.overflow != null && (this.style.overflow = "hidden");
                for (a in e)
                    f = new H.fx(this, t, a), u = e[a], yn.test(u) ? (m = H._data(this, "toggle" + a) || (u === "toggle" ? i ? "show" : "hide" : 0), m ? (H._data(this, "toggle" + a, m === "show" ? "hide" : "show"), f[m]()) : f[u]()) : (h = bn.exec(u), p = f.cur(), h ? (d = parseFloat(h[2]), v = h[3] || (H.cssNumber[a] ? "" : "px"), v !== "px" && (H.style(this, a, (d || 1) + v), p = (d || 1) / f.cur() * p, H.style(this, a, p + v)), h[1] && (d = (h[1] === "-=" ? -1 : 1) * d + p), f.custom(p, d, v)) : f.custom(p, u, ""));
                return!0
            }
            var o = H.speed(t, n, i);
            return H.isEmptyObject(e) ? this.each(o.complete, [!1]) : (e = H.extend({}, e), o.queue === !1 ? this.each(s) : this.queue(o.queue, s))
        }, stop: function(e, n, r) {
            return typeof e != "string" && (r = n, n = e, e = t), n && e !== !1 && this.queue(e || "fx", []), this.each(function() {
                function t(e, t, n) {
                    var i = t[n];
                    H.removeData(e, n, !0), i.stop(r)
                }
                var n, i = !1, s = H.timers, o = H._data(this);
                r || H._unmark(!0, this);
                if (e == null)
                    for (n in o)
                        o[n] && o[n].stop && n.indexOf(".run") === n.length - 4 && t(this, o, n);
                else
                    o[n = e + ".run"] && o[n].stop && t(this, o, n);
                for (n = s.length; n--; )
                    s[n].elem === this && (e == null || s[n].queue === e) && (r ? s[n](!0) : s[n].saveState(), i = !0, s.splice(n, 1));
                (!r || !i) && H.dequeue(this, e)
            })
        }}), H.each({slideDown: i("show", 1), slideUp: i("hide", 1), slideToggle: i("toggle", 1), fadeIn: {opacity: "show"}, fadeOut: {opacity: "hide"}, fadeToggle: {opacity: "toggle"}}, function(e, t) {
        H.fn[e] = function(e, n, r) {
            return this.animate(t, e, n, r)
        }
    }), H.extend({speed: function(e, t, n) {
            var r = e && typeof e == "object" ? H.extend({}, e) : {complete: n || !n && t || H.isFunction(e) && e, duration: e, easing: n && t || t && !H.isFunction(t) && t};
            r.duration = H.fx.off ? 0 : typeof r.duration == "number" ? r.duration : r.duration in H.fx.speeds ? H.fx.speeds[r.duration] : H.fx.speeds._default;
            if (r.queue == null || r.queue === !0)
                r.queue = "fx";
            return r.old = r.complete, r.complete = function(e) {
                H.isFunction(r.old) && r.old.call(this), r.queue ? H.dequeue(this, r.queue) : e !== !1 && H._unmark(this)
            }, r
        }, easing: {linear: function(e) {
                return e
            }, swing: function(e) {
                return-Math.cos(e * Math.PI) / 2 + .5
            }}, timers: [], fx: function(e, t, n) {
            this.options = t, this.elem = e, this.prop = n, t.orig = t.orig || {}
        }}), H.fx.prototype = {update: function() {
            this.options.step && this.options.step.call(this.elem, this.now, this), (H.fx.step[this.prop] || H.fx.step._default)(this)
        }, cur: function() {
            if (this.elem[this.prop] == null || !!this.elem.style && this.elem.style[this.prop] != null) {
                var e, t = H.css(this.elem, this.prop);
                return isNaN(e = parseFloat(t)) ? !t || t === "auto" ? 0 : t : e
            }
            return this.elem[this.prop]
        }, custom: function(e, n, r) {
            function i(e) {
                return s.step(e)
            }
            var s = this, u = H.fx;
            this.startTime = Sn || o(), this.end = n, this.now = this.start = e, this.pos = this.state = 0, this.unit = r || this.unit || (H.cssNumber[this.prop] ? "" : "px"), i.queue = this.options.queue, i.elem = this.elem, i.saveState = function() {
                H._data(s.elem, "fxshow" + s.prop) === t && (s.options.hide ? H._data(s.elem, "fxshow" + s.prop, s.start) : s.options.show && H._data(s.elem, "fxshow" + s.prop, s.end))
            }, i() && H.timers.push(i) && !wn && (wn = setInterval(u.tick, u.interval))
        }, show: function() {
            var e = H._data(this.elem, "fxshow" + this.prop);
            this.options.orig[this.prop] = e || H.style(this.elem, this.prop), this.options.show = !0, e !== t ? this.custom(this.cur(), e) : this.custom(this.prop === "width" || this.prop === "height" ? 1 : 0, this.cur()), H(this.elem).show()
        }, hide: function() {
            this.options.orig[this.prop] = H._data(this.elem, "fxshow" + this.prop) || H.style(this.elem, this.prop), this.options.hide = !0, this.custom(this.cur(), 0)
        }, step: function(e) {
            var t, n, r, i = Sn || o(), s = !0, u = this.elem, a = this.options;
            if (e || i >= a.duration + this.startTime) {
                this.now = this.end, this.pos = this.state = 1, this.update(), a.animatedProperties[this.prop] = !0;
                for (t in a.animatedProperties)
                    a.animatedProperties[t] !== !0 && (s = !1);
                if (s) {
                    a.overflow != null && !H.support.shrinkWrapBlocks && H.each(["", "X", "Y"], function(e, t) {
                        u.style["overflow" + t] = a.overflow[e]
                    }), a.hide && H(u).hide();
                    if (a.hide || a.show)
                        for (t in a.animatedProperties)
                            H.style(u, t, a.orig[t]), H.removeData(u, "fxshow" + t, !0), H.removeData(u, "toggle" + t, !0);
                    r = a.complete, r && (a.complete = !1, r.call(u))
                }
                return!1
            }
            return a.duration == Infinity ? this.now = i : (n = i - this.startTime, this.state = n / a.duration, this.pos = H.easing[a.animatedProperties[this.prop]](this.state, n, 0, 1, a.duration), this.now = this.start + (this.end - this.start) * this.pos), this.update(), !0
        }}, H.extend(H.fx, {tick: function() {
            var e, t = H.timers, n = 0;
            for (; n < t.length; n++)
                e = t[n], !e() && t[n] === e && t.splice(n--, 1);
            t.length || H.fx.stop()
        }, interval: 13, stop: function() {
            clearInterval(wn), wn = null
        }, speeds: {slow: 600, fast: 200, _default: 400}, step: {opacity: function(e) {
                H.style(e.elem, "opacity", e.now)
            }, _default: function(e) {
                e.elem.style && e.elem.style[e.prop] != null ? e.elem.style[e.prop] = e.now + e.unit : e.elem[e.prop] = e.now
            }}}), H.each(En.concat.apply([], En), function(e, t) {
        t.indexOf("margin") && (H.fx.step[t] = function(e) {
            H.style(e.elem, t, Math.max(0, e.now) + e.unit)
        })
    }), H.expr && H.expr.filters && (H.expr.filters.animated = function(e) {
        return H.grep(H.timers, function(t) {
            return e === t.elem
        }).length
    });
    var xn, Tn = /^t(?:able|d|h)$/i, Nn = /^(?:body|html)$/i;
    "getBoundingClientRect"in _.documentElement ? xn = function(e, t, r, i) {
        try {
            i = e.getBoundingClientRect()
        } catch (s) {
        }
        if (!i || !H.contains(r, e))
            return i ? {top: i.top, left: i.left} : {top: 0, left: 0};
        var o = t.body, u = n(t), a = r.clientTop || o.clientTop || 0, f = r.clientLeft || o.clientLeft || 0, l = u.pageYOffset || H.support.boxModel && r.scrollTop || o.scrollTop, c = u.pageXOffset || H.support.boxModel && r.scrollLeft || o.scrollLeft, h = i.top + l - a, p = i.left + c - f;
        return{top: h, left: p}
    } : xn = function(e, t, n) {
        var r, i = e.offsetParent, s = e, o = t.body, u = t.defaultView, a = u ? u.getComputedStyle(e, null) : e.currentStyle, f = e.offsetTop, l = e.offsetLeft;
        while ((e = e.parentNode) && e !== o && e !== n) {
            if (H.support.fixedPosition && a.position === "fixed")
                break;
            r = u ? u.getComputedStyle(e, null) : e.currentStyle, f -= e.scrollTop, l -= e.scrollLeft, e === i && (f += e.offsetTop, l += e.offsetLeft, H.support.doesNotAddBorder && (!H.support.doesAddBorderForTableAndCells || !Tn.test(e.nodeName)) && (f += parseFloat(r.borderTopWidth) || 0, l += parseFloat(r.borderLeftWidth) || 0), s = i, i = e.offsetParent), H.support.subtractsBorderForOverflowNotVisible && r.overflow !== "visible" && (f += parseFloat(r.borderTopWidth) || 0, l += parseFloat(r.borderLeftWidth) || 0), a = r
        }
        if (a.position === "relative" || a.position === "static")
            f += o.offsetTop, l += o.offsetLeft;
        return H.support.fixedPosition && a.position === "fixed" && (f += Math.max(n.scrollTop, o.scrollTop), l += Math.max(n.scrollLeft, o.scrollLeft)), {top: f, left: l}
    }, H.fn.offset = function(e) {
        if (arguments.length)
            return e === t ? this : this.each(function(t) {
                H.offset.setOffset(this, e, t)
            });
        var n = this[0], r = n && n.ownerDocument;
        return r ? n === r.body ? H.offset.bodyOffset(n) : xn(n, r, r.documentElement) : null
    }, H.offset = {bodyOffset: function(e) {
            var t = e.offsetTop, n = e.offsetLeft;
            return H.support.doesNotIncludeMarginInBodyOffset && (t += parseFloat(H.css(e, "marginTop")) || 0, n += parseFloat(H.css(e, "marginLeft")) || 0), {top: t, left: n}
        }, setOffset: function(e, t, n) {
            var r = H.css(e, "position");
            r === "static" && (e.style.position = "relative");
            var i = H(e), s = i.offset(), o = H.css(e, "top"), u = H.css(e, "left"), a = (r === "absolute" || r === "fixed") && H.inArray("auto", [o, u]) > -1, f = {}, l = {}, c, h;
            a ? (l = i.position(), c = l.top, h = l.left) : (c = parseFloat(o) || 0, h = parseFloat(u) || 0), H.isFunction(t) && (t = t.call(e, n, s)), t.top != null && (f.top = t.top - s.top + c), t.left != null && (f.left = t.left - s.left + h), "using"in t ? t.using.call(e, f) : i.css(f)
        }}, H.fn.extend({position: function() {
            if (!this[0])
                return null;
            var e = this[0], t = this.offsetParent(), n = this.offset(), r = Nn.test(t[0].nodeName) ? {top: 0, left: 0} : t.offset();
            return n.top -= parseFloat(H.css(e, "marginTop")) || 0, n.left -= parseFloat(H.css(e, "marginLeft")) || 0, r.top += parseFloat(H.css(t[0], "borderTopWidth")) || 0, r.left += parseFloat(H.css(t[0], "borderLeftWidth")) || 0, {top: n.top - r.top, left: n.left - r.left}
        }, offsetParent: function() {
            return this.map(function() {
                var e = this.offsetParent || _.body;
                while (e && !Nn.test(e.nodeName) && H.css(e, "position") === "static")
                    e = e.offsetParent;
                return e
            })
        }}), H.each({scrollLeft: "pageXOffset", scrollTop: "pageYOffset"}, function(e, r) {
        var i = /Y/.test(r);
        H.fn[e] = function(s) {
            return H.access(this, function(e, s, o) {
                var u = n(e);
                if (o === t)
                    return u ? r in u ? u[r] : H.support.boxModel && u.document.documentElement[s] || u.document.body[s] : e[s];
                u ? u.scrollTo(i ? H(u).scrollLeft() : o, i ? o : H(u).scrollTop()) : e[s] = o
            }, e, s, arguments.length, null)
        }
    }), H.each({Height: "height", Width: "width"}, function(e, n) {
        var r = "client" + e, i = "scroll" + e, s = "offset" + e;
        H.fn["inner" + e] = function() {
            var e = this[0];
            return e ? e.style ? parseFloat(H.css(e, n, "padding")) : this[n]() : null
        }, H.fn["outer" + e] = function(e) {
            var t = this[0];
            return t ? t.style ? parseFloat(H.css(t, n, e ? "margin" : "border")) : this[n]() : null
        }, H.fn[n] = function(e) {
            return H.access(this, function(e, n, o) {
                var u, a, f, l;
                if (H.isWindow(e))
                    return u = e.document, a = u.documentElement[r], H.support.boxModel && a || u.body && u.body[r] || a;
                if (e.nodeType === 9)
                    return u = e.documentElement, u[r] >= u[i] ? u[r] : Math.max(e.body[i], u[i], e.body[s], u[s]);
                if (o === t)
                    return f = H.css(e, n), l = parseFloat(f), H.isNumeric(l) ? l : f;
                H(e).css(n, o)
            }, n, e, arguments.length, null)
        }
    }), e.jQuery = e.$ = H, typeof define == "function" && define.amd && define.amd.jQuery && define("jquery", [], function() {
        return H
    })
})(window), function(e, t) {
    var n;
    e.rails = n = {linkClickSelector: "a[data-confirm], a[data-method], a[data-remote], a[data-disable-with]", inputChangeSelector: "select[data-remote], input[data-remote], textarea[data-remote]", formSubmitSelector: "form", formInputClickSelector: "form input[type=submit], form input[type=image], form button[type=submit], form button:not(button[type])", disableSelector: "input[data-disable-with], button[data-disable-with], textarea[data-disable-with]", enableSelector: "input[data-disable-with]:disabled, button[data-disable-with]:disabled, textarea[data-disable-with]:disabled", requiredInputSelector: "input[name][required]:not([disabled]),textarea[name][required]:not([disabled])", fileInputSelector: "input:file", linkDisableSelector: "a[data-disable-with]", CSRFProtection: function(t) {

            var n = e('meta[name="csrf-token"]').attr("content");
            n && t.setRequestHeader("X-CSRF-Token", n)
        }, fire: function(t, n, r) {
            var i = e.Event(n);
            return t.trigger(i, r), i.result !== !1
        }, confirm: function(e) {
            return confirm(e)
        }, ajax: function(t) {
            return e.ajax(t)
        }, href: function(e) {
            return e.attr("href")
        }, handleRemote: function(r) {
            var i, s, o, u, a, f;
            if (n.fire(r, "ajax:before")) {
                u = r.data("cross-domain") || null, a = r.data("type") || e.ajaxSettings && e.ajaxSettings.dataType;
                if (r.is("form")) {
                    i = r.attr("method"), s = r.attr("action"), o = r.serializeArray();
                    var l = r.data("ujs:submit-button");
                    l && (o.push(l), r.data("ujs:submit-button", null))
                } else
                    r.is(n.inputChangeSelector) ? (i = r.data("method"), s = r.data("url"), o = r.serialize(), r.data("params") && (o = o + "&" + r.data("params"))) : (i = r.data("method"), s = n.href(r), o = r.data("params") || null);
                return f = {type: i || "GET", data: o, dataType: a, crossDomain: u, beforeSend: function(e, i) {
                        return i.dataType === t && e.setRequestHeader("accept", "*/*;q=0.5, " + i.accepts.script), n.fire(r, "ajax:beforeSend", [e, i])
                    }, success: function(e, t, n) {
                        r.trigger("ajax:success", [e, t, n])
                    }, complete: function(e, t) {
                        r.trigger("ajax:complete", [e, t])
                    }, error: function(e, t, n) {
                        r.trigger("ajax:error", [e, t, n])
                    }}, s && (f.url = s), n.ajax(f)
            }
            return!1
        }, handleMethod: function(r) {
            var i = n.href(r), s = r.data("method"), o = r.attr("target"), u = e("meta[name=csrf-token]").attr("content"), a = e("meta[name=csrf-param]").attr("content"), f = e('<form method="post" action="' + i + '"></form>'), l = '<input name="_method" value="' + s + '" type="hidden" />';
            a !== t && u !== t && (l += '<input name="' + a + '" value="' + u + '" type="hidden" />'), o && f.attr("target", o), f.hide().append(l).appendTo("body"), f.submit()
        }, disableFormElements: function(t) {
            t.find(n.disableSelector).each(function() {
                var t = e(this), n = t.is("button") ? "html" : "val";
                t.data("ujs:enable-with", t[n]()), t[n](t.data("disable-with")), t.prop("disabled", !0)
            })
        }, enableFormElements: function(t) {
            t.find(n.enableSelector).each(function() {
                var t = e(this), n = t.is("button") ? "html" : "val";
                t.data("ujs:enable-with") && t[n](t.data("ujs:enable-with")), t.prop("disabled", !1)
            })
        }, allowAction: function(e) {
            var t = e.data("confirm"), r = !1, i;
            return t ? (n.fire(e, "confirm") && (r = n.confirm(t), i = n.fire(e, "confirm:complete", [r])), r && i) : !0
        }, blankInputs: function(t, n, r) {
            var i = e(), s, o = n || "input,textarea";
            return t.find(o).each(function() {
                s = e(this);
                if (r ? s.val() : !s.val())
                    i = i.add(s)
            }), i.length ? i : !1
        }, nonBlankInputs: function(e, t) {
            return n.blankInputs(e, t, !0)
        }, stopEverything: function(t) {
            return e(t.target).trigger("ujs:everythingStopped"), t.stopImmediatePropagation(), !1
        }, callFormSubmitBindings
                : function(n, r) {
            var i = n.data("events"), s = !0;
            return i !== t && i.submit !== t && e.each(i.submit, function(e, t) {
                if (typeof t.handler == "function")
                    return s = t.handler(r)
            }), s
        }, disableElement: function(e) {
            e.data("ujs:enable-with", e.html()), e.html(e.data("disable-with")), e.bind("click.railsDisable", function(e) {
                return n.stopEverything(e)
            })
        }, enableElement: function(e) {
            e.data("ujs:enable-with") !== t && (e.html(e.data("ujs:enable-with")), e.data("ujs:enable-with", !1)), e.unbind("click.railsDisable")
        }}, e.ajaxPrefilter(function(e, t, r) {
        e.crossDomain || n.CSRFProtection(r)
    }), e(document).delegate(n.linkDisableSelector, "ajax:complete", function() {
        n.enableElement(e(this))
    }), e(document).delegate(n.linkClickSelector, "click.rails", function(r) {
        var i = e(this), s = i.data("method"), o = i.data("params");
        if (!n.allowAction(i))
            return n.stopEverything(r);
        i.is(n.linkDisableSelector) && n.disableElement(i);
        if (i.data("remote") !== t)
            return(r.metaKey || r.ctrlKey) && (!s || s === "GET") && !o ? !0 : (n.handleRemote(i) === !1 && n.enableElement(i), !1);
        if (i.data("method"))
            return n.handleMethod(i), !1
    }), e(document).delegate(n.inputChangeSelector, "change.rails", function(t) {
        var r = e(this);
        return n.allowAction(r) ? (n.handleRemote(r), !1) : n.stopEverything(t)
    }), e(document).delegate(n.formSubmitSelector, "submit.rails", function(r) {
        var i = e(this), s = i.data("remote") !== t, o = n.blankInputs(i, n.requiredInputSelector), u = n.nonBlankInputs(i, n.fileInputSelector);
        if (!n.allowAction(i))
            return n.stopEverything(r);
        if (o && i.attr("novalidate") == t && n.fire(i, "ajax:aborted:required", [o]))
            return n.stopEverything(r);
        if (s)
            return u ? n.fire(i, "ajax:aborted:file", [u]) : !e.support.submitBubbles && e().jquery < "1.7" && n.callFormSubmitBindings(i, r) === !1 ? n.stopEverything(r) : (n.handleRemote(i), !1);
        setTimeout(function() {
            n.disableFormElements(i)
        }, 13)
    }), e(document).delegate(n.formInputClickSelector, "click.rails", function(t) {
        var r = e(this);
        if (!n.allowAction(r))
            return n.stopEverything(t);
        var i = r.attr("name"), s = i ? {name: i, value: r.val()} : null;
        r.closest("form").data("ujs:submit-button", s)
    }), e(document).delegate(n.formSubmitSelector, "ajax:beforeSend.rails", function(t) {
        this == t.target && n.disableFormElements(e(this))
    }), e(document).delegate(n.formSubmitSelector, "ajax:complete.rails", function(t) {
        this == t.target && n.enableFormElements(e(this))
    })
}(jQuery), function(e) {
    function t() {
        if (!e.fn.ajaxSubmit.debug)
            return;
        var t = "[jquery.form] " + Array.prototype.join.call(arguments, "");
        window.console && window.console.log ? window.console.log(t) : window.opera && window.opera.postError && window.opera.postError(t)
    }
    e.fn.ajaxSubmit = function(n) {
        function S(t) {
            var r = new FormData;
            for (var i = 0; i < t.length; i++) {
                if (t[i].type == "file")
                    continue;
                r.append(t[i].name, t[i].value)
            }
            o.find("input:file:enabled").each(function() {
                var t = e(this).attr("name"), n = this.files;
                if (t)
                    for (var i = 0; i < n.length; i++)
                        r.append(t, n[i])
            });
            if (n.extraData)
                for (var s in n.extraData)
                    r.append(s, n.extraData[s]);
            n.data = null;
            var u = e.extend(!0, {}, e.ajaxSettings, n, {contentType: !1, processData: !1, cache: !1, type: "POST"});
            u.data = null;
            var a = u.beforeSend;
            u.beforeSend = function(e, t) {
                t.data = r, e.upload && (e.upload.onprogress = function(e) {
                    t.progress(e.position, e.total)
                }), a && a.call(t, e, n)
            }, e.ajax(u)
        }
        function x(i) {
            function S(e) {
                var t = e.contentWindow ? e.contentWindow.document : e.contentDocument ? e.contentDocument : e.document;
                return t
            }
            function N() {
                function u() {
                    try {
                        var e = S(p).readyState;
                        t("state = " + e), e.toLowerCase() == "uninitialized" && setTimeout(u, 50)
                    } catch (n) {
                        t("Server abort: ", n, " (", n.name, ")"), O(E), y && clearTimeout(y), y = undefined
                    }
                }
                var n = o.attr("target"), i = o.attr("action");
                s.setAttribute("target", c), r || s.setAttribute("method", "POST"), i != f.url && s.setAttribute("action", f.url), !f.skipEncodingOverride && (!r || /post/i.test(r)) && o.attr({encoding: "multipart/form-data", enctype: "multipart/form-data"}), f.timeout && (y = setTimeout(function() {
                    g = !0, O(w)
                }, f.timeout));
                var a = [];
                try {
                    if (f.extraData)
                        for (var l in f.extraData)
                            a.push(e('<input type="hidden" name="' + l + '">').attr("value", f.extraData[l]).appendTo(s)[0]);
                    f.iframeTarget || (h.appendTo("body"), p.attachEvent ? p.attachEvent("onload", O) : p.addEventListener("load", O, !1)), setTimeout(u, 15), s.submit()
                } finally {
                    s.setAttribute("action", i), n ? s.setAttribute("target", n) : o.removeAttr("target"), e(a).remove()
                }
            }
            function O(n) {
                if (d.aborted || A)
                    return;
                try {
                    k = S(p)
                } catch (r) {
                    t("cannot access response document: ", r), n = E
                }
                if (n === w && d) {
                    d.abort("timeout");
                    return
                }
                if (n == E && d) {
                    d.abort("server abort");
                    return
                }
                if (!k || k.location.href == f.iframeSrc)
                    if (!g)
                        return;
                p.detachEvent ? p.detachEvent("onload", O) : p.removeEventListener("load", O, !1);
                var i = "success", s;
                try {
                    if (g)
                        throw"timeout";
                    var o = f.dataType == "xml" || k.XMLDocument || e.isXMLDoc(k);
                    t("isXml=" + o);
                    if (!o && window.opera && (k.body == null || k.body.innerHTML == "") && --L) {
                        t("requeing onLoad callback, DOM not available"), setTimeout(O, 250);
                        return
                    }
                    var u = k.body ? k.body : k.documentElement;
                    d.responseText = u ? u.innerHTML : null, d.responseXML = k.XMLDocument ? k.XMLDocument : k, o && (f.dataType = "xml"), d.getResponseHeader = function(e) {
                        var t = {"content-type": f.dataType};
                        return t[e]
                    }, u && (d.status = Number(u.getAttribute("status")) || d.status, d.statusText = u.getAttribute("statusText") || d.statusText);
                    var a = (f.dataType || "").toLowerCase(), c = /(json|script|text)/.test(a);
                    if (c || f.textarea) {
                        var v = k.getElementsByTagName("textarea")[0];
                        if (v)
                            d.responseText = v.value, d.status = Number(v.getAttribute("status")) || d.status, d.statusText = v.getAttribute("statusText") || d.statusText;
                        else if (c) {
                            var m = k.getElementsByTagName("pre")[0], b = k.getElementsByTagName("body")[0];
                            m ? d.responseText = m.textContent ? m.textContent : m.innerText : b && (d.responseText = b.textContent ? b.textContent : b.innerText)
                        }
                    } else
                        a == "xml" && !d.responseXML && d.responseText != null && (d.responseXML = M(d.responseText));
                    try {
                        C = D(d, a, f)
                    } catch (n) {
                        i = "parsererror", d.error = s = n || i
                    }
                } catch (n) {
                    t("error caught: ", n), i = "error", d.error = s = n || i
                }
                d.aborted && (t("upload aborted"), i = null), d.status && (i = d.status >= 200 && d.status < 300 || d.status === 304 ? "success" : "error"), i === "success" ? (f.success && f.success.call(f.context, C, "success", d), l && e.event.trigger("ajaxSuccess", [d, f])) : i && (s == undefined && (s = d.statusText), f.error && f.error.call(f.context, d, i, s), l && e.event.trigger("ajaxError", [d, f, s])), l && e.event.trigger("ajaxComplete", [d, f]), l && !--e.active && e.event.trigger("ajaxStop"), f.complete && f.complete.call(f.context, d, i), A = !0, f.timeout && clearTimeout(y), setTimeout(function() {
                    f.iframeTarget || h.remove(), d.responseXML = null
                }, 100)
            }
            var s = o[0], u, a, f, l, c, h, p, d, v, m, g, y, b = !!e.fn.prop;
            if (i)
                if (b)
                    for (a = 0; a < i.length; a++)
                        u = e(s[i[a].name]), u.prop("disabled", !1);
                else
                    for (a = 0; a < i.length; a++)
                        u = e(s[i[a].name]), u.removeAttr("disabled");
            if (e(":input[name=submit],:input[id=submit]", s).length) {
                alert('Error: Form elements must not have name or id of "submit".');
                return
            }
            f = e.extend(!0, {}, e.ajaxSettings, n), f.context = f.context || f, c = "jqFormIO" + (new Date).getTime(), f.iframeTarget ? (h = e(f.iframeTarget), m = h.attr("name"), m == null ? h.attr("name", c) : c = m) : (h = e('<iframe name="' + c + '" src="' + f.iframeSrc + '" />'), h.css({position: "absolute", top: "-1000px", left: "-1000px"})), p = h[0], d = {aborted: 0, responseText: null, responseXML: null, status: 0, statusText: "n/a", getAllResponseHeaders: function() {
                }, getResponseHeader: function() {
                }, setRequestHeader: function() {
                }, abort: function(n) {
                    var r = n === "timeout" ? "timeout" : "aborted";
                    t("aborting upload... " + r), this.aborted = 1, h.attr("src", f.iframeSrc), d.error = r, f.error && f.error.call(f.context, d, r, n), l && e.event.trigger("ajaxError", [d, f, r]), f.complete && f.complete.call(f.context, d, r)
                }}, l = f.global, l && !(e.active++) && e.event.trigger("ajaxStart"), l && e.event.trigger("ajaxSend", [d, f]);
            if (f.beforeSend && f.beforeSend.call(f.context, d, f) === !1) {
                f.global && e.active--;
                return
            }
            if (d.aborted)
                return;
            v = s.clk, v && (m = v.name, m && !v.disabled && (f.extraData = f.extraData || {}, f.extraData[m] = v.value, v.type == "image" && (f.extraData[m + ".x"] = s.clk_x, f.extraData[m + ".y"] = s.clk_y)));
            var w = 1, E = 2, x = e("meta[name=csrf-token]").attr("content"), T = e("meta[name=csrf-param]").attr("content");
            T && x && (f.extraData = f.extraData || {}, f.extraData[T] = x), f.forceSync ? N() : setTimeout(N, 10);
            var C, k, L = 50, A, M = e.parseXML || function(e, t) {
                return window.ActiveXObject ? (t = new ActiveXObject("Microsoft.XMLDOM"), t.async = "false", t.loadXML(e)) : t = (new DOMParser).parseFromString(e, "text/xml"), t && t.documentElement && t.documentElement.nodeName != "parsererror" ? t : null
            }, _ = e.parseJSON || function(e) {
                return window.eval("(" + e + ")")
            }, D = function(t, n, r) {
                var i = t.getResponseHeader("content-type") || "", s = n === "xml" || !n && i.indexOf("xml") >= 0, o = s ? t.responseXML : t.responseText;
                return s && o.documentElement.nodeName === "parsererror" && e.error && e.error("parsererror"), r && r.dataFilter && (o = r.dataFilter(o, n)), typeof o == "string" && (n === "json" || !n && i.indexOf("json") >= 0 ? o = _(o) : (n === "script" || !n && i.indexOf("javascript") >= 0) && e.globalEval(o)), o
            }
        }
        if (!this.length)
            return t("ajaxSubmit: skipping submit process - no element selected"), this;
        var r, i, s, o = this;
        typeof n == "function" && (n = {success: n}), r = this.attr("method"), i = this.attr("action"), s = typeof i == "string" ? e.trim(i) : "", s = s || window.location.href || "", s && (s = (s.match(/^([^#]+)/) || [])[1]), n = e.extend(!0, {url: s, success: e.ajaxSettings.success, type: r || "GET", iframeSrc: /^https/i.test(window.location.href || "") ? "javascript:false" : "about:blank"}, n);
        var u = {};
        this.trigger("form-pre-serialize", [this, n, u]);
        if (u.veto)
            return t("ajaxSubmit: submit vetoed via form-pre-serialize trigger"), this;
        if (n.beforeSerialize && n.beforeSerialize(this, n) === !1)
            return t("ajaxSubmit: submit aborted via beforeSerialize callback"), this;
        var a = n.traditional;
        a === undefined && (a = e.ajaxSettings.traditional);
        var f, l, c, h = this.formToArray(n.semantic);
        n.data && (n.extraData = n.data, f = e.param(n.data, a));
        if (n.beforeSubmit && n.beforeSubmit(h, this, n) === !1)
            return t("ajaxSubmit: submit aborted via beforeSubmit callback"), this;
        this.trigger("form-submit-validate", [h, this, n, u]);
        if (u.veto)
            return t("ajaxSubmit: submit vetoed via form-submit-validate trigger"), this;
        var p = e.param(h, a);
        f && (p = p ? p + "&" + f : f), n.type.toUpperCase() == "GET" ? (n.url += (n.url.indexOf("?") >= 0 ? "&" : "?") + p, n.data = null) : n.data = p;
        var d = [];
        n.resetForm && d.push(function() {
            o.resetForm()
        }), n.clearForm && d.push(function() {
            o.clearForm(n.includeHidden)
        });
        if (!n.dataType && n.target) {
            var v = n.success || function() {
            };
            d.push(function(t) {
                var r = n.replaceTarget ? "replaceWith" : "html";
                e(n.target)[r](t).each(v, arguments)
            })
        } else
            n.success && d.push(n.success);
        n.success = function(e, t, r) {
            var i = n.context || n;
            for (var s = 0, u = d.length; s < u; s++)
                d[s].apply(i, [e, t, r || o, o])
        };
        var m = e("input:file:enabled[value]", this), g = m.length > 0, y = "multipart/form-data", b = o.attr("enctype") == y || o.attr("encoding") == y, w = !!(g && m.get(0).files && window.FormData);
        t("fileAPI :" + w);
        var E = (g || b) && !w;
        return n.iframe !== !1 && (n.iframe || E) ? n.closeKeepAlive ? e.get(n.closeKeepAlive, function() {
            x(h)
        }) : x(h) : (g || b) && w ? (n.progress = n.progress || e.noop, S(h)) : e.ajax(n), this.trigger("form-submit-notify", [this, n]), this
    }, e.fn.ajaxForm = function(n) {
        if (this.length === 0) {
            var r = {s: this.selector, c: this.context};
            return!e.isReady && r.s ? (t("DOM not ready, queuing ajaxForm"), e(function() {
                e(r.s, r.c).ajaxForm(n)
            }), this) : (t("terminating; zero elements found by selector" + (e.isReady ? "" : " (DOM not ready)")), this)
        }
        return this.ajaxFormUnbind().bind("submit.form-plugin", function(t) {
            t.isDefaultPrevented() || (t.preventDefault(), e(this).ajaxSubmit(n))
        }).bind("click.form-plugin", function(t) {
            var n = t.target, r = e(n);
            if (!r.is(":submit,input:image")) {
                var i = r.closest(":submit");
                if (i.length == 0)
                    return;
                n = i[0]
            }
            var s = this;
            s.clk = n;
            if (n.type == "image")
                if (t.offsetX != undefined)
                    s.clk_x = t.offsetX, s.clk_y = t.offsetY;
                else if (typeof e.fn.offset == "function") {
                    var o = r.offset();
                    s.clk_x = t.pageX - o.left, s.clk_y = t.pageY - o.top
                } else
                    s.clk_x = t.pageX - n.offsetLeft, s.clk_y = t.pageY - n.offsetTop;
            setTimeout(function() {
                s.clk = s.clk_x = s.clk_y = null
            }, 100)
        })
    }, e.fn.ajaxFormUnbind = function() {
        return this.unbind("submit.form-plugin click.form-plugin")
    }, e.fn.formToArray = function(t) {
        var n = [];
        if (this.length === 0)
            return n;
        var r = this[0], i = t ? r.getElementsByTagName("*") : r.elements;
        if (!i)
            return n;
        var s, o, u, a, f, l, c;
        for (s = 0, l = i.length; s < l; s++) {
            f = i[s], u = f.name;
            if (!u)
                continue;
            if (t && r.clk && f.type == "image") {
                !f.disabled && r.clk == f && (n.push({name: u, value: e(f).val(), type: f.type}), n.push({name: u + ".x", value: r.clk_x}, {name: u + ".y", value: r.clk_y}));
                continue
            }
            a = e.fieldValue(f, !0);
            if (a && a.constructor == Array)
                for (o = 0, c = a.length; o < c; o++)
                    n.push({name: u, value: a[o]});
            else
                a !== null && typeof a != "undefined" && n.push({name: u, value: a, type: f.type})
        }
        if (!t && r.clk) {
            var h = e(r.clk), p = h[0];
            u = p.name, u && !p.disabled && p.type == "image" && (n.push({name: u, value: h.val()}), n.push({name: u + ".x", value: r.clk_x}, {name: u + ".y", value: r.clk_y}))
        }
        return n
    }, e.fn.formSerialize = function(t) {
        return e.param(this.formToArray(t))
    }, e.fn.fieldSerialize = function(t) {
        var n = [];
        return this.each(function() {
            var r = this.name;
            if (!r)
                return;
            var i = e.fieldValue(this, t);
            if (i && i.constructor == Array)
                for (var s = 0, o = i.length; s < o; s++)
                    n.push({name: r, value: i[s]});
            else
                i !== null && typeof i != "undefined" && n.push({name: this.name, value: i})
        }), e.param(n)
    }, e.fn.fieldValue = function(t) {
        for (var n = [], r = 0, i = this.length; r < i; r++) {
            var s = this[r], o = e.fieldValue(s, t);
            if (o === null || typeof o == "undefined" || o.constructor == Array && !o.length)
                continue;
            o.constructor == Array ? e.merge(n, o) : n.push(o)
        }
        return n
    }, e.fieldValue = function(t, n) {
        var r = t.name, i = t.type, s = t.tagName.toLowerCase();
        n === undefined && (n = !0);
        if (n && (!r || t.disabled || i == "reset" || i == "button" || (i == "checkbox" || i == "radio") && !t.checked || (i == "submit" || i == "image") && t.form && t.form.clk != t || s == "select" && t.selectedIndex == -1))
            return null;
        if (s == "select") {
            var o = t.selectedIndex;
            if (o < 0)
                return null;
            var u = [], a = t.options, f = i == "select-one", l = f ? o + 1 : a.length;
            for (var c = f ? o : 0; c < l; c++) {
                var h = a[c];
                if (h.selected) {
                    var p = h.value;
                    p || (p = h.attributes && h.attributes.value && !h.attributes.value.specified ? h.text : h.value);
                    if (f)
                        return p;
                    u.push(p)
                }
            }
            return u
        }
        return e(t).val()
    }, e.fn.clearForm = function(t) {
        return this.each(function() {
            e("input,select,textarea", this).clearFields(t)
        })
    }, e.fn.clearFields = e.fn.clearInputs = function(e) {
        var t = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;
        return this.each(function() {
            var n = this.type, r = this.tagName.toLowerCase();
            t.test(n) || r == "textarea" || e && /hidden/.test(n) ? this.value = "" : n == "checkbox" || n == "radio" ? this.checked = !1 : r == "select" && (this.selectedIndex = -1)
        })
    }, e.fn.resetForm = function() {
        return this.each(function() {
            (typeof this.reset == "function" || typeof this.reset == "object" && !this.reset.nodeType) && this.reset()
        })
    }, e.fn.enable = function(e) {
        return e === undefined && (e = !0), this.each(function() {
            this.disabled = !e
        })
    }, e.fn.selected = function(t) {
        return t === undefined && (t = !0), this.each(function() {
            var n = this.type;
            if (n == "checkbox" || n == "radio")
                this.checked = t;
            else if (this.tagName.toLowerCase() == "option") {
                var r = e(this).parent("select");
                t && r[0] && r[0].type == "select-one" && r.find("option").selected(!1), this.selected = t
            }
        })
    }, e.fn.ajaxSubmit.debug = !1
}(jQuery), function(e) {
    e.fn.uploadProgress = function(t) {
        return t = e.extend({dataType: "json", interval: 2e3, progressBar: "#progressbar", progressUrl: "/progress", start: function() {
            }, uploading: function() {
            }, complete: function() {
            }, success: function() {
            }, error: function() {
            }, preloadImages: [], uploadProgressPath: "/javascripts/jquery.uploadProgress.js", jqueryPath: "/javascripts/jquery.js", timer: ""}, t), e(function() {
            for (var n = 0; n < t.preloadImages.length; n++)
                t.preloadImages[n] = e("<img>").attr("src", t.preloadImages[n]);
            if (e.browser.safari && top.document == document) {
                iframe = document.createElement("iframe"), iframe.name = "progressFrame", e(iframe).css({width: "0", height: "0", position: "absolute", top: "-3000px"}), document.body.appendChild(iframe);
                var r = iframe.contentWindow.document;
                r.open(), r.write("<html><head></head><body></body></html>"), r.close();
                var i = r.body, s = r.createElement("script");
                s.src = t.jqueryPath, s.onload = function() {
                    var e = r.createElement("script");
                    e.src = t.uploadProgressPath, i.appendChild(e)
                }, i.appendChild(s)
            }
        }), this.each(function() {
            e(this).bind("submit", function() {
                var n = "";
                for (i = 0; i < 32; i++)
                    n += Math.floor(Math.random() * 16).toString(16);
                t.uuid = n, t.start();
                if (old_id = /X-Progress-ID=([^&]+)/.exec(e(this).attr("action"))) {
                    var r = e(this).attr("action").replace(old_id[1], n);
                    e(this).attr("action", r)
                } else
                    e(this).attr("action", jQuery(this).attr("action") + (jQuery(this).attr("action").indexOf("?") == -1 ? "?" : "&") + "X-Progress-ID=" + n);
                var s = e.browser.safari ? progressFrame.jQuery.uploadProgress : jQuery.uploadProgress;
                t.timer = window.setInterval(function() {
                    s(this, t)
                }, t.interval)
            })
        })
    }, jQuery.uploadProgress = function(t, n) {
        jQuery.ajax({type: "GET", url: n.progressUrl + "?X-Progress-ID=" + n.uuid, dataType: n.dataType, success: function(t) {
                if (t.state == "uploading") {
                    t.percents = Math.floor(t.received / t.size * 1e3) / 10;
                    var r = e.browser.safari ? e(n.progressBar, parent.document) : e(n.progressBar);
                    r.css({width: t.percents + "%"}), n.uploading(t)
                }
                if (t.state == "done" || t.state == "error")
                    window.clearTimeout(n.timer), n.complete(t);
                t.state == "done" && n.success(t), t.state == "error" && n.error(t)
            }})
    }
}(jQuery), window.Modernizr = function(e, t, n) {
    function r(e) {
        g.cssText = e
    }
    function i(e, t) {
        return r(E.join(e + ";") + (t || ""))
    }
    function s(e, t) {
        return typeof e === t
    }
    function o(e, t) {
        return!!~("" + e).indexOf(t)
    }
    function u(e, t) {
        for (var r in e) {
            var i = e[r];
            if (!o(i, "-") && g[i] !== n)
                return t == "pfx" ? i : !0
        }
        return!1
    }
    function a(e, t, r) {
        for (var i in e) {
            var o = t[e[i]];
            if (o !== n)
                return r === !1 ? e[i] : s(o, "function") ? o.bind(r || t) : o
        }
        return!1
    }
    function f(e, t, n) {
        var r = e.charAt(0).toUpperCase() + e.slice(1), i = (e + " " + x.join(r + " ") + r).split(" ");
        return s(t, "string") || s(t, "undefined") ? u(i, t) : (i = (e + " " + T.join(r + " ") + r).split(" "), a(i, t, n))
    }
    function l() {
        h.input = function(n) {
            for (var r = 0, i = n.length; r < i; r++)
                L[n[r]] = n[r]in y;
            return L.list && (L.list = !!t.createElement("datalist") && !!e.HTMLDataListElement), L
        }("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")), h.inputtypes = function(e) {
            for (var r = 0, i, s, o, u = e.length; r < u; r++)
                y.setAttribute("type", s = e[r]), i = y.type !== "text", i && (y.value = b, y.style.cssText = "position:absolute;visibility:hidden;", /^range$/.test(s) && y.style.WebkitAppearance !== n ? (d.appendChild(y), o = t.defaultView, i = o.getComputedStyle && o.getComputedStyle(y, null).WebkitAppearance !== "textfield" && y.offsetHeight !== 0, d.removeChild(y)) : /^(search|tel)$/.test(s) || (/^(url|email)$/.test(s) ? i = y.checkValidity && y.checkValidity() === !1 : i = y.value != b)), k[e[r]] = !!i;
            return k
        }("search tel url email datetime date month week time datetime-local number range color".split(" "))
    }
    var c = "2.6.1", h = {}, p = !0, d = t.documentElement, v = "modernizr", m = t.createElement(v), g = m.style, y = t.createElement("input"), b = ":)", w = {}.toString, E = " -webkit- -moz- -o- -ms- ".split(" "), S = "Webkit Moz O ms", x = S.split(" "), T = S.toLowerCase().split(" "), N = {svg: "http://www.w3.org/2000/svg"}, C = {}, k = {}, L = {}, A = [], O = A.slice, M, _ = function(e, n, r, i) {
        var s, o, u, a = t.createElement("div"), f = t.body, l = f ? f : t.createElement("body");
        if (parseInt(r, 10))
            while (r--)
                u = t.createElement("div"), u.id = i ? i[r] : v + (r + 1), a.appendChild(u);
        return s = ["&#173;", '<style id="s', v, '">', e, "</style>"].join(""), a.id = v, (f ? a : l).innerHTML += s, l.appendChild(a), f || (l.style.background = "", d.appendChild(l)), o = n(a, e), f ? a.parentNode.removeChild(a) : l.parentNode.removeChild(l), !!o
    }, D = function(t) {
        var n = e.matchMedia || e.msMatchMedia;
        if (n)
            return n(t).matches;
        var r;
        return _("@media " + t + " { #" + v + " { position: absolute; } }", function(t) {
            r = (e.getComputedStyle ? getComputedStyle(t, null) : t.currentStyle)["position"] == "absolute"
        }), r
    }, P = function() {
        function e(e, i) {
            i = i || t.createElement(r[e] || "div"), e = "on" + e;
            var o = e in i;
            return o || (i.setAttribute || (i = t.createElement("div")), i.setAttribute && i.removeAttribute && (i.setAttribute(e, ""), o = s(i[e], "function"), s(i[e], "undefined") || (i[e] = n), i.removeAttribute(e))), i = null, o
        }
        var r = {select: "input", change: "input", submit: "form", reset: "form", error: "img", load: "img", abort: "img"};
        return e
    }(), H = {}.hasOwnProperty, B;
    !s(H, "undefined") && !s(H.call, "undefined") ? B = function(e, t) {
        return H.call(e, t)
    } : B = function(e, t) {
        return t in e && s(e.constructor.prototype[t], "undefined")
    }, Function.prototype.bind || (Function.prototype.bind = function(e) {
        var t = this;
        if (typeof t != "function")
            throw new TypeError;
        var n = O.call(arguments, 1), r = function() {
            if (this instanceof r) {
                var i = function() {
                };
                i.prototype = t.prototype;
                var s = new i, o = t.apply(s, n.concat(O.call(arguments)));
                return Object(o) === o ? o : s
            }
            return t.apply(e, n.concat(O.call(arguments)))
        };
        return r
    }), C.flexbox = function() {
        return f("flexWrap")
    }, C.canvas = function() {
        var e = t.createElement("canvas");
        return!!e.getContext && !!e.getContext("2d")
    }, C.canvastext = function() {
        return!!h.canvas && !!s(t.createElement("canvas").getContext("2d").fillText, "function")
    }, C.webgl = function() {
        return!!e.WebGLRenderingContext
    }, C.touch = function() {
        var n;
        return"ontouchstart"in e || e.DocumentTouch && t instanceof DocumentTouch ? n = !0 : _(["@media (", E.join("touch-enabled),("), v, ")", "{#modernizr{top:9px;position:absolute}}"].join(""), function(e) {
            n = e.offsetTop === 9
        }), n
    }, C.geolocation = function() {
        return"geolocation"in navigator
    }, C.postmessage = function() {
        return!!e.postMessage
    }, C.websqldatabase = function() {
        return!!e.openDatabase
    }, C.indexedDB = function() {
        return!!f("indexedDB", e)
    }, C.hashchange = function() {
        return P("hashchange", e) && (t.documentMode === n || t.documentMode > 7)
    }, C.history = function() {
        return!!e.history && !!history.pushState
    }, C.draganddrop = function() {
        var e = t.createElement("div");
        return"draggable"in e || "ondragstart"in e && "ondrop"in e
    }, C.websockets = function() {
        return"WebSocket"in e || "MozWebSocket"in e
    }, C.rgba = function() {
        return r("background-color:rgba(150,255,150,.5)"), o(g.backgroundColor, "rgba")
    }, C.hsla = function() {
        return r("background-color:hsla(120,40%,100%,.5)"), o(g.backgroundColor, "rgba") || o(g.backgroundColor, "hsla")
    }, C.multiplebgs = function() {
        return r("background:url(https://),url(https://),red url(https://)"), /(url\s*\(.*?){3}/.test(g.background)
    }, C.backgroundsize = function() {
        return f("backgroundSize")
    }, C.borderimage = function() {
        return f("borderImage")
    }, C.borderradius = function() {
        return f("borderRadius")
    }, C.boxshadow = function() {
        return f("boxShadow")
    }, C.textshadow = function() {
        return t.createElement("div").style.textShadow === ""
    }, C.opacity = function() {
        return i("opacity:.55"), /^0.55$/.test(g.opacity)
    }, C.cssanimations = function() {
        return f("animationName")
    }, C.csscolumns = function() {
        return f("columnCount")
    }, C.cssgradients = function() {
        var e = "background-image:", t = "gradient(linear,left top,right bottom,from(#9f9),to(white));", n = "linear-gradient(left top,#9f9, white);";
        return r((e + "-webkit- ".split(" ").join(t + e) + E.join(n + e)).slice(0, -e.length)), o(g.backgroundImage, "gradient")
    }, C.cssreflections = function() {
        return f("boxReflect")
    }, C.csstransforms = function() {
        return!!f("transform")
    }, C.csstransforms3d = function() {
        var e = !!f("perspective");
        return e && "webkitPerspective"in d.style && _("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}", function(t, n) {
            e = t.offsetLeft === 9 && t.offsetHeight === 3
        }), e
    }, C.csstransitions = function() {
        return f("transition")
    }, C.fontface = function() {
        var e;
        return _('@font-face {font-family:"font";src:url("https://")}', function(n, r) {
            var i = t.getElementById("smodernizr"), s = i.sheet || i.styleSheet, o = s ? s.cssRules && s.cssRules[0] ? s.cssRules[0].cssText : s.cssText || "" : "";
            e = /src/i.test(o) && o.indexOf(r.split(" ")[0]) === 0
        }), e
    }, C.generatedcontent = function() {
        var e;
        return _(['#modernizr:after{content:"', b, '";visibility:hidden}'].join(""), function(t) {
            e = t.offsetHeight >= 1
        }), e
    }, C.video = function() {
        var e = t.createElement("video"), n = !1;
        try {
            if (n = !!e.canPlayType)
                n = new Boolean(n), n.ogg = e.canPlayType('video/ogg; codecs="theora"').replace(/^no$/, ""), n.h264 = e.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/, ""), n.webm = e.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/, "")
        } catch (r) {
        }
        return n
    }, C.audio = function() {
        var e = t.createElement("audio"), n = !1;
        try {
            if (n = !!e.canPlayType)
                n = new Boolean(n), n.ogg = e.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ""), n.mp3 = e.canPlayType("audio/mpeg;").replace(/^no$/, ""), n.wav = e.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ""), n.m4a = (e.canPlayType("audio/x-m4a;") || e.canPlayType("audio/aac;")).replace(/^no$/, "")
        } catch (r) {
        }
        return n
    }, C.localstorage = function() {
        try {
            return localStorage.setItem(v, v), localStorage.removeItem(v), !0
        } catch (e) {
            return!1
        }
    }, C.sessionstorage = function() {
        try {
            return sessionStorage.setItem(v, v), sessionStorage.removeItem(v), !0
        } catch (e) {
            return!1
        }
    }, C.webworkers = function() {
        return!!e.Worker
    }, C.applicationcache = function() {
        return!!e.applicationCache
    }, C.svg = function() {
        return!!t.createElementNS && !!t.createElementNS(N.svg, "svg").createSVGRect
    }, C.inlinesvg = function() {
        var e = t.createElement("div");
        return e.innerHTML = "<svg/>", (e.firstChild && e.firstChild.namespaceURI) == N.svg
    }, C.smil = function() {
        return!!t.createElementNS && /SVGAnimate/.test(w.call(t.createElementNS(N.svg, "animate")))
    }, C.svgclippaths = function() {
        return!!t.createElementNS && /SVGClipPath/.test(w.call(t.createElementNS(N.svg, "clipPath")))
    };
    for (var j in C)
        B(C, j) && (M = j.toLowerCase(), h[M] = C[j](), A.push((h[M] ? "" : "no-") + M));
    return h.input || l(), h.addTest = function(e, t) {
        if (typeof e == "object")
            for (var r in e)
                B(e, r) && h.addTest(r, e[r]);
        else {
            e = e.toLowerCase();
            if (h[e] !== n)
                return h;
            t = typeof t == "function" ? t() : t, p && (d.className += " " + (t ? "" : "no-") + e), h[e] = t
        }
        return h
    }, r(""), m = y = null, function(e, t) {
        function n(e, t) {
            var n = e.createElement("p"), r = e.getElementsByTagName("head")[0] || e.documentElement;
            return n.innerHTML = "x<style>" + t + "</style>", r.insertBefore(n.lastChild, r.firstChild)
        }
        function r() {
            var e = g.elements;
            return typeof e == "string" ? e.split(" ") : e
        }
        function i(e) {
            var t = v[e[p]];
            return t || (t = {}, d++, e[p] = d, v[d] = t), t
        }
        function s(e, n, r) {
            n || (n = t);
            if (m)
                return n.createElement(e);
            r || (r = i(n));
            var s;
            return r.cache[e] ? s = r.cache[e].cloneNode() : c.test(e) ? s = (r.cache[e] = r.createElem(e)).cloneNode() : s = r.createElem(e), s.canHaveChildren && !l.test(e) ? r.frag.appendChild(s) : s
        }
        function o(e, n) {
            e || (e = t);
            if (m)
                return e.createDocumentFragment();
            n = n || i(e);
            var s = n.frag.cloneNode(), o = 0, u = r(), a = u.length;
            for (; o < a; o++)
                s.createElement(u[o]);
            return s
        }
        function u(e, t) {
            t.cache || (t.cache = {}, t.createElem = e.createElement, t.createFrag = e.createDocumentFragment, t.frag = t.createFrag()), e.createElement = function(n) {
                return g.shivMethods ? s(n, e, t) : t.createElem(n)
            }, e.createDocumentFragment = Function("h,f", "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" + r().join().replace(/\w+/g, function(e) {
                return t.createElem(e), t.frag.createElement(e), 'c("' + e + '")'
            }) + ");return n}")(g, t.frag)
        }
        function a(e) {
            e || (e = t);
            var r = i(e);
            return g.shivCSS && !h && !r.hasCSS && (r.hasCSS = !!n(e, "article,aside,figcaption,figure,footer,header,hgroup,nav,section{display:block}mark{background:#FF0;color:#000}")), m || u(e, r), e
        }
        var f = e.html5 || {}, l = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i, c = /^<|^(?:a|b|button|code|div|fieldset|form|h1|h2|h3|h4|h5|h6|i|iframe|img|input|label|li|link|ol|option|p|param|q|script|select|span|strong|style|table|tbody|td|textarea|tfoot|th|thead|tr|ul)$/i, h, p = "_html5shiv", d = 0, v = {}, m;
        (function() {
            try {
                var e = t.createElement("a");
                e.innerHTML = "<xyz></xyz>", h = "hidden"in e, m = e.childNodes.length == 1 || function() {
                    t.createElement("a");
                    var e = t.createDocumentFragment();
                    return typeof e.cloneNode == "undefined" || typeof e.createDocumentFragment == "undefined" || typeof e.createElement == "undefined"
                }()
            } catch (n) {
                h = !0, m = !0
            }
        })();
        var g = {elements: f.elements || "abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video", shivCSS: f.shivCSS !== !1, supportsUnknownElements: m, shivMethods: f.shivMethods !== !1, type: "default", shivDocument: a, createElement: s, createDocumentFragment: o};
        e.html5 = g, a(t)
    }(this, t), h._version = c, h._prefixes = E, h._domPrefixes = T, h._cssomPrefixes = x, h.mq = D, h.hasEvent = P, h.testProp = function(e) {
        return u([e])
    }, h.testAllProps = f, h.testStyles = _, h.prefixed = function(e, t, n) {
        return t ? f(e, t, n) : f(e, "pfx")
    }, d.className = d.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") + (p ? " js " + A.join(" ") : ""), h
}(this, this.document), function(e, t, n) {
    function r(e) {
        return d.call(e) == "[object Function]"
    }
    function i(e) {
        return typeof e == "string"
    }
    function s() {
    }
    function o(e) {
        return!e || e == "loaded" || e == "complete" || e == "uninitialized"
    }
    function u() {
        var e = v.shift();
        m = 1, e ? e.t ? h(function() {
            (e.t == "c" ? k.injectCss : k.injectJs)(e.s, 0, e.a, e.x, e.e, 1)
        }, 0) : (e(), u()) : m = 0
    }
    function a(e, n, r, i, s, a, f) {
        function l(t) {
            if (!d && o(c.readyState) && (w.r = d = 1, !m && u(), c.onload = c.onreadystatechange = null, t)) {
                e != "img" && h(function() {
                    b.removeChild(c)
                }, 50);
                for (var r in T[n])
                    T[n].hasOwnProperty(r) && T[n][r].onload()
            }
        }
        var f = f || k.errorTimeout, c = {}, d = 0, g = 0, w = {t: r, s: n, e: s, a: a, x: f};
        T[n] === 1 && (g = 1, T[n] = [], c = t.createElement(e)), e == "object" ? c.data = n : (c.src = n, c.type = e), c.width = c.height = "0", c.onerror = c.onload = c.onreadystatechange = function() {
            l.call(this, g)
        }, v.splice(i, 0, w), e != "img" && (g || T[n] === 2 ? (b.insertBefore(c, y ? null : p), h(l, f)) : T[n].push(c))
    }
    function f(e, t, n, r, s) {
        return m = 0, t = t || "j", i(e) ? a(t == "c" ? E : w, e, t, this.i++, n, r, s) : (v.splice(this.i++, 0, e), v.length == 1 && u()), this
    }
    function l() {
        var e = k;
        return e.loader = {load: f, i: 0}, e
    }
    var c = t.documentElement, h = e.setTimeout, p = t.getElementsByTagName("script")[0], d = {}.toString, v = [], m = 0, g = "MozAppearance"in c.style, y = g && !!t.createRange().compareNode, b = y ? c : p.parentNode, c = e.opera && d.call(e.opera) == "[object Opera]", c = !!t.attachEvent && !c, w = g ? "object" : c ? "script" : "img", E = c ? "script" : w, S = Array.isArray || function(e) {
        return d.call(e) == "[object Array]"
    }, x = [], T = {}, N = {timeout: function(e, t) {
            return t.length && (e.timeout = t[0]), e
        }}, C, k;
    k = function(e) {
        function t(e) {
            var e = e.split("!"), t = x.length, n = e.pop(), r = e.length, n = {url: n, origUrl: n, prefixes: e}, i, s, o;
            for (s = 0; s < r; s++)
                o = e[s].split("="), (i = N[o.shift()]) && (n = i(n, o));
            for (s = 0; s < t; s++)
                n = x[s](n);
            return n
        }
        function o(e, i, s, o, a) {
            var f = t(e), c = f.autoCallback;
            f.url.split(".").pop().split("?").shift(), f.bypass || (i && (i = r(i) ? i : i[e] || i[o] || i[e.split("/").pop().split("?")[0]] || u), f.instead ? f.instead(e, i, s, o, a) : (T[f.url] ? f.noexec = !0 : T[f.url] = 1, s.load(f.url, f.forceCSS || !f.forceJS && "css" == f.url.split(".").pop().split("?").shift() ? "c" : n, f.noexec, f.attrs, f.timeout), (r(i) || r(c)) && s.load(function() {
                l(), i && i(f.origUrl, a, o), c && c(f.origUrl, a, o), T[f.url] = 2
            })))
        }
        function a(e, t) {
            function n(e, n) {
                if (e) {
                    if (i(e))
                        n || (f = function() {
                            var e = [].slice.call(arguments);
                            l.apply(this, e), c()
                        }), o(e, f, t, 0, u);
                    else if (Object(e) === e)
                        for (p in h = function() {
                            var t = 0, n;
                            for (n in e)
                                e.hasOwnProperty(n) && t++;
                            return t
                        }(), e)
                            e.hasOwnProperty(p) && (!n && !--h && (r(f) ? f = function() {
                                var e = [].slice.call(arguments);
                                l.apply(this, e), c()
                            } : f[p] = function(e) {
                                return function() {
                                    var t = [].slice.call(arguments);
                                    e && e.apply(this, t), c()
                                }
                            }(l[p])), o(e[p], f, t, p, u))
                } else
                    !n && c()
            }
            var u = !!e.test, a = e.load || e.both, f = e.callback || s, l = f, c = e.complete || s, h, p;
            n(u ? e.yep : e.nope, !!a), a && n(a)
        }
        var f, c, h = this.yepnope.loader;
        if (i(e))
            o(e, 0, h, 0);
        else if (S(e))
            for (f = 0; f < e.length; f++)
                c = e[f], i(c) ? o(c, 0, h, 0) : S(c) ? k(c) : Object(c) === c && a(c, h);
        else
            Object(e) === e && a(e, h)
    }, k.addPrefix = function(e, t) {
        N[e] = t
    }, k.addFilter = function(e) {
        x.push(e)
    }, k.errorTimeout = 1e4, t.readyState == null && t.addEventListener && (t.readyState = "loading", t.addEventListener("DOMContentLoaded", C = function() {
        t.removeEventListener("DOMContentLoaded", C, 0), t.readyState = "complete"
    }, 0)), e.yepnope = l(), e.yepnope.executeStack = u, e.yepnope.injectJs = function(e, n, r, i, a, f) {
        var l = t.createElement("script"), c, d, i = i || k.errorTimeout;
        l.src = e;
        for (d in r)
            l.setAttribute(d, r[d]);
        n = f ? u : n || s, l.onreadystatechange = l.onload = function() {
            !c && o(l.readyState) && (c = 1, n(), l.onload = l.onreadystatechange = null)
        }, h(function() {
            c || (c = 1, n(1))
        }, i), a ? l.onload() : p.parentNode.insertBefore(l, p)
    }, e.yepnope.injectCss = function(e, n, r, i, o, a) {
        var i = t.createElement("link"), f, n = a ? u : n || s;
        i.href = e, i.rel = "stylesheet", i.type = "text/css";
        for (f in r)
            i.setAttribute(f, r[f]);
        o || (p.parentNode.insertBefore(i, p), h(n, 0))
    }
}(this, document), Modernizr.load = function() {
    yepnope.apply(window, [].slice.call(arguments, 0))
}, function(e) {
    function t(t, n) {
        function y() {
            return r.update(), w(), r
        }
        function b() {
            var e = p.toLowerCase();
            l.obj.css(h, d / u.ratio), o.obj.css(h, -d), m.start = l.obj.offset()[h], u.obj.css(e, f[n.axis]), f.obj.css(e, f[n.axis]), l.obj.css(e, l[n.axis])
        }
        function w() {
            g ? s.obj[0].ontouchstart = function(e) {
                1 === e.touches.length && (E(e.touches[0]), e.stopPropagation())
            } : (l.obj.bind("mousedown", E), f.obj.bind("mouseup", x)), n.scroll && window.addEventListener ? (i[0].addEventListener("DOMMouseScroll", S, !1), i[0].addEventListener("mousewheel", S, !1)) : n.scroll && (i[0].onmousewheel = S)
        }
        function E(t) {
            var n = parseInt(l.obj.css(h), 10);
            m.start = c ? t.pageX : t.pageY, v.start = n == "auto" ? 0 : n, g ? (document.ontouchmove = function(e) {
                e.preventDefault(), x(e.touches[0])
            }, document.ontouchend = T) : (e(document).bind("mousemove", x), e(document).bind("mouseup", T), l.obj.bind("mouseup", T))
        }
        function S(t) {
            if (o.ratio < 1) {
                var r = t || window.event, i = r.wheelDelta ? r.wheelDelta / 120 : -r.detail / 3;
                d -= i * n.wheel, d = Math.min(o[n.axis] - s[n.axis], Math.max(0, d)), l.obj.css(h, d / u.ratio), o.obj.css(h, -d);
                if (n.lockscroll || d !== o[n.axis] - s[n.axis] && d !== 0)
                    r = e.event.fix(r), r.preventDefault()
            }
        }
        function x(e) {
            o.ratio < 1 && (g ? v.now = Math.min(f[n.axis] - l[n.axis], Math.max(0, v.start + (m.start - (c ? e.pageX : e.pageY)))) : v.now = Math.min(f[n.axis] - l[n.axis], Math.max(0, v.start + ((c ? e.pageX : e.pageY) - m.start))), d = v.now * u.ratio, o.obj.css(h, -d), l.obj.css(h, v.now))
        }
        function T() {
            e(document).unbind("mousemove"
                    , x), e(document).unbind("mouseup", T), l.obj.unbind("mouseup", T), document.ontouchmove = document.ontouchend = null
        }
        var r = this, i = t, s = {obj: e(".viewport", t)}, o = {obj: e(".overview", t)}, u = {obj: e(".scrollbar", t)}, f = {obj: e(".track", u.obj)}, l = {obj: e(".thumb", u.obj)}, c = n.axis === "x", h = c ? "left" : "top", p = c ? "Width" : "Height", d = 0, v = {start: 0, now: 0}, m = {}, g = "ontouchstart"in document.documentElement ? !0 : !1;
        return this.update = function(e) {
            s[n.axis] = s.obj[0]["offset" + p], o[n.axis] = o.obj[0]["scroll" + p], o.ratio = s[n.axis] / o[n.axis], u.obj.toggleClass("disable", o.ratio >= 1), f[n.axis] = n.size === "auto" ? s[n.axis] : n.size, l[n.axis] = Math.min(f[n.axis], Math.max(0, n.sizethumb === "auto" ? f[n.axis] * o.ratio : n.sizethumb)), u.ratio = n.sizethumb === "auto" ? o[n.axis] / f[n.axis] : (o[n.axis] - s[n.axis]) / (f[n.axis] - l[n.axis]), d = e === "relative" && o.ratio <= 1 ? Math.min(o[n.axis] - s[n.axis], Math.max(0, d)) : 0, d = e === "bottom" && o.ratio <= 1 ? o[n.axis] - s[n.axis] : isNaN(parseInt(e, 10)) ? d : parseInt(e, 10), b()
        }, y()
    }
    e.tiny = e.tiny || {}, e.tiny.scrollbar = {options: {axis: "y", wheel: 40, scroll: !0, lockscroll: !0, size: "auto", sizethumb: "auto"}}, e.fn.tinyscrollbar = function(n) {
        var r = e.extend({}, e.tiny.scrollbar.options, n);
        return this.each(function() {
            e(this).data("tsb", new t(e(this), r))
        }), this
    }, e.fn.tinyscrollbar_update = function(t) {
        return e(this).data("tsb").update(t)
    }
}(jQuery), function(e, t) {
    var n = function(e) {
        return parseFloat(e) || 0
    }, r = Array.prototype.slice, i = e.jcarousel = {};
    i.version = "@VERSION";
    var s = /^([+\-]=)?(.+)$/;
    i.parseTarget = function(e) {
        var t = !1, n = "object" != typeof e ? s.exec(e) : null;
        return n ? (e = parseInt(n[2], 10) || 0, n[1] && (t = !0, "-=" === n[1] && (e *= -1))) : "object" != typeof e && (e = parseInt(e, 10) || 0), {target: e, relative: t}
    }, i.detectCarousel = function(e) {
        for (var t; 0 < e.size(); ) {
            t = e.filter("[data-jcarousel]");
            if (0 < t.size())
                return t;
            t = e.find("[data-jcarousel]");
            if (0 < t.size())
                return t;
            e = e.parent()
        }
        return null
    }, i.basePrototype = function(t) {
        return{version: i.version, _options: {}, _element: null, _init: e.noop, _create: e.noop, _destroy: e.noop, _reload: e.noop, create: function() {
                return this._element.attr("data-" + t.toLowerCase(), !0).data(t, this), !1 === this._trigger("create") ? this : (this._create(), this._trigger("createend"), this)
            }, destroy: function() {
                return!1 === this._trigger("destroy") ? this : (this._destroy(), this._trigger("destroyend"), this._element.removeData(t).removeAttr("data-" + t.toLowerCase()), this)
            }, reload: function(e) {
                return!1 === this._trigger("reload") ? this : (e && this.options(e), this._reload(), this._trigger("reloadend"), this)
            }, element: function() {
                return this._element
            }, options: function(t, n) {
                if (0 === arguments.length)
                    return e.extend({}, this._options);
                if ("string" == typeof t) {
                    if ("undefined" == typeof n)
                        return"undefined" == typeof this._options[t] ? null : this._options[t];
                    this._options[t] = n
                } else
                    this._options = e.extend({}, this._options, t);
                return this
            }, _trigger: function(n, r, i) {
                return n = e.Event((n + "." + t).toLowerCase()), (r || this._element).trigger(n, [this].concat(i || [])), !n.isDefaultPrevented()
            }}
    }, i.plugin = function(t, n) {
        return i.create(t, e.extend({}, {_carousel: null, carousel: function() {
                return this._carousel || (this._carousel = i.detectCarousel(this.options("carousel") || this._element)) || e.error('Could not detect carousel for plugin "' + t + '"'), this._carousel
            }}, n))
    }, i.create = function(t, n) {
        var s = function(t, n) {
            this._element = e(t), this.options(n), this._init(), this.create()
        };
        return s.prototype = e.extend({}, i.basePrototype(t), n), e.fn[t] = function(n) {
            var i = r.call(arguments, 1), o = this;
            return"string" == typeof n ? this.each(function() {
                var r = e(this).data(t);
                if (!r)
                    return e.error("Cannot call methods on " + t + ' prior to initialization; attempted to call method "' + n + '"');
                if (!e.isFunction(r[n]) || "_" === n.charAt(0))
                    return e.error('No such method "' + n + '" for ' + t + " instance");
                var s = r[n].apply(r, i);
                if (s !== r && "undefined" != typeof s)
                    return o = s, !1
            }) : this.each(function() {
                var r = e(this).data(t);
                r ? r.reload(n) : new s(this, n)
            }), o
        }, s
    }, i.create("jcarousel", {animating: !1, tail: 0, inTail: !1, resizeTimer: null, lt: null, vertical: !1, rtl: !1, circular: !1, _options: {list: function() {
                return this.element().children().eq(0)
            }, items: function() {
                return this.list().children()
            }, animation: 400, wrap: null, vertical: null, rtl: null, center: !1}, _list: null, _items: null, _target: null, _first: null, _last: null, _visible: null, _fullyvisible: null, _init: function() {
            var t = this;
            return this.onWindowResize = function() {
                t.resizeTimer && clearTimeout(t.resizeTimer), t.resizeTimer = setTimeout(function() {
                    t.reload()
                }, 100)
            }, this.onAnimationComplete = function(n) {
                t.animating = !1;
                var r = t.list().find("[data-jcarousel-clone]");
                0 < r.size() && (r.remove(), t._reload()), t._trigger("animateend"), e.isFunction(n) && n.call(t, !0)
            }, this
        }, _create: function() {
            this._reload(), e(t).bind("resize.jcarousel", this.onWindowResize)
        }, _destroy: function() {
            e(t).unbind("resize.jcarousel", this.onWindowResize)
        }, _reload: function() {
            this.vertical = this.options("vertical"), null == this.vertical && (this.vertical = this.list().height() > this.list().width()), this.rtl = this.options("rtl");
            if (null == this.rtl) {
                var t;
                t = this._element;
                if ("rtl" === ("" + t.attr("dir")).toLowerCase())
                    t = !0;
                else {
                    var n = !1;
                    t.parents("[dir]").each(function() {
                        if (/rtl/i.test(e(this).attr("dir")))
                            return n = !0, !1
                    }), t = n
                }
                this.rtl = t
            }
            return this.lt = this.vertical ? "top" : "left", this._items = null, t = this._target || this.closest(), this.circular = "circular" === this.options("wrap"), this.list().css({left: 0, top: 0}), 0 < t.size() && (this._prepare(t), this.list().find("[data-jcarousel-clone]").remove(), this._items = null, this.circular = "circular" === this.options("wrap") && this._fullyvisible.size() < this.items().size(), this.list().css(this.lt, this._position(t) + "px")), this
        }, list: function() {
            if (null === this._list) {
                var t = this.options("list");
                this._list = e.isFunction(t) ? t.call(this) : this._element.find(t)
            }
            return this._list
        }, items: function() {
            if (null === this._items) {
                var t = this.options("items");
                this._items = (e.isFunction(t) ? t.call(this) : this.list().find(t)).not("[data-jcarousel-clone]")
            }
            return this._items
        }, closest: function() {
            var t = this, r = this.list().position()[this.lt], i = e(), s = !1, o = this.vertical ? "bottom" : this.rtl ? "left" : "right", u;
            return this.rtl && !this.vertical && (r = -1 * (r + this.list().width() - this.clipping())), this.items().each(function() {
                i = e(this);
                if (s)
                    return!1;
                var l = t.dimension(i);
                r += l;
                if (0 <= r) {
                    if (u = l - n(i.css("margin-" + o)), !(0 >= Math.abs(r) - l + u / 2))
                        return!1;
                    s = !0
                }
            }), i
        }, target: function() {
            return this._target
        }, first: function() {
            return this._first
        }, last: function() {
            return this._last
        }, visible: function() {
            return this._visible
        }, fullyvisible: function() {
            return this._fullyvisible
        }, hasNext: function() {
            if (!1 === this._trigger("hasnext"))
                return!0;
            var e = this.options("wrap"), t = this.items().size() - 1;
            return 0 <= t && (e && "first" !== e || this._last.index() < t || this.tail && !this.inTail) ? !0 : !1
        }, hasPrev: function() {
            if (!1 === this._trigger("hasprev"))
                return!0;
            var e = this.options("wrap");
            return 0 < this.items().size() && (e && "last" !== e || 0 < this._first.index() || this.tail && this.inTail) ? !0 : !1
        }, clipping: function() {
            return this._element["inner" + (this.vertical ? "Height" : "Width")]()
        }, dimension: function(e) {
            return e["outer" + (this.vertical ? "Height" : "Width")](!0)
        }, scroll: function(t, r, s) {
            if (this.animating || !1 === this._trigger("scroll", null, [t, r]))
                return this;
            e.isFunction(r) && (s = r, r = !0);
            var o = i.parseTarget(t);
            if (o.relative) {
                var t = this.items().size() - 1, u = Math.abs(o.target), a, f = this.options("wrap");
                if (0 < o.target)
                    if (o = this._last.index(), o >= t && this.tail)
                        this.inTail ? "both" === f || "last" === f ? this._scroll(0, r, s) : this._scroll(Math.min(this._target.index() + u, t), r, s) : this._scrollTail(r, s);
                    else if (o !== t || "both" !== f && "last" !== f)
                        if (a = this._target.index(), o = a + u, this.circular && o > t) {
                            u = t;
                            for (t = this.items().get( - 1); u++ < o; )
                                t = this.items().eq(0), t.after(t.clone(!0).attr("data-jcarousel-clone", !0)), this.list().append(t), this._items = null;
                            this._scroll(t, r, s)
                        } else
                            this._scroll(Math.min(o, t), r, s);
                    else
                        this._scroll(0, r, s);
                else if (this.inTail)
                    this._scroll(Math.max(this._first.index() - u + 1, 0), r, s);
                else if (a = this._first.index(), o = a - u, 0 !== a || "both" !== f && "first" !== f)
                    if (this.circular && 0 > o) {
                        u = o;
                        for (t = this.items().get(0); 0 > u++; )
                            t = this.items().eq(-1), t.after(t.clone(!0).attr("data-jcarousel-clone", !0)), this.list().prepend(t), this._items = null, o = n(this.list().css(this.lt)), f = this.dimension(t), o = this.rtl && !this.vertical ? o + f : o - f, this.list().css(this.lt, o + "px");
                        this._scroll(t, r, s)
                    } else
                        this._scroll(Math.max(a - u, 0), r, s);
                else
                    this._scroll(t, r, s)
            } else
                this._scroll(o.target, r, s);
            return this._trigger("scrollend"), this
        }, _scroll: function(t, r, i) {
            if (this.animating)
                return e.isFunction(i) && i.call(this, !1), this;
            "object" != typeof t ? t = this.items().eq(t) : "undefined" == typeof t.jquery && (t = e(t));
            if (0 === t.size())
                return e.isFunction(i) && i.call(this, !1), this;
            this.inTail = !1, this._prepare(t);
            var t = this._position(t), s = n(this.list().css(this.lt));
            return t === s ? (e.isFunction(i) && i.call(this, !1), this) : (s = {}, s[this.lt] = t + "px", this._animate(s, r, i), this)
        }, _scrollTail: function(t, n) {
            if (this.animating || !this.tail)
                return e.isFunction(n) && n.call(this, !1), this;
            var r = this.list().position()[this.lt], r = this.rtl ? r + this.tail : r - this.tail;
            this.inTail = !0;
            var i = {};
            return i[this.lt] = r + "px", this._update({target: this._target.next(), fullyvisible: this._fullyvisible.slice(1).add(this._visible.last())}), this._animate(i, t, n), this
        }, _animate: function(t, n, r) {
            if (!1 === this._trigger("animate"))
                return e.isFunction(r) && r.call(this, !1), this;
            this.animating = !0;
            var i = this.options("animation");
            if (!i || !1 === n)
                this.list().css(t), this.onAnimationComplete(r);
            else {
                var s = this;
                if (e.isFunction(i))
                    i.call(this, t, function() {
                        s.onAnimationComplete(r)
                    });
                else {
                    var n = "object" == typeof i ? i : {duration: i}, o = n.complete;
                    n.complete = function() {
                        s.onAnimationComplete(r), e.isFunction(o) && o.call(this)
                    }, this.list().animate(t, n)
                }
            }
            return this
        }, _prepare: function(t) {
            var r = t.index(), i = r, s = this.dimension(t), o = this.clipping(), u = {target: t, first: t, last: t, visible: t, fullyvisible: s <= o ? t : e()}, a = this.vertical ? "bottom" : this.rtl ? "left" : "right", f, l;
            this.options("center") && (s /= 2, o /= 2);
            if (s < o)
                for (; ; ) {
                    f = this.items().eq(++i);
                    if (0 === f.size()) {
                        if (!this.circular)
                            break;
                        f = this.items().eq(0);
                        if (t.get(0) === f.get(0))
                            break;
                        f.after(f.clone(!0).attr("data-jcarousel-clone", !0)), this.list().append(f), this._items = null
                    }
                    s += this.dimension(f), u.last = f, u.visible = u.visible.add(f), l = n(f.css("margin-" + a)), s - l <= o && (u.fullyvisible = u.fullyvisible.add(f));
                    if (s >= o)
                        break
                }
            if (!this.circular && s < o)
                for (i = r; !(0 > --i); ) {
                    f = this.items().eq(i);
                    if (0 === f.size())
                        break;
                    s += this.dimension(f), u.first = f, u.visible = u.visible.add(f), l = n(f.css("margin-" + a)), s - l <= o && (u.fullyvisible = u.fullyvisible.add(f));
                    if (s >= o)
                        break
                }
            return this._update(u), this.tail = 0, "circular" !== this.options("wrap") && "custom" !== this.options("wrap") && u.last.index() === this.items().size() - 1 && (s -= n(u.last.css("margin-" + a)), s > o && (this.tail = s - o)), this
        }, _position: function(e) {
            var t = this._first, n = t.position()[this.lt];
            return this.rtl && !this.vertical && (n -= this.clipping() - this.dimension(t)), this.options("center") && (n -= this.clipping() / 2 - this.dimension(t) / 2), (e.index() > t.index() || this.inTail) && this.tail ? (n = this.rtl ? n - this.tail : n + this.tail, this.inTail = !0) : this.inTail = !1, -n
        }, _update: function(t) {
            var n = this, r = {target: this._target || e(), first: this._first || e(), last: this._last || e(), visible: this._visible || e(), fullyvisible: this._fullyvisible || e()}, i = (t.first || r.first).index() < r.first.index(), s, o = function(s) {
                var o = [], u = [];
                t[s].each(function() {
                    0 > r[s].index(this) && o.push(this)
                }), r[s].each(function() {
                    0 > t[s].index(this) && u.push(this)
                }), i ? o = o.reverse() : u = u.reverse(), n._trigger("item" + s + "in", e(o)), n._trigger("item" + s + "out", e(u)), n["_" + s] = t[s]
            };
            for (s in t)
                o(s);
            return this
        }})
}(jQuery, window), function(e) {
    e.jcarousel.plugin("jcarouselControl", {_options: {target: "+=1", event: "click"}, _active: null, _init: function() {
            this.onDestroy = e.proxy(function() {
                this._destroy(), this.carousel().one("createend.jcarousel", e.proxy(this._create, this))
            }, this), this.onReload = e.proxy(this._reload, this), this.onEvent = e.proxy(function(e) {
                e.preventDefault(), this.carousel().jcarousel("scroll", this.options("target"))
            }, this)
        }, _create: function() {
            this.carousel().one("destroy.jcarousel", this.onDestroy).bind("reloadend.jcarousel scrollend.jcarousel", this.onReload), this._element.bind(this.options("event") + ".jcarouselcontrol", this.onEvent), this._reload()
        }, _destroy: function() {
            this._element.unbind(".jcarouselcontrol", this.onEvent), this.carousel().unbind("destroy.jcarousel", this.onDestroy).unbind("reloadend.jcarousel scrollend.jcarousel", this.onReload)
        }, _reload: function() {
            var t = e.jcarousel.parseTarget(this.options("target")), n = this.carousel();
            return t.relative ? n = n.jcarousel(0 < t.target ? "hasNext" : "hasPrev") : (t = "object" != typeof t.target ? n.jcarousel("items").eq(t.target) : t.target, n = 0 <= n.jcarousel("target").index(t)), this._active !== n && (this._trigger(n ? "active" : "inactive"), this._active = n), this
        }})
}(jQuery), !function(e) {
    var t = function(e, t) {
        this.init("tooltip", e, t)
    };
    t.prototype = {constructor: t, init: function(t, n, r) {
            this.type = t, this.$element = e(n), this.options = this.getOptions(r), this.enabled = !0, "manual" != this.options.trigger && (t = "hover" == this.options.trigger ? "mouseenter" : "focus", n = "hover" == this.options.trigger ? "mouseleave" : "blur", this.$element.on(t, this.options.selector, e.proxy(this.enter, this)), this.$element.on(n, this.options.selector, e.proxy(this.leave, this))), this.options.selector ? this._options = e.extend({}, this.options, {trigger: "manual", selector: ""}) : this.fixTitle()
        }, getOptions: function(t) {
            return t = e.extend({}, e.fn[this.type].defaults, t, this.$element.data()), t.delay && "number" == typeof t.delay && (t.delay = {show: t.delay, hide: t.delay}), t
        }, enter: function(t) {
            var n = e(t.currentTarget)[this.type](this._options).data(this.type);
            if (!n.options.delay || !n.options.delay.show)
                return n.show();
            clearTimeout(this.timeout), n.hoverState = "in", this.timeout = setTimeout(function() {
                "in" == n.hoverState && n.show()
            }, n.options.delay.show)
        }, leave: function(t) {
            var n = e(t.currentTarget)[this.type](this._options).data(this.type);
            this.timeout && clearTimeout(this.timeout);
            if (!n.options.delay || !n.options.delay.hide)
                return n.hide();
            n.hoverState = "out", this.timeout = setTimeout(function() {
                "out" == n.hoverState && n.hide()
            }, n.options.delay.hide)
        }, show: function() {
            var e, t, n, r, i, s, o;
            if (this.hasContent() && this.enabled) {
                e = this.tip(), this.setContent(), this.options.animation && e.addClass("fade"), s = "function" == typeof this.options.placement ? this.options.placement.call(this, e[0], this.$element[0]) : this.options.placement, t = /in/.test(s), e.remove().css({top: 0, left: 0, display: "block"}).appendTo(t ? this.$element : document.body), n = this.getPosition(t), r = e[0].offsetWidth, i = e[0].offsetHeight;
                switch (t ? s.split(" ")[1] : s) {
                    case"bottom":
                        o = {top: n.top + n.height, left: n.left + n.width / 2 - r / 2};
                        break;
                    case"top":
                        o = {top: n.top - i, left: n.left + n.width / 2 - r / 2};
                        break;
                    case"left":
                        o = {top: n.top + n.height / 2 - i / 2, left: n.left - r};
                        break;
                    case"right":
                        o = {top: n.top + n.height / 2 - i / 2, left: n.left + n.width}
                }
                e.css(o).addClass(s).addClass("in")
            }
        }, isHTML: function(e) {
            return"string" != typeof e || "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && 3 <= e.length || /^(?:[^<]*<[\w\W]+>[^>]*$)/.exec(e)
        }, setContent: function() {
            var e = this.tip(), t = this.getTitle();
            e.find(".tooltip-inner")[this.isHTML(t) ? "html" : "text"](t), e.removeClass("fade in top bottom left right")
        }, hide: function() {
            var t = this.tip();
            t.removeClass("in");
            if (e.support.transition && this.$tip.hasClass("fade")) {
                var n = setTimeout(function() {
                    t.off(e.support.transition.end).remove()
                }, 500);
                t.one(e.support.transition.end, function() {
                    clearTimeout(n), t.remove()
                })
            } else
                t.remove()
        }, fixTitle: function() {
            var e = this.$element;
            (e.attr("title") || "string" != typeof e.attr("data-original-title")) && e.attr("data-original-title", e.attr("title") || "").removeAttr("title")
        }, hasContent: function() {
            return this.getTitle()
        }, getPosition: function(t) {
            return e.extend({}, t ? {top: 0, left: 0} : this.$element.offset(), {width: this.$element[0].offsetWidth, height: this.$element[0].offsetHeight})
        }, getTitle: function() {
            var e = this.$element, t = this.options;
            return e.attr("data-original-title") || ("function" == typeof t.title ? t.title.call(e[0]) : t.title)
        }, tip: function() {
            return this.$tip = this.$tip || e(this.options.template)
        }, validate: function() {
            this.$element[0].parentNode || (this.hide(), this.options = this.$element = null)
        }, enable: function() {
            this.enabled = !0
        }, disable: function() {
            this.enabled = !1
        }, toggleEnabled: function() {
            this.enabled = !this.enabled
        }, toggle: function() {
            this[this.tip().hasClass("in") ? "hide" : "show"]()
        }}, e.fn.tooltip = function(n) {
        return this.each(function() {
            var r = e(this), i = r.data("tooltip"), s = "object" == typeof n && n;
            i || r.data("tooltip", i = new t(this, s)), "string" == typeof n && i[n]()
        })
    }, e.fn.tooltip.Constructor = t, e.fn.tooltip.defaults = {animation: !0, placement: "top", selector: !1, template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>', trigger: "hover", title: "", delay: 0}
}(window.jQuery), function(e) {
    var t = !1;
    e(document).on("click", "a[data-reveal-id]", function(t) {
        t.preventDefault(), t = e(this).attr("data-reveal-id"), e("#" + t).reveal(e(this).data())
    }), e.fn.reveal = function(n) {
        var r = e(document), n = e.extend({}, {animation: "fadeAndPop", animationSpeed: 300, closeOnBackgroundClick: !0, dismissModalClass: "close-reveal-modal", open: e.noop, opened: e.noop, close: e.noop, closed: e.noop}, n);
        return this.each(function() {
            var i = e(this), s = parseInt(i.css("top"), 10), o = i.height() + s, u = !1, a = e(".reveal-modal-bg"), l = {top: 0, opacity: 0, visibility: "visible", display: "block"}, h = {top: s, opacity: 1, visibility: "hidden", display: "none"}, p;
            0 === a.length && (a = e("<div />", {"class": "reveal-modal-bg"}).insertAfter(i), a.fadeTo("fast", .8)), i.bind("reveal:open.reveal", function() {
                if (!u) {
                    u = !0;
                    var h = e(".reveal-modal.open");
                    h.length === 1 && (t = !0, h.trigger("reveal:close")), i.addClass("open"), n.animation === "fadeAndPop" && (l.top = r.scrollTop() - o, l.opacity = 0, i.css(l), a.fadeIn(n.animationSpeed / 2), i.delay(n.animationSpeed / 2).animate({top: r.scrollTop() + s + "px", opacity: 1}, n.animationSpeed, function() {
                        i.trigger("reveal:opened")
                    })), n.animation === "fade" && (l.top = r.scrollTop() + s, l.opacity = 0, i.css(l), a.fadeIn(n.animationSpeed / 2), i.delay(n.animationSpeed / 2).animate({opacity: 1}, n.animationSpeed, function() {
                        i.trigger("reveal:opened")
                    })), n.animation === "none" && (l.top = r.scrollTop() + s, l.opacity = 1, i.css(l), a.css({display: "block"}), i.trigger("reveal:opened"))
                }
            }), i.bind("reveal:close.reveal", function() {
                u || (u = !0, i.removeClass("open"), n.animation === "fadeAndPop" && (i.animate({top: r.scrollTop() - o + "px", opacity: 0}, n.animationSpeed / 2, function() {
                    i.css(h)
                }), t ? i.trigger("reveal:closed") : a.delay(n.animationSpeed).fadeOut(n.animationSpeed, function() {
                    i.trigger("reveal:closed")
                }), t = !1), n.animation === "fade" && (i.animate({opacity: 0}, n.animationSpeed, function() {
                    i.css(h)
                }), t ? i.trigger("reveal:closed") : a.delay(n.animationSpeed).fadeOut(n.animationSpeed, function() {
                    i.trigger("reveal:closed")
                })), n.animation === "none" && (i.css(h), t || a.css({display: "none"}), i.trigger("reveal:closed")))
            }), i.bind("reveal:opened.reveal reveal:closed.reveal", function() {
                u = !1
            }), i.bind("reveal:closed.reveal", function() {
                i.unbind(".reveal"), a.unbind(".reveal"), p.unbind(".reveal"), e("body").unbind(".reveal")
            }), i.bind("reveal:open.reveal", n.open), i.bind("reveal:opened.reveal", n.opened), i.bind("reveal:close.reveal", n.close), i.bind("reveal:closed.reveal", n.closed), i.trigger("reveal:open"), p = e("." + n.dismissModalClass).bind("click.reveal", function() {
                i.trigger("reveal:close")
            }), n.closeOnBackgroundClick && (a.css({cursor: "pointer"}), a.bind("click.reveal", function() {
                i.trigger("reveal:close")
            })), e("body").bind("keyup.reveal", function(e) {
                e.which === 27 && i.trigger("reveal:close")
            })
        })
    }
}(jQuery), function(window, undefined) {
    function returnTrue() {
        return!0
    }
    function returnFalse() {
        return!1
    }
    var document = window.document;
    document.createElement("video"), document.createElement("audio");

    var VideoJS = function(e, t, n) {
        var r;
        if (typeof e == "string") {
            e.indexOf("#") === 0 && (e = e.slice(1));
            if (_V_.players[e])
                return _V_.players[e];
            r = _V_.el(e)
        } else
            r = e;

        if (!r || !r.nodeName)
            return true;
        //throw new TypeError("The element or ID supplied is not valid. (VideoJS)");
        return r.player || new _V_.Player(r, t, n)
    }, _V_ = VideoJS, CDN_VERSION = "3.2";
    VideoJS.players = {}, VideoJS.options = {techOrder: ["html5", "flash"], html5: {}, flash: {swf: "http://vjs.zencdn.net/c/video-js.swf"}, width: "auto", height: "auto", defaultVolume: 0, components: {posterImage: {}, textTrackDisplay: {}, loadingSpinner: {}, bigPlayButton: {}, controlBar: {}}}, CDN_VERSION != "GENERATED_CDN_VSN" && (_V_.options.flash.swf = "http://vjs.zencdn.net/" + CDN_VERSION + "/video-js.swf"), _V_.merge = function(e, t, n) {
        t || (t = {});
        for (var r in t)
            t.hasOwnProperty(r) && (!n || !e.hasOwnProperty(r)) && (e[r] = t[r]);
        return e
    }, _V_.extend = function(e) {
        this.merge(this, e, !0)
    }, _V_.extend({tech: {}, controlSets: {}, isIE: function() {
            return!1
        }, isFF: function() {
            return!!_V_.ua.match("Firefox")
        }, isIPad: function() {
            return navigator.userAgent.match(/iPad/i) !== null
        }, isIPhone: function() {
            return navigator.userAgent.match(/iPhone/i) !== null
        }, isIOS: function() {
            return VideoJS.isIPhone() || VideoJS.isIPad()
        }, iOSVersion: function() {
            var e = navigator.userAgent.match(/OS (\d+)_/i);
            if (e && e[1])
                return e[1]
        }, isAndroid: function() {
            return navigator.userAgent.match(/Android.*AppleWebKit/i) !== null
        }, androidVersion: function() {
            var e = navigator.userAgent.match(/Android (\d+)\./i);
            if (e && e[1])
                return e[1]
        }, testVid: document.createElement("video"), ua: navigator.userAgent, support: {}, each: function(e, t) {
            if (!e || e.length === 0)
                return;
            for (var n = 0, r = e.length; n < r; n++)
                t.call(this, e[n], n)
        }, eachProp: function(e, t) {
            if (!e)
                return;
            for (var n in e)
                e.hasOwnProperty(n) && t.call(this, n, e[n])
        }, el: function(e) {
            return document.getElementById(e)
        }, createElement: function(e, t) {
            var n = document.createElement(e), r;
            for (r in t)
                t.hasOwnProperty(r) && (r.indexOf("-") !== -1 ? n.setAttribute(r, t[r]) : n[r] = t[r]);
            return n
        }, insertFirst: function(e, t) {
            t.firstChild ? t.insertBefore(e, t.firstChild) : t.appendChild(e)
        }, addClass: function(e, t) {
            (" " + e.className + " ").indexOf(" " + t + " ") == -1 && (e.className = e.className === "" ? t : e.className + " " + t)
        }, removeClass: function(e, t) {
            if (e.className.indexOf(t) == -1)
                return;
            var n = e.className.split(" ");
            n.splice(n.indexOf(t), 1), e.className = n.join(" ")
        }, remove: function(e, t) {
            if (!t)
                return;
            var n = t.indexOf(e);
            if (n != -1)
                return t.splice(n, 1)
        }, blockTextSelection: function() {
            document.body.focus(), document.onselectstart = function() {
                return!1
            }
        }, unblockTextSelection: function() {
            document.onselectstart = function() {
                return!0
            }
        }, formatTime: function(e, t) {
            t = t || e;
            var n = Math.floor(e % 60), r = Math.floor(e / 60 % 60), i = Math.floor(e / 3600), s = Math.floor(t / 60 % 60), o = Math.floor(t / 3600);
            return i = i > 0 || o > 0 ? i + ":" : "", r = ((i || s >= 10) && r < 10 ? "0" + r : r) + ":", n = n < 10 ? "0" + n : n, i + r + n
        }, uc: function(e) {
            return e.charAt(0).toUpperCase() + e.slice(1)
        }, getRelativePosition: function(e, t) {
            return Math.max(0, Math.min(1, (e - _V_.findPosX(t)) / t.offsetWidth))
        }, getComputedStyleValue: function(e, t) {
            return window.getComputedStyle(e, null).getPropertyValue(t)
        }, trim: function(e) {
            return e.toString().replace(/^\s+/, "").replace(/\s+$/, "")
        }, round: function(e, t) {
            return t || (t = 0), Math.round(e * Math.pow(10, t)) / Math.pow(10, t)
        }, isEmpty: function(e) {
            for (var t in e)
                return!1;
            return!0
        }, createTimeRange: function(e, t) {
            return{length: 1, start: function() {
                    return e
                }, end: function() {
                    return t
                }}
        }, cache: {}, guid: 1, expando: "vdata" + (new Date).getTime(), getData: function(e) {
            var t = e[_V_.expando];
            return t || (t = e[_V_.expando] = _V_.guid++, _V_.cache[t] = {}), _V_.cache[t]
        }, removeData: function(e) {
            var t = e[_V_.expando];
            if (!t)
                return;
            delete _V_.cache[t];
            try {
                delete e[_V_.expando]
            } catch (n) {
                e.removeAttribute ? e.removeAttribute(_V_.expando) : e[_V_.expando] = null
            }
        }, proxy: function(e, t, n) {
            t.guid || (t.guid = _V_.guid++);
            var r = function() {
                return t.apply(e, arguments)
            };
            return r.guid = n ? n + "_" + t.guid : t.guid, r
        }, get: function(e, t, n) {
            var r = e.indexOf("file:") == 0 || window.location.href.indexOf("file:") == 0 && e.indexOf("http:") == -1;
            typeof XMLHttpRequest == "undefined" && (XMLHttpRequest = function() {
                try {
                    return new ActiveXObject("Msxml2.XMLHTTP.6.0")
                } catch (e) {
                }
                try {
                    return new ActiveXObject("Msxml2.XMLHTTP.3.0")
                } catch (t) {
                }
                try {
                    return new ActiveXObject("Msxml2.XMLHTTP")
                } catch (n) {
                }
                throw new Error("This browser does not support XMLHttpRequest.")
            });
            var i = new XMLHttpRequest;
            try {
                i.open("GET", e)
            } catch (s) {
                return _V_.log("VideoJS XMLHttpRequest (open)", s), !1
            }
            i.onreadystatechange = _V_.proxy(this, function() {
                i.readyState == 4 && (i.status == 200 || r && i.status == 0 ? t(i.responseText) : n && n())
            });
            try {
                i.send()
            } catch (s) {
                _V_.log("VideoJS XMLHttpRequest (send)", s), n && n(s)
            }
        }, setLocalStorage: function(e, t) {
            var n = window.localStorage || !1;
            if (!n)
                return;
            try {
                n[e] = t
            } catch (r) {
                r.code == 22 || r.code == 1014 ? _V_.log("LocalStorage Full (VideoJS)", r) : _V_.log("LocalStorage Error (VideoJS)", r)
            }
        }, getAbsoluteURL: function(e) {
            return e.match(/^https?:\/\//) || (e = _V_.createElement("div", {innerHTML: '<a href="' + e + '">x</a>'}).firstChild.href), e
        }}), _V_.log = function() {
        _V_.log.history = _V_.log.history || [], _V_.log.history.push(arguments);
        if (window.console) {
            arguments.callee = arguments.callee.caller;
            var e = [].slice.call(arguments);
            typeof console.log == "object" ? _V_.log.apply.call(console.log, console, e) : console.log.apply(console, e)
        }
    }, function(e) {
        function t() {
        }
        for (var n = "assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,timeStamp,profile,profileEnd,time,timeEnd,trace,warn".split(","), r; r = n.pop(); )
            e[r] = e[r] || t
    }(function() {
        try {
            return console.log(), window.console
        } catch (e) {
            return window.console = {}
        }
    }()), "getBoundingClientRect"in document.documentElement ? _V_.findPosX = function(e) {
        var t;
        try {
            t = e.getBoundingClientRect()
        } catch (n) {
        }
        if (!t)
            return 0;
        var r = document.documentElement, i = document.body, s = r.clientLeft || i.clientLeft || 0, o = window.pageXOffset || i.scrollLeft, u = t.left + o - s;
        return u
    } : _V_.findPosX = function(e) {
        var t = e.offsetLeft;
        while (e = obj.offsetParent)
            e.className.indexOf("video-js") == -1, t += e.offsetLeft;
        return t
    }, function() {
        var e = !1, t = /xyz/.test(function() {
            xyz
        }) ? /\b_super\b/ : /.*/;
        _V_.Class = function() {
        }, _V_.Class.extend = function(n) {
            function o() {
                if (!e && this.init)
                    return this.init.apply(this, arguments);
                if (!e)
                    return arguments.callee.prototype.init()
            }
            var r = this.prototype;
            e = !0;
            var i = new this;
            e = !1;
            for (var s in n)
                i[s] = typeof n[s] == "function" && typeof r[s] == "function" && t.test(n[s]) ? function(e, t) {
                    return function() {
                        var n = this._super;
                        this._super = r[e];
                        var i = t.apply(this, arguments);
                        return this._super = n, i
                    }
                }(s, n[s]) : n[s];
            return o.prototype = i, o.constructor = o, o.extend = arguments.callee, o
        }
    }(), _V_.Component = _V_.Class.extend({init: function(e, t) {
            this.player = e, t = this.options = _V_.merge(this.options || {}, t), t.el ? this.el = t.el : this.el = this.createElement(), this.initComponents()
        }, destroy: function() {
        }, createElement: function(e, t) {
            return _V_.createElement(e || "div", t)
        }, buildCSSClass: function() {
            return""
        }, initComponents: function() {
            var e = this.options;
            e && e.components && this.eachProp(e.components, function(e, t) {
                var n = this.proxy(function() {
                    this[e] = this.addComponent(e, t)
                });
                t.loadEvent ? this.one(t.loadEvent, n) : n()
            })
        }, addComponent: function(e, t) {
            var n, r;
            return typeof e == "string" ? (t = t || {}, r = t.componentClass || _V_.uc(e), n = new _V_[r](this.player || this, t)) : n = e, this.el.appendChild(n.el), n
        }, removeComponent: function(e) {
            this.el.removeChild(e.el)
        }, show: function() {
            this.el.style.display = "block"
        }, hide: function() {
            this.el.style.display = "none"
        }, fadeIn: function() {
            this.removeClass("vjs-fade-out"), this.addClass("vjs-fade-in")
        }, fadeOut: function() {
            this.removeClass("vjs-fade-in"), this.addClass("vjs-fade-out")
        }, lockShowing: function() {
            var e = this.el.style;
            e.display = "block", e.opacity = 1, e.visiblity = "visible"
        }, unlockShowing: function() {
            var e = this.el.style;
            e.display = "", e.opacity = "", e.visiblity = ""
        }, addClass: function(e) {
            _V_.addClass(this.el, e)
        }, removeClass: function(e) {
            _V_.removeClass(this.el, e)
        }, addEvent: function(e, t, n) {
            return _V_.addEvent(this.el, e, _V_.proxy(this, t))
        }, removeEvent: function(e, t) {
            return _V_.removeEvent(this.el, e, t)
        }, triggerEvent: function(e, t) {
            return _V_.triggerEvent(this.el, e, t)
        }, one: function(e, t) {
            _V_.one(this.el, e, _V_.proxy(this, t))
        }, ready: function(e) {
            return e ? (this.isReady ? e.call(this) : (this.readyQueue === undefined && (this.readyQueue = []), this.readyQueue.push(e)), this) : this
        }, triggerReady: function() {
            this.isReady = !0, this.readyQueue && this.readyQueue.length > 0 && (this.each(this.readyQueue, function(e) {
                e.call(this)
            }), this.readyQueue = [], this.triggerEvent("ready"))
        }, each: function(e, t) {
            _V_.each.call(this, e, t)
        }, eachProp: function(e, t) {
            _V_.eachProp.call(this, e, t)
        }, extend: function(e) {
            _V_.merge(this, e)
        }, proxy: function(e, t) {
            return _V_.proxy(this, e, t)
        }}), _V_.Control = _V_.Component.extend({buildCSSClass: function() {
            return"vjs-control " + this._super()
        }}), _V_.ControlBar = _V_.Component.extend({options: {loadEvent: "play", components: {playToggle: {}, fullscreenToggle: {}, currentTimeDisplay: {}, timeDivider: {}, durationDisplay: {}, remainingTimeDisplay: {}, progressControl: {}, volumeControl: {}, muteToggle: {}}}, init: function(e, t) {
            this._super(e, t), e.addEvent("play", this.proxy(function() {
                this.fadeIn(), this.player.addEvent("mouseover", this.proxy(this.fadeIn)), this.player.addEvent("mouseout", this.proxy(this.fadeOut))
            }))
        }, createElement: function() {
            return _V_.createElement("div", {className: "vjs-controls"})
        }, fadeIn: function() {
            this._super(), this.player.triggerEvent("controlsvisible")
        }, fadeOut: function() {
            this._super(), this.player.triggerEvent("controlshidden")
        }, lockShowing: function() {
            this.el.style.opacity = "1"
        }}), _V_.Button = _V_.Control.extend({init: function(e, t) {
            this._super(e, t), this.addEvent("click", this.onClick), this.addEvent("focus", this.onFocus), this.addEvent("blur", this.onBlur)
        }, createElement: function(e, t) {
            return t = _V_.merge({className: this.buildCSSClass(), innerHTML: '<div><span class="vjs-control-text">' + (this.buttonText || "Need Text") + "</span></div>", role: "button", tabIndex: 0}, t), this._super(e, t)
        }, onClick: function() {
        }, onFocus: function() {
            _V_.addEvent(document, "keyup", _V_.proxy(this, this.onKeyPress))
        }, onKeyPress: function(e) {
            if (e.which == 32 || e.which == 13)
                e.preventDefault(), this.onClick()
        }, onBlur: function() {
            _V_.removeEvent(document, "keyup", _V_.proxy(this, this.onKeyPress))
        }}), _V_.PlayButton = _V_.Button.extend({buttonText: "Play", buildCSSClass: function() {
            return"vjs-play-button " + this._super()
        }, onClick: function() {
            this.player.play()
        }}), _V_.PauseButton = _V_.Button.extend({buttonText: "Pause", buildCSSClass: function() {
            return"vjs-pause-button " + this._super()
        }, onClick: function() {
            this.player.pause()
        }}), _V_.PlayToggle = _V_.Button.extend({buttonText: "Play", init: function(e, t) {
            this._super(e, t), e.addEvent("play", _V_.proxy(this, this.onPlay)), e.addEvent("pause", _V_.proxy(this, this.onPause))
        }, buildCSSClass: function() {
            return"vjs-play-control " + this._super()
        }, onClick: function() {
            this.player.paused() ? this.player.play() : this.player.pause()
        }, onPlay: function() {
            _V_.removeClass(this.el, "vjs-paused"), _V_.addClass(this.el, "vjs-playing")
        }, onPause: function() {
            _V_.removeClass(this.el, "vjs-playing"), _V_.addClass(this.el, "vjs-paused")
        }}), _V_.FullscreenToggle = _V_.Button.extend({buttonText: "Fullscreen", buildCSSClass: function() {
            return"vjs-fullscreen-control " + this._super()
        }, onClick: function() {
            this.player.isFullScreen ? this.player.cancelFullScreen() : this.player.requestFullScreen()
        }}), _V_.BigPlayButton = _V_.Button.extend({init: function(e, t) {
            this._super(e, t), e.addEvent("play", _V_.proxy(this, this.hide)), e.addEvent("ended", _V_.proxy(this, this.show))
        }, createElement: function() {
            return this._super("div", {className: "vjs-big-play-button", innerHTML: "<span></span>"})
        }, onClick: function() {
            this.player.currentTime() && this.player.currentTime(0), this.player.play()
        }}), _V_.LoadingSpinner = _V_.Component.extend({init: function(e, t) {
            this._super(e, t), e.addEvent("canplay", _V_.proxy(this, this.hide)), e.addEvent("canplaythrough", _V_.proxy(this, this.hide)), e.addEvent("playing", _V_.proxy(this, this.hide)), e.addEvent("seeking", _V_.proxy(this, this.show)), e.addEvent("error", _V_.proxy(this, this.show)), e.addEvent("waiting", _V_.proxy(this, this.show))
        }, createElement: function() {
            var e, t;
            return typeof this.player.el.style.WebkitBorderRadius == "string" || typeof this.player.el.style.MozBorderRadius == "string" || typeof this.player.el.style.KhtmlBorderRadius == "string" || typeof this.player.el.style.borderRadius == "string" ? (e = "vjs-loading-spinner", t = "<div class='ball1'></div><div class='ball2'></div><div class='ball3'></div><div class='ball4'></div><div class='ball5'></div><div class='ball6'></div><div class='ball7'></div><div class='ball8'></div>") : (e = "vjs-loading-spinner-fallback", t = ""), this._super("div", {className: e, innerHTML: t})
        }}), _V_.CurrentTimeDisplay = _V_.Component.extend({init: function(e, t) {
            this._super(e, t), e.addEvent("timeupdate", _V_.proxy(this, this.updateContent))
        }, createElement: function() {
            var e = this._super("div", {className: "vjs-current-time vjs-time-controls vjs-control"});
            return this.content = _V_.createElement("div", {className: "vjs-current-time-display", innerHTML: "0:00"}), e.appendChild(_V_.createElement("div").appendChild(this.content)), e
        }, updateContent: function() {
            var e = this.player.scrubbing ? this.player.values.currentTime : this.player.currentTime();
            this.content.innerHTML = _V_.formatTime(e, this.player.duration())
        }}), _V_.DurationDisplay = _V_.Component.extend({init: function(e, t) {
            this._super(e, t), e.addEvent("timeupdate", _V_.proxy(this, this.updateContent))
        }, createElement: function() {
            var e = this._super("div", {className: "vjs-duration vjs-time-controls vjs-control"});
            return this.content = _V_.createElement("div", {className: "vjs-duration-display", innerHTML: "0:00"}), e.appendChild(_V_.createElement("div").appendChild(this
                    .content)), e
        }, updateContent: function() {
            this.player.duration() && (this.content.innerHTML = _V_.formatTime(this.player.duration()))
        }}), _V_.TimeDivider = _V_.Component.extend({createElement: function() {
            return this._super("div", {className: "vjs-time-divider", innerHTML: "<div><span>/</span></div>"})
        }}), _V_.RemainingTimeDisplay = _V_.Component.extend({init: function(e, t) {
            this._super(e, t), e.addEvent("timeupdate", _V_.proxy(this, this.updateContent))
        }, createElement: function() {
            var e = this._super("div", {className: "vjs-remaining-time vjs-time-controls vjs-control"});
            return this.content = _V_.createElement("div", {className: "vjs-remaining-time-display", innerHTML: "-0:00"}), e.appendChild(_V_.createElement("div").appendChild(this.content)), e
        }, updateContent: function() {
            this.player.duration() && (this.content.innerHTML = "-" + _V_.formatTime(this.player.remainingTime()))
        }}), _V_.Slider = _V_.Component.extend({init: function(e, t) {
            this._super(e, t), e.addEvent(this.playerEvent, _V_.proxy(this, this.update)), this.addEvent("mousedown", this.onMouseDown), this.addEvent("focus", this.onFocus), this.addEvent("blur", this.onBlur), this.player.addEvent("controlsvisible", this.proxy(this.update)), this.update()
        }, createElement: function(e, t) {
            return t = _V_.merge({role: "slider", "aria-valuenow": 0, "aria-valuemin": 0, "aria-valuemax": 100, tabIndex: 0}, t), this._super(e, t)
        }, onMouseDown: function(e) {
            e.preventDefault(), _V_.blockTextSelection(), _V_.addEvent(document, "mousemove", _V_.proxy(this, this.onMouseMove)), _V_.addEvent(document, "mouseup", _V_.proxy(this, this.onMouseUp)), this.onMouseMove(e)
        }, onMouseUp: function(e) {
            _V_.unblockTextSelection(), _V_.removeEvent(document, "mousemove", this.onMouseMove, !1), _V_.removeEvent(document, "mouseup", this.onMouseUp, !1), this.update()
        }, update: function() {
            var e, t = this.getPercent();
            handle = this.handle, bar = this.bar, isNaN(t) && (t = 0), e = t;
            if (handle) {
                var n = this.el, r = n.offsetWidth, i = handle.el.offsetWidth, s = i ? i / r : 0, o = 1 - s;
                adjustedProgress = t * o, e = adjustedProgress + s / 2, handle.el.style.left = _V_.round(adjustedProgress * 100, 2) + "%"
            }
            bar.el.style.width = _V_.round(e * 100, 2) + "%"
        }, calculateDistance: function(e) {
            var t = this.el, n = _V_.findPosX(t), r = t.offsetWidth, i = this.handle;
            if (i) {
                var s = i.el.offsetWidth;
                n += s / 2, r -= s
            }
            return Math.max(0, Math.min(1, (e.pageX - n) / r))
        }, onFocus: function(e) {
            _V_.addEvent(document, "keyup", _V_.proxy(this, this.onKeyPress))
        }, onKeyPress: function(e) {
            e.which == 37 ? (e.preventDefault(), this.stepBack()) : e.which == 39 && (e.preventDefault(), this.stepForward())
        }, onBlur: function(e) {
            _V_.removeEvent(document, "keyup", _V_.proxy(this, this.onKeyPress))
        }}), _V_.ProgressControl = _V_.Component.extend({options: {components: {seekBar: {}}}, createElement: function() {
            return this._super("div", {className: "vjs-progress-control vjs-control"})
        }}), _V_.SeekBar = _V_.Slider.extend({options: {components: {loadProgressBar: {}, bar: {componentClass: "PlayProgressBar"}, handle: {componentClass: "SeekHandle"}}}, playerEvent: "timeupdate", init: function(e, t) {
            this._super(e, t)
        }, createElement: function() {
            return this._super("div", {className: "vjs-progress-holder"})
        }, getPercent: function() {
            return this.player.currentTime() / this.player.duration()
        }, onMouseDown: function(e) {
            this._super(e), this.player.scrubbing = !0, this.videoWasPlaying = !this.player.paused(), this.player.pause()
        }, onMouseMove: function(e) {
            var t = this.calculateDistance(e) * this.player.duration();
            t == this.player.duration() && (t -= .1), this.player.currentTime(t)
        }, onMouseUp: function(e) {
            this._super(e), this.player.scrubbing = !1, this.videoWasPlaying && this.player.play()
        }, stepForward: function() {
            this.player.currentTime(this.player.currentTime() + 1)
        }, stepBack: function() {
            this.player.currentTime(this.player.currentTime() - 1)
        }}), _V_.LoadProgressBar = _V_.Component.extend({init: function(e, t) {
            this._super(e, t), e.addEvent("progress", _V_.proxy(this, this.update))
        }, createElement: function() {
            return this._super("div", {className: "vjs-load-progress", innerHTML: '<span class="vjs-control-text">Loaded: 0%</span>'})
        }, update: function() {
            this.el.style && (this.el.style.width = _V_.round(this.player.bufferedPercent() * 100, 2) + "%")
        }}), _V_.PlayProgressBar = _V_.Component.extend({createElement: function() {
            return this._super("div", {className: "vjs-play-progress", innerHTML: '<span class="vjs-control-text">Progress: 0%</span>'})
        }}), _V_.SeekHandle = _V_.Component.extend({createElement: function() {

            return this._super("div", {className: "vjs-seek-handle", innerHTML: '<span class="vjs-control-text">00:00</span>'})
        }}), _V_.VolumeControl = _V_.Component.extend({options: {components: {volumeBar: {}}}, createElement: function() {
            return this._super("div", {className: "vjs-volume-control vjs-control"})
        }}), _V_.VolumeBar = _V_.Slider.extend({options: {components: {bar: {componentClass: "VolumeLevel"}, handle: {componentClass: "VolumeHandle"}}}, playerEvent: "volumechange", createElement: function() {
            return this._super("div", {className: "vjs-volume-bar"})
        }, onMouseMove: function(e) {
            this.player.volume(this.calculateDistance(e))
        }, getPercent: function() {
            return this.player.volume()
        }, stepForward: function() {
            this.player.volume(this.player.volume() + .1)
        }, stepBack: function() {
            this.player.volume(this.player.volume() - .1)
        }}), _V_.VolumeLevel = _V_.Component.extend({createElement: function() {
            return this._super("div", {className: "vjs-volume-level", innerHTML: '<span class="vjs-control-text"></span>'})
        }}), _V_.VolumeHandle = _V_.Component.extend({createElement: function() {
            return this._super("div", {className: "vjs-volume-handle", innerHTML: '<span class="vjs-control-text"></span>'})
        }}), _V_.MuteToggle = _V_.Button.extend({init: function(e, t) {
            this._super(e, t), e.addEvent("volumechange", _V_.proxy(this, this.update))
        }, createElement: function() {
            return this._super("div", {className: "vjs-mute-control vjs-control", innerHTML: '<div><span class="vjs-control-text">Mute</span></div>'})
        }, onClick: function(e) {
            this.player.muted(this.player.muted() ? !1 : !0)
        }, update: function(e) {
            var t = this.player.volume(), n = 3;
            t == 0 || this.player.muted() ? n = 0 : t < .33 ? n = 1 : t < .67 && (n = 2), _V_.each.call(this, [0, 1, 2, 3], function(e) {
                _V_.removeClass(this.el, "vjs-vol-" + e)
            }), _V_.addClass(this.el, "vjs-vol-" + n)
        }}), _V_.PosterImage = _V_.Button.extend({init: function(e, t) {
            this._super(e, t), this.player.options.poster || this.hide(), e.addEvent("play", _V_.proxy(this, this.hide))
        }, createElement: function() {
            return _V_.createElement("img", {className: "vjs-poster", src: this.player.options.poster, tabIndex: -1})
        }, onClick: function() {
            this.player.play()
        }}), _V_.Menu = _V_.Component.extend({init: function(e, t) {
            this._super(e, t)
        }, addItem: function(e) {
            this.addComponent(e), e.addEvent("click", this.proxy(function() {
                this.unlockShowing()
            }))
        }, createElement: function() {
            return this._super("ul", {className: "vjs-menu"})
        }}), _V_.MenuItem = _V_.Button.extend({init: function(e, t) {
            this._super(e, t), t.selected && this.addClass("vjs-selected")
        }, createElement: function(e, t) {
            return this._super("li", _V_.merge({className: "vjs-menu-item", innerHTML: this.options.label}, t))
        }, onClick: function() {
            this.selected(!0)
        }, selected: function(e) {
            e ? this.addClass("vjs-selected") : this.removeClass("vjs-selected")
        }}), Array.prototype.indexOf || (Array.prototype.indexOf = function(e) {
        if (this === void 0 || this === null)
            throw new TypeError;
        var t = Object(this), n = t.length >>> 0;
        if (n === 0)
            return-1;
        var r = 0;
        arguments.length > 0 && (r = Number(arguments[1]), r !== r ? r = 0 : r !== 0 && r !== 1 / 0 && r !== -1 / 0 && (r = (r > 0 || -1) * Math.floor(Math.abs(r))));
        if (r >= n)
            return-1;
        var i = r >= 0 ? r : Math.max(n - Math.abs(r), 0);
        for (; i < n; i++)
            if (i in t && t[i] === e)
                return i;
        return-1
    }), _V_.extend({addEvent: function(e, t, n) {
            var r = _V_.getData(e), i;
            r && !r.handler && (r.handler = function(t) {
                t = _V_.fixEvent(t);
                var n = _V_.getData(e).events[t.type];
                if (n) {
                    var r = [];
                    _V_.each(n, function(e, t) {
                        r[t] = e
                    });
                    for (var i = 0, s = r.length; i < s; i++)
                        r[i].call(e, t)
                }
            }), r.events || (r.events = {}), i = r.events[t], i || (i = r.events[t] = [], document.addEventListener ? e.addEventListener(t, r.handler, !1) : document.attachEvent && e.attachEvent("on" + t, r.handler)), n.guid || (n.guid = _V_.guid++), i.push(n)
        }, removeEvent: function(e, t, n) {
            var r = _V_.getData(e), i;
            if (!r.events)
                return;
            if (!t) {
                for (t in r.events)
                    _V_.cleanUpEvents(e, t);
                return
            }
            i = r.events[t];
            if (!i)
                return;
            if (n && n.guid)
                for (var s = 0; s < i.length; s++)
                    i[s].guid === n.guid && i.splice(s--, 1);
            _V_.cleanUpEvents(e, t)
        }, cleanUpEvents: function(e, t) {
            var n = _V_.getData(e);
            n.events[t].length === 0 && (delete n.events[t], document.removeEventListener ? e.removeEventListener(t, n.handler, !1) : document.detachEvent && e.detachEvent("on" + t, n.handler)), _V_.isEmpty(n.events) && (delete n.events, delete n.handler), _V_.isEmpty(n) && _V_.removeData(e)
        }, fixEvent: function(e) {
            if (e[_V_.expando])
                return e;
            var t = e;
            e = new _V_.Event(t);
            for (var n = _V_.Event.props.length, r; n; )
                r = _V_.Event.props[--n], e[r] = t[r];
            e.target || (e.target = e.srcElement || document), e.target.nodeType === 3 && (e.target = e.target.parentNode), !e.relatedTarget && e.fromElement && (e.relatedTarget = e.fromElement === e.target ? e.toElement : e.fromElement);
            if (e.pageX == null && e.clientX != null) {
                var i = e.target.ownerDocument || document, s = i.documentElement, o = i.body;
                e.pageX = e.clientX + (s && s.scrollLeft || o && o.scrollLeft || 0) - (s && s.clientLeft || o && o.clientLeft || 0), e.pageY = e.clientY + (s && s.scrollTop || o && o.scrollTop || 0) - (s && s.clientTop || o && o.clientTop || 0)
            }
            return e.which == null && (e.charCode != null || e.keyCode != null) && (e.which = e.charCode != null ? e.charCode : e.keyCode), !e.metaKey && e.ctrlKey && (e.metaKey = e.ctrlKey), !e.which && e.button !== undefined && (e.which = e.button & 1 ? 1 : e.button & 2 ? 3 : e.button & 4 ? 2 : 0), e
        }, triggerEvent: function(e, t) {
            var n = _V_.getData(e), r = e.parentNode || e.ownerDocument, i = t.type || t, s;
            n && (s = n.handler), t = typeof t == "object" ? t[_V_.expando] ? t : new _V_.Event(i, t) : new _V_.Event(i), t.type = i, s && s.call(e, t), t.result = undefined, t.target = e
        }, one: function(e, t, n) {
            _V_.addEvent(e, t, function() {
                _V_.removeEvent(e, t, arguments.callee), n.apply(this, arguments)
            })
        }}), _V_.Event = function(e, t) {
        e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || e.returnValue === !1 || e.getPreventDefault && e.getPreventDefault() ? returnTrue : returnFalse) : this.type = e, t && _V_.merge(this, t), this.timeStamp = (new Date).getTime(), this[_V_.expando] = !0
    }, _V_.Event.prototype = {preventDefault: function() {
            this.isDefaultPrevented = returnTrue;
            var e = this.originalEvent;
            if (!e)
                return;
            e.preventDefault ? e.preventDefault() : e.returnValue = !1
        }, stopPropagation: function() {
            this.isPropagationStopped = returnTrue;
            var e = this.originalEvent;
            if (!e)
                return;
            e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0
        }, stopImmediatePropagation: function() {
            this.isImmediatePropagationStopped = returnTrue, this.stopPropagation()
        }, isDefaultPrevented: returnFalse, isPropagationStopped: returnFalse, isImmediatePropagationStopped: returnFalse}, _V_.Event.props = "altKey attrChange attrName bubbles button cancelable charCode clientX clientY ctrlKey currentTarget data detail eventPhase fromElement handler keyCode metaKey newValue offsetX offsetY pageX pageY prevValue relatedNode relatedTarget screenX screenY shiftKey srcElement target toElement view wheelDelta which".split(" ");
    var JSON;
    JSON || (JSON = {}), function() {
        var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
        typeof JSON.parse != "function" && (JSON.parse = function(text, reviver) {
            function walk(e, t) {
                var n, r, i = e[t];
                if (i && typeof i == "object")
                    for (n in i)
                        Object.prototype.hasOwnProperty.call(i, n) && (r = walk(i, n), r !== undefined ? i[n] = r : delete i[n]);
                return reviver.call(e, t, i)
            }
            var j;
            text = String(text), cx.lastIndex = 0, cx.test(text) && (text = text.replace(cx, function(e) {
                return"\\u" + ("0000" + e.charCodeAt(0).toString(16)).slice(-4)
            }));
            if (/^[\],:{}\s]*$/.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]").replace(/(?:^|:|,)(?:\s*\[)+/g, "")))
                return j = eval("(" + text + ")"), typeof reviver == "function" ? walk({"": j}, "") : j;
            throw new SyntaxError("JSON.parse")
        })
    }(), _V_.Player = _V_.Component.extend({init: function(e, t, n) {
            this.tag = e;
            var r = this.el = _V_.createElement("div"), i = this.options = {}, s = i.width = e.getAttribute("width"), o = i.height = e.getAttribute("height"), u = s || 300, a = o || 150;
            e.player = r.player = this, this.ready(n), e.parentNode.insertBefore(r, e), r.appendChild(e), r.id = this.id = e.id, r.className = e.className, e.id += "_html5_api", e.className = "vjs-tech", _V_.players[r.id] = this, r.setAttribute("width", u), r.setAttribute("height", a), r.style.width = u + "px", r.style.height = a + "px", e.removeAttribute("width"), e.removeAttribute("height"), _V_.merge(i, _V_.options), _V_.merge(i, this.getVideoTagSettings()), _V_.merge(i, t), e.removeAttribute("controls"), e.removeAttribute("poster");
            if (e.hasChildNodes())
                for (var f = 0, l = e.childNodes; f < l.length; f++)
                    (l[f].nodeName == "SOURCE" || l[f].nodeName == "TRACK") && e.removeChild(l[f]);
            this.values = {}, this.addClass("vjs-paused"), this.addEvent("ended", this.onEnded), this.addEvent("play", this.onPlay), this.addEvent("pause", this.onPause), this.addEvent("progress", this.onProgress), this.addEvent("error", this.onError), i.controls && this.ready(function() {
                this.initComponents()
            }), this.textTracks = [], i.tracks && i.tracks.length > 0 && this.addTextTracks(i.tracks);
            if (!i.sources || i.sources.length == 0)
                for (var f = 0, l = i.techOrder; f < l.length; f++) {
                    var c = l[f], h = _V_[c];
                    if (h.isSupported()) {
                        this.loadTech(c);
                        break
                    }
                }
            else
                this.src(i.sources)
        }, values: {}, destroy: function() {
            this.stopTrackingProgress(), this.stopTrackingCurrentTime(), _V_.players[this.id] = null, delete _V_.players[this.id], this.tech.destroy(), this.el.parentNode.removeChild(this.el)
        }, createElement: function(e, t) {
        }, getVideoTagSettings: function() {
            var e = {sources: [], tracks: []};
            e.src = this.tag.getAttribute("src"), e.controls = this.tag.getAttribute("controls") !== null, e.poster = this.tag.getAttribute("poster"), e.preload = this.tag.getAttribute("preload"), e.autoplay = this.tag.getAttribute("autoplay") !== null, e.loop = this.tag.getAttribute("loop") !== null, e.muted = this.tag.getAttribute("muted") !== null;
            if (this.tag.hasChildNodes())
                for (var t, n = 0, r = this.tag.childNodes; n < r.length; n++)
                    t = r[n], t.nodeName == "SOURCE" && e.sources.push({src: t.getAttribute("src"), type: t.getAttribute("type"), media: t.getAttribute("media"), title: t.getAttribute("title")}), t.nodeName == "TRACK" && e.tracks.push({src: t.getAttribute("src"), kind: t.getAttribute("kind"), srclang: t.getAttribute("srclang"), label: t.getAttribute("label"), "default": t.getAttribute("default") !== null, title: t.getAttribute("title")});
            return e
        }, loadTech: function(e, t) {
            this.tech ? this.unloadTech() : e != "html5" && this.tag && (this.el.removeChild(this.tag), this.tag = !1), this.techName = e, this.isReady = !1;
            var n = function() {
                this.player.triggerReady(), this.support.progressEvent || this.player.manualProgressOn(), this.support.timeupdateEvent || this.player.manualTimeUpdatesOn()
            }, r = _V_.merge({source: t, parentEl: this.el}, this.options[e]);
            t && (t.src == this.values.src && this.values.currentTime > 0 && (r.startTime = this.values.currentTime), this.values.src = t.src), this.tech = new _V_[e](this, r), this.tech.ready(n)
        }, unloadTech: function() {
            this.tech.destroy(), this.manualProgress && this.manualProgressOff(), this.manualTimeUpdates && this.manualTimeUpdatesOff(), this.tech = !1
        }, manualProgressOn: function() {
            this.manualProgress = !0, this.trackProgress(), this.tech.addEvent("progress", function() {
                this.removeEvent("progress", arguments.callee), this.support.progressEvent = !0, this.player.manualProgressOff()
            })
        }, manualProgressOff: function() {
            this.manualProgress = !1, this.stopTrackingProgress()
        }, trackProgress: function() {
            this.progressInterval = setInterval(_V_.proxy(this, function() {
                this.values.bufferEnd < this.buffered().end(0) ? this.triggerEvent("progress") : this.bufferedPercent() == 1 && (this.stopTrackingProgress(), this.triggerEvent("progress"))
            }), 500)
        }, stopTrackingProgress: function() {
            clearInterval(this.progressInterval)
        }, manualTimeUpdatesOn: function() {
            this.manualTimeUpdates = !0, this.addEvent("play", this.trackCurrentTime), this.addEvent("pause", this.stopTrackingCurrentTime), this.tech.addEvent("timeupdate", function() {
                this.removeEvent("timeupdate", arguments.callee), this.support.timeupdateEvent = !0, this.player.manualTimeUpdatesOff()
            })
        }, manualTimeUpdatesOff: function() {
            this.manualTimeUpdates = !1, this.stopTrackingCurrentTime(), this.removeEvent("play", this.trackCurrentTime), this.removeEvent("pause", this.stopTrackingCurrentTime)
        }, trackCurrentTime: function() {
            this.currentTimeInterval && this.stopTrackingCurrentTime(), this.currentTimeInterval = setInterval(_V_.proxy(this, function() {
                this.triggerEvent("timeupdate")
            }), 250)
        }, stopTrackingCurrentTime: function() {
            clearInterval(this.currentTimeInterval)
        }, onEnded: function() {
            this.options.loop ? (this.currentTime(0), this.play()) : (this.pause(), this.currentTime(0), this.pause())
        }, onPlay: function() {
            _V_.removeClass(this.el, "vjs-paused"), _V_.addClass(this.el, "vjs-playing")
        }, onPause: function() {
            _V_.removeClass(this.el, "vjs-playing"), _V_.addClass(this.el, "vjs-paused")
        }, onProgress: function() {
            this.bufferedPercent() == 1 && this.triggerEvent("loadedalldata")
        }, onError: function(e) {
            _V_.log("Video Error", e)
        }, techCall: function(e, t) {
            if (!this.tech.isReady)
                this.tech.ready(function() {
                    this[e](t)
                });
            else
                try {
                    this.tech[e](t)
                } catch (n) {
                    _V_.log(n)
                }
        }, techGet: function(e) {
            if (this.tech.isReady)
                try {
                    return this.tech[e]()
                } catch (t) {
                    this.tech[e] === undefined ? _V_.log("Video.js: " + e + " method not defined for " + this.techName + " playback technology.", t) : t.name == "TypeError" ? (_V_.log("Video.js: " + e + " unavailable on " + this.techName + " playback technology element.", t), this.tech.isReady = !1) : _V_.log(t)
                }
            return
        }, play: function() {
            return this.techCall("play"), this
        }, pause: function() {
            return this.techCall("pause"), this
        }, paused: function() {
            return this.techGet("paused") === !1 ? !1 : !0
        }, currentTime: function(e) {
            return e !== undefined ? (this.values.lastSetCurrentTime = e, this.techCall("setCurrentTime", e), this.manualTimeUpdates && this.triggerEvent("timeupdate"), this) : this.values.currentTime = this.techGet("currentTime") || 0
        }, duration: function() {
            return parseFloat(this.techGet("duration"))
        }, remainingTime: function() {
            return this.duration() - this.currentTime()
        }, buffered: function() {
            var e = this.techGet("buffered"), t = 0, n = this.values.bufferEnd = this.values.bufferEnd || 0, r;
            return e && e.length > 0 && e.end(0) !== n && (n = e.end(0), this.values.bufferEnd = n), _V_.createTimeRange(t, n)
        }, bufferedPercent: function() {
            return this.duration() ? this.buffered().end(0) / this.duration() : 0
        }, volume: function(e) {
            var t;
            return e !== undefined ? (t = Math.max(0, Math.min(1, parseFloat(e))), this.values.volume = t, this.techCall("setVolume", t), _V_.setLocalStorage("volume", t), this) : (t = parseFloat(this.techGet("volume")), isNaN(t) ? 1 : t)
        }, muted: function(e) {
            return e !== undefined ? (this.techCall("setMuted", e), this) : this.techGet("muted") || !1
        }, width: function(e, t) {
            return e !== undefined ? (this.el.width = e, this.el.style.width = e + "px", t || this.triggerEvent("resize"), this) : parseInt(this.el.getAttribute("width"))
        }, height: function(e) {
            return e !== undefined ? (this.el.height = e, this.el.style.height = e + "px", this.triggerEvent("resize"), this) : parseInt(this.el.getAttribute("height"))
        }, size: function(e, t) {
            return this.width(e, !0).height(t)
        }, supportsFullScreen: function() {
            return this.techGet("supportsFullScreen") || !1
        }, requestFullScreen: function() {
            var e = _V_.support.requestFullScreen;
            return this.isFullScreen = !0, e ? (_V_.addEvent(document, e.eventName, this.proxy(function() {
                this.isFullScreen = document[e.isFullScreen], this.isFullScreen == 0 && _V_.removeEvent(document, e.eventName, arguments.callee), this.triggerEvent("fullscreenchange")
            })), this.tech.support.fullscreenResize === !1 && this.options.flash.iFrameMode != 1 ? (this.pause(), this.unloadTech(), _V_.addEvent(document, e.eventName, this.proxy(function() {
                _V_.removeEvent(document, e.eventName, arguments.callee), this.loadTech(this.techName, {src: this.values.src})
            })), this.el[e.requestFn]()) : this.el[e.requestFn]()) : this.tech.supportsFullScreen() ? (this.triggerEvent("fullscreenchange"), this.techCall("enterFullScreen")) : (this.triggerEvent("fullscreenchange"), this.enterFullWindow()), this
        }, cancelFullScreen: function() {
            var e = _V_.support.requestFullScreen;
            return this.isFullScreen = !1, e ? this.tech.support.fullscreenResize === !1 && this.options.flash.iFrameMode != 1 ? (this.pause(), this.unloadTech(), _V_.addEvent(document, e.eventName, this.proxy(function() {
                _V_.removeEvent(document, e.eventName, arguments.callee), this.loadTech(this.techName, {src: this.values.src})
            })), document[e.cancelFn]()) : document[e.cancelFn]() : this.tech.supportsFullScreen() ? (this.techCall("exitFullScreen"), this.triggerEvent("fullscreenchange")) : (this.exitFullWindow(), this.triggerEvent("fullscreenchange")), this
        }, enterFullWindow: function() {
            this.isFullWindow = !0, this.docOrigOverflow = document.documentElement.style.overflow, _V_.addEvent(document, "keydown", _V_.proxy(this, this.fullWindowOnEscKey)), document.documentElement.style.overflow = "hidden", _V_.addClass(document.body, "vjs-full-window"), _V_.addClass(this.el, "vjs-fullscreen"), this.triggerEvent("enterFullWindow")
        }, fullWindowOnEscKey: function(e) {
            e.keyCode == 27 && (this.isFullScreen == 1 ? this.cancelFullScreen() : this.exitFullWindow())
        }, exitFullWindow: function() {
            this.isFullWindow = !1, _V_.removeEvent(document, "keydown", this.fullWindowOnEscKey), document.documentElement.style.overflow = this.docOrigOverflow, _V_.removeClass(document.body, "vjs-full-window"), _V_.removeClass(this.el, "vjs-fullscreen"), this.triggerEvent("exitFullWindow")
        }, selectSource: function(e) {
            for (var t = 0, n = this.options.techOrder; t < n.length; t++) {
                var r = n[t], i = _V_[r];
                if (i.isSupported())
                    for (var s = 0, o = e; s < o.length; s++) {
                        var u = o[s];
                        if (i.canPlaySource.call(this, u))
                            return{source: u, tech: r}
                    }
            }
            return!1
        }, src: function(e) {
            if (e instanceof Array) {
                var t = this.selectSource(e), e, n;
                t ? (e = t.source, n = t.tech, n == this.techName ? this.src(e) : this.loadTech(n, e)) : _V_.log("No compatible source and playback technology were found.")
            } else
                e instanceof Object ? _V_[this.techName].canPlaySource(e) ? this.src(e.src) : this.src([e]) : (this.values.src = e, this.isReady ? (this.techCall("src", e), this.options.preload == "auto" && this.load(), this.options.autoplay && this.play()) : this.ready(function() {
                    this.src(e)
                }));
            return this
        }, load: function() {
            return this.techCall("load"), this
        }, currentSrc: function() {
            return this.techGet("currentSrc") || this.values.src || ""
        }, preload: function(e) {
            return e !== undefined ? (this.techCall("setPreload", e), this.options.preload = e, this) : this.techGet("preload")
        }, autoplay: function(e) {
            return e !== undefined ? (this.techCall("setAutoplay", e), this.options.autoplay = e, this) : this.techGet("autoplay", e)
        }, loop: function(e) {
            return e !== undefined ? (this.techCall("setLoop", e), this.options.loop = e, this) : this.techGet("loop")
        }, controls: function() {
            return this.options.controls
        }, poster: function() {
            return this.techGet("poster")
        }, error: function() {
            return this.techGet("error")
        }, ended: function() {
            return this.techGet("ended")
        }}), function() {
        var e, t, n, r, i = _V_.Player.prototype;
        document.cancelFullscreen !== undefined ? (e = "requestFullscreen", t = "exitFullscreen", n = "fullscreenchange", r = "fullScreen") : _V_.each(["moz", "webkit"], function(i) {
            (i != "moz" || document.mozFullScreenEnabled) && document[i + "CancelFullScreen"] !== undefined && (e = i + "RequestFullScreen", t = i + "CancelFullScreen", n = i + "fullscreenchange", i == "webkit" ? r = i + "IsFullScreen" : r = i + "FullScreen")
        }), e && (_V_.support.requestFullScreen = {requestFn: e, cancelFn: t, eventName: n, isFullScreen: r})
    }(), _V_.PlaybackTech = _V_.Component.extend({init: function(e, t) {
        }, onClick: function() {
            this.player.options.controls && _V_.PlayToggle.prototype.onClick.call(this)
        }}), _V_.apiMethods = "play,pause,paused,currentTime,setCurrentTime,duration,buffered,volume,setVolume,muted,setMuted,width,height,supportsFullScreen,enterFullScreen,src,load,currentSrc,preload,setPreload,autoplay,setAutoplay,loop,setLoop,error,networkState,readyState,seeking,initialTime,startOffsetTime,played,seekable,ended,videoTracks,audioTracks,videoWidth,videoHeight,textTracks,defaultPlaybackRate,playbackRate,mediaGroup,controller,controls,defaultMuted".split(","), _V_.each(_V_.apiMethods, function(e) {
        _V_.PlaybackTech.prototype[e] = function() {
            throw new Error("The '" + e + "' method is not available on the playback technology's API")
        }
    }), _V_.html5 = _V_.PlaybackTech.extend({init: function(e, t, n) {
            this.player = e, this.el = this.createElement(), this.ready(n), this.addEvent("click", this.proxy(this.onClick));
            var r = t.source;
            r && this.el.currentSrc == r.src ? e.triggerEvent("loadstart") : r && (this.el.src = r.src), e.ready(function() {
                this.options.autoplay && this.paused() && (this.tag.poster = null, this.play())
            }), this.setupTriggers(), this.triggerReady()
        }, destroy: function() {
            this.player.tag = !1, this.removeTriggers(), this.el.parentNode.removeChild(this.el)
        }, createElement: function() {
            var e = _V_.html5, t = this.player, n = t.tag, r;
            if (!n || this.support.movingElementInDOM === !1)
                n && t.el.removeChild(n), r = _V_.createElement("video", {id: n.id || t.el.id + "_html5_api", className: n.className || "vjs-tech"}), n = r, _V_.insertFirst(n, t.el);
            return _V_.each(["autoplay", "preload", "loop", "muted"], function(e) {
                t.options[e] !== null && (n[e] = t.options[e])
            }, this), n
        }, setupTriggers: function() {
            _V_.each.call(this, _V_.html5.events, function(e) {
                _V_.addEvent(this.el, e, _V_.proxy(this.player, this.eventHandler))
            })
        }, removeTriggers: function() {
            _V_.each.call(this, _V_.html5.events, function(e) {
                _V_.removeEvent(this.el, e, _V_.proxy(this.player, this.eventHandler))
            })
        }, eventHandler: function(e) {
            e.stopPropagation(), this.triggerEvent(e)
        }, play: function() {
            this.el.play()
        }, pause: function() {
            this.el.pause()
        }, paused: function() {
            return this.el.paused
        }, currentTime: function() {
            return this.el.currentTime
        }, setCurrentTime: function(e) {
            try {
                this.el.currentTime = e
            } catch (t) {
                _V_.log(t, "Video isn't ready. (VideoJS)")
            }
        }, duration: function() {
            return this.el.duration || 0
        }, buffered: function() {
            return this.el.buffered
        }, volume: function() {
            return this.el.volume
        }, setVolume: function(e) {
            this.el.volume = e
        }, muted: function() {
            return this.el.muted
        }, setMuted: function(e) {
            this.el.muted = e
        }, width: function() {
            return this.el.offsetWidth
        }, height: function() {
            return this.el.offsetHeight
        }, supportsFullScreen: function() {
            return typeof this.el.webkitEnterFullScreen == "function" && !navigator.userAgent.match("Chrome") && !navigator.userAgent.match("Mac OS X 10.5") ? !0 : !1
        }, enterFullScreen: function() {
            try {
                this.el.webkitEnterFullScreen()
            } catch (e) {
                e.code == 11 && _V_.log("VideoJS: Video not ready.")
            }
        }, src: function(e) {
            this.el.src = e
        }, load: function() {
            this.el.load()
        }, currentSrc: function() {
            return this.el.currentSrc
        }, preload: function() {
            return this.el.preload
        }, setPreload: function(e) {
            this.el.preload = e
        }, autoplay: function() {
            return this.el.autoplay
        }, setAutoplay: function(e) {
            this.el.autoplay = e
        }, loop: function() {
            return this.el.loop
        }, setLoop: function(e) {
            this.el.loop = e
        }, error: function() {
            return this.el.error
        }, seeking: function() {
            return this.el.seeking
        }, ended: function() {
            return this.el.ended
        }, controls: function() {
            return this.player.options.controls
        }, defaultMuted: function() {
            return this.el.defaultMuted
        }}), _V_.html5.isSupported = function() {
        return!!document.createElement("video").canPlayType
    }, _V_.html5.canPlaySource = function(e) {
        return!!document.createElement("video").canPlayType(e.type)
    }, _V_.html5.events = "loadstart,suspend,abort,error,emptied,stalled,loadedmetadata,loadeddata,canplay,canplaythrough,playing,waiting,seeking,seeked,ended,durationchange,timeupdate,progress,play,pause,ratechange,volumechange".split(","), _V_.html5.prototype.support = {fullscreen: typeof _V_.testVid.webkitEnterFullScreen !== undefined ? !_V_.ua.match("Chrome") && !_V_.ua.match("Mac OS X 10.5") ? !0 : !1 : !1, movingElementInDOM: !_V_.isIOS()}, _V_.isAndroid() && _V_.androidVersion() < 3 && (document.createElement("video").constructor.prototype.canPlayType = function(e) {
        return e && e.toLowerCase().indexOf("video/mp4") != -1 ? "maybe" : ""
    }), _V_.flash = _V_.PlaybackTech.extend({init: function(e, t) {
            this.player = e;
            var n = t.source, r = t.parentEl, i = this.el = _V_.createElement("div", {id: r.id + "_temp_flash"}), s = e.el.id + "_flash_api", o = e.options, u = _V_.merge({readyFunction: "_V_.flash.onReady", eventProxyFunction: "_V_.flash.onEvent", errorEventProxyFunction: "_V_.flash.onError", autoplay: o.autoplay, preload: o.preload, loop: o.loop, muted: o.muted}, t.flashVars), a = _V_.merge({wmode: "opaque", bgcolor: "#000000"}, t.params), f = _V_.merge({id: s, name: s, "class": "vjs-tech"}, t.attributes);
            n && (u.src = encodeURIComponent(_V_.getAbsoluteURL(n.src))), _V_.insertFirst(i, r), t.startTime && this.ready(function() {
                this.load(), this.play(), this.currentTime(t.startTime)
            });
            if (t.iFrameMode == 1 && !_V_.isFF) {
                var l = _V_.createElement("iframe", {id: s + "_iframe", name: s + "_iframe", className: "vjs-tech", scrolling: "no", marginWidth: 0, marginHeight: 0, frameBorder: 0});
                u.readyFunction = "ready", u.eventProxyFunction = "events", u.errorEventProxyFunction = "errors", _V_.addEvent(l, "load", _V_.proxy(this, function() {
                    var e, n, r, i = l.contentWindow, s = "";
                    e = l.contentDocument ? l.contentDocument : l.contentWindow.document, e.write(_V_.flash.getEmbedCode(t.swf, u, a, f)), i.player = this.player, i.ready = _V_.proxy(this.player, function(t) {
                        var n = e.getElementById(t), r = this, i = r.tech;
                        i.el = n, _V_.addEvent(n, "click", i.proxy(i.onClick)), _V_.flash.checkReady(i)
                    }), i.events = _V_.proxy(this.player, function(e, t, n) {
                        var r = this;
                        r && r.techName == "flash" && r.triggerEvent(t)
                    }), i.errors = _V_.proxy(this.player, function(e, t) {
                        _V_.log("Flash Error", t)
                    })
                })), i.parentNode.replaceChild(l, i)
            } else
                _V_.flash.embed(t.swf, i, u, a, f)
        }, destroy: function() {
            this.el.parentNode.removeChild(this.el)
        }, play: function() {
            this.el.vjs_play()
        }, pause: function() {
            this.el.vjs_pause()
        }, src: function(e) {
            e = _V_.getAbsoluteURL(e), this.el.vjs_src(e);
            if (this.player.autoplay()) {
                var t = this;
                setTimeout(function() {
                    t.play()
                }, 0)
            }
        }, load: function() {
            this.el.vjs_load()
        }, poster: function() {
            this.el.vjs_getProperty("poster")
        }, buffered: function() {
            return _V_.createTimeRange(0, this.el.vjs_getProperty("buffered"))
        }, supportsFullScreen: function() {
            return!1
        }, enterFullScreen: function() {
            return!1
        }}), function() {
        var e = _V_.flash.prototype, t = "preload,currentTime,defaultPlaybackRate,playbackRate,autoplay,loop,mediaGroup,controller,controls,volume,muted,defaultMuted".split(","), n = "error,currentSrc,networkState,readyState,seeking,initialTime,duration,startOffsetTime,paused,played,seekable,ended,videoTracks,audioTracks,videoWidth,videoHeight,textTracks".split(","), r = "load,play,pause".split(",");
        createSetter = function(t) {
            var n = t.charAt(0).toUpperCase() + t.slice(1);
            e["set" + n] = function(e) {
                return this.el.vjs_setProperty(t, e)
            }
        }, createGetter = function(t) {
            e[t] = function() {
                return this.el.vjs_getProperty(t)
            }
        }, _V_.each(t, function(e) {
            createGetter(e), createSetter(e)
        }), _V_.each(n, function(e) {
            createGetter(e)
        })
    }(), _V_.flash.isSupported = function() {
        return _V_.flash.version()[0] >= 10
    }, _V_.flash.canPlaySource = function(e) {
        if (e.type in _V_.flash.prototype.support.formats)
            return"maybe"
    }, _V_.flash.prototype.support = {formats: {"video/flv": "FLV", "video/x-flv": "FLV", "video/mp4": "MP4", "video/m4v": "MP4"}, progressEvent: !1, timeupdateEvent: !1, fullscreenResize: !1, parentResize: !_V_.ua.match("Firefox")}, _V_.flash.onReady = function(e) {
        var t = _V_.el(e), n = t.player || t.parentNode.player, r = n.tech;
        t.player = n, r.el = t, r.addEvent("click", r.onClick), _V_.flash.checkReady(r)
    }, _V_.flash.checkReady = function(e) {
        e.el.vjs_getProperty ? e.triggerReady() : setTimeout(function() {
            _V_.flash.checkReady(e)
        }, 50)
    }, _V_.flash.onEvent = function(e, t) {
        var n = _V_.el(e).player;
        n.triggerEvent(t)
    }, _V_.flash.onError = function(e, t) {
        var n = _V_.el(e).player;
        n.triggerEvent("error"), _V_.log("Flash Error", t, e)
    }, _V_.flash.version = function() {
        var e = "0,0,0";
        try {
            e = (new ActiveXObject("ShockwaveFlash.ShockwaveFlash")).GetVariable("$version").replace(/\D+/g, ",").match(/^,?(.+),?$/)[1]
        } catch (t) {
            try {
                navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin && (e = (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]).description.replace(/\D+/g, ",").match(/^,?(.+),?$/)[1])
            } catch (t) {
            }
        }
        return e.split(",")
    }, _V_.flash.embed = function(e, t, n, r, i) {
        var s = _V_.flash.getEmbedCode(e, n, r, i), o = _V_.createElement("div", {innerHTML: s}).childNodes[0], u = t.parentNode;
        t.parentNode.replaceChild(o, t);
        if (_V_.isIE()) {
            var a = u.childNodes[0];
            setTimeout(function() {
                a.style.display = "block"
            }, 1e3)
        }
        return o
    }, _V_.flash.getEmbedCode = function(e, t, n, r) {
        var i = '<object type="application/x-shockwave-flash"', s = "", o = "";
        return attrsString = "", t && _V_.eachProp(t, function(e, t) {
            s += e + "=" + t + "&amp;"
        }), n = _V_.merge({movie: e, flashvars: s, allowScriptAccess: "always", allowNetworking: "all"}, n), _V_.eachProp(n, function(e, t) {
            o += '<param name="' + e + '" value="' + t + '" />'
        }), r = _V_.merge({data: e, width: "100%", height: "100%"}, r), _V_.eachProp(r, function(e, t) {
            attrsString += e + '="' + t + '" '
        }), i + attrsString + ">" + o + "</object>"
    }, _V_.merge(_V_.Player.prototype, {addTextTracks: function(e) {
            var t = this.textTracks = this.textTracks ? this.textTracks : [], n = 0, r = e.length, i, s;
            for (; n < r; n++)
                s = _V_.uc(e[n].kind || "subtitles"), i = new _V_[s + "Track"](this, e[n]), t.push(i), i["default"] && this.ready(_V_.proxy(i, i.show));
            return this
        }, showTextTrack: function(e, t) {
            var n = this.textTracks, r = 0, i = n.length, s, o, u;
            for (; r < i; r++)
                s = n[r], s.id === e ? (s.show(), o = s) : t && s.kind == t && s.mode > 0 && s.disable();
            return u = o ? o.kind : t ? t : !1, u && this.triggerEvent(u + "trackchange"), this
        }}), _V_.Track = _V_.Component.extend({init: function(e, t) {
            this._super(e, t), _V_.merge(this, {id: t.id || "vjs_" + t.kind + "_" + t.language + "_" + _V_.guid++, src: t.src, "default": t["default"], title: t.title, language: t.srclang, label: t.label, cues: [], activeCues: [], readyState: 0, mode: 0})
        }, createElement: function() {
            return this._super("div", {className: "vjs-" + this.kind + " vjs-text-track"})
        }, show: function() {
            this.activate(), this.mode = 2, this._super()
        }, hide: function() {
            this.activate(), this.mode = 1, this._super
                    ()
        }, disable: function() {
            this.mode == 2 && this.hide(), this.deactivate(), this.mode = 0
        }, activate: function() {
            this.readyState == 0 && this.load(), this.mode == 0 && (this.player.addEvent("timeupdate", this.proxy(this.update, this.id)), this.player.addEvent("ended", this.proxy(this.reset, this.id)), (this.kind == "captions" || this.kind == "subtitles") && this.player.textTrackDisplay.addComponent(this))
        }, deactivate: function() {
            this.player.removeEvent("timeupdate", this.proxy(this.update, this.id)), this.player.removeEvent("ended", this.proxy(this.reset, this.id)), this.reset(), this.player.textTrackDisplay.removeComponent(this)
        }, load: function() {
            this.readyState == 0 && (this.readyState = 1, _V_.get(this.src, this.proxy(this.parseCues), this.proxy(this.onError)))
        }, onError: function(e) {
            this.error = e, this.readyState = 3, this.triggerEvent("error")
        }, parseCues: function(e) {
            var t, n, r, i = e.split("\n"), s = "", o;
            for (var u = 1, a = i.length; u < a; u++) {
                s = _V_.trim(i[u]);
                if (s) {
                    s.indexOf("-->") == -1 ? (o = s, s = _V_.trim(i[++u])) : o = this.cues.length, t = {id: o, index: this.cues.length}, n = s.split(" --> "), t.startTime = this.parseCueTime(n[0]), t.endTime = this.parseCueTime(n[1]), r = [];
                    while (i[++u] && (s = _V_.trim(i[u])))
                        r.push(s);
                    t.text = r.join("<br/>"), this.cues.push(t)
                }
            }
            this.readyState = 2, this.triggerEvent("loaded")
        }, parseCueTime: function(e) {
            var t = e.split(":"), n = 0, r, i, s, o, u, a;
            return t.length == 3 ? (r = t[0], i = t[1], s = t[2]) : (r = 0, i = t[0], s = t[1]), s = s.split(/\s+/), o = s.splice(0, 1)[0], o = o.split(/\.|,/), u = parseFloat(o[1]), o = o[0], n += parseFloat(r) * 3600, n += parseFloat(i) * 60, n += parseFloat(o), u && (n += u / 1e3), n
        }, update: function() {
            if (this.cues.length > 0) {
                var e = this.player.currentTime();
                if (this.prevChange === undefined || e < this.prevChange || this.nextChange <= e) {
                    var t = this.cues, n = this.player.duration(), r = 0, i = !1, s = [], o, u, a = "", f, l, c;
                    e >= this.nextChange || this.nextChange === undefined ? l = this.firstActiveIndex !== undefined ? this.firstActiveIndex : 0 : (i = !0, l = this.lastActiveIndex !== undefined ? this.lastActiveIndex : t.length - 1);
                    for (; ; ) {
                        f = t[l];
                        if (f.endTime <= e)
                            r = Math.max(r, f.endTime), f.active && (f.active = !1);
                        else if (e < f.startTime) {
                            n = Math.min(n, f.startTime), f.active && (f.active = !1);
                            if (!i)
                                break
                        } else
                            i ? (s.splice(0, 0, f), u === undefined && (u = l), o = l) : (s.push(f), o === undefined && (o = l), u = l), n = Math.min(n, f.endTime), r = Math.max(r, f.startTime), f.active = !0;
                        if (i) {
                            if (l === 0)
                                break;
                            l--
                        } else {
                            if (l === t.length - 1)
                                break;
                            l++
                        }
                    }
                    this.activeCues = s, this.nextChange = n, this.prevChange = r, this.firstActiveIndex = o, this.lastActiveIndex = u, this.updateDisplay(), this.triggerEvent("cuechange")
                }
            }
        }, updateDisplay: function() {
            var e = this.activeCues, t = "", n = 0, r = e.length;
            for (; n < r; n++)
                t += "<span class='vjs-tt-cue'>" + e[n].text + "</span>";
            this.el.innerHTML = t
        }, reset: function() {
            this.nextChange = 0, this.prevChange = this.player.duration(), this.firstActiveIndex = 0, this.lastActiveIndex = 0
        }}), _V_.CaptionsTrack = _V_.Track.extend({kind: "captions"}), _V_.SubtitlesTrack = _V_.Track.extend({kind: "subtitles"}), _V_.ChaptersTrack = _V_.Track.extend({kind: "chapters"}), _V_.TextTrackDisplay = _V_.Component.extend({createElement: function() {
            return this._super("div", {className: "vjs-text-track-display"})
        }}), _V_.TextTrackMenuItem = _V_.MenuItem.extend({init: function(e, t) {
            var n = this.track = t.track;
            t.label = n.label, t.selected = n["default"], this._super(e, t), this.player.addEvent(n.kind + "trackchange", _V_.proxy(this, this.update))
        }, onClick: function() {
            this._super(), this.player.showTextTrack(this.track.id, this.track.kind)
        }, update: function() {
            this.track.mode == 2 ? this.selected(!0) : this.selected(!1)
        }}), _V_.OffTextTrackMenuItem = _V_.TextTrackMenuItem.extend({init: function(e, t) {
            t.track = {kind: t.kind, player: e, label: "Off"}, this._super(e, t)
        }, onClick: function() {
            this._super(), this.player.showTextTrack(this.track.id, this.track.kind)
        }, update: function() {
            var e = this.player.textTracks, t = 0, n = e.length, r, i = !0;
            for (; t < n; t++)
                r = e[t], r.kind == this.track.kind && r.mode == 2 && (i = !1);
            i ? this.selected(!0) : this.selected(!1)
        }}), _V_.TextTrackButton = _V_.Button.extend({init: function(e, t) {
            this._super(e, t), this.menu = this.createMenu(), this.items.length === 0 && this.hide()
        }, createMenu: function() {
            var e = new _V_.Menu(this.player);
            return e.el.appendChild(_V_.createElement("li", {className: "vjs-menu-title", innerHTML: _V_.uc(this.kind)})), e.addItem(new _V_.OffTextTrackMenuItem(this.player, {kind: this.kind})), this.items = this.createItems(), this.each(this.items, function(t) {
                e.addItem(t)
            }), this.addComponent(e), e
        }, createItems: function() {
            var e = [];
            return this.each(this.player.textTracks, function(t) {
                t.kind === this.kind && e.push(new _V_.TextTrackMenuItem(this.player, {track: t}))
            }), e
        }, buildCSSClass: function() {
            return this.className + " vjs-menu-button " + this._super()
        }, onFocus: function() {
            this.menu.lockShowing(), _V_.one(this.menu.el.childNodes[this.menu.el.childNodes.length - 1], "blur", this.proxy(function() {
                this.menu.unlockShowing()
            }))
        }, onBlur: function() {
        }, onClick: function() {
            this.one("mouseout", this.proxy(function() {
                this.menu.unlockShowing(), this.el.blur()
            }))
        }}), _V_.CaptionsButton = _V_.TextTrackButton.extend({kind: "captions", buttonText: "Captions", className: "vjs-captions-button"}), _V_.SubtitlesButton = _V_.TextTrackButton.extend({kind: "subtitles", buttonText: "Subtitles", className: "vjs-subtitles-button"}), _V_.ChaptersButton = _V_.TextTrackButton.extend({kind: "chapters", buttonText: "Chapters", className: "vjs-chapters-button", createItems: function(e) {
            var t = [];
            return this.each(this.player.textTracks, function(e) {
                e.kind === this.kind && t.push(new _V_.TextTrackMenuItem(this.player, {track: e}))
            }), t
        }, createMenu: function() {
            var e = this.player.textTracks, t = 0, n = e.length, r, i, s = this.items = [];
            for (; t < n; t++) {
                r = e[t];
                if (r.kind == this.kind && r["default"]) {
                    if (r.readyState < 2) {
                        this.chaptersTrack = r, r.addEvent("loaded", this.proxy(this.createMenu));
                        return
                    }
                    i = r;
                    break
                }
            }
            var o = this.menu = new _V_.Menu(this.player);
            o.el.appendChild(_V_.createElement("li", {className: "vjs-menu-title", innerHTML: _V_.uc(this.kind)}));
            if (i) {
                var u = i.cues, t = 0, n = u.length, a, f;
                for (; t < n; t++)
                    a = u[t], f = new _V_.ChaptersTrackMenuItem(this.player, {track: i, cue: a}), s.push(f), o.addComponent(f)
            }
            return this.addComponent(o), this.items.length > 0 && this.show(), o
        }}), _V_.ChaptersTrackMenuItem = _V_.MenuItem.extend({init: function(e, t) {
            var n = this.track = t.track, r = this.cue = t.cue, i = e.currentTime();
            t.label = r.text, t.selected = r.startTime <= i && i < r.endTime, this._super(e, t), n.addEvent("cuechange", _V_.proxy(this, this.update))
        }, onClick: function() {
            this._super(), this.player.currentTime(this.cue.startTime), this.update(this.cue.startTime)
        }, update: function(e) {
            var t = this.cue, n = this.player.currentTime();
            t.startTime <= n && n < t.endTime ? this.selected(!0) : this.selected(!1)
        }}), _V_.merge(_V_.ControlBar.prototype.options.components, {subtitlesButton: {}, captionsButton: {}, chaptersButton: {}}), _V_.autoSetup = function() {
        var e, t, n, r = document.getElementsByTagName("video");
        if (r && r.length > 0)
            for (var i = 0, s = r.length; i < s; i++) {
                t = r[i];
                if (!t || !t.getAttribute) {
                    _V_.autoSetupTimeout(1);
                    break
                }
                t.player === undefined && (e = t.getAttribute("data-setup"), e !== null && (e = JSON.parse(e || "{}"), n = _V_(t, e)))
            }
        else
            _V_.windowLoaded || _V_.autoSetupTimeout(1)
    }, _V_.autoSetupTimeout = function(e) {
        setTimeout(_V_.autoSetup, e)
    }, _V_.addEvent(window, "load", function() {
        _V_.windowLoaded = !0
    }), _V_.autoSetup(), window.VideoJS = window._V_ = VideoJS
}(window);
var wysihtml5ParserRules = {classes: {"wysiwyg-clear-both": 1, "wysiwyg-clear-left": 1, "wysiwyg-clear-right": 1, "wysiwyg-color-aqua": 1, "wysiwyg-color-black": 1, "wysiwyg-color-blue": 1, "wysiwyg-color-fuchsia": 1, "wysiwyg-color-gray": 1, "wysiwyg-color-green": 1, "wysiwyg-color-lime": 1, "wysiwyg-color-maroon": 1, "wysiwyg-color-navy": 1, "wysiwyg-color-olive": 1, "wysiwyg-color-purple": 1, "wysiwyg-color-red": 1, "wysiwyg-color-silver": 1, "wysiwyg-color-teal": 1, "wysiwyg-color-white": 1, "wysiwyg-color-yellow": 1, "wysiwyg-float-left": 1, "wysiwyg-float-right": 1, "wysiwyg-font-size-large": 1, "wysiwyg-font-size-larger": 1, "wysiwyg-font-size-medium": 1, "wysiwyg-font-size-small": 1, "wysiwyg-font-size-smaller": 1, "wysiwyg-font-size-x-large": 1, "wysiwyg-font-size-x-small": 1, "wysiwyg-font-size-xx-large": 1, "wysiwyg-font-size-xx-small": 1, "wysiwyg-text-align-center": 1, "wysiwyg-text-align-justify": 1, "wysiwyg-text-align-left": 1, "wysiwyg-text-align-right": 1}, tags: {tr: {add_class: {align: "align_text"}}, strike: {remove: 1}, form: {rename_tag: "div"}, rt: {rename_tag: "span"}, code: {}, acronym: {rename_tag: "span"}, br: {add_class: {clear: "clear_br"}}, details: {rename_tag: "div"}, h4: {add_class: {align: "align_text"}}, em: {}, title: {remove: 1}, multicol: {rename_tag: "div"}, figure: {rename_tag: "div"}, xmp: {rename_tag: "span"}, small: {rename_tag: "span", set_class: "wysiwyg-font-size-smaller"}, area: {remove: 1}, time: {rename_tag: "span"}, dir: {rename_tag: "ul"}, bdi: {rename_tag: "span"}, command: {remove: 1}, ul: {}, progress: {rename_tag: "span"}, dfn: {rename_tag: "span"}, iframe: {remove: 1}, figcaption: {rename_tag: "div"}, a: {check_attributes: {href: "url"}, set_attributes: {rel: "nofollow", target: "_blank"}}, img: {check_attributes: {width: "numbers", alt: "alt", src: "url", height: "numbers"}, add_class: {align: "align_img"}}, rb: {rename_tag: "span"}, footer: {rename_tag: "div"}, noframes: {remove: 1}, abbr: {rename_tag: "span"}, u: {}, bgsound: {remove: 1}, sup: {rename_tag: "span"}, address: {rename_tag: "div"}, basefont: {remove: 1}, nav: {rename_tag: "div"}, h1: {add_class: {align: "align_text"}}, head: {remove: 1}, tbody: {add_class: {align: "align_text"}}, dd: {rename_tag: "div"}, s: {rename_tag: "span"}, li: {}, td: {check_attributes: {rowspan: "numbers", colspan: "numbers"}, add_class: {align: "align_text"}}, object: {remove: 1}, div: {add_class: {align: "align_text"}}, option: {rename_tag: "span"}, select: {rename_tag: "span"}, i: {}, track: {remove: 1}, wbr: {remove: 1}, fieldset: {rename_tag: "div"}, big: {rename_tag: "span", set_class: "wysiwyg-font-size-larger"}, button: {rename_tag: "span"}, noscript: {remove: 1}, svg: {remove: 1}, input: {remove: 1}, table: {}, keygen: {remove: 1}, h5: {add_class: {align: "align_text"}}, meta: {remove: 1}, map: {rename_tag: "div"}, isindex: {remove: 1}, mark: {rename_tag: "span"}, caption: {add_class: {align: "align_text"}}, tfoot: {add_class: {align: "align_text"}}, base: {remove: 1}, video: {remove: 1}, strong: {}, canvas: {remove: 1}, output: {rename_tag: "span"}, marquee: {rename_tag: "span"}, b: {}, q: {check_attributes: {cite: "url"}}, applet: {remove: 1}, span: {}, rp: {rename_tag: "span"}, spacer: {remove: 1}, source: {remove: 1}, aside: {rename_tag: "div"}, frame: {remove: 1}, section: {rename_tag: "div"}, body: {rename_tag: "div"}, ol: {}, nobr: {rename_tag: "span"}, html: {rename_tag: "div"}, summary: {rename_tag: "span"}, "var": {rename_tag: "span"}, del: {remove: 1}, blockquote: {check_attributes: {cite: "url"}}, style: {remove: 1}, device: {remove: 1}, meter: {rename_tag: "span"}, h3: {add_class: {align: "align_text"}}, textarea: {rename_tag: "span"}, embed: {remove: 1}, hgroup: {rename_tag: "div"}, font: {rename_tag: "span", add_class: {size: "size_font"}}, tt: {rename_tag: "span"}, noembed: {remove: 1}, thead: {add_class: {align: "align_text"}}, blink: {rename_tag: "span"}, plaintext: {rename_tag: "span"}, xml: {remove: 1}, h6: {add_class: {align: "align_text"}}, param: {remove: 1}, th: {check_attributes: {rowspan: "numbers", colspan: "numbers"}, add_class: {align: "align_text"}}, legend: {rename_tag: "span"}, hr: {}, label: {rename_tag: "span"}, dl: {rename_tag: "div"}, kbd: {rename_tag: "span"}, listing: {rename_tag: "div"}, dt: {rename_tag: "span"}, nextid: {remove: 1}, pre: {}, center: {rename_tag: "div", set_class: "wysiwyg-text-align-center"}, audio: {remove: 1}, datalist: {rename_tag: "span"}, samp: {rename_tag: "span"}, col: {remove: 1}, article: {rename_tag: "div"}, cite: {}, link: {remove: 1}, script: {remove: 1}, bdo: {rename_tag: "span"}, menu: {rename_tag: "ul"}, colgroup: {remove: 1}, ruby: {rename_tag: "span"}, h2: {add_class: {align: "align_text"}}, ins: {rename_tag: "span"}, p: {add_class: {align: "align_text"}}, sub: {rename_tag: "span"}, comment: {remove: 1}, frameset: {remove: 1}, optgroup: {rename_tag: "span"}, header: {rename_tag: "div"}}}, wysihtml5 = {version: "0.3.0_rc2", commands: {}, dom: {}, quirks: {}, toolbar: {}, lang: {}, selection: {}, views: {}, INVISIBLE_SPACE: "﻿", EMPTY_FUNCTION: function() {
    }, ELEMENT_NODE: 1, TEXT_NODE: 3, BACKSPACE_KEY: 8, ENTER_KEY: 13, ESCAPE_KEY: 27, SPACE_KEY: 32, DELETE_KEY: 46};
window.rangy = function() {
    function e(e, t) {
        var n = typeof e[t];
        return n == f || n == a && !!e[t] || "unknown" == n
    }
    function t(e, t) {
        return typeof e[t] == a && !!e[t]
    }
    function n(e, t) {
        return typeof e[t] != l
    }
    function r(e) {
        return function(t, n) {
            for (var r = n.length; r--; )
                if (!e(t, n[r]))
                    return!1;
            return!0
        }
    }
    function i(e) {
        return e && v(e, d) && g(e, p)
    }
    function s(e) {
        window.alert("Rangy not supported in your browser. Reason: " + e), y.initialized = !0, y.supported = !1
    }
    function o() {
        if (!y.initialized) {
            var n, r = !1, o = !1;
            e(document, "createRange") && (n = document.createRange(), v(n, h) && g(n, c) && (r = !0), n.detach()), (n = t(document, "body") ? document.body : document.getElementsByTagName("body")[0]) && e(n, "createTextRange") && (n = n.createTextRange(), i(n) && (o = !0)), !r && !o && s("Neither Range nor TextRange are implemented"), y.initialized = !0, y.features = {implementsDomRange: r, implementsTextRange: o}, r = w.concat(b), o = 0;
            for (n = r.length; o < n; ++o)
                try {
                    r[o](y)
                } catch (u) {
                    t(window, "console") && e(window.console, "log") && window.console.log("Init listener threw an exception. Continuing.", u)
                }
        }
    }
    function u(e) {
        this.name = e, this.supported = this.initialized = !1
    }
    var a = "object", f = "function", l = "undefined", c = "startContainer,startOffset,endContainer,endOffset,collapsed,commonAncestorContainer,START_TO_START,START_TO_END,END_TO_START,END_TO_END".split(","), h = "setStart,setStartBefore,setStartAfter,setEnd,setEndBefore,setEndAfter,collapse,selectNode,selectNodeContents,compareBoundaryPoints,deleteContents,extractContents,cloneContents,insertNode,surroundContents,cloneRange,toString,detach".split(","), p = "boundingHeight,boundingLeft,boundingTop,boundingWidth,htmlText,text".split(","), d = "collapse,compareEndPoints,duplicate,getBookmark,moveToBookmark,moveToElementText,parentElement,pasteHTML,select,setEndPoint,getBoundingClientRect".split(","), v = r(e), m = r(t), g = r(n), y = {version: "1.2.2", initialized: !1, supported: !0, util: {isHostMethod: e, isHostObject: t, isHostProperty: n, areHostMethods: v, areHostObjects: m, areHostProperties: g, isTextRange: i}, features: {}, modules: {}, config: {alertOnWarn: !1, preferTextRange: !1}};
    y.fail = s, y.warn = function(e) {
        e = "Rangy warning: " + e, y.config.alertOnWarn ? window.alert(e) : typeof window.console != l && typeof window.console.log != l && window.console.log(e)
    }, {}.hasOwnProperty ? y.util.extend = function(e, t) {
        for (var n in t)
            t.hasOwnProperty(n) && (e[n] = t[n])
    } : s("hasOwnProperty not supported");
    var b = [], w = [];
    y.init = o, y.addInitListener = function(e) {
        y.initialized ? e(y) : b.push(e)
    };
    var E = [];
    y.addCreateMissingNativeApiListener = function(e) {
        E.push(e)
    }, y.createMissingNativeApi = function(e) {
        e = e || window, o();
        for (var t = 0, n = E.length; t < n; ++t)
            E[t](e)
    }, u.prototype.fail = function(e) {
        throw this.initialized = !0, this.supported = !1, Error("Module '" + this.name + "' failed to load: " + e)
    }, u.prototype.warn = function(e) {
        y.warn("Module " + this.name + ": " + e)
    }, u.prototype.createError = function(e) {
        return Error("Error in Rangy " + this.name + " module: " + e)
    }, y.createModule = function(e, t) {
        var n = new u(e);
        y.modules[e] = n, w.push(function(e) {
            t(e, n), n.initialized = !0, n.supported = !0
        })
    }, y.requireModules = function(e) {
        for (var t = 0, n = e.length, r, i; t < n; ++t) {
            i = e[t], r = y.modules[i];
            if (!r || !(r instanceof u))
                throw Error("Module '" + i + "' not found");
            if (!r.supported)
                throw Error("Module '" + i + "' not supported")
        }
    };
    var S = !1, m = function() {
        S || (S = !0, y.initialized || o())
    };
    if (typeof window == l)
        s("No window found");
    else {
        if (typeof document != l)
            return e(document, "addEventListener") && document.addEventListener("DOMContentLoaded", m, !1), e(window, "addEventListener") ? window.addEventListener("load", m, !1) : e(window, "attachEvent") ? window.attachEvent("onload", m) : s("Window does not have required addEventListener or attachEvent method"), y;
        s("No document found")
    }
}(), rangy.createModule("DomUtil", function(e, t) {
    function n(e) {
        for (var t = 0; e = e.previousSibling; )
            t++;
        return t
    }
    function r(e, t) {
        var n = [], r;
        for (r = e; r; r = r.parentNode)
            n.push(r);
        for (r = t; r; r = r.parentNode)
            if (v(n, r))
                return r;
        return null
    }
    function i(e, t, n) {
        for (n = n ? e : e.parentNode; n; ) {
            e = n.parentNode;
            if (e === t)
                return n;
            n = e
        }
        return null
    }
    function s(e) {
        return e = e.nodeType, 3 == e || 4 == e || 8 == e
    }
    function o(e, t) {
        var n = t.nextSibling, r = t.parentNode;
        return n ? r.insertBefore(e, n) : r.appendChild(e), e
    }
    function u(e) {
        if (9 == e.nodeType)
            return e;
        if (typeof e.ownerDocument != h)
            return e.ownerDocument;
        if (typeof e.document != h)
            return e.document;
        if (e.parentNode)
            return u(e.parentNode);
        throw Error("getDocument: no document found for node")
    }
    function a(e) {
        return e ? s(e) ? '"' + e.data + '"' : 1 == e.nodeType ? "<" + e.nodeName + (e.id ? ' id="' + e.id + '"' : "") + ">[" + e.childNodes.length + "]" : e.nodeName : "[No node]"
    }
    function f(e) {
        this._next = this.root = e
    }
    function l(e, t) {
        this.node = e, this.offset = t
    }
    function c(e) {
        this.code = this[e], this.codeName = e, this.message = "DOMException: " + this.codeName
    }
    var h = "undefined", p = e.util;
    p.areHostMethods(document, ["createDocumentFragment", "createElement", "createTextNode"]) || t.fail("document missing a Node creation method"), p.isHostMethod(document, "getElementsByTagName") || t.fail("document missing getElementsByTagName method");
    var d = document.createElement("div");
    p.areHostMethods(d, ["insertBefore", "appendChild", "cloneNode"]) || t.fail("Incomplete Element implementation"), p.isHostProperty(d, "innerHTML") || t.fail("Element is missing innerHTML property"), d = document.createTextNode("test"), p.areHostMethods(d, ["splitText", "deleteData", "insertData", "appendData", "cloneNode"]) || t.fail("Incomplete Text Node implementation");
    var v = function(e, t) {
        for (var n = e.length; n--; )
            if (e[n] === t)
                return!0;
        return!1
    };
    f.prototype = {_current: null, hasNext: function() {
            return!!this._next
        }, next: function() {
            var e = this._current = this._next, t;
            if (this._current) {
                t = e.firstChild;
                if (!t)
                    for (t = null; e !== this.root && !(t = e.nextSibling); )
                        e = e.parentNode;
                this._next = t
            }
            return this._current
        }, detach: function() {
            this._current = this._next = this.root = null
        }}, l.prototype = {equals: function(e) {
            return this.node === e.node & this.offset == e.offset
        }, inspect: function() {
            return"[DomPosition(" + a(this.node) + ":" + this.offset + ")]"
        }}, c.prototype = {INDEX_SIZE_ERR: 1, HIERARCHY_REQUEST_ERR: 3, WRONG_DOCUMENT_ERR: 4, NO_MODIFICATION_ALLOWED_ERR: 7, NOT_FOUND_ERR: 8, NOT_SUPPORTED_ERR: 9, INVALID_STATE_ERR: 11}, c.prototype.toString = function() {
        return this.message
    }, e.dom = {arrayContains: v, isHtmlNamespace: function(e) {
            var t;
            return typeof e.namespaceURI == h || null === (t = e.namespaceURI) || "http://www.w3.org/1999/xhtml" == t
        }, parentElement: function(e) {
            return e = e.parentNode, 1 == e.nodeType ? e : null
        }, getNodeIndex: n, getNodeLength: function(e) {
            var t;
            return s(e) ? e.length : (t = e.childNodes) ? t.length : 0
        }, getCommonAncestor: r, isAncestorOf: function(e, t, n) {
            for (t = n ? t : t.parentNode; t; ) {
                if (t === e)
                    return!0;
                t = t.parentNode
            }
            return!1
        }, getClosestAncestorIn: i, isCharacterDataNode: s, insertAfter: o, splitDataNode: function(e, t) {
            var n = e.cloneNode(!1);
            return n.deleteData(0, t), e.deleteData(t, e.length - t), o(n, e), n
        }, getDocument: u, getWindow: function(e) {
            e = u(e);
            if (typeof e.defaultView != h)
                return e.defaultView;
            if (typeof e.parentWindow != h)
                return e.parentWindow;
            throw Error("Cannot get a window object for node")
        }, getIframeWindow: function(e) {
            if (typeof e.contentWindow != h)
                return e.contentWindow;
            if (typeof e.contentDocument != h)
                return e.contentDocument.defaultView;
            throw Error("getIframeWindow: No Window object found for iframe element")
        }, getIframeDocument: function(e) {
            if (typeof e.contentDocument != h)
                return e.contentDocument;
            if (typeof e.contentWindow != h)
                return e.contentWindow.document;
            throw Error("getIframeWindow: No Document object found for iframe element")
        }, getBody: function(e) {
            return p.isHostObject(e, "body") ? e.body : e.getElementsByTagName("body")[0]
        }, getRootContainer: function(e) {
            for (var t; t = e.parentNode; )
                e = t;
            return e
        }, comparePoints: function(e, t, s, o) {
            var u;
            if (e == s)
                return t === o ? 0 : t < o ? -1 : 1;
            if (u = i(s, e, !0))
                return t <= n(u) ? -1 : 1;
            if (u = i(e, s, !0))
                return n(u) < o ? -1 : 1;
            t = r(e, s), e = e === t ? t : i(e, t, !0), s = s === t ? t : i(s, t, !0);
            if (e === s)
                throw Error("comparePoints got to case 4 and childA and childB are the same!");
            for (t = t.firstChild; t; ) {
                if (t === e)
                    return-1;
                if (t === s)
                    return 1;
                t = t.nextSibling
            }
            throw Error("Should not be here!")
        }, inspectNode: a, fragmentFromNodeChildren: function(e) {
            for (var t = u(e).createDocumentFragment(), n; n = e.firstChild; )
                t.appendChild(n);
            return t
        }, createIterator: function(e) {
            return new f(e)
        }, DomPosition: l}, e.DOMException = c
}), rangy.createModule("DomRange", function(e) {
    function t(e, t) {
        return 3 != e.nodeType && (D.isAncestorOf(e, t.startContainer, !0) || D.isAncestorOf(e, t.endContainer, !0))
    }
    function n(e) {
        return D.getDocument(e.startContainer)
    }
    function r(e, t, n) {
        if (t = e._listeners[t])
            for (var r = 0, i = t.length; r < i; ++r)
                t[r].call(e, {target: e, args: n})
    }
    function i(e) {
        return new P(e.parentNode, D.getNodeIndex(e))
    }
    function s(e) {
        return new P(e.parentNode, D.getNodeIndex(e) + 1)
    }
    function o(e, t, n) {
        var r = 11 == e.nodeType ? e.firstChild : e;
        return D.isCharacterDataNode(t) ? n == t.length ? D.insertAfter(e, t) : t.parentNode.insertBefore(e, 0 == n ? t : D.splitDataNode(t, n)) : n >= t.childNodes.length ? t.appendChild(e) : t.insertBefore(e, t.childNodes[n]), r
    }
    function u(e) {
        for (var t, r, i = n(e.range).createDocumentFragment(); r = e.next(); ) {
            t = e.isPartiallySelectedSubtree(), r = r.cloneNode(!t), t && (t = e.getSubtreeIterator(), r.appendChild(u(t)), t.detach(!0));
            if (10 == r.nodeType)
                throw new H("HIERARCHY_REQUEST_ERR");
            i.appendChild(r)
        }
        return i
    }
    function a(e, t, n) {
        for (var r, i, n = n || {stop: !1}; r = e.next(); )
            if (e.isPartiallySelectedSubtree()) {
                if (!1 === t(r)) {
                    n.stop = !0;
                    break
                }
                if (r = e.getSubtreeIterator(), a(r, t, n), r.detach(!0), n.stop)
                    break
            } else
                for (r = D.createIterator(r); i = r.next(); )
                    if (!1 === t(i)) {
                        n.stop = !0;
                        return
                    }
    }
    function f(e) {
        for (var t; e.next(); )
            e.isPartiallySelectedSubtree() ? (t = e.getSubtreeIterator(), f(t), t.detach(!0)) : e.remove()
    }
    function l(e) {
        for (var t, r = n(e.range).createDocumentFragment(), i; t = e.next(); ) {
            e.isPartiallySelectedSubtree() ? (t = t.cloneNode(!1), i = e.getSubtreeIterator(), t.appendChild(l(i)), i.detach(!0)) : e.remove();
            if (10 == t.nodeType)
                throw new H("HIERARCHY_REQUEST_ERR");
            r.appendChild(t)
        }
        return r
    }
    function c(e, t, n) {
        var r = !!t && !!t.length, i, s = !!n;
        r && (i = RegExp("^(" + t.join("|") + ")$"));
        var o = [];
        return a(new p(e, !1), function(e) {
            (!r || i.test(e.nodeType)) && (!s || n(e)) && o.push(e)
        }), o
    }
    function h(e) {
        return"[" + ("undefined" == typeof e.getName ? "Range" : e.getName()) + "(" + D.inspectNode(e.startContainer) + ":" + e.startOffset + ", " + D.inspectNode(e.endContainer) + ":" + e.endOffset + ")]"
    }
    function p(e, t) {
        this.range = e, this.clonePartiallySelectedTextNodes = t;
        if (!e.collapsed) {
            this.sc = e.startContainer, this.so = e.startOffset, this.ec = e.endContainer, this.eo = e.endOffset;
            var n = e.commonAncestorContainer;
            this.sc === this.ec && D.isCharacterDataNode(this.sc) ? (this.isSingleCharacterDataNode = !0, this._first = this._last = this._next = this.sc) : (this._first = this._next = this.sc === n && !D.isCharacterDataNode(this.sc) ? this.sc.childNodes[this.so] : D.getClosestAncestorIn(this.sc, n, !0), this._last = this.ec === n && !D.isCharacterDataNode(this.ec) ? this.ec.childNodes[this.eo - 1] : D.getClosestAncestorIn(this.ec, n, !0))
        }
    }
    function d(e) {
        this.code = this[e], this.codeName = e, this.message = "RangeException: " + this.codeName
    }
    function v(e, t, n) {
        this.nodes = c(e, t, n), this._next = this.nodes[0], this._position = 0
    }
    function m(e) {
        return function(t, n) {
            for (var r, i = n ? t : t.parentNode; i; ) {
                r = i.nodeType;
                if (D.arrayContains(e, r))
                    return i;
                i = i.parentNode
            }
            return null
        }
    }
    function g(e, t) {
        if (z(e, t))
            throw new d("INVALID_NODE_TYPE_ERR")
    }
    function y(e) {
        if (!e.startContainer)
            throw new H("INVALID_STATE_ERR")
    }
    function b(e, t) {
        if (!D.arrayContains(t, e.nodeType))
            throw new d("INVALID_NODE_TYPE_ERR")
    }
    function w(e, t) {
        if (0 > t || t > (D.isCharacterDataNode(e) ? e.length : e.childNodes.length))
            throw new H("INDEX_SIZE_ERR")
    }
    function E(e, t) {
        if (R(e, !0) !== R(t, !0))
            throw new H("WRONG_DOCUMENT_ERR")
    }
    function S(e) {
        if (U(e, !0))
            throw new H("NO_MODIFICATION_ALLOWED_ERR")
    }
    function x(e, t) {
        if (!e)
            throw new H(t)
    }
    function T(e) {
        y(e);
        if (!D.arrayContains(j, e.startContainer.nodeType) && !R(e.startContainer, !0) || !D.arrayContains(j, e.endContainer.nodeType) && !R(e.endContainer, !0) || !(e.startOffset <= (D.isCharacterDataNode(e.startContainer) ? e.startContainer.length : e.startContainer.childNodes.length)) || !(e.endOffset <= (D.isCharacterDataNode(e.endContainer) ? e.endContainer.length : e.endContainer.childNodes.length)))
            throw Error("Range error: Range is no longer valid after DOM mutation (" + e.inspect() + ")")
    }
    function N() {
    }
    function C(e) {
        e.START_TO_START = J, e.START_TO_END = K, e.END_TO_END = Q, e.END_TO_START = G, e.NODE_BEFORE = Y, e.NODE_AFTER = Z, e.NODE_BEFORE_AND_AFTER = et, e.NODE_INSIDE = tt
    }
    function k(e) {
        C(e), C(e.prototype)
    }
    function L(e, t) {
        return function() {
            T(this);
            var n = this.startContainer, r = this.startOffset, i = this.commonAncestorContainer, o = new p(this, !0);
            return n !== i && (n = D.getClosestAncestorIn(n, i, !0), r = s(n), n = r.node, r = r.offset), a(o, S), o.reset(), i = e(o), o.detach(), t(this, n, r, n, r), i
        }
    }
    function A(n, r, o) {
        function u(e, t) {
            return function(n) {
                y(this), b(n, B), b(q(n), j), n = (e ? i : s)(n), (t ? a : c)(this, n.node, n.offset)
            }
        }
        function a(e, t, n) {
            var i = e.endContainer, s = e.endOffset;
            if (t !== e.startContainer || n !== e.startOffset) {
                if (q(t) != q(i) || 1 == D.comparePoints(t, n, i, s))
                    i = t, s = n;
                r(e, t, n, i, s)
            }
        }
        function c(e, t, n) {
            var i = e.startContainer, s = e.startOffset;
            if (t !== e.endContainer || n !== e.endOffset) {
                if (q(t) != q(i) || -1 == D.comparePoints(t, n, i, s))
                    i = t, s = n;
                r(e, i, s, t, n)
            }
        }
        n.prototype = new N, e.util.extend(n.prototype, {setStart: function(e, t) {
                y(this), g(e, !0), w(e, t), a(this, e, t)
            }, setEnd: function(e, t) {
                y(this), g(e, !0), w(e, t), c(this, e, t)
            }, setStartBefore: u(!0, !0), setStartAfter: u(!1, !0), setEndBefore: u(!0, !1), setEndAfter: u(!1, !1), collapse: function(e) {
                T(this), e ? r(this, this.startContainer, this.startOffset, this.startContainer, this.startOffset) : r(this, this.endContainer, this.endOffset, this.endContainer, this.endOffset)
            }, selectNodeContents: function(e) {
                y(this), g(e, !0), r(this, e, 0, e, D.getNodeLength(e))
            }, selectNode: function(e) {
                y(this), g(e, !1), b(e, B);
                var t = i(e), e = s(e);
                r(this, t.node, t.offset, e.node, e.offset)
            }, extractContents: L(l, r), deleteContents: L(f, r), canSurroundContents: function() {
                T(this), S(this.startContainer), S(this.endContainer);
                var e = new p(this, !0), n = e._first && t(e._first, this) || e._last && t(e._last, this);
                return e.detach(), !n
            }, detach: function() {
                o(this)
            }, splitBoundaries: function() {
                T(this);
                var e = this.startContainer, t = this.startOffset, n = this.endContainer, i = this.endOffset, s = e === n;
                D.isCharacterDataNode(n) && 0 < i && i < n.length && D.splitDataNode(n, i), D.isCharacterDataNode(e) && 0 < t && t < e.length && (e = D.splitDataNode(e, t), s ? (i -= t, n = e) : n == e.parentNode && i >= D.getNodeIndex(e) && i++, t = 0), r(this, e, t, n, i)
            }, normalizeBoundaries: function() {
                T(this);
                var e = this.startContainer, t = this.startOffset, n = this.endContainer, i = this.endOffset, s = function(e) {
                    var t = e.nextSibling;
                    t && t.nodeType == e.nodeType && (n = e, i = e.length, e.appendData(t.data), t.parentNode.removeChild(t))
                }, o = function(r) {
                    var s = r.previousSibling;
                    if (s && s.nodeType == r.nodeType) {
                        e = r;
                        var o = r.length;
                        t = s.length, r.insertData(0, s.data), s.parentNode.removeChild(s), e == n ? (i += t, n = e) : n == r.parentNode && (s = D.getNodeIndex(r), i == s ? (n = r, i = o) : i > s && i--)
                    }
                }, u = !0;
                D.isCharacterDataNode(n) ? n.length == i && s(n) : (0 < i && (u = n.childNodes[i - 1]) && D.isCharacterDataNode(u) && s(u), u = !this.collapsed), u ? D.isCharacterDataNode(e) ? 0 == t && o(e) : t < e.childNodes.length && (s = e.childNodes[t]) && D.isCharacterDataNode(s) && o(s) : (e = n, t = i), r(this, e, t, n, i)
            }, collapseToPoint: function(e, t) {
                y(this), g(e, !0), w(e, t), (e !== this.startContainer || t !== this.startOffset || e !== this.endContainer || t !== this.endOffset) && r(this, e, t, e, t)
            }}), k(n)
    }
    function O(e) {
        e.collapsed = e.startContainer === e.endContainer && e.startOffset === e.endOffset, e.commonAncestorContainer = e.collapsed ? e.startContainer : D.getCommonAncestor(e.startContainer, e.endContainer)
    }
    function M(e, t, n, i, s) {
        var o = e.startContainer !== t || e.startOffset !== n, u = e.endContainer !== i || e.endOffset !== s;
        e.startContainer = t, e.startOffset = n, e.endContainer = i, e.endOffset = s, O(e), r(e, "boundarychange", {startMoved: o, endMoved: u})
    }
    function _(e) {
        this.startContainer = e, this.startOffset = 0, this.endContainer = e, this.endOffset = 0, this._listeners = {boundarychange: [], detach: []}, O(this)
    }
    e.requireModules(["DomUtil"]);
    var D = e.dom, P = D.DomPosition, H = e.DOMException;
    p.prototype = {_current: null, _next: null, _first: null, _last: null, isSingleCharacterDataNode: !1, reset: function() {
            this._current = null, this._next = this._first
        }, hasNext: function() {
            return!!this._next
        }, next: function() {
            var e = this._current = this._next;
            return e && (this._next = e !== this._last ? e.nextSibling : null, D.isCharacterDataNode(e) && this.clonePartiallySelectedTextNodes && (e === this.ec && (e = e.cloneNode(!0)).deleteData(this.eo, e.length - this.eo), this._current === this.sc && (e = e.cloneNode(!0)).deleteData(0, this.so))), e
        }, remove: function() {
            var e = this._current, t, n;
            !D.isCharacterDataNode(e) || e !== this.sc && e !== this.ec ? e.parentNode && e.parentNode.removeChild(e) : (t = e === this.sc ? this.so : 0, n = e === this.ec ? this.eo : e.length, t != n && e.deleteData(t, n - t))
        }, isPartiallySelectedSubtree: function() {
            return t(this._current, this.range)
        }, getSubtreeIterator: function() {
            var e;
            if (this.isSingleCharacterDataNode)
                e = this.range.cloneRange(), e.collapse();
            else {
                e = new _(n(this.range));
                var t = this._current, r = t, i = 0, s = t, o = D.getNodeLength(t);
                D.isAncestorOf(t, this.sc, !0) && (r = this.sc, i = this.so), D.isAncestorOf(t, this.ec, !0) && (s = this.ec, o = this.eo), M(e, r, i, s, o)
            }
            return new p(e, this.clonePartiallySelectedTextNodes)
        }, detach: function(e) {
            e && this.range.detach(), this.range = this._current = this._next = this._first = this._last = this.sc = this.so = this.ec = this.eo = null
        }}, d.prototype = {BAD_BOUNDARYPOINTS_ERR: 1, INVALID_NODE_TYPE_ERR: 2}, d.prototype.toString = function() {
        return this.message
    }, v.prototype = {_current: null, hasNext: function() {
            return!!this._next
        }, next: function() {
            return this._current = this._next, this._next = this.nodes[++this._position], this._current
        }, detach: function() {
            this._current = this._next = this.nodes = null
        }};
    var B = [1, 3, 4, 5, 7, 8, 10], j = [2, 9, 11], F = [1, 3, 4, 5, 7, 8, 10, 11], I = [1, 3, 4, 5, 7, 8], q = D.getRootContainer, R = m([9, 11]), U = m([5, 6, 10, 12]), z = m([6, 10, 12]), W = document.createElement("style"), X = !1;
    try {
        W.innerHTML = "<b>x</b>", X = 3 == W.firstChild.nodeType
    } catch (V) {
    }
    e.features.htmlParsingConforms = X;
    var $ = "startContainer,startOffset,endContainer,endOffset,collapsed,commonAncestorContainer".split(","), J = 0, K = 1, Q = 2, G = 3, Y = 0, Z = 1, et = 2, tt = 3;
    N.prototype = {attachListener: function(e, t) {
            this._listeners[e].push(t)
        }, compareBoundaryPoints: function(e, t) {
            T(this), E(this.startContainer, t.startContainer);
            var n = e == G || e == J ? "start" : "end", r = e == K || e == J ? "start" : "end";
            return D.comparePoints(this[n + "Container"], this[n + "Offset"], t[r + "Container"], t[r + "Offset"])
        }, insertNode: function(e) {
            T(this), b(e, F), S(this.startContainer);
            if (D.isAncestorOf(e, this.startContainer, !0))
                throw new H("HIERARCHY_REQUEST_ERR");
            this.setStartBefore(o(e, this.startContainer, this.startOffset))
        }, cloneContents: function() {
            T(this);
            var e, t;
            return this.collapsed ? n(this).createDocumentFragment() : this.startContainer === this.endContainer && D.isCharacterDataNode(this.startContainer) ? (e = this.startContainer.cloneNode(!0), e.data = e.data.slice(this.startOffset, this.endOffset), t = n(this).createDocumentFragment(), t.appendChild(e), t) : (t = new p(this, !0), e = u(t), t.detach(), e)
        }, canSurroundContents: function() {
            T(this), S(this.startContainer), S(this.endContainer);
            var e = new p(this, !0), n = e._first && t(e._first, this) || e._last && t(e._last, this);
            return e.detach(), !n
        }, surroundContents: function(e) {
            b(e, I);
            if (!this.canSurroundContents())
                throw new d("BAD_BOUNDARYPOINTS_ERR");
            var t = this.extractContents();
            if (e.hasChildNodes())
                for (; e.lastChild; )
                    e.removeChild(e.lastChild);
            o(e, this.startContainer, this.startOffset), e.appendChild(t), this.selectNode(e)
        }, cloneRange: function() {
            T(this);
            for (var e = new _(n(this)), t = $.length, r; t--; )
                r = $[t], e[r] = this[r];
            return e
        }, toString: function() {
            T(this);
            var e = this.startContainer;
            if (e === this.endContainer && D.isCharacterDataNode(e))
                return 3 == e.nodeType || 4 == e.nodeType ? e.data.slice(this.startOffset, this.endOffset) : "";
            var t = [], e = new p(this, !0);
            return a(e, function(e) {
                (3 == e.nodeType || 4 == e.nodeType) && t.push(e.data)
            }), e.detach(), t.join("")
        }, compareNode: function(e) {
            T(this);
            var t = e.parentNode, n = D.getNodeIndex(e);
            if (!t)
                throw new H("NOT_FOUND_ERR");
            return e = this.comparePoint(t, n), t = this.comparePoint(t, n + 1), 0 > e ? 0 < t ? et : Y : 0 < t ? Z : tt
        }, comparePoint: function(e, t) {
            return T(this), x(e, "HIERARCHY_REQUEST_ERR"), E(e, this.startContainer), 0 > D.comparePoints(e, t, this.startContainer, this.startOffset) ? -1 : 0 < D.comparePoints(e, t, this.endContainer, this.endOffset) ? 1 : 0
        }, createContextualFragment: X ? function(e) {
            var t = this.startContainer, n = D.getDocument(t);
            if (!t)
                throw new H("INVALID_STATE_ERR");
            var r = null;
            return 1 == t.nodeType ? r = t : D.isCharacterDataNode(t) && (r = D.parentElement(t)), r = null === r || "HTML" == r.nodeName && D.isHtmlNamespace(D.getDocument(r).documentElement) && D.isHtmlNamespace(r) ? n.createElement("body") : r.cloneNode(!1), r.innerHTML = e, D.fragmentFromNodeChildren(r)
        } : function(e) {
            y(this);
            var t = n(this).createElement("body");
            return t.innerHTML = e, D.fragmentFromNodeChildren(t)
        }, toHtml: function() {
            T(this);
            var e = n(this).createElement("div");
            return e.appendChild(this.cloneContents()), e.innerHTML
        }, intersectsNode: function(e, t) {
            T(this), x(e, "NOT_FOUND_ERR");
            if (D.getDocument(e) !== n(this))
                return!1;
            var r = e.parentNode, i = D.getNodeIndex(e);
            x(r, "NOT_FOUND_ERR");
            var s = D.comparePoints(r, i, this.endContainer, this.endOffset), r = D.comparePoints(r, i + 1, this.startContainer, this.startOffset);
            return t ? 0 >= s && 0 <= r : 0 > s && 0 < r
        }, isPointInRange: function(e, t) {
            return T(this), x(e, "HIERARCHY_REQUEST_ERR"), E(e, this.startContainer), 0 <= D.comparePoints(e, t, this.startContainer, this.startOffset) && 0 >= D.comparePoints(e, t, this.endContainer, this.endOffset)
        }, intersectsRange: function(e, t) {
            T(this);
            if (n(e) != n(this))
                throw new
                        H("WRONG_DOCUMENT_ERR");
            var r = D.comparePoints(this.startContainer, this.startOffset, e.endContainer, e.endOffset), i = D.comparePoints(this.endContainer, this.endOffset, e.startContainer, e.startOffset);
            return t ? 0 >= r && 0 <= i : 0 > r && 0 < i
        }, intersection: function(e) {
            if (this.intersectsRange(e)) {
                var t = D.comparePoints(this.startContainer, this.startOffset, e.startContainer, e.startOffset), n = D.comparePoints(this.endContainer, this.endOffset, e.endContainer, e.endOffset), r = this.cloneRange();
                return-1 == t && r.setStart(e.startContainer, e.startOffset), 1 == n && r.setEnd(e.endContainer, e.endOffset), r
            }
            return null
        }, union: function(e) {
            if (this.intersectsRange(e, !0)) {
                var t = this.cloneRange();
                return-1 == D.comparePoints(e.startContainer, e.startOffset, this.startContainer, this.startOffset) && t.setStart(e.startContainer, e.startOffset), 1 == D.comparePoints(e.endContainer, e.endOffset, this.endContainer, this.endOffset) && t.setEnd(e.endContainer, e.endOffset), t
            }
            throw new d("Ranges do not intersect")
        }, containsNode: function(e, t) {
            return t ? this.intersectsNode(e, !1) : this.compareNode(e) == tt
        }, containsNodeContents: function(e) {
            return 0 <= this.comparePoint(e, 0) && 0 >= this.comparePoint(e, D.getNodeLength(e))
        }, containsRange: function(e) {
            return this.intersection(e).equals(e)
        }, containsNodeText: function(e) {
            var t = this.cloneRange();
            t.selectNode(e);
            var n = t.getNodes([3]);
            return 0 < n.length ? (t.setStart(n[0], 0), e = n.pop(), t.setEnd(e, e.length), e = this.containsRange(t), t.detach(), e) : this.containsNodeContents(e)
        }, createNodeIterator: function(e, t) {
            return T(this), new v(this, e, t)
        }, getNodes: function(e, t) {
            return T(this), c(this, e, t)
        }, getDocument: function() {
            return n(this)
        }, collapseBefore: function(e) {
            y(this), this.setEndBefore(e), this.collapse(!1)
        }, collapseAfter: function(e) {
            y(this), this.setStartAfter(e), this.collapse(!0)
        }, getName: function() {
            return"DomRange"
        }, equals: function(e) {
            return _.rangesEqual(this, e)
        }, inspect: function() {
            return h(this)
        }}, A(_, M, function(e) {
        y(e), e.startContainer = e.startOffset = e.endContainer = e.endOffset = null, e.collapsed = e.commonAncestorContainer = null, r(e, "detach", null), e._listeners = null
    }), e.rangePrototype = N.prototype, _.rangeProperties = $, _.RangeIterator = p, _.copyComparisonConstants = k, _.createPrototypeRange = A, _.inspect = h, _.getRangeDocument = n, _.rangesEqual = function(e, t) {
        return e.startContainer === t.startContainer && e.startOffset === t.startOffset && e.endContainer === t.endContainer && e.endOffset === t.endOffset
    }, e.DomRange = _, e.RangeException = d
}), rangy.createModule("WrappedRange", function(e) {
    function t(e, t, n, r) {
        var o = e.duplicate();
        o.collapse(n);
        var u = o.parentElement();
        i.isAncestorOf(t, u, !0) || (u = t);
        if (!u.canHaveHTML)
            return new s(u.parentNode, i.getNodeIndex(u));
        var t = i.getDocument(u).createElement("span"), a, l = n ? "StartToStart" : "StartToEnd";
        do
            u.insertBefore(t, t.previousSibling), o.moveToElementText(t);
        while (0 < (a = o.compareEndPoints(l, e)) && t.previousSibling);
        l = t.nextSibling;
        if (-1 == a && l && i.isCharacterDataNode(l)) {
            o.setEndPoint(n ? "EndToStart" : "EndToEnd", e);
            if (/[\r\n]/.test(l.data)) {
                u = o.duplicate(), n = u.text.replace(/\r\n/g, "\r").length;
                for (n = u.moveStart("character", n); - 1 == u.compareEndPoints("StartToEnd", u); )
                    n++, u.moveStart("character", 1)
            } else
                n = o.text.length;
            u = new s(l, n)
        } else
            l = (r || !n) && t.previousSibling, u = (n = (r || n) && t.nextSibling) && i.isCharacterDataNode(n) ? new s(n, 0) : l && i.isCharacterDataNode(l) ? new s(l, l.length) : new s(u, i.getNodeIndex(t));
        return t.parentNode.removeChild(t), u
    }
    function n(e, t) {
        var n, r, s = e.offset, o = i.getDocument(e.node), u = o.body.createTextRange(), a = i.isCharacterDataNode(e.node);
        return a ? (n = e.node, r = n.parentNode) : (n = e.node.childNodes, n = s < n.length ? n[s] : null, r = e.node), o = o.createElement("span"), o.innerHTML = "&#feff;", n ? r.insertBefore(o, n) : r.appendChild(o), u.moveToElementText(o), u.collapse(!t), r.removeChild(o), a && u[t ? "moveStart" : "moveEnd"]("character", s), u
    }
    e.requireModules(["DomUtil", "DomRange"]);
    var r, i = e.dom, s = i.DomPosition, o = e.DomRange;
    if (e.features.implementsDomRange && (!e.features.implementsTextRange || !e.config.preferTextRange))
        (function() {
            function t(e) {
                for (var t = s.length, n; t--; )
                    n = s[t], e[n] = e.nativeRange[n]
            }
            var n, s = o.rangeProperties, u;
            r = function(e) {
                if (!e)
                    throw Error("Range must be specified");
                this.nativeRange = e, t(this)
            }, o.createPrototypeRange(r, function(e, t, n, r, i) {
                var s = e.endContainer !== r || e.endOffset != i;
                if (e.startContainer !== t || e.startOffset != n || s)
                    e.setEnd(r, i), e.setStart(t, n)
            }, function(e) {
                e.nativeRange.detach(), e.detached = !0;
                for (var t = s.length, n; t--; )
                    n = s[t], e[n] = null
            }), n = r.prototype, n.selectNode = function(e) {
                this.nativeRange.selectNode(e), t(this)
            }, n.deleteContents = function() {
                this.nativeRange.deleteContents(), t(this)
            }, n.extractContents = function() {
                var e = this.nativeRange.extractContents();
                return t(this), e
            }, n.cloneContents = function() {
                return this.nativeRange.cloneContents()
            }, n.surroundContents = function(e) {
                this.nativeRange.surroundContents(e), t(this)
            }, n.collapse = function(e) {
                this.nativeRange.collapse(e), t(this)
            }, n.cloneRange = function() {
                return new r(this.nativeRange.cloneRange())
            }, n.refresh = function() {
                t(this)
            }, n.toString = function() {
                return this.nativeRange.toString()
            };
            var a = document.createTextNode("test");
            i.getBody(document).appendChild(a);
            var f = document.createRange();
            f.setStart(a, 0), f.setEnd(a, 0);
            try {
                f.setStart(a, 1), n.setStart = function(e, n) {
                    this.nativeRange.setStart(e, n), t(this)
                }, n.setEnd = function(e, n) {
                    this.nativeRange.setEnd(e, n), t(this)
                }, u = function(e) {
                    return function(n) {
                        this.nativeRange[e](n), t(this)
                    }
                }
            } catch (l) {
                n.setStart = function(e, n) {
                    try {
                        this.nativeRange.setStart(e, n)
                    } catch (r) {
                        this.nativeRange.setEnd(e, n), this.nativeRange.setStart(e, n)
                    }
                    t(this)
                }, n.setEnd = function(e, n) {
                    try {
                        this.nativeRange.setEnd(e, n)
                    } catch (r) {
                        this.nativeRange.setStart(e, n), this.nativeRange.setEnd(e, n)
                    }
                    t(this)
                }, u = function(e, n) {
                    return function(r) {
                        try {
                            this.nativeRange[e](r)
                        } catch (i) {
                            this.nativeRange[n](r), this.nativeRange[e](r)
                        }
                        t(this)
                    }
                }
            }
            n.setStartBefore = u("setStartBefore", "setEndBefore"), n.setStartAfter = u("setStartAfter", "setEndAfter"), n.setEndBefore = u("setEndBefore", "setStartBefore"), n.setEndAfter = u("setEndAfter", "setStartAfter"), f.selectNodeContents(a), n.selectNodeContents = f.startContainer == a && f.endContainer == a && 0 == f.startOffset && f.endOffset == a.length ? function(e) {
                this.nativeRange.selectNodeContents(e), t(this)
            } : function(e) {
                this.setStart(e, 0), this.setEnd(e, o.getEndOffset(e))
            }, f.selectNodeContents(a), f.setEnd(a, 3), u = document.createRange(), u.selectNodeContents(a), u.setEnd(a, 4), u.setStart(a, 2), n.compareBoundaryPoints = -1 == f.compareBoundaryPoints(f.START_TO_END, u) & 1 == f.compareBoundaryPoints(f.END_TO_START, u) ? function(e, t) {
                return t = t.nativeRange || t, e == t.START_TO_END ? e = t.END_TO_START : e == t.END_TO_START && (e = t.START_TO_END), this.nativeRange.compareBoundaryPoints(e, t)
            } : function(e, t) {
                return this.nativeRange.compareBoundaryPoints(e, t.nativeRange || t)
            }, e.util.isHostMethod(f, "createContextualFragment") && (n.createContextualFragment = function(e) {
                return this.nativeRange.createContextualFragment(e)
            }), i.getBody(document).removeChild(a), f.detach(), u.detach()
        })(), e.createNativeRange = function(e) {
            return(e || document).createRange()
        };
    else if (e.features.implementsTextRange) {
        r = function(e) {
            this.textRange = e, this.refresh()
        }, r.prototype = new o(document), r.prototype.refresh = function() {
            var e, n, r = this.textRange;
            e = r.parentElement();
            var s = r.duplicate();
            s.collapse(!0), n = s.parentElement(), s = r.duplicate(), s.collapse(!1), r = s.parentElement(), n = n == r ? n : i.getCommonAncestor(n, r), n = n == e ? n : i.getCommonAncestor(e, n), 0 == this.textRange.compareEndPoints("StartToEnd", this.textRange) ? n = e = t(this.textRange, n, !0, !0) : (e = t(this.textRange, n, !0, !1), n = t(this.textRange, n, !1, !1)), this.setStart(e.node, e.offset), this.setEnd(n.node, n.offset)
        }, o.copyComparisonConstants(r);
        var u = function() {
            return this
        }();
        "undefined" == typeof u.Range && (u.Range = r), e.createNativeRange = function(e) {
            return(e || document).body.createTextRange()
        }
    }
    e.features.implementsTextRange && (r.rangeToTextRange = function(e) {
        if (e.collapsed)
            return n(new s(e.startContainer, e.startOffset), !0);
        var t = n(new s(e.startContainer, e.startOffset), !0), r = n(new s(e.endContainer, e.endOffset), !1), e = i.getDocument(e.startContainer).body.createTextRange();
        return e.setEndPoint("StartToStart", t), e.setEndPoint("EndToEnd", r), e
    }), r.prototype.getName = function() {
        return"WrappedRange"
    }, e.WrappedRange = r, e.createRange = function(t) {
        return new r(e.createNativeRange(t || document))
    }, e.createRangyRange = function(e) {
        return new o(e || document)
    }, e.createIframeRange = function(t) {
        return e.createRange(i.getIframeDocument(t))
    }, e.createIframeRangyRange = function(t) {
        return e.createRangyRange(i.getIframeDocument(t))
    }, e.addCreateMissingNativeApiListener(function(t) {
        t = t.document, typeof t.createRange == "undefined" && (t.createRange = function() {
            return e.createRange(this)
        }), t = t = null
    })
}), rangy.createModule("WrappedSelection", function(e, t) {
    function n(e) {
        return(e || window).getSelection()
    }
    function r(e) {
        return(e || window).document.selection
    }
    function i(e, t, n) {
        var r = n ? "end" : "start", n = n ? "start" : "end";
        e.anchorNode = t[r + "Container"], e.anchorOffset = t[r + "Offset"], e.focusNode = t[n + "Container"], e.focusOffset = t[n + "Offset"]
    }
    function s(e) {
        e.anchorNode = e.focusNode = null, e.anchorOffset = e.focusOffset = 0, e.rangeCount = 0, e.isCollapsed = !0, e._ranges.length = 0
    }
    function o(t) {
        var n;
        return t instanceof g ? (n = t._selectionNativeRange, n || (n = e.createNativeRange(v.getDocument(t.startContainer)), n.setEnd(t.endContainer, t.endOffset), n.setStart(t.startContainer, t.startOffset), t._selectionNativeRange = n, t.attachListener("detach", function() {
            this._selectionNativeRange = null
        }))) : t instanceof y ? n = t.nativeRange : e.features.implementsDomRange && t instanceof v.getWindow(t.startContainer).Range && (n = t), n
    }
    function u(e) {
        var t = e.getNodes(), n;
        e:if (!t.length || 1 != t[0].nodeType)
            n = !1;
        else {
            n = 1;
            for (var r = t.length; n < r; ++n)
                if (!v.isAncestorOf(t[0], t[n])) {
                    n = !1;
                    break e
                }
            n = !0
        }
        if (!n)
            throw Error("getSingleElementFromRange: range " + e.inspect() + " did not consist of a single element");
        return t[0]
    }
    function a(e, t) {
        var n = new y(t);
        e._ranges = [n], i(e, n, !1), e.rangeCount = 1, e.isCollapsed = n.collapsed
    }
    function f(t) {
        t._ranges.length = 0;
        if ("None" == t.docSelection.type)
            s(t);
        else {
            var n = t.docSelection.createRange();
            if (n && "undefined" != typeof n.text)
                a(t, n);
            else {
                t.rangeCount = n.length;
                for (var r, o = v.getDocument(n.item(0)), u = 0; u < t.rangeCount; ++u)
                    r = e.createRange(o), r.selectNode(n.item(u)), t._ranges.push(r);
                t.isCollapsed = 1 == t.rangeCount && t._ranges[0].collapsed, i(t, t._ranges[t.rangeCount - 1], !1)
            }
        }
    }
    function l(e, t) {
        for (var n = e.docSelection.createRange(), r = u(t), i = v.getDocument(n.item(0)), i = v.getBody(i).createControlRange(), s = 0, o = n.length; s < o; ++s)
            i.add(n.item(s));
        try {
            i.add(r)
        } catch (a) {
            throw Error("addRange(): Element within the specified Range could not be added to control selection (does it have layout?)")
        }
        i.select(), f(e)
    }
    function c(e, t, n) {
        this.nativeSelection = e, this.docSelection = t, this._ranges = [], this.win = n, this.refresh()
    }
    function h(e, t) {
        for (var n = v.getDocument(t[0].startContainer), n = v.getBody(n).createControlRange(), r = 0, i; r < rangeCount; ++r) {
            i = u(t[r]);
            try {
                n.add(i)
            } catch (s) {
                throw Error("setRanges(): Element within the one of the specified Ranges could not be added to control selection (does it have layout?)")
            }
        }
        n.select(), f(e)
    }
    function p(e, t) {
        if (e.anchorNode && v.getDocument(e.anchorNode) !== v.getDocument(t))
            throw new b("WRONG_DOCUMENT_ERR")
    }
    function d(e) {
        var t = [], n = new w(e.anchorNode, e.anchorOffset), r = new w(e.focusNode, e.focusOffset), i = "function" == typeof e.getName ? e.getName() : "Selection";
        if ("undefined" != typeof e.rangeCount)
            for (var s = 0, o = e.rangeCount; s < o; ++s)
                t[s] = g.inspect(e.getRangeAt(s));
        return"[" + i + "(Ranges: " + t.join(", ") + ")(anchor: " + n.inspect() + ", focus: " + r.inspect() + "]"
    }
    e.requireModules(["DomUtil", "DomRange", "WrappedRange"]), e.config.checkSelectionRanges = !0;
    var v = e.dom, m = e.util, g = e.DomRange, y = e.WrappedRange, b = e.DOMException, w = v.DomPosition, E, S, x = e.util.isHostMethod(window, "getSelection"), T = e.util.isHostObject(document, "selection"), N = T && (!x || e.config.preferTextRange);
    N ? (E = r, e.isSelectionValid = function(e) {
        var e = (e || window).document, t = e.selection;
        return"None" != t.type || v.getDocument(t.createRange().parentElement()) == e
    }) : x ? (E = n, e.isSelectionValid = function() {
        return!0
    }) : t.fail("Neither document.selection or window.getSelection() detected."), e.getNativeSelection = E;
    var x = E(), C = e.createNativeRange(document), k = v.getBody(document), L = m.areHostObjects(x, m.areHostProperties(x, ["anchorOffset", "focusOffset"]));
    e.features.selectionHasAnchorAndFocus = L;
    var A = m.isHostMethod(x, "extend");
    e.features.selectionHasExtend = A;
    var O = "number" == typeof x.rangeCount;
    e.features.selectionHasRangeCount = O;
    var M = !1, _ = !0;
    m.areHostMethods(x, ["addRange", "getRangeAt", "removeAllRanges"]) && "number" == typeof x.rangeCount && e.features.implementsDomRange && function() {
        var e = document.createElement("iframe");
        k.appendChild(e);
        var t = v.getIframeDocument(e);
        t.open(), t.write("<html><head></head><body>12</body></html>"), t.close();
        var n = v.getIframeWindow(e).getSelection(), r = t.documentElement.lastChild.firstChild, t = t.createRange();
        t.setStart(r, 1), t.collapse(!0), n.addRange(t), _ = n.rangeCount == 1, n.removeAllRanges();
        var i = t.cloneRange();
        t.setStart(r, 0), i.setEnd(r, 2), n.addRange(t), n.addRange(i), M = n.rangeCount == 2, t.detach(), i.detach(), k.removeChild(e)
    }(), e.features.selectionSupportsMultipleRanges = M, e.features.collapsedNonEditableSelectionsSupported = _;
    var D = !1, P;
    k && m.isHostMethod(k, "createControlRange") && (P = k.createControlRange(), m.areHostProperties(P, ["item", "add"]) && (D = !0)), e.features.implementsControlRange = D, S = L ? function(e) {
        return e.anchorNode === e.focusNode && e.anchorOffset === e.focusOffset
    } : function(e) {
        return e.rangeCount ? e.getRangeAt(e.rangeCount - 1).collapsed : !1
    };
    var H;
    m.isHostMethod(x, "getRangeAt") ? H = function(e, t) {
        try {
            return e.getRangeAt(t)
        } catch (n) {
            return null
        }
    } : L && (H = function(t) {
        var n = v.getDocument(t.anchorNode), n = e.createRange(n);
        return n.setStart(t.anchorNode, t.anchorOffset), n.setEnd(t.focusNode, t.focusOffset), n.collapsed !== this.isCollapsed && (n.setStart(t.focusNode, t.focusOffset), n.setEnd(t.anchorNode, t.anchorOffset)), n
    }), e.getSelection = function(e) {
        var e = e || window, t = e._rangySelection, n = E(e), i = T ? r(e) : null;
        return t ? (t.nativeSelection = n, t.docSelection = i, t.refresh(e)) : (t = new c(n, i, e), e._rangySelection = t), t
    }, e.getIframeSelection = function(t) {
        return e.getSelection(v.getIframeWindow(t))
    }, P = c.prototype;
    if (!N && L && m.areHostMethods(x, ["removeAllRanges", "addRange"])) {
        P.removeAllRanges = function() {
            this.nativeSelection.removeAllRanges(), s(this)
        };
        var B = function(t, n) {
            var r = g.getRangeDocument(n), r = e.createRange(r);
            r.collapseToPoint(n.endContainer, n.endOffset), t.nativeSelection.addRange(o(r)), t.nativeSelection.extend(n.startContainer, n.startOffset), t.refresh()
        };
        P.addRange = O ? function(t, n) {
            if (D && T && this.docSelection.type == "Control")
                l(this, t);
            else if (n && A)
                B(this, t);
            else {
                var r;
                M ? r = this.rangeCount : (this.removeAllRanges(), r = 0), this.nativeSelection.addRange(o(t)), this.rangeCount = this.nativeSelection.rangeCount, this.rangeCount == r + 1 ? (e.config.checkSelectionRanges && (r = H(this.nativeSelection, this.rangeCount - 1)) && !g.rangesEqual(r, t) && (t = new y(r)), this._ranges[this.rangeCount - 1] = t, i(this, t, I(this.nativeSelection)), this.isCollapsed = S(this)) : this.refresh()
            }
        } : function(e, t) {
            t && A ? B(this, e) : (this.nativeSelection.addRange(o(e)), this.refresh())
        }, P.setRanges = function(e) {
            if (D && e.length > 1)
                h(this, e);
            else {
                this.removeAllRanges();
                for (var t = 0, n = e.length; t < n; ++t)
                    this.addRange(e[t])
            }
        }
    } else {
        if (!(m.isHostMethod(x, "empty") && m.isHostMethod(C, "select") && D && N))
            return t.fail("No means of selecting a Range or TextRange was found"), !1;
        P.removeAllRanges = function() {
            try {
                this.docSelection.empty();
                if (this.docSelection.type != "None") {
                    var e;
                    if (this.anchorNode)
                        e = v.getDocument(this.anchorNode);
                    else if (this.docSelection.type == "Control") {
                        var t = this.docSelection.createRange();
                        t.length && (e = v.getDocument(t.item(0)).body.createTextRange())
                    }
                    e && (e.body.createTextRange().select(), this.docSelection.empty())
                }
            } catch (n) {
            }
            s(this)
        }, P.addRange = function(e) {
            this.docSelection.type == "Control" ? l(this, e) : (y.rangeToTextRange(e).select(), this._ranges[0] = e, this.rangeCount = 1, this.isCollapsed = this._ranges[0].collapsed, i(this, e, !1))
        }, P.setRanges = function(e) {
            this.removeAllRanges();
            var t = e.length;
            t > 1 ? h(this, e) : t && this.addRange(e[0])
        }
    }
    P.getRangeAt = function(e) {
        if (e < 0 || e >= this.rangeCount)
            throw new b("INDEX_SIZE_ERR");
        return this._ranges[e]
    };
    var j;
    if (N)
        j = function(t) {
            var n;
            e.isSelectionValid(t.win) ? n = t.docSelection.createRange() : (n = v.getBody(t.win.document).createTextRange(), n.collapse(!0)), t.docSelection.type == "Control" ? f(t) : n && typeof n.text != "undefined" ? a(t, n) : s(t)
        };
    else if (m.isHostMethod(x, "getRangeAt") && "number" == typeof x.rangeCount)
        j = function(t) {
            if (D && T && t.docSelection.type == "Control")
                f(t);
            else {
                t._ranges.length = t.rangeCount = t.nativeSelection.rangeCount;
                if (t.rangeCount) {
                    for (var n = 0, r = t.rangeCount; n < r; ++n)
                        t._ranges[n] = new e.WrappedRange(t.nativeSelection.getRangeAt(n));
                    i(t, t._ranges[t.rangeCount - 1], I(t.nativeSelection)), t.isCollapsed = S(t)
                } else
                    s(t)
            }
        };
    else {
        if (!L || "boolean" != typeof x.isCollapsed || "boolean" != typeof C.collapsed || !e.features.implementsDomRange)
            return t.fail("No means of obtaining a Range or TextRange from the user's selection was found"), !1;
        j = function(e) {
            var t;
            t = e.nativeSelection, t.anchorNode ? (t = H(t, 0), e._ranges = [t], e.rangeCount = 1, t = e.nativeSelection, e.anchorNode = t.anchorNode, e.anchorOffset = t.anchorOffset, e.focusNode = t.focusNode, e.focusOffset = t.focusOffset, e.isCollapsed = S(e)) : s(e)
        }
    }
    P.refresh = function(e) {
        var t = e ? this._ranges.slice(0) : null;
        j(this);
        if (e) {
            e = t.length;
            if (e != this._ranges.length)
                return!1;
            for (; e--; )
                if (!g.rangesEqual(t[e], this._ranges[e]))
                    return!1;
            return!0
        }
    };
    var F = function(e, t) {
        var n = e.getAllRanges(), r = !1;
        e.removeAllRanges();
        for (var i = 0, o = n.length; i < o; ++i)
            r || t !== n[i] ? e.addRange(n[i]) : r = !0;
        e.rangeCount || s(e)
    };
    P.removeRange = D ? function(e) {
        if (this.docSelection.type == "Control") {
            for (var t = this.docSelection.createRange(), e = u(e), n = v.getDocument(t.item(0)), n = v.getBody(n).createControlRange(), r, i = !1, s = 0, o = t.length; s < o; ++s)
                r = t.item(s), r !== e || i ? n.add(t.item(s)) : i = !0;
            n.select(), f(this)
        } else
            F(this, e)
    } : function(e) {
        F(this, e)
    };
    var I;
    !N && L && e.features.implementsDomRange ? (I = function(e) {
        var t = !1;
        return e.anchorNode && (t = v.comparePoints(e.anchorNode, e.anchorOffset, e.focusNode, e.focusOffset) == 1), t
    }, P.isBackwards = function() {
        return I(this)
    }) : I = P.isBackwards = function() {
        return!1
    }, P.toString = function() {
        for (var e = [], t = 0, n = this.rangeCount; t < n; ++t)
            e[t] = "" + this._ranges[t];
        return e.join("")
    }, P.collapse = function(t, n) {
        p(this, t);
        var r = e.createRange(v.getDocument(t));
        r.collapseToPoint(t, n), this.removeAllRanges(), this.addRange(r), this.isCollapsed = !0
    }, P.collapseToStart = function() {
        if (!this.rangeCount)
            throw new b("INVALID_STATE_ERR");
        var e = this._ranges[0];
        this.collapse(e.startContainer, e.startOffset)
    }, P.collapseToEnd = function() {
        if (!this.rangeCount)
            throw new b("INVALID_STATE_ERR");
        var e = this._ranges[this.rangeCount - 1];
        this.collapse(e.endContainer, e.endOffset)
    }, P.selectAllChildren = function(t) {
        p(this, t);
        var n = e.createRange(v.getDocument(t));
        n.selectNodeContents(t), this.removeAllRanges(), this.addRange(n)
    }, P.deleteFromDocument = function() {
        if (D && T && this.docSelection.type == "Control") {
            for (var e = this.docSelection.createRange(), t; e.length; )
                t = e.item(0), e.remove(t), t.parentNode.removeChild(t);
            this.refresh()
        } else if (this.rangeCount) {
            e = this.getAllRanges(), this.removeAllRanges(), t = 0;
            for (var n = e.length; t < n; ++t)
                e[t].deleteContents();
            this.addRange(e[n - 1])
        }
    }, P.getAllRanges = function() {
        return this._ranges.slice(0)
    }, P.setSingleRange = function(e) {
        this.setRanges([e])
    }, P.containsNode = function(e, t) {
        for (var n = 0, r = this._ranges.length; n < r; ++n)
            if (this._ranges[n].containsNode(e, t))
                return!0;
        return!1
    }, P.toHtml = function() {
        var e = "";
        if (this.rangeCount) {
            for (var e = g.getRangeDocument(this._ranges[0]).createElement("div"), t = 0, n = this._ranges.length; t < n; ++t)
                e.appendChild(this._ranges[t].cloneContents());
            e = e.innerHTML
        }
        return e
    }, P.getName = function() {
        return"WrappedSelection"
    }, P.inspect = function() {
        return d(this)
    }, P.detach = function() {
        this.win = this.anchorNode = this.focusNode = this.win._rangySelection = null
    }, c.inspect = d, e.Selection = c, e.selectionPrototype = P, e.addCreateMissingNativeApiListener(function(t) {
        typeof t.getSelection == "undefined" && (t.getSelection = function() {
            return e.getSelection(this)
        }), t = null
    })
});
var Base = function() {
};
Base.extend = function(e, t) {
    var n = Base.prototype.extend;
    Base._prototyping = !0;
    var r = new this;
    n.call(r, e), r.base = function() {
    }, delete Base._prototyping;
    var i = r.constructor, s = r.constructor = function() {
        if (!Base._prototyping)
            if (this._constructing || this.constructor == s)
                this._constructing = !0, i.apply(this, arguments), delete this._constructing;
            else if (null != arguments[0])
                return(arguments[0].extend || n).call(arguments[0], r)
    };
    return s.ancestor = this, s.extend = this.extend, s.forEach = this.forEach, s.implement = this.implement, s.prototype = r, s.toString = this.toString, s.valueOf = function(e) {
        return"object" == e ? s : i.valueOf()
    }, n.call(s, t), "function" == typeof s.init && s.init(), s
}, Base.prototype = {extend: function(e, t) {
        if (1 < arguments.length) {
            var n = this[e];
            if (n && "function" == typeof t && (!n.valueOf || n.valueOf() != t.valueOf()) && /\bbase\b/.test(t)) {
                var r = t.valueOf(), t = function() {
                    var e = this.base || Base.prototype.base;
                    this.base = n;
                    var t = r.apply(this, arguments);
                    return this.base = e, t
                };
                t.valueOf = function(e) {
                    return"object" == e ? t : r
                }, t.toString = Base.toString
            }
            this[e] = t
        } else if (e) {
            var i = Base.prototype.extend;
            !Base._prototyping && "function" != typeof this && (i = this.extend || i);
            for (var s = {toSource: null}, o = ["constructor", "toString", "valueOf"], u = Base._prototyping ? 0 : 1; a = o[u++]; )
                e[a] != s[a] && i.call(this, a, e[a]);
            for (var a in e)
                s[a] || i.call(this, a, e[a])
        }
        return this
    }}, Base = Base.extend({constructor: function(e) {
        this.extend(e)
    }}, {ancestor: Object, version: "1.1", forEach: function(e, t, n) {
        for (var r in e)
            void 0 === this.prototype[r] && t.call(n, e[r], r, e)
    }, implement: function() {
        for (var e = 0; e < arguments.length; e++)
            "function" == typeof arguments[e] ? arguments[e](this.prototype) : this.prototype.extend(arguments[e]);
        return this
    }, toString: function() {
        return"" + this.valueOf()
    }}), wysihtml5.browser = function() {
    var e = navigator.userAgent, t = document.createElement("div"), n = -1 !== e.indexOf("MSIE") && -1 === e.indexOf("Opera"), r = -1 !== e.indexOf("Gecko") && -1 === e.indexOf("KHTML"), i = -1 !== e.indexOf("AppleWebKit/"), s = -1 !== e.indexOf("Chrome/"), o = -1 !== e.indexOf("Opera/");
    return{USER_AGENT: e, supported: function() {
            var e = this.USER_AGENT.toLowerCase(), n = "contentEditable"in t, r = document.execCommand && document.queryCommandSupported && document.queryCommandState, i = document.querySelector && document.querySelectorAll, e = this.isIos() && 5 > (/ipad|iphone|ipod/.test(e) && e.match(/ os (\d+).+? like mac os x/) || [, 0])[1] || -1 !== e.indexOf("opera mobi") || -1 !== e.indexOf("hpwos/");
            return n && r && i && !e
        }, isTouchDevice: function() {
            return this.supportsEvent("touchmove")
        }, isIos: function() {
            var e = this.USER_AGENT.toLowerCase();
            return-1 !== e.indexOf("webkit") && -1 !== e.indexOf("mobile")
        }, supportsSandboxedIframes: function() {
            return n
        }, throwsMixedContentWarningWhenIframeSrcIsEmpty: function() {
            return!("querySelector"in document)
        }, displaysCaretInEmptyContentEditableCorrectly: function() {
            return!r
        }, hasCurrentStyleProperty: function() {
            return"currentStyle"in t
        }, insertsLineBreaksOnReturn: function() {
            return r
        }, supportsPlaceholderAttributeOn: function(e) {
            return"placeholder"in e
        }, supportsEvent: function(e) {
            var n;
            return(n = "on" + e in t) || (t.setAttribute("on" + e, "return;"), n = "function" == typeof t["on" + e]), n
        }, supportsEventsInIframeCorrectly: function() {
            return!o
        }, firesOnDropOnlyWhenOnDragOverIsCancelled: function() {
            return i || r
        }, supportsDataTransfer: function() {
            try {
                return i && (window.Clipboard || window.DataTransfer).prototype.getData
            } catch (e) {
                return!1
            }
        }, supportsHTML5Tags: function(e) {
            return e = e.createElement("div"), e.innerHTML = "<article>foo</article>", "<article>foo</article>" === e.innerHTML.toLowerCase()
        }, supportsCommand: function() {
            var e = {formatBlock: n, insertUnorderedList: n || o, insertOrderedList: n || o}, t = {insertHTML: r};
            return function(n, r) {
                if (!e[r]) {
                    try {
                        return n.queryCommandSupported(r)
                    } catch (i) {
                    }
                    try {
                        return n.queryCommandEnabled(r)
                    } catch (s) {
                        return!!t[r]
                    }
                }
                return!1
            }
        }(), doesAutoLinkingInContentEditable: function() {
            return n
        }, canDisableAutoLinking: function() {
            return this.supportsCommand(document, "AutoUrlDetect")
        }, clearsContentEditableCorrectly: function() {
            return r || o || i
        }, supportsGetAttributeCorrectly: function() {
            return"1" != document.createElement("td").getAttribute("rowspan")
        }, canSelectImagesInContentEditable: function() {
            return r || n || o
        }, clearsListsInContentEditableCorrectly: function() {
            return r || n || i
        }, autoScrollsToCaret: function() {
            return!i
        }, autoClosesUnclosedTags: function() {
            var e = t.cloneNode(!1), n;
            return e.innerHTML = "<p><div></div>", e = e.innerHTML.toLowerCase(), n = "<p></p><div></div>" === e || "<p><div></div></p>" === e, this.autoClosesUnclosedTags = function() {
                return n
            }, n
        }, supportsNativeGetElementsByClassName: function() {
            return-1 !== ("" + document.getElementsByClassName).indexOf("[native code]")
        }, supportsSelectionModify: function() {
            return"getSelection"in window && "modify"in window.getSelection()
        }, supportsClassList: function() {
            return"classList"in t
        }, needsSpaceAfterLineBreak: function() {
            return o
        }, supportsSpeechApiOn: function(t) {
            return 11 <= (e.match(/Chrome\/(\d+)/) || [, 0])[1] && ("onwebkitspeechchange"in t || "speech"in t)
        }, crashesWhenDefineProperty: function(e) {
            return n && ("XMLHttpRequest" === e || "XDomainRequest" === e)
        }, doesAsyncFocus: function() {
            return n
        }, hasProblemsSettingCaretAfterImg: function() {
            return n
        }, hasUndoInContextMenu: function() {
            return r || s || o
        }}
}(), wysihtml5.lang.array = function(e) {
    return{contains: function(t) {
            if (e.indexOf)
                return-1 !== e.indexOf(t);
            for (var n = 0, r = e.length; n < r; n++)
                if (e[n] === t)
                    return!0;
            return!1
        }, without: function(t) {
            for (var t = wysihtml5.lang.array(t), n = [], r = 0, i = e.length; r < i; r++)
                t.contains(e[r]) || n.push(e[r]);
            return n
        }, get: function() {
            for (var t = 0, n = e.length, r = []; t < n; t++)
                r.push(e[t]);
            return r
        }}
}, wysihtml5.lang.Dispatcher = Base.extend({observe: function(e, t) {
        return this.events = this.events || {}, this.events[e] = this.events[e] || [], this.events[e].push(t), this
    }, on: function() {
        return this.observe.apply(this, wysihtml5.lang.array(arguments).get())
    }, fire: function(e, t) {
        this.events = this.events || {};
        for (var n = this.events[e] || [], r = 0; r < n.length; r++)
            n[r].call(this, t);
        return this
    }, stopObserving: function(e, t) {
        this.events = this.events || {};
        var n = 0, r, i;
        if (e) {
            r = this.events[e] || [];
            for (i = []; n < r.length; n++)
                r[n] !== t && t && i.push(r[n]);
            this.events[e] = i
        } else
            this.events = {};
        return this
    }}), wysihtml5.lang.object = function(e) {
    return{merge: function(t) {
            for (var n in t)
                e[n] = t[n];
            return this
        }, get: function() {
            return e
        }, clone: function() {
            var t = {}, n;
            for (n in e)
                t[n] = e[n];
            return t
        }, isArray: function() {
            return"[object Array]" === Object.prototype.toString.call(e)
        }}
}, function() {
    var e = /^\s+/, t = /\s+$/;
    wysihtml5.lang.string = function(n) {
        return n = "" + n, {trim: function() {
                return n.replace(e, "").replace(t, "")
            }, interpolate: function(e) {
                for (var t in e)
                    n = this.replace("#{" + t + "}").by(e[t]);
                return n
            }, replace: function(e) {
                return{by: function(t) {
                        return n.split(e).join(t)
                    }}
            }}
    }
}(), function(e) {
    function t(e) {
        return e.replace(i, function(e, t) {
            var n = (t.match(s) || [])[1] || "", r = u[n], t = t.replace(s, "");
            t.split(r).length > t.split(n).length && (t += n, n = "");
            var i = r = t;
            return t.length > o && (i = i.substr(0, o) + "..."), "www." === r.substr(0, 4) && (r = "http://" + r), '<a href="' + r + '">' + i + "</a>" + n
        })
    }
    function n(s) {
        if (!r.contains(s.nodeName)) {
            if (s.nodeType !== e.TEXT_NODE || !s.data.match(i)) {
                o = e.lang.array(s.childNodes).get(), u = o.length;
                for (f = 0; f < u; f++)
                    n(o[f]);
                return s
            }
            var o = s.parentNode, u;
            u = o.ownerDocument;
            var f = u._wysihtml5_tempElement;
            f || (f = u._wysihtml5_tempElement = u.createElement("div")), u = f, u.innerHTML = "<span></span>" + t(s.data);
            for (u.removeChild(u.firstChild); u.firstChild; )
                o.insertBefore(u.firstChild, s);
            o.removeChild(s)
        }
    }
    var r = e.lang.array("CODE,PRE,A,SCRIPT,HEAD,TITLE,STYLE".split(",")), i = /((https?:\/\/|www\.)[^\s<]{3,})/gi, s = /([^\w\/\-](,?))$/i, o = 100, u = {")": "(", "]": "[", "}": "{"};
    e.dom.autoLink = function(e) {
        var t;
        e:{
            t = e;
            for (var i; t.parentNode; ) {
                t = t.parentNode, i = t.nodeName;
                if (r.contains(i)) {
                    t = !0;
                    break e
                }
                if ("body" === i)
                    break
            }
            t = !1
        }
        return t ? e : (e === e.ownerDocument.documentElement && (e = e.ownerDocument.body), n(e))
    }, e.dom.autoLink.URL_REG_EXP = i
}(wysihtml5), function(e) {
    var t = e.browser.supportsClassList(), n = e.dom;
    n.addClass = function(e, r) {
        if (t)
            return e.classList.add(r);
        n.hasClass(e, r) || (e.className += " " + r)
    }, n.removeClass = function(e, n) {
        if (t)
            return e.classList.remove(n);
        e.className = e.className.replace(RegExp("(^|\\s+)" + n + "(\\s+|$)"), " ")
    }, n.hasClass = function(e, n) {
        if (t)
            return e.classList.contains(n);
        var r = e.className;
        return 0 < r.length && (r == n || RegExp("(^|\\s)" + n + "(\\s|$)").test(r))
    }
}(wysihtml5), wysihtml5.dom.contains = function() {
    var e = document.documentElement;
    if (e.contains)
        return function(e, t) {
            return t.nodeType !== wysihtml5.ELEMENT_NODE && (t = t.parentNode), e !== t && e.contains(t)
        };
    if (e.compareDocumentPosition)
        return function(e, t) {
            return!!(e.compareDocumentPosition(t) & 16)
        }
}(), wysihtml5.dom.convertToList = function() {
    function e(e, t) {
        var n = e.createElement("li");
        return t.appendChild(n), n
    }
    return function(t, n) {
        if ("UL" === t.nodeName || "OL" === t.nodeName || "MENU" === t.nodeName)
            return t;
        for (var r = t.ownerDocument, i = r.createElement(n), s = wysihtml5.lang.array(t.childNodes).get(), o = s.length, u, a, f, l, c = 0; c < o; c++)
            l = l || e(r, i), u = s[c], a = "block" === wysihtml5.dom.getStyle("display").from(u), f = "BR" === u.nodeName, a ? (l = l.firstChild ? e(r, i) : l, l.appendChild(u), l = null) : f ? l = l.firstChild ? null : l : l.appendChild(u);
        return t.parentNode.replaceChild(i, t), i
    }
}(), wysihtml5.dom.copyAttributes = function(e) {
    return{from: function(t) {
            return{to: function(n) {
                    for (var r, i = 0, s = e.length; i < s; i++)
                        r = e[i], t[r] && (n[r] = t[r]);
                    return{andTo: arguments.callee}
                }}
        }}
}, function(e) {
    var t = ["-webkit-box-sizing", "-moz-box-sizing", "-ms-box-sizing", "box-sizing"], n = function(n) {
        var r;
        e:for (var i = 0, s = t.length; i < s; i++)
            if ("border-box" === e.getStyle(t[i]).from(n)) {
                r = t[i];
                break e
            }
        return r ? parseInt(e.getStyle("width").from(n), 10) < n.offsetWidth : !1
    };
    e.copyStyles = function(r) {
        return{from: function(i) {
                n(i) && (r = wysihtml5.lang.array(r).without(t));
                for (var s = "", o = r.length, u = 0, f; u < o; u++)
                    f = r[u], s += f + ":" + e.getStyle(f).from(i) + ";";
                return{to: function(t) {
                        return e.setStyles(s).on(t), {andTo: arguments.callee}
                    }}
            }}
    }
}(wysihtml5.dom), function(e) {
    e.dom.delegate = function(t, n, r, i) {
        return e.dom.observe(t, r, function(r) {
            for (var s = r.target, o = e.lang.array(t.querySelectorAll(n)); s && s !== t; ) {
                if (o.contains(s)) {
                    i.call(s, r);
                    break
                }
                s = s.parentNode
            }
        })
    }
}(wysihtml5), wysihtml5.dom.getAsDom = function() {
    var e = "abbr,article,aside,audio,bdi,canvas,command,datalist,details,figcaption,figure,footer,header,hgroup,keygen,mark,meter,nav,output,progress,rp,rt,ruby,svg,section,source,summary,time,track,video,wbr".split(",");
    return function(t, n) {
        var n = n || document, r;
        if ("object" == typeof t && t.nodeType)
            r = n.createElement("div"), r.appendChild(t);
        else if (wysihtml5.browser.supportsHTML5Tags(n))
            r = n.createElement("div"), r.innerHTML = t;
        else {
            r = n;
            if (!r._wysihtml5_supportsHTML5Tags) {
                for (var i = 0, s = e.length; i < s; i++)
                    r.createElement(e[i]);
                r._wysihtml5_supportsHTML5Tags = !0
            }
            r = n, i = r.createElement("div"), i.style.display = "none", r.body.appendChild(i);
            try {
                i.innerHTML = t
            } catch (o) {
            }
            r.body.removeChild(i), r = i
        }
        return r
    }
}(), wysihtml5.dom.getParentElement = function() {
    function e(e, t) {
        return!t || !t.length ? !0 : "string" == typeof t ? e === t : wysihtml5.lang.array(t).contains(e)
    }
    return function(t, n, r) {
        r = r || 50;
        if (n.className || n.classRegExp) {
            e:{
                for (var i = n.nodeName, s = n.className, n = n.classRegExp; r-- && t && "BODY" !== t.nodeName; ) {
                    var o;
                    if (o = t.nodeType === wysihtml5.ELEMENT_NODE)
                        if (o = e(t.nodeName, i)) {
                            o = s;
                            var u = (t.className || "").match(n) || [];
                            o = o ? u[u.length - 1] === o : !!u.length
                        }
                    if (o)
                        break e;
                    t = t.parentNode
                }
                t = null
            }
            return t
        }
        e:{
            i = n.nodeName;
            for (s = r; s-- && t && "BODY" !== t.nodeName; ) {
                if (e(t.nodeName, i))
                    break e;
                t = t.parentNode
            }
            t = null
        }
        return t
    }
}(), wysihtml5.dom.getStyle = function() {
    function e(e) {
        return e.replace(n, function(e) {
            return e.charAt(1).toUpperCase()
        })
    }
    var t = {"float": "styleFloat"in document.createElement("div").style ? "styleFloat" : "cssFloat"}, n = /\-[a-z]/g;
    return function(n) {
        return{from: function(r) {
                if (r.nodeType === wysihtml5.ELEMENT_NODE) {
                    var i = r.ownerDocument, s = t[n] || e(n), o = r.style, u = r.currentStyle, f = o[s];
                    if (f)
                        return f;
                    if (u)
                        try {
                            return u[s]
                        } catch (l) {
                        }
                    var s = i.defaultView || i.parentWindow, i = ("height" === n || "width" === n) && "TEXTAREA" === r.nodeName, h;
                    if (s.getComputedStyle)
                        return i && (h = o.overflow, o.overflow = "hidden"), r = s.getComputedStyle(r, null).getPropertyValue(n), i && (o.overflow = h || ""), r
                }
            }}
    }
}(), wysihtml5.dom.hasElementWithTagName = function() {
    var e = {}, t = 1;
    return function(n, r) {
        var i = (n._wysihtml5_identifier || (n._wysihtml5_identifier = t++)) + ":" + r, s = e[i];
        return s || (s = e[i] = n.getElementsByTagName(r)), 0 < s.length
    }
}(), function(e) {
    var t = {}, n = 1;
    e.dom.hasElementWithClassName = function(r, i) {
        if (!e.browser.supportsNativeGetElementsByClassName())
            return!!r.querySelector("." + i);
        var s = (r._wysihtml5_identifier || (r._wysihtml5_identifier = n++)) + ":" + i, o = t[s];
        return o || (o = t[s] = r.getElementsByClassName(i)), 0 < o.length
    }
}(wysihtml5), wysihtml5.dom.insert = function(e) {
    return{after: function(t) {
            t.parentNode.insertBefore(e, t.nextSibling)
        }, before: function(t) {
            t.parentNode.insertBefore(e, t)
        }, into: function(t) {
            t.appendChild(e)
        }}
}, wysihtml5.dom.insertCSS = function(e) {
    return e = e.join("\n"), {into: function(t) {
            var n = t.head || t.getElementsByTagName("head")[0], r = t.createElement("style");
            r.type = "text/css", r.styleSheet ? r.styleSheet.cssText = e : r.appendChild(t.createTextNode(e)), n && n.appendChild(r)
        }}
}, wysihtml5.dom.observe =
        function(e, t, n) {
            for (var t = "string" == typeof t ? [t] : t, r, i, s = 0, o = t.length; s < o; s++)
                i = t[s], e.addEventListener ? e.addEventListener(i, n, !1) : (r = function(t) {
                    "target"in t || (t.target = t.srcElement), t.preventDefault = t.preventDefault || function() {
                        this.returnValue = !1
                    }, t.stopPropagation = t.stopPropagation || function() {
                        this.cancelBubble = !0
                    }, n.call(e, t)
                }, e.attachEvent("on" + i, r));
            return{stop: function() {
                    for (var i, s = 0, o = t.length; s < o; s++)
                        i = t[s], e.removeEventListener ? e.removeEventListener(i, n, !1) : e.detachEvent("on" + i, r)
                }}
        }, wysihtml5.dom.parse = function() {
    function e(t, i) {
        var s = t.childNodes, o = s.length, u;
        u = n[t.nodeType];
        var a = 0;
        u = u && u(t);
        if (!u)
            return null;
        for (a = 0; a < o; a++)
            (newChild = e(s[a], i)) && u.appendChild(newChild);
        return i && 1 >= u.childNodes.length && u.nodeName.toLowerCase() === r && !u.attributes.length ? u.firstChild : u
    }
    function t(e, t) {
        var t = t.toLowerCase(), n;
        if (n = "IMG" == e.nodeName)
            if (n = "src" == t) {
                var r;
                try {
                    r = e.complete && !e.mozMatchesSelector(":-moz-broken")
                } catch (i) {
                    e.complete && "complete" === e.readyState && (r = !0)
                }
                n = !0 === r
            }
        return n ? e.src : u && "outerHTML"in e ? -1 != e.outerHTML.toLowerCase().indexOf(" " + t + "=") ? e.getAttribute(t) : null : e.getAttribute(t)
    }
    var n = {1: function(e) {
            var n, s, u = o.tags;
            s = e.nodeName.toLowerCase(), n = e.scopeName;
            if (e._wysihtml5)
                return null;
            e._wysihtml5 = 1;
            if ("wysihtml5-temp" === e.className)
                return null;
            n && "HTML" != n && (s = n + ":" + s), "outerHTML"in e && !wysihtml5.browser.autoClosesUnclosedTags() && "P" === e.nodeName && "</p>" !== e.outerHTML.slice(-4).toLowerCase() && (s = "div");
            if (s in u) {
                n = u[s];
                if (!n || n.remove)
                    return null;
                n = "string" == typeof n ? {rename_tag: n} : n
            } else {
                if (!e.firstChild)
                    return null;
                n = {rename_tag: r}
            }
            s = e.ownerDocument.createElement(n.rename_tag || s);
            var u = {}, l = n.set_class, p = n.add_class, v = n.set_attributes, m = n.check_attributes, y = o.classes, b = 0, w = [];
            n = [];
            var E = [], S = [], x;
            v && (u = wysihtml5.lang.object(v).clone());
            if (m)
                for (x in m)
                    if (v = a[m[x]])
                        v = v(t(e, x)), "string" == typeof v && (u[x] = v);
            l && w.push(l);
            if (p)
                for (x in p)
                    if (v = f[p[x]])
                        l = v(t(e, x)), "string" == typeof l && w.push(l);
            y["_wysihtml5-temp-placeholder"] = 1, (S = e.getAttribute("class")) && (w = w.concat(S.split(i)));
            for (p = w.length; b < p; b++)
                e = w[b], y[e] && n.push(e);
            for (y = n.length; y--; )
                e = n[y], wysihtml5.lang.array(E).contains(e) || E.unshift(e);
            E.length && (u["class"] = E.join(" "));
            for (x in u)
                try {
                    s.setAttribute(x, u[x])
                } catch (T) {
                }
            return u.src && ("undefined" != typeof u.width && s.setAttribute("width", u.width), "undefined" != typeof u.height && s.setAttribute("height", u.height)), s
        }, 3: function(e) {
            return e.ownerDocument.createTextNode(e.data)
        }}, r = "span", i = /\s+/, s = {tags: {}, classes: {}}, o = {}, u = !wysihtml5.browser.supportsGetAttributeCorrectly(), a = {url: function() {
            var e = /^https?:\/\//i;
            return function(t) {
                return!t || !t.match(e) ? null : t.replace(e, function(e) {
                    return e.toLowerCase()
                })
            }
        }(), alt: function() {
            var e = /[^ a-z0-9_\-]/gi;
            return function(t) {
                return t ? t.replace(e, "") : ""
            }
        }(), numbers: function() {
            var e = /\D/g;
            return function(t) {
                return(t = (t || "").replace(e, "")) || null
            }
        }()}, f = {align_img: function() {
            var e = {left: "wysiwyg-float-left", right: "wysiwyg-float-right"};
            return function(t) {
                return e[("" + t).toLowerCase()]
            }
        }(), align_text: function() {
            var e = {left: "wysiwyg-text-align-left", right: "wysiwyg-text-align-right", center: "wysiwyg-text-align-center", justify: "wysiwyg-text-align-justify"};
            return function(t) {
                return e[("" + t).toLowerCase()]
            }
        }(), clear_br: function() {
            var e = {left: "wysiwyg-clear-left", right: "wysiwyg-clear-right", both: "wysiwyg-clear-both", all: "wysiwyg-clear-both"};
            return function(t) {
                return e[("" + t).toLowerCase()]
            }
        }(), size_font: function() {
            var e = {1: "wysiwyg-font-size-xx-small", 2: "wysiwyg-font-size-small", 3: "wysiwyg-font-size-medium", 4: "wysiwyg-font-size-large", 5: "wysiwyg-font-size-x-large", 6: "wysiwyg-font-size-xx-large", 7: "wysiwyg-font-size-xx-large", "-": "wysiwyg-font-size-smaller", "+": "wysiwyg-font-size-larger"};
            return function(t) {
                return e[("" + t).charAt(0)]
            }
        }()};
    return function(t, n, r, i) {
        wysihtml5.lang.object(o).merge(s).merge(n).get();
        for (var r = r || t.ownerDocument || document, n = r.createDocumentFragment(), u = "string" == typeof t, t = u ? wysihtml5.dom.getAsDom(t, r) : t; t.firstChild; )
            r = t.firstChild, t.removeChild(r), (r = e(r, i)) && n.appendChild(r);
        return t.innerHTML = "", t.appendChild(n), u ? wysihtml5.quirks.getCorrectInnerHTML(t) : t
    }
}(), wysihtml5.dom.removeEmptyTextNodes = function(e) {
    for (var t = wysihtml5.lang.array(e.childNodes).get(), n = t.length, r = 0; r < n; r++)
        e = t[r], e.nodeType === wysihtml5.TEXT_NODE && "" === e.data && e.parentNode.removeChild(e)
}, wysihtml5.dom.renameElement = function(e, t) {
    for (var n = e.ownerDocument.createElement(t), r; r = e.firstChild; )
        n.appendChild(r);
    return wysihtml5.dom.copyAttributes(["align", "className"]).from(e).to(n), e.parentNode.replaceChild(n, e), n
}, wysihtml5.dom.replaceWithChildNodes = function(e) {
    if (e.parentNode)
        if (e.firstChild) {
            for (var t = e.ownerDocument.createDocumentFragment(); e.firstChild; )
                t.appendChild(e.firstChild);
            e.parentNode.replaceChild(t, e)
        } else
            e.parentNode.removeChild(e)
}, function(e) {
    function t(e) {
        var t = e.ownerDocument.createElement("br");
        e.appendChild(t)
    }
    e.resolveList = function(n) {
        if ("MENU" === n.nodeName || "UL" === n.nodeName || "OL" === n.nodeName) {
            var r = n.ownerDocument.createDocumentFragment(), i = n.previousSibling, s, o, u;
            for (i && "block" !== e.getStyle("display").from(i) && t(r); u = n.firstChild; ) {
                for (s = u.lastChild; i = u.firstChild; )
                    o = (o = i === s) && "block" !== e.getStyle("display").from(i) && "BR" !== i.nodeName, r.appendChild(i), o && t(r);
                u.parentNode.removeChild(u)
            }
            n.parentNode.replaceChild(r, n)
        }
    }
}(wysihtml5.dom), function(e) {
    var t = document, n = "parent,top,opener,frameElement,frames,localStorage,globalStorage,sessionStorage,indexedDB".split(","), r = "open,close,openDialog,showModalDialog,alert,confirm,prompt,openDatabase,postMessage,XMLHttpRequest,XDomainRequest".split(","), i = ["referrer", "write", "open", "close"];
    e.dom.Sandbox = Base.extend({constructor: function(t, n) {
            this.callback = t || e.EMPTY_FUNCTION, this.config = e.lang.object({}).merge(n).get(), this.iframe = this._createIframe()
        }, insertInto: function(e) {
            "string" == typeof e && (e = t.getElementById(e)), e.appendChild(this.iframe)
        }, getIframe: function() {
            return this.iframe
        }, getWindow: function() {
            this._readyError()
        }, getDocument: function() {
            this._readyError()
        }, destroy: function() {
            var e = this.getIframe();
            e.parentNode.removeChild(e)
        }, _readyError: function() {
            throw Error("wysihtml5.Sandbox: Sandbox iframe isn't loaded yet")
        }, _createIframe: function() {
            var n = this, r = t.createElement("iframe");
            return r.className = "wysihtml5-sandbox", e.dom.setAttributes({security: "restricted", allowtransparency: "true", frameborder: 0, width: 0, height: 0, marginwidth: 0, marginheight: 0}).on(r), e.browser.throwsMixedContentWarningWhenIframeSrcIsEmpty() && (r.src = "javascript:'<html></html>'"), r.onload = function() {
                r.onreadystatechange = r.onload = null, n._onLoadIframe(r)
            }, r.onreadystatechange = function() {
                /loaded|complete/.test(r.readyState) && (r.onreadystatechange = r.onload = null, n._onLoadIframe(r))
            }, r
        }, _onLoadIframe: function(s) {
            if (e.dom.contains(t.documentElement, s)) {
                var o = this, u = s.contentWindow, f = s.contentWindow.document, l = this._getHtml({charset: t.characterSet || t.charset || "utf-8", stylesheets: this.config.stylesheets});
                f.open("text/html", "replace"), f.write(l), f.close(), this.getWindow = function() {
                    return s.contentWindow
                }, this.getDocument = function() {
                    return s.contentWindow.document
                }, u.onerror = function(e, t, n) {
                    throw Error("wysihtml5.Sandbox: " + e, t, n)
                };
                if (!e.browser.supportsSandboxedIframes()) {
                    var h, l = 0;
                    for (h = n.length; l < h; l++)
                        this._unset(u, n[l]);
                    l = 0;
                    for (h = r.length; l < h; l++)
                        this._unset(u, r[l], e.EMPTY_FUNCTION);
                    l = 0;
                    for (h = i.length; l < h; l++)
                        this._unset(f, i[l]);
                    this._unset(f, "cookie", "", !0)
                }
                this.loaded = !0, setTimeout(function() {
                    o.callback(o)
                }, 0)
            }
        }, _getHtml: function(t) {
            var n = t.stylesheets, r = "", i = 0, s;
            if (n = "string" == typeof n ? [n] : n)
                for (s = n.length; i < s; i++)
                    r += '<link rel="stylesheet" href="' + n[i] + '">';
            return t.stylesheets = r, e.lang.string('<!DOCTYPE html><html><head><meta charset="#{charset}">#{stylesheets}</head><body></body></html>').interpolate(t)
        }, _unset: function(t, n, r, i) {
            try {
                t[n] = r
            } catch (s) {
            }
            try {
                t.__defineGetter__(n, function() {
                    return r
                })
            } catch (o) {
            }
            if (i)
                try {
                    t.__defineSetter__(n, function() {
                    })
                } catch (u) {
                }
            if (!e.browser.crashesWhenDefineProperty(n))
                try {
                    var a = {get: function() {
                            return r
                        }};
                    i && (a.set = function() {
                    }), Object.defineProperty(t, n, a)
                } catch (f) {
                }
        }})
}(wysihtml5), function() {
    var e = {className: "class"};
    wysihtml5.dom.setAttributes = function(t) {
        return{on: function(n) {
                for (var r in t)
                    n.setAttribute(e[r] || r, t[r])
            }}
    }
}(), wysihtml5.dom.setStyles = function(e) {
    return{on: function(t) {
            t = t.style;
            if ("string" == typeof e)
                t.cssText += ";" + e;
            else
                for (var n in e)
                    "float" === n ? (t.cssFloat = e[n], t.styleFloat = e[n]) : t[n] = e[n]
        }}
}, function(e) {
    e.simulatePlaceholder = function(t, n, r) {
        var i = function() {
            n.hasPlaceholderSet() && n.clear(), e.removeClass(n.element, "placeholder")
        }, s = function() {
            n.isEmpty() && (n.setValue(r), e.addClass(n.element, "placeholder"))
        };
        t.observe("set_placeholder", s).observe("unset_placeholder", i).observe("focus:composer", i).observe("paste:composer", i).observe("blur:composer", s), s()
    }
}(wysihtml5.dom), function(e) {
    var t = document.documentElement;
    "textContent"in t ? (e.setTextContent = function(e, t) {
        e.textContent = t
    }, e.getTextContent = function(e) {
        return e.textContent
    }) : "innerText"in t ? (e.setTextContent = function(e, t) {
        e.innerText = t
    }, e.getTextContent = function(e) {
        return e.innerText
    }) : (e.setTextContent = function(e, t) {
        e.nodeValue = t
    }, e.getTextContent = function(e) {
        return e.nodeValue
    })
}(wysihtml5.dom), wysihtml5.quirks.cleanPastedHTML = function() {
    var e = {"a u": wysihtml5.dom.replaceWithChildNodes};
    return function(t, n, r) {
        var n = n || e, r = r || t.ownerDocument || document, i = "string" == typeof t, s, o, u, a = 0, t = i ? wysihtml5.dom.getAsDom(t, r) : t;
        for (u in n) {
            s = t.querySelectorAll(u), r = n[u];
            for (o = s.length; a < o; a++)
                r(s[a])
        }
        return i ? t.innerHTML : t
    }
}(), function(e) {
    var t = e.dom;
    e.quirks.ensureProperClearing = function() {
        var e = function() {
            var e = this;
            setTimeout(function() {
                var t = e.innerHTML.toLowerCase();
                if ("<p>&nbsp;</p>" == t || "<p>&nbsp;</p><p>&nbsp;</p>" == t)
                    e.innerHTML = ""
            }, 0)
        };
        return function(n) {
            t.observe(n.element, ["cut", "keydown"], e)
        }
    }(), e.quirks.ensureProperClearingOfLists = function() {
        var n = ["OL", "UL", "MENU"];
        return function(r) {
            t.observe(r.element, "keydown", function(i) {
                if (i.keyCode === e.BACKSPACE_KEY) {
                    var s = r.selection.getSelectedNode(), i = r.element;
                    i.firstChild && e.lang.array(n).contains(i.firstChild.nodeName) && (s = t.getParentElement(s, {nodeName: n})) && s == i.firstChild && 1 >= s.childNodes.length && (s.firstChild ? "" === s.firstChild.innerHTML : 1) && s.parentNode.removeChild(s)
                }
            })
        }
    }()
}(wysihtml5), function(e) {
    e.quirks.getCorrectInnerHTML = function(t) {
        var n = t.innerHTML;
        if (-1 === n.indexOf("%7E"))
            return n;
        var t = t.querySelectorAll("[href*='~'], [src*='~']"), r, i, s, o;
        o = 0;
        for (s = t.length; o < s; o++)
            r = t[o].href || t[o].src, i = e.lang.string(r).replace("~").by("%7E"), n = e.lang.string(n).replace(i).by(r);
        return n
    }
}(wysihtml5), function(e) {
    var t = e.dom, n = "LI,P,H1,H2,H3,H4,H5,H6".split(","), r = ["UL", "OL", "MENU"];
    e.quirks.insertLineBreakOnReturn = function(i) {
        function s(n) {
            if (n = t.getParentElement(n, {nodeName: ["P", "DIV"]}, 2)) {
                var r = document.createTextNode(e.INVISIBLE_SPACE);
                t.insert(r).before(n), t.replaceWithChildNodes(n), i.selection.selectNode(r)
            }
        }
        t.observe(i.element.ownerDocument, "keydown", function(o) {
            var u = o.keyCode;
            if (!(o.shiftKey || u !== e.ENTER_KEY && u !== e.BACKSPACE_KEY)) {
                var l = i.selection.getSelectedNode();
                (l = t.getParentElement(l, {nodeName: n}, 4)) ? "LI" !== l.nodeName || u !== e.ENTER_KEY && u !== e.BACKSPACE_KEY ? l.nodeName.match(/H[1-6]/) && u === e.ENTER_KEY && setTimeout(function() {
                    s(i.selection.getSelectedNode())
                }, 0) : setTimeout(function() {
                    var e = i.selection.getSelectedNode(), n;
                    e && ((n = t.getParentElement(e, {nodeName: r}, 2)) || s(e))
                }, 0) : u === e.ENTER_KEY && !e.browser.insertsLineBreaksOnReturn() && (i.commands.exec("insertLineBreak"), o.preventDefault())
            }
        })
    }
}(wysihtml5), function(e) {
    e.quirks.redraw = function(t) {
        e.dom.addClass(t, "wysihtml5-quirks-redraw"), e.dom.removeClass(t, "wysihtml5-quirks-redraw");
        try {
            var n = t.ownerDocument;
            n.execCommand("italic", !1, null), n.execCommand("italic", !1, null)
        } catch (r) {
        }
    }
}(wysihtml5), function(e) {
    var t = e.dom;
    e.Selection = Base.extend({constructor: function(e) {
            window.rangy.init(), this.editor = e, this.composer = e.composer, this.doc = this.composer.doc
        }, getBookmark: function() {
            var e = this.getRange();
            return e && e.cloneRange()
        }, setBookmark: function(e) {
            e && this.setSelection(e)
        }, setBefore: function(e) {
            var t = rangy.createRange(this.doc);
            return t.setStartBefore(e), t.setEndBefore(e), this.setSelection(t)
        }, setAfter: function(e) {
            var t = rangy.createRange(this.doc);
            return t.setStartAfter(e), t.setEndAfter(e), this.setSelection(t)
        }, selectNode: function(n) {
            var r = rangy.createRange(this.doc), i = n.nodeType === e.ELEMENT_NODE, s = "canHaveHTML"in n ? n.canHaveHTML : "IMG" !== n.nodeName, o = i ? n.innerHTML : n.data, o = "" === o || o === e.INVISIBLE_SPACE, u = t.getStyle("display").from(n), u = "block" === u || "list-item" === u;
            if (o && i && s)
                try {
                    n.innerHTML = e.INVISIBLE_SPACE
                } catch (a) {
                }
            s ? r.selectNodeContents(n) : r.selectNode(n), s && o && i ? r.collapse(u) : s && o && (r.setStartAfter(n), r.setEndAfter(n)), this.setSelection(r)
        }, getSelectedNode: function(e) {
            return e && this.doc.selection && "Control" === this.doc.selection.type && (e = this.doc.selection.createRange()) && e.length ? e.item(0) : (e = this.getSelection(this.doc), e.focusNode === e.anchorNode ? e.focusNode : (e = this.getRange(this.doc)) ? e.commonAncestorContainer : this.doc.body)
        }, executeAndRestore: function(t, n) {
            var r = this.doc.body, i = n && r.scrollTop, s = n && r.scrollLeft, o = '<span class="_wysihtml5-temp-placeholder">' + e.INVISIBLE_SPACE + "</span>", u = this.getRange(this.doc);
            if (u) {
                o = u.createContextualFragment(o), u.insertNode(o);
                try {
                    t(u.startContainer, u.endContainer)
                } catch (a) {
                    setTimeout(function() {
                        throw a
                    }, 0)
                }
                (caretPlaceholder = this.doc.querySelector("._wysihtml5-temp-placeholder")) ? (u = rangy.createRange(this.doc), u.selectNode(caretPlaceholder), u.deleteContents(), this.setSelection(u)) : r.focus(), n && (r.scrollTop = i, r.scrollLeft = s);
                try {
                    caretPlaceholder.parentNode.removeChild(caretPlaceholder)
                } catch (f) {
                }
            } else
                t(r, r)
        }, executeAndRestoreSimple: function(e) {
            var t, n, r = this.getRange(), i = this.doc.body, s;
            if (r) {
                t = r.getNodes([3]), i = t[0] || r.startContainer, s = t[t.length - 1] || r.endContainer, t = i === r.startContainer ? r.startOffset : 0, n = s === r.endContainer ? r.endOffset : s.length;
                try {
                    e(r.startContainer, r.endContainer)
                } catch (o) {
                    setTimeout(function() {
                        throw o
                    }, 0)
                }
                e = rangy.createRange(this.doc);
                try {
                    e.setStart(i, t)
                } catch (u) {
                }
                try {
                    e.setEnd(s, n)
                } catch (a) {
                }
                try {
                    this.setSelection(e)
                } catch (f) {
                }
            } else
                e(i, i)
        }, insertHTML: function(e) {
            var e = rangy.createRange(this.doc).createContextualFragment(e), t = e.lastChild;
            this.insertNode(e), t && this.setAfter(t)
        }, insertNode: function(e) {
            var t = this.getRange();
            t && t.insertNode(e)
        }, surround: function(e) {
            var t = this.getRange();
            if (t)
                try {
                    t.surroundContents(e), this.selectNode(e)
                } catch (n) {
                    e.appendChild(t.extractContents()), t.insertNode(e)
                }
        }, scrollIntoView: function() {
            var t = this.doc, n = t.documentElement.scrollHeight > t.documentElement.offsetHeight, r;
            (r = t._wysihtml5ScrollIntoViewElement) || (r = t.createElement("span"), r.innerHTML = e.INVISIBLE_SPACE), r = t._wysihtml5ScrollIntoViewElement = r;
            if (n) {
                this.insertNode(r);
                var n = r, i = 0;
                if (n.parentNode)
                    do
                        i += n.offsetTop || 0, n = n.offsetParent;
                    while (n);
                n = i, r.parentNode.removeChild(r), n > t.body.scrollTop && (t.body.scrollTop = n)
            }
        }, selectLine: function() {
            e.browser.supportsSelectionModify() ? this._selectLine_W3C() : this.doc.selection && this._selectLine_MSIE()
        }, _selectLine_W3C: function() {
            var e = this.doc.defaultView.getSelection();
            e.modify("extend", "left", "lineboundary"), e.modify("extend", "right", "lineboundary")
        }, _selectLine_MSIE: function() {
            var e = this.doc.selection.createRange(), t = e.boundingTop, n = this.doc.body.scrollWidth, r;
            if (e.moveToPoint) {
                0 === t && (r = this.doc.createElement("span"), this.insertNode(r), t = r.offsetTop, r.parentNode.removeChild(r)), t += 1;
                for (r = - 10; r < n; r += 2)
                    try {
                        e.moveToPoint(r, t);
                        break
                    } catch (i) {
                    }
                for (r = this.doc.selection.createRange(); 0 <= n; n--)
                    try {
                        r.moveToPoint(n, t);
                        break
                    } catch (s) {
                    }
                e.setEndPoint("EndToEnd", r), e.select()
            }
        }, getText: function() {
            var e = this.getSelection();
            return e ? e.toString() : ""
        }, getNodes: function(e, t) {
            var n = this.getRange();
            return n ? n.getNodes([e], t) : []
        }, getRange: function() {
            var e = this.getSelection();
            return e && e.rangeCount && e.getRangeAt(0)
        }, getSelection: function() {
            return rangy.getSelection(this.doc.defaultView || this.doc.parentWindow)
        }, setSelection: function(e) {
            return rangy.getSelection(this.doc.defaultView || this.doc.parentWindow).setSingleRange(e)
        }})
}(wysihtml5), function(e, t) {
    function n(e, n) {
        return t.dom.isCharacterDataNode(e) ? 0 == n ? !!e.previousSibling : n == e.length ? !!e.nextSibling : !0 : 0 < n && n < e.childNodes.length
    }
    function r(e, n, i) {
        var s;
        t.dom.isCharacterDataNode(n) && (0 == i ? (i = t.dom.getNodeIndex(n), n = n.parentNode) : i == n.length ? (i = t.dom.getNodeIndex(n) + 1, n = n.parentNode) : s = t.dom.splitDataNode(n, i));
        if (!s) {
            s = n.cloneNode(!1), s.id && s.removeAttribute("id");
            for (var o; o = n.childNodes[i]; )
                s.appendChild(o);
            t.dom.insertAfter(s, n)
        }
        return n == e ? s : r(e, s.parentNode, t.dom.getNodeIndex(s))
    }
    function i(t) {
        this.firstTextNode = (this.isElementMerge = t.nodeType == e.ELEMENT_NODE) ? t.lastChild : t, this.textNodes = [this.firstTextNode]
    }
    function s(e, t, n, r) {
        this.tagNames = e || [o], this.cssClass = t || "", this.similarClassRegExp = n, this.normalize = r, this.applyToAnyTagName = !1
    }
    var o = "span", u = /\s+/g;
    i.prototype = {doMerge: function() {
            for (var e = [], t, n, r = 0, i = this.textNodes.length; r < i; ++r)
                t = this.textNodes[r], n = t.parentNode, e[r] = t.data, r && (n.removeChild(t), n.hasChildNodes() || n.parentNode.removeChild(n));
            return this.firstTextNode.data = e = e.join("")
        }, getLength: function() {
            for (var e = this.textNodes.length, t = 0; e--; )
                t += this.textNodes[e].length;
            return t
        }, toString: function() {
            for (var e = [], t = 0, n = this.textNodes.length; t < n; ++t)
                e[t] = "'" + this.textNodes[t].data + "'";
            return"[Merge(" + e.join(",") + ")]"
        }}, s.prototype = {getAncestorWithClass: function(n) {
            for (var r; n; ) {
                if (this.cssClass)
                    if (r = this.cssClass, n.className) {
                        var i = n.className.match(this.similarClassRegExp) || [];
                        r = i[i.length - 1] === r
                    } else
                        r = !1;
                else
                    r = !0;
                if (n.nodeType == e.ELEMENT_NODE && t.dom.arrayContains(this.tagNames, n.tagName.toLowerCase()) && r)
                    return n;
                n = n.parentNode
            }
            return!1
        }, postApply: function(e, t) {
            for (var n = e[0], r = e[e.length - 1], s = [], o, u = n, a = r, f = 0, l = r.length, c, h, p = 0, d = e.length; p < d; ++p)
                if (c = e[p], h = this.getAdjacentMergeableTextNode(c.parentNode, !1)) {
                    if (o || (o = new i(h), s.push(o)), o.textNodes.push(c), c === n && (u = o.firstTextNode, f = u.length), c === r)
                        a = o.firstTextNode, l = o.getLength()
                } else
                    o = null;
            if (n = this.getAdjacentMergeableTextNode(r.parentNode, !0))
                o || (o = new i(r), s.push(o)), o.textNodes.push(n);
            if (s.length) {
                p = 0;
                for (d = s.length; p < d; ++p)
                    s[p].doMerge();
                t.setStart(u, f), t.setEnd(a, l)
            }
        }, getAdjacentMergeableTextNode: function(t, n) {
            var r = t.nodeType == e.TEXT_NODE, i = r ? t.parentNode : t, s = n ? "nextSibling" : "previousSibling";
            if (r) {
                if ((r = t[s]) && r.nodeType == e.TEXT_NODE)
                    return r
            } else if ((r = i[s]) && this.areElementsMergeable(t, r))
                return r[n ? "firstChild" : "lastChild"];
            return null
        }, areElementsMergeable: function(e, n) {
            var r;
            if (r = t.dom.arrayContains(this.tagNames, (e.tagName || "").toLowerCase()))
                if (r = t.dom.arrayContains(this.tagNames, (n.tagName || "").toLowerCase()))
                    if (r = e.className.replace(u, " ") == n.className.replace(u, " "))
                        e:if (e.attributes.length != n.attributes.length)
                            r = !1;
                        else {
                            r = 0;
                            for (var i = e.attributes.length, s, o; r < i; ++r)
                                if (s = e.attributes[r], o = s.name, "class" != o && (o = n.attributes.getNamedItem(o), s.specified != o.specified || s.specified && s.nodeValue !== o.nodeValue)) {
                                    r = !1;
                                    break e
                                }
                            r = !0
                        }
            return r
        }, createContainer: function(e) {
            return e = e.createElement(this.tagNames[0]), this.cssClass && (e.className = this.cssClass), e
        }, applyToTextNode: function(e) {
            var n = e.parentNode;
            1 == n.childNodes.length && t.dom.arrayContains(this.tagNames, n.tagName.toLowerCase()) ? this.cssClass && (e = this.cssClass, n.className ? (n.className && (n.className = n.className.replace(this.similarClassRegExp, "")), n.className += " " + e) : n.className = e) : (n = this.createContainer(t.dom.getDocument(e)), e.parentNode.insertBefore(n, e), n.appendChild(e))
        }, isRemovable: function(n) {
            return t.dom.arrayContains(this.tagNames, n.tagName.toLowerCase()) && e.lang.string(n.className).trim() == this.cssClass
        }, undoToTextNode: function(e, t, i) {
            t.containsNode(i) || (e = t.cloneRange(), e.selectNode(i), e.isPointInRange(t.endContainer, t.endOffset) && n(t.endContainer, t.endOffset) && (r(i, t.endContainer, t.endOffset), t.setEndAfter(i)), e.isPointInRange(t.startContainer, t.startOffset) && n(t.startContainer, t.startOffset) && (i = r(i, t.startContainer, t.startOffset))), this.similarClassRegExp && i.className && (i.className = i.className.replace(this.similarClassRegExp, ""));
            if (this.isRemovable(i)) {
                t = i;
                for (i = t.parentNode; t.firstChild; )
                    i.insertBefore(t.firstChild, t);
                i.removeChild(t)
            }
        }, applyToRange: function(t) {
            var n = t.getNodes([e.TEXT_NODE]);
            if (!n.length)
                try {
                    var r = this.createContainer(t.endContainer.ownerDocument);
                    t.surroundContents(r), this.selectNode(t, r);
                    return
                } catch (i) {
                }
            t.splitBoundaries(), n = t.getNodes([e.TEXT_NODE]);
            if (n.length) {
                for (var s = 0, o = n.length; s < o; ++s)
                    r = n[s], this.getAncestorWithClass(r) || this.applyToTextNode(r);
                t.setStart(n[0], 0), r = n[n.length - 1], t.setEnd(r, r.length), this.normalize && this.postApply(n, t)
            }
        }, undoToRange: function(t) {
            var n = t.getNodes([e.TEXT_NODE]), r, i;
            n.length ? (t.splitBoundaries(), n = t.getNodes([e.TEXT_NODE])) : (n = t.endContainer.ownerDocument.createTextNode(e.INVISIBLE_SPACE), t.insertNode(n), t.selectNode(n), n = [n]);
            for (var s = 0, o = n.length; s < o; ++s)
                r = n[s], (i = this.getAncestorWithClass(r)) && this.undoToTextNode(r, t, i);
            1 == o ? this.selectNode(t, n[0]) : (t.setStart(n[0], 0), r = n[n.length - 1], t.setEnd(r, r.length), this.normalize && this.postApply(n, t))
        }, selectNode: function(t, n) {
            var r = n.nodeType === e.ELEMENT_NODE, i = "canHaveHTML"in n ? n.canHaveHTML : !0, s = r ? n.innerHTML : n.data;
            if ((s = "" === s || s === e.INVISIBLE_SPACE) && r && i)
                try {
                    n.innerHTML = e.INVISIBLE_SPACE
                } catch (o) {
                }
            t.selectNodeContents(n), s && r ? t.collapse(!1) : s && (t.setStartAfter(n), t.setEndAfter(n))
        }, getTextSelectedByRange: function(e, t) {
            var n = t.cloneRange();
            n.selectNodeContents(e);
            var r = n.intersection(t), r = r ? r.toString() : "";
            return n.detach(), r
        }, isAppliedToRange: function(t) {
            var n = [], r, i = t.getNodes([e.TEXT_NODE]);
            if (!i.length)
                return(r = this.getAncestorWithClass(t.startContainer)) ? [r] : !1;
            for (var s = 0, o = i.length, u; s < o; ++s) {
                u = this.getTextSelectedByRange(i[s], t), r = this.getAncestorWithClass(i[s]);
                if ("" != u && !r)
                    return!1;
                n.push(r)
            }
            return n
        }, toggleRange: function(e) {
            this.isAppliedToRange(e) ? this.undoToRange(e) : this.applyToRange(e)
        }}, e.selection.HTMLApplier = s
}(wysihtml5, rangy), wysihtml5.Commands = Base.extend({constructor: function(e) {
        this.editor = e, this.composer = e.composer, this.doc = this.composer.doc
    }, support: function(e) {
        return wysihtml5.browser.supportsCommand(this.doc, e)
    }, exec: function(e, t) {
        var n = wysihtml5.commands[e], r = n && n.exec;
        this.editor.fire("beforecommand:composer");
        if (r)
            return r.call(n, this.composer, e, t);
        try {
            return this.doc.execCommand(e, !1, t)
        } catch (i) {
        }
        this.editor.fire("aftercommand:composer")
    }, state: function(e, t) {
        var n = wysihtml5.commands[e], r = n && n.state;
        if (r)
            return r.call(n, this.composer, e, t);
        try {
            return this.doc.queryCommandState(e)
        } catch (i) {
            return!1
        }
    }, value: function(e) {
        var t = wysihtml5.commands[e], n = t && t.value;
        if (n)
            return n.call(t, this.composer, e);
        try {
            return this.doc.queryCommandValue(e)
        } catch (r) {
            return null
        }
    }}), function(e) {
    e.commands.bold = {exec: function(t, n) {
            return e.commands.formatInline.exec(t, n, "b")
        }, state: function(t, n) {
            return e.commands.formatInline.state(t, n, "b")
        }, value: function() {
        }}
}(wysihtml5), function(e) {
    function t(t, s) {
        var o = t.doc, u = "_wysihtml5-temp-" + +(new Date), f = 0, l, c, h;
        e.commands.formatInline.exec(t, n, r, u, /non-matching-class/g), l = o.querySelectorAll(r + "." + u);
        for (u = l.length; f < u; f++)
            for (h in c = l[f], c.removeAttribute("class"), s)
                c.setAttribute(h, s[h]);
        f = c, 1 === u && (h = i.getTextContent(c), u = !!c.querySelector("*"), h = "" === h || h === e.INVISIBLE_SPACE, !u && h && (i.setTextContent(c, c.href), o = o.createTextNode(" "), t.selection.setAfter(c), t.selection.insertNode(o), f = o)), t.selection.setAfter(f)
    }
    var n, r = "A", i = e.dom;
    e.commands.createLink = {exec: function(e, n, r) {
            var s = this.state(e, n);
            s ? e.selection.executeAndRestore(function() {
                for (var e = s.length, t = 0, n, r, o; t < e; t++)
                    n = s[t], r = i.getParentElement(n, {nodeName: "code"}), o = i.getTextContent(n), o.match(i.autoLink.URL_REG_EXP) && !r ? i.renameElement(n, "code") : i.replaceWithChildNodes(n)
            }) : (r = "object" == typeof r ? r : {href: r}, t(e, r))
        }, state: function(t, n) {
            return e.commands.formatInline.state(t, n, "A")
        }, value: function() {
            return n
        }}
}(wysihtml5), function(e) {
    var t = /wysiwyg-font-size-[a-z]+/g;
    e.commands.fontSize = {exec: function(n, r, i) {
            return e.commands.formatInline.exec(n, r, "span", "wysiwyg-font-size-" + i, t)
        }, state: function(n, r, i) {
            return e.commands.formatInline.state(n, r, "span", "wysiwyg-font-size-" + i, t)
        }, value: function() {
        }}
}(wysihtml5), function(e) {
    var t = /wysiwyg-color-[a-z]+/g;
    e.commands.foreColor = {exec: function(n, r, i) {
            return e.commands.formatInline.exec(n, r, "span", "wysiwyg-color-" + i, t)
        }, state: function(n, r, i) {
            return e.commands.formatInline.state(n, r, "span", "wysiwyg-color-" + i, t)
        }, value: function() {
        }}
}(wysihtml5), function(e) {
    function t(t) {
        for (t = t.previousSibling; t && t.nodeType === e.TEXT_NODE && !e.lang.string(t.data).trim(); )
            t = t.previousSibling;
        return t
    }
    function n(t) {
        for (t = t.nextSibling; t && t.nodeType === e.TEXT_NODE && !e.lang.string(t.data).trim(); )
            t = t.nextSibling;
        return t
    }
    function r(e) {
        return"BR" === e.nodeName || "block" === o.getStyle("display").from(e) ? !0 : !1
    }
    function i(t, n, r, i) {
        if (i)
            var s = o.observe(t, "DOMNodeInserted", function(t) {
                var t = t.target, n;
                t.nodeType === e.ELEMENT_NODE && (n = o.getStyle("display").from(t), "inline" !== n.substr(0, 6) && (t.className += " " + i))
            });
        t.execCommand(n, !1, r), s && s.stop()
    }
    function s(e, r) {
        e.selection.selectLine(), e.selection.surround(r);
        var i = n(r), s = t(r);
        i && "BR" === i.nodeName && i.parentNode.removeChild(i), s && "BR" === s.nodeName && s.parentNode.removeChild(s), (i = r.lastChild) && "BR" === i.nodeName && i.parentNode.removeChild(i), e.selection.selectNode(r)
    }
    var o = e.dom, u = "H1,H2,H3,H4,H5,H6,P,BLOCKQUOTE,DIV".split(",");
    e.commands.formatBlock = {exec: function(l, p, v, m, g) {
            var y = l.doc, w = this.state(l, p, v, m, g), E, v = "string" == typeof v ? v.toUpperCase() : v;
            if (w)
                l.selection.executeAndRestoreSimple(function() {
                    g && (w.className = w.className.replace(g, ""));
                    var i = !!e.lang.string(w.className).trim();
                    if (!i && w.nodeName === (v || "DIV")) {
                        var i = w, s = i.ownerDocument, u = n(i), f = t(i);
                        u && !r(u) && i.parentNode.insertBefore(s.createElement("br"), u), f && !r(f) && i.parentNode.insertBefore(s.createElement("br"), i), o.replaceWithChildNodes(w)
                    } else
                        i && o.renameElement(w, "DIV")
                });
            else {
                if (null === v || e.lang.array(u).contains(v))
                    if (E = l.selection.getSelectedNode(), w = o.getParentElement(E, {nodeName: u})) {
                        l.selection.executeAndRestoreSimple(function() {
                            v && (w = o.renameElement(w, v));
                            if (m) {
                                var e = w;
                                e.className ? (e.className = e.className.replace(g, ""), e.className += " " + m) : e.className = m
                            }
                        });
                        return
                    }
                l.commands.support(p) ? i(y, p, v || "DIV", m) : (w = y.createElement(v || "DIV"), m && (w.className = m), s(l, w))
            }
        }, state: function(e, t, n, r, i) {
            return n = "string" == typeof n ? n.toUpperCase() : n, e = e.selection.getSelectedNode(), o.getParentElement(e, {nodeName: n, className: r, classRegExp: i})
        }, value: function() {
        }}
}(wysihtml5), function(e) {
    function t(t, i, s) {
        var o = t + ":" + i;
        if (!r[o]) {
            var u = r, f = e.selection.HTMLApplier, l = n[t], t = l ? [t.toLowerCase(), l.toLowerCase()] : [t.toLowerCase()];
            u[o] = new f(t, i, s, !0)
        }
        return r[o]
    }
    var n = {strong: "b", em: "i", b: "strong", i: "em"}, r = {};
    e.commands.formatInline = {exec: function(e, n, r, i, s) {
            n = e.selection.getRange();
            if (!n)
                return!1;
            t(r, i, s).toggleRange(n), e.selection.setSelection(n)
        }, state: function(r, i, s, o, u) {
            var i = r.doc, f = n[s] || s;
            return!e.dom.hasElementWithTagName(i, s) && !e.dom.hasElementWithTagName(i, f) || o && !e.dom.hasElementWithClassName(i, o) ? !1 : (r = r.selection.getRange(), r ? t(s, o, u).isAppliedToRange(r) : !1)
        }, value: function() {
        }}
}(wysihtml5), function(e) {
    e.commands.insertHTML = {exec: function(e, t, n) {
            e.commands.support(t) ? e.doc.execCommand(t, !1, n) : e.selection.insertHTML(n)
        }, state: function() {
            return!1
        }, value: function() {
        }}
}(wysihtml5), function(e) {
    e.commands.insertImage = {exec: function(t, n, r) {
            var r = "object" == typeof r ? r : {src: r}, i = t.doc, n = this.state(t), s;
            if (n)
                t.selection.setBefore(n), r = n.parentNode, r.removeChild(n), e.dom.removeEmptyTextNodes(r), "A" === r.nodeName && !r.firstChild && (t.selection.setAfter(r), r.parentNode.removeChild(r)), e.quirks.redraw(t.element);
            else {
                n = i.createElement("IMG");
                for (s in r)
                    n[s] = r[s];
                t.selection.insertNode(n), e.browser.hasProblemsSettingCaretAfterImg() ? (r = i.createTextNode(e.INVISIBLE_SPACE), t.selection.insertNode(r), t.selection.setAfter(r)) : t.selection.setAfter(n)
            }
        }, state: function(t) {
            var n;
            return e.dom.hasElementWithTagName(t.doc, "IMG") ? (n = t.selection.getSelectedNode(), n ? "IMG" === n.nodeName ? n : n.nodeType !== e.ELEMENT_NODE ? !1 : (n = t.selection.getText(), (n = e.lang.string(n).trim()) ? !1 : (t = t.selection.getNodes(e.ELEMENT_NODE, function(e) {
                return"IMG" === e.nodeName
            }), 1 !== t.length ? !1 : t[0])) : !1) : !1
        }, value: function(e) {
            return(e = this.state(e)) && e.src
        }}
}(wysihtml5), function(e) {
    var t = "<br>" + (e.browser.needsSpaceAfterLineBreak() ? " " : "");
    e.commands.insertLineBreak = {exec: function(n, r) {
            n.commands.support(r) ? (n.doc.execCommand(r, !1, null), e.browser.autoScrollsToCaret() || n.selection.scrollIntoView()) : n.commands.exec("insertHTML", t)
        }, state: function() {
            return!1
        }, value: function() {
        }}
}(wysihtml5), function(e) {
    e.commands.insertOrderedList = {exec: function(t, n) {
            var r = t.doc, i, s, o;
            t.commands.support(n) ? r.execCommand(n, !1, null) : (i = t.selection.getSelectedNode(), (o = e.dom.getParentElement(i, {nodeName: ["UL", "OL"]}, 4)) ? t.selection.executeAndRestoreSimple(function() {
                "OL" === o.nodeName ? e.dom.resolveList(o) : ("UL" === o.nodeName || "MENU" === o.nodeName) && e.dom.renameElement(o, "ol")
            }) : (s = r.createElement("span"), t.selection.surround(s), r = "" === s.innerHTML || s.innerHTML === e.INVISIBLE_SPACE, t.selection.executeAndRestoreSimple(function() {
                o = e.dom.convertToList(s, "ol")
            }), r && t.selection.selectNode(o.querySelector("li"))))
        }, state: function(e, t) {
            try {
                return e.doc.queryCommandState(t)
            } catch (n) {
                return!1
            }
        }, value: function() {
        }}
}(wysihtml5), function(e) {
    e.commands.insertUnorderedList = {exec: function(t, n) {
            var r = t.doc, i, s, o;
            t.commands.support(n) ? r.execCommand(n, !1, null) : (i = t.selection.getSelectedNode(), (o = e.dom.getParentElement(i, {nodeName: ["UL", "OL"]})) ? t.selection.executeAndRestoreSimple(function() {
                "UL" === o.nodeName ? e.dom.resolveList(o) : ("OL" === o.nodeName || "MENU" === o.nodeName) && e.dom.renameElement(o, "ul")
            }) : (s = r.createElement("span"), t.selection.surround(s), r = "" === s.innerHTML || s.innerHTML === e.INVISIBLE_SPACE, t.selection.executeAndRestoreSimple(function() {
                o = e.dom.convertToList(s, "ul")
            }), r && t.selection.selectNode(o.querySelector("li"))))
        }, state: function(e, t) {
            try {
                return e.doc.queryCommandState(t)
            } catch (n) {
                return!1
            }
        }, value: function() {
        }}
}(wysihtml5), function(e) {
    e.commands.italic = {exec: function(t, n) {
            return e.commands.formatInline.exec(t, n, "i")
        }, state: function(t, n) {
            return e.commands.formatInline.state(t, n, "i")
        }, value: function() {
        }}
}(wysihtml5), function(e) {
    var t = /wysiwyg-text-align-[a-z]+/g;
    e.commands.justifyCenter = {exec: function(n) {
            return e.commands.formatBlock.exec(n, "formatBlock", null, "wysiwyg-text-align-center", t)
        }, state: function(n) {
            return e.commands.formatBlock.state(n, "formatBlock", null, "wysiwyg-text-align-center", t)
        }, value: function() {
        }}
}(wysihtml5), function(e) {
    var t = /wysiwyg-text-align-[a-z]+/g;
    e.commands.justifyLeft = {exec: function(n) {
            return e.commands.formatBlock.exec(n, "formatBlock", null, "wysiwyg-text-align-left", t)
        }, state: function(n) {
            return e.commands.formatBlock.state(n, "formatBlock", null, "wysiwyg-text-align-left", t)
        }, value: function() {
        }}
}(wysihtml5), function(e) {
    var t = /wysiwyg-text-align-[a-z]+/g;
    e.commands.justifyRight = {exec: function(n) {
            return e.commands.formatBlock.exec(n, "formatBlock", null, "wysiwyg-text-align-right", t)
        }, state: function(n) {
            return e.commands.formatBlock.state(n, "formatBlock", null, "wysiwyg-text-align-right", t)
        }, value: function() {
        }}
}(wysihtml5), function(e) {
    var t = /wysiwyg-text-decoration-underline/g;
    e.commands.underline = {exec: function(n, r) {
            return e.commands.formatInline.exec(n, r, "span", "wysiwyg-text-decoration-underline", t)
        }, state: function(n, r) {
            return e.commands.formatInline.state(n, r, "span", "wysiwyg-text-decoration-underline", t)
        }, value: function() {
        }}
}(wysihtml5), function(e) {
    var t = '<span id="_wysihtml5-undo" class="_wysihtml5-temp">' + e.INVISIBLE_SPACE + "</span>", n = '<span id="_wysihtml5-redo" class="_wysihtml5-temp">' + e.INVISIBLE_SPACE + "</span>", r = e.dom;
    e.UndoManager = e.lang.Dispatcher.extend({constructor: function(e) {
            this.editor = e, this.composer = e.composer, this.element = this.composer.element, this.history = [this.composer.getValue()], this.position = 1, this.composer.commands.support("insertHTML") && this._observe()
        }, _observe: function() {
            var i = this, s = this.composer.sandbox.getDocument(), o;
            r.observe(this.element, "keydown", function(e) {
                if (!(e.altKey || !e.ctrlKey && !e.metaKey)) {
                    var t = e.keyCode, n = 90 === t && e.shiftKey || 89 === t;
                    90 === t && !e.shiftKey ? (i.undo(), e.preventDefault()) : n && (i.redo(), e.preventDefault())
                }
            }), r.observe(this.element, "keydown", function(e) {
                e = e.keyCode, e !== o && (o = e, (8 === e || 46 === e) && i.transact())
            });
            if (e.browser.hasUndoInContextMenu()) {
                var u, f, l = function() {
                    for (var e; e = s.querySelector("._wysihtml5-temp"); )
                        e.parentNode.removeChild(e);
                    clearInterval(u)
                };
                r.observe(this.element, "contextmenu", function() {
                    l(), i.composer.selection.executeAndRestoreSimple(function() {
                        i.element.lastChild && i.composer.selection.setAfter(i.element.lastChild), s.execCommand("insertHTML", !1, t), s.execCommand("insertHTML", !1, n), s.execCommand("undo", !1, null)
                    }), u = setInterval(function() {
                        s.getElementById("_wysihtml5-redo") ? (l(), i.redo()) : s.getElementById("_wysihtml5-undo") || (l(), i.undo
                                ())
                    }, 400), f || (f = !0, r.observe(document, "mousedown", l), r.observe(s, ["mousedown", "paste", "cut", "copy"], l))
                })
            }
            this.editor.observe("newword:composer", function() {
                i.transact()
            }).observe("beforecommand:composer", function() {
                i.transact()
            })
        }, transact: function() {
            var e = this.history[this.position - 1], t = this.composer.getValue();
            t != e && (40 < (this.history.length = this.position) && (this.history.shift(), this.position--), this.position++, this.history.push(t))
        }, undo: function() {
            this.transact(), 1 >= this.position || (this.set(this.history[--this.position - 1]), this.editor.fire("undo:composer"))
        }, redo: function() {
            this.position >= this.history.length || (this.set(this.history[++this.position - 1]), this.editor.fire("redo:composer"))
        }, set: function(e) {
            this.composer.setValue(e), this.editor.focus(!0)
        }})
}(wysihtml5), wysihtml5.views.View = Base.extend({constructor: function(e, t, n) {
        this.parent = e, this.element = t, this.config = n, this._observeViewChange()
    }, _observeViewChange: function() {
        var e = this;
        this.parent.observe("beforeload", function() {
            e.parent.observe("change_view", function(t) {
                t === e.name ? (e.parent.currentView = e, e.show(), setTimeout(function() {
                    e.focus()
                }, 0)) : e.hide()
            })
        })
    }, focus: function() {
        if (this.element.ownerDocument.querySelector(":focus") !== this.element)
            try {
                this.element.focus()
            } catch (e) {
            }
    }, hide: function() {
        this.element.style.display = "none"
    }, show: function() {
        this.element.style.display = ""
    }, disable: function() {
        this.element.setAttribute("disabled", "disabled")
    }, enable: function() {
        this.element.removeAttribute("disabled")
    }}), function(e) {
    var t = e.dom, n = e.browser;
    e.views.Composer = e.views.View.extend({name: "composer", CARET_HACK: "<br>", constructor: function(e, t, n) {
            this.base(e, t, n), this.textarea = this.parent.textarea, this._initSandbox()
        }, clear: function() {
            this.element.innerHTML = n.displaysCaretInEmptyContentEditableCorrectly() ? "" : this.CARET_HACK
        }, getValue: function(t) {
            var n = this.isEmpty() ? "" : e.quirks.getCorrectInnerHTML(this.element);
            return t && (n = this.parent.parse(n)), n = e.lang.string(n).replace(e.INVISIBLE_SPACE).by("")
        }, setValue: function(e, t) {
            t && (e = this.parent.parse(e)), this.element.innerHTML = e
        }, show: function() {
            this.iframe.style.display = this._displayStyle || "", this.disable(), this.enable()
        }, hide: function() {
            this._displayStyle = t.getStyle("display").from(this.iframe), "none" === this._displayStyle && (this._displayStyle = null), this.iframe.style.display = "none"
        }, disable: function() {
            this.element.removeAttribute("contentEditable"), this.base()
        }, enable: function() {
            this.element.setAttribute("contentEditable", "true"), this.base()
        }, focus: function(t) {
            e.browser.doesAsyncFocus() && this.hasPlaceholderSet() && this.clear(), this.base();
            var n = this.element.lastChild;
            t && n && ("BR" === n.nodeName ? this.selection.setBefore(this.element.lastChild) : this.selection.setAfter(this.element.lastChild))
        }, getTextContent: function() {
            return t.getTextContent(this.element)
        }, hasPlaceholderSet: function() {
            return this.getTextContent() == this.textarea.element.getAttribute("placeholder")
        }, isEmpty: function() {
            var e = this.element.innerHTML;
            return"" === e || e === this.CARET_HACK || this.hasPlaceholderSet() || "" === this.getTextContent() && !this.element.querySelector("blockquote, ul, ol, img, embed, object, table, iframe, svg, video, audio, button, input, select, textarea")
        }, _initSandbox: function() {
            var e = this;
            this.sandbox = new t.Sandbox(function() {
                e._create()
            }, {stylesheets: this.config.stylesheets}), this.iframe = this.sandbox.getIframe();
            var n = document.createElement("input");
            n.type = "hidden", n.name = "_wysihtml5_mode", n.value = 1;
            var r = this.textarea.element;
            t.insert(this.iframe).after(r), t.insert(n).after(r)
        }, _create: function() {
            var r = this;
            this.doc = this.sandbox.getDocument(), this.element = this.doc.body, this.textarea = this.parent.textarea, this.element.innerHTML = this.textarea.getValue(!0), this.enable(), this.selection = new e.Selection(this.parent), this.commands = new e.Commands(this.parent), t.copyAttributes("className,spellcheck,title,lang,dir,accessKey".split(",")).from(this.textarea.element).to(this.element), t.addClass(this.element, this.config.composerClassName), this.config.style && this.style(), this.observe();
            var i = this.config.name;
            i && (t.addClass(this.element, i), t.addClass(this.iframe, i)), (i = "string" == typeof this.config.placeholder ? this.config.placeholder : this.textarea.element.getAttribute("placeholder")) && t.simulatePlaceholder(this.parent, this, i), this.commands.exec("styleWithCSS", !1), this._initAutoLinking(), this._initObjectResizing(), this._initUndoManager(), (this.textarea.element.hasAttribute("autofocus") || document.querySelector(":focus") == this.textarea.element) && setTimeout(function() {
                r.focus()
            }, 100), e.quirks.insertLineBreakOnReturn(this), n.clearsContentEditableCorrectly() || e.quirks.ensureProperClearing(this), n.clearsListsInContentEditableCorrectly() || e.quirks.ensureProperClearingOfLists(this), this.initSync && this.config.sync && this.initSync(), this.textarea.hide(), this.parent.fire("beforeload").fire("load")
        }, _initAutoLinking: function() {
            var r = this, i = n.canDisableAutoLinking(), s = n.doesAutoLinkingInContentEditable();
            i && this.commands.exec("autoUrlDetect", !1);
            if (this.config.autoLink) {
                (!s || s && i) && this.parent.observe("newword:composer", function() {
                    r.selection.executeAndRestore(function(e, n) {
                        t.autoLink(n.parentNode)
                    })
                });
                var o = this.sandbox.getDocument().getElementsByTagName("a"), u = t.autoLink.URL_REG_EXP, f = function(n) {
                    return n = e.lang.string(t.getTextContent(n)).trim(), "www." === n.substr(0, 4) && (n = "http://" + n), n
                };
                t.observe(this.element, "keydown", function(e) {
                    if (o.length) {
                        var e = r.selection.getSelectedNode(e.target.ownerDocument), n = t.getParentElement(e, {nodeName: "A"}, 4), i;
                        n && (i = f(n), setTimeout(function() {
                            var e = f(n);
                            e !== i && e.match(u) && n.setAttribute("href", e)
                        }, 0))
                    }
                })
            }
        }, _initObjectResizing: function() {
            var r = ["width", "height"], i = r.length, s = this.element;
            this.commands.exec("enableObjectResizing", this.config.allowObjectResizing), this.config.allowObjectResizing ? n.supportsEvent("resizeend") && t.observe(s, "resizeend", function(t) {
                for (var t = t.target || t.srcElement, n = t.style, o = 0, u; o < i; o++)
                    u = r[o], n[u] && (t.setAttribute(u, parseInt(n[u], 10)), n[u] = "");
                e.quirks.redraw(s)
            }) : n.supportsEvent("resizestart") && t.observe(s, "resizestart", function(e) {
                e.preventDefault()
            })
        }, _initUndoManager: function() {
            new e.UndoManager(this.parent)
        }})
}(wysihtml5), function(e) {
    var t = e.dom, n = document, r = window, i = n.createElement("div"), s = "background-color,color,cursor,font-family,font-size,font-style,font-variant,font-weight,line-height,letter-spacing,text-align,text-decoration,text-indent,text-rendering,word-break,word-wrap,word-spacing".split(","), o = "background-color,border-collapse,border-bottom-color,border-bottom-style,border-bottom-width,border-left-color,border-left-style,border-left-width,border-right-color,border-right-style,border-right-width,border-top-color,border-top-style,border-top-width,clear,display,float,margin-bottom,margin-left,margin-right,margin-top,outline-color,outline-offset,outline-width,outline-style,padding-left,padding-right,padding-top,padding-bottom,position,top,left,right,bottom,z-index,vertical-align,text-align,-webkit-box-sizing,-moz-box-sizing,-ms-box-sizing,box-sizing,-webkit-box-shadow,-moz-box-shadow,-ms-box-shadow,box-shadow,-webkit-border-top-right-radius,-moz-border-radius-topright,border-top-right-radius,-webkit-border-bottom-right-radius,-moz-border-radius-bottomright,border-bottom-right-radius,-webkit-border-bottom-left-radius,-moz-border-radius-bottomleft,border-bottom-left-radius,-webkit-border-top-left-radius,-moz-border-radius-topleft,border-top-left-radius,width,height".split(","), u = "width,height,top,left,right,bottom".split(","), a = ["html             { height: 100%; }", "body             { min-height: 100%; padding: 0; margin: 0; margin-top: -1px; padding-top: 1px; }", "._wysihtml5-temp { display: none; }", e.browser.isGecko ? "body.placeholder { color: graytext !important; }" : "body.placeholder { color: #a9a9a9 !important; }", "body[disabled]   { background-color: #eee !important; color: #999 !important; cursor: default !important; }", "img:-moz-broken  { -moz-force-broken-image-icon: 1; height: 24px; width: 24px; }"], f = function(e) {
        if (e.setActive)
            try {
                e.setActive()
            } catch (i) {
            }
        else {
            var s = e.style, o = n.documentElement.scrollTop || n.body.scrollTop, u = n.documentElement.scrollLeft || n.body.scrollLeft, s = {position: s.position, top: s.top, left: s.left, WebkitUserSelect: s.WebkitUserSelect};
            t.setStyles({position: "absolute", top: "-99999px", left: "-99999px", WebkitUserSelect: "none"}).on(e), e.focus(), t.setStyles(s).on(e), r.scrollTo && r.scrollTo(u, o)
        }
    };
    e.views.Composer.prototype.style = function() {
        var l = this, p = n.querySelector(":focus"), v = this.textarea.element, m = v.hasAttribute("placeholder"), y = m && v.getAttribute("placeholder");
        this.focusStylesHost = this.focusStylesHost || i.cloneNode(!1), this.blurStylesHost = this.blurStylesHost || i.cloneNode(!1), m && v.removeAttribute("placeholder"), v === p && v.blur(), t.copyStyles(o).from(v).to(this.iframe).andTo(this.blurStylesHost), t.copyStyles(s).from(v).to(this.element).andTo(this.blurStylesHost), t.insertCSS(a).into(this.element.ownerDocument), f(v), t.copyStyles(o).from(v).to(this.focusStylesHost), t.copyStyles(s).from(v).to(this.focusStylesHost);
        var w = e.lang.array(o).without(["display"]);
        return p ? p.focus() : v.blur(), m && v.setAttribute("placeholder", y), e.browser.hasCurrentStyleProperty() || t.observe(r, "resize", function() {
            var e = t.getStyle("display").from(v);
            v.style.display = "", t.copyStyles(u).from(v).to(l.iframe).andTo(l.focusStylesHost).andTo(l.blurStylesHost), v.style.display = e
        }), this.parent.observe("focus:composer", function() {
            t.copyStyles(w).from(l.focusStylesHost).to(l.iframe), t.copyStyles(s).from(l.focusStylesHost).to(l.element)
        }), this.parent.observe("blur:composer", function() {
            t.copyStyles(w).from(l.blurStylesHost).to(l.iframe), t.copyStyles(s).from(l.blurStylesHost).to(l.element)
        }), this
    }
}(wysihtml5), function(e) {
    var t = e.dom, n = e.browser, r = {66: "bold", 73: "italic", 85: "underline"};
    e.views.Composer.prototype.observe = function() {
        var i = this, s = this.getValue(), o = this.sandbox.getIframe(), u = this.element, f = n.supportsEventsInIframeCorrectly() ? u : this.sandbox.getWindow(), l = n.supportsEvent("drop") ? ["drop", "paste"] : ["dragdrop", "paste"];
        t.observe(o, "DOMNodeRemoved", function() {
            clearInterval(h), i.parent.fire("destroy:composer")
        });
        var h = setInterval(function() {
            t.contains(document.documentElement, o) || (clearInterval(h), i.parent.fire("destroy:composer"))
        }, 250);
        t.observe(f, "focus", function() {
            i.parent.fire("focus").fire("focus:composer"), setTimeout(function() {
                s = i.getValue()
            }, 0)
        }), t.observe(f, "blur", function() {
            s !== i.getValue() && i.parent.fire("change").fire("change:composer"), i.parent.fire("blur").fire("blur:composer")
        }), e.browser.isIos() && t.observe(u, "blur", function() {
            var e = u.ownerDocument.createElement("input"), t = document.documentElement.scrollTop || document.body.scrollTop, n = document.documentElement.scrollLeft || document.body.scrollLeft;
            try {
                i.selection.insertNode(e)
            } catch (r) {
                u.appendChild(e)
            }
            e.focus(), e.parentNode.removeChild(e), window.scrollTo(n, t)
        }), t.observe(u, "dragenter", function() {
            i.parent.fire("unset_placeholder")
        }), n.firesOnDropOnlyWhenOnDragOverIsCancelled() && t.observe(u, ["dragover", "dragenter"], function(e) {
            e.preventDefault()
        }), t.observe(u, l, function(e) {
            var t = e.dataTransfer, r;
            t && n.supportsDataTransfer() && (r = t.getData("text/html") || t.getData("text/plain")), r ? (u.focus(), i.commands.exec("insertHTML", r), i.parent.fire("paste").fire("paste:composer"), e.stopPropagation(), e.preventDefault()) : setTimeout(function() {
                i.parent.fire("paste").fire("paste:composer")
            }, 0)
        }), t.observe(u, "keyup", function(t) {
            t = t.keyCode, (t === e.SPACE_KEY || t === e.ENTER_KEY) && i.parent.fire("newword:composer")
        }), this.parent.observe("paste:composer", function() {
            setTimeout(function() {
                i.parent.fire("newword:composer")
            }, 0)
        }), n.canSelectImagesInContentEditable() || t.observe(u, "mousedown", function(e) {
            var t = e.target;
            "IMG" === t.nodeName && (i.selection.selectNode(t), e.preventDefault())
        }), t.observe(u, "keydown", function(e) {
            var t = r[e.keyCode];
            (e.ctrlKey || e.metaKey) && t && (i.commands.exec(t), e.preventDefault())
        }), t.observe(u, "keydown", function(t) {
            var n = i.selection.getSelectedNode(!0), r = t.keyCode;
            n && "IMG" === n.nodeName && (r === e.BACKSPACE_KEY || r === e.DELETE_KEY) && (r = n.parentNode, r.removeChild(n), "A" === r.nodeName && !r.firstChild && r.parentNode.removeChild(r), setTimeout(function() {
                e.quirks.redraw(u)
            }, 0), t.preventDefault())
        });
        var p = {IMG: "Image: ", A: "Link: "};
        t.observe(u, "mouseover", function(e) {
            var e = e.target, t = e.nodeName;
            "A" !== t && "IMG" !== t || (t = p[t] + (e.getAttribute("href") || e.getAttribute("src")), e.setAttribute("title", t))
        })
    }
}(wysihtml5), function(e) {
    e.views.Synchronizer = Base.extend({constructor: function(e, t, n) {
            this.editor = e, this.textarea = t, this.composer = n, this._observe()
        }, fromComposerToTextarea: function(t) {
            this.textarea.setValue(e.lang.string(this.composer.getValue()).trim(), t)
        }, fromTextareaToComposer: function(e) {
            var t = this.textarea.getValue();
            t ? this.composer.setValue(t, e) : (this.composer.clear(), this.editor.fire("set_placeholder"))
        }, sync: function(e) {
            "textarea" === this.editor.currentView.name ? this.fromTextareaToComposer(e) : this.fromComposerToTextarea(e)
        }, _observe: function() {
            var t, n = this, r = this.textarea.element.form, i = function() {
                t = setInterval(function() {
                    n.fromComposerToTextarea()
                }, 400)
            }, s = function() {
                clearInterval(t), t = null
            };
            i(), r && (e.dom.observe(r, "submit", function() {
                n.sync(!0)
            }), e.dom.observe(r, "reset", function() {
                setTimeout(function() {
                    n.fromTextareaToComposer()
                }, 0)
            })), this.editor.observe("change_view", function(e) {
                e === "composer" && !t ? (n.fromTextareaToComposer(!0), i()) : e === "textarea" && (n.fromComposerToTextarea(!0), s())
            }), this.editor.observe("destroy:composer", s)
        }})
}(wysihtml5), wysihtml5.views.Textarea = wysihtml5.views.View.extend({name: "textarea", constructor: function(e, t, n) {
        this.base(e, t, n), this._observe()
    }, clear: function() {
        this.element.value = ""
    }, getValue: function(e) {
        var t = this.isEmpty() ? "" : this.element.value;
        return e && (t = this.parent.parse(t)), t
    }, setValue: function(e, t) {
        t && (e = this.parent.parse(e)), this.element.value = e
    }, hasPlaceholderSet: function() {
        var e = wysihtml5.browser.supportsPlaceholderAttributeOn(this.element), t = this.element.getAttribute("placeholder") || null, n = this.element.value;
        return e && !n || n === t
    }, isEmpty: function() {
        return!wysihtml5.lang.string(this.element.value).trim() || this.hasPlaceholderSet()
    }, _observe: function() {
        var e = this.element, t = this.parent, n = {focusin: "focus", focusout: "blur"}, r = wysihtml5.browser.supportsEvent("focusin") ? ["focusin", "focusout", "change"] : ["focus", "blur", "change"];
        t.observe("beforeload", function() {
            wysihtml5.dom.observe(e, r, function(e) {
                e = n[e.type] || e.type, t.fire(e).fire(e + ":textarea")
            }), wysihtml5.dom.observe(e, ["paste", "drop"], function() {
                setTimeout(function() {
                    t.fire("paste").fire("paste:textarea")
                }, 0)
            })
        })
    }}), function(e) {
    var t = e.dom;
    e.toolbar.Dialog = e.lang.Dispatcher.extend({constructor: function(e, t) {
            this.link = e, this.container = t
        }, _observe: function() {
            if (!this._observed) {
                var n = this, r = function(e) {
                    var t = n._serialize();
                    t == n.elementToChange ? n.fire("edit", t) : n.fire("save", t), n.hide(), e.preventDefault(), e.stopPropagation()
                };
                t.observe(n.link, "click", function() {
                    t.hasClass(n.link, "wysihtml5-command-dialog-opened") && setTimeout(function() {
                        n.hide()
                    }, 0)
                }), t.observe(this.container, "keydown", function(t) {
                    var i = t.keyCode;
                    i === e.ENTER_KEY && r(t), i === e.ESCAPE_KEY && n.hide()
                }), t.delegate(this.container, "[data-wysihtml5-dialog-action=save]", "click", r), t.delegate(this.container, "[data-wysihtml5-dialog-action=cancel]", "click", function(e) {
                    n.fire("cancel"), n.hide(), e.preventDefault(), e.stopPropagation()
                });
                for (var i = this.container.querySelectorAll("input, select, textarea"), s = 0, o = i.length, u = function() {
                    clearInterval(n.interval)
                }; s < o; s++)
                    t.observe(i[s], "change", u);
                this._observed = !0
            }
        }, _serialize: function() {
            for (var e = this.elementToChange || {}, t = this.container.querySelectorAll("[data-wysihtml5-dialog-field]"), n = t.length, r = 0; r < n; r++)
                e[t[r].getAttribute("data-wysihtml5-dialog-field")] = t[r].value;
            return e
        }, _interpolate: function(e) {
            for (var t, n, r = document.querySelector(":focus"), i = this.container.querySelectorAll("[data-wysihtml5-dialog-field]"), s = i.length, o = 0; o < s; o++)
                t = i[o], t !== r && (!e || "hidden" !== t.type) && (n = t.getAttribute("data-wysihtml5-dialog-field"), n = this.elementToChange ? this.elementToChange[n] || "" : t.defaultValue, t.value = n)
        }, show: function(e) {
            var n = this, r = this.container.querySelector("input, select, textarea");
            this.elementToChange = e, this._observe(), this._interpolate(), e && (this.interval = setInterval(function() {
                n._interpolate(!0)
            }, 500)), t.addClass(this.link, "wysihtml5-command-dialog-opened"), this.container.style.display = "", this.fire("show");
            if (r && !e)
                try {
                    r.focus()
                } catch (i) {
                }
        }, hide: function() {
            clearInterval(this.interval), this.elementToChange = null, t.removeClass(this.link, "wysihtml5-command-dialog-opened"), this.container.style.display = "none", this.fire("hide")
        }})
}(wysihtml5), function(e) {
    var t = e.dom, n = {position: "relative"}, r = {left: 0, margin: 0, opacity: 0, overflow: "hidden", padding: 0, position: "absolute", top: 0, zIndex: 1}, i = {cursor: "inherit", fontSize: "50px", height: "50px", marginTop: "-25px", outline: 0, padding: 0, position: "absolute", right: "-4px", top: "50%"}, s = {"x-webkit-speech": "", speech: ""};
    e.toolbar.Speech = function(o, u) {
        var l = document.createElement("input");
        if (e.browser.supportsSpeechApiOn(l)) {
            var h = document.createElement("div");
            e.lang.object(r).merge({width: u.offsetWidth + "px", height: u.offsetHeight + "px"}), t.insert(l).into(h), t.insert(h).into(u), t.setStyles(i).on(l), t.setAttributes(s).on(l), t.setStyles(r).on(h), t.setStyles(n).on(u), t.observe(l, "onwebkitspeechchange"in l ? "webkitspeechchange" : "speechchange", function() {
                o.execCommand("insertText", l.value), l.value = ""
            }), t.observe(l, "click", function(e) {
                t.hasClass(u, "wysihtml5-command-disabled") && e.preventDefault(), e.stopPropagation()
            })
        } else
            u.style.display = "none"
    }
}(wysihtml5), function(e) {
    var t = e.dom;
    e.toolbar.Toolbar = Base.extend({constructor: function(t, n) {
            this.editor = t, this.container = "string" == typeof n ? document.getElementById(n) : n, this.composer = t.composer, this._getLinks("command"), this._getLinks("action"), this._observe(), this.show();
            for (var r = this.container.querySelectorAll("[data-wysihtml5-command=insertSpeech]"), i = r.length, s = 0; s < i; s++)
                new e.toolbar.Speech(this, r[s])
        }, _getLinks: function(t) {
            for (var n = this[t + "Links"] = e.lang.array(this.container.querySelectorAll("[data-wysihtml5-" + t + "]")).get(), r = n.length, i = 0, s = this[t + "Mapping"] = {}, o, u, a, f; i < r; i++)
                o = n[i], u = o.getAttribute("data-wysihtml5-" + t), a = o.getAttribute("data-wysihtml5-" + t + "-value"), f = this._getDialog(o, u), s[u + ":" + a] = {link: o, name: u, value: a, dialog: f, state: !1}
        }, _getDialog: function(t, n) {
            var r = this, i = this.container.querySelector("[data-wysihtml5-dialog='" + n + "']"), s, o;
            return i && (s = new e.toolbar.Dialog(t, i), s.observe("show", function() {
                o = r.composer.selection.getBookmark(), r.editor.fire("show:dialog", {command: n, dialogContainer: i, commandLink: t})
            }), s.observe("save", function(e) {
                o && r.composer.selection.setBookmark(o), r._execCommand(n, e), r.editor.fire("save:dialog", {command: n, dialogContainer: i, commandLink: t})
            }), s.observe("cancel", function() {
                r.editor.focus(!1), r.editor.fire("cancel:dialog", {command: n, dialogContainer: i, commandLink: t})
            })), s
        }, execCommand: function(e, t) {
            if (!this.commandsDisabled) {
                var n = this.commandMapping[e + ":" + t];
                n && n.dialog && !n.state ? n.dialog.show() : this._execCommand(e, t)
            }
        }, _execCommand: function(e, t) {
            this.editor.focus(!1), this.composer.commands.exec(e, t), this._updateLinkStates()
        }, execAction: function(e) {
            var t = this.editor;
            switch (e) {
                case"change_view":
                    t.currentView === t.textarea ? t.fire("change_view", "composer") : t.fire("change_view", "textarea")
            }
        }, _observe: function() {
            for (var e = this, n = this.editor, r = this.container, i = this.commandLinks.concat(this.actionLinks), s = i.length, o = 0; o < s; o++)
                t.setAttributes({href: "javascript:;", unselectable: "on"}).on(i[o]);
            t.delegate(r, "[data-wysihtml5-command]", "mousedown", function(e) {
                e.preventDefault()
            }), t.delegate(r, "[data-wysihtml5-command]", "click", function(t) {
                var n = this.getAttribute("data-wysihtml5-command"), r = this.getAttribute("data-wysihtml5-command-value");
                e.execCommand(n, r), t.preventDefault()
            }), t.delegate(r, "[data-wysihtml5-action]", "click", function(t) {
                var n = this.getAttribute("data-wysihtml5-action");
                e.execAction(n), t.preventDefault()
            }), n.observe("focus:composer", function() {
                e.bookmark = null, clearInterval(e.interval), e.interval = setInterval(function() {
                    e._updateLinkStates()
                }, 500)
            }), n.observe("blur:composer", function() {
                clearInterval(e.interval)
            }), n.observe("destroy:composer", function() {
                clearInterval(e.interval)
            }), n.observe("change_view", function(n) {
                setTimeout(function() {
                    e.commandsDisabled = "composer" !== n, e._updateLinkStates(), e.commandsDisabled ? t.addClass(r, "wysihtml5-commands-disabled") : t.removeClass(r, "wysihtml5-commands-disabled")
                }, 0)
            })
        }, _updateLinkStates: function() {
            var n = this.commandMapping, r, i, s;
            for (r in n)
                if (s = n[r], this.commandsDisabled ? (i = !1, t.removeClass(s.link, "wysihtml5-command-active"), s.dialog && s.dialog.hide()) : (i = this.composer.commands.state(s.name, s.value), e.lang.object(i).isArray() && (i = 1 === i.length ? i[0] : !0), t.removeClass(s.link, "wysihtml5-command-disabled")), s.state !== i)
                    (s.state = i) ? (t.addClass(s.link, "wysihtml5-command-active"), s.dialog && ("object" == typeof i ? s.dialog.show(i) : s.dialog.hide())) : (t.removeClass(s.link, "wysihtml5-command-active"), s.dialog && s.dialog.hide())
        }, show: function() {
            this.container.style.display = ""
        }, hide: function() {
            this.container.style.display = "none"
        }})
}(wysihtml5), function(e) {
    var t = {name: void 0, style: !0, toolbar: void 0, autoLink: !0, parserRules: {tags: {br: {}, span: {}, div: {}, p: {}}, classes: {}}, parser: e.dom.parse, composerClassName: "wysihtml5-editor", bodyClassName: "wysihtml5-supported", stylesheets: [], placeholderText: void 0, allowObjectResizing: !0, supportTouchDevices: !0};
    e.Editor = e.lang.Dispatcher.extend({constructor: function(n, r) {
            this.textareaElement = "string" == typeof n ? document.getElementById(n) : n, this.config = e.lang.object({}).merge(t).merge(r).get(), this.currentView = this.textarea = new e.views.Textarea(this, this.textareaElement, this.config), this._isCompatible = e.browser.supported();
            if (!this._isCompatible || !this.config.supportTouchDevices && e.browser.isTouchDevice()) {
                var i = this;
                setTimeout(function() {
                    i.fire("beforeload").fire("load")
                }, 0)
            } else {
                e.dom.addClass(document.body, this.config.bodyClassName), this.currentView = this.composer = new e.views.Composer(this, this.textareaElement, this.config), "function" == typeof this.config.parser && this._initParser(), this.observe("beforeload", function() {
                    this.synchronizer = new e.views.Synchronizer(this, this.textarea, this.composer), this.config.toolbar && (this.toolbar = new e.toolbar.Toolbar(this, this.config.toolbar))
                });
                try {
                    console.log("Heya! This page is using wysihtml5 for rich text editing. Check out https://github.com/xing/wysihtml5");
                } catch (s) {
                }
            }
        }, isCompatible: function() {
            return this._isCompatible
        }, clear: function() {
            return this.currentView.clear(), this
        }, getValue: function(e) {
            return this.currentView.getValue(e)
        }, setValue: function(e, t) {
            return e ? (this.currentView.setValue(e, t), this) : this.clear()
        }, focus: function(e) {
            return this.currentView.focus(e), this
        }, disable: function() {
            return this.currentView.disable(), this
        }, enable: function() {
            return this.currentView.enable(), this
        }, isEmpty: function() {
            return this.currentView.isEmpty()
        }, hasPlaceholderSet: function() {
            return this.currentView.hasPlaceholderSet()
        }, parse: function(t) {
            var n = this.config.parser(t, this.config.parserRules, this.composer.sandbox.getDocument(), !0);
            return"object" == typeof t && e.quirks.redraw(t), n
        }, _initParser: function() {
            this.observe("paste:composer", function() {
                var t = this;
                t.composer.selection.executeAndRestore(function() {
                    e.quirks.cleanPastedHTML(t.composer.element), t.parse(t.composer.element)
                }, !0)
            }), this.observe("paste:textarea", function() {
                this.textarea.setValue(this.parse(this.textarea.getValue()))
            })
        }})
}(wysihtml5), !function(e) {
    "use strict";
    var t = function(e, t) {
        this.init("popover", e, t)
    };
    t.prototype = e.extend({}, e.fn.tooltip.Constructor.prototype, {constructor: t, setContent: function() {
            var e = this.tip(), t = this.getTitle(), n = this.getContent();
            e.find(".popover-title")[this.options.html ? "html" : "text"](t), e.find(".popover-content > *")[this.options.html ? "html" : "text"](n), e.removeClass("fade top bottom left right in")
        }, hasContent: function() {
            return this.getTitle() || this.getContent()
        }, getContent: function() {
            var e, t = this.$element, n = this.options;
            return e = t.attr("data-content") || (typeof n.content == "function" ? n.content.call(t[0]) : n.content), e
        }, tip: function() {
            return this.$tip || (this.$tip = e(this.options.template)), this.$tip
        }, destroy: function() {
            this.hide().$element.off("." + this.type).removeData(this.type)
        }}), e.fn.popover = function(n) {
        return this.each(function() {
            var r = e(this), i = r.data("popover"), s = typeof n == "object" && n;
            i || r.data("popover", i = new t(this, s)), typeof n == "string" && i[n]()
        })
    }, e.fn.popover.Constructor = t, e.fn.popover.defaults = e.extend({}, e.fn.tooltip.defaults, {placement: "right", trigger: "click", content: "", template: '<div class="popover"><div class="arrow"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>'})
}(window.jQuery), function(e) {
    var t = function() {
        var t = {}, n, r = 65, i, s = '<div class="colorpicker"><div class="colorpicker_color"><div><div></div></div></div><div class="colorpicker_hue"><div></div></div><div class="colorpicker_new_color"></div><div class="colorpicker_current_color"></div><div class="colorpicker_hex"><input type="text" maxlength="6" size="6" /></div><div class="colorpicker_rgb_r colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_rgb_g colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_rgb_b colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_hsb_h colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_hsb_s colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_hsb_b colorpicker_field"><input type="text" maxlength="3" size="3" /><span></span></div><div class="colorpicker_submit"></div></div>', o = {eventName: "click", onShow: function() {
            }, onBeforeShow: function() {
            }, onHide: function() {
            }, onChange: function() {
            }, onSubmit: function() {
            }, color: "ff0000", livePreview: !0, flat: !1}, u = function(t, n) {
            var r = q(t);
            e(n).data("colorpicker").fields.eq(1).val(r.r).end().eq(2).val(r.g).end().eq(3).val(r.b).end()
        }, a = function(t, n) {
            e(n).data("colorpicker").fields.eq(4).val(t.h).end().eq(5).val(t.s).end().eq(6).val(t.b).end()
        }, f = function(t, n) {
            e(n).data("colorpicker").fields.eq(0).val(U(t)).end()
        }, l = function(t, n) {
            e(n).data("colorpicker").selector.css("backgroundColor", "#" + U({h: t.h, s: 100, b: 100})), e(n).data("colorpicker").selectorIndic.css({left: parseInt(150 * t.s / 100, 10), top: parseInt(150 * (100 - t.b) / 100, 10)})
        }, c = function(t, n) {
            e(n).data("colorpicker").hue.css("top", parseInt(150 - 150 * t.h / 360, 10))
        }, h = function(t, n) {
            e(n).data("colorpicker").currentColor.css("backgroundColor", "#" + U(t))
        }, p = function(t, n) {
            e(n).data("colorpicker").newColor.css("backgroundColor", "#" + U(t))
        }, d = function(t) {
            var n = t.charCode || t.keyCode || -1;
            if (n > r && n <= 90 || n == 32)
                return!1;
            var i = e(this).parent().parent();
            i.data("colorpicker").livePreview === !0 && v.apply(this)
        }, v = function(t) {
            var n = e(this).parent().parent(), r;
            this.parentNode.className.indexOf("_hex") > 0 ? n.data("colorpicker").color = r = F(B(this.value)) : this.parentNode.className.indexOf("_hsb") > 0 ? n.data("colorpicker").color = r = P({h: parseInt(n.data("colorpicker").fields.eq(4).val(), 10), s: parseInt(n.data("colorpicker").fields.eq(5).val(), 10), b: parseInt(n.data("colorpicker").fields.eq(6).val(), 10)}) : n.data("colorpicker").color = r = I(H({r: parseInt(n.data("colorpicker").fields.eq(1).val(), 10), g: parseInt(n.data("colorpicker").fields.eq(2).val(), 10), b: parseInt(n.data("colorpicker").fields.eq(3).val(), 10)})), t && (u(r, n.get(0)), f(r, n.get(0)), a(r, n.get(0))), l(r, n.get(0)), c(r, n.get(0)), p(r, n.get(0)), n.data("colorpicker").onChange.apply(n, [r, U(r), q(r)])
        }, m = function(t) {
            var n = e(this).parent().parent();
            n.data("colorpicker").fields.parent().removeClass("colorpicker_focus")
        }, g = function() {
            r = this.parentNode.className.indexOf("_hex") > 0 ? 70 : 65, e(this).parent().parent().data("colorpicker").fields.parent().removeClass("colorpicker_focus"), e(this).parent().addClass("colorpicker_focus")
        }, y = function(t) {
            var n = e(this).parent().find("input").focus(), r = {el: e(this).parent().addClass("colorpicker_slider"), max: this.parentNode.className.indexOf("_hsb_h") > 0 ? 360 : this.parentNode.className.indexOf("_hsb") > 0 ? 100 : 255, y: t.pageY, field: n, val: parseInt(n.val(), 10), preview: e(this).parent().parent().data("colorpicker").livePreview};
            e(document).bind("mouseup", r, w), e(document).bind("mousemove", r, b)
        }, b = function(e) {
            return e.data.field.val(Math.max(0, Math.min(e.data.max, parseInt(e.data.val + e.pageY - e.data.y, 10)))), e.data.preview && v.apply(e.data.field.get(0), [!0]), !1
        }, w = function(t) {
            return v.apply(t.data.field.get(0), [!0]), t.data.el.removeClass("colorpicker_slider").find("input").focus(), e(document).unbind("mouseup", w), e(document).unbind("mousemove", b), !1
        }, E = function(t) {
            var n = {cal: e(this).parent(), y: e(this).offset().top};
            n.preview = n.cal.data("colorpicker").livePreview, e(document).bind("mouseup", n, x), e(document).bind("mousemove", n, S)
        }, S = function(e) {
            return v.apply(e.data.cal.data("colorpicker").fields.eq(4).val(parseInt(360 * (150 - Math.max(0, Math.min(150, e.pageY - e.data.y))) / 150, 10)).get(0), [e.data.preview]), !1
        }, x = function(t) {
            return u(t.data.cal.data("colorpicker").color, t.data.cal.get(0)), f(t.data.cal.data("colorpicker").color, t.data.cal.get(0)), e(document).unbind("mouseup", x), e(document).unbind("mousemove", S), !1
        }, T = function(t) {
            var n = {cal: e(this).parent(), pos: e(this).offset()};
            n.preview = n.cal.data("colorpicker").livePreview, e(document).bind("mouseup", n, C), e(document).bind("mousemove", n, N)
        }, N = function(e) {
            return v.apply(e.data.cal.data("colorpicker").fields.eq(6).val(parseInt(100 * (150 - Math.max(0, Math.min(150, e.pageY - e.data.pos.top))) / 150, 10)).end().eq(5).val(parseInt(100 * Math.max(0, Math.min(150, e.pageX - e.data.pos.left)) / 150, 10)).get(0), [e.data.preview]), !1
        }, C = function(t) {
            return u(t.data.cal.data("colorpicker").color, t.data.cal.get(0)), f(t.data.cal.data("colorpicker").color, t.data.cal.get(0)), e(document).unbind("mouseup", C), e(document).unbind("mousemove", N), !1
        }, k = function(t) {
            e(this).addClass("colorpicker_focus")
        }, L = function(t) {
            e(this).removeClass("colorpicker_focus")
        }, A = function(t) {
            var n = e(this).parent(), r = n.data("colorpicker").color;
            n.data("colorpicker").origColor = r, h(r, n.get(0)), n.data("colorpicker").onSubmit(r, U(r), q(r), n.data("colorpicker").el)
        }, O = function(t) {
            var n = e("#" + e(this).data("colorpickerId"));
            n.data("colorpicker").onBeforeShow.apply(this, [n.get(0)]);
            var r = e(this).offset(), i = D(), s = r.top + this.offsetHeight, o = r.left;
            return s + 176 > i.t + i.h && (s -= this.offsetHeight + 176), o + 356 > i.l + i.w && (o -= 356), n.css({left: o + "px", top: s + "px"}), n.data("colorpicker").onShow.apply(this, [n.get(0)]) != 0 && n.show(), e(document).bind("mousedown", {cal: n}, M), !1
        }, M = function(t) {
            _(t.data.cal.get(0), t.target, t.data.cal.get(0)) || (t.data.cal.data("colorpicker").onHide.apply(this, [t.data.cal.get(0)]) != 0 && t.data.cal.hide(), e(document).unbind("mousedown", M))
        }, _ = function(e, t, n) {
            if (e == t)
                return!0;
            if (e.contains)
                return e.contains(t);
            if (e.compareDocumentPosition)
                return!!(e.compareDocumentPosition(t) & 16);
            var r = t.parentNode;
            while (r && r != n) {
                if (r == e)
                    return!0;
                r = r.parentNode
            }
            return!1
        }, D = function() {
            var e = document.compatMode == "CSS1Compat";
            return{l: window.pageXOffset || (e ? document.documentElement.scrollLeft : document.body.scrollLeft), t: window.pageYOffset || (e ? document.documentElement.scrollTop : document.body.scrollTop), w: window.innerWidth || (e ? document.documentElement.clientWidth : document.body.clientWidth), h: window.innerHeight || (e ? document.documentElement.clientHeight : document.body.clientHeight)}
        }, P = function(e) {
            return{h: Math.min(360, Math.max(0, e.h)), s: Math.min(100, Math.max(0, e.s)), b: Math.min(100, Math.max(0, e.b))}
        }, H = function(e) {
            return{r: Math.min(255, Math.max(0, e.r)), g: Math.min(255, Math.max(0, e.g)), b: Math.min(255, Math.max(0, e.b))}
        }, B = function(e) {
            var t = 6 - e.length;
            if (t > 0) {
                var n = [];
                for (var r = 0; r < t; r++)
                    n.push("0");
                n.push(e), e = n.join("")
            }
            return e
        }, j = function(e) {
            var e = parseInt(e.indexOf("#") > -1 ? e.substring(1) : e, 16);
            return{r: e >> 16, g: (e & 65280) >> 8, b: e & 255}
        }, F = function(e) {
            return I(j(e))
        }, I = function(e) {
            var t = {h: 0, s: 0, b: 0}, n = Math.min(e.r, e.g, e.b), r = Math.max(e.r, e.g, e.b), i = r - n;
            return t.b = r, r != 0, t.s = r != 0 ? 255 * i / r : 0, t.s != 0 ? e.r == r ? t.h = (e.g - e.b) / i : e.g == r ? t.h = 2 + (e.b - e.r) / i : t.h = 4 + (e.r - e.g) / i : t.h = -1, t.h *= 60, t.h < 0 && (t.h += 360), t.s *= 100 / 255, t.b *= 100 / 255, t
        }, q = function(e) {
            var t = {}, n = Math.round(e.h), r = Math.round(e.s * 255 / 100), i = Math.round(e.b * 255 / 100);
            if (r == 0)
                t.r = t.g = t.b = i;
            else {
                var s = i, o = (255 - r) * i / 255, u = (s - o) * (n % 60) / 60;
                n == 360 && (n = 0), n < 60 ? (t.r = s, t.b = o, t.g = o + u) : n < 120 ? (t.g = s, t.b = o, t.r = s - u) : n < 180 ? (t.g = s, t.r = o, t.b = o + u) : n < 240 ? (t.b = s, t.r = o, t.g = s - u) : n < 300 ? (t.b = s, t.g = o, t.r = o + u) : n < 360 ? (t.r = s, t.g = o, t.b = s - u) : (t.r = 0, t.g = 0, t.b = 0)
            }
            return{r: Math.round(t.r), g: Math.round(t.g), b: Math.round(t.b)}
        }, R = function(t) {
            var n = [t.r.toString(16), t.g.toString(16), t.b.toString(16)];
            return e.each(n, function(e, t) {
                t.length == 1 && (n[e] = "0" + t)
            }), n.join("")
        }, U = function(e) {
            return R(q(e))
        }, z =
                function() {
                    var t = e(this).parent(), n = t.data("colorpicker").origColor;
                    t.data("colorpicker").color = n, u(n, t.get(0)), f(n, t.get(0)), a(n, t.get(0)), l(n, t.get(0)), c(n, t.get(0)), p(n, t.get(0))
                };
        return{init: function(t) {
                t = e.extend({}, o, t || {});
                if (typeof t.color == "string")
                    t.color = F(t.color);
                else if (t.color.r != undefined && t.color.g != undefined && t.color.b != undefined)
                    t.color = I(t.color);
                else {
                    if (t.color.h == undefined || t.color.s == undefined || t.color.b == undefined)
                        return this;
                    t.color = P(t.color)
                }
                return this.each(function() {
                    if (!e(this).data("colorpickerId")) {
                        var n = e.extend({}, t);
                        n.origColor = t.color;
                        var r = "collorpicker_" + parseInt(Math.random() * 1e3);
                        e(this).data("colorpickerId", r);
                        var i = e(s).attr("id", r);
                        n.flat ? i.appendTo(this).show() : i.appendTo(document.body), n.fields = i.find("input").bind("keyup", d).bind("change", v).bind("blur", m).bind("focus", g), i.find("span").bind("mousedown", y).end().find(">div.colorpicker_current_color").bind("click", z), n.selector = i.find("div.colorpicker_color").bind("mousedown", T), n.selectorIndic = n.selector.find("div div"), n.el = this, n.hue = i.find("div.colorpicker_hue div"), i.find("div.colorpicker_hue").bind("mousedown", E), n.newColor = i.find("div.colorpicker_new_color"), n.currentColor = i.find("div.colorpicker_current_color"), i.data("colorpicker", n), i.find("div.colorpicker_submit").bind("mouseenter", k).bind("mouseleave", L).bind("click", A), u(n.color, i.get(0)), a(n.color, i.get(0)), f(n.color, i.get(0)), c(n.color, i.get(0)), l(n.color, i.get(0)), h(n.color, i.get(0)), p(n.color, i.get(0)), n.flat ? i.css({position: "relative", display: "block"}) : e(this).bind(n.eventName, O)
                    }
                })
            }, showPicker: function() {
                return this.each(function() {
                    e(this).data("colorpickerId") && O.apply(this)
                })
            }, hidePicker: function() {
                return this.each(function() {
                    e(this).data("colorpickerId") && e("#" + e(this).data("colorpickerId")).hide()
                })
            }, setColor: function(t) {
                if (typeof t == "string")
                    t = F(t);
                else if (t.r != undefined && t.g != undefined && t.b != undefined)
                    t = I(t);
                else {
                    if (t.h == undefined || t.s == undefined || t.b == undefined)
                        return this;
                    t = P(t)
                }
                return this.each(function() {
                    if (e(this).data("colorpickerId")) {
                        var n = e("#" + e(this).data("colorpickerId"));
                        n.data("colorpicker").color = t, n.data("colorpicker").origColor = t, u(t, n.get(0)), a(t, n.get(0)), f(t, n.get(0)), c(t, n.get(0)), l(t, n.get(0)), h(t, n.get(0)), p(t, n.get(0))
                    }
                })
            }}
    }();
    e.fn.extend({ColorPicker: t.init, ColorPickerHide: t.hidePicker, ColorPickerShow: t.showPicker, ColorPickerSetColor: t.setColor})
}(jQuery), function(e) {
    function n(n, r) {
        if (!this.isSupported)
            throw"Older browesers are not supported.";
        this.$element = e(n);
        var i = this.options = e.extend({}, e.fn.imageLoader.defaults, r);
        this.$placeholder = e(i.show), this.$element.on("change." + t, e.proxy(this._onChange, this))
    }
    var t = "imageloader";
    n.prototype = {$: function(e) {
            return this.$element.find(e)
        }, _onChange: function(e) {
            this._reader(e.currentTarget)
        }, _reader: function(t) {
            var n = new FileReader, r = this, i = this.options;
            n.onload = function(t) {
                return e.proxy(r[i.cssBackground ? "_setBackground" : "_setImage"], r)
            }(t.files[0]), n.readAsDataURL(t.files[0]), e.isFunction(i.callback) && i.callback()
        }, _setImage: function(e) {
            var t = this.options;
            return this.$placeholder.html('<img src="' + e.target.result + '" class="' + t.imgClass + '" />')
        }, _setBackground: function(e) {
            return this.$placeholder.css({backgroundImage: "url(" + e.target.result + ")"})
        }, isSupported: function() {
            return window.File && window.FileReader && window.FileList && window.Blob
        }()}, e.fn.imageLoader = function(r) {
        return this.each(function() {
            var i = e(this), s = i.data(t), o = typeof r == "object" && r;
            s || i.data(t, s = new n(this, o)), typeof r == "string" && r.charAt(0) !== "_" && s[r]()
        })
    }, e.fn.imageLoader.Constructor = n, e.fn.imageLoader.defaults = {show: ".show", width: "auto", height: "auto", imgClass: "img-load", cssBackground: !1, callback: null}
}(jQuery), function(e, t, n) {
    function G(n, r, i) {
        var o = t.createElement(n);
        return r && (o.id = s + r), i && (o.style.cssText = i), e(o)
    }
    function Y(e) {
        var t = T.length, n = (U + e) % t;
        return n < 0 ? t + n : n
    }
    function Z(e, t) {
        return Math.round((/%/.test(e) ? (t === "x" ? tt() : nt()) / 100 : 1) * parseInt(e, 10))
    }
    function et(e) {
        return B.photo || /\.(gif|png|jp(e|g|eg)|bmp|ico)((#|\?).*)?$/i.test(e)
    }
    function tt() {
        return n.innerWidth || N.width()
    }
    function nt() {
        return n.innerHeight || N.height()
    }
    function rt() {
        var t, n = e.data(R, i);
        n == null ? (B = e.extend({}, r), console && console.log && console.log("Error: cboxElement missing settings object")) : B = e.extend({}, n);
        for (t in B)
            e.isFunction(B[t]) && t.slice(0, 2) !== "on" && (B[t] = B[t].call(R));
        B.rel = B.rel || R.rel || e(R).data("rel") || "nofollow", B.href = B.href || e(R).attr("href"), B.title = B.title || R.title, typeof B.href == "string" && (B.href = e.trim(B.href))
    }
    function it(t, n) {
        e.event.trigger(t), n && n.call(R)
    }
    function st() {
        var e, t = s + "Slideshow_", n = "click." + s, r, i, o;
        B.slideshow && T[1] ? (r = function() {
            M.html(B.slideshowStop).unbind(n).bind(f, function() {
                if (B.loop || T[U + 1])
                    e = setTimeout(J.next, B.slideshowSpeed)
            }).bind(a, function() {
                clearTimeout(e)
            }).one(n + " " + l, i), g.removeClass(t + "off").addClass(t + "on"), e = setTimeout(J.next, B.slideshowSpeed)
        }, i = function() {
            clearTimeout(e), M.html(B.slideshowStart).unbind([f, a, l, n].join(" ")).one(n, function() {
                J.next(), r()
            }), g.removeClass(t + "on").addClass(t + "off")
        }, B.slideshowAuto ? r() : i()) : g.removeClass(t + "off " + t + "on")
    }
    function ot(t) {
        V || (R = t, rt(), T = e(R), U = 0, B.rel !== "nofollow" && (T = e("." + o).filter(function() {
            var t = e.data(this, i), n;
            return t && (n = e(this).data("rel") || t.rel || this.rel), n === B.rel
        }), U = T.index(R), U === -1 && (T = T.add(R), U = T.length - 1)), W || (W = X = !0, g.show(), B.returnFocus && e(R).blur().one(c, function() {
            e(this).focus()
        }), m.css({opacity: +B.opacity, cursor: B.overlayClose ? "pointer" : "auto"}).show(), B.w = Z(B.initialWidth, "x"), B.h = Z(B.initialHeight, "y"), J.position(), d && N.bind("resize." + v + " scroll." + v, function() {
            m.css({width: tt(), height: nt(), top: N.scrollTop(), left: N.scrollLeft()})
        }).trigger("resize." + v), it(u, B.onOpen), H.add(A).hide(), P.html(B.close).show()), J.load(!0))
    }
    function ut() {
        !g && t.body && (Q = !1, N = e(n), g = G(K).attr({id: i, "class": p ? s + (d ? "IE6" : "IE") : ""}).hide(), m = G(K, "Overlay", d ? "position:absolute" : "").hide(), L = G(K, "LoadingOverlay").add(G(K, "LoadingGraphic")), y = G(K, "Wrapper"), b = G(K, "Content").append(C = G(K, "LoadedContent", "width:0; height:0; overflow:hidden"), A = G(K, "Title"), O = G(K, "Current"), _ = G(K, "Next"), D = G(K, "Previous"), M = G(K, "Slideshow").bind(u, st), P = G(K, "Close")), y.append(G(K).append(G(K, "TopLeft"), w = G(K, "TopCenter"), G(K, "TopRight")), G(K, !1, "clear:left").append(E = G(K, "MiddleLeft"), b, S = G(K, "MiddleRight")), G(K, !1, "clear:left").append(G(K, "BottomLeft"), x = G(K, "BottomCenter"), G(K, "BottomRight"))).find("div div").css({"float": "left"}), k = G(K, !1, "position:absolute; width:9999px; visibility:hidden; display:none"), H = _.add(D).add(O).add(M), e(t.body).append(m, g.append(y, k)))
    }
    function at() {
        return g ? (Q || (Q = !0, j = w.height() + x.height() + b.outerHeight(!0) - b.height(), F = E.width() + S.width() + b.outerWidth(!0) - b.width(), I = C.outerHeight(!0), q = C.outerWidth(!0), g.css({"padding-bottom": j, "padding-right": F}), _.click(function() {
            J.next()
        }), D.click(function() {
            J.prev()
        }), P.click(function() {
            J.close()
        }), m.click(function() {
            B.overlayClose && J.close()
        }), e(t).bind("keydown." + s, function(e) {
            var t = e.keyCode;
            W && B.escKey && t === 27 && (e.preventDefault(), J.close()), W && B.arrowKey && T[1] && (t === 37 ? (e.preventDefault(), D.click()) : t === 39 && (e.preventDefault(), _.click()))
        }), e("." + o, t).live("click", function(e) {
            e.which > 1 || e.shiftKey || e.altKey || e.metaKey || (e.preventDefault(), ot(this))
        })), !0) : !1
    }
    var r = {transition: "elastic", speed: 300, width: !1, initialWidth: "600", innerWidth: !1, maxWidth: !1, height: !1, initialHeight: "450", innerHeight: !1, maxHeight: !1, scalePhotos: !0, scrolling: !0, inline: !1, html: !1, iframe: !1, fastIframe: !0, photo: !1, href: !1, title: !1, rel: !1, opacity: .9, preloading: !0, current: "image {current} of {total}", previous: "previous", next: "next", close: "close", xhrError: "This content failed to load.", imgError: "This image failed to load.", open: !1, returnFocus: !0, reposition: !0, loop: !0, slideshow: !1, slideshowAuto: !0, slideshowSpeed: 2500, slideshowStart: "start slideshow", slideshowStop: "stop slideshow", onOpen: !1, onLoad: !1, onComplete: !1, onCleanup: !1, onClosed: !1, overlayClose: !0, escKey: !0, arrowKey: !0, top: !1, bottom: !1, left: !1, right: !1, fixed: !1, data: undefined}, i = "colorbox", s = "cbox", o = s + "Element", u = s + "_open", a = s + "_load", f = s + "_complete", l = s + "_cleanup", c = s + "_closed", h = s + "_purge", p = !e.support.opacity && !e.support.style, d = p && !n.XMLHttpRequest, v = s + "_IE6", m, g, y, b, w, E, S, x, T, N, C, k, L, A, O, M, _, D, P, H, B, j, F, I, q, R, U, z, W, X, V, $, J, K = "div", Q;
    if (e.colorbox)
        return;
    e(ut), J = e.fn[i] = e[i] = function(t, n) {
        var s = this;
        t = t || {}, ut();
        if (at()) {
            if (!s[0]) {
                if (s.selector)
                    return s;
                s = e("<a/>"), t.open = !0
            }
            n && (t.onComplete = n), s.each(function() {
                e.data(this, i, e.extend({}, e.data(this, i) || r, t))
            }).addClass(o), (e.isFunction(t.open) && t.open.call(s) || t.open) && ot(s[0])
        }
        return s
    }, J.position = function(e, t) {
        function f(e) {
            w[0].style.width = x[0].style.width = b[0].style.width = e.style.width, b[0].style.height = E[0].style.height = S[0].style.height = e.style.height
        }
        var n, r = 0, i = 0, o = g.offset(), u, a;
        N.unbind("resize." + s), g.css({top: -9e4, left: -9e4}), u = N.scrollTop(), a = N.scrollLeft(), B.fixed && !d ? (o.top -= u, o.left -= a, g.css({position: "fixed"})) : (r = u, i = a, g.css({position: "absolute"})), B.right !== !1 ? i += Math.max(tt() - B.w - q - F - Z(B.right, "x"), 0) : B.left !== !1 ? i += Z(B.left, "x") : i += Math.round(Math.max(tt() - B.w - q - F, 0) / 2), B.bottom !== !1 ? r += Math.max(nt() - B.h - I - j - Z(B.bottom, "y"), 0) : B.top !== !1 ? r += Z(B.top, "y") : r += Math.round(Math.max(nt() - B.h - I - j, 0) / 2), g.css({top: o.top, left: o.left}), e = g.width() === B.w + q && g.height() === B.h + I ? 0 : e || 0, y[0].style.width = y[0].style.height = "9999px", n = {width: B.w + q, height: B.h + I, top: r, left: i}, e === 0 && g.css(n), g.dequeue().animate(n, {duration: e, complete: function() {
                f(this), X = !1, y[0].style.width = B.w + q + F + "px", y[0].style.height = B.h + I + j + "px", B.reposition && setTimeout(function() {
                    N.bind("resize." + s, J.position)
                }, 1), t && t()
            }, step: function() {
                f(this)
            }})
    }, J.resize = function(e) {
        W && (e = e || {}, e.width && (B.w = Z(e.width, "x") - q - F), e.innerWidth && (B.w = Z(e.innerWidth, "x")), C.css({width: B.w}), e.height && (B.h = Z(e.height, "y") - I - j), e.innerHeight && (B.h = Z(e.innerHeight, "y")), !e.innerHeight && !e.height && (C.css({height: "auto"}), B.h = C.height()), C.css({height: B.h}), J.position(B.transition === "none" ? 0 : B.speed))
    }, J.prep = function(t) {
        function o() {
            return B.w = B.w || C.width(), B.w = B.mw && B.mw < B.w ? B.mw : B.w, B.w
        }
        function u() {
            return B.h = B.h || C.height(), B.h = B.mh && B.mh < B.h ? B.mh : B.h, B.h
        }
        if (!W)
            return;
        var n, r = B.transition === "none" ? 0 : B.speed;
        C.remove(), C = G(K, "LoadedContent").append(t), C.hide().appendTo(k.show()).css({width: o(), overflow: B.scrolling ? "auto" : "hidden"}).css({height: u()}).prependTo(b), k.hide(), e(z).css({"float": "none"}), d && e("select").not(g.find("select")).filter(function() {
            return this.style.visibility !== "hidden"
        }).css({visibility: "hidden"}).one(l, function() {
            this.style.visibility = "inherit"
        }), n = function() {
            function y() {
                p && g[0].style.removeAttribute("filter")
            }
            var t, n, o = T.length, u, a = "frameBorder", l = "allowTransparency", c, d, v, m;
            if (!W)
                return;
            c = function() {
                clearTimeout($), L.detach().hide(), it(f, B.onComplete)
            }, p && z && C.fadeIn(100), A.html(B.title).add(C).show();
            if (o > 1) {
                typeof B.current == "string" && O.html(B.current.replace("{current}", U + 1).replace("{total}", o)).show(), _[B.loop || U < o - 1 ? "show" : "hide"]().html(B.next), D[B.loop || U ? "show" : "hide"]().html(B.previous), B.slideshow && M.show();
                if (B.preloading) {
                    t = [Y(-1), Y(1)];
                    while (n = T[t.pop()])
                        m = e.data(n, i), m && m.href ? (d = m.href, e.isFunction(d) && (d = d.call(n))) : d = n.href, et(d) && (v = new Image, v.src = d)
                }
            } else
                H.hide();
            B.iframe ? (u = G("iframe")[0], a in u && (u[a] = 0), l in u && (u[l] = "true"), B.scrolling || (u.scrolling = "no"), e(u).attr({src: B.href, name: (new Date).getTime(), "class": s + "Iframe", allowFullScreen: !0, webkitAllowFullScreen: !0, mozallowfullscreen: !0}).one("load", c).one(h, function() {
                u.src = "//about:blank"
            }).appendTo(C), B.fastIframe && e(u).trigger("load")) : c(), B.transition === "fade" ? g.fadeTo(r, 1, y) : y()
        }, B.transition === "fade" ? g.fadeTo(r, 0, function() {
            J.position(0, n)
        }) : J.position(r, n)
    }, J.load = function(t) {
        var n, r, i = J.prep;
        X = !0, z = !1, R = T[U], t || rt(), it(h), it(a, B.onLoad), B.h = B.height ? Z(B.height, "y") - I - j : B.innerHeight && Z(B.innerHeight, "y"), B.w = B.width ? Z(B.width, "x") - q - F : B.innerWidth && Z(B.innerWidth, "x"), B.mw = B.w, B.mh = B.h, B.maxWidth && (B.mw = Z(B.maxWidth, "x") - q - F, B.mw = B.w && B.w < B.mw ? B.w : B.mw), B.maxHeight && (B.mh = Z(B.maxHeight, "y") - I - j, B.mh = B.h && B.h < B.mh ? B.h : B.mh), n = B.href, $ = setTimeout(function() {
            L.show().appendTo(b)
        }, 100), B.inline ? (G(K).hide().insertBefore(e(n)[0]).one(h, function() {
            e(this).replaceWith(C.children())
        }), i(e(n))) : B.iframe ? i(" ") : B.html ? i(B.html) : et(n) ? (e(z = new Image).addClass(s + "Photo").error(function() {
            B.title = !1, i(G(K, "Error").html(B.imgError))
        }).load(function() {
            var e;
            z.onload = null, B.scalePhotos && (r = function() {
                z.height -= z.height * e, z.width -= z.width * e
            }, B.mw && z.width > B.mw && (e = (z.width - B.mw) / z.width, r()), B.mh && z.height > B.mh && (e = (z.height - B.mh) / z.height, r())), B.h && (z.style.marginTop = Math.max(B.h - z.height, 0) / 2 + "px"), T[1] && (B.loop || T[U + 1]) && (z.style.cursor = "pointer", z.onclick = function() {
                J.next()
            }), p && (z.style.msInterpolationMode = "bicubic"), setTimeout(function() {
                i(z)
            }, 1)
        }), setTimeout(function() {
            z.src = n
        }, 1)) : n && k.load(n, B.data, function(t, n, r) {
            i(n === "error" ? G(K, "Error").html(B.xhrError) : e(this).contents())
        })
    }, J.next = function() {
        !X && T[1] && (B.loop || T[U + 1]) && (U = Y(1), J.load())
    }, J.prev = function() {
        !X && T[1] && (B.loop || U) && (U = Y(-1), J.load())
    }, J.close = function() {
        W && !V && (V = !0, W = !1, it(l, B.onCleanup), N.unbind("." + s + " ." + v), m.fadeTo(200, 0), g.stop().fadeTo(300, 0, function() {
            g.add(m).css({opacity: 1, cursor: "auto"}).hide(), it(h), C.remove(), setTimeout(function() {
                V = !1, it(c, B.onClosed)
            }, 1)
        }))
    }, J.remove = function() {
        e([]).add(g).add(m).remove(), g = null, e("." + o).removeData(i).removeClass(o).die()
    }, J.element = function() {
        return e(R)
    }, J.settings = r
}(jQuery, document, window), function() {
    function e(e, t) {
        return[].slice.call((t || document).querySelectorAll(e))
    }
    if (!window.addEventListener)
        return;
    var t = window.StyleFix = {link: function(e) {
            try {
                if (e.rel !== "stylesheet" || e.hasAttribute("data-noprefix"))
                    return
            } catch (n) {
                return
            }
            var r = e.href || e.getAttribute("data-href"), i = r.replace(/[^\/]+$/, ""), s = e.parentNode, o = new XMLHttpRequest, u;
            o.onreadystatechange = function() {
                o.readyState === 4 && u()
            }, u = function() {
                var n = o.responseText;
                if (n && e.parentNode && (!o.status || o.status < 400 || o.status > 600)) {
                    n = t.fix(n, !0, e);
                    if (i) {
                        n = n.replace(/url\(\s*?((?:"|')?)(.+?)\1\s*?\)/gi, function(e, t, n) {
                            return/^([a-z]{3,10}:|\/|#)/i.test(n) ? e : 'url("' + i + n + '")'
                        });
                        var r = i.replace(/([\\\^\$*+[\]?{}.=!:(|)])/g, "\\$1");
                        n = n.replace(RegExp("\\b(behavior:\\s*?url\\('?\"?)" + r, "gi"), "$1")
                    }
                    var u = document.createElement("style");
                    u.textContent = n, u.media = e.media, u.disabled = e.disabled, u.setAttribute("data-href", e.getAttribute("href")), s.insertBefore(u, e), s.removeChild(e), u.media = e.media
                }
            };
            try {
                o.open("GET", r), o.send(null)
            } catch (n) {
                typeof XDomainRequest != "undefined" && (o = new XDomainRequest, o.onerror = o.onprogress = function() {
                }, o.onload = u, o.open("GET", r), o.send(null))
            }
            e.setAttribute("data-inprogress", "")
        }, styleElement: function(e) {
            if (e.hasAttribute("data-noprefix"))
                return;
            var n = e.disabled;
            e.textContent = t.fix(e.textContent, !0, e), e.disabled = n
        }, styleAttribute: function(e) {
            var n = e.getAttribute("style");
            n = t.fix(n, !1, e), e.setAttribute("style", n)
        }, process: function() {
            e('link[rel="stylesheet"]:not([data-inprogress])').forEach(StyleFix.link), e("style").forEach(StyleFix.styleElement), e("[style]").forEach(StyleFix.styleAttribute)
        }, register: function(e, n) {
            (t.fixers = t.fixers || []).splice(n === undefined ? t.fixers.length : n, 0, e)
        }, fix: function(e, n, r) {
            for (var i = 0; i < t.fixers.length; i++)
                e = t.fixers[i](e, n, r) || e;
            return e
        }, camelCase: function(e) {
            return e.replace(/-([a-z])/g, function(e, t) {
                return t.toUpperCase()
            }).replace("-", "")
        }, deCamelCase: function(e) {
            return e.replace(/[A-Z]/g, function(e) {
                return"-" + e.toLowerCase()
            })
        }};
    (function() {
        setTimeout(function() {
            e('link[rel="stylesheet"]').forEach(StyleFix.link)
        }, 10), document.addEventListener("DOMContentLoaded", StyleFix.process, !1)
    })()
}(), function(e) {
    function t(e, t, r, i, s) {
        e = n[e];
        if (e.length) {
            var o = RegExp(t + "(" + e.join("|") + ")" + r, "gi");
            s = s.replace(o, i)
        }
        return s
    }
    if (!window.StyleFix || !window.getComputedStyle)
        return;
    var n = window.PrefixFree = {prefixCSS: function(e, r, i) {
            var s = n.prefix;
            n.functions.indexOf("linear-gradient") > -1 && (e = e.replace(/(\s|:|,)(repeating-)?linear-gradient\(\s*(-?\d*\.?\d*)deg/ig, function(e, t, n, r) {
                return r = Math.abs(r - 450) % 360, t + (n || "") + "linear-gradient(" + r + "deg"
            })), e = t("functions", "(\\s|:|,)", "\\s*\\(", "$1" + s + "$2(", e), e = t("keywords", "(\\s|:)", "(\\s|;|\\}|$)", "$1" + s + "$2$3", e), e = t("properties", "(^|\\{|\\s|;)", "\\s*:", "$1" + s + "$2:", e);
            if (n.properties.length) {
                var o = RegExp("\\b(" + n.properties.join("|") + ")(?!:)", "gi");
                e = t("valueProperties", "\\b", ":(.+?);", function(e) {
                    return e.replace(o, s + "$1")
                }, e)
            }
            return r && (e = t("selectors", "", "\\b", n.prefixSelector, e), e = t("atrules", "@", "\\b", "@" + s + "$1", e)), e = e.replace(RegExp("-" + s, "g"), "-"), e = e.replace(/-\*-(?=[a-z]+)/gi, n.prefix), e
        }, property: function(e) {
            return(n.properties.indexOf(e) ? n.prefix : "") + e
        }, value: function(e, r) {
            return e = t("functions", "(^|\\s|,)", "\\s*\\(", "$1" + n.prefix + "$2(", e), e = t("keywords", "(^|\\s)", "(\\s|$)", "$1" + n.prefix + "$2$3", e), e
        }, prefixSelector: function(e) {
            return e.replace(/^:{1,2}/, function(e) {
                return e + n.prefix
            })
        }, prefixProperty: function(e, t) {
            var r = n.prefix + e;
            return t ? StyleFix.camelCase(r) : r
        }};
    (function() {
        var e = {}, t = [], r = {}, i = getComputedStyle(document.documentElement, null), s = document.createElement("div").style, o = function(n) {
            if (n.charAt(0) === "-") {
                t.push(n);
                var r = n.split("-"), i = r[1];
                e[i] = ++e[i] || 1;
                while (r.length > 3) {
                    r.pop();
                    var s = r.join("-");
                    u(s) && t.indexOf(s) === -1 && t.push(s)
                }
            }
        }, u = function(e) {
            return StyleFix.camelCase(e)in s
        };
        if (i.length > 0)
            for (var a = 0; a < i.length; a++)
                o(i[a]);
        else
            for (var f in i)
                o(StyleFix.deCamelCase(f));
        var l = {uses: 0};
        for (var c in e) {
            var h = e[c];
            l.uses < h && (l = {prefix: c, uses: h})
        }
        n.prefix = "-" + l.prefix + "-", n.Prefix = StyleFix.camelCase(n.prefix), n.properties = [];
        for (var a = 0; a < t.length; a++) {
            var f = t[a];
            if (f.indexOf(n.prefix) === 0) {
                var p = f.slice(n.prefix.length);
                u(p) || n.properties.push(p)
            }
        }
        n.Prefix == "Ms" && !("transform"in s) && !("MsTransform"in s) && "msTransform"in s && n.properties.push("transform", "transform-origin"), n.properties.sort()
    })(), function() {
        function e(e, t) {
            return i[t] = "", i[t] = e, !!i[t]
        }
        var t = {"linear-gradient": {property: "backgroundImage", params: "red, teal"}, calc: {property: "width", params: "1px + 5%"}, element: {property: "backgroundImage", params: "#foo"}, "cross-fade": {property: "backgroundImage", params: "url(a.png), url(b.png), 50%"}};
        t["repeating-linear-gradient"] = t["repeating-radial-gradient"] = t["radial-gradient"] = t["linear-gradient"];
        var r = {initial: "color", "zoom-in": "cursor", "zoom-out": "cursor", box: "display", flexbox: "display", "inline-flexbox": "display", flex: "display", "inline-flex": "display"};
        n.functions = [], n.keywords = [];
        var i = document.createElement("div").style;
        for (var s in t) {
            var o = t[s], u = o.property, a = s + "(" + o.params + ")";
            !e(a, u) && e(n.prefix + a, u) && n.functions.push(s)
        }
        for (var f in r) {
            var u = r[f];
            !e(f, u) && e(n.prefix + f, u) && n.keywords.push(f)
        }
    }(), function() {
        function t(e) {
            return s.textContent = e + "{}", !!s.sheet.cssRules.length
        }
        var r = {":read-only": null, ":read-write": null, ":any-link": null, "::selection": null}, i = {keyframes: "name", viewport: null, document: 'regexp(".")'};
        n.selectors = [], n.atrules = [];
        var s = e.appendChild(document.createElement("style"));
        for (var o in r) {
            var u = o + (r[o] ? "(" + r[o] + ")" : "");
            !t(u) && t(n.prefixSelector(u)) && n.selectors.push(o)
        }
        for (var a in i) {
            var u = a + " " + (i[a] || "");
            !t("@" + u) && t("@" + n.prefix + u) && n.atrules.push(a)
        }
        e.removeChild(s)
    }(), n.valueProperties = ["transition", "transition-property"], e.className += " " + n.prefix, StyleFix.register(n.prefixCSS)
}(document.documentElement), $(function(e) {
    e.fn.pixels = function(e) {
        return parseInt(this.css(e).slice(0, -2))
    }, e.sum = function(t) {
        var n = 0;
        return e.each(t, function(e, t) {
            n += t
        }), n
    };
    var t = {popoverTimer: null, init: function() {
            this.appendix(e(".appendix")), this.commentMore(), this.popover(), this.placeholder(), this.checkbox(e(".checkbox")), this.radio(e(".radio")), this.autoadd(), this.checkAll(), this.addClone(), this.close(), this.removeParentParent(), this.carousel(), this.synchro(), this.contentAside(), this.invitationBox(), this.editors(), e('[rel="tooltip"]').tooltip(), e(".widget-scrollable").tinyscrollbar();
            var t = e(".widget-scrollable.horizontal .overview"), n = 0;
            e.each(t, function() {
                var t = e.map(e(this).children(), function(t) {
                    return e(t).outerWidth() + e(t).pixels("margin-left") + e(t).pixels("margin-right")
                });
                e(this).width(e.sum(t))
            }), e(".widget-scrollable.horizontal").tinyscrollbar({axis: "x"})
        }, upload: function() {
            e(".upload-toggle").on("click", function(t) {
                t.preventDefault(), e(this).next().trigger("click")
            }), e(".upload-toggle").next().imageLoader({show: ".upload-preview", width: "150px"})
        }, colorPicker: function() {
            var t = function(t) {
                return"#" + e.map(t.match(/\b(\d+)\b/g), function(e) {
                    return("0" + parseInt(e).toString(16)).slice(-2)
                }).join("")
            };
            e(".color-preview").each(function() {
                var n = e(this);
                n.ColorPicker({color: t(n.css("backgroundColor")), onChange: function(e, t) {
                        n.val(t).css("background-color", "#" + t)
                    }, onSubmit: function(e, t, r, i) {
                        n.val(t).css("background-color", "#" + t).ColorPickerHide()
                    }, onBeforeShow: function() {
                        e(this).ColorPickerSetColor(this.value)
                    }})
            })
        }, appendix: function(t) {
            t.each(function() {
                var t = e(this), n = t.next(".appendix-content"), r = null, i, s, o;
                t.on("click", function(u) {
                    i = t.position(), s = t.offsetParent().width(), o = i.left < s / 2, o && n.addClass("left-aligned"), r == null && (r = n.hasClass("down") ? {top: i.top + 40, left: i.left - (o ? 0 : n.outerWidth() + 40) - t.width() / 2} : r = n.hasClass("card") ? {top: i.top - n.outerHeight() - 20, left: i.left - (o ? 0 : n.outerWidth() + 40) - t.width() / 2} : {top: i.top - n.outerHeight() - 20, left: i.left - n.outerWidth() / 2 + 10}, n.offset(r)), u.preventDefault(), e(".appendix-content").not(n).css("display", "none"), n.stop().fadeToggle("fast")
                })
            })
        }, commentMore: function() {
            e(".comment-more-trigger").live("click", function(t) {
                t.preventDefault();
                var n = e(this), r = e(this).parents(".widget-scrollable");
                n.prev().fadeToggle(function() {
                    n.html(e(this).is(":visible") ? "&hellip; less" : "&hellip; more"), r.length > 0 && r.tinyscrollbar_update()
                })
            })
        }, popover: function() {
            e('[rel="image-uploader"]').each(function() {
                var t = e(this), n = t.prev(), r = t.offset();
                r.left += t.width() + 10, console.log(r.left), r.left = 120.5, r.top = 12, n.offset(r), t.hover(function() {
                    n.show()
                }), n.find(".cancel").on("click", function(e) {
                    e.preventDefault(), n.hide()
                })
            })
        }, placeholder: function() {
            "placeholder"in document.createElement("input") || (e("[placeholder]").focus(function() {
                var t = e(this);
                t.val() == t.attr("placeholder") && (t.val(""), t.removeClass("placeholder"))
            }).blur(function() {
                var t = e(this);
                if (t.val() == "" || t.val() == t.attr("placeholder"))
                    t.addClass("placeholder"), t.val(t.attr("placeholder"))
            }).blur(), e("[placeholder]").parents("form").submit(function() {
                e(this).find("[placeholder]").each(function() {
                    var t = e(this);
                    t.val() == t.attr("placeholder") && t.val("")
                })
            }))
        }, checkbox: function(t) {
            t.each(function() {
                var t = e(this), n = e("<span/>").addClass("checkbox-substitute");
                t.wrap(n), n = t.parent(), n[t.is(":checked") ? "addClass" : "removeClass"]("checked"), t.on("click change", function() {
                    e(this).parent()[t.is(":checked") ? "addClass" : "removeClass"]("checked")
                })
            })
        }, radio: function(t) {
            t.each(function() {
                var t = e(this), n = e("<span/>").addClass("radio-substitute");
                t.wrap(n), n = t.parent(), n[t.is(":checked") ? "addClass" : "removeClass"]("checked"), t.on("click", function() {
                    e('.radio[name="' + t.attr("name") + '"]').parent().removeClass("checked"), e(this).parent()[t.is(":checked") ? "addClass" : "removeClass"]("checked")
                })
            })
        }, isEmail: function(e) {
            return pattern = new RegExp(/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/i), pattern.test(e)
        }, autoadd: function() {
            e(".autoadd").delegate("input", "blur", function() {
                var t = e(this).attr("type"), n = e(this).val();
                e(this).parent().removeClass("valid").removeClass("invalid").addClass(t == "text" && n.length > 0 ? "valid" : t == "email" && isEmail(n) ? "valid" : "invalid")
            }).delegate("li:last-child input", "focus", function() {
                var t = e(this).closest("li").clone();
                e(this).closest(".autoadd").append(t)
            }).delegate("input", "focus", function() {
                e(this).parent().addClass("active")
            }).delegate(".close", "click", function(t) {
                t.preventDefault(), e(this).closest(".autoadd").children().length > 1 && e(this).closest("li").fadeOut("fast", function() {
                    e(this).remove()
                })
            })
        }, checkAll: function() {
            e(".check-parent-next, .uncheck-parent-next").on("click", function(t) {
                t.preventDefault();
                var n = e(this).parent().next().find('input[type="checkbox"]').removeAttr("checked").parent().removeClass("checked");
                e(this).hasClass("check-parent-next") && n.children().attr("checked", "").parent().addClass("checked")
            }), e(".check-parent-prev, .uncheck-parent-prev").on("click", function(t) {
                t.preventDefault();
                var n = e(this).parent().prev().find('input[type="checkbox"]').removeAttr("checked").parent().removeClass("checked");
                e(this).hasClass("check-parent-prev") && n.children().attr("checked", "").parent().addClass("checked")
            })
        }, addClone: function() {
            e(".clone-parent-prev").each(function() {
                var t = e(this).parent().prev().clone();
                e(this).on("click", function(n) {
                    n.preventDefault(), t.clone().insertBefore(e(this))
                })
            })
        }, close: function() {
            e(".close-parent").on("click", function(t) {
                t.preventDefault(), e(this).parent().fadeOut()
            })
        }, removeParentParent: function() {
            e("body").delegate(".remove-parent-parent", "click", function(t) {
                t.preventDefault(), e(this).parent().parent().fadeOut(function() {
                    e(this).remove()
                })
            })
        }, carousel: function() {
            /*e(".vl").jcarousel(), e(".vl-nav.prev").jcarouselControl({target: "-=7"}), e(".vl-nav.next").jcarouselControl({target: "+=7"}), e(".vl-toggle").on("click", function(t) {
             t.preventDefault();
             var n = e(this), r = e(this).closest(".video-list").children(".vl-wrap");
             r.slideToggle("fast", function() {
             e(this).removeClass("closed"), n[e(this).is(":visible") ? "addClass" : "removeClass"]("up")
             })
             }), e(".jcarousel").jcarousel(), e(".jcarousel-prev").jcarouselControl({target: "-=1"}), e(".jcarousel-next").jcarouselControl({target: "+=1"})*/
        }, synchro: function() {
            e(".synchro").each(function() {
                var t = e(this), n = t.find("li.comment"), r = n.length;
                (r ? n.last() : t).find(".comment-date").addClass("last-reply")
            })
        }, contentAside: function() {
            e(".content-aside").each(function() {
                var t = e(this).closest(".row-fluid").height();
                e(this).css("min-height", t + 30)
            });
            var t = setInterval(function() {
                e(".rel").each(function() {
                    var t = e(this), n = t.height(), r = t.attr("rel");
                    n += t.find(".full-rel").length ? 30 : 0, e("#rel-" + r).height(n)
                })
            }, 10)
        }, substitutions: function() {
            e(".styled-input-group input, .styled-input-group input").each(function() {
                var t = e(this);
                t.next(".substitution").length ? "" : t.after(e("<span/>").addClass("substitution").addClass(t.attr("type")))
            })
        }, invitationBox: function() {
            e(".new-invitation-box").map(function() {
                e(this).height(e(this).prev().outerHeight())
            })
        }, editors: function() {
            if (supress_app_init)
                return;
            /*e.each(e('[id*="editor-"]'), function() {
                var t = e(this), n = t.attr("id"), r = n.split("-")[1], i = "editor" + r + "-toolbar", s = new wysihtml5.Editor(n, {stylesheets: [home_url + "/css/editor.css"], toolbar: i})
            })*/
        }, disscussion: function() {
            var t = e("#disscussion-reply-form");
            e(".disscussion .comment-reply").on("click", function(n) {
                n.preventDefault(), t.hide().insertAfter(e(this).closest(".comment").find("> .comment-footer")).fadeIn(), t.find("#disscussion_comment_ID").val(t.closest(".comment").attr("id").split("-")[1])
            }), e(".js-close-disscussion-reply").on("click", function(t) {
                t.preventDefault(), e(this).parent().parent().fadeOut()
            })
        }}, n = {player: null, duration: null, $player: null, $handle: null, $bar: null, $progress: null, init: function() {
            var t = e(".video-js").first().attr("id");
            this.player = _V_(t), this.$player = e("#" + t).children("video");
            var r = Boolean(e("#vid-container").attr("vidonly"));
            if (this.player.ready) {
                this.player.ready(function() {
                    r != 1 && this.addEvent("loadeddata", function() {
                        n.$handle = e(".vjs-seek-handle"), n.$bar = e(".vjs-progress-control"), n.$progress = e(".vjs-play-progress"), n.duration = this.pause().duration(), n.Markers.init(), n.Comments.init(), n.Tooltip.init(), this.play()
                    })
                })
            }
        }, getOffset: function(e) {
            var t, r = e / n.player.duration(), i = n.$handle;
            isNaN(r) && (r = 0);
            var s = n.$handle.width(), o = s ? s / n.$bar.width() : 0;
            return _V_.round(r * (1 - o) * 100, 2) + "%"
        }, Markers: {data: [], init: function() {
                n.$player.data("markers") && (this.data = n.$player.data("markers"), this.create()), this.create()
            }, create: function() {
                var t = e('<div class="vjs-marker"/>');
                e.each(n.Markers.data, function(e, r) {
                    t.clone().attr("id", "marker-" + e).css("left", n.getOffset(r)).appendTo(n.$bar).on("click", function() {
                        n.player.pause().currentTime(r), window.location = "#add_comment_button", n.Tooltip.show()
                    })
                })
            }}, Tooltip: {$el: null, $timer: null, $link: null, init: function() {
                this.$el = e("<div/>").addClass("vjs-tooltip"), this.$timer = e("<span/>").addClass("vjs-tooltip-timer").appendTo(this.$el), this.$link = e("Add Comment Here".link("#")).addClass("vjs-tooltip-link").appendTo(this.$el), this.$el.appendTo(n.$bar), this.$link.on("click", function(t) {
                    e('#add_comment_button').css('display','none')
                    t.preventDefault(), n.player.pause().cancelFullScreen(), e("#comment_form").show("slow"), e("input:radio[name=for]")[0].checked = !0, n.Comments.focusForm(n.player.id, Math.floor(n.player.currentTime()))
                }), n.player.addEvent("timeupdate", function() {
                    n.Tooltip.changeTime(this.currentTime())
                }), n.$handle.on("hover", function(e) {
                    n.Tooltip[e.type === "mouseenter" ? "show" : "hide"]()
                })
            }, hide: function() {
                var e = setInterval(function() {
                    return clearInterval(e), n.Tooltip.$el.removeClass("hover")
                }, 300)
            }, show: function() {
                return this.$el.addClass("hover")
            }, changeTime: function(e) {
                this.$el.css({left: n.$handle.css("left"), "margin-left": -(this.$el.outerWidth() - n.$handle.width()) / 2}), this.$timer.text(_V_.formatTime(e))
            }}, Comments: {$form: null, $replyForm: null, $reply: null, $textarea: null, init: function() {
                this.$form = e("#comments-form"), this.$replyForm = e("#reply-form"), this.$reply = e("#reply"), this.$textarea = e("#comment"), e('[name="video_ID"]').val(n.player.id), e("#add-comment").on("click", function(e) {
                    e.preventDefault(), n.Comments.showForm()
                }), this.synchro(), n.player.addEvent("timeupdate", function() {
                    n.Comments.updatedRightNow(this.currentTime())
                }), this.closeForm(".js-close-comments-form"), this.closeReplyForm(".js-close-reply-form"), e(".comments").not(".disscussion").find(".comment-reply").on("click", function(t) {
                    t.preventDefault(), n.Comments.$replyForm.insertAfter(e(this).closest(".comment").find("> .comment-footer")).fadeIn()
                })
            }, focusForm: function(t, n) {
                e("#right-now").attr("checked", ""), this.updatedRightNow(n).showForm()
            }, updatedRightNow: function(t) {
                return t = Math.floor(t), e('[name="synchro_time"]').val(t), t = _V_.formatTime(t), e("#right-now-time").text(t), this
            }, synchro: function() {
                e(".synchro-time").on("click", function() {
                    n.player.currentTime(e(this).data("time")), e('[name="synchro_time"]').val(e(this).data("time")), e("#video_span").length && e("html, body").animate({scrollTop: e("#video_span").offset().top}, 400)
                })
            }, showForm: function() {
                return this.$form.fadeIn(), this.$textarea.focus(), this.$form.length && e("html, body").animate({scrollTop: n.Comments.$form.offset().top}, 400), this
            }, closeForm: function(t) {
                e(t).on("click", function(e) {
                    e.preventDefault(), n.Comments.$form.fadeOut(), n.Comments.$textarea.val("")
                })
            }, closeReplyForm: function(t) {
                e(t).on("click", function(e) {
                    e.preventDefault(), n.Comments.$replyForm.fadeOut(), n.Comments.$reply.val("")
                })
            }}};
    e(document).ready(function() {
        t.init(), n.init()
    })
}), $(function(e) {
    App2 = {init: function() {
            this.priceButtons(e(".js-price-monthly, .js-price-yearly")), this.placeholder(), this.inputAppendix(e(".input-appendix")), e(".play-video").colorbox({iframe: !0, innerWidth: 425, innerHeight: 344})
        }, priceButtons: function(t) {
            var n = e(".price .monthly"), r = e(".price .yearly"), i = e(".price .per");
            t.on("click", function(t) {
                t.preventDefault(), n.hide(), r.hide(), e(this).addClass("active").siblings().removeClass("active"), e(this).is(".js-price-monthly") ? n.show() : r.show()
            })
        }, placeholder: function() {
            Modernizr.input.placeholder || (e("[placeholder]").focus(function() {
                var t = e(this);
                t.val() == t.attr("placeholder") && (t.val(""), t.removeClass("placeholder"))
            }).blur(function() {
                var t = e(this);
                if (t.val() == "" || t.val() == t.attr("placeholder"))
                    t.addClass("placeholder"), t.val(t.attr("placeholder"))
            }).blur(), e("[placeholder]").parents("form").submit(function() {
                e(this).find("[placeholder]").each(function() {
                    var t = e(this);
                    t.val() == t.attr("placeholder") && t.val("")
                })
            }))
        }, equalizeCols: function(t) {
            var n, r, i, s = setTimeout(function() {
                t.each(function() {
                    i = e(this).children(), r = e.map(i, function(t) {
                        return e(t).height()
                    }), n = Math.max.apply(Math, r), i.height(n)
                }), clearTimeout(s)
            }, 100)
        }, twitterFavourites: function(t) {
            var n, r;
            statusHTML = e.map(t, function(e) {
                return n = e.user.screen_name, r = e.text.replace(/((https?|s?ftp|ssh)\:\/\/[^"\s\<\>]*[^.,;'">\:\s\<\>\)\]\!])/g, function(e) {
                    return'<a href="' + e + '">' + e + "</a>"
                }).replace(/\B@([_a-z0-9]+)/ig, function(e) {
                    return'<a href="http://twitter.com/' + e.substring(1) + '">' + e + "</a>"
                }), '<li class="media micro"><div class="img"><img src="' + e.user.profile_image_url + '" alt=""></div>' + '<div class="body">' + '<div class="tweet-header">' + "<b>" + e.user.name + "</b> " + "<span>@" + e.user.screen_name + "</span>" + "</div>" + '<span class="tweet-body">' + r + "</span> " + '<a href="http://twitter.com/' + n + "/statuses/" + e.id_str + '" class="tweet-ago">' + App.relative_time(e.created_at) + "</a>" + "</div>" + "</li>"
            }), e("#twitter_update_list").html(statusHTML.join(""))
        }, relative_time: function(e) {
            var t = e.split(" "), n = Date.parse(t[1] + " " + t[2] + ", " + t[5] + " " + t[3]), r = arguments.length > 1 ?
                    arguments[1] : new Date, i = parseInt((r.getTime() - n) / 1e3) + r.getTimezoneOffset() * 60;
            return i < 60 ? "less than a minute ago" : i < 120 ? "about a minute ago" : i < 3600 ? parseInt(i / 60).toString() + " minutes ago" : i < 7200 ? "about an hour ago" : i < 86400 ? "about " + parseInt(i / 3600).toString() + " hours ago" : i < 172800 ? "1 day ago" : parseInt(i / 86400).toString() + " days ago"
        }, select: function(t) {
            t.each(function() {
                var t = e(this), n = t.children(), r = t.wrap(e("<span/>").addClass("select-substitute")).parent(), i = e("<span/>").addClass("select-toggle").appendTo(r), s = n.map(function() {
                    return e(this).attr("value") ? e(this).text().link("#") : null
                }).get().join("</li><li>"), o = e("<ul/>").addClass("select-content list").html("<li>" + s + "</li>").appendTo(r), u = n.filter(":selected").index();
                i.text(n.eq(u).text()), n = n.not('[value=""]'), s = o.find("a"), s.on("click", function() {
                    var t = s.index(e(this)), r = n.eq(t);
                    r.attr("selected", "").trigger("change").siblings().removeAttr("selected"), i.text(r.text())
                })
            }).parent().on("click", function(t) {
                t.stopPropagation(), t.preventDefault(), e(this).toggleClass("active"), e(".select-substitute").not(e(this)).removeClass("active")
            }), e(document).on("click", function() {
                e(".select-substitute").removeClass("active")
            })
        }, appendix: function(t) {
            t.each(function() {
                var t = e(this), n = t.next(".appendix-content"), r = t.offset();
                n.offset({top: r.top - n.outerHeight() / 2 + 10, left: r.left + t.width() + 20}), t.on("mouseover mouseout", function(e) {
                    n.stop()[e.type == "mouseout" ? "fadeOut" : "fadeIn"]("fast")
                })
            })
        }, getTextWidth: function(t) {
            var n = e("<span/>").text(t).appendTo("body"), r = n.width();
            return n.remove(), r
        }, inputAppendix: function(t) {
            t.each(function() {
                var t = e(this), n = t.prev("input"), r = t.offset(), i;
                n.on("keyup", function() {
                    i = e(this).val() || e(this).attr("placeholder"), t.css("left", App.getTextWidth(i) + 2)
                }).trigger("keyup")
            })
        }}
}), $(document).ready(function() {
    $('#flashMessage').css('cursor','pointer');
    $("#indicator").hide().ajaxStart(function() {
        $(this).show()
    }).ajaxStop(function() {
        $(this).hide()
    })
    $('#flashMessage').click(function(e) {
        $('#flashMessage').fadeOut();
    })
});