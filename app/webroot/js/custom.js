var MYBLOG_LIMIT = 3;
var MYWRAPPER_CLASS = 'posts-list';

var WP = {open: function(b) {
        var a = {posts: function() {
                var d = MYBLOG_LIMIT;
                var e = 0;
                var c = {all: function(g) {
                        var f = b + "/api/get_recent_posts/";
                        f += "?count=" + d + "&page=" + e + "&callback=?";
                        jQuery.getJSON(f, function(l) {
                            var k = l.posts;
                            for (var j = 0; j < k.length; j++) {
                                var h = k[j];
                                h.createComment = function(i, m) {
                                    i.postId = h.id;
                                    a.comments().create(i, m)
                                }
                            }
                            g(k)
                        })
                    }, findBySlug: function(f, h) {
                        var g = b + "/api/get_post/";
                        g += "?slug=" + f + "&callback=?";
                        jQuery.getJSON(g, function(i) {
                            h(i.post)
                        })
                    }, limit: function(f) {
                        d = f;
                        return c
                    }, page: function(f) {
                        e = f;
                        return c
                    }};
                return c
            }, pages: function() {
                var c = {findBySlug: function(d, f) {
                        var e = b + "/api/get_page/";
                        e += "?slug=" + d + "&callback=?";
                        jQuery.getJSON(e, function(g) {
                            f(g.page)
                        })
                    }};
                return c
            }, categories: function() {
                var c = {all: function(e) {
                        var d = b + "/api/get_category_index/";
                        d += "?callback=?";
                        jQuery.getJSON(d, function(f) {
                            e(f.categories)
                        })
                    }};
                return c
            }, tags: function() {
                var c = {all: function(e) {
                        var d = b + "/api/get_tag_index/";
                        d += "?callback=?";
                        jQuery.getJSON(d, function(f) {
                            e(f.tags)
                        })
                    }};
                return c
            }, comments: function() {
                var c = {create: function(f, e) {
                        var d = b + "/api/submit_comment/";
                        d += "?post_id=" + f.postId + "&name=" + f.name + "&email=" + f.email + "&content=" + f.content + "&callback=?";
                        jQuery.getJSON(d, function(g) {
                            e(g)
                        })
                    }};
                return c
            }};
        return a
    }};

var blog = WP.open('http://blog.sibme.com');
blog.posts().all(function(posts) {
    for (var i = 0; i < posts.length; i++) {
        theDate = $.format.date(posts[i].date, "MMM d, yyyy")

        jQuery('.' + MYWRAPPER_CLASS).append(function() {
            return '<li class="media"><div class="img"><i class="s s-document"></i></div><div class="body"><h5 class="zeta"><a href="' + posts[i].url + '">' + posts[i].title + '</a></h5><span class="micro">' + theDate + '</span></div></li>';

        });
    }
});
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-36675102-1']);
_gaq.push(['_setDomainName', 'sibme.com']);
_gaq.push(['_trackPageview']);

(function() {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();

$(document).ready(function() {
    $('#pres-nav li').mouseover(function() {
        if (!$(this).hasClass("active")) {
            $(this).addClass("hover");
        }
        if ($(this).is(':last-child')) {
            $(this).addClass("rborder-gray");
        } else {
            if (!$(this).next().hasClass("active")) {
                $(this).next().addClass("lborder-gray");
            }
        }


    });
    $('#pres-nav li').mouseout(function() {
        $(this).removeClass("hover");
        $(this).removeClass("rborder-gray");
        if (!$(this).next().hasClass("active")) {
            $(this).next().removeClass("lborder-gray");
        }
    });

    $("#button-1").click(function() {
        resetNav($(this));
        $("#slide-1").css("display", "block");
    });

    $("#button-2").click(function() {
        resetNav($(this));
        $("#slide-2").css("display", "block");
    });

    $("#button-3").click(function() {
        resetNav($(this));
        $("#slide-3").css("display", "block");
    });

    $("#button-4").click(function() {
        resetNav($(this));
        $("#slide-4").css("display", "block");
    });
})

function resetNav(whichButton) {
    $("#pres-nav li").removeClass("active");
    $("#pres-nav li").removeClass("hover");
    $("#pres-nav li").removeClass("lborder-blue");
    $("#pres-nav li").removeClass("rborder-blue");
    $(".slide").css("display", "none");
    whichButton.addClass("active");
    if (whichButton.is(':last-child')) {
        whichButton.addClass("rborder-blue");
    } else {
        whichButton.next().addClass("lborder-blue");
    }
}
