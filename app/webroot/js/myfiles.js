

var MyFiles = {};
window.language_based_content = {};
MyFiles.videoPage = 0;
MyFiles.videoLimit = 12;
MyFiles.documentPage = 0;
MyFiles.documentLimit = 10;



function load_language_based_data_for_js(url)
{
    
     $.ajax({
        type: 'POST',
        data: {
            url: url
        },
        url: home_url + '/Huddles/get_language_based_content_for_js/',
        success: function (response) {
            
           
           language_based_content = JSON.parse(response); 
           
        },
        errors: function (response) {
            
        }
    });
    
    
}

load_language_based_data_for_js('JSDATA');


MyFiles.downloadFiles = function (type) {
    var fileIds = this.getSelectedDocuments(type);

    if (fileIds.length > 0) {
        if(type=='video' && fileIds.length > 1){
            alert(language_based_content.multiple_video_download_not_allowed_js);
        } else {
            this.requestDownloadZip(fileIds);
        }
    } else {
        if(type=='video')
        {
        alert(language_based_content.please_select_atleast_one_video_js);
        }
        else
        {
         alert(language_based_content.please_select_atleast_one_resource_js);
        }
    }
    return false;
};

MyFiles.requestDownloadZip = function (fileIds) {
    var form = $('#downloadZipForm');
    if (form.length == 0) {
        form = $('<form id="downloadZipForm"></form>');
        form.appendTo('body');
    }
    form.hide()
            .prop('action', home_url + '/MyFiles/downloadZip')
            .prop('method', 'POST')
            .html('<input type="hidden" name="download_as_zip_docs" value="' + fileIds.join(',') + '" />')
            ;
    form.submit();
};

MyFiles.getSelectedDocuments = function (type, callback) {
    var ids = [];
    var map = {
        'document': '.download_file_doc_checkbox',
        'video': '.download_vid_checkbox'
    };

    if(type=='document' && $("label.docSearchTab.active").length==0){
        return ids;
    }

    if (map[type] != undefined) {
        $(map[type]).each(function (index, el) {
            el = $(el);
            if (el.parent('label').hasClass('active')) {
                if (callback !== undefined && typeof callback == 'function') {
                    ids.push(callback(el));
                } else {
                    ids.push(el.val());
                }
            }
        });
    }

    return ids;
};


MyFiles.deleteFiles = function () {
    var self = this;
    var checked_items = self.getCheckedDocDocuments();
    if (checked_items == '') {
        alert(language_based_content.please_select_atleast_one_resource_js);
        return false;
    }
    if (!confirm(language_based_content.are_you_sure_want_del_this_resource_js)) {
        return false;
    }
    $('#accountFolderIDocsIds').val(checked_items);
    $('#myFilesDocDelete').submit();
    return true;
};

MyFiles.getCheckedDocDocuments = function () {
    var docs = [];
    $('.download_file_doc_checkbox').each(function (index, value) {
        if ($(this).prop('checked') == true) {
            docs.push($(this).attr('value'));
        }
    });
    return docs.join(',');
};

MyFiles.rename_doc = function () {
    var docTab = $('.documentTab');
    if (docTab.is(":checked")) {
        docTab.each(function () {
            if ($(this).prop('checked') != true)
                return;

            var document_id = $(this).attr('data-account-folder-document-id2');
            var input = "#input-field-" + document_id;
            var video_title = "#videos-title-" + document_id;
            var input_block = "#input-title-" + document_id;
            var title = $(video_title).attr('data-title');
            $(input).val(title);
            $(input_block).css('display', 'block');
            $(video_title).css('display', 'none');
        });
    } else {
        alert(language_based_content.please_select_atleast_one_resource_js);
        return false;
    }
};

MyFiles.initDocuments = function () {
    $('.my-file-documents-list')
            .on('click', '.video-dropdown', function () {
                if ($(this).hasClass('video-dropdown-selected')) {
                    $(this).removeClass('video-dropdown-selected');
                    $($(this).siblings('.video-dropdown-container')[0]).css('display', 'none');
                } else {
                    MyFiles.CloseAllDropDown();
                    $(this).addClass('video-dropdown-selected');
                    $($(this).siblings('.video-dropdown-container')[0]).css('display', 'block');
                }
            })
            .on('click', '.btnDoneVideoLibrary', MyFiles.CloseDropDown)
            .on('click', '.btnCancelVideoLibrary', MyFiles.CloseAllDropDown)
            .on('click', '.documentTab', function () {
                var docTab = $('.documentTab');
                if (docTab.is(":checked")) {
                    $.each($('.documentTab'), function () {
                        if ($(this).prop('checked') != true)
                            return;
                        $('#copy-docs-files')
                                .attr('data-toggle', 'modal')
                                .attr('data-target', '#copyDocFiles');
                    });
                } else {
                    $('#copy-docs-files')
                            .attr('data-toggle', '')
                            .attr('data-target', '');
                }
            })
            ;

    $(".selectall-docs").click(function () {
        if ($(".selectall-docs").is(":checked")) {
            $('.docSearchTab').addClass('ui-checkbox active');
            $('.docsSearchTab').addClass('ui-checkbox active');
            $('.model').removeClass('active');
            $('.documentTab').attr('checked', true);
            $.each($('.documentTab'), function () {
                if ($(this).attr('checked') == 'checked' )
                {
                $('#copy-docs-files')
                        .attr('data-toggle', 'modal')
                        .attr('data-target', '#copyDocFiles');
                }
            });
        } else {
            $('.docSearchTab').removeClass('active');
            $('.docsSearchTab').removeClass('ui-checkbox active');
            $('.model').removeClass('active');
            $('#copy-docs-files')
                    .attr('data-toggle', '')
                    .attr('data-target', '');
            $('.documentTab').attr('checked', false);
        }
    });


    //========My Files Videos===========//
    $('#txtMyFilesSearchVideos').bind('keypress', function (e) {
        $('#My-File-Video-Clear').css('display', 'block');
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            $('#copy-video')
                    .attr('data-toggle', '')
                    .attr('data-target', '');
            
            doMyFileVideoSearchAjax();
            return false;
        }
    });

    $('#btnMyFileSearchVideo').bind('click', function (e) {
        $('#copy-video')
                    .attr('data-toggle', '')
                    .attr('data-target', '');
        doMyFileVideoSearchAjax();
        return false;
    });

    $('#My-File-Video-Clear').click(function (e) {
        $("#txtMyFilesSearchVideos:input").val('');
        $('#copy-video')
                    .attr('data-toggle', '')
                    .attr('data-target', '');
        doMyFileVideoSearchAjax();
        $('#My-File-Video-Clear').css('display', 'none');
        return false;
    });

    if ($('#myFilesVideoSort').length > 0) {
        $('#myFilesVideoSort').change(function () {
            doMyFileVideoSearchAjax();
        });
    }

    //=========My Files Documents===============//
    $('#txtSearchMyFilesDocuments').bind('keypress', function (e) {
        $('#My-File-Doc-Clear').css('display', 'block');
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
            //Enter keycode
            $('#copy-docs-files')
                    .attr('data-toggle', '')
                    .attr('data-target', '');
            doMyFileDocumentsSearchAjax();
            return false;
        }
    });

    $('#My-File-Doc-Clear').click(function (e) {
        $("#txtSearchMyFilesDocuments:input").val('');
        $('#copy-docs-files')
                    .attr('data-toggle', '')
                    .attr('data-target', '');
        $('.selectall-docs').attr('checked', false);
        doMyFileDocumentsSearchAjax();
        $('#My-File-Doc-Clear').css('display', 'none');
        return false;
    });

    $('#btnSearchMyFileDocuments').click(function () {
        $('#copy-docs-files')
                    .attr('data-toggle', '')
                    .attr('data-target', '');
        $('.selectall-docs').attr('checked', false);
        doMyFileDocumentsSearchAjax();
    });

    $('#cmbMyFileDocsSort').change(function () {
        doMyFileDocumentsSearchAjax();
    });

    $('#txtSearchHuddles').bind('keypress', function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) { //Enter keycode
            doHuddleSearch();
        }
        $('#clearSearchHuddles').css('display', 'block');
    });

    $('#btnHuddleSearch').click(function () {
        doHuddleSearch();
        $('#clearSearchHuddles').css('display', 'block');
    });

    $('#clearSearchHuddles').click(function (e) {
        $("#txtSearchHuddles:input").val('');
        doHuddleSearch();
        $('#clearSearchHuddles').css('display', 'none');
        return false;
    });


    if ($('#txtSearchMyFilesDocuments').length > 0) {
        $('#doc-load-more').click(function () {
            doMyFileDocumentsSearchAjax(true);
        });
    }

    if ($('#txtMyFilesSearchVideos').length > 0) {

        $('#video-load-more').click(function () {
            $('#video-load-more').hide();
            $('#loading_gif').show();
            doMyFileVideoSearchAjax(true);
        });
    }

    $("#comment_form").hide();
};

MyFiles.initVideoDetail = function () {
    /* don't execute script if there is video detail page */
    if ($('.video-detail').length == 0)
        return;

    var $window = $(window);
    var $document = $(document);
    var yTrigger = 325;
    var viz = false;
    var minHeight = 640;
    var minWidth = 950;
    var position;
    var rightBox = $('.right-box');
    var leftBox = $('.left-box');
    var docContainerUl = $("#docs-container ul");
    var videoOuter = $(".video-outer");
    var small_scroll = false;
    var currentWinH = $window.height();
    var currentWinW;
    var checkW = (currentWinH <= minHeight) ? addScroll() : $.noop();
    if (!small_scroll) {
        currentWinW = $window.width();
        checkW = (currentWinW <= minWidth) ? addScroll() : removeScroll();
    }

    $window.resize(function () {
        currentWinH = $window.height();
        checkW = (currentWinH <= minHeight) ? addScroll() : removeScroll();
        if (!small_scroll) {
            currentWinW = $window.width();
            checkW = (currentWinW <= minWidth) ? addScroll() : removeScroll();
        }
    });

    $window.load(function () {
        $window.scroll(function () {
            if (leftBox.outerHeight() > rightBox.outerHeight())
                return;

            position = $document.scrollTop();
            if (position >= yTrigger && viz == false && !small_scroll) {
                videoOuter.addClass("videoPosition");
                $("#comment_add_form_html").addClass("docsPosition");
                viz = true;
            } else if ((position < yTrigger && viz == true) || small_scroll) {
                videoOuter.removeClass("videoPosition");
                $("#comment_add_form_html").removeClass("docsPosition");
                viz = false;
            }
        });
    });

    function addScroll() {
        small_scroll = true;
        docContainerUl.addClass("scroll-small");
        $window.trigger('scroll');
    }

    function removeScroll() {
        small_scroll = false;
        docContainerUl.removeClass("scroll-small");
        $window.trigger('scroll');
    }

    /* @todo: rewrite this function by using jQuery.ajax for unified */
    var ajaxField = function () {
        //variables
        var xmlHttp = null;
        if (typeof String.prototype.trim !== 'function') { //this will add trim to IE8
            String.prototype.trim = function () {
                return this.replace(/^\s+|\s+$/g, '');
            }
        }

        return {
            init: function () {

            },
            update: function (responseField, url) {
                xmlHttp = new XMLHttpRequest();
                xmlHttp.onreadystatechange = function () {
                    if (xmlHttp.readyState != 4) {
                        return;
                    }
                    if (xmlHttp.status != 200 && xmlHttp.status != 304) {
                        alert( language_based_content.http_error_js + ' ' + xmlHttp.status);
                        return;
                    }
                    if (xmlHttp.readyState == 4) {
                        ajaxField.handleResponse(responseField, xmlHttp.responseText);
                    }
                };
                var newText = document.getElementById('ajaxInput').value.trim();
                var videoId = $('#video-id').val();
                var huddleId = $('#huddle_id').val();
                url = url + "/" + huddleId + "/" + videoId;
                var formData = new FormData();
                formData.append("title", newText);
                xmlHttp.open("POST", url, true);
                xmlHttp.send(formData);
                $("#vidTitle").css("display", "inline-block");
                $(".editArea").css("display", "none");
                $("#title_change").val(newText);
            },
            handleResponse: function (responseField, response) {
                document.getElementById(responseField).innerHTML = response;
            }

        };
    }();

    var vidTitle = $("#vidTitle");
    var ajaxInput = $("#ajaxInput");
    var fieldWidth = parseInt(vidTitle.css("width"));

    $(".submit").click(function () {
        if (console.log)
            console.log('save title');
        ajaxField.update("vidTitle", home_url + "/Huddles/changeTitle");
    });

    $(".editArea").css("width", fieldWidth + "px");

    ajaxInput
            .css("width", (fieldWidth - 150) + "px")
            .blur(function () {
                if (!$('.submit').is(':hover')) {
                    $(".editArea").css("display", "none");
                    $("#vidTitle").css("display", "inline-block");
                }
            })
            .keypress(function (e) {
                var code = (e.keyCode ? e.keyCode : e.which);
                if (code == 13) {
                    $(".submit").trigger('click');
                }
            })
            ;

    vidTitle
            .mouseover(function () {
                $(this).css("backgroundColor", "#FAFABE")
            })
            .mouseout(function () {
                $(this).css("backgroundColor", "#ffffff")
            })
            .click(function () {
                $(this).css("display", "none");
                $(".editArea").css("display", "inline-block");
                $("#ajaxInput")
                        .val($("#title_change").val())
                        .focus();
            })
            ;
    /* fix video progress bar */
    $('.vjs-play-progress').css('padding-right', '8px');
};

MyFiles.getCheckedVideos = function (document_id) {
    var users_id = '';
    $.each($('.doc_checkbox_' + document_id), function (index, value) {
        if ($(this).prop('checked') == true) {
            if (users_id != '')
                users_id += ',';
            users_id += $(this).attr('value');
        }
    });
    return users_id;
};

MyFiles.CloseDropDown = function () {
    var document_id = $(this).attr('id').split('-')[1];
    var checked_videos = MyFiles.getCheckedVideos(document_id);
    var huddle_id = $(this).parent().find('input[name=account_folder_id]').val();
    $.ajax({
        type: 'POST',
        data: {
            associated_videos: checked_videos
        },
        url: home_url + '/Huddles/associateVideoDocuments/' + document_id + '/' + huddle_id,
        success: function () {
            MyFiles.CloseAllDropDown();
        },
        errors: function (response) {
            alert(response.contents);
            location.href = home_url + '/MyFiles/view/2/';
        }
    });
};

MyFiles.CloseAllDropDown = function () {
    var btnDones = $('.btnDoneVideoLibrary');
    for (var i = 0; i < btnDones.length; i++) {
        var btnDone = $(btnDones[i]);
        var current_dropdwn = $(btnDone.parent().parent().prev()[0]);
        if (current_dropdwn.hasClass('video-dropdown-selected')) {
            current_dropdwn.removeClass('video-dropdown-selected');
            $(current_dropdwn.siblings('.video-dropdown-container')[0]).css('display', 'none');
        }
    }
};

$(function () {
    MyFiles.initVideoDetail();
    MyFiles.initDocuments();

//    $('#gVideoUpload').click(function() {
//        OpenFilePicker('MyFilesVideo');
//    });
//
//    $('#gDocumentUpload').click(function() {
//        OpenFilePicker('MyFilesDocs');
//    });

    $('#docs-container').on('submit', 'form', function (event) {
        var $form = $(this);
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function () {
                $form.closest('li').remove();
                if ($('ul.video-docs li').length == 0) {
                    $('ul.video-docs').html('<li style="clear:left;">No Attachments have been added.</li>');
                }
                doMyFileDocumentsSearchAjax();
            }
        });
        event.preventDefault();
    });

    var tab = $('#myFileTabIndex').val();
    var isLiveRecording = $('#isLiveRecording').val();
    if (tab == 1 && isLiveRecording == '0') {
        $('#gVideoUpload').show();
        $('#gDocumentUpload').hide();
    } else if (tab == 1 && isLiveRecording == '1') {
        $('#gVideoUpload').hide();
        $('#gDocumentUpload').hide();
    } else if (tab == 2) {
        $('#gDocumentUpload').show();
        $('#gVideoUpload').hide();
    }

    $('ul.tabset li a').bind('click', function () {
        if ($(this).attr('href') === '#tabbox1') {
            $('#gVideoUpload').show();
            $('#gDocumentUpload').hide();
            $('.table-container').css('display', 'block');
        } else if ($(this).attr('href') === '#tabbox2') {
            var $video_id = $('#video-id').val();
            $('#gDocumentUpload').show();
            $('#tabbox2').css('visibility', 'visible');
            if ($video_id == 'none') {
                $('.table-container').css('display', 'block');
            }
            $('#gVideoUpload').hide();
        } else {
            $('#gDocumentUpload').hide();
            $('#gVideoUpload').hide();
        }
    });

    refreshAttachedDocumentList();
    initVideoComments();

    $('.my-file-videos-list').on('click', '[id^=videos-title-]', function () {
        var docId = this.id.replace('videos-title-', '');
        var title = $(this).attr('data-title');
        var input = $('#input-field-' + docId);
        var titleBlock = $('#input-title-' + docId);
        input.val(title);
        $(this).hide();
        titleBlock.show();
    }).on('mouseover', '[id^=videos-title-]', function () {
        $(this).css("backgroundColor", "#FAFABE")
    }).on('mouseout', '[id^=videos-title-]', function () {
        $(this).css("backgroundColor", "#ffffff")
    });

    $('.my-file-videos-list').on('click', '.videoTab', function () {
        if ($('.download_vid_checkbox').is(":checked")) {
            $.each($('.download_vid_checkbox'), function (index, value) {
                if ($(this).prop('checked') == true) {
                    $('#copy-video')
                            .attr('data-toggle', 'modal')
                            .attr('data-target', '#copyFiles');
                }
            });
        } else {
            $('#copy-video')
                    .attr('data-toggle', '')
                    .attr('data-target', '');
        }
    });

    $(".selectall-videos").click(function () {
        if ($(".selectall-videos").is(":checked")) {
            $('.videoSearchTab').addClass('ui-checkbox active');
            $('.model').removeClass('active');
            $('.download_vid_checkbox').attr('checked', true);
             $.each($('.download_vid_checkbox'), function (index, value) {
                if ($(this).attr('checked') == 'checked') {
                    $('#copy-video')
                            .attr('data-toggle', 'modal')
                            .attr('data-target', '#copyFiles');
                }
            });
        } else {
            $('.videoSearchTab').removeClass('active');
            $('.model').remove('active');
            $('.download_vid_checkbox').attr('checked', false);
            $('#copy-video')
                    .attr('data-toggle', '')
                    .attr('data-target', '');
        }
    });

    var show_ff_message = $.cookie('show_ff_message');
    if (isFirefox() && !show_ff_message) {
        $.cookie('show_ff_message', 'displayed', {expires: new Date((new Date).getTime() + 14 * 86400 * 1000)});
        if ($('#flashMessage').length == 0) {
            $('#main').prepend('<div class="notice" id="flashMessage" style="cursor: pointer;">At this time, the video trimmer does not work with FireFox, please use Google Chrome, Internet Explorer 9+, or Safari.</div>');
            $('#flashMessage').click(function () {
                $(this).css('display', 'none')
            })
        } else {
            var html = $('#flashMessage').html();
            $('#flashMessage').html(html + '<br/>At this time, the video trimmer does not work with FireFox, please use Google Chrome, Internet Explorer 9+, or Safari.');
        }
    }
});

function doMyFileVideoSearchAjax(loadMore) {
    var keywords = $('#txtMyFilesSearchVideos').val();
    if ("Search Videos..." == keywords || undefined == keywords) {
        keywords = "";
    }
    if (loadMore == undefined)
        loadMore = false;
    if (loadMore) {
        MyFiles.videoPage++;
    } else {
        MyFiles.videoPage = 0;
    }
    $.ajax({
        type: 'POST',
        data: {
            type: 'get_video_comments',
            sort: $('#myFilesVideoSort').val(),
            limit: MyFiles.videoLimit,
            page: MyFiles.videoPage
        },
        dataType: 'json',
        url: home_url + '/MyFiles/getVideoSearch/' + keywords,
        success: function (response) {
            $('#video-load-more').show();
            $('#loading_gif').hide();
            if (response.html) {
                var list = $('.my-file-videos-list');
                var more = $('#video-load-more');
                if (loadMore) {
                    $('#myFileVideosCount').html(response.total);
                    list.append(response.html);
                } else {
                    $('#myFileVideosCount').html(response.total);
                    list.html(response.html);
                }

                if (MyFiles.videoLimit * (MyFiles.videoPage + 1) >= response.total) {
                    more.hide();
                } else {
                    more.show();
                }
                gApp.updateCheckboxEvent($('.my-file-videos-list .ui-checkbox input'));
            }
        },
        errors: function (response) {
            console.log(response);
            alert(response.contents);
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if (textStatus == 'error') {
//            alert('An error just occurred. The site will be refreshed.');
            location.reload(true);
        }
    });
}

function doMyFileDocumentsSearchAjax(loadMore, callback) {
    var keywords = $('#txtSearchMyFilesDocuments').val();
    if (keywords == 'Search Documents...' || undefined == keywords) {
        keywords = '';
    }
    if (loadMore == undefined)
        loadMore = false;
    if (loadMore) {
        MyFiles.documentPage++;
    } else {
        MyFiles.documentPage = 0;
    }
    $.ajax({
        type: 'POST',
        data: {
            type: 'get_video_comments',
            sort: $('#cmbMyFileDocsSort').val(),
            limit: MyFiles.documentLimit,
            page: MyFiles.documentPage
        },
        dataType: 'json',
        url: home_url + '/MyFiles/getDocSearch/' + keywords,
        success: function (response) {
            if (response.html) {
                var list = $('.my-file-documents-list');
                var more = $('#doc-load-more');
                if (loadMore) {
                    list.append(response.html);
                } else {
                    list.html(response.html);
                }

                if (MyFiles.documentLimit * (MyFiles.documentPage + 1) >= response.total) {
                    more.hide();
                } else {
                    more.show();
                }
                gApp.updateCheckboxEvent($('.my-file-documents-list .ui-checkbox input'));

                if (typeof callback == 'function') {
                    callback();
                }
            }
        },
        errors: function (response) {
            alert(response.contents);
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if (textStatus == 'error') {
//            alert('An error just occurred. The site will be refreshed.');
            location.reload(true);
        }
    });
}

function doMyFileDocumentsSearchAjaxInit(loadMore, callback) {
    var keywords = $('#txtSearchMyFilesDocuments').val();
    if (keywords == 'Search Documents...' || undefined == keywords) {
        keywords = '';
    }
    if (loadMore == undefined)
        loadMore = false;
    if (loadMore) {
        MyFiles.documentPage++;
    } else {
        MyFiles.documentPage = 0;
    }
    $.ajax({
        type: 'POST',
        data: {
            type: 'get_video_comments',
            sort: $('#cmbMyFileDocsSort').val(),
            limit: MyFiles.documentLimit,
            page: MyFiles.documentPage,
            init: 1
        },
        dataType: 'json',
        url: home_url + '/MyFiles/getDocSearch/' + keywords,
        success: function (response) {
            if (response.html) {
                var list = $('.my-file-documents-list');
                var more = $('#doc-load-more');
                if (loadMore) {
                    list.append(response.html);
                } else {
                    list.html(response.html);
                }

                if (MyFiles.documentLimit * (MyFiles.documentPage + 1) >= response.total) {
                    more.hide();
                } else {
                    more.show();
                }
                gApp.updateCheckboxEvent($('.my-file-documents-list .ui-checkbox input'));

                if (typeof callback == 'function') {
                    callback();
                }
            }
        },
        errors: function (response) {
            alert(response.contents);
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if (textStatus == 'error') {
//            alert('An error just occurred. The site will be refreshed.');
            location.reload(true);
        }
    });
}

function deleteVideos() {
    if ($('.videoTab').is(':checked')) {
        var checked_items = getCheckedDocuments();
        if (checked_items === '') {
            alert(language_based_content.please_select_atleast_one_video_to_del_js);
            return false;
        }
        if (!confirm(language_based_content.are_you_sure_want_to_del_this_video_js)) {
            return false;
        }
        $('#accountFolderIds').val(checked_items);
        $('#myFilesVideoDelete').submit();
        return true;
    } else {
        alert(language_based_content.please_select_atleast_one_video_js);
        return false;
    }
}

function getCheckedDocuments() {
    var users_id = '';
    $.each($('.download_vid_checkbox'), function (index, value) {
        if ($(this).prop('checked') == true) {
            if (users_id != '')
                users_id += ',';
            users_id += $(this).attr('value');
        }
    });
    return users_id;
}

function getCheckedDocuments2() {
    var users_id = '';
    var t_comments = false;
    $.each($('.download_vid_checkbox'), function () {
        if ($(this).attr('checked') === 'checked' || $(this).prop('checked') == true) {
            if (users_id != '')
                users_id += ',';
            users_id += $(this).attr('data-account-folder-document-id');
            if ($(this).attr('data-total-comments') > 0)
                t_comments = true;
        }
    });
    if (t_comments == false)
        $('.copy_video_box').css('display', 'none');
    else
        $('.copy_video_box').css('display', 'block');
    return users_id;
}

function getCheckedDocuments3() {
    var users_id = '';
    $.each($('.download_file_doc_checkbox'), function (index, value) {
        if ($(this).attr('checked') == 'checked' || $(this).prop('checked') == true) {
            if (users_id != '')
                users_id += ',';
            users_id += $(this).attr('data-account-folder-document-id2');
        }
    });
    return users_id;
}

function copy() {
    var checked_items = getCheckedDocuments2();

    if (checked_items == '') {
        alert(language_based_content.please_select_atleast_one_video_js);
        return false;
    } else {
        $('.copy-documents-tab').val('1');
        $('.copy-document-ids').val(checked_items);
        return true;
    }
}

function copy2() {
    var checked_items = getCheckedDocuments3();
    if (checked_items == '') {
        alert(language_based_content.please_select_atleast_one_resource_js);
        return false;
    } else {
        $('.copy-documents-tab').val('2');
        $('.copy-document-ids').val(checked_items);
        return true;
    }
}

function OpenFilePicker(docType) {

    // filepicker.setKey(filepicker_access_key);
    var client = filestack.init(filepicker_access_key);

    var uploadPath = $("#uploadPath").val();
    $('#txtUploadedFilePath').val("");
    $('#txtUploadedFileName').val("");
    $('#txtUploadedFileMimeType').val("");
    $('#txtUploadedFileSize').val("");
    $('#txtUploadedDocType').val(docType);

    var filepickerOptions;
    if (docType == 'MyFilesVideo') {
        filepickerOptions = {
            multiple: true,
            fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive', 'video', 'webcam'],
            extensions: ['3gp', '3gpp', 'avi', 'divx', 'dv', 'flv', 'm4v', 'mjpeg', 'mkv', 'mod', 'mov', 'mp4', 'mpeg', 'mpg', 'm2ts', 'mts', 'mxf', 'ogv', 'wmv', 'aif', 'mp3', 'm4a', 'ogg', 'wav', 'wma']
        }
    } else {
        filepickerOptions = {
            multiple: true,
            extensions: ['bmp', 'gif', 'jpeg', 'jpg', 'png', 'tif', 'tiff', 'swf', 'pdf', 'txt', 'docx', 'ppt', 'pptx', 'potx', 'xls', 'xlsx', 'xlsm', 'rtf', 'odt', 'doc', 'mp3', 'm4a'],
            fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive', 'url'],
        }
    }

    // fix for iOS 7
    if (isIOS()) {
        filepickerOptions.multiple = false;
    }

    // new window for iPhone
    if (!!navigator.userAgent.match(/iPhone/i)) {
        filepickerOptions.container = 'window';
    }


    if (docType == 'MyFilesVideo') {

        client.pick({
            maxFiles: 1000,
            fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive', 'video'],
            accept: ['video/*', '3gp', '3gpp', 'avi', 'divx', 'dv', 'flv', 'm4v', 'mjpeg', 'mkv', 'mod', 'mov', 'mp4', 'mpeg', 'mpg', 'm2ts', 'mts', 'mxf', 'ogv', 'wmv', 'aif', 'mp3', 'm4a', 'ogg', 'wav', 'wma'],
            lang: language_based_content.language_setting_js,
            storeTo: {
                location: "s3",
                path: uploadPath,
                access: 'public',
                container: bucket_name,
                region: 'us-east-1'
            }
        }).then(
                function (inkBlob) {
                    inkBlob = inkBlob.filesUploaded;
                    if (inkBlob && inkBlob.length > 0) {
                        for (var i = 0; i < inkBlob.length; i++) {
                            var blob = inkBlob[i];
                            var fileExt = getFileExtension(blob.filename).toLowerCase();

                            $('#txtUploadedFilePath').val(blob.key);
                            $('#txtUploadedFileName').val(blob.filename);
                            $('#txtUploadedFileMimeType').val(blob.mimetype);
                            $('#txtUploadedFileSize').val(blob.size);
                            $('#txtUploadedUrl').val(blob.url);


                            if (docType == 'MyFilesVideo') {
                                PostMyFilesVideo(docType, (fileExt == 'mp4' ? true : false));
                            } else {
                                PostMyFilesDocument();
                            }
                        }
                    }
                }),
                function (FPError) {
                    var error_desc = 'Unkown Error';
                    //as per filepicker documentation these are possible two errors
                    if (FPError.code == 101) {
                        error_desc = 'The user closed the dialog without picking a file';
                    } else if (FPError.code = 151) {
                        error_desc = 'The file store couldnt be reached';
                    }

                    $.ajax({
                        type: 'POST',
                        data: {
                            type: 'Workspace',
                            id: '',
                            error_id: FPError.code,
                            error_desc: error_desc,
                            docType: docType,
                            current_user_id: current_user_id
                        },
                        url: home_url + '/Huddles/logFilePickerError/',
                        success: function (response) {
                            //Do nothing.
                        },
                        errors: function (response) {
                            alert(language_based_content.error_occured_while_logging_js + ' ' + FPError.code);
                        }

                    });
                }

    }

    else
    {


        client.pick({
            maxFiles: 1000,
            accept: ['application/*', '.bmp', '.gif', '.jpeg', '.jpg', '.png', '.tif', '.tiff', '.swf', '.pdf', '.txt', '.docx', '.ppt', '.pptx', '.potx', '.xls', '.xlsx', '.xlsm', '.rtf', '.odt', '.doc'],
            fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive'],
            lang: language_based_content.language_setting_js,
            storeTo: {
                location: "s3",
                path: uploadPath,
                access: 'public',
                container: bucket_name,
                region: 'us-east-1'
            }
        }).then(
                function (inkBlob) {
                    inkBlob = inkBlob.filesUploaded;
                    if (inkBlob && inkBlob.length > 0) {
                        for (var i = 0; i < inkBlob.length; i++) {
                            var blob = inkBlob[i];
                            var fileExt = getFileExtension(blob.filename).toLowerCase();

                            $('#txtUploadedFilePath').val(blob.key);
                            $('#txtUploadedFileName').val(blob.filename);
                            $('#txtUploadedFileMimeType').val(blob.mimetype);
                            $('#txtUploadedFileSize').val(blob.size);
                            $('#txtUploadedUrl').val(blob.url);


                            if (docType == 'MyFilesVideo') {
                                PostMyFilesVideo(docType, (fileExt == 'mp4' ? true : false));
                            } else {
                                var attachment_no = $("#attachment").text();
                                var attach_new = attachment_no.substring(13, 14);
                                attachment_no = parseInt(attach_new) + 1;
                                $("#attachment").text('Attachments (' + attachment_no + ')');
                                PostMyFilesDocument();
                            }
                        }
                    }
                }),
                function (FPError) {
                    var error_desc = 'Unkown Error';
                    //as per filepicker documentation these are possible two errors
                    if (FPError.code == 101) {
                        error_desc = 'The user closed the dialog without picking a file';
                    } else if (FPError.code = 151) {
                        error_desc = 'The file store couldnt be reached';
                    }

                    $.ajax({
                        type: 'POST',
                        data: {
                            type: 'Workspace',
                            id: '',
                            error_id: FPError.code,
                            error_desc: error_desc,
                            docType: docType,
                            current_user_id: current_user_id
                        },
                        url: home_url + '/Huddles/logFilePickerError/',
                        success: function (response) {
                            //Do nothing.
                        },
                        errors: function (response) {
                            alert( language_based_content.error_occured_while_logging_js + ' ' + FPError.code);
                        }

                    });
                }

    }

}

function getFileExtension(filename) {

    return filename.substr(filename.lastIndexOf('.') + 1)

}

function PostMyFilesVideo(doc_type, direct_publish) {
    $.ajax({
        url: home_url + (doc_type == 'MyFilesVideo' ? '/MyFiles/add/1' : '/MyFiles/add/2'),
        method: 'POST',
        success: function (data) {
            CloseVideoUpload(doc_type);
            if ($('#flashMessage').length == 0) {
                $('#main').prepend('<div class="success" id="flashMessage" style="cursor: pointer;">' + language_based_content.sit_tight_your_video_is_currently_js + '</div>');
                $('#flashMessage').click(function () {
                    $(this).css('display', 'none')
                })
            } else {
                //var html = $('#flashMessage').html();
                //$('#flashMessage').html(html + '<br/>Sit tight, your video is currently processing.  You will receive an email when your video is ready to be viewed.');
                $('#flashMessage').css('display', 'block');
                $('#flashMessage').html(language_based_content.sit_tight_your_video_is_currently_js);
            }
        },
        data: {
            video_title: $('#txtUploadedFileName').val(),
            video_desc: '',
            video_url: $('#txtUploadedFilePath').val(),
            video_file_name: $('#txtUploadedFileName').val(),
            video_mime_type: $('#txtUploadedFileMimeType').val(),
            video_file_size: $('#txtUploadedFileSize').val(),
            rand_folder: $('#txtVideoPopupRandomNumber').val(),
            direct_publish: (direct_publish == true ? 1 : 0)
        }
    });
}

function CloseVideoUpload(doc_type) {
    var ts = Math.round((new Date()).getTime() / 1000);
    if (doc_type == 'doc' && $('ul.tabset li a.active').length > 0 && $('ul.tabset li a.active').html() == 'Videos' && $('#txtCurrentVideoID').length > 0) {
        location.href = $('#txtCurrentVideoUrl').val() + '?ts=' + ts;
    } else if (doc_type == 'doc') {
        if ($('#tabbox2 .docs-list').length == 0)
            location.href = home_url + '/MyFiles/view/2';
        else
            doDocumentSearchAjax();
    } else {
        if ($('#tabbox1 .docs-list').length == 0)
            location.href = home_url + '/MyFiles/view/1';
        else
            doMyFileVideoSearchAjax();
    }
}

function PostMyFilesDocument() {
    showProcessOverlay($('#docs-container'));
    showProcessOverlay($('.docs-list'));
    $.ajax({
        url: home_url + '/MyFiles/add',
        method: 'POST',
        success: function (data) {
            $('#doc_checkbox').show();
            if (parseInt(data) == 0) {
                alert(language_based_content.unable_to_save_data_js);
            } else {
                CloseDocumentUpload();
            }
        },
        data: {
            video_title: $('#txtUploadedFileName').val(),
            video_desc: "",
            video_url: $('#txtUploadedFilePath').val(),
            video_file_name: $('#txtUploadedFileName').val(),
            video_mime_type: $('#txtUploadedFileMimeType').val(),
            video_file_size: $('#txtUploadedFileSize').val(),
            video_id: $('#txtCurrentVideoID').val(),
            url: $('#txtUploadedUrl').val(),
            rand_folder: "0"
        }
    });
}

function CloseDocumentUpload() {
    doMyFileDocumentsSearchAjax(false, function () {
        hideProcessOverlay($('.docs-list'));
    });
    refreshAttachedDocumentList();
}

function refreshAttachedDocumentList() {
    if ($('#tab-area a.active').attr('href') != '#tabbox1')
        return;
    var videoInp = $('#txtCurrentVideoID');
    if (videoInp.length == 0)
        return;
    var url = home_url + '/MyFiles/getAttachedDocuments/' + videoInp.val() + '?t=' + (new Date()).getTime();
    $.getJSON(url).done(function (data) {
        var container = $('#docs-container');
        container.html(data.html);
        hideProcessOverlay(container);
    });
}

function update_title($document_id) {
    var $value = '#input-field-' + $document_id;
    var $video_title = "#videos-title-" + $document_id;
    var $title_block = "#input-title-" + $document_id;
    var $newText = $($value).val();
    if ($newText == '') {
        $($value).css('border', '1px red solid');
        return false;
    }
    var $nexText = $.trim($newText);
    $.ajax({
        url: home_url + "/MyFiles/editTitle",
        data: {title: $nexText, document_id: $document_id},
        type: 'POST',
        success: function (response) {
            $($video_title).attr('data-title', response)
            $($video_title).html(response);
            $($value).val("");
            $($title_block).css('display', 'none');
            $($video_title).css('display', 'block');
        },
        error: function () {
            alert(language_based_content.network_error_occured_js);
        }
    });
}

function cancel($document_id) {
    var $video_title = "#videos-title-" + $document_id;
    var $title_block = "#input-title-" + $document_id;
    $($title_block).css('display', 'none');
    $($video_title).css('display', 'block');
}

function rename() {
    if ($('.download_vid_checkbox').is(":checked")) {
        $.each($('.download_vid_checkbox'), function () {
            if ($(this).prop('checked') == true) {
                var document_id = $(this).attr('data-account-folder-document-id');
                var input = "#input-field-" + document_id;
                var video_title = "#videos-title-" + document_id;
                var input_block = "#input-title-" + document_id;
                var title = $(video_title).attr('data-title');
                $(input).val(title);
                $(input_block).css('display', 'block');
                $(video_title).css('display', 'none');

            }
        });
    } else {
        alert(language_based_content.please_select_atleast_one_video_js);
        return false;
    }
}


