/* By Kasper Mikiewicz @Idered */

$(function($) {

    App2 = {

        init: function() {

            this.priceButtons($('.js-price-monthly, .js-price-yearly'));
            this.placeholder();
            //this.select($('.select'));
            //this.appendix($('.appendix'));
            this.inputAppendix($('.input-appendix'));
            //this.equalizeCols($('.equalize-cols'));
            $(".play-video").colorbox({iframe:true, innerWidth:425, innerHeight:344});

        }

        , priceButtons: function($buttons) {

            var $monthlyPrices = $('.price .monthly'),
                $yearlyPrices = $('.price .yearly'),
                $per = $('.price .per');

            $buttons.on('click', function(e) {

                e.preventDefault();

                $monthlyPrices.hide();
                $yearlyPrices.hide();

                $(this).addClass('active').siblings().removeClass('active');

                $(this).is('.js-price-monthly') ?
                    $monthlyPrices.show() :
                    $yearlyPrices.show();

            });

        } // priceButtons


        , placeholder: function() {

            if(!Modernizr.input.placeholder){

                $('[placeholder]').focus(function() {
                  var input = $(this);
                  if (input.val() == input.attr('placeholder')) {
                    input.val('');
                    input.removeClass('placeholder');
                  }
                }).blur(function() {
                  var input = $(this);
                  if (input.val() == '' || input.val() == input.attr('placeholder')) {
                    input.addClass('placeholder');
                    input.val(input.attr('placeholder'));
                  }
                }).blur();
                $('[placeholder]').parents('form').submit(function() {
                  $(this).find('[placeholder]').each(function() {
                    var input = $(this);
                    if (input.val() == input.attr('placeholder')) {
                      input.val('');
                    }
                  })
                });
            }

        } // placeholder

        , equalizeCols: function($elements) {

            var max, heights, childrens,
                timer = setTimeout(function() {

                $elements.each(function() {

                    childrens = $(this).children();

                    heights = $.map(childrens, function(block) { return $(block).height() });

                    max = Math.max.apply(Math, heights);

                    childrens.height(max);

                });

                clearTimeout(timer);

            }, 100);

        } // equalizeCols

        , twitterFavourites: function(tweets) {

            var username, status
                statusHTML = $.map(tweets, function(tweet) {

                    username = tweet.user.screen_name,
                    status = tweet.text.replace(/((https?|s?ftp|ssh)\:\/\/[^"\s\<\>]*[^.,;'">\:\s\<\>\)\]\!])/g, function(url) {
                                return '<a href="'+url+'">'+url+'</a>';
                            }).replace(/\B@([_a-z0-9]+)/ig, function(reply) {
                                return  '<a href="http://twitter.com/'+reply.substring(1)+'">'+reply+'</a>';
                            });

                return ''
                +'<li class="media micro">'
                    + '<div class="img"><img src="' + tweet.user.profile_image_url + '" alt=""></div>'
                    + '<div class="body">'
                        + '<div class="tweet-header">'
                            + '<b>' + tweet.user.name + '</b> '
                            + '<span>@' + tweet.user.screen_name + '</span>'
                        + '</div>'
                        + '<span class="tweet-body">' + status +'</span> '
                        + '<a href="http://twitter.com/' + username + '/statuses/' + tweet.id_str + '" class="tweet-ago">' + App.relative_time(tweet.created_at) + '</a>'
                    + '</div>'
                +'</li>'

            });

            $('#twitter_update_list').html(statusHTML.join(''));

        } // twitterFavourites

         , relative_time: function(time_value) {
            var values = time_value.split(" "),
                parsed_date = Date.parse(values[1] + " " + values[2] + ", " + values[5] + " " + values[3]),
                relative_to = (arguments.length > 1) ? arguments[1] : new Date(),
                delta = parseInt((relative_to.getTime() - parsed_date) / 1000) + (relative_to.getTimezoneOffset() * 60);

            return  delta < 60 ? 'less than a minute ago'
                : delta < 120 ? 'about a minute ago'
                : delta < (60*60) ? (parseInt(delta / 60)).toString() + ' minutes ago'
                : delta < (120*60) ? 'about an hour ago'
                : delta < (24*60*60) ? 'about ' + (parseInt(delta / 3600)).toString() + ' hours ago'
                : delta < (48*60*60) ? '1 day ago'
                : (parseInt(delta / 86400)).toString() + ' days ago';
        } // relative_time

        , select: function($this) {

            $this.each(function() {

                var $select = $(this),

                    $options = $select.children(),

                    $substitute = $select.wrap($('<span/>').addClass('select-substitute')).parent(),

                    $toggle = $('<span/>').addClass('select-toggle').appendTo($substitute),

                    $optionsList = $options.map(function() {
                        return $(this).attr('value') ? $(this).text().link('#') : null;
                    }).get().join('</li><li>'),

                    $content = $('<ul/>').addClass('select-content list').html('<li>' + $optionsList + '</li>').appendTo($substitute),

                    current = $options.filter(':selected').index();

                $toggle.text($options.eq(current).text());

                $options = $options.not('[value=""]');

                $optionsList = $content.find('a');

                $optionsList.on('click', function() {

                    var index = $optionsList.index($(this)),
                        $option = $options.eq(index);

                    $option.attr('selected', '').trigger('change').siblings().removeAttr('selected');

                    $toggle.text($option.text());

                });

            }).parent().on('click', function(event) {

                event.stopPropagation();

                event.preventDefault();

                $(this).toggleClass('active');

                $('.select-substitute').not($(this)).removeClass('active');

            });

            $(document).on('click', function() {
                $('.select-substitute').removeClass('active');
            });

        } // select

        , appendix: function($el) {

            $el.each(function() {

                var $this    = $(this),
                    $content = $this.next('.appendix-content'),
                    offset   = $this.offset();

                $content.offset({
                    top: offset.top - $content.outerHeight() / 2 +10,
                    left: offset.left + $this.width() +20
                });

                $this.on('mouseover mouseout', function(e) {

                    $content.stop()[e.type == 'mouseout' ? 'fadeOut' : 'fadeIn']('fast');

                });

            });

        } // appendix

        , getTextWidth: function(text) {

            var $el = $('<span/>').text(text).appendTo('body'),
                width = $el.width();

            $el.remove();

            return width;

        } // getTextWidth

        , inputAppendix: function($el) {

            $el.each(function() {

                var $this   = $(this),
                    $input  = $this.prev('input'),
                    offset  = $this.offset(),
                    val;

                $input.on('keyup', function() {

                    val = $(this).val() || $(this).attr('placeholder');

                    $this.css('left', App.getTextWidth(val) + 2);

                }).trigger('keyup');

            });

        } // inputAppendix

    }

   // App2.init();

});