var minLevel = "0";
var maxLevel = "4";
var currentLevel = "1";
var counter = 2;
var tier_rows = 2;
var standard_rattings = $('#total-used-count').attr('data-count');
var entry_count = 2;
var currentGroup = $(".f-group:eq( 0 )");
var currentCheckLevel = $('#check-level input:radio:checked').val();
var current_tier = $('#loopCounter').val();

window.language_based_content_jsData = {};

$(document).ready(function () {
    //enableUniqueDescription();
    pl_sorting();
    setHighestLowest();


    $('.input_limit').on('keydown keyup change', function () {
        var maxLength = $(this).attr('max_length');
        var char = $(this).val();
        var charLength = $(this).val().length;
        if (charLength > maxLength) {
            //$('.error_msg_text').text('Length is not valid, maximum ' + maxLength + ' allowed.');
            $(this).next().text(language_based_content_jsData.Length_is_not_valid_maximum+' ' + maxLength + ' '+language_based_content_jsData.allowed);
            $(this).val(char.substring(0, maxLength));
        } else {
            $(this).next().text('');
        }
    });
    //add prefix field content to framework item
    $("#itemPre").keyup(function () {
        content = $(this).val();
        $(currentGroup).find(".pre").html(content);
        $(currentGroup).find(".pre_input_box").val(content);

    });
    $("#itemPrePl").keyup(function () {
        content = $(this).val();
        $(currentGroup).find(".plTitleText").html($.trim(content));
        $(currentGroup).find(".plTitleInput").val($.trim(content));
    });

    //add prefix field content to framework item
    $("#itemContent").keyup(function () {
        content = $(this).val();
        $(currentGroup).find(".content").html(content);
        $(currentGroup).find(".content_input_box").val($.trim(content));
    });
    $('.pl_desc').keyup(function (e) {
        content = $(this).val();
        pl_text_id = $(this).attr('pl_data');
        pl_counter = $(this).attr('data_counter');
        $(currentGroup).find(".pl_desc_text_" + pl_counter).val(content);
        $(currentGroup).find(".pl_desc_text_id_" + pl_counter).val(pl_text_id);
    });
    $('#standardLabel').keyup(function (e) {
        content = $(this).val();
        $(currentGroup).find(".pl_standard_label").val(content);
    });
    $("#itemContentPl").keyup(function () {
        content = $(this).val();
        $(currentGroup).find(".pl_contents_text").html($.trim(content));
        $(currentGroup).find(".pl_contents_input").val($.trim(content));
    });

    //add new framework row
    $(".add-entry").click(function () {
        currentCheckLevel = $('#check-level input:radio:checked').val();
        resetSelected();
        entry_count = $('#chunks_level').val();
        current_tier = $('#loopCounter').val();
        entry = newEntry();
        $("#f-preview").append(entry);
        console.log(currentCheckLevel + " // " + currentLevel);
        currentGroup = $(".f-group:last");
        $("#itemContent").val("new item");
        populate_contents(currentGroup);
        tier_rows++;

    });

    //add new PL row
    $(".pl-add-entry").click(function () {
        counter = $('#pl-f-preview').find('.f-group').length;
        standard_rattings = $('#total-used-count').attr('data-count');

        if (counter >= 10) {
            alert('You have exceeding the maximum limit.');
            return false;
        }
        resetSelected();
        var total_dives_length = $('#pl-f-preview').find('.f-group').length + 1;
        entry = newEntryPl(total_dives_length);
        $('.rub_used').html(total_dives_length + '/' + 10 + ' Used');
        $('#total-used-count').attr('data-count', total_dives_length);
        pl_sorting();
        if ($('#auto_scroll_switch3').is(':checked') == true) {
            $("#pl-f-preview").append(entry);
            currentGroup = $(".f-group:last");
        } else {
            $("#pl-f-preview").prepend(entry);
            currentGroup = $(".f-group:first");
            setHighestLowest();
        }


        $("#itemContent").val("new item");
        //enableUniqueDescription();
        repopulate(currentGroup);
        enablePerformanceLevelInbox();
        
        standard_rattings++;
        $(currentGroup).trigger('click');

    });


    function enableUniqueDescription() {
        if ($('#auto_scroll_switch2').is(':checked') == true) {
            $('.pl_contents_show').hide();
            $('#itemContentBoxPl').hide();
        } else {
            $('.pl_contents_show').show();
            $('#itemContentBoxPl').show();

        }
        $('.titlecls').html('<div class="rub_rowleft"><span>'+language_based_content_jsData.highest+'</span><div class="rub_line"></div></div>')
    }

    function populate_contents(currentGroup) {
        resetSelected();
        $(currentGroup).addClass('selectedRB');
        repopulate($(currentGroup));
        enablePerformanceLevelInbox();
    }


    $("#f-preview").on("click", ".f-group", function () {
        currentGroup = $(this);
        resetSelected();
        $(this).addClass('selectedRB');
        repopulate($(this));
        enablePerformanceLevelInbox();
    });

    $("#pl-f-preview").on("click", ".f-group", function () {
        if ($(this).attr('published') == 'true') {
           $('#showflashmessage1').html('<div class="message error" style="color: red;">'+language_based_content_jsData.this_rubric_has_been_published_and_edits_in_this_step_are_not_allowed+'</div>');
            $('html, body').animate({
                scrollTop: 0
            }, 800);
            $('.input_limit').attr('readonly','readonly');
        }else{
            $('#showflashmessage1').html('');
            $('.input_limit').removeAttr('readonly');
        }
        currentGroup = $(this);
        resetSelected();
        $(this).addClass('selectedRB');
        repopulate_pl($(this));
    })


    $(".f-indent").on("click", function () {

        indent();

    })


    $(".f-outdent").on("click", function () {

        outdent();

    })

    $(".f-up").on("click", function () {
        selectUp();
        enablePerformanceLevelInbox();
    })
    $(".f-up-pl").on("click", function () {
        selectUpPl();
    })

    $(".f-down").on("click", function () {
        selectDown();
        enablePerformanceLevelInbox();
    })
    $(".f-down-pl").on("click", function () {
        selectDownPl();
    })

    $("#check-level").on("change", function () {
        currentCheckLevel = $('#check-level input:radio:checked').val();
        resetCheckLevel(currentCheckLevel);
    })
    $('#auto_scroll_switch2').on('click', function (e) {
        if ($('#auto_scroll_switch2').is(':checked') == true) {
            $('#itemContentBoxPl').hide();
            $('.pl_contents_show').hide();
        } else {
            $('#itemContentBoxPl').show();
            $('.pl_contents_show').show();


        }
    });

    $('#auto_scroll_switch3').on('click', function (e) {
        if ($('#auto_scroll_switch3').is(':checked') == true) {
            $('.sortable').each(function () {
                var $this = $(this);
                $this.append($this.find('.pl-levels').get().sort(function (a, b) {
                    return $(a).data('index') - $(b).data('index');
                }));
            });
        } else {
            $('.sortable').each(function () {
                var $this = $(this);
                $this.append($this.find('.pl-levels').get().sort(function (a, b) {
                    return $(b).data('index') - $(a).data('index');
                }));
            });
        }
        setHighestLowest();

    });
    //trigger last index
    $('.f-group').first().trigger('click');
    // remove function
    $('.pl-remove-entry').on('click', function (e) {
        var total_counter = $('#total-used-count').attr('data-count');
        currentGroup = $('div.selectedRB').index();
        pl_id = $('div.selectedRB').find('.performance_level_ids').val();
        framework_id = $("input[name=framework_id]").val();
        if (typeof(pl_id) != 'undefined' && pl_id != '') {
            $.ajax({
                type: 'POST',
                url: home_url + '/accounts/delete_performance_level/',
                data: {
                    pl_id: pl_id,
                    account_tag_id: framework_id
                },
                dataType: 'json',
                success: function (r) {
                    if (r.success == true) {
                        $('#showflashmessage1').html('<div id="flashMessage" class="message success" style="cursor: pointer;">'+language_based_content_jsData.PL_title_deleted_successfully+'</div>');
                    } else {
                        $('#showflashmessage1').html('<div id="flashMessage" class="message success" style="cursor: pointer;">'+language_based_content_jsData.PL_title_is_not_deleted_successfully+'</div>');
                    }

                }
            });
        }


        if ($('#pl-f-preview').find('.f-group').length == 1) {
            alert(language_based_content_jsData.You_can_not_delete_all_performance);
            return false;
        }
        if($('div.selectedRB').index()==0){
            newIndex = $('div.selectedRB').index() + 0;
        }else{
            newIndex = $('div.selectedRB').index() - 1;
        }


        $('div.selectedRB').remove();
        var total_dives_length = $('#pl-f-preview').find('.f-group').length;
        if (total_dives_length != 1) {
            $('.rub_used').html(total_dives_length + '/' + 10 + ' '+language_based_content_jsData.used);
            //console.log(total_dives_length);
            //  if(currentGroup !=0){
            $('#total-used-count').attr('data-count', total_dives_length);
            //}
        } else {
            $('.rub_used').html(total_dives_length + '/' + 10 + ' '+language_based_content_jsData.used);
            $('#total-used-count').attr('data-count', 2);
        }
        pl_sorting_revision();
        setHighestLowest();
        currentGroup = $(".f-group:eq(" + newIndex + ")");
        if(currentGroup.length == 0){
            $('#itemPrePl').val('');
            $('#itemContentPl').val('');
        }
        $(currentGroup).addClass('selectedRB');
        $('.selectedRB').trigger('click');
    });

    $('.std-remove-entry').on('click', function (e) {
        currentGroup = $('#f-preview .selectedRB').index();
        account_tag_id = $('#f-preview .selectedRB').find('.account_tag_id').val();

        if ($('#f-preview').find('.f-group').length == 1) {
            alert(language_based_content_jsData.You_can_not_delete_all_performance);
            return false;
        }
        newIndex = $('#f-preview .selectedRB').index()-1;
        $('#f-preview .selectedRB').remove();
        if (typeof(account_tag_id) != 'undefined' && account_tag_id != '') {
            $.ajax({
                type: 'POST',
                url: home_url + '/accounts/delete_tag_standard',
                data: {account_tag_id: account_tag_id},
                dataType: 'json',
                success: function (r) {
                    if (r.success == true) {
                        $('#showflashmessage1').html('<div id="flashMessage" class="message success" style="cursor: pointer;">'+language_based_content_jsData.PL_title_deleted_successfully+'</div>');
                    } else {
                        $('#showflashmessage1').html('<div id="flashMessage" class="message success" style="cursor: pointer;">'+language_based_content_jsData.PL_title_is_not_deleted_successfully+'</div>');
                    }

                }
            });
        }
        currentGroup = $(".f-group:eq(" + newIndex + ")");
        $(currentGroup).addClass('selectedRB');
        if($('#f-preview').find('.selectedRB').length ==1){
            $('.selectedRB').trigger('click');
        }else{
            $('.f-group').first().addClass('selectedRB');
            $('.f-group').first().trigger('click');
        }

    })

});

function pl_sorting_revision() {
    if ($('#auto_scroll_switch3').is(':checked') == true) {
        var tCount = 1;
        $('.sortable .pl-levels').each(function (index) {
            var $this = $(this);
            $(this).attr('data-level', tCount);
            $(this).attr('data-index', tCount);
            $(this).find('.standards_rattings').val(tCount);
            tCount++;
        });
    } else {
        var tCount = $('#pl-f-preview').find('.f-group').length;
        $('.sortable .pl-levels').each(function (index) {
            var $this = $(this);
            //alert(tCount);
            console.log(tCount);
            $(this).attr('data-level', tCount);
            $(this).attr('data-index', tCount);
            $(this).find('.standards_rattings').val(tCount);
            tCount--;
        });
    }
}

function pl_sorting() {
    if ($('#auto_scroll_switch3').is(':checked') == true) {
        $('.sortable').each(function () {
            var $this = $(this);
            $this.append($this.find('.pl-levels').get().sort(function (a, b) {
                return $(a).data('index') - $(b).data('index');
            }));
        });
    } else {
        $('.sortable').each(function () {
            var $this = $(this);
            $this.append($this.find('.pl-levels').get().sort(function (a, b) {
                return $(b).data('index') - $(a).data('index');
            }));
        });
    }
    setHighestLowest();
}

function enablePerformanceLevelInbox() {
    $('.pl_desc').val('');
    var counter = 1;
    $('div.selectedRB .pl_desc_data').each(function (value) {
        var pl_values = $(this).val();
        var pl_id_with_value = $(this).attr('pl_id_with_value');
        $('#pl_desc_' + pl_id_with_value).val(pl_values);
        counter++;
    });
    var sring_label_len = '';
    var pl_string_len = $(currentGroup).find('.pl_standard_label').val();
    if(pl_string_len.length > 24){
        sring_label_len = pl_string_len.substr(0,24);
    }else{
        sring_label_len = pl_string_len;
    }
    $('#standardLabel').val(sring_label_len);
    if ($('.selectedRB').find('.pl_standard_level').hasClass('pl_standard_level') == true) {
        $('.performance_levels_with_desc').show();
    } else {
        $('.performance_levels_with_desc').hide();
    }
}

function newEntry() {
    var entry = '';
    currentCheckLevel = $('#current_level_tier').val();
    currentLevel = $('#current_level_checkbox').val();
    var next_row = $('#f-preview .f-group').length + 1;
   // var next_row1 = parseFloat($('#f-preview .f-group:last-child .pre_input_box').val());
    var next_row1 = parseFloat($('#f-preview .f-group').length);
    var next_row2 =  (next_row1 += 0.1).toFixed(1);
    entry += "<div class='f-group L1 selectedRB' data-level='1'>";
    if (currentLevel == 1) {
        entry += "<input type='checkbox' name='checkbox_level' value='1' class='form-check-input cbox pl_standard_level'>";
        entry += '<input type="hidden" class="pl_standard_label" name="result[' + next_row + '][pl_standard_label]">';
    } else {
        entry += "<input type='checkbox' name='checkbox_level' value='1' class='form-check-input cbox d-none'>";
        entry += '<input type="hidden" class="pl_standard_label" name="result[' + next_row + '][pl_standard_label]">';
    }
    //entry +='<input type="hidden" name="contents['+i+']>';
    entry += '<input type="hidden" class="pre_input_box" name="result[' + next_row + '][prefix_level]" value="' + next_row2 + '">';
    entry += "<div class='f-item pre'>" + next_row2 + "</div>";
    entry += "<div class='f-item content'>"+language_based_content_jsData.new_item_jsdata+"</div>";
    entry += '<input type="hidden" class="content_input_box" name="result[' + next_row + '][contents]" value="new item">';
    entry += '<input type="hidden" class="standard_level" name="result[' + next_row + '][standard_level]" value="1">';

    var total_performance_leve = $('#total_performance_levels').val();
    for (var cnt = 1; cnt <= total_performance_leve; cnt++) {
        entry += '<input type="hidden" class="pl_desc_text_' + cnt + '" name="result[' + next_row + '][pl_desc][' + cnt + ']">';
        entry += '<input type="hidden" class="pl_desc_text_id_' + cnt + '" name="result[' + next_row + '][pl_desc_id][' + cnt + ']">';
    }
    entry += "</div>";
    return entry;
}

function newEntry2() {
    var entry = '';
    currentCheckLevel = $('#current_level_tier').val();
    currentLevel = $('#current_level_checkbox').val();
    for (var i = 1; i <= currentCheckLevel; i++) {
        if (currentCheckLevel == i) {
            entry += "<div class='f-group L" + i + " selectedRB' data-level=" + current_tier + ">";

            if (currentLevel == i) {
                entry += "<input type='checkbox' name='checkbox_level' value='1' class='form-check-input cbox pl_standard_level'>";
                entry += '<input type="hidden" class="pl_standard_label" name="result[' + entry_count + '][' + current_tier + '][pl_standard_label]">';
            } else {
                entry += "<input type='checkbox' name='checkbox_level' value='1' class='form-check-input cbox d-none'>";
            }

            //entry +='<input type="hidden" name="contents['+i+']>';
            entry += '<input type="hidden" class="pre_input_box" name="result[' + entry_count + '][' + current_tier + '][prefix_level]" value="1.' + i + '">';
            entry += "<div class='f-item pre'>1." + i + "</div>";
            entry += "<div class='f-item content'>"+language_based_content_jsData.new_item_jsdata+"</div>";
            entry += '<input type="hidden" class="content_input_box" name="result[' + entry_count + '][' + current_tier + '][contents]" value="new item">';
            entry += '<input type="hidden" class="standard_level" name="result[' + entry_count + '][' + current_tier + '][standard_level]" value="' + i + '">';

            var total_performance_leve = $('#total_performance_levels').val();
            for (var cnt = 1; cnt <= total_performance_leve; cnt++) {
                entry += '<input type="hidden" class="pl_desc_text_' + cnt + '" name="result[' + entry_count + '][' + current_tier + '][pl_desc][' + cnt + ']">';
                entry += '<input type="hidden" class="pl_desc_text_id_' + cnt + '" name="result[' + entry_count + '][' + current_tier + '][pl_desc_id][' + cnt + ']">';
            }
            entry += "</div>";
        } else {
            entry += "<div class='f-group L" + i + "'data-level=" + current_tier + ">";

            if (currentLevel == i) {
                entry += "<input type='checkbox' name='checkbox_level' value='1' class='form-check-input cbox pl_standard_level'>";
                entry += '<input type="hidden" class="pl_standard_label" name="result[' + entry_count + '][' + current_tier + '][pl_standard_label]">';
            } else {
                entry += "<input type='checkbox' name='checkbox_level' value='1' class='form-check-input cbox d-none'>";
            }

            entry += '<input type="hidden" class="pre_input_box" name="result[' + entry_count + '][' + current_tier + '][prefix_level]" value="1.' + i + '">';
            entry += "<div class='f-item pre'>1." + i + "</div>";
            entry += "<div class='f-item content'>"+language_based_content_jsData.new_item_jsdata+"</div>";
            entry += '<input type="hidden" class="content_input_box" name="result[' + entry_count + '][' + current_tier + '][contents]" value="new item">';
            entry += '<input type="hidden" class="standard_level" name="result[' + entry_count + '][' + current_tier + '][standard_level]" value="' + i + '" >';

            var total_performance_leve = $('#total_performance_levels').val();
            for (var cnt = 1; cnt <= total_performance_leve; cnt++) {
                entry += '<input type="hidden" class="pl_desc_text_' + cnt + '" name="result[' + entry_count + '][' + current_tier + '][pl_desc][' + cnt + ']">';
                entry += '<input type="hidden" class="pl_desc_text_id_' + cnt + '" name="result[' + entry_count + '][' + current_tier + '][pl_desc_id][' + cnt + ']">';
            }
            entry += "</div>";
        }
        current_tier++;
        $('#loopCounter').val(current_tier);
    }
    entry_count++;
    $('#chunks_level').val(entry_count);
    return entry;


}

function newEntryPl(total_dives_length) {
    var entry;
    class_txt = 'class="rub_box_dark f-group L' + total_dives_length + ' selectedRB pl-levels"';
    entry = '<div ' + class_txt + ' data-level=' + total_dives_length + ' data-index=' + total_dives_length + '>';
    entry += '<div class="titlecls">';
    entry += '</div>';
    entry += '<div class="f-item pre"><label class="plTitleText">'+language_based_content_jsData.PL_title+' '+total_dives_length+' </label></div>';
    entry += '<input type="hidden" name="title[]" value="PL title '+total_dives_length+' " class="f-item plTitleInput">';
    entry += '<input type="hidden" name="performance_level_ids[]" value="" class="performance_level_ids">'
    entry += '<input type="hidden" name="ratting_standards[]" value=' + total_dives_length + ' class="standards_rattings">';
    entry += '<div class="f-item content pl_contents_show" style="display: none;">';
    entry += '<p class="pl_contents_text">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text</p>';
    entry += '<textarea class="f-item pl_contents_input" style="display: none;" name="description[]">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text</textarea>';
    entry += '</div>';

    return entry;

}

function setHighestLowest() {
    $('.titlecls').html('');
    if ($('#auto_scroll_switch3').is(':checked') == true) {
        $(".titlecls").last().html('<div class="rub_rowleft rub_higest"><span>'+language_based_content_jsData.highest+'</span><div class="rub_line"></div></div>');
        $(".titlecls").first().html('<div class="rub_rowleft rub_lowest"><span>'+language_based_content_jsData.lowest+'</span><div class="rub_line"></div></div>');
    } else {
        $(".titlecls").first().html('<div class="rub_rowleft rub_higest"><span>'+language_based_content_jsData.highest+'</span><div class="rub_line"></div></div>');
        $(".titlecls").last().html('<div class="rub_rowleft rub_lowest"><span>'+language_based_content_jsData.lowest+'</span><div class="rub_line"></div></div>');
    }


}

function resetSelected() {
    $(".f-group").removeClass('selectedRB');
}

function repopulate_pl(whichItem) {
    //alert("dkkddk");
    value = $(whichItem).find('.plTitleText').text();
    $("#itemPrePl").val($.trim(value));

    txt = $(whichItem).find('.pl_contents_text').text();
    $("#itemContentPl").val($.trim(txt.replace(/[\s\n\r]+/g, ' ')));
}

function repopulate(whichItem) {
    value = $(whichItem).find('.pre').text();
    $("#itemPre").val(value);

    txt = $(whichItem).find('.content_1').text();
    if(txt =="" || txt ==false){
        txt = $(whichItem).find('.content').text();
    }
    $("#itemContent").val($.trim(txt));
}

function indent() {
    var dataLevel = currentGroup.attr("data-level");
    var current_level = $('#current_level_tier').val();

    if (dataLevel < current_level) {
        currentGroup.removeClass('L' + dataLevel);
        nextLevel = ++dataLevel;
        currentGroup.addClass('L' + nextLevel);
        if (nextLevel != 0) {
            currentGroup.find('.standard_level').val(nextLevel);
        }
        currentGroup.attr("data-level", nextLevel);
        setCheck(nextLevel, whichLevel());
        enablePerformanceLevelInbox();
    } else {
        //do nothing
    }
}

function outdent() {

    var dataLevel = currentGroup.attr("data-level");
    if (dataLevel == 1) {
        return false;
    }
    if (dataLevel > minLevel) {
        $(currentGroup).removeClass('L' + dataLevel);
        nextLevel = --dataLevel;
        $(currentGroup).addClass('L' + nextLevel);
        currentGroup.find('.standard_level').val(nextLevel);
        currentGroup.attr("data-level", nextLevel);
        setCheck(nextLevel, whichLevel());
        if (nextLevel != 0) {
            currentGroup.find('.standard_level').val(nextLevel);
        }
        enablePerformanceLevelInbox();


    } else {
        //do nothing
    }
}

function selectUp() {
    //if not lowest index change selected
    console.log($(currentGroup).index());
    if ($(currentGroup).index() > 0) {
        newIndex = $(currentGroup).index() - 1;
        console.log(newIndex);
        currentGroup = $(".f-group:eq(" + newIndex + ")");
        resetSelected();
        $(currentGroup).addClass('selectedRB');
        repopulate($(currentGroup));
    } else {
        //do nothing
    }
}

function selectUpPl() {
    //if not lowest index change selectedRB
    console.log($(currentGroup).index());
    if ($(currentGroup).index() > 0) {
        newIndex = $(currentGroup).index() - 1;
        console.log(newIndex);
        currentGroup = $(".f-group:eq(" + newIndex + ")");
        resetSelected();
        $(currentGroup).addClass('selectedRB');
        repopulate_pl($(currentGroup));
    } else {
        //do nothing
    }
}

function selectDownPl() {
    //if not highest index change selectedRB
    console.log($(currentGroup).index());
    console.log($(".f-group").length);
    if ($(currentGroup).index() < $(".f-group").length - 1) {
        newIndex = $(currentGroup).index() + 1;
        currentGroup = $(".f-group:eq(" + newIndex + ")");
        resetSelected();
        $(currentGroup).addClass('selectedRB');
        repopulate_pl($(currentGroup));
    } else {

        //do nothing
    }
}

function selectDown() {
    //if not highest index change selectedRB
    if ($(currentGroup).index() < $(".f-group").length - 1) {
        newIndex = $(currentGroup).index() + 1;
        currentGroup = $(".f-group:eq(" + newIndex + ")");
        resetSelected();
        $(currentGroup).addClass('selectedRB');
        repopulate($(currentGroup));
    } else {

        //do nothing
    }
}


//KEYPRESS CODE
window.document.onkeydown = function (e) {
    e = e || window.event;
    var keyCode = e.keyCode || e.which,
        arrow = {left: 37, up: 38, right: 39, down: 40};

    if (e.ctrlKey) {
        switch (keyCode) {
            case arrow.up: //up arrow

                //if not first item
                selectUp();

                break;

            case arrow.down: // down arrow

                //if not last item
                selectDown();

                break;

            case arrow.left: // left arrow

                //if not lowest tier
                outdent();

                break;

            case arrow.right: // right arrow

                //if not highest tier
                indent();

                break;
        }
    }
};


function resetCheckLevel(currentCheckLevel) {
    $('.cbox').addClass('d-none');
    $('.L' + currentCheckLevel).find('.cbox').removeClass('d-none');
}

function setCheck(nextLevel, whichLevel) {
    if (whichLevel == nextLevel) {
        $(currentGroup).find('.cbox').removeClass('d-none');
        $(currentGroup).find('.cbox').addClass('pl_standard_level');
    } else {
        $(currentGroup).find('.cbox').addClass('d-none');
        $(currentGroup).find('.cbox').removeClass('pl_standard_level');
    }
}

function whichLevel() {
    // return ($('#check-level input:radio:checked').val());
    return ($('#current_level_checkbox').val());
}

