$( window ).load(function() {

    var paths = location.pathname.toLowerCase().split('/');
    while (paths.length > 0 && (paths[0] == '' || paths[0] == 'app')) {
        paths = paths.splice(1);
    }
    if (paths.length == 0 || !(paths[0] == 'users' && paths[1] == 'administrators_groups')) return;

    var storage = 'localStorage' in window && window['localStorage'] !== null ? window['localStorage'] : false;
    var show_tour = 1;

    if (storage) {
        show_tour = storage.getItem('show_tour_' + current_user_id);
        if (show_tour == null) show_tour = 1;
    }

    if (show_tour == 0) return;

    window.tour = new Tour({
        steps: [
            {
                element: $("#owners-container ul.users li:first img"),
                title: "User",
                content: "This is you. You own the account and control everything. " +
                    "Super Users can help you manage the account, so only give this role to people you really trust."
            },
            {
                element: "#super-admins-container ul.users li:first a.icon-locked",
                title: "Super User",
                content: "Click the lock icon to access your user's permission and privileges page. " +
                "To delete this sample user, click the trash can icon on this page."
            },
            {
                element: "#super-admins-container a.btn-green",
                title: "Add Super User",
                content: "Click here to invite a new Super User into your account. Super Users have administrative privileges in your account, " +
                "so only add people as Super Users if you really trust them.",
                placement: "left"
            },
            {
                element: "#admin-container ul.users li:first a.icon-locked",
                title: "Users",
                content: "You can customize what users can and cannot do by clicking the lock icon next to their name. " +
                "Feel free to change their individual privileges on this page."
            },
            {
                element: "#admin-container a.btn-blue",
                title: "Add User",
                content: "Click here to add a new user to your account.",
                placement: "left"
            },
            {
                element: "#grous-list > div.span5:first",
                title: "Group",
                content: "Groups make adding people in your account to Huddles efficient. If you want to quickly create a Huddle with multiple people, " +
                "but do not want to add them individually. You can create a custom group.",
                placement: "top"
            }
        ],
        debug: true,
        backdrop : true,
        onEnd: function(tour) {
            if ($("#tour-off").prop('checked')) {
                storage.setItem('show_tour_' + current_user_id, 0);
            } else {
                storage.setItem('show_tour_' + current_user_id, 1);
            }
        },
        onHide: function(tour) {
            tour.show_tour = $("#tour-off").prop('checked');
        },
        onShown: function(tour) {
            $("#tour-off").prop('checked', !!tour['show_tour']);
        },
        template: '<div class="popover" role="tooltip"> <div class="arrow"></div> <h3 class="popover-title"></h3> <div class="popover-content"></div> <div class="popover-navigation"> <div class="btn-group"> <button class="btn btn-sm btn-blue" data-role="prev">&laquo; Prev</button> <button class="btn btn-sm btn-blue" data-role="next">Next &raquo;</button> <button class="btn btn-sm btn-green" data-role="pause-resume" data-pause-text="Pause" data-resume-text="Resume">Pause</button> <button class="btn btn-sm btn-green" data-role="end">End tour</button> </div> <div class="btn-group"> <input type="checkbox" id="tour-off"> <label for="tour-off">Do not show tour next time</label> </div></div> </div>',
    });

    // Initialize the tour
    tour.init();

    // Start the tour
    tour.restart();

});


