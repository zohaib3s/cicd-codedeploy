

(function($) {
    jQuery.expr[':'].Contains = function(a, i, m) {
        return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
    };

    function listFilter(header, list) {
        var form = $("<div>").attr({"class": "filterform"}),
            input = $("<input id='input-filter' placeholder='Search people or group'><div  id='cancel-btn' type='text' style=' width: 10px;  position: absolute; right: 93px; margin-top: 7px; display:none;cursor: pointer; '>X</div>").attr({"class": "filterinput", "type": "text"});
        $(form).append(input).appendTo(header);
        $('.filterform input').css({'width': '233px', 'float': 'right'});
        $(input).change(function() {
            var filter = $(this).val();
            if (filter) {
                $search_count = $(list).find("a:Contains(" + filter + ")").length;
                if ($search_count > 4) {
                    $('div.thumb').css('top', '0px');
                    $('div.overview').css('top', '0px');
                    $('.scrollbar').css('display', 'block');
                } else {
                    $('div.thumb').css('top', '0px');
                    $('div.overview').css('top', '0px');
                    $('.scrollbar').css('display', 'none');
                }
                $(list).find("a:not(:Contains(" + filter + "))").parent().parent().slideUp(400, function() {
                    $('.widget-scrollable').tinyscrollbar_update();
                });
                $(list).find("a:Contains(" + filter + ")").parent().parent().slideDown(400, function() {
                    $('.widget-scrollable').tinyscrollbar_update();
                });
                jQuery('#cancel-btn').css('display', 'block');
                if ($(list).find("a:Contains(" + filter + ")").length == 0) {
                    $('#liNodataFound').remove();
                    $("#list-containers").append('<li id="liNodataFound">No people or groups match this search. Please try again.</li>');
                } else {
                    $('#liNodataFound').remove();
                }

            } else {
                jQuery('#cancel-btn').css('display', 'none');
                $('.scrollbar').css('display', 'block');
                $(list).find("li").slideDown(400, function() {
                    $('.widget-scrollable').tinyscrollbar_update();
                });

            }
            return false;
        }).keyup(function() {
            $(this).change();
        });

        $('#cancel-btn').click(function(e) {
            jQuery('#cancel-btn').css('display', 'none');
            $('div.thumb').css('top', '0px');
            $('div.overview').css('top', '0px');
            $('.scrollbar').css('display', 'block');
            jQuery('#input-filter').val('');
            $(list).find("li").slideDown(400, function() {
                $('.widget-scrollable').tinyscrollbar_update();
            });
        });
    }

    function beforeHuddleAdd() {
        console.log('before huddle add ' + (new Date()).getTime());
        var video_huddle_name = $("#video_huddle_name");
        if ( video_huddle_name.val().trim().length > 0 ) {
            $('#btnSaveHuddle').attr('disabled', true);
            $('#new_video_huddle').submit();
        } else {
            video_huddle_name.addClass('error');
            video_huddle_name.next().css('display', 'block');
        }
        console.log('before huddle add ' + (new Date()).getTime());
        return false;
    }

    function is_email_exists(email) {

        for (var i = 0; i < posted_data.length; i++) {

            var posted_user = posted_data[i];
            if (posted_user[1] == email)
                return true;
        }

        return false;
    }

    function AddToAccount() {

        var lposted_data = [];
        var userNames = document.getElementsByName('users[][name]');
        var userEmails = document.getElementsByName('users[][email]');
        var huddleType = document.getElementsByName('huddle_type')[0].value;
        
        for (i = 0; i < userNames.length; i++) {

            var userName = $(userNames[i]);
            var userEmail = $(userEmails[i]);
            if (is_email_exists(userEmail.val())) {
                alert('A new user with this email is already added.');
                return false;
            }
        }

        for (i = 0; i < userNames.length; i++) {

            var userName = $(userNames[i]);
            var userEmail = $(userEmails[i]);
            if (userName.val() != '' || userEmail.val() != '') {

                if (userName.val() == '') {
                    alert('Please enter a valid Full Name.');
                    userName.focus();
                    return false;
                }

                if (userEmail.val() == '') {
                    alert('Please enter a valid Email.');
                    userEmail.focus();
                    return false;
                }

                if (is_email_exists(userEmail.val())) {
                    alert('A new user with this email is already added.');
                    return false;
                }


                var newUser = [];
                newUser[newUser.length] = userName.val();
                newUser[newUser.length] = userEmail.val();
                lposted_data[lposted_data.length] = newUser;
            }
        }

        if (lposted_data.length == 0) {
            alert('Please enter atleast one Full name or email.');
            return;
        }

        $.ajax({
            type: 'POST',
            data: {
                user_data: lposted_data
            },
            url: home_url + '/Huddles/verifyNewUsers',
            success: function (response) {
                if (response != '') {
                    $('#flashMessage2').css('display', 'block');
                    $('#flashMessage2').html(response);
                } else {

                    $('#flashMessage2').css('display', 'none');
                    new_ids -= 1;
                    for (var i = 0; i < lposted_data.length; i++) {

                        var data = lposted_data[i];
                        var html = '';
                        html += '<li>';
                        html += '<label class="huddle_permission_editor_row" for="super_admin_ids_' + new_ids + '"><input type="checkbox" value="' + new_ids + '" name="super_admin_ids[]" checked="checked" id="super_admin_ids_' + new_ids + '" style="display:none;"><a style="color: #757575; font-weight: normal;">' + data[0] + '</a> </label>';
                        html += '<input type="hidden" value="' + data[0] + '" name="super_admin_fullname_' + new_ids + '" id="super_admin_fullname_' + new_ids + '">';
                        html += '<input type="hidden" value="' + data[1] + '" name="super_admin_email_' + new_ids + '" id="super_admin_email_' + new_ids + '">';
                        
                        if(huddleType == 2){
                            html += '<div class="permissions coach_mentee">';    
                        }else{
                            html += '<div class="permissions coach_mentee" style="display:none;">';
                        }
                        
                        html += '<span class="caoch-checkbox" style="margin-right: 5px;">';
                        html += '<input type="radio" id="caoch-checkbox_'+new_ids+'" value="1" name="coach_'+new_ids+'"><label for="caoch-checkbox_'+new_ids+'" class="cls_sp_label">Coach</label></span>';
                        html += '<span class="mentee-checkbox"><input type="radio" value="2" id="mentee-checkbox_'+new_ids+'" name="coach_'+new_ids+'"><label for="mentee-checkbox_'+new_ids+'" class="cls_sp_label">Coachee</label></span>';
                        html += '</div>';
                        if(huddleType == 2){
                            html += '<div class="permissions" style="display:none;">';
                        }else{
                            html += '<div class="permissions">';
                        }
                        html += '<label for="user_role_' + new_ids + '_200"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_200" value="200">Admin</label>';
                        html += '<label for="user_role_' + new_ids + '_210"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_210" value="210" checked="checked">Member</label>';
                        html += '<label for="user_role_' + new_ids + '_220"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_220" value="220">Viewer</label>';
                        html += '</div>';
                        html += '<span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;" for="123"><input type="checkbox" value="' + new_ids + '" id="new_checkbox_' + new_ids + '"  name="" class="huddle_edit_cls_chkbx"><label for="new_checkbox_' + new_ids + '" class="cls_sp_label">Huddle Participant</label></span>';
                        html += '</li>';
                        $('.groups-table .huddle-span4 .groups-table-content ul').append(html);
                        new_ids -= 1;
                        var newUser = [];
                        newUser[newUser.length] = data[0];
                        newUser[newUser.length] = data[1];
                        posted_data[posted_data.length] = newUser;
                    }

                    var $overview = $('.widget-scrollable.horizontal .overview'),
                            overviewWidth = 0;
                    $.each($overview, function () {

                        var width = $.map($(this).children(), function (child) {
                            return $(child).outerWidth() +
                                    $(child).pixels('margin-left') + $(child).pixels('margin-right');
                        });
                        $(this).width($.sum(width));
                    });

                    $('.widget-scrollable').tinyscrollbar();
                    $('#addSuperAdminModal').modal('hide');
                }
            },
            errors: function (response) {
                alert(response.contents);
            }

        });
    }

    function loadUninvitedUserList(page) {
        if (page == undefined) page = 0;
        var huddle_id = $('input[name=huddle_id]').val();
        var keyword = $.trim( $('.uninvited-users [name=filter]').val() );
        var ignoreList = [];
        $('.invited-users .user-list input:checkbox').each(function(idx, el) {
            ignoreList.push( el.value );
        });
        if (keyword.length) {
            $('.uninvited-users .cancel-btn').show();
        } else {
            $('.uninvited-users .cancel-btn').hide();
        }

        $.ajax({
            type: "POST",
            data : {
                keywords : keyword,
                limit : user_number_limit,
                page : page,
                ignore : ignoreList.join(',')
            },
            dataType: 'json',
            url : home_url + '/Huddles/ajax_get_uninvited_users/' + huddle_id,
            success: function(res) {
                console.log(res.total);
                if (res.total > 0 && $.trim(res.html).length == 0) {
                    loadUninvitedUserList(0);
                }
                $('.uninvited-users .user-list').html( res.html );
                updateUserPager( res.total, page, loadUninvitedUserList );
                selectUser( $('.uninvited-users .user-list input:checkbox') );
            },
            errors: function(res) {
                alert(res.contents);
            }
        });
    }

    function loadUninvitedGroupList(page) {
        if (page == undefined) page = 0;
        var huddle_id = $('input[name=huddle_id]').val();
        var keyword = $.trim( $('.uninvited-groups [name=filter]').val() );
        var ignoreList = [];
        $('.invited-groups .group-list input:checkbox').each(function(idx, el) {
            ignoreList.push( el.value );
        });
        if (keyword.length) {
            $('.uninvited-groups .cancel-btn').show();
        } else {
            $('.uninvited-groups .cancel-btn').hide();
        }

        $.ajax({
            type: "POST",
            data : {
                keywords : keyword,
                limit : group_number_limit,
                page : page,
                ignore : ignoreList.join(',')
            },
            dataType: 'json',
            url : home_url + '/Huddles/ajax_get_uninvited_groups/' + huddle_id,
            success: function(res) {
                console.log(res.total);
                if (res.total > 0 && $.trim(res.html).length == 0) {
                    loadUninvitedGroupList(0);
                }
                $('.uninvited-groups .group-list').html( res.html );
                updateGroupPager( res.total, page, loadUninvitedGroupList );
                selectGroup( $('.uninvited-groups .group-list input:checkbox') );
            },
            errors: function(res) {
                alert(res.contents);
            }
        });
    }

    function updateUserPager(total, current) {
        var n = Math.ceil(total / user_number_limit);
        if ( n <= 1 ) {
            $('.uninvited-users .pagination').html('');
            $('.invited-users .pagination, .uninvited-users .pagination').addClass('hidden');
            return;
        }
        var s = 0, e = n - 1;
        if (n > 5) {
            if (current - 2 < 0) {
                s = 0;
                e = 5;
            }
            else if (current + 2 >= n) {
                e = n - 1;
                s = n - 6;
            }
            else {
                s = current - 2;
                e = current + 2;
            }
        }
        var html = $('<ul></ul>'), li;
        for(var i = s; i <= e; i++) {
            li = $("<li>" + i + "</li>");
            if (i == current) li.addClass('current');
            li.attr('data-index', i);
            html.append(li);
            li.click(function() {
                loadUninvitedUserList( $(this).attr('data-index') );
            });
        }

        li = $("<li> > </li>");
        li.attr('data-index', current + 1);
        if (current + 1 < n) {
            li.click(function() {
                loadUninvitedUserList( $(this).attr('data-index') );
            });
            li.addClass('active');
        } else {
            li.addClass('inactive');
        }
        html.append( li );

        li = $("<li> < </li>");
        li.attr('data-index', current - 1);
        if (current - 1 >= 0) {
            li.click(function() {
                loadUninvitedUserList( $(this).attr('data-index') );
            });
            li.addClass('active');
        } else {
            li.addClass('inactive');
        }
        html.prepend( li );

        $('.uninvited-users .pagination').html( html );
        $('.invited-users .pagination, .uninvited-users .pagination').removeClass('hidden');
    }

    function updateGroupPager(total, current) {
        var n = Math.ceil(total / group_number_limit);
        if ( n <= 1 ) {
            $('.uninvited-groups .pagination').html('');
            $('.invited-groups .pagination, .uninvited-groups .pagination').addClass('hidden');
            return;
        }
        var s = 0, e = n - 1;
        if (n > 5) {
            if (current - 2 < 0) {
                s = 0;
                e = 5;
            }
            else if (current + 2 >= n) {
                e = n - 1;
                s = n - 6;
            }
            else {
                s = current - 2;
                e = current + 2;
            }
        }
        var html = $('<ul></ul>'), li;
        for(var i = s; i <= e; i++) {
            li = $("<li>" + i + "</li>");
            if (i == current) li.addClass('current');
            li.attr('data-index', i);
            html.append(li);
            li.click(function() {
                loadUninvitedGroupList( $(this).attr('data-index') );
            });
        }

        li = $("<li> > </li>");
        li.attr('data-index', current + 1);
        if (current + 1 < n) {
            li.click(function() {
                loadUninvitedGroupList( $(this).attr('data-index') );
            });
            li.addClass('active');
        } else {
            li.addClass('inactive');
        }
        html.append( li );

        li = $("<li> < </li>");
        li.attr('data-index', current - 1);
        if (current - 1 >= 0) {
            li.click(function() {
                loadUninvitedGroupList( $(this).attr('data-index') );
            });
            li.addClass('active');
        } else {
            li.addClass('inactive');
        }
        html.prepend( li );

        $('.uninvited-groups .pagination').html( html );
        $('.invited-groups .pagination, .uninvited-groups .pagination').removeClass('hidden');
    }

    function selectUser(list) {
        list.change(function() {
            var self = $(this);
            var li = self.closest('li');
            if ( self.prop('checked') ) {
                li.appendTo( $('.invited-users .user-list ul') );
                li.find('input.user-info').attr('name', 'selected_user[]');
                li.find('select').attr('name', 'selected_user_role[]');
            }
            else {
                li.appendTo( $('.uninvited-users .user-list ul') );
                li.find('input.user-info').attr('name', 'unselected_user[]');
                li.find('select').attr('name', 'unselected_user_role[]');
            }
        });
    }

    function selectGroup(list) {
        list.change(function(){
            var self = $(this);
            var li = self.closest('li');
            if ( self.prop('checked') ) {
                li.appendTo( $('.invited-groups .group-list ul') );
                li.find('input.group-info').attr('name', 'selected_group[]');
                li.find('select').attr('name', 'selected_group_role[]');
            }
            else {
                li.appendTo( $('.uninvited-groups .group-list ul') );
                li.find('input.group-info').attr('name', 'unselected_group[]');
                li.find('select').attr('name', 'unselected_group_role[]');
            }
        });
    }

    function initSearchUninvitedUser() {
        $('.uninvited-users .filterinput').keypress(function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                loadUninvitedUserList();
                $(this).blur();
                e.preventDefault();
                return false;
            }
        });

        $('.uninvited-users .cancel-btn').click(function() {
            $('.uninvited-users .filterinput').val('');
            loadUninvitedUserList();
        })
    }

    function initSearchUninvitedGroup() {
        $('.uninvited-groups .filterinput').keypress(function(e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                loadUninvitedGroupList();
                $(this).blur();
                e.preventDefault();
                return false;
            }
        });

        $('.uninvited-groups .cancel-btn').click(function() {
            $('.uninvited-groups .filterinput').val('');
            loadUninvitedGroupList();
        })
    }

    function initTabAction() {
        $(".toolbar input:radio.filter-by").change(function() {
           if ( 'user' == $(this).val()) {
               $(".uninvited-users").removeClass('hidden');
               $(".invited-users").removeClass('hidden');
               $(".uninvited-groups").addClass('hidden');
               $(".invited-groups").addClass('hidden');
           } else {
               $(".uninvited-users").addClass('hidden');
               $(".invited-users").addClass('hidden');
               $(".uninvited-groups").removeClass('hidden');
               $(".invited-groups").removeClass('hidden');
           }
        });
    }

    var posted_data = [];
    var user_number_limit = 20;
    var group_number_limit = 20;

    $(function() {
        loadUninvitedUserList();
        loadUninvitedGroupList();

        initSearchUninvitedUser();
        initSearchUninvitedGroup();

        selectUser( $('.invited-users .user-list input:checkbox') );
        selectGroup( $('.invited-groups .group-list input:checkbox') );

        initTabAction();
        //listFilter( $("#header-container"), $("#list-containers") );

        $('#select-all-none').click(function() {
            if ($('#select-all').is(':checked')) {
                $('#select-all-label').html('Select None');
                $('#select-all').prop('checked', true);
                $(".member-user").prop('checked', true);
                $(".super-user").prop('checked', true);
                $(".viewer-user").prop('checked', true);

            } else {
                $('#select-all-label').html('Select All');
                $('#select-all').prop('checked', false);
                $(".member-user").prop('checked', false);
                $(".super-user").prop('checked', false);
                $(".viewer-user").prop('checked', false);
            }

        });

        $("#admin-radio").click(function() {
            if ($(this).is(':checked')) {
                $(".member-btn").removeAttr("checked");
                $(".viewer-btn").removeAttr("checked");
                $(".admin-btn").prop('checked', true);
            }
        });

        $("#member-radio").click(function() {
            if ($(this).is(':checked')) {
                $(".admin-btn").removeAttr("checked");
                $(".viewer-btn").removeAttr("checked");
                $(".member-btn").prop('checked', true);
            }
        });

        $("#viewers-radio").click(function() {
            if ($(this).is(':checked')) {
                $(".admin-btn").removeAttr("checked");
                $(".member-btn").removeAttr("checked");
                $(".viewer-btn").prop('checked', true);
            }

        });

        $('#btnAddToAccount').click(function() {
            AddToAccount();
        });

        $('#btnSaveHuddle').click(beforeHuddleAdd);

        $('#video_huddle_name').keypress(function(e) {
            if (e.keyCode == 13) {
                $(this).closest('form').find('textarea').eq(0).focus();
                return false;
            }
            $(this).removeClass('error').next().hide();
        });
    });
}(jQuery));

