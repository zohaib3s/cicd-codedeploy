var gApp;
var isCurrentVideoPlaying = false;

$(function($) {

    $.fn.pixels = function(property) {
        return parseInt(this.css(property).slice(0, -2));
    };

    $.sum = function(arr) {
        var r = 0;
        $.each(arr, function(i, v) {
            r += v;
        });
        return r;
    };



    var App = {
        popoverTimer: null

                ,
        init: function() {

            $('[rel~="tooltip"]').tooltip({container: 'body'});
            $(".widget-scrollable").tinyscrollbar();

            this.editorItems = []
            this.commentMore();
            this.popover();
            this.placeholder();
            this.checkbox($('.checkbox'));
            this.radio($('.radio'));
            this.autoadd();
            this.checkAll();
            this.addClone();
            this.close();
            this.removeParentParent();
            this.carousel();
            this.synchro();
            this.contentAside();
            this.invitationBox();
            this.editors();
            this.updateCheckboxEvent($('.ui-checkbox input'));
            this.initAntonationEmptyHuddle();
            this.initUserSearchInGroup();

            var $overview = $('.widget-scrollable.horizontal .overview');

            $.each($overview, function() {
                var width = $.map($(this).children(), function(child) {
                    return $(child).outerWidth() + $(child).pixels('margin-left') + $(child).pixels('margin-right');
                });
                $(this).width($.sum(width));
            });
        },

        updateCheckboxEvent: function(els) {
            els.on({
                'change': function() {
                    $(this).parent()[$(this).prop('checked') ? 'addClass' : 'removeClass']('active');
                },
                'focus blur': function(e) {
                    $(this).parent()[e.type == 'focus' ? 'addClass' : 'removeClass']('focused');
                }
            }).trigger('change');
        },

        upload: function() {
            $('.upload-toggle').on('click', function(e) {
                e.preventDefault();
                $(this).next().trigger('click');
            });
            $('.upload-toggle').next().imageLoader({
                'show': '.upload-preview',
                'width': '150px'
            });
        } // upload

        ,
        colorPicker: function() {

            var RGBtoHEX = function(color) {
                return "#" + $.map(color.match(/\b(\d+)\b/g), function(digit) {
                    return ('0' + parseInt(digit).toString(16)).slice(-2);
                }).join('');
            };

            $('.color-preview').each(function() {
                var $el = $(this);
                $el.ColorPicker({
                    color: RGBtoHEX($el.css('backgroundColor')),
                    onChange: function(hsb, hex) {
                        $el.val(hex).css('background-color', '#' + hex);
                    },
                    onSubmit: function(hsb, hex, rgb, el) {
                        $el.val(hex).css('background-color', '#' + hex).ColorPickerHide();
                    },
                    onBeforeShow: function() {
                        $(this).ColorPickerSetColor(this.value);
                    }
                });
            });

        } // colorPicker

        ,
        appendix: function($el) {

            $el.each(function() {

                var $this = $(this),
                        $content = $this.next('.appendix-content'),
                        offset = null,
                        position,
                        parentWidth,
                        leftAligned;

                $this.on('click', function(e) {

                    position = $this.position();
                    parentWidth = $this.offsetParent().width();
                    leftAligned = position.left < parentWidth / 2;

                    if (leftAligned)
                        $content.addClass('left-aligned');

                    if (offset == null) {

                        offset = $content.hasClass('down') ? {
                            top: position.top + 40,
                            left: position.left - (leftAligned ? 0 : $content.outerWidth() + 40) - $this.width() / 2
                        } : offset = $content.hasClass('card') ? {
                            top: position.top - $content.outerHeight() - 20,
                            left: position.left - (leftAligned ? 0 : $content.outerWidth() + 40) - $this.width() / 2
                        } : {
                            top: position.top - $content.outerHeight() - 20,
                            left: position.left - $content.outerWidth() / 2 + 10

                        };

                        $content.offset(offset)

                    }

                    //$content.stop()[e.type == 'mouseout' ? 'fadeOut' : 'fadeIn']('fast');
                    e.preventDefault();
                    $(".appendix-content").not($content).css('display', 'none');
                    $content.stop().fadeToggle('fast');

                });

            });

        } // appendix

        ,
        commentMore: function() {

            $('.comment-more-trigger').click(function(event) {

                event.preventDefault()

                var $more = $(this);
                var $scrollbar = $(this).parents(".widget-scrollable");

                $more.prev().fadeToggle(function() {
                    $more.html($(this).is(':visible') ? '&hellip; less' : '&hellip; more');

                    if ($scrollbar.length > 0) {
                        $scrollbar.tinyscrollbar_update();
                    }
                });

            });

        } // commentMore

        ,
        popover: function() {

            $('[rel="image-uploader"]').each(function() {

                var $this = $(this),
                        $popup = $this.prev(),
                        offset = $this.offset();

                //console.log(offset);
                offset.left += $this.width() + 10;
                console.log(offset.left);
                offset.left = offset.left - 120.5;
                offset.top = 12;

                $popup.css('left', $this.width() + 10);

                $this.hover(function() {
                    $popup.show();
                });

                $popup.find('.cancel').on('click', function(e) {
                    e.preventDefault();
                    $popup.hide();
                });

            });



        } // popover

        ,
        placeholder: function() {

            if (!('placeholder' in document.createElement('input'))) {

                $('[placeholder]').focus(function() {
                    var input = $(this);
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                        input.removeClass('placeholder');
                    }
                }).blur(function() {
                    var input = $(this);
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.addClass('placeholder');
                        input.val(input.attr('placeholder'));
                    }
                }).blur();
                $('[placeholder]').parents('form').submit(function() {
                    $(this).find('[placeholder]').each(function() {
                        var input = $(this);
                        if (input.val() == input.attr('placeholder')) {
                            input.val('');
                        }
                    })
                });
            }
        } // placeholder

        ,
        checkbox: function($this) {

            $this.each(function() {

                var $checkbox = $(this),
                        $substitute = $('<span/>').addClass('checkbox-substitute');

                $checkbox.wrap($substitute);

                $substitute = $checkbox.parent();

                $substitute[$checkbox.is(':checked') ? 'addClass' : 'removeClass']('checked');

                $checkbox.on('click change', function() {
                    $(this).parent()[$checkbox.is(':checked') ? 'addClass' : 'removeClass']('checked');
                })

            });

        }

        ,
        radio: function($this) {
            $this.each(function() {
                var $radio = $(this),
                    $substitute = $('<span/>').addClass('radio-substitute');
                $radio.wrap($substitute);
                $substitute = $radio.parent();
                $substitute[$radio.is(':checked') ? 'addClass' : 'removeClass']('checked');
                $radio.on('click', function() {
                    $('.radio[name="' + $radio.attr('name') + '"]').parent().removeClass('checked');
                    $(this).parent()[$radio.is(':checked') ? 'addClass' : 'removeClass']('checked');
                });
            });
        }

        ,
        isEmail: function(str) {
            pattern = new RegExp(/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/i);
            return pattern.test(str);
        }

        ,
        autoadd: function() {
            var self = this;

            $(".autoadd").delegate("input", "blur", function() {
                var type = $(this).attr('type'),
                        val = $(this).val();

                $(this).parent().removeClass('valid').removeClass('invalid').addClass(
                        type == "text" && val.length > 0 ? 'valid' :
                        type == "email" && self.isEmail(val) ? 'valid' : 'invalid'
                        );
            }).delegate("li:last-child input", "focus", function() {
                var clone = $(this).closest('li').clone();
                $(this).closest('.autoadd').append(clone);
            }).delegate("input", "focus", function() {
                $(this).parent().addClass('active');
            }).delegate(".close", "click", function(event) {
                event.preventDefault();
                if ($(this).closest('.autoadd').children().length > 1) {
                    $(this).closest('li').fadeOut("fast", function() {
                        $(this).remove();
                    });
                }
            });
        }

        ,
        checkAll: function() {

            $('.check-parent-next, .uncheck-parent-next').on('click', function(event) {
                event.preventDefault();
                var $checkboxes = $(this).parent().next().find('input[type="checkbox"]').prop('checked', false).parent().removeClass('checked');
                if ($(this).hasClass('check-parent-next'))
                    $checkboxes.children().prop('checked', true).parent().addClass('checked');
            });

        }

        ,
        addClone: function() {

            $('.clone-parent-prev').each(function() {
                var $clone = $(this).parent().prev().clone();

                $(this).on('click', function(event) {
                    event.preventDefault();
                    $clone.clone().insertBefore($(this));
                });

            });

        }

        ,
        close: function() {

            $('.close-parent').on('click', function(event) {
                event.preventDefault();
                $(this).parent().fadeOut();
            });

        }

        ,
        removeParentParent: function() {

            $('body').delegate('.remove-parent-parent', 'click', function(event) {
                event.preventDefault();
                $(this).parent().parent().fadeOut(function() {
                    $(this).remove();
                });
            });

        }
        ,carousel: function() {

            $('.video-list__items-wrap').jcarousel();
            $('.video-list__left').jcarouselControl({ target: '-=7' });
            $('.video-list__right').jcarouselControl({ target: '+=7'  });
            $('.video-list__toggle').on('click', function(event) {
                event.preventDefault();
                var $videoList = $('.video-list');

                $videoList[ $videoList.hasClass('open') ? 'removeClass' : 'addClass' ]('open');
            });

            $('.jcarousel').jcarousel();
            $('.jcarousel-prev').jcarouselControl({ target: '-=1' });
            $('.jcarousel-next').jcarouselControl({ target: '+=1'  });

        }

        ,
        synchro: function() {

            $('.synchro').each(function() {
                var $this = $(this),
                        $replies = $this.find('li.comment'),
                        hasReplies = $replies.length;
                (hasReplies ? $replies.last() : $this).find('.comment-date').addClass('last-reply');
            });

        }

        ,
        contentAside: function() {

            $('.content-aside').each(function() {
                var height = $(this).closest('.row-fluid').height();
                $(this).css('min-height', height + 30);
            });

            var relAside = setInterval(function() {
                $('.rel').each(function() {
                    var $this = $(this),
                            height = $this.height(),
                            rel = $this.attr('rel');
                    height += $this.find('.full-rel').length ? 30 : 0;
                    $('#rel-' + rel).height(height);
                });
            }, 10);

        }

        ,
        substitutions: function() {

            $('.styled-input-group input, .styled-input-group input').each(function() {
                var $this = $(this);
                if ($this.next('.substitution').length > 0) {
                    $this.after($('<span/>').addClass('substitution').addClass($this.attr('type')));
                }
            });

        }

        ,
        invitationBox: function() {

            $('.new-invitation-box').map(function() {
                $(this).height($(this).prev().outerHeight());
            });

        }

        ,
        editors: function() {

            $.each($('[id*="editor-"]'), $.proxy(function(index, value) {
                var $this = $(value),
                        editor = $this.attr('id'),
                        id = editor.split('-')[1],
                        toolbar = "editor" + id + "-toolbar";                       
                var textArea = new wysihtml5.Editor(editor, {
                    stylesheets: [home_url + "/css/editor.css"],
                    toolbar: toolbar,
                    autoLink: false,
                    parserRules: wysihtml5ParserRules
                });

                /*textArea.on("load", function() {
                    var editor_value = $('<div/>').html(value).text();
                    //if(editor_value) 
                    //textArea.setValue(editor_value);
                });*/

                textArea.on("paste", function(e){
                    
                    // cancel paste                     
                    e.preventDefault(); // if not possible, maybe execute 'undo' command?

                    // grab it from clipboard
                    var text = e.clipboardData.getData("text/plain");

                    alert(e.clipboardData.getData("text/html"));

                    // insert as plain text
                    textArea.composer.commands.exec("insertHTML", text);
                });

                this.editorItems[this.editorItems.length] = textArea;
            }, this));

        }

        ,
        disscussion: function() {

            var $replyForm = $('#disscussion-reply-form');

            $('.disscussion .comment-reply').on('click', function(e) {
                e.preventDefault();
                $replyForm.hide().insertAfter($(this).closest('.comment').find('> .comment-footer')).fadeIn();
                $replyForm.find('#disscussion_comment_ID').val($replyForm.closest('.comment').attr('id').split('-')[1]);
            });

            $('.js-close-disscussion-reply').on('click', function(e) {
                e.preventDefault()
                $(this).parent().parent().fadeOut();
            });

        },

        initAntonationEmptyHuddle : function() {
            var annotation = $("#huddle-listings .btn-annotation");
            if (annotation.length > 0) {
                var main = $('#main');
                var btn  = $('#btn-new-huddle');
                annotation.css('top', btn.offset().top - main.offset().top + btn.outerHeight() + 10);
            }
        },

        initUserSearchInGroup: function() {
            var MAX_USER_PAGING = 10;
            var filterUserList = function(container, filter, page, size) {
                if (size == undefined) size = MAX_USER_PAGING;
                container.find("span:not(:Contains(" + filter + "))").closest('p').slideUp();

                var visibleList = container.find("span:Contains(" + filter + ")").closest('p');
                var max = page * size;
                var min = max - size;

                visibleList.each(function(idx, el) {
                    if (min <= idx && idx < max) {
                        $(el).slideDown();
                    } else {
                        $(el).slideUp();
                    }
                });

                return visibleList.length;
            };
            var suserSearchInp = $('#suser-search');
            var suserSearchCancelBtn = $('#suser-search-cancel-btn');
            var suserPagination = $('#suser-pagination');
            var suserChangeTimeout = 0;

            suserSearchInp.change(function() {
                var filter = this.value;
                var list   = $('#susers');
                var length = filterUserList(list, filter, 1);
                var maxPage = Math.ceil(length / MAX_USER_PAGING);
                if (maxPage > 1) {
                    suserPagination.show();
                    suserPagination.jqPagination('option', 'current_page', 1);
                    suserPagination.jqPagination('option', 'max_page', maxPage);
                } else {
                    suserPagination.hide();
                }

                if (filter.length > 0) {
                    suserSearchCancelBtn.removeClass('hidden');
                } else {
                    suserSearchCancelBtn.addClass('hidden');
                }
            }).keyup(function() {
                clearTimeout(suserChangeTimeout);
                suserChangeTimeout = setTimeout(function() { suserSearchInp.change(); }, 600);
            });

            suserSearchCancelBtn.click(function() {
                suserSearchInp.val('').change();
            });

            if (suserPagination.length > 0) {
                suserPagination.jqPagination({
                    max_page: Math.ceil( $('#susers p').length / MAX_USER_PAGING ),
                    paged: function(page) {
                        var filter = suserSearchInp.val();
                        var list   = $('#susers');
                        filterUserList(list, filter, page);
                    }
                });
            }

            var userSearchInp = $('#user-search');
            var userSearchCancelBtn = $('#user-search-cancel-btn');
            var userPagination = $('#user-pagination');
            var userChangeTimeout = 0;

            userSearchInp.change(function() {
                var filter = this.value;
                var list   = $('#users');
                var length = filterUserList(list, filter, 1);
                var maxPage = Math.ceil(length / MAX_USER_PAGING);
                if (maxPage > 1) {
                    userPagination.show();
                    userPagination.jqPagination('option', 'current_page', 1);
                    userPagination.jqPagination('option', 'max_page', maxPage);
                } else {
                    userPagination.hide();
                }

                if (filter.length > 0) {
                    userSearchCancelBtn.removeClass('hidden');
                } else {
                    userSearchCancelBtn.addClass('hidden');
                }
            }).keyup(function() {
                clearTimeout(userChangeTimeout);
                userChangeTimeout = setTimeout(function() { userSearchInp.change(); }, 600);
            });

            userSearchCancelBtn.click(function() {
                userSearchInp.val('').change();
            });

            if (userPagination.length > 0) {
                userPagination.jqPagination({
                    max_page : Math.ceil($('#users p').length / MAX_USER_PAGING),
                    paged: function(page) {
                        var filter = userSearchInp.val();
                        var list   = $('#users');
                        filterUserList(list, filter, page);
                    }
                });
            }

            var length;
            length = filterUserList($('#susers'), suserSearchInp.val(), 1);
            if (Math.ceil(length/MAX_USER_PAGING) <= 1) {
                suserPagination.hide();
            }

            length = filterUserList($('#users'), userSearchInp.val(), 1);
            if (Math.ceil(length/MAX_USER_PAGING) <= 1) {
                userPagination.hide();
            }
        }

    };

    $(document).ready(function() {

        gApp = App;
        App.init();
        //Video.init();
        //gApp.Video = Video;
        //console.log("loading script.js");
    });


});

$(window).load(function() {
    if ($('.widget-scrollable').length > 0) {
        $('.widget-scrollable').tinyscrollbar_update();
    }
});
