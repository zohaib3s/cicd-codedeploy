import { Component, OnChanges, Input, Output, EventEmitter, ChangeDetectorRef, ElementRef } from '@angular/core';
import * as videojs from "video.js";
import { PlayerService } from "../services/player.service";
import { trigger, style, animate, transition } from '@angular/animations';
import * as _ from "underscore";
import { MainService } from "../services/main.service";
import { HeaderService } from "../header.service";

@Component({
  selector: 'video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':leave', [
          style({transform: 'translateY(0)', opacity: 1}),
          animate('500ms', style({transform: 'translateY(100%)', opacity: 0}))
        ])
      ]
    )
  ]
})
export class VideoPlayerComponent implements OnChanges {

@Input('src') src;
@Input("VideoInfo") VideoInfo;
@Output("VideoCurrentTime") VideoCurrentTime:EventEmitter<any> = new EventEmitter<any>();
public player;
public header_data;
public translation;
@Input("ShowInfo") ShowInfo;
@Output() ShowInfoBack = new EventEmitter<any>();
@Output() IsPlaying: EventEmitter<any> = new EventEmitter<any>();
@Input() comments;
@Input() colors;
@Input() customMarkers;
@Input() params;
  constructor(private headerService:HeaderService, private mainService:MainService, private cd: ChangeDetectorRef, private playerService:PlayerService) { }

  ngOnInit() {

    this.header_data = this.headerService.getStaticHeaderData();
    this.translation = this.header_data.language_translation;
  	this.RunSubscribers();
    // this.ShowInfo = false;
  }

  private RunSubscribers(){

    let that = this;

    this.mainService.CommentAddedNotification.subscribe((comment)=>{

      this.AddCue(comment);

    });

    this.mainService.ReRenderMarkers.subscribe((flag)=>{
      
      this.player.markers.removeAll();
      this.AddCues();

    });

  	this.playerService.PlayerPlayingState.subscribe((s)=>{

  		this.PlayPause(s);

  	});

    this.playerService.Seek.subscribe((t)=>{

      this.player.currentTime(t);
      this.player.play();

    });

    setInterval(()=>{

      that.cd.detectChanges();

    }, 100);
  }

  ngOnChanges(change){

  	if(change.src && change.src.firstChange){
  		if(document.getElementById("mainVideo").children.length>2) return;
  		let that = this;
  		setTimeout(()=>{

        if(document.getElementById("mainVideo").children.length>2) return;
        
	  		that.player = videojs("mainVideo", {
			  controls: true,
			  autoplay: false,
			  preload: false,
        
			});


       videojs.plugin("markers", (<any>window).vjsMarkers);

        let player = videojs('mainVideo');
       
      player.controlBar.addChild('button', {
        'el':videojs.createEl('button', { className:  'oi player_bk',
              'role': 'button'}, {"data-glyph":"media-skip-backward"})
        }).on('click', function(e) {
          player.currentTime(player.currentTime()-10);
        });

      player.controlBar.addChild('button', {
        'el':videojs.createEl('button', { className:  'oi player_fwd',
              'role': 'button'}, {"data-glyph":"media-skip-forward"})
        }).on('click', function(e) {
          player.currentTime(player.currentTime()+10);
        });


      setInterval(()=>{

        let player = videojs('mainVideo');

        if(player.isFullscreen()){

          document.getElementsByClassName("player_bk")[0].classList.add("bk_full");
          document.getElementsByClassName("player_fwd")[0].classList.add("fw_full");
          document.getElementsByClassName("vjs-play-control")[0].classList.add("play_adjusted");

        }else{
          document.getElementsByClassName("player_bk")[0].classList.remove("bk_full");
          document.getElementsByClassName("player_fwd")[0].classList.remove("fw_full");
          document.getElementsByClassName("vjs-play-control")[0].classList.remove("play_adjusted");
        }

      }, 1000);

      // <span class="oi" data-glyph="media-skip-forward"></span>

       if(change.src && change.src.currentValue && that.player){
      that.player.src(change.src.currentValue.path);
      that.player.markers({
      });
      that.player.play();
    };


  		},100);
  		
  	}

    if(change.src && change.src.currentValue && this.player){
      this.player.src(change.src.currentValue.path);
      this.player.markers({
      });
      this.player.play();
    };
    

    // if(change.comments && !change.firstChange && change.comments.currentValue){

    //   // this.player.ready(()=>{

    //   //   // this.AddCues();

    //   // });

      
    //   }

  }

  public AddCue(comment?){

    this.player.markers.add([

      {
      backgroundColor:this.getCueBg(comment),
      time: comment.time,
      text: comment.comment
      }

      ]);

  }

  private AddCues(){

    let that = this;

    that.comments.forEach((c)=>{

         that.AddCue(c);

       });

     // setTimeout(()=>{

     //   that.comments.forEach((c)=>{

     //     that.AddCue(c);

     //   });

     // }, 5000);

  }

  public getCueBg(comment){

    if(comment.default_tags.length>0){

       let ret = "rgb(118, 0, 0)";

       comment.default_tags.forEach((dt)=>{

        if(dt){
          let index = _.findIndex(this.customMarkers, {account_tag_id:dt.account_tag_id});

         if(index>-1) {
           ret = this.colors[index];
         }
        }
         

       });

       return ret;

    }else{
      return "rgb(118, 0, 0)";
    }

  }

  ngAfterViewInit(){
  	let that = this;
    let cuesAdded = false;
    let time = 0;
    let end_recorded = false;

    setInterval(()=>{

      if(!this.player.paused() && this.player.currentTime() > 0){

         time++;
         // console.log(that.player.currentTime(), that.player.duration());
         
        // console.log(this.player.ended()); 

         if(time % 5 == 0 && this.params){
          // this.playerService.ModifyPlayingState("pause");
           let sessionData:any = this.headerService.getStaticHeaderData();
           let user_id = sessionData.user_current_account.User.id;
           let document_id = this.params.video_id;

           this.mainService.LogViewerHistory({user_id: user_id, document_id: document_id, minutes_watched: time}).subscribe((data)=>{

            // console.log(data);

           });


         }

       }

    }, 1100);

  	setInterval(()=>{

  		// that.player;
      if(that.player){

        
        this.player.on("ended", ()=>{
          if(!end_recorded && time % 5 != 0){

            end_recorded = true;
            

          // this.playerService.ModifyPlayingState("pause");
           let sessionData:any = this.headerService.getStaticHeaderData();
           let user_id = sessionData.user_current_account.User.id;
           let document_id = this.params.video_id;

           this.mainService.LogViewerHistory({user_id: user_id, document_id: document_id, minutes_watched: time}).subscribe((data)=>{

            // console.log(data);

           });


         
        }
        })

  		 that.VideoCurrentTime.emit(that.player.currentTime());
       if(that.player.currentTime() > 0 && !cuesAdded){
         cuesAdded = true;
         that.AddCues();
       }
       this.srtTrack();
      }

      else that.VideoCurrentTime.emit(0);

      that.IsPlaying.emit(!that.player.paused());

  	}, 1000);

  }

  public seek(where, elementRef:ElementRef){


  }

public PlayPause(arg){

    if(!this.player)
      setTimeout(()=>{
      if(arg=="pause"){
            this.player.pause();
          }else if(arg=="play"){
            this.player.play();
      }
      }, 1000);

    else{
      if(arg=="pause"){
            this.player.pause();
          }else if(arg=="play"){
            this.player.play();
      }
    }

  }

    srtTrack()
    {
        let document_id = this.params.video_id;
        if(this.VideoInfo && this.VideoInfo.subtitle_available == 1)
        {
            let url = this.VideoInfo.url;
            let path = url.substring(0, url.lastIndexOf("/"));
            this.player.on('loadedmetadata', function () {
                console.log("loadedmetadata");
                let subtitle_path = path+"/"+document_id+"_transcribe.vtt";
                console.log("subtitle_path",subtitle_path);
                this.addRemoteTextTrack({
                    src: subtitle_path,
                    srclang: 'en',
                    label: 'english',
                    kind: 'captions'
                }, true);
            });
        }

    }
}
