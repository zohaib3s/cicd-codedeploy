let env = window.location.hostname.substring(0, window.location.hostname.indexOf(".")).toUpperCase();


env = env || "COACHINGSTUDIO"; //in case of localhost

let relativePaths = {
  APP: {
    production: false,
    APIbaseUrl: "https://api.sibme.com",
    baseHeaderUrl: "https://app.sibme.com/api",
    baseUrl: "https://app.sibme.com",
    fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
    container: "sibme-production",
    appPath: "https://app.sibme.com",
    appUrl: "http://sibme.com",
    imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
    enableLogging: false,
    channelsArr: []
  },
  QA: {
    production: false,
    APIbaseUrl: "https://qaapi.sibme.com",
    baseHeaderUrl: "https://qa.sibme.com/api",
    baseUrl: "https://qa.sibme.com",
    fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
    container: "sibme-production",
    appPath: "https://qa.sibme.com",
    appUrl: "http://sibme.com",
    imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
    enableLogging: false,
    channelsArr: []
  },
  Q2: {
    production: false,
    APIbaseUrl: "https://q2api.sibme.com",
    baseHeaderUrl: "https://q2.sibme.com/api",
    baseUrl: "https://q2.sibme.com",
    fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
    container: "sibme-production",
    appPath: "https://q2.sibme.com",
    appUrl: "http://sibme.com",
    imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
    enableLogging: true,
    channelsArr: []
  },
  Q3: {
    production: false,
    APIbaseUrl: "https://q3api.sibme.com",
    baseHeaderUrl: "https://q3.sibme.com/api",
    baseUrl: "https://q3.sibme.com",
    fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
    container: "sibme-production",
    appPath: "https://q3.sibme.com",
    appUrl: "http://sibme.com",
    imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
    enableLogging: true,
    channelsArr: []
  },
  STAGING: {
    production: false,
    APIbaseUrl: "https://stagingapi.sibme.com",
    baseHeaderUrl: "https://staging.sibme.com/api",
    baseUrl: "https://staging.sibme.com",
    fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
    container: "sibme-production",
    appPath: "https://staging.sibme.com",
    appUrl: "http://sibme.com",
    imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
    enableLogging: false,
    channelsArr: []
  },
  HMH: {
    production: false,
    APIbaseUrl: "https://hmhapi.sibme.com",
    baseHeaderUrl: "https://hmh.sibme.com/api",
    baseUrl: "https://hmh.sibme.com",
    fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
    container: "sibme-production",
    appPath: "https://hmh.sibme.com",
    appUrl: "http://hmh.com",
    imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
    enableLogging: false,
    channelsArr: []
  },
  Q2HMH: {
      production: false,
      APIbaseUrl: "https://q2hmhapi.sibme.com",
      baseHeaderUrl: "https://q2hmh.sibme.com/api",
      baseUrl: "https://q2hmh.sibme.com",
      fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
      container: "sibme-production",
      appPath: "https://q2hmh.sibme.com",
      appUrl: "http://hmh.com",
      imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
      enableLogging: false,
      channelsArr: []
  },
  WL: {
    production: false,
    APIbaseUrl: "https://wlapi.sibme.com",
    baseHeaderUrl: "https://wl.sibme.com/api",
    baseUrl: "https://wl.sibme.com",
    fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
    container: "sibme-production",
    appPath: "https://wl.sibme.com",
    appUrl: "http://hmh.com",
    imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
    enableLogging: false,
    channelsArr: []
  },
  COACHINGSTUDIO: {
    production: true,
    APIbaseUrl: "https://csapi.sibme.com",
    baseHeaderUrl: "https://coachingstudio.hmhco.com/api",
    baseUrl: "https://coachingstudio.hmhco.com",
    fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
    container: "sibme-production",
    appPath: "https://coachingstudio.hmhco.com",
    appUrl: "https://coachingstudio.hmhco.com",
    imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
    enableLogging: false,
    channelsArr: []
  },
  Q22: {
    production: false,
    APIbaseUrl: "https://q22api.sibme.com",
    baseHeaderUrl: "https://q22.sibme.com/api",
    baseUrl: "https://q22.sibme.com",
    fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
    container: "sibme-production",
    appPath: "https://q22.sibme.com",
    appUrl: "http://sibme.com",
    imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
    enableLogging: true,
    channelsArr: []
  },
};

export const environment = relativePaths[env];
