import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GenralService {

  constructor(private http:HttpClient) { }

  public GetOrders(body){
    let url = environment.APIbaseUrl + "/get_orders";
    let data = {
      sap_type: body.sap_type,
      admin_key: body.admin_key,
    }
    return this.http.post(url,data);
  }
  public SendInvitation(body){
    let url = environment.APIbaseUrl + "/send_invitations";
    return this.http.post(url,body);
  }
  public ReSendInvitation(body){
    let url = environment.APIbaseUrl + "/resend_invitation";
    return this.http.post(url,body);
  }
}
