import { Component, OnInit,ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ActivatedRoute, Router } from '@angular/router';
import { GenralService } from 'src/app/services/genral.service';
import * as _ from 'lodash';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {
  @ViewChild('Assigin_license', { static: false }) Assigin_license: ModalDirective;
  orders:any;
  admin_key: any;
  licencesData: any = [];
  orderIndex=null;
  orderId=null;
  popupData: any;
  search:any;
  p:any;
  constructor(private aroute:ActivatedRoute,private api:GenralService,private toastr:ToastrService, private router:Router) { }

  ngOnInit() {
    let body:any = {};
    this.aroute.parent.params.subscribe(params=>{
      this.admin_key = params.id;
      params.id ? body.admin_key = params.id : false;
      this.aroute.params.subscribe(qparam=>{
        this.orderId=qparam.lid;
        this.getOrder(body,qparam);
      })
    })
  }
  getOrder(body,qparam){
    this.api.GetOrders(body).subscribe((data:any)=>{
      if(data.success==false){
        this.orders = [];
        this.router.navigate(['/']);
      }else{
        this.orders = data
        let order_detail:any = [];
          order_detail.licenses=[]
          order_detail.licenses.unused = [];
          order_detail.licenses.used = [];
          this.orderIndex = _.findIndex(this.orders, {id: Number(qparam.lid)});
          if(this.orderIndex==-1){
            this.router.navigate(['/root/'+this.admin_key+'/'])
          }
          let n = 1;
          this.orders[this.orderIndex].licenses.forEach((o,i) => {
            o.touched=false;
            o.full_name = (o.first_name==null?'':o.first_name) +' '+ (o.last_name==null?'':o.last_name);
            if(o.is_used==0){
              order_detail.licenses.unused.push(o);
            }else{
              o.n = n++;
              order_detail.licenses.used.push(o);
            }
          });
          this.licencesData = order_detail;
          this.popupData = [];
          this.popupData.license = [];
          this.popupData.license.unused = [];
          this.popupData.license.used = [];
          this.popupData.license.unused = JSON.parse(JSON.stringify(this.licencesData.licenses.unused))
          this.popupData.license.used = JSON.parse(JSON.stringify(this.licencesData.licenses.used))
          this.popupData.license.unused.forEach(o => {
            o.role = 'Viewer';
          });
          this.popupData.license.used.forEach(x => {
            x.role = 'Viewer';
          });
    }
    },err=>{
      this.router.navigate(['/']);
    });
  }
  sendInvites(){
    let dirtymind = true;
    for (let i = 0; i < this.popupData.license.unused.length; i++) {
      if(this.popupData.license.unused[i].touched){
        if(!this.emailChecker(this.popupData.license.unused[i].email) ){
          this.toastr.info('A valid email is requried');
          
          dirtymind=false;
          break;
        }
        else if(!this.popupData.license.unused[i].first_name || !this.popupData.license.unused[i].first_name.trim()  ){
          this.toastr.info('First Name Requried');
            
          dirtymind=false;
          break;
        }
        else if(!this.popupData.license.unused[i].last_name || this.popupData.license.unused[i].last_name.length===0 || !this.popupData.license.unused[i].last_name.trim() ){
          this.toastr.info('Last Name Requried');
            
          dirtymind=false;
          break;
        }
        else {
          this.popupData.license.unused[i].role = "User";
        }        
      }
    }
    if(dirtymind){
      this.sendInviteAPI(); 
    }
  }
  touchedChecker(item){
    if(item.email || item.last_name || item.first_name) item.touched=true;
    else if(!item.email && !item.last_name && !item.first_name) item.touched=false;
  }
  emailChecker(email) {
    var regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/
    return regex.test(email);
  }
  sendInviteAPI(){
    let body:any = {};
    body.admin_key = this.admin_key;
    body.order_id = this.orderId;
    body.licenses = [];
    this.popupData.license.unused.forEach(data => {
      if(data.first_name!=null && data.last_name!=null && data.email!=null && data.first_name!="" && data.last_name!="" && data.email!=""){
        let nData = {id:data.id,order_id:data.order_id,is_used:data.is_used,is_invited:data.is_invited,first_name:data.first_name,last_name:data.last_name,email:data.email,created_at:data.created_at,updated_at:data.updated_at,role:data.role};
        body.licenses.push(nData)
      }
    });
    this.api.SendInvitation(body).subscribe((data:any)=>{
      if(data.success){
        this.hideModal('Assigin_license')
        this.toastr.success(data.message)
        this.ngOnInit();
      } else {
        this.toastr.info(data.message)
      }
    })
  }
  reSendInvite(obj){
    let body = {
      admin_key: this.admin_key,
      license_id: obj.id,
      order_id: this.orderId
    }
    this.api.ReSendInvitation(body).subscribe((data:any)=>{
      if(data.success){
        this.toastr.info(data.message)
      }
    })
  }
  showModal(modal){
    
    if(modal=="Assigin_license"){
      this.Assigin_license.show();
    }
  }
  hideModal(modal){
    if(modal=="Assigin_license"){
      this.popupData.license.unused = JSON.parse(JSON.stringify(this.licencesData.licenses.unused))
      this.popupData.license.unused.forEach(p => {
        p.role = 'Viewer';
      });
      this.Assigin_license.hide();
    }
  }

}
