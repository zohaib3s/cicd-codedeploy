import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GenralService } from 'src/app/services/genral.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  @ViewChild('licen_detail', { static: false }) licen_detail: ModalDirective;
  @ViewChild('LicensesId', { static: false }) LicensesId: ModalDirective;
  @ViewChild('Order_detail', { static: false }) Order_detail: ModalDirective;
  orders:any;
  public licencesData;
  admin_key: any;
  notfound:boolean=false;
  public order_Detail: any = {}; 
  constructor(private api:GenralService,private aroute:ActivatedRoute, private router:Router, private toastr: ToastrService) { }

  ngOnInit() {
    let body:any = {};
    this.aroute.parent.params.subscribe(params=>{
      this.admin_key = params.id;
      params.id ? body.admin_key = params.id : false;
        this.api.GetOrders(body).subscribe((data:any)=>{
          if(data.success==false){
            this.orders = [];
            this.router.navigate(['/']);
          }else{
            this.orders = data
            
          }
        },err=>{
          this.router.navigate(['/']);
        });
    })
  }
  sendInvites(){
    this.licencesData.licenses.every(x => {
      if(x.email!=null && x.email!="" && x.full_name!=null && x.full_name!=""){
        this.sendInviteAPI();
      }else{
        this.toastr.info('Email and Full Name Requried')
        return;
      }
    });
  }
  sendInviteAPI(){
    let body:any = {};
    body.admin_key = this.admin_key;
    body.licenses = this.licencesData.licenses;
    
    this.api.SendInvitation(body).subscribe((data:any)=>{
      if(data.success){
        this.hideModal('LicensesId')
        this.toastr.success(data.message)
      }
    })
  }
  reSendInvite(obj){
    let body = {
      admin_key: this.admin_key,
      license_id: obj.id
    }
    this.api.ReSendInvitation(body).subscribe((data:any)=>{
      if(data.success){
        this.hideModal('LicensesId')
        this.toastr.info(data.message)
      }
    })
  }
  show_data(data){
    this.licencesData = data;
  }
  showModal(modal,order_detail?){
    if(order_detail){
      this.order_Detail = order_detail;
      
    }
    if(order_detail){
      order_detail.licenses.unused = [];
      order_detail.licenses.used = [];
      order_detail.licenses.forEach(o => {
        if(o.is_used==0){
          order_detail.licenses.unused.push(o);
        }else{
          order_detail.licenses.used.push(o);
        }
      });
      this.licencesData = order_detail;
      
      
    }
    if(modal=="licen_detail"){
      this.licen_detail.show();
    }else if(modal=="LicensesId"){
      this.LicensesId.show()
    }else if(modal == "Order_detail"){
      this.Order_detail.show()
    }
  }
  hideModal(modal){
    if(modal=="licen_detail"){
      this.licen_detail.hide();
    }else if(modal=="LicensesId"){
      this.LicensesId.hide()
    }else if(modal=="Order_detail"){
      this.Order_detail.hide();
    }
  }

}
