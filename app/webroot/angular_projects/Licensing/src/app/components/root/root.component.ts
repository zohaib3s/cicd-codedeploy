import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css']
})
export class RootComponent implements OnInit {
  sap_type:any = '1';
  constructor(private aroute:ActivatedRoute,private router:Router) { }

  ngOnInit() {
  }

}
