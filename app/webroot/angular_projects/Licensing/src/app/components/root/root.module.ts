import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RootRoutingModule } from './root-routing.module';
import { OrdersComponent } from './innercomponents/orders/orders.component';
import { FAQComponent } from './innercomponents/faq/faq.component';
import { RootComponent } from './root.component';
import { ModalModule } from 'ngx-bootstrap';
import { TabsModule } from 'ngx-bootstrap/tabs'
import { HttpClientModule } from '@angular/common/http';
import { OrderDetailsComponent } from './innercomponents/order-details/order-details.component';
import { SearchPipe } from 'src/app/pipe/search.pipe';
import {NgxPaginationModule} from 'ngx-pagination';
import {TooltipModule} from 'ngx-bootstrap';

@NgModule({
  declarations: [RootComponent,OrdersComponent, FAQComponent, OrderDetailsComponent,SearchPipe],
  imports: [
    CommonModule,
    RootRoutingModule,
    FormsModule,
    TabsModule.forRoot(),
    ModalModule.forRoot(),
    HttpClientModule,
    NgxPaginationModule,
    TooltipModule.forRoot()
  ]
})
export class RootModule { }
