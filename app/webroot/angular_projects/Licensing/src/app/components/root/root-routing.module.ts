import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrdersComponent } from './innercomponents/orders/orders.component';
import { FAQComponent } from './innercomponents/faq/faq.component';
import { RootComponent } from './root.component';
import { OrderDetailsComponent } from './innercomponents/order-details/order-details.component';


const routes: Routes = [

  {path:'',component:RootComponent,children:[
    { path: '', redirectTo:'orders' ,pathMatch:'full' },
    { path: 'orders', component: OrdersComponent},
    {path:'orders/:lid/manage-license', component:OrderDetailsComponent},
    { path: 'FAQs', component: FAQComponent },
  ]},
  { path: 'orders', component: OrdersComponent },
  { path: 'FAQs', component: FAQComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RootRoutingModule { }
