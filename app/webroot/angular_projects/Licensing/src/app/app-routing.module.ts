import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RootComponent } from './components/root/root.component';
import { RootModule } from './components/root/root.module';


const routes: Routes = [
{ path: 'root/:id', loadChildren: './components/root/root.module#RootModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
