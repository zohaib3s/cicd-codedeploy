body{background:#f3f2ea; font-family: 'Roboto', sans-serif; }
 .topheader{text-align: center;background:url(../img/bgrep.png) repeat-x;}
.box_outer{ background:#fff;  border-radius:5px;     margin-top: 15px !important;     max-width: 1024px !important; margin-bottom: 20px !important;}
 .topheader{text-align: center; background:url(../img/bgrep.png) repeat-x;}
.panl_heading { border-bottom:1px solid #ddd; padding:18px 10px;     background: #f9f9f9;     border-top-right-radius: 5px;     border-top-left-radius: 5px;}
img{ max-width:100%;}
.panl_heading  h3{    font-family: 'Roboto', sans-serif; margin: 0; font-weight: 400;font-size: 24px;     line-height: 40px; color:#484848;}
.clear{ clear:both;}
.ass_right p{ margin:0; color:#010101; font-size:16px;}
.ass_right label{ margin:0; color:#616161; font-size:14px;}
.acc_profile img{ width:30px; height:30px; border-radius:50%;}
.ass_icon { float:left; width:18%;     padding-top: 9px;}
.ass_detail { float:left; width:82%;}
.section_control {     text-align: right;
    font-size: 13px;
    padding: 8px 15px;}
.section_control span{ cursor:pointer;}
.section_control span img { width:15px;}
.ass_left {color:#010101;}
.ass_left h3 {     margin: 0 0 11px 0;
    font-weight: 400;
    color: #010101;
    font-size: 21px;
}
.clear{ clear:both;}
.video_detail { padding-top:15px;}
.ass_left p { margin:0 0 15px 0;color:#010101;}
.ass_info { margin-bottom:15px;}
.ass_left  .alertcls {color:#FF0000;}
.video_signle { padding:10px 0;}

.top_detail {background: #F9F9F9; }
.acc_edit{  text-align:right; }
.acc_edit a{ border:1px solid #DADADA;     padding: 7px 6px 7px 6px; border-radius:4px; width:85%; background:#fff; display:inline-block; color:#484848; text-decoration:none; text-align:center;}
.sample_data {background: #F9F9F9; margin:0 -15px; padding:25px 30px 30px 30px; border-bottom: 1px solid #D8D8D8;}
.sample_top {  border-top:1px solid #D8D8D8; padding:20px 0 0 0;}

.sample_top h5{ color:#010101; font-size:15px;}


.plus_acc { text-align:right;     position: relative;}
.plus_acc button{ border:0; padding:0;background:#28A745;     outline: none; cursor:pointer;           padding: 7px 18px;
    border-radius: 4px;
    margin-left: 12px;}
  .plus_acc .dropdown-toggle::after { display:none;}
  .plus_acc button img { position:relative; top:-1px;}
  .plus_acc .dropdown-menu{    padding: 10px 6px 6px 9px !important;            -webkit-box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.1);
    box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.1); left:-124px !important;}
  
  .plus_acc .dropdown-menu a {  font-size:14px !important; background: none !important; color:#484848; margin-bottom:5px;     margin-left: 0;}
  .sample_inner{    box-shadow: 0 1px 7px 0 rgba(0,0,0,.1); background:#fff; border-radius:5px;     border: 1px solid #ddd;
    margin-bottom: 28px;     border-bottom-left-radius: 5px;   border-top-left-radius: 5px;    border-top-right-radius: 5px;
  }

  .placeholder_v img{   border-bottom-left-radius: 5px;   border-top-left-radius: 5px;}
  .sample_inner .ac_op {float: right; text-align: right; width: 14%;}
  .acc_option { margin: 10px 0 0 0;}
  
  .acc_option a {
    text-decoration: none;
    border-radius: 50px;
    display: inline-block;
    padding: 3px 10px;
    line-height: 19px;
    height: 25px;
    font-size: 13px;
}
  
  .ac_op button {
    border: 0;
    padding: 0;
    background: none;
    outline: none !important;
    cursor: pointer !important;
}

.ac_op .dropdown-toggle::after { display:none;}
  .ac_op .dropdown-menu{    min-width: 85px !important;padding: 10px 6px 6px 9px !important;     left: -75px !important;}
  .ac_op .dropdown-menu a {  font-size:14px !important; background: none !important; color:#484848; margin-bottom:5px; cursor:pointer !important;}
  

  .chat_cls {
    background: #EAEEF3;
    color: #1485FE;
    margin-right: 5px;
}

.h_att {
    background: #EAF3EB;
    color: #28A745;
}

.thumb_ac {
    background: #e0e0e0;

    margin: -1px -1px 0 -1px;
      border-bottom-left-radius: 5px;
    border-top-left-radius: 5px;
    text-align: center;
    position: relative;
    display: table;
    width: 40%;
    vertical-align: middle; float:left; margin-right:4%;
}

.play_acc {
    position: absolute;
    top: 30px;
    left: 0;
    right: 0;
    cursor: pointer;
}
.play_acc img { max-width:50px;}

.acc_time {
    position: absolute;
    right: 4px;
    bottom: 4px;
    background-color: rgba(0,0,0, 0.7);
    color: #fff;
    border-radius: 4px;
    padding: 3px 9px 3px 6px;
}
.sample_detail { float:left; width:52%;     padding-top: 5px;}
.sample_detail .ac_video {
    float: left;
    width: 80%;    margin-bottom: 6px;
}

.sample_detail .ac_op {
    float: right;
    text-align: right;
    width: 14%;
}
.sample_detail p{ margin-bottom:6px;}
.sample_detail label{ margin-bottom:0;}

.placeholder_v img{    height: 124px;     object-fit: cover;
    width: 100%;}
.sample_top .col-6 { padding-bottom:10px;}
.list_img{ float:left; width:25%;}
.list_detail{float:left; width:75%;}

  .modal-title {font-family: 'Roboto', sans-serif; font-weight:400; 
    font-size: 18px;    display: block; color: #484848;
    text-align: center;
    width: 95%; }
  
  .modal-body {
    padding: 1rem 20px!important;
}



  
  /* The container */
.checkboxcls {
  display: block;
  position: relative;
  padding-left: 30px;
    margin: 13px 0;
  cursor: pointer;
  font-size: 13px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  color:#616161;
}

/* Hide the browser's default checkbox */
.checkboxcls input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
  height: 0;
  width: 0;
}

/* Create a custom checkbox */
.checkmark {
  position: absolute;
  top: -1px;
  left: 0;
  height: 20px;
  width: 20px;
  background-color: #fff;
   border: 1px solid #B4BAC3;
    border-radius: 3px;
}

/* On mouse-over, add a grey background color */
.checkboxcls:hover input ~ .checkmark {
  background-color: #fff;
}

/* When the checkbox is checked, add a blue background */
.checkboxcls input:checked ~ .checkmark {
      background-color: #fff;
   
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the checkmark when checked */
.checkboxcls input:checked ~ .checkmark:after {
  display: block;
}

/* Style the checkmark/indicator */
.checkboxcls .checkmark:after {
  left: 7px;
  top: 3px;
  width: 5px;
  height: 10px;
  border: solid #B4BAC3;
  border-width: 0 2px 2px 0;
  -webkit-transform: rotate(40deg);
  -ms-transform: rotate(40deg);
  transform: rotate(40deg);
}



.addvideo { width:100%;}
.addvideo input[type=text]{ width:100%; height:40px; border:1px solid #ddd; border-radius:5px; padding:10px 11px;    background: url(../img/search_icon.svg) no-repeat center left 11px;
    background-size: 19px;
    padding-left: 35px;}


.addv_footer .btn-success {
    background: #28a745!important;
    border: 0;
    font-weight: 500;
}


.addv_footer .row {
    width: 100%;
    border-top: 1px solid #e9ecef;
    padding-top: 16px;
    padding-bottom: 3px;
    margin: 0!important;
}
.addv_footer { border:0 !important;}
.modal_btn { text-align:right;}
.addv_footer .col-6 { padding:0;}


 .video_thumbnail {
    background: #e0e0e0;
    height: 53px;
    margin: -1px 0 0 -1px;
    border-radius: 4px;
    text-align: center;
    position: relative;
    width: 37%;
    float: left;
    display: table;
}


 .title_cls {
    width:58%;
    float: left;
    margin-left: 5%;
    color: #010101;
}

.play_icon_ass {
    position: absolute;
    left: 3px;
    cursor: pointer;
    background: #FFFFFF;
    border-radius: 3px;
    bottom: 5px;
    width: 24px;
    height: 22px;
    padding: 0px 0;
}


.time_cls {
    position: absolute;
    right: 4px;
    bottom: 4px;
    background-color: rgba(0,0,0, 0.7);
    color: #fff;
    border-radius: 4px;
    padding: 3px 9px 3px 6px;font-size: 12px;
}



img { max-width:100%;}
.added_btn { background:#28A745; display:inline-block; text-align:center; color:#fff !important;
 border-radius:50px;     padding: 4px 3px;
   
    width: 100%;}
.video_addbtn { border-radius:50px; display:inline-block; text-align:center; 
  border:1px solid #CCCCCC; color:#616161 !important; padding: 4px 3px;
   
    width: 100%;}

.video_heading { border-top:1px solid #ddd; padding:10px 0 10px 0; margin-top: 11px;} 
.video_list { border-top:1px solid #ddd; padding:10px 0 10px 0;}

 .video_thumbnail .thumb_outer img{    border-radius: 4px;}


.video_list .col-2{ padding: 0; }
.video_heading .col-2{ padding: 0; }

.video_list .col-7{flex: 0 0 50%;
    max-width: 50%;}
.video_list .col-3{flex: 0 0 25%;
    max-width: 25%;}
.video_list .col-2{flex: 0 0 25%;
    max-width: 25%;}


    .video_heading .col-7{flex: 0 0 50%;
    max-width: 50%;}
.video_heading .col-3{flex: 0 0 25%;
    max-width: 25%;}
.video_heading .col-2{flex: 0 0 25%;
    max-width: 25%;}




.assessor_box {
    margin: 0 -8px;
    padding-bottom: 40px;
}


.assessor_inner{    border: 1px solid #ddd;
    border-radius: 5px;
    -webkit-box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.1);
    box-shadow: 0px 1px 7px 0px rgba(0,0,0,0.1);
    margin-bottom: 28px;}

.assessor_hu {
    background: #e0e0e0;
    height: 150px;
    margin: -1px -1px 0 -1px;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    text-align: center;
    position: relative;
    display: table;
    width: 101%;
    vertical-align: middle;
}

.assessor_thumb img {
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;    width: 100%;
    max-height: 150px;
    cursor: pointer;
    object-fit: cover;
}


.assessor_detail {
    padding: 10px;   min-height: 123px;
}
.asse_video {float: left;
    width: 80%;
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;}
  
  .asse_op {    float: right;
    text-align: right;
    width: 14%;}
  
  .asse_op button {
    border: 0;
    padding: 0;
    background: 0 0;
      outline: 0;
    cursor: pointer;
}

.asse_op img {
    max-width: 20px;
}

.asse_op .dropdown-menu {
    left: -76px !important;
  min-width: 85px;
    padding: 10px 6px 6px 9px;
}

.asse_op .dropdown-toggle::after { display:none;}


.asse_op .dropdown-menu a {
    font-size: 14px;
    background: 0 0;
    color: #484848;
    margin-bottom: 5px;cursor: pointer!important;
}


.asse_video label {
    font-size: 16px;
    display: inline-block;
    
    line-height: 20px;
    color: #010101;
      font-weight: 400;
    margin-bottom: -2px;
}

.assessor_detail p{margin:0;}
.asse_video p {
    padding: 1px 0 0;
    font-size: 14px; 
}

.asse_by b {
    color: #616161;
    font-weight: 500;
    font-size: 13px;
}


.asse_date {
    color: #a3a3a3;font-size: 13px;
}


.asse_option {
    margin: 10px 0 0;
}

.asse_option a {
    text-decoration: none;
    border-radius: 50px;
    display: inline-block;
    padding: 2px 9px 1px;
    line-height: 19px;
    height: 22px;
    font-size: 12px;
    font-weight: 500;
}

.asse_comm{background: #eaeef3;
    color: #1485fe;
    margin-right: 8px;}
.asse_att{    background: #eaf3eb;
    color: #28a745;}
  
  .asse_time {
        position: absolute;
    right: 8px;
    bottom: 8px;
    background-color: rgba(0,0,0,.7);
    color: #fff;
    border-radius: 4px;
    padding: 3px 7px 3px 5px;
    font-size: 13px;
}


.asse_icon {
    position: absolute;
    top: 48px;
    left: 0;
    right: 0;
    cursor: pointer;
}


.doc_icon {
    display: table-cell;
    height: 100%;
    padding: 0;
    width: 100%;
    vertical-align: middle;
}

.doc_icon img {
    max-width: 45px;
    border-radius: 0;
}

.asses_detail {    padding: 22px 0;}
.asse_profile {}
.backtoall { text-align:right;}
.backtoall img { margin-right:12px;}

.assessor_box .col-md-3 {
    padding: 0 11px;
}

.doucment_blank{ border:2px dashed #ddd; min-height:275px; padding:10px; text-align:center; border-radius:5px; margin-bottom:20px;}
.doucment_blank label { display:block;    padding: 18px 0 10px 0;     font-weight: 500;
    font-size: 16px;}
.submitcls { text-align:right;}
.add_green { padding-top:80px;}
.add_green  button {
    border: 0;
    padding: 0;
    background: #28A745;
    outline: none;
    cursor: pointer;
    padding: 7px 18px;
    border-radius: 4px;
    margin-left: 12px;
}


.add_green button img {
    position: relative;
    top: -1px;
}

.submitcls a{    background: #28A745;
    display: inline-block;
    color: #fff;
    text-decoration: none;
    border-radius: 4px;
    padding: 8px 26px;
    font-size: 14px;}

.blank_img { min-height:55px;}

.popup_scroll{
   max-height: 400px;
   overflow: auto;    overflow-x: hidden;    padding-right: 18px;
}




.ani2{
  -webkit-animation-duration: 1.25s;
          animation-duration: 1.25s;
  -webkit-animation-fill-mode: forwards;
          animation-fill-mode: forwards;
  -webkit-animation-iteration-count: infinite;
          animation-iteration-count: infinite;
  -webkit-animation-name: placeHolderShimmer;
          animation-name: placeHolderShimmer;
  -webkit-animation-timing-function: linear;
          animation-timing-function: linear;
    background: #d9d9d9;
    background: linear-gradient(to right, #e9e9e9 8%, #F0F0F0 18%, #efefef 33%);
  background-size: 800px 104px;
  position: relative;
}

.ani{
  -webkit-animation-duration: 1.25s;
          animation-duration: 1.25s;
  -webkit-animation-fill-mode: forwards;
          animation-fill-mode: forwards;
  -webkit-animation-iteration-count: infinite;
          animation-iteration-count: infinite;
  -webkit-animation-name: placeHolderShimmer;
          animation-name: placeHolderShimmer;
  -webkit-animation-timing-function: linear;
          animation-timing-function: linear;
  background: #F6F6F6;
  background: linear-gradient(to right, #F6F6F6 8%, #F0F0F0 18%, #F6F6F6 33%);
  background-size: 800px 104px;
  position: relative;
}

@-webkit-keyframes placeHolderShimmer {
  0% {
    background-position: -468px 0;
  }
  100% {
    background-position: 468px 0;
  }
}

@keyframes placeHolderShimmer {
  0% {
    background-position: -468px 0;
  }
  100% {
    background-position: 468px 0;
  }
}


.aniheading { display:block; height:20px;}
.anipara { display:block; height:10px;}

.acc_publish {
    text-align: right; margin-bottom:6px;
}

.acc_publish a{    border: 1px solid #28A745;
      padding: 7px 6px 7px 6px;
    border-radius: 4px;
    width: 85%;
    background: #28A745;
    display: inline-block;
    color: #fff !important;
    text-decoration: none !important;
    text-align: center;}
.anib { display:block; height:15px; width:121px;}
.rcircle { width:30px; height:30px; border-radius:50%;     display: block;}
.an40{ display:block; width:40%; height:10px; margin:8px 0 10px 0 !important;}
.an10 { max-width:10%;}
.sa_ani { display:block;     width: 120px; height: 20px;}
.thumbani { display:block; width:100%; height:120px;}
.anititle {display:block; width:80%; height:12px; margin:10px 0 4px 0 !important;}
.anby {display:block; width:90%; height:12px; margin:10px 0 4px 0 !important;}
.andate {display:block; width:100%; height:12px; margin:10px 0 4px 0 !important;}
.ancount {display:block; width:30px; height:16px; margin:10px 10px 4px 0 !important; float:left;}
.asse_par h2{     margin: 0; padding: 23px 0;font-size: 20px; font-weight:400; color:#010101;}
.asse_par_heading  { border-bottom:1px solid #ddd; padding:10px 0; margin-bottom:8px;}
.asse_par_list  { padding:9px 0;}
.asse_par_list p { margin:0;}
.asse_par_list label{ margin:0; color:#616161; font-weight:600; font-size:16px;}

.video_cou{    display: inline-block;background: #F1EFFF;border-radius: 50px; padding: 2px 13px; color:#935AFF;}
.video_file{    display: inline-block;background: #F1EFFF;border-radius: 50px; padding: 2px 13px; color:#935AFF;}

.video_comm{    display: inline-block;background: #EAEEF3;border-radius: 50px; padding: 2px 13px; color:#057EFF;}
.asses_count {display: inline-block;background-color:rgba(244,90,38,0.1);border-radius: 50px; padding: 2px 13px; color:#057EFF;}
.cross_gry { display: inline-block;background: #EAEEF3;border-radius: 50px; padding: 2px 13px; color:#057EFF;}
.count1 { display:block; height:15px; width:25px;   margin: 10px 0 4px 0 !important;}

.rcircle2 {
    width: 40px;
    height: 40px;
    border-radius: 50%;
    display: block;
}
.anititle2{display: block;
    width: 80%;
    height: 12px;
    margin: 0px 0 4px 0 !important;}
  
  .anby2{display: block;
    width: 80%;
    height: 12px;
    margin: 10px 0 4px 0 !important;}

.stitle { display:block !important; height:15px;}
.sopt { display:block !important; height:15px;}
.sby { display:block; width:100%; height:12px; margin:10px 0 10px 0 !important;}
.sdate { display:block; width:100%; height:12px; margin:0 0 10px 0 !important;}
.scountcls {display:block; width:25px; height:20px; float:left; margin-right:10px;}
.imgcircle { display:block; float:left; width:40px; height:40px; border-radius:50%;}
.imgtitle {display:block; float:left; width:150px; height:15px;     margin: 13px 0 0px 20px;}

/*no*/
.no_part { text-align:center; padding:80px 0 30px 0;}
.no_part h3 { font-weight:400; color:#484848; margin:0; padding:40px 0; font-size:18px;}

.doc_icons{    display: table-cell;
    height: 100%;
    padding: 0;
    width: 100%;
    vertical-align: middle;}
.doc_icons img { max-width:30px;}
.thumb_ac { height:122px;}

.ac_video label{    margin-bottom: 0;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    display: flow-root;     line-height: 20px;
    color: #010101;
    font-size: 16px;}
    .p0 { padding: 0 !important; }
    .asse_profile img{width: 40px;
    height: 40px;     border-radius: 50%;}

    .share_popup .addvideo{ position: relative; }
    .share_popup .addvideo img{cursor: pointer;
    right: 6px;
    position: absolute;
    top: 15px;}
    .thumb_outer img{    width: 100%;
    height: 70px;
    cursor: pointer;
    object-fit: cover;}
      .modal_btn .btn{ outline: 0 !important; }
    .modal_btn .btn-default{    background: none;}
   .modal_btn2 { border: 0 !important; text-align:right;}
    .modal_btn2 .btn-default {
    background: none;
}

.submis_cls p{    padding: 56px 30px 0 30px;
    margin: 0;
    text-align: center;
    font-size: 16px;}