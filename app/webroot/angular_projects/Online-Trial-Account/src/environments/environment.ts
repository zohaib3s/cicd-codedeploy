// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.


export let environment;

let env = window.location.hostname.substring(0, window.location.hostname.indexOf(".")).toUpperCase();

env = env || "Q32"; //in case of localhost

let relativePaths = {
    "APP":{
       production: true,
       APIbaseUrl: "https://api.sibme.com",
       baseHeaderUrl: "https://app.sibme.com/api",
       deploymentPath: 'app/webroot/online_trial_account',
       baseUrl: "https://app.sibme.com",
       fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
       container: "sibme-production",
       appPath: "https://app.sibme.com",
       appUrl: "http://sibme.com",
       project_title: "online_trial"
    },
    "QA":{
      production: false,
      APIbaseUrl: "https://qaapi.sibme.com",
      baseHeaderUrl: "https://qa.sibme.com/api",
      deploymentPath: 'app/webroot/online_trial_account',
      baseUrl: "https://qa.sibme.com",
      fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
      container: "sibme-production",
      appPath: "https://qa.sibme.com",
      appUrl: "http://sibme.com",
       project_title: "online_trial"
    },
    "Q3":{
      production: false,
      APIbaseUrl: "https://q3api.sibme.com",
      baseHeaderUrl: "https://q3.sibme.com/api",
      deploymentPath: 'app/webroot/online_trial_account',
      baseUrl: "https://q3.sibme.com",
      fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
      container: "sibme-production",
      appPath: "https://q3.sibme.com",
      appUrl: "http://sibme.com",
       project_title: "online_trial"
    },
    "Q31":{
        production: false,
        APIbaseUrl: "https://q31api.sibme.com",
        baseHeaderUrl: "https://q31.sibme.com/api",
        deploymentPath: 'app/webroot/online_trial_account',
        baseUrl: "https://q31.sibme.com",
        fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
        container: "sibme-production",
        appPath: "https://q31.sibme.com",
        appUrl: "http://sibme.com",
        project_title: "online_trial"
    },
    "Q2":{
      production: false,
      APIbaseUrl: "https://q2api.sibme.com",
      baseHeaderUrl: "https://q2.sibme.com/api",
      deploymentPath: 'app/webroot/online_trial_account',
      baseUrl: "https://q2.sibme.com",
      fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
      container: "sibme-production",
      appPath: "https://q2.sibme.com",
      appUrl: "http://sibme.com",
      imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
      project_title: "online_trial"
    },
    "STAGING":{
       production: false,
       APIbaseUrl: "https://stagingapi.sibme.com",
       baseHeaderUrl: "https://staging.sibme.com/api",
       deploymentPath: 'app/webroot/online_trial_account',
       baseUrl: "https://staging.sibme.com",
       fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
       container: "sibme-production",
       appPath: "https://staging.sibme.com",
       appUrl: "http://sibme.com",
       project_title: "online_trial"
    },
    "LOCAL":{
      production: false,
      APIbaseUrl: "http://localapi.sibme.com:8020",
      baseHeaderUrl: "http://local.sibme.com:8021/api",
      deploymentPath: 'app/webroot/video_details',
      baseUrl: "http://local.sibme.com:8021",
      fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
      container: "sibme-production",
      appPath: "http://local.sibme.com:8021",
      appUrl: "http://sibme.com",
      imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
       project_title: "online_trial"
    },
    "HMH":{
       production: false,
       APIbaseUrl: "https://hmhapi.sibme.com",
       baseHeaderUrl: "https://hmh.sibme.com/api",
       deploymentPath: 'app/webroot/add_huddle_angular',
       baseUrl: "https://hmh.sibme.com",
       fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
       container: "sibme-production",
       appPath: "https://hmh.sibme.com",
       appUrl: "http://hmh.com",
       imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
       project_title: "online_trial"
    },
    "WL":{
       production: false,
       APIbaseUrl: "https://wlapi.sibme.com",
       baseHeaderUrl: "https://wl.sibme.com/api",
       deploymentPath: 'app/webroot/add_huddle_angular',
       baseUrl: "https://wl.sibme.com",
       fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
       container: "sibme-production",
       appPath: "https://wl.sibme.com",
       appUrl: "http://hmh.com",
       imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
       project_title: "online_trial"
    },
    "COACHINGSTUDIO":{
       production: false,
       APIbaseUrl: "https://csapi.sibme.com",
       baseHeaderUrl: "https://coachingstudio.hmhco.com/api",
       deploymentPath: 'app/webroot/add_huddle_angular',
       baseUrl: "https://coachingstudio.hmhco.com",
       fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
       container: "sibme-production",
       appPath: "https://coachingstudio.hmhco.com",
       appUrl: "https://coachingstudio.hmhco.com",
       imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
       project_title: "online_trial"
    },
    "Q22":{
      production: false,
      APIbaseUrl: "https://q22api.sibme.com",
      baseHeaderUrl: "https://q22.sibme.com/api",
      deploymentPath: 'app/webroot/video_details',
      baseUrl: "https://q22.sibme.com",
      fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
      container: "sibme-production",
      appPath: "https://q22.sibme.com",
      appUrl: "http://sibme.com",
      imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
      project_title: "online_trial"
    },
    "Q21":{
      production: false,
      APIbaseUrl: "https://q21api.sibme.com",
      baseHeaderUrl: "https://q21.sibme.com/api",
      deploymentPath: 'app/webroot/video_details',
      baseUrl: "https://q21.sibme.com",
      fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
      container: "sibme-production",
      appPath: "https://q21.sibme.com",
      appUrl: "http://sibme.com",
      imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
      project_title: "online_trial"
    },
     "Q32": {
      production: false,
      APIbaseUrl: "https://q32api.sibme.com",
      baseHeaderUrl: "https://q32.sibme.com/api",
      deploymentPath: "app/webroot/video_details",
      baseUrl: "https://q32.sibme.com",
      fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
      container: "sibme-production",
      appPath: "https://q32.sibme.com",
      appUrl: "http://sibme.com",
      imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
      project_title: "online_trial"
    },
    "CLS":{
      production: false,
      APIbaseUrl: "https://clsapi.sibme.com",
      baseHeaderUrl: "https://cls.sibme.com/api",
      deploymentPath: 'app/webroot/online_trial_account',
      baseUrl: "https://cls.sibme.com",
      fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
      container: "sibme-production",
      appPath: "https://cls.sibme.com",
      appUrl: "http://sibme.com",
      imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
      project_title: "online_trial"
    },
    "DEVTEAMAPP":{
      production: false,
      APIbaseUrl: "https://devteam-api.sibme.com",
      baseHeaderUrl: "https://devteam-app.sibme.com/api",
      deploymentPath: 'app/webroot/online_trial_account',
      baseUrl: "https://devteam-app.sibme.com",
      fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
      container: "sibme-production",
      appPath: "https://devteam-app.sibme.com",
      appUrl: "http://sibme.com",
      imageBaseUrl: "https://s3.amazonaws.com/sibme.com/static/users",
      project_title: "online_trial"
    },
    "DOAPP":{
        production: false,
        APIbaseUrl: "https://doapi.sibme.com",
        baseHeaderUrl: "https://doapp.sibme.com/api",
        deploymentPath: 'app/webroot/online_trial_account',
        baseUrl: "https://doapp.sibme.com",
        fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
        container: "sibme-production",
        appPath: "https://doapp.sibme.com",
        appUrl: "http://sibme.com",
        project_title: "online_trial"
    },
    "DEVOPS":{
        production: false,
        APIbaseUrl: "https://devopsapi.sibme.com",
        baseHeaderUrl: "https://devops.sibme.com/api",
        deploymentPath: 'app/webroot/online_trial_account',
        baseUrl: "https://devops.sibme.com",
        fileStackAPIKey: "A3w6JbXR2RJmKr3kfnbZtz",
        container: "sibme-production",
        appPath: "https://devops.sibme.com",
        appUrl: "http://sibme.com",
        project_title: "online_trial"
    }
}

environment = relativePaths[env];

