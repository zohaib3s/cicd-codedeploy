import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";

import {ToastyModule} from 'ng2-toasty';
import { ArchwizardModule } from 'angular-archwizard';
import { HttpClientModule } from "@angular/common/http";
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarModule } from '@ngx-loading-bar/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { SignupWizardComponent } from './signup-wizard/signup-wizard.component';
import { TrialService } from "./trial.service";
import { DatalinkService } from "./datalink.service";
import { CustomProgressBarComponent } from './custom-progress-bar/custom-progress-bar.component';
import { InputValidatorDirective } from './input-validator.directive';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SignupWizardComponent,
    CustomProgressBarComponent,
    InputValidatorDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ArchwizardModule,
    FormsModule,
    ToastyModule.forRoot(),
    HttpClientModule,
    LoadingBarHttpClientModule,
    LoadingBarModule.forRoot()
  ],
  providers: [TrialService, DatalinkService],
  bootstrap: [AppComponent]
})
export class AppModule { }
