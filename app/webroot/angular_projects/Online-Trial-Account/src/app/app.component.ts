import { Component, OnInit } from '@angular/core';
import { environment } from '../environments/environment';
import { DatalinkService } from "./datalink.service";
import {TrialService} from "./trial.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  
  constructor(private datalayerService:DatalinkService, private trialService:TrialService){
    this.datalayerService.SetProperty({
      'event' : 'reg_step_1'
      });
  }
  ngOnInit(){
      this.trialService.initSettings();
  	// if(environment.production){
      let env = window.location.hostname.substring(0, window.location.hostname.indexOf(".")).toUpperCase();

      if (location.protocol != 'https:' && env!=""){
           location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
        }
//  	}
  }


}
