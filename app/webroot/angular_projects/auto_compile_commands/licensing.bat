cd ../../licensing
attrib +r .htaccess
echo y|del /s *.*
echo y|rmdir assets /s /q
attrib -r .htaccess
cd..
cd angular_projects/Licensing
ng build --prod --aot --build-optimizer --base-href="/licensing/" --output-path=../../licensing --delete-output-path=false
