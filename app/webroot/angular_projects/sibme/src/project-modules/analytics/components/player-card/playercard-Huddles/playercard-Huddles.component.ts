import { Input, OnChanges, ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from '@src/project-modules/app/services';
import { encodeCompress } from '@src/project-modules/shared/components/common/utility';
import { DetailsHttpService } from '@src/project-modules/video-huddle/child-modules/details/servic/details-http.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { ListAnimation } from './animation';
import { BsModalRef, BsModalService, ModalDirective, ModalOptions } from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-playercard-Huddles',
  templateUrl: './playercard-Huddles.component.html',
  styleUrls: ['./playercard-Huddles.component.css'],
     animations: [ListAnimation]
})
export class PlayercardHuddlesComponent implements OnInit, OnChanges {
  @Input() huddles;
  @ViewChild('childModal',{static:false}) childModal: ModalDirective;
  assementItems = [];
  collaborationItems = [];
  coachingItems = [];
  assesmentLess = false;
  collaborationLess = false;
  coachingLess = false;
  isDataFound = false;
  private headerData: any;
  public translation: any = {};
  private translationSubscription: Subscription;
  public translationLoaded: boolean = false;
  private subscriptions: Subscription = new Subscription();
  public totalHuddles: number;
​
  constructor(
    private router: Router,
    private headerService: HeaderService,
    private detailService: DetailsHttpService
  ) {
    this.translationSubscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      if (this.translation.analytics_user_role) { // check if translation for analytics loaded
        this.translationLoaded = true;
      }
    });

    this.subscriptions.add(this.headerService.behaviorialHeaderData$.subscribe(headerData => {
      this.headerData = headerData;
    }));
​
  }
​
  ngOnInit() {
  }
  ngOnChanges() {
​
    if (this.huddles) {
      this.totalHuddles = 0;
      this.assementItems = [];
      this.collaborationItems = [];
      this.coachingItems = [];
      if (this.huddles.assesment) {
        this.assementItems = this.huddles.assesment.slice(0, 4);
        this.totalHuddles += this.huddles.assesment.length;
        this.assesmentLess = false;
      }
      if (this.huddles.collaboration) {
        this.collaborationItems = this.huddles.collaboration.slice(0, 4);
        this.totalHuddles += this.huddles.collaboration.length;
        this.collaborationLess = false;
      }
      if (this.huddles.coaching) {
        this.coachingLess = false;
        this.coachingItems = this.huddles.coaching.slice(0, 4);
        this.totalHuddles += this.huddles.coaching.length;

      }
    }
    this.isDataFoundInHuddles();
  }
​
  isDataFoundInHuddles() {
    if(this.assementItems.length == 0 && this.collaborationItems.length == 0 && this.coachingItems.length == 0) {
      this.isDataFound = false;
    } else this.isDataFound = true;
  }
​
  assesmentSeeMore() {
    this.assementItems = this.huddles.assesment;
    this.assesmentLess = true;
    this.collaborationSeeLess();
    this.coachingSeeLess();
  }
  assesmentSeeLess() {
    this.assesmentLess = false;
    this.assementItems = this.huddles.assesment?.slice(0, 4);
  }
  collaborationSeeMore() {
    this.collaborationLess = true;
    this.collaborationItems = this.huddles.collaboration;
    this.assesmentSeeLess();
    this.coachingSeeLess();
​
  }
  collaborationSeeLess() {
    this.collaborationLess = false;
    this.collaborationItems = this.huddles.collaboration?.slice(0, 4);
  }
​
  coachingSeeMore() {
    this.coachingLess = true;
    this.coachingItems = this.huddles.coaching;
    this.assesmentSeeLess();
    this.collaborationSeeLess();
  }
  coachingSeeLess() {
    this.coachingLess = false;
    this.coachingItems = this.huddles.coaching?.slice(0, 4);
  }
  navigateToHuddle(huddle: any) {
    var obj = {
      huddle_id:huddle.account_folder_id,
      title:"",
      account_id: this.headerData.user_current_account.accounts.account_id,
      user_id: this.headerData.user_current_account.User.id,
      role_id:this.headerData.user_current_account.roles.role_id,
      page:1,
      doc_type:0,
      sort:"",
      goal_evidence:false
    }
    this.detailService.GetArtifacts(obj).subscribe((data: any) => {
      if (data.success) {
        let link = '';
        if (huddle.huddle_type == 3) {
          link = "/video_huddles/assessment/" + huddle.account_folder_id + '/huddle/details';
        } else {
          link = "/video_huddles/huddle/details/" + huddle.account_folder_id;
        }
        let queryParamsFrom = { displayName:'analytics_user_profile', url:this.router.url};
        if (link != '')
          this.router.navigate([link],{ queryParams: { from: encodeCompress(queryParamsFrom) }});
      } else {
        this.showChildModal();
      }
    }, error => {
      this.showChildModal();
    });
  }
​
  ngOnDestroy() {
    this.translationSubscription.unsubscribe();
    this.subscriptions.unsubscribe();
  }
​
   trackByFn(index, item) {
    return item.account_folder_id; // or item.id
  }

  showChildModal(): void {
    this.childModal.show();
  }

  hideChildModal(): void {
    this.childModal.hide();
  }
}