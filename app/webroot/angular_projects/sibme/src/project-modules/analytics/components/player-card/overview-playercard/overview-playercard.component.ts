import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PlaycardService } from '@src/project-modules/analytics/services';
import { MainAnalyticsService } from '@src/project-modules/analytics/services/main-analytics.service';
import { HeaderService } from '@src/project-modules/app/services';
import { GoalsFilters } from '@src/project-modules/goals/models';
import { GoalsService } from '@src/project-modules/goals/services';
import { TrackerFiltersService } from '@src/project-modules/shared/services/tracker-filters.service';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-overview-playercard',
  templateUrl: './overview-playercard.component.html',
  styleUrls: ['./overview-playercard.component.css']
})
export class OverviewPlayercardComponent implements OnInit {
  params = null;
  filterObject = null;
  public userDetails = null;
  private subscriptions: Subscription = new Subscription();
  isLoading = true;
  public sortData: any;
  goals:any;
  userDetailClone:any;
  public goalsFilters: GoalsFilters = new GoalsFilters();
  constructor(
    private activatedRoute: ActivatedRoute,
    private trackerFiltersService: TrackerFiltersService,
    private mainAnalyticsService: MainAnalyticsService,
    private headerService: HeaderService,
    private goalsService: GoalsService,
    private router: Router,
    private playcardService: PlaycardService,
    private toastr: ShowToasterService,
  ) {

  }

  ngOnInit() {
    setTimeout(() => {
      this.subscriptions.add(this.trackerFiltersService.filtersKeys.subscribe(data => {
        this.filterObject = data;
        setTimeout(() => {
          this.apiCall();
          this.getGoals();
        }, 500);
      }, error => {
        this.toastr.ShowToastr('info',error.message);
      }));
    }, 10);
    this.subscriptions.add(this.activatedRoute.parent.params.subscribe(activatedRouteParams => {
    
      this.params =Object.assign({},(activatedRouteParams));
      delete this.params['start_date']
      delete this.params['end_date']
    }));
  }

  apiCall() {
    let filterData = { ...this.filterObject, ...this.params };
    let sessionData = this.headerService.getStaticHeaderData();
    filterData.user_current_account = sessionData.user_current_account;
    filterData.user_permissions = sessionData.user_permissions;
    this.subscriptions.add(this.playcardService.LoadPlaycard(filterData).subscribe(data => {
      this.mainAnalyticsService.playerCardLoading.emit(false);
      this.userDetails = data;
      let userName = this.userDetails.user_essentials[0].first_name + ' ' + this.userDetails.user_essentials[0].last_name;
      this.mainAnalyticsService.breadCrumUserName.emit(userName);
    },(error)=>{
      this.toastr.ShowToastr('info',error.message);
    }));
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscriptions.unsubscribe()
  }

  getGoals() {
    let playercardSortDataKey = `playerCardSortData-${this.params.user_id}`;
    var data;
    this.goalsFilters.sortBy = "";
    if (this.headerService.getLocalStorage(playercardSortDataKey) != null || this.headerService.getLocalStorage(playercardSortDataKey) != undefined) {
      this.sortData = this.headerService.getLocalStorage(playercardSortDataKey);
      if (this.sortData.sortBy.VALUE == "default_view") {
        this.goalsFilters.sortBy = ""
      } else {
        this.goalsFilters.sortBy = this.sortData.sortBy.VALUE;
      }
    }
    data = {
      account_id: this.params.account_id,
      user_id: this.params.user_id,
      page: 0,
      limit: 10,
      my_goals: true,
      sort_by: this.goalsFilters.sortBy,
      start_date: this.filterObject?.start_date,
      end_date: this.filterObject?.end_date,
    };

    this.subscriptions.add(this.goalsService.getGoals(data).subscribe((res) => {
      this.isLoading = false;
      this.goals = res;
    }, (error) => {
         this.toastr.ShowToastr('info',error.message);
          setTimeout(() => this.router.navigate(['/profile-page']), 500);
    }))
  }
}
