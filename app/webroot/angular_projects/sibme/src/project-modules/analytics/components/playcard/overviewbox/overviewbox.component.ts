import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { HeaderService } from "@projectModules/app/services";
import { Subscription } from 'rxjs';

@Component({
	selector: 'overviewbox',
	templateUrl: './overviewbox.component.html',
	styleUrls: ['./overviewbox.component.css']
})
export class OverviewboxComponent implements OnInit, OnDestroy {

	public details;
	public translation: any = {};
	private translationSubscription: Subscription;
	constructor(private headerService: HeaderService) {
		this.translationSubscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
			this.translation = languageTranslation;
		});
	}

	ngOnInit() {
	}
	@Input() set config(val) {

		this.prepare_details(val);
	};


	private prepare_details(val) {
		if (!val) return;
		this.details = [];
		this.details.push({ iconUrl: "assets/img/analytics/huddle_icon.png", title: this.translation.huddle_created, value: val.total_account_huddles });
		this.details.push({ iconUrl: "assets/img/analytics/total_video.png", title: this.translation.analytics_video_uploaded, value: val.total_account_videos });
	    this.details.push({ iconUrl: "assets/img/analytics/resources_viewed.png", title: this.translation.analytics_leaderboard_resources_uploaded, value: val.resources_viewed });
		this.details.push({ iconUrl: "assets/img/analytics/comm_added.png", title: this.translation.analytics_leaderboard_videos_comments, value: val.total_comments_added });
		this.details.push({ iconUrl: "assets/img/analytics/discussion_posts.png", title: this.translation.analytics_leaderboard_discussion_posts, value: val.huddle_discussion_posts });
		this.details.push({ iconUrl: "assets/img/analytics/video_viewed.png", title: this.translation.analytics_video_views, value: val.total_viewed_videos });
		this.details.push({ iconUrl: "assets/img/analytics/vhu.png", title: this.translation.analytics_hours_uploaded, value: val.total_video_duration });
		this.details.push({ iconUrl: "assets/img/analytics/vhv.png", title: this.translation.analytics_hours_viewed, value: val.total_min_watched });

	}

	ngOnDestroy() {
		this.translationSubscription.unsubscribe();
	}

}
