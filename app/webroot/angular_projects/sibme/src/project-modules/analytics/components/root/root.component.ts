import { Component, OnInit, OnDestroy, ViewEncapsulation, ViewChild, ApplicationRef, NgZone } from '@angular/core';
import * as moment from "moment";
import { HeaderService } from "@projectModules/app/services";
import { ActivatedRoute, ActivatedRouteSnapshot, ChildActivationEnd, NavigationEnd, PRIMARY_OUTLET, Router, RoutesRecognized } from '@angular/router';
import { fromEvent, Subscription } from 'rxjs';
import { TrackerFiltersComponent } from '@src/project-modules/shared/components';
import { BodyService, OverviewService, SummaryService, TabsService } from '../../services';
import { TrackerFiltersService } from '@src/project-modules/shared/services/tracker-filters.service';
import { Location, LocationStrategy } from '@angular/common';
import { isEmpty } from "lodash"
import { filter, map, mergeMap, startWith, take } from 'rxjs/operators';
import { MainAnalyticsService } from '../../services/main-analytics.service';
import { MenuItem } from 'primeng/api';
import { isNullOrUndefined } from 'util';
import { decode, deepCopy } from '@src/project-modules/shared/components/common/utility';
import { environment } from '@src/environments/environment';
interface IBreadCrumb {
  label: string;
  url: string;
}
@Component({
  selector: "analytics-root",
  templateUrl: "./root.component.html",
  styleUrls: ["./root.component.css"],
  encapsulation: ViewEncapsulation.None
})
export class RootComponent implements OnInit, OnDestroy {
  private projectTitle: string = 'analytics';
  public translation: any = {};
  public translationLoaded: boolean = false;
  public fromAnalytics: boolean = true;
  public routeOnDuration: boolean = false;
  private subscriptions: Subscription = new Subscription();
  apiUrl = 'analytics_new';
  type = '';
  callsFunctionFormFilter = true;
  playerCardParams = null;
  @ViewChild(TrackerFiltersComponent) trackerFiltersComponent: TrackerFiltersComponent;
  queryParams = null;
  filterObject = null;
  isLoading = true;
  isWorSpaceAllow = false;
  private styles = [
    { id: 'analyticsGlobal', path: 'assets/analytics/css/global.css' },
    { id: 'analyticsAnimate', path: 'assets/analytics/css/animate.min.css' },
    { id: 'analyticsExport', path: 'assets/analytics/css/export.css' },
  ];
  itemsPerPage = 10;
  isSingleSelection = false;
  ROUTE_DATA_BREADCRUMB = 'breadcrumb';
  readonly home = { icon: 'pi pi-home', url: 'home' };
  menuItems = [];
  userName:any = null;
  fromParams = null;
  header_data = null;
  browserFresh = true;
   subscriptionReload =new Subscription() ;
  constructor(private headerService: HeaderService, private router: Router,
    private overviewService: OverviewService,
    private summaryService: SummaryService,
    private mainAnalyticsService: MainAnalyticsService,
    private bodyService: BodyService,
    private trackerFiltersService: TrackerFiltersService,
    private tabService: TabsService,
    private location: Location,
    private locationStrategy: LocationStrategy,
    private activatedRoute: ActivatedRoute,
    private applicationRef: ApplicationRef,
    private zone: NgZone) {
    let header_data = this.headerService.getStaticHeaderData();
    fromEvent(window, 'popstate')
      .subscribe((e) => {
        // window.location.reload();
        if(this.trackerFiltersComponent){
          this.trackerFiltersComponent.update(false);
        }
      });
    // // router.events.subscribe(() => {
    //     zone.run(() =>
    //       setTimeout(() => {
    //         this.applicationRef.tick();
    //       }, 0)
    //     );
    //   });
    //TODO: WIP
    // activatedRoute.url.subscribe(() => {
    //     this.type = activatedRoute.snapshot.firstChild.routeConfig.data.type;
    //   this.onTabChange(this.type, false);
    // });

    // router onchange
    let data = { state: this.activatedRoute.snapshot['_routerState'] };
    this.buildBreadCrumbs(data);
    this.getUnique();
    this.subscriptions.add(this.router.events.subscribe(eventData => {
      if (eventData instanceof RoutesRecognized) {
        this.buildBreadCrumbs(eventData);
        this.getUnique();
        this.browserFresh = false;
      }

    }));
    if (activatedRoute.snapshot.firstChild && activatedRoute.snapshot.firstChild.firstChild) {
      this.type = activatedRoute.snapshot.firstChild.firstChild.data.type;
    } else {
      this.type = activatedRoute.snapshot.firstChild.data.type;
    }
    this.subscriptions.add(this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        startWith(this.router)
      )
      .subscribe((event: NavigationEnd) => {

        if (activatedRoute.snapshot.firstChild.queryParams && activatedRoute.snapshot.firstChild.queryParams.from) {
          this.fromParams = decode(activatedRoute.snapshot.firstChild.queryParams.from);
          this.menuItems.splice(1, 0, this.fromParams);
          this.getUnique();
        } else {
          this.fromParams = null;
        }
        if (activatedRoute.snapshot.firstChild && activatedRoute.snapshot.firstChild.firstChild) {
          this.type = activatedRoute.snapshot.firstChild.firstChild.data.type;
        } else {
          this.type = activatedRoute.snapshot.firstChild.data.type;
        }
        this.onTabChange(this.type, false);
      }))
    //END TODO

    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      if (this.translation.account_analytics) {
        this.translationLoaded = true;
      }
    }));

    this.subscriptions.add(this.tabService.typeChange.subscribe(type => {
      if (type) {
        this.type = type;
        this.onTabChange(this.type, false);
      }
    }));

    this.subscriptions.add(this.mainAnalyticsService.pageChanged.subscribe(pageNumber => {
      if (pageNumber) {
        this.trackerFiltersComponent.setPage(pageNumber);
      }
    }));

    this.subscriptions.add(this.mainAnalyticsService.limitChanged.subscribe(limit => {
      if (limit) {
        this.trackerFiltersComponent.filters_obj.page = 1;
        this.trackerFiltersComponent.setItemsPerPage(limit);
      }
    }));
    this.subscriptions.add(this.mainAnalyticsService.sortData.subscribe(data => {
      this.trackerFiltersComponent.sortTable(data.value , data.order);
    }));

    this.subscriptions.add(this.mainAnalyticsService.routeParams.subscribe(params => {
      this.playerCardParams = params;
      if (this.trackerFiltersComponent) {
        if (isEmpty(params)) {
          this.trackerFiltersComponent.standard_ids = [];
          this.trackerFiltersComponent.framework_id = "";
          this.trackerFiltersComponent.selectedFramework = "";
          this.trackerFiltersComponent.playCardAccountId = null;
          this.trackerFiltersComponent.playCardUserId = null;
          // this.trackerFiltersComponent.update();
          this.playerCardParams = null;
        }
        //this.browserFresh is true by default and  navigating from another module , browserFresh will be false
        if (!this.browserFresh) {
          this.trackerFiltersComponent.getFiltersData();
          // setTimeout(() => {
          this.trackerFiltersComponent.update();
          // }, 500);
        }
      }
    }))

    this.subscriptions.add(this.mainAnalyticsService.playerCardLoading.subscribe(loading => {
      this.isLoading = false;
    }));

    this.subscriptions.add(this.mainAnalyticsService.breadCrumUserName.subscribe(name => {
      this.headerService.setLocalStorage('playercardUserNameForBreakCrum',name);
      this.userName = name;
    }));
  }


  ngOnInit() {
    this.styles.forEach(style => {
      this.loadCss(style);
    });
    // setTimeout(() => {
    //   this.trackerFiltersComponent.update();
    // }, 500);
    this.subscriptions.add(this.headerService.behaviorialHeaderData$.subscribe(headerData => {
      this.header_data = this.headerService.getStaticHeaderData();
      if (this.header_data.analytics_ready != undefined) {
        //   this.analytics_ready = this.header_data.analytics_ready;
        this.checkPermission(this.header_data);
      }
    }))
  }

  ngOnDestroy() {
    this.styles.forEach(style => {
      let element = document.getElementById(style.id);
      element.parentNode.removeChild(element);
    });
    this.subscriptions.unsubscribe();
      this.subscriptionReload.unsubscribe();
  }

  private loadCss(style: any) {
    let head = document.getElementsByTagName('head')[0];
    let link = document.createElement('link');
    link.id = style.id;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = style.path;
    head.appendChild(link);
  }

  onTabChange(type, shouldCallUpdate = true, url = '') {
    let urlHasParams = <any>this.getParameterByName('updateClick');
    let urlHasFrom = <any>this.getParameterByName('from');
    this.callsFunctionFormFilter = true;
    if (type == 'overview') {

      this.apiUrl = 'analytics_new';
      this.callsFunctionFormFilter = true;
      if (shouldCallUpdate) {
        this.router.navigate([url]).then(done => {
          if (done) {
            this.trackerFiltersComponent.setPage(1, false);
            this.trackerFiltersComponent.setUrl(this.apiUrl);
            this.trackerFiltersComponent.update(urlHasParams);
            this.trackerFiltersComponent.makeHuddleDropDowns();
          }

        });
      }

    }
    //playercard overview or user summary
    if (type == 'playercard-overview') {
      this.apiUrl = 'playercard-overview';
      this.callsFunctionFormFilter = false;
      if (shouldCallUpdate) {
        this.router.navigate([url], { queryParams: { from: urlHasFrom } }).then(done => {
          if (done) {
            this.trackerFiltersComponent.setPage(1, false);
            this.trackerFiltersComponent.setUrl(this.apiUrl);
            this.trackerFiltersComponent.update(urlHasParams);
            this.trackerFiltersComponent.makeHuddleDropDowns();
          }

        });
      }

    }

    if (type == 'user-summary-main') {
      this.isWorSpaceAllow = true;
      this.apiUrl = 'user_summary_report';
      this.itemsPerPage = 10;
      this.callsFunctionFormFilter = true;
      if (shouldCallUpdate) {
        this.router.navigate([url]).then(done => {
          if (done) {
            setTimeout(() => {
              this.trackerFiltersComponent.setPage(1, false);
              this.trackerFiltersComponent.setItemsPerPage(this.itemsPerPage, false);
              this.trackerFiltersComponent.setUrl(this.apiUrl);
              this.trackerFiltersComponent.update(urlHasParams);
              this.trackerFiltersComponent.makeHuddleDropDowns();
            }, 50);
          }
        });
      }
    }
    //playercard user summary type
    if (type == 'playercard-user-summary') {
      this.apiUrl = 'playercard-user-summary';
      this.callsFunctionFormFilter = false;
      if (shouldCallUpdate) {
        this.router.navigate([url], { queryParams: { from: urlHasFrom } }).then(done => {
          if (done) {
            this.trackerFiltersComponent.setPage(1, false);
            this.trackerFiltersComponent.setUrl(this.apiUrl);
            this.trackerFiltersComponent.update(urlHasParams);
            this.trackerFiltersComponent.makeHuddleDropDowns();
          }
        });
      }
    }

    if (type == 'tags') {
      this.itemsPerPage = 5;
      this.isSingleSelection = true;
      this.apiUrl = 'frequency_of_tagged_standard';
      this.callsFunctionFormFilter = true;
      if (shouldCallUpdate) {
        this.router.navigate([url]).then(done => {
          if (done) {
            setTimeout(() => {
              this.trackerFiltersComponent.setPage(1, false);
              // this.trackerFiltersComponent.setItemsPerPage(this.itemsPerPage, false);
              this.trackerFiltersComponent.setUrl(this.apiUrl);
              this.trackerFiltersComponent.update(urlHasParams);
              this.trackerFiltersComponent.makeHuddleDropDowns();
            }, 50);
          }
        });
      }
    }

    if(type == 'custom-marker') {
      this.itemsPerPage = 5;
      this.isSingleSelection = true;
      this.apiUrl = 'frequency_of_tagged_standard';
      this.callsFunctionFormFilter = true;
      if (shouldCallUpdate) {
        this.router.navigate([url]).then(done => {
          if (done) {
            setTimeout(() => {
              this.trackerFiltersComponent.setPage(1, false);
              // this.trackerFiltersComponent.setItemsPerPage(this.itemsPerPage, false);
              this.trackerFiltersComponent.setUrl(this.apiUrl);
              this.trackerFiltersComponent.update(urlHasParams);
              this.trackerFiltersComponent.makeHuddleDropDowns();
            }, 50);
          }
        });
      }
    }


    //player card tags
    if (type == 'playercard-tags') {
      this.itemsPerPage = 5;
      this.isSingleSelection = true;
      this.apiUrl = 'playercard-tags';
      this.callsFunctionFormFilter = false;
      if (shouldCallUpdate) {
        this.router.navigate([url], { queryParams: { from: urlHasFrom } }).then(done => {
          if (done) {
            setTimeout(() => {
              this.trackerFiltersComponent.setPage(1, false);
              // this.trackerFiltersComponent.setItemsPerPage(this.itemsPerPage, false);
              this.trackerFiltersComponent.setUrl(this.apiUrl);
              this.trackerFiltersComponent.update(urlHasParams);
              this.trackerFiltersComponent.makeHuddleDropDowns();
            }, 50);
          }
        });
      }
    }

    if(type == 'playercard-custommarker') {
      this.itemsPerPage = 5;
      this.isSingleSelection = true;
      this.apiUrl = 'playercard-tags';
      this.callsFunctionFormFilter = false;
      if (shouldCallUpdate) {
        this.router.navigate([url], { queryParams: { from: urlHasFrom } }).then(done => {
          if (done) {
            setTimeout(() => {
              this.trackerFiltersComponent.setPage(1, false);
              // this.trackerFiltersComponent.setItemsPerPage(this.itemsPerPage, false);
              this.trackerFiltersComponent.setUrl(this.apiUrl);
              this.trackerFiltersComponent.update(urlHasParams);
              this.trackerFiltersComponent.makeHuddleDropDowns();
            }, 50);
          }
        });
      }
    }
    if (type == 'tags' || type == 'playercard-tags' || type == 'custom-marker' || type == 'playercard-custommarker') {
      this.itemsPerPage = 5;
      this.isSingleSelection = false;
    } else {
      this.itemsPerPage = 10;
    }

  }

  getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    let regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  handleChange(data) {
    if (this.type == 'overview') {
      // data = this.ValidateData(data);
      this.summaryService.AnalyticsData(data);
      this.bodyService.BroadcastOverviewValues(data.account_overview);
    }

    if (this.type == 'user-summary-main') {
      // data = this.ValidateData(data);
      this.summaryService.AnalyticsData(data);
      // this.bodyService.BroadcastOverviewValues(data.account_overview);
    }

    if (this.type == 'tags') {
      this.bodyService.BroadcastTagsValues(data);
    }

    if (this.type == 'custom-marker') {
      this.bodyService.BroadcastTagsValues(data);
    }
    this.isLoading = false;
  }

  onLoading(loading) {
    this.mainAnalyticsService.loadingChanged.emit(loading);
  }

  exportReport(exportType, active) {
    this.trackerFiltersComponent.setExportReport(exportType, active)
  }

  filterKeyEmitter(event) {
    event['type'] = this.type;
    //   if (this.type == 'overview' || this.type == 'user-summary-main') {
    //     this.subscriptions.add(this.overviewService.getNewOverviewData(event).subscribe((data: any) => {
    //       data = this.ValidateData(data);
    //       this.summaryService.AnalyticsData(data);
    //       this.bodyService.BroadcastOverviewValues(data.account_overview);
    //     }));
    //   }

    //   if (this.type == 'tags') {
    //     this.bodyService.SetFilters(event);
    //   }


    if (event && event.updateClick) {
      event['type'] = this.type;
      this.filterObject = event;
      const urlTree = this.router.createUrlTree([], {
        relativeTo: this.activatedRoute,
        queryParams: event,
        queryParamsHandling: 'merge',
      });

      this.location.go(urlTree.toString());
    }
    setTimeout(() => {
      this.trackerFiltersService.filtersKeys.next(event);
    }, 50);

  }

  buildBreadCrumbs(eventData) {
    let event: any = eventData;
    let currentUrlPart = event.state._root;
    let currUrl = ''; //for HashLocationStrategy
    let userSummaryUserName = null;
    this.menuItems = [];
    while (currentUrlPart.children.length > 0) {
      currentUrlPart = currentUrlPart.children[0];
      let routeSnapshot = <ActivatedRouteSnapshot>currentUrlPart.value;

      let letter = currUrl.slice(-1) != '/' ? '/' : '';
      currUrl += letter + routeSnapshot.url.map(function (item) {
        return item.path;
      }).join('/');
      if((<any>routeSnapshot.data).breadcrumb == 'player_card_user_summary') {
        if(this.userName) {
          userSummaryUserName = this.userName;
        } else {
        userSummaryUserName = this.headerService.getLocalStorage('playercardUserNameForBreakCrum');
        }
      }
      if ((<any>routeSnapshot.data).breadcrumb) {
        this.menuItems.push({
          displayName: (<any>routeSnapshot.data).breadcrumb,
          url: currUrl,
          userName: userSummaryUserName,
          params: routeSnapshot.params,
          queryParams: this.filterObject ? this.filterObject : {}
        })
      }

      this.getUnique();
    }
  }

  sortTable(value, order) {
    if(order)
    this.trackerFiltersComponent.sortTable(value.sorting_key, order)
    else
    this.trackerFiltersComponent.sortTable("", order)

  }


  getUnique() {
    this.menuItems = [...new Map(this.menuItems.map(item =>
      [item['displayName'], item])).values()];
  }

  checkAndCall(clickedItem, evt) {
    if (!this.router.url.includes("playercard")) {
      if (!evt.ctrlKey) {
        if (clickedItem.displayName == 'header_list_analytics') {
          this.onTabChange('overview', true, '/analytics_angular/overview');
        }
      }
    } if (this.router.url.includes("playercard")) {
      if (!evt.ctrlKey) {
        if (clickedItem.displayName == 'analytics_user_profile') {
          this.onTabChange('playercard-overview', true, '/analytics_angular/playercard/' + this.playerCardParams.account_id + '/' + this.playerCardParams.user_id + '/' + this.playerCardParams.start_date + '/' + this.playerCardParams.end_date + '/' + 'overview');
        }
      }
    }

  }

  urlParameter() {
    let url = window.location.href,
      retObject = {},
      parameters;

    if (url.indexOf('?') === -1) {
      return null;
    }

    url = url.split('?')[1];

    parameters = url.split('&');

    for (let i = 0; i < parameters.length; i++) {
      retObject[parameters[i].split('=')[0]] = decodeURIComponent(parameters[i].split('=')[1]);
    }

    if (!retObject) {
      return {}
    }
    return retObject;
  }

  checkPermission(sessionData) {
    let account_id = this.header_data.user_current_account.accounts.account_id;
    let user_id = this.header_data.user_current_account.users_accounts.user_id;
    let start_date = moment().subtract(1, 'years').format('YYYY-MM-DD');
    let end_date = moment().format('YYYY-MM-DD');
    let folder_type = 2;

    if (sessionData.user_permissions.UserAccount.permission_view_analytics == 1 || sessionData.user_permissions.UserAccount.role_id == 115) {
      if (sessionData.user_permissions.UserAccount.role_id == 120) {
        this.router.navigate([`/analytics_angular/playercard/${account_id}/${user_id}/${start_date}/${end_date}`]);
        return;
      }

    }
    else {
      this.router.navigate(['/page-not-found']);
      return;
    }

  }

  pointToReports(){
    const link = `${environment.baseUrl}/home/trackers/coaching`;
    if (link) window.open(link, "_blank");
  }
}
