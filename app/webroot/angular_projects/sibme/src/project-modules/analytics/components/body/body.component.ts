import { Component, OnInit, ViewChild, OnDestroy, TemplateRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ShowToasterService } from '@projectModules/app/services';

import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { BsLocaleService } from "ngx-bootstrap/datepicker";
import { esLocale } from "ngx-bootstrap/locale";
import { defineLocale } from "ngx-bootstrap/chronos";

import * as _ from 'lodash';
import * as moment from "moment";
import { Subscription } from 'rxjs';

import { HeaderService } from "@projectModules/app/services";
import { TabsService, OverviewService, BodyService, SummaryService } from "@analytics/services";
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import { TrackerFiltersService } from '@src/project-modules/shared/services/tracker-filters.service';

@Component({
	selector: 'app-body',
	templateUrl: './body.component.html',
	styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit, OnDestroy {

	public sub_accounts;
	public settings;
	public sub_1;
	public selectedItem;
	public radioModel;
	public filters: filter = {};
	public bsConfig: Partial<BsDatepickerConfig>;
	public settings2;
	public currentTab;
	public selectedSub1;
	public selectedSub2;
	public OverviewData;
	public dropdowns;
	public framework_settings;
	public huddle_settings;
	public standards_settings;
	public isLoggedIn: boolean = false;
	public params;
	private accountChanged;
	public header_color;
	public primery_button_color;
	public secondry_button_color;
	public header_data;
	public translation: any = {};
	public event: any = {};
	public translationLoaded: boolean = false;
	private subscriptions: Subscription = new Subscription();
	private queryParams: any;
	private assessment_array = [];
	public analytics_ready: boolean = true;
	public framework_id: any;
	// public modalRef: BsModalRef;
	// public loadModalRef: BsModalRef;
	// public deleteModalRef: BsModalRef;
	// public savedFilterArray:any=[];
	// public selectedloadFilter:any;
	// public searchPreference:string='';
	// public filterName:string='';
	// public currentFilter:string='Default Filter';
	// public isCached=0;
	// public lastCachedDate:any;
	// public loadFromFilter=false;
	// public isForceRefresh=false;
	// public selectedControl:string='month';
	// public itemTodel:any;
	// public saveAndUpdate=false;
	// public filterNameSU='';
	// private isFirstTime=false;
	@ViewChild('dp', { static: false }) dp;
	filtersData = null;
	public analyticsTabs = GLOBAL_CONSTANTS.ANALYTICS_TABS;
	constructor(
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private tabsService: TabsService,
		private bodyService: BodyService,
		private overviewService: OverviewService,
		private summaryService: SummaryService,
		private headerService: HeaderService,
		private toastr: ShowToasterService,
		private localeService: BsLocaleService,
		private trackerFilterService: TrackerFiltersService
		// private modalService: BsModalService
	) {

		this.header_data = this.headerService.getStaticHeaderData();
		if (this.header_data.analytics_ready != undefined) this.analytics_ready = this.header_data.analytics_ready;
		if(Object.keys(this.header_data).length != 0 && this.header_data.constructor === Object){
			console.log("sds:", this.header_data);
		
			this.checkPermission(this.header_data);
	
		}
		
		this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
			this.translation = languageTranslation;
			if (this.translation.account_analytics) {
				this.translationLoaded = true;
				this.initiazlizeSettingWithTranslation();
			}
		}));


		this.activatedRoute.queryParams.subscribe(queryParams => {
			// this.trackerFilterService.filtersKeys.emit(queryParams);
			this.queryParams = queryParams;
			this.framework_id = queryParams.framework_id;


			if (queryParams.subAccount) this.bodyService.UpdateFilters("subAccount", Number(queryParams.subAccount));
			this.bodyService.UpdateFilters('folder_type', queryParams.folder_type);


			if (queryParams.framework_id) this.bodyService.UpdateFilters("framework_id", queryParams.framework_id);
			if (queryParams.standards) {
				this.bodyService.UpdateFilters("standards", queryParams.standards);
			}
		});

		if (this.translation.account_analytics && this.translationLoaded && this.analytics_ready) {
			this.load_accounts();
		}


		// Dynamic Button Colors Start
		this.header_color = this.header_data.header_color;
		this.primery_button_color = this.header_data.primery_button_color;
		this.secondry_button_color = this.header_data.secondry_button_color;

		if (this.header_data.language_translation.current_lang == 'es') defineLocale(this.header_data.language_translation.current_lang, esLocale);
		this.localeService.use(this.header_data.language_translation.current_lang);

		this.trackerFilterService.filtersKeys.subscribe(filtersKeys=>{
		})

	}

	public showdp() {

		this.dp.nativeElement.click();

	}

	private handleParams() {
		this.activatedRoute.params.subscribe(p => {
			let that = this;
			if (p.tab_id) {
				this.params = p;

			}
		})
	}

	ngOnInit() {
		this.filtersData = JSON.parse(localStorage.getItem('analytics_filter'));
		if (this.header_data.analytics_ready) {
			// this.getAllFilter();
			this.handleParams();
			let filters = this.bodyService.GetFilters();


			if (this.queryParams.start_date && this.queryParams.end_date) {
				filters.start_date = this.queryParams.start_date;
				filters.end_date = this.queryParams.end_date;
			}

			if (!this.dropdowns) {
				this.dropdowns = {};
			}

			if (filters && (filters.start_date || filters.duration)) {

				this.filters = JSON.parse(JSON.stringify(filters));


				if (this.filters.duration && this.filters.duration != -1) {
					this.filters.duration = function (d) {

						return d == 1 ? 1 : d == 2 ? 3 : d == 3 ? 6 : 12;

					}(this.filters.duration)
				} else {
					this.filters.date_range = [new Date(this.filters.start_date), new Date(this.filters.end_date)];
					let start_date = this.getFormattedDate(this.filters.date_range[0]);
					let end_date = this.getFormattedDate(this.filters.date_range[1]);

					let diff = moment(end_date).diff(moment(start_date), 'months', true);
					diff = Math.ceil(diff);

					if (diff <= 12) {



						if (diff == 1 || diff == 3 || diff == 6 || diff == 12) {
							this.filters.duration = diff;
							this.bodyService.UpdateFilters('duration', diff);
						} else {
							this.filters.duration = -1;
							this.bodyService.UpdateFilters('duration', -1);
						}


						this.bodyService.UpdateFilters("start_date", start_date);
						this.bodyService.UpdateFilters("end_date", end_date);


					} else {
						this.filters.duration = -1;
						this.bodyService.UpdateFilters('duration', -1);
						this.bodyService.UpdateFilters("start_date", start_date);
						this.bodyService.UpdateFilters("end_date", end_date);
					}

				}


			}
			else {

				this.filters = { "duration": 12 };
				this.selectDuration(12);

			}

			this.init();
		}

	}

	init() {

		this.bsConfig = Object.assign({}, { containerClass: "theme-default" });

		this.updateData(true);
		this.RunSubscribers();
		this.sub_1 = [
			{ item_id: 1, item_text: 'Sub Account 1' },
			{ item_id: 2, item_text: 'Sub Account 2' },
			{ item_id: 3, item_text: 'Sub Account 3' },
			{ item_id: 4, item_text: 'Sub Account 4' },
			{ item_id: 5, item_text: 'Sub Account 5' }
		];

	}

	public FilterCharts() {

		this.tabsService.Submit();

	}

	private RunSubscribers() {

		this.tabsService.Tabs.subscribe(tab => this.ActivateTab(tab));
	}

	private ActivateTab(tab) {

		this.currentTab = tab;

	}

	public onStandardsSelectSelect(standard) {
		if (!standard) return;
		if (standard.length == 0) {
			return
		} else {
			let current_standards = [];
			current_standards = standard.map((s) => {

				return s.account_tag_id;

			}).join();

			this.bodyService.UpdateFilters("standards", current_standards);
		}

	}

	public onFrameworkSelectSelect(framework) {

		this.bodyService.UpdateFilters("framework_id", framework.account_tag_id);
		this.bodyService.deleteFilter("standards");
		this.filters.standards = [];
		this.bodyService.LoadStandards(framework.account_tag_id).subscribe(data => {
			this.PrepareAndAssignStandards(data);

		})

	}

	public PrepareAndAssignStandards(standards) {
		standards.forEach(s => {

			s.account_tag_id = s.value;
			s.tag_title = s.label;

		});

		let current_standards = this.bodyService.GetFilter("standards");

		if (!current_standards && this.queryParams.standards) {
			this.bodyService.UpdateFilters("standards", this.queryParams.standards);
		}

		this.chooseStandard(standards);
		this.dropdowns.standards = [];
		this.dropdowns.standards = standards;


	}

	public onHuddleSelectSelect(huddle) {


		if (Number(huddle.id) == 2 || Number(huddle.id) == 4) {

			this.bodyService.UpdateFilters('folder_type', 2);

			if (Number(huddle.id) == 4) {

				this.bodyService.UpdateFilters('coachee_view', 2);

			}
			else {
				this.bodyService.UpdateFilters('coachee_view', 1);
			}

		} else {
			this.bodyService.deleteFilter('coachee_view');
			this.bodyService.UpdateFilters('folder_type', huddle.id);
		}




	}


	private handleDropdowns(data) {
		if (data.frameworks) {
			this.PreareFrameworks(data.frameworks);
			this.dropdowns.frameworks = data.frameworks;
			this.chooseFramework(this.dropdowns.frameworks);
			this.dropdowns.huddles_filters = this.ObjectoArray(data.huddles_filters).reverse();
			this.selectHuddle();
			this.dropdowns.standards = data.standards;
			this.chooseStandard(this.dropdowns.standards);
		}


	}
	private chooseStandard(standards) {
		let current_standards = this.bodyService.GetFilter("standards");
		if (current_standards) {

			current_standards = current_standards.split(",");
			this.filters.standards = [];
			current_standards.forEach(s => {

				let index = _.findIndex(standards, { account_tag_id: Number(s) });
				if (index > -1) {
					this.filters.standards.push(standards[index]);
				}
			});
		}

	}


	private chooseFramework(frameworks) {
		let account_tag_id = this.bodyService.GetFilter("framework_id");


		if (frameworks[0] && account_tag_id && account_tag_id != 'undefined') {

			let index = _.findIndex(frameworks, { account_tag_id: Number(account_tag_id) });

			if (index > -1) {
				this.onFrameworkSelectSelect({ account_tag_id: account_tag_id });
				this.dropdowns.selectedFramework = [frameworks[index]];
			}


		} else {
			if (frameworks.length <= 0) {
				return;
			}
			this.bodyService.UpdateFilters("framework_id", frameworks[0].account_tag_id);
			this.dropdowns.selectedFramework = [frameworks[0]];
		}

	}

	private selectHuddle() {
		if (!this.dropdowns || !this.dropdowns.huddles_filters) {
			return;
		}

		if (this.filters.folder_type) {

			let index = _.findIndex(this.dropdowns.huddles_filters, { id: this.filters.folder_type });
			this.dropdowns.selectedHuddle = [this.dropdowns.huddles_filters[index]];
			return;

		} else {

			let coaching_index = _.findIndex(this.dropdowns.huddles_filters, { id: "2" });

			if (coaching_index > -1) {

				this.bodyService.UpdateFilters("folder_type", "2");
				this.dropdowns.selectedHuddle = [this.dropdowns.huddles_filters[coaching_index]];

			} else {
				let assessment_index = _.findIndex(this.dropdowns.huddles_filters, { id: "3" });

				if (assessment_index > -1) {

					this.bodyService.UpdateFilters("folder_type", "3");
					this.dropdowns.selectedHuddle = [this.dropdowns.huddles_filters[assessment_index]];

				} else {

					this.bodyService.UpdateFilters("folder_type", "1");
					this.dropdowns.selectedHuddle = [this.dropdowns.huddles_filters[0]];

				}

			}

		}

	}

	private ObjectoArray(object) {

		let arr = [];

		Object.keys(object).forEach((k) => {

			arr.push({ id: k, value: object[k] });

		});

		if (arr[2] && arr[3]) {
			let t = arr[2];
			arr[2] = arr[3];
			arr[3] = t;
		}

		return arr;

	}

	private PreareFrameworks(fr) {

		if (fr) {
			fr.forEach(f => { f.val_id = f.account_id + "-" + f.account_tag_id });
		}


	}

	private updateDropdowns(accId) {
		this.bodyService.LoadFilterDropdowns(accId).subscribe((data: any) => {
			this.handleDropdowns(data)
		});
	}

	private load_accounts() {
		let that = this;

		let intervalId = setInterval(function () {

			let sessionData: any;

			sessionData = that.headerService.getStaticHeaderData();
			let all_accounts = that.translation.all_accounts;
			if (sessionData && sessionData.user_current_account) {

				clearInterval(intervalId);

				that.bodyService.LoadFilterDropdowns().subscribe((data: any) => {
					that.handleDropdowns(data)

					if (that.framework_id) {
						let framework = data.frameworks.findIndex(element => element.account_tag_id == that.framework_id);

						if (framework == -1) {
							that.router.navigate(['/profile-page'])
						}
					}


				});



				that.bodyService.getChildAccounts().subscribe((data: any) => {

					let _data = [{ "id": "-1", "company_name": all_accounts }];
					let accounts = JSON.parse(JSON.stringify(_data));
					if (data.length > 1) {
						that.sub_accounts = accounts.concat(data);
					} else {
						that.sub_accounts = data;
					}

					let subAccount = that.bodyService.GetFilter("subAccount");

					if (subAccount) {

						let index = _.findIndex(that.sub_accounts, { id: subAccount });
						if (index != -1) {
							that.selectedItem = [that.sub_accounts[index]];
						} else {
							that.selectedItem = data.length > 1 ? _data : [data[0]];
							if (data.length <= 1) {
								that.bodyService.UpdateFilters("subAccount", data[0].id)
							}

						}


					} else {
						that.selectedItem = data.length > 1 ? _data : [data[0]];
						if (data.length <= 1) {
							that.bodyService.UpdateFilters("subAccount", data[0].id)
						}

					}

				});

			}


		}, 500);


	}

	public updateData(loadOnPageInitialise?, isPL?) {
		if (this.event.length != 0 && this.queryParams.tab == "overview") {
			this.onItemSelect(this.event);
		}
		this.bodyService.UpdatePressed();
		let filters = this.bodyService.GetFilters();

		if (!filters.start_date) {
			filters = this.initializeAPIDataWithoutAnyFilter();
		} else {
			let sessionData: any = this.headerService.getStaticHeaderData();
			filters.filter_type = (filters.duration) ? "custom" : "default";
			filters.account_id = sessionData.user_current_account.users_accounts.account_id;
			filters.user_id = sessionData.user_current_account.User.id;

		}


		if (!filters.subAccount) {

			filters.subAccount = "";

		}

		if ((!filters.duration || filters.duration == -1) && !filters.start_date) {

			this.toastr.ShowToastr('info',this.translation.analytics_please_select_durtion);
			return;

		}

		// if(this.isForceRefresh){
		// 	this.saveAndUpdate=false;
		// 	filters.forceRefresh=this.isForceRefresh;
		// 	this.bodyService.UpdateFilters("filter_id", this.selectedloadFilter != undefined ? this.selectedloadFilter.id : this.selectedloadFilter=0);
		// }
		// else if(!filterUpdate){
		// 	this.loadFromFilter=false;
		// 	this.bodyService.UpdateFilters("filter_id", 0);
		// 	this.saveAndUpdate=false;
		// }
		// else if(filterUpdate==='update'){
		// 	this.bodyService.UpdateFilters("is_updated", 1);
		// 	this.bodyService.UpdateFilters("filter_id", 0);
		// }
		// else if(filterUpdate==='loadFilter'){
		// 	this.bodyService.UpdateFilters("filter_id", this.selectedloadFilter.id);
		// 	if(this.selectedloadFilter.id>0)
		// 	this.bodyService.UpdateFilters("is_updated", 0);
		// 	else
		// 	this.bodyService.UpdateFilters("is_updated", 1);
		// }
		// if(this.bodyService.GetFilter('filter_id')==-1)
		// this.bodyService.UpdateFilters("is_updated", 0);
		if (!loadOnPageInitialise) this.updateRouteBasedOnFilters(filters);
		if (isPL) this.GetPLAssesmentArray();
		if (this.filters.date_range && this.filters.date_range.length === 2 && this.filters.date_range[0] != null && this.filters.date_range[1] != null) {
			this.subscriptions.add(this.overviewService.getNewOverviewData(filters).subscribe((data: any) => {
				data = this.ValidateData(data);
				this.summaryService.AnalyticsData(data);
				this.bodyService.BroadcastOverviewValues(data.account_overview);
				//   this.isCached=data.is_cached;
				//   this.lastCachedDate=data.cached_datetime;
				//   filters.forceRefresh=this.isForceRefresh=false;
				//   if(filterUpdate==='update')
				//   this.saveAndUpdate=false;
			}));
		}
		else {
			this.toastr.ShowToastr('info',this.translation.analytics_invalid_date_range);
		}
		let tab_id;




		this.FilterCharts();
	}
	GetPLAssesmentArray() {
		let obj = {
			framework_id: this.bodyService.GetFilter("framework_id"),
			account_id: this.header_data.user_current_account.accounts.account_id
		}
		this.bodyService.getAssesmentArray(obj).subscribe((data: any) => {
			this.UpdatePLData(data)
		});
	}

	private UpdatePLData(assessment_array) {

		if (assessment_array) {

			this.bodyService.StorePLData(assessment_array);

		}

	}

	private getPadding(n) {

		return n < 10 ? "0" + n : n + "";

	}

	private getFormattedDate(date) {

		return (this.getPadding(date.getMonth() + 1)) + "/" + this.getPadding(date.getDate()) + "/" + date.getFullYear();

	}

	public deselectDuration(event, data): void {
		if (event == null) return;

		if (event[0] !== this.filters.date_range[0] || event[1] !== this.filters.date_range[1]) this.filters.date_range = event;

		if (event[0] == null) {
			// if(this.isFirstTime){
			// this.isFirstTime=false;
			this.toastr.ShowToastr('info',this.translation.analytics_invalid_start_date);
			// }
			return;
		}

		if (event[1] == null) {
			// if(this.isFirstTime){
			// this.isFirstTime=false;
			this.toastr.ShowToastr('info',this.translation.analytics_invalid_end_date);
			// }
			return;
		}

		if (!event[0] || !event[1]) {
			// if(this.isFirstTime){
			// 	this.isFirstTime=false;
			this.toastr.ShowToastr('info',this.translation.analytics_start_and_end_dates);
			// }
			return;
		}

		if (moment(event[0]).isAfter(event[1])) {
			// if(this.isFirstTime){
			// 	this.isFirstTime=false;
			// setTimeout(() => {
			// this is done because of this.filters.date_range getting date even if we clear array
			this.filters.date_range = [];
			// }, 200);
			this.toastr.ShowToastr('info',this.translation.analytics_strt_date_check);
			// }
			return;
		}

		if (!this.filters) {
			this.filters = {};
		}
		if (Array.isArray(this.filters.date_range) && this.filters.date_range.length > 0) {

			let start_date = this.getFormattedDate(this.filters.date_range[0]);
			let end_date = this.getFormattedDate(this.filters.date_range[1]);

			let diff = moment(end_date).diff(moment(start_date), 'months', true);

			diff = Math.ceil(diff);

			if (diff <= 12) {



				if (diff == 1 || diff == 3 || diff == 6 || diff == 12) {
					this.filters.duration = diff;
					this.bodyService.UpdateFilters('duration', diff);
				} else {
					this.filters.duration = -1;
					this.bodyService.UpdateFilters('duration', -1);
				}

				this.bodyService.UpdateFilters("start_date", start_date);
				this.bodyService.UpdateFilters("end_date", end_date);

			} else {
				this.filters.duration = -1;
				this.bodyService.UpdateFilters('duration', -1);
				this.bodyService.UpdateFilters("start_date", start_date);
				this.bodyService.UpdateFilters("end_date", end_date);
			}
		}

		// setTimeout(() => {
		// 	this.isFirstTime=true;
		// }, 300);
	}

	public selectDuration(dur) {
		let val = (dur == 1) ? 1 : (dur == 3) ? 2 : (dur == 6) ? 3 : 4;
		if (!this.filters.duration) {
			this.filters.duration = dur;
		}
		this.bodyService.UpdateFilters("duration", -1);
		if (!this.filters) {
			this.filters = {};
		}
		if (dur) {
			let date_range = [moment().subtract(dur, 'months'), moment()];
			this.filters.date_range = [date_range[0].toDate(), date_range[1].toDate()];
			let start_date = date_range[0].format("MM/DD/YYYY");
			let end_date = date_range[1].format("MM/DD/YYYY");
			this.bodyService.UpdateFilters("start_date", start_date);
			this.bodyService.UpdateFilters("end_date", end_date);
			this.filters.duration = dur;
			return;
		}
		this.filters.duration = dur;
		this.filters.date_range = [];
		this.bodyService.deleteFilter("start_date");
		this.bodyService.deleteFilter("end_date");
	}

	public onItemSelect(event) {
		if (this.event.length != 0 && this.queryParams.tab == "overview") {
			this.event = {};
		}
		this.event = event;
		this.accountChanged = true;
		this.bodyService.UpdateFilters("subAccount", (event.id == -1) ? "" : event.id);

		this.dropdowns.selectedHuddle = [];
		this.dropdowns.selectedFramework = [];
		this.filters.standards = [];

		this.bodyService.deleteFilter("framework_id");
		this.bodyService.deleteFilter("folder_type");
		this.bodyService.deleteFilter("standards");
		this.updateDropdowns(event.id);

	}

	public onItemChange(oldVal, model) {
		this.selectedItem = [oldVal];
	}

	public onHuddleDeselect(oldVal) {

		this.dropdowns.selectedHuddle = [oldVal];

	}

	public onFrameworkDeselect(oldVal) {

		this.dropdowns.selectedFramework = [oldVal];

	}

	private initializeAPIDataWithoutAnyFilter() {
		let headerData = this.headerService.getStaticHeaderData();
		return {
			'subAccount': '',
			'duration': '4',
			'filter_type': 'default',
			'user_current_account': headerData.user_current_account
		}
	}

	private initiazlizeSettingWithTranslation() {
		this.settings = {
			"singleSelection": true,
			"selectAllText": this.translation.analytics_select_all,
			"unSelectAllText": this.translation.analytics_un_select_all,
			"allowSearchFilter": true,
			"closeDropDownOnSelection": true,
			"idField": "id",
			"textField": "company_name"
		};

		this.framework_settings = {
			"singleSelection": true,
			"selectAllText": this.translation.analytics_select_all,
			"unSelectAllText": this.translation.analytics_un_select_all,
			"allowSearchFilter": true,
			"closeDropDownOnSelection": true,
			"idField": "account_tag_id",
			"textField": "tag_title"
		};

		this.huddle_settings = {
			"singleSelection": true,
			"selectAllText": this.translation.analytics_select_all,
			"unSelectAllText": this.translation.analytics_un_select_all,
			"allowSearchFilter": true,
			"closeDropDownOnSelection": true,
			"idField": "id",
			"textField": "value"
		};

		this.settings2 = {
			"singleSelection": false,
			"selectAllText": this.translation.analytics_select_all,
			"unSelectAllText": this.translation.analytics_un_select_all,
			"allowSearchFilter": true,
			"itemsShowLimit": 2,
			"closeDropDownOnSelection": false,
			"idField": "item_id",
			"textField": "item_text"
		};

		this.standards_settings = {
			"singleSelection": false,
			"selectAllText": this.translation.analytics_select_all,
			"unSelectAllText": this.translation.analytics_un_select_all,
			"allowSearchFilter": true,
			"itemsShowLimit": 2,
			"closeDropDownOnSelection": false,
			"idField": "account_tag_id",
			"textField": "tag_title"
		};
	}

	private updateRouteBasedOnFilters(filters: any) {
		let urlQueryParams: any = {};
		if (filters.subAccount) urlQueryParams.subAccount = filters.subAccount;
		else urlQueryParams.subAccount = null;

		if (filters.start_date) urlQueryParams.start_date = filters.start_date;
		else urlQueryParams.start_date = null;
		if (filters.end_date) urlQueryParams.end_date = filters.end_date;
		else urlQueryParams.end_date = null;
		if (filters.folder_type) urlQueryParams.folder_type = filters.folder_type;
		else urlQueryParams.folder_type = null;
		if (filters.coachee_view) urlQueryParams.coachee_view = filters.coachee_view;
		else urlQueryParams.coachee_view = null;
		if (filters.framework_id) urlQueryParams.framework_id = filters.framework_id;
		else urlQueryParams.framework_id = null;
		if (filters.standards) urlQueryParams.standards = filters.standards;
		else urlQueryParams.standards = null;

		this.router.navigate([], {
			relativeTo: this.activatedRoute,
			queryParams: urlQueryParams,
			queryParamsHandling: 'merge',
		});
	}
	ValidateData(data) {
		if (data.account_overview.active_users < 0) data.account_overview.active_users = 0;
		if (data.account_overview.total_account_huddles < 0) data.account_overview.total_account_huddles = 0;
		if (data.account_overview.total_account_users < 0) data.account_overview.total_account_users = 0;
		if (data.account_overview.total_account_videos < 0) data.account_overview.total_account_videos = 0;

		if (data.account_overview.total_comments_added < 0) data.account_overview.total_comments_added = 0;
		if (data.account_overview.total_live_min_watched < 0) data.account_overview.total_live_min_watched = 0;
		if (data.account_overview.total_live_sessions < 0) data.account_overview.total_live_sessions = 0;
		if (data.account_overview.total_live_vide_duration < 0) data.account_overview.total_live_vide_duration = 0;

		if (data.account_overview.total_min_watched < 0) data.account_overview.total_min_watched = 0
		if (data.account_overview.total_video_duration < 0) data.account_overview.total_video_duration = 0
		if (data.account_overview.total_viewed_videos < 0) data.account_overview.total_viewed_videos = 0;
		if (data.account_overview.total_viewers < 0) data.account_overview.total_viewers = 0;

		data.user_summary.forEach(u => {
			if (u.workspace_upload_counts < 0) u.workspace_upload_counts = 0;
			if (u.shared_upload_counts < 0) u.shared_upload_counts = 0;
			if (u.workspace_videos_viewed_count < 0) u.workspace_videos_viewed_count = 0;
			if (u.video_upload_counts < 0) u.video_upload_counts = 0;
			if (u.videos_viewed_count < 0) u.videos_viewed_count = 0;

			if (u.total_hours_uploaded < 0) u.total_hours_uploaded = 0;
			if (u.total_hours_viewed < 0) u.total_hours_viewed = 0;
			if (u.scripted_video_observations < 0) u.scripted_video_observations = 0;
			if (u.scripted_observations < 0) u.scripted_observations = 0;
			if (u.huddle_created_count < 0) u.huddle_created_count = 0;

			if (u.workspace_comments_initiated_count < 0) u.workspace_comments_initiated_count = 0;
			if (u.workspace_resources_uploaded < 0) u.workspace_resources_uploaded = 0;
			if (u.workspace_resources_viewed < 0) u.workspace_resources_viewed = 0;
			if (u.comments_initiated_count < 0) u.comments_initiated_count = 0;
			if (u.replies_initiated_count < 0) u.replies_initiated_count = 0;

			if (u.documents_uploaded_count < 0) u.documents_uploaded_count = 0;
			if (u.documents_viewed_count < 0) u.documents_viewed_count = 0;
			if (u.web_login_counts < 0) u.web_login_counts = 0;
			if (u.library_shared_upload_counts < 0) u.library_shared_upload_counts = 0;
			if (u.library_upload_counts < 0) u.library_upload_counts = 0;

			if (u.library_videos_viewed_count < 0) u.library_videos_viewed_count = 0;
			if (u.huddle_discussion_created < 0) u.huddle_discussion_created = 0;
			if (u.huddle_discussion_posts < 0) u.huddle_discussion_posts = 0;
			if (u.scripted_notes_shared_upload_counts < 0) u.scripted_notes_shared_upload_counts = 0;

		});
		return data;
	}
	// public saveFilterDialog(template: TemplateRef<any>, file) {
	// 	if(this.filters.date_range !=null && this.filters.date_range[0] !=null && Object.keys(this.filters.date_range).length==2){
	// 	if(this.filters.duration==-1){
	// 		this.filterName='';
	// 		this.searchPreference='date';
	// 		this.selectedControl='date';
	// 	}
	// 	if(this.selectedControl=='month'){
	// 		this.filterName=this.filters.duration+' Months'
	// 		this.searchPreference='month'
	// 	}
	// 	else if (this.selectedControl=='date'){
	// 		this.filterName='Custom Date - '+ this.getFormattedDate(this.filters.date_range[0]) + ' to '+ this.getFormattedDate(this.filters.date_range[1])
	// 		this.searchPreference='date'
	// 	}
	// 	this.modalRef = this.modalService.show(template, { class: "modal-md sfMaxcls",backdrop:'static' });
	//   }
	//   else
	//   this.toastr.ShowToastr('info',this.translation.analytics_invalid_start_date);
	// }

	// public hideSaveFilterModal() {
	// 	this.modalRef.hide();

	// 	setTimeout(() => {
	// 		this.filterName='';
	// 		this.searchPreference='';
	// 	}, 500);

	//   }

	// public hideChangeFilterModal(){
	// 	this.modalRef.hide();
	//   }


	// public getAllFilter(){

	// 	this.bodyService.getAllFilters().subscribe((data:any)=>{
	// 		if(data.success){
	// 		this.savedFilterArray=data.data;
	// 		this.addAllAccount();

	// 		}
	// 	});
	//   }

	//  public deleteSavedFilter(item){
	// 	this.bodyService.deleteSavedFilter(this.itemTodel).subscribe((data:any)=>{
	// 		if(data.success){
	// 			this.savedFilterArray=this.savedFilterArray.filter(x=> x.id!=this.itemTodel.id);
	// 			this.toastr.ShowToastr('info',data.messages);
	// 			this.deleteModalRef.hide();
	// 		}
	// 	});
	//   }

	// public loadFilter(){
	// 	if(this.selectedloadFilter){
	// 		if(this.selectedloadFilter.id==-1){
	// 			this.bodyService.UpdateFilters("subAccount", '');
	// 		}
	// 		let seletedObj ={id:this.selectedloadFilter.subAccount,company_name:this.selectedloadFilter.account_name};
	// 		this.selectedItem=[seletedObj];
	// 		if(this.selectedloadFilter.search_preference=='month'){

	// 			this.selectDuration(this.selectedloadFilter.months);
	// 		}
	// 		else{
	// 			let date_range = [moment(this.selectedloadFilter.start_date).add('days', 1), moment(this.selectedloadFilter.end_date).add('days', 1)];
	// 			var startDate = new Date(date_range[0].toDate());
	// 			startDate.setDate(startDate.getDate() -1);
	// 			var endDate = new Date(date_range[1].toDate());
	// 			endDate.setDate(endDate.getDate() -1);

	// 			this.filters.date_range = [startDate, endDate];
	// 			let diff = moment(date_range[1]).diff(moment(date_range[0]), 'months', true);
	// 			diff = Math.ceil(diff);
	// 			if (diff <= 12) {
	// 				if (diff == 1 || diff == 3 || diff == 6 || diff == 12) {
	// 					this.filters.duration = diff;
	// 					this.bodyService.UpdateFilters('duration', diff);
	// 				} else {
	// 					this.filters.duration = -1;
	// 					this.bodyService.UpdateFilters('duration', -1);
	// 				}
	// 				this.bodyService.UpdateFilters("start_date", this.selectedloadFilter.start_date);
	// 				this.bodyService.UpdateFilters("end_date", this.selectedloadFilter.end_date);
	// 				this.bodyService.UpdateFilters("duration", 4);

	// 			} else {
	// 				this.filters.duration = -1;
	// 				this.bodyService.UpdateFilters('duration', -1);
	// 				this.bodyService.UpdateFilters("start_date", this.selectedloadFilter.start_date);
	// 				this.bodyService.UpdateFilters("end_date", this.selectedloadFilter.end_date);
	// 			}

	// 		}
	// 		this.event=seletedObj;
	// 		this.loadFromFilter=true;
	// 		this.updateData(false,'loadFilter');
	// 		this.currentFilter=this.selectedloadFilter.filter_name;
	// 		this.loadModalRef.hide();
	// 	}
	// 	else{
	// 		this.toastr.ShowToastr('info',this.translation.analytics_select_filter);
	// 	}
	// }

	// public saveNewFilter(update){
	// 	if(this.filterName!='' || this.filterName.trim()!=''){
	// 		if(this.searchPreference == 'month' || this.searchPreference=='date') {
	// 	let sessionData: any = this.headerService.getStaticHeaderData();
	// 	let user_account=sessionData.user_current_account;
	// 	let start_date= this.getFormattedDate(this.filters.date_range[0]);
	// 	let end_date=	this.getFormattedDate(this.filters.date_range[1]);

	// 	  let obj={
	// 		filter_name:this.filterName,
	// 		subAccount:this.selectedItem[0].id,
	// 		months:this.filters.duration,
	// 		start_date:start_date,
	// 		end_date:end_date,
	// 		search_preference:this.searchPreference,
	// 		user_current_account:user_account
	// 	  }
	// 	  this.bodyService.saveNewFilter(obj).subscribe((data:any)=>{
	// 		if(data.success){
	// 			this.toastr.ShowToastr('success',data.messages);
	// 			if(update){
	// 				this.filterNameSU=this.filterName;
	// 				this.selectedloadFilter={id:data.filter_id};
	// 				this.updateData(false,'loadFilter');
	// 			}
	// 			this.getAllFilter();
	// 			this.modalRef.hide();
	// 			this.filterName='';
	// 			this.searchPreference='';

	// 		}
	// 		else{
	// 			this.toastr.ShowToastr('error',data.messages.filter_name);
	// 		}
	// 	  });
	// 	}
	// 	else{
	// 		this.toastr.ShowToastr('error',this.translation.analytics_select_preference);
	// 	}
	// }
	// 	else{
	// 		this.toastr.ShowToastr('error',this.translation.analytics_new_filter_name);
	// 	}

	//   }

	// public loadFilterDialog(template: TemplateRef<any>, file) {

	// 	this.loadModalRef = this.modalService.show(template, { class: "modal-md  maxcls",backdrop:'static' });
	//   }

	//   public deleteDialog(template: TemplateRef<any>, data) {
	// 	this.loadModalRef.hide();
	// 	this.deleteModalRef = this.modalService.show(template, { class: "modal-md  deleteModalcls",backdrop:'static' });
	//   }

	// public forceRefreshData(){
	// 	this.isForceRefresh=true;
	// 	this.isCached=0;
	// 	this.updateData(false);
	// 	setTimeout(() => {
	// 		this.isForceRefresh=false;
	// 	}, 200);
	//   }

	// public addAllAccount(){
	// 	this.savedFilterArray.unshift(
	// 		{	id:-1,
	// 			filter_name:'All Accounts',
	// 			subAccount:-1,
	// 			months:12,
	// 			start_date:'',
	// 			end_date:'',
	// 			search_preference:'month',
	// 			account_name:'All Accounts'

	// 		});
	// }
	// public controlCheck(data){
	// 	this.selectedControl=data;
	// }

	// public isFirst(){
	// 	this.isFirstTime=true;
	// }

	checkPermission(sessionData) {
		let account_id = this.header_data.user_current_account.accounts.account_id;
		let user_id = this.header_data.user_current_account.users_accounts.user_id;
		let start_date = moment().subtract(1, 'years').format('YYYY-MM-DD');
		let end_date = moment().format('YYYY-MM-DD');
		let folder_type = 2;

		if (sessionData.user_permissions.UserAccount.permission_view_analytics == 1 || sessionData.user_permissions.UserAccount.role_id == 115) {
			if (sessionData.user_permissions.UserAccount.role_id == 120) {
				this.router.navigate([`/analytics_angular/playcard/${account_id}/${user_id}/${start_date}/${end_date}/${folder_type}`]);
				return;
			}

		}
		else {
			this.router.navigate(['/page-not-found']);
			return;
		}

	}

	handleChange(event) {
	}

	filterKeyEmitter(event) {
		this.setQueryParams(event);
	}

	onSearch(event) {
		this.setQueryParams(event);
	}

	setQueryParams(event) {
		let tab = this.currentTab ? this.currentTab : 1;
		let selectedRouteTab = this.analyticsTabs.find(item => item.value == tab);
		event['tab'] = selectedRouteTab.label;
		// localStorage.setItem('analytics_filter', JSON.stringify(event));
		this.router.navigate([], {
			relativeTo: this.activatedRoute,
			queryParams: event,
			queryParamsHandling: 'merge',
		});
		// this.trackerFilterService.filtersKeys.emit(event);
	}

	ngOnDestroy() {
		this.subscriptions.unsubscribe();

		this.bodyService.deleteAllFilters()
	}

	paramsToObject(entries) {
		const result = {}
		for (const [key, value] of entries) { // each 'entry' is a [key, value] tupple
			result[key] = value;
		}
		return result;
	}

}
interface filter {

	[key: string]: any

}

