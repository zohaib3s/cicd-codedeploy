import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HeaderService } from "@projectModules/app/services";
import *  as _ from 'lodash';
import { BodyService, SummaryService, TabsService } from "@analytics/services";
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
	selector: 'user-summary',
	templateUrl: './user-summary.component.html',
	styleUrls: ['./user-summary.component.css']
})
export class UserSummaryComponent implements OnInit, OnDestroy {

	@ViewChild('pUsertable', { static: true }) pUsertable: any;

	public cars;
	public carsOriginal;
	public UsersOriginal;
	public cols;
	public dt;
	public gb;
	public isLoading: boolean = true;
	public users;
	private allowAjax: boolean = true;
	public header_data;
	public translation: any = {};
	private subscriptions: Subscription = new Subscription();
	private queryParams: any;
	public searchText;
	private analyticsData:any;
	constructor(
		private bodyService: BodyService,
		private summaryService: SummaryService,
		private headerService: HeaderService,
		private router: Router,
		private activatedRoute: ActivatedRoute,
		private tabService: TabsService
	) {
		 this.tabService.typeChange.emit('user-summary-main')
		this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
			this.translation = languageTranslation;

			this.activatedRoute.queryParams.subscribe(queryParams => {
				this.queryParams = queryParams;
			});

			this.cols = [
				{ field: 'account_name', header: this.translation.analytics_name },
				{ field: 'company_name', header: this.translation.analytics_account_name },
				{ field: 'email', header: this.translation.analytics_email },
				{ field: 'user_role', header: this.translation.analytics_user_role },
				{ field: 'user_type', header: this.translation.analytics_user_type },
				{ field: 'workspace_upload_counts', header: this.translation.analytics_videos_upload_workspace }, //ok
				{ field: 'video_upload_counts', header: this.translation.analytics_videos_upload_huddle },
				{ field: 'shared_upload_counts', header: this.translation.analytics_videos_shared_to_huddles },
				{ field: 'scripted_notes_shared_upload_counts', header: this.translation.scripted_notes_shared_upload_counts },
				{ field: 'library_shared_upload_counts', header: this.translation.analytics_videos_shared_to_library },
				{ field: 'library_upload_counts', header: this.translation.analytics_videos_uploaded_to_library },//ok
				{ field: 'videos_viewed_count', header: this.translation.analytics_huddle_video_viewed },
				{ field: 'workspace_videos_viewed_count', header: this.translation.analytics_video_viewed_in_workspace },
				{ field: 'library_videos_viewed_count', header: this.translation.analytics_library_videos_viewed },
				{ field: 'total_hours_uploaded', header: this.translation.analytics_hours_uploaded },
				{ field: 'total_hours_viewed', header: this.translation.analytics_hours_viewed },
				{ field: 'huddle_created_count', header: this.translation.analytics_huddle_created }, // ok
				{ field: 'workspace_comments_initiated_count', header: this.translation.analytics_workspace_video_notes },
				{ field: 'scripted_observations', header: this.translation.analytics_workspace_scripted_notes },
				{ field: 'workspace_resources_uploaded', header: this.translation.analytics_workspace_resources_uploaded },
				{ field: 'workspace_resources_viewed', header: this.translation.analytics_workspace_resources_viewed },
				{ field: 'comments_initiated_count', header: this.translation.analytics_huddle_video_comments },
				{ field: 'replies_initiated_count', header: this.translation.analytics_huddle_video_replies },
				{ field: 'documents_uploaded_count', header: this.translation.analytics_resources_uploaded },
				{ field: 'documents_viewed_count', header: this.translation.analytics_resources_viewed },
				{ field: 'huddle_discussion_created', header: this.translation.huddle_discussion_created },
				{ field: 'huddle_discussion_posts', header: this.translation.huddle_discussion_posts },
				{ field: 'web_login_counts', header: this.translation.analytics_total_logins },
			];

		}));
	}

	ngOnInit() {
		let filters=this.bodyService.GetFilters();
		this.summaryService.analyticsData.subscribe(data=>{
        console.log("UserSummaryComponent -> ngOnInit -> data", data)
			this.analyticsData=data;
			this.analyticsData.start_date = this.reformatDateString(filters.start_date);;
			this.analyticsData.end_date = this.reformatDateString(filters.end_date);
			this.LoadNewUserSummary(this.analyticsData);
		})
		this.gb = '';
		this.header_data = this.headerService.getStaticHeaderData();
		this.bodyService.FilterSubmitPressed.subscribe(d => {
			if (d) {
				this.pUsertable.first = 0; this.gb = "";
			}
		});
		if(this.analyticsData){
			// this.LoadNewUserSummary(this.analyticsData);
		}
	}

	private preventDuplication() {

		let that = this;
		setTimeout(() => {

			that.allowAjax = true;

		}, 1000);

	}

	private LoadNewUserSummary(analyticsData) {

			let sessionData: any;
			sessionData = this.headerService.getStaticHeaderData();

			if (sessionData && sessionData.user_current_account) {
				analyticsData.user_summary=analyticsData.user_summary.filter(x=> x.is_user_active==1)
					this.prepare_users(analyticsData);
					this.users = _.clone(analyticsData);
					this.UsersOriginal = _.clone(analyticsData);
					this.isLoading = false;
                    console.log("UserSummaryComponent -> LoadNewUserSummary -> this.isLoading", this.isLoading)
						if (this.queryParams.userSummaryPage)
						 this.pUsertable.first = ((this.queryParams.userSummaryPage - 1) * 10);
			}

	}

	public ExportExcel() {

		let filters = this.bodyService.GetFilters();
		filters.filter_type = (filters.start_date) ? "custom" : "default";
		filters.rows = "export";
		filters.page = "";
		filters.search_text = "";
		filters.sidx = "account_name";
		filters.sord = "asc";

		this.summaryService.GetFile(filters).subscribe(data => this.handleFile(data));

	

	}

	private handleFile(result) {


		var blob = new Blob([result]);
		var link = document.createElement('a');
		link.href = window.URL.createObjectURL(blob);
		link.download = "users.csv";
		link.click();

	}

	private prepare_users(users) {

		users.folder_type = this.bodyService.GetFilter("folder_type");

		users.folder_type = users.folder_type || "1";
		

		users.user_summary.forEach(u => {
			u.workspace_upload_counts = '<span  data-link=' + this.paramLink(u, "workspace_upload_counts", users.start_date, users.end_date, 4, u.user_id, u.account_id, 3) + '>' + u.workspace_upload_counts + "</span>";//ok
			u.video_upload_counts = '<span  data-link=' + this.paramLink(u, "video_upload_counts", users.start_date, users.end_date, 4, u.user_id, u.account_id, 1) + '>' + u.video_upload_counts + " </span>"; //ok
			u.shared_upload_counts = '<span  data-link=' + this.paramLink(u, "shared_upload_counts", users.start_date, users.end_date, 22, u.user_id, u.account_id, 1) + '>' + u.shared_upload_counts + "</span>"; //ok
			u.workspace_videos_viewed_count = '<span  data-link=' + this.paramLink(u, "workspace_videos_viewed_count", users.start_date, users.end_date, 11, u.user_id, u.account_id, 3) + '>' + u.workspace_videos_viewed_count + " </span>"; //ok
			u.workspace_upload_counts = '<span  data-link=' + this.paramLink(u, "workspace_upload_counts", users.start_date, users.end_date, 4, u.user_id, u.account_id, 3) + '>' + u.workspace_upload_counts + "</span>"; // twice
			u.video_upload_counts = '<span  data-link=' + this.paramLink(u, "video_upload_counts", users.start_date, users.end_date, 4, u.user_id, u.account_id, 1) + '>' + u.video_upload_counts + " </span>"; // twice
			u.shared_upload_counts = '<span  data-link=' + this.paramLink(u, "shared_upload_counts", users.start_date, users.end_date, 22, u.user_id, u.account_id, 1) + '>' + u.shared_upload_counts + "</span>";// twice
			u.workspace_videos_viewed_count = '<span  data-link=' + this.paramLink(u, "workspace_videos_viewed_count", users.start_date, users.end_date, 11, u.user_id, u.account_id, 3) + '>' + u.workspace_videos_viewed_count + " </span>";// twice
			u.videos_viewed_count = '<span  data-link=' + this.paramLink(u, "videos_viewed_count", users.start_date, users.end_date, 11, u.user_id, u.account_id, 1) + '>' + u.videos_viewed_count + "</span>";//ok
			u.total_hours_uploaded = '<span  data-link=' + this.paramLink(u, "total_hours_uploaded", users.start_date, users.end_date, 24, u.user_id, u.account_id, 2) + '>' + u.total_hours_uploaded + "</span>"; //ok
			u.total_hours_viewed = '<span  data-link=' + this.paramLink(u, "total_hours_viewed", users.start_date, users.end_date, 25, u.user_id, u.account_id, 2) + '>' + u.total_hours_viewed + "</span>";
			u.scripted_video_observations = '<span  data-link=' + this.paramLink(u, "scripted_video_observations", users.start_date, users.end_date, 20, u.user_id, u.account_id, 0) + '>' + u.scripted_video_observations + "</span>"; //ok
			u.scripted_observations = '<span  data-link=' + this.paramLink(u, "scripted_observations", users.start_date, users.end_date, 23, u.user_id, u.account_id, 0) + '>' + u.scripted_observations + "</span>"; //ok
			u.huddle_created_count = '<span  data-link=' + this.paramLink(u, "huddle_created_count", users.start_date, users.end_date, 1, u.user_id, u.account_id, 0) + '>' + u.huddle_created_count + "</span>"; // ok
			u.workspace_comments_initiated_count = '<span  data-link=' + this.paramLink(u, "workspace_comments_initiated_count", users.start_date, users.end_date, 5, u.user_id, u.account_id, 3) + '>' + u.workspace_comments_initiated_count + "</span>"; //ok
			u.scripted_observations = '<span  data-link=' + this.paramLink(u, "scripted_observations", users.start_date, users.end_date, 23, u.user_id, u.account_id, 3) + '>' + u.scripted_observations + "</span>";// twice
			u.workspace_resources_uploaded = '<span  data-link=' + this.paramLink(u, "workspace_resources_uploaded", users.start_date, users.end_date, 3, u.user_id, u.account_id, 3) + '>' + u.workspace_resources_uploaded + "</span>";
			u.workspace_resources_viewed = '<span  data-link=' + this.paramLink(u, "workspace_resources_viewed", users.start_date, users.end_date, 13, u.user_id, u.account_id, 3) + '>' + u.workspace_resources_viewed + "</span>";
			u.comments_initiated_count = '<span  data-link=' + this.paramLink(u, "comments_initiated_count", users.start_date, users.end_date, 5, u.user_id, u.account_id, 1) + '>' + u.comments_initiated_count + "</span>"; //ok
			u.replies_initiated_count = '<span  data-link=' + this.paramLink(u, "replies_initiated_count", users.start_date, users.end_date, 8, u.user_id, u.account_id, 3) + '>' + u.replies_initiated_count + "</span>"; //ok
			u.documents_uploaded_count = '<span  data-link=' + this.paramLink(u, "documents_uploaded_count", users.start_date, users.end_date, 3, u.user_id, u.account_id, 1) + '>' + u.documents_uploaded_count + "</span>"; //ok
			u.documents_viewed_count = '<span  data-link=' + this.paramLink(u, "documents_viewed_count", users.start_date, users.end_date, 13, u.user_id, u.account_id, 1) + '>' + u.documents_viewed_count + "</span>";//ok
			u.web_login_counts = '<span  data-link=' + this.paramLink(u, "web_login_counts", users.start_date, users.end_date, 9, u.user_id, u.account_id, 0) + '>' + u.web_login_counts + "</span>"; // ok
			u.library_shared_upload_counts = '<span  data-link=' + this.paramLink(u, "library_shared_upload_counts", users.start_date, users.end_date, 22, u.user_id, u.account_id, 2) + '>' + u.library_shared_upload_counts + "</span>"; //ok
			u.library_upload_counts = '<span  data-link=' + this.paramLink(u, "library_upload_counts", users.start_date, users.end_date, 2, u.user_id, u.account_id, 2) + '>' + u.library_upload_counts + "</span>"; // ok
			u.library_videos_viewed_count = '<span  data-link=' + this.paramLink(u, "library_videos_viewed_count", users.start_date, users.end_date, 11, u.user_id, u.account_id, 2) + '>' + u.library_videos_viewed_count + "</span>";//ok
			u.videos_viewed_count = '<span  data-link=' + this.paramLink(u, "videos_viewed_count", users.start_date, users.end_date, 11, u.user_id, u.account_id, 1) + '>' + u.videos_viewed_count + "</span>"; //ok
			u.huddle_discussion_created = '<span  data-link=' + this.paramLink(u, "huddle_discussion_created", users.start_date, users.end_date, 6, u.user_id, u.account_id, 1) + '>' + u.huddle_discussion_created + "</span>"; //ok
			u.huddle_discussion_posts = '<span  data-link=' + this.paramLink(u, "huddle_discussion_posts", users.start_date, users.end_date, 8, u.user_id, u.account_id, 1) + '>' + u.huddle_discussion_posts + "</span>"; //ok
			u.scripted_notes_shared_upload_counts = '<span  data-link=' + this.paramLink(u, "scripted_notes_shared_upload_counts", users.start_date, users.end_date, 26, u.user_id, u.account_id, 1) + '>' + u.scripted_notes_shared_upload_counts + "</span>";// ok

		});
		
	}

	public selectRow(rowData) {
		this.users.user_summary.forEach((u) => u.isSelected = false);
		rowData.isSelected = !Boolean(rowData.isSelected);

	}

	private paramLink(u, prop, startDate, endDate, type, userid, account_id, workspace_huddle_library = 0) {
		var subAccount = account_id;
		if (subAccount == '')
			subAccount = 0;
		var re = /\//gi;
		u[prop + "_link"] = ["../details", userid, subAccount, startDate, endDate, type, workspace_huddle_library];
		return "";
	}

	public OnSearchChange(searchString){
		this.users.user_summary = this.search(this.UsersOriginal.user_summary,searchString);
		this.users.records=this.users.user_summary.length;
	}

	search(array, text) {
		text = text.toLowerCase();
		return array.filter(function (o) {
			return ['account_name','company_name','user_name','email','user_role','user_type'].some(function (k) {
				return o[k].toString().toLowerCase().indexOf(text) !== -1;
			});
		});

	}
	private reformatDateString(s) {
		if(s){
		let b = s.split(/\D/);
		return b[2]+'-'+b[0]+'-'+b[1]; // Date Format YYYY-MM-DD
	  }
	}

	private udpateSearchQueryInRoute(searchQuery: string) {
		this.router.navigate([], {
			relativeTo: this.activatedRoute,
			queryParams: { search: searchQuery || null, userSummaryPage: null },
			queryParamsHandling: 'merge'
		});
	}

	ngOnDestroy() {
		this.subscriptions.unsubscribe();
	}

}
