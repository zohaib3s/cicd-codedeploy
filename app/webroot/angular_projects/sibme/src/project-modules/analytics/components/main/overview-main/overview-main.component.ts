import { Component, OnInit } from '@angular/core';
import { TabsService } from '@src/project-modules/analytics/services';
import { HeaderService } from '@src/project-modules/app/services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-overview-main',
  templateUrl: './overview-main.component.html',
  styleUrls: ['./overview-main.component.css']
})
export class OverviewMainComponent implements OnInit {
  navTabsLoaded = true;
  private subscriptions: Subscription = new Subscription();
  enableLeaderBoard = true;
  public translations:any;
  constructor(
    private tabService: TabsService,
    public headerService: HeaderService,
    ) {

    this.tabService.typeChange.emit('overview')
    this.subscriptions.add(this.headerService.behaviorialHeaderData$.subscribe(headerData => {
      this.enableLeaderBoard = Boolean(Number(headerData.enableleaderboard));
      if(headerData && headerData.user_permissions && headerData.user_permissions.roles && +headerData.user_permissions.roles.role_id == 115 ){
        this.enableLeaderBoard = <any>+headerData.user_permissions.UserAccount.admins_can_view_leaderboard;
      } 
    }));

    this.subscriptions = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
    });
  }

  ngOnInit(): void {
  }
  
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}
