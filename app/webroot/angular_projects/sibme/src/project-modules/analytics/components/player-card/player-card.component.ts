import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, startWith } from 'rxjs/operators';
import { MainAnalyticsService } from '../../services/main-analytics.service';

@Component({
  selector: 'app-player-card',
  templateUrl: './player-card.component.html',
  styleUrls: ['./player-card.component.css']
})
export class PlayerCardComponent implements OnInit {
  private subscriptions: Subscription = new Subscription();
  constructor(
		private activatedRoute: ActivatedRoute,
     public router: Router,
    private mainAnalyticsService: MainAnalyticsService
  ) { 

  }

  ngOnInit() {   


    this.subscriptions.add(this.activatedRoute.params.subscribe(params => {
    let accountId = params['account_id'];
    let userId =  params['user_id'];
    setTimeout(() => {
      this.mainAnalyticsService.routeParams.emit(params);
    }, 500);
  }));

  }

ngOnDestroy(): void {
  //Called once, before the instance is destroyed.
  //Add 'implements OnDestroy' to the class.
  setTimeout(() => {
     this.mainAnalyticsService.routeParams.emit({}); 
  }, 500);
  this.subscriptions.unsubscribe();
}
}
