import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import { HeaderService, SocketService } from '@src/project-modules/app/services';
import * as moment from 'moment';
import { BsModalService, ModalDirective, ModalOptions } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { AddGoalModalComponent, DeleteComponent } from "@goals/modals";
import { GoalsFilters } from '@src/project-modules/goals/models/goals-filters.model';
import { MainService } from '@src/project-modules/video-page/services';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { defineLocale } from "ngx-bootstrap/chronos";
import { esLocale } from "ngx-bootstrap/locale";
import { ActivatedRoute, Router } from '@angular/router';
import { encodeCompress } from '@src/project-modules/shared/components/common/utility';
import { ShowToasterService } from '@projectModules/app/services';
import { GoalsService, EditGoalService } from "@goals/services";
import { MainAnalyticsService } from '@src/project-modules/analytics/services/main-analytics.service';
@Component({
  selector: 'app-playercard-goals',
  templateUrl: './playercard-goals.component.html',
  styleUrls: ['./playercard-goals.component.css']
})
export class PlayercardGoalsComponent implements OnInit {
  public goals: any;
  loaded = true;
  private userCurrAcc: any;
  public goalAltName: string;
  private headerData: any;
  @Input() userConfig: any;
  @Input() goalsData: any;
  defaultGoalFilter: string;
  public goalsTypeByValue: any = GLOBAL_CONSTANTS.GOALS_TYPE_BY_VALUE;
  public translation: any = {};
  public goalsFilters: GoalsFilters = new GoalsFilters();
  private taAddEditGoalsKey: string = '';
  private USER_ROLES = GLOBAL_CONSTANTS.USER_ROLES;
  public enableGoals = false;
  playercardSortDataKey: string = '';
  playercardSortTableKey: string = '';
  breadCrumUserName:string = '';
  public goalsSortBy: any = JSON.parse(JSON.stringify(GLOBAL_CONSTANTS.GOAL_SORT_BY)); // deep copy, so that constant default value must not be changed
  private subscriptions: Subscription = new Subscription();
  config = {
    id: '',
    currentPage: 1,
    itemsPerPage: 10,
    totalItems: 20
  };
  params = null;
  public sortData: any;
  public storageValue: any = {
    sortBy: {},
    // goalFilter: {},
    // goalType: [],
    // goalCategory: [],
    // startDate: '',
    // endDate: ''
  };
  sortByAsc: boolean = true;
  goalDetailsObj: {};
  @ViewChild('childModal',{static:false}) childModal: ModalDirective;
  constructor(
    public headerService: HeaderService,
    private goalsService: GoalsService,
    private modalService: BsModalService,
    private mainService: MainService,
    private router: Router,
    private mainAnalyticsService: MainAnalyticsService,
    private activatedRoute: ActivatedRoute,
    public editGoalService: EditGoalService,
    private toastr: ShowToasterService,
    private socketService: SocketService,
    private localeService: BsLocaleService,
  ) {
 
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.goalAltName = this.headerService.getGoalsAltName();
    }));

    this.subscriptions.add(this.headerService.behaviorialHeaderData$.subscribe(headerData => {
      this.headerData = headerData;
      this.enableGoals = Boolean(Number(headerData.user_current_account.accounts.enable_goals)) && headerData.user_current_account.roles.role_id != this.USER_ROLES.VIEWER.ID; // goals are enables for current account AND current user is not viewer
    }));
    this.subscriptions.add(this.activatedRoute.parent.params.subscribe(activatedRouteParams => {
      this.params =Object.assign({},(activatedRouteParams));
    }));
  }
  ngOnChanges() {
    if (this.goalsData) {
      this.ngOnInit();
    }
  }
  ngOnInit() {
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
    if (this.headerData.language_translation.current_lang == 'es') defineLocale(this.headerData.language_translation.current_lang, esLocale);
    this.localeService.use(this.headerData.language_translation.current_lang);
    this.playercardSortDataKey = `playerCardSortData-${this.userCurrAcc.User.id}`;
    this.taAddEditGoalsKey = `${GLOBAL_CONSTANTS.LOCAL_STORAGE.GOAL.ADD_EDIT_TA}${this.userCurrAcc.accounts.account_id}-${this.userCurrAcc.User.id}`;
    this.subscribeSockets();
      if (this.headerService.getLocalStorage(this.playercardSortDataKey) != null || this.headerService.getLocalStorage(this.playercardSortDataKey) != undefined) {
        this.sortData = this.headerService.getLocalStorage(this.playercardSortDataKey);
        this.storageValue = this.sortData;
        if (Object.keys(this.sortData.sortBy).length === 0) {
          this.storageValue.sortBy = { T_KEY: 'default_view', VALUE: 'default_view', active: true }
        }
        if(this.sortData.sortBy.VALUE == "default_view") {
          this.goalsFilters.sortBy = ""
        } else {
          this.goalsFilters.sortBy = this.sortData.sortBy.VALUE;
        }
        this.goalsSortBy.forEach(item => {
          if (item.VALUE === this.sortData.sortBy.VALUE) {
            item.active = true;
          } else {
            item.active = false;
          }
        });
      } else {
        this.goalsSortBy[0].active = true; // setting default value to true
      }

          this.config.totalItems = this.goalsData.total_goals;
          this.goals = this.goalsData;
          const goalsData = this.formatGoalsArray(this.goalsData.data);
          this.goals.data = goalsData;

          // used for sorting
          this.goals.data.map((obj) => {
            let g_type = obj.goal_type == 1 ? 'd-template' : (obj.goal_type == 2 ? 'a-account' : (obj.goal_type == 3 ? 'b-group' : (obj.goal_type == 4 ? 'c-individual' : '')))
            obj.g_type = g_type;

            let published_date = obj.publish_date != null ? obj.publish_date : 'AAA';
            obj.published_date = published_date;

            obj.g_percentage = Number(obj.percentage ? (obj.percentage.toString()).replace('%', '') : -5);
           if (obj.g_percentage && obj.is_incomplete != undefined) {
              if (obj.g_percentage == 0 && obj.is_incomplete) {
                obj.g_percentage = -3;
              }
            } else {
              if (obj.is_incomplete) {
                obj.g_percentage = -2
              } else {
                obj.g_percentage = -4;
              }
            }

          });
          this.goals.data = this.formatGoalsArray(this.goals.data);

          this.restoreAddEditGoalDataFromLS(this.goals.data);
          // Sort Data according to column header Like Title
          this.playercardSortTableKey = `playerCardSortTable-${this.userCurrAcc.User.id}`;
          if (this.headerService.getLocalStorage(this.playercardSortTableKey)) {
            var sortTableData = this.headerService.getLocalStorage(this.playercardSortTableKey);
            sortTableData.sortByAsc ? this.sortByAsc = false : this.sortByAsc = true;
            this.sortTable(sortTableData.parm);
          }
        
  }

  public useTemplate(goal: any) {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-container-700',
      initialState: {
        useTemplate: false,
        templateId: goal.id
      }
    };

   this.modalService.show(AddGoalModalComponent, config);
  }

  navigateToGoalWork(goal) {

    if(goal.routerLink === '') {
      this.childModal.show();
      return;
    }
    var goalDetailsObj = {
      account_id: this.headerData.user_current_account.accounts.account_id,
      user_id: this.headerData.user_current_account.users_accounts.user_id,
      goal_id: goal.id,
      goal_type: goal.goal_type
    };
    this.mainAnalyticsService.breadCrumUserName.subscribe((name)=> {
      this.breadCrumUserName = name;
    })
    if(this.breadCrumUserName) { } else {
      this.breadCrumUserName = this.headerService.getLocalStorage('playercardUserNameForBreakCrum');
    }
    let queryParamsFrom = { displayName: 'analytics_user_profile', url: this.router.url, userName : this.breadCrumUserName};

    this.editGoalService.getGoalDetails(goalDetailsObj).subscribe((res : any)=> {
      if(res.goal_permission && res.success) {
        if(goal.queryParams) {
          this.router.navigate([goal.routerLink], { queryParams: { from: encodeCompress(queryParamsFrom), previewMode: goal.queryParams.previewMode } });
        } else {
          this.router.navigate([goal.routerLink], { queryParams: { from: encodeCompress(queryParamsFrom) } });
        }
      } else {
        this.childModal.show();
      }
    })
  }
  
  hideChildModal(): void {
    this.childModal.hide();
  }
  navigateToEditDelete(goalId) {

    if(this.breadCrumUserName) { } else {
      this.breadCrumUserName = this.headerService.getLocalStorage('playercardUserNameForBreakCrum');
    }

    let queryParamsFrom = { displayName: 'analytics_user_profile', url: this.router.url, userName : this.breadCrumUserName };
    this.router.navigate(['/goals/edit', goalId], { queryParams: { from: encodeCompress(queryParamsFrom) } });
  }

  /** Local storage functions starts */
  private restoreAddEditGoalDataFromLS(goals: any) {
    const taAddEditGoals = this.headerService.getLocalStorage(this.taAddEditGoalsKey); 

    if (Array.isArray(taAddEditGoals)) {
      taAddEditGoals.forEach(taAddEditGoal => {
        const goalIndex = goals.findIndex(goal => goal.id == taAddEditGoal.goal_id);

        if (goalIndex > -1) {
          goals[goalIndex].tryAgain = true;
          goals[goalIndex].uuid = (taAddEditGoal.uuid) ? taAddEditGoal.uuid : null;
          goals[goalIndex].goal_id = taAddEditGoal.goal_id;
          goals[goalIndex].goal_type = taAddEditGoal.goal_type;
          goals[goalIndex].title = taAddEditGoal.title;
          goals[goalIndex].desc = taAddEditGoal.desc;
        } else if (taAddEditGoal.uuid) {
          taAddEditGoal.tryAgain = true;
          taAddEditGoal.uuid = taAddEditGoal.uuid;
          taAddEditGoal.goal_id = taAddEditGoal.goal_id;
          goals.unshift(taAddEditGoal);
        }
      });
    }
  }

  public retryGoal(goal: any) {
    if (!goal.isProcessing) {
      goal.isProcessing = true;

      const goalData: any = {
        goal_id: goal.goal_id,
        uuid: (goal.uuid) ? goal.uuid : null,
        goal_type: goal.goal_type,
        title: goal.title,
        desc: goal.desc,
        start_date: goal.start_date,
        end_date: goal.end_date,
        account_id: this.userCurrAcc.users_accounts.account_id,
        user_id: this.userCurrAcc.users_accounts.user_id
      };

      this.editGoalService.updateGoal(goalData).subscribe(() => {
        const key = (goalData.goal_id) ? 'goal_id' : 'uuid';
        this.removeAddEditGoalDataFromLS(goalData[key], key);
      }, () => {
        setTimeout(() => {
          goal.isProcessing = false;
        }, 200);

      });
    }
  }

  private removeAddEditGoalDataFromLS(id: number, key: string) {
    const taAddEditGoals = this.headerService.getLocalStorage(this.taAddEditGoalsKey);

    if (Array.isArray(taAddEditGoals)) {
      const goalIndex = taAddEditGoals.findIndex(taAddEditGoal => taAddEditGoal[key] == id);
      if (goalIndex > -1) {
        taAddEditGoals.splice(goalIndex, 1);
        this.headerService.setLocalStorage(this.taAddEditGoalsKey, taAddEditGoals);
      }
    }
  }


  /** Socket functionality section start */
  private subscribeSockets() {
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(`goals_${this.userCurrAcc.accounts.account_id}`).subscribe(res => {

      switch (res.event) {
        case "goal_created":
          this.processGoalCreatedEdited(res);
          break;
        case "goal_edited":
          this.processGoalCreatedEdited(res);
          break;
        case "goal_deleted":
          this.processGoalDeleted(res.item_id);
          break;
      }
    }));
  }
  sortTable(parm) {

    if (this.sortByAsc == true) {
      this.sortByAsc = false;
      this.goals.data.sort((a, b) => {
        if (typeof a[parm] != "number") {
          if (a[parm].toLowerCase() < b[parm].toLowerCase())
            return -1;
          if (a[parm] > b[parm])
            return 1;
          return -1;
        }
        else {
          if (a[parm] < b[parm])
            return -1;
          if (a[parm] > b[parm])
            return 1;
          return -1;
        }
      }
      );
    }
    else {
      this.sortByAsc = true;
      this.goals.data.sort((a, b) => {
        if (typeof a[parm] != "number") {
          if (a[parm].toLowerCase() > b[parm].toLowerCase())
            return -1;
          if (a[parm] < b[parm])
            return 1;
          return -1;
        } else {
          if (a[parm] > b[parm])
            return -1;
          if (a[parm] < b[parm])
            return 1;
          return -1;
        }
      });
    }

   // this.playercardSortTableKey = `playerCardSortTable-${this.userCurrAcc.User.id}`;
    // Add to local storage so that user can view data when he re-visited the page
    this.headerService.setLocalStorage(this.playercardSortTableKey, { parm: parm, sortByAsc: this.sortByAsc });
  }
  public goalsExportData(to) {
    let userId = this.params.user_id;
    let sessionData: any = this.headerService.getStaticHeaderData();
    var obj = {
      "account_id": this.userCurrAcc.users_accounts.account_id,
      "user_id": userId,
      "export_type": to,
      "my_goals": true,
      "start_date": this.userConfig?.start_date,
      "end_date": this.userConfig?.end_dates,
      "sort_by": this.goalsFilters.sortBy,
      "current_lang": sessionData.language_translation.current_lang
    }
    this.mainService.goalsExportData(obj, to);
  }
  private formatGoalsArray(data: any) {
    data.forEach(goal => {
      // if (goal.goal_type === 3 || (goal.goal_type === 4 && goal.owners_updated.length === 1)) {
        // Calculate completion percentage
        // if (goal.total_items && goal.total_items > 0) {
        //   goal.percentage = Math.round((goal.completed_items / goal.total_items) * 100);
        //   goal.percentage = `${goal.percentage}%`;
        // } else goal.percentage = '0%';
      // }
      goal.percentage = goal.progress;
      let totalDoneItems = 0;
        if (goal.goal_type === 3 || (goal.goal_type === 4 && goal.owners_updated.length === 1)) {
        // Calculate completion percentage
        if (goal.total_items && goal.total_items > 0) {
            const share = (100/goal.total_items).toFixed(0);
            goal.action_items.forEach(item => {
              if(item.item_feedbacks){
                if(item.item_feedbacks.is_done == 1){
                  //goal.percentage += parseInt(share);
                  totalDoneItems ++;
                }else{
                  item.current_value = item.item_feedbacks.current_value;
                  const itemProgress = this.checkItemProgress(item);
                  if(typeof itemProgress !== "boolean" && typeof itemProgress === "number"){
                    goal.percentage += (parseInt(share)*itemProgress/100);
                  }
                }
              }
            }); 
          if(goal.total_items > 0 && totalDoneItems == goal.total_items && goal.is_complete == 1){
              goal.percentage = 100;
           } 
          goal.percentage = Math.floor(goal.percentage);
          goal.percentage = `${goal.percentage}%`;
        } else goal.percentage = '0%';
      }


      goal.is_incomplete = !goal.is_complete && moment(goal.end_date).toDate().setHours(0, 0, 0, 0) < moment().toDate().setHours(0, 0, 0, 0); // compare only date, not time
      goal.is_progress = !goal.is_complete && moment(goal.end_date).toDate().setHours(0, 0, 0, 0) > moment().toDate().setHours(0, 0, 0, 0); // goal is in progress if goal is not complete and due date should be greater than current data, compare only date, not time

      if (goal.is_progress) {
        if (goal.action_items.length) {
          goal.is_progress = false;
          goal.action_items.forEach(item => {
            if (item.item_feedbacks) {
              if (item.item_feedbacks.current_value || item.item_feedbacks.reflection) {
                goal.is_progress = true;
              }
            }
            if (item.submitted_evidence?.length) {
              goal.is_progress = true;
            }
          });
        }
      }

      goal.routerLink = this.getRouterLink(goal);
      goal.disabled = (!goal.routerLink) ? true : false;
      goal.canEditDelete = this.checkEditDeletePermission(goal);

    });

    return data;
  }

  checkItemProgress(item){
    if(item.add_measurement == 1){
      if(item.start_value == item.current_value){
      return 0;
      }else if(item.end_value > item.start_value){
        if(item.current_value <= item.start_value){
          return 0;
        } else if(item.current_value >= item.end_value){
          return 100;
        }else{
          let percentage=  Math.floor((item.current_value-item.start_value)/(item.end_value - item.start_value)*100);
          return percentage;
        }

      }else if(item.start_value > item.end_value){
        if(item.current_value >= item.start_value){
          return 0;
        }else if(item.current_value <= item.end_value){
          return 100;
        }else{
          return Math.floor((item.start_value - item.current_value)/(item.start_value - item.end_value) * 100);
        }
      }
      return true;
    }else{
      return false;
    }
  }

  private checkEditDeletePermission(goal: any) {
    const userId = this.userCurrAcc.User.id;
    const isCollaborator = goal.collaborators.find(collaborator => collaborator.user_id == userId); // user is collaborator
    const isCollaboratorWithPermissionE = isCollaborator && isCollaborator.permission.indexOf(2) >= 0; // user is collaborator with edit permission
    const isCreator = goal.created_by == userId;

    return isCreator || isCollaboratorWithPermissionE;
  }
  deleteGoal(goal) {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-container-600',
      initialState: { goal }
    };
    this.modalService.show(DeleteComponent, config);
  }
  parseDate(date) {
    // parse the date to replace the offset as it is not parsed by safari
    if (date) {
      return date.replace(/\s/g, "T")
    } else return false;

  }

  private getRouterLink(goal: any) {

    if (goal.goal_type == 1) { // template goal
      goal.queryParams = { previewMode: 'navigated-from-list' };
      return `/goals/view/${goal.id}`;
    }

    const userId = this.userCurrAcc.User.id;
    const isCollaborator = goal.collaborators.find(collaborator => collaborator.user_id == userId); // user is collaborator
    const isCollaboratorWithPermissionVFR = isCollaborator && (isCollaborator.permission.indexOf(1) >= 0 || isCollaborator.permission.indexOf(3) >= 0 || isCollaborator.permission.indexOf(4) >= 0); // user is collaborator with view or add evidence or review feedback permission
    const isCollaboratorWithPermissionE = isCollaborator && isCollaborator.permission.indexOf(2) >= 0; // user is collaborator with edit permission
    const isOwner = goal.owners_updated.find(owner => owner.user_id == userId);
    const isCreator = goal.created_by == userId;

    const gs = this.headerData.goal_settings; // goal settings
    const allGoalsPublic = gs.make_all_goals_public;
    const userCanViewGoalProg = Boolean(Number(this.headerData.goal_users_settings.can_view_goal_progress));

    const accountGoalsPublic = allGoalsPublic || (goal.goal_type == 2 && gs.account_type_goals_public) || userCanViewGoalProg;
    const groupGoalsPublic = allGoalsPublic || (goal.goal_type == 3 && gs.group_type_goals_public) || userCanViewGoalProg;
    const individualGoalsPublic = allGoalsPublic || (goal.goal_type == 4 && gs.individual_type_goals_public) || userCanViewGoalProg;

    if (goal.is_published) {
      if (goal.goal_type == 2) { // account goal
        if (isCollaboratorWithPermissionVFR || accountGoalsPublic) {
          return `/goals/status/${goal.id}/${goal.goal_type}`;
        } else {
          return `/goals/work/${goal.id}/${goal.goal_type}/${userId}`;
        }
      } else if (goal.goal_type == 3) { // group goal
        if (isCollaboratorWithPermissionVFR || isOwner || groupGoalsPublic) {
          return `/goals/work/${goal.id}/${goal.goal_type}/${userId}`;
        } else if (isCollaboratorWithPermissionE || isCreator) {
          goal.queryParams = { previewMode: 'navigated-from-list' };
          return `/goals/view/${goal.id}`;
        }
      } else if (goal.goal_type == 4 && goal.owners_updated.length === 1) { // single individual goal
        if (isCollaboratorWithPermissionVFR || isOwner || individualGoalsPublic) {
          return `/goals/work/${goal.id}/${goal.goal_type}/${goal.owners_updated[0].user_id}`;
        } else if (isCollaboratorWithPermissionE || isCreator) {
          goal.queryParams = { previewMode: 'navigated-from-list' };
          return `/goals/view/${goal.id}`;
        }
      } else if (goal.goal_type == 4 && goal.owners_updated.length !== 1) { // multiple individual goal
        if (isCollaboratorWithPermissionVFR || individualGoalsPublic) {
          return `/goals/status/${goal.id}/${goal.goal_type}`;
        }
       
        else if (isCollaboratorWithPermissionE || isCreator) {
          goal.queryParams = { previewMode: 'navigated-from-list' };
          return `/goals/view/${goal.id}`;
        }
      }
    } else {
      goal.queryParams = { previewMode: 'navigated-from-list' };
      return `/goals/view/${goal.id}`;
    }

    return '';
  }
  onPageChange(event) {
    this.loaded = false;
    this.config.currentPage = event;
    this.onFiltersChage();
  }
  public onSort(sort: any) {
    this.goalsFilters.sortBy = sort.VALUE;
    this.onFiltersChage();
    this.goalsSortBy.forEach(item => {
      if (item.VALUE === sort.VALUE) {
        item.active = true;
        this.storageValue.sortBy = item;
      } else {
        item.active = false;
      }
    });

  
    this.headerService.setLocalStorage(this.playercardSortDataKey, this.storageValue);
  }

  public onFiltersChage() {

    if (this.goalsFilters.sortBy === "default_view" || this.goalsFilters.sortBy == undefined) {
      this.goalsFilters.sortBy = "";
      this.storageValue.sortBy = { T_KEY: 'default_view', VALUE: 'default_view', active: true }
    }
    let pageNumber;
    if(this.config.currentPage > 0) {
      pageNumber = this.config.currentPage;
      pageNumber = pageNumber - 1;
    } else pageNumber = 0;
    
    const data = {
      account_id: this.userCurrAcc?.accounts.account_id,
      user_id: this.params.user_id,
      start_date: this.userConfig?.start_date,
      end_date: this.userConfig?.end_dates,
      sort_by: this.goalsFilters.sortBy,
      page: pageNumber,
      limit: this.config.itemsPerPage,
      my_goals: true
    };

    this.headerService.setLocalStorage(this.playercardSortDataKey, this.storageValue);
    this.subscriptions.add(this.goalsService.getGoals(data).subscribe((res: any) => {
      this.config.totalItems = res.total_goals;
      const goalsData = this.formatGoalsArray(res.data);
  
      this.goals.data = goalsData;

      this.goals.data.map((obj) => {
        let owner_name = obj.owners_updated.length > 0 ? obj.owners_updated[0].first_name : '';
        obj.owner_name = owner_name;

        let g_type = obj.goal_type == 1 ? 'd-template' : (obj.goal_type == 2 ? 'a-account' : (obj.goal_type == 3 ? 'b-group' : (obj.goal_type == 4 ? 'c-individual' : '')))
        obj.g_type = g_type;

        let published_date = obj.publish_date != null ? obj.publish_date : 'AAA';
        obj.published_date = published_date;

        obj.g_percentage = Number(obj.percentage ? (obj.percentage.toString()).replace('%', '') : -5);
        if (obj.g_percentage && obj.is_incomplete != undefined) {
          if (obj.g_percentage == 0 && obj.is_incomplete) {
            obj.g_percentage = -3;
          }
        } else {
          if (obj.is_incomplete) {
            obj.g_percentage = -2
          } else {
            obj.g_percentage = -4;
          }
        }
      
      });
      this.loaded = true;
    },(error) => {
      this.toastr.ShowToastr('info',error.message);
    }));
  }

  onPerPageLimitChange(event) {
    this.loaded = false;
    this.config.currentPage = 0;
    this.onFiltersChage();
  }

  private processGoalCreatedEdited(socketRes: any) {
    let goalIsQualified: boolean = false;
    let goal = socketRes.data;
    goal.owners_updated = JSON.parse(JSON.stringify(goal.owners));

    /* First check if goal is qualified to add or update in existing goals array start */
    const userId = this.userCurrAcc.User.id;
    const isOwner = goal.owners_updated.find(owner => owner.user_id == userId);
    const isCollaborator = goal.collaborators.find(collaborator => collaborator.user_id == userId); // user is collaborator
    const isCollaboratorWithPermissionE = isCollaborator && isCollaborator.permission.indexOf(2) >= 0; // user is collaborator with edit permission
    const isCreator = goal.created_by == userId;

    const gs = this.headerData.goal_settings; // goal settings
    const allGoalsPublic = gs.make_all_goals_public;
    const accountTypeGoal = goal.goal_type == 2;
    const groupGoalsPublic = goal.goal_type == 3 && gs.group_type_goals_public;
    const individualGoalsPublic = goal.goal_type == 4 && gs.individual_type_goals_public;

    const userCanViewGoalProg = Boolean(Number(this.headerData.goal_users_settings.can_view_goal_progress));

    const userCanViewGoalPublically = allGoalsPublic || accountTypeGoal || groupGoalsPublic || individualGoalsPublic || userCanViewGoalProg;

    if (isCreator || isCollaboratorWithPermissionE) goalIsQualified = true;
    else if (goal.is_published) {
      if (isOwner || isCollaborator) goalIsQualified = true;
      else if (userCanViewGoalPublically) goalIsQualified = true;
    }
    /* First check if goal is qualified to add or update in existing goals array end */

    if (goalIsQualified) {

      if (goal.goal_type == 4 && !allGoalsPublic && !individualGoalsPublic && !userCanViewGoalProg && !isCollaborator && isOwner) {
        goal.owners_updated = [];
        goal.owners_updated.push(isOwner);
      }

      if (goal.goal_type == 4 && goal.owners_updated.length === 1) {
        this.goalsService.goalCompletedItems(goal.id, goal.owners_updated[0].user_id).subscribe((res: any) => {

          if (res.success) {
            goal.completed_items = res.complete_items;
            this.addUpdateGoalsInListing(goal, socketRes)
          }
        });
      } else {
        this.addUpdateGoalsInListing(goal, socketRes);
      }
    }
  }

  private addUpdateGoalsInListing(goal, socketRes) {
    let matchingKey = 'id';
    let matchingValue;
    if (socketRes.uuid && (socketRes.uuid != socketRes.data.id)) {
      matchingKey = 'uuid';
      matchingValue = socketRes[matchingKey];
    } else {
      matchingValue = socketRes.data.id;
    }

    this.formatGoal(goal);

    let goalIndex = this.goals.data.findIndex(item => item[matchingKey] == matchingValue);

    if (goalIndex >= 0) this.goals.data[goalIndex] = goal;
    else this.goals.data.unshift(goal);

    this.config.totalItems = this.goals.data.length;
  }

  private processGoalDeleted(goalId: number) {
    const index = this.goals.data.findIndex(item => item.id === goalId);

    if (index >= 0) {
      this.goals.data.splice(index, 1);
    }
  }

  private formatGoal(goal: any) {
    if (goal.goal_type === 3 || (goal.goal_type === 4 && goal.owners_updated.length === 1)) {
      // Calculate completion percentage
      goal.total_items = goal.total_items || goal.action_items_count;
      if (goal.total_items && goal.total_items > 0) {
        goal.percentage = Math.round((goal.completed_items / (goal.total_items || goal.action_items_count)) * 100);
        goal.percentage = `${goal.percentage}%`;

        goal.is_complete = goal.total_items == goal.completed_items;

      } else goal.percentage = '0%';
    }


    goal.is_incomplete = !goal.is_complete && new Date(goal.end_date).setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0); // compare only date, not time
    goal.routerLink = this.getRouterLink(goal);
    goal.disabled = (!goal.routerLink) ? true : false;
    goal.canEditDelete = this.checkEditDeletePermission(goal);

  }
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
