import { Component, OnInit } from '@angular/core';
import { SummaryService } from '@src/project-modules/analytics/services';
import { HeaderService } from '@src/project-modules/app/services';
import { Chart } from 'angular-highcharts';
import { numberFormat } from 'highcharts';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-leader-board',
  templateUrl: './leader-board.component.html',
  styleUrls: ['./leader-board.component.css']
})
export class LeaderBoardComponent implements OnInit {
  updateFlag: boolean = true; // optional boolean
  private subscriptions: Subscription = new Subscription();
  public translation: any = {};
  userNames = [];
  videosUpload = [];
  minutesViewed = [];
  videoComments = [];
  discussionPosts = [];
  resourceUploaded = [];
  resourceViewed = [];
  isLoading = true;
  chart = null;
  constructor(
    private summaryService: SummaryService, private headerService: HeaderService
  ) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;

      // if (this.translation.analytics_total_user) { // check if translation for analytics is loaded
      //   this.translationLoaded = true;
      // }
    }));
  }

  ngOnInit(): void {
    this.setChart();
    this.summaryService.analyticsData.subscribe(analyticsData => {
      
      // let users = analyticsData.user_summary.splice(0, 10).map(_data => {
      //   return _data;
      // })
      this.drawCharts(analyticsData.leaderboard);
      this.isLoading = false;
    })
  }

  drawCharts(data) {
    let that = this;
    this.userNames = this.getFields(data, 'user_name');
    this.videosUpload = this.getFields(data, 'video_uploaded').map(x => that.isFloat(+x) ? +x.toFixed(2) : +x);
    this.minutesViewed = this.getFields(data, 'minutes_viewed').map(x => that.isFloat(+x) ? +x.toFixed(2) : +x);
    this.videoComments = this.getFields(data, 'video_comments').map(x => that.isFloat(+x) ? +x.toFixed(2) : +x);
    this.discussionPosts = this.getFields(data, 'discussion_posts').map(x => that.isFloat(+x) ? +x.toFixed(2) : +x);
    this.resourceUploaded = this.getFields(data, 'resources_uploaded').map(x => that.isFloat(+x) ? +x.toFixed(2) : +x);
    this.resourceViewed = this.getFields(data, 'resources_viewed').map(x => that.isFloat(+x) ? +x.toFixed(2) : +x);
    this.setChart();
  }
  isFloat(n) {
    return n === +n && n !== (n | 0);
  }

  isInteger(n) {
    return n === +n && n === (n | 0);
  }
  // a=["printChart","downloadPNG","downloadJPEG","downloadJPEG","downloadPDF","downloadSVG"]
  //        exporting: {
  //   buttons: {
  //     contextButton: {
  //       menuItems: this.a
  //     }
  //   }
  //      },
  setChart() {
    this.chart = new Chart({

      lang: {
        downloadPNG: this.translation?.downloadPNG,
        downloadJPEG: this.translation?.downloadJPEG,
        downloadPDF: this.translation?.downloadPDF,
        downloadSVG: this.translation?.downloadSVG,
        downloadCSV: this.translation?.downloadCSV,
        downloadXLS: this.translation?.downloadXLS,
        exitFullscreen: this.translation?.exitFullscreen,
        noData: this.translation?.noData,
        contextButtonTitle: this.translation?.contextButtonTitle,
        // exportButtonTitle: "Export du graphique",
        loading: this.translation?.loading,
        printChart: this.translation?.printChart,
        resetZoom: this.translation?.resetZoom,
        resetZoomTitle: this.translation?.resetZoomTitle,
        viewFullscreen: this.translation?.viewFullscreen,
        numericSymbols: null,
        thousandsSep: '.',
        decimalPoint: ',',
      },
      chart: {
        type: 'bar',
        renderTo: 'container',
      },
      title: {
        text: '',
        align: 'left',
      },
      credits: {
        enabled: false
      },
      xAxis: {
        type: 'category',
        categories: this.userNames
      },
      yAxis: {
        title: {
          text: this.translation?.analytics_leaderboard_count_of_activities
        },
        stackLabels: {
          enabled: true,
          formatter: function () {
            return numberFormat(this.total, 0, '', ',');
          },
          style: {
            fontWeight: 'bold',
            color: 'gray'
          }
        }
      },
      legend: {
        align: 'left',
        verticalAlign: 'top',
      },
      plotOptions: {
        series: {
          stacking: 'normal',

        },
        bar: {
          dataLabels: {
            enabled: false
          },
          grouping: false,
          pointWidth: 8
        }
      },
      series: [
        {
          type: 'bar',
          name: this.translation?.analytics_leaderboard_videos_upload,
          color: '#c4a7fa',
          data: this.videosUpload //videos upload by 10 user data,means video upload in separate object in array of 10 users
        },
        {
          type: 'bar',
          name: this.translation?.analytics_leaderboard_minutes_viewed,
          color: '#24a8eb',
          data: this.minutesViewed
        },
        {
          type: 'bar',
          name: this.translation?.analytics_leaderboard_videos_comments,
          color: '#fb9562',
          data: this.videoComments
        },
        {
          type: 'bar',
          name: this.translation?.analytics_leaderboard_discussion_posts,
          color: '#47c87f',
          data: this.discussionPosts
        },
        {
          type: 'bar',
          name: this.translation?.analytics_leaderboard_resources_uploaded,
          color: '#616161',
          data: this.resourceUploaded,
        },
        {
          type: 'bar',
          name: this.translation?.analytics_leaderboard_resources_viewed,
          color: '#cccccc',
          data: this.resourceViewed
        }
      ]
    });
  }

  getFields(input, field) {
    if (input) {
      return input.map((o) => {
        return o[field];
      });
    }
    return []
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscriptions.unsubscribe();
  }
}



