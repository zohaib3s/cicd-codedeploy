import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MainAnalyticsService } from '@src/project-modules/analytics/services/main-analytics.service';
import { HeaderService, LocalStorageService } from '@src/project-modules/app/services';
import { encodeCompress, getPathAndQuery, guidGenerator } from '@src/project-modules/shared/components/common/utility';
import { TrackerFiltersService } from '@src/project-modules/shared/services/tracker-filters.service';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import { NgScrollbar } from 'ngx-scrollbar';
import { KeyValue } from '@angular/common';
import { isString } from 'lodash';


@Component({
  selector: 'app-playercard-usersummary',
  templateUrl: './playercard-usersummary.component.html',
  styleUrls: ['./playercard-usersummary.component.css']
})
export class PlayercardUsersummaryComponent implements OnInit {
  @ViewChild('stickyDiv') stickyDiv: ElementRef;
  @ViewChild('scrollable') scrollbarRef: NgScrollbar;
  attributes = [];
  imageBaseUrl: any;
  public header_data;
  headers = [];
  translation: any;
  width: any;
  public count: number = 0;
  private subscription: Subscription;
  filterObj: any;
  public scrollWidth: number;

  params: any;
  config = {
    id: '',
    currentPage: 1,
    itemsPerPage: 10,
    totalItems: 20
  };
  isLoading = true;
  constructor(private headerService: HeaderService,
    private trackerFilterService: TrackerFiltersService,
    private localStorage: LocalStorageService,
    private activatedRoute: ActivatedRoute,
    private mainAnalyticsService: MainAnalyticsService,
    private router: Router, private toastr: ShowToasterService,) {
    this.header_data = this.headerService.getStaticHeaderData();
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
    this.subscription.add(this.trackerFilterService.filtersKeys.subscribe(filterObj => {
      this.isLoading=true;
      if (filterObj) {
        filterObj?.limit < 10 ? filterObj.limit = 10 : false;

        this.filterObj = filterObj;
        setTimeout(() => {
          this.apiCall();
        }, 500);
      }
    }));
    this.activatedRoute.parent.params.subscribe(activatedRouteParams => {
      // this.params = activatedRouteParams;
           this.params =Object.assign({},(activatedRouteParams));
      delete this.params['start_date']
      delete this.params['end_date']
    })
  }

  ngOnInit() {
  }

  originalOrder = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => {
    return 0;
  }

  scrollRight(){
      this.count = this.count + 150;
      this.scrollbarRef.scrollTo({left: this.count});
  }
  scrollLeft(){
    if (this.count === 0){
      this.count = 150;
      return;
    } else {
      this.count = this.count - 150;
      this.scrollbarRef.scrollTo({left: this.count});
    }
  }

  showArrowLeft() {
    if (this.count === 0){
      return false;
    }else {
      return true;
    }

  }

  showArrowRight() {
    const scroll = document.getElementById('scrollBar');

    if (this.scrollWidth){
      return scroll.scrollWidth < this.scrollWidth && (scroll.scrollWidth + this.count) < this.scrollWidth;
    }else{
      return false;
    }

  }

  getImage(userId, image) {
    if (image) {
      return this.imageBaseUrl = 'https://s3.amazonaws.com/sibme.com/static/users/' + userId + '/' + image;
    } else return this.header_data.base_url + "/img/home/photo-default.png";
  }

  navigateToDetail(data, index) {
    
    this.headers.forEach((element) => {
      element['fake_id'] = guidGenerator()
    });
    let header = this.headers[index];
    const obj = {
      coach_id: '',
      user_id: data.user_id,
      account_id: data.account_id,
      report_type: 'user_summary_report',
      huddle_type: this.filterObj.huddle_ids,
      huddle_id: '',
      start_date: this.filterObj.start_date,
      end_date: this.filterObj.end_date,
      duration: this.filterObj.duration,
      header: [header],
      path: getPathAndQuery()
    }


    this.localStorage.set(obj.report_type, this.headers);
    //  let key = this.headers[index];
    // key = key.toLowerCase();
    // key = key.replace(/ /g,"_");
    this.headerService.reportsDetailData(obj);
    this.router.navigate(['trackers/report-detail-view/' + header.key], { queryParams: { query: encodeCompress(obj) } });

  }

  isNumeric(value) {
    return !isString(value);
  }

  apiCall() {
    let filterData = { ...this.filterObj, ...this.params };
    let sessionData = this.headerService.getStaticHeaderData();
    filterData.user_current_account = sessionData.user_current_account;
    filterData.user_permissions = sessionData.user_permissions;
    this.trackerFilterService.getReport(filterData, 'user_summary_report').subscribe((data: any) => {
      this.mainAnalyticsService.playerCardLoading.emit(false);
        this.isLoading=false;
      if (data && data.success == false) {
        this.toastr.ShowToastr('info',data.message);
        return;
      }
      this.headers = data.attributes_header;
      this.attributes = data.attributes_data;
      this.config.totalItems = data.total_users;
      this.config.currentPage = data.page;

      setTimeout(() => {
      
        this.width = this.stickyDiv.nativeElement.children.length * 150 + 120 + 'px';
        this.scrollWidth = this.stickyDiv.nativeElement.children.length * 150 + 120;
  
      }, 1000);
    });
  }

  // scrollRight(){ 
  //   if(!this.enableScroll){
  //     return;
  //   } else {
  //     this.count = this.count + 150
  //     this.scrollbarRef.scrollTo({left: this.count})
  //    }  
  // }
  // scrollLeft(){
  //   if(this.count == 0){
  //     this.count = 150
  //     this.enableScroll = true;
  //     return;
  //   } else {
  //     this.enableScroll = true;
  //     this.count = this.count - 150;
  //     this.scrollbarRef.scrollTo({left: this.count})
  //   } 
  // }

  // reachedEnd(){
  //   this.enableScroll = false
  // }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscription.unsubscribe();
  }
}
