import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { AmChartsService } from "@amcharts/amcharts3-angular";
import { HeaderService } from "@projectModules/app/services";
import { TabsService, BodyService } from "@analytics/services";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { TrackerFiltersService } from '@src/project-modules/shared/services/tracker-filters.service';
import { MainAnalyticsService } from '../../services/main-analytics.service';
@Component({
  selector: 'app-main-tags',
  templateUrl: './main-tags.component.html',
  styleUrls: ['./main-tags.component.css']
})
export class MainTagsComponent implements OnInit, OnDestroy {
  public optionsChart;
  public optionsChart2;
  // public pieData;
  public serialCharts;
  public serialChartCount;
  public serialData;
  public top_charts: top_charts;
  public current_chart_tab: number = 1;
  private list = ["#025c8a", "#28a745", "#004061", "#5e9d31", "#4f91cd"];
  private charts_data;
  public p;
  private loadingModalRef: BsModalRef;
  public header_data;
  public translation: any = {};
  public translationLoaded: boolean = false;
  public chartContainerLoaded: boolean = false;
  public chartContainer2Loaded: boolean = false;
  // public pieContainerLoaded: boolean = false;
  private subscriptions: Subscription = new Subscription();
  private chartImage = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ4AAABMCAYAAACccw5fAAAABHNCSVQICAgIfAhkiAAAGRJJREFUeF7tXXl81NW1/577m0lYKwIVTCZALa0I7mGiLNWQmUHR2lZfo0JCpdRCgtrW99r3tGqbatVXfa21FhKposIEF17dW4WZITwXlBlSd16fUEVmEkBcWCQhyfzueZ87w0xmX8OWzv18+IPMueeec+73d9dzziX0k8IMqm+ZMbWpquXVfqJSv1aD+ot2C52W7wqIiY1Wx639Raf+rEd/AB7VrbVWE9PjLPn2Jpvz5v7cYf1Ft2MaeAtaKkcK3XgfEa5UHcKg25ssawrAOwbQecwBr4EbxHur3jOMGLHbTkB1pI0ZfHOTxXn7MWD3f3oRjxng/aSlclyXbrgHRFUABgPQ4nuPf9Rocd73T9+rx4ABjirgqZ3pNWvOM0lj0USAqohxPjNOJMJwAEPS2ZOEvHLJDNfj6egKvx95CxwS4FWvrx44ovPT00jQKeynEyFwAjFGABhOREKpLYFiAgYi8I8HAXQ8GCNBCPyeSxEspy62ul7LpW6hzuG1QJ8Ar95pOYmILpOgGQRMADAOyB1AuZqA/TSq6YI1H+dav1Dv8FkgZ+A1vDexaPv2E68mEnOIMSWfkSprdZmXQlA1GMeH6zI6Gq0OtfYrlGPAAlkDr/qJiUXDR5TOF4zfgjDosOpIYKnzY5L4HgOJDQAi5d/caHF8/bDKU2gsZwtkBbyFTstEQWI5gPKcW8y1IqtlIVY2Wh1z65xWNxGZY1itbbQ4LLmyL9Q7vBbICHgNDRDbv2FbIIDGwyteuLUuEN/SWOW8e4HDepwmaBcAY4wsTY0WR/0Rkq/QbJYWSAs8Bbqd59nuBuNfs+TdJ+QM3gmJWU025xvq8HjH2le8BCqJZU4sa5dYXc190miBySG3QFrg1TmtDxLR/EMuSYIGGHzj6KrpdzVQg5pmUb/WOgdMCcGlSx621ObccyTkLLSZvQVSAm+h0/ZbQbg+ZhGffSvZ1TgAxg8NQwzP3nfuC3tDVRe1WKdLSS9R9IYi8DMz3mmyOk7PrpkC9ZG0QFLgXbPGZpEaXkiwljoU8urMeJYl7ijyG96576IXuiIbWeiqOo9YcxChKFHjkvnu+63Ofz8UghV4HhoLJAVencvmJcB0SJplbIbAGgleSyTeN3Zqm2PBFmq3zmW7lAA7kPzohqBPW2JZu/6QyFpgekgskBB4dU7bQiI05dliDxitEGglSZt0IdtEj/ANHbz//bumv7ovHe95LZUDBrHxN5L5OgKlWBLwPxqrnF8DgdPxLPx+9FggrkMbWioNO6Vx90EPkOwkZWwH4S/M9MzAQce57pm6qjM7BkHqeqfFCqI/AnRyuvqS6Of3V625Mx1d4fejywJxwFvgqvqqBm1LVmIy3iTCvbs+HbZi1eWr9KzqRhAvarGdKSV+QcClGfLwG0TPV+6bsc6XIX2B7CixQBzwFrlsT3LmHd8Bxi2NFsc9uU51P109c/B+TT+bIe4iwrnZ2IUJf2iqcvw4mzoF2qPDAlHAq36iWhs5Yrc/Q9E6u3SMXzbT0Z4hfZhswcZyY/G+E0b5df8iJr4h9RouCXeGFKSftNiy9qNs2y/QH3kLRAGv/uWLj0d392cZiNVNomfEkhnrvsiANkCibh12/sV5HA8c+EsCFuV9TEP088bC2i5T8x91dFHAW9RSOZqlcXs6KSVQf7/FkX7Xy6A6p/V8CLoFYDOBlNtSzo6evXKx/wu/GLbigjX708la+P3otEAU8K5rmWXyS783lagMfqPJ4jw7KY0CW4u1miRuApFyCP1Sn6vO8oxGq+vtPudbYHjYLBAFvDpXVSlBS7lDlDrM9890bIyVUK3bxN7hvyZGLYC4S/y+0ogE37yk0nlHrpuZvpKjwCc/C0QB72qnZZSRxI6kLAnexirHmNjf61oqx0EanAT6an7ipKtNKxsta2rSURV+P/otEAW8+a9MG1rcNSh8MR8nPuOVRqvjG7F/r3daN4HolEOrLj/faHFecmjbKHA/XBaIAp4KL1y01qausxLGLjDw1yaL4+JI4eaunjl4iIEz3t3mohgz/7nJ6vxuLnULdY5OC8QdINe7rMsA+n4ScV9otDguivyt+r3qopE79nQC3Ae71bhWewD5u0aL64Z8zFda8b0RJHh8iIfswfb21hXb8uFZqJufBeKAd+0rthK9Gz6ViCSWNQMvNVkc58et8Zy2diKcmJ8oMbUJn0PK+Y1W19PJ+I6qmPsVI/NtIJbQcbevtfmdRLQl5porNKLHwsBj/m2bp/mnfSpvgVlWFkjiJGDYC5AKto4qDPy9yeKIW8vVO23PgxA1BWclRRwxv61LnJfSo7iy0lDWYVKxF8NC1f06yre32v8Wy64AvPx641DUTuIWZV1MROp2IRZ6nY0WZ1xI4/dfmvXlAT1+dXVmyFdIIr5qyQzninTHJWPMsy9h0p6N+TRWet3NcbveAvDy7ZW+r58QeJUtlYZTpPETAMfFQU/IS5pmuJ6Pn24ttxCJnJMiMmO1Blmz2Or6NBM1Ted873Ri+VYM8G73upvj0pQVgJeJRQ8vTVIHy/q1lhvB4o4E023czlbRBHbELssNoPg6KVUirNaIfvLHGWv+nq3qZebaZSAENkLM/CYM3VN9r8X7ABaAl61lDz19UuAFgNRiex+M8G4wKA7vaLQ4k24kFriqztZY3Aqi5Gu+gOu7fAiQTzZWtfxfPmqWlM+eQAYxjET3W4lAp3gXgJePhQ9N3ZRRZvUtFit0sToqL4pKIyHltPvTZGW6xlU1VgITmcSJLGkomD8XxO3cY9zSNOvFrYdGncRcC8A7nNbOrK20cbX1LutTAH0nht36RotjWmZNHHmqAvCOfB/ESpAWeNeusZXoGt6LPLYAo2OU1nNcw4x1mTqNHlHNC8A7ouZP2Hha4Kla9S7rdQD9IZIDM+5qsjr+4+hTKV6iAvCOvl7KCHhB8NleARA5vfYYugxDk8XDplGVSspnjxCCzgPRtQSaEdi2AB0AtxPoXTA9o6PH1T54wnasa0g4sprMNUtBFMwgwDjg89grE7WbCfAmTqwu2jO06ASSqCVCJUDnBbKVMnUBvBGS7VykPefrHvAxWpf2ZNKVpZNrF5II7rpVoR7/td43HotyKRtz2pzj9UF0MjH9AIw5pFK/MfYz8VYCvc5S/Lmnm1/a+faKtE6vSofPirQSo1H7FyaqIvBEsHJRo89A2ArwI6xpz/leO2k7EEwLknVRB/d7Rp8gDYbpAjwr4BzCOAUU8LvczYznWReP+wewe+f65bsAShh2mjHwFq2xncka1K1AuI7UcfH9Mx1/zUZ4ZWg5QDxNBNWxmZTdTLLGt2GlymoQpUSZufYVUPhj6PC67QmdG9IBb0z53OmssTqbjDu3TCDgF7pOs9pbV6gXhFLG8pZNrvk1BN0U4iGZrW2eZlfg/9XVWtnWogdANC8TI7DEd30b7U8ma9N05pyvURF5AEqvA7Ozs6j4sk9eXZY2vjlCNiqtmH2uYPECKIM2gqPBVt4/oNz33oNx4RQZA0+xqXNZGwj0y15h+IB+3OdfWjq5NaMRYNyU2RP8fm0jUWLvlxQdoDp4i9dtj0q8mDfwgp3/GogmZ5kfhhloMQwyXLx13cMHksmdDHijps49ocjPm3Pwzl7jdY+fFT1aNQiTefMqIhUSmirwPVZK6tAJF7RvWKFmstSloUGU/XXLCwDbsmsjwFaXxHe2bWi+JbKRrIA3/5lpQ4uHDFKu8eGvShj18sXnrY27H43TRHXyR8XKu3l0+DeGZOAtYlrBAnsZcjQBJxHo20AgWXcEKdb63PaoxIv5AI8M3beQXqw2TV+JlpX3SmCzADzgwG8mEE1K1DMMfEQ9/qneNx5LGGmXCHhdkjYN1PABgAHp+jvJ70943fYrAr9VVhpMHaVrQkuVHPh1EuvmbZ5HlR0SlpPKFxzXLTo2ECEuuJ6BN8FwgvERCCYCTwdhamJwcpPX3RzOX5gV8JRkC52WCwUJNe0FinIcGF01bVIolVjSr7+iphGguggkPdsl9AUfb3h0Z4I6NPrcK8capXYhg24j4EtCx7iPWu1RgUj5AE+QOhgPADxYJH5HGj84ZF/3lk2bVnVHyETjzPNGdbM8Rwi5goChMfJu8rrtCYEZCzyd6VuC5GIClQVsx5AE/g2Ap4Qk3+DOrk87ThgseJ//eL9GpxDzvUQ4NdY+BJ63zd38iKlizg8I4oGIz3MNJC0mku92FA3Y9Ynxg85xu8cN6SnyjxXMV4DoxlhezFjv89gTHo2Vly8w7hSdbxOxSqgeKt2SuQGkLW1zL4+73hx1+tzBxiJ9Egmh2oo6hpOSq9s2Nv+3YpQ18FRshWHP8FYGTguLInhK4wzn66m+OFNF7VYCxh6k2aINMpyWapoK8Rp35rxhfqNe5fOsUOubqJIr8Bh4m4DApoQBH0hW+TasVFNfyqKMWjRA/gmg2ZGEDDT73HYVaxItX8waj5n/REQ/DBAxP24Q8scfJv7wQnyozFxTD6Lfx4SDbuo20IyiHt4GQjEDfkG4aNuG8a5Um4aS8nkThOZ3xCZjIha2bZ7lzjj5K2peBOiC0N8Z2MwsL2/zrHwzna2AwBJgDhGtiPgwOg09fMqHb6z8KGvgKSaLWi4Yz1KGO4rBriaL05pKmLKKmk8BUg+lqC99qc9jX5he+NQUuQIvguuO7gM0PpMdY6Qkpora+wi4NuJvB4jlpG2elWoKDZfYES/4vAcEGM95x3VdilWZpfswmWt+QUS/irYGNx2cQTq7WZ66M6btZJYrKZ89UtO094GIjPnAo163fU5kndKz51YJjR29t1bs9Rv4G9vXr8wqgL7EXPt9jfBgaJBjplt9nhUqtjqHokIYXZZnicQ3Q7V1yacvtTkTOmIqmrKKWpUIKLg2ZHrc61kRePgun5IX8NSRhUGc7HtteVsuMpjMta9SYD0TLMxw+Tz2qI8vAfAUaZvXbc86/VtZRa0Cdcx6FDox5mzz2J/IRgeTufZuIkQ6wsYtF8oqaj4EAuGpQfUYF/k89hezaSdI2yDKKrYoXEw8WPdzbZChJDfgAbhu86xi/za/2o4HkmAzybuaqlxJD5TLKmrV+iy0seiUxF9v29CcV7KdfIDHLB/weVYGp70cSpm5xgzQ66ERgYF9Prc9KoY4AfCYWb/I53k06w40mWv+SETXxIj6rtdt713yZKiHWrt9rO3/AqBAostY2cdVzhugd/hV0FcowfnTXrc900RKcVKYzvheKRVLFWoQCI/QWdpyBp5iULfW6iSm4E6T0fWFTiOSRfeXTa59GQLTwyMEsI1ZfrttXM87mU45sRrlDDxGl9djz3VXGRajzDzXAeLwKNfTw5N2vNG8KUSQAHjbvG57aJ2bIUyCZKbJtZUk0BJZiRm1Po89p4TjZRW1aicbGoXgdY/XQuvD0nNqfiKY7gm3JeV3vBtXPpOVwDHEZebaj0H48kGsqIxguZfq9VMGjuwcor6MoOcxY26j1aGyd8aVkvK5Y4TGH1Dsq4vMexg8v6dLW53tWisP4L3j9djzzplsMs+9gYh7c/NJ/M670f5vyYDHzC/5PM1xMSuZ9MDY8toTpYbIYxu/d2zXgNw/2ppXQBTezUrsH9nmfiqwSy2rqFUfT2+Ig7Go1PvqsqyTM0XqVVZRq9zfAuewzPhzXsBTTOpd1vcACn45xC82VjlnJTNkmbnmv0AU7pgYOh2MjyXxjW1ju+2ZGDQP4Dm8HvvMTDo8FU2JuXaqRlA3GMHvLuZoInbEY+YbfZ7m/8yp3fIFxjKtI/KY50Ov235STrwUuM6Z6wD3jta9wGsQporNXQQKDCZqGiaW4bV8ru0B9CsQBa80mdz5A2/dzFnQOXBtRoD/QPHA4cumP5vsKoZMFTXfIdDKNAeo3QzeDj+u9/2t+ankQM7tygzMv/d6mlU2+7xKLPAA7Pe67eHnTeOBl/vUiPJyY5l2SiTwclrfhUfjJMALrv86OvoifiaFcbfkDbzAqOe0tYGC+VJY8g+abM5lqXrUNKV6OOnGq5jFbWmuz9TV1NtSp28lioPNdcSTfRTeqLyfNU373whdUwJPl/rM9o2POnJCezzwPF63vSInXilGPOVosG9IsQJegoeoc20trp6vj4A3M7zIZubmJqsz7jA1kcimKdUDpb/4LA2wMHAVERLnXmHsBpHN614R5dmRK/AArPK67Zfna8ZsR7woJ4FsG48FHrHbu6H5nGzZpBvxUNlgKOvYonJXH4wY5G4GxUTz5dpqsB6BP+kT4NW5rPUEWnJwTZAw9jYTUceYr5okSZ8L4GqKvatl7Bo/eHzJuggXqVyBx+DXfO7m8BlcJrIloimbPOdKCPFo6DdmvOjz2MNr3JTeKdk2eriAB9DBM9fQ0RBTpxyx7Z2Vn2crcir6vgFey4XjSOofBoDH6B6t9QzOyzu5ssFg6nj/JoBuDi1yDypxh9dtD7sZ5Qo8ADu9bnuvs0KOFi2rqHkQ6H1uSxIWtm2wLw2PKqncorJt8/ABDyZzzQoiCs9aEmRpc69Ym63Ihxx4qoG6tda9xBS4QCcDT1xyvjNy7ZOTzKaKGvXGRa/nM2OX12M/IdyxOfrjBepLzPFutIdHq2wFDK5Ti9X1UWgzIXV90ND21qVqfRQox+iIh5LJV56lCUNr+JoLeMDntud82J7Itn0y4inG9U5bOw7mTxHQz19sWftStp0ZRx90pVKZqEKHvbu9bru67w04YOYx4qnqOV1dRYD+VhAifcx2eN32qLDPYxV4AdtGXnECsof103ekcJ/Ktq/7DHh1LtuHBATv9oS0Nc5wRXk7qG16a4Yu45FKmCpqNxPCsb2fHwRecETJZ8QLjnrLvRvtV2VrtNKK2jOE8kWLLBI3eTfaowLgj2Xgmcw1zUQUdhxQfpPQuqYki11OacPqaq3kgwPF7a3PhWeDvgTeDgJGBXAHvXyxpdc5tKS8eoymFb2jsfHkrZ6Hk2ccjZG+pHzBICE69hGFE3ZHjSp5AC/oJRIcOm/wue13pXNjD4kWOEIR2mug3mRBALZ73fa49LvHMvCCo16vR9FBW23wje2alsnhfshegXNBsf99JhpxwLi39JNXg2e8fQK869dXDzzQuVsFo6gXxeQMy7Ciyyns8qMOjbcRyARQF5hv93rst2UyypSZax6KiklgzPd67A9FTHc5xVyA8QgIvSMd411d6jPaWx9V+WKSljHmml9x0Jky+nVw4gu8G5rXxFY85oEXs2s/qF9bT7HxrB0vP6QydaUsJvPs8wniWRAFd8jMH2iDjZOUH2afAK/eZbsawJ8OStHaaHGoGIZAGX1W9ZeNxmI1yoUTNzJjFwEuZl7u29isPDWigmZM5jmXAXQ7EUV6vu7wuseXRjo65jriqQNkkmIVaRzpvNrJwDYQrdQkO1nwbsAwkKV+OoivANPZ4UvuSHMzfpHsQzrWgafUNJlrbiCi2LfidCbaDCmflqw/0b5xwlvBfqk0lJpLTgXEjwTBFggbiLbVQ0P3d9UpD+++Ap7qwMBhJoMbmizOKKfFoIu2thjg4gSfiApd3IPAIXHAUbI0wROhHTrD1u6xRz0Nmg/w2jzNPystn1MpNKHWojllM5WSr2/b2Ky8gxOW/gC8IPjm/Igg1LNhyeykXurcDwqEBSS88WDmX46Sg+8MrfPzBl6dw3oWCXIHT7r5AEOOb7KsjXOuHGOecxKTUBFqKn9dNtcxLxPr9YkCUvIEXsARcox59iQm7REA5emmjojf31WO2F63/eVUdfoL8JSOJ5bXnq0J3Bfp/JqBvRjgNYC4OfbWKS/gqfgLbe/wf4ARCF4B485Gq+PnqQQyTakdTzpfcdCX/4wkIX4fKo9eaPyk7/X4qTjEvy+AF+Jlqqi5lECzmVFJIb+xCEUY+FgwXmbG8qEdXS/GBAT16xGvVzkVR7FlJiEQOKTiohN7xzB/ANBqAdg/ipmlQrxyBp5613aHNGwgUOiVn3d1ydNTpo+N654GMfqczROEFF8lkirr8ifdsnvrLs+qjHa+yscPxIGUuUKTMlnAzsiT5w8tGtod3nVKveuzHW+sSro4Lj2nxqTpGOsnMVyw9MPPW3xvpg8GilVPxTeAtN4wTd7jjTxSyGDEiCIpmVzbG2LI1JlPAnGlI+uBJ74CpX1j1xYgsxiQUJ1AUnOWZgkyCvj3Mhm3d3/R2b5r06q0rwDkBLx5LZXDBkrD/wAH00cAnwmWUxZbXSqIpFAKFkhrgayA19AA0f6NqjMFi3VEwesxBu8jMpgbq17MK8FiWkkLBP3KApkBj0EL1lWO0HSjSv0a+brOru6BYsKDU1dn8tRovzJcQZn8LJAWeHUtM08lyXYwTgtvpwPHxLhnlGXaz9JlEMhPvELt/mqBeOAFnpWa+U0J+WMwnUUUCPyNpFtfxPKye62uRKkn+qudCnr1sQWoruXCcULXvy2JLyGIcQCrA9zY0L8eBj2sUc+9i6taVARSyvRcfSxjgV0/tABFujNF6sfMBwCxmgQv66Selx6esU5lAiiUggX6xAJUF3iHjAkglSJBjWatOnT3+0K+ve4YyXHcJ5YoMDmsFvh/EjF+BJYgvC8AAAAASUVORK5CYII=";
  @ViewChild("loadingModal", { static: false }) loadingModal;
  filterObject = null;
  constructor(
    private modalService: BsModalService,
    private tabsService: TabsService,
    private AmCharts: AmChartsService,
    private headerService: HeaderService,
    private bodyService: BodyService,
    private trackerFiltersService: TrackerFiltersService,
    private mainAnalyticsService: MainAnalyticsService,
    private router: Router,
    private activatedRoute: ActivatedRoute) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      if (this.translation.analytics_top_five_tagged_standard && !this.translationLoaded) {  // check if translation for analytics is loaded
        this.translationLoaded = true;
      }
    }));
    this.tabsService.typeChange.emit('tags');
    this.subscriptions.add(this.tabsService.typeChange.subscribe(type => {
      if (type) {
      }
    }));
    this.activatedRoute.queryParams.subscribe(queryParams => {
      if (queryParams.page) this.p = queryParams.page;
    });

  }
  ngOnInit() {

    this.header_data = this.headerService.getStaticHeaderData();
    this.checkPermission()
    this.top_charts = { 
      top_available: false,
      least_available: false,
      // pie_available: false
     }
    let xlsx_menu_item = { "label": "XLSX", "click": () => { this.export_excel(); } };
    this.RunSubscribers();
    this.optionsChart = {
      "export": {
        "enabled": true,
        "menu": [{
          "class": "export-main",
          "menu": ["PNG", "JPG", "SVG", 
          {
            "format": "PDF",
            "content": [

               {
                layout: 'lightHorizontalLines',
                table:{
                  widths: [ '*', 200 ],
                  body:[
                    [ {"image": this.chartImage,
                    "width": 100,
                    "height": 50},{
                      style: 'table',
                      layout: 'noBorders', // optional 
                      table:{
                        
                        body: [
                          [ { text: 'Account Name:', bold: true }, this.header_data.user_current_account.accounts.company_name  ],
                          [ { text: 'Report Name:', bold: true }, 'Tags & Performance Chart' ],
                          [ { text: 'Exported By:', bold: true }, this.header_data.user_current_account.User.first_name + " " + this.header_data.user_current_account.User.last_name ],
                          [ { text: 'Exported Date:', bold: true }, moment(new Date()).format("yyyy-MM-DD  h:mm:ss a") ]
                        ]
                      }
                    }]

                  ]
                }

              },
              {canvas: [ { type: 'line', x1: 0, y1: 0, x2: 515, y2: 0, lineWidth: 1 } ]},
              
               {
              "image": "reference",
              "fit": [ 523.28, 769.89 ] // fit image to A4
            } ],
            styles: {
              table: {
                fontSize: 10,
              }
            }
          }
        ]
        }],
        
      },
      "type": "serial",
      "categoryField": "country",
      "rotate": true,
      "hideCredits": true,
      "autoMarginOffset": 40,
      "marginRight": 60,
      "marginTop": 60,
      "zoomOutButtonColor": "#FFFFFF",
      "colors": [
        "#0000"
      ],
      "startDuration": 1,
      "startEffect": "easeOutSine",
      "backgroundAlpha": 0.79,
      "fontFamily": "Roboto",
      "fontSize": 13,
      "theme": "default",
      "categoryAxis": {
        "autoRotateAngle": 1.8,
        "autoWrap": true,
        "gridPosition": "start",
        "axisAlpha": 0,
        "axisThickness": 0,
        "fillColor": "#ED1616",
        "gridCount": 11,
        "minorGridAlpha": 0,
        "offset": 30,
        "titleColor": "#FFFFFF"
      },
      "trendLines": [],
      "graphs": [
        {
          "balloonColor": "#FFFFFF",
          "balloonText": "[[title]] of [[country_full]]:[[value]]",
          "bulletBorderThickness": 0,
          "bulletSize": 0,
          "colorField": "color",
          "fillAlphas": 1,
          "gapPeriod": -5,
          "id": "AmGraph-1",
          "labelText": "",
          "precision": -9,
          "title": "graph 1",
          "type": "column",
          "valueField": "litres",
          "fixedColumnWidth": 20
        }
      ],
      "guides": [],
      "valueAxes": [
        {
          "id": "ValueAxis-1",
          "axisColor": "#E1E1E1",
          "tickLength": 1,
          "title": "",
          "titleRotation": 1,
          "integersOnly": true
        }
      ],
      "allLabels": [],
      "balloon": {
        "borderAlpha": 0,
        "borderColor": "#000000",
        "borderThickness": 0,
        "color": "#FDFDFD",
        "fillColor": "#040404"
      },
      "titles": [],
      "dataProvider": [
        {
          "category": this.translation.analytics_data_and_assessment,
          "column-1": 3,
          "column-2": 5,
          "color": "#5e9d31"
        },
        {
          "category": this.translation.analytics_standard_and_alignment,
          "column-1": 8,
          "column-2": 8,
          "color": "#004160"
        },
        {
          "category": this.translation.analytics_content_knowledge_expertise,
          "column-1": 6,
          "column-2": 5,
          "color": "#5191cc"
        },
        {
          "category": this.translation.analytics_knowledge_of_std,
          "column-1": 4,
          "column-2": "5",
          "color": "#5e9d31"
        },
        {
          "category": this.translation.analytics_achieving_expectations,
          "column-1": 7,
          "column-2": "9",
          "color": "#004160"
        }
      ]
    };
    this.serialData = {
      "hideCredits": true,
      "type": "serial",
      "path": "/amcharts/",
      "theme": "light",
      "categoryField": "date",
      "rotate": false,
      "marginTop": 40,
      "marginBottom": 0,
      "startEffect": 'easeInSine',
      "startDuration": 0.5,
      "autoMargins": true,
      "addClassNames": true,
      "categoryAxis": {
        "gridPosition": "start",
        "position": "left",
      },
      "balloon": {
        "borderAlpha": 0,
        "borderColor": "#000000",
        "borderThickness": 0,
        "color": "#FDFDFD",
        "fillColor": "#040404"
      },
      "legend": {
        "equalWidths": false,
        "autoMargins": false,
        "position": "bottom",
        "valueAlign": "left",
        "markerType": "square",
        "valueWidth": 2,
        "verticalGap": 2,
        "data": []
      },
      "graphs": [
        {
          "valueAxis": "ValueAxis-17",
          "colorField": "color",
          "fixedColumnWidth": 10,
          "balloonText": this.translation.analytics_data_and_assessment + ":[[value]]",
          "fillAlphas": 0.8,
          "id": "AmGraph-17",
          "lineAlpha": 0.2,
          "title": this.translation.analytics_data_and_assessment,
          "type": "column",
          "valueField": "total_tags",
        },
        {
          "valueAxis": "ValueAxis-r",
          "colorField": "color_rating",
          "balloonText": "[[average_rating_name]]:[[value]]",
          "fixedColumnWidth": 10,
          "fillAlphas": 0.8,
          "lineAlpha": 0.2,
          "title": "Performance Level",
          "type": "column",
          "valueField": "standard_rating_avg",
        }
      ],
      "guides": [],
      "valueAxes": [
        {
          "id": "ValueAxis-17",
          "position": "left",
          "axisAlpha": 1,
          "title": this.translation.analytics_number_of_standard_tags,
          "titleBold": false,
          "titleFontSize": "10",
          "integersOnly": true
        },
        {
          "id": "ValueAxis-r",
          "axisAlpha": 1,
          "autoGridCount": true,
          "gridCount": 6,
          "integersOnly": true,
          "dashLength": 1,
          "position": "right",
          "minimum": 0,
          "maximum": 6,
          "valueText": "standard_rating_avg",
          "labelFunction": function (v) {
          },
          "title": "Performance Level",
          "titleBold": false,
          "titleFontSize": "10",
          "minVerticalGap": 1,
        }
      ],
      "dataProvider": [{ "date": "Aug-17", "color": "#85c4e3", "standard_rating_avg": 2, "average_rating_name": "Developing", "color_rating": "#000", "total_tags": "1" }, { "date": "Sep-17", "color": "#85c4e3", "standard_rating_avg": 2, "average_rating_name": "Developing", "color_rating": "#000", "total_tags": "0" }, { "date": "Oct-17", "color": "#85c4e3", "standard_rating_avg": "No Ratings", "average_rating_name": "No Ratings", "color_rating": "#000", "total_tags": "1" }, { "date": "Nov-17", "color": "#85c4e3", "standard_rating_avg": 3, "average_rating_name": "Proficient", "color_rating": "#000", "total_tags": "3" }, { "date": "Dec-17", "color": "#85c4e3", "standard_rating_avg": "No Ratings", "average_rating_name": "No Ratings", "color_rating": "#000", "total_tags": "2" }, { "date": "Jan-18", "color": "#85c4e3", "standard_rating_avg": 2, "average_rating_name": "Developing", "color_rating": "#000", "total_tags": "5" }, { "date": "Feb-18", "color": "#85c4e3", "standard_rating_avg": 3, "average_rating_name": "Proficient", "color_rating": "#000", "total_tags": "0" }, { "date": "Mar-18", "color": "#85c4e3", "standard_rating_avg": "No Ratings", "average_rating_name": "No Ratings", "color_rating": "#000", "total_tags": "1" }, { "date": "Apr-18", "color": "#85c4e3", "standard_rating_avg": "No Ratings", "average_rating_name": "No Ratings", "color_rating": "#000", "total_tags": "0" }, { "date": "May-18", "color": "#85c4e3", "standard_rating_avg": "No Ratings", "average_rating_name": "No Ratings", "color_rating": "#000", "total_tags": "0" }, { "date": "Jun-18", "color": "#85c4e3", "standard_rating_avg": "No Ratings", "average_rating_name": "No Ratings", "color_rating": "#000", "total_tags": "0" }, { "date": "Jul-18", "color": "#85c4e3", "standard_rating_avg": "No Ratings", "average_rating_name": "No Ratings", "color_rating": "#000", "total_tags": "0" }],
      "export": {
        "enabled": true,
        "menu": [{
          "class": "export-main",
          "menu": ["PNG", "JPG", "SVG", {
            "format": "PDF",
            "content": [

               {
                layout: 'lightHorizontalLines',
                table:{
                  widths: [ '*', 200 ],
                  body:[
                    [ {"image": this.chartImage,
                    "width": 100,
                    "height": 50},{
                      style: 'table',
                      layout: 'noBorders', // optional 
                      table:{
                        
                        body: [
                          [ { text: 'Account Name:', bold: true }, this.header_data.user_current_account.accounts.company_name  ],
                          [ { text: 'Report Name:', bold: true }, 'Tags & Performance Chart' ],
                          [ { text: 'Exported By:', bold: true }, this.header_data.user_current_account.User.first_name + " " + this.header_data.user_current_account.User.last_name ],
                          [ { text: 'Exported Date:', bold: true }, moment(new Date()).format("yyyy-MM-DD  h:mm:ss a") ]
                        ]
                      }
                    }]

                  ]
                }

              },
              {canvas: [ { type: 'line', x1: 0, y1: 0, x2: 515, y2: 0, lineWidth: 1 } ]},
              
               {
              "image": "reference",
              "fit": [ 523.28, 769.89 ] // fit image to A4
            } ],
            styles: {
              table: {
                fontSize: 10,
              }
            }
          }]
        }]
      },
    };
    this.optionsChart2 = JSON.parse(JSON.stringify(this.optionsChart));
    this.optionsChart2.export.menu[0].menu.push(xlsx_menu_item);
    this.optionsChart.export.menu[0].menu.push(xlsx_menu_item);
    var balloonText = '[[country]]<br><b><span style=\'font-size:14px;\'>[[value]]</span></b> [[percents]]%\
          <table>\
            <tr><th>Accounts </th></tr>\
			<ng-container *ngFor="let item of [[accounts]] ">\
			 <tr><td>[[litres]]</td></tr>\
			</ng-container>\
          </table>';
    // this.pieData = {
    //   "export": {
    //     "enabled": true,
    //     "menu": [{
    //       "class": "export-main",
    //       "menu": ["PNG", "JPG", "SVG",{
    //         "format": "PDF",
    //         "content": [

    //            {
    //             layout: 'lightHorizontalLines',
    //             table:{
    //               widths: [ '*', 200 ],
    //               body:[
    //                 [ {"image": this.chartImage,
    //                 "width": 100,
    //                 "height": 50},{
    //                   style: 'table',
    //                   layout: 'noBorders', // optional 
    //                   table:{
                        
    //                     body: [
    //                       [ { text: 'Account Name:', bold: true }, this.header_data.user_current_account.accounts.company_name  ],
    //                       [ { text: 'Report Name:', bold: true }, 'Tags & Performance Chart' ],
    //                       [ { text: 'Exported By:', bold: true }, this.header_data.user_current_account.User.first_name + " " + this.header_data.user_current_account.User.last_name ],
    //                       [ { text: 'Exported Date:', bold: true }, moment(new Date()).format("yyyy-MM-DD  h:mm:ss a") ]
    //                     ]
    //                   }
    //                 }]

    //               ]
    //             }

    //           },
    //           {canvas: [ { type: 'line', x1: 0, y1: 0, x2: 515, y2: 0, lineWidth: 1 } ]},
              
    //            {
    //           "image": "reference",
    //           "fit": [ 523.28, 769.89 ] // fit image to A4
    //         } ],
    //         styles: {
    //           table: {
    //             fontSize: 10,
    //           }
    //         }
    //       }, xlsx_menu_item]
    //     }],
    //   },
      
    //   "type": "pie",
    //   "theme": "none",
    //   // 'balloonText': balloonText,
    //   balloonFunction: this.adjustBalloonText,
    //   "labelText": "[[percents]]%",
    //   "hideCredits": true,
    //   "startEffect": "easeInSine",
    //   "startAlpha": 0,
    //   "startRadius": 0.5,
    //   "legend": {
    //     "position": "bottom",
    //     "align": "center",
    //     "marginRight": 100,
    //     "autoMargins": true,
    //     "equalWidths": true,
    //     "valueWidth": 0,
    //   },
    //   "dataProvider": [
    //   ],
    //   "valueField": "litres",
    //   "titleField": "country",
    //   "colorField": "color",
    //   "balloon": {
    //     "borderAlpha": 0,
    //     "borderColor": "#000000",
    //     "borderThickness": 0,
    //     "color": "#FDFDFD",
    //     "fillColor": "#040404"
    //   },
    //   "listeners": [{
    //     "event": "clickSlice",
    //     "method": this.handleClick
    //   }]
    // };
    this.refactorChartLabels();
  }
  ngAfterViewInit() {
    // this.HandleSubmit();
  }
  // public activate_chart_tab(n) {
  //   this.current_chart_tab = n;
  //   if (n == 1) {
  //     let that = this;
  //     that.clearChart("chartContainer");
  //     that.clearChart("chartContainer2");
  //     setTimeout(() => {
  //       that.activateLinearCharts();
  //     }, 500);
  //   }
  // }
  private clearChart(id) {
    var allCharts = this.AmCharts.charts;
    for (var i = 0; i < allCharts.length; i++) {
      if (allCharts[i].div && id == allCharts[i].div.id) {
        allCharts[i].clear();
      }
    }
  }
  // private activateLinearCharts() {
  //   if (this.charts_data) {
  //     this.RenderCharts(this.charts_data, true, true);
  //   }
  // }
  private refactorChartLabels() {
    this.AmCharts.addInitHandler(function (chart) {
      if (chart.dataProvider && chart.dataProvider.length > 0 && chart.dataProvider[0].country) {
        for (var i = 0; i < chart.dataProvider.length; i++) {
          var label = chart.dataProvider[i].country;
          var tooltip = chart.dataProvider[i].country;
          if (label && label.length && label.length > 20) {
            label = label.substr(0, 20) + '...'
          } else if (label && label.length && label.length <= 20) {
          }
          chart.dataProvider[i].country = label;
          chart.dataProvider[i].country_full = tooltip;
        }
      }
    }, ["serial"]);
  }
  private RunSubscribers() {
    this.subscriptions.add(this.tabsService.Tabs.subscribe(tab => this.ActivateTab(tab)));
    // this.tabsService.FiltersSubmitted.subscribe(data => {
    // 	// this.HandleSubmit()
    // });


    this.subscriptions.add(this.bodyService.triggerTagsDataChange.subscribe(tagsData => {
      this.hideLoader();
      this.getChartData(tagsData);
    }));

    this.subscriptions.add(this.trackerFiltersService.filtersKeys.subscribe(data => {
      if (data['type'] == 'tags') {
        this.showLoader();
        this.filterObject = data;
        // this.HandleSubmit();
        if (data['framework_id']) {
          this.GetPLAssesmentArray();
        }
      }
    })
    );
  }

  showLoader() {
    this.chartContainer2Loaded = false;
    this.chartContainerLoaded = false;
    // this.pieContainerLoaded = false;
  }

  hideLoader() {
    this.chartContainer2Loaded = true;
    this.chartContainerLoaded = true;
    // this.pieContainerLoaded = true;
  }


  GetPLAssesmentArray() {
    let obj = {
      framework_id: this.filterObject['framework_id'],
      account_id: this.header_data.user_current_account.accounts.account_id
    }
    this.bodyService.getAssesmentArray(obj).subscribe((data: any) => {
      this.UpdatePLData(data)
    });
  }

  private UpdatePLData(assessment_array) {

    if (assessment_array) {

      this.bodyService.StorePLData(assessment_array);

    }

  }
  public HandleSubmit() {
    this.p = 1;
    this.current_chart_tab = 1;
    let filters = this.bodyService.GetFilters();
    // if (filters && filters.subAccount != '') this.LoadCharts(false);
    this.LoadCharts(false);
    if (filters && filters.subAccount != '') this.LoadCharts(false);
    else {
      this.chartContainer2Loaded = true;
      this.chartContainerLoaded = true;
      // this.pieContainerLoaded = true;
    }
  }
  private ActivateTab(tab) {
    let filters = this.bodyService.GetFilters();
    if (tab == 1) {
      if (filters && filters.subAccount != '') this.LoadCharts(true);
      else {
        this.chartContainer2Loaded = true;
        this.chartContainerLoaded = true;
        // this.pieContainerLoaded = true;
      }
    }
  }

  private RenderCharts(data, tags_standards_only?, is_internal?) {
    let folder_type = this.bodyService.GetFilter("folder_type");
    if (void 0 != folder_type) {
      if (+folder_type == 6) {
        this.optionsChart2.export.menu[0].menu.pop();
        this.optionsChart.export.menu[0].menu.pop();
      } else {
        let found = this.optionsChart2.export.menu[0].menu.some(item => item.label == "XLSX");
        if (!found) {
          let xlsx_menu_item = { "label": "XLSX", "click": () => { this.export_excel(); } };
          this.optionsChart2.export.menu[0].menu.push(xlsx_menu_item);
          this.optionsChart.export.menu[0].menu.push(xlsx_menu_item);
        }

        this.optionsChart.exporting.adapter.add("pdfmakeDocument", function(pdf, target) {

          // ...
        
          // Add logo
          pdf.doc.content.unshift({
            image: "https://homepages.cae.wisc.edu/~ece533/images/airplane.png", // actual content removed
            fit: [119, 54]
          });
        
          return pdf;
        });
      }
    }
    let thisRef = this;
    if (this.p == 1 || is_internal) {
      this.charts_data = data;
      this.clearChart("chartContainer");
      this.clearChart("chartContainer2");
      // this.clearChart("pieContainer");
      this.top_charts = {
        top_available: false,
        least_available: false,
        // pie_available: false
      };
      if (data && data.frequency_of_tagged_standars_chart && data.frequency_of_tagged_standars_chart.standarads_tag.length > 0) {
        this.top_charts.top_available = true;
        let standard_tags = data.frequency_of_tagged_standars_chart.standarads_tag;
        this.assign_colors(standard_tags, "bar");
        this.optionsChart.dataProvider = standard_tags;
        this.AmCharts.makeChart("chartContainer2", this.optionsChart);
        this.chartContainer2Loaded = true;
      } else this.chartContainer2Loaded = true;
      if (data && data.frequency_of_tagged_standars_chart_least && data.frequency_of_tagged_standars_chart_least.standarads_tag_least && data.frequency_of_tagged_standars_chart_least.standarads_tag_least.length > 0) {
        let standard_tags_least = data.frequency_of_tagged_standars_chart_least.standarads_tag_least;
        this.assign_colors(standard_tags_least, "bar");
        this.optionsChart2.dataProvider = standard_tags_least;
        this.top_charts.least_available = true;
        this.AmCharts.makeChart("chartContainer", this.optionsChart2);
        this.chartContainerLoaded = true;
      } else this.chartContainerLoaded = true;
      // if ((data && data.custom_markers_tag && data.custom_markers_tag.length > 0)) {
      //   let custom_markers_tag = data.custom_markers_tag.filter(item => {
      //     return item.litres > 0
      //   });
      //   this.assign_colors(custom_markers_tag, "pie");
      //   this.pieData.dataProvider = custom_markers_tag;
      //   this.top_charts.pie_available = true;
      //   let pieChart = this.AmCharts.makeChart("pieContainer", this.pieData);
      //   pieChart.addListener('clickSlice', this.handleClick);
      //   this.pieContainerLoaded = true;
      // } else this.pieContainerLoaded = true;


    } else {
      this.chartContainer2Loaded = true;
      this.chartContainerLoaded = true;
      // this.pieContainerLoaded = true;
    }
    if (data.serial_charts && (!tags_standards_only || tags_standards_only == false)) {
      if (!this.charts_data) this.charts_data = {};
      this.charts_data.serial_charts = data.serial_charts;
      let that = this;
      this.serialCharts = data.serial_charts;
      this.serialChartCount = data.serial_charts_count;
      let folder_type = this.bodyService.GetFilter("folder_type");
      data.serial_charts.forEach((c, i) => {
        setTimeout(function (argument) {
          let data = JSON.parse(JSON.stringify(that.serialData));
          data.legend.divId = "legenddiv_" + i;
          data.legend.data = [
            {
              title: c.title + ":" + c.total_tagged_standards,
              color: that.list[(i % 2 == 0) ? 0 : 1]
            }
          ];
          if (folder_type != 5) {
            data.legend.data.push(
              {
                title: c.ratting_title + ":" + c.total_avg,
                color: "#000"
              }
            );
          }
          data.graphs = [
            {
              "valueAxis": "ValueAxis-" + i,
              "colorField": "color",
              "fixedColumnWidth": 15,
              "balloonText": c.title + ": " + "[[value]]",
              "fillAlphas": 0.8,
              "id": "AmGraph-" + c.count,
              "lineAlpha": 0.2,
              "title": c.title,
              "type": "column",
              "valueField": "total_tags"
            },
            {
              "valueAxis": "ValueAxis-r",
              "colorField": "color_rating",
              "balloonText": "[[average_rating_name]]:[[value]]",
              "fixedColumnWidth": 15,
              "fillAlphas": 0.8,
              "lineAlpha": 0.2,
              "title": thisRef.translation.analytics_performance_level,
              "type": "column",
              "valueField": "standard_rating_avg",
            }
          ];
          data.allLabels = [
            {
              "bold": true,
              "id": "standard-tag",
              "tabIndex": -5,
              "text": thisRef.translation.analytics_standard_tags,
              "x": 15,
              "y": 13
            },
            {
              "bold": true,
              "id": "performance-level",
              "text": thisRef.translation.analytics_performance_level,
              "x": 820,
              "y": 13
            }
          ]
          data.valueAxes = [
            {
              "id": "ValueAxis-" + i,
              "position": "left",
              "axisAlpha": 1,
              "title": "",
              "titleBold": false,
              "titleFontSize": "10",
              "axisColor": "#CCCCCC",
              "integersOnly": true,
             // "maximum": c.assessment_array.length - 1
            },
            {
              "id": "ValueAxis-r",
              "axisAlpha": 1,
              "autoGridCount": true,
              "gridCount": 6,
              "integersOnly": true,
              "dashLength": 1,
              "position": "right",
              "axisColor": "#CCCCCC",
              "minimum": 0,
              "maximum": c.assessment_array.length - 1,
              "valueText": "standard_rating_avg",
              "title": "",
              "titleBold": false,
              "labelFunction": (value) => {
                let assessment_array = that.bodyService.GetPLData();
                if (!assessment_array) return value;
                return (assessment_array[value]) ? assessment_array[value] : value;
              },
              "titleFontSize": "10",
              "minVerticalGap": 1,
            }
          ];
          data.dataProvider = c.data_provider;
          that.AmCharts.makeChart("serial_" + i, data);
        }, 500);
      });
    }
  }

  adjustBalloonText(graphDataItem, graph) {
    let accounts = '';
  	const sortable = Object.entries(graphDataItem.dataContext.composition)
			.sort(([, a], [, b]) => (<any>b)-(<any>a) )
			.reduce((r, [k, v]) => ({ ...r, [k]: v }), {});
		for (const property in sortable) {
			accounts = accounts + ' ' + property + ' (' + graphDataItem.dataContext.composition[property] + ')<br>'
		}
    // var value = graphDataItem.values.value;
    return graph + "<br>" + accounts;
    // if(value < 500){
    //     return graph + "<br>(Little)";
    // }
    // else{
    //     return graph + "<br>(A Lot)";
    // }
  }

  handleClick(event) {

    // alert();
  }
  private assign_colors(data, type) {
    if (type == "bar" || type == "serial") {
      data.forEach((_d, i) => { _d.color = this.list[i % 2 == 0 ? 0 : 1] });
    } else if (type == "pie") {
      // data.forEach((_d, i) => { _d.color = this.list[i] });
    } else if (type == "serial") {
      data.forEach((_d, i) => { if (i % 2 == 0) { _d.color = _d.color_rating = this.list[0] } else { _d.color = _d.color_rating = this.list[1] } });
    }
  }
  private LoadCharts(is_first = false, page = 0) {

    // if (filters.account_id == filters.subAccount) {
    // 	filters.account_id = "";
    // }
    if ((!this.filterObject.duration || this.filterObject.duration == -1) && !this.filterObject.start_date) {
      return;
    }
    this.subscriptions.add(this.bodyService.GetCharts(this.filterObject).subscribe(data => this.RenderCharts(data)));
  }

  getChartData(data) {
    this.RenderCharts(data, false, true);
  }

  public export_excel() {
    
    
    let obj: any = {};
    this.loadingModalRef = this.modalService.show(this.loadingModal, { backdrop: 'static' });
    this.bodyService.Export(this.filterObject).subscribe((data: any) => {
      let file_name = data.file_name;
      let interval_id = setInterval(() => {
        obj.file_name = file_name;
        this.filterObject.file_name = file_name;
        this.bodyService.is_download_ready(this.filterObject).subscribe((data: any) => {
          let download_status = data.download_status;
          if (download_status) {
            clearInterval(interval_id);
            obj.download_url = data.download_url;
            obj.file_name = file_name;
            this.loadingModalRef.hide();
            this.bodyService.download_analytics_standards_to_excel(obj);
          }
        });
      }, 3000);
    });
  }

  checkPermission() {
    let account_id = this.header_data.user_current_account.accounts.account_id;
    let user_id = this.header_data.user_current_account.users_accounts.user_id;
    let start_date = moment().subtract(1, 'years').format('YYYY-MM-DD');
    let end_date = moment().format('YYYY-MM-DD');
    let folder_type = 2;
    if (this.header_data.user_permissions.UserAccount.permission_view_analytics == 1 || this.header_data.user_permissions.UserAccount.role_id == 115) {
      // if (this.header_data.user_permissions.UserAccount.role_id == 120) {
      // 	this.router.navigate([`/analytics_angular/playcard/${account_id}/${user_id}/${start_date}/${end_date}/${folder_type}`]);
      // 	return;
      // }
    }
    else if (this.header_data.user_permissions.UserAccount.role_id == 120) {
      this.router.navigate([`/analytics_angular/playcard/${account_id}/${user_id}/${start_date}/${end_date}/${folder_type}`]);
      return;
    }
    else {
      this.router.navigate(['/page-not-found']);
      return;
    }
  }

  public getPage(page) {
    this.p = page;
    // this.router.navigate([], {
    // 	relativeTo: this.activatedRoute,
    // 	queryParams: { page: page },
    // 	queryParamsHandling: 'merge'
    // })
    // this.LoadCharts(false, page - 1);
    this.mainAnalyticsService.pageChanged.emit(this.p)
  }
  
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}

interface top_charts {
  top_available: boolean,
  least_available: boolean,
  // pie_available: boolean
}
