import { Component, OnInit, Input, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { KeyValue } from '@angular/common';
import { HeaderService } from '@src/project-modules/app/services';
import { TrackerFiltersComponent } from '@src/project-modules/shared/components';
import { BodyService, SummaryService, TabsService } from '../../services';
import { MainAnalyticsService } from '../../services/main-analytics.service';
import { Router } from '@angular/router';
import { encodeCompress, guidGenerator, formatDateUtil, getPathAndQuery } from '@src/project-modules/shared/components/common/utility';
import { TrackerFiltersService } from '@src/project-modules/shared/services/tracker-filters.service';
import { LocalStorageService } from '@src/project-modules/app/services/localStorage.service';
import { NgScrollbar } from 'ngx-scrollbar';
import { ThrowStmt } from '@angular/compiler';



@Component({
  selector: 'app-new-user-summary',
  templateUrl: './new-user-summary.component.html',
  styleUrls: ['./new-user-summary.component.css']
})
export class NewUserSummaryComponent implements OnInit {
  @ViewChild(TrackerFiltersComponent) trackerFiltersComponent: TrackerFiltersComponent
  @ViewChild('stickyDiv') stickyDiv: ElementRef;
  @ViewChild('headerScrollBar') headerScrollBarRef: NgScrollbar;
  private subscription: Subscription;
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  p: number = 1;
  translation: any;
  @ViewChild('scrollable') scrollbarRef: NgScrollbar;
  public count: number = 0;
  headers = [] as any;
  attributes = [] as any;
  sortByAsc: boolean;
  SearchString = '';
  imageStaticUrl: any;
  baseUrl: any;
  public header_data;
  imageBaseUrl: any;
  config = {
    id: '',
    currentPage: 1,
    itemsPerPage: 10,
    totalItems: 20
  };



  isLoading = true;
  filterObj: any;
  width: any;
  sort_by: any;
  sort_by_order: any;
  private subscriptions: Subscription = new Subscription();
  public scrollWidth: number;
  public enableArrow: boolean;

  constructor(private headerService: HeaderService,
    private summaryService: SummaryService,
    private tabService: TabsService,
    private mainAnalyticsService: MainAnalyticsService,
    private router: Router, private localStorage: LocalStorageService,
    private trackerFilterServivce: TrackerFiltersService
  ) {
    this.tabService.typeChange.emit('user-summary-main')
    this.header_data = this.headerService.getStaticHeaderData();
    if (Object.keys(this.header_data).length != 0 && this.header_data.constructor === Object) {
      this.header_data = this.headerService.getStaticHeaderData();
    }
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
  }

  ngOnInit() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    };

    this.subscriptions.add(this.summaryService.analyticsData.subscribe(data => {
      this.handleChange(data);
    }));

    this.subscriptions.add(this.mainAnalyticsService.loadingChanged.subscribe(loading => {
      this.isLoading = loading;
    }));

    this.subscription.add(this.trackerFilterServivce.filtersKeys.subscribe(filterObj => {
      if (filterObj) {
        this.filterObj = filterObj
      }

    }));


  }
  scrollRight(){
      this.count = this.count + 150;
      this.scrollbarRef.scrollTo({left: this.count});
      this.headerScrollBarRef.scrollTo({left: this.count});
  }
  scrollLeft(){
    if (this.count === 0){
      this.count = 150;
      return;
    } else {
      this.count = this.count - 150;
      this.scrollbarRef.scrollTo({left: this.count});
      this.headerScrollBarRef.scrollTo({left: this.count});
    }
  }

  showArrowLeft() {
    if (this.count === 0){
      return false;
    }else {
      return true;
    }

  }

  showArrowRight() {
    const scroll = document.getElementById('scrollBar');

    if (this.scrollWidth && scroll){
      return scroll.scrollWidth < this.scrollWidth && (scroll.scrollWidth + this.count) < this.scrollWidth;
    }else{
      return false;
    }

  }

  scrollSync(selector) {
    let active = null;
    document.querySelectorAll(selector).forEach((element) => {
      element.addEventListener("mouseenter", (e) => {
        active = e.target;
      });
      element.addEventListener("scroll", (e) => {
        if (e.target !== active) return;

        document.querySelectorAll(selector).forEach((target) => {
          if (active === target) return;
          target.scrollLeft = active.scrollLeft;
        });
      });
    });
  }

  originalOrder = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => {
    return 0;
  }


  handleChange(data) {

    this.sort_by = data.sort_by;
    this.sort_by_order = data.sort_by_order;
    this.headers = data.attributes_header;
    this.attributes = data.attributes_data;
    this.config.totalItems = data.total_users;
    this.config.currentPage = data.page;
    setTimeout(() => {

      this.width = this.stickyDiv?.nativeElement.children.length * 150 + 120 + 'px';
      this.scrollWidth = this.stickyDiv?.nativeElement.children.length * 150 + 120;
      this.scrollSync(".ng-scroll-viewport");

    }, 1000);
    this.isLoading = false;


  }

  turnOnCollapse(attribute) {
    this.attributes.map(x => x.collapse = false)
    attribute.collapse = true
  }

  onSearch(searchString) {
    this.SearchString = searchString
  }


  sortTable(value, order) {
    if (order)
      this.mainAnalyticsService.sortData.emit({ value: value.sorting_key, order: order })
    // this.trackerFiltersComponent.sortTable(value.sorting_key, order)
    else
      this.mainAnalyticsService.sortData.emit({ value: "", order: order })
    // this.trackerFiltersComponent.sortTable("", order)




    // if (this.sortByAsc) {
    //   this.sortByAsc = !this.sortByAsc;
    //   this.attributes.sort((a, b) => {

    //     return isNaN(a[j] - b[j]) ? (a[j] === b[j]) ? 0 : (a[j] < b[j]) ? -1 : 1 : a[j] - b[j];
    //   }
    //   );
    // }
    // else {
    //   this.sortByAsc = !this.sortByAsc;

    //   this.attributes.sort((a, b) => {
    //     return isNaN(b[j] - a[j]) ? (b[j] === a[j]) ? 0 : (b[j] < a[j]) ? -1 : 1 : b[j] - a[j];
    //   });
    // }

  }


  getImage(userId, image) {
    if (image) {
      return this.imageBaseUrl = 'https://s3.amazonaws.com/sibme.com/static/users/' + userId + '/' + image;
    } else return this.header_data.base_url + "/img/home/photo-default.png";
  }

  isNumeric(value) {
    return isNaN(value);
  }

  onPageChange(event) {
    this.config.currentPage = event;
    // this.trackerFiltersComponent.setPage(event);
    this.mainAnalyticsService.pageChanged.emit(event);
  }

  onPerPageLimitChange() {
    this.mainAnalyticsService.limitChanged.emit(this.config.itemsPerPage);
    // this.trackerFiltersComponent.setItemsPerPage(this.config.itemsPerPage)
  }

  exportReport(exportType, active) {
    this.trackerFiltersComponent.setExportReport(exportType, active)
  }
  navigateToPlayerCard(user_id, account_id) {
    let queryParamsFrom = { displayName: 'analytics_user_summary', url: this.router.url };
    let queryObj = { from: encodeCompress(queryParamsFrom) };
    queryObj = { ...queryObj, ...this.filterObj };
    this.router.navigate(['/analytics_angular/playercard', account_id, user_id, formatDateUtil(this.filterObj.start_date), formatDateUtil(this.filterObj.end_date)], { queryParams: queryObj });
  }
  navigateToDetail(data, index) {

    this.headers.forEach((element) => {
      element['fake_id'] = guidGenerator()
    });
    let header = this.headers[index];
    const obj = {
      coach_id: '',
      user_id: data.user_id,
      account_id: data.account_id,
      report_type: 'user_summary_report',
      huddle_type: this.filterObj.huddle_ids,
      huddle_id: '',
      start_date: this.filterObj.start_date,
      end_date: this.filterObj.end_date,
      duration: this.filterObj.duration,
      header: [header],
      path: getPathAndQuery()
    }


    this.localStorage.set(obj.report_type, this.headers);
    //  let key = this.headers[index];
    // key = key.toLowerCase();
    // key = key.replace(/ /g,"_");
    this.headerService.reportsDetailData(obj);
    this.router.navigate(['trackers/report-detail-view/' + header.key], { queryParams: { query: encodeCompress(obj) } });

  }

  setSortingOrder(value, order, index) {

    if (value.sorting) {
      if (this.sort_by_order == '' && this.sort_by_order != 'DESC' && this.sort_by_order != 'ASC') {
        this.sortTable(value, 'ASC');
      }
      if (this.sort_by_order != '' && this.sort_by_order != 'DESC' && this.sort_by_order == 'ASC') {
        this.sortTable(value, 'DESC');
      }
      if (this.sort_by_order != '' && this.sort_by_order == 'DESC' && this.sort_by_order != 'ASC') {
        this.sortTable(value, '');
      }
    } else return false;

  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscriptions.unsubscribe();
  }
}
