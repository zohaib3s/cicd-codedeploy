import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { AmChartsService } from "@amcharts/amcharts3-angular";
import { HeaderService } from "@projectModules/app/services";
import { TabsService, BodyService } from "@analytics/services";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { TrackerFiltersService } from '@src/project-modules/shared/services/tracker-filters.service';
@Component({
	selector: 'tagsnpls',
	templateUrl: './tagsnpls.component.html',
	styleUrls: ['./tagsnpls.component.css']
})
export class TagsnplsComponent implements OnInit, OnDestroy {
	public optionsChart;
	public optionsChart2;
	public pieData;
	public serialCharts;
	public serialChartCount;
	public serialData;
	public top_charts: top_charts;
	public current_chart_tab: number = 1;
	private list = ["#025c8a", "#28a745", "#004061", "#5e9d31", "#4f91cd"];
	private charts_data;
	public p;
	private loadingModalRef: BsModalRef;
	public header_data;
	public translation: any = {};
	public translationLoaded: boolean = false;
	public chartContainerLoaded: boolean = false;
	public chartContainer2Loaded: boolean = false;
	public pieContainerLoaded: boolean = false;
	private subscriptions: Subscription = new Subscription();
	@ViewChild("loadingModal", { static: false }) loadingModal;
	filterObject = null;
	constructor(
		private modalService: BsModalService,
		private tabsService: TabsService,
		private AmCharts: AmChartsService,
		private headerService: HeaderService,
		private bodyService: BodyService,
		private trackerFiltersService: TrackerFiltersService,
		private router: Router,
		private activatedRoute: ActivatedRoute) {
		this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
			this.translation = languageTranslation;
			if (this.translation.analytics_top_five_tagged_standard && !this.translationLoaded) {  // check if translation for analytics is loaded
				this.translationLoaded = true;
			}
		}));
		this.tabsService.typeChange.emit('tags');
		this.subscriptions.add(this.tabsService.typeChange.subscribe(type => {
			if (type) {
			}
		}));
		this.activatedRoute.queryParams.subscribe(queryParams => {
			if (queryParams.page) this.p = queryParams.page;
		});

	}
	ngOnInit() {

		this.header_data = this.headerService.getStaticHeaderData();
		this.checkPermission()
		this.top_charts = { top_available: false, least_available: false, pie_available: false }
		let xlsx_menu_item = { "label": "XLSX", "click": () => { this.export_excel(); } };
		this.RunSubscribers();
		this.optionsChart = {
			"export": {
				"enabled": true,
				"menu": [{
					"class": "export-main",
					"menu": ["PNG", "JPG", "SVG", "PDF"]
				}]
			},
			"type": "serial",
			"categoryField": "country",
			"rotate": true,
			"hideCredits": true,
			"autoMarginOffset": 40,
			"marginRight": 60,
			"marginTop": 60,
			"zoomOutButtonColor": "#FFFFFF",
			"colors": [
				"#0000"
			],
			"startDuration": 1,
			"startEffect": "easeOutSine",
			"backgroundAlpha": 0.79,
			"fontFamily": "Roboto",
			"fontSize": 13,
			"theme": "default",
			"categoryAxis": {
				"autoRotateAngle": 1.8,
				"autoWrap": true,
				"gridPosition": "start",
				"axisAlpha": 0,
				"axisThickness": 0,
				"fillColor": "#ED1616",
				"gridCount": 11,
				"minorGridAlpha": 0,
				"offset": 30,
				"titleColor": "#FFFFFF"
			},
			"trendLines": [],
			"graphs": [
				{
					"balloonColor": "#FFFFFF",
					"balloonText": "[[title]] of [[category]]:[[value]]",
					"bulletBorderThickness": 0,
					"bulletSize": 0,
					"colorField": "color",
					"fillAlphas": 1,
					"gapPeriod": -5,
					"id": "AmGraph-1",
					"labelText": "",
					"precision": -9,
					"title": "graph 1",
					"type": "column",
					"valueField": "litres",
					"fixedColumnWidth": 20
				}
			],
			"guides": [],
			"valueAxes": [
				{
					"id": "ValueAxis-1",
					"axisColor": "#E1E1E1",
					"tickLength": 1,
					"title": "",
					"titleRotation": 1,
					"integersOnly": true
				}
			],
			"allLabels": [],
			"balloon": {
				"borderAlpha": 0,
				"borderColor": "#000000",
				"borderThickness": 0,
				"color": "#FDFDFD",
				"fillColor": "#040404"
			},
			"titles": [],
			"dataProvider": [
				{
					"category": this.translation.analytics_data_and_assessment,
					"column-1": 3,
					"column-2": 5,
					"color": "#5e9d31"
				},
				{
					"category": this.translation.analytics_standard_and_alignment,
					"column-1": 8,
					"column-2": 8,
					"color": "#004160"
				},
				{
					"category": this.translation.analytics_content_knowledge_expertise,
					"column-1": 6,
					"column-2": 5,
					"color": "#5191cc"
				},
				{
					"category": this.translation.analytics_knowledge_of_std,
					"column-1": 4,
					"column-2": "5",
					"color": "#5e9d31"
				},
				{
					"category": this.translation.analytics_achieving_expectations,
					"column-1": 7,
					"column-2": "9",
					"color": "#004160"
				}
			]
		};
		this.serialData = {
			"hideCredits": true,
			"type": "serial",
			"path": "/amcharts/",
			"theme": "light",
			"categoryField": "date",
			"rotate": false,
			"marginTop": 40,
			"marginBottom": 0,
			"startEffect": 'easeInSine',
			"startDuration": 0.5,
			"autoMargins": true,
			"addClassNames": true,
			"categoryAxis": {
				"gridPosition": "start",
				"position": "left",
			},
			"balloon": {
				"borderAlpha": 0,
				"borderColor": "#000000",
				"borderThickness": 0,
				"color": "#FDFDFD",
				"fillColor": "#040404"
			},
			"legend": {
				"equalWidths": false,
				"autoMargins": false,
				"position": "bottom",
				"valueAlign": "left",
				"markerType": "square",
				"valueWidth": 2,
				"verticalGap": 2,
				"data": []
			},
			"graphs": [
				{
					"valueAxis": "ValueAxis-17",
					"colorField": "color",
					"fixedColumnWidth": 10,
					"balloonText": this.translation.analytics_data_and_assessment + ":[[value]]",
					"fillAlphas": 0.8,
					"id": "AmGraph-17",
					"lineAlpha": 0.2,
					"title": this.translation.analytics_data_and_assessment,
					"type": "column",
					"valueField": "total_tags",
				},
				{
					"valueAxis": "ValueAxis-r",
					"colorField": "color_rating",
					"balloonText": "[[average_rating_name]]:[[value]]",
					"fixedColumnWidth": 10,
					"fillAlphas": 0.8,
					"lineAlpha": 0.2,
					"title": "Performance Level",
					"type": "column",
					"valueField": "standard_rating_avg",
				}
			],
			"guides": [],
			"valueAxes": [
				{
					"id": "ValueAxis-17",
					"position": "left",
					"axisAlpha": 1,
					"title": this.translation.analytics_number_of_standard_tags,
					"titleBold": false,
					"titleFontSize": "10",
					"integersOnly": true
				},
				{
					"id": "ValueAxis-r",
					"axisAlpha": 1,
					"autoGridCount": true,
					"gridCount": 6,
					"integersOnly": true,
					"dashLength": 1,
					"position": "right",
					"minimum": 0,
					"maximum": 6,
					"valueText": "standard_rating_avg",
					"labelFunction": function (v) {
					},
					"title": "Performance Level",
					"titleBold": false,
					"titleFontSize": "10",
					"minVerticalGap": 1,
				}
			],
			"dataProvider": [{ "date": "Aug-17", "color": "#85c4e3", "standard_rating_avg": 2, "average_rating_name": "Developing", "color_rating": "#000", "total_tags": "1" }, { "date": "Sep-17", "color": "#85c4e3", "standard_rating_avg": 2, "average_rating_name": "Developing", "color_rating": "#000", "total_tags": "0" }, { "date": "Oct-17", "color": "#85c4e3", "standard_rating_avg": "No Ratings", "average_rating_name": "No Ratings", "color_rating": "#000", "total_tags": "1" }, { "date": "Nov-17", "color": "#85c4e3", "standard_rating_avg": 3, "average_rating_name": "Proficient", "color_rating": "#000", "total_tags": "3" }, { "date": "Dec-17", "color": "#85c4e3", "standard_rating_avg": "No Ratings", "average_rating_name": "No Ratings", "color_rating": "#000", "total_tags": "2" }, { "date": "Jan-18", "color": "#85c4e3", "standard_rating_avg": 2, "average_rating_name": "Developing", "color_rating": "#000", "total_tags": "5" }, { "date": "Feb-18", "color": "#85c4e3", "standard_rating_avg": 3, "average_rating_name": "Proficient", "color_rating": "#000", "total_tags": "0" }, { "date": "Mar-18", "color": "#85c4e3", "standard_rating_avg": "No Ratings", "average_rating_name": "No Ratings", "color_rating": "#000", "total_tags": "1" }, { "date": "Apr-18", "color": "#85c4e3", "standard_rating_avg": "No Ratings", "average_rating_name": "No Ratings", "color_rating": "#000", "total_tags": "0" }, { "date": "May-18", "color": "#85c4e3", "standard_rating_avg": "No Ratings", "average_rating_name": "No Ratings", "color_rating": "#000", "total_tags": "0" }, { "date": "Jun-18", "color": "#85c4e3", "standard_rating_avg": "No Ratings", "average_rating_name": "No Ratings", "color_rating": "#000", "total_tags": "0" }, { "date": "Jul-18", "color": "#85c4e3", "standard_rating_avg": "No Ratings", "average_rating_name": "No Ratings", "color_rating": "#000", "total_tags": "0" }],
			"export": {
				"enabled": true,
				"menu": [{
					"class": "export-main",
					"menu": ["PNG", "JPG", "SVG", "PDF"]
				}]
			},
		};
		this.optionsChart2 = JSON.parse(JSON.stringify(this.optionsChart));
		this.optionsChart2.export.menu[0].menu.push(xlsx_menu_item);
		this.optionsChart.export.menu[0].menu.push(xlsx_menu_item);
		var balloonText = '[[country]]<br><b><span style=\'font-size:14px;\'>[[value]]</span></b> [[percents]]%\
          <table>\
            <tr><th>Accounts </th></tr>\
			<ng-container *ngFor="let item of [[accounts]] ">\
			 <tr><td>[[litres]]</td></tr>\
			</ng-container>\
          </table>';
		this.pieData = {
			"export": {
				"enabled": true,
				"menu": [{
					"class": "export-main",
					"menu": ["PNG", "JPG", "SVG", "PDF", xlsx_menu_item]
				}]
			},
			"type": "pie",
			"theme": "none",
			// 'balloonText': balloonText,
			balloonFunction : this.adjustBalloonText,
			"labelText": "[[percents]]%",
			"hideCredits": true,
			"startEffect": "easeInSine",
			"startAlpha": 0,
			"startRadius": 0.5,
			"legend": {
				"position": "bottom",
				"align": "center",
				"marginRight": 100,
				"autoMargins": true,
				"equalWidths": true,
				"valueWidth": 0,
			},
			"dataProvider": [
			],
			"valueField": "litres",
			"titleField": "country",
			"colorField": "color",
			"balloon": {
				"borderAlpha": 0,
				"borderColor": "#000000",
				"borderThickness": 0,
				"color": "#FDFDFD",
				"fillColor": "#040404"
			},
			  "listeners": [{
        "event": "clickSlice",
        "method":this.handleClick
    }]
		};
		this.refactorChartLabels();
	}
	ngAfterViewInit() {
		// this.HandleSubmit();
	}
	public activate_chart_tab(n) {
		this.current_chart_tab = n;
		if (n == 1) {
			let that = this;
			that.clearChart("chartContainer");
			that.clearChart("chartContainer2");
			setTimeout(() => {
				that.activateLinearCharts();
			}, 500);
		}
	}
	private clearChart(id) {
		var allCharts = this.AmCharts.charts;
		for (var i = 0; i < allCharts.length; i++) {
			if (allCharts[i].div && id == allCharts[i].div.id) {
				allCharts[i].clear();
			}
		}
	}
	private activateLinearCharts() {
		if (this.charts_data) {
			this.RenderCharts(this.charts_data, true, true);
		}
	}
	private refactorChartLabels() {
		this.AmCharts.addInitHandler(function (chart) {
			if (chart.dataProvider && chart.dataProvider.length > 0 && chart.dataProvider[0].country) {
				for (var i = 0; i < chart.dataProvider.length; i++) {
					var label = chart.dataProvider[i].country;
					if (label && label.length && label.length > 20) {
						label = label.substr(0, 20) + '...'
					} else if (label && label.length && label.length <= 20) {
					}
					chart.dataProvider[i].country = label;
				}
			}
		}, ["serial"]);
	}
	private RunSubscribers() {
		this.subscriptions.add(this.tabsService.Tabs.subscribe(tab => this.ActivateTab(tab)));
		// this.tabsService.FiltersSubmitted.subscribe(data => {
		// 	// this.HandleSubmit()
		// });


		this.subscriptions.add(this.bodyService.triggerTagsDataChange.subscribe(tagsData => {
			this.getChartData(tagsData);
		}));

		this.subscriptions.add(this.trackerFiltersService.filtersKeys.subscribe(data => {
			if (data['type'] == 'tags') {
				this.filterObject = data;
				// this.HandleSubmit();
				if (data['framework_id']) {
					this.GetPLAssesmentArray();
				}
			}
		})
		);
	}

	GetPLAssesmentArray() {
		let obj = {
			framework_id: this.filterObject['framework_id'],
			account_id: this.header_data.user_current_account.accounts.account_id
		}
		this.bodyService.getAssesmentArray(obj).subscribe((data: any) => {
			this.UpdatePLData(data)
		});
	}

	private UpdatePLData(assessment_array) {

		if (assessment_array) {

			this.bodyService.StorePLData(assessment_array);

		}

	}
	public HandleSubmit() {
		this.p = 1;
		this.current_chart_tab = 1;
		let filters = this.bodyService.GetFilters();
		// if (filters && filters.subAccount != '') this.LoadCharts(false);
		this.LoadCharts(false);
		if (filters && filters.subAccount != '') this.LoadCharts(false);
		else {
			this.chartContainer2Loaded = true;
			this.chartContainerLoaded = true;
			this.pieContainerLoaded = true;
		}
	}
	private ActivateTab(tab) {
		let filters = this.bodyService.GetFilters();
		if (tab == 1) {
			if (filters && filters.subAccount != '') this.LoadCharts(true);
			else {
				this.chartContainer2Loaded = true;
				this.chartContainerLoaded = true;
				this.pieContainerLoaded = true;
			}
		}
	}
	public getPage(page) {
		this.p = page;
		// this.router.navigate([], {
		// 	relativeTo: this.activatedRoute,
		// 	queryParams: { page: page },
		// 	queryParamsHandling: 'merge'
		// })
		this.LoadCharts(false, page - 1);
	}
	private RenderCharts(data, tags_standards_only?, is_internal?) {
		let folder_type = this.bodyService.GetFilter("folder_type");
		if (void 0 != folder_type) {
			if (+folder_type == 6) {
				this.optionsChart2.export.menu[0].menu.pop();
				this.optionsChart.export.menu[0].menu.pop();
			} else {
				let found = this.optionsChart2.export.menu[0].menu.some(item => item.label == "XLSX");
				if (!found) {
					let xlsx_menu_item = { "label": "XLSX", "click": () => { this.export_excel(); } };
					this.optionsChart2.export.menu[0].menu.push(xlsx_menu_item);
					this.optionsChart.export.menu[0].menu.push(xlsx_menu_item);
				}
			}
		}
		let thisRef = this;
		if (this.p == 1 || is_internal) {
			this.charts_data = data;
			this.clearChart("chartContainer");
			this.clearChart("chartContainer2");
			this.clearChart("pieContainer");
			this.top_charts = {
				top_available: false,
				least_available: false,
				pie_available: false
			};
			if (data && data.frequency_of_tagged_standars_chart && data.frequency_of_tagged_standars_chart.standarads_tag.length > 0) {
				this.top_charts.top_available = true;
				let standard_tags = data.frequency_of_tagged_standars_chart.standarads_tag;
				this.assign_colors(standard_tags, "bar");
				this.optionsChart.dataProvider = standard_tags;
				this.AmCharts.makeChart("chartContainer2", this.optionsChart);
				this.chartContainer2Loaded = true;
			} else this.chartContainer2Loaded = true;
			if (data && data.frequency_of_tagged_standars_chart_least && data.frequency_of_tagged_standars_chart_least.standarads_tag_least && data.frequency_of_tagged_standars_chart_least.standarads_tag_least.length > 0) {
				let standard_tags_least = data.frequency_of_tagged_standars_chart_least.standarads_tag_least;
				this.assign_colors(standard_tags_least, "bar");
				this.optionsChart2.dataProvider = standard_tags_least;
				this.top_charts.least_available = true;
				this.AmCharts.makeChart("chartContainer", this.optionsChart2);
				this.chartContainerLoaded = true;
			} else this.chartContainerLoaded = true;
			if ((data && data.custom_markers_tag && data.custom_markers_tag.length > 0)) {
				let custom_markers_tag = data.custom_markers_tag.filter(item => {
					return item.litres > 0
				});
				this.assign_colors(custom_markers_tag, "pie");
				this.pieData.dataProvider = custom_markers_tag;
				this.top_charts.pie_available = true;
				let pieChart = this.AmCharts.makeChart("pieContainer", this.pieData);
				pieChart.addListener('clickSlice', this.handleClick);
				this.pieContainerLoaded = true;
			} else this.pieContainerLoaded = true;
		} else {
			this.chartContainer2Loaded = true;
			this.chartContainerLoaded = true;
			this.pieContainerLoaded = true;
		}
		if (data.serial_charts && (!tags_standards_only || tags_standards_only == false)) {
			if (!this.charts_data) this.charts_data = {};
			this.charts_data.serial_charts = data.serial_charts;
			let that = this;
			this.serialCharts = data.serial_charts;
			this.serialChartCount = data.serial_charts_count;
			let folder_type = this.bodyService.GetFilter("folder_type");
			data.serial_charts.forEach((c, i) => {
				setTimeout(function (argument) {
					let data = JSON.parse(JSON.stringify(that.serialData));
					data.legend.divId = "legenddiv_" + i;
					data.legend.data = [
						{
							title: c.title + ":" + c.total_tagged_standards,
							color: that.list[(i % 2 == 0) ? 0 : 1]
						}
					];
					if (folder_type != 5) {
						data.legend.data.push(
							{
								title: c.ratting_title + ":" + c.total_avg,
								color: "#000"
							}
						);
					}
					data.graphs = [
						{
							"valueAxis": "ValueAxis-" + i,
							"colorField": "color",
							"fixedColumnWidth": 15,
							"balloonText": c.title + ": " + "[[value]]",
							"fillAlphas": 0.8,
							"id": "AmGraph-" + c.count,
							"lineAlpha": 0.2,
							"title": c.title,
							"type": "column",
							"valueField": "total_tags"
						},
						{
							"valueAxis": "ValueAxis-r",
							"colorField": "color_rating",
							"balloonText": "[[average_rating_name]]:[[value]]",
							"fixedColumnWidth": 15,
							"fillAlphas": 0.8,
							"lineAlpha": 0.2,
							"title": thisRef.translation.analytics_performance_level,
							"type": "column",
							"valueField": "standard_rating_avg",
						}
					];
					data.allLabels = [
						{
							"bold": true,
							"id": "standard-tag",
							"tabIndex": -5,
							"text": thisRef.translation.analytics_standard_tags,
							"x": 15,
							"y": 13
						},
						{
							"bold": true,
							"id": "performance-level",
							"text": thisRef.translation.analytics_performance_level,
							"x": 820,
							"y": 13
						}
					]
					data.valueAxes = [
						{
							"id": "ValueAxis-" + i,
							"position": "left",
							"axisAlpha": 1,
							"title": "",
							"titleBold": false,
							"titleFontSize": "10",
							"axisColor": "#CCCCCC",
							"integersOnly": true
						},
						{
							"id": "ValueAxis-r",
							"axisAlpha": 1,
							"autoGridCount": true,
							"gridCount": 6,
							"integersOnly": true,
							"dashLength": 1,
							"position": "right",
							"axisColor": "#CCCCCC",
							"minimum": 0,
							"maximum": 5,
							"valueText": "standard_rating_avg",
							"title": "",
							"titleBold": false,
							"labelFunction": (value) => {
								let assessment_array = that.bodyService.GetPLData();
								if (!assessment_array) return value;
								return (assessment_array[value]) ? assessment_array[value] : value;
							},
							"titleFontSize": "10",
							"minVerticalGap": 1,
						}
					];
					data.dataProvider = c.data_provider;
					that.AmCharts.makeChart("serial_" + i, data);
				}, 500);
			});
		}
	}

adjustBalloonText(graphDataItem, graph){
	let accounts='';
	for (const property in graphDataItem.dataContext.composition) {
  accounts=accounts+' '+property+' ('+ graphDataItem.dataContext.composition[property]+')<br>'
}
    // var value = graphDataItem.values.value;
return graph+ "<br>"+accounts;
    // if(value < 500){
    //     return graph + "<br>(Little)";
    // }
    // else{
    //     return graph + "<br>(A Lot)";
    // }
}

	handleClick(event) {
		
		// alert();
	}
	private assign_colors(data, type) {
		if (type == "bar" || type == "serial") {
			data.forEach((_d, i) => { _d.color = this.list[i % 2 == 0 ? 0 : 1] });
		} else if (type == "pie") {
			// data.forEach((_d, i) => { _d.color = this.list[i] });
		} else if (type == "serial") {
			data.forEach((_d, i) => { if (i % 2 == 0) { _d.color = _d.color_rating = this.list[0] } else { _d.color = _d.color_rating = this.list[1] } });
		}
	}
	private LoadCharts(is_first = false, page = 0) {
		let filters: any;
		let sessionData: any = this.headerService.getStaticHeaderData();
		filters = this.bodyService.GetFilters();
		filters.filter_type = (filters.duration == -1) ? "custom" : "default";
		filters.account_id = sessionData.user_current_account.accounts.account_id;
		filters.role_id = sessionData.user_current_account.roles.role_id;
		filters.startDate = new Date().getFullYear() - 1 + "-" + new Date().getMonth();
		filters.bool = 1;
		filters.user_id = sessionData.user_current_account.User.id;
		if (filters.standards)
			filters.search_by_standards = filters.standards;
		else filters.search_by_standards = "";
		if (!filters.subAccount) {
			filters.subAccount = "";
		}
		// if (filters.account_id == filters.subAccount) {
		// 	filters.account_id = "";
		// }
		if ((!filters.duration || filters.duration == -1) && !filters.start_date) {
			return;
		}
		filters.offset_serial = this.p - 1 || page;
		filters.limit_serial = 5;
		if (filters.filter_id == 0) filters.is_updated = 1;
		if (filters.filter_id == -1) filters.is_updated = 0;
		this.subscriptions.add(this.bodyService.GetCharts(filters).subscribe(data => this.RenderCharts(data)));
	}

	getChartData(data) {
		this.RenderCharts(data, false, true);
	}

	public export_excel() {
		let obj: any = {};
		let type_id = this.bodyService.GetFilter("folder_type");
		if (type_id == 2) {
			let coach_view = this.bodyService.GetFilter("coachee_view");
			type_id = coach_view == 2 ? 4 : 2;
		}
		this.loadingModalRef = this.modalService.show(this.loadingModal, { backdrop: 'static' });
		obj.start_date = this.bodyService.GetFilter("start_date");
		obj.end_date = this.bodyService.GetFilter("end_date");
		obj.standards = this.bodyService.GetFilter("standards");
		obj.framework_id = this.bodyService.GetFilter("framework_id");
		obj.subAccount = this.bodyService.GetFilter("subAccount");;
		obj.huddle_type = type_id;
		let sessionData: any = this.headerService.getStaticHeaderData();
		obj.user_id = sessionData.user_current_account.User.id;
		obj.account_id = sessionData.user_current_account.accounts.account_id;
		this.bodyService.Export(this.filterObject).subscribe((data: any) => {
			let file_name = data.file_name;
			let interval_id = setInterval(() => {
				obj.file_name = file_name;
				this.filterObject.file_name = file_name;
				this.bodyService.is_download_ready(this.filterObject).subscribe((data: any) => {
					let download_status = data.download_status;
					if (download_status) {
						clearInterval(interval_id);
						obj.download_url = data.download_url;
						obj.file_name = file_name;
						this.loadingModalRef.hide();
						this.bodyService.download_analytics_standards_to_excel(obj);
					}
				});
			}, 3000);
		});
	}

	checkPermission() {
		let account_id = this.header_data.user_current_account.accounts.account_id;
		let user_id = this.header_data.user_current_account.users_accounts.user_id;
		let start_date = moment().subtract(1, 'years').format('YYYY-MM-DD');
		let end_date = moment().format('YYYY-MM-DD');
		let folder_type = 2;
		if (this.header_data.user_permissions.UserAccount.permission_view_analytics == 1 || this.header_data.user_permissions.UserAccount.role_id == 115) {
			// if (this.header_data.user_permissions.UserAccount.role_id == 120) {
			// 	this.router.navigate([`/analytics_angular/playcard/${account_id}/${user_id}/${start_date}/${end_date}/${folder_type}`]);
			// 	return;
			// }
		}
		else if (this.header_data.user_permissions.UserAccount.role_id == 120) {
			this.router.navigate([`/analytics_angular/playcard/${account_id}/${user_id}/${start_date}/${end_date}/${folder_type}`]);
			return;
		}
		else {
			this.router.navigate(['/page-not-found']);
			return;
		}
	}
	ngOnDestroy() {
		this.subscriptions.unsubscribe();
	}
}
interface top_charts {
	top_available: boolean,
	least_available: boolean,
	pie_available: boolean
}