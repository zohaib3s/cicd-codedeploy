import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RootComponent, BodyComponent, PlaycardComponent, UserdetailsComponent, NewUserSummaryComponent, NavtabsComponent, OverviewComponent, TagsnplsComponent, UserSummaryComponent, CustomMarkersComponent, PlayercardCustommarkerComponent } from './components';
import { SharedPageNotFoundComponent } from '../shared/shared-page-not-found/shared-page-not-found.component';
import { OverviewMainComponent } from './components/main/overview-main/overview-main.component';
import { PlayerCardComponent } from './components/player-card/player-card.component';
import { OverviewPlayercardComponent } from './components/player-card/overview-playercard/overview-playercard.component';
import { PlayercardUsersummaryComponent } from './components/player-card/playercard-usersummary/playercard-usersummary.component';
import { PlayercardTagsComponent } from './components/player-card/playercard-tags/playercard-tags.component';
import { MainTagsComponent } from './components/main-tags/main-tags.component';
import { PlayerCardPermissionGuard } from './guards/playercard-permission.guards';
const routes: Routes = [
  { path: '', redirectTo: 'overview', pathMatch: 'full' },
  {
    path: '', component: RootComponent,
    children: [
      { path: 'user-summary', component: NewUserSummaryComponent, data: { type: 'user-summary-main', breadcrumb:'analytics_user_summary' } },
      { path: 'home', component: BodyComponent },
      { path: 'overview', component: OverviewMainComponent, data: { type: 'overview', breadcrumb:'analytics_overview' } , canActivate: [PlayerCardPermissionGuard] },
      { path: 'tags', component: MainTagsComponent, data: { type: 'tags', breadcrumb:'analytics_tp_level' } , canActivate: [PlayerCardPermissionGuard] },
      { path: 'custom-marker', component: CustomMarkersComponent, data: { type: 'custom-marker', breadcrumb:'custom_marker' } , canActivate: [PlayerCardPermissionGuard] },
      { path: 'user-summary1', component: UserSummaryComponent, data: { type: 'analytics_user_summary'} , canActivate: [PlayerCardPermissionGuard] },
      { path: 'home/:tab_id', component: BodyComponent },
      // { path: 'playcard/:account_id/:user_id/:start_date/:end_date/:folder_type', component: PlaycardComponent },s
      { 
         path: 'playercard/:account_id/:user_id/:start_date/:end_date', component: PlayerCardComponent, 
         children: [
          { path: '', redirectTo: 'overview', pathMatch: 'full' },
          { path: 'tags', component: PlayercardTagsComponent, data: { type: 'playercard-tags', breadcrumb:'analytics_tp_level' } },
          { path: 'playercard-custommarker', component: PlayercardCustommarkerComponent, data: { type: 'playercard-custommarker', breadcrumb:'custom_marker_tags_all_global' } },
          { path: 'overview', component: OverviewPlayercardComponent, data: { type: 'playercard-overview', breadcrumb:'analytics_overview' } },
          { path: 'user-summary', component: PlayercardUsersummaryComponent, data: { type: 'playercard-user-summary', breadcrumb:'player_card_user_summary' } },
         ],
          data: {  breadcrumb:'analytics_user_profile' } 
      },
      { path: 'playcard-overview', component: OverviewPlayercardComponent },
      { path: 'details/:user_id/:account_id/:start_date/:end_date/:type/:workspace_huddle_library', component: UserdetailsComponent }
    ]
  },
  { path: 'page_not_found', component: SharedPageNotFoundComponent },

  {
    path: '**',
    redirectTo: 'page_not_found',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnalyticsRoutes { }