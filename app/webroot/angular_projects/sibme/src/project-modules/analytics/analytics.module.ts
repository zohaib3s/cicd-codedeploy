import { NgModule } from '@angular/core';

import { __IMPORTS, __DECLARATIONS, __PROVIDERS } from './components.barrel';
import { AnalyticsRoutes } from './analytics.routes';
import { GoalsModule } from '../goals/goals.module';

@NgModule({
  declarations: [__DECLARATIONS, ],
  imports: [__IMPORTS, AnalyticsRoutes,GoalsModule],
  providers: [__PROVIDERS],
})
export class AnalyticsModule { }