import { CommonModule } from '@angular/common';
import { MomentModule } from 'ngx-moment';
import { ToastrModule } from 'ngx-toastr';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { NgxPaginationModule } from 'ngx-pagination';
import { AmChartsModule } from '@amcharts/amcharts3-angular';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { NgScrollbarReachedModule } from 'ngx-scrollbar/reached-event';
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ModalModule } from "ngx-bootstrap/modal";
import { TabsModule } from "ngx-bootstrap/tabs";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { MainService } from "../trackers/services/main.service";

import {
  RootComponent,
  BodyComponent,
  BreadcrumbsComponent,
  OverviewComponent,
  TagsnplsComponent,
  UserSummaryComponent,
  NavtabsComponent,
  PlaycardComponent,
  PcbreadcrumbsComponent,
  PcBodyComponent,
  PctabsComponent,
  OverviewboxComponent,
  CurrentuserComponent,
  UsertagsComponent,
  UserdetailsComponent,
  UdbreadcrumbsComponent,
  UdBodyComponent,
  NewUserSummaryComponent,
  PlayercardTagsComponent,
  PlayercardUsersummaryComponent,
  OverviewPlayercardComponent,
  PlayercardGoalsComponent,
  PlayercardHuddlesComponent,
  PlayerCardComponent,
  CustomMarkersComponent,
  PlayercardCustommarkerComponent
} from "./components";


import { OverviewService, BodyService, TabsService, SummaryService, PlaycardService, TagsService, DetailsService } from './services';
import { AppLevelSharedModule } from '../shared/shared.module';
import { SLIMSCROLL_DEFAULTS } from 'ngx-slimscroll';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { LeaderBoardComponent } from './components/overview/leader-board/leader-board.component';
import { ChartModule, HIGHCHARTS_MODULES  } from 'angular-highcharts';
import * as more from 'highcharts/highcharts-more.src';
import * as exporting from 'highcharts/modules/exporting.src';
import { OverviewMainComponent } from './components/main/overview-main/overview-main.component';
import { MainAnalyticsService } from './services/main-analytics.service';
import { EditGoalService, GoalsService } from '../goals/services';
import { MainTagsComponent } from './components/main-tags/main-tags.component';
import { PlayerCardPermissionGuard } from './guards/playercard-permission.guards';



export const __IMPORTS = [
  CommonModule,

  TooltipModule.forRoot(),
  BsDropdownModule.forRoot(),
  ButtonsModule.forRoot(),
  AmChartsModule,
  TableModule,
  PaginatorModule,
  NgMultiSelectDropDownModule.forRoot(),
  BsDatepickerModule.forRoot(),
  ModalModule.forRoot(),
  TabsModule.forRoot(),
  ToastrModule.forRoot(),
  LoadingBarHttpClientModule,
  MomentModule,
  NgxPaginationModule,
  AppLevelSharedModule,
  NgScrollbarModule,
  NgScrollbarReachedModule,
  ChartModule
];

export const __DECLARATIONS = [
  RootComponent,
  BodyComponent,
  BreadcrumbsComponent,
  OverviewComponent,
  TagsnplsComponent,
  UserSummaryComponent,
  NavtabsComponent,
  PlaycardComponent,
  PcbreadcrumbsComponent,
  PcBodyComponent,
  PctabsComponent,
  OverviewboxComponent,
  CurrentuserComponent,
  UsertagsComponent,
  UserdetailsComponent,
  UdbreadcrumbsComponent,
  UdBodyComponent,
  NewUserSummaryComponent,
  LeaderBoardComponent,
  OverviewMainComponent,
  PlayerCardComponent,
  OverviewPlayercardComponent,
  PlayercardUsersummaryComponent,
  PlayercardTagsComponent,
  PlayercardGoalsComponent,
  PlayercardHuddlesComponent,
  MainTagsComponent,
  CustomMarkersComponent,
  PlayercardCustommarkerComponent
  // DeleteComponent
  // NgScrollbarModule
];

export const __PROVIDERS = [
  OverviewService, 
  BodyService, 
  TabsService, 
  SummaryService, 
  PlaycardService, 
  TagsService, 
  DetailsService,
  MainService,
  EditGoalService,
  MainAnalyticsService,
  GoalsService,
  PlayerCardPermissionGuard,
  { provide: SLIMSCROLL_DEFAULTS, useValue: { alwaysVisible: true } },
   { provide: HIGHCHARTS_MODULES, useFactory: () => [ more, exporting ] } // add as factory to your providers
];