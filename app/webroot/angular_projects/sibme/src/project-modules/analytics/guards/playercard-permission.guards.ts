import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HeaderService } from '@app/services';
import * as moment from "moment";
@Injectable()
export class PlayerCardPermissionGuard implements CanActivate {
    constructor(private headerService: HeaderService, private router: Router) { }
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot

    ): Observable<boolean> | Promise<boolean> | boolean {
        const headerData = this.headerService.getStaticHeaderData();
        if (Object.keys(headerData).length != 0 && headerData.constructor === Object) { // if header data exists aka in app navigation
            return this.verifyPermission(headerData);
        } else { // else wait for header data and perform action
            return this.headerService.headerData$.pipe(map((headerData: any) => {
                return this.verifyPermission(headerData)
            }));
        }
    }

    private verifyPermission(headerData: any) {
        if (headerData) {
            if (headerData.user_permissions && headerData.analytics_permissions && headerData.user_current_account.roles.role_id !== '120') {
                return true;
            } else if(headerData.user_permissions && headerData.analytics_permissions && headerData.user_current_account.roles.role_id == '120'){
                let start_date = moment().subtract(1, 'years').format('YYYY-MM-DD');
                let end_date = moment().format('YYYY-MM-DD');
                 let account_id = headerData.user_current_account.accounts.account_id;
                let user_id = headerData.user_current_account.users_accounts.user_id;
                this.router.navigate([`/analytics_angular/playercard/${account_id}/${user_id}/${start_date}/${end_date}`]);
                return false; 
            } else if (!headerData.user_permissions || !headerData.analytics_permissions){
                this.router.navigate(['/page-not-found']);
                return false;
            }
          
        }
    }
}