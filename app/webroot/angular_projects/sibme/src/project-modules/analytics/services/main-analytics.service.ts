import { EventEmitter, Injectable, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "@environments/environment";

@Injectable()
export class MainAnalyticsService {
    @Output() pageChanged: EventEmitter<any> = new EventEmitter();
    @Output() limitChanged: EventEmitter<any> = new EventEmitter();
    @Output() loadingChanged: EventEmitter<any> = new EventEmitter();
    @Output() routeParams: EventEmitter<any> = new EventEmitter();
    @Output() playerCardLoading: EventEmitter<any> = new EventEmitter();
    @Output() breadCrumUserName: EventEmitter<any> = new EventEmitter();
    @Output() sortData: EventEmitter<any> = new EventEmitter();
    constructor(private http: HttpClient) { }


}