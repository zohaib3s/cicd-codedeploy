import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from "@environments/environment";

@Injectable()
export class OverviewService {

	constructor(private http: HttpClient) { }

	public getNewOverviewData(conf){

		// let overview_url = (environment.APIbaseUrlDirect || environment.APIbaseUrl) + "/analytics_new";

		// analytics.sibme.com is not needed because we are now using summary tables.
		let overview_url = (environment.APIbaseUrl) + "/analytics_new";
		return this.http.post(overview_url, conf);
	}

}
