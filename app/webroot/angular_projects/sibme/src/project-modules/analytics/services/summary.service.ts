import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { HeaderService } from "@projectModules/app/services";
import { environment } from "@environments/environment";
import { BodyService } from "./body.service";
import { filter } from 'minimatch';

@Injectable()
export class SummaryService {

	@Output() Summary: EventEmitter<any> = new EventEmitter();
	public analyticsData: EventEmitter<any> = new EventEmitter<any>();
	constructor(private headerService: HeaderService, private http: HttpClient, private bodyService: BodyService) { }

	public AnalyticsData(state) {
		this.analyticsData.emit(state);
	}
  

	public GetFile(filters){

		let path = environment.APIbaseUrl + "/export_user_summery";
		return this.http.post(path, filters, { responseType: 'text' } );

	}

  public EmitSummary(summary:any){

	  this.Summary.emit(summary);

  }

}
