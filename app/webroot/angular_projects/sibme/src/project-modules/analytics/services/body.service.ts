import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeaderService } from "@projectModules/app/services";
import { environment } from "@environments/environment";

@Injectable()
export class BodyService {

	private filtervals: filters;
	private PLs: Array<string>;

	@Output() triggerOverviewDataChange: EventEmitter<any> = new EventEmitter();
	@Output() triggerTagsDataChange: EventEmitter<any> = new EventEmitter();
	@Output() FilterSubmitPressed: EventEmitter<any> = new EventEmitter();
	@Output() CommandTabChange: EventEmitter<any> = new EventEmitter();
	constructor(private headerService: HeaderService, private http: HttpClient) {
		this.filtervals = {};
	}


	public updateTab(val) {

		this.CommandTabChange.emit(val);

	}

	public BroadcastOverviewValues(values) {

		this.triggerOverviewDataChange.emit(values);

	}

	public BroadcastTagsValues(values) {

		this.triggerTagsDataChange.emit(values);

	}

	public UpdateFilters(key, val) {

		this.filtervals[key] = val;

	}

	public SetFilters(filters) {

		this.filtervals = filters;

	}

	public GetFilter(key) {

		return this.filtervals[key];

	}

	public GetFilters() {

		return this.filtervals;

	}

	public deleteFilter(key) {

		delete this.filtervals[key];

	}

	public deleteAllFilters() {
		this.filtervals = {};
	}

	public getChildAccounts() {

		let path = environment.APIbaseUrl + "/get_child_accounts";
		let sessionData: any;
		sessionData = this.headerService.getStaticHeaderData();
		let account_id = sessionData.user_current_account.users_accounts.account_id;

		return this.http.post(path, { account_id: account_id });

	}

	public GetSubFilters() {

		let path = environment.APIbaseUrl + "/get_child_accounts";
		let sessionData: any;
		sessionData = this.headerService.getStaticHeaderData();
		let account_id = sessionData.user_current_account.users_accounts.account_id;

		return this.http.post(path, {});
	}

	public GetCharts(filters) {

		let path = environment.APIbaseUrl + "/frequency_of_tagged_standard";

		return this.http.post(path, filters);

	}

	public LoadFilterDropdowns(acc = -1, user_id?) {

		let sessionData: any;
		sessionData = this.headerService.getStaticHeaderData();
		let account_id;
		if (acc == -1)
			account_id = sessionData.user_current_account.accounts.account_id;
		else account_id = acc;
		let obj: any = {};
		if (user_id) {
			obj.user_id = user_id;
		}
		let path = environment.APIbaseUrl + "/filters_dropdown/" + account_id;

		return this.http.post(path, obj);


	}

	public LoadStandards(framework_id) {

		let sessionData: any;
		sessionData = this.headerService.getStaticHeaderData();
		let account_id = sessionData.user_current_account.accounts.account_id;

		let path = environment.APIbaseUrl + "/getFramework/" + account_id + "/" + framework_id;

		return this.http.post(path, {});

	}

	public StorePLData(data: Array<string>) {

		this.PLs = data;

	}

	public GetPLData() {

		return this.PLs;

	}

	public UpdatePressed() {

		this.FilterSubmitPressed.emit(true);

	}
	/*
	  public Export(obj){
	
			let path = environment.APIbaseUrl+"/export_analytics_standards_to_excel";
	  	
			var form = document.createElement("form");
	    
		form.setAttribute("target", "_blank");
	    
		for(let k in obj){
	
			var input = document.createElement("input");
	    
			input.setAttribute("value", obj[k]);
		    
			input.setAttribute("name", k);
		    
			form.appendChild(input);
	
		}
	
		form.setAttribute("action", path);
	    
		form.setAttribute("method","post");
	    
		document.body.appendChild(form);
	
		form.submit();
	
		document.body.removeChild(form);
	
	  }
	*/
	public getAssesmentArray(obj) {
		let path = environment.APIbaseUrl + "/performance_level_assessment_array";
		return this.http.post(path, obj);
	}
	
	public Export(obj) {
		let sessionData: any;
		sessionData = this.headerService.getStaticHeaderData();
		let account_id = sessionData.user_current_account.accounts.account_id;

		let path = environment.APIbaseUrl + "/export_analytics_standards_to_excel";

		return this.http.post(path, obj, { headers: { ignoreLoadingBar: '' } });
	}

	public is_download_ready(obj,huddle=false) {
		let sessionData: any;
		sessionData = this.headerService.getStaticHeaderData();
		let account_id = sessionData.user_current_account.accounts.account_id;

		let path = environment.APIbaseUrl + "/is_export_analytics_download_ready";
		if(huddle){
			return this.http.post(path, obj);
		}
		return this.http.post(path, obj, { headers: { ignoreLoadingBar: '' } });
	}

	public download_analytics_standards_to_excel(obj) {

		var link = document.createElement("a");
		link.href = obj.download_url;
		link.download = obj.file_name;
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);

	}

	getAllFilters(){
		let path = environment.APIbaseUrl + "/analytics_filters/get_all";
		let sessionData = this.headerService.getStaticHeaderData();
		let user_account = sessionData.user_current_account;
		let obj ={user_current_account:user_account}
		return this.http.post(path, obj);

	}

	saveNewFilter(obj){
		let path = environment.APIbaseUrl + "/analytics_filters/save";
		return this.http.post(path, obj);
	}

	

	deleteSavedFilter(item){

		let path = environment.APIbaseUrl + "/analytics_filters/delete";
		let sessionData = this.headerService.getStaticHeaderData();
		let user_current_account = sessionData.user_current_account;

		let obj={id:item.id,user_current_account:user_current_account}
		return this.http.post(path, obj);


	}

}

interface filters {

	[key: string]: any

}