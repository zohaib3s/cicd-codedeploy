import { Directive, HostListener, EventEmitter, Output, ElementRef, Renderer2, Input, OnDestroy } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { HeaderService } from '@app/services';

type HoleType = 'record' | 'resume';
@Directive({
    selector: '[holdable]',
    host: { '[style.cursor]': '"pointer"' }
})
export class HoldableDirective implements OnDestroy {

    @Input() private type: HoleType;
    @Output() hold: EventEmitter<string> = new EventEmitter();


    @HostListener('mouseup', ['$event'])
    @HostListener('mousedown', ['$event'])
    @HostListener('mouseleave', ['$event'])
    onExit(event: MouseEvent) {
        // listen only for left clicks and mouseleave
        if (event.which === 1 || event.which === 0) this.eventChange$.next(event.type);
    }

    private holdStarted = false;
    private eventChange$ = new Subject<string>();
    private tooltipWrapper: ElementRef;
    private isHoldAvailable: boolean = false;

    private translations: any;
    private subscription: Subscription;

    constructor(private headerService: HeaderService, private el: ElementRef, private renderer2: Renderer2) {

        this.subscription = this.headerService.languageTranslation$.subscribe(translations => this.translations = translations);

        this.eventChange$.pipe(debounceTime(200)).subscribe((eventType: string) => {
            if (!this.holdStarted && eventType === 'mousedown') {
                this.expandParentElementSize(this.type);
                this.holdStarted = true;
                this.hold.emit('started');
            } else if (this.holdStarted && (eventType === 'mouseup' || this.holdStarted && eventType === 'mouseleave')) {
                this.collapseParentElementSize(this.type);
                this.holdStarted = false;
                this.hold.emit('stopped');

                let tooltipText: string;
                if(this.type === 'record') tooltipText = this.translations.hold_to_record_audio;
                else tooltipText = this.translations.hold_to_resume_audio;
            }
        });
    }


    private expandParentElementSize(type: HoleType) {
        if (type === 'record') {
            this.renderer2.setStyle(this.el.nativeElement, 'width', '60px');
            this.renderer2.setStyle(this.el.nativeElement, 'height', '60px');
        } else if (type === 'resume') {
            this.renderer2.setStyle(this.el.nativeElement, 'width', '50px');
            this.renderer2.setStyle(this.el.nativeElement, 'height', '50px');
        }
    }

    private collapseParentElementSize(type: HoleType) {
        if (type === 'record') {
            this.renderer2.setStyle(this.el.nativeElement, 'width', '50px');
            this.renderer2.setStyle(this.el.nativeElement, 'height', '50px');
        } else if (type === 'resume') {
            this.renderer2.setStyle(this.el.nativeElement, 'width', '40px');
            this.renderer2.setStyle(this.el.nativeElement, 'height', '40px');
        }
    }


    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}