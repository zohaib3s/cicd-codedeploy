import { CommonModule } from '@angular/common';
import { TooltipModule} from 'ngx-bootstrap/tooltip';

import { AudioRecorderPlayComponent, AudioRecorderPlayPOCComponent } from "./components";
import { AudioRecorderPlayPdfComponent } from './components/audio-recorder-play-pdf/audio-recorder-play-pdf.component';
import { HoldableDirective } from "./directives";
import { DisplayTime } from "./pipes";
import { FilestackService } from './services';

export const __IMPORTS = [CommonModule, TooltipModule];

export const __DECLARATIONS = [
  AudioRecorderPlayComponent,
  AudioRecorderPlayPOCComponent,
  AudioRecorderPlayPdfComponent,
  HoldableDirective,
  DisplayTime
];

export const __EXPORTS = [AudioRecorderPlayComponent, AudioRecorderPlayPOCComponent, AudioRecorderPlayPdfComponent];

export const __PROVIDERS = [FilestackService]