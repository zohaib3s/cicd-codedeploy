import { Component, ElementRef, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HeaderService, SocketService } from '@app/services';
import { HttpClient } from '@angular/common/http';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import { UserSettingsService } from '../services/user-settings.service';
import { log } from 'console';


@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit, OnDestroy {

  public socket_listener: Subscription = new Subscription();
  public userSettings: any = {};
  public email_unsubscribers: any = {};
  public header_data;
  public translation;
  public account_id;
  public user_id;
  public userRoleId;
  public user_current_account;
  public userObject: any;
  public userAccount;
  public password = '';
  public confirm_password = '';
  public username = '';
  public email = '';
  public url = '';
  public body = new FormData();
  public skelton_loading: boolean = true;
  public translations: any = {};
  private subscription: Subscription;
  private popup = false;
  public formSubmitted: boolean = false;
  public userDescription: any;
  @ViewChild('photo', { static: false }) photo: ElementRef;
  @ViewChild('updateUser', { static: false }) updateUser;
  public isChecked: boolean;
  public firstName = '';
  public lastName = '';
  public currentAccount: any = {};
  public Customfield_array: any;
  public Editable: boolean = false;
  public tab_toggle = true;
  public breadcrumb : boolean= false;


  constructor(public userSettingsService: UserSettingsService,
    private headerService: HeaderService, private http: HttpClient,
    private socketService: SocketService, private toastr: ShowToasterService) {
    this.header_data = this.headerService.getStaticHeaderData();
    if(Object.keys(this.header_data).length != 0 && this.header_data.constructor === Object){
      this.userRoleId = this.header_data.user_permissions.roles.role_id;
      this.userObject = {
        account_id: this.header_data.user_current_account.users_accounts.account_id,
        user_id: this.header_data.user_current_account.users_accounts.user_id,
        user_current_account: this.header_data.user_current_account,
        user_accounts: this.header_data.user_accounts
      };

      this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
        this.translations = languageTranslation;
      });
    }
  }

  ngOnInit() {

    // this.Customfield=[
    //   {id:'ww', title:"Grade",data:"A 90%", type:"Text"},
    //   {id:'ww', title:"Percentage",data:"90.0", type:"Percentage"},
    //   {id:'ww', title:"Numbers",data:"25", type:"Number"},
    //   {id:'ww', title:"Yes/No", data:"false",type:"Yes/No"},
    // ];
    // console.log(this.Customfield);
    if(Object.keys(this.header_data).length != 0 && this.header_data.constructor === Object) {
      this.getUserSettings();
    }
   
    this.socketGetUserSummary();
    setTimeout(() => {
      this.breadcrumb=true;
    }, 1000);
  }

  onChange(event, obj) {
    if (event.target.value < 0) {
      event.target.value = 0;
    }
    else if (event.target.value > 100) {
      event.target.value = 100;
    }
    else if (event.target.value.toString().length > 8) {
      event.target.value = Math.round(event.target.value).toFixed(0);
    }
    obj.custom_field_value = event.target.value;

  }

  private socketGetUserSummary() {
    this.socket_listener.add(this.socketService.pushEventWithNewLogic(`user-settings-${this.header_data.user_current_account.users_accounts?.account_id}-${this.header_data.user_current_account.users_accounts.user_id}`).subscribe(data => this.processEventSubscriptions(data)));
  }

  private processEventSubscriptions(res) {

    switch (res.event) {
      case "user_custom_field_updated":
        this.Customfield_array = res.data.user_custom_fields;
        break;

      default:
      break;
    }
  }

  onpaste(event: ClipboardEvent, paste) {

    let clipboardData = event.clipboardData
    let pastedText = clipboardData.getData('text');

    const pattern = /^[0-9\+\-\.]{1,9}$/;
    if (paste === "Number") {
      if (pastedText.toString().length > 10) {
        event.preventDefault();
      }
    }
    if (paste === "Percentage") {
      if (Number(pastedText) > 100 || Number(pastedText) < 0) {
        event.preventDefault();
      }
    }
    if (!pattern.test(pastedText)) {
      event.preventDefault();
    }
  }

  getUserSettings() {
    console.log(this.userObject);
    
    this.userSettingsService.GetUserSettings(this.userObject).subscribe((data: any) => {

      this.userSettings = data;
      this.username = this.userSettings.user.username;
      this.userDescription = this.userSettings.user.user_description;
      this.email = this.userSettings.user.email.replace(/\s/g, "");
      let email_unsubscribers = this.userSettings.email_unsubscribers.filter((item) => item.account_name == this.header_data.user_permissions.accounts.company_name);
      this.email_unsubscribers = email_unsubscribers[0];
      this.userAccount = this.header_data.user_permissions.accounts.company_name;
      this.getImage();
      this.headerService.userImageSubject.next(this.url);
      this.headerService.userFirstNameSubject.next(this.userSettings.user.first_name);
      this.headerService.userLastNameSubject.next(this.userSettings.user.last_name);
      this.headerService.userEmailSubject.next(this.userSettings.user.email);
      this.skelton_loading = false;
      this.firstName = this.userSettings.user.first_name;
      this.lastName = this.userSettings.user.last_name;
      this.currentAccount = this.header_data.user_current_account.accounts.company_name;
      this.Editable = this.userSettings.user.can_user_edit_custom_fields;
      this.Customfield_array = this.userSettings.user.user_custom_fields;
      this.updateButtonOnLoad(this.currentAccount);
    }
    );
  }

  updateButton(accountName: any) {
    this.userSettings.email_unsubscribers.forEach((item) => {
      if (item.account_name === accountName) {
        item.unsubscribers.forEach((item2) => {
          this.isChecked = item2.status == 'checked';
        });
      }
    });
  }

  updateButtonOnLoad(accountName: any) {
    this.userSettings.email_unsubscribers.forEach((item) => {
      if (item.account_name === accountName) {
        item.unsubscribers.forEach((item2) => {
          this.isChecked = item2.status == 'checked';
        });
      }
    });
  }

  showSubscribers(account: any) {
    let email_unsubscribers = this.userSettings.email_unsubscribers.filter((item) => item.account_name == account.target.value);
    this.email_unsubscribers = email_unsubscribers[0];


  }

  getImage() {
    if (this.userSettings.user.image != null) {
      this.url = 'https://s3.amazonaws.com/sibme.com/static/users/' + this.userObject.user_id + '/' + this.userSettings.user.image;
    }
  }

  updateSubscribers(e, account_name, id) {
    this.userSettings.email_unsubscribers.forEach((item) => {
      if (item.account_name === account_name) {
        item.unsubscribers.forEach((item2) => {
          if (item2.id === id) {
            if (e.target.checked) {
              item2.status = 'checked';
              this.isChecked = true;
            } else {
              item2.status = 'unchecked';
              this.isChecked = false;
            }
          }
        });
      }
    });
  }

  subscribeAll(account_name: any) {
    const checks = document.getElementsByName('checks');
    this.userSettings.email_unsubscribers.forEach((item) => {
      if (item.account_name === account_name) {
        item.unsubscribers.forEach((item2) => {
          if (checks) {
            item2.status = 'checked';
            this.isChecked = true;
          } else {
            item2.status = 'unchecked';
            this.isChecked = false;
          }
        });
      }
    });
  }

  unsubscribeAll(account_name: any) {
    const checks = document.getElementsByName('checks');
    this.userSettings.email_unsubscribers.forEach((item) => {
      if (item.account_name === account_name) {
        item.unsubscribers.forEach((item2) => {
          if (checks) {
            item2.status = 'unchecked';
            this.isChecked = false;
          } else {
            item2.status = 'checked';
            this.isChecked = true;
          }
        });
      }
    });
  }

  update() {
    this.formSubmitted = true;

    if (!this.updateUser.valid) {
      return;
    }

    // Trim if custom fields have space
    for (let i = 0; i < this.Customfield_array.length; i++) {
      if(this.Customfield_array[i].custom_field_value) this.Customfield_array[i].custom_field_value = this.Customfield_array[i].custom_field_value.trim();
    }

    let updatedUser = {
      default_account: this.userSettings.default_account,
      user_id: this.userObject.user_id,
      first_name: this.userSettings.user.first_name.trim(),
      last_name: this.userSettings.user.last_name.trim(),
      username: this.userSettings.user.username.trim(),
      title: this.userSettings.user.title ? this.userSettings.user.title.trim() : this.userSettings.user.title,
      email: this.userSettings.user.email.trim(),
      lang: this.userSettings.user.lang,
      institution_id: this.userSettings.user.institution_id ? this.userSettings.user.institution_id.trim() : this.userSettings.user.institution_id,
      password: this.password,
      confirm_password: this.confirm_password,
      email_subscribers: this.userSettings.email_unsubscribers,
      user_description: this.userDescription,
      user_custom_fields: this.Customfield_array,
      user_current_account: this.userObject.user_current_account
    };
    console.log(updatedUser);
    if (updatedUser.first_name == '') {
      this.toastr.ShowToastr('info',this.translations.first_name_cant_empty);
    }
    else if (updatedUser.last_name == '') {
      this.toastr.ShowToastr('info',this.translations.last_name_cant_empty);

    }
    else if (updatedUser.username == '') {
      this.toastr.ShowToastr('info',this.translations.user_name_cant_empty);
    }
    else {
      this.userSettingsService.updateUser(updatedUser).subscribe((data: any) => {
        this.getUserSettings();
        this.headerService.getStaticHeaderData();
        if (data.success == false) {
          if (updatedUser.confirm_password == '' && updatedUser.password != '') {
            this.toastr.ShowToastr('info',this.translations.confirm_password_cant_empty);
          }
          else if (updatedUser.password == '' && updatedUser.confirm_password != '') {
            this.toastr.ShowToastr('info',this.translations.new_password_cant_empty);
          }

          else {
            this.toastr.ShowToastr('info',data.message);
          }
        } else {

          this.userSettingsService.updateHeaderSession().subscribe((data) => {
            this.password = '';
            this.confirm_password = '';
          });
          this.toastr.ShowToastr('success',this.translations.user_profile_updated_us);
        }

      }, error1 => {
      });
    }
  }

  onFileChanged(files: FileList) {
    let fileToUpload: File = files.item(0);
    this.body.append('image', fileToUpload, fileToUpload.name);
    this.body.append('user_id', this.userObject.user_id);
  }

  onUpload() {
    this.userSettingsService.uploadImage(this.body).subscribe((data: any) => {
      this.toastr.ShowToastr('success',this.translations.image_upload_successfully);
      this.getUserSettings();
      this.reset();
      this.popup = false;
      this.updateHeaderImage(data.image);
    }, error1 => {
    });
  }

  updateHeaderImage(image) {
    this.userSettingsService.updateHeaderData(image).subscribe((data) => {
    }, error1 => {
    });
  }

  showPopup() {
    this.popup = !this.popup;
  }

  reset() {
    this.photo.nativeElement.value = '';
    this.showPopup();
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
    this.socket_listener.unsubscribe();
  }
}
