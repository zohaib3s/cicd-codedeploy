import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { UserSettingsRoutingModule } from './user-settings-routing.module';
import { UserSettingsComponent } from './components/user-settings.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { FormsModule } from '@angular/forms';
import { AppLevelSharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    UserSettingsComponent,
  ],
  imports: [
    CommonModule,
    UserSettingsRoutingModule,
    LoadingBarHttpClientModule,
    BsDropdownModule.forRoot(),
    FormsModule,
    AppLevelSharedModule
],
})
export class UserSettingsModule {
}
