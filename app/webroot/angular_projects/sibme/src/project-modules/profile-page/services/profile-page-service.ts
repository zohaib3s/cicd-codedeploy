import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ProfilePageService {

  constructor(private http: HttpClient) {

  }

  public getBannerText(obj){
    let path = environment.APIbaseUrl + '/get_banners';
    return this.http.post(path , obj);
  }
  public hidebanner(obj){
    let path = environment.APIbaseUrl + '/hid_banner';
    return this.http.post(path , obj);
  }
}
