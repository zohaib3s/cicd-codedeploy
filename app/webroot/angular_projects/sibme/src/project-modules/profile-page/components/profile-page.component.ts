import { Component, OnInit, OnDestroy, ViewChild, ɵConsole, EventEmitter, TemplateRef } from '@angular/core';
import { ProfilePageService } from '../services/profile-page-service';
import { HeaderService, SocketService, AppMainService } from '@src/project-modules/app/services';
import { Subscription, Subject, Observable } from 'rxjs';
import { environment } from '@src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import * as moment from 'moment';
import { ShowToasterService } from '@projectModules/app/services';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';




@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit, OnDestroy {
  public header_data;
  public userData: any;
  public translation;
  private subscription: Subscription;
  public translations: any = {};
  public url = '';
  options: any;
  recent_activities = [] as any;
  analytics_data = {} as any;
  public isLoading = true;
  unsubscribe$: Subject<void> = new Subject();
  public checkAnalyticsPermission: any;
  public base_url: any;
  public anaylyticsObj = {} as any;
  public userObject: any;
  config: any;
  public socket_listener: any;
  user_id: any = '0';
  account_id: any = '0';
  imageStaticUrl: any;
  site_id: any;
  user_role: any;
  permission_access_my_workspace: any;
  library_permission: any;
  permission_administrator_user_new_role: any;
  enable_lti_permission: any;
  enable_coaching_tracker: any;
  enable_assessment_tracker: any;
  enable_tracking: any;
  enable_edtpa: any;
  assessment_huddle_active: any;
  onGettingStartedModal: boolean = true;
  dont_show_box: boolean = false;
  getting_started_box: any = '0';
  enable_goals: any;
  banner_data = {} as any;
  banner: any;
  banner_heading;
  banner_desc;

  page: number = 1;
  totalItems;
  activityLoading = false;

  baseUrl: any;
  private styles = [
    { id: 'profilePageApp', path: 'assets/profile-page/css/app.css' },
  ];

  public showPassword = 'password';
  public showConfirmPassword = 'password';
  public showPasswordFlag = false;
  public showConfirmPasswordFlag = false;
  public checkHuddleTracker = false;
  show_question: boolean = true;

  public model = {
    password: '',
    confirm_password: ''
  }
  public a: ISlimScrollOptions;
  public scrollEvents: EventEmitter<SlimScrollEvent>;
  public currentDate: any;
  public isVisible: boolean = false;
  public lang: any;
  public changedAccountOwner;
  public user_hTracker_permission = 0;
  public windowWidth: any;
  public activitySkelton = true;
  public analytics_ready: boolean = true;
  public huddleParticipations: number;

  @ViewChild('createNewLearningCenterUser', { static: false }) childModal: ModalDirective;
  @ViewChild('gettingStartedModal', { static: true }) gettingStartedModal: ModalDirective;
  @ViewChild('changeAccount', { static: false }) changeAccount: ModalDirective;
  @ViewChild('box', { static: false }) box;

  constructor(public profilePageService: ProfilePageService, private headerService: HeaderService, private http: HttpClient, private socketService: SocketService, private toastr: ShowToasterService, private appMainService: AppMainService) {

    this.header_data = this.headerService.getStaticHeaderData();
  }

  ngOnInit() {

    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
    });
    if (Object.keys(this.header_data).length != 0 && this.header_data.constructor === Object) {
      if (this.header_data) {
        if (this.header_data?.analytics_ready != undefined) this.analytics_ready = this.header_data.analytics_ready;
        this.windowWidth = window.innerWidth;

        this.currentDate = moment().format('MMMM D, YYYY');

        this.banner = this.header_data?.hide_banner;
        // console.log(this.banner);

        this.isVisible = this.checkBanner;

        this.lang = this.header_data?.user_current_account.User.lang;



        // console.log(this.translations.dashboard_more_analytics);
        // console.log(window.innerWidth);
        // if (window.innerWidth <= 1024){
        //   this.translations.dashboard_more_analytics = this.translations.dashboard_view_more_analytics
        // }


        this.scrollEvents = new EventEmitter<SlimScrollEvent>();
        this.a = {
          position: 'right',
          barBackground: '#C9C9C9',
          barOpacity: '0.8',
          barWidth: '10',
          barBorderRadius: '20',
          barMargin: '0',
          gridBackground: '#D9D9D9',
          gridOpacity: '1',
          gridWidth: '0',
          gridBorderRadius: '20',
          gridMargin: '0',
          alwaysVisible: true,
          visibleTimeout: 1000,
        }
        this.styles.forEach(style => {
          this.loadCss(style);
        });



        this.checkAnalyticsPermission = this.header_data.analytics_permissions;
        this.baseUrl = this.header_data.base_url;

        this.userObject = {
          account_id: this.header_data.user_current_account.users_accounts.account_id,
          user_id: this.header_data.user_current_account.users_accounts.user_id,
          user_current_account: this.header_data.user_current_account,
          user_accounts: this.header_data.user_accounts
        };
        this.imageStaticUrl = this.baseUrl + "/img/home/photo-default.png";

        this.account_id = this.header_data.user_current_account.accounts.account_id;
        this.user_id = this.header_data.user_current_account.User.id;
        this.site_id = this.header_data.site_id;
        this.user_role = this.header_data.user_current_account.roles.role_id;
        this.permission_access_my_workspace = this.header_data.user_permissions.UserAccount.parmission_access_my_workspace;
        this.library_permission = this.header_data.library_permission;
        this.permission_administrator_user_new_role = this.header_data.user_permissions.UserAccount.permission_administrator_user_new_role;
        this.enable_lti_permission = this.header_data.enable_lti_permission;
        this.enable_coaching_tracker = this.header_data.enable_coaching_tracker;
        this.enable_assessment_tracker = this.header_data.enable_assessment_tracker;
        this.enable_tracking = this.header_data.enable_tracking;
        this.assessment_huddle_active = this.header_data.assessment_huddle_active;
        this.getting_started_box = this.header_data.user_permissions.users.getting_started_show;
        this.enable_goals = this.header_data.enable_goals;
        this.enable_edtpa = this.header_data.user_current_account.accounts.enable_edtpa;



        let headers = new HttpHeaders({
          'current-lang': this.translations.current_lang
        });
        this.options = { headers: headers };
        // this.getImage();

        this.GetRecentActivityPage(this.page);


        this.anaylyticsObj = {
          user_current_account: this.header_data.user_current_account,
          forceRefresh: false,
          subAccount: '',
          account_id: this.header_data.user_current_account.accounts.account_id,
          user_id: this.header_data.user_current_account.users_accounts.user_id,
          start_date: moment().subtract('1', 'years').format('YYYY-MM-DD'),
          end_date: moment().format('YYYY-MM-DD'),
          folder_type: 2,
          user_permissions: this.header_data.user_permissions
          // is_updated: 1
        }

        // this.profilePageService.getBannerText({ user_id: this.user_id, account_id: this.account_id }).subscribe(data => {
        //   this.banner_data = data;
        // });

        this.getAnalyticsData(this.anaylyticsObj).subscribe((data: any) => {
          // console.log(data);

          this.userData = {
            user_id: this.header_data.user_current_account.users_accounts.user_id,
            first_name: data.user.first_name,
            last_name: data.user.last_name,
            email: data.user.email,
            avatar: this.header_data.user_current_account.User.image,
            role: this.header_data.user_current_account.roles.name,
            image: data.user.image ? 'https://s3.amazonaws.com/sibme.com/static/users/' + this.header_data.user_current_account.users_accounts.user_id + '/' + data.user.image : this.imageStaticUrl,
            roleId: this.header_data.user_current_account.roles.role_id,
            userDescription: data.user.user_description
          };
          this.user_hTracker_permission = data.is_coach;
          var bold = document.createElement("b");
          bold.innerText = 'you';
          let first_name = this.userData?.first_name.substring(0, 12);
          if (this.userData?.first_name.length > 12) first_name = first_name + '...';
          this.banner_heading = this.translations.dashboard_welcom_banner.replace('{account_title}', first_name);
          this.banner_desc = this.translations.dashboard_welcom_banner_desc.replace('{you_bold}', '<b>you</b>')
          if (this.header_data.site_id == 2) this.banner_heading = this.banner_heading.replace('Sibme', 'HMH-CS')
          this.checkHuddleTrackerPermission();

          this.analytics_data = data.profile_analytics[0];
          this.huddleParticipations = data.huddle_participations;
          this.isLoading = false;

        });

        this.socketPushStartSyncNote();
        this.socketGlobalChanel();

      }
    }
  }

  ngAfterViewInit() {

    if (localStorage.getItem('newAccountOwner')) {
      this.changedAccountOwner = localStorage.getItem('newAccountOwner');
      setTimeout(() => {
        this.changeAccount.show();
        localStorage.removeItem('newAccountOwner');
      }, 1100);
    }

    // if(this.getting_started_box == '1')
    // {
    //   setTimeout(() => {
    //     this.gettingStartedModal.show();
    //   }, 200);

    // }

  }

  get checkBanner() {
    const todayDate = moment().format('YYYY-MM-DD');
    return moment(todayDate).isBefore('2020-07-01', 'month');
  }

  onScroll(event) {
    let element = this.box.nativeElement;
    let atBottom = parseInt(element.scrollHeight) - parseInt(element.scrollTop) === element.clientHeight;

    if (atBottom) {
      if (!this.activityLoading && this.page * 10 < this.totalItems) {
        this.GetRecentActivityPage(this.page, true);
      }
    }

  }

  GetRecentActivityPage(page, onScrollCall?: boolean) {
    if (onScrollCall) this.activityLoading = true;

    let obj = {
      account_id: this.header_data.user_current_account.accounts.account_id,
      user_id: this.header_data.user_current_account.users_accounts.user_id,
      user_current_account: this.header_data.user_current_account,
      page: page
    }

    this.GetRecentActivity(obj).subscribe((data: any) => {
      
      console.log('activities12345',data);

      this.recent_activities = [...this.recent_activities, ...data.activity_record];

      console.log('recentactivity12345',this.recent_activities);

      const event = new SlimScrollEvent({
        type: 'scrollToPercent',
        percent: 95,
        duration: 100,
        easing: 'linear'
      });

      // this.scrollEvents.emit(event);

      this.activityLoading = false;

      if (page == 1) {

        this.totalItems = data.total_count;
        this.activitySkelton = false;
      }
      this.page = data.next_page;



    });

  }


  hideGettingStartedModalDone() {
    this.gettingStartedModal.hide();

    let obj = {

      account_id: this.account_id,
      user_id: this.user_id

    }
    if (this.dont_show_box) {
      this.getting_started_box = '0';
      this.UpdateGettingStarted(obj).subscribe((data: any) => {

      })
    }
  }

  public UpdateGettingStarted(obj) {

    let path = environment.APIbaseUrl + "/update_getting_started_model_show";

    return this.http.post(path, obj, this.options);

  }

  showChildModal(): void {
    this.childModal.show();
  }

  hideChildModal(): void {
    this.childModal.hide();
  }

  public RedirectToLearningCenter() {
    let path = "";
    if (this.onGettingStartedModal) {
      path = environment.baseUrl + "/app/sibme_learning_center/1";
    } else {
      path = environment.baseUrl + "/app/sibme_learning_center/";
    }
    return this.http.get(path, this.options);
  }

  public RedirectToLearningCenterOrCreateUser() {
    if (this.site_id == 1) {
      this.RedirectToLearningCenter().subscribe((data: any) => {
        if (data.show_popup == true) {
          this.showChildModal();
        } else {
          if (data.learning_center_login_url) {
            window.open(data.learning_center_login_url, "_blank");
            this.hideChildModal();
          }
        }

      });
    } else {
      window.open("https://learn.sibme.com", "_blank");
    }
  }

  public CreateUserInLearningCenter(obj) {
    let path = environment.baseUrl + "/app/create_new_thinkific_account";
    return this.http.post(path, obj, this.options);
  }

  toggleNewPassword() {
    this.showPasswordFlag = !this.showPasswordFlag;
    if (this.showPassword == 'password')
      this.showPassword = 'text';
    else
      this.showPassword = 'password'

  }

  toggleConfirmPassword() {
    this.showConfirmPasswordFlag = !this.showConfirmPasswordFlag;
    if (this.showConfirmPassword == 'password')
      this.showConfirmPassword = 'text';
    else
      this.showConfirmPassword = 'password'

  }

  OpenModalcreateNewLearningCenterUser() {
    if (this.model.password == '') {
      this.toastr.ShowToastr('info',this.translations.new_password_field_required_cp)
    }
    else if (this.model.confirm_password == '') {
      this.toastr.ShowToastr('info',this.translations.confirm_password_field_required_cp)
    }
    else if ((this.model.password.length > 0 && this.model.confirm_password.length > 0) && (this.model.password.length < 8 || this.model.confirm_password.length < 8)) {
      this.toastr.ShowToastr('info',this.translations.password_limit_cp)
    }

    else if (this.model.password != this.model.confirm_password) {
      this.toastr.ShowToastr('info',this.translations.password_dont_match_cp)
    }

    if ((this.model.password.length >= 8 && this.model.confirm_password.length >= 8) && (this.model.password == this.model.confirm_password)) {
      let sessionData: any = this.headerService.getStaticHeaderData();
      let obj = {
        password: this.model.password,
        user_current_account: sessionData.user_current_account,
        returnToBundles: this.onGettingStartedModal
      };

      this.CreateUserInLearningCenter(obj).subscribe((data: any) => {
        if (data.success == false) {
          this.toastr.ShowToastr('info',data.message);
        } else {
          if (data.learning_center_login_url) {
            window.open(data.learning_center_login_url, "_blank");
            this.hideChildModal();
          }
        }
      }, error => {

      })

    }
  }

  createUser(flag) {
    if (flag == 0) {
      this.show_question = true;
      this.hideChildModal();
    } else {
      this.show_question = false;
    }
  }


  hideGettingStartedModal() {
    this.onGettingStartedModal = false;
    this.gettingStartedModal.hide();
  }

  showGettingStartedModal() {
    // this.onGettingStartedModal = true;
    // this.gettingStartedModal.show();
  }

  public GetRecentActivity(obj) {

    let path = environment.APIbaseUrl + "/get_dashboard_user_activities";
    // console.log('path',path);
    // console.log('obj',obj);
    // console.log('option',this.options);
    return this.http.post(path, obj, this.options);

  }

  public getAnalyticsData(obj) {
    let path = environment.APIbaseUrl + "/get_analytics_stats";
    return this.http.post(path, obj, this.options);
  }

  pageChanged(page) {
    this.page = page;
    this.GetRecentActivityPage(page);
  }



  private socketPushStartSyncNote() {
    this.socket_listener = this.socketService.pushEventWithNewLogic(`homepage-${this.account_id}-${this.user_id}`).subscribe(data => this.processStartSyncNote(data));


  }

  private socketGlobalChanel() {
    this.socketService.pushEventWithNewLogic('global-channel').subscribe(data => {
      if (data.live_video_event == "1") {
        this.updateActivity(data);
      } else if (data.recent_activity_data) {
        this.syncStreamStarted(data.recent_activity_data);
      }
    });
  }
  /**FOr Recent activities */
  private processStartSyncNote(data) {
    switch (data.event) {
      case "sync_note_started":
        this.syncNoteStarted(data.data);
        break;
      default:
        break;
    }
  }
  private syncNoteStarted(activity_data) {
    activity_data.activityLogs_users = this.translations.you_dashboard;
    activity_data.resource_name = this.translations.started_synced_video_notes_activity;
    this.recent_activities.unshift(activity_data);
  }

  private syncStreamStarted(activity_data) {

    this.appMainService.check_if_huddle_participant(activity_data.huddle_id, this.user_id).subscribe((data: any) => {
      if (data.result) {
        if (activity_data.activityLogs_user_id == this.user_id) {
          activity_data.activityLogs_users = this.translations.you_dashboard;
        }
        this.recent_activities.unshift(activity_data);
      }
    });

  }
  /**update live streaming recent activity after saved video */
  private updateActivity(data) {

    var activity = this.recent_activities.find(item => item.item_id = data.item_id);
    if (activity) {
      activity.resource_url = `/video_details/home/${data.huddle_id}/${data.item_id}`;

    }
  }

  hideBanner() {

    let obj = {
      hide_banner: 1,
      user_id: this.header_data.user_current_account.users_accounts.user_id,
      account_id: this.header_data.user_current_account.accounts.account_id

    }
    this.profilePageService.hidebanner(obj).subscribe((data: any) => {
      if (data.success) {
        this.banner = 1;
      }
      else {
        this.toastr.ShowToastr('error',"Network failure");
      }
    })

  }

  private loadCss(style: any) {
    let head = document.getElementsByTagName('head')[0];
    let link = document.createElement('link');
    link.id = style.id;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = style.path;
    head.appendChild(link);
  }

  public checkHuddleTrackerPermission() {
    if ((this.user_role == 100 || this.user_role == 110 || this.user_role == 115 || this.user_role == 120) && (this.header_data.tracker_permission || this.user_hTracker_permission == 1)) {
      if (this.user_role == 120 && !this.header_data.is_user_coach) {
        this.checkHuddleTracker = false;
        return false
      }
      this.checkHuddleTracker = true;
    }
    else {
      this.checkHuddleTracker = false;
    }
  }

  public redirectToBetterTogether() {
    window.open("https://info.sibme.com/better-together", "_blank");
  }

  closeChangeAccountModal() {
    this.changeAccount.hide();
  }




  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
