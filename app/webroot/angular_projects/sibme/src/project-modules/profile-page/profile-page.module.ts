import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { ProfilePageRoutingModule } from './profile-page-routing.module';
import { ProfilePageComponent } from './components/profile-page.component';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgxPaginationModule } from 'ngx-pagination';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgSlimScrollModule, SLIMSCROLL_DEFAULTS } from 'ngx-slimscroll';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ProfilePageComponent
  ],
  imports: [
    CommonModule,
    LoadingBarHttpClientModule,
    ProfilePageRoutingModule,
    FormsModule,
    BsDropdownModule,
    NgxPaginationModule,
    ModalModule.forRoot(),
    NgSlimScrollModule,
    
],
providers: [{ provide: SLIMSCROLL_DEFAULTS, useValue: { alwaysVisible: false }}]
})
export class ProfilePageModule {
}
