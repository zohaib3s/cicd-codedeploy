import { NgModule } from '@angular/core';

import { __IMPORTS, __DECLARATIONS, __PROVIDERS, __ENTRY_COMPONENTS } from './components.barrel';

import { AppRoutes } from './app.routes';
import { AppComponent } from './components';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { AppLevelSharedModule } from '../shared/shared.module';
import {PublicModule} from '../public/public.module'
import {DropdownModule} from 'primeng/dropdown';

import { ArchivingGuard } from '../shared/guards/archiving.guard';
import { GlobalExportGuard } from '../shared/guards/global-export.guard';
import { PlansGuard } from '../shared/guards/plans.guard';

@NgModule({
  declarations: [__DECLARATIONS, PageNotFoundComponent],
  imports: [__IMPORTS, AppRoutes,AppLevelSharedModule,DropdownModule,PublicModule],
  providers: [__PROVIDERS, ArchivingGuard, GlobalExportGuard, PlansGuard],
  bootstrap: [AppComponent],
  entryComponents: [__ENTRY_COMPONENTS]
})
export class AppModule { }
