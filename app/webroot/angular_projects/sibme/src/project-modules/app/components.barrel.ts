import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { ErrorHandler } from '@angular/core';

import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { ToastrModule } from 'ngx-toastr';

import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ModalModule } from "ngx-bootstrap/modal";
import { TabsModule } from "ngx-bootstrap/tabs";
import { TooltipModule } from "ngx-bootstrap/tooltip";

import { BaseService, GlobalErrorHandlerService } from "./services";

import {
  AppComponent, AppHeaderComponent, LogoAreaComponent, AccountInformationComponent, TrialMessageComponent, UserComponent,
  NavigationComponent, ImpersonationComponent, BreadcrumbsComponent
} from './components';

import { LanguageInterceptor } from "@projectModules/app/helpers";
import { IndeterminateDirective } from '../account-settings/components/directives';
import { PrivateAppComponent } from './components/private/private-app/private-app.component';
import { RouterModule } from '@angular/router';
import { LoginCheckGuard } from './guards/login-check.guard';
import { LoginComponent } from './components/public/login/login.component';
import { ForgotPasswordComponent } from './components/public/forgot-password/forgot-password.component';
import { LaunchpadComponent } from './components/private/launchpad/launchpad.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { CookieInterceptor } from './helpers/cookie-interceptor';
import { AngularCallInterceptor } from './helpers/angular-call-interceptor';
import { FilestackService } from '../video-page/services';
export const __IMPORTS = [
  BrowserModule,
  BrowserAnimationsModule,
  FormsModule,
  HttpClientModule,
  LoadingBarHttpClientModule,
  ModalModule.forRoot(),
  BsDropdownModule.forRoot(),
  ToastrModule.forRoot({
    maxOpened: 0,
    preventDuplicates: true
  }),
  TooltipModule.forRoot(),
  TabsModule.forRoot(),
  RouterModule,
  Ng2SearchPipeModule
];

export const __DECLARATIONS = [
  AppComponent, PrivateAppComponent, AppHeaderComponent, LogoAreaComponent, AccountInformationComponent, TrialMessageComponent, UserComponent,
  NavigationComponent, ImpersonationComponent, BreadcrumbsComponent, IndeterminateDirective, LoginComponent, LaunchpadComponent, ForgotPasswordComponent
];

export const __ENTRY_COMPONENTS = [
];

export const __PROVIDERS = [
  BaseService,
  FilestackService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: LanguageInterceptor,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: CookieInterceptor,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AngularCallInterceptor,
    multi: true
  },
  { provide: ErrorHandler, useClass: GlobalErrorHandlerService },
  { provide: Window, useValue: window },
  LoginCheckGuard
]