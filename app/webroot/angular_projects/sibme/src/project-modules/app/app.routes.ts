import { NgModule } from '@angular/core';
import { Routes, RouterModule, ExtraOptions } from '@angular/router';

import { SharedPageNotFoundComponent } from '../shared/shared-page-not-found/shared-page-not-found.component';
import { PublicAuthGuard } from '../shared/guards/public-auth.guard';
import { GLOBAL_CONSTANTS } from '@constants/constant';
import { PrivateAppComponent } from './components/private/private-app/private-app.component';
import { LoginComponent } from './components/public/login/login.component';
import { LaunchpadComponent } from './components/private/launchpad/launchpad.component';
import { LoginCheckGuard } from './guards/login-check.guard';
import { ForgotPasswordComponent } from './components/public/forgot-password/forgot-password.component';

const videoPageType = GLOBAL_CONSTANTS.VIDEO_PAGE_TYPE;

const routerOptions: ExtraOptions = {
  scrollPositionRestoration: 'enabled',
  anchorScrolling: 'enabled',
  scrollOffset: [0, 64],
};

const routes: Routes = [
  { path: '', redirectTo: 'profile-page', pathMatch: 'full' },
  { path: 'dashboard_angular', redirectTo: 'profile-page', pathMatch: 'full' },
  { path: 'dashboard_angular/home', redirectTo: 'profile-page', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, canActivate: [LoginCheckGuard] },
  { path: 'forgot_password', component: ForgotPasswordComponent, canActivate: [LoginCheckGuard] },

  // { path: '', component: PrivateAppComponent, children: [

    {
      path: 'launchpad',
      component: LaunchpadComponent
    },
    {
      path: 'goals',
      loadChildren: () => import('./../goals/goals.module').then(m => m.GoalsModule)
    },
    {
      path: 'video_huddles',
      loadChildren: () => import('./../video-huddle/video-huddle.module').then(m => m.VideoHuddleModule),
      
    },
    {
      path: 'account-settings',
      loadChildren: './../account-settings/account-settings.module#AccountSettingsModule',
      canActivate: [PublicAuthGuard]
    },
  {
    path: 'video_details',
    loadChildren: () => import('./../video-page/video-page.module').then(m => m.VideoPageModule),
    data: { pageType: videoPageType.HUDDLE }
  },
  {
    path: 'user-settings',
    loadChildren: './../user-settings/user-settings.module#UserSettingsModule'
  },
  {
    path: 'add_huddle_angular',
    loadChildren: () => import('./../add-huddle/add-huddle.module').then(m => m.AddHuddleModule),
  },
  {
    path: 'workspace',
    loadChildren: () => import('./../workspace/workspace.module').then(m => m.WorkspaceModule),
    data: { name: 'workspace' }
  },
  {
    path: 'VideoLibrary',
    loadChildren: './../video-library/video-library.module#VideoLibraryModule'
  },
  {
    path: 'workspace_video',
    loadChildren: () => import('./../video-page/video-page.module').then(m => m.VideoPageModule),
    data: { pageType: videoPageType.WORKSPACE }
  },
  {
    path: 'document-commenting',
    loadChildren: () => import('./../video-page/video-page.module').then(m => m.VideoPageModule),
    data: { pageType: videoPageType.WORKSPACE }
  },
  {
    path: 'library_video',
    loadChildren: './../video-page/video-page.module#VideoPageModule',
    data: { pageType: videoPageType.LIBRARY }
  },
  {
    path: 'analytics_angular',
    loadChildren: () => import('./../analytics/analytics.module').then(m => m.AnalyticsModule),
    data:{breadcrumb:'header_list_analytics'}
  },
  {
    path: 'trackers',
    loadChildren: () => import('./../trackers/trackers.module').then(m => m.TrackersModule),
  },
  {
    path: 'people',
    loadChildren: () => import('./../people/people.module').then(m => m.PeopleModule),
  },
  {
    path: 'profile-page',
    loadChildren: () => import('./../profile-page/profile-page.module').then(m => m.ProfilePageModule),
  },// ]},

  { path: 'page_not_found', component: SharedPageNotFoundComponent },
  { path: '**', redirectTo: 'page_not_found', pathMatch: 'full'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes, routerOptions)],
  exports: [RouterModule],
  providers: [PublicAuthGuard]
})
export class AppRoutes { }
