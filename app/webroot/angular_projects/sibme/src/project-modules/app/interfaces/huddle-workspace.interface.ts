/**
 * Declare all interfaces for huddle and workspace
 */

export type DisplayStyle = 'list' | 'grid';
export type PageType = 'huddle-page' | 'workspace-page' | 'library-page' | 'workspace-video-page' | 'huddle-video-page';