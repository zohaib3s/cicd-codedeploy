import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, NavigationEnd  } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from "@environments/environment";
import { HeaderService } from '@app/services';
import { map } from 'rxjs/operators';
import { Location } from '@angular/common';
import { filter } from 'rxjs/operators';

@Injectable()
export class LoginCheckGuard implements CanActivate {

    constructor(private headerService: HeaderService, private router: Router, private location: Location) {
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {

        return this.headerService.checkUserLoginStatus().pipe(map((headerData: any) => {
            if(route.queryParams.logout) {
                if(headerData.status) {
                    this.headerService.logoutUser().subscribe(() => {
                        if(this.headerService.getLocalStorage("auth_token")){        //Removing Auth Token if remember me is selected
                            this.headerService.removeLocalStorage("auth_token")
                        }
                        this.headerService.setalertPrenventValue(true);
                    });
                } 
                return true;
            } else {
                if(headerData.status) {
                    if (window.history.length > 2) this.location.back();
                    else this.router.navigate(['profile-page']);

                    return false;
                } else {
                    return true;
                }
            }
        }));
    }
}