/**
 * {comfortZome} On page load, user didn't interact any of the recording option
 * {initialized} User clicks on one of the recording option with or without camera
 * {started} Recording has been started
 * {paused} Recording has been paused
 * {stopped} Recording has been stopped
 * {uploading} Recording is being uploading
 * {aborted} Recording has been aborted
 */
export type ScreenRecordingState = 'comfortZone' | 'initialized' | 'started' | 'paused' | 'resumed' | 'stopped' | 'cancelled' | 'uploading' | 'aborted';
export type ScreenRecordingType = 'withCam' | 'withoutCam';

type ScreenRecordinQueryParams = { huddleId?: number, huddleName?: string, folderId?: string };
export type ScreenRecordingUrl = { link: string, queryParams: string | ScreenRecordinQueryParams };

export type LiveStreamState = 'comfortZone' | 'initialized' | 'started' | 'stopped' | 'initialized-screen-sharing' | 'started-screen-sharing' | 'stopped-screen-sharing' | 'cancelled';

type LiveStreamQueryParams = { live_streamer?: boolean, huddleName?: string, folderId?: string };
export type LiveStreamData = { videoId?: number, huddleId?: number, link?: string, queryParams?: string | LiveStreamQueryParams };

export type BrowserAgent = 'opera' | 'firefox' | 'safari' | 'ie' | 'edge' | 'chrome' | 'chromium';
export type InternetState = 'online' | 'offline';