import { Injectable} from '@angular/core';
import oEcho from 'laravel-echo';
import io from 'socket.io-client';
import * as _ from "underscore";
import { environment } from "@environments/environment";
import { BehaviorSubject, Observable } from 'rxjs';


declare global {
  interface Window { io: any; }
  interface Window { Echo: any; }
}

declare var Echo: any;

window.io = io;
window.Echo = window.Echo || {};

@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private eventDataSource = new BehaviorSubject<any>({});
  public EventData = this.eventDataSource.asObservable();

  private config: any = {
    broadcaster: 'socket.io',
    host: environment.socketHost
  }

  constructor() {
    if (typeof window.io !== 'undefined') {
      window.Echo = new oEcho(this.config);
    } else {
      console.error("There's some problem accessing sockets host = "+this.config.host);
    }
    // this.debugWebsocketConnection();
  }

  public pushEvent(name, event = "BroadcastSibmeEvent") {

    if (!_.contains(environment.channelsArr, name)) {
      environment.channelsArr.push(name);
      window.Echo.channel(name).listen(event, (data) => {
        setTimeout(() => {
          this.eventDataSource.next(data);
        }, 500);
      });
    }

  }

  public pushEventWithNewLogic(name, event = "BroadcastSibmeEvent") {
    return new Observable<any>(observer => {

      if (!_.contains(environment.channelsArr, name)) {
        environment.channelsArr.push(name);
      }

      window.Echo.channel(name).listen(event, (data) => {
        observer.next(data)
      });
    });

  }

  public debugWebsocketConnection(){
    /*
    socket.on('update', data => console.log(data));
    socket.on('connect_error', err => handleErrors(err));
    socket.on('connect_failed', err => handleErrors(err));
    socket.on('disconnect', err => handleErrors(err));
    */
    window.Echo.connector.socket.on('connect', function() {
      console.log("ws- ----------------------------------Websocket Connected");
    });
    window.Echo.connector.socket.on('update', function(data) {
      console.log("ws- ----------------------------------Websocket Updated", data);
    });
    window.Echo.connector.socket.on('connect_error', function(err) {
      console.log("ws- ----------------------------------Websocket Error Connecting", err);
    });
    window.Echo.connector.socket.on('connect_failed', function(err) {
      console.log("ws- ----------------------------------Websocket Connection Failed", err);
    });
    window.Echo.connector.socket.on('disconnect', function(err) {
      console.log("ws- ----------------------------------Websocket Disconnected", err);
    });
  }

}
