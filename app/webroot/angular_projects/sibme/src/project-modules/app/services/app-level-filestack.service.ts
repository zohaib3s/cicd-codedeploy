import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as filestack from 'filestack-js';

import { environment } from "@environments/environment";

@Injectable({ providedIn: 'root' })
export class AppLevelFilestackService {

	private options = {
		location: "s3",
		path: '',
		access: 'public',
		container: environment.container,
		region: 'us-east-1',
	}

	private uploadProgressSource = new BehaviorSubject<number>(0);
	public uploadProgress$ = this.uploadProgressSource.asObservable();

	constructor() { }

	public upload(audioBlob: Blob, filename: string, accountId: number, token: any) {
		const filestackInstance = filestack.init(environment.fileStackAPIKey);

		const date = new Date();
		this.options.path = this.makeUploadPath(accountId, date);
		return filestackInstance.upload(audioBlob, { onProgress: this.onProgress }, { ...this.options, filename: filename }, token);
	}

	/**
	 * Update upload progress observable
	 * Fake update progress by 1% of each time 0 returned from server upto 4% for a visual display
	 * @param evt 
	 */
	public onProgress = (evt) => {
		if (evt.totalPercent < 5) {
			if(this.uploadProgressSource.getValue() < 5) {
				let totalPercent = this.uploadProgressSource.getValue();
				this.uploadProgressSource.next(++totalPercent);
			}
		} else this.uploadProgressSource.next(evt.totalPercent);
	};

	public resetUploadProgress() {
		this.uploadProgressSource.next(0);
	}

	private makeUploadPath(accountId: number, date: Date) {
		return `/tempupload/${accountId}/recorded-video/${date.getFullYear()}/${this.getZeroedNum(date.getMonth() + 1)}/${this.getZeroedNum(date.getDate())}/`;
	}

	/**
	 * Return number with 0 prefix if number is less than 10 otherwise return number
	 * @param number 
	 */
	private getZeroedNum(number: number) {
		if (number < 10) return `0${number}`;
		else return number;
	}
}
