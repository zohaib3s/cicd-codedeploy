import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { BehaviorSubject, Subject, Observable, Observer, fromEvent, merge} from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { environment } from "@environments/environment";
import { ScreenRecordingState, LiveStreamState, LiveStreamData, ScreenRecordingUrl } from "@app/types";

@Injectable({ providedIn: 'root' })

export class HeaderService {

  private headerDataUrl: string = `${environment.baseHeaderUrl}/angular_header_api/huddle_list`;
  public loggedIn = false;
  @Output() public LoggedIn: EventEmitter<boolean> = new EventEmitter();
  @Output() public current_lang: EventEmitter<boolean> = new EventEmitter();
  @Output() public account_change: EventEmitter<boolean> = new EventEmitter();
  @Output() public uploadFilesList: EventEmitter<boolean> = new EventEmitter();
  @Output() public changeMousePointer: EventEmitter<boolean> = new EventEmitter();

  private languageTranslationSource = new BehaviorSubject<any>({});
  public languageTranslation$ = this.languageTranslationSource.asObservable();
  public isActive = new Subject<any>();
  private behaviorialHeaderDataSource = new BehaviorSubject<any>({});
  public behaviorialHeaderData$ = this.behaviorialHeaderDataSource.asObservable();
  private headerDataSource = new Subject<any>();
  public headerData$ = this.headerDataSource.asObservable();
  private alertPrevent = new BehaviorSubject<any>({});
  public alertPrevent$ = this.alertPrevent.asObservable();
  private currentUserImage = new BehaviorSubject<any>({});
  public currentUserImage$ = this.currentUserImage.asObservable();
  public tabrecordIcon = new BehaviorSubject<Boolean>(false);
  public tabrecordIcon$ = this.tabrecordIcon.asObservable();
  public audioExtesionList: any = ['aif', 'aiff', 'mp3', 'm4a', 'ogg', 'wav', 'wma']
  public selectedLibAcct = -1;
  private data: any = {};
  private userId: number;
  private colors: any = {};
  public isPFanalytics: boolean = false;
  public g_modal_data: any;
  private baseUrl = environment.baseUrl;
  public userImageSubject: BehaviorSubject<any>;
  public platformCheckOnLogin: BehaviorSubject<any>;
  public userFirstNameSubject: BehaviorSubject<any>;
  public userLastNameSubject: BehaviorSubject<any>;
  public userEmailSubject: BehaviorSubject<any>;
  public libraryPermissionSubject: BehaviorSubject<any>;
  public analyticsPermissionSubject: BehaviorSubject<any>;
  public accountAnalyticsPermissionSubject: BehaviorSubject<any>;
  public accountNameSubject: BehaviorSubject<any>;
  public header_background_color: BehaviorSubject<any>;
  public logo_background_color: BehaviorSubject<any>;
  public logo_image: BehaviorSubject<any>;
  public lastTriedToNavigateLink: string;

  private screenRecordingDataSource = new BehaviorSubject<any>({});
  public screenRecordingData$ = this.screenRecordingDataSource.asObservable();

  /** New screen recording workflow variables start */
  private screenRecordingStateSource = new BehaviorSubject<ScreenRecordingState>('comfortZone');
  public screenRecordingState$ = this.screenRecordingStateSource.asObservable();
  private videoRecordTimeSource = new BehaviorSubject<number>(0);
  public videoRecordTime$ = this.videoRecordTimeSource.asObservable();
  private screenRecordingUrlSource = new Subject<ScreenRecordingUrl>();
  public screenRecordingUrl$ = this.screenRecordingUrlSource.asObservable();

  private reportsDetailDataSource = new BehaviorSubject<any>({});
  public reportsDetailData$ = this.reportsDetailDataSource.asObservable();
  
  /** New screen recording workflow variables end */

  /** Live stream screen sharing start */
  private liveStreamStateSource = new BehaviorSubject<LiveStreamState>('comfortZone');
  public liveStreamState$ = this.liveStreamStateSource.asObservable();
  private liveStreamVideoTimeSource = new BehaviorSubject<number>(0);
  public liveStreamVideoTime$ = this.liveStreamVideoTimeSource.asObservable();
  private liveStreamDataSource = new BehaviorSubject<LiveStreamData>({});
  public liveStreamData$ = this.liveStreamDataSource.asObservable();
  /** Live stream screen sharing end */

  /**Toastr show  */
  public enableToasterPermission : BehaviorSubject<any>;

  constructor(private http: HttpClient) {
    this.userImageSubject = new BehaviorSubject<any>('');
    this.platformCheckOnLogin = new BehaviorSubject<any>('');
    this.userFirstNameSubject = new BehaviorSubject<any>('');
    this.userLastNameSubject = new BehaviorSubject<any>('');
    this.userEmailSubject = new BehaviorSubject<any>('');
    this.libraryPermissionSubject = new BehaviorSubject<any>('');
    this.analyticsPermissionSubject = new BehaviorSubject<any>('');
    this.accountAnalyticsPermissionSubject = new BehaviorSubject<any>('');
    this.accountNameSubject = new BehaviorSubject<any>('');
    this.header_background_color = new BehaviorSubject<any>('');
    this.logo_background_color = new BehaviorSubject<any>('');
    this.logo_image = new BehaviorSubject<any>('');
    this.enableToasterPermission = new BehaviorSubject<any>('');
  }

  hideTabRecordIcon(value) {
    this.tabrecordIcon.next(value);
  }
  setalertPrenventValue(value) {
    this.alertPrevent.next(value);
  }

  setCurrentUserImageValue(value) {
    this.currentUserImage.next(value);
  }

  public getExtension(url) {
    var re = /(?:\.([^.]+))?$/;
    var ext = re.exec(url)[1];
    return ext;
  }

  public getAudioURL() {
    let str = "https://s3.amazonaws.com/sibme.com/app/img/audio-thumbnail.png";
    let headerData = this.getStaticHeaderData();
    if (headerData.site_id == 2)
      str = "https://s3.amazonaws.com/sibme.com/app/img/audio-hmh-thumbnail.png";

    return str;
  }

  public isAValidAudio(type = 'mp4') {
    let result = false;
    this.audioExtesionList.forEach(element => {
      if (element == type.toLowerCase())
        result = true;

    });

    return result;


  }

  public getColors() {

    return this.colors;

  }
  formateDate(date, format) {
    return moment(date).format(format);
  }

  public checkUserLoginStatus(){        //To check on login-check guard if user is logged in already or not
    let path = this.baseUrl + "/users/check_login_status";
    return this.http.get<any>(path).pipe(tap((data : any) => {
      this.platformCheckOnLogin.next(data.site_id);
    }));
  }

  public loadData(path) {
    return this.http.get<headerData[]>(path).pipe(tap((data : any) => {
      if(data.status == false) {
        this.data = false;
        //If user is not logged in, redirect the url to login page
        if(!environment.production)  {
          location.href = "/login";
        } else {
          location.href = "/home/login";
        }

      } else {
        this.data = data;
        if ('idp_link' in this.data) {
          location.href = this.data.idp_link;
          return;
        } else {
          this.UpdateLoggedInStatus(data);
          this.updateBehaviorialHeaderDataSource(data);
          this.updateHeaderDataSource(data);
          this.userImageSubject.next(this.data.avatar_path);
          this.platformCheckOnLogin.next(this.data.site_id);
          this.userFirstNameSubject.next(this.data.user_current_account.User.first_name);
          this.userLastNameSubject.next(this.data.user_current_account.User.last_name);
          this.userEmailSubject.next(this.data.user_current_account.User.email);
          this.libraryPermissionSubject.next(this.data.library_permission);
          this.analyticsPermissionSubject.next(this.data.user_permissions.UserAccount.permission_view_analytics);
          this.accountAnalyticsPermissionSubject.next(this.data.analytics_permissions);
          this.accountNameSubject.next(this.data.user_current_account.accounts.company_name);
          this.enableToasterPermission.next(this.data.enable_angular_toasters);
          if(this.data.user_permissions.accounts.header_background_color === null) {
            if(this.data.user_permissions.accounts.site_id === '1'){
              this.header_background_color.next('#004061');
            }else {
              this.header_background_color.next('#7B7E81')
            }
          }
          else {
            this.header_background_color.next('#'+this.data.user_permissions.accounts.header_background_color)
          }
          this.logo_background_color.next(this.data.logo_background_color);
          this.userId = this.data.user_current_account.User.id;
        }
      }
    }));
  }

  private redirectToIdp(data){
    if ('idp_link' in data) {
      location.href = data.idp_link;
      return;
    }
  }

  private UpdateLoggedInStatus(data) {
    this.colors = {

      loading_bar_color: data.secondry_button_color,
      primery_button_color: data.primery_button_color,
      secondry_button_color: data.secondry_button_color

    };
    if (data.language_translation) {
      this.current_lang.emit(data.language_translation.current_lang);
      this.updateLanguageTranslationSource(data.language_translation);

    }

    this.LoggedIn.emit(data.status);
    this.loggedIn = data.status;

  }

  UpdateUploadFilesList(data){
    
    this.uploadFilesList.emit(data)
  }

  UpdateMouseEvent(data){
    this.changeMousePointer.emit(data);
  }

  private updateBehaviorialHeaderDataSource(headerData: any) {
    this.behaviorialHeaderDataSource.next(headerData);
  }

  private updateHeaderDataSource(headerData: any) {
    this.headerDataSource.next(headerData);
  }

  /**
   * Return logged-in user data
   * @param key optional if provided then return data w.r.t key
  */
  public getStaticHeaderData(key?: string) {

    if (key) return this.data[key];
    else return this.data;
  }



  public checkFileSizeLimit(files, notAllowedType = 'video') {
    let mblimit = 30;
    let limit = (1048576 * mblimit);
    if (files.length) {
      let size = 0;
      let isvalid = true
      files.forEach(function (item) {
        size = size + item.size;
        let x = item.type.split('/')
        if (x[0] == notAllowedType) {
          isvalid = false
        }
      });

      if (size > limit) {
        return { "status": false, "message": `Files size should be less then ${mblimit} MBs` };
      } else if (!isvalid) {
        return { "status": false, "message": `${notAllowedType} files are not allowed` };
      }
      else {
        return { "status": true, "message": "Success" };
      }
    }
    return { "status": true, "message": "No file selected yet!" };
  }

  public getHeaderData(path?: string) {
    path = path || this.headerDataUrl;
    return this.loadData(path);
  }

  public getCurrentUserImage() {

  }
  public getEnglishSpanishDate(unix_timestamp, location = '') {
    let sessionData = this.getStaticHeaderData();
    let current_lang = sessionData.language_translation.current_lang;
    let date = new Date(unix_timestamp * 1000);
    let ano = date.getFullYear();
    let mes = date.getMonth();
    let dia = date.getDay();
    let time = this.formatAMPM(date);

    /*let diasemana = date('w',timestamp);
    let diassemanaN= ["Domingo","Lunes","Martes","Mi�rcoles",
        "Jueves","Viernes","S�bado"];
    let mesesN= {1:"Enero",2:"Febrero",3:"Marzo",4:"Abril",5:"Mayo",6:"Junio",7:"Julio",
    8:"Agosto",9:"Septiembre",10:"Octubre",11:"Noviembre",12:"Diciembre"};*/
    let mesesN = [];
    if (current_lang == "es") {
      mesesN = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];
    }
    else {
      mesesN = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    }

    if (location == 'dashboard') {
      return mesesN[mes] + ' ' + dia;
    }
    else if (location == 'trackers') {
      return dia + '-' + mesesN[mes];
    }
    else if (location == 'trackers_filter') {
      return mesesN[mes];
    }
    else if (location == 'archive_module') {
      return mesesN[mes] + ' ' + dia + ', ' + ano;
    }
    else if (location == 'discussion_time') {
      return time;
    }
    else if (location == 'discussion_date') {
      return mesesN[mes] + ' ' + dia + ', ' + ano + ' ' + time;
    }

  }

  public formatAMPM(date) {
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    let strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
  }

  formatDate(date, format) {
    return moment(date).format(format);
  }

  public updateLanguageTranslationSource(languageTranslation: any) {
    this.languageTranslationSource.next(languageTranslation);
  }

  public afterWorkspaceTrim(origionalDoc, duplictedoc, startTime, endTime, currentUser) {
    let obj =
    {
      src_video_id: origionalDoc,
      dest_video_id: duplictedoc,
      is_duplicated: 1,
      workspace: 1,
      huddle_id: 0,
      user_id: currentUser,
      start_duration: startTime,
      end_duration: endTime,
    }
    let path = environment.APIbaseUrl + "/copy_comments_trimming";

    return this.http.post(path, obj);

  }
  public afterHuddleTrim(origionalDoc, duplictedoc, startTime, endTime, currentUser, huddleId, library = 0) {
    let obj =
    {
      src_video_id: origionalDoc,
      dest_video_id: duplictedoc,
      is_duplicated: 0,
      workspace: 0,
      library: library,
      huddle_id: huddleId,
      user_id: currentUser,
      start_duration: startTime,
      end_duration: endTime,
    }
    let path = environment.APIbaseUrl + "/copy_comments_trimming";
    return this.http.post(path, obj);

  }

  public localStorage(data, location) {
    if (location && data) localStorage.setItem(location, data)
    if (data == null || data == '') localStorage.removeItem(location);
  }

  public configureLocalStorage(key: string, value: string) {

    if (key && value) localStorage.setItem(key, value)
    if (value == null || value == '') localStorage.removeItem(key);

  }

  public setLocalStorage(key: string, data: any): void {

    if (key && data) {
      let dataToBeStored = JSON.stringify(data);
      localStorage.setItem(key, dataToBeStored)
    }

  }
  public setLocalStorage2(key: string, data: any): void {
    localStorage.setItem(key, JSON.stringify(data));
  }

  public getLocalStorage(key: string) {
    return JSON.parse(localStorage.getItem(key))
  }

  public getLocalStoragewithoutParse(key: string) {

    let temp: any = localStorage.getItem(key)
    return temp;

  }

  public removeLocalStorage(key: string): void {
    localStorage.removeItem(key);
  }

  /**
   * Return userId of current logged in user
   */
  public getUserId() {
    return this.userId;
  }
  public getAssesseesubmissions(obj) {

    let path = environment.APIbaseUrl + "/get_assessee_data";
    return this.http.post(path, obj);
  }

  public FormatSeconds(time) {
    if (time == 0 || time == null) return "00:00:00";
    let sec_num: any = parseInt(time, 10);
    let hours: any = Math.floor(sec_num / 3600);
    let minutes: any = Math.floor((sec_num - hours * 3600) / 60);
    let seconds: any = sec_num - hours * 3600 - minutes * 60;

    if (hours < 10) {
      hours = "0" + hours;
    }
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    return hours + ":" + minutes + ":" + seconds;
  }

  public isOpenedInIE() {
    let result = false;
    if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > -1)
      result = true;

    return result;


  }

  public formatToSeconds(time) {
    if (time == 0) return 0;
    if (typeof (time) == "number") return time;
    let stamp = time.split(":");
    return Number(stamp[0] * 3600) + Number(stamp[1] * 60) + Number(stamp[2]);
  }

  public getGoalsAltName(goalAltName?: string) {
    return goalAltName || this.getStaticHeaderData('goal_settings').goal_alt_name || this.getStaticHeaderData('language_translation').goals;
  }

  public getItemAltName() {
    return this.getStaticHeaderData('goal_settings').item_alt_name || this.getStaticHeaderData('language_translation').action_item;
  }

  private getEvidenceAltName() {
    return this.getStaticHeaderData('goal_settings').evidence_alt_name || this.getStaticHeaderData('language_translation').goals_evidence;
  }

  private getMeasurementAltName() {
    return this.getStaticHeaderData('goal_settings').measurement_alt_name || this.getStaticHeaderData('language_translation').goals_measurement;
  }

  /**
  * Replaces goal, goals, Goal and Goals with goal terminology alternate name
  * Replaces action item, action items, Action item and Action Items etc with action item terminology alternate name
  * Replaces evidence and Evidence with evidence terminology alternate name
  * Replaces measurement and Measurement with measurement terminology alternate name
  * @param translation Translation to be modified
  */
  getGoalAltTranslation(translation: string) {
    const goalAltName = this.getGoalsAltName();
    const actionItemAltName = this.getItemAltName();
    const evidenceItemAltName = this.getEvidenceAltName();
    const measurementItemAltName = this.getMeasurementAltName();

    // replace word goal with goal alternate name
    if (goalAltName !== this.getStaticHeaderData('language_translation').goals) {
      const lRegex = /\b(?:goal|goals|meta|metas)\b/g;
      const uRegex = /\b(?:Goal|Goals|Meta|Metas)\b/g;

      translation = translation.replace(lRegex, goalAltName.toLowerCase());
      translation = translation.replace(uRegex, goalAltName);
    }

    // replace word action item with action item alternate name
    if (actionItemAltName !== this.getStaticHeaderData('language_translation').action_item) {
      const lRegex = /\b(?:action item|action items|acción|acciónes)\b/g;
      const uRegex = /\b(?:Action Item|Action Items|Acción|Acciónes)\b/g;
      const cRegex = /\b(?:Action item|Action items)\b/g;

      translation = translation.replace(lRegex, actionItemAltName.toLowerCase());
      translation = translation.replace(uRegex, actionItemAltName);
      translation = translation.replace(cRegex, actionItemAltName);
    }

    // replace word evidence with evidence alternate name
    if (evidenceItemAltName !== this.getStaticHeaderData('language_translation').goals_evidence) {
      const lRegex = /\b(?:evidence|evidencia)\b/g;
      const uRegex = /\b(?:Evidence|Evidencia)\b/g;

      translation = translation.replace(lRegex, evidenceItemAltName.toLowerCase());
      translation = translation.replace(uRegex, evidenceItemAltName);
    }

    // replace word measurement with measurement alternate name
    if (measurementItemAltName !== this.getStaticHeaderData('language_translation').goals_measurement) {
      const lRegex = /\b(?:measurement|indicador|indicadores)\b/g;
      const uRegex = /\b(?:Measurement|Indicador|Indicadores)\b/g;

      translation = translation.replace(lRegex, measurementItemAltName.toLowerCase());
      translation = translation.replace(uRegex, measurementItemAltName);
    }

    return translation;
  }

  public getHeaderDataFromServer() {
    return this.http.get(this.headerDataUrl);
  }

  public updateScreenRecordingDataSource(screenRecordingData: any) {
    this.screenRecordingDataSource.next(screenRecordingData);
  }

  public getCurrentValueScreenRecordingData() {
    return this.screenRecordingDataSource.getValue();
  }

  public changeDateFormat(item) {
    let lang = this.getCurrentLang();
    if (lang == 'es') {
      return moment(item).locale('es').format('LL');
    }
    else {
      return moment(item).format('LL');
    }
  }

  getCurrentLang() {
    let sessionData = this.getStaticHeaderData();
    let lang = "en";
    if (sessionData && sessionData.language_translation && sessionData.language_translation.current_lang) {
      lang = sessionData.language_translation.current_lang;
    }
    return lang;
  }

  /**
   * Return current time of recorded video or default value which is 0
   */
  public getCurrentVideoRecordTime() {
    return this.videoRecordTimeSource.getValue();
  }

  /**
   * Update video record time
   * @param time Updated time
   */
  public updateVideoRecordTimeSource(time: number) {
    return this.videoRecordTimeSource.next(time);
  }

  /**
   * Update screen recording state
   * @param screenRecordingState
   */
  public updateScreenRecordingStateSource(screenRecordingState: ScreenRecordingState) {
    this.screenRecordingStateSource.next(screenRecordingState);
  }

  /**
   * Update screen recording url
   * @param screenRecordingUrl
   */
  public updateScreenRecordingUrlSource(screenRecordingUrl: ScreenRecordingUrl) {
    this.screenRecordingUrlSource.next(screenRecordingUrl);
  }

  public login_user(data) {
    let path = this.baseUrl + "/users/login_api";
    return this.http.post(path, data);
  }

  public getLaunchpadData(){
    let path = this.baseUrl + "/launchpad/get_accounts_api";
    return this.http.get(path);
  }

  public getLaunchpadAccount(account_id){
    let obj = { account_id : account_id }
    let path = this.baseUrl + `/launchpad/switch_account_api`;
    return this.http.post(path, obj);
  }

  public signInBySso(data) {
    let obj = { email : data }
    let path = environment.APIbaseUrl + "/get/sso-link";
    return this.http.post(path, obj);
  }

  public forgotPassword(data) {
    let obj = { email : data }
    let path = this.baseUrl + "/users/forgot_password_api";
    return this.http.post(path, obj);
  }

  public getLanguage(data, keys) {
    let obj = { lang : data, keys : keys }
    let path = environment.APIbaseUrl + "/get_language_based_content";
    return this.http.post(path, obj);
  }

  public changeLanguageApi(data) {
    let obj = { lang : data }
    let path = this.baseUrl + "/app/language_update_in_cookie_session";
    return this.http.post(path, obj);
  }

  public logoutUser(){
    let path = this.baseUrl + "/users/logout";
    return this.http.get(path);
  }

  /**
   * Return current time of recorded video or default value which is 0
   */
  public getLiveStreamVideoTimeSource() {
    return this.liveStreamVideoTimeSource.getValue();
  }

  /**
   * Update live stream video record time
   * @param time Updated time
   */
  public updateLiveStreamVideoTimeSource(time: number) {
    return this.liveStreamVideoTimeSource.next(time);
  }

  /**
   * Update live stream screen sharing state
   * @param LiveStreamState
   */
  public updateLiveStreamStateSource(liveStreamState: LiveStreamState) {
    this.liveStreamStateSource.next(liveStreamState);
  }

  /**
   * Update live stream data
   * @param liveStreamData
   */
  public updateLiveStreamDataSource(liveStreamData: LiveStreamData) {
    this.liveStreamDataSource.next(liveStreamData);
  }

  /**
   * Return current live stream data
   */
  public getLiveStreamDataSource() {
    return this.liveStreamDataSource.getValue();
  }

  /**
   * Check if screen is recording
   */
  public isScreenRecording(): boolean {
    const currentScreenRecordingState: ScreenRecordingState = this.screenRecordingStateSource.getValue();
    return (currentScreenRecordingState === 'started' || currentScreenRecordingState === 'paused' || currentScreenRecordingState === 'resumed');
  }

  public reportsDetailData(data){
    
    this.reportsDetailDataSource.next(data);
  }
}
export interface headerData extends Window {

  [x: string]: any

}