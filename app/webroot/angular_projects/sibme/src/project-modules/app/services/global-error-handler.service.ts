import { Injectable } from '@angular/core';
import { ErrorHandler } from '@angular/core';

import { HeaderService } from "@app/services/header.service";

@Injectable()
export class GlobalErrorHandlerService implements ErrorHandler {

    constructor(private headerService: HeaderService) { }

    handleError(error: any): void {
        console.error('error in global error handler: ', error)
        const chunkFailedMessage = /Loading chunk [\d]+ failed/;

        if (chunkFailedMessage.test(error.message)) {
            console.log('headerService.lastTriedToNavigateLink', this.headerService.lastTriedToNavigateLink);
            if (this.headerService.lastTriedToNavigateLink) window.location.href = this.headerService.lastTriedToNavigateLink;
            else window.location.reload();
        }
    }
}