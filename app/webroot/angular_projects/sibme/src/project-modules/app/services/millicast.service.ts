import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { MillicastModel } from '@app/models';

@Injectable()
export class MillicastService {

  private readonly token: string = "71785ead5b5d08f9651d07f021ae0d53c7152ec913000171aee287b02417642f";
  private readonly milicastPublishAPI: string = 'https://director.millicast.com/api/director/publish';
  private readonly milicastSubAPI: string = 'https://director.millicast.com/api/director/subscribe';
  private readonly milicastTurnUrl: string = 'https://turn.millicast.com/webrtc/_turn';

  private millicast: MillicastModel = new MillicastModel();
  constructor(private httpClient: HttpClient) {
    console.log('MillicastService constructor ===============================')
    console.log('iceServers: ', this.iceServers);
    this.getICEServers();
  }

  get jwt() { return this.millicast.jwt }
  get wsUrl() { return this.millicast.wsUrl }
  get iceServers() { return this.millicast.iceServers }
  get fileName() { return this.millicast.fileName }
  get streamName() { return this.millicast.streamName }
  get streamNameSS() { return this.millicast.streamNameSS }

  private getICEServers() {
    this.httpClient.put(this.milicastTurnUrl, null).toPromise().then((res: any) => {
      if (res.s === 'ok') {
        res.v.iceServers.forEach(iceServer => {
          if (!!iceServer.url) {
            iceServer.urls = iceServer.url;
            delete iceServer.url;
          }
          this.millicast.iceServers.push(iceServer);
        });
      }
    });
  }

  public getJWTAndSocketUrl(streamName: string): Promise<{ status: number, jwt: string, wsUrl: string }> {
    return new Promise((resolve, reject) => {
      const headers: HttpHeaders = new HttpHeaders({ 'Authorization': `Bearer ${this.token}`, 'Content-Type': 'application/json' });
      return this.httpClient.post(this.milicastPublishAPI, { streamName }, { headers }).toPromise().then((res: any) => {
        if (res.status === 'success') resolve({ status: 200, jwt: res.data.jwt, wsUrl: res.data.wsUrl });
        else reject({ status: 500, data: res });
      }).catch(err => {
        reject({ status: 500, data: err });
      });
    });
  }

  public getJWTAndSocketUrlSub(streamName: string): Promise<{ status: number, jwt: string, wsUrl: string }> {
    return new Promise((resolve, reject) => {
      const headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json' });
      return this.httpClient.post(this.milicastSubAPI, { streamAccountId: 'M8Reue', streamName: streamName, unauthorizedSubscribe: true }).toPromise().then((res: any) => {
        if (res.status === 'success') resolve({ status: 200, jwt: res.data.jwt, wsUrl: res.data.wsUrl });
        else reject({ status: 500, data: res });
      }).catch(err => {
        reject({ status: 500, data: err });
      });
    });
  }

  public updateStreamName(streamName: string) {
    this.millicast.streamName = streamName;
  }

  public updateFileName(fileName: string) {
    this.millicast.fileName = fileName;
  }

  public generateStreamNameSS() {
    this.millicast.currentSSNumber++;
    this.millicast.streamNameSS = `${this.millicast.streamName}_screen_sharing_${this.millicast.currentSSNumber}`;
  }

}