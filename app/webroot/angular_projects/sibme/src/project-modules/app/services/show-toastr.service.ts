import { Injectable } from '@angular/core';
import {HeaderService} from './header.service'
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ShowToasterService {
  enable_Toastr;

  constructor(private headerService:HeaderService, private toastr:ToastrService) {
    this.headerService.enableToasterPermission.subscribe((data:any)=>{
      this.enable_Toastr=data;
    })
   }

  ShowToastr(toastrType:string,toastrMessage?:any,title?: string, override? : any ){
    if(this.enable_Toastr == 1){
      switch(toastrType){
        case 'info':
          this.toastr.info(toastrMessage,title,override);
          break;
        case 'success':
          this.toastr.success(toastrMessage);
          break;
        case 'remove':
          this.toastr.remove(toastrMessage);
        case 'clear': 
          this.toastr.clear();
        case 'error':
          this.toastr.error(toastrMessage,title,override);
          break;
        case 'show': 
          return this.toastr.show(toastrMessage,title,override);
          break;
      }
    }
  }

}
