import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HeaderService } from './header.service';

@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {
    localStorage: Storage;
    keyPreFix = ''

    changes$ = new Subject();

    constructor(private headerService: HeaderService) {
        this.localStorage = window.localStorage;
        let header_data = this.headerService.getStaticHeaderData();
        if (Object.keys(header_data).length != 0 && header_data.constructor === Object) {
            let userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
            let account_id = userCurrAcc.users_accounts.account_id;
            let user_id = userCurrAcc.User.id;
            this.keyPreFix = account_id + '-' + user_id + '-';
        }

    }

    get(key: string): any {
        key = this.keyPreFix + key;
        if (this.isLocalStorageSupported) {
            return JSON.parse(this.localStorage.getItem(key));
        }

        return null;
    }

    set(key: string, value: any): boolean {
        key = this.keyPreFix + key;
        if (this.isLocalStorageSupported) {
            this.localStorage.setItem(key, JSON.stringify(value));
            this.changes$.next({
                type: 'set',
                key,
                value
            });
            return true;
        }

        return false;
    }

    remove(key: string): boolean {
        key = this.keyPreFix + key;
        if (this.isLocalStorageSupported) {
            this.localStorage.removeItem(key);
            this.changes$.next({
                type: 'remove',
                key
            });
            return true;
        }

        return false;
    }

    get isLocalStorageSupported(): boolean {
        return !!this.localStorage
    }
}