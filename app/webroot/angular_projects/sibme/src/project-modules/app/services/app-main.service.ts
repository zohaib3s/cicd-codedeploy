import { EventEmitter, Injectable } from '@angular/core';

import { environment } from "@environments/environment";
import { BaseService } from "./base.service";
import { BreadCrumbInterface, ViewerHistory, PublishSetting, PLTabData } from "@app/interfaces";
import { BrowserAgent, InternetState } from "@app/types";
import { Subject, BehaviorSubject } from 'rxjs';
import * as _ from 'lodash';

/**
 * Definition: coming soon!
 */

@Injectable({
  providedIn: 'root'
})
export class AppMainService {

  private APIbaseUrl: string = environment.APIbaseUrl;
  private baseUrl = environment.baseUrl;
  public LiveStreamPage: EventEmitter<any> = new EventEmitter<any>();
 // public fileListner:Subject<any> = new Subject();

  private breadCrumbSource = new Subject<BreadCrumbInterface[]>();
  public breadCrumb$ = this.breadCrumbSource.asObservable();

  private _huddlesDataForShare = new BehaviorSubject<any>(null); // Singleton huddle data for sharing artifacts
  private _categoryDataForShare = new BehaviorSubject<any>(null); // Singleton category data for sharing artifacts

  /** Store audio extensions which are playable by current browser. It would be a singleton array which will be detected on app load
   *  and will be used to play audio or show transcoding message based on this array
   */
  public playableAudioExtensions: string[];
  public browserAgent: BrowserAgent;

  private connectionRequestInProgress: boolean;
  private internetStateSource = new BehaviorSubject<InternetState>('online');
  public internetState$ = this.internetStateSource.asObservable();

  constructor(private baseService: BaseService) {
    this.checkInternetConnectivity();
   }

  updateBreadCrumb(breadCrumbs: BreadCrumbInterface[]) {
    this.breadCrumbSource.next(breadCrumbs);
  }

  public sysRefreash() {
    return this.baseService.__get(`${this.baseUrl}/Huddles/sysRefreash?json_response=true`, true);
  }

  /** Section for video page API's start */
  public GetDocumentComments(data: object) {
    return this.baseService.__post(`${this.APIbaseUrl}/get_document_comments`, data, true);
  }
  public getActiveUsers(data: object) {
    return this.baseService.__post(`${this.APIbaseUrl}/get_user_online_status`, data, true);
  }
  public AddComment(data: object) {
    return this.baseService.__post(`${this.APIbaseUrl}/add_comment`, data, true);
  }
  
  public AddDiscussion(data: object) {
    return this.baseService.__post(`${this.APIbaseUrl}/add_discussion`, data, true);
  }
  public DeleteDiscussion(data: any) {
    return this.baseService.__post(`${this.APIbaseUrl}/delete_discussion`, data, true);
  }

  public EditComment(data: object) {
    return this.baseService.__post(`${this.APIbaseUrl}/edit_comment`, data, true);
  }

  public EditCustomMarket(data: object) {
    return this.baseService.__post(`${this.APIbaseUrl}/update_document_comment_custom_marker`, data, true);
  }

  public EditCommentStatus(data: object) {
    return this.baseService.__post(`${this.APIbaseUrl}/update_document_comment_status`, data, true);
  }

  public DeleteComment(data: object) {
    return this.baseService.__post(`${this.APIbaseUrl}/delete_comment`, data, true);
  }

  public AddReply(data: object) {
    return this.baseService.__post(`${this.APIbaseUrl}/add_reply`, data, true);
  }

  public LogViewerHistory(data: ViewerHistory) {
    return this.baseService.__post(`${this.baseUrl}/app/add_viewer_history`, data, true);
  }

  public GetPublishSettings(data: PublishSetting) {
    return this.baseService.__post(`${this.APIbaseUrl}/show_publish_button_or_not`, data, true);
  }

  public GetPLTabData(data: PLTabData) {
    return this.baseService.__post(`${this.APIbaseUrl}/performace_level_update`, data, true);
  }

  public UploadResource(data: object) {
    return this.baseService.__post(`${this.APIbaseUrl}/upload_document`, data, true);
  }

  public setUserOnlineStatus(data: object) {
    return this.baseService.__post(`${this.APIbaseUrl}/set_user_online_status`, data, true);
  }

  public check_if_huddle_participant(huddle_id, user_id) {
    return this.baseService.__post(`${environment.baseHeaderUrl}/check_if_huddle_participant/${huddle_id}/${user_id}`, {}, true);
  }

  /** Section for video page API's end */
  public parseCookieValue(name) {
    let cookieStr = document.cookie;
    name = encodeURIComponent(name);
    for (const cookie of cookieStr.split(';')) {
      /** @type {?} */
      const eqIndex = cookie.indexOf('=');
      const [cookieName, cookieValue] = eqIndex == -1 ? [cookie, ''] : [cookie.slice(0, eqIndex), cookie.slice(eqIndex + 1)];
      if (cookieName.trim() === name) {
        return decodeURIComponent(cookieValue);
      }
    }
    return null;
  }

  /** Section for Discussion API's start */
  public EditDiscussion(data: object) {
    return this.baseService.__post(`${this.APIbaseUrl}/edit_discussion`, data, true);
  }

  /** Section for Discussion API's start */

  public cropVideo(data: any, callType: any, huddle_id: any, video_id: any) {
    if(callType == "2") return this.baseService.__post(`${this.APIbaseUrl}/trim_video`, data);
    else return this.baseService.__post(`${this.baseUrl}/Huddles/submit_trim_request/${video_id}/${huddle_id}`, data);
  }

  public copyArtifact(data: any) {
    return this.baseService.__post(`${this.APIbaseUrl}/copy`, data);
  }

  public shareFolders(data: any) {
    return this.baseService.__post(`${this.APIbaseUrl}/folders/share_folder`, data);
  }

  public copyVideoToAccounts(data: any) {
    return this.baseService.__post(`${this.APIbaseUrl}/copytoaccounts`, data);
  }

  get huddlesDataForShare(): any {
    return this._huddlesDataForShare.getValue();
  }

  updateHuddlesDataForShare(data: any) {
    this._huddlesDataForShare.next(data);
  }
  get CategoriesDataForShare(): any {
    return _.cloneDeep(this._categoryDataForShare.getValue());
  }

  updateCategoriesDataForShare(data: any) {
    this._categoryDataForShare.next(data);
  }

  public getHuddlesForCopy(data: any) {
    return this.baseService.__post(`${this.APIbaseUrl}/get_copy_huddle`, data);
  }

  public getCategories(data: any) {
    data.library = 1;
    return this.baseService.__post(`${this.APIbaseUrl}/get_categories`, data);
  }
  public getSearchCategories(data: any) {
    return this.baseService.__post(`${this.APIbaseUrl}/folders/search_huddle_folders`, data);
  }
  public getVideoCategories(data: any) {
    return this.baseService.__post(`${this.APIbaseUrl}/get_video_categories`, data);
  }
  public changeVideoCategory(data: any) {
    return this.baseService.__post(`${this.APIbaseUrl}/change_video_categories`, data);
  }

  public submitURLArtifact(apiURL: string, data: any) {
    return this.baseService.__post(`${this.APIbaseUrl}/${apiURL}`, data);
  }

  public updateURLViews(data: any) {
    return this.baseService.__post(`${this.APIbaseUrl}/document_viewed_mobile`, data);
  }

  public getFrameworks(accountId: number) {
    return this.baseService.__get(`${this.APIbaseUrl}/get_frameworks_list/${accountId}`);
  }

  public getFrameworkSettingsById(frameworkId: number) {
    return this.baseService.__post(`${this.APIbaseUrl}/get_framework_settings_by_id`, { framework_id: frameworkId });
  }

  public noSaveVideo(data: any) {
    return this.baseService.__post(`${environment.baseUrl}/Api/video_not_saved`, data);
  }

  public createLiveRecording(data: any) {
    return this.baseService.__post(`${environment.baseUrl}/Api/create_live_recording`, data);
  }

 public liveStreamStatusCheck(data: any) {
    return this.baseService.__post(`${environment.APIbaseUrl}/updateLiveStreamStatus`, data,true);
  }

  public saveVideo(data: any) {
    return this.baseService.__post(`${environment.baseUrl}/Api/save_live_rec_video`, data);
  }

  /**
   * Internet connection state is only required in case of safari to record video with millicast stream, that's why this code will only be executed in case of safari
   * Wait for 5 seconds to until browser agent detected by app-private component
   * Checks realtime internet connectivity by getting an image from current server with http get request and update internet state accordingly
   * It will invoke in interval of 5 seconds and sends next request only if previous one is completed  
   */
  public checkInternetConnectivity() {
    setTimeout(() => {
      if(this.browserAgent === 'safari') {
        setInterval(() => {
          if(!this.connectionRequestInProgress) {
            const randomNum = new Date().getTime();
            this.connectionRequestInProgress = true;
            return this.baseService.__get(`${environment.baseUrl}/img/logo-dark.svg?randomNum=${randomNum}`, true, true).toPromise()
            .then(() => this.internetStateSource.next('online'))
            .catch(() => this.internetStateSource.next('offline'))
            .finally(() => this.connectionRequestInProgress = false);
          }
        }, 5000);
      }
    }, 5000);
  }
}
