import { Component, OnInit, ViewChild } from '@angular/core';
import { HeaderService } from "@projectModules/app/services";
import { environment } from "@environments/environment";
import { Router, NavigationStart, NavigationEnd, ActivatedRoute } from "@angular/router";
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import { LaunchpadComponent } from '../launchpad/launchpad.component';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent implements OnInit {
  public showSibmeLogo: boolean = false;
  public loaded: boolean = false;
  public is_library: boolean = false;
  public headerdetails;
  public launchpad_check: boolean;
  isLoading = true;
  public header_background_color: any;
  // containerClass: any;
  header_class = null;
  containerClass: any;
  constructor(private headerService: HeaderService, private router: Router, private activatedRoute: ActivatedRoute) {
    if (document.location.href.indexOf('hmh') < 0) this.showSibmeLogo = true;
    this.headerService.header_background_color.subscribe((data) => {
      data.includes('##') ? data = data.slice(1, data.length) : false
      this.header_background_color = data;
    });

      router.events.forEach((event) => {
        if (event instanceof NavigationStart || event instanceof NavigationEnd) {
          if (document.location.href.indexOf( GLOBAL_CONSTANTS.LAUNCHPAD.LAUNCHPAD_URL ) >= 0) {
            this.launchpad_check = true;
          } else {
            this.launchpad_check = false;
          }
          if (document.location.href.indexOf('VideoLibrary') >= 0 || document.location.href.indexOf('library_video') >= 0 || document.location.href.indexOf('user-settings') >= 0 || document.location.href.indexOf('account-settings') >= 0 || document.location.href.indexOf('people') >= 0 || document.location.href.indexOf('trackers') >= 0 || document.location.href.indexOf('custom-fields') >= 0|| document.location.href.indexOf('workspace') >= 0 || document.location.href.indexOf('video_huddles') >= 0 || document.location.href.indexOf('add_huddle_angular') >= 0 || document.location.href.indexOf('video_details') >= 0 || document.location.href.indexOf('profile-page') >= 0 || (document.location.href.indexOf('goals') >= 0 && document.location.href.indexOf('goals-settings') < 0 ) ||  document.location.href.indexOf('rubrics') >= 0 ||  document.location.href.indexOf('launchpad') >= 0
          ||  document.location.href.indexOf('trackers') >= 0
          ||  document.location.href.indexOf('pdf-renderer') >= 0
          ||  document.location.href.indexOf('analytics_angular') >= 0 ){
            this.containerClass = 'library_container';
            this.header_class = 'library_container';
          } else this.containerClass = 'container';
        }
      });

  }

  ngOnInit() {
    if (document.location.href.indexOf( GLOBAL_CONSTANTS.LAUNCHPAD.LAUNCHPAD_URL ) >= 0) {
      this.launchpad_check = true;
    } else {
      this.launchpad_check = false;
    }
    if (document.location.href.indexOf('VideoLibrary') >= 0 || document.location.href.indexOf('library_video') >= 0  || document.location.href.indexOf('user-settings') >= 0 || document.location.href.indexOf('people') >= 0 || document.location.href.indexOf('account-settings') >= 0 || document.location.href.indexOf('trackers') >= 0 || document.location.href.indexOf('custom-fields') >= 0|| document.location.href.indexOf('workspace') >= 0 || document.location.href.indexOf('video_huddles') >= 0 || document.location.href.indexOf('add_huddle_angular') >= 0 || document.location.href.indexOf('video_details') >= 0 || document.location.href.indexOf('profile-page') >= 0 || (document.location.href.indexOf('goals') >= 0 && document.location.href.indexOf('goals-settings') < 0 ) ||  document.location.href.indexOf('rubrics') >= 0 ||  document.location.href.indexOf('launchpad') >= 0){
      this.containerClass = 'library_container';
      this.header_class = 'library_container';
    } else this.containerClass = 'container';

    this.headerdetails = {};
    this.headerdetails.status = false;
    let project_title = environment.project_title;

    let account_id = this.getParameterByName('account_id');
    let user_id = this.getParameterByName('user_id');

    let base_url = environment.baseHeaderUrl + `/angular_header_api/${project_title}`;

    if (user_id && account_id && document.location.href.indexOf('/canvas/') >= 0) {
      base_url = base_url + `/${account_id}/${user_id}`;
    }
      this.headerService.getHeaderData(base_url).subscribe((header_details: any) => {
        this.headerService.header_background_color.next('#'+header_details.user_permissions.accounts.header_background_color);
  
        //Saving Data To check if user is loggedIn
        // this.headerService.setLocalStorage('headerData', header_details);
  
        this.handleResponse(header_details);
        if (header_details.user_current_account.User.image)
          this.headerService.setCurrentUserImageValue(header_details.avatar_path);
      });

    this.headerService.account_change.subscribe((header_details:any) => {
      if(header_details){
        this.headerdetails.user_current_account.roles.name=header_details.user_role;
        this.headerdetails.user_permissions=header_details.user_permissions;
      }
   });
  }

  private getParameterByName(name, url?) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  private handleResponse(args: any) {
    this.headerdetails = args;

    if (!args.status) {
      var getUrl = window.location;
      var baseUrl = getUrl.protocol + "//" + getUrl.host;
      location.href = baseUrl;
    } else {
      this.loaded = true;
      this.isLoading = false;
    }
  }

  public get_header_bg(args: any) {

    if (args && args.status) {

      return this.header_background_color;

    }

  }

}
