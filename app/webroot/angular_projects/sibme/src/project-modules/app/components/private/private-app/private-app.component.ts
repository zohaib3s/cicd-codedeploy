import { Component, Renderer2, OnInit, ViewChild, OnDestroy, HostListener } from '@angular/core';
import { Event as RouterEvent, Router, NavigationStart, NavigationEnd, ActivatedRoute } from "@angular/router";
import { EventManager } from "@angular/platform-browser";
import { filter, debounceTime } from 'rxjs/operators';
import { ModalDirective } from "ngx-bootstrap/modal";
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';

import { environment } from '@src/environments/environment';
import { HeaderService, SocketService, AppMainService } from "@app/services";
import { ScreenRecordingState, LiveStreamState, LiveStreamData, ScreenRecordingUrl, BrowserAgent } from "@app/types";
import { GLOBAL_CONSTANTS } from "@constants/constant";
import { LaunchpadComponent } from '../launchpad/launchpad.component';
import { FilestackService } from '@videoPage/services';
import { WorkspaceHttpService } from '@src/project-modules/workspace/services';
import { DetailsHttpService } from '@src/project-modules/video-huddle/child-modules/details/servic/details-http.service';
import { timeStamp } from 'console';
import { checkIfStateModificationsAreAllowed } from 'mobx/lib/internal';
import { HighlightSpanKind, isConstructorDeclaration } from 'typescript';


declare var gtag;
declare var window: any;
declare var opr: any;
declare var InstallTrigger: any;
declare var document: any;
declare var safari: any;

@Component({
  selector: "private-app-root",
  templateUrl: "./private-app.component.html",
  styleUrls: ["./private-app.component.css"]
})
export class PrivateAppComponent implements OnInit, OnDestroy {
  @ViewChild('wowzaNotification', { static: false }) wowzaNotification: ModalDirective;
  public isLoggedIn;
  private userCurrAcc: any;
  public title = "huddles";
  public changed_account_name = "";
  public account_switch_title = "Account Change";
  public ok_text = "ok";
  public total_toasters = [];
  public account_switch_message = "It appears you’ve clicked on a link pointing to a different account. Your account has been switch to";
  public colors;
  public channels = environment.channelsArr;
  public screenRecordingData: any;
  public screenSharingStarted: boolean = false;
  private subscriptions: Subscription = new Subscription();
  public screenRecordingState: ScreenRecordingState;
  public liveStreamState: LiveStreamState;
  private timeInterval: any = null;
  public showHeader: boolean = true;
  private liveStreamTimeInterval: any = null;
  public liveStreamData: LiveStreamData;
  public minimizeBox: boolean = false;
  public loaderDivBox: boolean = false;
  public countProgress = {};
  public uploads = [];
  public fileTypeUrl: any = '';
  public allResponses: any = [];
  public confirmCanel: boolean = false;
  public confirmNoToCanel: boolean = false;
  public count:number = 0;
  public leftuploading:number;
  public completeUploadedFilesData:number = 0;

  public globalOjbect:any = {};

  public filesCount:number = 0;

  public toCheckFileUploadProgress:boolean = false;

  public testFilesCount:number = 0;
  public testVidesoCount:number = 0;

  public nfile: any = {
    arrayTwo: []
  };

  @ViewChild('accountChange', { static: false }) accountChange: ModalDirective;
  @ViewChild('confirmToCanelUploading', { static: false }) confirmToCanelUploading: ModalDirective;


  
  baseUrl = environment.baseUrl;

  public is_account_switched: any;

  public screenRecordingUrl: ScreenRecordingUrl;
  public translation: any = {};
  public landingpage = false;
  files: any = {};
  header_data: any;
  public lastUploadFileType:any;
  public uploadFileTypes: any = [
    { type: 'image/jpeg', url: 'assets/upload_file_types/image.png' },
    { type: 'image/png', url: 'assets/upload_file_types/image.png' },
    { type: 'image/jpg', url: 'assets/upload_file_types/image.png' },
    { type: 'application/pdf', url: 'assets/upload_file_types/pdf.png' },
    { type: 'video/mp4', url: 'assets/upload_file_types/video.png' },
    { type: 'audio/mp3', url: 'assets/upload_file_types/video.png' },
    { type: 'text/plain', url: 'assets/upload_file_types/txt.png' },
    { type: 'xls', url: 'assets/upload_file_types/xls.png' },
    { type: 'xlsx', url: 'assets/upload_file_types/xls.png' },
    { type: 'csv', url: 'assets/upload_file_types/xls.png' },
    { type: 'doc', url: 'assets/upload_file_types/word.png' },
    { type: 'docx', url: 'assets/upload_file_types/word.png' },
    { type: 'ppt', url: 'assets/upload_file_types/powerpoint.png' },
    { type: 'pptx', url: 'assets/upload_file_types/powerpoint.png' },
    { type: '3gp', url: 'assets/upload_file_types/video.png' },
    { type: 'mkv', url: 'assets/upload_file_types/video.png' },
    { type: 'mpg', url: 'assets/upload_file_types/video.png' },
    { type: 'mp2', url: 'assets/upload_file_types/video.png' },
    { type: 'mpeg', url: 'assets/upload_file_types/video.png' },
    { type: 'mpe', url: 'assets/upload_file_types/video.png' },
    { type: 'mpv', url: 'assets/upload_file_types/video.png' },
    { type: 'ogg', url: 'assets/upload_file_types/video.png' },
    { type: 'm4p', url: 'assets/upload_file_types/video.png' },
    { type: 'avi', url: 'assets/upload_file_types/video.png' },
    { type: 'wmv', url: 'assets/upload_file_types/video.png' },
    { type: 'mov', url: 'assets/upload_file_types/video.png' },
    { type: 'qt', url: 'assets/upload_file_types/video.png' },
    { type: 'flv', url: 'assets/upload_file_types/video.png' },
    { type: 'swf', url: 'assets/upload_file_types/video.png' },
    { type: 'f4v', url: 'assets/upload_file_types/video.png' },
    { type: 'general', url: 'assets/upload_file_types/generic.png' }
  ];

  public notAllowedFileTypes: any = ['dot','dotm','dotx','docm','ods','xla','xlam','xlsb','xlt','xltm', 'xltx','ppam','pot','potm','ppsx','ppsm','pps','ppa','pptm','tif','tiff','raw','php','java','css', 'js','jsx','py','bat','cmd','apk','msi','xd'];

  private uploadFileSubsciptons: Subscription = new Subscription();

  // private _uuid: string;

  @HostListener("window:beforeunload", ["$event"]) beforeunloadHandler(event: Event) {
    if (this.screenRecordingState === 'started' || this.screenRecordingState === 'paused' || this.screenRecordingState === 'resumed' || this.screenRecordingState === 'uploading') {
      if (confirm('Screen sharing is on, Your changes will be lost')) {
        return true;
      }
      event.returnValue = false;
    }
    if (this.liveStreamState === 'started' || this.liveStreamState === 'initialized-screen-sharing' || this.liveStreamState === 'started-screen-sharing' || this.liveStreamState === 'stopped-screen-sharing') {
      if (confirm('Live stream screen sharing is on, Your changes will be lost')) {
        this.destroyLiveStream();
        return true;
      }
      event.returnValue = false;
    }

    if (this.toCheckFileUploadProgress) {
      if (confirm('File uploading is in progress, changes will be lost')) {
        return true;
      }
      event.returnValue = false;
    }



  }

  @HostListener("window:unload", ["$event"]) unloadHandler(event: Event) {
    console.log('unload', this.liveStreamState);
    console.log(document.location.href.indexOf('/assignment'));


    if (this.liveStreamState === 'started' || this.liveStreamState === 'initialized-screen-sharing' || this.liveStreamState === 'started-screen-sharing' || this.liveStreamState === 'stopped-screen-sharing') {
      this.destroyLiveStream();
      event.returnValue = false;
    }
  }

 

  constructor(private headerService: HeaderService,
    private renderer: Renderer2,
    private router: Router,
    private socketService: SocketService,
    private activatedRoute: ActivatedRoute,
    private toastr: ShowToasterService,
    private appMainService: AppMainService,
    private eventManager: EventManager,
    private windowRef: Window,
    private filestackService: FilestackService,
    private workService: WorkspaceHttpService,
    public detailsService: DetailsHttpService,

  ) {
    this.header_data = this.headerService.getStaticHeaderData();



    this.activatedRoute.queryParams.subscribe(params => {
      if (params.is_account_switched) {
        this.is_account_switched = params.is_account_switched;
      }
    });

    // fire google analytics event on click on analytics router
    const navEndEvents = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
    );
    navEndEvents.subscribe((event: NavigationEnd) => {
      gtag('config', 'UA-77752865-1', {
        'page-path': event.urlAfterRedirects
      });
    });

    router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationStart),
      debounceTime(200)).subscribe(
        (): void => {
          let sessionData: any = this.headerService.getStaticHeaderData();
          if (sessionData && sessionData.account_expired_status && !sessionData.account_expired_status.status) {
            this.toastr.ShowToastr('info',sessionData.account_expired_status.message);
            window.location = sessionData.account_expired_status.url;
          }
          if (document.location.href.indexOf('/canvas/') >= 0) {
            this.showHeader = false;
          }

        }
      );

    let env = window.location.hostname
      .substring(0, window.location.hostname.indexOf("."))
      .toUpperCase();

    if (location.protocol != "https:" && env != "") {
      location.href =
        "https:" +
        window.location.href.substring(window.location.protocol.length);
    }
    this.subscriptions.add(this.headerService.current_lang.subscribe(data => {
      if (data == "es") {
        this.renderer.addClass(document.body, 'language_sp');
      }
    }));

    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => this.translation = languageTranslation

     
      
      )
      
      
      );
     
    this.subscriptions.add(this.headerService.screenRecordingUrl$.subscribe(screenRecordingUrl => this.screenRecordingUrl = screenRecordingUrl));
    this.subscriptions.add(this.headerService.liveStreamData$.subscribe(liveStreamData => this.liveStreamData = liveStreamData));

    this.subscriptions.add(this.headerService.LoggedIn.subscribe(data => {
      this.isLoggedIn = data;
      if (data) {
        this.writeToHead();
        this.colors = this.headerService.getColors();

        let sessionData: any = this.headerService.getStaticHeaderData();
        this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
        // this._uuid = this.headerService.getStaticHeaderData('uudi');

        if (!sessionData.account_expired_status.status) {
          this.toastr.ShowToastr('info',sessionData.account_expired_status.message);
          window.location = sessionData.account_expired_status.url;
        }

        let is_account_switched = this.appMainService.parseCookieValue('is_account_switched');

        if (sessionData.is_account_switched || is_account_switched || this.is_account_switched == 1) {
          this.changed_account_name = sessionData.user_current_account.accounts.company_name;
          this.account_switch_message = sessionData.language_translation.account_switch_message;
          this.account_switch_title = sessionData.language_translation.account_switch_title;
          this.ok_text = sessionData.language_translation.myfile_ok;
          this.accountChange.show();
          this.router.navigate([], { queryParams: { is_account_switched: 0 }, queryParamsHandling: 'merge' });
        }
      }
      else {
        window.location.href = this.baseUrl;
      }
    }));

    this.subscriptions.add(this.headerService.screenRecordingData$.subscribe(screenRecordingData => {
      this.screenRecordingData = screenRecordingData;
      // if ((document.location.href.indexOf('/screen_sharing') < 0) && this.screenSharingVideoData) this.screenSharingStarted = true;
      // else this.screenSharingStarted = false;
    }));

    this.subscriptions.add(this.headerService.screenRecordingState$.subscribe(screenRecordingState => {
      this.screenRecordingState = screenRecordingState;

      if (screenRecordingState === 'started' || screenRecordingState === 'resumed') this.startResumeVideoRecordTimer();
      else if (screenRecordingState === 'paused') this.pauseVideoRecordTimer();
      else if (screenRecordingState === 'stopped' || screenRecordingState === 'cancelled' || screenRecordingState === 'aborted') this.resetVideoRecordTimer();
    }));

    this.subscriptions.add(this.headerService.liveStreamState$.subscribe(liveStreamState => {
      this.liveStreamState = liveStreamState;

      if (liveStreamState === 'started') this.startLiveStreamVideoTimer();
      else if (liveStreamState === 'stopped' || liveStreamState === 'cancelled') this.resetLiveStreamVideoTimer();
    }));

  }

  ngOnInit() {

     console.log('upload status',this.toCheckFileUploadProgress);
    //  console.log('ngoninitfiles',this.files);
    //  console.log('ngoninitarray',this.files);
    this.lastUploadFileType = this.uploadFileTypes.length - 1;
    //console.log('uploadFileTypes',this.lastUploadFileType);
    this.getStreamprogress();
    this.sysRefreash();
    this.callIntervalToCheckLiveStreamStatus();
    if (!(document.location.href.indexOf('/canvas/') >= 0 || document.location.href.indexOf('/analytics_angular/') >= 0)) {
      this.liveStreamStarted();
    }
    this.appMainService.browserAgent = this.detectBrowserAgent();
    this.appMainService.playableAudioExtensions = GLOBAL_CONSTANTS.BROWSER_PLAYABLE_AUDIO_EXTENSION[this.appMainService.browserAgent];
    this.appMainService.LiveStreamPage.subscribe(data => {
      if (this.total_toasters[data.item_id]) {
        this.toastr.ShowToastr('remove',this.total_toasters[data.item_id]);
        delete this.total_toasters[data.item_id];
      }
    });

    this.storeLastTriedToNavigateLink();
   this.fetchFilesToUpload();
    
   

    this.subscriptions.add(this.socketService.pushEventWithNewLogic('user_online_check').subscribe(data => {

      let my_data = {
        account_id : this.userCurrAcc.accounts.account_id,
        user_id : this.userCurrAcc.User.id,
        environment_type : 2
      }
      this.appMainService.setUserOnlineStatus(my_data).subscribe((data: any) => {
 
      });
    }));
  }

  private startResumeVideoRecordTimer() {
    let time = this.headerService.getCurrentVideoRecordTime();
    this.timeInterval = setInterval(() => {
      time++;
      this.headerService.updateVideoRecordTimeSource(time);
    }, 1000);
  }

  private pauseVideoRecordTimer() {
    clearInterval(this.timeInterval);
  }

  private resetVideoRecordTimer() {
    clearInterval(this.timeInterval);
    this.headerService.updateVideoRecordTimeSource(0);
  }

  private startLiveStreamVideoTimer() {
    let time = this.headerService.getLiveStreamVideoTimeSource();
    this.liveStreamTimeInterval = setInterval(() => {
      time++;
      this.headerService.updateLiveStreamVideoTimeSource(time);
    }, 1000);
  }

  private resetLiveStreamVideoTimer() {
    clearInterval(this.liveStreamTimeInterval);
    this.headerService.updateLiveStreamVideoTimeSource(0);
  }
  onRouterOutletActivate(event) {
    if (event instanceof LaunchpadComponent) {
      this.landingpage = true;
    }
    else {
      this.landingpage = false;
    }

  }
  private liveStreamStarted() {
    this.subscriptions.add(this.socketService.pushEventWithNewLogic('global-channel').subscribe(data => {
      if (data.event == "refresh-header-settings") {
        if (data.account_id == this.userCurrAcc.accounts.account_id) {
          if (data.user_id) {
            if (data.user_id == this.userCurrAcc.User.id) this.headerService.getHeaderData().subscribe();
          } else {
            this.headerService.getHeaderData().subscribe(); // update header data aka user data in observable
          }
        }

      }

      else if (data.event == "account-change" && !this.landingpage) {
        // console.log('UUID', data.uuid, this._uuid);
        // if (data.account_id == this.userCurrAcc.accounts.account_id && data.uuid === this._uuid) {
        //   if (data.user_id) {
        //     this.headerService.getHeaderData().subscribe(() => {
        //       location.replace("/");

        //     });
        //   }
        // }

      }

      else {

        var item_id = data.item_id;
        var huddle_id = data.huddle_id;
        var streamer_name = data.streamer_name ? data.streamer_name : 'SomeOne';
        var huddle_name = data.huddle_name ? data.huddle_name : 'Huddle';
        let sessionData = this.headerService.getStaticHeaderData();
        let user_id = sessionData.user_current_account.User.id;
        this.appMainService.check_if_huddle_participant(huddle_id, user_id).subscribe((Pdata: any) => {
          if (Pdata.result && data.data && data.event == "live_stream_started") {
            var htmlForToast = `<div id="flashMessage" class="live_re_msg live_recording_${item_id}" style="cursor: pointer;">
          <a href = "/video_details/live-streaming/${huddle_id}/${item_id}" style="color:#fff;">
          <div class="icon_box_notification"><img src="${environment.baseUrl}/img/recording_live_video.png" /></div>
              <div class="noteificationRight">
                  <p>${streamer_name} ${sessionData.language_translation.is_now_live_in_the_live_streaming} ${huddle_name}</p>
              </div>
          <div class="clear"></div>
          </a>
          </div>`;
            if (data.data.created_by != user_id) {
              var current_toaster = this.toastr.ShowToastr('show',htmlForToast,
                null, {
                enableHtml: true,
                closeButton: true,
                disableTimeOut: true
              });
              this.total_toasters[item_id] = current_toaster.toastId;
            }
          }
          else if (data.event == "live_stream_stopped") {
            if (this.total_toasters[data.item_id]) {
              this.toastr.ShowToastr('remove',this.total_toasters[data.item_id]);
              delete this.total_toasters[data.item_id];
            }
          }
        });
      }
    })
    );
  }

  private writeToHead() {
    let sessionData: any = this.headerService.getStaticHeaderData();
    let user_role = sessionData.user_role;
    let is_in_trial = sessionData.is_in_trial;
    let site_id = sessionData.site_id;
    let full_name =
      sessionData.user_current_account.User.first_name +
      " " +
      sessionData.user_current_account.User.last_name;
    let intercom_app_id = '';

    if (site_id == '2') {
      intercom_app_id = 'ymjv4ih3';
    }
    else {
      intercom_app_id = 'uin0il18';
    }

    let intercomSettings = {
      app_id: intercom_app_id,
      name: full_name,
      email: sessionData.user_current_account.User.email,
      created_at: sessionData.user_created_date_timestamp,
      user_role: user_role,
      is_in_trial: is_in_trial,
      AccountID: sessionData.user_current_account.accounts.account_id
    };
    if (!!(<any>window).Intercom) (<any>window).Intercom("update", intercomSettings);
  }

  private sysRefreash() {
    if (!environment.production) {
      return;
    }
    setInterval(() => {
      if (document.location.href.indexOf('/login') <= 0) {
        this.appMainService.sysRefreash().subscribe((respoonse: any) => {
          if (!respoonse.ok && !(document.location.href.indexOf('/canvas/') >= 0))
            window.open(this.baseUrl + '/users/logout', '_self');

        })
      }
    }, 1000 * 20);
  }

  public hideAccountChange() {
    this.accountChange.hide();
  }

  /**
   * Listen for click event on document and store href for use on build error to navigate to page programatically
   */
  private storeLastTriedToNavigateLink() {
    this.eventManager.addGlobalEventListener('document', 'click', (event: any) => {
      const { localName, href } = event.target;
      if (localName === 'a') this.headerService.lastTriedToNavigateLink = href;
    });
  }

  public screenIsRecording() {
    // console.log(this.screenRecordingState)
    return (this.screenRecordingState === 'started' || this.screenRecordingState === 'paused' || this.screenRecordingState === 'resumed') && (document.location.href.indexOf('/record-screen') < 0);
  }

  public liveStreamScreenSharing() {
    return (this.liveStreamState === 'started' || this.liveStreamState === 'started-screen-sharing' || this.liveStreamState === 'stopped-screen-sharing') && (document.location.href.indexOf('/live-streaming') < 0);
  }

  private destroyLiveStream() {
    const liveStreamData = this.headerService.getLiveStreamDataSource();

    const data = {
      accountID: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id,
      video_id: liveStreamData.videoId,
      huddle_id: liveStreamData.huddleId,
      lang: 'en'
    };

    this.appMainService.noSaveVideo(data).subscribe();


    const postData = {
      accountID: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id,
      LiveRecordingID: liveStreamData.videoId,
      environment_type: 2,
      videoDuration: 0,
      huddleID: liveStreamData.huddleId,
      // filename: this.millicastService.fileName,
      eventType: 4
    };
    this.appMainService.createLiveRecording(postData).subscribe();



    if (this.windowRef['rtcPeerConnection'].connectionState == 'connected') {
      this.windowRef['rtcPeerConnection'].close();

      if (this.windowRef['rtcPeerConnectionSS'] && this.windowRef['rtcPeerConnectionSS'].connectionState == 'connected') {
        this.windowRef['rtcPeerConnectionSS'].close();
      }
    }
  }

  callIntervalToCheckLiveStreamStatus() {
    let intervalId = setInterval(() => {
      //  console.log('liveStreamState',this.liveStreamState);
      if (this.liveStreamState === 'started' || this.liveStreamState === 'initialized-screen-sharing' || this.liveStreamState === 'started-screen-sharing' || this.liveStreamState === 'stopped-screen-sharing') {
        this.checkLiveStreamStatus();
      }
    }, 1000 * 20);
  }

  checkLiveStreamStatus() {
    const liveStreamData = this.headerService.getLiveStreamDataSource();
    const postData = {
      accountID: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id,
      document_id: liveStreamData.videoId,
    };
    this.appMainService.liveStreamStatusCheck(postData).subscribe();
  }
  public checkScreenSharingPreview() {
    return this.screenRecordingData && this.screenRecordingData.params && (document.location.href.indexOf('/screen_sharing') < 0) && (document.location.href.indexOf('/screen-sharing-preview') < 0);
  }

  private detectBrowserAgent(): BrowserAgent {
    // Opera 8.0+
    const isOpera = (!!this.windowRef['opr'] && !!opr.addons) || !!this.windowRef['opera'] || navigator.userAgent.indexOf(' OPR/') >= 0;
    if (isOpera) return 'opera';

    // Firefox 1.0+
    const isFirefox = typeof InstallTrigger !== 'undefined';
    if (isFirefox) return 'firefox';

    // Safari 3.0+ "[object HTMLElementConstructor]"
    const isSafari = /constructor/i.test(this.windowRef['HTMLElement']) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!this.windowRef['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
    if (isSafari) return 'safari';

    // Internet Explorer 6-11
    const isIE = /*@cc_on!@*/false || !!document.documentMode;
    if (isIE) return 'ie';

    // Edge 20+
    const isEdge = !isIE && !!window.StyleMedia;
    if (isEdge) return 'edge';

    // Chrome 1 - 71
    const isChrome = !!this.windowRef['chrome'] && (!!this.windowRef['chrome'].webstore || !!this.windowRef['chrome'].runtime);
    if (isChrome) return 'chrome';

    const isEdgeChromium = isChrome && (navigator.userAgent.indexOf("Edg") != -1);
    if (isEdgeChromium) return 'chromium';
  }

  cancel_old(key) {
    this.uploads[key.key].cancel();
    this.files.forEach((value, index) => {
      if (index == key.key || index === 0) {
        this.files.splice(index, 1);
      }
    });
  }

  cancel(key) {
   // console.log('files come in popup', this.files);
   // console.log('cancel key : ', key);
    this.uploads[key.key].cancel();
    //delete this.files[key.key];
    this.files[key.key].error = `${this.translation.multi_file_upload_status_canceled}`;
    this.files[key.key].progress = 101;
    //this.files[key.key].progress = 100;
    
  }

  retry(key){
    console.log('retry file',key);
    this.files[key.key].error = '';
    this.files[key.key].progress = 0;
    
    let obj = this.globalOjbect;
    this.uploadFileSubsciptons.add(this.filestackService.uploadFiles(this.files[key.key], 'image/jpeg', this.userCurrAcc.accounts.account_id, this.userCurrAcc.User.id, this.files[key.key].name, key.key, this.uploads[key.key]).then((res: any) => {
      console.log("resafterretry", res);

      if(this.files[key.key].progress==0 || this.files[key.key].progress < 100 || this.files[key.key].progress == 100){
        this.files[key.key].progress = 100;
        this.count = this.count + 1;
        this.files[key.key].fileUploadStatus = "done";
      }
     //this.files[key.key].progress = 100;
      
      let file = res
      obj.parent_folder_id = this.files[key.key].parent_folder_id;
      obj.huddle_id = this.files[key.key].huddle_id;
      obj.account_folder_id = this.files[key.key].huddle_id;
      obj.fileStack_handle = file.handle;
      obj.fileStack_url = file.url;
      obj.video_file_name = file.filename;
      obj.stack_url = file.url;
      obj.video_url = file.key;
      obj.video_file_size = file.size;
      obj.video_title = file.filename.split(".")[0];
      if (GLOBAL_CONSTANTS.RESOURCE_UPLOAD_EXTENSIONS.includes('.' + file.filename.split(".")[1].toLowerCase())) {
        this.subscriptions.add(this.workService.uploadResource(obj).subscribe((data: any) => {
          
          this.toastr.ShowToastr('info',`${this.translation.workspace_newresourceuploaded}`)
        }, error => {
            
          console.log('retry error one',error)
            this.count = this.count - 1;
            this.files[key.key].progress = 101;
            this.files[key.key].error = `${this.translation.multi_file_upload_error}`;

        }));
      } else {
        if (file.videoDuration) obj.video_duration = file.videoDuration;
        obj.video_source = (file.video_source) ? file.video_source : GLOBAL_CONSTANTS.RECORDING_SOURCE.FILESTACK;
        obj.millicast_rec_id = (file.millicast_rec_id) ? file.millicast_rec_id : null;
        obj.streamName = (file.streamName) ? file.streamName : null;
       this.subscriptions.add(this.detailsService.uploadVideo(obj).subscribe((data: any) => {
          
          let validAudioType = this.headerService.isAValidAudio(data.data.file_type);
          if (validAudioType == false) {
            this.toastr.ShowToastr(`${this.translation.artifact_newvideouploadedsuccessfully}`);
            
          }
          else {
            this.toastr.ShowToastr(`${this.translation.workspace_new_audio_uploaded}`);
          }
        }, error => {
          console.log('retry error two',error)
          this.count = this.count - 1;
          this.files[key.key].progress = 101;
          this.files[key.key].error = `${this.translation.multi_file_upload_error}`;

          
        }));
      }
    }, error => {
      console.log('error in upload image api: ', error);
      if(!this.files[key.key].error){
        //this.files[key.key].error = error.status;
        this.files[key.key].error = `${this.translation.multi_file_upload_status_canceled}`;
     }
    }, () => {

    }));
   }
  

  pauseUploading(showPopup = true, userSelection = false) {

    console.log('userselection',userSelection);  
    this.leftuploading = 0;
    for (let i = 0; i < this.files.length; i++) {
      if (this.files[i].progress < 100) {
        this.leftuploading = this.leftuploading + 1;
      }
    }
    console.log('left uploading',this.leftuploading);

    if (showPopup && this.leftuploading > 0) {
      this.confirmToCanelUploading.show();
    }

    if(this.leftuploading == 0 && userSelection==false){
      this.loaderDivBox = false;
      this.nfile.arrayTwo = [];
      this.countProgress = {};
      this.count = 0;
      this.filesCount = 0;
      this.toCheckFileUploadProgress = false;
      return;
    }

    for (let i = 0; i < this.files.length; i++) {
     // console.log('this.files.progress', this.files[i].progress);
     // if (this.files[i].progress < 100) {
      //  console.log('upload file key', this.uploads[i]);
        if (userSelection)  {
          if(this.uploads[i].cancel()){
          this.files[i].error = `${this.translation.multi_file_upload_status_canceled}`;
          this.files[i].progress = 101;
        }
      } 
     // }
    }

  }

  yesToCancel() {
    this.pauseUploading(false,true);
    this.confirmCanel = true;
    this.confirmToCanelUploading.hide();

  }

  noToCancel() {
    this.pauseUploading(false, false);
    this.confirmCanel = false;
    this.confirmToCanelUploading.hide();
  }


  public alreadyLoopValue = {};

  public totalResponses:number = 0;

  fetchFilesToUpload() {
   //debugger;
    let me = this;
  
    this.subscriptions.add(me.headerService.uploadFilesList.subscribe((data: any) => {
    // console.log('dataobject',data);
     //return;

     //debugger;
     me.completeUploadedFilesData = data.files.length;
      let totalFiles = 1;
      if(data.files.length > 1){
        totalFiles = data.files.length;
      }
      for (let i = 0; i < data.files.length; i++) {
       
        let item = data.files[i].webkitGetAsEntry();
       
        if (item) {
          //if(data.obj.huddle_id != null && data.obj.parent_folder_id == null){
           
            //console.log('huddle condition');
            //me.scanFiles(item,data.obj.huddle_id);  
          
          //}else{   
          
            console.log('other condition');
            me.scanFiles(item,data.obj.parent_folder_id,data.obj.huddle_id);
          //}
        }

      }
      
      //console.log('arraytow',this.nfile.arrayTwo);

      //return;
      
      let checkFileUploadInterval = setInterval(() => {

        if (this.nfile && 'arrayTwo' in this.nfile && this.nfile.arrayTwo.length > 0) {

          clearInterval(checkFileUploadInterval);

          this.loaderDivBox = true;

          //this.files = Array.from(this.nfile.arrayTwo).concat(Array.from(data.files));

          this.files = Array.from(this.nfile.arrayTwo);

         console.log('this.files',this.files);

          this.globalOjbect = data.obj;
          
          let obj = data.obj

         // console.log('p0bj',this.globalOjbect);

         //return;

         let storekey;

          for (const key in this.files) {

           // console.log('yut',this.files[key]?.checkProgressComplete);
            //storekey = key;
            if (this.files[key]?.progress >= 0 ) continue
            
           // this.toCheckFileUploadProgress = true;
            
            let fileExtension = this.files[key].name.split('.').pop();            
           
            this.uploads[key] = {};
         
            //console.log('this.uploads two',key);
            
            for (let i = 0; i < this.uploadFileTypes.length; i++) {

              if(this.uploadFileTypes[i].type == fileExtension){

                this.files[key].fileTypeUrl = this.uploadFileTypes[i].url;
              }

              if (this.uploadFileTypes[i].type == this.files[key].type) {

                  this.files[key].fileTypeUrl = this.uploadFileTypes[i].url;
         
              } 
            }
        
           // this.files[key].progress = 1;
            this.uploadFileSubsciptons.add(this.filestackService.uploadFiles(this.files[key], 'image/jpeg', this.userCurrAcc.accounts.account_id, this.userCurrAcc.User.id, this.files[key].name, key, this.uploads[key]).then((res: any) => {
              
            // console.log('totalresponses',this.totalResponses++);
             console.log('responsefirsttry',res);
            res.keyIndex = [key];
            console.log('responsefirsttrysec',res);
              this.allResponses.push(key);

              //console.log("resafteruploadallResponses", this.allResponses);
              if(this.files[key].progress==0 || this.files[key].progress < 100 || this.files[key].progress==100){
                this.files[key].progress = 100;
                this.count = this.count + 1;
                this.files[key].fileUploadStatus = "done";
               // this.toCheckFileUploadProgress = false;
              }
             
              let file = res
              this.globalOjbect.parent_folder_id = this.files[key].parent_folder_id;
              this.globalOjbect.huddle_id = this.files[key].huddle_id;
              this.globalOjbect.account_folder_id = this.files[key].huddle_id;
              this.globalOjbect.fileStack_handle = file.handle;
              this.globalOjbect.fileStack_url = file.url;
              this.globalOjbect.video_file_name = file.filename;
              this.globalOjbect.stack_url = file.url;
              this.globalOjbect.video_url = file.key;
              this.globalOjbect.video_file_size = file.size;
              this.globalOjbect.video_title = file.filename.split(".")[0];
              //this.globalOjbect.activity = this.files[key].activity;
              //this.globalOjbect.category = this.files[key].category;
              //this.globalOjbect.totalFiles = this.completeUploadedFilesData;  
              

              if (GLOBAL_CONSTANTS.RESOURCE_UPLOAD_EXTENSIONS.includes('.' + file.filename.split(".")[1].toLowerCase())) {
                //console.log('testfilecount',this.testFilesCount++);
               this.subscriptions.add(this.workService.uploadResource(this.globalOjbect).subscribe((data: any) => {
                 
                  this.toastr.ShowToastr('info',`${this.translation.workspace_newresourceuploaded}`)
                  
                }, error => {

                  this.count = this.count - 1;
                  this.files[key].progress = 101;
                  this.files[key].error = `${this.translation.multi_file_upload_error}`;
                  console.log('database resource error: ', error);

                }));
              } else {
                if (file.videoDuration) this.globalOjbect.video_duration = file.videoDuration;
                this.globalOjbect.video_source = (file.video_source) ? file.video_source : GLOBAL_CONSTANTS.RECORDING_SOURCE.FILESTACK;
                this.globalOjbect.millicast_rec_id = (file.millicast_rec_id) ? file.millicast_rec_id : null;
                this.globalOjbect.streamName = (file.streamName) ? file.streamName : null;
                this.globalOjbect.parent_folder_id = this.files[key].parent_folder_id;
                //this.globalOjbect.huddle_id = this.files[key].huddle_id;
                console.log('testVidesoCount',this.testVidesoCount++);
              this.subscriptions.add(this.detailsService.uploadVideo(this.globalOjbect).subscribe((data: any) => {
                  
                  let validAudioType = this.headerService.isAValidAudio(data.data.file_type);
                  if (validAudioType == false) {
                    this.toastr.ShowToastr('info',`${this.translation.artifact_newvideouploadedsuccessfully}`);
                  }
                  else {
                    this.toastr.ShowToastr('info',`${this.translation.workspace_new_audio_uploaded}`);
                  }
                }, error => {

                  this.count = this.count - 1;
                  this.files[key].progress = 101;
                  this.files[key].error = `${this.translation.multi_file_upload_error}`;

                  console.log('database video error: ', error);

                }));
              }
            }, error => {
              console.log('error in upload image api: ', error);

            
                this.files[key].progress = 101;
              
               if(!this.files[key].error){
                  //this.files[key].error = error.status;
                  this.files[key].error = `${this.translation.multi_file_upload_status_canceled}`;
               }
               
            }, () => {

            }));
          } // loop end here
        }
      }, 300);
      
    }));
  }

  



  scanFiles(item,folder_id,huddle_id) {
    let me = this;
    
    var jsFileObject;
    if (item.isFile) {
      item.file(function (file) {
        
        jsFileObject = file;
        jsFileObject.parent_folder_id = folder_id;
        jsFileObject.huddle_id = huddle_id;
        jsFileObject.fileUploadStatus = "inProgress";
       
       console.log('jsfileobject',jsFileObject);

       console.log('type',jsFileObject.type.split("/")[0].toLowerCase()+'/*');

       //!me.notAllowedFileTypes.includes(jsFileObject.name.split('.').pop())

        if(GLOBAL_CONSTANTS.RESOURCE_UPLOAD_EXTENSIONS.includes('.' + jsFileObject.name.split(".").pop().toLowerCase()) || GLOBAL_CONSTANTS.RESOURCE_UPLOAD_VIDEO_EXTENSIONS.includes('.' + jsFileObject.name.split(".").pop().toLowerCase()) || GLOBAL_CONSTANTS.RESOURCE_UPLOAD_VIDEO_EXTENSIONS.includes(jsFileObject.type.split("/")[0].toLowerCase()+'/*')){

          me.filesCount = me.filesCount + 1;

          if(me.filesCount <= 100){
            me.nfile['arrayTwo'].push(jsFileObject);
          }
          console.log('filecount',me.filesCount);
          
          
          
          
        }else{

          if(me.filesCount >= 100 ){

            me.toastr.ShowToastr('error',`${me.translation.multi_upload_file_exceed_lmit}`);

          }
          if(me.filesCount <= 100)

          //File {file_name} is not an accepted file type
           me.toastr.ShowToastr('error',`${me.translation.vd_file}`+ ' ' + jsFileObject.name + ' ' + `${me.translation.multi_upload_file_not_accepted}`);
         // me.toastr.ShowToastr('error',`${me.translation.workspace_new_audio_uploaded}`);
        }
        
        
      });
    }
    if (item.isDirectory) {
      let directoryReader = item.createReader();
      directoryReader.readEntries(function (entries) {
        entries.forEach(function (entry,value) {
          me.scanFiles(entry,folder_id,huddle_id);
        });
      });
    }
  }

  
  getStreamprogress() {
    this.filestackService.streamUploadFileProgress.subscribe((progress: any) => {
    console.log('getstreamprogress',progress);

      for (const key in this.files[progress.key]) {
        this.files[progress.key].progress = progress.totalPercent

        // if(this.files[progress.key].progress == 100){
        //   this.toCheckFileUploadProgress = false;
        // }else{
          
        // }
        
      }
      //console.log('this.files.stream',this.files);
      for(let i=0; i<this.files.length;i++){
      // console.log('fileprogress',this.files[i].progress);
        if(this.files[i].fileUploadStatus == 'inProgress' )
        {
          this.toCheckFileUploadProgress = true;
        }else{
          this.toCheckFileUploadProgress = false;
        }
      }
    },(error:any) =>{

      console.log('stream error',error);
    }
    );
  }

  deleteFile(data: any) {
    this.uploadFileSubsciptons.unsubscribe();
  }

  formatBytes(bytes, decimals) {
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals || 2;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  ngOnDestroy() {
    console.log('called99999999');
    this.subscriptions.unsubscribe();
    this.uploadFileSubsciptons.unsubscribe();
    this.files = {};
   // this.nfile.arrayTwo = [];
    this.loaderDivBox = false;
      this.nfile.arrayTwo = [];
      this.countProgress = {};
      this.count = 0;
      this.filesCount = 0;
      this.toCheckFileUploadProgress = false;
    //this.headerService.uploadFilesList.unsubscribe();
   // console.log('files',this.files);
    //console.log('nfile',this.nfile.arrayTwo);
  }

}
