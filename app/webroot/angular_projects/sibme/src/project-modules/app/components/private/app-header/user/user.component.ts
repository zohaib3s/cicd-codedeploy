import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import {HeaderService} from '@projectModules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  public header_data;
  public translation;
  public details: any = {};
  public userImage = '';
  alertPrevent = false;
  public userFirstName;
  public userLastName;
  public userEmail;
  public sortKey: string = '';
  public sortTableKey: string = null;
  public userCurrentAccount: any;
  unsubscribe$: Subject<void> = new Subject();

  constructor(private headerService: HeaderService, private toastr: ShowToasterService, private router: Router) {


    this.header_data = this.headerService.getStaticHeaderData();
    this.sortKey = `sortData-${this.header_data.user_current_account.users_accounts.account_id}-${this.header_data.user_current_account.User.id}`
    this.sortTableKey = `sortTable-${this.header_data.user_current_account.users_accounts.account_id}-${this.header_data.user_current_account.User.id}`
    this.translation = this.header_data.language_translation;
    this.headerService.behaviorialHeaderData$.subscribe(headerData => {
      this.details = headerData;        //Updating data after account selected in launchpad component
    });
    this.headerService.userImageSubject.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((data) => {
      this.userImage = data;
    });
    this.headerService.userFirstNameSubject.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((data) => {
      this.userFirstName = data;
    });
    this.headerService.userLastNameSubject.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((data) => {
      this.userLastName = data;
    });
    this.headerService.userEmailSubject.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((data) => {
      this.userEmail = data;
    });
    this.headerService.accountNameSubject.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((data) => {
      this.userCurrentAccount = data;
    });


  }

  ngOnInit() {

  }

  setAlertPreventValue(){
    let userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
    let account_id = userCurrAcc.users_accounts.account_id;
    let user_id = userCurrAcc.User.id;
    let keyPreFix = account_id + '-' + user_id + '-';
    // Remove Goal Sort Data and Sort Header Table If Present In Local Storage.
    if(this.headerService.getLocalStorage(this.sortKey)) {
      this.headerService.removeLocalStorage(this.sortKey)
    }
    if(this.headerService.getLocalStorage(this.sortTableKey)){
      this.headerService.removeLocalStorage(this.sortTableKey)
    }

  if(this.headerService.getLocalStorage('analytics_filter')){
      this.headerService.removeLocalStorage('analytics_filter')
    }

    if(this.headerService.getLocalStorage(keyPreFix+'assessment_report')){
      this.headerService.removeLocalStorage(keyPreFix+'assessment_report')
    }
    if(this.headerService.getLocalStorage(keyPreFix+'huddle_report')){
      this.headerService.removeLocalStorage(keyPreFix+'huddle_report')
    }
    if(this.headerService.getLocalStorage(keyPreFix+'coaching_report')){
      this.headerService.removeLocalStorage(keyPreFix+'coaching_report')
    }
    if(this.headerService.getLocalStorage(keyPreFix+'user_summary_report')){
      this.headerService.removeLocalStorage(keyPreFix+'user_summary_report')
    }
    
    this.router.navigate(['login'], {queryParams: {logout: true}});
    
    // this.headerService.logoutUser().subscribe((logout_res: any) => {
    //   // if(logout_res.success == false){
    //   //   this.toastr.ShowToastr('error',logout_res.message)
    //   // } else {
    //     if(this.headerService.getLocalStorage("auth_token")){        //Removing Auth Token if remember me is selected
    //       this.headerService.removeLocalStorage("auth_token")
    //     }

    //     this.headerService.setalertPrenventValue(true);
        
    //   // }
    // });
  }

  redirectRoute(route){
    // if(route.hard_redirect) window.location.href = route.redirect_to;
    this.router.navigate([route]);
  }

  public getAvatarPath(details) {

    if (details.user_current_account.User.image) {

      return details.avatar_path;

  	} else{
  		return details.base_url+"/img/home/photo-default.png";
  	}

  }

  public SettingsAllowed(details) {

    return (details.user_permissions.roles.role_id == 100 || details.user_permissions.roles.role_id == 110 );

  }
  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
