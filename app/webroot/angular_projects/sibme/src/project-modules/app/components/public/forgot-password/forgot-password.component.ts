import { Component, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from "@app/services";
import { ShowToasterService } from '@projectModules/app/services';

@Component({
  selector: 'forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnDestroy {

  public forgotEmail: any;
  public translation: any;
  public selectedLanguage: any;
  public checkPlatform: any;
  public isLoading = true;
  public translationKeys: any = ["enter_email_fp_placeholder","send_reset_link","reset_password_descritpion","forgot_password"];

  constructor( 
    private router: Router, 
    private headerService: HeaderService,
    private toastr: ShowToasterService) {
      this.toastr.enable_Toastr = 1; // 1 only for login and forgot password.
      //Check if platform is Sibme or HMH
      this.headerService.platformCheckOnLogin.subscribe((data) => {
        this.checkPlatform = data;
      });

      if(this.checkPlatform == '1') {
        if(this.headerService.getLocalStorage("selectedLanguage")) {
          this.selectedLanguage = this.headerService.getLocalStorage("selectedLanguage");
        } else {
          if(window.navigator.language == "es") this.selectedLanguage = window.navigator.language;        //langauge use that is stored in localstorage, otherwise use broweser language
          else this.selectedLanguage = "en";
        }  
      } else {
        this.selectedLanguage = "en";
      }
      this.headerService.getLanguage(this.selectedLanguage, this.translationKeys).subscribe((res: any) => {
        this.translation = res;
        this.isLoading = false;
      });
  }

  ngOnInit(){

  }

  forgotPassword() {
    this.headerService.forgotPassword(this.forgotEmail).subscribe((res: any) => {     
      if(res.success == false){
        this.toastr.ShowToastr('error',res.message)
      } else {
        this.toastr.ShowToastr('success',res.message)
        this.redirectRoute(res); 
      }
    });
  }

  redirectRoute(route){
    if(route.hard_redirect) window.location.href = route.redirect_to;
    else this.router.navigate([route.redirect_to]);
  }

  ngOnDestroy() {
    
  }

}
