import {Component, OnInit, Input} from '@angular/core';
import {HeaderService} from '@app/services';
import { environment } from "@environments/environment";

@Component({
  selector: 'logo-area',
  templateUrl: './logo-area.component.html',
  styleUrls: ['./logo-area.component.css']
})
export class LogoAreaComponent implements OnInit {

  public details;
  public logo_background_color: any;
  public logo_image: any;
  public baseUrl: any;
  constructor(private headerService: HeaderService , ) {
    this.headerService.logo_background_color.subscribe((data) => {
      this.logo_background_color = data;
    });

    this.headerService.logo_image.subscribe((data) => {
      this.logo_image = data; 
    });
  }
  imageUrl() {
    return 'url(' + '"' + this.logo_image + '"' + ')';
  }
  ngOnInit() {
    let project_title = environment.project_title;

    let account_id = this.getParameterByName('account_id');
    let user_id = this.getParameterByName('user_id');

    let base_url = environment.baseHeaderUrl + `/angular_header_api/${project_title}`;

    if (user_id && account_id && document.location.href.indexOf('/canvas/') >= 0) {
      base_url = base_url + `/${account_id}/${user_id}`;
    }
    this.headerService.getHeaderData(base_url).subscribe((header_details: any) => {
      this.headerService.header_background_color.next('#'+header_details.user_permissions.accounts.header_background_color);
      this.details = header_details;
    });
  }

  private getParameterByName(name, url?) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  public load_bg(details): string {
    this.baseUrl = details.base_url;
    if (details && details.base_url && !details.user_current_account.accounts.image_logo) {
      return 'url(' + '"' + details.base_url + details.logo_image + '"' + ')'
    } else {
      return 'url(' + '"' + details.logo_image + '"' + ')'
    }

  }

}
