import { Component, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeaderService } from "@app/services";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnDestroy {

  public userLoginCre = {
    username : null,
    password: null,
    remember_me: false
  }
  public modalRef: BsModalRef;
  public sub = new Subscription();
  public queryParams: any;
  public selectedLanguage: any;
  public ssoEmail: any;
  public translation: any;
  public selectedLanguageLocalStorage: any;
  public header_data: any;
  public checkPlatform: any;
  public showPass: boolean = false;
  public loadLoginData: boolean = false;
  public hidePass: boolean = true;
  public translationKeys: any = ["login_to_sibme","email_placeholder_login","password_placeholder_login","sign_in","forgot_password","remember_me","or_login","sign_in_google","sign_in_sso","by_signing_agreement","privacy_policy_payment","and","terms_of_use_payment","sign_in_model_close"];

  constructor( 
    private router: Router, 
    private headerService: HeaderService,
    private toastr: ShowToasterService,
    private modalService: BsModalService,
    private route: ActivatedRoute 
  ){
    this.toastr.enable_Toastr = 1; // 1 only for login and forgot password.
    this.sub.add(this.route.queryParams.subscribe(params => {
      // Showing Error message and then deleting it
      if(params.error){
        const params = { ...this.route.snapshot.queryParams };
        this.toastr.ShowToastr('error',params.error)
        delete params.error;
        this.router.navigate([], { queryParams: params });
      }
    }))

    //Check if platform is Sibme or HMH
    this.headerService.platformCheckOnLogin.subscribe((data) => {
      this.checkPlatform = data;
    });

    //Check For Language selected in LocalStorage
    this.changeLanguage();
  }

  ngOnInit(){

  }

  changeLanguage(){
    if(this.checkPlatform == '1') {
      this.selectedLanguageLocalStorage = this.headerService.getLocalStorage("selectedLanguage")
      if( this.selectedLanguageLocalStorage ){
        this.selectedLanguage = this.selectedLanguageLocalStorage;
      } else {
        if(window.navigator.language == "es") this.selectedLanguage = window.navigator.language;    //langauge use that is stored in localstorage, otherwise use broweser language
        else this.selectedLanguage = "en";
      }
    } else {
      this.selectedLanguage = "en";
    }
    
    this.headerService.getLanguage(this.selectedLanguage, this.translationKeys).subscribe((res: any) => {
      this.translation = res;
      this.loadLoginData = true;
    });

    //Change Language in header api
    this.headerService.changeLanguageApi(this.selectedLanguage).subscribe((res: any) => {});
  }

  signIn() {
    this.headerService.login_user(this.userLoginCre).subscribe((login_res: any) => {
      if(login_res.success == false){
        this.toastr.ShowToastr('error',login_res.message)
      } else {
        if(this.userLoginCre.remember_me){
          this.headerService.setLocalStorage("auth_token", login_res.data?.authentication_token);   //Setting Auth Token If remember me is true
        }
        this.redirectRoute(login_res);
      }
    });
  }

  redirectRoute(route){
    if(route.hard_redirect) window.location.href = route.redirect_to;
    else this.router.navigate([route.redirect_to]);
  }

  loginBySso(template) {
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }

  showError(){
    this.toastr.ShowToastr('error',"Please fill all the fields")
  }

  languageChange(){
    this.headerService.setLocalStorage("selectedLanguage", this.selectedLanguage)
    this.changeLanguage();
  }

  signInSso(){
    this.modalRef.hide();
    this.headerService.signInBySso(this.ssoEmail).subscribe((res: any) => {
      if(res.success == false){
        this.toastr.ShowToastr('error',res.message)
      } else {
        window.location.href = res?.idp_login_url;
      }
    });
  }

  ngOnDestroy() {
  }

}
