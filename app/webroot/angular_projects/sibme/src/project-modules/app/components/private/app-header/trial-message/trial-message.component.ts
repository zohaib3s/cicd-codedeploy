import { Component, OnInit, Input } from '@angular/core';
import { HeaderService } from "@projectModules/app/services";
import { environment } from "@environments/environment";
import { Subscription } from 'rxjs';

@Component({
  selector: 'trial-message',
  templateUrl: './trial-message.component.html',
  styleUrls: ['./trial-message.component.css']
})
export class TrialMessageComponent implements OnInit {
  public header_data;
  public translation;
  @Input() details;

  constructor(private headerService: HeaderService) {
    this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      
		});
   }

  ngOnInit() {
    this.fetchData();
    this.headerService.isActive.subscribe(res =>{
      this.fetchUpdated();
    })
  }
  fetchData() {
    this.headerService.LoggedIn.subscribe((f) => {

      if (f) {
        this.header_data = this.headerService.getStaticHeaderData();
        this.translation = this.header_data.language_translation;
      }

    })
  }
  fetchUpdated(){
    let project_title = environment.project_title;    
    let base_url = environment.baseHeaderUrl + `/angular_header_api/${project_title}`;
    this.headerService.getHeaderData(base_url).subscribe((header_details:any) => {
      // console.log('trial message component');
      // debugger;
      this.details = header_details;
    })
  }

}
