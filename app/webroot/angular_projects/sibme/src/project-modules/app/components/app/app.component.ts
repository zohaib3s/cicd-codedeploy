import { Component, OnInit } from '@angular/core';
import { Event as RouterEvent, Router, NavigationStart, NavigationEnd, ActivatedRoute } from "@angular/router";
import { filter, debounceTime } from 'rxjs/operators';
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {

  public publicRoute = true;
  public publicParam = true;

  constructor( private router: Router, private activetedRoute: ActivatedRoute) {
    this.activetedRoute.queryParams.subscribe(params => {
      if(params.logout || document.location.href.indexOf('/login') >= 0){
        this.publicParam = true
      } else {
        this.publicParam = false
      }
    });
    router.events.pipe(
      filter((event: RouterEvent) => event instanceof NavigationEnd),
      debounceTime(800)).subscribe(
        (): void => {
          if (document.location.href.indexOf('/login') >= 0 || document.location.href.indexOf('/forgot_password') >= 0) {
            this.publicRoute = true;
            this.publicParam = true;
          } else {
            this.publicRoute = false;
            this.publicParam = false;
          }   
        }
      );
  }

  ngOnInit() {
    if (document.location.href.indexOf('/login') >= 0 || document.location.href.indexOf('/forgot_password') >= 0) {
      this.publicRoute = true;
      this.publicParam = true;
    } else {
      this.publicRoute = false;
      this.publicParam = false;
    }
  }

}
