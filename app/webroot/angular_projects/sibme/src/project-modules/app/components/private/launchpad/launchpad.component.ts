import { Component, OnInit, OnDestroy, ViewChild, forwardRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '@src/environments/environment';
import { HeaderService } from '@src/project-modules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import { AppHeaderComponent } from '../..';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-launchpad',
  templateUrl: './launchpad.component.html',
  styleUrls: ['./launchpad.component.css']
})
export class LaunchpadComponent implements OnInit, OnDestroy {
  @ViewChild(forwardRef(() => AppHeaderComponent))appHeaderComponent : AppHeaderComponent;
 // @ViewChild(AppHeaderComponent) appHeaderComponent : AppHeaderComponent;
  public checkData: boolean = false;
  public allAccounts: any = [];
  public currentComanyName: any = [];
  public header_data: any;
  public sub = new Subscription();
  public translation: any;
  public checkPlatform: any;
  searchCompanyname;
  public project_title = environment.project_title;
  unsubscribe$: Subject<void> = new Subject();
  public noAccount = true;

  constructor(
    private headerService: HeaderService,
    private toastr: ShowToasterService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    setTimeout(() => {
      this.checkData = true;
      this.sub.add(this.route.queryParams.subscribe(params => {
        // Showing Error message and then deleting it
        if(params.error){
          const params = { ...this.route.snapshot.queryParams };
          this.toastr.ShowToastr('error',params.error)
          delete params.error;
          this.router.navigate([], { queryParams: params });
        }
      }))
    }, 800);

    this.header_data = this.headerService.getStaticHeaderData();
    this.translation = this.header_data?.language_translation;           //Get Translation

    //Check if platform is Sibme or HMH
    this.headerService.platformCheckOnLogin.subscribe((data) => {
      this.checkPlatform = data;
    });
    
    this.headerService.accountNameSubject.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((data) => {
      this.currentComanyName = data;
    });
  }

  ngOnInit(): void {
    this.headerService.getLaunchpadData().subscribe((res: any) => {
      // this.allAccounts = res;
      if(res.redirect_to){
        this.router.navigate([res.redirect_to]);
      } else {
        res.forEach((value, key) => {
          if(!value.accounts.is_suspended){
            var obj = {};
            obj["user_id"] = value.User.id;
            obj["account_id"] = value.accounts.account_id;
            obj["image_logo"] = value.accounts.image_logo;
            obj["company_name"] = value.accounts.company_name;
            obj["role_id"] = value.roles.role_id;
            this.allAccounts.push(obj);
            this.noAccount = false;
          }
        });
      }
    });
  }

  getAccountData(account_id, user_id): void {
    this.headerService.getLaunchpadAccount(account_id).subscribe((res: any) => {
      if(res.success == false){
        this.toastr.ShowToastr('error',res.message)
      } else {
        this.updateHeaderData(account_id, user_id, res);    //  Updating header api data after selecting a account
      }
    });
  }

  updateHeaderData(account_id, user_id, res){
   
    let base_url = environment.baseHeaderUrl + `/angular_header_api/${this.project_title}`;
    base_url = base_url + `/${account_id}/${user_id}`;
    this.headerService.getHeaderData(base_url).subscribe((header_details: any) => {
      this.redirectRoute(res); 
    });
  }

  redirectRoute(route){
    if(route.hard_redirect) window.location.href = route.redirect_to;
    else this.router.navigate([route.redirect_to]);
  }

  ngOnDestroy() {
    
  }

}
