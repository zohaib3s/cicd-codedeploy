import {Component, OnInit, Input, OnDestroy} from '@angular/core';
import {HeaderService} from '@projectModules/app/services';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'account-information',
  templateUrl: './account-information.component.html',
  styleUrls: ['./account-information.component.css']
})

export class AccountInformationComponent implements OnInit, OnDestroy {

  public details: any = {};
  public translations;
  public userCurrentAccount: any;
  unsubscribe$: Subject<void> = new Subject();

  constructor(private headerService: HeaderService) {
    this.headerService.accountNameSubject.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((data) => {
      this.userCurrentAccount = data;
    });

    this.headerService.behaviorialHeaderData$.subscribe(headerData => {
      this.details = headerData;        //Updating data after account selected in launchpad component
    });
  }

  ngOnInit() {
    let data = this.headerService.getStaticHeaderData();
    this.translations = data.language_translation;
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
  public change_account():void{

  }
 

}
