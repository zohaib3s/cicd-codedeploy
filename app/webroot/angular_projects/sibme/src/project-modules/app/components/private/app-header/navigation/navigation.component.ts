import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Event as RouterEvent, Router, NavigationEnd } from "@angular/router";
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { HeaderService } from "@projectModules/app/services";
import { GoogleAnalyticsEventsService } from '@src/project-modules/app/services/google-analytics-events.service';
import { GLOBAL_CONSTANTS } from '@constants/constant';

declare var ga;

@Component({
  selector: 'navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit, OnDestroy {
  public navItems = GLOBAL_CONSTANTS.NAV_ITEMS;
  private USER_ROLES = GLOBAL_CONSTANTS.USER_ROLES;
  public selectedNavItem: string = '';
  public translation;
  public checkLibraryPermission: any;
  public checkAnalyticsPermission: any;
  public details: any = {};
  public checkAccountAnalyticsPermission: any;
  unsubscribe$: Subject<void> = new Subject();

  public enableGoals: boolean;
  public goalAltName: string;

  constructor(private headerService: HeaderService, private router: Router, private googleAnalyticsEventsService: GoogleAnalyticsEventsService) {

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        ga('set', 'page', event.urlAfterRedirects);
        ga('send', 'pageview');
      }
    });

    // update selected nav item before the router events changed i.e on first load of app
    this.updateSelectedNavItem(router.url);

    this.router.events.subscribe((event: RouterEvent): void => {
      if (event instanceof NavigationEnd) {
        // update selected nav item when the router events changed i.e user navigate from one module to another or internal module routing
        this.updateSelectedNavItem(event.urlAfterRedirects);
      }
    }
    );

    this.headerService.libraryPermissionSubject.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((data) => {
      this.checkLibraryPermission = data;
    });
    this.headerService.analyticsPermissionSubject.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((data) => {
      this.checkAnalyticsPermission = data;
    });

    this.headerService.accountAnalyticsPermissionSubject.pipe(
      takeUntil(this.unsubscribe$)
    ).subscribe((data) => {
      this.checkAccountAnalyticsPermission = data;
    });

    this.headerService.behaviorialHeaderData$.subscribe(headerData => {
      this.enableGoals = Boolean(Number(headerData.user_current_account.accounts.enable_goals)) && headerData.user_current_account.roles.role_id != this.USER_ROLES.VIEWER.ID; // goals are enables for current account AND current user is not viewer
      this.goalAltName = this.headerService.getGoalsAltName(headerData.goal_settings.goal_alt_name);
      this.details = headerData;        //Updating data after account selected in launchpad component
    });

  }

  public submitEvent() { // event fired from home.component.html element (button, link, ... )
    this.googleAnalyticsEventsService.eventEmitter("sibme-analytics/home", "sibme-analytics", "analytics", "click", 10);
  }

  ngOnInit() {
    this.translation = this.headerService.getStaticHeaderData('language_translation');
  }

  public isAllowedPeople() {
    if (!this.details) {
      return false;
    } else {

      let roleId = this.GetRoleId();

      if (roleId == 100 || roleId == 110) {
        return true;
      } else if (roleId == 120 && this.details.user_current_account.users_accounts.permission_administrator_user_new_role == '1') {

        return true;

      } else if (roleId == 115 && this.details.user_current_account.users_accounts.permission_administrator_user_new_role == '1') {

        return true;

      } else {

        return false;


      }


    }

  }

  private GetRoleId() {

    return this.details.user_current_account.roles.role_id;

  }

  /**
   * Update the selectedNavItem based on current base url
   * @param {currentUrl: string}
   */
  private updateSelectedNavItem(currentUrl: string) {
    if (currentUrl.indexOf('workspace/') > 0 || currentUrl.indexOf('workspace_video/') > 0 || (currentUrl.indexOf('video_details/') > 0 && currentUrl.indexOf('workspace') > 0)) {
      this.selectedNavItem = this.navItems.WORKSPACE;
    } else if (currentUrl.indexOf('video_huddles/') > 0 || (currentUrl.indexOf('video_details/') > 0 && currentUrl.indexOf('workspace') < 0)) {
      this.selectedNavItem = this.navItems.HUDDLE;
    } else if (currentUrl.indexOf('goals/') > 0) {
      this.selectedNavItem = this.navItems.GOALS;
    } else if (currentUrl.indexOf('analytics_angular/') > 0) {
      this.selectedNavItem = this.navItems.ANALYTICS;
    } else if (currentUrl.indexOf('people') > 0) {
      this.selectedNavItem = this.navItems.PEOPLE;
    } else if (currentUrl.indexOf('VideoLibrary') > 0 || currentUrl.indexOf('library_video') > 0) {
      this.selectedNavItem = this.navItems.LIBRARY;
    } else {
      this.selectedNavItem = '';
    }
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
