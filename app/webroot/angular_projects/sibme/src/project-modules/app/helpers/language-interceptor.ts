import { Injectable } from "@angular/core";
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from "@angular/common/http";
import { Observable } from "rxjs";
import { HeaderService } from "@projectModules/app/services";

@Injectable()
export class LanguageInterceptor implements HttpInterceptor {
    constructor(private headerService: HeaderService) { }
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        let lang = 'en';
        const sessionData = this.headerService.getStaticHeaderData();

        if (this.headerService.getLocalStorage('selectedLanguage')) {lang = this.headerService.getLocalStorage('selectedLanguage');}

        if (sessionData && sessionData.language_translation && sessionData.language_translation.current_lang) {
            lang = sessionData.language_translation.current_lang;
        }

        if (!request.url.includes('millicast.com')) {
            const updatedRequest = request.clone({
                headers: request.headers.set('current-lang', lang)
            });
            return next.handle(updatedRequest);
        } else { return next.handle(request); }
    }
}