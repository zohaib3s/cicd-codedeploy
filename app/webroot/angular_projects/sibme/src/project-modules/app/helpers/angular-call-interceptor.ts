import { Injectable } from "@angular/core";
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from "@angular/common/http";
import { Observable } from "rxjs";
import { HeaderService } from "@projectModules/app/services";

@Injectable()
export class AngularCallInterceptor implements HttpInterceptor {
    constructor() { }
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {         //Intercepter to add a check if call is from angular side
        // let updatedRequest = request.clone({});
        // console.log("angular-request");
        
        if (!request.url.includes('millicast.com')) {
            let updatedRequest = request.clone({
                headers: request.headers.set("Angular-Request", "true")
            });
            return next.handle(updatedRequest);
        } else { return next.handle(request); }
    }
}