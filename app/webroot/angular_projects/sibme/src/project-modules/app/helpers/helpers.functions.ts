/**
 * Check if array, object or string is empty
 * @param value array | object | string
 */
export function isEmpty(value: [] | object | string) {
    if (!value) return true;
    else if (typeof value === 'object' || typeof value === 'string') return Object.keys(value).length === 0;
    else throw new Error('Invalid value to check isEmpty, Consider checking arrays, objects and strings');
}