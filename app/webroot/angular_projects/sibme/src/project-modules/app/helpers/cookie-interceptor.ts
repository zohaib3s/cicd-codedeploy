import { Injectable } from "@angular/core";
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from "@angular/common/http";
import { Observable } from "rxjs";
import { HeaderService } from "@projectModules/app/services";

@Injectable()
export class CookieInterceptor implements HttpInterceptor {
    constructor(private headerService: HeaderService) { }
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {         //Intercepter to add Auth Token in every call if user click on remember me
        let auth_token = this.headerService.getLocalStorage("auth_token");
        let updatedRequest = request.clone({});
        if(auth_token) {
            updatedRequest = request.clone({
                headers: request.headers.set("Authentication-Token", auth_token)
            });
        }
        return next.handle(updatedRequest)
    }
}