export class MillicastModel {
  public jwt: string = null;
  public wsUrl: string = null;
  public iceServers: any[] = [];
  public fileName: string = null;
  public streamName: string = null;
  public streamNameSS: string = null;
  public currentSSNumber: number = 0;
}