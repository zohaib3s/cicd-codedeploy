import { Component, OnInit, ViewChild, HostListener, OnDestroy,ElementRef, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { ShowToasterService } from '@projectModules/app/services';
import * as _ from "underscore";

import { SocketService, HeaderService, AppMainService, HomeService } from '@app/services';
import { WorkspaceHttpService } from '@workspace/services';
import { ModalDirective, BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { URLArtifactModalComponent, RecordVideoModelComponent, MillicastRecordVideoModelComponent, ActivityModelComponent } from "@shared/modals";

import { SharedFolderModal } from '@src/project-modules/shared/components/list/rename-folder-modal/rename-folder-modal.component'
import { environment } from "@environments/environment";
import { DisplayStyle, PageType } from '@app/interfaces';
import { ConfirmationDialogComponent } from '@src/project-modules/shared/modals/confirmation-dialog/confirmation-dialog.component'
import { ShareFolderModalComponent } from '@src/project-modules/shared/modals/share-folder-modal/share-folder-modal.component';
import { BrowserAgent } from "@app/types";
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import { DndDropEvent } from 'ngx-drag-drop';
import { NewfolderService } from '@src/project-modules/workspace/services/newfolder.service';
import { isConstructorDeclaration } from 'typescript';

@Component({
  selector: 'workspace-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChild('syncnotes', { static: false }) syncnotes: ModalDirective;

  public browserAgent: BrowserAgent;
  public pageType: PageType = 'workspace-page';

  public changeBackgroundColor:boolean = false;
  public changeBackgroundColorArtifact:boolean = false;
  public dragFolderId:any;

  bsModalRef: BsModalRef;
  public toggleStyleView = 'grid';
  public resourceFilter;
  public searchString = '';
  private searchInput: Subject<string> = new Subject();
  sort: any;
  page: number = 1;
  user_id: any = '';
  account_id: any = '';
  Loadings: any = {};
  artifacts: any = [];
  total_artifacts: any;
  bysearch: any;
  sessionData: any;
  activities_request_ref: any;
  noRecorFound: boolean;
  public socket_listener: any;
  NewVideos: any = [];
  NewVideosTime: NodeJS.Timer;
  public translation: any = {};
  private subscriptions: Subscription = new Subscription();
  total_artifactsload: boolean = true;
  showSkeleton = true;
  public displayStyle: DisplayStyle;
  public additionalData: any = {};

  public ready_for_download: any;
  public file_type: any;
  public artifact_moved_count: number = 0;
  filterIconHeight: number;
  public userAccountLevelRoleId: number | string = null;

  public screenSharingStarted: boolean = false;
  public liveStreamStarted: boolean = false;

  public containerInnerHeight: number = null;
  private headerAndBreadCrumbHeight: number = 94;
  public modalRef;
  public foldersExpanded: boolean;
  public folders: any = []
  public folderId;
  public bcFolders: any = [];

  public valueEmittedFromChildComponent: boolean;

  public workspace_header_title: string = "";

  public counter:number = 0;

  public checkProgressComplete:boolean = false;

 @HostListener("window:scroll", ["$event"])

 
  //el: ElementRef
  onScroll() {
    const scrollOnHeight = window.innerHeight + window.pageYOffset >= document.body.offsetHeight + 20;
    if (scrollOnHeight && !this.Loadings.isNextPageLoading && this.total_artifacts > this.artifacts.length) { // scroll reached on bottom
      this.LoadNextPage(true);
    }
  }

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private workService: WorkspaceHttpService, private headerService: HeaderService,
    private toastr: ShowToasterService, private socketService: SocketService, private modalService: BsModalService, private appMainService: AppMainService,
    private newfolderService: NewfolderService, private homeService: HomeService, private el:ElementRef) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    }));

    this.subscriptions.add(this.headerService.screenRecordingState$.subscribe(screenRecordingState => {
      if (screenRecordingState === 'started' || screenRecordingState === 'paused' || screenRecordingState === 'resumed') {
        this.screenSharingStarted = true;
      } else {
        this.screenSharingStarted = false;
      }
    }));

      this.subscriptions.add(this.headerService.liveStreamState$.subscribe(liveStreamState => {
      if (liveStreamState === 'started' || liveStreamState === 'started-screen-sharing' || liveStreamState === 'stopped-screen-sharing') this.liveStreamStarted = true;
      else this.liveStreamStarted = false;
    }));

    this.browserAgent = this.appMainService.browserAgent;
    this.filterIconHeight = (this.browserAgent === 'ie') ? 30 : 25;

  }

  

  ngOnInit() {


    console.log('translation',this.translation);
  //   window.onbeforeunload = function(event) {
  //     // do some stuff here, like reloading your current state
  //     //this would work only if the user chooses not to leave the page
  //     alert('are you ok');
  // }

    // console.log('test');
    // debugger;
    this.sessionData = this.headerService.getStaticHeaderData();
    if(Object.keys(this.sessionData).length != 0 && this.sessionData.constructor === Object) {
      this.userAccountLevelRoleId = this.sessionData.user_permissions.roles.role_id;
      this.SubscribeSearch();
      this.activatedRoute.params.subscribe((p) => {
      this.folderId = p.folderId;
      if(this.folderId) {
        this.page = 1;
        this.LoadNextPage(false);
      }
    });
    this.activatedRoute.queryParams.subscribe(params => {
      let searchchecker = params.search

      if (searchchecker != undefined) {
        this.bysearch = true
        this.artifact_moved_count = 0;
      } else {
        this.bysearch = false
      }
      this.page = 1;
      var url = this.router.url.split("/");

      if(this.folderId) {
        let r = url[url.length - 2].split('?');
        this.toggleStyleView = r[0];
      } else {
        let r = url[url.length - 1].split('?');
        this.toggleStyleView = r[0];
      }

      if (params.resource) {
        (this.resourceFilter = params.resource);
        this.folders = [];

        this.artifact_moved_count = 0;
       } else  (this.resourceFilter = 0);
      params.search
        ? (this.searchString = params.search)
        : (this.searchString = "");
      params.sort
        ? (this.sort = params.sort)
        : (this.sort = "");
      if (this.page) {
        this.artifacts = []
        this.workService.setArtifact([]);
        this.total_artifacts = 0;
        this.workService.setTotalAritifactCount(0);
      }
      this.LoadNextPage(false);
      this.workService.getArtifact().subscribe(d => {
        this.artifacts = d
      })
    });
      this.activatedRoute.params.subscribe((p) => {
      this.displayStyle = p.displayStyle;
      if(p.folderId) {
        this.toggleStyleView = p.displayStyle
        // console.log(p.displayStyle)
      }
      });
      this.workService.count_emmiter.subscribe((count: any) => {
        this.total_artifactsload = true
        this.total_artifacts = count;
        this.total_artifactsload = false
      });
      this.workService.resourceToOpen.subscribe(artifact => {
        this.FileClicked("td", artifact);
      });
      this.checkValidRoute(this.displayStyle)
      let channel_name = `workspace-${this.sessionData.user_current_account.users_accounts.account_id}-${this.sessionData.user_current_account.users_accounts.user_id}`;

      console.log('channel',channel_name);


      this.socket_listener = this.socketService.pushEventWithNewLogic(channel_name).subscribe(data => this.processEventSubscriptions(data));

      if(window.innerHeight) {
        this.containerInnerHeight = window.innerHeight - this.headerAndBreadCrumbHeight;
      }
    }
    let toggleSetting =JSON.parse(localStorage.getItem('foldersExpanded'));
		toggleSetting!=undefined ? this.foldersExpanded = toggleSetting : this.foldersExpanded=true;
  }

  openNewFolderModal() {
    this.bsModalRef = this.modalService.show(SharedFolderModal, {ariaDescribedby: 'my-modal-description',ariaLabelledBy: 'my-modal-title'});
    this.bsModalRef.content.folderId = this.folderId;
  }

  folderEdit(event, folderName) {
    this.bsModalRef = this.modalService.show(SharedFolderModal, {ariaDescribedby: 'my-modal-description',ariaLabelledBy: 'my-modal-title'});
    this.bsModalRef.content.parentFolderId = event.id;
    this.bsModalRef.content.NewFolderName = folderName;
    this.bsModalRef.content.folderId = event.parentFolderId;
  }

  openShareFolderModal(folderId) {
    this.bsModalRef = this.modalService.show(ShareFolderModalComponent, { class: 'share-modal-container',backdrop:'static' })
  }

  ngOnChanges(){

    console.log('emitvalue',this.valueEmittedFromChildComponent)

  }
  // parentEventHandlerFunction(valueEmitted){
  //   this.valueEmittedFromChildComponent = valueEmitted;
  //   console.log('value',this.valueEmittedFromChildComponent);
  //   this.changeBackgroundColor = false;
  // }

  changeBackground(id,event){
      event.preventDefault();

      console.log('folderevent',event);

      let catchEvent = event.dataTransfer.effectAllowed;

     // console.log('valueemitted',this.valueEmittedFromChildComponent);

      if(event._dndDropzoneActive === true && catchEvent == 'all'){

        this.changeBackgroundColor = true;
        this.dragFolderId = id;
        //console.log('albertactive');
      }
//      console.log('start',event);
     
  
  }

  changeDefaultBackground(event){

    event.preventDefault();
      
      if(!event._dndDropzoneActive){
        this.changeBackgroundColor = false;
      }
  }

 

  // changeBackgroundArtifacts(){
  //   this.changeBackgroundColorArtifact = true;
  //   console.log('artifact gb',this.changeBackgroundColorArtifact);
  // }

  toTest(){
    console.log('mouse leave');
  }

  originalBackground(){
    console.log('function class');
    //this.changeBackgroundColor = false;
  }

  onDrop(event: DndDropEvent, target: any) {

    console.log('totest123');

    this.changeBackgroundColor = false;

    let x: any = this.headerService.getStaticHeaderData()
    let obj = {
			folder_id: target.id,
      object_id: event.data.doc_id, //Artifacts id
      site_id: 1,
      account_id: target.account_id,
      account_folder_id: target.account_folder_id,
      workspace: 1,
      user_id: x.user_current_account.users_accounts.user_id
    }
    this.newfolderService.moveDocumentFolder(obj).subscribe((data: any) => {
      if(data.success) {
        this.toastr.ShowToastr('info',this.translation.df_artifact_moved_to_folder);
        this.artifact_moved_count++;
      } else {
        this.toastr.ShowToastr('info',data.message);
      }
    })
  }



  // uploadFiles(event){
  //   debugger;
  //   var files;
  //   event.preventDefault();
  //   var data = event.dataTransfer.items;
  //   files = data;
  //   let that = this;
  //   let obj: any = {};
  //   obj.user_current_account = that.sessionData.user_current_account;
  //   obj.account_folder_id = null;
  //   obj.huddle_id = null;
  //   // filestack handle key added
  //   // filestack url added
  //   obj.account_id = that.sessionData.user_current_account.accounts.account_id;
  //   obj.site_id = that.sessionData.site_id;
  //   obj.user_id = that.sessionData.user_current_account.User.id;
  //   obj.current_user_role_id = that.sessionData.user_current_account.roles.role_id;
  //   obj.current_user_email = that.sessionData.user_current_account.User.email;
  //   obj.suppress_render = false;
  //   obj.suppress_success_email = false;
  //   obj.workspace = this.pageType=='workspace-page'?true : false;
  //   obj.activity_log_id = "";
  //   obj.video_id = "";
  //   obj.direct_publish = true;
  //   obj.url_stack_check = 1;
  //   obj.parent_folder_id = this.folderId ? this.folderId : 0
  //   this.headerService.UpdateUploadFilesList({files, obj})

  //   if(this.huddle_id_parent) {
  //     obj.huddle_id = this.huddle_id_parent;
  //   }
  // }

  dragstart_handler(ev){
    console.log('ev',ev);
  }

  uploadFiles(event, folder){
    //return;
    this.changeBackgroundColor = false;
  //   console.log('this.backgroundcolor', this.changeBackgroundColor);
  //   console.log('view');
  // console.log('this.userAccountLevelRoleId', this.userAccountLevelRoleId);
  //  console.log('this.sesseiondata.user_permission',this.sessionData.user_permissions.UserAccount.permission_video_workspace_upload);

   if(this.userAccountLevelRoleId != 125 && ((this.userAccountLevelRoleId == 120 && this.sessionData.user_permissions.UserAccount.permission_video_workspace_upload == 1) || this.userAccountLevelRoleId != 120)){

   // console.log('permission access');

   }else{

    this.toastr.ShowToastr('error', 'You do not have permissions to upload the files');
     //alert('You dont have permissions to upload the files');

     return false;

   }

    
    //debugger;
    //console.log('eventfiles',event);
   // return;
   
    var files;
    event.preventDefault();
    var data = event.dataTransfer.items;
   //console.log('data',event);
  // return;
    files = data;
   // console.log('files',files);

    //return;
    let that = this;
    let obj: any = {};
    obj.user_current_account = that.sessionData.user_current_account;
    obj.account_folder_id = null;
    obj.huddle_id = null;
    // filestack handle key added
    // filestack url added
    obj.account_id = that.sessionData.user_current_account.accounts.account_id;
    obj.site_id = that.sessionData.site_id;
    obj.user_id = that.sessionData.user_current_account.User.id;
    obj.current_user_role_id = that.sessionData.user_current_account.roles.role_id;
    obj.current_user_email = that.sessionData.user_current_account.User.email;
    obj.suppress_render = false;
    obj.suppress_success_email = false;
    obj.workspace = this.pageType=='workspace-page'?true : false;
    obj.activity_log_id = "";
    obj.video_id = "";
    obj.direct_publish = true;
    obj.url_stack_check = 1;
    obj.parent_folder_id = folder.id

    console.log('homeobject',obj);
   console.log('files from home page',files);

   // return;
    this.headerService.UpdateUploadFilesList({files, obj})
  }

  // uploadFiles(files, folder){
  //   debugger;
  //   console.log('this is folder');
  //   let that = this;
  //   let obj: any = {};
  //   obj.user_current_account = that.sessionData.user_current_account;
  //   obj.account_folder_id = null;
  //   obj.huddle_id = null;
  //   // filestack handle key added
  //   // filestack url added
  //   obj.account_id = that.sessionData.user_current_account.accounts.account_id;
  //   obj.site_id = that.sessionData.site_id;
  //   obj.user_id = that.sessionData.user_current_account.User.id;
  //   obj.current_user_role_id = that.sessionData.user_current_account.roles.role_id;
  //   obj.current_user_email = that.sessionData.user_current_account.User.email;
  //   obj.suppress_render = false;
  //   obj.suppress_success_email = false;
  //   obj.workspace = this.pageType=='workspace-page'?true : false;
  //   obj.activity_log_id = "";
  //   obj.video_id = "";
  //   obj.direct_publish = true;
  //   obj.url_stack_check = 1;
  //   obj.parent_folder_id = folder.id
  //   this.headerService.UpdateUploadFilesList({files, obj})
  // }

  public ToggleFoldersExpand(flag) {
		this.foldersExpanded = flag;
		localStorage.setItem('foldersExpanded',flag);
	}

  OnFolderDelete(data) {
    this.bsModalRef = this.modalService.show(ConfirmationDialogComponent, {ariaDescribedby: 'my-modal-description',ariaLabelledBy: 'my-modal-title'});
    this.bsModalRef.content.folderData = data;
    this.bsModalRef.content.title = data.title
  }

  showModal(value): void {
    switch (value) {
      case 'sync_notes':
        this.syncnotes.show();
        break;
    }
  }

  hideModal(value) {
    switch (value) {
      case 'sync_notes':
        this.syncnotes.hide();
        break;
    }
  }

  public OnSearchChange(event) {
    this.searchInput.next(event);

  }
  private SubscribeSearch() {
    this.searchInput
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(value => {

        if (this.searchString) {
          this.router.navigate([], {
            queryParams: { search: this.searchString }, queryParamsHandling: 'merge'
          });
        }
        else {
          this.router.navigate([], {
            queryParams: { search: null }, queryParamsHandling: 'merge'
          });
        }

        this.search();

      });
  }

  public search() {
    this.workService.Loadings.IsLoadingArtifacts.emit(true);
    this.LoadNextPage(false);
  }
  GetArtifacts() {

    this.artifacts = []
    this.workService.setArtifact([]);
    this.total_artifacts = 0;
    let x: any = this.headerService.getStaticHeaderData()
    this.user_id = x.user_current_account.users_accounts.user_id;
    this.account_id = x.user_current_account.users_accounts.account_id;
    let obj = {
      account_id: this.account_id,
      user_id: this.user_id,
      title: this.searchString,
      page: this.page,
      sort: this.sort,
      doc_type: this.resourceFilter,
      artifact_moved_count: this.artifact_moved_count,
      with_folders : 1
    }
    if (this.activities_request_ref) {
      this.activities_request_ref.unsubscribe();
    }
    this.activities_request_ref = this.workService.getWorkspaceArtifects(obj).subscribe((d: any) => {
      // debugger;
      // console.log(obj);
      this.workService.setArtifact(d.data);
      this.workService.setTotalAritifactCount(d.total_records);
    }, err => {
    })
    this.workService.Loadings.IsLoadingArtifacts.emit(false);
  }

  public getBreadCrumbs(data: any) {

      if (data.success === -1) {
        this.toastr.ShowToastr('info',data.message);

        setTimeout(() => {
          this.router.navigate(['/']);
        }, 4000);
        this.homeService.updateBreadcrumb(data.breadcumbs);

      }
      else {
        this.bcFolders = data.breadcumbs;
        const folders = data.breadcumbs;

        if (folders && folders?.length > 0) {
        folders.push({
          folder_name: this.translation.artifacts_breacrum,
          folder_id: folders[0].folder_id,
          isCustom: true
        });
        }
        this.homeService.updateBreadcrumb(folders);

      }
  }

  public LoadNextPage(increment?) {
    //debugger
    if (increment) this.page++;
    this.workService.Loadings.IsLoadingArtifacts.emit(true);
    this.Loadings.isNextPageLoading = true;
    let x: any = this.headerService.getStaticHeaderData()
    this.user_id = x.user_current_account.users_accounts.user_id;
    this.account_id = x.user_current_account.users_accounts.account_id;
    let obj: any = {
      account_id: this.account_id,
      user_id: this.user_id,
      title: this.searchString,
      page: this.page,
      sort: this.sort,
      doc_type: this.resourceFilter,
      artifact_moved_count: this.artifact_moved_count,
      with_folders : 1
    };
    if(this.folderId) {
      obj.parent_folder_id = this.folderId
    }
    if (this.activities_request_ref) {
      this.activities_request_ref.unsubscribe();
    }
    this.activities_request_ref = this.workService.getWorkspaceArtifects(obj).subscribe((d: any) => {
      this.getBreadCrumbs(d);
      this.Loadings.isNextPageLoading = false;
      this.showSkeleton = false;
      if (this.page == 1) {
        this.workspace_header_title = d.title_for_header=="" ? this.translation.workspace_myworkspace : d.title_for_header;
        if (!d.success) {
          this.toastr.ShowToastr('info',this.translation.u_dont_permission_vd);
          setTimeout(() => location.href = environment.baseUrl, 1000);
          return;
        }
        this.total_artifactsload = true
        this.total_artifacts = d.total_records;
        this.total_artifactsload = false;
        this.workService.setArtifact(d.data);
        //console.log('folders',d.folders)
        d.folders ? this.folders = [...d.folders] : false;
      } else {
        if (d.data.length) {
          this.artifacts = [...this.artifacts, ...d.data];
          this.workService.setArtifact(this.artifacts);
        }
      }
      setTimeout(() => {
        // this.Loadings.isNextPageLoading = false;
        this.workService.Loadings.IsLoadingArtifacts.emit(false);
        if (this.artifacts.length == 0)
          this.noRecorFound = true
      }, 100);
    });

  }
  public onMediaUpload(event) {
    if (event.from && event.files.length) {
      let that = this;
      for (let file_key in event.files) {
        if (event.files.hasOwnProperty(file_key)) {
          let file = event.files[file_key];
          let obj: any = {};
          obj.user_current_account = that.sessionData.user_current_account;
          obj.account_folder_id = null;
          obj.huddle_id = null;
          // filestack handle key added
          obj.fileStack_handle = file.handle;
          // filestack url added
          obj.fileStack_url = file.url;
          obj.account_id = that.sessionData.user_current_account.accounts.account_id;
          obj.site_id = that.sessionData.site_id;
          obj.user_id = that.sessionData.user_current_account.User.id;
          obj.current_user_role_id = that.sessionData.user_current_account.roles.role_id;
          obj.current_user_email = that.sessionData.user_current_account.User.email;
          obj.suppress_render = false;
          obj.suppress_success_email = false;
          obj.workspace = true;
          obj.activity_log_id = "";
          obj.direct_publish = event.from == "Upload";
          obj.video_file_name = file.filename;
          obj.stack_url = file.url;
          obj.video_url = file.key;
          obj.video_id = "";
          obj.video_file_size = file.size;
          obj.direct_publish = true;
          obj.video_title = file.filename.split(".")[0];
          obj.url_stack_check = 1;
          obj.parent_folder_id = this.folderId ? this.folderId : 0
          if (event.from == "Resource") {
            this.workService.uploadResource(obj).subscribe((data: any) => {
              /*that.total_artifacts++;
              if(this.sort == "uploaded_date"){
                this.artifacts.unshift(data.data)
                this.workService.setArtifact(this.artifacts);
              }else{
                this.toastr.ShowToastr('info','New Record Available Please Click Here to Refresh', 'New Record!').onTap.subscribe(() => this.routeRefresh());
              }*/
              this.toastr.ShowToastr('info',`${this.translation.workspace_newresourceuploaded}`)
            }, error => {

            });
          }
          else {
            if(file.videoDuration) obj.video_duration = file.videoDuration;
            obj.video_source = (file.video_source) ? file.video_source : GLOBAL_CONSTANTS.RECORDING_SOURCE.FILESTACK;
            obj.millicast_rec_id = (file.millicast_rec_id) ? file.millicast_rec_id : null;
            obj.streamName = (file.streamName) ? file.streamName : null;
            this.workService.uploadVideo(obj).subscribe((data: any) => {
              /*that.total_artifacts++;
              if(this.sort == "uploaded_date"){
                this.artifacts.unshift(data.data)
                this.workService.setArtifact(this.artifacts);
              }else{
                this.toastr.ShowToastr('info','New Record Available Please Click Here to Refresh', 'New Record!').onTap.subscribe(() => this.routeRefresh());
              }*/
              let d: any = data
              let type = this.headerService.isAValidAudio(d.data.file_type);
              if (type == false) {
                this.toastr.ShowToastr('info',`${this.translation.workspace_newvideouploaded}`);
              }
              else {
                this.toastr.ShowToastr('info',this.translation.workspace_new_audio_uploaded);
              }

            }, error => {
            });
          }

        }
      }
    }
  }


  public FileClicked(from, file) {
    console.trace();
    if (from == "td") {

      if (file.stack_url && file.stack_url != null) {

        if(this.sessionData.enable_document_commenting == '0') {
          let path = environment.baseUrl + "/app/view_document" + file.stack_url.substring(file.stack_url.lastIndexOf("/"), file.stack_url.length);
          window.open(path, "_blank");
        } else {
          let path = "/document-commenting/pdf-renderer/workspace-video-page/" + file.doc_id + file.stack_url.substring(file.stack_url.lastIndexOf("/")) + "/" + file.file_type + "/" + file.account_folder_id + "/0/false";
          this.router.navigate([path]);
        }
      }
      else {
        this.DownloadFile(file);
      }
    }
    else {
      this.DownloadFile(file);
    }
  }
  private DownloadFile(file) {
    this.workService.DownloadFile(file.doc_id);
  }
  routeRefresh() {
    this.ngOnInit();
  }

  startScriptedNotes() {
    let obj = {
      observation_title: `${this.translation.workspace_scriptednotes} ` + new Date().toUTCString(),
      observations_comments: '',
      observations_standards: '',
      observations_tags: '',
      observations_time: '00: 00: 00',
      account_id: this.sessionData.user_current_account.accounts.account_id,
      user_id: this.sessionData.user_current_account.User.id,
      from_workspace: true,
      video_id: null,
      huddle_id: null,
      parent_folder_id: this.folderId ? this.folderId : 0
    }
    this.workService.startScriptedNotes(obj).subscribe((data: any) => {
      let path = '/video_details/scripted_observations/' + data.huddle_id + '/' + data.document_id;

      this.router.navigate([path], { queryParams: { workspace: true } });
    })
  }
  private processEventSubscriptions(data) {
      //console.log('test',data);

      switch (data.event) {
        case "resource_added":
        //console.log('add resource: ', data);

          if(data.is_folder == 1) {
            if(data.data.parent_folder_id && data.data.parent_folder_id != "0"){
              if(data.data.parent_folder_id == this.folderId){
                this.folders.unshift(data.data)
              } else {
                this.folders.forEach(element => {
                  if(data.data.parent_folder_id == element.id) {
                    element.stats.folders += 1;
                  }
                });
              }
            } else {
              this.folders.unshift(data.data)
            }
          }
          else if(data.data.parent_folder_id && data.data.parent_folder_id != "0") {

            this.folders.forEach(element => {
              if(data.data.parent_folder_id == element.id) {
                if(data.data.doc_type == 1) {
                  element.stats.videos += 1;
                } else if(data.data.doc_type == 2) {
                  element.stats.resources += 1;
                } else if((data.data.is_processed == 4 || data.data.is_processed == 5) && data.data.doc_type == 3) {
                  element.stats.videos += 1;
                } else if(data.data.doc_type == 3) {
                  element.stats.scripted_notes += 1;
                } else if(data.data.doc_type == 4) {
                  element.stats.urls += 1;
                }
              }
            });
          }

          if((this.resourceFilter==0 || this.resourceFilter == 1 && data.data.doc_type==1 || this.resourceFilter == 2 && data.data.doc_type==2 || this.resourceFilter == 3 && data.data.doc_type==3 || this.resourceFilter == 5 && data.data.doc_type==5) && data.is_folder != 1) {
            let should_wait = 0;
            if (data.is_video_crop) {
              should_wait = 1;
            }
            this.processResourceAdded(data.data, should_wait);
            if (data.new_video_uploaded) {
              this.NewVideos.push(data.data);
              if (this.NewVideosTime) {
                clearTimeout(this.NewVideosTime)
              }
              this.NewVideosTime = setTimeout(() => {
                this.getThumbNail()
              }, 30000);
            }
          }
          break;
        case "resource_renamed":
          data.data.title.split('.').length >= 2 ? data.data.title = data.data.title.substr(0,data.data.title.lastIndexOf('.')): false;
          this.processResourceRenamed(data.data, data.is_dummy, data.is_folder);
          break;
        case "synced_notes_uploads":
          this.processResourceUpload(data.data);
          break;
        case "resource_deleted":
          this.processResourceDeleted(data.data, data.deleted_by, data.is_folder, data.parent_folder_id, data.doc_type, data.is_processed);
          break;
        case "resource_moved":
          this.processResourceMoved(data);
          break;
        default:
          break;
      }
  }

  private processResourceMoved(data) {
    console.log('test10');
    if(this.folderId == data.dest_folder_id) {
      this.artifacts.unshift(data.data)
    } else {
      if(this.resourceFilter != data.data.doc_type) {         //If filter is selected and user move the artifact we don't have to apply socket
        this.artifacts = this.artifacts.filter(function( obj ) {
          return obj.doc_id !== data.item_id;
        });
      }
    }

    if(data.data.parent_folder_id && data.data.parent_folder_id != "0"){
      this.folders.forEach(element => {
        if(data.data.parent_folder_id == element.id) {
          if(data.data.doc_type == 1) {
            element.stats.videos += 1;
          } else if(data.data.doc_type == 2) {
            element.stats.resources += 1;
          } else if((data.data.is_processed == 4 || data.data.is_processed == 5) && data.data.doc_type == 3) {
            element.stats.videos += 1;
          } else if(data.data.doc_type == 3) {
            element.stats.scripted_notes += 1;
          } else if(data.data.doc_type == 4) {
            element.stats.urls += 1;
          }
        }
      });
    }

    if(data.old_folder_id){
      this.folders.forEach(element => {
        if(data.old_folder_id == element.id) {
          if(data.data.doc_type == 1) {
            element.stats.videos--;
          } else if(data.data.doc_type == 2) {
            element.stats.resources--;
          } else if((data.data.is_processed == 4 || data.data.is_processed == 5) && data.data.doc_type == 3) {
            element.stats.videos--;
          } else if(data.data.doc_type == 3) {
            element.stats.scripted_notes--;
          } else if(data.data.doc_type == 4) {
            element.stats.urls--;
          }
        }
      });
    }

    if(this.resourceFilter != data.data.doc_type) {         //If filter is selected and user move the artifact we don't have to apply socket
      //decreament of count in folder/main when artifact move
      if(this.folderId) {
        if (data.old_folder_id == this.folderId){
          if (this.resourceFilter == 0) {
            this.total_artifacts--;
          } else if ((this.resourceFilter == 1 && data.data.doc_type == 1) || (this.resourceFilter == 1 && data.data.doc_type == 3 && data.data.is_processed >= 4) || (this.resourceFilter == 3 && data.data.doc_type == 3 && data.data.is_processed < 4)) {
            this.total_artifacts--;
          } else if ((this.resourceFilter == 2 && data.data.doc_type == 2) || (this.resourceFilter == 5 && data.data.doc_type == 5)) {
            this.total_artifacts--;
          }
        }
      } else {
        if(!data.old_folder_id) {
          if (this.resourceFilter == 0) {
            this.total_artifacts--;
          } else if ((this.resourceFilter == 1 && data.data.doc_type == 1) || (this.resourceFilter == 1 && data.data.doc_type == 3 && data.data.is_processed >= 4) || (this.resourceFilter == 3 && data.data.doc_type == 3 && data.data.is_processed < 4)) {
            this.total_artifacts--;
          } else if ((this.resourceFilter == 2 && data.data.doc_type == 2) || (this.resourceFilter == 5 && data.data.doc_type == 5)) {
            this.total_artifacts--;
          }
        }
      }
    }

    //Increament of count in folder when artifact move in to a folder
    if(this.folderId) {
      if (data.dest_folder_id == this.folderId){
        if (this.resourceFilter == 0) {
          this.total_artifacts++;
        } else if ((this.resourceFilter == 1 && data.data.doc_type == 1) || (this.resourceFilter == 1 && data.data.doc_type == 3 && data.data.is_processed >= 4) || (this.resourceFilter == 3 && data.data.doc_type == 3 && data.data.is_processed < 4)) {
          this.total_artifacts++;
        } else if ((this.resourceFilter == 2 && data.data.doc_type == 2) || (this.resourceFilter == 5 && data.data.doc_type == 5)) {
          this.total_artifacts++;
        }
      }
    }
  }

  private processResourceAdded(resource, should_wait) {
    let that = this;
    let wait_time = 0;
    if (should_wait) {
      wait_time = 12000;
      resource.published = 0;
    }

    if(this.folderId) {
     // console.log('one');
      if (resource.parent_folder_id == this.folderId) that.artifacts.unshift(resource);
    } else {
     // console.log('two');
      if(!resource.parent_folder_id ||  resource.parent_folder_id == "0") that.artifacts.unshift(resource);
    }


    if (this.resourceFilter == 0) {
     console.log('resource filter');
      
      that.total_artifacts++;

      if(resource.parent_folder_id==this.folderId){
        //this.isToShow = true;
        this.folders.forEach(element => {
          if(resource.parent_folder_id == element.id) {
          
            console.log('consider one side');
          }
          else{
           console.log('consider two side')
          }
        });
       }
       else{
         console.log('consider third side');
         that.total_artifacts--;

          // if(resource.parent_folder_id == 0 && this.folderId == undefined ){
          //   that.total_artifacts++;
          // }
        
       }






      
    } else if ((this.resourceFilter == 1 && resource.doc_type == 1) || (this.resourceFilter == 1 && resource.doc_type == 3 && resource.is_processed >= 4) || (this.resourceFilter == 3 && resource.doc_type == 3 && resource.is_processed < 4)) {
      console.log('four');
      that.total_artifacts++;
    } else if ((this.resourceFilter == 2 && resource.doc_type == 2) || (this.resourceFilter == 5 && resource.doc_type == 5)) {
      console.log('five');
      that.total_artifacts++;
    }
  }

  private processResourceDeleted(resource_id, deleted_by, is_folder?, parent_folder_id?, doc_type?, is_processed?) {

    console.log('deleteone');
    var indexOfMyObject = this.artifacts.findIndex(x => {
      return x.doc_id == resource_id || x.id == resource_id;
    });
    if (indexOfMyObject > -1) {
      console.log('deletetwo');
      let obj = this.artifacts[indexOfMyObject];
      this.artifacts.splice(indexOfMyObject, 1);
      this.total_artifacts -= 1;
      this.workService.setTotalAritifactCount(this.total_artifacts);
    }
    else{
      console.log('deletethree');
      console.log('totalartifacts',this.total_artifacts);
      // if(this.total_artifacts!=0){
      //   this.total_artifacts--
      // };
      this.workService.setTotalAritifactCount(this.total_artifacts);
    }

    if(is_folder == 1) {
      console.log('deletefour');
      this.folders = this.folders.filter(function( obj ) {
        return obj.id !== resource_id;
      });
    }

    if(parent_folder_id) {
      console.log('deletefive');
      // if(this.total_artifacts!=0){
      //   this.total_artifacts--
      // }
      this.folders.forEach(element => {
        if(parent_folder_id == element.id) {

          if(is_folder) {
            element.stats.folders--;
          } else if(doc_type == 1) {
            element.stats.videos--;
          } else if(doc_type == 2) {
            element.stats.resources--;
          } else if((is_processed == 4 || is_processed == 5) && doc_type == 3) {
            element.stats.videos--;
          } else if(doc_type == 3) {
            element.stats.scripted_notes--;
          } else if(doc_type == 4) {
            element.stats.urls--;
          }
        }
      });
    }

  }

  private processResourceRenamed(resource, is_dummy?, folder?) {
    let objResource = _.find(this.artifacts, function (item) {
      return ((parseInt(item.id) == parseInt(resource.id)) || parseInt(item.doc_id) == parseInt(resource.doc_id));
    });
    let index = -1;
    this.artifacts.forEach((item, i) => {
      if ((parseInt(item.id) == parseInt(resource.id)) || (parseInt(item.doc_id) == parseInt(resource.doc_id))) {
        index = i;
      }
    });
    if (objResource) {
      if (is_dummy) {
        this.artifacts[index].total_comments = resource.total_comments;
        this.artifacts[index].total_attachment = resource.total_attachment;
      } else {
        this.artifacts[index] = resource;
      }
    }
    if(folder == 1) {                       //Change name of a Folder
      this.folders.forEach(element => {
        if(element.id == resource.id) {
          element.name = resource.name;
          element.title = resource.name;
        }
      });
    }

  }
  private processResourceUpload(resource) {
    let objResource = _.find(this.artifacts, function (item) {
      return parseInt(item.doc_id) == parseInt(resource.id);
    });
    let index = -1;
    this.artifacts.forEach((item, i) => {
      if (parseInt(item.doc_id) == parseInt(resource.id)) {
        index = i;
      }
    });
    if (objResource) {
      this.artifacts[index].upload_progress = resource.upload_progress;
      this.artifacts[index].total_comments = resource.total_comments;

      this.artifacts[index].video_duration = resource.video_duration;
      if (!resource.video_duration) {
        this.artifacts[index].video_duration = resource.current_duration;
      }
      this.artifacts[index].upload_status = resource.upload_status;
      this.artifacts[index].thubnail_url = resource.thubnail_url;
      this.artifacts[index].static_url = resource.static_url;
    }
  }

  openActivityModal() {
    const initialState = {
      pageType: 'workspace',
      class: 'modalcls2'
    };
    this.bsModalRef = this.modalService.show(ActivityModelComponent, Object.assign({ initialState }, { class: 'modalcls2' }));
  }

  public checkValidRoute(data) {
    if (data == "grid" || data == "list") {

    }
    else
      this.router.navigate(['/page-not-found']);
    return;
  }
  ngOnDestroy() {
    /*
    this.ARouter.params.subscribe(p => {
      this.huddle_id = p.id;
      this.socketService.destroyEvent(`huddle-details-${this.huddle_id}`);
    });
    */
      if(this.socket_listener)
        this.socket_listener.unsubscribe();

    this.subscriptions.unsubscribe();
  }

  getThumbNail() {

    this.NewVideos.forEach(e => {
      let obj = {
        document_id: e.doc_id,
        folder_type: 3,
        account_folder_id: e.account_folder_id,
        user_id: e.created_by,
      }
      this.workService.getSingleVideo(obj).subscribe((data: any) => {
        this.processResourceRenamed(data.data)
      })
    });
  }

  public openURLArtifactModal() {
    const initialState = { pageType: this.pageType };
    this.bsModalRef = this.modalService.show(URLArtifactModalComponent, { initialState, class: 'url-artifact-modal-container' });
    this.bsModalRef.content.folderId = this.folderId;
  }

  public openRecordVideoModal() {
    let bsModalRef;
    if(this.browserAgent !== 'safari')
      bsModalRef = this.modalService.show(RecordVideoModelComponent, { class: 'modal-container-750', backdrop: 'static' });
    else bsModalRef = this.modalService.show(MillicastRecordVideoModelComponent, { class: 'modal-container-750', backdrop: 'static' });

    bsModalRef.content.onRecordedUpload.subscribe(data => {
      this.onMediaUpload(data);
    });
  }
}
