import { Component, OnInit, OnDestroy } from '@angular/core';
import { HeaderService } from "@projectModules/app/services";
import { HomeService } from "@projectModules/app/services";
import { DiscussionService } from '@videoHuddle/child-modules/details/servic/discussion.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'workspace-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.css']
})

export class BreadcrumbsComponent implements OnInit, OnDestroy {
  public translation: any = {};
  public breadcrubms: any;
  private translationSubscription: Subscription;
  private breadCrumbsSubscription: Subscription;

  constructor(private homeService: HomeService, public discussionService: DiscussionService, private headerService: HeaderService) {
    this.translationSubscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
    this.breadCrumbsSubscription = this.homeService.breadcrumbs$.subscribe(data => {
      setTimeout(() => {
        this.breadcrubms = data;
      }, 800);

    });
  }

  ngOnInit() { }

  ngOnDestroy() {
    this.translationSubscription.unsubscribe();
    this.breadCrumbsSubscription.unsubscribe();
  }

}
