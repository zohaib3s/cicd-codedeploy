import { NgModule } from '@angular/core';

import { __IMPORTS, __DECLARATIONS, __PROVIDERS } from './components.barrel';
import { WorkspaceRoutes } from './workspace.routes';
import { AppLevelSharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [__DECLARATIONS],
  imports: [__IMPORTS, WorkspaceRoutes,AppLevelSharedModule],
  providers: [__PROVIDERS],
})
export class WorkspaceModule { }
