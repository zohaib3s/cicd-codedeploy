import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NewfolderService {

  constructor(
    private http: HttpClient
  ) { }

  public createNewFolder(obj) {
    let path = environment.APIbaseUrl + "/folders/create";
    return this.http.post(path, obj);
  }

  public renameFolder(obj) {
    let path = environment.APIbaseUrl + "/folders/rename";
    return this.http.post(path, obj);
  }

  public deleteFolder(obj) {
    let path = environment.APIbaseUrl + "/folders/delete";
    return this.http.post(path, obj);
  }

  public folderTreeViewData(obj) {
    let path = environment.APIbaseUrl + "/folders/treeview_data";
    return this.http.post(path, obj);
  }

  moveDocumentFolder(obj) {
    let path = environment.APIbaseUrl + "/folders/move_document_folder";
    return this.http.post(path, obj);
  }

}
