import { Injectable, EventEmitter } from '@angular/core';
import * as filestack from 'filestack-js';
import { environment } from "@environments/environment";
import { HeaderService } from "@projectModules/app/services";
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
@Injectable()
export class FilestackService {

    private client;
   
	private options = {
		location: "s3",
		path: '/tempupload',
		access: 'public',
		container: environment.container,
		region: 'us-east-1'
	}

    public GlobalOptions: any = [];

    public FilesUploaded: EventEmitter<any> = new EventEmitter<any>();

    constructor(private headerService: HeaderService) {

        this.GlobalOptions["videoOptions"] = {
            maxFiles: 100,
            maxSize: GLOBAL_CONSTANTS.FILESTACK_MAXSIZE,
            accept: ['.3gp','.mkv','video/*', 'audio/*'],
            fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive']
        };
        this.GlobalOptions["audioOptions"] = {
            maxFiles: 10,
            maxSize: GLOBAL_CONSTANTS.FILESTACK_MAXSIZE,
            accept: ['audio/*'],
            fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive']
        };
        this.GlobalOptions["resourceOptions"] = {
            maxFiles: 100,
            maxSize: GLOBAL_CONSTANTS.FILESTACK_MAXSIZE,
            accept: GLOBAL_CONSTANTS.RESOURCE_UPLOAD_EXTENSIONS,
            fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive'],
          customText: {
            'File {displayName} is not an accepted file type. The accepted file types are {types}': 'File {displayName} is not an accepted file type. The accepted file types are image, text',
          }
        };
        this.GlobalOptions["cameraOptions"] = {
            maxFiles: 1,
            maxSize: GLOBAL_CONSTANTS.FILESTACK_MAXSIZE,
            fromSources: ["video"]
        };
    }

    public InitFileStack() {
        let key = environment.fileStackAPIKey;
        this.client = filestack.init(key);

    }

    public uploadToS3(audioBlob: Blob, audioExtension: string, accountId: number, userId: number) {
        this.InitFileStack();
		const date = new Date();
		this.options.path = '/tempupload'; // Think and find a way that why we reset it to /uploads
		this.options.path = this.makeUploadPath(accountId, userId, date);
		return this.client.upload(audioBlob, {}, { ...this.options, filename: `ScreenSharing_${accountId}_${userId}_${date.getTime()}.${audioExtension}` });
	}
    private makeUploadPath(accountId: number, userId: number, date: Date) {
		return `${this.options.path}/${accountId}/audio/${date.getFullYear()}/${this.getTwoDigitNumber(date.getMonth() + 1)}/${this.getTwoDigitNumber(date.getDate())}/`;
    }private getTwoDigitNumber(number: number) {
		if (number < 10) return `0${number}`;
		else return number;
	}
    

    public showPicker(optionsFor) {

        let options = this.GlobalOptions[optionsFor] || {};
        options.storeTo = {
            location: "s3",
            path: this.getUploadPath(),
            access: 'public',
            container: environment.container,
            region: 'us-east-1'
        }
        options.onUploadDone = (res) => { this.FilesUploaded.emit(res.filesUploaded) }
        let sessionData: any = this.headerService.getStaticHeaderData();
        let language = sessionData.language_translation.current_lang;
        options.lang = language;
        return this.client.picker(options).open();

    }

    private getUploadPath() {

        let temp = "/tempupload/";

        let sessionData: any = this.headerService.getStaticHeaderData();

        let account_id = sessionData.user_current_account.accounts.account_id;

        temp = temp + account_id;

        let date = new Date();

        temp += "/" + date.getFullYear() + "/" + this.padNumber(date.getMonth() + 1) + "/" + this.padNumber(date.getDate()) + "/";

        return temp;

    }

    private padNumber(n) {

        let number = Number(n);

        if (number <= 9) {
            return "0" + "" + number;
        } else {
            return n;
        }

    }
}
