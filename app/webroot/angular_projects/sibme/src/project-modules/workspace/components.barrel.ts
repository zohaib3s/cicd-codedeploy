import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgSlimScrollModule, SLIMSCROLL_DEFAULTS } from 'ngx-slimscroll';
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ModalModule } from "ngx-bootstrap/modal";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { ToastrModule } from 'ngx-toastr';

import { AppLevelSharedModule } from '@src/project-modules/shared/shared.module';

import { FilestackService, WorkspaceHttpService } from "./services";

import { RootComponent, BreadcrumbsComponent, HomeComponent } from "./components";
import { FSUploaderDirective } from './directives';
import { DndModule } from "ngx-drag-drop";

export const __IMPORTS = [
    CommonModule,
    FormsModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ToastrModule.forRoot(),
    NgSlimScrollModule,
    AppLevelSharedModule,
    DndModule
];

export const __DECLARATIONS = [RootComponent, BreadcrumbsComponent, HomeComponent, FSUploaderDirective];

export const __PROVIDERS = [
    FilestackService, WorkspaceHttpService,
    { provide: SLIMSCROLL_DEFAULTS, useValue: { alwaysVisible: false } }
];