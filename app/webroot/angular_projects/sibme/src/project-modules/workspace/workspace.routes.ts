import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RootComponent, HomeComponent } from './components';
import { SharedPageNotFoundComponent } from '../shared/shared-page-not-found/shared-page-not-found.component';

const routes: Routes = [
  { path: '', redirectTo: 'workspace/home/grid', pathMatch: 'full' },
  { path: 'workspace', redirectTo: 'workspace/home/grid', pathMatch: 'full' },
  { path: 'workspace/home', redirectTo: 'workspace/home/grid', pathMatch: 'full' },
  {
    path: 'workspace', component: RootComponent, children: [
      { path: 'home/:displayStyle', component: HomeComponent },
      { path: 'home/:displayStyle/:folderId', component: HomeComponent }
    ]
  },
  { path: 'page_not_found', component: SharedPageNotFoundComponent },
  { path: '**', redirectTo: 'page_not_found', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkspaceRoutes { }