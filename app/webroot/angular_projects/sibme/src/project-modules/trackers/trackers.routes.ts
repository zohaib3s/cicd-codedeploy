import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { AssessmentMainComponent } from './components/assessment-main/assessment-main.component';
import { CoachingMainComponent } from './components/coaching-main/coaching-main.component';
import { RootComponent, CoachingReportComponent, HuddleReportComponent, AssessmentReportComponent } from './components';
import { SharedPageNotFoundComponent } from '../shared/shared-page-not-found/shared-page-not-found.component';
import { ReportsDetailView } from '../shared/components/reports-detail-view/reports-detail-view.component';
import { TrackerPermissionGuard } from '../trackers/guards/tracker-permission.guard';

const routes: Routes = [
  { path: '', redirectTo: 'assessment', pathMatch: 'full' },
  {path: '',component:RootComponent,children:[
  { path: 'assessment', component: AssessmentMainComponent } ,
  { path: 'coaching', component: CoachingMainComponent , canActivate: [TrackerPermissionGuard]},
  {path: 'coaching-report' , component: CoachingReportComponent},
  {path: 'huddle-report' , component: HuddleReportComponent , canActivate: [TrackerPermissionGuard]},
  {path: 'assessment-report' , component: AssessmentReportComponent , canActivate: [TrackerPermissionGuard]}
  ]},
  {path:'page_not_found',component:SharedPageNotFoundComponent},
  {path: 'report-detail-view/:report_type' , component: ReportsDetailView},
  
  {
    path: '**',
    redirectTo: 'page_not_found',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

exports: [RouterModule]
})
export class TrackersRoutes { }