import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { ToastrModule } from 'ngx-toastr';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { NgScrollbarReachedModule } from 'ngx-scrollbar/reached-event';
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ModalModule } from "ngx-bootstrap/modal";
import { TabsModule } from "ngx-bootstrap/tabs";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { AlertModule } from 'ngx-bootstrap/alert';
import { SortableModule } from 'ngx-bootstrap/sortable';

import { MainService } from "./services";

import {
  AssessmentMainComponent, CoachingMainComponent,
  AssessmentDetailComponent, AssessmentReportComponent,
  CoachingDetailComponent, RootComponent,
  CoachingReportComponent, HuddleReportComponent
} from "./components";

import { NgxPaginationModule } from 'ngx-pagination';
import { DndModule } from 'ngx-drag-drop';
import { NgSlimScrollModule, SLIMSCROLL_DEFAULTS } from 'ngx-slimscroll';
import { NgScrollbarModule, NG_SCROLLBAR_OPTIONS } from 'ngx-scrollbar';
import { SearchPipe } from './pipe/search.pipe';
import { AmChartsModule } from '@amcharts/amcharts3-angular';
import { OrderModule } from 'ngx-order-pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AppLevelSharedModule } from '../shared/shared.module';
import { TrackerPermissionGuard } from './guards/tracker-permission.guard';
import { QuillModule } from 'ngx-quill';
import { SafeHtmlPipe } from './pipe/safe-html.pipe';
import { ClickOutsideModule } from 'ng-click-outside';
export const __IMPORTS = [
  Ng2SearchPipeModule,
  CommonModule,
  FormsModule,
  LoadingBarHttpClientModule,
  MomentModule,
  UiSwitchModule,
  TooltipModule.forRoot(),
  BsDropdownModule.forRoot(),
  ButtonsModule.forRoot(),
  BsDatepickerModule.forRoot(),
  TabsModule.forRoot(),
  ToastrModule.forRoot(),
  ModalModule.forRoot(),
  TypeaheadModule.forRoot(),
  AlertModule.forRoot(),
  SortableModule.forRoot(),
  NgxPaginationModule,
  CarouselModule,
  DndModule,
  NgScrollbarReachedModule,
  NgSlimScrollModule,
  AmChartsModule,
  OrderModule,
  NgScrollbarModule,
  AppLevelSharedModule,
  QuillModule.forRoot(),
  ClickOutsideModule
];



export const __DECLARATIONS = [
  RootComponent,
  AssessmentMainComponent,
  CoachingMainComponent,
  AssessmentDetailComponent,
  CoachingDetailComponent,
  CoachingReportComponent,
  HuddleReportComponent,
  AssessmentReportComponent,
  // SearchPipe,
  SafeHtmlPipe
];

export const __PROVIDERS = [
  TrackerPermissionGuard,
  MainService,
  { provide: SLIMSCROLL_DEFAULTS, useValue: { alwaysVisible: true } }
];