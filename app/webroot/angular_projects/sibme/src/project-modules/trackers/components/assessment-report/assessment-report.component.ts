import { Component, OnInit, Input, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { HeaderService, LocalStorageService } from '@src/project-modules/app/services';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { KeyValue } from '@angular/common';
import { BsModalRef, BsModalService, ModalDirective, ModalOptions } from 'ngx-bootstrap/modal';
import { TrackerFiltersComponent } from '@src/project-modules/shared/components';
import { MainService } from '../../services';
import { encodeCompress, getPathAndQuery, guidGenerator,formatDateUtil, deepCopy } from '@src/project-modules/shared/components/common/utility';
import { Router } from '@angular/router';
import { NgScrollbar } from 'ngx-scrollbar';
import { environment } from '@src/environments/environment';
import { TrackerFiltersService } from '@src/project-modules/shared/services/tracker-filters.service';



@Component({
  selector: 'app-assessment-report',
  templateUrl: './assessment-report.component.html',
  styleUrls: ['./assessment-report.component.css']
})
export class AssessmentReportComponent implements OnInit {
  @ViewChild(TrackerFiltersComponent) trackerFiltersComponent: TrackerFiltersComponent
  @ViewChild('childModal',{static:false}) childModal: ModalDirective;
  private subscription: Subscription;
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  p: number = 1;
  translation: any;
  bsModalRef: BsModalRef;

  headers = [] as any;
  attributes = [] as any;
  sortByAsc: boolean;
  SearchString = '';
  filterObj: any;
  imageStaticUrl: any;
  baseUrl: any;
  public header_data;
  imageBaseUrl: any;
  config = {
    id: '',
    currentPage: 1,
    itemsPerPage: 10,
    totalItems: 20
  };
  isOpenParaBox = false;
  public isOpenBoxAssessmentSummary: boolean = false;
  public currentIndex=1;
  styledValue: any;
  isLoading = true;
  isLoadingTabs = true;
  huddleAssessors = [];
  width: any;
  @ViewChild('assessorModal', { static: false }) assessorModal: ModalDirective;
  @ViewChild('stickyDiv') stickyDiv: ElementRef;
  @ViewChild('scrollable') scrollbarRef: NgScrollbar;
  public count: number = 0;
  public expandDesc: boolean;
  public scrollWidth: number;
  list_container_height = false;
  autohight: any;
  lastParaIndex: any;
  last_assessment_index: any;
  assessee_index: any;
  paraIndex: any;
  ip = 1;
  selectedHuddleId;
  selectedHuddleIndex;
  assesseePreLoader=false;

  constructor(
    public headerService: HeaderService,
    private modalService: BsModalService,
    public mainService: MainService,
    private localStorage: LocalStorageService,
    private trackerFilterService: TrackerFiltersService,
    public router: Router

  ) {
    this.header_data = this.headerService.getStaticHeaderData();
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
  }

  ngOnInit() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    };
  }
  scrollRight(){
      this.count = this.count + 150;
      this.scrollbarRef.scrollTo({left: this.count});
  }
  scrollLeft(){
    if (this.count === 0){
      this.count = 150;
      return;
    } else {
      this.count = this.count - 150;
      this.scrollbarRef.scrollTo({left: this.count});
    }
  }

  showArrowLeft() {
    if (this.count === 0){
      return false;
    }else {
      return true;
    }
  }

  showArrowRight() {
    const scroll = document.getElementById('scrollBar');

    if (this.scrollWidth){
      return scroll.scrollWidth < this.scrollWidth && (scroll.scrollWidth + this.count) < this.scrollWidth;
    }else{
      return false;
    }

  }

  originalOrder = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => {
    return 0;
  }

  scrollSync(selector) {
    let active = null;
    document.querySelectorAll(selector).forEach((element) => {
      element.addEventListener('mouseenter', (e) => {
        active = e.target;
      });
      element.addEventListener('scroll', (e) => {
        if (e.target !== active) return;

        document.querySelectorAll(selector).forEach((target) => {
          if (active === target) return;
          target.scrollLeft = active.scrollLeft;
        });
      });
    });
  }

  openDetailsBox(evt, index,k, huddle_assessee) {
    console.log('open paragraph modal'+ index)
    this.isOpenBoxAssessmentSummary = false;
    this.isOpenParaBox = true;
    this.lastParaIndex = k;
    this.assessee_index = index;
    if(index < 3) {
      this.list_container_height = true;
      this.autohight = false;
    } else {
      let assessee = huddle_assessee;
      let lastIndexs = [assessee.length - 1,assessee.length - 2,assessee.length - 3];
      for(let i=0;i<lastIndexs.length;i++){
        if(index == lastIndexs[i]){
         this.autohight = true;
         console.log(true)
        }
       }
    }
  }
  getAutoSideClass(h_index,a_index) {

    if(h_index == 0)
    {
      if(a_index < 3) {
        return 'auto_side';
      }
      if(a_index >= 3) {
        return 'auto_side_bottom';
      }
    }
    return '';
  }
  closeDetailsBox() {
    this.isOpenParaBox = false;

    this.list_container_height = false;
  }
  onClickedParaOutside(evt,k,j) {
    console.log('para close'+ k+j);
    this.closeDetailsBox();
  }
  onClickedOutside(evt,is_summary) {
    console.log('outside'+ is_summary);
    evt.stopPropagation();
    if(this.isOpenBoxAssessmentSummary) {
      this.closeBoxAssessmentSummary();
    }
  }
  HtmlFiletr(assessment_custom_field_value){
    let doc:any = new DOMParser()
    const parsedDocument = doc.parseFromString(assessment_custom_field_value, "text/html")
    var allText = parsedDocument.all[0].textContent;
    return allText
  }

  assessmentSummaryLeft(evt, assessment_summaryList) {
    if(this.currentIndex > 1 && this.currentIndex <= assessment_summaryList?.length) {
      this.currentIndex--;
    }
  }
  closeBoxAssessmentSummary() {
    this.isOpenBoxAssessmentSummary = false;
    this.list_container_height = false;
  }
  assessmentSummaryRight(evt, assessment_summaryList) {
    if(this.currentIndex < assessment_summaryList?.length) {
      this.currentIndex++;
    }
  }
  public openAssessmentSummaryDetailsBox(event,index,k,huddle_assessee) {
    this.currentIndex = 1;
    this.isOpenParaBox = false;
    this.isOpenBoxAssessmentSummary = true;
    this.last_assessment_index = index;
    if(index < 3) {
      this.list_container_height = true;
      this.autohight = false;
    } else {
      let assessee = huddle_assessee;
      let lastIndexs = [assessee.length - 1,assessee.length - 2,assessee.length - 3];
      for(let i=0;i<lastIndexs.length;i++){
        if(index == lastIndexs[i]){
         this.autohight = true;
         console.log(true)
        }
       }
    }
  }

  handleChange(data) {
    this.headers = data.attributes_header;
    this.attributes = data.assessment_huddles;
    this.attributes.map(x => x.collapse = false);

    if (this.attributes.length > 0){
      this.attributes[0].collapse = true;
    }

    this.config.totalItems = data.assessment_huddles_count;
    this.config.currentPage = data.page;
    this.isLoading = false;
    this.isLoadingTabs = false;
    setTimeout(() => {
      this.width = this.stickyDiv?.nativeElement.children.length * 150 + 50 + 'px';
      this.scrollWidth = this.stickyDiv?.nativeElement.children.length * 150 + 120;
      this.scrollSync('.ng-scroll-viewport');
    }, 1000);
  }

  turnOnCollapse(attribute) {
    this.expandDesc = false;
    this.attributes.map(x => x.collapse = false)
    attribute.collapse = true;
    //close assessment summary and paragraph
    this.closeBoxAssessmentSummary();
    this.closeDetailsBox();
    setTimeout(() => {
      this.width = this.stickyDiv.nativeElement.children.length * 150 + 50 + 'px';
      this.scrollWidth = this.stickyDiv.nativeElement.children.length * 150 + 120;
      this.scrollSync('.ng-scroll-viewport');
    }, 1000);
  }

  onSearch(searchString) {
    this.SearchString = searchString
  }

  sortTable(j, assesseeList) {

    if (this.sortByAsc) {
      this.sortByAsc = !this.sortByAsc;
      assesseeList.sort((a, b) => {

        return isNaN(a[j] - b[j]) ? (a[j] === b[j]) ? 0 : (a[j] < b[j]) ? -1 : 1 : a[j] - b[j];
      }
      );
    }
    else {
      this.sortByAsc = !this.sortByAsc;

      assesseeList.sort((a, b) => {
        return isNaN(b[j] - a[j]) ? (b[j] === a[j]) ? 0 : (b[j] < a[j]) ? -1 : 1 : b[j] - a[j];
      });
    }

  }


  sortTableByName(assesseeList) {

    if (this.sortByAsc) {
      this.sortByAsc = !this.sortByAsc;
      assesseeList.sort((a, b) => {

        return isNaN(a - b) ? (a.user_name === b.user_name) ? 0 : (a.user_name < b.user_name) ? -1 : 1 : a.user_name - b.user_name;
      }
      );
    }
    else {
      this.sortByAsc = !this.sortByAsc;

      assesseeList.sort((a, b) => {
        return isNaN(b - a) ? (b.user_name === a.user_name) ? 0 : (b.user_name < a.user_name) ? -1 : 1 : b.user_name - a.user_name;
      });
    }

  }


  getImage(userId, image) {
    if (image) {
      return this.imageBaseUrl = 'https://s3.amazonaws.com/sibme.com/static/users/' + userId + '/' + image;
    } else return this.header_data.base_url + "/img/home/photo-default.png";
  }

  checkValueIfStyleNeeded(value) {
    if(isNaN(value) && !Array.isArray(value)) {
      if(['videos', 'resources', 'comments', 'assessed'].includes(value.split(",")[1])) {
        return value.split(",")[1]
      } else {
        return 'orignal'
      }
    } else {
      return 'orignal'
    }
  }

  openAssessorModal(arr) {
    this.huddleAssessors = arr;
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-container-700',
    };

    this.bsModalRef = this.modalService.show(this.assessorModal, config);
  }


  onPageChange(event) {
    this.config.currentPage = event;
    this.trackerFiltersComponent.setPage(event)
  }

  onPerPageLimitChange(event) {
    this.trackerFiltersComponent.filters_obj.page = 1;
    this.trackerFiltersComponent.setItemsPerPage(this.config.itemsPerPage)
  }

  exportReport(exportType, active) {
    this.trackerFiltersComponent.setExportReport(exportType, active)
  }

  isNumeric(value) {
    return isNaN(value);
  }
  navigateToDetail(data, index) {


    this.headers.forEach((element) => {
      element['fake_id'] = guidGenerator()
    });
    let header = this.headers[index];
    const obj = {
      coach_id: '',
      user_id: data.value.user_id,
      account_id: data.value.account_id,
      report_type: 'assessment_report',
      huddle_type: '',
      huddle_id: data.value.huddle_id,
      start_date: this.filterObj.start_date,
      end_date: this.filterObj.end_date,
      duration: this.filterObj.duration,
      path: getPathAndQuery(),
      header: [header]
    }

    this.localStorage.set(obj.report_type, this.headers)
    // let key = this.headers[index];
    // key = key.toLowerCase();
    // key = key.replace(/ /g, "_");
    this.headerService.reportsDetailData(obj);
    this.router.navigate(['trackers/report-detail-view/' + header.key], { queryParams: { query: encodeCompress(obj) } });
  }

  navigateToPlayerCard(user_id,account_id) {

    let queryParamsFrom = {displayName:'reporting_assessment_report',url:this.router.url};
    let obj={from:null};
    if(this.filterObj.updateClick){
      obj=deepCopy(this.filterObj);
      obj.from=encodeCompress(queryParamsFrom);
    } else {
      obj.from=encodeCompress(queryParamsFrom);
    }

    this.router.navigate(['/analytics_angular/playercard',account_id,user_id,formatDateUtil(this.filterObj.start_date),formatDateUtil(this.filterObj.end_date)], { queryParams: obj });
  }

  getSingleAsignment(data, index){

    if (data.value.is_submitted) {
      const link = `${environment.baseUrl}/home/video_huddles/assessment/${data.value.huddle_id}/huddle/assignment/${data.value.user_id}`;
      if (link) window.open(link, "_blank");
      // this.router.navigate([`/video_huddles/assessment/${data.value.huddle_id}/huddle/assignment/${data.value.user_id}`]);

    } else {
      this.showChildModal()
    }

   }

   getParticipantLimit(participant_Count) {
    let result;
    if (participant_Count > 999 && participant_Count <= 999999) {
        result = Math.floor(participant_Count / 1000) + 'K';
    } else {
        result = participant_Count;
    }
    return result;
  }

  showChildModal(): void {

    this.childModal.show();
  }

  hideChildModal(): void {
    this.childModal.hide();
  }
  pageChanged(page){
    this.assesseePreLoader=true;
    if(this.selectedHuddleIndex==''|| this.selectedHuddleIndex==null || this.selectedHuddleIndex==undefined){
      this.selectedHuddleIndex=0;
      this.selectedHuddleId = this.attributes[0].assessee[0].huddle_id
    }
    this.ip=page;
    const payload={
      account_id: this.header_data.user_current_account.users_accounts.account_id,
      account_ids: this.filterObj.account_ids,
      active: false,
      duration: this.filterObj.duration,
      end_date: this.filterObj.end_date,
      folder_type: this.filterObj.folder_type,
      framework_id: this.filterObj.framework_id,
      huddle_ids: this.filterObj.huddle_ids,
      huddle_type: this.filterObj.huddle_type,
      limit: this.filterObj.limit,
      page: this.filterObj.page,
      role_id: this.filterObj.role_id,
      start_date: this.filterObj.start_date,
      updateClick: this.filterObj.updateClick,
      user_id: this.filterObj.user_id,
      user_role: this.filterObj.user_role,
      huddle_id:this.selectedHuddleId,
      sub_page:page,
      report_id: this.filterObj.report_id      //repot_id will only go when user select any repory from settings

    }
      this.trackerFilterService.getAssessesForpage(payload).subscribe((data:any)=>{
      this.assesseePreLoader=false;
      this.attributes[this.selectedHuddleIndex].assessee = data.slice(0);

      })


  }

  collapsReport(){
   this.ip=1;
  }
  expandReport(data,index){
   this.selectedHuddleId = data.huddle_id
   this.selectedHuddleIndex = index;
   this.ip=1;
   this.pageChanged(this.ip);
  }

}
