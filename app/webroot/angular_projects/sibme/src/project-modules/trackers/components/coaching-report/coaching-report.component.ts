import { Component, OnInit, Input, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { HeaderService } from '@src/project-modules/app/services';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { KeyValue } from '@angular/common';
import { TrackerFiltersComponent } from '@src/project-modules/shared/components';
import { Router } from '@angular/router';
import { encodeCompress, getPathAndQuery, guidGenerator, formatDateUtil, deepCopy } from '@src/project-modules/shared/components/common/utility';
import { LocalStorageService } from '@src/project-modules/app/services/localStorage.service';
import { NgScrollbar } from 'ngx-scrollbar';
@Component({
  selector: 'app-coaching-report',
  templateUrl: './coaching-report.component.html',
  styleUrls: ['./coaching-report.component.css']
})
export class CoachingReportComponent implements OnInit {
  @ViewChild(TrackerFiltersComponent) trackerFiltersComponent: TrackerFiltersComponent
  @ViewChild('stickyDiv') stickyDiv: ElementRef;
  @ViewChild('scrollable') scrollbarRef: NgScrollbar;
  @ViewChild('headerScrollBar') headerScrollBarRef: NgScrollbar;
  public count: number = 0;
  private subscription: Subscription;
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  p: number = 1;
  translation: any;
  headers = [] as any;
  attributes = [] as any;
  sortByAsc: boolean;
  SearchString = '';
  imageStaticUrl: any;
  baseUrl: any;
  public header_data;
  imageBaseUrl: any;
  config = {
    id: '',
    currentPage: 1,
    itemsPerPage: 10,
    totalItems: 20
  };
  user_data: any;

  isLoading = true;
  isLoadingTabs = true;
  filterObj: any;
  width: any;
  sort_by: any;
  sort_by_order: any;
  headerIndex: any;
  scrollWidth: number;

  constructor(private headerService: HeaderService, private router: Router, private localStorage: LocalStorageService) {
    this.header_data = this.headerService.getStaticHeaderData();
    this.user_data = this.header_data.user_current_account;
    if (this.user_data.roles.role_id == '120' && !this.header_data.is_user_coach) {
      this.router.navigate(['/page-not-found']);
    }
    if (this.user_data.roles.role_id == '125') {
      this.router.navigate(['/profile-page']);
    }


    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
  }

  ngOnInit() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    };
  }

  scrollRight(){
      this.count = this.count + 150;
      this.headerScrollBarRef.scrollTo({left: this.count});
      this.scrollbarRef.scrollTo({left: this.count});
  }
  scrollLeft(){
    if (this.count === 0){
      this.count = 150;
      return;
    } else {
      this.count = this.count - 150;
      this.headerScrollBarRef.scrollTo({left: this.count});
      this.scrollbarRef.scrollTo({left: this.count});
    }
  }

  showArrowLeft() {
    if (this.count === 0){
      return false;
    }else {
      return true;
    }

  }

  showArrowRight() {
    const scroll = document.getElementById('scrollBar');

    if (this.scrollWidth && scroll){
      return scroll.scrollWidth < this.scrollWidth && (scroll.scrollWidth + this.count) < this.scrollWidth;
    }else{
      return false;
    }

  }

  originalOrder = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => {
    return 0;
  }

  scrollSync(selector) {
    let active = null;
    document.querySelectorAll(selector).forEach((element) => {
      element.addEventListener("mouseenter", (e) => {
        active = e.target;
      });
      element.addEventListener("scroll", (e) => {
        if (e.target !== active) return;

        document.querySelectorAll(selector).forEach((target) => {
          if (active === target) return;
          target.scrollLeft = active.scrollLeft;
        });
      });
    });
  }

  handleChange(data) {
    this.sort_by = data.sort_by;
    this.sort_by_order = data.sort_by_order;
    this.headers = data.attributes_header;
    this.attributes = data.attributes_data;
    this.sortData(this.attributes)
    this.config.totalItems = data.total_coach;
    this.config.currentPage = data.page;
    setTimeout(() => {

      this.width = this.stickyDiv?.nativeElement.children.length * 150 + 131 + 'px';
      this.scrollWidth = this.stickyDiv?.nativeElement.children.length * 150 + 120;
      this.scrollSync(".ng-scroll-viewport");

    }, 1000);
    this.isLoading = false;
    this.isLoadingTabs = false;
  }

  turnOnCollapse(attribute: any) {
    this.attributes.map(x => x.collapse = false)
    attribute.collapse = true

  }

  sortData(attribute) {
    let j = this.headerIndex
    if(j>-1){
    if (this.sort_by_order == 'ASC') {
      attribute.sort((a, b) => {

          return isNaN(a[j] - b[j]) ? (a[j] === b[j]) ? 0 : (a[j] < b[j]) ? -1 : 1 : a[j] - b[j];
        }
        );
      }
      else {
        this.sortByAsc = !this.sortByAsc;

      attribute.sort((a, b) => {
        return isNaN(b[j] - a[j]) ? (b[j] === a[j]) ? 0 : (b[j] < a[j]) ? -1 : 1 : b[j] - a[j];
      });
    }
  }
  }

  onSearch(searchString) {
    this.SearchString = searchString
  }


  sortTable(value, order, index) {
    this.headerIndex = index
    if (order)
      this.trackerFiltersComponent.sortTable(value.sorting_key, order)
    else
      this.trackerFiltersComponent.sortTable("", order)

  }

  getImage(userId, image) {
    if (image) {
      return this.imageBaseUrl = 'https://s3.amazonaws.com/sibme.com/static/users/' + userId + '/' + image;
    } else return this.header_data.base_url + "/img/home/photo-default.png";
  }

  onPageChange(event) {

    this.config.currentPage = event;
    this.trackerFiltersComponent.setPage(event)
  }

  onPerPageLimitChange(event) {

    this.trackerFiltersComponent.filters_obj.page = 1;
    this.trackerFiltersComponent.setItemsPerPage(this.config.itemsPerPage)
  }

  exportReport(exportType, active) {
    this.trackerFiltersComponent.setExportReport(exportType, active)
  }

  isNumeric(value) {
    return isNaN(value);
  }

  navigateToDetail(data, index) {

    this.headers.forEach((element) => {
      element['fake_id'] = guidGenerator()
    });
    let header = this.headers[index];


    const obj = {
      coach_id: data.coach_id,
      user_id: data.user_id,
      account_id: data.account_id,
      report_type: 'coaching_report',
      huddle_type: '',
      huddle_id: '',
      start_date: this.filterObj.start_date,
      end_date: this.filterObj.end_date,
      duration: this.filterObj.duration,
      path: getPathAndQuery(),
      header: [header]
    }

    this.localStorage.set(obj.report_type, this.headers)
    // let key = this.headers[index];
    // key = key.toLowerCase();
    // key = key.replace(/ /g, "_");
    this.headerService.reportsDetailData(obj);
    this.router.navigate(['trackers/report-detail-view/' + header.key], { queryParams: { query: encodeCompress(obj) } });

  }

  navigateToPlayerCard(user_id, account_id) {
    let queryParamsFrom = { displayName: 'reporting_coaching_report', url: this.router.url };
    let obj = { from: null };
    if (this.filterObj.updateClick) {
      obj = deepCopy(this.filterObj);
      obj.from = encodeCompress(queryParamsFrom);
    } else {
      obj.from = encodeCompress(queryParamsFrom);
    }
    this.router.navigate(['/analytics_angular/playercard', account_id, user_id, formatDateUtil(this.filterObj.start_date), formatDateUtil(this.filterObj.end_date)], { queryParams: obj });
  }

  setSortingOrder(value, order, index) {
    // console.log(value)
    if (value.sorting) {
      if (this.sort_by_order == '' && this.sort_by_order != 'DESC' && this.sort_by_order != 'ASC') {
        this.sortTable(value, 'ASC', index);
      }
      if (this.sort_by_order != '' && this.sort_by_order != 'DESC' && this.sort_by_order == 'ASC') {
        this.sortTable(value, 'DESC', index);
      }
      if (this.sort_by_order != '' && this.sort_by_order == 'DESC' && this.sort_by_order != 'ASC') {
        this.sortTable(value, '', index);
      }
    } else return false;
  }
}
