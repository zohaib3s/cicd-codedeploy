import { Injectable } from '@angular/core';
import { Router, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HeaderService } from '@app/services';
import { GLOBAL_CONSTANTS } from '@constants/constant';

@Injectable()
export class TrackerPermissionGuard implements CanActivate {

    constructor(private headerService: HeaderService, private router: Router) { }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot

    ): Observable<boolean> | Promise<boolean> | boolean {
        const headerData = this.headerService.getStaticHeaderData();
        if (Object.keys(headerData).length != 0 && headerData.constructor === Object) {

            return this.verifyPermission(headerData);
        } else { // else wait for header data and perform action
            return this.headerService.headerData$.pipe(map((headerData: any) => {
                return this.verifyPermission(headerData)
            }));
        }
    }

    private verifyPermission(headerData: any) {
        if (headerData) {
            if (headerData.tracker_permission && headerData.user_current_account.roles.role_id !== '120') {

                return true;
            } else if (headerData.tracker_permission && headerData.user_current_account.roles.role_id == '120' && headerData.is_user_coach) {


                this.router.navigate(['/trackers/coaching-report']);
                return false;
            } else if (headerData.user_current_account.roles.role_id == '120' && !headerData.is_user_coach) {
                this.router.navigate(['/page-not-found']);
                return false;
            }
            else if (!headerData.tracker_permission) {
                this.router.navigate(['/profile-page']);
                return false;
            }
        }


        // console.log("userCurrAcc: ", headerData.user_current_account)
        // if (Boolean(headerData.tracker_permission) && headerData.user_current_account.roles.role_id != GLOBAL_CONSTANTS.USER_ROLES.USER.ID) { // goals are enables for current account AND current user is not viewer
        //     console.log("userCurrAcc in")
        //     return true;
        // } else {
        //     this.router.navigate(['/profile-page']);
        //     return false;
        // }
    }


}