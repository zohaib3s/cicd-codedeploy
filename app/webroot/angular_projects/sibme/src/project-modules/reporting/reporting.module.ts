import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';

import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { NgxPaginationModule } from 'ngx-pagination';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    
  ],
  imports: [
    CommonModule,
    LoadingBarHttpClientModule,
    
    FormsModule,
    BsDropdownModule,
    NgxPaginationModule,
    ModalModule.forRoot(),
    
],
providers: []
})
export class ReportingModule {
}
