import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MomentModule } from 'ngx-moment';
import { ToastrModule } from 'ngx-toastr';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { QuillModule } from 'ngx-quill';


import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ModalModule } from "ngx-bootstrap/modal";
import { TabsModule } from "ngx-bootstrap/tabs";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { AlertModule } from 'ngx-bootstrap/alert';

import { MainService } from "./services";
import { CanDeactivateGuard } from "./helpers";

import {
  RootComponent, BreadcrumbsComponent, MainComponent, AssessmentHuddleFormComponent, AssessmentHuddleNewUserComponent,
  MoreSettingsDialogComponent, CoachingHuddleComponent, CollaborationHuddleComponent, HuddleTypeComponent, MoreSettingsComponent,
  NewUserComponent, AddHuddleComponent, AddCollaborationComponent, AddAssessmentComponent, LandingPageComponent
} from "./components";

export const __IMPORTS = [
  CommonModule,
  FormsModule,
  LoadingBarHttpClientModule,
  MomentModule,
  UiSwitchModule,
  TooltipModule.forRoot(),
  BsDropdownModule.forRoot(),
  ButtonsModule.forRoot(),
  BsDatepickerModule.forRoot(),
  TabsModule.forRoot(),
  ToastrModule.forRoot(),
  ModalModule.forRoot(),
  TypeaheadModule.forRoot(),
  AlertModule.forRoot(),
  QuillModule.forRoot()
];



export const __DECLARATIONS = [
  RootComponent,
  BreadcrumbsComponent,
  MainComponent,
  AssessmentHuddleFormComponent,
  CoachingHuddleComponent,
  CollaborationHuddleComponent,
  HuddleTypeComponent,
  MoreSettingsComponent,
  NewUserComponent,
  AddHuddleComponent,
  AddCollaborationComponent,
  AddAssessmentComponent,
  AssessmentHuddleNewUserComponent,
  MoreSettingsDialogComponent,
  LandingPageComponent
];

export const __ENTRY_COMPONENTS = [
  MoreSettingsDialogComponent
];
export const __PROVIDERS = [
  MainService,
  CanDeactivateGuard
];