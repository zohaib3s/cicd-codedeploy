import { Component, OnInit, TemplateRef, Input, OnChanges, OnDestroy } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { MainService } from '@addHuddle/services';
import { HeaderService, AppMainService } from "@app/services";

import * as _ from "underscore";
import { Subscription } from 'rxjs';

@Component({
  selector: 'more-settings',
  templateUrl: './more-settings.component.html',
  styleUrls: ['./more-settings.component.css']
})


export class MoreSettingsComponent implements OnInit, OnDestroy, OnChanges {

  modalRef: BsModalRef;
	public frameworks;
	public header_data;
	public translation: any = {};
	private subscription: Subscription;
	@Input('huddle_type') huddle_type;
	@Input('HuddleSettings') HuddleSettings;
	@Input('EditMode') EditMode;
	@Input() enable_framework_standard;
	@Input('cirqEnabled') cirqEnabled;
	constructor(private headerService:HeaderService, private appMainService: AppMainService, private modalService: BsModalService, private mainService:MainService) {
		this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
			this.translation = languageTranslation;
		});
	}

  ngOnInit() {
    this.header_data = this.headerService.getStaticHeaderData();
	  this.GetFrameworks();



  }

  ngOnChanges(change){


	  	if(change.HuddleSettings && change.HuddleSettings.currentValue.selected_framework && this.EditMode && this.frameworks && this.frameworks.length>0){
	  		let index = _.findIndex(this.frameworks, {account_tag_id: Number(change.HuddleSettings.currentValue.selected_framework)});
	  		if(index == -1){
	  			change.HuddleSettings.currentValue.selected_framework = 0;
	  			this.HuddleSettings.selected_framework = 0;
	  		}
	  	}

	  	if(!this.EditMode){
	  		this.HuddleSettings.show_publish_comments = true;
	  	}

	}

  ngAfterViewInit(){

  	if(!this.EditMode){

  		this.HuddleSettings.selected_framework = 0;

  	}else if(!this.HuddleSettings.selected_framework){

  		this.HuddleSettings.selected_framework = 0;

  	}

  }

  	private GetFrameworks(){

		this.appMainService.getFrameworks(this.header_data.user_current_account.accounts.account_id).subscribe((fr)=>{

				this.frameworks = fr;

			});
  	}

	public LoadMoreSettings(template:TemplateRef<any>, event){

		event.preventDefault();

		this.modalRef = this.modalService.show(template, { class: 'modal-lg' });
	}

	public resolve_settings(flag){

		if(flag==0){
			this.modalRef.hide();
		}else if(flag==1){

			this.modalRef.hide();
			this.mainService.PublishMoreSettings(this.HuddleSettings);
		}

	}

	ngOnDestroy(){
		this.subscription.unsubscribe();
		if(this.modalRef) this.modalRef.hide();
	  }



}
