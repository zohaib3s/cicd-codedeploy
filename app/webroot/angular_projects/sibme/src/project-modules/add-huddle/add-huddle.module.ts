import { NgModule } from '@angular/core';

import { __IMPORTS, __DECLARATIONS, __ENTRY_COMPONENTS, __PROVIDERS } from './components.barrel';
import { AddHuddleRoutes } from './add-huddle.routes';

import { AppLevelSharedModule } from '../shared/shared.module';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [__DECLARATIONS],
  imports: [__IMPORTS, AddHuddleRoutes, AppLevelSharedModule, DropdownModule],
  providers: [__PROVIDERS],
  entryComponents: [__ENTRY_COMPONENTS]
})
export class AddHuddleModule { }
