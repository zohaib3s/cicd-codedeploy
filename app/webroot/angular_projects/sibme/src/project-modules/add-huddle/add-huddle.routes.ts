import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RootComponent, MainComponent, AddHuddleComponent, AddCollaborationComponent, AssessmentHuddleFormComponent, AddAssessmentComponent } from './components';
import { CanDeactivateGuard } from "./helpers";
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import { SharedPageNotFoundComponent } from '../shared/shared-page-not-found/shared-page-not-found.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: '', component: RootComponent, children: [
      { path: 'home', component: LandingPageComponent },
      { path: 'coaching', component: AddHuddleComponent },
      { path: 'coaching/:folderId', component: AddHuddleComponent },
      { path: 'assessment', component: AssessmentHuddleFormComponent, canDeactivate: [CanDeactivateGuard], data: { pageMode: GLOBAL_CONSTANTS.PAGE_MODE.ADD } }, 
      { path: 'assessment/:folderId', component: AssessmentHuddleFormComponent, canDeactivate: [CanDeactivateGuard], data: { pageMode: GLOBAL_CONSTANTS.PAGE_MODE.ADD } }, 
      { path: 'assessment/edit/:huddleId', component: AssessmentHuddleFormComponent, canDeactivate: [CanDeactivateGuard], data: { pageMode: GLOBAL_CONSTANTS.PAGE_MODE.EDIT } }, 
      { path: 'collaboration', component: AddCollaborationComponent },
      { path: 'collaboration/:folderId', component: AddCollaborationComponent },
      { path: 'edit/:huddle_id', component: MainComponent }
    ]
  },
  { path: 'page_not_found', component: SharedPageNotFoundComponent },

  {
    path: '**',
    redirectTo: 'page_not_found',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

  exports: [RouterModule]
})
export class AddHuddleRoutes { }