import { FSUploaderDirective } from './directives';
import { UploadVideoComponent } from './components/uploadVideo/uploadVideo.component';
import { AppLevelSharedModule } from '@src/project-modules/shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CategoryModalComponent } from './components/categoryModal/categoryModal.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VideoLibraryRoutingModule } from './video-library-routing.module';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { HomeComponent } from './components/home/home.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { HttpClientModule } from '@angular/common/http';
import { NgSlimScrollModule, SLIMSCROLL_DEFAULTS } from 'ngx-slimscroll';

@NgModule({
  declarations: [ BreadcrumbsComponent, HomeComponent, SidebarComponent,
     CategoryModalComponent, UploadVideoComponent, FSUploaderDirective],
  imports: [
    CommonModule,
    FormsModule,
    VideoLibraryRoutingModule,
    HttpClientModule,
    ModalModule.forRoot(),
    NgSlimScrollModule,
    BsDropdownModule.forRoot(),
    AppLevelSharedModule,
    TooltipModule.forRoot(),
  ],
  entryComponents: [CategoryModalComponent, UploadVideoComponent],
  providers: [
    { provide: SLIMSCROLL_DEFAULTS, useValue: { alwaysVisible: false } }
  ]
})
export class VideoLibraryModule { }
