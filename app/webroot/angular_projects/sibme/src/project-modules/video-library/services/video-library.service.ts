import { BehaviorSubject, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HeaderService } from '@src/project-modules/app/services';
import { environment } from '@src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class VideoLibraryService {

  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/json"
    })
  };

  public _videoLibararyGlobalObeservable = new Subject<any>();
  
  constructor(private http: HttpClient, private headerService: HeaderService) { }

  public getLibraryData(obj) {

    let path = environment.APIbaseUrl + "/getLibraryData";
    return this.http.post(path, obj);
  }

  public addCategories(obj) {

    let path = environment.APIbaseUrl + "/add_categories";
    return this.http.post(path, obj);
  }
  public uploadVideo(obj) {
    let path = environment.APIbaseUrl + "/uploadLibraryVideo";
    return this.http.post(path, obj, this.httpOptions);
  }

}
