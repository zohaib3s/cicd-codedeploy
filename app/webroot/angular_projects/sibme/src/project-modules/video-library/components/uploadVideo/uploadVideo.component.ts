import { ShowToasterService } from '@projectModules/app/services';
import { VideoLibraryService } from './../../services/video-library.service';
import { Component, OnInit, Input, EventEmitter, OnDestroy } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { HeaderService } from '@src/project-modules/app/services';
import { SlimScrollEvent, ISlimScrollOptions } from 'ngx-slimscroll';

@Component({
  selector: 'uploadVideo',
  templateUrl: './uploadVideo.component.html',
  styleUrls: ['./uploadVideo.component.css']
})
export class UploadVideoComponent implements OnInit, OnDestroy {
  maxChars = 150;
  categories: any;
  user: any;
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  subCategories: any = {};
  sessionData: any;
  videoTitle: any;
  videoDescription: any;
  public translation: any = {};
  private translationSubscription: Subscription;
  
  selectedCategories: any = [];
  constructor(public bsModalRef: BsModalRef, private videoLibraryService: VideoLibraryService,
    private headerService: HeaderService, private toastr: ShowToasterService) {
    this.translationSubscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
  }
  ngOnInit() {

    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
        position: 'right',
        barBackground: '#C9C9C9',
        barOpacity: '0.8',
        barWidth: '10',
        barBorderRadius: '20',
        barMargin: '0',
        gridBackground: '#D9D9D9',
        gridOpacity: '1',
        gridWidth: '0',
        gridBorderRadius: '20',
        gridMargin: '0',
        alwaysVisible: true,
        visibleTimeout: 1000,
    };
    this.categories = this.categories.map(x => ({ ...x, isChecked: false, nocategoryCheck:false, nocategoryName:this.translation.vl_no_subcategory }));

    this.categories.forEach(parent => {
      if (parent.childs.length > 0) {
        parent.childs.forEach(child => {
          child.isChecked = false;
          child.parentId = parent.subject_id;
        });
      }
    });
  }

  checkCategory(category, parentCategory?) {
    category.isChecked = !category.isChecked;
    if (category.isChecked) {
        this.selectedCategories.push(category.subject_id);
        if (category.hasOwnProperty('parentId')) {
        if(parentCategory) parentCategory.nocategoryCheck = false;
            if (!this.selectedCategories.includes(category.parentId)) {
                let parent= this.categories.find(x => x.subject_id === category.parentId);
                parent.isChecked=category.isChecked;
                parent.nocategoryCheck=false;
                this.selectedCategories.push(parent.subject_id)
            }
        }
      else {
        category.nocategoryCheck = !category.nocategoryCheck;
        category.isChecked = category.nocategoryCheck;
      }
    } else {
        this.selectedCategories = this.selectedCategories.filter(id => id !== category.subject_id);
        if (category.hasOwnProperty('childs')) {
            category.nocategoryCheck = false;
            if (category.childs.length > 0) {
                category.childs.forEach(child => {
                    this.selectedCategories = this.selectedCategories.filter(id => id !== child.subject_id);
                    child.isChecked = false;
                });
            }
        }
      if (category.hasOwnProperty('parentId')) {
        if (parentCategory.childs.length > 0) {
          let result = parentCategory.childs.filter(x=> x.isChecked)
          if(result.length <= 0 && parentCategory.isChecked) parentCategory.nocategoryCheck = true
    }
  }
  
    }
}


checkNoCategory(category, ischecked){
  category.isChecked =ischecked;
  category.nocategoryCheck = ischecked;

  if(ischecked && category.childs.length > 0){
    category.childs.forEach(child => {
      this.selectedCategories = this.selectedCategories.filter(id => id !== child.subject_id);
      child.isChecked = false;
    });
  }

  if (category.isChecked) {    
    this.selectedCategories.push(category.subject_id);
  }
  else{
    this.selectedCategories = this.selectedCategories.filter(id => id !== category.subject_id);
  }

}

  public onMediaUpload(event) {
    if (event.from && event.files.length) {
      let that = this;
      for (let file_key in event.files) {
        if (event.files.hasOwnProperty(file_key)) {
          let file = event.files[file_key];
          console.log("response from filestack ->", file)
          let obj: any = {};
          obj.user_current_account = that.sessionData.user_current_account;
          obj.account_folder_id = null;
          obj.huddle_id = null;
          //fileStack handle key added 
          obj.fileStack_handle = file.handle;
          //filestack url added
          obj.fileStack_url = file.url; 
          obj.account_id = that.sessionData.user_current_account.accounts.account_id;
          obj.site_id = that.sessionData.site_id;
          obj.user_id = that.sessionData.user_current_account.User.id;
          obj.current_user_role_id = that.sessionData.user_current_account.roles.role_id;
          obj.current_user_email = that.sessionData.user_current_account.User.email;
          obj.suppress_render = false;
          obj.suppress_success_email = false;
          obj.library = true;
          obj.activity_log_id = "";
          obj.direct_publish = event.from == "Upload";
          obj.video_file_name = file.filename;
          obj.stack_url = file.url;
          obj.video_url = file.key;
          obj.video_id = "";
          obj.video_file_size = file.size;
          obj.direct_publish = true;
          obj.video_title = this.videoTitle;
          obj.video_desc = this.videoDescription;
          obj.category = this.selectedCategories;
          obj.url_stack_check = 1;
          this.bsModalRef.hide();
          this.videoLibraryService.uploadVideo(obj).subscribe((data: any) => {
            let d: any = data;
            let type = this.headerService.isAValidAudio(d.data.file_type);
            if (type == false) {
              this.toastr.ShowToastr('info',`${this.translation.workspace_newvideouploaded}`);
            }
            else {
              this.toastr.ShowToastr('info',this.translation.workspace_new_audio_uploaded);
            }

          }, error => {
          });
        }

      }
    }
  }
    ngOnDestroy() {
        this.translationSubscription.unsubscribe();
    }
}

