import { ShowToasterService } from '@projectModules/app/services';
import { VideoLibraryService } from './../../services/video-library.service';
import { Component, OnInit, TemplateRef, EventEmitter, OnDestroy, ViewChild, Input } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { Subscription } from "rxjs/index";
import { HeaderService } from "@app/services";

@Component({
  selector: 'categoryModal',
  templateUrl: './categoryModal.component.html',
  styleUrls: ['./categoryModal.component.css']
})
export class CategoryModalComponent implements OnInit, OnDestroy {

  user: any;
  categories = [];
  subCategory;
  category;
  hideModal: boolean = false;
  deleted_categories = [];
  subCategories: any = {};
  disableBtn = false;
  count = 0;
  isRenameCategory = false;
  isSubCategorySelected = false;
  isRenameSubCategory = false;
  opts: ISlimScrollOptions;
  modalRef: BsModalRef;
  confirmDelete: boolean = false;
  @ViewChild('confirmDialog', { static: false }) confirmDialog;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  renameCategory: any = '';
  public Inputs: GeneralInputs;
  renameSubCategory: any = '';
  public translation: any = {};
  private translationSubscription: Subscription;
  constructor(public bsModalRef: BsModalRef, private headerService: HeaderService, private modalService: BsModalService, private videoLibraryService: VideoLibraryService, private toastr: ShowToasterService) {
    this.translationSubscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.Inputs = { NewFolderName: "", Confirmation: "", ConfirmationKey: this.translation.Huddle_confirmation_bit };
    });
  }

  ngOnInit() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    };
  }
  addSubCategory() {
    if (this.subCategories.subject_id) {
      this.categories.find(x => x.subject_id === this.subCategories.subject_id).
        childs.unshift({ name: this.subCategory, subject_id: this.getNewId() });
      this.subCategory = '';
    }
  }
  addCategory() {
    this.categories.unshift({ name: this.category, subject_id: this.getNewId(), childs: [] });
    this.category = '';
  }
  getNewId() {
    this.count = this.count - 1;
    return this.count;
  }
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, {
      backdrop: 'static'
    });
    this.hideModal = !this.hideModal;
  }
  save() {
    this.disableBtn = true;
    let obj = {
      user_id: this.user.user_id,
      account_id: this.user.account_id,
      categories: this.categories,
      deleted_categories: this.deleted_categories,
      library: 1
    };

    this.videoLibraryService.addCategories(obj).subscribe((data: any) => {
      if (data.status) {
        this.toastr.ShowToastr('success',this.translation.vl_category_managed);
        this.bsModalRef.hide();
      } else {
        this.toastr.ShowToastr('error',data);
      }
    }, err => {
      console.log('addCategories api error', err);
    }
    );
  }


  setRenameCategory(category) {
    this.isRenameCategory = true;
    this.renameCategory = _.cloneDeep(category);
  }

  setRenameSubCategory(category) {
    this.isRenameSubCategory = true;
    this.renameSubCategory = _.cloneDeep(category);
  }
  updateCategory() {
    this.categories.find(x => x.subject_id === this.renameCategory.subject_id ? x.name = this.renameCategory.name : '');
    this.isRenameCategory = false;
  }
  updateSubCategory() {
    this.subCategories.childs.find(x => x.subject_id === this.renameSubCategory.subject_id ? x.name = this.renameSubCategory.name : '');
    this.isRenameSubCategory = false;
  }

  deleteCategory(category) {
    //this.Inputs.Confirmation=null;
    if (this.Inputs.ConfirmationKey != this.Inputs.Confirmation) {
      this.toastr.ShowToastr('info',this.translation.huddle_you_typed_in + " '" + this.Inputs.ConfirmationKey + "' ");
      return;
    } else {
      this.modalRef.hide();
      this.hideModal = false;
      this.Inputs.Confirmation = null;
      this.categories = this.categories.filter(x => x.subject_id !== category.subject_id);
      this.subCategories = {};
      if (category.subject_id > 0) {
        this.deleted_categories.push(category.subject_id);

      }
    }
  }
  public ResolveDeleteFile(flag: boolean, category) {
    console.log(category)
    if (this.Inputs.ConfirmationKey != this.Inputs.Confirmation) {
      this.toastr.ShowToastr('info',this.translation.huddle_you_typed_in + " '" + this.Inputs.ConfirmationKey + "' ");
      return;
    } else {
      if (flag) {
        this.modalRef.hide();
        this.confirmDelete = true;
      }
    }
  }

  deleteSubCategory(subCategory) {

    if (this.Inputs.ConfirmationKey != this.Inputs.Confirmation) {
      this.toastr.ShowToastr('info',this.translation.huddle_you_typed_in + " '" + this.Inputs.ConfirmationKey + "' ");
      return;
    } else {
      this.modalRef.hide()
      this.hideModal = false;
      this.Inputs.Confirmation = null;
      this.subCategories.childs = [...this.subCategories.childs.filter(x => x.subject_id !== subCategory.subject_id)];
      this.categories.forEach(x => {
        if (x.subject_id === this.subCategories.subject_id) {
          x = this.subCategories;
        }
      });
      if (subCategory.subject_id > 0) {
        this.deleted_categories.push(subCategory.subject_id);
      }
    }
  }
  IsScrollToBottom(parent, child, item){
    let totalItem = parent.length;
    let p =[...parent]
    let ind =p.findIndex(x=> x.subject_id==child.subject_id);
    if(totalItem-ind <=3){
      let element
      if(item=='category') element = (<HTMLElement>document.querySelector(".category-bottom"));
      else  if(item=='subCategory')  element = (<HTMLElement>document.querySelector(".subcategory-bottom"));
        
      setTimeout(() => {
        element.scrollIntoView({ behavior: 'smooth' });
      }, 300);
    }
  }
  ngOnDestroy() {
    this.translationSubscription.unsubscribe();
  }
}
interface GeneralInputs {
  NewFolderName: string,
  Confirmation: string,
  ConfirmationKey: string
}
