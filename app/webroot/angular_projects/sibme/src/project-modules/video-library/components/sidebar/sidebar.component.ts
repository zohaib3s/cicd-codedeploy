import { VideoLibraryService } from './../../services/video-library.service';

import { CategoryModalComponent } from './../categoryModal/categoryModal.component';
import { Component, OnInit, Input, TemplateRef, OnChanges, OnDestroy } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { Subscription } from "rxjs/index";
import { HeaderService } from "@app/services";
@Component({
  selector: 'video-library-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnChanges,OnDestroy {
  @Input('loading') loading: boolean = true;
  @Input('uncategorized') uncategorized: any = {};
  @Input('categories') categories: any = {};
  @Input('user') user: any;
  @Input('totalVideos') totalVideos: any;
  @Input('original_account') original_account: boolean;
  private subscriptions: Subscription = new Subscription();
  bsModalRef: BsModalRef;
  // categories: any;
  subject_ids: any = [];
  sessionData: any;
  except_categories = [];
  isCategoriesProcessed = false;
  domain = 'all';
  checkUncategorized: boolean;
  public translation: any = {};
  constructor(private modalService: BsModalService, private headerService: HeaderService, private videoLibraryService: VideoLibraryService) {
    this.subscriptions.add( this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    }));
  }

  ngOnInit() {
    this.checkUncategorized = false;
      this.sessionData = this.headerService.getStaticHeaderData();
    let subscription = this.videoLibraryService._videoLibararyGlobalObeservable.subscribe(res => {
      if (res.triggerFromHome) {
        if (res.triggerFromHome.updatedCategoriesCount) {
          this.updateCategoriesCount(res.triggerFromHome.updatedCategoriesCount);
        }
        if (res.triggerFromHome.updatedCategories) {

          let data = res.triggerFromHome.updatedCategories;

          this.updateCategory(data.updated);
          this.deleteCategory(data.deleted);
          this.processNoCategory(res.triggerFromHome.sidebar_categories);

          if (data.added) {
            data.added.forEach(newAdded => {
              if (newAdded.hasOwnProperty('parent_subject_id')) {
                this.categories.forEach(parent => {
                  if (parent.subject_id === newAdded.parent_subject_id) {
                    // delete newAdded.parent_subject_id;
                    newAdded.isChecked = false;
                    newAdded.parentId = parent.subject_id;
                    parent.childs.push(newAdded);
                  }
                });
              } else {
                newAdded = this.processCategories([newAdded]);
                this.categories.push(newAdded[0]);
              }
            });
          }
        }
        setTimeout(() => {
          this.categories.forEach(parent => {
            parent.childs.sort((a, b) => a.name.localeCompare(b.name));
          });
          this.categories.sort((a, b) => a.name.localeCompare(b.name));
        }, 500);
      }
    });
    this.subscriptions.add(subscription);
  }

  ngOnChanges() {

      this.categories = this.processCategories(this.categories);
  }

  lsCollapseSetting(category){
    let lsCategory = this.headerService.getLocalStorage('lsCollapse');
    let index = lsCategory.findIndex(x => x.subject_id == category.subject_id);
    if(index>-1){
      if(lsCategory[index].toggle)
      lsCategory[index].toggle=false;
      else
      lsCategory[index].toggle=true;
    }
    else{
      let obj={subject_id:category.subject_id, name:category.name, toggle:false}
      lsCategory.push(obj);
    }

    this.headerService.setLocalStorage('lsCollapse', lsCategory);
  }
  processCategories(categories) {
    if (categories) {
      categories= categories.map(x => ({
        ...x,  isChecked: false, isUncategorizeVideosCheck: false
      }));
      categories.forEach(parent => {
        if (parent.childs.length > 0) {
          let count = 0;
          parent.childs.forEach(child => {
            child.isChecked = false;
            child.parentId = parent.subject_id;
            count += child.count;
          });
        }
      });
      this.isCategoriesProcessed = true;
      return categories;
    }

  }
  sortSubCategory(subCategory?, category?) {
    subCategory.isChecked = !subCategory.isChecked;
    if (!subCategory.isChecked) {
      this.subject_ids = this.subject_ids.filter(x => x !== subCategory.subject_id);
      if (category !== undefined) {
        if (!category.isUncategorizeVideosCheck) {
          this.subject_ids = this.subject_ids.filter(x => x !== category.subject_id);
        }
        category.isChecked = subCategory.isChecked;
        if (category.isUncategorizeVideosCheck) {
          this.except_categories.push(subCategory.subject_id);
        }
      }
    } else {
      this.subject_ids.push(subCategory.subject_id);
      this.except_categories = this.except_categories.filter(x => x != subCategory.subject_id);
      if (category !== undefined) {
        this.checkAll(category, subCategory.isChecked);
      }
    }
    this.subject_ids = [...new Set(this.subject_ids.map(x => x))];
    this.triggerData();
  }

  deleteCategory = (deletedData) => {
    setTimeout(() => {
      console.log("called")
      deletedData.forEach(deletedId => {
        this.subject_ids = this.subject_ids.filter(x=>x !== deletedId);
        this.except_categories = this.except_categories.filter( x=>x!== deletedId);
        this.categories = this.categories.filter(x => {
          if (x.subject_id !== deletedId) {
            if (x.childs.length > 0) {
              x.childs = x.childs.filter(child => child.subject_id !== deletedId);
            }
            return true;
          } else {
            return false;
          }
        });
      });
    }, 500);
  }
  processNoCategory(sidebar){
    if(sidebar){
      setTimeout(() => {
          this.categories.forEach(element => {
            sidebar.forEach(x => {
              if(x.subject_id && element.subject_id == x.subject_id){
                element.no_category_count = x.no_category_count
              }
            });
          });
        }, 500);
    }
  }
  unCheckAll() {
    this.checkUncategorized = false;
    this.categories.forEach(parent => {
      parent.isChecked = false;
      parent.isUncategorizeVideosCheck = false;
      if (parent.childs.length > 0) {
        parent.childs.forEach(child => {
          child.isChecked = false;
        });
      }
    });
    this.subject_ids = [];
    this.except_categories = [];
    this.domain = 'all';
    this.triggerData();
  }
  updateCategory = (updatedData) => {
    setTimeout(() => {
      for (let parentIndex = 0; parentIndex < updatedData.length; parentIndex++) {
        this.categories[parentIndex].name = updatedData[parentIndex].name;
        if (this.categories[parentIndex].childs.length > 0) {
          for (let childIndex = 0; childIndex < this.categories[parentIndex].childs.length; childIndex++) {
            if(updatedData[parentIndex].childs[childIndex]){
            this.categories[parentIndex].childs[childIndex].name = updatedData[parentIndex].childs[childIndex].name;
          }
        }
      }
    }
    }, 500);
  }
  updateCategoriesCount = (updatedData) => {
    setTimeout(() => {
      for (let parentIndex = 0; parentIndex < this.categories.length; parentIndex++) {
        let count = 0;
        this.categories[parentIndex].count = updatedData[parentIndex].count;
        this.categories[parentIndex].no_category_count =  updatedData[parentIndex].no_category_count;
        if (this.categories[parentIndex].childs.length > 0) {
          for (let childIndex = 0; childIndex < this.categories[parentIndex].childs.length; childIndex++) {
            if (this.categories[parentIndex].childs.length > 0) {
            this.categories[parentIndex].childs[childIndex].count = updatedData[parentIndex].childs[childIndex].count;
            count +=  this.categories[parentIndex].childs[childIndex].count;
          }
        }

        }
      }
    }, 500);
  }

  checkAll(category?, isChecked?) {
    if (isChecked) {
      let checkedChilds = [];
      category.childs.forEach(child => {
        if (child.isChecked) {
          checkedChilds.push(child.subject_id);
        }
      });
      if (checkedChilds.length === category.childs.length && category.isUncategorizeVideosCheck) {
        category.isChecked = isChecked;
        this.subject_ids.push(category.subject_id);
      }
    } else {
      category.isChecked = isChecked;
      this.subject_ids = this.subject_ids.filter(x => x !== category.subject_id);
    }
  }
  sortCategory(category?) {
    category.isChecked = !category.isChecked;
    if (category.isChecked) {
      this.subject_ids.push(category.subject_id);
    }
    if (category.childs.length > 0) {
      category.isUncategorizeVideosCheck = category.isChecked;
      category.childs.forEach(child => {
        child.isChecked = category.isChecked;
        if (child.isChecked) {
          this.subject_ids.push(child.subject_id);
          if (category.isUncategorizeVideosCheck) {
            this.except_categories = this.except_categories.filter(x => x !== child.subject_id);
          }
        } else {
          this.subject_ids = this.subject_ids.filter(x => x !== child.subject_id && x !== category.subject_id);
          if (category.isUncategorizeVideosCheck) {
            this.except_categories.push(child.subject_id);
          }
        }
      });
    }
    this.subject_ids = [...new Set(this.subject_ids.map(x => x))];
    this.triggerData();
  }

  sortUncategorized(category) {
    category.isUncategorizeVideosCheck = !category.isUncategorizeVideosCheck;
    if (category.childs.length > 0) {
      category.childs.forEach(child => {
        if (category.isUncategorizeVideosCheck) {
          if (!this.subject_ids.includes(child.subject_id)) {
            this.except_categories.push(child.subject_id);
          }
        } else {
          this.except_categories = this.except_categories.filter(x => x !== child.subject_id);
        }
      });
      if (!category.isUncategorizeVideosCheck) {
        this.subject_ids = this.subject_ids.filter(x => x !== category.subject_id);
      } else {
        this.subject_ids.push(category.subject_id);
      }
      this.checkAll(category, category.isUncategorizeVideosCheck);
      this.triggerData();
    }

  }
  openCategoryModal() {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'categoryModal-container',
      initialState: {
        user: this.user,
        categories: _.cloneDeep(this.categories)
      }
    };
    this.bsModalRef = this.modalService.show(CategoryModalComponent, config);
  }


  getUncategorizedVideo() {
    this.checkUncategorized = !this.checkUncategorized;
    this.checkUncategorized ?
      this.domain = 'uncategorized' : this.domain = 'all';
    this.triggerData();
  }

  triggerData() {
    this.videoLibraryService._videoLibararyGlobalObeservable.next({
      triggerFromSidebar: {
        except_categories: this.except_categories,
        subject_id: this.subject_ids,
        domain: this.domain
      }
    });
  }
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}
