import { Router, ActivatedRoute } from '@angular/router';
import { UploadVideoComponent } from './../uploadVideo/uploadVideo.component';
import { Subscription, Subject } from 'rxjs';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { Component, OnInit, TemplateRef, HostListener, OnDestroy } from '@angular/core';
import { HeaderService, SocketService } from '@projectModules/app/services';
import { VideoLibraryService } from '../../services/video-library.service';
import { ShowToasterService } from '@projectModules/app/services';
import * as _ from 'lodash';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { MainService } from '@src/project-modules/video-page/services';
import { environment } from "@environments/environment";

@Component({
  selector: 'video_library-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  sessionData: any = {};
  user: any;
  videosList: any = [];
  sidebarObj: { uncategorized: any; subjects: any; };
  private projectTitle: string = 'workspace';
  modalRef: BsModalRef;
  isSorted = false;
  count = 0;
  totalVideos = 0;
  videoCount = 0;
  accountSelectElement : HTMLElement;
  currentVideos = 0;
  page = 0;
  public socket_listener: any;
  isLoading = true;
  isLoadingSidebar = true;
  selectedAccount;
  selectedCategoris;
  userAccounts;
  selectedLibraryAccountId = -1;
  public isIEOpened = false;
  public original_account:boolean = false;
  public orderBy = 'created_date';
  private searchInput: Subject<string> = new Subject();
  librarySubscription: Subscription = new Subscription();
  subscription: Subscription = new Subscription();
  public additionalData: any = {};
  private styles = [
    { id: 'libraryCustom', path: 'assets/video-library/css/custom.css' },
    { id: 'workspaceAnimations', path: 'assets/workspace/css/animations.css' },
    { id: 'videoHuddleCustom2', path: 'assets/video-huddle/css/custom-2.css' },
  ];
  uploadVideoModalRef: BsModalRef;
  public searchString = '';
  public translation: any = {};
  private translationSubscription: Subscription;

  constructor(private toastr: ShowToasterService, private videoLibraryService: VideoLibraryService,
    private modalService: BsModalService, private headerService: HeaderService, private router: Router,
    private activatedRoute: ActivatedRoute, private socketService: SocketService, public mainService: MainService,) {
    this.translationSubscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
    this.mainService.vlSortOnRename.subscribe(docId=>{
      if( this.orderBy==='updated_date'){
        let index = this.videosList.findIndex(x=> x.document_id==docId);
        let item = this.videosList.find(x => x.document_id==docId);
        this.videosList.splice(index, 1);
        this.videosList.unshift(item);
        }
    });

  }
  ngOnInit() {
    this.styles.forEach(style => {
      this.loadCss(style);
    });
    this.sessionData = this.headerService.getStaticHeaderData();
    if (Object.keys(this.sessionData).length != 0 && this.sessionData.constructor === Object) {
      if(!(this.sessionData.user_current_account.users_accounts.role_id == 100 || this.sessionData.user_current_account.users_accounts.role_id == 110  || this.sessionData.user_current_account.users_accounts.permission_access_video_library == 1))
      {
          this.router.navigate(["/page_not_found"]);
      }
      this.SubscribeSearch();

      this.additionalData = {
        users_accounts: this.sessionData.user_current_account.users_accounts
      };
      this.activatedRoute.queryParams.subscribe(params => {
        if (params.search) {
          this.searchString = params.search;
          this.user.title = params.search;
        }
        if (params.orderBy) {
          this.orderBy = params.orderBy;
        }
        if (params.id) {
          this.headerService.selectedLibAcct = params.id;
        }
      });
      this.user = {
        account_id: this.sessionData.user_current_account.users_accounts.account_id,
        user_id: this.sessionData.user_current_account.users_accounts.user_id,
        user_current_account: this.sessionData.user_current_account,
        subject_id: '',
        domain: 'all',
        page: this.page,
        title: this.searchString,
        except_categories: [],
        order_by: this.orderBy
      };
      if(this.headerService.selectedLibAcct >-1){
        this.user.account_id=this.headerService.selectedLibAcct;
        this.selectedLibraryAccountId= this.user.account_id;
        this.router.navigate([], {queryParams: { id: this.user.account_id }, queryParamsHandling: 'merge'});
      }
      this.selectedAccount = this.user.account_id.toString();

      this.getLibraryData(this.user);
      this.subscription = this.videoLibraryService._videoLibararyGlobalObeservable.subscribe(res => {
        if (res.triggerFromSidebar) {
          let data = res.triggerFromSidebar;
          this.isSorted = true;
          this.resetPageValues();
          this.user.except_categories = data.except_categories;
          this.user.domain = data.domain;
          this.user.subject_id = data.subject_id.toString();
          this.selectedCategoris = data.subject_id;
          this.getLibraryData(this.user);
        }
      });
      let channel_name = `library-${this.sessionData.user_current_account.users_accounts.account_id}`;
      this.socket_listener = this.socketService.pushEventWithNewLogic(channel_name).subscribe(data => this.processEventSubscriptions(data));
    }
  }
  @HostListener('window:scroll', [])
  onScroll(): void {
    let doc = document.documentElement;
    let currentScroll =
      (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
    if (((window.innerHeight + window.scrollY) >= document.body.offsetHeight) && (this.currentVideos < this.videoCount)
      && !this.isLoading) {
      if (this.page === 0) {
        window.scroll(0, currentScroll - 100);
      } else {
        this.setScroll();

        if (
          window.innerHeight + window.pageYOffset >=
          document.body.offsetHeight - 2
        ) {
          window.scroll(0, currentScroll - 100);
        }
      }
      this.user.page = ++this.page;
      this.getLibraryData(this.user);
    }
  }
  setScroll() {
    window.scroll(0, document.body.offsetHeight - ((9 / 100) * document.body.offsetHeight)
    );
  }

  createAccountSelectElement()
  {
      this.accountSelectElement = document.getElementById("account_selection") as HTMLElement;
      this.accountSelectElement.addEventListener("blur", ()=>{
          this.accountSelectElement.classList.remove("clicked_once");
      });
  }

  getLibraryData(obj) {
    this.isLoading = true;
    if (this.librarySubscription) {
      this.librarySubscription.unsubscribe();
    }
    this.librarySubscription = this.videoLibraryService.getLibraryData(obj).subscribe((res: any) => {

      if (res.status) {
        this.videosList = [...this.videosList, ...res.data.get_videos];
        this.userAccounts = res.data.user_accounts;
        this.videoCount = res.data.total_videos;
        if (!this.isSorted) {
          if(this.searchString == '')
          this.totalVideos = res.data.total_videos;
          this.sidebarObj = {
            uncategorized: res.data.uncat_videos || 0,
            subjects: res.data.subjects || []
          };
          // Saving Category Collapse and Expand setting in Local Storage
          let uniqueElement=[]
          this.sidebarObj.subjects.forEach(x => { x.toggle=true; uniqueElement.push(x.subject_id) });
          let lsCategory =this.headerService.getLocalStorage('lsCollapse');
          let tempArr=[];
          if(lsCategory){
            this.sidebarObj.subjects.forEach(item => {
              let index = lsCategory.findIndex(x => x.subject_id == item.subject_id);
              if(index!= -1)
                item.toggle=lsCategory[index].toggle;
              else{
                lsCategory.forEach(x => {tempArr.push(x.subject_id)});
                uniqueElement=uniqueElement.filter(id=> !tempArr.includes(id))
                uniqueElement.forEach(subject_id => {
                  let obj ={ subject_id:subject_id, toggle:true};
                  lsCategory.push(obj);
                });
                this.headerService.setLocalStorage('lsCollapse',lsCategory);
              }
            });
          }

          else{
            let arr=[];
            this.sidebarObj.subjects.forEach(element =>{
              let obj ={ subject_id:element.subject_id, name:element.name, toggle:true}
              arr.push(obj);
            } );
            this.headerService.setLocalStorage('lsCollapse',arr);
          }
        }
        if(this.user.account_id != res.data.account_id)
        {
            this.socket_listener.unsubscribe();
            let channel_name = `library-${res.data.account_id}`;
            this.socket_listener = this.socketService.pushEventWithNewLogic(channel_name).subscribe(data => this.processEventSubscriptions(data));
        }
        this.selectedAccount = this.userAccounts.filter(x => x.id == this.user.account_id.toString());
        if(this.userAccounts.length==1 && !this.original_account)
        {
          this.selectedAccount = this.userAccounts;
        }
        this.currentVideos = this.videosList.length;
        this.isLoading = false;
        this.isLoadingSidebar = false;
        this.original_account = res.data.original_account;
      } else {
        this.toastr.ShowToastr('info',this.translation.u_dont_permission_vd);
        setTimeout(() => location.href = environment.baseUrl, 1000);
        return;
      }
    }, err => {
      console.log("library data error", err);
    });

  }

  openUploadVideoModal() {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'uploadVideo-container',
      initialState: {
        user: this.user,
        categories: _.cloneDeep(this.sidebarObj.subjects),
        sessionData: this.sessionData
      }
    };
    this.uploadVideoModalRef = this.modalService.show(UploadVideoComponent, config);
  }

  accountChange() {
    this.isSorted = false;
    this.resetPageValues();
    this.user.account_id = this.selectedLibraryAccountId;
    this.getLibraryData(this.user);
    this.headerService.selectedLibAcct=this.user.account_id;
    this.router.navigate([], { queryParams: { id: this.user.account_id }, queryParamsHandling: 'merge'});
    if(!this.accountSelectElement)
    {
        this.createAccountSelectElement();
    }
    this.accountSelectElement.blur();
    setTimeout(()=>{
        this.accountSelectElement.classList.remove("clicked_once");
    },1000)

  }
    removeFocus(){
      if(!this.accountSelectElement)
      {
          this.createAccountSelectElement();
      }
      if(this.accountSelectElement.classList.contains("clicked_once"))
      {
          this.accountSelectElement.blur();
      }
      else
      {
          this.accountSelectElement.classList.add("clicked_once");
      }
    }

  seletedAccount(selectedAccountId) {
    this.selectedLibraryAccountId = selectedAccountId;
    this.selectedAccount = this.userAccounts.filter(x => x.id == this.selectedLibraryAccountId);
  }
  public OnSearchChange(event) {
    this.searchInput.next(event);

  }
  private SubscribeSearch() {
    this.searchInput
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(value => {
        this.user.title = this.searchString;
        this.resetPageValues();
        this.getLibraryData(this.user);
        if (this.searchString) {
          this.router.navigate([], {
            queryParams: { search: this.searchString }, queryParamsHandling: 'merge'
          });
        }
        else {
          this.router.navigate([], {
            queryParams: { search: null }, queryParamsHandling: 'merge'
          });
        }
      });
  }
  changeOrderBy(orderBy) {
    this.orderBy = orderBy;
    this.resetPageValues();
    this.user.order_by = this.orderBy;
    this.getLibraryData(this.user);
  }

  resetPageValues() {
    this.videosList = [];
    this.page = 0;
    this.user.page = this.page;
  }



  private processEventSubscriptions(data) {
    console.log("Websocket : ", data);
    switch (data.event) {
      case "resource_added":
        let should_wait = 0;
        if (data.is_video_crop) {
          should_wait = 1;
        }
        this.sidebarObj.uncategorized = data.uncat_videos;
        this.processCategoriesCount(data.sidebar_categories);
        let result = this.processVideosCount(data.video_categories);
        if (result) {
          this.processResourceAdded(data.data, should_wait);
        }
        break;
      case "resource_renamed":
        this.processResourceRenamed(data.data, data.is_dummy);
        break;
      case "categories":
        this.sidebarObj.uncategorized = data.uncat_videos;
        this.processCategories(data.data, data.sidebar_categories);
        this.refreshPage(data);
        this.addCategorytoLS(data)
        break;
      case "resource_deleted":
        this.sidebarObj.uncategorized = data.uncat_videos;
        this.totalVideos -= 1;
        this.processResourceDeleted(data.data, data.deleted_by, data.sidebar_categories);
        break;
      case "video_description":
        this.processVideoDescription(data.data, data.video_id);
        break;
      case "video_category":
        this.sidebarObj.uncategorized = data.uncat_videos;
        this.processVideoCategory(data.data, data.video_id, data.sidebar_categories);
        break;
      default:
        break;
    }
  }
  private processResourceAdded(resource, should_wait) {

    let that = this;
    let wait_time = 0;
    if (should_wait) {
      wait_time = 12000;
      resource.published = 0;
    }
    that.videosList.unshift(resource);
    this.totalVideos++;

  }
  refreshPage(data) {
    // this function checks if deleted category was checked in sidebar if true then it recall the api with existing checks
    if (data.data.deleted.length > 0) {
      let deletedCatgories = data.data.deleted;
      let result = this.findCommonElements(this.selectedCategoris, deletedCatgories);
      if (this.user.domain == 'uncategorized') {
        console.log('uncate');
        this.resetPageValues();
        this.getLibraryData(this.user);
      } else
     if(result){
       console.log('result: ', result);
       this.user.domain = 'all';
       this.user.subject_id = '';
       this.isSorted = false;
       this.isLoadingSidebar = true;
       this.user.except_categories = [];
       this.resetPageValues();
       this.getLibraryData(this.user);
     }

    }
  }
  private processResourceDeleted(resource_id, deleted_by, sidebarcategories) {
    if (resource_id && deleted_by) {
      var indexOfMyObject = this.videosList.findIndex(x => {
        return x.doc_id == resource_id || x.id == resource_id;
      });
    }
    if (indexOfMyObject > -1) {
      let obj = this.videosList[indexOfMyObject];
      this.videosList.splice(indexOfMyObject, 1);
      this.videoCount -= 1;
    }
    this.processCategoriesCount(sidebarcategories);

  }
  private processResourceRenamed(resource, is_dummy?) {

    let objResource = _.find(this.videosList, function (item) {
      return ((parseInt(item.id) == parseInt(resource.id)) || parseInt(item.doc_id) == parseInt(resource.doc_id));
    });
    let index = -1;
    this.videosList.forEach((item, i) => {
      if ((parseInt(item.id) == parseInt(resource.id)) || (parseInt(item.doc_id) == parseInt(resource.doc_id))) {
        index = i;
      }
    });
    if (objResource) {
      if (is_dummy) {
        this.videosList[index].total_comments = resource.total_comments;
        this.videosList[index].total_attachment = resource.total_attachment;
      } else {
        this.videosList[index] = resource;
      }

    }

  }
  processCategories(categories, sidebar_categories?) {
    if(categories.added){
      categories.added.forEach(x => x.toggle=true);
    }
    this.videoLibraryService._videoLibararyGlobalObeservable.next({ triggerFromHome: { updatedCategories: categories,toggle:true, sidebar_categories:sidebar_categories?sidebar_categories :false } });
  }
  processVideosCount(videoCategories) {
    if (this.user.domain === 'uncategorized' && videoCategories.length > 0) {
      this.totalVideos++;
      return false;
    }
    let sortedCategories = JSON.parse("[" + this.user.subject_id + "]");
    if (videoCategories.length > 0 && sortedCategories.length > 0) {
      let common = sortedCategories.some(x => videoCategories.includes(x));
      if (common) {
        this.videoCount++;
        return true;
      } else {
        this.totalVideos++;
        return false;
      }
    } else {
      this.videoCount++;
      return true;
    }
  }
  private processVideoDescription(description, resource_id) {
    var indexOfMyObject = this.videosList.findIndex(x => {
      return x.doc_id == resource_id;
    });
    if (indexOfMyObject > -1) {
      this.videosList[indexOfMyObject].desc = description;
    }
  }

  processCategoriesCount(sidebarcategories) {
    this.videoLibraryService._videoLibararyGlobalObeservable.next({ triggerFromHome: { updatedCategoriesCount: sidebarcategories } });

  }
  private processVideoCategory(categories, document_id, sidebarcategories) {
    if (categories && this.selectedCategoris) {
      let common = this.findCommonElements(categories, this.selectedCategoris);
      if (!common) {
        this.processResourceDeleted(document_id, this.sessionData.user_current_account.users_accounts.user_id, sidebarcategories);
      }
      else {
        this.processCategoriesCount(sidebarcategories);
      }
    }
    else {
      this.processCategoriesCount(sidebarcategories);
    }
  }

  private findCommonElements(arr1, arr2) {
    if(arr1 && arr2)
    {  
      return arr1.some(item => arr2.includes(item));
    }
  }
  private addCategorytoLS(category){
    if(category.data.added){
    let lsCategory = this.headerService.getLocalStorage('lsCollapse');
          if(lsCategory ){
            category.data.added.forEach(element => {
              let obj = {
                subject_id:element.subject_id,
                name:element.name,
                toggle:true
              }
              lsCategory.push(obj)
            });
            this.headerService.setLocalStorage('lsCollapse',lsCategory)
          }
        }

        if(category.data.deleted){
          let lsCategory = this.headerService.getLocalStorage('lsCollapse');
          if(lsCategory ){
            category.data.deleted.forEach(deleted_Id => {
              let index = lsCategory.findIndex(x=> x.subject_id==deleted_Id);
              if(index>-1)
              lsCategory.splice(index, 1);
            });
            this.headerService.setLocalStorage('lsCollapse',lsCategory)
          }
         }
  }



  ngOnDestroy() {
    this.styles.forEach(style => {
      let element = document.getElementById(style.id);
      element.parentNode.removeChild(element);
    });
    this.translationSubscription.unsubscribe();
    this.librarySubscription.unsubscribe();
    this.subscription.unsubscribe();
    this.socket_listener?.unsubscribe();
  }

  private loadCss(style: any) {
    let head = document.getElementsByTagName('head')[0];
    let link = document.createElement('link');
    link.id = style.id;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = style.path;
    head.appendChild(link);
  }
}
