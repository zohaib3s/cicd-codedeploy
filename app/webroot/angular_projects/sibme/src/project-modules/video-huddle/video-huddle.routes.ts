import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RootComponent } from './components';
import { SharedPageNotFoundComponent } from '../shared/shared-page-not-found/shared-page-not-found.component';

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: '', component: RootComponent, children: [
      {
        path: 'assessment/:id',
        loadChildren:()=> import('./child-modules/assessment-huddle-details/assessment-huddle-details.module').then(m => m.AssessmentHuddleDetailsModule),
        
      },
      {
        path: 'huddle/details/:id',
        loadChildren:()=> import('./child-modules/details/details.module').then(m => m.DetailsModule),
        data: { name: 'video_huddle' }
      },
      {
        path: 'list',
        loadChildren:()=> import('./child-modules/list/list.module').then(m => m.ListModule),
      },
      {
        path: 'list/:folder_id',
        loadChildren:()=> import('./child-modules/list/list.module').then(m => m.ListModule),
      },
    ]
  },
  { path: 'page_not_found', component: SharedPageNotFoundComponent },

  {
    path: '**',
    redirectTo: 'page_not_found',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideoHuddleRoutes { }