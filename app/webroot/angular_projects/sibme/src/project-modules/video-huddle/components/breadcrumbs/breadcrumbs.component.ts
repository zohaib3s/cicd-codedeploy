import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router, RoutesRecognized } from "@angular/router";
import { HeaderService } from "@projectModules/app/services";
import { HomeService } from "@projectModules/app/services";
import { decode } from '@src/project-modules/shared/components/common/utility';
import { DiscussionService } from '@videoHuddle/child-modules/details/servic/discussion.service';
import { Subscription } from 'rxjs';

@Component({
	selector: 'video-huddle-breadcrumbs',
	templateUrl: './breadcrumbs.component.html',
	styleUrls: ['./breadcrumbs.component.css']
})

export class BreadcrumbsComponent implements OnInit, OnDestroy {
	public translation: any = {};
	public breadcrubms :any;
	public assesseeName :any;
	public checkAssesseeParam :boolean = false;
	private translationSubscription: Subscription;
	private breadCrumbsSubscription: Subscription;
	private subscriptions: Subscription = new Subscription();
	fromRoute: any;
	constructor(private route: ActivatedRoute, private router: Router, private homeService: HomeService, public discussionService: DiscussionService, private headerService: HeaderService) {
		this.translationSubscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
			this.translation = languageTranslation;
		});

		// this.router.queryParams.subscribe(params => {
		// 	if (params.assessee_detail) {
		// 		this.checkAssesseeParam = true;
		// 	}
		// });

		// this.assesseeName = JSON.parse(localStorage.getItem('assessee_detail'));

		this.breadCrumbsSubscription = this.homeService.breadcrumbs$.subscribe(data => {
			setTimeout(() => {
				const crumbs = data?.document_folders ? [...data?.document_folders, ...data?.folders] : [...data];

				this.breadcrubms = crumbs;
			}, 800);

		});

		if (route.snapshot.firstChild.queryParams.from) {
			this.fromRoute = decode(route.snapshot.firstChild.queryParams.from);
			this.fromRoute.url = decodeURIComponent(this.fromRoute.url);
		}
	}

	ngOnInit() {
	}

	ngOnDestroy() {
		this.translationSubscription.unsubscribe();
		this.breadCrumbsSubscription.unsubscribe();
		this.subscriptions.unsubscribe();
	}

	routeTo() {
		this.router.navigateByUrl(this.fromRoute.url);
	}
}
