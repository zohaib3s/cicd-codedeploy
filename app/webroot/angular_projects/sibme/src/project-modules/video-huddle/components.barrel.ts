import { CommonModule } from '@angular/common';

import { AppLevelSharedModule } from "@shared/shared.module";
import { EditGoalService, GoalWorkService } from '../goals/services';

import { RootComponent, BreadcrumbsComponent } from "./components";

export const __IMPORTS = [CommonModule, AppLevelSharedModule];

export const __DECLARATIONS = [RootComponent, BreadcrumbsComponent];

export const __PROVIDERS = [GoalWorkService, EditGoalService]