import { Component, OnInit, Input,ViewChild, OnChanges, SimpleChanges,TemplateRef, OnDestroy, EventEmitter, HostListener} from '@angular/core';
import { AssessmentService } from '../services/assessment.service';
import { ModalDirective, BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ShowToasterService } from '@projectModules/app/services';
import * as _ from "underscore";
import io from 'socket.io-client';
import { Router, ActivatedRoute } from '@angular/router';
import { HeaderService, SocketService } from '@src/project-modules/app/services';
import { Subscription } from 'rxjs';
import { environment } from '@src/environments/environment';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { BodyService } from 'src/project-modules/analytics/services/body.service';
declare global {
  interface Window { io: any; }
  interface Window { Echo: any; }
}

declare var Echo: any;

window.io = io;
window.Echo = window.Echo || {};

@Component({
  selector: 'huddle-participants-list',
  templateUrl: './huddle-participants-list.component.html',
  styleUrls: ['./huddle-participants-list.component.css']
})
export class HuddleParticipantsListComponent implements OnInit, OnChanges, OnDestroy {


  dataLoading: boolean = true;
  participantsList: any = [];
  participantsCustomFieldList: any = [];
  isGroupImage: boolean;
  staticImageServiceIp = "https://s3.amazonaws.com/sibme.com/static/users/";
  @ViewChild('childModal',{static:false}) childModal: ModalDirective;
  @ViewChild('delete',{static:false}) delete;
  @Input('particiants') particiants;
  @Input('isSubmissionValid') isSubmissionValid;
  @ViewChild("loadingModal", { static: false }) loadingModal;
  sessionData: any = {};
  private loadingModalRef: BsModalRef;
  public socket_listener: any;
  public current_huddel_id;
  artifact: any;
  public ModalRefs: Modals;
  public participantForDelete;
  messageDelete: string;
  subscription: Subscription;
  translation:any={};
  userAccount:any;
  public sortOrderName: any = "up";
  public sortOrderAssessed: any = "up";
  private page = 1;
  private totalParticipants = 0;
  public loadingParticipants = false;

  p: number = 1;

  @HostListener("window:scroll", ["$event"])
  onScroll() {
    const scrollOnHeight = window.innerHeight + window.pageYOffset >= document.body.offsetHeight + 20;
    if (scrollOnHeight && (this.participantsList.length < this.totalParticipants) ) {
      this.loadNextPage();
    }
  }
  

  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  sortOptions: { key: string; field_label: any; selected: boolean; sort: any; showArrow: any;}[];
  userAccountLevelRoleId: any=0;
  public isOpenBox: boolean = false;
  public isOpenBoxAssessmentSummary: boolean = false;
  public startAssessmentIndex = 0;
  public leftAssessmentIndex = 0;
  public lastParaIndex  = 0;
  public lastAssessmentSummaryIndex = 0;
  public assessmentSummarylist : any;
  public assessmentSummary = '';
  public assessment_custom_field_value = '';
  public paragraph_field_label = '';
  public video_title_assessment_summary = '';
  public showDeadlineBar = true; 
  public list_container_height =false;
  public onetime:any;
  public selectedRowSummary:any=null
  public currentIndex=1;
  public autohight= false;
  constructor(private assessmentService: AssessmentService,
    private modalService: BsModalService,
    private toastr: ShowToasterService,
    public headerService: HeaderService,
    private socketService: SocketService,
    private router:Router,
    private activeRoute: ActivatedRoute,
    private bodyService: BodyService
    ) {

      this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
        this.translation = languageTranslation;
            this.sortOptions = [
          { key: 'user_name', field_label:this.translation.artifact_assessee , selected: false, sort: "down", showArrow: false },
          { key: 'last_modified_with_time', field_label: this.translation.artifacts_last_modified , selected: false, sort: "down", showArrow: false },
          { key: 'last_submission_with_time', field_label: this.translation.artifacts_last_submission , selected: false, sort: "down", showArrow: false },
          { key: 'assessed', field_label: this.translation.artifacts_assessed_tracker, selected: false , sort: "down", showArrow: false},
          { key: 'avg_performance_level_rating', field_label: 'Av. Performance', selected: false , sort: "down", showArrow: false },
          { key: 'videos_count', field_label: 'Videos' , selected: false, sort: "down", showArrow: false },
          { key: 'resources_count', field_label: 'Resources', selected: false , sort: "down", showArrow: false },
          { key: 'comments_count', field_label: 'Comments', selected: false , sort: "down", showArrow: false }
        ];
      });
     }

  ngOnInit() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      
      visibleTimeout: 1000,
    }
      

    this.sessionData = this.headerService.getStaticHeaderData();
    this.userAccountLevelRoleId = this.sessionData.user_permissions.roles.role_id;

    this.current_huddel_id=this.assessmentService.getHuddleId();
    this.ModalRefs = {};
    this.userAccount =this.sessionData.user_current_account;    
  }

  ngOnDestroy() {
    this.socket_listener.unsubscribe();
  }

  private loadNextPage(){
    this.loadingParticipants = true;
    this.page++;
    const obj = {
      huddle_id: this.current_huddel_id,
      user_current_account: this.userAccount,
      page: this.page
    }

    this.assessmentService.getHuddleAssessee(obj).subscribe((data:any) => {
      if(data.success && data){
        this.loadingParticipants = false;
        data.assessees_list.forEach(element => {
          this.participantsList?.push(element);
        });
      }
      if(!data.success){
        this.toastr.ShowToastr('error',data.message)
        this.router.navigate(['/list'])
      }

    }, error => {
      console.error(error)
    });
  }

  getSingleAsignment(item) {
    let urlTree = this.router.parseUrl(this.router.url);
    urlTree.queryParams = {}; 
    let url = urlTree.toString();
    let str= url.slice(0,-7) +`assignment/${item.user_id}`
     if(item.is_submitted==1)
     this.router.navigate([str]);
     else
     this.showChildModal()
  }


  public openAssessmentSummaryDetailsBox(assessmentSummary,index,event) {
    this.closeDetailsBox();
   this.closeBoxAssessmentSummary();
    if(this.participantsList.length<=5){
      this.list_container_height = true;
    }
    else{
      let lastIndexs = [this.participantsList.length-1,this.participantsList.length-2,this.participantsList.length-3];
      for(let i=0;i<lastIndexs.length;i++){
       if(index==lastIndexs[i]){
        this.autohight = true;
        console.log(true)
        
       }
      }
    }
    event.stopPropagation();
    this.isOpenBoxAssessmentSummary = true;
    this.selectedRowSummary =index
    this.currentIndex=1;
    // this.closeBoxAssessmentSummary();
    this.assessmentSummary = assessmentSummary[0].comment;
    this.video_title_assessment_summary = assessmentSummary[0].title;
    this.leftAssessmentIndex = 0;


  

  }

  public openDetailsBox(customFieldList,index,participantsListIndex,evn) {
   evn.stopPropagation();
   this.closeDetailsBox();
   this.closeBoxAssessmentSummary();
    this.onetime = participantsListIndex;
    
  if(this.participantsList.length<=5){
    this.list_container_height = true;
  }
  else{
    let lastIndexs = [this.participantsList.length-1,this.participantsList.length-2,this.participantsList.length-3];
    for(let i=0;i<lastIndexs.length;i++){
     if(participantsListIndex==lastIndexs[i]){
      this.autohight = true;
      console.log(true)
      
     }
    }
    console.log(this.list_container_height);

  }

  
 


  
   this.lastParaIndex = index;
   this.assessment_custom_field_value = customFieldList[index].assessment_custom_field_value;
   this.paragraph_field_label = customFieldList[index].field_label;
    console.log(this.assessment_custom_field_value)
    this.isOpenBox = true;
  }

  public assessmentSummaryLeft(event,summary) {
       event.stopPropagation()
       if(summary.length>0 && this.currentIndex !=1){
        this.currentIndex--;
      }
    
  }

  public assessmentSummaryRight(event,summary) {
    event.stopPropagation()
     if(this.currentIndex==summary.length){
       return
     }
     else{
       this.currentIndex++;
     }
    
  }

  public closeBoxAssessmentSummary() {
   this.onetime=null;
    this.isOpenBoxAssessmentSummary= false;
    this.autohight = false;
    this.list_container_height = false;
  }

  onClickedOutside(evt,isA_Summary){
    evt.stopPropagation();
    if(isA_Summary == 1) {
      this.closeBoxAssessmentSummary();
    }
  }

  public closeDetailsBox(){
    this.onetime=null;
    this.list_container_height = false;
    this.autohight = false
    this.isOpenBox = false;
  }



  public deleteAssessee() {
    
    let user = this.sessionData;
    let obj = {
      huddle_id: this.assessmentService.getHuddleId(),
      user_current_account: this.sessionData.user_current_account,
      assessee_id:this.participantForDelete
    }
    this.assessmentService.deleteAssessee(obj).subscribe((data: any) => {

      if (data.success) {
        this.totalParticipants--;
        this.toastr.ShowToastr('info',this.translation.assessee_removed_from_huddle)        
      }

    }, error => {

    })
    this.ModalRefs.participentsModal.hide();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.participantsList = changes.particiants.currentValue.assessees_list;
    console.log("this.participantsList: ",this.participantsList)
    // this.participantsCustomFieldList = changes.particiants.currentValue.custom_assessment_fields;
     console.log(this.participantsList);
    if(this.participantsList){
      this.dataLoading = false;
    }

    if(changes?.particiants?.currentValue?.huddleUsers) {
      this.totalParticipants = changes.particiants.currentValue.huddleUsers.length;
    }

    if(changes.particiants.currentValue.custom_assessment_fields){
      this.participantsCustomFieldList = changes.particiants.currentValue.custom_assessment_fields.filter(item => item.assessment_custom_field_id);
      this.participantsCustomFieldList.map(item => {item.sort = 'up'; item.showArrow = false});     
    }

    this.socketPushFunctionDiscussionDetail();

    if(this.participantsList) {
      this.participantsList.forEach(element => {
        element.custom_fields_data.forEach(function(key, val) {
          if(key.assessment_custom_field_id != null){
            if(parseInt(key.assessment_custom_field_value)){
              element[key.field_label] = parseInt(key.assessment_custom_field_value);
            } else {
              element[key.field_label] = key.assessment_custom_field_value;
            }
            
          }  
        });
      });
    }  
  }

  ImageUrlBuilder(participent: any) {
    if (participent.image == 'groups') {
      this.isGroupImage = true
      return true
    } else {
      this.isGroupImage = false
      let image = participent.image || "assets/video-huddle/img/c1.png";
      let url = `${this.staticImageServiceIp}${participent.user_id}/${
        participent.image
        }`;
      return participent.image ? url : image;
    }

  }

  /**
   * Sort participantsList based on the key param with differnet conditions based on key
   * If key is `last_modified` or `last_submission` which are type of date then sort the participantsList according to date
   * If key is `assessed` then sort the participantsList according to assessed boolean type and then username
   * Else sort the participantsList according to key param which in current case will be `user_name`
   * @param {key: string} 
   */

  public sortParticipants(key: string, option) {
    this.toggleSelectedSortOptions(option.field_label);
    if(option.sort == "up") { 
      option.sort = "down";
    }
    else if(option.sort == "down") {
      option.sort = "up";
    } 

    this.participantsList.sort((firstIndex, secondIndex) => {
      if ((key === 'last_modified_with_time' || key === 'last_submission_with_time') && option.sort == "up" ) {
        let fIndexDate: any = new Date(firstIndex['last_submission_with_time']);
        let sIndexDate: any = new Date(secondIndex['last_submission_with_time']);
        return sIndexDate - fIndexDate;
      } else if ((key === 'last_modified_with_time' || key === 'last_submission_with_time') && option.sort == "down" ) {
        let fIndexDate: any = new Date(firstIndex['last_submission_with_time']);
        let sIndexDate: any = new Date(secondIndex['last_submission_with_time']);
        return fIndexDate - sIndexDate;
      }  
      else if (key === 'assessed' && option.sort == "up") {
        if (firstIndex[key] && secondIndex[key]) {
          let firstIndexKey = firstIndex[key];
          let secondIndexKey = secondIndex[key];
          if (firstIndexKey < secondIndexKey) return -1;
          if (firstIndexKey > secondIndexKey) return 1;
          return 0;
        }
        if (firstIndex[key]) return -1;
        if (secondIndex[key]) return 1;
      } else if (key === 'assessed' && option.sort == "down") {
        if (firstIndex[key] && secondIndex[key]) {
          let firstIndexKey = firstIndex[key];
          let secondIndexKey = secondIndex[key];
          if (firstIndexKey < secondIndexKey) return 1;
          if (firstIndexKey > secondIndexKey) return -1;
          return 0;
        }
        if (firstIndex[key]) return 1;
        if (secondIndex[key]) return -1;
      } else if( option.sort == "up"){
        let firstIndexKey = firstIndex[key];
        let secondIndexKey = secondIndex[key];
        if (firstIndexKey < secondIndexKey) return -1;
        if (firstIndexKey > secondIndexKey) return 1;
        return 0;
      } else if( option.sort == "down"){
        let firstIndexKey = firstIndex[key];
        let secondIndexKey = secondIndex[key];
        if (firstIndexKey < secondIndexKey) return 1;
        if (firstIndexKey > secondIndexKey) return -1;
        return 0;
      }
    });
  }

  // Sorting Dymanic header for custom Fields
  public sortCustomFields(prop){ 
    
    this.toggleSelectedSortOptions(prop.field_label);
    if(prop.sort == "up") { 
      prop.sort = "down";
    }
    else if(prop.sort == "down") {
      prop.sort = "up";
    } 

    this.participantsList.sort((a, b) => {

      if(!a[prop.field_label]) a[prop.field_label] = "-1";
      if(!b[prop.field_label]) b[prop.field_label] = "-1";

      if (prop.sort == "up"){
        if (a[prop.field_label] && b[prop.field_label]) {
          if (a[prop.field_label]  < b[prop.field_label] ) return -1;
          if (a[prop.field_label]  > b[prop.field_label] ) return 1;
          return 0;
        }
        if (a[prop.field_label]) return -1;
        if (b[prop.field_label]) return 1;
      } 
      else if (prop.sort == "down"){
        if (a[prop.field_label] && b[prop.field_label]) {
          if (a[prop.field_label]  < b[prop.field_label] ) return 1;
          if (a[prop.field_label]  > b[prop.field_label] ) return -1;
          return 0;
        }
        if (a[prop.field_label]) return 1;
        if (b[prop.field_label]) return -1;
      } 
    }); 
  }

  // Hide Show arrow while sorting from two arrays
  private toggleSelectedSortOptions(key: string) {  
    this.sortOptions.forEach(option => {
      if (option.field_label == key) {
        option.showArrow = true;
      }  

      if (option.field_label != key) {
        option.showArrow = false;
      }  
    });

    this.participantsCustomFieldList.forEach(option => {   
      if (option.field_label == key) {
        option.showArrow = true;
      }  

      if (option.field_label != key) {
        option.showArrow = false;
      } 
    });
  }

   // Downloading Data list 
  exportParticipantsList(type) {
    this.loadingModalRef = this.modalService.show(this.loadingModal, { backdrop: 'static' });
    const obj = {
      huddle_id: this.assessmentService.getHuddleId(),
      current_user_role: this.sessionData.user_current_account.roles.role_id,
      current_user_id: this.sessionData.user_current_account.User.id,
      account_id: this.sessionData.user_current_account.accounts.account_id,
      current_lang: this.sessionData.language_translation.current_lang,
    }
    this.assessmentService.exportParticipantsList(obj, type).subscribe((data:any)=>{
      if(data){
        this.exportFiles(data.filename)
      }
    });
    
  }

exportFiles(fileName) {
  let obj: any = {};
  let file_name = fileName;
  let interval_id = setInterval(() => {
    obj.file_name = file_name;

    this.bodyService.is_download_ready(obj,true).subscribe((data: any) => {
      let download_status = data.download_status;
      if (download_status) {
        clearInterval(interval_id);
        obj.download_url = data.download_url;
        obj.file_name = file_name;
        this.loadingModalRef.hide();
        this.bodyService.download_analytics_standards_to_excel(obj);
      }
    });
  }, 3000);
}
  
  private socketPushFunctionDiscussionDetail() {
    this.socket_listener = this.socketService.pushEventWithNewLogic(`huddle-details-${this.current_huddel_id}`).subscribe(data => this.processEventSubscriptions(data));
  }

  private processEventSubscriptions(data) {
    console.log(data);
    
      switch (data.event) {
        case "resource_added":
          if (this.isAllowed(data.allowed_participants) || (typeof data.allowed_participants === 'undefined') || data.allowed_participants.length == 0 || data.allowed_participants == undefined) {
            if(data.is_comment_resource || data.data.comment_id){

          }
          else{
            let should_wait = 0;
            if (data.is_video_crop) {
              should_wait = 1;
            }
            this.participantsList.forEach(x => {
              if(x.user_id == data.data.created_by){
                
                if(data.data.doc_type==1){
                  x.videos_count++
                }
                if(data.data.doc_type==2 || data.data.doc_type==5){
                  x.resources_count++
                }
              }
            });
          }
        }
          break;

        case "resource_renamed":
          this.participantsList.forEach(x => {
            data.assessees_list.forEach(element => {
              if(element.user_id == x.user_id) {
                x.assessed = element.assessed
              }
            });
          });

          break;

        case "resource_deleted":
          
        this.participantsList.forEach(x => {
         if(x.user_id == data.participant_id){
          let obj = {
            huddle_id: this.activeRoute.parent.snapshot.paramMap.get('id'),
            user_id: data.participant_id,
            video_id:''
          }
           x.is_submitted=0;
          this.assessmentService.getCommentCount(obj).subscribe((data:any) => {
            if(data.success && data){
              x.comments_count=data.data.comments_count;
            }
        });
          
           if(data.doc_type==1){
             x.videos_count--
           }
           if(data.doc_type==2){
             x.resources_count--
           }
           if(data.doc_type==5){
            x.resources_count--
          }
         }
       });


          break;
        
        case "comment_added":  
        if (this.isAllowed(data.allowed_participants) || (typeof data.allowed_participants === 'undefined') || data.allowed_participants.length == 0 || data.allowed_participants == undefined) {
          console.log(this.participantsList);
          
          this.participantsList.forEach(x => {
              if (data.from_cake == 1) {
                  let odata = JSON.parse(data.data)
                  if (x.user_id == odata.created_by) {
                      x.comments_count++
                  }
              } else {
                  if (x.user_id == data.data.created_by) {
                      x.comments_count++
                  }
              }
          });

          this.participantsList.forEach(x => {
            data.assessees_list.forEach(element => {
              if(element.user_id == x.user_id) {
                x.assessed = element.assessed
              }
            });
          });
 
      }
        break;

        case "comment_deleted":  
        if (this.isAllowed(data.allowed_participants) || (typeof data.allowed_participants === 'undefined') || data.allowed_participants.length == 0 || data.allowed_participants == undefined) {
          this.participantsList.forEach(x => {
            if(x.user_id == data.data || x.user_id ==data.user_id){
              x.comments_count--
            }
          });
          
        }
        break;
        
        case "assessee_deleted":
        this.processAssesseeDeleted(data.data)
        break;

        case "assignment_submitted":
        this.participantsList.forEach(x => {
          if(x.user_id == data.submitted_by){
            x.last_submission=data.data.last_submission;
            x.last_modified=data.data.last_submission;
            x.is_submitted=data.data.is_submitted;
            x.comments_count=data.data.comments_count;
          }
        });
        break;

        case "user_assessment_custom_field_updated":
          if(data.assessee_id) {
            this.participantsList.forEach(x => {
              if (x.user_id == data.assessee_id) {
                x.custom_fields_data = data.data.user_assessment_custom_fields;             //Changing custom data
                x.custom_fields_data.forEach(function(key, val) {             //Adding Data in parent Array
                  x[key.field_label] = key.assessment_custom_field_value
                });
              } 
            });
          }
        break;

        default:
        break;
      }
  }

  private processAssesseeDeleted(data){
    var indexOfMyObject = this.participantsList.findIndex(x => x.user_id == data.assessee_id);

    if (indexOfMyObject > -1) {
      this.participantsList.splice(indexOfMyObject, 1);
    }
  }

  private isAllowed(allowed_participants = []) {
    let that = this;
    let allowed = _.find(allowed_participants, function (item) {
      return parseInt(item.user_id) == parseInt(that.sessionData.user_current_account.User.id);
    });
    if (allowed) {
      return true;
    }
  }

  OnclickDeleteModal(assesse_id){
    this.OpenDeleteModal(this.delete,"sm_popup", this.translation.artifacts_are_you_sure_to_delete_assessee );
    this.participantForDelete= assesse_id;
  }

  OpenDeleteModal(template: TemplateRef<any>,class_name,message?:string){
    if(message){
      this.messageDelete = message
    }else{
      this.messageDelete = this.translation.artifacts_you_are_about_to_delete_the_assesee ;
    }
    this.ModalRefs.participentsModal = this.modalService.show(template, {
      ignoreBackdropClick :true,
      class: class_name
    });                                                                                                                                                                                                                                 

  }

  showChildModal(): void {
    this.childModal.show();
  }
 
  hideChildModal(): void {
    this.childModal.hide();
  }

  goToHuddleReport(){
    const link = `${environment.baseUrl}/home/trackers/assessment-report`;
    window.open(link, "_blank");
  }

public  ArrayCheck(assessment_summary){
  if(Array.isArray(assessment_summary)){
    return false;
  }
  else{
    return true;
  }
}
HtmlFiletr(assessment_custom_field_value){
 
  let doc:any = new DOMParser()
  const parsedDocument = doc.parseFromString(assessment_custom_field_value, "text/html")
  var allText = parsedDocument.all[0].textContent;
  //  console.log(allText)
  return allText
    // let doc = new DOMParser().parseFromString(assessment_custom_field_value, 'text/html');
    // , "text/html")
  // let z= doc.querySelector('p').textContent;

  // return z;
  
}



}

interface Modals {
  [key: string]: any;
}
