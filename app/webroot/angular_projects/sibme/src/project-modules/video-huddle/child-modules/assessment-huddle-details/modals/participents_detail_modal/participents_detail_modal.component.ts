import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { HeaderService } from '@src/project-modules/app/services';
import { Subscription } from 'rxjs';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';

@Component({
  selector: 'participents_detail_modal',
  templateUrl: './participents_detail_modal.component.html',
  styleUrls: ['./participents_detail_modal.component.css']
})
export class ParticipentsDetailModal implements OnInit {
  public data: any;
  public participents: any;
  public translation: any;
  private subscriptions: Subscription = new Subscription();
  scrollEvents: EventEmitter<SlimScrollEvent>;
  opts: ISlimScrollOptions;

  constructor( public modalRef: BsModalRef, public headerService: HeaderService) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => this.translation = languageTranslation));
  }

  ngOnInit() {
    this.opts = {
			position: 'right',
			barBackground: '#C9C9C9',
			barOpacity: '0.8',
			barWidth: '10',
			barBorderRadius: '20',
			barMargin: '0',
			gridBackground: '#D9D9D9',
			gridOpacity: '1',
			gridWidth: '0',
			gridBorderRadius: '20',
			gridMargin: '0',
			alwaysVisible: true,
			visibleTimeout: 1000,
		};
    this.participents = this.data.participents;
  }
}
