import {  Component,  OnInit,  Input,  ViewChild,  TemplateRef,  EventEmitter,  Output, OnDestroy, HostListener} from "@angular/core";
import { DetailsHttpService } from "../servic/details-http.service";
import { BsModalService } from "ngx-bootstrap/modal";
import { DiscussionService } from "../servic/discussion.service";
import IUpload from "../discussions/IUpload";
import { ShowToasterService } from '@projectModules/app/services';
import { HeaderService, AppMainService} from "@app/services";
import { PermissionService } from '../servic/permission.service';
import { Subscription } from 'rxjs';
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';


@Component({
  selector: "single-discussion",
  templateUrl: "./single-discussion.component.html",
  styleUrls: ["./single-discussion.component.css"]
})
export class SingleDiscussionComponent implements OnInit, OnDestroy, IUpload {
  
  
  @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {
      
      
      if(this.editDiscData.title && this.editDiscData.comment){
        if(this.titleEdited || this.commentEdited)
       {
        this.headerService.removeLocalStorage(this.editDiscussionTitleLSKey);
        this.headerService.removeLocalStorage(this.editDiscussionCommentLSKey);
          $event.returnValue =false;
      }
  }
}
  @Input() discussion;
  @Input() params;
  @Input() discssionRoles;
  public editDiscData: any = {};
  public ModalRefs: any = {};
  public Files: any = [];
  public header_data;
  public translation: any = {};
  private subscriptions: Subscription = new Subscription();
  public userAccountLevelRoleId: number | string = null;
  private DISCUSSION_LS_KEYS = GLOBAL_CONSTANTS.LOCAL_STORAGE.DISCUSSION;
  private editDiscussionTitleLSKey: string = '';
  private editDiscussionCommentLSKey: string = '';
  private editDiscussionTAKey: string = '';
  public isRequestCompleted;
  public selectedId;
  public editDescriptionLength;
  public actMain=false;
  public actEdit=false;
  public actDelete=false;
  public goalEvidence: boolean = false;
  public commentEdited=false;
  public titleEdited=false;
  public exportPermission: boolean;
  
  @Output() onDelete: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild("editDiscussion", {static: false}) editDiscussion;

  constructor(
    public appMainService:AppMainService,
    public discussionService: DiscussionService,
    public detailService: DetailsHttpService,
    private modalService: BsModalService,
    private toastr: ShowToasterService,
    public headerService: HeaderService,
    public permissionService:PermissionService, 
    private activatedRoute: ActivatedRoute  ) {

      this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
        this.translation = languageTranslation;
      }));
      
      this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
        this.goalEvidence = params.goalEvidence;
      }));
    }

  ngOnInit() {
    this.discussion.parsedComment = this.discussion.comment.replace(/<.*?>/g, "");
    this.header_data = this.headerService.getStaticHeaderData();
    this.userAccountLevelRoleId = this.header_data.user_permissions.roles.role_id;
    
    this.editDiscussionTitleLSKey = `${this.DISCUSSION_LS_KEYS.EDIT_D_TITLE}${this.discussion.id}_${this.params.id}_${this.headerService.getUserId()}`;
    this.editDiscussionCommentLSKey = `${this.DISCUSSION_LS_KEYS.EDIT_D_COMMENT}${this.discussion.id}_${this.params.id}_${this.headerService.getUserId()}`;
    this.editDiscussionTAKey = `${this.DISCUSSION_LS_KEYS.EDIT_TA}${this.params.id}_${this.headerService.getUserId()}`;
    this.discussionService.responseSubject.subscribe(data => {
      this.isRequestCompleted = data;
    });
    this.discussionService.selectedId$.subscribe( data => {
      this.selectedId = data;
     });

     this.editDeletePersissions();
    window.onbeforeunload = () => this.ngOnDestroy();

  }

  public editDiscussion_popup(item: any) {
    this.editDiscData = JSON.parse(JSON.stringify(item));

    /** Restore  and set edit discussion localstorage object start */
    const lsEditDiscussionTitle = this.headerService.getLocalStorage(this.editDiscussionTitleLSKey);
    if(lsEditDiscussionTitle){
      this.editDiscData.title = lsEditDiscussionTitle;  
    }

    const lsEditDiscussionComment = this.headerService.getLocalStorage(this.editDiscussionCommentLSKey);
    if(lsEditDiscussionComment){
      this.editDiscData.comment = lsEditDiscussionComment;  
    }
    /** Restore  and set edit discussion localstorage object end */
    this.unDeleteAttachments();
    this.ShowModal(this.editDiscussion, "lg_popup");
  }

  onUpload(e: any) {
    let target = e.target;
    target.files.length > 0 && this.pushToFiles(target.files);
    target.value = "";
  }
  public TriggerUpload() {
    document.getElementById(`file_${this.discussion.id}`).click();
  }
  public pushToFiles(files: any): any {
    for (let i = 0; i < files.length; i++) {
      this.Files.push(this.discussionService.parseFile(files[i]));
    }
  }

  public unDeleteAttachments() {
    this.discussion.attachments.length > 0 &&
      (() => {
        this.discussion.attachments.forEach(
          d => (d.isDeleted = false)
        );
      })();
  }

  public onEditSubmission() {
    this.headerService.removeLocalStorage(this.editDiscussionTitleLSKey);
    this.headerService.removeLocalStorage(this.editDiscussionCommentLSKey);

    if (!this.editDiscData.title || this.editDiscData.title.trim()=="" || 
      !this.editDiscData.comment || this.editDiscData.comment.trim() == "" || 
      this.editDiscData.comment.toString().replace(/<.*?>/g, "") == "" || 
      this.editDiscData.comment.toString().replace(/<.*?>/g, "").trim() == "") {
        this.toastr.ShowToastr('info',this.translation.discussion_title_description_required_to_edit );
        return;
    } else {
        if(this.Files.length)
        {
            let result =  this.headerService.checkFileSizeLimit(this.Files);
            if(!result.status)
            {
                this.toastr.ShowToastr('error',result.message);
                return;
            }
        }
      let sessionData: any = this.headerService.getStaticHeaderData();
      let comment_time = moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS")

      let obj: any = {
        huddle_id: this.params.id,
        send_email: true,
        comment_id: this.discussion.id,
        discussion_id: this.discussion.id,
        user_current_account: sessionData.user_current_account,
        remove_attachments: this.discussion.attachments
          .filter(i => i.isDeleted)
          .map(f => f.document_id)
          .join(","),
          created_at_gmt: comment_time
      };

      ({ comment: obj.comment, title: obj.title } = this.editDiscData);

      ({
        User: { id: obj.user_id },
        accounts: { account_id: obj.account_id }
      } = sessionData.user_current_account);

      let fd = this.discussionService.ToFormData(obj);
      this.Files.length > 0 &&
        (() => {
          for (let i = 0; i < this.Files.length; i++) {
            fd.append("attachment[]", this.Files[i],this.Files[i].name);
          }
        })();

      this.ModalRefs.editDiscussionModal.hide();
      this.editDiscData = { title: "", comment: "" };
      this.discussionService.responseSubject.next(true);
      this.appMainService.EditDiscussion(fd).subscribe((d: any) => {
        this.discussionService.responseSubject.next(false);
        if (d.success) {
          this.toastr.ShowToastr('info',d.message)
          this.discussion = d.data[0];
        } else {
          this.toastr.ShowToastr('info',
            d.message || this.translation.something_went_wrong_msg
          );
        }
      }, err => {
        obj.first_name = sessionData.user_current_account.User.first_name;
        obj.last_name = sessionData.user_current_account.User.last_name;
        obj.thumb_image = 'assets/img/photo-default.png', 
        obj.tryAgain = true;
        obj.editTryAgain = true;
        obj.uuid = new Date().getTime();
        obj.parsedComment = obj.comment.replace(/<.*?>/g, "");
        obj.image=this.discussion.image;

        this.discussion = obj;
        let tmpDiscussionEditArray = this.headerService.getLocalStorage(this.editDiscussionTAKey);
        if(!Array.isArray(tmpDiscussionEditArray)) tmpDiscussionEditArray = [];
          tmpDiscussionEditArray.push(obj);
          this.headerService.setLocalStorage(this.editDiscussionTAKey, tmpDiscussionEditArray);
      });
    }
  }

  public ShowModal(template: TemplateRef<any>, class_name) {
    this.ModalRefs.editDiscussionModal = this.modalService.show(template, {
      ignoreBackdropClick :true,
      class: class_name
    });
  }

  findLength(){
    let comnt = this.discussion.comment.replace(/<.*?>/g, "");
    return comnt.length;
  }
  public saveDiscussionAgain(discussion){
    if(discussion.processing) return;
    discussion.processing = true;

    this.detailService.saveDiscussionEmitter(discussion);
  }

  public editDiscussionTryAgain(discussion: any){
    discussion.tryAgain=false;
    if(discussion.processing) return;
    discussion.processing = true;
    let fd = this.discussionService.ToFormData(discussion);

    this.appMainService.EditDiscussion(fd).subscribe((d: any) => {
      if (d.success) {
        this.toastr.ShowToastr('info',d.message)
        this.discussion = d.data[0];


        let tmpDiscussionEditArray = this.headerService.getLocalStorage(this.editDiscussionTAKey);
        if(Array.isArray(tmpDiscussionEditArray) && tmpDiscussionEditArray.length > 0){
          tmpDiscussionEditArray = tmpDiscussionEditArray.filter(dis => dis.uuid != discussion.uuid);
          this.headerService.setLocalStorage(this.editDiscussionTAKey, tmpDiscussionEditArray);
        }
        if(d.code == 409){
          const taComments = this.headerService.getLocalStorage(this.editDiscussionTAKey);
          if (Array.isArray(taComments)) {
            const commentIndex = taComments.findIndex(tsComment => tsComment.created_at_gmt === d.data.created_at_gmt);
            if (commentIndex > -1) {
              taComments.splice(commentIndex, 1);
              this.headerService.setLocalStorage(this.editDiscussionTAKey, taComments);
            }
          }
        }
        this.discussion.parsedComment = this.discussion.comment.replace(/<.*?>/g, "");
    
      } else {
        this.toastr.ShowToastr('info',
          d.message || this.translation.something_went_wrong_msg
        );
      }
    }, err => {
      setTimeout(() => {
        discussion.processing = false;
        discussion.tryAgain=true;
      }, 500);

    });

  }

  public editModalChange(key: string, value: string){
 
    if(key === 'title'){
      this.titleEdited = true;
      if(value) this.headerService.setLocalStorage(this.editDiscussionTitleLSKey, value);
      else this.headerService.removeLocalStorage(this.editDiscussionTitleLSKey);
    } else if(key === 'comment'){
      this.commentEdited = true;
      console.log(value,this.commentEdited);
      if(value) this.headerService.setLocalStorage(this.editDiscussionCommentLSKey, value);
      else this.headerService.removeLocalStorage(this.editDiscussionCommentLSKey);
    }

  }

  public cancelEditDiscussionModal(){
    if(this.titleEdited || this.commentEdited){
      if(confirm(this.translation.you_have_unsaved_changes)){
        this.ModalRefs.editDiscussionModal.hide();
        this.commentEdited=false;
        this.titleEdited=false;
        this.headerService.removeLocalStorage(this.editDiscussionTitleLSKey);
        this.headerService.removeLocalStorage(this.editDiscussionCommentLSKey);
      }
    }

    else{
      this.commentEdited=false;
      this.titleEdited = false;
      this.ModalRefs.editDiscussionModal.hide();
      this.headerService.removeLocalStorage(this.editDiscussionTitleLSKey);
      this.headerService.removeLocalStorage(this.editDiscussionCommentLSKey);
    }
  }
  
  selectedComment(discussion){
    if(discussion)
    this.discussionService.selectedID(discussion.discussion_id ||discussion.id);
    else{
      this.discussionService.selectedID(this.editDiscData.id);
    }
  }


  editDeletePersissions(){
      if( this.permissionService.getdiscussionPermissions(this.discussion.created_by) && this.userAccountLevelRoleId != '125'){
        this.actMain=true;
        this.actEdit=true;
        this.actDelete=true;
      }
       else if(this.discussion.huddle_type==1 && this.discssionRoles.user_id==this.header_data.user_current_account.User.id && this.discssionRoles.role_id=='200' && this.userAccountLevelRoleId != '125'){
        this.actMain=true;
        this.actEdit=false;
        this.actDelete=true;
       }
       else if((this.discssionRoles.role_id == 200 && this.discussion.huddle_type == '2')  || (this.discssionRoles.role_id == 210 && this.discussion.coachee_permission == '1' && this.discussion.huddle_type == '2' && this.permissionService.getdiscussionPermissions(this.discussion.created_by) ))
       {
          this.actDelete=true;
          this.actMain=true;
       }else {
                this.exportPermission = true;
        }
  }

  exportDiscussion(discussion: any) {
    const sessionData: any = this.headerService.getStaticHeaderData();
    const obj: any = {
      export_type : 'word',
      huddle_id: discussion.ref_id,
      detail_id: discussion.id
    };

    ({
      User: { id: obj.user_id },
      accounts: { account_id: obj.account_id }
    } = sessionData.user_current_account);

    this.discussionService.exportDiscussion(obj);
  }
  ngOnDestroy(){

    this.subscriptions.unsubscribe();
    if(this.ModalRefs && this.ModalRefs.editDiscussionModal) this.ModalRefs.editDiscussionModal.hide();
  }

  
}
