import { Component, OnInit, ViewChild, OnDestroy, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ShowToasterService } from '@projectModules/app/services';
import { AssessmentService } from '../services/assessment.service';
import { DetailsHttpService } from '../../details/servic/details-http.service';
import { environment } from '@src/environments/environment';
import { HeaderService, SocketService, HomeService } from '@src/project-modules/app/services';
import { Subscription } from 'rxjs';
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import * as quillNamespace from 'quill';
import QuillImageDropAndPaste from 'quill-image-drop-and-paste'
@Component({
  selector: 'app-asignment-detail-single-assesee',
  templateUrl: './asignment-detail-single-assesee.component.html',
  styleUrls: ['./asignment-detail-single-assesee.component.css']
})
export class AsignmentDetailSingleAsseseeComponent implements OnInit, OnDestroy {
  @ViewChild('popUp', { static: false }) popUpModel;
  @ViewChild('deleteArtifectModal', { static: false }) deleteArtifectModal: ModalDirective;
  basePath = environment.baseUrl;
  public dataLoading: boolean = true;
  private userId: number;
  public sessionData: any = {};
  public userHuddleSubmitted: boolean | number = true;
  public userArtifects: any = [];
  public userVideoArtifects: any = [];
  public userResourceArtifects: any = [];
  public artifact: any = {};
  public huddleId: number;
  public huddleData: any;
  public assesseeName: any;
  public huddlePublichedCheck: any;
  public userName: string = '';
  public userImage: string = '';
  public showDialogBox = false;
  subscription: Subscription;
  translation: any;
  showDeleteBtn: boolean = true;
  userAccountLevelRoleId: any = 0;
  public assessment_custom_fields: any = [];
  saveBtnCheck: boolean = true;
  public socket_listener: any;
  public socket_listener_artifects: any;
  public quillConfiguration:any = GLOBAL_CONSTANTS.QUILL_CONFIGURATION;
  public isTextChanged = false;
  public quill: any = quillNamespace;
  private breadCrumbs: any;

 
  @HostListener('window:beforeunload', ['$event']) onPopState(event: Event) {
    debugger;
    if (this.showDialogBox && this.assessment_custom_fields.length > 0) {
      if (confirm(this.translation.you_are_about_to_leave_framework_angular_rubrics)) {
          window.onbeforeunload = () => {
              this.ngOnDestroy();
          }
      }
      event.returnValue = false;
  }
    // this.showDialogBox = false;
    // console.log('in listner');

    // if (this.showDialogBox && this.assessment_custom_fields.length > 0) {
    //   return;
    // }
    // event.returnValue = false;
  }
  unloadNotification($event: any) {
    if (this.isTextChanged) {
      $event.returnValue = false;
    }
  }
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public headerService: HeaderService,
    public detailsService: DetailsHttpService,
    private assessmentService: AssessmentService,
    public toastr: ShowToasterService,
    private socketService: SocketService,
    private homeService: HomeService
  ) {

    this.activatedRoute.params.subscribe(param => {
      this.userId = param.id;
    });

    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });

  }

  ngOnInit() {


    this.quill.register('modules/imageDropAndPaste', QuillImageDropAndPaste);
    this.quillConfiguration.MODULES = {...this.quillConfiguration.MODULES, imageDropAndPaste: {handler: this.imageHandler1}};

    this.sessionData = this.headerService.getStaticHeaderData();
    this.userAccountLevelRoleId = this.sessionData.user_permissions.roles.role_id;

    this.huddleId = this.assessmentService.getHuddleId();
    this.getUserArtifects();
    this.socketGetCustomFieldData();
  }

  imageHandler1 = (imageDataUrl, type, imageData) => {}

  detectChanges(event){
    this.showDialogBox = true;
  }

  private getUserArtifects() {
    const data = {
      user_id: this.userId,
      account_id: this.sessionData.user_current_account.users_accounts.account_id,
      role_id: this.sessionData.user_current_account.users_accounts.role_id,
      huddle_id: this.huddleId,
      only_user_artifacts: 1,
      page: 1,
      send_remaining_artifact: 1,
      logged_in_user_id: this.sessionData.user_current_account.User.id
    };

    this.assessmentService.getUserArtifects(data).subscribe((response: any) => {
      if (!response.success) {
        alert('Something went wrong while getting data from server');
        this.router.navigate(['/assessment', this.huddleId]);
      } else {


        this.userVideoArtifects = [];
        this.userResourceArtifects = [];
        this.userArtifects = response.artifects.all;
        this.assessment_custom_fields = response.assessment_custom_fields;
        this.assessment_custom_fields?.forEach(x => {
          if(x.field_type === 5 && x.assessment_custom_field_value) {
            x.edit = false;
          }else if (x.field_type === 5 && !x.assessment_custom_field_value) {
            x.edit = true;
          }
          
        });
        this.huddlePublichedCheck = response.huddles_data?.Huddle?.is_published;
        this.userArtifects.forEach(artifec => {
          if (artifec.doc_type == 1) this.userVideoArtifects.push(artifec);
          else this.userResourceArtifects.push(artifec);
        });

        const currentHuddleAssessee = response.huddles_data.HuddleUsers.find(user => {
          return user.user_id == this.userId;
        });
        if (currentHuddleAssessee) this.userHuddleSubmitted = currentHuddleAssessee.is_submitted;


        this.detailsService.setArtifactlList(this.userArtifects);
        this.huddleData = response.huddles_data;
        this.userName = `${response.user_info.first_name} ${response.user_info.last_name}`;
        this.userImage = this.assessmentService.getUserAvatar(response.user_info.id, response.user_info.image);

        // console.log("this.homeService.getCurrBreadcrumbVal();", this.homeService.getCurrBreadcrumbVal());
        
        const existingBreadcrums = this.homeService.getCurrBreadcrumbVal();
        !this.breadCrumbs ? existingBreadcrums.folders.push({userName: this.userName, forAssesse: true}): false;
        this.homeService.updateBreadcrumb(existingBreadcrums);
        this.breadCrumbs = existingBreadcrums;
      }
      this.dataLoading = false;
      this.saveBtnCheck = true;
    });
  }

  private socketGetCustomFieldData() {
    this.socket_listener = this.socketService.pushEventWithNewLogic(`huddle-details-${this.huddleId}-${this.userId}`).subscribe(data => this.processEventSubscriptions(data));
    this.socket_listener_artifects = this.socketService.pushEventWithNewLogic(`huddle-details-${this.huddleId}`).subscribe(data => {


      this.processEventSubscriptions(data)
    }
    );
    console.log(this.socket_listener_artifects);

  }

  private processEventSubscriptions(res) {

    console.log('socket res: ', res)


    switch (res.event) {


      case "user_assessment_custom_field_updated":
        if (res.assessee_id == this.userId) {
          this.assessment_custom_fields = res.data.user_assessment_custom_fields;
        }

        break;
      case "videos_assessed":
        this.processVideoAssessed(res);
        break;
      case "resource_renamed":
        this.processResourceRenamed(res.data, res.is_dummy);
        break;
      case "resource_deleted":
        this.processResourceDeleted(res.data, res.deleted_by, res.assignment_unsubmitted);
        break;

      default:
        break;
    }

  }

  private processVideoAssessed(res){
    console.log('here');
    
    if (res.data &&res.data.length > 0) {
      let videoIds = this.userVideoArtifects.map(function(a) {return a.doc_id;});
      res.data.forEach(element => {
        let videoId = videoIds.indexOf(element);
        if (videoId != -1) {
          videoId = videoIds[videoId]
          var indexOfMyObject = this.userVideoArtifects.findIndex(x => {
            return x.doc_id == videoId || x.id == videoId;
          });
          let video = this.userVideoArtifects[indexOfMyObject];
          video.assessed = true;
          this.userVideoArtifects[indexOfMyObject] = video
        }
      });
    }
    
    
  }
  private processResourceDeleted(resource_id, deleted_by, issubmit?) {

    var indexOfMyObject = this.userVideoArtifects.findIndex(x => {
      return x.doc_id == resource_id || x.id == resource_id;
    });
    var indexOfMyObject2 = this.userResourceArtifects.findIndex(x => {
      return x.doc_id == resource_id || x.id == resource_id;
    });
    // if (issubmit) {
    //   this.is_submitted = false
    // }
    if (indexOfMyObject > -1) {
      this.userVideoArtifects[indexOfMyObject].is_dummy = 1;
    }
    else if (indexOfMyObject2 > -1) {
      this.userResourceArtifects[indexOfMyObject2].is_dummy = 1;
    }
    if (deleted_by != this.sessionData.user_current_account.User.id) {
    }
  }
  private processResourceRenamed(resource, dont_show_toast = 0) {
    if (!resource ||  resource == '') {
      return false;
    }
    var indexOfMyObject = this.userVideoArtifects.findIndex(x => x.id == resource.id);
    var indexOfMyObject2 = this.userResourceArtifects.findIndex(x => x.doc_id == resource.doc_id);
    let objResource = null;

    if (indexOfMyObject > -1) {
      objResource = this.userVideoArtifects[indexOfMyObject];
      this.userVideoArtifects[indexOfMyObject] = resource;
    }

    else if (indexOfMyObject2 > -1) {
      objResource = this.userResourceArtifects[indexOfMyObject2];
      this.userResourceArtifects[indexOfMyObject2] = resource;
    }

    if (objResource) {
      if (dont_show_toast == 0) {
        objResource.title.slice(0, 25)
        if (this.sessionData.user_current_account.User.id != resource.updated_by) {
          if (objResource.title.length > 25) {
            this.toastr.ShowToastr('info',`'${objResource.title}'... ${this.translation.artifacts_renamed_to} '${resource.title}'...`);
          } else {
            this.toastr.ShowToastr('info',`'${objResource.title}' ${this.translation.artifacts_renamed_to} '${resource.title}'`);
          }
        }
      }

    }
  }

  public OpenModel(artifact, modalName) {
    
    this.artifact = artifact;
    if (modalName === 'download'){
      
      // this.showDialogBox = false;
      if (this.showDialogBox == true) {
        console.log(this.showDialogBox);
        this.showDialogBox = false;
        this.popUpModel.DownloadResource(artifact);
        setTimeout(() => {
          this.showDialogBox = true;
        }, 500);
      } else{
        this.popUpModel.DownloadResource(artifact);
      }
      
      
    }
      
    else if (modalName === 'delete') {
      this.showDialogBox = false;
      if (confirm(this.translation.artifacts_deleting_video_will_unsubmit_assessment)) {
        this.deleteArtifectModal.show();
      }
    }

  }

  private generateSubmissionDeadlineDate(date: string, time: string) {
    let deadlineDate = new Date(date);
    let timeParts = time.match(/(\d+)\:(\d+) (\w+)/);
    const hours = /am/i.test(timeParts[3]) ? parseInt(timeParts[1], 10) : parseInt(timeParts[1], 10) + 12;
    const minutes = parseInt(timeParts[2], 10);

    deadlineDate.setHours(hours);
    deadlineDate.setMinutes(minutes);

    return deadlineDate;

  }

  editorChange() {
    // this.isTextChanged = true;
    this.showDialogBox = true;
  }

  onChange(event, obj) {
    if (event.target.value < 0) {
      event.target.value = 0;
    }
    else if (event.target.value > 100) {
      event.target.value = 100;
    }
    else if (event.target.value.toString().length > 8) {
      event.target.value = Math.round(event.target.value).toFixed(0);
    }
    obj.assessment_custom_field_value = event.target.value;
  }

  onpaste(event: ClipboardEvent, paste) {
    console.log(paste);

    let clipboardData = event.clipboardData
    let pastedText = clipboardData.getData('text');

    const pattern = /^[0-9\+\-\.]{1,9}$/;
    if (paste === "Number") {
      if (pastedText.toString().length > 10) {
        event.preventDefault();
      }
    }
    if (paste === "Percentage") {
      if (Number(pastedText) > 100 || Number(pastedText) < 0) {
        event.preventDefault();
      }
    }
    if (!pattern.test(pastedText)) {
      event.preventDefault();
    }
  }

  goToDetailPage(artifact) {
    
    if (artifact.doc_type == 2) {
      this.openResource(artifact)
    } else {
      if (artifact.published == 1) {
        let obj = {
          name: this.userName,
          path: this.getPathAndQuery()
        }
        localStorage.setItem('assessee_detail', JSON.stringify(obj))
        this.router.navigate(['video_details/home', artifact.account_folder_id, artifact.doc_id], { queryParams: { assessment: 'true' } });
      }
    }
  }

  public openResource(artifact) {
    // this.showDialogBox = false;
    // this.detailsService.openResource(artifact);
    if(artifact.doc_type != 5){

      if(this.sessionData.enable_document_commenting == '0') {
        let path = environment.baseUrl + "/app/view_document" + artifact.stack_url.substring(artifact.stack_url.lastIndexOf("/"), artifact.stack_url.length);
        window.open(path, "_blank");
      } else {
        let path = "/document-commenting/pdf-renderer/huddle-video-page/" + artifact.doc_id + artifact.stack_url.substring(artifact.stack_url.lastIndexOf("/")) + "/" + artifact.file_type + "/" + artifact.account_folder_id + "/0/false";
        this.router.navigate([path]);
      }
    }
    else{
      window.open(artifact.url, "_blank");
    }
  }

  public artifectModalDelete() {
    this.showDeleteBtn = false;
    let obj = {
      document_id: this.artifact.doc_id,
      huddle_id: this.artifact.account_folder_id,
      user_id: 0,
      doc_type: this.artifact.doc_type,
      account_id: this.sessionData.user_current_account.users_accounts.account_id
    };
    ({
      User: {
        id: obj.user_id
      }
    } = this.sessionData.user_current_account);
    this.detailsService.DeleteResource(obj).subscribe(data => {
      let d: any = data;
      this.artifectDeleteModalHide();

      if (d.success) {
        let obj = {
          artifact: this.artifact,
          userId: this.userId,
          type: (this.artifact.doc_type == 1) ? "video" : "resource"
        }

        let str = `A ${obj.type} in ${obj.artifact.first_name}'s ${this.translation.artifacts_assessment_was_deleted_and_unsubmitted}`;
        this.assessmentService.setDeleteObject(obj);
        this.toastr.ShowToastr('info',str);

        this.router.navigate(['video_huddles/huddle/details', this.huddleId])
      }
      else {
        this.toastr.ShowToastr('info',d.message)

      }
      this.artifact.is_dummy = 1;
      this.showDeleteBtn = true;

    }, error => {
      this.toastr.ShowToastr('error',error.message)
    });
  }

  public artifectDeleteModalHide(): void {
    this.deleteArtifectModal.hide();
  }

  public backToAssessmentDetails(huddleId) {
    this.detailsService.updateRefreshGetParticipantList(true);
    this.router.navigate(['/video_huddles/huddle/details', huddleId]);
  }

  public updateAssesment() {
    this.showDialogBox = false;
    this.saveBtnCheck = false;
    let obj = {
      user_current_account: this.sessionData.user_current_account,
      assessee_id: this.userId,
      huddle_id: this.huddleId,
      assessment_custom_fields: this.assessment_custom_fields
    }

    this.assessmentService.saveAssessmentFields(obj).subscribe((res: any) => {
      if (res.success == true) {
        this.isTextChanged = false;
        this.toastr.ShowToastr('success',res.messages);
        this.getUserArtifects();
        this.assessment_custom_fields?.forEach(x => {
          if(x.field_type === 5 && x.assessment_custom_field_value) {
            x.edit = false;
          }else if (x.field_type === 5 && !x.assessment_custom_field_value) {
            x.edit = true;
          }
          
        });
      }
    });
  }

  public editParagraph(singletonCustom: any) {
    this.assessment_custom_fields?.forEach(x => {
      if(x.assessment_custom_field_id === singletonCustom.assessment_custom_field_id) {
        x.edit = !x.edit;
      }
    });
  }

  getPathAndQuery() {
    let strurl = window.location.href;
    let url = new URL(strurl)
    return url.pathname + url.search;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.socket_listener.unsubscribe();
    this.socket_listener_artifects.unsubscribe();
  }

}
