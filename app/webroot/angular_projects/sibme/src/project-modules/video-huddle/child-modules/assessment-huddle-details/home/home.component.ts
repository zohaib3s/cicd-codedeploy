import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AssessmentService } from '../services/assessment.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DetailsHttpService } from '../../details/servic/details-http.service';
import { ShowToasterService } from '@projectModules/app/services';
import { environment } from "src/environments/environment";
import { HeaderService, HomeService } from '@src/project-modules/app/services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  sessionData: any = [];
  huddle_id: number;
  videoslist: any=[];
  participantlist: any=[];
  page: any=1;

 

  constructor(private router:Router,private homeService: HomeService,
     private toastr: ShowToasterService, private detailsService: DetailsHttpService,
      private headerService: HeaderService, private activeRoute: ActivatedRoute, private assessmentService: AssessmentService) { }
  
  ngOnInit() {

    this.detailsService.resourceToOpen.subscribe(artifact => {
      this.FileClicked("td", artifact);
    });

    this.huddle_id = 0;
    this.activeRoute.params.subscribe(p =>{this.huddle_id = p.id;
    this.assessmentService.setHuddleId(this.huddle_id);
    })
    this.sessionData = this.headerService.getStaticHeaderData();
    let userAccount =
      this.sessionData.user_current_account;

    let obj = {
      huddle_id: this.huddle_id,
      user_current_account: userAccount
    }

    this.assessmentService.getHuddleDetails(obj).subscribe((data:any) => {
      if(data.success && data){
        this.assessmentService.setHuddleData(data);
      }
      if(!data.success){
        this.toastr.ShowToastr('error',data.message)
this.router.navigate(['/list'])
      }

    }, error => {
      console.error(error)
    });

    this.getHuddleParticipants();
    this.getBreadCrubms();
    this.getWorkSpaceVideos();

    this.assessmentService.deleteAlertObject$.subscribe(obj=>{

      if(obj.userId){
        
        this.getHuddleData();

      }
    })

    /** Arif code for refreshing participant list to update assessed status start */
   this.detailsService.refreshGetParticipantList$.subscribe(() => {
      let userAccount = this.sessionData.user_current_account;
       
      let obj = {
        huddle_id: this.huddle_id,
        user_current_account: userAccount
      }

      this.assessmentService.getHuddleDetails(obj).subscribe((data:any) => {
        if(data){
          this.assessmentService.setHuddleData(data);
        }
      });
    });


  }

  getHuddleData(){
   debugger;
    this.sessionData = this.headerService.getStaticHeaderData();
    let userAccount =
      this.sessionData.user_current_account;

    let obj = {
      huddle_id: this.huddle_id,
      user_current_account: userAccount
    }
    this.assessmentService.getHuddleDetails(obj).subscribe((data:any) => {
      if(data){

        this.assessmentService.setHuddleData(data);
      }
      if(!data.success){
        this.toastr.ShowToastr('error',data.message)
this.router.navigate(['/video_huddles/list'])
      }

    }, error => {
      console.error(error)
    });
  }

  public FileClicked(from, file) {

    if (from == "td") {

      if (file.stack_url && file.stack_url != null) {

        if(this.sessionData.enable_document_commenting == '0') {
          let path = environment.baseUrl + "/app/view_document" + file.stack_url.substring(file.stack_url.lastIndexOf("/"), file.stack_url.length);
          window.open(path, "_blank");
        } else {
          let path = "/document-commenting/pdf-renderer/huddle-video-page/" + file.doc_id + file.stack_url.substring(file.stack_url.lastIndexOf("/")) + "/" + file.file_type + "/" + file.account_folder_id + "/0/false";
          this.router.navigate([path]);
        }       
      }
      else {
        this.DownloadFile(file);
      }
    }
    else {
      this.DownloadFile(file);
    }
  }

  private DownloadFile(file) {
    this.detailsService.DownloadFile(file.doc_id);
  }

  public getWorkSpaceVideos(){
    let obj={
      title:'',
      page:this.page,
      sort:'',
      doc_type:'1',
      account_id:this.sessionData.user_current_account.users_accounts.account_id,
      user_id:this.sessionData.user_current_account.users_accounts.user_id,
      with_folders: 1
    };
    this.assessmentService.getWorkspaceVideos(obj).subscribe((data:any)=>{
this.videoslist=data.data;
this.assessmentService.setvideoList(this.videoslist)

    },error=>{

    })
  }

  public getHuddleParticipants(){
    let obj={
      huddle_id:this.huddle_id,
      user_role_id:this.sessionData.user_current_account.users_accounts.role_id,
      user_id:this.sessionData.user_current_account.users_accounts.user_id,

    };
    this.detailsService.GetParticipents(obj).subscribe((data:any)=>{
      this.participantlist=[...data];
this.assessmentService.setParticipantsList(this.participantlist)

    },error=>{

    })
  }

 


  getBreadCrubms() {
    let obj: any = {
      folder_id: this.huddle_id
    };
    let sessionData: any = this.headerService.getStaticHeaderData();

    ({
      User: { id: obj.user_id },
      accounts: { account_id: obj.account_id }
    } = sessionData.user_current_account);

    let ref = this.homeService.GetBreadcrumbs(obj).subscribe((data: any) => {
      
      this.homeService.updateBreadcrumb(data);

    });
  }

}



