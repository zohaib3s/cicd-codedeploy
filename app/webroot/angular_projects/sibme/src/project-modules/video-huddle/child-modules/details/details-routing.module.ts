import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { DiscussionsComponent } from "./discussions/discussions.component";
import { DiscussionDetailsComponent } from "./discussion-details/discussion-details.component";
import { DiscussionListComponent } from "./discussion-list/discussion-list.component";
import { ConferencingComponent } from './conferencing/conferencing.component';
import { SharedPageNotFoundComponent } from '@src/project-modules/shared/shared-page-not-found/shared-page-not-found.component';

const routes: Routes = [
  { path: "", redirectTo: "artifacts/grid", pathMatch: "full" },
  { path: "artifacts", redirectTo: "artifacts/grid", pathMatch: "full" },
  {
    path: "artifacts/:displayStyle",
    component: HomeComponent
  },
  {
    path: "artifacts/:displayStyle/:folderId",
    component: HomeComponent
  },
  {
    path: "discussions",
    component: DiscussionsComponent,
    children: [
      { path: "", redirectTo: "list" },
      { path: "list", component: DiscussionListComponent },
      { path: "details/:discussion_id", component: DiscussionDetailsComponent }
    ]
  },
  { path: "conferencing", component: ConferencingComponent },
  
  { path: 'page_not_found', component: SharedPageNotFoundComponent },

  {
    path: '**',
    redirectTo: 'page_not_found',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailsRoutingModule {}
