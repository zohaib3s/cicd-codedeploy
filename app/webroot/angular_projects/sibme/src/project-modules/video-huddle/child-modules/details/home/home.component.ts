import { Component, OnInit, OnDestroy, ViewChild, TemplateRef, EventEmitter, HostListener } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { HeaderService, SocketService, HomeService, AppMainService } from '@app/services';
import { SessionService } from "@videoHuddle/child-modules/list/services/session.service";
import { ShowToasterService } from '@projectModules/app/services';
import { BsModalService, ModalDirective, BsModalRef } from "ngx-bootstrap/modal";
import { DetailsHttpService } from "../servic/details-http.service";
import { PermissionService } from '../servic/permission.service';
import { Subject, Subscription } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { environment } from "@environments/environment";
import * as _ from "underscore";
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { DiscussionService } from "../servic/discussion.service";

import io from 'socket.io-client';
import { DisplayStyle, PageType } from '@app/interfaces';
// import { ActivityModelComponent } from '@src/project-modules/shared/components/activity-model/activity-model.component';
import { SharedFolderModal } from '@src/project-modules/shared/components/list/rename-folder-modal/rename-folder-modal.component';
import { ConfirmationDialogComponent } from '@src/project-modules/shared/modals/confirmation-dialog/confirmation-dialog.component';
import { BrowserAgent } from "@app/types";
import { URLArtifactModalComponent, RecordVideoModelComponent, MillicastRecordVideoModelComponent, ActivityModelComponent } from "@shared/modals";
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import { DndDropEvent } from 'ngx-drag-drop';
import { NewfolderService } from '@src/project-modules/workspace/services/newfolder.service';

declare global {
  interface Window { io: any; }
  interface Window { Echo: any; }
}

window.io = io;
window.Echo = window.Echo || {};

@Component({
  selector: "video-huddle-details-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})

export class HomeComponent implements OnInit, OnDestroy {
  public browserAgent: BrowserAgent;
  public pageType: PageType = 'huddle-page';
  public ModalRefs: Modals;
  bsModalRef: BsModalRef;
  public params: any = {};
  public showHide = false;
  public popup_val;
  public cirqPath;
  public socket_listener: any;

  @ViewChild("participantsModal", { static: false }) participantsModal;
  @ViewChild("renameModal", { static: true }) renameModal;
  @ViewChild("cropVideo", { static: true }) cropVideo;
  @ViewChild("delete_confim", { static: true }) delete_confim;
  @ViewChild("activity", { static: false }) activity;
  @ViewChild("email", { static: true }) email;
  @ViewChild("newDiscussions", { static: false }) newDiscussions;

  @ViewChild("popUp", { static: false }) popUpModel;

  @ViewChild('deleteHuddleModal', { static: false }) deleteHuddleModal: ModalDirective;
  @ViewChild('moveHuddleModal', { static: false }) moveHuddleModal: ModalDirective;

  /** New Variables for singleton start */
  public additionalData: any = {};
  /** New Variables for singleton end */

  public ready_for_download:any ;
  public file_type: any;

  public ToggleView: string = "all";
  private searchInput: Subject<string> = new Subject();
  ToggleStyleView = "grid";
  Filters: any = {};
  assessment_permissions;
  SearchString = "";
  Loadings: any = {};
  page: number = 0;
  HuddlesAndFolders;
  foldersExpanded: boolean;
  FoldersTreeData: any = {};
  subscriptionRefs: any = {};
  EditableFolder: any = {};
  PipeTrigger: boolean;
  permissions: any = {};
  colors;
  artifacts: any = [];
  resource_filter: any = 0;
  participents: any = [];
  staticImageServiceIp = "https://s3.amazonaws.com/sibme.com/static/users/";
  showArtifactButton = true;
  showDiscussionButton;
  loading_huddles: boolean = false;
  preLoaderLoop = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
  actvities: any = [];
  path = environment.APIbaseUrl;
  rootPath = environment.baseUrl;
  isNotDiscussion: boolean;
  lastRoute: string = "grid";
  huddle_type: any = 3;
  artifacts_name;
  huddle_id: any;
  sort: any = '';
  user_huddle_role: any;
  sessionData: any = [];
  public total_artifacts: number = 0;
  public artifact_moved_count: number = 0;
  @ViewChild("popupModal", { static: false }) popModal;
  noRecorFound: boolean = false;
  popupIsLoading: boolean;
  isGroupImage: boolean = false;
  bysearch: boolean;
  activities_request_ref: any = null;
  viewerObj: any = {}
  huddle_name: any;
  changed_account_name: "";
  previous_page: any;
  public header_data;
  public translation: any = {};
  private subscriptions: Subscription = new Subscription();
  public userAccountLevelRoleId: number | string = null;
  public translationLoaded: boolean = false;
  public addBtnPermissionLoaded: boolean = false;
  public cirqLiveData:any;
  public artifactsDataLoaded:boolean=false;
  public enable_live_recording:any;
  public site_id;
  public folderId;
  Inputs: { NewFolderName: string; Confirmation: string; ConfirmationKey: any; };
	DeletableItem
  MovableItem: { id: any; isHuddle: boolean; type: any; };
  showMomeoptions=false;
  public displayStyle: DisplayStyle;
  private goalEvidence: boolean = false;
  public h_live_stream_permission:any;
  public liveStreamStarted: boolean = false;

  public screenSharingStarted: boolean = false;
  public huddleFolders: any = [];
  private documentFolderId: any;
  public parentFolderId: string;
  public mainHuddle_Creator;
  public changeBackgroundColor:boolean = false;
  public changeBackgroundColorArtifact:boolean = false;
  public dragFolderId:any;

  @HostListener("window:scroll", ["$event"])
  onScroll(event) {
    let stop_api_call = 0;
    if (this.total_artifacts) {
      let total_pages = Math.ceil(this.total_artifacts / 12)
      if (this.page > total_pages) {
        stop_api_call = 1;
      }
    }
    if (this.Loadings.isNextPageLoading) {
      let doc = document.documentElement;
      let currentScroll =
        (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

      var d = document.documentElement;
      var offset = window.innerHeight + window.pageYOffset;
      var height = d.offsetHeight;
      if (
        window.innerHeight + window.pageYOffset >=
        document.body.offsetHeight - 2
      ) {
        window.scroll(0, currentScroll - 100);
      }
    } else if (!this.Loadings.isNextPageLoading && this.total_artifacts > this.artifacts.length && !stop_api_call) {
      setTimeout(() => {
        let doc = document.documentElement;
        let currentScroll =
          (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

        var d = document.documentElement;
        var offset = d.scrollTop + window.innerHeight;
        var height = d.offsetHeight;

        if (
          window.innerHeight + window.pageYOffset >=
          document.body.offsetHeight - 2
        ) {
          this.LoadNextPage(true);
          window.scroll(
            0,
            document.body.offsetHeight -
            this.getPercentage(document.body.offsetHeight, 9)
          );
        }
      }, 100);
    }
  }
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  public huddleHeaderData:any;
  constructor(
    public detailsService: DetailsHttpService,
    private fullRouter: Router,
    private activatedRoute: ActivatedRoute,
    public headerService: HeaderService,
    private sessionService: SessionService,
    private toastr: ShowToasterService,
    public windowRef: Window,
    private homeService: HomeService,
    private modalService: BsModalService,
    private socketService: SocketService,
    public permissionService: PermissionService,
    private discussionService: DiscussionService,
    private newfolderService: NewfolderService,
    private appMainService: AppMainService) {

    this.subscriptions.add(this.headerService.screenRecordingState$.subscribe(screenRecordingState => {
      if (screenRecordingState === 'started' || screenRecordingState === 'paused' || screenRecordingState === 'resumed') this.screenSharingStarted = true;
      else this.screenSharingStarted = false;
    }));

    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.translationLoaded = true;
    })
    );

    this.subscriptions.add(this.detailsService.huddleHeaderData$.subscribe(data => {
      this.huddleHeaderData = data;
    }));

    this.subscriptions.add(this.headerService.liveStreamState$.subscribe(liveStreamState => {
      if (liveStreamState === 'started' || liveStreamState === 'started-screen-sharing' || liveStreamState === 'stopped-screen-sharing') this.liveStreamStarted = true;
      else this.liveStreamStarted = false;
    }));

    this.subscriptions.add(this.headerService.screenRecordingState$.subscribe(screenRecordingState => {
      console.log("screenRecordingState",screenRecordingState);
    }));

    this.browserAgent = this.appMainService.browserAgent;
  }

  isViewer() {

  }


  ngOnInit() {
    this.sessionData = this.headerService.getStaticHeaderData();
    this.site_id = this.sessionData?.site_id;
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.header_data = this.sessionData;
    this.userAccountLevelRoleId = this.header_data?.user_permissions?.roles.role_id;
    this.enable_live_recording = this.header_data?.enable_live_recording;

			this.Inputs = { NewFolderName: "", Confirmation: "", ConfirmationKey: 'DELETE' };

    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    }
    this.GetParams();
    this.SubscribeSearch();
    let id: any;
    // this.activatedRoute.params.subscribe((p) => {
    //   console.log(p)
    //   this.folderId = p.folderId;
    //   this.page = 0;
    //   this.ToggleStyleView = p.displayStyle;
    //   if(this.folderId) {
    //     this.LoadNextPage(false);
    //   }
    // });
    this.activatedRoute.queryParams.subscribe(params => {
      this.goalEvidence = params.goalEvidence;
      this.total_artifacts = 0;
      let searchchecker = params.search;
      params.folderId ? this.documentFolderId = params.folderId: false;
      if (searchchecker != undefined) {
        this.bysearch = true;
        this.artifact_moved_count = 0;
      } else {
        this.bysearch = false
      }

      this.page = 0;

      if(params.resource){
        (this.resource_filter = params.resource);
        this.huddleFolders = [];
        this.artifact_moved_count = 0;
       }else (this.resource_filter = 0);
      params.search
        ? (this.SearchString = params.search)
        : (this.SearchString = "");
      params.sort
        ? (this.sort = params.sort)
        : (this.sort = "");
      this.Loadings.isNextPageLoading = true;
      this.artifacts = [];
      this.previous_page = 0;

      this.LoadNextPage(true);
      this.detailsService.setResource(this.resource_filter);
    });
    this.activatedRoute.params.subscribe((p) => {
      this.folderId = p.folderId;

      var url = this.fullRouter.url.split("/");

      let r;
      p.folderId ? r = url[url.length - 1].split('?') : r = url[url.length - 1].split('?') ;
      this.ToggleStyleView = r[0];
      // this.page = 0;
      this.ToggleStyleView = p.displayStyle;
      if(this.folderId) {
        this.page = 0;
        this.previous_page = 0;
        this.LoadNextPage(false);
      }
    });

    this.detailsService.popup_value.subscribe(v => {
      this.trigger_model(v);
    });
    this.detailsService.resourceToOpen.subscribe(artifact => {
      this.FileClicked("td", artifact);
    });

    this.ModalRefs = {};

    this.isNotDiscussion = false;

    this.activatedRoute.params.subscribe((p) => {

      this.displayStyle = p.displayStyle;
      this.params = p;
      this.detailsService.setParams(this.params);

    });

    this.checkValidRoute(this.displayStyle)
    this.observeNewRecords();
    this.getBreadCrumbs();

    let toggleSetting =JSON.parse(localStorage.getItem('foldersExpanded'));
    toggleSetting!=undefined ? this.foldersExpanded = toggleSetting : this.foldersExpanded=true;

    // let channel_name = `workspace-${this.sessionData.user_current_account.users_accounts.account_id}-${this.sessionData.user_current_account.users_accounts.user_id}`;
    // this.socket_listener = this.socketService.pushEventWithNewLogic(channel_name).subscribe(data => this.processEventSubscriptions(data));

  }
  ngOnDestroy() {
    /*
    this.ARouter.params.subscribe(p => {
      this.huddle_id = p.id;
      this.socketService.destroyEvent(`huddle-details-${this.huddle_id}`);
    });
    */
    this.subscriptions.unsubscribe();
    if (this.ModalRefs && this.ModalRefs.participentsModal) this.ModalRefs.participentsModal.hide();

  }

  private getPercentage(n, what) {
    return (what / 100) * n;
  }

  public GetParticipents() {
    let obj: any = {
      huddle_id: +this.huddle_id,
      user_role_id: +this.sessionData.user_current_account.roles.role_id,
      user_id: +this.sessionData.user_current_account.User.id,
    };

    this.detailsService.GetParticipents(obj).subscribe(
      data => {
        this.participents = data;
        this.mainHuddle_Creator = this.participents[0].createdBy;
        this.participents = _.sortBy(this.participents, 'role_name');
        //sorting by role
        let creator = this.participents.find(p => p.user_name == this.mainHuddle_Creator);
        let index = this.participents.indexOf(creator);
        if(index != -1) {
          this.participents.splice(index,1);
          this.participents.unshift(creator);
        }
        this.detailsService.setParticients(this.participents);
        this.popupIsLoading = false;
      },
      error => { }
    );
  }

  changeBackground(id,event){
    event.preventDefault();
    this.changeBackgroundColor = true;
    this.dragFolderId = id;

    if(event._dndDropzoneActive === true){
      console.log('albertactive');
    }

}

changeDefaultBackground(event){
  event.preventDefault();

  if(!event._dndDropzoneActive){
    this.changeBackgroundColor = false;
  }

}

  uploadFiles(event, folder){

   
    this.changeBackgroundColor = false;
    console.log('tst');

    console.log('this.userAccountLevelRoleId',this.userAccountLevelRoleId);
    console.log('this.viewerObj?.role_id',this.viewerObj?.role_id);
    console.log('this.viewerObj.huddle_type',this.viewerObj.huddle_type);

    //return;

    if(this.userAccountLevelRoleId !='125' && !(this.viewerObj?.role_id==220 && this.viewerObj.huddle_type=='1' )){

      console.log('permission access');
    }else{
      console.log('permission denied');
      this.toastr.ShowToastr('error', 'You do not have permissions to upload the files');
      return false;
    }



    var files;
    event.preventDefault();
    var data = event.dataTransfer.items;
    files = data;
    let that = this;
    let obj: any = {};
    obj.user_current_account = that.sessionData.user_current_account;
    obj.account_folder_id = this.huddle_id;
    obj.huddle_id = this.huddle_id;

 
    // let that = this;
    // let obj: any = {};
    // obj.user_current_account = that.sessionData.user_current_account;
    // obj.account_folder_id = null;
    // obj.huddle_id = null;
    // filestack handle key added
    // filestack url added
    obj.account_id = that.sessionData.user_current_account.accounts.account_id;
    obj.site_id = that.sessionData.site_id;
    obj.user_id = that.sessionData.user_current_account.User.id;
    obj.current_user_role_id = that.sessionData.user_current_account.roles.role_id;
    obj.current_user_email = that.sessionData.user_current_account.User.email;
    obj.suppress_render = false;
    obj.suppress_success_email = false;
    obj.workspace = this.pageType=='workspace-page'?true : false;
    obj.activity_log_id = "";
    obj.video_id = "";
    obj.direct_publish = true;
    obj.url_stack_check = 1;
    obj.parent_folder_id = folder.id

    // console.log('obj',obj);
    // return;
    this.headerService.UpdateUploadFilesList({files, obj})

    if(this.huddle_id) {
      obj.huddle_id = this.huddle_id;
    }
  }

  ImageUrlBuilder(participent: any) {
    if (participent.image == 'groups') {
      this.isGroupImage = true
      return true
    } else {
      this.isGroupImage = false
      let image = participent.image || "assets/video-huddle/img/c1.png";
      let url = `${this.staticImageServiceIp}${participent.user_id}/${
        participent.image
        }`;
      return participent.image ? url : image;
    }

  }
  private SubscribeSearch() {
    this.searchInput
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(value => {
        let url = this.fullRouter.url;
        if (this.SearchString) {
          this.fullRouter.navigate([], {
            queryParams: { search: this.SearchString }, queryParamsHandling: 'merge'
          });
        }
        else {
          this.fullRouter.navigate([], {
            queryParams: { search: null }, queryParamsHandling: 'merge'
          });
        }

        this.search();




      });
  }

  public search() {
    this.initVars(true, true);

  }

  private LoadFilterSettings() {
    this.Filters = {};
    this.ModalRefs = { newFolderModal: "" };

    this.Filters.type = this.NVals(
      this.sessionService.GetItem("huddle_type"),
      ["0", "1", "2", "3"],
      "0"
    );
    this.Filters.layout = this.NVals(
      this.sessionService.GetItem("huddle_layout"),
      ["0", "1"],
      "0"
    );
    this.Filters.sort_by = this.NVals(
      this.sessionService.GetItem("huddle_sort"),
      ["0", "1", "2", "3"],
      "0"
    );
    if (this.Filters.type == 3 && !this.Assessment_huddle_permissions()) {
      this.DigestFilters("huddle_type", "0");
    }
  }
  private NVals(src, possible_values, default_value) {
    return possible_values.indexOf(src) > 0 ? src : default_value;
  }

  public DigestFilters(key, value, force_block?) {
    this.resource_filter = +value;
    this.sessionService.SetItem(key, value);
    if (key != "huddle_layout" && !force_block) {
      this.initVars(true, true);
    }
  }

  public Assessment_huddle_permissions() {
    let data: any = this.headerService.getStaticHeaderData();
    this.assessment_permissions =
      data.user_permissions.UserAccount.manage_evaluation_huddles == 1;
    return this.assessment_permissions;
  }

  private GetParams() {
    this.activatedRoute.params.subscribe(p => {
      this.huddle_id = p.id;
      this.documentFolderId = p.folderId;
    })
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(`huddle-details-${this.huddle_id}`).pipe(distinctUntilChanged()).subscribe(data => {
      this.processEventSubscriptions(data);
    })
    );
    if (this.huddle_id) {
      this.params = this.huddle_id;

      this.detailsService.SetParams(this.params);

      let obj: any = {
        folder_id: this.huddle_id,
        goal_evidence: Boolean(this.goalEvidence),
        document_folder_id: this.documentFolderId
      };
      let sessionData: any = this.headerService.getStaticHeaderData();

      ({
        User: { id: obj.user_id },
        accounts: { account_id: obj.account_id }
      } = sessionData?.user_current_account);

      let ref = this.homeService.GetBreadcrumbs(obj).subscribe((data: any) => {





      });
    } else {
    }


    this.activatedRoute.params.subscribe(p => {
      this.params = p;
      this.huddle_id = p.id;
      if (p.id) {
      }
    });
  }
  private initVars(preserve_params?, preserve_search_string?) {
    this.LoadFilterSettings();
    this.page = 1;
    this.Loadings = {};
    this.foldersExpanded = true;

    if (!preserve_search_string) {
      this.SearchString = "";
    }

    this.FoldersTreeData = {};
    this.subscriptionRefs = {};
    this.EditableFolder = {};
    this.PipeTrigger = false;
    let sessionData: any = this.headerService.getStaticHeaderData();
    this.permissions.allow_move =
      sessionData.user_current_account.users_accounts.folders_check == 1;
    this.Loadings.isNextPageLoading;
    if (!preserve_params) {
      this.params = {};
    }

    if (this.HuddlesAndFolders != void 0)
      if (
        void 0 != this.HuddlesAndFolders.huddle_create_permission ||
        this.HuddlesAndFolders.folder_create_permission != void 0
      ) {
        let copy = JSON.parse(JSON.stringify(this.HuddlesAndFolders));
        this.HuddlesAndFolders = {};
        ({
          huddle_create_permission: this.HuddlesAndFolders
            .huddle_create_permission,
          folder_create_permission: this.HuddlesAndFolders
            .folder_create_permission
        } = copy);

        copy = {};
      } else {
        this.HuddlesAndFolders = {};
      }

    this.colors = this.headerService.getColors();
  }

  public Onclickonparticipant() {
    this.ShowParticipentsModal(this.participantsModal, "lg_popup");
    this.participents = []
    this.popupIsLoading = true;
    this.GetParticipents();

  }

  public ShowParticipentsModal(template: TemplateRef<any>, class_name) {
    this.ModalRefs.participentsModal = this.modalService.show(template, {
      class: class_name
    });
  }
  timedateSpanish(timedate, index) {
    let d = timedate.split(',')
    if (index == 1) return d[0]
    if (index == 2) {
      let dd = timedate.split(' ')
      return dd[3] + ' ' + dd[4]
    }
  }
  public LoadNextPage(increment?) {

    if (increment) this.page++;
    if ((this.previous_page + 1) != this.page) {
      this.page = (this.previous_page + 1);
    }
    let reload_current_page = false;
    // let sessionData: any = this.headerService.getStaticHeaderData();

    let url = this.fullRouter.url.split("/");
    let { id } = this.huddle_id;
    let {
      user_current_account: {
        accounts: { account_id },
        User: { id: user_id },
        roles: { role_id }
      }
    } = this.sessionData;

    // if(this.folderId) {
    //   this.page = 1
    // }
    let obj: any = {
      huddle_id: this.huddle_id,
      title: this.SearchString || "",
      account_id,
      user_id,
      role_id,
      page: this.page,
      doc_type: this.resource_filter || 0,
      sort: this.sort,
      goal_evidence: Boolean(this.goalEvidence),
      artifact_moved_count: this.artifact_moved_count,
      with_folders: 1
    };

    if(this.folderId) {
      obj.parent_folder_id = this.folderId
    }

    this.Loadings.isNextPageLoading = true;
    let _interval_id = setInterval(() => {

      if (this.Loadings.isNextPageLoading) {
        this.detailsService.Loadings.IsLoadingArtifacts.emit(true);
      } else {
        clearInterval(_interval_id);
      }

    }, 200);
    if (this.activities_request_ref) {
      this.activities_request_ref.unsubscribe();
    }
    this.getBreadCrumbs();
    this.activities_request_ref = this.detailsService.GetArtifacts(obj).subscribe((data: any) => {
      this.parentFolderId = data.huddles_data.Huddle.parent_folder_id;
      if (data.success) {
        if(this.page == 1) {
          data?.folders ? this.huddleFolders = [...data?.folders] : false;
        }

        this.GetCirqLiveData(data);// Get cirqlive data after getting artifacts
        this.previous_page = this.page;
        this.Loadings.is_loading_huddles = false;
        this.huddle_type = data.huddle_type;
        this.h_live_stream_permission = data.allow_live_stream;
        if(this.huddle_type==3){
          let url=`/video_huddles/assessment/${this.huddle_id}`
          this.fullRouter.navigate([url])

        }
        let artifactObj = {
          huddle_info: data.huddle_info,
          huddle_type: data.huddle_type,
          users_accounts: data.users_accounts,
          role_id: data.role_id,
          evaluators_ids: data.evaluators_ids,
          participants_ids: data.participants_ids,
          dis_mem_del_video: data.dis_mem_del_video,
          coachee_permissions: data.coachee_permissions,
          is_evaluator: data.is_evaluator,
          submission_date_passed: data.submission_date_passed,
          get_account_video_library_permissions: data.get_account_video_library_permissions
        }
        this.viewerObj = {
          role_id: artifactObj.role_id,
          huddle_type: artifactObj.huddle_type,
          isEvaluator:artifactObj.is_evaluator
        };
        const huddleHeaderData = {
          id: this.huddle_id,
          name: data.title_for_header == "" ? data.huddle_info[0].name : data.title_for_header,
          huddle_type: data.huddle_type,
          role_id: data.role_id,
          isEvaluator: data.is_evaluator,
          userAccountLevelRoleId: this.userAccountLevelRoleId
        }
        this.detailsService.updateHuddleHeaderDataSource(huddleHeaderData);
        this.addBtnPermissionLoaded = true;
        this.detailsService.setObj(artifactObj);
        this.additionalData = artifactObj;
        if (1) {
          if (this.page == 1) {
            (this.total_artifacts = data.artifects.total_records);
            if (data.isGoalViewer) this.userAccountLevelRoleId = 125;
          }
          this.loading_huddles = true;
          this.huddle_type = data.huddle_type
          this.artifacts_name = data.huddle_info[0].name;
          this.user_huddle_role = data.role_id;
        } else {
          this.toastr.ShowToastr('info',`${this.translation.artifact_somethingwentwrongpleaseretrylater}`);
        }

        if (this.page == 1) {
          // this.huddleFolders = [...data.folders]
          this.artifacts = data.artifects.all;
          this.detailsService.setArtifactlList(this.artifacts);

        } else {
          this.artifacts = [...this.artifacts, ...data.artifects.all];
          data.artifects.all.length == 0 && (this.total_artifacts = this.total_artifacts);
          this.detailsService.setArtifactlList(this.artifacts);

        }
        setTimeout(() => {
          if (this.artifacts.length == 0)
            this.noRecorFound = true

        }, 200);

      } else {
            this.toastr.ShowToastr('error',data.message);
            this.fullRouter.navigate(["/list"]);
      }
      setTimeout(() => {
        this.Loadings.isNextPageLoading = false;
        this.detailsService.Loadings.IsLoadingArtifacts.emit(false);
      }, 200);

      this.activities_request_ref.unsubscribe();
    }, error => {
      this.toastr.ShowToastr('error',error.message);
      this.fullRouter.navigate(["/list"]);
    });
  }
  public OnSearchChange(event) {
    this.searchInput.next(event);

  }

  getAcitives() {

    let sessionData: any = this.headerService.getStaticHeaderData();

    let obj: any = {
      folder_id: this.huddle_id,
      account_id: sessionData.user_current_account.accounts.account_id,
      user_id: sessionData.user_current_account.User.id,
      role_id: sessionData.user_current_account.roles.role_id,
      goal_evidence: Boolean(this.goalEvidence)
    };

    this.detailsService.getActivities(obj).subscribe(
      data => {
        this.actvities = data;
        this.detailsService.setActivities(this.actvities)
        this.popupIsLoading = false;
      },
      error => { }
    );
  }




  changeToggle(value: string) {
    this.ToggleView = value;
  }



  public trigger_model(v) {
    switch (v) {
      case "rename":
        this.ShowParticipentsModal(this.renameModal, "sm_popup");
        break;
      case "crope":
        this.ShowParticipentsModal(this.cropVideo, "lg_popup");
        break;
      case "duplicate":
        break;
      case "Delete":
        this.ShowParticipentsModal(this.delete_confim, "sm_popup");
        break;
      case "email":
        this.ShowParticipentsModal(this.email, "lg_popup");
      default:
        break;
    }
  }

  public Onclickactivity() {
    this.ShowParticipentsModal(this.activity, "lg_popup");
    this.actvities = []
    this.popupIsLoading = true;
    this.getAcitives();

  }

  public Newdiscussion() {
    this.ShowParticipentsModal(this.newDiscussions, "lg_popup");
  }


  public onMediaUpload(event) {
    if (event.from && event.files.length) {
      for (let file_key in event.files) {
        if (event.files.hasOwnProperty(file_key)) {
          let file = event.files[file_key];
          let obj: any = {};
          let sessionData: any = this.headerService.getStaticHeaderData();
          obj.user_current_account = sessionData.user_current_account;
          obj.account_folder_id = this.huddle_id;
          obj.huddle_id = this.huddle_id;
          // filestack handle key added
          obj.fileStack_handle = file.handle;
          // filestack url added
          obj.fileStack_url = file.url;
          obj.account_id = sessionData.user_current_account.accounts.account_id;
          obj.site_id = sessionData.site_id;
          obj.user_id = sessionData.user_current_account.User.id;
          obj.current_user_role_id = sessionData.user_current_account.roles.role_id;
          obj.current_user_email = sessionData.user_current_account.User.email;
          obj.suppress_render = false;
          obj.suppress_success_email = false;
          obj.workspace = false;
          obj.activity_log_id = "";
          obj.direct_publish = event.from == "Upload";
          obj.video_file_name = file.filename;
          obj.stack_url = file.url;
          obj.video_url = file.key;
          obj.video_id = "";
          obj.video_file_size = file.size;
          obj.direct_publish = true;
          obj.video_title = file.filename.split(".")[0];
          obj.url_stack_check = 1;
          obj.parent_folder_id = this.folderId ? this.folderId : 0
          if (event.from == "Resource") {
            this.detailsService.uploadResource(obj).subscribe((data: any) => {
              this.toastr.ShowToastr('info',`${this.translation.artifact_newresourceuploadedsuccessfully}`);
            },error=>{

            });
          }
          else {
            if(file.videoDuration) obj.video_duration = file.videoDuration;
            obj.video_source = (file.video_source) ? file.video_source : GLOBAL_CONSTANTS.RECORDING_SOURCE.FILESTACK;
            obj.millicast_rec_id = (file.millicast_rec_id) ? file.millicast_rec_id : null;
            obj.streamName = (file.streamName) ? file.streamName : null;
            this.detailsService.uploadVideo(obj).subscribe((data: any) => {
              let validAudioType =  this.headerService.isAValidAudio(data.data.file_type);
              if(validAudioType==false){
                this.toastr.ShowToastr('info',`${this.translation.artifact_newvideouploadedsuccessfully}`);
              }
              else {
                this.toastr.ShowToastr('info',`${this.translation.workspace_new_audio_uploaded}`);

              }

            },error=>{

            });
          }

        }
      }
    }
  }
  routeRefresh() {
    this.ngOnInit();
  }
  public OpenModal(modelName) {
    this.popModal.showModal(modelName)
  }

  public FileClicked(from, file) {

    if (from == "td") {

      if (file.stack_url && file.stack_url != null) {
      }
      else {
        this.DownloadFile(file);
      }
    }
    else {
      this.DownloadFile(file);
    }
  }

  private DownloadFile(file) {
    this.detailsService.DownloadFile(file.doc_id);
  }

  private observeNewRecords() {


  }

  private processEventSubscriptions(data) {
    console.log(data);
    
      switch (data.event) {
        case "resource_added":
          if(data.is_folder == 1) {

            if(data.huddle_id) {
              if(this.huddle_id == data.huddle_id) {
                this.huddleFolders.push(data.data)
              }
            }
            else {
              if(data.data.parent_folder_id && data.data.parent_folder_id != "0"){
                this.huddleFolders.forEach(element => {
                  if(data.data.parent_folder_id == element.id) {
                    element.stats.huddleFolders += 1;
                  }
                });
                this.huddleFolders.push(data.data)
              }
            }
          }
          else if(data.data.parent_folder_id && data.data.parent_folder_id != "0") {
            this.huddleFolders.forEach(element => {
              if(data.data.parent_folder_id == element.id) {
                if(data.data.doc_type == 1) {
                  element.stats.videos += 1;
                } else if(data.data.doc_type == 2) {
                  element.stats.resources += 1;
                } else if((data.data.is_processed == 4 || data.data.is_processed == 5) && data.data.doc_type == 3) {
                  element.stats.videos += 1;
                } else if(data.data.doc_type == 3) {
                  element.stats.scripted_notes += 1;
                } else if(data.data.doc_type == 4) {
                  element.stats.urls += 1;
                }
              }
            });
            this.processResourceAdded(data);
          }
          else if(this.resource_filter==0 || this.resource_filter == 1 && data.data.doc_type==1 || this.resource_filter == 2 && data.data.doc_type==2 || this.resource_filter == 3 && data.data.doc_type==3 || this.resource_filter == 5 && data.data.doc_type==5) {
            let should_wait = 0;
            if (data.is_video_crop) {
              should_wait = 1;
            }
            this.processResourceAdded(data);
          }
          break;

        case "resource_renamed":
          this.processResourceRenamed(data.data, data.is_dummy, data);
          break;

        case "resource_deleted":
          this.processResourceDeleted(data.data, data.deleted_by, data);
          break;
        case "resource_moved":
            this.processResourceMoved(data);
            break;
        default:
          break;
      }

  }

  private processResourceMoved(data) {
    if(this.folderId == data.dest_folder_id) {
      this.artifacts.unshift(data.data)
    } else {
      if(this.resource_filter != data.data.doc_type) {         //If filter is selected and user move the artifact we don't have to apply socket
        this.artifacts = this.artifacts.filter(function( obj ) {
          return obj.doc_id !== data.item_id;
        });
      }
    }

    if(data.data.parent_folder_id && data.data.parent_folder_id != "0"){
      this.huddleFolders.forEach(element => {
        if(data.data.parent_folder_id == element.id) {
          if(data.data.doc_type == 1) {
            element.stats.videos += 1;
          } else if(data.data.doc_type == 2) {
            element.stats.resources += 1;
          } else if((data.data.is_processed == 4 || data.data.is_processed == 5) && data.data.doc_type == 3) {
            element.stats.videos += 1;
          } else if(data.data.doc_type == 3) {
            element.stats.scripted_notes += 1;
          } else if(data.data.doc_type == 4) {
            element.stats.urls += 1;
          }
        }
      });
    }


    if(data.old_folder_id){
      this.huddleFolders.forEach(element => {
        if(data.old_folder_id == element.id) {
          if(data.data.doc_type == 1) {
            element.stats.videos--;
          } else if(data.data.doc_type == 2) {
            element.stats.resources--;
          } else if((data.data.is_processed == 4 || data.data.is_processed == 5) && data.data.doc_type == 3) {
            element.stats.videos--;
          } else if(data.data.doc_type == 3) {
            element.stats.scripted_notes--;
          } else if(data.data.doc_type == 4) {
            element.stats.urls--;
          }
        }
      });
    }

    if(this.resource_filter != data.data.doc_type) {         //If filter is selected and user move the artifact we don't have to apply socket
      //decreament of count in main huddle
      if(this.huddle_id == data.huddle_id && data.old_folder_id == 0 && this.folderId == undefined) {
        if (this.resource_filter == 0) {
          this.total_artifacts--;
        } else if ((this.resource_filter == 1 && data.data.doc_type == 1) || (this.resource_filter == 1 && data.data.doc_type == 3 && data.data.is_processed >= 4) || (this.resource_filter == 3 && data.data.doc_type == 3 && data.data.is_processed < 4)) {
          this.total_artifacts--;
        } else if ((this.resource_filter == 2 && data.data.doc_type == 2) || (this.resource_filter == 5 && data.data.doc_type == 5)) {
          this.total_artifacts--;
        }
      }

      //decreament of count in folder/main when artifact move
      if(this.folderId) {
        if (data.old_folder_id == this.folderId){
          if (this.resource_filter == 0) {
            this.total_artifacts--;
          } else if ((this.resource_filter == 1 && data.data.doc_type == 1) || (this.resource_filter == 1 && data.data.doc_type == 3 && data.data.is_processed >= 4) || (this.resource_filter == 3 && data.data.doc_type == 3 && data.data.is_processed < 4)) {
            this.total_artifacts--;
          } else if ((this.resource_filter == 2 && data.data.doc_type == 2) || (this.resource_filter == 5 && data.data.doc_type == 5)) {
            this.total_artifacts--;
          }
        }
      }
    }

    //Increament of count in folder when artifact move in to a folder
    if(this.folderId) {
      if (data.dest_folder_id == this.folderId){
        if (this.resource_filter == 0) {
          this.total_artifacts++;
        } else if ((this.resource_filter == 1 && data.data.doc_type == 1) || (this.resource_filter == 1 && data.data.doc_type == 3 && data.data.is_processed >= 4) || (this.resource_filter == 3 && data.data.doc_type == 3 && data.data.is_processed < 4)) {
          this.total_artifacts++;
        } else if ((this.resource_filter == 2 && data.data.doc_type == 2) || (this.resource_filter == 5 && data.data.doc_type == 5)) {
          this.total_artifacts++;
        }
      }
    }

  }

  private processResourceAdded(socketData: any) {
    if(this.resource_filter==0 || this.resource_filter == 1 && socketData.data.doc_type==1 || this.resource_filter == 2 && socketData.data.doc_type==2 || this.resource_filter == 3 && socketData.data.doc_type==3 || this.resource_filter == 5 && socketData.data.doc_type==5){
      let should_wait = (socketData.is_video_crop) ? 1 : 0;
      const resource = socketData.data;
      let wait_time = 0;
      if (should_wait) {
        wait_time = 10000;
        resource.published = 0;
      }

      if(this.folderId) {
        if (resource.parent_folder_id == this.folderId){
          this.artifacts.unshift(resource);
          if(this.resource_filter == 0){
            this.total_artifacts++;
          }else if((this.resource_filter == 1 && resource.doc_type == 1) || (this.resource_filter == 1 && resource.doc_type == 3 && resource.is_processed >= 4) || (this.resource_filter == 3 && resource.doc_type == 3 && resource.is_processed < 4)){
            this.total_artifacts++;
          }else if((this.resource_filter == 2 && resource.doc_type == 2) || this.resource_filter == 5 && resource.doc_type == 5){
            this.total_artifacts++;
          }
        }
      } else {
        if(!resource.parent_folder_id || resource.parent_folder_id == "0") {
          this.artifacts.unshift(resource);
          if(this.resource_filter == 0){
            this.total_artifacts++;
          }else if((this.resource_filter == 1 && resource.doc_type == 1) || (this.resource_filter == 1 && resource.doc_type == 3 && resource.is_processed >= 4) || (this.resource_filter == 3 && resource.doc_type == 3 && resource.is_processed < 4)){
            this.total_artifacts++;
          }else if((this.resource_filter == 2 && resource.doc_type == 2) || this.resource_filter == 5 && resource.doc_type == 5){
            this.total_artifacts++;
          }
        }
      }

      if (this.sessionData.user_current_account.User.id != resource.created_by && resource.doc_type !=4) {
        this.toastr.ShowToastr('info',this.translation.artifact_newartifactadded);
      }

    }
  }

  private processResourceDeleted(resource_id, deleted_by, socketData) {

    if(socketData.is_folder) {

      this.huddleFolders = this.huddleFolders.filter(x => {
        return x.id !== resource_id;
      });
    } else {
      var indexOfMyObject = this.artifacts.findIndex(x => {
        return x.doc_id == resource_id || x.id == resource_id
      });

      if (indexOfMyObject > -1) {
         let obj = this.artifacts[indexOfMyObject];
         this.artifacts.splice(indexOfMyObject, 1);
         if (deleted_by != this.sessionData.user_current_account.User.id) {
           this.toastr.ShowToastr('info',`${this.translation.artifact_artifactdeleted}`);
         }
      }
      this.total_artifacts--;
    }
    if(socketData.parent_folder_id  && socketData.parent_folder_id != "0") {
      this.huddleFolders.forEach(element => {
        if(socketData.parent_folder_id == element.id) {

          if(socketData.is_folder) {
            element.stats.folders--;
          } else if(socketData.doc_type == 1) {
            element.stats.videos--;
          } else if(socketData.doc_type == 2) {
            element.stats.resources--;
          } else if((socketData.is_processed == 4 || socketData.is_processed == 5) && socketData.doc_type == 3) {
            element.stats.videos--;
          } else if(socketData.doc_type == 3) {
            element.stats.scripted_notes--;
          } else if(socketData.doc_type == 4) {
            element.stats.urls--;
          }
        }
      });
    }
  }

  private processResourceRenamed(resource, dont_show_toast = 0, socketData:any) {
    let objResource = _.find(this.artifacts, function (item) {
      return (parseInt(item.id) == parseInt(resource.id)) || parseInt(item.doc_id) == parseInt(resource.doc_id);
    });
    let index = -1;
    this.artifacts.forEach((item, i) => {
      if ((parseInt(item.id) == parseInt(resource.id)) || parseInt(item.doc_id) == parseInt(resource.doc_id)) {
        index = i;
      }

    });

    if (objResource) {

      if(dont_show_toast == 1){
        this.artifacts[index].total_comments = resource.total_comments;
        this.artifacts[index].total_attachment = resource.total_attachment;
        if(socketData && socketData.live_video_event && socketData.live_video_event == "1"){
        this.artifacts[index].video_is_saved = resource.video_is_saved;
        this.artifacts[index].doc_type = resource.doc_type;
        this.artifacts[index].published = resource.published;
        this.artifacts[index].thubnail_url = resource.thubnail_url;
        this.artifacts[index].video_duration = resource.video_duration;
        }

      }else{
        this.artifacts[index] = resource;
        resource.video_duration == -1 ?  this.artifacts[index].video_duration=0 : '';
          objResource.title.slice(0, 25)
          if (resource.doc_type != 5 && this.sessionData.user_current_account.User.id != (resource.updated_by || resource.last_edit_by)) {

            if (objResource.title.length > 25) {
              this.toastr.ShowToastr('info',"'" + objResource.title + `'... ${this.translation.artifact_renamedto} '` + resource.title + "'...");
            } else {
              this.toastr.ShowToastr('info',"'" + objResource.title + `' ${this.translation.artifact_renamedto} '` + resource.title + "'");
          }
        }
      }
    } else if(socketData.video_source === GLOBAL_CONSTANTS.RECORDING_SOURCE.CUSTOM) this.processResourceAdded(socketData);


    if(socketData.is_folder == 1) {                       //Change name of a Folder
      this.huddleFolders.forEach(element => {
        if(element.id == resource.id) {
          element.name = resource.name;
          element.title = resource.name;
        }
      });
    }
  }

  public getBreadCrumbs() {
    let obj: any = {
      folder_id: this.huddle_id,
      goal_evidence: Boolean(this.goalEvidence),
      document_folder_id: this.documentFolderId

    };
    let sessionData: any = this.headerService.getStaticHeaderData();

    ({
      User: { id: obj.user_id },
      accounts: { account_id: obj.account_id }
    } = sessionData.user_current_account);

    let ref = this.homeService.GetBreadcrumbs(obj).subscribe((data: any) => {
      if (data.success == -1) {
        this.toastr.ShowToastr('info',data.message);

        setTimeout(() => {
          this.fullRouter.navigate(['/list'])
        }, 4000);
        this.homeService.updateBreadcrumb(data)

      }
      else {
        let folders = data.folders;
        let docFolder = data.document_folders;

        let huddleObj=folders[folders.length - 1];
        this.huddle_name = huddleObj ? huddleObj.folder_name :  folders[0].folder_name;
        if(data?.document_folders?.length <= 0) {
          folders.push({
            folder_name: this.translation.artifacts_breacrum,
            folder_id: folders[0].folder_id,
            isCustom: true
          });
        } else {
          docFolder.push({
            folder_name: this.translation.artifacts_breacrum,
            folder_id: folders[0].account_folder_id,
            isCustom: true
          })
        }
        this.discussionService.SetBreadcrumbs(folders);
        const crumbs = [...folders, ...docFolder]
        this.homeService.updateBreadcrumb(crumbs);

      }



    });
  }


  public OnHuddleEdit(huddle_id: number) {
		this.fullRouter.navigate(['/add_huddle_angular/edit', huddle_id]);
  }


  showDeleteHuddleModal(huddle_id): void {
    this.DeletableItem=huddle_id;
    this.deleteHuddleModal.show();
  }

  hideDeleteHuddleModal(): void {
    this.deleteHuddleModal.hide();
  }

  public ConfirmDelete() {
		;
		if (this.Inputs.ConfirmationKey != this.Inputs.Confirmation) {

			this.toastr.ShowToastr('info',this.translation.huddle_you_typed_in + " '" + this.Inputs.ConfirmationKey + "' ");
			return;
		}

		let obj: any = {};

			obj.huddle_id = this.DeletableItem;
          let sessionData: any = this.headerService.getStaticHeaderData();
          obj.user_id = sessionData.user_current_account.User.id;
			this.homeService.DeleteItem(obj, false).subscribe((d: any) => {

				this.hideDeleteHuddleModal();

				if (d.success) {
          this.toastr.ShowToastr('info',d.message);
          this.fullRouter.navigate(['/video_huddles/list'])
				} else {
					this.toastr.ShowToastr('info',d.message);
				}

			});

  }
  public TriggerTextChange(ev) {

		if (ev.keyCode == 13) {
			this.ConfirmDelete()

		}
  }

  folderEdit(event, folderName, account_folder_id = 0) {
    this.bsModalRef = this.modalService.show(SharedFolderModal, {ariaDescribedby: 'my-modal-description',ariaLabelledBy: 'my-modal-title'});
    this.bsModalRef.content.parentFolderId = event.id;
    this.bsModalRef.content.NewFolderName = folderName;
    this.bsModalRef.content.huddleId = account_folder_id;
    this.bsModalRef.content.folderId = event.parentFolderId;
  }

  onDrop(event: DndDropEvent, target: any) {
    // console.log(event,target);

    let obj = {
			folder_id: target.id,
      object_id: event.data.doc_id, //Artifacts id
      site_id: 1,
      account_id: target.account_id,
      account_folder_id: target.account_folder_id,
      workspace: 0,
      user_id: this.sessionData.user_current_account.users_accounts.user_id
    }
    this.newfolderService.moveDocumentFolder(obj).subscribe((data: any) => {
      if(data.success) {
        this.toastr.ShowToastr('info',this.translation.df_artifact_moved_to_folder);
        this.artifact_moved_count++;
      } else {
        this.toastr.ShowToastr('info',data.message);
      }
    })
  }

  OnFolderDelete(data) {
    this.bsModalRef = this.modalService.show(ConfirmationDialogComponent, {ariaDescribedby: 'my-modal-description',ariaLabelledBy: 'my-modal-title'});
    this.bsModalRef.content.folderData = data;
    this.bsModalRef.content.title = data.title
  }


  showMoveHuddleModal(): void {
    this.showMomeoptions=true
    this.moveHuddleModal.show();
  }

  hideMoveHuddleModal(): void {
    this.showMomeoptions=false

    this.moveHuddleModal.hide();
  }

  public OnFolderClick(folder_id, tree) {

	}

  public OnFolderMove(folder_id) {
    let isHuddle=true;
   let hType= this.huddle_type

		if (!folder_id) return;

		this.MovableItem = {
			id: folder_id,
			isHuddle: Boolean(isHuddle),
			type: hType
		};

		let sessionData: any = this.headerService.getStaticHeaderData();

		let obj: any = {

			account_id: sessionData.user_current_account.accounts.account_id,
			user_id: sessionData.user_current_account.User.id,
			id: isHuddle ? "" : folder_id

		};

		let ref = this.homeService.GetFolderList(obj).subscribe((data) => {

			this.FoldersTreeData = this.list_to_tree(data, 'parent');
      this.showMoveHuddleModal()
			ref.unsubscribe();

		});


  }
  private list_to_tree(list, parentProp) {
		var map = {}, node, roots = [], i;
		for (i = 0; i < list.length; i += 1) {
			map[list[i].id] = i;
			list[i].children = [];
		}
		for (i = 0; i < list.length; i += 1) {
			node = list[i];
			node.name = node.text;
			if (node[parentProp] !== "#") {
				if (list[map[node[parentProp]]]) {
					list[map[node[parentProp]]].children.push(node);
				} else {
				}

			} else {
				roots.push(node);
			}
		}
		return roots;
	}

  public getMovePath(tree) {

		if (!tree || !tree.treeModel || !tree.treeModel.getActiveNode() || !tree.treeModel.getActiveNode().data) {
			return;
		}
		let head = tree.treeModel.getActiveNode();

		if (tree.treeModel.getActiveNode().id == -1) return [tree.treeModel.getActiveNode().data];

		let arr = [];



		while (head.parent != null) {

			if (head.data) {

				arr.push(head.data);

			}
			else if (head.treeModel.getActiveNode()) {

				arr.push(head.treeModel.getActiveNode().data);

			}

			head = head.parent;
		}

		if (arr.length == 0) return [tree.treeModel.getActiveNode().data];

		return arr.reverse();

	}

	public MoveItem(tree) {

		if (!tree.treeModel.getActiveNode()) {

			this.toastr.ShowToastr('info',this.translation.huddle_no_traget_folder);

		}

		if (!(tree.treeModel.getActiveNode() && this.MovableItem.id)) return;

		if (this.Loadings.MovingItem) {
			return;
		}

		let target = tree.treeModel.getActiveNode().data;

		let obj = {
			folder_id: target.id,
			huddle_id: this.MovableItem.id
		}

		if (this.MovableItem.isHuddle) {

			this.Loadings.MovingItem = true;
			this.homeService.Move(obj).subscribe((d: any) => {

				this.Loadings.MovingItem = false;
				if (d.success) {

					this.toastr.ShowToastr('info',this.translation.huddle_moved_successfully);

					this.HuddlesAndFolders.huddles = this.HuddlesAndFolders.huddles.filter((h) => {

						return h.huddle_id != this.MovableItem.id;

					});

					this.HuddlesAndFolders.total_huddles--;

					let index = _.findIndex(this.HuddlesAndFolders.folders, { folder_id: target.id });

					if (index > -1) {

						this.HuddlesAndFolders.folders[index].stats[this.MovableItem.type]++;

					}


				} else {
					this.toastr.ShowToastr('info',d.message);
				}

			});


      this.hideMoveHuddleModal();


		} else {

			this.Loadings.MovingItem = true;
			this.homeService.Move(obj).subscribe((d: any) => {
				this.Loadings.MovingItem = false;
				if (d.success) {
					this.toastr.ShowToastr('info',this.translation.huddle_folder_moved_successfully);
					this.HuddlesAndFolders.folders.forEach((f, i) => {
						if (f.folder_id == target.id) {

							f.stats[this.MovableItem.type]++;

						}
						if (f.folder_id == this.MovableItem.id) {
							this.HuddlesAndFolders.folders.splice(i, 1);
						}
					})
				} else {
					this.toastr.ShowToastr('info',d.message);
				}

				this.PipeTrigger = !this.PipeTrigger;

			});



      this.hideMoveHuddleModal();

		}


  }

  openActivityModal(){
    const initialState = {
      pageType: 'huddle',
      folder_id: this.huddle_id,
      activityData: this.actvities,
      goalEvidence: Boolean(this.goalEvidence)
    };
    this.bsModalRef = this.modalService.show(ActivityModelComponent, Object.assign({initialState}, { class: 'modalcls2' } ));
  }
  public checkValidRoute(data){
    if(data=="grid" || data=="list"){

    }
    else
    this.fullRouter.navigate(['/page-not-found']);
    return;
  }
/**
 * GetCirqliveData
 */
public GetCirqLiveData(artifactsData) {

  if(artifactsData && artifactsData.huddle_type != 3 && !this.artifactsDataLoaded){
    this.artifactsDataLoaded = true;


    let userInfoForCirq ={
    userId: artifactsData.user_info.id,
    userFirstName: artifactsData.user_info.first_name,
    userLastName: artifactsData.user_info.last_name,
    userCourseId: artifactsData.huddle_info[0].account_folder_id,
    userCourseName: encodeURIComponent(artifactsData.huddle_info[0].name),
    userRole: (artifactsData.role_id == 200 && artifactsData.users_accounts.role_id !=125) ? 'teacher' : 'student',
    id: this.huddle_id,
    name: artifactsData.huddle_info[0].name,
    huddle_type: artifactsData.huddle_type,
    role_id: artifactsData.role_id,
    isGoalViewer: artifactsData.isGoalViewer,
    isEvaluator: artifactsData.is_evaluator
  }

  this.detailsService.GetCirqLiveData().subscribe((data) => {
  this.cirqLiveData = data;
  if(this.cirqLiveData){
  this.cirqLiveData={...this.cirqLiveData,...userInfoForCirq};
  this.cirqPath = `${environment.APIbaseUrl}/get_cirq_page_data?u_id=${this.cirqLiveData.userId}&u_f_name=${this.cirqLiveData.userFirstName}&u_l_name=${this.cirqLiveData.userLastName}&lti_email=${encodeURIComponent(this.sessionData.user_current_account.User.email)}&course_id=${this.cirqLiveData.userCourseId}&course_name=${this.cirqLiveData.userCourseName}&role=${this.cirqLiveData.userRole}&lti_sub_domain=${this.cirqLiveData.lti_sub_domain}&lti_key=${this.cirqLiveData.lti_zoom_key}&lti_secret=${this.cirqLiveData.lti_zoom_secret}`;
  this.detailsService.updateCirqliveData(this.cirqLiveData);// send data to service for using in other components
  }else{
    this.detailsService.updateCirqliveData('disabled');
  }
  });
}
}

public ToggleFoldersExpand(flag) {
  this.foldersExpanded = flag;
  localStorage.setItem('foldersExpanded',flag);
}

public openURLArtifactModal() {
  const initialState = { pageType: this.pageType, huddleId: this.huddle_id, folderId: this.folderId };
  this.modalService.show(URLArtifactModalComponent, { initialState, class: 'url-artifact-modal-container'});
}

  openNewFolderModal() {
    const initialState = { huddleId: this.huddle_id, folderId: this.folderId, pageType: 'Huddle' }
    this.bsModalRef = this.modalService.show(SharedFolderModal, {initialState, ariaDescribedby: 'my-modal-description',ariaLabelledBy: 'my-modal-title'});
  }
public openRecordVideoModal() {
  let bsModalRef;
  if(this.browserAgent !== 'safari')
    bsModalRef = this.modalService.show(RecordVideoModelComponent, { class: 'modal-container-750', backdrop: 'static' });
  else bsModalRef = this.modalService.show(MillicastRecordVideoModelComponent, { class: 'modal-container-750', backdrop: 'static' });

  bsModalRef.content.onRecordedUpload.subscribe(data => {
    this.onMediaUpload(data);
  });
}

}




interface Modals {
  [key: string]: any;
}
