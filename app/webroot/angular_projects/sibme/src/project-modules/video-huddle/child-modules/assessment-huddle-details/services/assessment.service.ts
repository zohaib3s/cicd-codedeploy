import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Subject, BehaviorSubject } from 'rxjs';
import { HeaderService } from '@src/project-modules/app/services';

@Injectable({
  providedIn: 'root'
})
export class AssessmentService {

  private workspaceVideosList: any = new Subject();
  private participantList: any = new Subject();
  public huddleData:any=new Subject();
  public huddleDataLocalSinglton=[];
  public workspaceVideosListSinglton=[];
  private huddleId:any=0;
   public deleteAlertObject=new BehaviorSubject<any>({});
   public deleteAlertObject$=this.deleteAlertObject.asObservable();

  constructor(private http: HttpClient, private headerService: HeaderService) { }

  setHuddleId(huddleId){
    this.huddleId=huddleId;
  }
  setDeleteObject(obj){
    this.deleteAlertObject.next(obj)
  }
  getHuddleId(){
    return this.huddleId;
  }
  

  setHuddleData(data){
    this.huddleData.next(data);
    this.huddleDataLocalSinglton=data;
  }
  getHuddleData(){
    return this.huddleData;
  }

  setvideoList(list) {
    this.workspaceVideosList.next(list);
    this.workspaceVideosListSinglton = list;

   }
  getVideoList() {
    return this.workspaceVideosList;
  }


  setParticipantsList(list) {
    this.participantList.next(list);
   }
  getParticipantsList() {
    return this.participantList;
  }
  getHuddleDetails(obj) {
     obj.is_web=true;
    let url = `${environment.APIbaseUrl}/view_huddle`;
    return this.http.post(url, obj);
  }

  getHuddleAssessee(obj) {
    let url = `${environment.APIbaseUrl}/get_assessees_list`;
    return this.http.post(url, obj);
  }

/**
 * New functionality added to show the accessor 
 * all the details on Show Grades button click
 */
  showFeedbackAndGrades(obj: any ): any {
    let url = `${environment.APIbaseUrl}/publish_feedback_and_grades`;
    return this.http.post(url, obj);
  }


  getCommentCount(obj) {
    let url = `${environment.APIbaseUrl}/getCommentCounts`;
    return this.http.post(url, obj);
  }
 
  getAssesseeAssessment(obj){
    let url = `${environment.APIbaseUrl}/get_user_assessment_custom_fields`;
    return this.http.post(url, obj);
  }

  getWorkspaceVideos(obj) {
    let url = `${environment.APIbaseUrl}/get_workspace_artifects`;
    return this.http.post(url, obj);
  }

  publishAssessment(data: any) {
    let url = `${environment.APIbaseUrl}/publish_assessment_huddle`;
    return this.http.post(url, data);
  }

  public exportParticipantsList(obj, to) {
    let path;
    if (to == "pdf") {
      path = environment.APIbaseUrl + "/export_assessees_pdf";
    } else if (to == "csv") {
      path = environment.APIbaseUrl + "/export_assessees_csv";
    } else if (to == "excel") {
      path = environment.APIbaseUrl + "/export_assessees_excel";
    }

    return this.http.post(path,obj);

    // let form = document.createElement("form");
    // form.setAttribute("action", path);
    // form.setAttribute("method", "post");
    // document.body.appendChild(form);

    // this.appendInputToForm(form, obj);
    // let sessionData: any = this.headerService.getStaticHeaderData();
    // this.appendInputToForm(form, { "current_lang": sessionData.language_translation.current_lang });
    //  form.submit();
    // document.body.removeChild(form);
  }

  private appendInputToForm(form, obj) {
    Object.keys(obj).forEach((key) => {
      let input = document.createElement("input");
      input.setAttribute("value", obj[key]);
      input.setAttribute("name", key);
      form.appendChild(input);
    });
  }
  
  submitAssignment(data: any) {
    let url = `${environment.APIbaseUrl}/submit_assessment_huddle`;
    return this.http.post(url, data);
  }

  saveAssessmentFields(data: any) {
    let url = `${environment.APIbaseUrl}/save_user_assessment_custom_fields`;
    return this.http.post(url, data);
  }

  deleteAssessee(data: any) {
    let url = `${environment.APIbaseUrl}/delete_assessee`;
    return this.http.post(url, data);
  }

  getUserArtifects(data: any){
    return this.http.post(`${environment.APIbaseUrl}/get_artifects`, data);
  }

  getUserAvatar(user_id: number, image: string) {
    return (
      (user_id &&
        image &&
        `https://s3.amazonaws.com/sibme.com/static/users/${user_id}/${
          image
        }`) ||
      `${environment.baseUrl}/img/home/photo-default.png`
    );
  }
}
