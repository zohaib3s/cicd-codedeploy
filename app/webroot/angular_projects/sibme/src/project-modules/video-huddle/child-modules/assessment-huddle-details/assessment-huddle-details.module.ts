import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AssessmentHuddleDetailsRoutingModule } from './assessment-huddle-details-routing.module';
import { HomeComponent } from './home/home.component';
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ModalModule } from "ngx-bootstrap/modal";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { DetailsHttpService } from '../details/servic/details-http.service';
import { AsignmentDetailSingleAsseseeComponent } from './asignment-detail-single-assesee/asignment-detail-single-assesee.component';
import { AsessorsDetailViewComponent } from './asessors-detail-view/asessors-detail-view.component';
import { AsesseeDetailViewComponent } from './asessee-detail-view/asessee-detail-view.component';
import { HuddleParticipantsListComponent } from './huddle-participants-list/huddle-participants-list.component';
import { VideoThumbComponent } from './asessee-detail-view/video-thumb/video-thumb.component';
import { ResourcesThumbComponent } from './asessee-detail-view/resources-thumb/resources-thumb.component';
import { CanDeactivateGuard } from './services/can-deactivate.guard';
import { ConfirmGuard } from './services/confirm.guard';
import { NgSlimScrollModule, SLIMSCROLL_DEFAULTS } from 'ngx-slimscroll';
import { AppLevelSharedModule } from "@shared/shared.module";
import { DetailsModule } from '../details/details.module';
import { TreeModule } from 'angular-tree-component';
import { SharedModule } from '../shared/shared.module';
import { ParticipentsDetailModal } from './modals/participents_detail_modal/participents_detail_modal.component';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { QuillModule } from 'ngx-quill';
import { ClickOutsideModule } from 'ng-click-outside';

@NgModule({
  declarations: [HomeComponent, AsignmentDetailSingleAsseseeComponent, AsessorsDetailViewComponent, AsesseeDetailViewComponent, HuddleParticipantsListComponent, VideoThumbComponent, ResourcesThumbComponent, ParticipentsDetailModal],
  imports: [
    CommonModule,
    FormsModule,
    AppLevelSharedModule,
    AssessmentHuddleDetailsRoutingModule,
    NgSlimScrollModule,
    ClickOutsideModule,
    ModalModule.forRoot(),
     BsDropdownModule.forRoot(),
      TooltipModule.forRoot(), 
      CollapseModule.forRoot(),
       DetailsModule,
       TreeModule.forRoot(),
       SharedModule,
       NgScrollbarModule,
    QuillModule.forRoot()
    
    
  ],
  providers: [{
    provide: SLIMSCROLL_DEFAULTS,
    useValue: {
      alwaysVisible : true
    }
  },DetailsHttpService,CanDeactivateGuard, ConfirmGuard]
})
export class AssessmentHuddleDetailsModule { }
