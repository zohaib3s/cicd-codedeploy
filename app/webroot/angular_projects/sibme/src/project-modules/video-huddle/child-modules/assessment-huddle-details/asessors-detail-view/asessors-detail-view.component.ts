import { Component, OnInit, AfterViewInit, ViewChild, ElementRef, HostListener, EventEmitter, Output, TemplateRef} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AssessmentService } from '../services/assessment.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DetailsHttpService } from '../../details/servic/details-http.service';
import { ShowToasterService } from '@projectModules/app/services';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { CanDeactivateGuard } from '../services/can-deactivate.guard';
import * as _ from "underscore";
import io from 'socket.io-client';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import * as timezone from 'moment-timezone';


import { HeaderService, SocketService, HomeService } from '@src/project-modules/app/services';
import { ParticipentsDetailModal } from '../modals/participents_detail_modal/participents_detail_modal.component';
import { AddEvidenceModalComponent } from '@src/project-modules/shared/modals/add-evidence-modal/add-evidence-modal.component';
import * as moment from 'moment';
declare global {
  interface Window { io: any; }
  interface Window { Echo: any; }
}

declare var Echo: any;

window.io = io;
window.Echo = window.Echo || {};

@Component({
  selector: 'asessors-detail-view',
  templateUrl: './asessors-detail-view.component.html',
  styleUrls: ['./asessors-detail-view.component.css']
})
export class AsessorsDetailViewComponent implements OnInit, AfterViewInit {
  private subscriptions: Subscription = new Subscription();
  translation:any;
  PipeTrigger: boolean;
  HuddlesAndFolders: any;
  Loadings: any={};
  FoldersTreeData: any[];
  public colors;
  public getArtifactsType: any;
  resource_allowed=1;
  bsModalRef: BsModalRef;
  public modalRef: BsModalRef;
  modalOptions: ModalOptions
  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (this.huddleData && this.huddleData.current_huddle_info && !this.isPublished) {
      $event.returnValue = false;
    }
  }

  @ViewChild('childModal', { static: false }) childModal: ModalDirective;
  @ViewChild('description', { static: false }) description: ElementRef
  @ViewChild('popUp', { static: false }) popUpModel;
  @ViewChild('popup_scroll', { static: false }) popup;
  @ViewChild('hiddenDescToCalculateActualDescLength', { static: false })
  set hiddenDescToCalculateActualDescLength(v: any) {
    setTimeout(() => { this.hiddenDescLength = v.nativeElement.offsetHeight; }, 0);
  };
  @ViewChild('deleteHuddleModal', { static: false }) deleteHuddleModal: ModalDirective;
  @ViewChild('moveHuddleModal', { static: false }) moveHuddleModal: ModalDirective;
  @ViewChild('confirmModal', { static: false }) confirmModal: ModalDirective;
  @ViewChild('searchString', { static: false }) searchString: ElementRef;

  public hiddenDescLength: number = 400;
  public VideoForm: boolean = false;
  public artifact: any = {};
  public huddle_id: any = 0;
  public sessionData: any = {};
  public videoList: any = [];
  public selectedVideo: any;
  public workspaceVideoLoading = true;
  public addVideoTitle: string = '';
  public removeVideoTitle: string = '';
  public copyVideoNotes: boolean; 
  public SearchString = ''
  public videoslist: any = [];
  public page: any = 1;
  public isAssessorOrCreator: boolean = true;
  public huddleData: any = {};
  public isDataAvailable = false;
  public is_expand = false;
  public is_enable;
  public isPublished = 0;
  public descString: string = '';
  public participantsList: any;
  huddleId: number;
  obj:any;
  activeComment:number;
  public opts: ISlimScrollOptions;
  public scrollEvents: EventEmitter<SlimScrollEvent>;
  private searchInput: Subject<string> = new Subject();
  public Inputs: { NewFolderName: string; Confirmation: string; ConfirmationKey: any; };
  public is_published_feedback : boolean = null;
  userAccountLevelRoleId:any=0;
	DeletableItem
  MovableItem: { id: any; isHuddle: boolean; type: any; };
  public isIEOpened=false;
  isResponseCompleted= false;
  public totalRecords;
  public callCount=0;
  public searchPage=1;
  public userId:number;
  public publishAll: boolean;
  public selectedVideoModal: any;
  isSubmissionValid = true;
  public selectedDocIds: any;
  constructor(
    private toastr: ShowToasterService,
    private detailsService: DetailsHttpService,
    public assessmentService: AssessmentService,
    public headerService: HeaderService,
    private router: Router,
    private Guard: CanDeactivateGuard,
    private socketService: SocketService,
    private homeService: HomeService,
    private activeRoute: ActivatedRoute,
    private modalService: BsModalService,
    ) {
      
      this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
        this.translation = languageTranslation;
        if(this.translation.artifacts_add_video){
          this.addVideoTitle = this.translation.artifacts_add_video;
          this.removeVideoTitle = this.translation.vd_cancel;
        }
      })
      );
      
    this.isIEOpened=this.headerService.isOpenedInIE();
    }

  clickme() {

  }
  public loadConfirmDialog(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: "modal-md  maxcls", backdrop: 'static' });
  }
 
  ngOnInit() {

    this.SubscribeSearch();
    this.Inputs = { NewFolderName: "", Confirmation: "", ConfirmationKey: 'DELETE' };
    this.colors = this.headerService.getColors();

    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    }

    this.sessionData = this.headerService.getStaticHeaderData();
    console.log(this.sessionData)
    this.userAccountLevelRoleId = this.sessionData.user_permissions.roles.role_id;
    setTimeout(() => {
      this.assessmentService.huddleData.subscribe((res:any)=>{
        console.log(res);
      })
      this.huddleData = this.assessmentService.huddleDataLocalSinglton;
      
      this.huddleData.assessees_list?.forEach((element:any,index) => {
       
        if(element.assessment_summary){
          element.summaryToShow=[];
         for(let key in element.assessment_summary){
           if(element.assessment_summary[key].length>0){
            //  console.log(element.assessment_summary[key][0])
             element.summaryToShow.push(element.assessment_summary[key][0])
           }
         }
        }
      });
      console.log(this.huddleData);

      this.participantsList = this.huddleData.assessors_list;
      this.huddle_id = this.huddleData.current_huddle_info;
      
      if (this.huddleData.sample_data) {
        this.huddleData.sample_data.map(item => {
          if (item.url.includes('.mp3')) item.is_mp3 = true;
          else item.is_mp3 = false;
        });
      }

      if (this.huddleData.current_huddle_info) {
        this.descString = this.huddleData.current_huddle_info.desc;
      this.huddle_id=this.huddleData.current_huddle_info.account_folder_id;


        this.isPublished = this.huddleData.current_huddle_info.is_published;
      }
      this.huddleData.publish_all == "1" ? this.publishAll = true : this.publishAll = false 
      this.huddleData.current_huddle_info?.is_published_feedback == 1 ? this.is_published_feedback = true : false
      //console.log("here : ", this.huddleData)
      //this.isDataAvailable = true;
      
    }, 3000);
    this.userId = this.sessionData.user_current_account.User.id;

    
  this.getId();
  }

  goToEditHuddle(huddleData) {
    let str = '/add_huddle_angular/assessment/edit/' + huddleData.current_huddle_info.account_folder_id
    this.router.navigate([str])
  }

  ngOnDestroy() {

    this.subscriptions.unsubscribe();
  }
  showFeedbackAndGrades(): void {

    const payload = {
      account_id: this.sessionData.user_current_account.accounts.account_id,
      user_id: this.userId,
      huddle_id: this.huddle_id
    } as any;

    this.subscriptions.add(this.assessmentService.showFeedbackAndGrades(payload).subscribe((res:any)=>{
      this.hideConfirmModal();
      this.publishAll = false;
    }, (err:any) => {
      this.hideConfirmModal();
      console.log("Something went wrong ", err);
    }))

  }

 
  public OnSearchChange(str) {
    this.searchPage=1;
    this.callCount=0;
    this.isResponseCompleted= false;
    this.workspaceVideoLoading = true;
    this.videoList = [];
    this.SearchString = str;
    this.searchInput.next(str)

  }


  private SubscribeSearch() {
    this.searchInput
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(value => {
        this.getWorkSpaceVideos()


      });
  }
  ngAfterViewInit() {

    this.assessmentService.getVideoList().subscribe(data => {
      this.videoList = data;
      this.videoList.map(video => {
        if (this.selectedVideo && this.selectedVideo.id === video.id) video.selected = true;
        else video.selected = false;
      });
      this.workspaceVideoLoading = false;
    });



    this.assessmentService.getHuddleData().subscribe((data: any) => {
      if (data) {
        this.huddleData = data;
        this.participantsList = data.assessors_list;
        this.huddleData.sample_data.map(item => {
          if (item.url.includes('.mp3')) item.is_mp3 = true;
          else item.is_mp3 = false;
        });

        if (this.huddleData.current_huddle_info) {
          this.descString = this.huddleData.current_huddle_info.desc;
          this.huddleData.submission_deadline_date = this.huddleData.submission_deadline_date.replace(/-/g, '/');;
          console.log('Dayte', this.huddleData.submission_deadline_time);
          var today = new Date();
          var now =timezone.utc(today).tz('America/Chicago').format("YYYY-MM-DD HH:mm:ss");
          let splitdate = this.huddleData.submission_deadline_date.split('/');
          let newdate =  splitdate[0] + '-' +splitdate[1] + '-' + splitdate[2] +" "+ this.huddleData.submission_deadline_time;
          // var submitDate = new Date(newdate);
          console.log('ASSESS before Date', this.huddleData.submission_deadline_time)

          var submittedDate = moment(newdate).format("YYYY-MM-DD HH:mm:ss")
          console.log('ASSESS Date', submittedDate)
        
          if (submittedDate < now) {
            this.isSubmissionValid = false;
          }
          
          this.isPublished = this.huddleData.current_huddle_info.is_published;
        }
        this.isDataAvailable = true;

        this.socketPushFunctionDiscussionDetail();
        if (this.huddleData.current_user_huddle_role_id == 200 || this.sessionData.user_current_account.User.id == this.huddleData.created_by)
          this.isAssessorOrCreator = true;
        else
          this.isAssessorOrCreator = false;

        this.isDataAvailable = true;
        this.detailsService.setArtifactlList(this.huddleData.sample_data);
      }

    });



  }

  public openResource(artifact) {
    if (artifact.doc_type == 5) {
      window.open(artifact.url, '_blank');
    } else {
      this.detailsService.openResource(artifact);
    }
  }
  goToDetailPage(artifact) {
    if (artifact.published == 1)
      this.router.navigate(["/video_details/home", artifact.account_folder_id, artifact.doc_id], { queryParams: { assessment: true, sampleData: true } })
  }

  getSingleAsignment() {
    this.router.navigate(['../asignment/details'])
  }

  getImgUrl(user_id, img){
    return this.assessmentService.getUserAvatar(user_id, img);
  }

  getParticipantLimit(participant_Count) {
    let result;
    if (participant_Count > 999 && participant_Count <= 999999) {
        result = Math.floor(participant_Count / 1000) + 'K';
    } else {
        result = participant_Count;
    }
    return result;
  }

  public RequestParticipentsDetails(data){
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-lg',
      initialState: {
        data: {
          participents: data
        }
      }
    };

    this.modalService.show(ParticipentsDetailModal, config);

  }

  onScroll(event) {
    let element = this.popup.nativeElement;
    let atBottom = parseInt(element.scrollHeight) - parseInt(element.scrollTop) === element.clientHeight
    if (atBottom) {
      this.page++;
      let records = Math.ceil(this.totalRecords/12) - 1; 
      if(this.callCount<records){
        this.callCount++;this.searchPage++;
        this.getWorkSpaceVideos(true);
      }
      else{
        return;
      }

    }

  }
  getId() {

    this.huddle_id = this.assessmentService.getHuddleId();


  }
  public onMediaUpload(event) {

    if (event.from && event.files.length) {
      let that = this;
      for (let file_key in event.files) {
        if (event.files.hasOwnProperty(file_key)) {
          let file = event.files[file_key];
          let obj: any = {};
  
          let sessionData: any = that.headerService.getStaticHeaderData();
          obj.user_current_account = sessionData.user_current_account;
          obj.account_folder_id = that.huddle_id;
          obj.huddle_id = that.huddle_id;
          // filestack handle key in asessors details 
          obj.fileStack_handle = file.handle;
          //filestack url added
          obj.fileStack_url = file.url;
          obj.account_id = sessionData.user_current_account.accounts.account_id;
          obj.site_id = sessionData.site_id;
          obj.user_id = sessionData.user_current_account.User.id;
          obj.current_user_role_id = sessionData.user_current_account.roles.role_id;
          obj.current_user_email = sessionData.user_current_account.User.email;
          obj.suppress_render = false;
          obj.suppress_success_email = false;
          obj.workspace = false;
          obj.activity_log_id = "";
          obj.direct_publish = event.from == "Upload";
          obj.video_file_name = file.filename;
          obj.stack_url = file.url;
          obj.video_url = file.key;
          obj.video_id = "";
          obj.video_file_size = file.size;
          obj.direct_publish = true;
          obj.video_title = file.filename.split(".")[0];
          obj.url_stack_check = 1;
          obj.sample = 1;
          if (event.from == "Resource") {
            this.detailsService.uploadResource(obj).subscribe((response: any) => {
              this.toastr.ShowToastr('info',this.translation.artifacts_new_resource_upload_successfully);
              this.getHuddleData();
              
            });
          }
          else {
            this.detailsService.uploadVideo(obj).subscribe((data: any) => {
              this.toastr.ShowToastr('info',this.translation.artifacts_new_video_uploaded_successfully);

            });
          }

        }
      }
    }
  }
  getHuddleData(){
     this.sessionData = this.headerService.getStaticHeaderData();
     let userAccount =
       this.sessionData.user_current_account;
 
     let obj = {
       huddle_id: this.huddle_id,
       user_current_account: userAccount,
       is_web:true
     }
     debugger
     this.assessmentService.getHuddleDetails(obj).subscribe((data:any) => {

       if(data){
 
         this.assessmentService.setHuddleData(data);
       }
//        if(!data.success){
//          this.toastr.ShowToastr('error',data.message)
//  this.router.navigate(['/video_huddles/list'])
//        }
 
     }, error => {
       console.error(error)
     });
   }

  public getWorkSpaceVideos(onScrollCall: boolean = false) {
    
    if (!onScrollCall) this.workspaceVideoLoading = true;
    let obj;
    if (this.getArtifactsType == "videoArtifacts") {
      obj = {
        title: this.SearchString,
        page: this.SearchString.length>0 ? this.searchPage: this.page,
        sort: '',
        doc_type: '1',
        account_id: this.sessionData.user_current_account.users_accounts.account_id,
        user_id: this.sessionData.user_current_account.users_accounts.user_id,
        with_folders: 1
      };
    } else if (this.getArtifactsType == "resourceArtifacts") {
      obj = {
        title: this.SearchString,
        page: this.SearchString.length>0 ? this.searchPage: this.page,
        sort: '',
        doc_type: '2',
        from_goals: true,
        account_id: this.sessionData.user_current_account.users_accounts.account_id,
        user_id: this.sessionData.user_current_account.users_accounts.user_id,
        with_folders: 1
      };
    }
    
    this.assessmentService.getWorkspaceVideos(obj).subscribe((data: any) => {
      if(data.total_records>0){
        this.totalRecords = data.total_records;
      }
      this.videoslist = data.data;
      this.videoList = [...this.videoList, ...this.videoslist];

      this.assessmentService.setvideoList(this.videoList)
      if(this.videoList.length>0)
        {
        this.isResponseCompleted = true;
       }
     else
        this.isResponseCompleted = false;
    }, error => {

    })
  }
  showChildModal(evidenceType): void {
    let bsModalRef;
    // this.getArtifactsType = type;
    // this.workspaceVideoLoading=true;
    // this.page=1;
    // this.videoList=[];
    // this.getWorkSpaceVideos();
    // this.childModal.show();
    // this.copyVideoNotes = false;
    // this.selectedVideo = null;
    let type = this.getEvidenceType(evidenceType)
    let submittedEvidence = []
    if (this.huddleData.sample_data) {
      this.huddleData.sample_data.forEach(evidence => {
        if(type == 2) {
          if (evidence.doc_type == 2 || evidence.doc_type == 5) {
            submittedEvidence.push(evidence.source_document_id)
          }
        } else {
          if (evidence.doc_type == type) {
            submittedEvidence.push(evidence.source_document_id)
          }
        }
      
      });
    }
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'evidance-modal-container',
      initialState: {
        evidenceType,
        item_id: 0,
        owner_id: 0,
        goal_id: 0,
        from_asessors: true,
        from_component: 'assessement',
        submitted_evidence: submittedEvidence
      }
    };
    // console.log('config.initialState: ', config.initialState);
    bsModalRef = this.modalService.show(AddEvidenceModalComponent, config)
    bsModalRef.content.onRecordedUpload.subscribe(data => {
      this.selectedVideoModal = data.selectedEvidence;
      this.copyVideoNotes = data.copyVideoNotes;
      this.selectedDocIds = data.selectedDocIds;
      this.submitForm();
      // console.log(data);
      
    });
    // this.modalService.show(EvidenceModalComponent, config)
  }

  getEvidenceType(evidenceType) {
    if (evidenceType == 'video') {
      return 1
    }
    if (evidenceType == 'resource') {
      return 2
    }
  }

  hideChildModal(): void {
      this.childModal.hide();
      this.resetItems()
  }

  OpenModel(artifact, modalName) {
   
    this.artifact = artifact;
    if (modalName === 'download')
      this.popUpModel.DownloadResource(artifact);
    else
      this.popUpModel.showModal(modalName);
  }

  toggleSelectedVide(item: any) {
    this.selectedVideo = item;
    this.videoList.forEach(video => {
      if (video.id === this.selectedVideo.id) video.selected = true;
      else video.selected = false;
    });
  }

  submitForm() {
    this.VideoForm = true
    // let selectedVideo = this.videoList.find(video => video.selected);
    // if (selectedVideo) {

      
      
      let formData = {
        document_id: this.selectedDocIds ? this.selectedDocIds : this.selectedVideoModal.doc_id,
        account_folder_id: [this.huddleData.current_huddle_info.account_folder_id],
        current_huddle_id: this.selectedVideoModal.account_folder_id,
        account_id: this.sessionData.user_current_account.accounts.account_id,
        user_id: this.sessionData.user_current_account.User.id, 
        copy_notes: this.copyVideoNotes,
        from_workspace: 1,
        sample: 1
      };

      this.detailsService.DuplicateResource(formData).subscribe((response: any) => {
        // this.hideChildModal();
        this.VideoForm = false
        if(this.getArtifactsType == "videoArtifacts") this.toastr.ShowToastr('info',this.translation.artifact_newvideouploadedsuccessfully);
        if(this.getArtifactsType == "resourceArtifacts") this.toastr.ShowToastr('info',this.translation.ah_new_resource_uploaded);

        // this.videoList.map(video => {
        //   video.selected = false;
        // });
        // this.copyVideoNotes = false;
        this.selectedVideo = null;
      }, error => {
        this.VideoForm = false

        // this.videoList.map(video => {
        //   video.selected = false;
        // });
        // this.copyVideoNotes = false;
        this.selectedVideo = null;
        // this.hideChildModal();
        this.toastr.ShowToastr('info',this.translation.sorry_for_inconvenience);
      });
    // } else {
    //   this.VideoForm = false
    //   if(this.getArtifactsType == "videoArtifacts") this.toastr.ShowToastr('info',this.translation.select_video_to_upload);
    //   if(this.getArtifactsType == "resourceArtifacts") this.toastr.ShowToastr('info',this.translation.ah_select_resource_to_upload);
    // }
  }

  publish() {
    let data = {
      account_folder_id: this.huddleData.current_huddle_info.account_folder_id,
      account_id: this.sessionData.user_current_account.accounts.account_id,
      user_current_account: this.sessionData.user_current_account
    };

    this.assessmentService.publishAssessment(data).subscribe((res: any) => {
      if (res.success) {
        res.publish_all_key == 1 ? this.publishAll = true : false 
        this.isPublished = 1;
        this.huddleData.current_huddle_info.published_date = res.published_date;
        this.toastr.ShowToastr('info',this.translation.assessment_huddle_published)
      }
    });
  }

  expandView() {
    this.is_expand = !this.is_expand;

  }


  private socketPushFunctionDiscussionDetail() {
    this.subscriptions.add(
      this.socketService.pushEventWithNewLogic(
        `huddle-details-${this.huddleData.current_huddle_info.account_folder_id}`).subscribe(data => 
        this.processEventSubscriptions(data)));
  }

  private processEventSubscriptions(data) {
    console.log("here data: ",data);
    switch (data.event) {
        case "resource_added":
          if (this.isAllowed(data.allowed_participants) || (typeof data.allowed_participants === 'undefined') || data.allowed_participants.length == 0 || data.allowed_participants == undefined) {
            let should_wait = 0;
            if (data.is_video_crop) {
              should_wait = 1;
            }
            if (data.data.assessment_sample == 1 || data.assessment_sample == 1 ) {
              this.processResourceAdded(data.data, should_wait);
            }
            else
            {
              this.huddleData.sample_data.forEach(x => {
                if(x.document_id== data.reference_id){
                  x.total_attachment++;
                }
              });
            }
          }
          break;

        case "resource_renamed":
          if (data.data.assessment_sample == 1) {
            this.processResourceRenamed(data.data, data.is_dummy);
          }
          break;

          case "comment_added":
            if (this.isAllowed(data.allowed_participants) || (typeof data.allowed_participants === 'undefined') || data.allowed_participants.length == 0 || data.allowed_participants == undefined) {
             if(data.from_cake==1){

               let odata=JSON.parse(data.data)
               this.huddleData.sample_data.forEach(x => {
                if(this.userId == odata.created_by && x.document_id == odata.ref_id ){
                  x.total_comments++
                }
                else{ 
                  if(x.document_id == odata.ref_id){
                  let obj = {
                    huddle_id: this.activeRoute.parent.snapshot.paramMap.get('id'),
                    user_id: odata.user_id,
                    video_id:x.document_id
                  }
                  this.assessmentService.getCommentCount(obj).subscribe((data:any) => {
                    if(data.success && data){
                      x.total_comments=data.data.active_comments_count;
                    }
                  });
                }
                }
              });
            }
            else{

              this.huddleData.sample_data.forEach(x => {
                if(x.created_by == data.data.created_by && x.document_id == data.video_id  ){
                  let obj = {
                    huddle_id: this.activeRoute.parent.snapshot.paramMap.get('id'),
                    user_id: data.participant_id,
                    video_id:x.document_id
                  }
                  this.assessmentService.getCommentCount(obj).subscribe((data:any) => {
                    if(data.success && data){
                      x.total_comments=data.data.active_comments_count;
                    }
                  });
                }
              });
            }
              
            }
            break;

        case "resource_deleted":
          if (data.assessment_sample == 1) {
            this.processResourceDeleted(data.data, data.deleted_by);
          }
          break;

        default:
          break;
      }

  } 

  private isAllowed(allowed_participants = []) {
    let that = this;
    let allowed = _.find(allowed_participants, function (item) {
      return parseInt(item.user_id) == parseInt(that.sessionData.user_current_account.User.id);
    });
    if (allowed) {
      return true;
    }
  }


  private processResourceAdded(resource, should_wait) {
    console.log("resource :",resource);
    console.log("should_wait :",should_wait);
    let wait_time = 0;
    if (should_wait) {
      wait_time = 10000;
      resource.published = 0;
    }
    const resourceExisted = this.huddleData.sample_data.find(r => r.id == resource.id);
    if(!resourceExisted) {
      this.huddleData.sample_data.push(resource);
      if (this.sessionData.user_current_account.User.id != resource.created_by) {
        this.toastr.ShowToastr('info',this.translation.artifacts_new_artifacts_added);
      }
    }
    


  }

  private processResourceRenamed(resource, dont_show_toast = 0) {
    let objResource = _.find(this.huddleData.sample_data, function (item) {
      return (parseInt(item.id) == parseInt(resource.id) || parseInt(item.doc_id) == parseInt(resource.doc_id));
    });
    let index = -1;
    this.huddleData.sample_data.forEach((item, i) => {
      if (parseInt(item.id) == parseInt(resource.id) || parseInt(item.doc_id) == parseInt(resource.doc_id)) {
        index = i;
      }

    });

    if (objResource) {
      if(dont_show_toast == 1 && this.activeComment==1){
        this.huddleData.sample_data[index].total_comments = resource.total_comments;
        this.huddleData.sample_data[index].total_attachment = resource.total_attachment;
      }else{
      if (dont_show_toast == 0) {
        objResource.title.slice(0, 25)
        if (this.sessionData.user_current_account.User.id != resource.updated_by) {
          if (objResource.title.length > 25) {
            this.toastr.ShowToastr('info',`'${objResource.title}'... ${this.translation.artifacts_renamed_to} '${resource.title}'...`);
          } else {
            this.toastr.ShowToastr('info',`'${objResource.title}' ${this.translation.artifacts_renamed_to} '${resource.title}'`);
          }
        }
      }
      this.huddleData.sample_data[index] = resource;
    }
    }
  }

  private processResourceDeleted(resource_id, deleted_by) {

    var indexOfMyObject = this.huddleData.sample_data.findIndex(x => {
      return x.doc_id == resource_id || x.id == resource_id;
    });
    if (indexOfMyObject > -1) {
      let obj = this.huddleData.sample_data[indexOfMyObject];
      this.huddleData.sample_data.splice(indexOfMyObject, 1);
      if (deleted_by != this.sessionData.user_current_account.User.id) {
        this.toastr.ShowToastr('info',this.translation.artifacts_new_artifacts_deleted);
      }
    }
  }


showDeleteHuddleModal(huddle_id): void {
  this.DeletableItem=huddle_id;
  this.deleteHuddleModal.show();
}

hideDeleteHuddleModal(): void {
  this.deleteHuddleModal.hide();
}

public ConfirmDelete() {
  ;
  if (this.Inputs.ConfirmationKey != this.Inputs.Confirmation) {

    this.toastr.ShowToastr('info',this.translation.huddle_you_typed_in + " '" + this.Inputs.ConfirmationKey + "' ");
    return;
  }

  let obj: any = {};
    let sessionData: any = this.headerService.getStaticHeaderData();
    obj.user_id = sessionData.user_current_account.User.id;
    obj.huddle_id = this.DeletableItem;
    this.homeService.DeleteItem(obj, false).subscribe((d: any) => {

      this.hideDeleteHuddleModal();

      if (d.success) {
        this.toastr.ShowToastr('info',d.message);
        this.router.navigate(['/video_huddles/list'])
      } else {
        this.toastr.ShowToastr('info',d.message);
      }

    });

}
public TriggerTextChange(ev) {

  if (ev.keyCode == 13) {
    this.ConfirmDelete()

  }
}



showMoveHuddleModal(): void {
  this.moveHuddleModal.show();
}

hideMoveHuddleModal(): void {
  this.moveHuddleModal.hide();
}
hideConfirmModal(): void{
  this.modalRef.hide();
}
public OnFolderClick(folder_id, tree) {

}

public OnFolderMove(folder_id) {
  let isHuddle=true;
 let hType= 3;

  if (!folder_id) return;

  this.MovableItem = {
    id: folder_id,
    isHuddle: Boolean(isHuddle),
    type: hType
  };

  let sessionData: any = this.headerService.getStaticHeaderData();

  let obj: any = {

    account_id: sessionData.user_current_account.accounts.account_id,
    user_id: sessionData.user_current_account.User.id,
    id: isHuddle ? "" : folder_id

  };

  let ref = this.homeService.GetFolderList(obj).subscribe((data) => {

    this.FoldersTreeData = this.list_to_tree(data, 'parent');
    this.showMoveHuddleModal()
    ref.unsubscribe();

  });


}
private list_to_tree(list, parentProp) {
  var map = {}, node, roots = [], i;
  for (i = 0; i < list.length; i += 1) {
    map[list[i].id] = i;
    list[i].children = [];
  }
  for (i = 0; i < list.length; i += 1) {
    node = list[i];
    node.name = node.text;
    if (node[parentProp] !== "#") {
      // if you have dangling branches check that map[node[parentProp]] exists
      if (list[map[node[parentProp]]]) {
        list[map[node[parentProp]]].children.push(node);
      }

    } else {
      roots.push(node);
    }
  }
  return roots;
}

public getMovePath(tree) {

  if (!tree || !tree.treeModel || !tree.treeModel.getActiveNode() || !tree.treeModel.getActiveNode().data) {
    return;
  }
  let head = tree.treeModel.getActiveNode();

  if (tree.treeModel.getActiveNode().id == -1) return [tree.treeModel.getActiveNode().data];

  let arr = [];


  while (head.parent != null) {

    if (head.data) {

      arr.push(head.data);

    }
    else if (head.treeModel.getActiveNode()) {

      arr.push(head.treeModel.getActiveNode().data);

    }

    head = head.parent;
  }

  if (arr.length == 0) return [tree.treeModel.getActiveNode().data];

  return arr.reverse();

}

 

public MoveItem(tree) {

  if (!tree.treeModel.getActiveNode()) {

    this.toastr.ShowToastr('info',this.translation.huddle_no_traget_folder);

  }

  if (!(tree.treeModel.getActiveNode() && this.MovableItem.id)) return;

  if (this.Loadings.MovingItem) {
    return;
  }

  let target = tree.treeModel.getActiveNode().data;

  let obj = {
    folder_id: target.id,
    huddle_id: this.MovableItem.id
  }

  if (this.MovableItem.isHuddle) {

    this.Loadings.MovingItem = true;
    this.homeService.Move(obj).subscribe((d: any) => {

      this.Loadings.MovingItem = false;
      if (d.success) {

        this.toastr.ShowToastr('info',this.translation.huddle_moved_successfully);

        this.HuddlesAndFolders.huddles = this.HuddlesAndFolders.huddles.filter((h) => {

          return h.huddle_id != this.MovableItem.id;

        });

        this.HuddlesAndFolders.total_huddles--;

        let index = _.findIndex(this.HuddlesAndFolders.folders, { folder_id: target.id });

        if (index > -1) {

          this.HuddlesAndFolders.folders[index].stats[this.MovableItem.type]++;

        }


      } else {
        this.toastr.ShowToastr('info',d.message);
      }

    });


    this.hideMoveHuddleModal();


  } else {

    this.Loadings.MovingItem = true;
    this.homeService.Move(obj).subscribe((d: any) => {
      this.Loadings.MovingItem = false;
      if (d.success) {
        this.toastr.ShowToastr('info',this.translation.huddle_folder_moved_successfully);
        this.HuddlesAndFolders.folders.forEach((f, i) => {
          if (f.folder_id == target.id) {

            f.stats[this.MovableItem.type]++;

          }
          if (f.folder_id == this.MovableItem.id) {
            this.HuddlesAndFolders.folders.splice(i, 1);
          }
        })
      } else {
        this.toastr.ShowToastr('info',d.message);
      }

      this.PipeTrigger = !this.PipeTrigger;

    });



    this.hideMoveHuddleModal();

  }


}
ignoreDefaultBehavior(e) {
  e.preventDefault();
}
public resetItems(){
  this.videoList=[];
  this.OnSearchChange('');
  this.page=1;
}

}

