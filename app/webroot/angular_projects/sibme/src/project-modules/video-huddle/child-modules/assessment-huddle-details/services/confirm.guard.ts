import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AsignmentDetailSingleAsseseeComponent } from '../asignment-detail-single-assesee/asignment-detail-single-assesee.component';

@Injectable({ providedIn: 'root' })
export class ConfirmGuard implements CanDeactivate<AsignmentDetailSingleAsseseeComponent> {

  canDeactivate(
    component?: AsignmentDetailSingleAsseseeComponent,
  ): Observable<boolean> | boolean {
    if (component.showDialogBox && component.assessment_custom_fields.length > 0) {

      if (confirm(component.translation.you_have_unsaved_changes)) {
        return true;
      } else {
        return false;
      }
    } else return true;

  }
}