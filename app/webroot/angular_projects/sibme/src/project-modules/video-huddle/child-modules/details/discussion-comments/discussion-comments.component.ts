import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from "@angular/core";
import { DiscussionService } from "../servic/discussion.service";
import IUpload from "../discussions/IUpload";
import { HeaderService } from "@app/services";
import { ShowToasterService } from '@projectModules/app/services';
import { DetailsHttpService } from "../servic/details-http.service";
import { PermissionService } from '../servic/permission.service';
import * as _ from "underscore";
import { DomSanitizer } from '@angular/platform-browser'
import { Subscription } from 'rxjs';
import { environment } from "@environments/environment";
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import * as moment from 'moment';
import { AppMainService } from '@app/services';

@Component({
  selector: "discussion-comment",
  templateUrl: "./discussion-comments.component.html",
  styleUrls: ["./discussion-comments.component.css"]
})
export class DiscussionCommentsComponent implements OnInit, OnDestroy, IUpload {
  public reply = false;
  public edit_discussion = false;
  public more_mode;
  public mode_flag;
  @Input() comment;
  @Input() params;
  @Input() users;
  @Input() comment_status;
  @Input() user_huddle_role;
  @Output() delete_emitter = new EventEmitter<any>();
  @Output() state_emitter = new EventEmitter<any>();
  @Output() comment_emitter = new EventEmitter<any>();
  @Output() edit_comment_emitter = new EventEmitter<any>();
  @Output() edit_comment_after_save_emitter_1 = new EventEmitter(true);
  public editor_flag;
  public Files: any = [];
  public stockFiles: any = [];
  public editableComment: any = {};
  public editablereply: any;
  public comment_id;
  public comment_details: any = {};
  public emailAll: boolean = false;
  public comment_complete = false;
  public comment_less = true;
  public reply_complete = false;
  public reply_less = true;
  public edit_btn = false;
  public reply_btn = false;
  public delete_btn = false;
  public header_data;
  public translation: any = {};
  private subscription: Subscription;
  staticImageServiceIp = "https://s3.amazonaws.com/sibme.com/static/users/";
  currentreplyid: any;
  public userAccountLevelRoleId: number | string = null;
  public negativeId = -1;
  public editNegativeId = -1;
  public editReplyNegativeId = -1;
  private disscussion_add_replies = [];
  private disscussion_edit_replies = [];
  private discussionEditCommentsTAArray = [];
  private DISCUSSION_LS_KEYS = GLOBAL_CONSTANTS.LOCAL_STORAGE.DISCUSSION;
  private discussionEditCommentLSKey: string = '';
  private discussionEditCommentTAKey: string = '';
  public user_id;
  thumb_image: any;
  thumb_image_Url: any;
  public isRequestCompleted;
  public selectedId;
  public uniqueCommentsArray;

  constructor(
    public appMainService:AppMainService,
    private toastr: ShowToasterService,
    private discussionService: DiscussionService,
    private headerService: HeaderService,
    private detailService: DetailsHttpService,
    public permissionService: PermissionService,
    private domSanitizer: DomSanitizer) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;

    });
    this.discussionService.responseSubject.subscribe(data => {
      this.isRequestCompleted = data;
    });
    this.discussionService.selectedId$.subscribe( data => {
      this.selectedId = data;
     });
  }
  ngOnInit() {
    this.more_mode = {};
    this.more_mode = {
      default: 0,
      edit: 1,
      reply: 2,
      replyedit: 3
      
    };
    this.header_data = this.headerService.getStaticHeaderData();
    this.userAccountLevelRoleId = this.header_data.user_permissions.roles.role_id;

    let tmpReplies = localStorage.getItem(`disscussion_add_replies${this.params.discussion_id}`);
    if(tmpReplies && tmpReplies != 'undefined' && tmpReplies != 'null'){
      this.disscussion_add_replies = JSON.parse(tmpReplies);
      this.disscussion_add_replies.forEach(reply => {
       if(reply.commentable_id == this.comment.id) this.comment.replies.unshift(reply);
      });
    }
  
  
    let tmpEditReplies = localStorage.getItem(`disscussion_edit_replies${this.params.discussion_id}`);
    if(tmpEditReplies && tmpEditReplies != 'undefined' && tmpEditReplies != 'null'){
      this.disscussion_edit_replies = JSON.parse(tmpEditReplies);
      this.disscussion_edit_replies.forEach(tmpReply => {
        let index = this.comment.replies.findIndex(x => x.id == tmpReply.comment_id);

        if(index > -1) this.comment.replies[index] = tmpReply;

      });
    }
    this.user_id = this.headerService.getUserId()
    this.discussionEditCommentTAKey = `${this.DISCUSSION_LS_KEYS.EDIT_COMMENT_TA}${this.params.discussion_id}_${this.headerService.getUserId()}`;

    /** Restore discussion edit comments try again array start */
    let tmpEditComments = this.headerService.getLocalStorage(this.discussionEditCommentTAKey);
    if(Array.isArray(tmpEditComments)){
      this.discussionEditCommentsTAArray = tmpEditComments
    }
    /** Restore discussion edit comments try again array start */
    this.headerService.currentUserImage$.subscribe(data=>{
      if(!_.isEmpty(data)){
      this.thumb_image_Url=data;
      this.thumb_image=data.replace(/^.*[\\\/]/, '');
      }
          })
  }
  onUpload(e) {
    let target = e.target;
    target.files.length > 0 && this.pushToFiles(target.files);
    target.value = "";
  }
  ImageBuilder(participent) {
    let image = participent.image || "assets/video-huddle/img/c1.png";
    let url = `${this.staticImageServiceIp}${participent.user_id}/${
      participent.image
      }`;
    return participent.image ? url : image;
  }
  public pushToFiles(files: any, is_reply = false): any {
    for (let i = 0; i < files.length; i++) {
      this.Files.push(this.discussionService.parseFile(files[i]));
    }
  }
  indexedValue(i: any){
  }

  public ActivateMode(mode, reply?: any) {
    this.mode_flag = mode;
    this.editor_state(false);
    this.editablereply = '';
    if(mode == this.more_mode.childReply){
      this.currentreplyid = reply.id
    }
    if (mode == this.more_mode.edit) {
      this.Files = [];
      this.stockFiles = JSON.parse(JSON.stringify(this.comment.attachments));
      this.editableComment = JSON.parse(JSON.stringify(this.comment));

      let tmpEditComment = this.headerService.getLocalStoragewithoutParse('discussionEditCommentLSKey_'+this.editableComment.id+this.user_id);
      if(tmpEditComment) this.editableComment.comment = tmpEditComment;
    } else if (mode == this.more_mode.default) {
      this.editor_state(true);
      this.stockFiles = [];
      this.Files = [];
      this.emailAll = false;
    } else if (mode == this.more_mode.replyedit) {
      this.currentreplyid = reply.id
      this.Files = [];
      this.stockFiles = JSON.parse(JSON.stringify(reply.attachments));
      this.editablereply = JSON.parse(JSON.stringify(reply.comment));
        if(localStorage.getItem(reply.id)!=null && localStorage.getItem(reply.id)!=''){
          this.editablereply = localStorage.getItem(reply.id);
        }
    }
  }

  public DownloadFile(attachment) {
    this.detailService.DownloadDiscussionFile(attachment.document_id);
  }
  public AddReply() {
    if (!this.editablereply || this.editablereply.trim() == "" || this.editablereply.toString().replace(/<.*?>/g, "") == "" || this.editablereply.toString().replace(/<.*?>/g, "").trim() == "") {
      this.toastr.ShowToastr('info',this.translation.discussion_enter_some_text);
      return;
    } else {
      if (this.Files.length) {
        let result = this.headerService.checkFileSizeLimit(this.Files);
        if (!result.status) {
          this.toastr.ShowToastr('error',result.message);
          return;
        }
      }
      let sessionData: any = this.headerService.getStaticHeaderData();
      let comment_time = moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS")
      let obj: any = {
        fake_id:moment(new Date()).format('x'),
        huddle_id: this.params.huddle_id,
        send_email: true,
        comment: this.editablereply,
        comment_id: this.editableComment.id,
        commentable_id: this.comment.id,
        discussion_id: this.comment.parent_comment_id,
        user_current_account: sessionData.user_current_account,
        remove_attachments: this.stockFiles
          .filter(f => f.isDeleted)
          .map(f => f.document_id)
          .join(","),
        title: "",
        notification_user_ids: this.users.map(u => u.id).join(","),
        created_at_gmt: comment_time

      };
      ({
        User: { id: obj.user_id },
        accounts: { account_id: obj.account_id }
      } = sessionData.user_current_account);
      let fd = this.discussionService.ToFormData(obj);
      this.Files.length > 0 &&
        (() => {
          for (let i = 0; i < this.Files.length; i++) {
            fd.append("attachment[]", this.Files[i], this.Files[i].name);
          }
        })();

        localStorage.removeItem('comment_'+this.comment.id);
        this.mode_flag = this.more_mode.default;
        this.Files = [];
        this.emailAll = false;

      this.comment.replies.unshift(this.createDummySubReply(obj,sessionData));
      this.comment.replies=[...this.comment.replies];
      this.appMainService.AddDiscussion(fd).subscribe((data : any) => {
        if(data.success){
        }
        
      },(error)=>{
        obj.unsavedreply_Id = this.negativeId;
        --this.negativeId;
        obj.first_name = sessionData.user_current_account.User.first_name;
        obj.last_name = sessionData.user_current_account.User.last_name;
        obj.tryAgain = true;
        obj.uuid = new Date().getTime();
        obj.thumb_image = `${environment.baseUrl}/img/home/photo-default.png`;

        this.disscussion_add_replies.push(obj);
        localStorage.setItem(`disscussion_add_replies${this.params.discussion_id}`, JSON.stringify(this.disscussion_add_replies));
          this.comment.replies = _.without(this.comment.replies, _.findWhere(this.comment.replies, {
              fake_id: obj.fake_id
          }));
        this.comment.replies.unshift(obj);
      });
    }
  }

  public createDummySubReply(reply,sessionData)
  {
    let obj={
      id:0,
      fake_id:reply.fake_id,
      parent_comment_id:reply.comment_id,
      title: reply.title,
      comment:reply.comment,
      first_name: sessionData.user_current_account.User.first_name,
  last_name: sessionData.user_current_account.User.last_name,
  image: this.thumb_image,
      user_id:sessionData.user_current_account.User.id,
      thumb_image: this.thumb_image_Url
    }
    
    return obj;
  }
  public onUpdate(commentObj) {
    this.headerService.removeLocalStorage('discussionEditCommentLSKey_'+this.editableComment.id+this.user_id);
    if (!this.editableComment.comment || this.editableComment.comment.trim() == "" || this.editableComment.comment.toString().replace(/<.*?>/g, "") == "" || this.editableComment.comment.toString().replace(/<.*?>/g, "").trim() == "") {
      this.toastr.ShowToastr('info',this.translation.discussion_enter_some_text);
      return;
    } else {
      if (this.Files.length) {
        let result = this.headerService.checkFileSizeLimit(this.Files);
        if (!result.status) {
          this.toastr.ShowToastr('error',result.message);
          return;
        }
      }
      
      let sessionData: any = this.headerService.getStaticHeaderData();
      let comment_time = moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS")

      let obj: any = {
        huddle_id: this.params.huddle_id,
        send_email: true,
        comment_id: this.editableComment.id,
        commentable_id: this.params.discussion_id,
        discussion_id: this.params.discussion_id,
        user_current_account: sessionData.user_current_account,
        remove_attachments: this.stockFiles
          .filter(f => f.isDeleted)
          .map(f => f.document_id)
          .join(","),
        title: "",
        notification_user_ids: this.emailAll
          ? this.users.map(u => u.id).join(",")
          : "",
          created_at_gmt: comment_time
      };

      ({ comment: obj.comment } = this.editableComment);

      ({
        User: { id: obj.user_id },
        accounts: { account_id: obj.account_id }
      } = sessionData.user_current_account);

      let fd = this.discussionService.ToFormData(obj);
      this.Files.length > 0 &&
        (() => {
          for (let i = 0; i < this.Files.length; i++) {
            fd.append("attachment[]", this.Files[i], this.Files[i].name);
          }
        })();
        this.discussionService.responseSubject.next(true);
        this.ActivateMode(this.more_mode.default);
      this.appMainService.EditDiscussion(fd).subscribe((data : any) => {
        if(data.success){
          this.discussionService.responseSubject.next(false);
       
          
        }

        this.mode_flag = this.more_mode.default;
        this.Files = [];
        this.emailAll = false;
      },
      (error)=>{
        this.discussionService.responseSubject.next(false);
        obj.unsavedEditedComment = this.editNegativeId;
        obj.first_name = sessionData.user_current_account.User.first_name;
        obj.last_name = sessionData.user_current_account.User.last_name;
        obj.id = this.comment.id;
        obj.thumb_image = `${environment.baseUrl}/img/home/photo-default.png`;
        obj.tryAgain = true;
        obj.editTryAgain = true;
        obj.uuid = new Date().getTime();
        obj.replies=commentObj.replies;
        let data =this.headerService.getLocalStorage(this.discussionEditCommentTAKey);
        if(data)
        {
          this.discussionEditCommentsTAArray=data;
          this.discussionEditCommentsTAArray.push(obj);
        }
        else{

          this.discussionEditCommentsTAArray.push(obj);
        }
         this.uniqueCommentsArray = new Set(this.discussionEditCommentsTAArray);
        this.headerService.setLocalStorage(this.discussionEditCommentTAKey, this.uniqueCommentsArray);
        this.edit_comment_emitter.emit(obj);
        

      }
      );
    }
  }
  public onReplyUpdate(reply) {
    if (!this.editablereply || this.editablereply.trim() == "" || this.editablereply.toString().replace(/<.*?>/g, "") == "" || this.editablereply.toString().replace(/<.*?>/g, "").trim() == "") {
      this.toastr.ShowToastr('info',this.translation.discussion_enter_some_text);
    } else {
      if (this.Files.length) {
        let result = this.headerService.checkFileSizeLimit(this.Files);
        if (!result.status) {
          this.toastr.ShowToastr('error',result.message);
          return;
        }
      }
      
      let sessionData: any = this.headerService.getStaticHeaderData();
      let comment_time = moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS")

      let obj: any = {
        huddle_id: this.params.huddle_id,
        send_email: true,
        comment: this.editablereply,
        comment_id: reply.id,
        commentable_id: this.comment.id,
        discussion_id: this.params.discussion_id,
        user_current_account: sessionData.user_current_account,
        remove_attachments: this.stockFiles
          .filter(f => f.isDeleted)
          .map(f => f.document_id)
          .join(","),
        title: "",
        notification_user_ids: this.emailAll
          ? this.users.map(u => u.id).join(",")
          : "",
          created_at_gmt: comment_time
      };

      ({
        User: { id: obj.user_id },
        accounts: { account_id: obj.account_id }
      } = sessionData.user_current_account);
      let fd = this.discussionService.ToFormData(obj);
      this.Files.length > 0 &&
        (() => {
          for (let i = 0; i < this.Files.length; i++) {
            fd.append("attachment[]", this.Files[i], this.Files[i].name);
          }
        })();
        this.discussionService.responseSubject.next(true);
        this.ActivateMode(this.more_mode.default);
      this.appMainService.EditDiscussion(fd).subscribe((data: any) => {
        if (data.success) {

          this.discussionService.responseSubject.next(false);
          this.click_cancle_reply();
          this.click_cancl_edit_reply();
        }

        this.mode_flag = this.more_mode.default;
        this.Files = [];
        this.emailAll = false;
      },
      (error)=>{
        obj.editReplyNegativeId = this.editReplyNegativeId;
        --this.editReplyNegativeId;
        obj.first_name = sessionData.user_current_account.User.first_name;
        obj.last_name = sessionData.user_current_account.User.last_name;
        obj.tryAgain = true;
        obj.editTryAgain = true;
        obj.uuid = new Date().getTime();
        obj.thumb_image = `${environment.baseUrl}/img/home/photo-default.png`;
        var indexofcomment =  this.comment.replies.findIndex(x => x.id == reply.id);
        obj.id = this.comment.replies[indexofcomment].id
        this.comment.replies[indexofcomment] = obj;
        this.ActivateMode(this.more_mode.default);
        this.click_cancl_edit_reply();

        this.disscussion_edit_replies.push(obj);
        localStorage.setItem(`disscussion_edit_replies${this.params.discussion_id}`, JSON.stringify(this.disscussion_edit_replies));
        this.discussionService.responseSubject.next(false);

        
      }
      );
    }
  }
  public ShowReply() {
    window.scrollTo(0, document.body.scrollHeight - 10);
    this.mode_flag = this.more_mode.default;
    this.Files = [];
    this.emailAll = false;

  }

  public deleteComment(comment) {
    // this.comment_id = this.comment.id;

    this.delete_emitter.emit(comment);
  }
  public deletereply(reply) {
    console.log(reply)
    this.delete_emitter.emit(reply);
  }

  public editor_state(flag) {
    this.editor_flag = flag;
    localStorage.setItem('flag', flag)
    this.state_emitter.emit(this.editor_flag);
  }

  public commentMoreShow() {
    this.comment_complete = true;
    this.comment_less = false;
  }
  public commentLessShow() {
    this.comment_less = true;
    this.comment_complete = false;
    window.scrollBy(0, -(this.findSiblingWithId(document.getElementById("show_less_" + this.comment.id), this.comment.id).offsetHeight - 100))
  }

  public replyMoreShow(id?: string) {
    this.currentreplyid = id
    this.reply_complete = true;
    this.reply_less = false;
  }
  public replyLessShow(id?: string) {
    this.reply_less = true;
    this.reply_complete = false;
    window.scrollBy(0, -(this.findSiblingWithId(document.getElementById("show_less_" + id), id).offsetHeight - 100))
  }
  public findSiblingWithId(element, id) {
    let siblings = element.parentNode.children,
      sibWithId = null;
    for (var i = siblings.length; i--;) {
      if (siblings[i].id == "comment_content_" + id) {
        sibWithId = siblings[i];
        break;
      }
    }


    return sibWithId;

  }
  findLength() {
    let comnt = this.comment.comment.replace(/<.*?>/g, "");
    return comnt.length;
  }

  click_edit() {
    this.edit_btn = true;
    this.reply_btn = true;
    this.delete_btn = true;
  }
  click_cancle_edit() {
    this.edit_btn = false;
    this.reply_btn = false;
    this.delete_btn = false;
  }

  click_edit_reply() {
    this.edit_btn = true;
    this.reply_btn = true;
    this.delete_btn = true;
  }
  click_cancl_edit_reply() {
    this.edit_btn = false;
    this.reply_btn = false;
    this.delete_btn = false;
  }
  click_reply(){
    this.editablereply = localStorage.getItem('comment_'+this.comment.id);
    this.edit_btn=true;
    this.delete_btn = true;
  }
  click_cancle_reply() {
    this.edit_btn = false;
    this.reply_btn = false;
    this.delete_btn = false;
  }

  saveCommentAgainEmit(comment){
    if(comment.processing) return;
    comment.processing = true;
    comment.fake_id=moment(new Date()).format('x');
    this.comment_emitter.emit(comment)
  }

  saveReplyAgain(reply){
    
    if(reply.processing) return;
    reply.processing = true;
    reply.fake_id=moment(new Date()).format('x');
    let fd = this.discussionService.ToFormData(reply);
      this.Files.length > 0 &&
        (() => {
          for (let i = 0; i < this.Files.length; i++) {
            fd.append("attachment[]", this.Files[i], this.Files[i].name);
          }
        })();


        

      this.discussionService.AddDiscussion(fd).subscribe((data : any) => {
        this.comment.replies = this.comment.replies.filter(x => x.uuid != reply.uuid);
        this.comment.replies = [...this.comment.replies];
        this.disscussion_add_replies = this.disscussion_add_replies.filter(x => x.uuid != reply.uuid);
        localStorage.setItem(`disscussion_add_replies${this.params.discussion_id}`, JSON.stringify(this.disscussion_add_replies));
        
        if(data.success){
          localStorage.removeItem('comment_'+this.comment.id);
          this.toastr.ShowToastr('info',this.translation.discussion_reply_added);
        }
        
        this.mode_flag = this.more_mode.default;
        this.Files = [];
        this.emailAll = false;
        let replyStorage = `disscussion_add_replies${this.params.discussion_id}`
        if(data.code == 409){
          const taComments = this.headerService.getLocalStorage(replyStorage);
          if (Array.isArray(taComments)) {
            const commentIndex = taComments.findIndex(tsComment => tsComment.created_at_gmt === data.data.created_at_gmt);
            if (commentIndex > -1) {
              taComments.splice(commentIndex, 1);
              this.headerService.setLocalStorage(replyStorage, taComments);
            }
          }
        }
        let indexToDelete = this.comment.replies.findIndex(x => x.created_at_gmt == data.data.created_at_gmt && x.tryAgain == true);
  
          if(indexToDelete>=0){
            this.comment.replies.splice(indexToDelete,1);
            this.comment.replies.replys =[...this.comment.replies];
            
          }
        let checkIfAdded = this.comment.replies.findIndex(x => x.created_at_gmt == data.data.created_at_gmt);
        if(checkIfAdded<0){
          this.comment.replies.push(data.data)
        }
      }, err => reply.processing = false);
    
  }

  editCommentSaveAgain(comment){
    comment.editTryAgain=false;
    comment.tryAgain=true;
    this.selectedId=comment.id;
    if(comment.processing) return;
    comment.processing = true;

    var temp = 0;
    let fd = this.discussionService.ToFormData(comment);
      this.Files.length > 0 &&
        (() => {
          for (let i = 0; i < this.Files.length; i++) {
            fd.append("attachment[]", this.Files[i], this.Files[i].name);
          }
        })();
        this.discussionService.responseSubject.next(true);
        this.appMainService.EditDiscussion(fd).subscribe((data: any)=>{
          this.discussionService.responseSubject.next(false);
        this.discussionEditCommentsTAArray = this.discussionEditCommentsTAArray.filter(x => x.uuid != comment.uuid);
        this.headerService.setLocalStorage(this.discussionEditCommentTAKey, this.discussionEditCommentsTAArray);
          
        }, err => {
          setTimeout(() => {
            comment.processing = false;
            comment.editTryAgain=true;
            this.discussionService.responseSubject.next(false);
          }, 500);
        });
        
        
  }

  editReplyAgain(reply){
    if(reply.processing) return;
    reply.processing = true;
    reply.editTryAgain= false;
    reply.tryAgain= true;
    let fd = this.discussionService.ToFormData(reply);
      this.Files.length > 0 &&
        (() => {
          for (let i = 0; i < this.Files.length; i++) {
            fd.append("attachment[]", this.Files[i], this.Files[i].name);
          }
        })();
        this.discussionService.responseSubject.next(true);
      this.appMainService.EditDiscussion(fd).subscribe((data : any) => {
        this.discussionService.responseSubject.next(false);
        this.comment.replies = this.comment.replies.filter(x => x.uuid != reply.uuid);
        this.comment.replies = [...this.comment.replies];
        this.disscussion_edit_replies = this.disscussion_edit_replies.filter(x => x.uuid != reply.uuid);
        localStorage.setItem(`disscussion_edit_replies${this.params.discussion_id}`, JSON.stringify(this.disscussion_edit_replies));

        if(data.success){
          this.toastr.ShowToastr('info',this.translation.discussion_reply_updated);
        }
        
        this.mode_flag = this.more_mode.default;
        this.Files = [];
        this.emailAll = false;

        let replyStorage = `disscussion_edit_replies${this.params.discussion_id}`
        if(data.code == 409){
          const taComments = this.headerService.getLocalStorage(replyStorage);
          if (Array.isArray(taComments)) {
            const commentIndex = taComments.findIndex(tsComment => tsComment.created_at_gmt === data.data.created_at_gmt);
            if (commentIndex > -1) {
              taComments.splice(commentIndex, 1);
              this.headerService.setLocalStorage(replyStorage, taComments);
            }
          }
        }
        let indexToDelete = this.comment.replies.findIndex(x => x.created_at_gmt == data.data.created_at_gmt && x.tryAgain == true);
  
          if(indexToDelete>=0){
            this.comment.replies.splice(indexToDelete,1);
            this.comment.replies.replys =[...this.comment.replies];
            
          }
        let checkIfAdded = this.comment.replies.findIndex(x => x.created_at_gmt == data.data.created_at_gmt);
        if(checkIfAdded<0){
          this.comment.replies.push(data.data)
        }

      }, err => {
        setTimeout(() => {
          this.discussionService.responseSubject.next(false);
          reply.processing = false;
          reply.editTryAgain = true
          reply.tryAgain= true;
        }, 500);
      });

  }

  public selectedComment (obj){
    this.discussionService.selectedID(obj.id);
  }
  
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }
}
