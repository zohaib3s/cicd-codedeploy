import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { AsignmentDetailSingleAsseseeComponent } from '../asignment-detail-single-assesee/asignment-detail-single-assesee.component';

@Injectable({ providedIn: 'root' })
export class CustomCanDeactivateGuard implements CanDeactivate<AsignmentDetailSingleAsseseeComponent> {

  canDeactivate(
    component?: AsignmentDetailSingleAsseseeComponent,
  ): Observable<boolean> | boolean {
    if (component.isTextChanged) {
      if (confirm(component.translation.you_have_unsaved_changes)) {
        return true;
      } else {
        return false;
      }
    }

    return true;
  }
}