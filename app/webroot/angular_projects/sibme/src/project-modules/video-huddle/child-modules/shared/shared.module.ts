import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { CustomDatePipe } from './pipes/customdate.pipe';

@NgModule({
  declarations: [SafeHtmlPipe, CustomDatePipe],
  exports:[SafeHtmlPipe, CustomDatePipe],
  imports: [
    CommonModule,
  ],
  providers: []
})
export class SharedModule { }
