import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { HomeService } from "../services/home.service";
import { HeaderService } from "@projectModules/app/services";
import { Subscription } from 'rxjs';

@Component({
  selector: 'huddle-thumb-participants',
  templateUrl: './huddle-thumb-participants.component.html',
  styleUrls: ['./huddle-thumb-participants.component.css']
})

export class HuddleThumbParticipantsComponent implements OnInit, OnDestroy {
  public header_data;
  public translation: any = {};
  private subscription: Subscription;
	@Input() data;
	@Output() RequestDetails: EventEmitter<any> = new EventEmitter<any>();
	@Output() OnDetailsClick: EventEmitter<any> = new EventEmitter<any>();
  constructor(private homeService:HomeService,private headerService:HeaderService) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
			this.translation = languageTranslation;
		});
   }

  ngOnInit() {
    this.header_data = this.headerService.getStaticHeaderData();
  }

    public getImgSrc(user){

    return this.homeService.getAvatarPath(user);

  }

  getParticipantLimit(participant_Count) {
    let result;
    if (participant_Count > 999 && participant_Count <= 999999) {
        result = Math.floor(participant_Count / 1000) + 'K';
    } else {
        result = participant_Count;
    }
    return result;
  }
  public RequestParticipentsDetails(e){
    this.OnDetailsClick.emit(true);

  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
