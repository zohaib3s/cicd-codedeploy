import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { DetailsRoutingModule } from "./details-routing.module";
import { HomeComponent } from "./home/home.component";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ModalModule } from "ngx-bootstrap/modal";
import { TabsModule } from "ngx-bootstrap/tabs";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { RangeSliderModule } from "ngx-rangeslider-component";
import { QuillModule } from "ngx-quill";
import { NgSlimScrollModule, SLIMSCROLL_DEFAULTS } from 'ngx-slimscroll';
import { HttpClientModule } from "@angular/common/http";
import { TiltleComponent } from "./tiltle/tiltle.component";
import { DiscussionsComponent } from "./discussions/discussions.component";

import { FormsModule } from "@angular/forms";
import { SharedPopupModelsComponent } from './shared-popup-models/shared-popup-models.component';
import { ToastrModule } from "ngx-toastr";
import { DiscussionLoadingComponent } from "./discussion-loading/discussion-loading.component";

import { FSUploaderDirective } from "@videoHuddle/directives";
import { DiscussionDetailsComponent } from "./discussion-details/discussion-details.component";
import { DiscussionListComponent } from "./discussion-list/discussion-list.component";
import { DiscussionCommentsComponent } from "./discussion-comments/discussion-comments.component";
import { SingleDiscussionComponent } from "./single-discussion/single-discussion.component";
import { SearchPipe } from "./pipes/search.pipe";
import {OrderModule} from "ngx-order-pipe";
import { NoDiscussionComponent } from './no-discussion/no-discussion.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { DiscussionSenitizePipe } from './discussion-senitize.pipe';
import { DiscussionSenitizationDirective } from './discussion-senitization.directive';
import { HtmlSenitizationPipe } from './pipes/html-senitization.pipe';
import { SharedModule } from "@videoHuddle/child-modules/shared/shared.module";
import { TreeModule } from 'angular-tree-component';
import { ConferencingComponent } from './conferencing/conferencing.component';
import { AppLevelSharedModule } from '@src/project-modules/shared/shared.module';
import { HuddleHeaderComponent } from "./huddle-header/huddle-header.component";
import { DndModule } from "ngx-drag-drop";
@NgModule({
  declarations: [
    HomeComponent,
    TiltleComponent,
    DiscussionsComponent,
    SharedPopupModelsComponent,
    DiscussionLoadingComponent,
    FSUploaderDirective,
    DiscussionDetailsComponent,
    DiscussionListComponent,
    DiscussionCommentsComponent,
    SingleDiscussionComponent,
    ConferencingComponent,
    SearchPipe,
    NoDiscussionComponent,
    DiscussionSenitizePipe,
    DiscussionSenitizationDirective,
    HtmlSenitizationPipe,
    HuddleHeaderComponent
  ],
  providers: [
    {
      provide: SLIMSCROLL_DEFAULTS,
      useValue: {
        alwaysVisible : false
      }
    },
  ],
  imports: [
    CommonModule,
    DetailsRoutingModule,
    HttpClientModule,
    TabsModule.forRoot(),
    BsDropdownModule.forRoot(),
    FormsModule,
    SharedModule,
    AppLevelSharedModule,
    ModalModule.forRoot(),
    ToastrModule.forRoot(),
    TooltipModule.forRoot(),
    RangeSliderModule,
    QuillModule.forRoot({
      modules: {
        toolbar: [
          [{ font: [] }],
          ["bold", "italic", "underline", { color: [] }],
          ["link", "image", "video"],
          [{ list: "ordered" }, { list: "bullet" }]
        ]
      }
    }),
    OrderModule,
    Ng2SearchPipeModule,
    NgSlimScrollModule,
    TreeModule.forRoot(),
    DndModule
  ],
  exports:[SharedPopupModelsComponent, FSUploaderDirective]
})
export class DetailsModule {}
