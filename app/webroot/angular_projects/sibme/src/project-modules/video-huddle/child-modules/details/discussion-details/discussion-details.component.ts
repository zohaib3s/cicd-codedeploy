import {  Component,  OnInit,  OnDestroy,  ViewChild,  TemplateRef,  EventEmitter, HostListener} from "@angular/core";
import { DiscussionService } from "../servic/discussion.service";
import { ActivatedRoute, Router } from "@angular/router";
import { DetailsHttpService } from "../servic/details-http.service";
import { HeaderService, SocketService, HomeService, AppMainService } from '@app/services';
import IUpload from "../discussions/IUpload";
import { BsModalService } from "ngx-bootstrap/modal";
import { ShowToasterService } from '@projectModules/app/services';
import * as _ from "underscore";
import { PermissionService } from '../servic/permission.service';
import { Subscription, combineLatest } from 'rxjs';
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import * as moment from 'moment';

@Component({
  selector: "app-discussion-details",
  templateUrl: "./discussion-details.component.html",
  styleUrls: ["./discussion-details.component.css"]
})
export class DiscussionDetailsComponent implements OnInit, OnDestroy, IUpload {
  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if(this.editDiscData.title && this.editDiscData.comment){
      if(this.titleEdited || this.commentEdited) {
          $event.returnValue =false;
          this.headerService.removeLocalStorage(this.editDiscussionLSKey);
      }
    }
  }
  @ViewChild("email_discussion", {static: false}) email_discussion;
  @ViewChild("deleteTemplate", {static: false}) deleteTemplate; 
  @ViewChild("deleteMainPost", {static: false}) deleteMainPost;
  public huddleHeaderData: any;
  public commentStatus;
  public ModalRefs: Modals = {};
  public params;
  public DiscussionDetails;
  public newComment: any = {};
  public isLoading: boolean = false;
  public selectedSort: number = 1;
  public filterOptions: any = {
    newest: 1,
    oldest: 2
  };
  public SearchString: any = "";
  public Files: any = [];
  public editDiscData: any = {};
  public stockFiles: any = {};
  @ViewChild("editDiscussion", {static: false}) editDiscussion;
  public dirty:boolean = false;

  public attachmentName: any;
  public hideModal: any;
  public setFile: any;
  public searchInput;
  public discussion_title;
  public user_email;
  public email_message =
      "Attached is a Word file export from the {{title}} Discussion.";
  public email_attachment_name;
  private isReplyMode = false;
  public replyFiles: any = [];
  public commentID;
  public confirmDeleteString;
  public emailAll:boolean = true;
  public sessionData:any = [];
  public breadcrumb_added_count:any = 0;
  public editorShow=true;
  public counter=0;
  dEmit: any = new EventEmitter<any>();
  public comment_flag=0;
  public Inputs: GeneralInputs;
  public showConfirmationDialog: boolean = false;
  public socket_listener:any;
  public header_data;
  public translation: any = {};
  public minus_Id = -1;
  private discussionAddCommentsTAArray = [];
  private subscription: Subscription;
  public userAccountLevelRoleId: number | string = null;
  private DISCUSSION_LS_KEYS = GLOBAL_CONSTANTS.LOCAL_STORAGE.DISCUSSION;
  private editDiscussionLSKey: string = '';
  private editDiscussionTAKey: string = '';
  private discussionAddCommentLSKey: string = '';
  private discussionAddCommentTAKey: string = '';
  private discussionEditCommentTAKey: string = '';
  private subscriptions: Subscription = new Subscription();
  thumb_image: any;
  thumb_image_Url: any;
  public isRequestCompleted;
  public selectedId;
  public editTitleLength;
  public editDescriptionLength;
  public user_huddle_role;
  public goalEvidence: boolean = false;
  public commentEdited=false;
  public titleEdited=false;
  public actDelete=false;
  public disc_detail;
  /*
    build props end
  */

  constructor(
    public appMainService:AppMainService,
    public discussionService: DiscussionService,
    private route: ActivatedRoute,
    private detailService: DetailsHttpService,
    public headerService: HeaderService,
    private modalService: BsModalService,
    public fullRouter: Router,
    private toastr: ShowToasterService,
    private homeService: HomeService,
    private socketService:SocketService,
    public permisionService: PermissionService,
    private router: Router) {
      this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
        this.translation = languageTranslation;
        this.Inputs = { NewFolderName: "", Confirmation: "", ConfirmationKey: this.translation.Huddle_confirmation_bit };
        this.email_message = this.translation.discussion_email_message_text;
      });
      this.subscriptions.add(this.detailService.huddleHeaderData$.subscribe(data => {
        this.huddleHeaderData = data;
        console.log('Datain detail', data)
        
      }));
      this.discussionService.responseSubject.subscribe(data => {
        this.isRequestCompleted = data;
      });
      this.discussionService.selectedId$.subscribe( data => {
        this.selectedId = data;
       });
    }
  editorOptions: any = {};
  more_mode;
  flag;
  ngOnInit() {
    this.sessionData = this.headerService.getStaticHeaderData();
    this.header_data = this.headerService.getStaticHeaderData();
    this.userAccountLevelRoleId = this.header_data.user_permissions.roles.role_id;
    this.discussionService.exportDiscussionEmitter.subscribe(() => {
      this.exportDiscussion();
    });

    combineLatest( this.route.params, this.route.queryParams, (params: any, queryParams: any) => {
      return { params, queryParams }}).subscribe(combined => {
        this.goalEvidence = combined.queryParams.goalEvidence;
        this.params = combined.params;

      let homeParams = this.detailService.GetParams();

      homeParams.id &&
        (this.params = { ...this.params, huddle_id: homeParams.id });
      this.getDiscussionDetails();

      this.editDiscussionLSKey = `${this.DISCUSSION_LS_KEYS.EDIT}${this.params.discussion_id}_${homeParams.id}_${this.headerService.getUserId()}`;
      this.editDiscussionTAKey = `${this.DISCUSSION_LS_KEYS.EDIT_TA}${homeParams.id}_${this.headerService.getUserId()}`;
      this.discussionAddCommentLSKey = `${this.DISCUSSION_LS_KEYS.ADD_COMMENT}${this.params.discussion_id}_${homeParams.id}_${this.headerService.getUserId()}`;
      this.discussionAddCommentTAKey = `${this.DISCUSSION_LS_KEYS.ADD_COMMENT_TA}${this.params.discussion_id}_${homeParams.id}_${this.headerService.getUserId()}`;
      this.discussionEditCommentTAKey = `${this.DISCUSSION_LS_KEYS.EDIT_COMMENT_TA}${this.params.discussion_id}_${this.headerService.getUserId()}`;

      this.selectedSort = combined.queryParams.sort ? this.filterOptions[combined.queryParams.sort] : "";
      let discusstion_details_sort=this.headerService.getLocalStorage('discusstion_details_sort');
      if (discusstion_details_sort) {
        this.setQueryparams(discusstion_details_sort);
      } else {
        this.setQueryparams("newest");
      }

    });

    this.socketPushFunctionDiscussionDetail();

    this.detailEmit();
    this.header_data = this.headerService.getStaticHeaderData();
    
    let discussionAddCommentLS = this.headerService.getLocalStorage(this.discussionAddCommentLSKey);
    if(discussionAddCommentLS) this.newComment.comment = discussionAddCommentLS;

    this.headerService.currentUserImage$.subscribe(data=>{
      if(!_.isEmpty(data)){
      this.thumb_image_Url=data;
      this.thumb_image=data.replace(/^.*[\\\/]/, '');
      }
          })
  window.onbeforeunload = () => this.ngOnDestroy(); 
  }

  private socketPushFunctionDiscussionDetail()
  {
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(`discussions-list-${this.params.huddle_id}`).subscribe(data => this.processDiscussionDetailEventSubscriptions(data)));
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(`discussions-details-${this.params.discussion_id}`).subscribe(data => this.processDiscussionDetailEventSubscriptions(data)));
  }

  public OnSearchChange(event) {
    this.searchInput.next(event);
  }

  public setQueryparams(param) {
    this.fullRouter.navigate([], {
      queryParams: { sort: param },
      queryParamsHandling: "merge"
    });

     this.getDiscussionDetails(param);
  }

  setSortLS(param){
    this.headerService.setLocalStorage('discusstion_details_sort',param)
  }

  public DownloadFile(attachment) {
    this.detailService.DownloadDiscussionFile(attachment.document_id);
  }

  public getDiscussionDetails(sort = 'newest') {
    this.isLoading = true;
    let sessionData: any = this.headerService.getStaticHeaderData(),
      obj: any = {
        huddle_id: this.params.huddle_id,
        detail_id: this.params.discussion_id,
        sort: sort,
        goal_evidence: Boolean(this.goalEvidence)
      };

    ({
      User: { id: obj.user_id },
      accounts: { account_id: obj.account_id }
    } = sessionData.user_current_account);

    this.discussionService
      .GetDiscussionDetails(obj)
      .subscribe((data) =>{this.handleDiscussionDetails(data)
        this.disc_detail=data;
        this.editDeletePersissions();
        }) ;
  }

  public handleDiscussionDetails(data) {
    setTimeout(() => {
      this.isLoading = false;
    }, 500);
    if (data.isGoalViewer) this.userAccountLevelRoleId = 125;
    this.DiscussionDetails = data;
    this.user_huddle_role = data.user_huddle_role;
    this.DiscussionDetails.discussion = this.DiscussionDetails.discussions.filter(
      d => d.id == this.params.discussion_id
    );
    this.DiscussionDetails.discussion.length > 0 &&
      (this.DiscussionDetails.discussion = this.DiscussionDetails.discussion[0]);
    delete this.DiscussionDetails.discussions;

    this.bindAvatars()(this.DiscussionDetails);
      this.breadcrumb_added_count++;
      if(this.breadcrumb_added_count == 1)
      {
          

          let breadcrumbs=[];
          breadcrumbs = this.discussionService.GetBreadcrumbs();
          breadcrumbs.push({
              folder_id: -1,
              folder_name: this.DiscussionDetails.discussion.title
          });

          breadcrumbs.forEach((crumb,i) => {
            crumb.isCustom && crumb.folder_name === 'Discussions' ? crumb['folder_id'] = this.params['huddle_id']: false;
          });

					this.homeService.updateBreadcrumb(breadcrumbs)

      }

      /** Restore edit discussion try again array start */
      let tmpDiscussionEditArray = this.headerService.getLocalStorage(this.editDiscussionTAKey);
      if(Array.isArray(tmpDiscussionEditArray)){
        tmpDiscussionEditArray.forEach(tmpDis => {
          if(this.DiscussionDetails.discussion.id == tmpDis.discussion_id){
            this.DiscussionDetails.discussion = tmpDis;
          }
        });
      }
      /** Restore edit discussion try again array end */

      /** Restore discussion add comments try again array start */
      let tmpComments = this.headerService.getLocalStorage(this.discussionAddCommentTAKey);
      if(Array.isArray(tmpComments)){
        this.discussionAddCommentsTAArray = tmpComments;
        this.DiscussionDetails.replys = [...this.discussionAddCommentsTAArray, ...this.DiscussionDetails.replys]; 
      }
      /** Restore discussion add comments try again array end */

      /** Restore discussion edit comments try again array start */
      let tmpEditComments = this.headerService.getLocalStorage(this.discussionEditCommentTAKey);
      if(Array.isArray(tmpEditComments)){
        tmpEditComments.forEach(tmpComment => {
          this.DiscussionDetails.replys.forEach(reply => {
            if(reply.id == tmpComment.comment_id){
              this.unSavedEditedCommentAgain(tmpComment);
            }
          });
        });
      }
      /** Restore discussion edit comments try again array start */
  }

  public bindAvatars() {
    return details => {
      details.discussion.thumb_image = this.discussionService.GetAvatar(
        details.discussion
      );

      details.replys.forEach(r => {
        r.thumb_image = this.discussionService.GetAvatar(r);
      });
    };
  }

  onUpload(e: any, is_reply: boolean = false) {
    this.isReplyMode = is_reply;
    let target = e.target;
    target.files.length > 0 && this.pushToFiles(target.files, is_reply);
    target.value = "";
  }

  public pushToFiles(files: any, is_reply = false): any {
    if (is_reply) {
      for (let i = 0; i < files.length; i++) {
        this.replyFiles.push(this.discussionService.parseFile(files[i]));
      }
    } else {
      for (let i = 0; i < files.length; i++) {
        this.Files.push(this.discussionService.parseFile(files[i]));
      }
    }
  }

  public TriggerUpload() {
    document.getElementById("discussion_editor_files").click();
  }

  public OnDiscussionEdit() {
    this.editDiscData = JSON.parse(JSON.stringify(this.DiscussionDetails.discussion));

    /** Restore  and set edit discussion localstorage object start */
    let lsEditDiscussionObj = this.headerService.getLocalStorage(this.editDiscussionLSKey);
    if(lsEditDiscussionObj){
      this.editDiscData.title = lsEditDiscussionObj.title ? lsEditDiscussionObj.title : "";  
      this.editDiscData.comment = lsEditDiscussionObj.comment ? lsEditDiscussionObj.comment : "";  
    }
    /** Restore  and set edit discussion localstorage object end */
    
    this.Files = [];
    this.stockFiles = JSON.parse(
      JSON.stringify(this.DiscussionDetails.discussion.attachments)
    );
    this.editTitleLength=this.editDiscData.title.length;
    this.editDescriptionLength= this.editDiscData.comment.toString().replace(/<.*?>/g, "").length;

    this.ShowModal(this.editDiscussion, "lg_popup", "editableDiscussionModal");
  }

  public SubmitReply() { 
    this.headerService.removeLocalStorage(this.discussionAddCommentLSKey);

    this.comment_flag=1;
    if (!this.newComment.comment || this.newComment.comment.trim() == "" || this.newComment.comment.toString().replace(/<.*?>/g, "") == "" || this.newComment.comment.toString().replace(/<.*?>/g, "").trim() == "") {
      this.toastr.ShowToastr('info',this.translation.discussion_enter_text_to_reply);
      this.comment_flag=0;
      return;
    } else {
        if(this.replyFiles.length)
        {
            let result =  this.headerService.checkFileSizeLimit(this.replyFiles);
            if(!result.status)
            {
                this.toastr.ShowToastr('error',result.message);
                this.comment_flag=0;
                return;
            }
        }
        let comment_time = moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS")
      let sessionData: any = this.headerService.getStaticHeaderData();
      let obj: any = {
        fake_id:moment(new Date()).format('x'),
        huddle_id: this.params.huddle_id,
        send_email: true,
        comment_id: this.DiscussionDetails.discussion.id,
        commentable_id: this.DiscussionDetails.discussion.id,
        discussion_id: this.DiscussionDetails.discussion.id,
        user_current_account: sessionData.user_current_account,
        remove_attachments: "",
        title: "",
        notification_user_ids: this.emailAll ? this.DiscussionDetails.adminUsers.map(u => u.id).join():"",
        created_at_gmt: comment_time

      };

      ({ comment: obj.comment } = this.newComment);

      ({
        User: { id: obj.user_id },
        accounts: { account_id: obj.account_id }
      } = sessionData.user_current_account);

      this.DiscussionDetails.replys.unshift(this.createDummyReply(obj,sessionData));
      this.DiscussionDetails.replys=[...this.DiscussionDetails.replys];

      let fd = this.discussionService.ToFormData(obj);
      this.replyFiles.length > 0 &&
        (() => {
          for (let i = 0; i < this.replyFiles.length; i++) {
            fd.append("attachment[]", this.replyFiles[i], this.replyFiles[i].name);
          }
        })();
        this.newComment = {};
        this.replyFiles = [];
        
        this.appMainService.AddDiscussion(fd).subscribe((data:any) => {

        if(data.success==true){
          this.commentStatus=true;          
        }
        else if(data.success == false){
          this.toastr.ShowToastr('error',data.message);
          this.fullRouter.navigate(['/video_huddles/list']);
          
        }
      },
      (error)=>{
        obj.unsaved_comment_id = this.minus_Id;
        obj.first_name = sessionData.user_current_account.User.first_name;
        obj.last_name = sessionData.user_current_account.User.last_name;
        obj.thumb_image =  'assets/video-huddle/img/c1.png';
        obj.tryAgain =  true;
        obj.uuid =  new Date().getTime();
          
        --this.minus_Id;
        this.discussionAddCommentsTAArray.push(obj);
        this.headerService.setLocalStorage(this.discussionAddCommentTAKey, this.discussionAddCommentsTAArray)
        this.DiscussionDetails.replys = _.without(this.DiscussionDetails.replys, _.findWhere(this.DiscussionDetails.replys, {
            fake_id: obj.fake_id
        }));
        this.DiscussionDetails.replys.unshift(obj);
        this.DiscussionDetails.replys=[...this.DiscussionDetails.replys]
        this.commentStatus=false;
        
      }
      );
      
    }
    let that = this;
    setTimeout(function(){
      that.comment_flag=0;
    },3000);
    
  }
  public createDummyReply(reply,sessionData)
{
    let obj={
    id:0,
    fake_id:reply.fake_id,
    parent_comment_id:reply.comment_id,
    title: reply.title,
    comment:reply.comment,
    first_name: sessionData.user_current_account.User.first_name,
last_name: sessionData.user_current_account.User.last_name,
image: this.thumb_image,
      user_id:sessionData.user_current_account.User.id,
      thumb_image: this.thumb_image_Url
  }

  return obj;
}
  public onCancelReply(){
    this.replyFiles = [];
    this.newComment = {};

    this.headerService.removeLocalStorage(this.discussionAddCommentLSKey);

  }

  public onEditSubmission() {
    this.headerService.removeLocalStorage(this.editDiscussionLSKey);

    if (!this.editDiscData.title || this.editDiscData.title.trim()==""|| !this.editDiscData.comment || 
      this.editDiscData.comment.trim() == "" || this.editDiscData.comment.toString().replace(/<.*?>/g, "") == "" || 
      this.editDiscData.comment.toString().replace(/<.*?>/g, "").trim() == "") {
        this.toastr.ShowToastr('info', this.translation.discussion_title_description_required_to_edit);
    } else {
        if(this.Files.length)
        {
            let result =  this.headerService.checkFileSizeLimit(this.Files);
            if(!result.status)
            {
                this.toastr.ShowToastr('error',result.message);
                return;
            }
        }

      let sessionData: any = this.headerService.getStaticHeaderData();
      let comment_time = moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS")
      let obj: any = {
        huddle_id: this.params.huddle_id,
        send_email: true,
        comment_id: this.editDiscData.id,
        discussion_id: this.editDiscData.id,
        user_current_account: sessionData.user_current_account,
        remove_attachments: this.stockFiles
          .filter(i => i.isDeleted)
          .map(f => f.document_id)
          .join(","),
          created_at_gmt: comment_time
      };

      ({ comment: obj.comment, title: obj.title } = this.editDiscData);

      ({
        User: { id: obj.user_id },
        accounts: { account_id: obj.account_id }
      } = sessionData.user_current_account);

      let fd = this.discussionService.ToFormData(obj);
      this.Files.length > 0 &&
        (() => {
          for (let i = 0; i < this.Files.length; i++) {
            fd.append("attachment[]", this.Files[i], this.Files[i].name);
          }
        })();

      this.ModalRefs.editableDiscussionModal.hide();
      this.editDiscData = { title: "", comment: "" };
      this.discussionService.responseSubject.next(true);
      this.appMainService.EditDiscussion(fd).subscribe((d: any) => {
        this.discussionService.responseSubject.next(false);
        if (d.success) {

          this.DiscussionDetails.discussion = d.data[0];
          this.bindAvatars()(this.DiscussionDetails);
        } else {
          this.toastr.ShowToastr('info',
            d.message || this.translation.something_went_wrong_msg
          );
        }
      }, err => {
        obj.first_name = sessionData.user_current_account.User.first_name;
        obj.last_name = sessionData.user_current_account.User.last_name;
        obj.thumb_image = 'assets/video-huddle/img/c1.png', 
        obj.tryAgain = true;
        obj.editTryAgain = true;
        obj.uuid = new Date().getTime();
        obj.parsedComment = obj.comment.replace(/<.*?>/g, "");
        this.discussionService.responseSubject.next(false);
        this.DiscussionDetails.discussion = obj;
        let tmpDiscussionEditArray = this.headerService.getLocalStorage(this.editDiscussionTAKey);
        if(!Array.isArray(tmpDiscussionEditArray)) tmpDiscussionEditArray = [];
          tmpDiscussionEditArray.push(obj);
          this.headerService.setLocalStorage(this.editDiscussionTAKey, tmpDiscussionEditArray);
      });
    }
  }

  public ShowModal(template: TemplateRef<any>, class_name, name) {

    this.ModalRefs[name] = this.modalService.show(template, {ignoreBackdropClick :true,
      class: class_name
    });
  }

  public exportDiscussion(to?) {
    let sessionData: any = this.headerService.getStaticHeaderData(),
      obj: any = {
        export_type: to,
        huddle_id: this.params.huddle_id,
        detail_id: this.params.discussion_id,
        
      };

    ({
      User: { id: obj.user_id },
      accounts: { account_id: obj.account_id }
    } = sessionData.user_current_account);

    this.discussionService.exportDiscussion(obj);
  }

  public onEmialClick() {
    this.updateEmailText(this.DiscussionDetails.discussion.title);
    this.discussion_title =
      this.DiscussionDetails.discussion.title + " - "+this.translation.discussion_discussion_export;
    this.email_attachment_name =
      this.DiscussionDetails.discussion.title + " - "+this.translation.discussion_discussion_export+".docx";
    this.Show_Modal(this.email_discussion, "lg_popup");
  }

  public Show_Modal(template: TemplateRef<any>, class_name) {

    this.ModalRefs.emailModal = this.modalService.show(template, {
      class: class_name
    });
  }

  public emailDiscussion() {
    let flag;
    if (this.user_email == "" || !this.user_email) {
      this.toastr.ShowToastr('info',this.translation.discussion_please_enter_email_address);
      return;
    } else {
      flag = this.validateEmail(this.user_email);
    }

    if (!flag) {
      this.toastr.ShowToastr('info',this.translation.discussion_please_enter_valid_email_address);
    } else {
      let sessionData: any = this.headerService.getStaticHeaderData(),
        obj: any = {
          huddle_id: this.params.huddle_id,
          detail_id: this.params.discussion_id,
          user_email: this.user_email,
          email_message: this.email_message
        };

      this.discussionService.email_discussion(obj).subscribe((data: any) => {
        this.toastr.ShowToastr('info',data.message);
        this.emptyEmailFields();
      });
      this.ModalRefs.emailModal.hide();
    }
  }

  public validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  public emptyEmailFields() {
    this.email_message = this.translation.discussion_email_message_text;
    this.user_email = "";
  }


  private processDiscussionDetailEventSubscriptions(data){

      switch (data.event) {
          case "discussion_deleted":
          this.isAllowed();
          this.processDiscussionDetailDeleted(data.data[0]);
          break;

          case "discussion_added":
          this.isAllowed();
          this.processDiscussionDetailAdded(data.data[0]);
          break;

          case "discussion_edited":
          this.isAllowed();
          this.processDiscussionDetailEdited(data.data[0]);
          break;

          

          
        
        default:
          break;
      }
    
  }

  private isAllowed()
  {

  }

  private processDiscussionDetailDeleted(discussion){
    if(discussion)
    {
    discussion.thumb_image = this.discussionService.GetAvatar(
      discussion
    );
    
      if(discussion.parent_comment_id == this.DiscussionDetails.discussion.id)
      {
          this.DiscussionDetails.replys = _.without(this.DiscussionDetails.replys, _.findWhere(this.DiscussionDetails.replys, {
              id: discussion.id
          }));
          if(discussion.created_by != this.sessionData.user_current_account.User.id)
          {
              this.toastr.ShowToastr('info',this.translation.discussion_a_comment_deleted);
          }
      }
      else
      {
          let that = this;
          this.DiscussionDetails.replys.forEach(function (comment) {
              if(comment.id == discussion.parent_comment_id)
              {
                  if(!comment.replies || comment.replies.length == 0)
                  {
                      comment.replies = [];
                  }
                  comment.replies = _.without(comment.replies, _.findWhere(comment.replies, {
                      id: discussion.id
                  }));
                  if(discussion.created_by != that.sessionData.user_current_account.User.id)
                  {
                      that.toastr.ShowToastr('info',that.translation.discussion_a_reply_deleted);
                  }
              }
          });
      }
    }

 }

 private processDiscussionDetailAdded(discussion){
  discussion.thumb_image = this.discussionService.GetAvatar(
    discussion
  );
  if(discussion.parent_comment_id != 0 && discussion.parent_comment_id==this.params.discussion_id)
   {
    if(!this.DiscussionDetails.replys || this.DiscussionDetails.replys.length == 0)
    {
      this.DiscussionDetails.replys = [];
    }
    

    let index=this.DiscussionDetails.replys.findIndex(d=>d.fake_id==discussion.fake_id);
    if(index > -1){
      this.DiscussionDetails.replys.splice(index,1);
    }
    this.DiscussionDetails.replys=[...this.DiscussionDetails.replys]

    if(this.selectedSort == 1){
      this.DiscussionDetails.replys.unshift(discussion);
    }else if(this.selectedSort == 2){
      this.DiscussionDetails.replys.push(discussion);
    }
   this.dirty = !this.dirty;
     if(discussion.created_by != this.sessionData.user_current_account.User.id)
     {
         this.toastr.ShowToastr('info',this.translation.discussion_new_comment_added);
     }
   }
   else if(this.DiscussionDetails.replys && this.DiscussionDetails.replys.length)
  {
      let that = this;
      this.DiscussionDetails.replys.forEach(function (comment) {
          if(comment.id == discussion.parent_comment_id)
          {
              if(!comment.replies || comment.replies.length == 0)
              {
                  comment.replies = [];
              }
              let index=comment.replies.findIndex(d=>d.fake_id==discussion.fake_id);
              if(index > -1){
                comment.replies.splice(index,1);
              }
              comment.replies=[...comment.replies]

              comment.replies.push(discussion);
              if(discussion.created_by != that.sessionData.user_current_account.User.id){
                  that.toastr.ShowToastr('info',that.translation.discussion_new_reply_added);
              }
          }
      })
  }
 }

 private processDiscussionDetailEdited(discussion)
 {
  discussion.thumb_image = this.discussionService.GetAvatar(
    discussion
  );
   if(discussion.parent_comment_id == 0)
   {
     
    this.DiscussionDetails.discussion = discussion;

   }
   else if(discussion.parent_comment_id == this.DiscussionDetails.discussion.id)
   {
     let replyIndex = this.DiscussionDetails.replys.findIndex(reply => reply.id == discussion.id);
     
     this.DiscussionDetails.replys[replyIndex] = discussion;



      this.dirty=!this.dirty;
     if(discussion.created_by != this.sessionData.user_current_account.User.id)
     {
         this.toastr.ShowToastr('info',this.translation.discussion_a_comment_updated);
     }
   }
   else
   {
       let that = this;
       this.DiscussionDetails.replys.forEach(function (comment) {
           if(comment.id == discussion.parent_comment_id)
           {
               if(!comment.replies || comment.replies.length == 0)
               {
                   comment.replies = [];
               }
               let index = -1;

               comment.replies.forEach((item, i)=>{
                   item.id == discussion.id && (index = i);
               });

               comment.replies[index] = discussion;
               that.dirty=!that.dirty;
               if(discussion.created_by != that.sessionData.user_current_account.User.id)
               {
                   that.toastr.ShowToastr('info',that.translation.discussion_a_reply_updated);
               }
           }
       });

   }


 }


  AddDiscussionSubmit() {}

  deleteComment(comment) {
    console.log(this.Inputs.Confirmation)
    this.commentID = comment.id ;
    if(comment.created_by != this.sessionData.user_current_account.User.id && comment.huddle_type == 2){
      this.showConfirmationDialog = true;
    } else {
      this.showConfirmationDialog = false;
    }
    this.Show_Modal(this.deleteTemplate, "sm_popup");
  }
  TriggerTextChangeConfig(ev){
    if(ev.keyCode==13) {
      this.confirmDeleteWithConfig()
    }
  }
  confirmDeleteWithConfig(){
    if (this.Inputs.ConfirmationKey != this.Inputs.Confirmation) {
			this.toastr.ShowToastr('info',this.translation.huddle_you_typed_in + " '" + this.Inputs.ConfirmationKey + "' ");
			return;
    } else {
      this.confirmDeleteString = 'DELETE'
      let sessionData: any = this.headerService.getStaticHeaderData();
      if (this.confirmDeleteString == "DELETE") {
        this.counter=1;
        let obj:any = {
          discussion_id: this.commentID,
          original_discussion_id: this.DiscussionDetails.discussion.id,
        };
  
        const params = this.detailService.GetParams();
        obj.huddle_id = params.id;
        obj.user_id = sessionData.user_current_account.User.id;
          this.appMainService.DeleteDiscussion(obj).subscribe(
       
          data => {
            let d: any = data;
            if (d.success) {
  
              this.DiscussionDetails.replys = this.DiscussionDetails.replys.filter(
                d => d.id != this.commentID
              );
              
            } else {
               this.toastr.ShowToastr('info',d.message, this.translation.discussion_delete_confirmation);
            }
            this.deSelectDeleteField();
          },
          error => {
            this.toastr.ShowToastr('error',error.message);
          }
        );
      } else {
        this.toastr.ShowToastr('info',this.translation.discussion_type_delete);
      }
    }
  }
  confirmDelete() {
    this.confirmDeleteString = 'DELETE'
    let sessionData: any = this.headerService.getStaticHeaderData();
    if (this.confirmDeleteString == "DELETE") {
      this.counter=1;
      let obj:any = {
        discussion_id: this.commentID,
        original_discussion_id: this.DiscussionDetails.discussion.id,
      };

      const params = this.detailService.GetParams();
      obj.huddle_id = params.id;
      obj.user_id = sessionData.user_current_account.User.id;
        this.appMainService.DeleteDiscussion(obj).subscribe(
     
        data => {
          let d: any = data;
          if (d.success) {

            this.DiscussionDetails.replys = this.DiscussionDetails.replys.filter(
              d => d.id != this.commentID
            );
            
          } else {
             this.toastr.ShowToastr('info',d.message, this.translation.discussion_delete_confirmation);
          }
          this.deSelectDeleteField();
        },
        error => {
          this.toastr.ShowToastr('error',error.message);
        }
      );
    } else {
      this.toastr.ShowToastr('info',this.translation.discussion_type_delete);
    }
    
  }

  deSelectDeleteField() {
   
    this.ModalRefs.emailModal.hide();
    this.Inputs.Confirmation = "";
    let that = this;
    setTimeout(function(){
      that.counter=0;
    },500);
    
    
    this.confirmDeleteString = "";
  }

  public editorStateListner(event){
    this.editorShow=event
  }
  public detailEmit(){
    this.discussionService.dDemit.emit(true);
  }

  public TriggerTextChange(event){
    if(this.counter==0){
      if(event.keyCode==13) {

      this.confirmDelete();
        
      }
    }
  }
  saveCommentAgain(event){
    let fd = this.discussionService.ToFormData(event);
      this.replyFiles.length > 0 &&
        (() => {
          for (let i = 0; i < this.replyFiles.length; i++) {
            fd.append("attachment[]", this.replyFiles[i], this.Files[i].name);
          }
        })();
    this.discussionService.AddDiscussion(fd).subscribe((data:any) => {
      let indexofcomment =  this.DiscussionDetails.replys.findIndex(x => x.uuid == event.uuid);
      let replyIndex = this.discussionAddCommentsTAArray.findIndex(x => x.uuid == event.uuid);

        if(indexofcomment>=0){
          this.DiscussionDetails.replys.splice(indexofcomment,1);
          this.DiscussionDetails.replys =[...this.DiscussionDetails.replys];
          
        }

        if(replyIndex > -1) {
          this.discussionAddCommentsTAArray.splice(replyIndex,1);
          this.headerService.setLocalStorage(this.discussionAddCommentTAKey, this.discussionAddCommentsTAArray)
        }
        
      if(data.success=true){

        this.commentStatus=true;          
      }
      this.newComment = {};
      this.replyFiles = [];
      if(data.code == 409){
        const taComments = this.headerService.getLocalStorage(this.discussionAddCommentTAKey);
        if (Array.isArray(taComments)) {
          const commentIndex = taComments.findIndex(tsComment => tsComment.created_at_gmt === data.data.created_at_gmt);
          if (commentIndex > -1) {
            taComments.splice(commentIndex, 1);
            this.headerService.setLocalStorage(this.discussionAddCommentTAKey, taComments);
          }
        }
      }
      let indexToDelete = this.discussionAddCommentsTAArray.findIndex(x => x.created_at_gmt == data.data.created_at_gmt && x.tryAgain == true);

        if(indexToDelete>=0){
          this.DiscussionDetails.replys.splice(indexToDelete,1);
          this.DiscussionDetails.replys =[...this.DiscussionDetails.replys];
          
        }
      let checkIfAdded = this.DiscussionDetails.replys.findIndex(x => x.created_at_gmt == data.data.created_at_gmt);
      if(checkIfAdded<0){
        this.DiscussionDetails.replys.push(data.data)
      }

    }, 
    (error)=>{
      event.processing = false;
    }
    );
  }
  unSavedEditedCommentAgain(editableComment){
    var indexofcomment =  this.DiscussionDetails.replys.findIndex(x => x.id == editableComment.comment_id);
    if(indexofcomment!=-1){
      this.DiscussionDetails.replys[indexofcomment] = editableComment
      
  
    this.DiscussionDetails.replys = [...this.DiscussionDetails.replys]
    }
    
  }
  unsavedEditedCommentRemove(editedComment){
    var indexofcomment =  this.DiscussionDetails.replys.findIndex(x => x.id == editedComment.comment_id);
    if(indexofcomment!=-1){
      this.DiscussionDetails.replys.splice(indexofcomment,1);
    this.DiscussionDetails.replys =[...this.DiscussionDetails.replys];
    }
    
  }

  public editDiscussionTryAgain(discussion: any){
    this.selectedId=discussion.discussion_id;
    this.DiscussionDetails.discussion.tryAgain =false;
    this.discussionService.responseSubject.next(true);
    if(discussion.processing) return;
    discussion.processing = true;

    let fd = this.discussionService.ToFormData(discussion);
    let newFd = new FormData;
    newFd.append('huddle_id',discussion.huddle_id)
    newFd.append('send_email',discussion.send_email)
    newFd.append('comment_id',discussion.comment_id)
    newFd.append('commentable_id',discussion.commentable_id)
    newFd.append('discussion_id',discussion.discussion_id)
    newFd.append('remove_attachments',discussion.remove_attachments)
    newFd.append('title',discussion.title)
    newFd.append('notification_user_ids',discussion.notification_user_ids)
    newFd.append('comment',discussion.comment)
    newFd.append('user_id',discussion.user_id)
    newFd.append('account_id',discussion.account_id)
    newFd.append('user_current_account',discussion.user_current_account)
    newFd.append('created_at_gmt',discussion.created_at_gmt)

    this.appMainService.EditDiscussion(newFd).subscribe((d: any) => {
      if (d.success) {
        this.toastr.ShowToastr('info',d.message);
        this.DiscussionDetails.discussion = d.data[0];
        this.bindAvatars()(this.DiscussionDetails);
        this.DiscussionDetails.discussion.parsedComment = this.DiscussionDetails.discussion.comment.replace(/<.*?>/g, "");
        this.discussionService.responseSubject.next(false);

        // remove from local storage
        let tmpDiscussionEditArray = this.headerService.getLocalStorage(this.editDiscussionTAKey);
        if(Array.isArray(tmpDiscussionEditArray) && tmpDiscussionEditArray.length > 0){
          tmpDiscussionEditArray = tmpDiscussionEditArray.filter(dis => dis.uuid != discussion.uuid);
          this.headerService.setLocalStorage(this.editDiscussionTAKey, tmpDiscussionEditArray);
        }

        if(d.code == 409){
          const taComments = this.headerService.getLocalStorage(this.editDiscussionTAKey);
          if (Array.isArray(taComments)) {
            const commentIndex = taComments.findIndex(tsComment => tsComment.created_at_gmt === d.data.created_at_gmt);
            if (commentIndex > -1) {
              taComments.splice(commentIndex, 1);
              this.headerService.setLocalStorage(this.editDiscussionTAKey, taComments);
            }
          }
        }
      } else {
        this.toastr.ShowToastr('info',
          d.message || this.translation.something_went_wrong_msg
        );
      }
    }, err => {
      setTimeout(() => {
        discussion.processing = false;
        discussion.tryAgain =true;
        this.discussionService.responseSubject.next(false);
      }, 500);
   

    });

  }
  public editModalChange(key: string, value: string){
    
    if(key === 'title'){
      this.titleEdited=true;
    } else if(key === 'comment'){
      this.commentEdited = true;
      console.log(value,this.commentEdited);
    }

  }
  public cancelEditDiscussionModal(){
    if(this.titleEdited || this.commentEdited){
      if(confirm(this.translation.you_have_unsaved_changes)){
        this.commentEdited=false;
        this.titleEdited=false;
        this.ModalRefs.editableDiscussionModal.hide();
        this.headerService.removeLocalStorage(this.editDiscussionLSKey);
        
      }
    }
    else
    {
      this.commentEdited=false;
      this.titleEdited=false;
    this.ModalRefs.editableDiscussionModal.hide();
    this.headerService.removeLocalStorage(this.editDiscussionLSKey);
  }

  }
  private saveEditDiscussionToLocalStorage (): void {
    if(this.editDiscData.title || this.editDiscData.comment){
      let obj = {title: this.editDiscData.title, comment: this.editDiscData.comment};
      this.headerService.setLocalStorage(this.editDiscussionLSKey, obj);
    }
  }

  private saveDiscussionAddCommentToLocalStorage (): void {
    if(this.newComment.comment){
      this.headerService.setLocalStorage(this.discussionAddCommentLSKey, this.newComment.comment);
    }
  }
  selectedComment(discussion){
    if(discussion)
    this.discussionService.selectedID(discussion.discussion_id ||discussion.id);
    else{
      this.discussionService.selectedID(this.editDiscData.id);
    }
  }
  // isUnsavedChanges(){
  //   if(this.editTitleLength !=this.editDiscData.title.length || this.editDescriptionLength != this.editDiscData.comment.toString().replace(/<.*?>/g, "").length)
  //   return true;

  //   else  
  //   return false;

  // }
  
    
    public updateEmailText(title)
    {
        this.email_message = this.translation.discussion_email_message_text.replace('{{title}}', title);
    }
    OnDeleteMainPost(){
      this.ShowModal(this.deleteMainPost, "sm_popup", "deleteMainPost");
    }

    public myConfirmDeleteWithConfig() {
      if (this.Inputs.ConfirmationKey != this.Inputs.Confirmation) {

			this.toastr.ShowToastr('info',this.translation.huddle_you_typed_in + " '" + this.Inputs.ConfirmationKey + "' ");
			return;
    } else {
    this.confirmDeleteString='DELETE';
    let sessionData: any = this.headerService.getStaticHeaderData();
    if (this.confirmDeleteString == "DELETE") {
      this.counter=1;
      let obj:any = {
        discussion_id: this.params.discussion_id,
        original_discussion_id: this.params.discussion_id,
      };

      const params = this.detailService.GetParams();
      obj.huddle_id = params.id;
      obj.user_id = sessionData.user_current_account.User.id;
        this.appMainService.DeleteDiscussion(obj).subscribe(
          data => {
          let d: any = data;
          if (d.success) {
          }
          this.deSelectDelete();
          //this.deletableDiscussion = {};
        },
        error => {
          this.toastr.ShowToastr('error',error.message);
        }
      );
    } else {
      this.toastr.ShowToastr('info',this.translation.discussion_type_delete);
    }
  }
  }

  deSelectDelete(){
    this.ModalRefs.deleteMainPost.hide();
    let discusstion_details_sort=this.headerService.getLocalStorage('discusstion_list_sort');
    this.router.navigate(['/video_huddles/huddle/details/' + this.params.huddle_id + '/discussions'],{queryParams: {sort: discusstion_details_sort, tab: 'discussion'}})
  }

  editDeletePersissions(){
   
    if( this.permisionService.getdiscussionPermissions(this.DiscussionDetails.discussion.created_by) && this.userAccountLevelRoleId != '125'){
      
      this.actDelete=true;
    }
     else if(this.huddleHeaderData.huddle_type==1 && this.params.user_id==this.header_data.user_current_account.User.id && this.header_data.user_permissions.UserAccount.role_id=='200' && this.userAccountLevelRoleId != '125'){
      
      this.actDelete=true;
     }
     else if((this.disc_detail.user_huddle_role == 200 && this.huddleHeaderData.huddle_type == '2')  || (this.disc_detail.user_huddle_role == 210 && this.DiscussionDetails?.coachee_permission == '1' && this.huddleHeaderData.huddle_type == '2' && this.permisionService.getdiscussionPermissions(this.DiscussionDetails?.discussion.created_by) ))
     {
        this.actDelete=true;
     }
}
 
  ngOnDestroy() {
    //this.saveEditDiscussionToLocalStorage();
    this.saveDiscussionAddCommentToLocalStorage();

    this.subscription.unsubscribe();
  }
  
}

interface Modals {
  [key: string]: any;
}
interface GeneralInputs {
	NewFolderName: string,
	Confirmation: string,
	ConfirmationKey: string
}
