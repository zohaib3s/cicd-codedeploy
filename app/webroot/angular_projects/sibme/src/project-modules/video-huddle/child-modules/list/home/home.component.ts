import { Component, OnInit, TemplateRef, ViewChild, OnDestroy, HostListener, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { SessionService } from "../services/session.service";
import { HeaderService, HomeService, SocketService } from '@projectModules/app/services';
import { Subject, Subscription } from "rxjs";
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { DndDropEvent } from "ngx-drag-drop";
import * as _ from "underscore";

import { ShowToasterService } from '@projectModules/app/services';
import { LandingPageComponent } from '@src/project-modules/add-huddle/components/landing-page/landing-page.component';
import { count } from 'console';

@Component({
	selector: 'video-huddle-list-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
	landingpageModalRef: BsModalRef;
	p: number = 1;
	public Filters: filters = {};
	public objHuddleType = 0;
	public ModalRefs: Modals;
	public Inputs: GeneralInputs;
	public page;
	public Loadings;
	public HuddlesAndFolders;
	public foldersExpanded: boolean;
	public DetailedHuddle;
	public SearchString;
	public subscriptionRefs;
	public EditableFolder;
	public PipeTrigger;
	public FoldersTreeData;
	public MovableItem;
	public params: any = {};
	public queryParamsObj: any = {};
	public DeletableItem;
	public ScrollVal: number = 500;
	private searchInput: Subject<string> = new Subject();
	public colors;
	public permissions: any = {};
	public header_data;
	public translation: any = {};
	subscription: Subscription;
	public userAccountLevelRoleId: number | string = null;
	public showSkeleton = true;
	public socket_listener: any;
	public coaching_permissions;
	public collaboration_permissions;
	public assessment_permission;
	public mainCreator_Huddle;
	public changeBackgroundColor:boolean = false;
	public dragHuddleId:any;
	public viewerObject:any;
	//public additionalData:any
	opts: ISlimScrollOptions;
	scrollEvents: EventEmitter<SlimScrollEvent>;
	@ViewChild('participentsDetailsModal', { static: false }) participentsDetailsModal;
	@ViewChild('LandingPage', { static: false }) LandingPage;
	assessment_permissions;
	landingpageclass: any;
	landingpagemodalclass: any;
	public assessment_huddle_active;
	user_role: any;
	public checkHuddleTracker = false;
	enable_goals: any;
	constructor(private fullRouter: Router, private ARouter: ActivatedRoute, private headerService: HeaderService, private sessionService: SessionService,
		private toastr: ShowToasterService, private modalService: BsModalService, private homeService: HomeService, private socketService: SocketService) {
		this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
			this.translation = languageTranslation;
			this.Inputs = { NewFolderName: "", Confirmation: "", ConfirmationKey: this.translation.Huddle_confirmation_bit };
		});
	}

	@HostListener('window:scroll', ['$event'])
	onScroll(event) {
		if (this.Loadings.isNextPageLoading) {

			let doc = document.documentElement;
			let currentScroll = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

			var d = document.documentElement;
			var offset = window.innerHeight + window.pageYOffset
			var height = d.offsetHeight;
			if ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight - 2) {
				window.scroll(0, currentScroll - 50);
			}

		}
		else if (!this.Loadings.isNextPageLoading && this.HuddlesAndFolders && this.HuddlesAndFolders.huddles.length < this.HuddlesAndFolders.total_huddles) {

			setTimeout(() => {

				let doc = document.documentElement;
				let currentScroll = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

				var d = document.documentElement;
				var offset = d.scrollTop + window.innerHeight;
				var height = d.offsetHeight;

				if ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight - 2) {
					this.LoadNextPage(this.params.folder_id, currentScroll);
					window.scroll(0, document.body.offsetHeight - this.getPercentage(document.body.offsetHeight, 8));
				}


			}, 100);

		} else {
		}



	}

	ngOnInit() {
		this.scrollEvents = new EventEmitter<SlimScrollEvent>();
		this.opts = {
			position: 'right',
			barBackground: '#C9C9C9',
			barOpacity: '0.8',
			barWidth: '10',
			barBorderRadius: '20',
			barMargin: '0',
			gridBackground: '#D9D9D9',
			gridOpacity: '1',
			gridWidth: '0',
			gridBorderRadius: '20',
			gridMargin: '0',
			alwaysVisible: true,
			visibleTimeout: 1000,
		}
		this.header_data = this.headerService.getStaticHeaderData();

		console.log('header_Data',this.header_data);
		this.userAccountLevelRoleId = this.header_data.user_permissions.roles.role_id;
		this.enable_goals = this.header_data.enable_goals;
		this.user_role = this.header_data.user_current_account.roles.role_id;
		this.assessment_huddle_active = this.header_data.assessment_huddle_active;
		this.coaching_permissions = this.header_data.user_permissions.UserAccount.manage_coach_huddles;
		this.collaboration_permissions = this.header_data.user_permissions.UserAccount.manage_collab_huddles;
		this.assessment_permission = this.header_data.user_permissions.UserAccount.manage_evaluation_huddles;
		console.log(this.coaching_permissions, this.collaboration_permissions, this.assessment_permission);
		let channel_name = `huddle-${this.header_data.user_current_account.users_accounts.account_id}`;
		this.subscription.add(this.socketService.pushEventWithNewLogic(channel_name).subscribe(data => this.processEventSubscriptions(data)));
		this.initVars();
		this.GetParams();
		this.SubscribeSearch();
		this.Assessment_huddle_permissions();
		this.LSfolderSetting();
		this.checkHuddleTrackerPermission();
		let Permissioncount = Number(this.collaboration_permissions) + Number(this.coaching_permissions) + Number(this.assessment_permission);

		if (Permissioncount > 2) {
			this.landingpageclass = "col-md-4";
			this.landingpagemodalclass = "modal-lg  new_hu_cls"
		}
		else if (Permissioncount == 2) {
			this.landingpageclass = "col-md-6";
			this.landingpagemodalclass = "modal-lg  new_hu_cls2"
		}
		else if (Permissioncount == 1) {
			this.landingpageclass = "col-md-12";
			this.landingpagemodalclass = "modal-lg  new_hu_cls1"
		}
	}
	openLandingPageModal() {


		this.landingpageModalRef = this.modalService.show(this.LandingPage, { class: this.landingpagemodalclass, backdrop: 'static' })

	}
	public checkHuddleTrackerPermission() {
		if ((this.user_role == 100 || this.user_role == 110 || this.user_role == 115 || this.user_role == 120) && (this.header_data.tracker_permission)) {
			if (this.user_role == 120 && !this.header_data.is_user_coach) {
				this.checkHuddleTracker = false;
				return false
			}
			this.checkHuddleTracker = true;
		}
		else {
			this.checkHuddleTracker = false;
		}
	}
	public getToPage(type) {
		if (type == "coaching") {
			let str = this.queryParamsObj.folderId ? `/add_huddle_angular/${type}/${this.queryParamsObj.folderId}` : `/add_huddle_angular/${type}`;
			this.landingpageModalRef.hide();
			setTimeout(() => {
				this.fullRouter.navigate([str]);
			}, 500);

		}
		else if (type == "collaboration") {
			let str = this.queryParamsObj.folderId ? `/add_huddle_angular/${type}/${this.queryParamsObj.folderId}` : `/add_huddle_angular/${type}`;
			this.landingpageModalRef.hide();
			setTimeout(() => {
				this.fullRouter.navigate([str]);
			}, 500);
		}
		else if (type == "assessment") {
			let str = this.queryParamsObj.folderId ? `/add_huddle_angular/${type}/${this.queryParamsObj.folderId}` : `/add_huddle_angular/${type}`;
			this.landingpageModalRef.hide();
			setTimeout(() => {
				this.fullRouter.navigate([str]);
			}, 500);
		}

	}
	private SubscribeSearch() {

		this.searchInput
			.pipe(debounceTime(1000), distinctUntilChanged())
			.subscribe(value => {

				this.search();
			});



	}

	public OnSearchChange(event) {
		this.searchInput.next(event);

		if (this.SearchString == '') {
			this.Loadings.isSearchEmpty = true;
		} else {
		}
	}

	private processEventSubscriptions(data) {
		console.log(data);

		switch (data.event) {
			case "resource_added":
				this.processResourceAdded(data.data);
				break;

			default:
				break;
		}
	}
	processResourceAdded(resource) {
		this.HuddlesAndFolders.huddles.forEach(huddle => {
			if (huddle.huddle_id == resource.account_folder_id) {
				if (resource.doc_type == 1)
					huddle.stats.videos++;

				if (resource.doc_type == 2)
					huddle.stats.attachments++;
			}
		});
	}

	private initVars(preserve_params?, preserve_search_string?) {
		this.LoadFilterSettings();
		this.page = 1;
		this.Loadings = {};
		let toggleSetting = JSON.parse(localStorage.getItem('foldersExpanded'));
		toggleSetting != undefined ? this.foldersExpanded = toggleSetting : this.foldersExpanded = true;

		if (!preserve_search_string) {
			this.SearchString = "";
		}

		this.FoldersTreeData = {};
		this.subscriptionRefs = {};
		this.EditableFolder = {};
		this.PipeTrigger = false;
		let sessionData: any = this.headerService.getStaticHeaderData();
		this.permissions.allow_move = sessionData.user_current_account.users_accounts.folders_check == 1;
		this.Loadings.isNextPageLoading;
		if (!preserve_params) {
			this.params = {};
		}

		if (this.HuddlesAndFolders != void 0)
			if (void 0 != this.HuddlesAndFolders.huddle_create_permission || this.HuddlesAndFolders.folder_create_permission != void 0) {

				let copy = JSON.parse(JSON.stringify(this.HuddlesAndFolders));
				this.HuddlesAndFolders = {};
				({
					huddle_create_permission: this.HuddlesAndFolders.huddle_create_permission,
					folder_create_permission: this.HuddlesAndFolders.folder_create_permission
				} = copy);

				copy = {};

			} else {
				this.HuddlesAndFolders = {};
			}



		this.colors = this.headerService.getColors();

	}

	private LoadFilterSettings() {

		this.Filters = {};
		this.ModalRefs = { newFolderModal: "" };

		this.Filters.type = this.NVals(this.sessionService.GetItem("huddle_type"), ["0", "1", "2", "3"], "0");
		this.Filters.layout = this.NVals(this.sessionService.GetItem("huddle_layout"), ["0", "1"], "0");
		this.Filters.sort_by = this.NVals(this.sessionService.GetItem("huddle_sort"), ["0", "1", "2", "3"], "3");
	}


	private GetParams() {

		this.ARouter.params.subscribe((p) => {

			if (p.folder_id) {
				this.queryParamsObj.folderId = p.folder_id;
				this.initVars();
				this.params = p;
				this.LoadNextPage(p.folder_id);
				let obj: any = {
					folder_id: p.folder_id
				};
				let sessionData: any = this.headerService.getStaticHeaderData();
				({
					User: {
						id: obj.user_id
					},
					accounts: {
						account_id: obj.account_id
					}
				} = sessionData.user_current_account);

				let ref = this.homeService.GetBreadcrumbs(obj).subscribe((data: any) => {

					if (void 0 != data.success && data.success == -1) {

						setTimeout(() => {
							this.fullRouter.navigate(['/list']);
						}, 2000);

						return;

					}

					this.homeService.updateBreadcrumb(data)

				});
			} else {
				this.LoadNextPage(false);
				this.homeService.updateBreadcrumb([])

			}

			this.params = p;

		});

	}








	private getPercentage(n, what) {

		return (what / 100) * n;

	}

	private NVals(src, possible_values, default_value) {

		return (possible_values.indexOf(src) !== -1) ? src : default_value;

	}

	public OnHuddleClick(huddle) {

		let str = '';
		if (huddle.huddle_type == 'Assessment') {
			str = "/video_huddles/assessment/" + huddle.huddle_id + '/huddle/details';
		} else {
			str = "/video_huddles/huddle/details/" + huddle.huddle_id;
		}
		this.fullRouter.navigate([str]);
	}

	private DealWithFakeHuddles(flag) {

		if (!this.HuddlesAndFolders || !this.HuddlesAndFolders.huddles) return;

		for (let i = 0; i < 10; i++) {

			if (!flag) {
				this.HuddlesAndFolders.huddles.push({ is_fake: true });
			} else {
				this.HuddlesAndFolders.huddles = this.HuddlesAndFolders.huddles.filter((h) => {
					return !h.is_fake;
				});
			}


		}

	}

	public search() {

		this.initVars(true, true);

		this.LoadNextPage(this.params.folder_id, false);

	}

	private ReloadCurrentPage() {

		this.LoadNextPage(this.params.folder_id, false, true);

	}
	public LoadNextPage(folder_id?, scroll?, reload_current_page?, fromTab?) {

		

		if (!reload_current_page && !scroll)
			this.Loadings.is_loading_huddles = true;

		let sessionData: any = this.headerService.getStaticHeaderData();

		let obj: any = {

			account_id: sessionData.user_current_account.accounts.account_id,
			user_id: sessionData.user_current_account.User.id,
			role_id: sessionData.user_current_account.roles.role_id,
			page: reload_current_page ? this.page <= 1 ? this.page : this.page - 1 : this.page,
			user_current_account: sessionData.user_current_account

		}

		obj.huddle_type = this.NVals(this.sessionService.GetItem("huddle_type"), ["0", "1", "2", "3"], "0");
		this.objHuddleType = obj.huddle_type;
		obj.huddle_sort = this.NVals(this.sessionService.GetItem("huddle_sort"), ["0", "1", "2", "3"], "3");


		if (this.SearchString) {
			obj.title = this.SearchString;
		}

		if (folder_id) {
			obj.folder_id = folder_id;
		} else {
			obj.folder_id = "";
		}

		this.Loadings.isNextPageLoading = true;
		this.DealWithFakeHuddles(false);
		let ref = this.homeService.GetHuddles(obj).subscribe((data: any) => {
			this.showSkeleton = false;
			this.Loadings.is_loading_huddles = false;
			if (data.success) {
				if (fromTab)
					this.HuddlesAndFolders = [];
				if (!this.HuddlesAndFolders || !this.HuddlesAndFolders.huddles) {
					this.HuddlesAndFolders = data;
					// this.ToggleFoldersExpand(true);
				} else {
					this.HuddlesAndFolders.huddles = this.HuddlesAndFolders.huddles.concat(data.huddles);
				}

				if (reload_current_page) {

					this.HuddlesAndFolders.huddles = _.uniq(this.HuddlesAndFolders.huddles, true, h => h.huddle_id);
					this.HuddlesAndFolders.total_huddles = data.total_huddles;
				} else {

					this.page++;

				}

				this.PipeTrigger = !this.PipeTrigger;
				this.DealWithFakeHuddles(true);
			} else {
				this.toastr.ShowToastr('info',data.message);
			}
			setTimeout(() => {

				this.Loadings.isNextPageLoading = false;

			}, 100);










			ref.unsubscribe();
		});
		this.viewerObject = obj
		console.log('obj',this.viewerObject);
	}

	public OnNameTextChange(e, mode) {

		if (e.keyCode == 13) {

			if (mode == "edit") {
				this.EditFolder();
			} else if (mode == "create") {
				this.CreateFolder();
			}

		}

	}

	public OnHuddleEdit(event) {
		let str = '';

		if (event.type === 'assessment')
			str = "/add_huddle_angular/assessment/edit/" + event.huddle_id;
		else
			str = "/add_huddle_angular/edit/" + event.huddle_id;

		this.fullRouter.navigate([str])

	}

	public CreateFolder() {

		if (!this.Inputs.NewFolderName || !this.Inputs.NewFolderName.trim()) {
			this.toastr.ShowToastr('info',this.translation.huddle_please_provide_folder_name);
			return;
		}

		let sessionData: any = this.headerService.getStaticHeaderData();

		let obj: any = {
			folder_name: this.Inputs.NewFolderName,
			account_id: sessionData.user_current_account.accounts.account_id,
			user_id: sessionData.user_current_account.User.id
		}

		if (this.params.folder_id) {
			obj.folder_id = this.params.folder_id;
		}

		this.HideModal("newFolderModal");
		this.homeService.CreateFolder(obj).subscribe((data: any) => {
			if (data.success) {
				this.toastr.ShowToastr('info',this.translation.huddle_folder_created_successfully);
				data.folder_object.title = data.folder_object.name;
				data.folder_object.folder_permissions = true;
				let currentSelectedSort = this.NVals(this.sessionService.GetItem("huddle_sort"), ["0", "1", "2", "3"], "3");
				if (currentSelectedSort == 3 || currentSelectedSort == 2) {
					this.HuddlesAndFolders.folders.unshift(data.folder_object);
				} else {
					this.HuddlesAndFolders.folders.push(data.folder_object);
					this.sortFoldersByKey(currentSelectedSort);
				}

				this.PipeTrigger = !this.PipeTrigger;
			} else {
				this.toastr.ShowToastr('info',data.message);
			}


		});
	}

	public DigestFilters(key, value, force_block?) {

		this.sessionService.SetItem(key, value);
		if (key != "huddle_layout" && !force_block) {
			this.initVars(true, true);
			this.LoadNextPage(this.params.folder_id, false, null, true);
		}


	}

	public onDragged(item: any, list: any) {
	}


	public onDrop(event: DndDropEvent, target: any) {




		if (this.Loadings.MovingItem) {
			return;
		}
		if (event.data.huddle_id && event.data.huddle_id >= 0) {

			this.Loadings.MovingItem = true;


			let ref = this.homeService.Move({ folder_id: target.folder_id, huddle_id: event.data.huddle_id }).subscribe((data: any) => {
				this.Loadings.MovingItem = false;

				if (data.status) {
					target.stats[event.data.type]++;

					this.toastr.ShowToastr('info',this.translation.huddle_moved_successfully);

					this.HuddlesAndFolders.huddles = this.HuddlesAndFolders.huddles.filter((huddle) => {

						return huddle.huddle_id != event.data.huddle_id;

					});

					this.HuddlesAndFolders.total_huddles--;
				} else {
					this.toastr.ShowToastr('info',data.message);
				}



				ref.unsubscribe();

			});





		} else if (event.data.folder_id && event.data.folder_id >= 0) {

			if (target.folder_id == event.data.folder_id) {
				return;
			}

			this.Loadings.MovingItem = true;
			let ref = this.homeService.Move({ folder_id: target.folder_id, huddle_id: event.data.folder_id }).subscribe((data: any) => {
				this.Loadings.MovingItem = false;
				if (data.success) {

					this.toastr.ShowToastr('info',this.translation.huddle_folder_moved_successfully);
					target.stats.folders++;
					this.HuddlesAndFolders.folders = this.HuddlesAndFolders.folders.filter((f) => {

						return f.folder_id != event.data.folder_id;

					});
				} else {
					this.toastr.ShowToastr('info',data.message);
				}



				ref.unsubscribe();

			});





		}

	}

	changeBackground(huddle,event){

		event.preventDefault();

		let catchEvent = event.dataTransfer.effectAllowed;

		//console.log('huddleevent',event.dataTransfer.effectAllowed);
  
		if(event._dndDropzoneActive === true && catchEvent == 'all' && huddle.meta_data_value != 3 ){
		
			this.changeBackgroundColor = true;
		
			this.dragHuddleId = huddle.huddle_id;
		
		}
  	
	}
  
	changeDefaultBackground(event){
  
	  event.preventDefault();
		
		if(!event._dndDropzoneActive){
		  this.changeBackgroundColor = false;
		}
	}

	uploadFiles(event, huddle){
		//debugger;
		console.log('huddle',huddle);
		//return;
		this.changeBackgroundColor = false;
		if(huddle.meta_data_value == 3){
			return false;
		}
		
	   if(this.userAccountLevelRoleId != 125 && ((this.userAccountLevelRoleId == 120 && this.		header_data.user_permissions.UserAccount.permission_video_workspace_upload == 1) || this.userAccountLevelRoleId != 120)){
	
	   		//console.log('permission access');
	
	   	} else {
		this.toastr.ShowToastr('error', 'You do not have permissions to upload the files');
		return false;
		}

		
		if(this.userAccountLevelRoleId !='125' && !(huddle.role_id==220 && huddle.type=='collaboration' )){
	 
		 //console.log('permission access abed');
	   	} else {
		 
		this.toastr.ShowToastr('error', 'You do not have permissions to upload the files');
		 return false;
	   }

		var files;
		event.preventDefault();
		var data = event.dataTransfer.items;
	   	files = data;
	   	let that = this;
		let obj: any = {};
		obj.user_current_account = that.header_data.user_current_account;
		obj.account_folder_id = null;
		obj.huddle_id = null;
		// filestack handle key added
		// filestack url added
		obj.account_id = that.header_data.user_current_account.accounts.account_id;
		obj.site_id = that.header_data.site_id;
		obj.user_id = that.header_data.user_current_account.User.id;
		obj.current_user_role_id = that.header_data.user_current_account.roles.role_id;
		obj.current_user_email = that.header_data.user_current_account.User.email;
		obj.suppress_render = false;
		obj.suppress_success_email = false;
		//obj.workspace = this.pageType=='workspace-page'?true : false;
		obj.activity_log_id = "";
		obj.video_id = "";
		obj.direct_publish = true;
		obj.url_stack_check = 1;
		obj.parent_folder_id = null;

		obj.account_folder_id = huddle.account_folder_id;
		obj.huddle_id = huddle.account_folder_id;
		//console.log('homeobjecthuddle',obj);
	   	//console.log('files from huddle home page',files);
	
	   // return;
		this.headerService.UpdateUploadFilesList({files, obj})
	}

	public getImgSrc(user) {

		return this.homeService.getAvatarPath(user);

	}

	public OnFolderClick(folder_id, tree) {

	}

	public getMovePath(tree) {

		if (!tree || !tree.treeModel || !tree.treeModel.getActiveNode() || !tree.treeModel.getActiveNode().data) {
			return;
		}
		let head = tree.treeModel.getActiveNode();

		if (tree.treeModel.getActiveNode().id == -1) return [tree.treeModel.getActiveNode().data];

		let arr = [];



		while (head.parent != null) {

			if (head.data) {

				arr.push(head.data);

			}
			else if (head.treeModel.getActiveNode()) {

				arr.push(head.treeModel.getActiveNode().data);

			}

			head = head.parent;
		}

		if (arr.length == 0) return [tree.treeModel.getActiveNode().data];

		return arr.reverse();

	}



	public MoveItem(tree) {

		if (!tree.treeModel.getActiveNode()) {

			this.toastr.ShowToastr('info',this.translation.huddle_no_traget_folder);

		}

		if (!(tree.treeModel.getActiveNode() && this.MovableItem.id)) return;

		if (this.Loadings.MovingItem) {
			return;
		}

		let target = tree.treeModel.getActiveNode().data;

		let obj = {
			folder_id: target.id,
			huddle_id: this.MovableItem.id
		}

		if (this.MovableItem.isHuddle) {

			this.Loadings.MovingItem = true;
			this.homeService.Move(obj).subscribe((d: any) => {

				this.Loadings.MovingItem = false;
				if (d.success) {

					this.toastr.ShowToastr('info',this.translation.huddle_moved_successfully);

					this.HuddlesAndFolders.huddles = this.HuddlesAndFolders.huddles.filter((h) => {

						return h.huddle_id != this.MovableItem.id;

					});

					this.HuddlesAndFolders.total_huddles--;

					let index = _.findIndex(this.HuddlesAndFolders.folders, { folder_id: target.id });

					if (index > -1) {

						this.HuddlesAndFolders.folders[index].stats[this.MovableItem.type]++;

					}

					this.ReloadCurrentPage();

				} else {
					this.toastr.ShowToastr('info',d.message);
				}

			});



			this.HideModal("TreeViewDialog");

		} else {

			this.Loadings.MovingItem = true;
			this.homeService.Move(obj).subscribe((d: any) => {
				this.Loadings.MovingItem = false;
				if (d.success) {
					this.toastr.ShowToastr('info',this.translation.huddle_folder_moved_successfully);
					this.HuddlesAndFolders.folders.forEach((f, i) => {
						if (f.folder_id == target.id) {

							f.stats[this.MovableItem.type]++;

						}
						if (f.folder_id == this.MovableItem.id) {
							this.HuddlesAndFolders.folders.splice(i, 1);
						}
					})
				} else {
					this.toastr.ShowToastr('info',d.message);
				}

				this.PipeTrigger = !this.PipeTrigger;

			});



			this.HideModal("TreeViewDialog");

		}


	}

	public OnFolderMove(template: TemplateRef<any>, folder_id, isHuddle?, hType?) {

		if (!folder_id) return;

		this.MovableItem = {
			id: folder_id,
			isHuddle: Boolean(isHuddle),
			type: hType
		};

		let sessionData: any = this.headerService.getStaticHeaderData();

		let obj: any = {

			account_id: sessionData.user_current_account.accounts.account_id,
			user_id: sessionData.user_current_account.User.id,
			id: isHuddle ? "" : folder_id

		};

		let ref = this.homeService.GetFolderList(obj).subscribe((data) => {

			this.FoldersTreeData = this.list_to_tree(data, 'parent');
			this.ModalRefs.TreeViewDialog = this.modalService.show(template);
			ref.unsubscribe();

		});


	}

	private list_to_tree(list, parentProp) {
		var map = {}, node, roots = [], i;
		for (i = 0; i < list.length; i += 1) {
			map[list[i].id] = i;
			list[i].children = [];
		}
		for (i = 0; i < list.length; i += 1) {
			node = list[i];
			node.name = node.text;
			if (node[parentProp] !== "#") {
				if (list[map[node[parentProp]]]) {
					list[map[node[parentProp]]].children.push(node);
				} else {
				}

			} else {
				roots.push(node);
			}
		}

		if (!this.queryParamsObj.folderId) {
			roots = roots.filter(function (obj) {
				return obj.id >= 0;
			});
		} else {
			roots = roots.filter((obj) => {
				if (this.queryParamsObj.folderId == obj.id && obj.children.length <= 0) {
					return false;
				} else {
					return true
				}
			});
		}

		return roots;
	}

	public ToggleFoldersExpand(flag) {
		this.foldersExpanded = flag;
		localStorage.setItem('foldersExpanded', flag);
	}

	public ShowNewFolderModal(template: TemplateRef<any>) {

		this.Inputs.NewFolderName = "";
		this.ModalRefs.newFolderModal = this.modalService.show(template);

	}

	public ShowParticipentsModal(template: TemplateRef<any>, class_name) {

		this.ModalRefs.participentsModal = this.modalService.show(template, {
			class: class_name
		});

	}

	public OnHuddleParticipentsClick(huddle_id) {

		let huddle = _.findWhere(this.HuddlesAndFolders.huddles, { huddle_id: huddle_id });

		if (!huddle) {
			this.toastr.ShowToastr('info',this.translation.something_went_wrong_msg);
			return;
		}

		this.DetailedHuddle = huddle;
		this.mainCreator_Huddle = this.DetailedHuddle.created_by;
		this.ShowParticipentsModal(this.participentsDetailsModal, "lg_popup");
		if (huddle.type == "collaboration") {
			  //sorting by role
			  this.DetailedHuddle.participants.collaboration_participants = _.sortBy(this.DetailedHuddle.participants.collaboration_participants, 'role_name');
			  let creator = this.DetailedHuddle.participants.collaboration_participants.find(p => p.user_name == this.mainCreator_Huddle);
			  let index = this.DetailedHuddle.participants.collaboration_participants.indexOf(creator);
			  if(index >= 0) {
				this.DetailedHuddle.participants.collaboration_participants.splice(index,1);
				this.DetailedHuddle.participants.collaboration_participants.unshift(creator);
			  }
 
		}


	}

	public HideModal(which) {

		if (which == 'newFolderModal') {
			this.ModalRefs.newFolderModal.hide();
			this.Inputs.NewFolderName = "";
		} else {

			this.ModalRefs[which].hide();
		}

	}


	public OnFolderEdit(folder_id, template: TemplateRef<any>) {

		let folder = _.findWhere(this.HuddlesAndFolders.folders, { folder_id: folder_id });

		if (folder && _.isObject(folder)) {
			if (template)
				this.EditableFolder = JSON.parse(JSON.stringify(folder));
			this.ModalRefs.RenameFolderModal = this.modalService.show(template);
		}


	}

	public OnFolderDelete(template: TemplateRef<any>, folder, isFolder?) {

		this.Inputs.Confirmation = "";
		this.DeletableItem = folder;
		this.DeletableItem.isFolder = isFolder;
		this.ModalRefs.confirmationDialog = this.modalService.show(template);

	}


	public ConfirmDelete() {
		;
		if (this.Inputs.ConfirmationKey != this.Inputs.Confirmation) {

			this.toastr.ShowToastr('info',this.translation.huddle_you_typed_in + " '" + this.Inputs.ConfirmationKey + "' ");
			return;
		}

		let obj: any = {};

		let sessionData: any = this.headerService.getStaticHeaderData();
		obj.user_id = sessionData.user_current_account.User.id;

		if (this.DeletableItem.isFolder) {

			obj.folder_id = this.DeletableItem.folder_id;

			this.homeService.DeleteItem(obj, true).subscribe((data: any) => {

				this.HideModal("confirmationDialog");

				if (data.success) {
					this.toastr.ShowToastr('info',data.message);
					this.HuddlesAndFolders.folders = this.HuddlesAndFolders.folders.filter((f) => {

						return this.DeletableItem.folder_id != f.folder_id;

					});
				} else {
					this.toastr.ShowToastr('info',data.message);
				}

			});

		} else {
			obj.huddle_id = this.DeletableItem.huddle_id;
			this.homeService.DeleteItem(obj, false).subscribe((d: any) => {

				this.HideModal("confirmationDialog");

				if (d.success) {
					this.toastr.ShowToastr('info',d.message);
					this.HuddlesAndFolders.huddles = this.HuddlesAndFolders.huddles.filter((h) => {

						return this.DeletableItem.huddle_id != h.huddle_id;

					});
					this.ReloadCurrentPage();
				} else {
					this.toastr.ShowToastr('info',d.message);
				}

			});
		}
	}

	public EditFolder() {


		if (!this.EditableFolder.title.trim()) {
			this.toastr.ShowToastr('info',this.translation.huddle_please_enter_folder_name);
			return;
		}
		if (this.EditableFolder && _.isObject(this.EditableFolder)) {
			this.HideModal('RenameFolderModal');

			let sessionData: any = this.headerService.getStaticHeaderData();

			let obj: any = {

				folder_id: this.EditableFolder.folder_id,
				account_id: sessionData.user_current_account.accounts.account_id,
				folder_name: this.EditableFolder.title,
				user_id: sessionData.user_current_account.User.id

			};

			if (this.params.folder_id) {
				obj.parent_account_folder_id = this.params.folder_id;
			}

			let ref = this.homeService.EditFolder(obj).subscribe((data: any) => {

				if (data.success) {
					this.toastr.ShowToastr('info',data.message);

					_.each(this.HuddlesAndFolders.folders, (f) => {

						if (f.folder_id == this.EditableFolder.folder_id) {


							f.title = this.EditableFolder.title;
							f.last_edit_date = data.last_edit_date;
							this.EditableFolder = {};

							this.PipeTrigger = !this.PipeTrigger;
						}

					});

					let currentSelectedSort = this.NVals(this.sessionService.GetItem("huddle_sort"), ["0", "1", "2", "3"], "3");
					this.sortFoldersByKey(currentSelectedSort);

				} else {
					if (data.message)
						this.toastr.ShowToastr('info',data.message);
					else
						this.toastr.ShowToastr('info',this.translation.something_went_wrong_msg);
				}

			});



		}

	}

	get_add_huddle_link() {

		let folder_id = this.params.folder_id;

		let link = "/add_huddle_angular/home";

		if (typeof folder_id != 'undefined') {

			link += `?folderId=${folder_id}`;

		}

		return link;
	}



	public TriggerTextChange(ev) {

		if (ev.keyCode == 13) {
			this.ConfirmDelete()

		}
	}

	public Assessment_huddle_permissions() {
		let data: any = this.headerService.getStaticHeaderData();
		this.assessment_permissions = data.user_permissions.UserAccount.manage_evaluation_huddles == 1;
		return this.assessment_permissions;
	}

	private sortFoldersByKey(key: number) {
		if (key == 3 || key == 1 || key == 0) {
			this.HuddlesAndFolders.folders.sort((firstIndex, secondIndex) => {
				if (key == 3) {
					let fIndexDate: any = new Date(firstIndex['last_edit_date']);
					let sIndexDate: any = new Date(secondIndex['last_edit_date']);
					return sIndexDate - fIndexDate;
				} else if (key == 1) {
					let firstIndexCreatedByName = firstIndex['created_by_name'].toLowerCase();
					let secondIndexCreatedByName = secondIndex['created_by_name'].toLowerCase();
					if (firstIndexCreatedByName < secondIndexCreatedByName) return -1;
					if (firstIndexCreatedByName > secondIndexCreatedByName) return 1;
					return 0;
				} else if (key == 0) {
					let firstIndexTitle = firstIndex['title'].toLowerCase();
					let secondIndexTitle = secondIndex['title'].toLowerCase();
					if (firstIndexTitle < secondIndexTitle) return -1;
					if (firstIndexTitle > secondIndexTitle) return 1;
					return 0;
				}
			});
		}
	}

	public LSfolderSetting() {
		if (localStorage.getItem('foldersExpanded') == undefined) {

			localStorage.setItem('foldersExpanded', 'true')
			this.foldersExpanded = JSON.parse(localStorage.getItem('foldersExpanded'));
		}
		else
			this.foldersExpanded = JSON.parse(localStorage.getItem('foldersExpanded'));
	}


	ngOnDestroy() {




		this.subscription.unsubscribe();
		if (this.ModalRefs && this.ModalRefs.TreeViewDialog) this.ModalRefs.TreeViewDialog.hide();
		if (this.ModalRefs && this.ModalRefs.newFolderModal) this.ModalRefs.newFolderModal.hide();
		if (this.ModalRefs && this.ModalRefs.participentsModal) this.ModalRefs.participentsModal.hide();
		if (this.ModalRefs && this.ModalRefs.RenameFolderModal) this.ModalRefs.RenameFolderModal.hide();
		if (this.ModalRefs && this.ModalRefs.confirmationDialog) this.ModalRefs.confirmationDialog.hide();

	}



}

interface filters {
	[key: string]: any
}

interface Modals {
	[key: string]: any
}

interface GeneralInputs {
	NewFolderName: string,
	Confirmation: string,
	ConfirmationKey: string
}

interface params {
	folder_id?: string,
	account_id: string,
	user_id: string,
	role_id: string,
	page: string,
	user_current_account: string
}