import { Component, OnInit, ViewChild, TemplateRef, EventEmitter, OnDestroy, HostListener } from "@angular/core";
import { BsModalService } from "ngx-bootstrap/modal";
import { ActivatedRoute, Router } from "@angular/router";
import { DetailsHttpService } from "../servic/details-http.service";
import { ShowToasterService } from '@projectModules/app/services';
import { HeaderService, HomeService, AppMainService } from "@projectModules/app/services";
import { Subject, Subscription, combineLatest } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { DiscussionService } from "../servic/discussion.service";
import IUpload from "./IUpload";
import { environment } from "@environments/environment";
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import * as moment from 'moment';
import * as _ from 'lodash';

type BrowserAgent = 'opera' | 'firefox' | 'safari' | 'ie' | 'edge' | 'chrome' | 'chromium';
declare var safari: any;

@Component({
  selector: "discussions",
  templateUrl: "./discussions.component.html",
  styleUrls: ["./discussions.component.css"]
})
export class DiscussionsComponent implements OnInit, OnDestroy, IUpload {
  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
      if (this.addDiscData.title.length >0 || this.addDiscData.comment.length> 0) {
          $event.returnValue =false;
      }
  }

  @ViewChild("addDiscussion", { static: false }) addDiscussion;
  @ViewChild("delete", { static: false }) delete;
  @ViewChild("popupModal", { static: false }) popModal;
  @ViewChild("activity", { static: true }) activity;
  @ViewChild("participantsModal", { static: false }) participantsModal;
  @ViewChild("uploader", { static: false }) uploader;
  private searchInput: Subject<string> = new Subject();
  public ModalRefs: Modals;
  SearchString: any = "";
  rootPath = environment.baseUrl;
  public editoroptions: Object = {
    heightMin: 200,
    pluginsEnabled: [
      "fontFamily",
      "paragraphFormat",
      "paragraphStyle",
      "link",
      "colors",
      "file",
      "lists"
    ],
    toolbarButtons: [
      "fontFamily",
      "|",
      "bold",
      "italic",
      "underline",
      "color",
      "|",
      "insertLink",
      "insertFile",
      "|",
      "formatOL",
      "formatUL"
    ]
  };
  dicussions: any = [];
  selectedDiscussion: any = {};
  confirmDeleteString: string = "";
  huddle_id: number = 0;
  addDiscData = {
    title: "",
    comment: ""
  };
  editDiscData:any = {};
  Loadings: any = {};
  page: number = 0;
  actvities: any = [];
  participents: any = [];
  sort: any = "";
  huddle_name;
  public editorOptions;
  public DiscussionFiles: any = [];
  public layout: string = "";
  popupIsLoading: boolean;
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  isthisDetail: any = false;
  public translation: any = {};
  public translationLoaded: boolean = false;
  private subscription: Subscription;
  public header_data;
  public userAccountLevelRoleId: number | string = null;
  public addBtnPermissionLoaded: boolean = false;
  public unsavedDiscussionId = -1;
  public addDiscussionTAArray= [];
  public cirqLiveData:any;
  viewerObj: any = {};
  private DISCUSSION_LS_KEYS = GLOBAL_CONSTANTS.LOCAL_STORAGE.DISCUSSION;
  private addDiscussionLSKey: string = '';
  private addDiscussionTAKey: string = '';
  thumb_image: any;
  public sessionData:any;
  private goalEvidence: boolean = false;
  public cirqPath:any;
  public safari: boolean = false;

  public browserAgent: BrowserAgent;
  public docFolderId: string;

  constructor(
    public appMainService:AppMainService,
    public toastr: ShowToasterService,
    private modalService: BsModalService,
    private ARouter: ActivatedRoute,
    public detailService: DetailsHttpService,
    public headerService: HeaderService,
    private fullRouter: Router,
    private homeService: HomeService,
    public windowRef: Window,
    private discussionService: DiscussionService) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.translationLoaded = true;
    });
  }
  ngOnInit() {
    this.getBrowserAgent();
    this.sessionData = this.headerService.getStaticHeaderData();

    combineLatest( this.ARouter.params, this.ARouter.queryParams, (params: any, queryParams: any) => {
      return { params, queryParams }}).subscribe(combined => {
        this.goalEvidence = combined.queryParams.goalEvidence;
        this.docFolderId = combined.queryParams.folderId;
        this.huddle_id = +combined.params.id;
        this.getArfifactsForTemp(this.huddle_id);
        this.detailService.SetParams(combined.params);
        this.initEditorOptions();
        this.GetCirqLiveData();
  
        this.addDiscussionLSKey = `${this.DISCUSSION_LS_KEYS.ADD}${this.huddle_id}_${this.headerService.getUserId()}`;
        this.addDiscussionTAKey = `${this.DISCUSSION_LS_KEYS.ADD_TA}${this.huddle_id}_${this.headerService.getUserId()}`;
    });

      
    this.detailService.discussionEmitter.subscribe((data:any)=>{
      let formData = this.discussionService.ToFormData(data);

      for (let i = 0; i < this.DiscussionFiles.length; i++) {
        formData.append("attachment[]", this.DiscussionFiles[i],this.DiscussionFiles[i].name);
      }

      this.discussionService.AddDiscussion(formData).subscribe((d: any) => {

        this.detailService.removeUnsavedDiscussion(data);
        if(d.code == 409){
          const taComments = this.headerService.getLocalStorage(this.addDiscussionTAKey);
          if (Array.isArray(taComments)) {
            const commentIndex = taComments.findIndex(tsComment => tsComment.created_at_gmt === d.data.created_at_gmt);
            if (commentIndex > -1) {
              taComments.splice(commentIndex, 1);
              this.headerService.setLocalStorage(this.addDiscussionTAKey, taComments);
            }
          }
        }
        // this.toastr.ShowToastr('info',d.message);
        this.addDiscData = {
          title: "",
          comment: ""
        };
      }, err => {
        data.processing = false;
      }); 
    })
    
    this.discussionService.dDemit.subscribe(d => {
      this.isthisDetail = d
    })
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    }
    this.ModalRefs = {};

    if (this.activity.lenght == 0) {
    }

    this.getBreadCrumbs();
    this.header_data = this.headerService.getStaticHeaderData();
    this.userAccountLevelRoleId = this.header_data.user_permissions.roles.role_id;

    /** Restore  and set add discussion localstorage object start */
    let lsAddDiscussionObj = this.headerService.getLocalStorage(this.addDiscussionLSKey);
    if(lsAddDiscussionObj){
      this.addDiscData.title = lsAddDiscussionObj.title ? lsAddDiscussionObj.title : "";  
      this.addDiscData.comment = lsAddDiscussionObj.comment ? lsAddDiscussionObj.comment : "";  
    }
    /** Restore  and set add discussion localstorage object end */

    /** Restore  and set add discussion try again array start */
    let taAddDiscussionArray = this.headerService.getLocalStorage(this.addDiscussionTAKey);
    if(Array.isArray(taAddDiscussionArray)) this.addDiscussionTAArray = taAddDiscussionArray;
    /** Restore  and set add discussion try again array end */
    this.headerService.currentUserImage$.subscribe(data=>{
      if(!_.isEmpty(data)) this.thumb_image=data.replace(/^.*[\\\/]/, '');
    });
    
    window.onbeforeunload = () => this.ngOnDestroy(); 

  }

  private SubscribeSearch() {
    this.searchInput
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(value => {
        let url = this.fullRouter.url;

        if (this.SearchString) {
          this.fullRouter.navigate([], {
            queryParams: { search: this.SearchString },
            queryParamsHandling: "merge"
          });
        } else {
          this.fullRouter.navigate([], {
            queryParams: { search: null },
            queryParamsHandling: "merge"
          });
        }
        this.search();
      });
  }

  setQueryparams(param) {
    this.fullRouter.navigate([], {
      queryParams: { sort: param },
      queryParamsHandling: "merge"
    });
  }

  public initEditorOptions() {
    this.editorOptions = this.discussionService.GetEditorOptionsAdd();
  }

  public onUpload(e) {
    let target = e.target;
    target.files.length > 0 && this.pushToFiles(target.files);
    target.value = "";
  }

  private pushToFiles(files) {
    for (let i = 0; i < files.length; i++) {
      this.DiscussionFiles.push(this.parseFile(files[i]));
    }
  }

  public search() {
  }

  private parseFile(file: any) {
    const name = file.name ;
    return new File([file], name, { type: file.type });
  }

  public OnSearchChange(event) {
    this.searchInput.next(event);
  }

  public Onclickonparticipant() {
    this.ShowParticipentsModal(this.participantsModal, "lg_popup");
    this.detailService.GetParticipentsList(this.huddle_id);
    this.participents = [];
    this.popupIsLoading = true;
    this.detailService.getParticientslist().subscribe(d => {
      this.participents = d;
      this.popupIsLoading = false;
    });
  }

  public ShowParticipentsModal(template: TemplateRef<any>, class_name) {
    this.ModalRefs.participentsModal = this.modalService.show(template, {
      ignoreBackdropClick: true,
      class: class_name
    });
  }

  public Onclickactivity() {
    this.ShowParticipentsModal(this.activity, "lg_popup");
    this.actvities = [];
    this.popupIsLoading = true;
    this.detailService.getAcitives(this.huddle_id, Boolean(this.goalEvidence));
    this.detailService.getActivitiesList().subscribe(d => {
      this.actvities = d;
      this.popupIsLoading = false;
    });
  }

  public OpenModal(modelName) {
    this.popModal.showModal(modelName);
  }
  getDiscussions(id: number) {
    this.Loadings.isNextPageLoading = true;
    let obj = {
      huddle_id: id
    };
    this.detailService.GetDiscussions(obj).subscribe(
      data => {
        let res: any = data;
        this.dicussions = res;
        this.Loadings.isNextPageLoading = false;
      },
      error => { }
    );
  }

  private getArfifactsForTemp(huddleId: number){
    let sessionData: any = this.headerService.getStaticHeaderData();

    let {
      user_current_account: {
        accounts: { account_id },
        User: { id: user_id },
        roles: { role_id }
      }
    } = sessionData;

    let obj: any = {
      huddle_id: huddleId,
      account_id, 
      user_id, 
      role_id,
      goal_evidence: Boolean(this.goalEvidence),
      with_folders: 1
    };

    this.detailService.GetArtifacts(obj).subscribe((data: any) => {
      if (data.success) {
        if (data.isGoalViewer) this.userAccountLevelRoleId = 125;
        this.viewerObj = {
          role_id: data.role_id,
          huddle_type: data.huddle_type
        };
        this.addBtnPermissionLoaded = true;
        const huddleHeaderData = {
          id: this.huddle_id,
          name: data.huddle_info[0].name,
          huddle_type: data.huddle_type,
          role_id: data.role_id,
          isEvaluator: data.is_evaluator,
          userAccountLevelRoleId: this.userAccountLevelRoleId
        }
        this.detailService.updateHuddleHeaderDataSource(huddleHeaderData);

      } else {
        this.toastr.ShowToastr('error',data.message);
        this.fullRouter.navigate(["/list"]);
      }
    }, error => {
      this.toastr.ShowToastr('error',error.message);
      this.fullRouter.navigate(["/list"]);
    });

  }

  ConfirmDelete() {
    let obj:any = {
      discussion_id: this.selectedDiscussion.id
    };
    let sessionData: any = this.headerService.getStaticHeaderData();
    this.confirmDeleteString = 'DELETE';
    if (this.confirmDeleteString === "DELETE") {
      const params = this.detailService.GetParams();
      obj.huddle_id = params.id;
      obj.user_id = sessionData.user_current_account.User.id;
      this.appMainService.DeleteDiscussion(obj).subscribe(
        data => {
          let d: any = data;
          if (d.success) {
          } else {
            this.toastr.ShowToastr('info',d.message);
          }
          this.ModalRefs.participentsModal.hide();
          this.selectedDiscussion = {};
          this.getDiscussions(this.huddle_id);
        },
        error => {
          this.toastr.ShowToastr('error',error.message);
        }
      );
    } else
      this.toastr.ShowToastr('info',
        this.translation.discussion_please_enter_delete_to_confirm
      );
  }

  AddDiscussionSubmit() {

    this.headerService.removeLocalStorage(this.addDiscussionLSKey);

    var x = this.addDiscData.comment

    x = x.toString().replace(/<.*?>/g, "");

    if (this.addDiscData.title.trim() == "" || this.addDiscData.comment.trim() == "" || this.addDiscData.comment.trim() == null || x == "" || x.trim() == "") {
      this.toastr.ShowToastr('info',this.translation.discussion_fields_cannot_be_empty);
    } else {
      if (this.DiscussionFiles.length) {
        let result = this.headerService.checkFileSizeLimit(this.DiscussionFiles);
        if (!result.status) {
          this.toastr.ShowToastr('error',result.message);
          return;
        }
      }
      let comment_time = moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS")

      let sessionData: any = this.headerService.getStaticHeaderData(),
        obj: any = {
          huddle_id: this.huddle_id,
          user_current_account: sessionData.user_current_account,
          title: this.addDiscData.title,
          comment: this.addDiscData.comment,
          send_email: true,
          notification_user_ids: "",
          commentable_id: null,
          fake_id:moment(new Date()).format('x'),
          created_at_gmt: comment_time

        };

      ({
        User: { id: obj.user_id },
        accounts: { account_id: obj.account_id }
      } = sessionData.user_current_account);

      let formData = this.discussionService.ToFormData(obj);

      for (let i = 0; i < this.DiscussionFiles.length; i++) {
        formData.append("attachment[]", this.DiscussionFiles[i], this.DiscussionFiles[i].name);
      }

      this.addDiscData = { title: "", comment: "" };
      this.detailService.getFlag(this.prepareDummyDiscussion(obj,sessionData));

      this.appMainService.AddDiscussion(formData).subscribe((data: any) => {
      },
      (error)=>{
        obj.first_name = sessionData.user_current_account.User.first_name;
        obj.last_name = sessionData.user_current_account.User.last_name;
        obj.id= this.unsavedDiscussionId+new Date().getTime();
        obj.uuid= new Date().getTime();
        obj.tryAgain = true;
        obj.thumb_image = `${environment.baseUrl}/img/home/photo-default.png`;
        this.addDiscussionTAArray.push(obj);
        this.headerService.setLocalStorage(this.addDiscussionTAKey, this.addDiscussionTAArray)
        this.detailService.getFlag(obj); 
      });

      this.ModalRefs.participentsModal.hide();
    }
  }

  prepareDummyDiscussion(comment,sessionData){
    let obj: any = {
      huddle_id: comment,
      user_current_account: sessionData.user_current_account,
      title:comment.title,
      comment: comment.comment,
      send_email: true,
      notification_user_ids: "",
      commentable_id: null,
      fake_id:comment.fake_id,
      image: this.thumb_image,
      user_id:sessionData.user_current_account.User.id,
      thumb_image: this.thumb_image
    };

 
// debugger
    return obj;
  }

  addDiscussion_popup() {
    this.DiscussionFiles = [];
    this.ShowParticipentsModal(this.addDiscussion, "lg_popup");
  }

  editDiscussion_popup(item: any) {
  }

  deleteDiscussion_popup(item: any) {
    this.selectedDiscussion = item;
    this.ShowParticipentsModal(this.delete, "sm_popup");
  }

  public getBreadCrumbs() {
    let obj: any = {
      document_folder_id: this.docFolderId,
      folder_id: this.huddle_id,
      goal_evidence: Boolean(this.goalEvidence)
    };
    let sessionData: any = this.headerService.getStaticHeaderData();

    ({
      User: { id: obj.user_id },
      accounts: { account_id: obj.account_id }
    } = sessionData?.user_current_account);

    this.homeService.GetBreadcrumbs(obj).subscribe((data: any) => {
      if (data.success === -1) {
        this.toastr.ShowToastr('info',data.message);

        setTimeout(() => {
          this.fullRouter.navigate(['/list']);
        }, 4000);

      }
      else {


        let dataDeepCopy = JSON.parse(JSON.stringify(data));
        
        let folderName = dataDeepCopy[dataDeepCopy.length - 1]?.folder_name;
        let folderId = dataDeepCopy[dataDeepCopy.length - 1]?.folder_id;

        const folders = data.folders;
        const docFolder = data.document_folders;
        if (folders) {
        let huddleObj=folders[folders?.length - 1];
        this.huddle_name = huddleObj ? huddleObj.folder_name :  folders[0].folder_name;
        }
        
        if (data?.document_folders?.length <= 0) {
          folders.push({
            folder_name: this.translation.discussion_breacrum,
            folder_id: folderId,
            isCustom: true
          });
        } else {
          docFolder.push({
            folder_name: this.translation.discussion_breacrum,
            folder_id: folderId,
            isCustom: true
          });
        }
        const crumbs = [...folders, ...docFolder];
        data = crumbs;
        this.discussionService.SetBreadcrumbs(crumbs);
      }

      this.homeService.updateBreadcrumb(data);
    });
  }
  public exportDiscussion() {
    this.discussionService.exportDiscussionTrigger();
  }

  public cancelAddDiscussionModal(){
    if(this.addDiscData.title.length>0 || this.addDiscData.comment.length> 0){ 
      if(confirm(this.translation.you_have_unsaved_changes)){
        this.ModalRefs.participentsModal.hide();
        this.addDiscData = { title: "", comment: "" };
        this.headerService.removeLocalStorage(this.addDiscussionLSKey);
      }
    }
    else{
      this.ModalRefs.participentsModal.hide();
      this.addDiscData = { title: "", comment: "" };
      this.headerService.removeLocalStorage(this.addDiscussionLSKey);
    }
  }

  private saveAddDiscussionToLocalStorage (): void {
    if(this.addDiscData.title || this.addDiscData.comment){
      let obj = {title: this.addDiscData.title, comment: this.addDiscData.comment};
      this.headerService.setLocalStorage(this.addDiscussionLSKey, obj);
    }
  }
  public GetCirqLiveData(){
  this.detailService.cirqliveData$.subscribe(
    data => {
      this.cirqLiveData = data;
    });
    if (this.cirqLiveData == 'disabled' || (this.cirqLiveData && this.cirqLiveData.auth_key)) {
      this.cirqPath = `${environment.APIbaseUrl}/get_cirq_page_data?u_id=${this.cirqLiveData.userId}&u_f_name=${this.cirqLiveData.userFirstName}&u_l_name=${this.cirqLiveData.userLastName}&lti_email=${encodeURIComponent(this.sessionData.user_current_account.User.email)}&course_id=${this.cirqLiveData.userCourseId}&course_name=${this.cirqLiveData.userCourseName}&role=${this.cirqLiveData.userRole}&lti_sub_domain=${this.cirqLiveData.lti_sub_domain}&lti_key=${this.cirqLiveData.lti_zoom_key}&lti_secret=${this.cirqLiveData.lti_zoom_secret}`;
    }else{
      this.subscription.add(this.detailService.GetCirqLiveData().subscribe((cirQdata) => {
        this.cirqLiveData = cirQdata;
        let obj={
          huddle_id: this.huddle_id,
          account_id:this.sessionData.user_current_account.accounts.account_id, 
          user_id:this.sessionData.user_current_account.User.id, 
          role_id:this.sessionData.user_current_account.roles.role_id, 
          with_folders: 1
        }
        if (this.cirqLiveData) {
        this.detailService.GetArtifacts(obj).subscribe((data: any) => {
          let userInfoForCirq ={
            userId: data.user_info.id,
            userFirstName: data.user_info.first_name,
            userLastName: data.user_info.last_name,
            userCourseId: data.huddle_info[0].account_folder_id,
            userCourseName: encodeURIComponent(data.huddle_info[0].name),
            userRole: (data.role_id == 200 && data.users_accounts.role_id !=125) ? 'teacher' : 'student',
            id: this.huddle_id,
            name: data.huddle_info[0].name,
            huddle_type: data.huddle_type,
            role_id: data.role_id,
            isGoalViewer: data.isGoalViewer,
            isEvaluator: data.is_evaluator
          }
          this.cirqLiveData={...this.cirqLiveData,...userInfoForCirq};
          this.cirqPath = `${environment.APIbaseUrl}/get_cirq_page_data?u_id=${this.cirqLiveData.userId}&u_f_name=${this.cirqLiveData.userFirstName}&u_l_name=${this.cirqLiveData.userLastName}&lti_email=${encodeURIComponent(this.sessionData.user_current_account.User.email)}&course_id=${this.cirqLiveData.userCourseId}&course_name=${this.cirqLiveData.userCourseName}&role=${this.cirqLiveData.userRole}&lti_sub_domain=${this.cirqLiveData.lti_sub_domain}&lti_key=${this.cirqLiveData.lti_zoom_key}&lti_secret=${this.cirqLiveData.lti_zoom_secret}`;
          this.detailService.updateCirqliveData(this.cirqLiveData);// send data to service for using in other components
        });
      }else{
        this.detailService.updateCirqliveData('disabled');
      }
      }));
    }
  }

  private getBrowserAgent(): void {
  
    // Safari 3.0+ "[object HTMLElementConstructor]"
    const isSafari = /constructor/i.test(this.windowRef['HTMLElement']) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!this.windowRef['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
    if (isSafari){
      this.safari = true;
    };
  }

  timedateSpanish(timedate, index) {
    let d = timedate.split(',')
    if (index == 1) return d[0]
    if (index == 2) {
      let dd = timedate.split(' ')
      return dd[3] + ' ' + dd[4].toLowerCase();
    }
  }
  ngOnDestroy() {

    this.saveAddDiscussionToLocalStorage();

    this.subscription.unsubscribe();
    if (this.ModalRefs && this.ModalRefs.participentsModal) this.ModalRefs.participentsModal.hide();
  }
}

interface Modals {
  [key: string]: any;
}
