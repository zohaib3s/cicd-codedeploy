import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { HeaderService, HomeService } from "@projectModules/app/services";
import { ActivatedRoute, Router } from '@angular/router';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import { DetailsHttpService } from '../servic/details-http.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { environment } from '@src/environments/environment';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: "conferencing",
  templateUrl: "./conferencing.component.html",
  styleUrls: ["./conferencing.component.css"]
})
export class ConferencingComponent implements OnInit {
  public huddle_name;
  public huddle_id;
  public upCommingEvents = [];
  public translation: any = {};
  public translationLoaded: boolean = false;
  public eventsLoaded: boolean = false;
  private subscriptions: Subscription = new Subscription();
  public addBtnPermissionLoaded: boolean = false;
  participents: any = [];
  popupIsLoading: boolean;
  actvities: any = [];
  sessionData: any = [];
  public ModalRefs: Modals = {};
  public cirqLiveData: any;
  public SearchString: any = "";
  public cirqPath: any;
  public cirqDataLoaded: boolean = false;
  public goalEvidence: boolean = false;
  public userAccountLevelRoleId: number | string = null;
  public docFolderId: string;

  @ViewChild("participantsModal", { static: false }) participantsModal;
  @ViewChild("activity", { static: false }) activity;
  constructor(
    private ARouter: ActivatedRoute,
    public toastr: ShowToasterService,
    public headerService: HeaderService,
    private router: Router,
    private homeService: HomeService,
    public detailsService: DetailsHttpService,
    private modalService: BsModalService,
    public sanitizer: DomSanitizer
  ) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.translationLoaded = true;
    }));
  }
  ngOnInit() {
    this.sessionData = this.headerService.getStaticHeaderData();
    this.userAccountLevelRoleId = this.sessionData.user_permissions.roles.role_id;

    this.ARouter.params.subscribe(p => {
      this.huddle_id = +p["id"];
    });

    this.ARouter.queryParams.subscribe(p => {
      this.goalEvidence = p.goalEvidence;
      this.docFolderId = p.folderId;
    });

    this.getBreadCrumbs();
    this.addBtnPermissionLoaded = true;
    this.GetCirqLiveData();
  }

  public getBreadCrumbs() {
    let obj: any = {
      document_folder_id: this.docFolderId,
      folder_id: this.huddle_id,
      goal_evidence: Boolean(this.goalEvidence)
    };
    let sessionData: any = this.headerService.getStaticHeaderData();

    ({
      User: { id: obj.user_id },
      accounts: { account_id: obj.account_id }
    } = sessionData.user_current_account);

    this.subscriptions.add(this.homeService.GetBreadcrumbs(obj).subscribe((data: any) => {
       if (data.success === -1) {
        this.toastr.ShowToastr('info',data.message);
      }
      else {
        let dataDeepCopy = JSON.parse(JSON.stringify(data));
        let folderId = dataDeepCopy[dataDeepCopy.length - 1]?.folder_id;

        const folders = data.folders;
        const docFolder = data.document_folders;

        if (folders) {
          const huddleObj = folders[folders.length - 1];
          this.huddle_name = huddleObj ? huddleObj.folder_name :  folders[0].folder_name;
        }

        if (data?.document_folders?.length <= 0) {
          folders.push({
            folder_name: 'Conferences',
            folder_id: folderId,
            isCustom: true
          });
        } else {
          docFolder.push({
            folder_name: 'Conferences',
            folder_id: folderId,
            isCustom: true
          });
        }
        const crumbs = [...folders, ...docFolder];
        data = crumbs;
      }

      this.homeService.updateBreadcrumb(data);
    }));
  }
  /**Get Cirqlive config data for passing to getupcomingeventss */
  public GetCirqLiveData() {
    this.detailsService.cirqliveData$.subscribe(
      data => {
        this.cirqLiveData = data;
        if(data.role_id || data.isGoalViewer){
          const huddleHeaderData = {
            id: this.huddle_id,
            name: data.name,
            huddle_type: data.huddle_type,
            role_id: data.role_id,
            isEvaluator: data.isEvaluator,
            userAccountLevelRoleId: this.userAccountLevelRoleId
          }
          this.detailsService.updateHuddleHeaderDataSource(huddleHeaderData);
        }
      });

    if (this.cirqLiveData && this.cirqLiveData.auth_key) {
      this.cirqDataLoaded = true;
      let path = `${environment.APIbaseUrl}/get_cirq_page_data?u_id=${this.cirqLiveData.userId}&u_f_name=${this.cirqLiveData.userFirstName}&u_l_name=${this.cirqLiveData.userLastName}&lti_email=${encodeURIComponent(this.sessionData.user_current_account.User.email)}&course_id=${this.cirqLiveData.userCourseId}&course_name=${this.cirqLiveData.userCourseName}&role=${this.cirqLiveData.userRole}&lti_sub_domain=${this.cirqLiveData.lti_sub_domain}&lti_key=${this.cirqLiveData.lti_zoom_key}&lti_secret=${this.cirqLiveData.lti_zoom_secret}`;
      this.cirqPath = this.sanitizer.bypassSecurityTrustResourceUrl(path);
    } else if (this.cirqLiveData == 'disabled') {
      this.router.navigate(['/video_huddles/huddle/details/' + this.huddle_id + '/artifacts/grid']);
      this.toastr.ShowToastr('error','You have no access for using this service');
    } else {
      this.subscriptions.add(this.detailsService.GetCirqLiveData().subscribe((cirQdata) => {
        this.cirqLiveData = cirQdata;
        if (this.cirqLiveData) {
          let obj = {
            huddle_id: this.huddle_id,
            account_id: this.sessionData.user_current_account.accounts.account_id,
            user_id: this.sessionData.user_current_account.User.id,
            role_id: this.sessionData.user_current_account.roles.role_id,
            goal_evidence: Boolean(this.goalEvidence),
            with_folders: 1
          }
          this.subscriptions.add(this.detailsService.GetArtifacts(obj).subscribe((data: any) => {
            let userInfoForCirq = {
              userId: data.user_info.id,
              userFirstName: data.user_info.first_name,
              userLastName: data.user_info.last_name,
              userCourseId: data.huddle_info[0].account_folder_id,
              userCourseName: encodeURIComponent(data.huddle_info[0].name),
              userRole: (data.role_id == 200 && data.users_accounts.role_id != 125) ? 'teacher' : 'student',
              id: this.huddle_id,
              name: data.huddle_info[0].name,
              huddle_type: data.huddle_type,
              role_id: data.role_id,
              isGoalViewer: data.isGoalViewer,
              isEvaluator: data.is_evaluator
            }
            this.cirqLiveData = { ...this.cirqLiveData, ...userInfoForCirq };
            this.cirqDataLoaded = true;
            let path = `${environment.APIbaseUrl}/get_cirq_page_data?u_id=${this.cirqLiveData.userId}&u_f_name=${this.cirqLiveData.userFirstName}&u_l_name=${this.cirqLiveData.userLastName}&lti_email=${encodeURIComponent(this.sessionData.user_current_account.User.email)}&course_id=${this.cirqLiveData.userCourseId}&course_name=${this.cirqLiveData.userCourseName}&role=${this.cirqLiveData.userRole}&lti_sub_domain=${this.cirqLiveData.lti_sub_domain}&lti_key=${this.cirqLiveData.lti_zoom_key}&lti_secret=${this.cirqLiveData.lti_zoom_secret}`;
            this.cirqPath = this.sanitizer.bypassSecurityTrustResourceUrl(path);
            this.detailsService.updateCirqliveData(this.cirqLiveData);
          }));
        } else {
          this.router.navigate(['/video_huddles/huddle/details/' + this.huddle_id + '/artifacts/grid']);
          this.toastr.ShowToastr('error','You have no access for using this service');
        }
      }));
    }

  }


  public Onclickonparticipant() {
    this.ShowParticipentsModal(this.participantsModal, "lg_popup");
    this.detailsService.GetParticipentsList(this.huddle_id);
    this.participents = [];
    this.popupIsLoading = true;
    this.detailsService.getParticientslist().subscribe(d => {
      this.participents = d;
      this.popupIsLoading = false;
    });
  }
  public Onclickactivity() {
    this.ShowParticipentsModal(this.activity, "lg_popup");
    this.actvities = []
    this.popupIsLoading = true;
    this.getAcitives();

  }
  public ShowParticipentsModal(template: TemplateRef<any>, class_name) {
    this.ModalRefs.participentsModal = this.modalService.show(template, {
      class: class_name
    });
  }
  public GetParticipents() {
    let obj: any = {
      huddle_id: +this.huddle_id,
      user_role_id: +this.sessionData.user_current_account.roles.role_id,
      user_id: +this.sessionData.user_current_account.User.id,
    };
  }
  getAcitives() {
    let sessionData: any = this.headerService.getStaticHeaderData();
    let obj: any = {
      folder_id: this.huddle_id,
      account_id: sessionData.user_current_account.accounts.account_id,
      user_id: sessionData.user_current_account.User.id,
      role_id: sessionData.user_current_account.roles.role_id,
      goal_evidence: Boolean(this.goalEvidence)
    };

    this.subscriptions.add(this.detailsService.getActivities(obj).subscribe(
      data => {
        this.actvities = data;
        this.detailsService.setActivities(this.actvities)
        this.popupIsLoading = false;
      },
      error => { }
    ));
  }
  timedateSpanish(timedate, index) {
    let d = timedate.split(',')
    if (index == 1) return d[0]
    if (index == 2) {
      let dd = timedate.split(' ')
      return dd[3] + ' ' + dd[4].toLowerCase();
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}

interface Modals {
  [key: string]: any;
}


