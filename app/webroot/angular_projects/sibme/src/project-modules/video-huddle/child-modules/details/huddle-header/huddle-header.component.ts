import { Component, OnInit, OnDestroy, ViewChild, Input} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { HeaderService, HomeService } from '@projectModules/app/services';

import { ShowToasterService } from '@projectModules/app/services';
import {  ModalDirective } from "ngx-bootstrap/modal";
import { DetailsHttpService } from "../servic/details-http.service";
import {  Subscription } from "rxjs";
import * as _ from "underscore";

/* Socket Imports */
import io from 'socket.io-client';
import { DiscussionService } from '../servic/discussion.service';


declare global {
  interface Window { io: any; }
  interface Window { Echo: any; }
}

declare var Echo: any;

window.io = io;
window.Echo = window.Echo || {};

@Component({
  selector: "video-huddle-details-header",
  templateUrl: "./huddle-header.component.html",
  styleUrls: ["./huddle-header.component.css"]
})

export class HuddleHeaderComponent implements OnInit, OnDestroy {
  @ViewChild('deleteHuddleModal', { static: false }) deleteHuddleModal: ModalDirective;
  @ViewChild('moveHuddleModal', { static: false }) moveHuddleModal: ModalDirective;

  public huddleHeaderData: any;
  public loaded: boolean = false;

  public colors: any;
  FoldersTreeData: any = {};
  HuddlesAndFolders;
  showMomeoptions = false;
  PipeTrigger: boolean = false;
  Loadings: any = {};
  public Inputs = { NewFolderName: '', Confirmation: '', ConfirmationKey: 'DELETE' };
  DeletableItem
  MovableItem: { id: any; isHuddle: boolean; type: any; };
  public header_data: any;
  public translations: any = {};
  private subscriptions: Subscription = new Subscription();
  public currentTab: string;

  @Input() parentFolderId: string;

  constructor(
    private router: Router,
    private toastr: ShowToasterService,
    public headerService: HeaderService,
    public detailsService: DetailsHttpService,
    private discussionService: DiscussionService,
    private ARouter: ActivatedRoute,
    private homeService: HomeService,
  ) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(lt => this.translations = lt));
    this.subscriptions.add(this.detailsService.huddleHeaderData$.subscribe(data => {
      this.huddleHeaderData = data;
      console.log('Data', data)
      this.loaded = true;
    }));

  }

  ngOnInit() {
    this.ARouter.params.subscribe(console.log);

    this.ARouter.queryParams.subscribe(params => {console.log('Query', params); this.currentTab = params.tab});
    this.colors = this.headerService.getColors();
    this.header_data = this.headerService.getStaticHeaderData();
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  showExport(){
    if ((this.huddleHeaderData.isEvaluator || this.huddleHeaderData.role_id==200) && this.huddleHeaderData.userAccountLevelRoleId != '125') {
      return true;
    }else {
      return false;
    }
  }

  public OnHuddleEdit(huddle_id: number) {
    this.router.navigate(['/add_huddle_angular/edit', huddle_id]);
  }

  showDeleteHuddleModal(huddle_id): void {
    this.DeletableItem = huddle_id;
    this.deleteHuddleModal.show();
  }

  hideDeleteHuddleModal(): void {
    this.deleteHuddleModal.hide();
  }

  public ConfirmDelete() {
    if (this.Inputs.ConfirmationKey != this.Inputs.Confirmation) {

      this.toastr.ShowToastr('info',this.translations.huddle_you_typed_in + " '" + this.Inputs.ConfirmationKey + "' ");
      return;
    }

    let obj: any = {};

    obj.huddle_id = this.DeletableItem;
    let sessionData: any = this.headerService.getStaticHeaderData();
    obj.user_id = sessionData.user_current_account.User.id;
    this.homeService.DeleteItem(obj, false).subscribe((d: any) => {

      this.hideDeleteHuddleModal();

      if (d.success) {
        this.toastr.ShowToastr('info',d.message);
        if(this.parentFolderId != null){
          this.router.navigate([`/video_huddles/list/${this.parentFolderId}`]);
        }
        else{
          this.router.navigate([`/video_huddles/list/`]);
        }
        
      } else {
        this.toastr.ShowToastr('info',d.message);
      }

    });

  }
  public TriggerTextChange(ev) {

    if (ev.keyCode == 13) {
      this.ConfirmDelete()

    }
  }

  public OnFolderMove(folder_id) {
    let isHuddle = true;
    let hType = this.huddleHeaderData.huddle_type

    if (!folder_id) return;

    this.MovableItem = {
      id: folder_id,
      isHuddle: Boolean(isHuddle),
      type: hType
    };

    let sessionData: any = this.headerService.getStaticHeaderData();

    let obj: any = {

      account_id: sessionData.user_current_account.accounts.account_id,
      user_id: sessionData.user_current_account.User.id,
      id: isHuddle ? "" : folder_id

    };

    this.homeService.GetFolderList(obj).subscribe((data) => {

      this.FoldersTreeData = this.list_to_tree(data, 'parent');
      this.showMoveHuddleModal()

    });


  }


  showMoveHuddleModal(): void {
    this.showMomeoptions = true
    this.moveHuddleModal.show();
  }

  hideMoveHuddleModal(): void {
    this.showMomeoptions = false
    this.moveHuddleModal.hide();
  }

  public MoveItem(tree) {

    if (!tree.treeModel.getActiveNode()) {

      this.toastr.ShowToastr('info',this.translations.huddle_no_traget_folder);

    }

    if (!(tree.treeModel.getActiveNode() && this.MovableItem.id)) return;

    if (this.Loadings.MovingItem) {
      return;
    }

    let target = tree.treeModel.getActiveNode().data;

    let obj = {
      folder_id: target.id,
      huddle_id: this.MovableItem.id
    }

    if (this.MovableItem.isHuddle) {

      this.Loadings.MovingItem = true;
      this.homeService.Move(obj).subscribe((d: any) => {
        this.getBreadCrumbs(obj);
        this.Loadings.MovingItem = false;
        if (d.success) {

          this.toastr.ShowToastr('info',this.translations.huddle_moved_successfully);

          this.HuddlesAndFolders.huddles = this.HuddlesAndFolders.huddles.filter((h) => {

            return h.huddle_id != this.MovableItem.id;

          });

          this.HuddlesAndFolders.total_huddles--;

          let index = _.findIndex(this.HuddlesAndFolders.folders, { folder_id: target.id });

          if (index > -1) {

            this.HuddlesAndFolders.folders[index].stats[this.MovableItem.type]++;

          }


        } else {
          this.toastr.ShowToastr('info',d.message);
        }

      });


      this.hideMoveHuddleModal();


    } else {

      this.Loadings.MovingItem = true;
      this.homeService.Move(obj).subscribe((d: any) => {
        this.Loadings.MovingItem = false;
        if (d.success) {
          this.toastr.ShowToastr('info',this.translations.huddle_folder_moved_successfully);
          this.HuddlesAndFolders.folders.forEach((f, i) => {
            if (f.folder_id == target.id) {

              f.stats[this.MovableItem.type]++;

            }
            if (f.folder_id == this.MovableItem.id) {
              this.HuddlesAndFolders.folders.splice(i, 1);
            }
          })
        } else {
          this.toastr.ShowToastr('info',d.message);
        }

        this.PipeTrigger = !this.PipeTrigger;

      });



      this.hideMoveHuddleModal();

    }


  }

  public getMovePath(tree) {

    if (!tree || !tree.treeModel || !tree.treeModel.getActiveNode() || !tree.treeModel.getActiveNode().data) {
      return;
    }
    let head = tree.treeModel.getActiveNode();

    if (tree.treeModel.getActiveNode().id == -1) return [tree.treeModel.getActiveNode().data];

    let arr = [];



    while (head.parent != null) {

      if (head.data) {

        arr.push(head.data);

      }
      else if (head.treeModel.getActiveNode()) {

        arr.push(head.treeModel.getActiveNode().data);

      }

      head = head.parent;
    }

    if (arr.length == 0) return [tree.treeModel.getActiveNode().data];

    return arr.reverse();

  }

  private list_to_tree(list, parentProp) {
    var map = {}, node, roots = [], i;
    for (i = 0; i < list.length; i += 1) {
      map[list[i].id] = i;
      list[i].children = [];
    }
    for (i = 0; i < list.length; i += 1) {
      node = list[i];
      node.name = node.text;
      if (node[parentProp] !== "#") {
        if (list[map[node[parentProp]]]) {
          list[map[node[parentProp]]].children.push(node);
        } else {
        }

      } else {
        roots.push(node);
      }
    }
    return roots;
  }


  public getBreadCrumbs(data) {
    let obj: any = {
      folder_id: data.huddle_id
    };
    let sessionData: any = this.headerService.getStaticHeaderData();

    ({
      User: { id: obj.user_id },
      accounts: { account_id: obj.account_id }
    } = sessionData.user_current_account);

    this.homeService.GetBreadcrumbs(obj).subscribe((data: any) => {
      if (data.success == -1) {
        this.toastr.ShowToastr('info',data.message);
      }
      else {
        data.push({
          folder_name: this.translations.artifacts_breacrum,
          folder_id: data[0].folder_id,
          isCustom: true
        });

      }
      this.homeService.updateBreadcrumb(data)
    });
  }

  exportDiscussion(discussion: any) {
    const sessionData: any = this.headerService.getStaticHeaderData();
    const obj: any = {
      huddle_id: discussion.id,
      export_type:'word',
    };

    ({
      User: { id: obj.user_id },
      accounts: { account_id: obj.account_id }
    } = sessionData.user_current_account);

    this.discussionService.exportAllDiscussions(obj);
  }

}