import {  Component,  OnInit,  OnDestroy,  HostListener,  ViewChild,  } from "@angular/core";
import { DetailsHttpService } from "../servic/details-http.service";
import { Router, ActivatedRoute } from "@angular/router";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { BsModalService } from "ngx-bootstrap/modal";
import { Subject, Subscription } from "rxjs";
import { DiscussionService } from "../servic/discussion.service";
import { ShowToasterService } from '@projectModules/app/services';
import { HeaderService, SocketService, HomeService, AppMainService } from '@projectModules/app/services';
import * as _ from "underscore";
import io from "socket.io-client";
import { GLOBAL_CONSTANTS } from '@src/constants/constant';


declare global {
  interface Window {
    io: any;
  }
  interface Window {
    Echo: any;
  }
}

declare var Echo: any;

window.io = io;
window.Echo = window.Echo || {};

@Component({
  selector: "app-discussion-list",
  templateUrl: "./discussion-list.component.html",
  styleUrls: ["./discussion-list.component.css"]
})
export class DiscussionListComponent implements OnInit, OnDestroy {
  public page: number = 0;
  public Loadings: any = {};
  public discussions: any = [];
  public huddle_id: number = 0;
  public params: any = {};
  public total_discussions: number = -1;
  private searchInput: Subject<string> = new Subject();
  public SearchString: string = "";
  public selectedSort: number = 1;
  public editDiscData: any;
  public ModalRefs: any = {};
  public deletableDiscussion: any = {};
  public confirmDeleteString: string = "";
  private sessionData: any = [];
  public counter=0;
  public socket_listener:any;
  public header_data;
  public translation: any = {};
  private subscriptions: Subscription = new Subscription();
  private DISCUSSION_LS_KEYS = GLOBAL_CONSTANTS.LOCAL_STORAGE.DISCUSSION;
  private addDiscussionTAKey: string = '';
  private editDiscussionTAKey: string = '';
  public discssionRoles:any;
  public Inputs: GeneralInputs;
  public showConfirmationDialog: boolean = false;
  private goalEvidence: boolean = false;
  @ViewChild("editDiscussion", {static: false}) editDiscussion;
  @ViewChild("deleteTemplate", {static: false}) deleteTemplate;

  public filterOptions: any = {
    topic: 1,
    date_created: 2,
    last_modified: 3,
    created_by: 4,
    unread: 5,
    export_word: 6,
    export_pdf: 7
  };
  sort: any;
  isReverse: boolean;
  bysearch: boolean;

  @HostListener("window:scroll", ["$event"])
  onScroll(event) {
    if (this.Loadings.isNextPageLoading) {
      let doc = document.documentElement;
      let currentScroll =
        (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

      var d = document.documentElement;
      var offset = window.innerHeight + window.pageYOffset; 
      var height = d.offsetHeight;
      if (
        window.innerHeight + window.pageYOffset >=
        document.body.offsetHeight - 2
      ) {
        window.scroll(0, currentScroll - 100);
      }
    } else if (
      !this.Loadings.isNextPageLoading &&
      this.total_discussions > this.discussions.length
    ) {
      setTimeout(() => {
        let doc = document.documentElement;
        let currentScroll =
          (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);

        var d = document.documentElement;
        var offset = d.scrollTop + window.innerHeight;
        var height = d.offsetHeight;

        if (
          window.innerHeight + window.pageYOffset >=
          document.body.offsetHeight - 2
        ) {
          this.getDiscussions(this.params.id, true);
        }
      }, 100);
    } else {
    }
  }
  constructor(
    public appMainService:AppMainService,
    private detailService: DetailsHttpService,
    private fullRouter: Router,
    private ARouter: ActivatedRoute,
    private modalService: BsModalService,
    private toastr: ShowToasterService,
    private homeService: HomeService,
    private discussionService: DiscussionService,
    private headerService: HeaderService,
    private socketService: SocketService  ) {
      this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
        this.translation = languageTranslation;
        this.Inputs = { NewFolderName: "", Confirmation: "", ConfirmationKey: this.translation.Huddle_confirmation_bit };
      })
      );
    }

  ngOnInit() {
      this.sessionData = this.headerService.getStaticHeaderData();
      let discusstion_details_sort=this.headerService.getLocalStorage('discusstion_list_sort');
      if(discusstion_details_sort) this.setQueryparams(discusstion_details_sort.name, discusstion_details_sort.id);
    this.ARouter.queryParams.subscribe(params => {
      this.goalEvidence = params.goalEvidence;
      params.search
      this.SearchString = params.search
      if(this.SearchString!=undefined){
        this.bysearch = true
      }else{
        this.bysearch = false
      }
      params.sort
      ? (this.sort = params.sort)
      : (this.sort = "");
      if(this.sort=="topic"){
        this.sort = "title"
        this.isReverse = false
      }else if(this.sort=="date_created"){
        this.sort = "created_date"
        this.isReverse = true
      }else if(this.sort=="created_by"){
        this.sort = "first_name"
        this.isReverse = false
      }else if(this.sort=="last_modified"){
        this.sort = "last_edit_date"
        this.isReverse = true
      }else if(this.sort=="unread"){
        this.sort= "unread"
        this.isReverse = false
      }else{
        this.sort = "date_createde"
        this.isReverse = true
      }
    });
    let breadcrumbs = this.discussionService.GetBreadcrumbs();
    if (
      breadcrumbs &&
      breadcrumbs[breadcrumbs.length - 1] &&
      breadcrumbs[breadcrumbs.length - 1].folder_id == -1
    ) {
      breadcrumbs.splice(breadcrumbs.length - 1, 1);
					this.homeService.updateBreadcrumb(breadcrumbs)

    }

    this.SubscribeSearch();

    this.ARouter.queryParams.subscribe(params => {
      this.page = 0;
      this.SearchString = params.search ? params.search : "";
      this.selectedSort = params.sort ? this.filterOptions[params.sort] : "";
      this.params = this.detailService.GetParams();
      if (!this.selectedSort && discusstion_details_sort) {
        this.setQueryparams(discusstion_details_sort.name, discusstion_details_sort.id);
      } else {
        this.getDiscussions(this.params.id, true, true);
      }

      this.addDiscussionTAKey = `${this.DISCUSSION_LS_KEYS.ADD_TA}${this.params.id}_${this.headerService.getUserId()}`;
      this.editDiscussionTAKey = `${this.DISCUSSION_LS_KEYS.EDIT_TA}${this.params.id}_${this.headerService.getUserId()}`;

    });



    this.socketPushFunctionDiscussion();
    this.header_data = this.headerService.getStaticHeaderData();
    this.detailService.flagEmitter.subscribe((data:any)=>{
      console.log(data)
      let dummyDiscussion=this.discussions.find(d=>d.fake_id==data.fake_id);
      let index=this.discussions.indexOf(d=>d.fake_id==data.fake_id)
      if(dummyDiscussion){
        this.discussions[index]=dummyDiscussion;
      }
      else  
        this.discussions.unshift(data);
      
      
});
    this.detailService.removeUnsavedDiscussionEmitter.subscribe((d:any)=>{
      var indexofcomment =  this.discussions.findIndex(x => x.uuid == d.uuid);
      this.discussions.splice(indexofcomment,1);
      this.discussions = [...this.discussions];

      let tmpDiscussionsArray = this.headerService.getLocalStorage(this.addDiscussionTAKey);
      if(Array.isArray(tmpDiscussionsArray)){
        tmpDiscussionsArray = tmpDiscussionsArray.filter(x => x.uuid != d.uuid);
        this.headerService.setLocalStorage(this.addDiscussionTAKey, tmpDiscussionsArray);
      }

    });

    /** Replace actual discussion if edit is not working section start */
    this.detailService.lsDiscussionEditArray$.subscribe((discussion:any) => {
      let disActIndex = this.discussions.findIndex(dis => dis.id == discussion.discussion_id);
      if(disActIndex > -1){
        this.discussions[disActIndex] = discussion;
        this.discussions = [...this.discussions];
      }
    });

    /** Replace actual discussion if edit is not working section end */


  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    if(this.ModalRefs.deletableDiscussion) this.ModalRefs.deletableDiscussion.hide();

  }

  private SubscribeSearch() {
    this.searchInput
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(value => {
        let url = this.fullRouter.url;

        if (this.SearchString) {
          this.fullRouter.navigate([], {
            queryParams: { search: this.SearchString },
            queryParamsHandling: "merge"
          });
        } else {
          this.fullRouter.navigate([], {
            queryParams: { search: null },
            queryParamsHandling: "merge"
          });
        }
      });
  }

  private socketPushFunctionDiscussion() {
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(`discussions-list-${this.params.id}`).subscribe(data => this.processDiscussionEventSubscriptions(data)));
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(`discussion-comment-changes-${this.params.id}`).subscribe(data => this.processDiscussionEventSubscriptions(data)));
  }

  public OnSearchChange(event) {
    this.searchInput.next(event);
  }

  setQueryparams(param, id) {
    this.fullRouter.navigate([], {
      queryParams: { sort: param },
      queryParamsHandling: "merge"
    });
    this.selectedSort = id;
  }
  setSortLS(param,id){
    let obj={name:param, id:id}
    this.headerService.setLocalStorage('discusstion_list_sort',obj)
  }

  async getDiscussions(id: number, next?, callFromNgOnInit?: boolean) {
    
    next && this.page++;
    !next && (this.page = 1);
    if(this.total_discussions<this.discussions.length){
      this.Loadings.isNextPageLoading = true;
    }
    let sessioData: any = this.headerService.getStaticHeaderData();

    let obj = {
      huddle_id: id,
      page: this.page,
      search: this.SearchString,
      sort: Object.keys(this.filterOptions)[this.selectedSort - 1],
      user_id: sessioData.user_current_account.User.id,
      goal_evidence: Boolean(this.goalEvidence)
    };

    let data: any = await this.detailService.GetDiscussions(obj).toPromise();
   
    this.page == 1 && (this.total_discussions = data.total_discussions);

    if (this.page == 1) {
      this.discussions = data.discussions;
      this.discssionRoles=data.usersRoles;

      /** Restore add discussion try again array start */
      let addDiscussionTAArray = this.headerService.getLocalStorage(this.addDiscussionTAKey);
      if(Array.isArray(addDiscussionTAArray)){
        addDiscussionTAArray.forEach(discussion => {
          this.discussions.unshift(discussion);
        });
        this.total_discussions += addDiscussionTAArray.length;
      }
      /** Restore add discussion try again array end */

      /** Restore edit discussion try again array start */
      let tmpDiscussionEditArray = this.headerService.getLocalStorage(this.editDiscussionTAKey);
      if(Array.isArray(tmpDiscussionEditArray)){
        tmpDiscussionEditArray.forEach(tmpDis => {
          let disActIndex = this.discussions.findIndex(dis => dis.id == tmpDis.discussion_id);
          if(disActIndex > -1){
            this.discussions[disActIndex] = tmpDis;
          }
          
        });
      }
      /** Restore edit discussion try again array end */
      
     } else {
       this.discussions = [...this.discussions, ...data.discussions];
      
    }

    setTimeout(() => {
      if(!(this.total_discussions<this.discussions.length)){
         this.Loadings.isNextPageLoading = false;
      }
    }, 300);
  }


  public onDiscussionDelete(item) {
    if(item.created_by != this.sessionData.user_current_account.User.id && item.huddle_type == 2){
      this.showConfirmationDialog = true;
    } else {
      this.showConfirmationDialog = false;
    }
    this.deletableDiscussion = JSON.parse(JSON.stringify(item));
    this.confirmDeleteString = "";
    this.ModalRefs.deletableDiscussion = this.modalService.show(
      this.deleteTemplate
    );
  }

  private processDiscussionEventSubscriptions(data) {
      switch (data.event) {
        case "discussion_deleted":
          this.isAllowed();
          this.processDiscussionDeleted(data.data[0]);
          break;

        case "discussion_added":
          this.isAllowed();
          this.processDiscussionAdded(data.data[0]);
          break;

        case "discussion_edited":
          this.isAllowed();
          this.processDiscussionEdited(data.data[0]);
          break;
        case "comment_added":
                  this.isAllowed();
                  this.processCommentAdded(data,"added");
                  break;
        case "comment_deleted":
                  this.isAllowed();
                  this.processCommentAdded(data,"deleted");
                  break;

        default:
          break;
      }
  }

  private isAllowed() {}

  private processDiscussionDeleted(discussion) {
    this.discussions = _.without(
      this.discussions,
      _.findWhere(this.discussions, {
        id: discussion.id
      })
    );

    this.total_discussions--;

    if(discussion.created_by != this.sessionData.user_current_account.User.id)
    {
      this.toastr.ShowToastr('info',this.translation.discussion_discussion_deleted);
    }
  }

  private processDiscussionAdded(discussion) {
    if(discussion.parent_comment_id == 0)
    {
      let dummyDiscussion=this.discussions.find(d=>d.fake_id==discussion.fake_id);
      let index=this.discussions.findIndex(d=>d.fake_id==discussion.fake_id);
      if(index > -1){
        this.discussions.splice(index,1);
      }


      this.discussions.push(discussion);

    }

    this.total_discussions++;

    if(discussion.created_by != this.sessionData.user_current_account.User.id)
    {
        this.toastr.ShowToastr('info',this.translation.discussion_new_discussion_added);
    }
  }

  private processCommentAdded(data,event) {
      let index = -1;
      this.discussions.forEach((item, i)=>{
          item.id == data.reference_id && (index = i);
      });

      let temp_item = this.discussions[index];
      let user_id = this.sessionData.user_current_account.User.id;
      if(temp_item)
      {
          if(event == "added")
          {
              temp_item.reply_count++;
              if(user_id != data.data[0].created_by)
              {
                  temp_item.unread_comments++;
              }
          }
          else if(event == "deleted")
          {
              temp_item.reply_count--;
              if(user_id != data.data[0].created_by)
              {
                  temp_item.unread_comments--;
              }
          }
          this.discussions[index] = temp_item;
      }
  }

  private processDiscussionEdited(discussion) {
    
    let index = -1;
    this.discussions.forEach((item, i)=>{
        item.id == discussion.id && (index = i);
    });

    this.discussions[index] = discussion;
      if(discussion.created_by != this.sessionData.user_current_account.User.id)
      {
          this.toastr.ShowToastr('info',this.translation.discussion_discussion_updated);
      }
  }

  //////////////////////////

  huddleExportData(to) {
         const sessionData: any = this.headerService.getStaticHeaderData();
         const obj: any = {
           huddle_id: this.params.id,
           export_type: to
         };
     
         ({
           User: { id: obj.user_id },
           accounts: { account_id: obj.account_id }
         } = sessionData.user_current_account);
     
         this.discussionService.exportAllDiscussions(obj);
       }
  ////////////////////////

  public ConfirmDelete() {
    this.confirmDeleteString='DELETE';
    let sessionData: any = this.headerService.getStaticHeaderData();
    if (this.confirmDeleteString == "DELETE") {
      this.counter=1;
      let obj:any = {
        discussion_id: this.deletableDiscussion.id,
        original_discussion_id: this.deletableDiscussion.id,
      };

      const params = this.detailService.GetParams();
      obj.huddle_id = params.id;
      obj.user_id = sessionData.user_current_account.User.id;
        this.appMainService.DeleteDiscussion(obj).subscribe(
          data => {
          let d: any = data;
          if (d.success) {
          }
          this.deSelectDelete();
          this.deletableDiscussion = {};
        },
        error => {
          this.toastr.ShowToastr('error',error.message);
        }
      );
    } else {
      this.toastr.ShowToastr('info',this.translation.discussion_type_delete);
    }
  }
  TriggerTextChangeConfig(ev){
    if(ev.keyCode==13) {
      this.ConfirmDeleteWithConfig()
    }
  }
  public ConfirmDeleteWithConfig() {
      if (this.Inputs.ConfirmationKey != this.Inputs.Confirmation) {

			this.toastr.ShowToastr('info',this.translation.huddle_you_typed_in + " '" + this.Inputs.ConfirmationKey + "' ");
			return;
    } else {
    this.confirmDeleteString='DELETE';
    let sessionData: any = this.headerService.getStaticHeaderData();
    if (this.confirmDeleteString == "DELETE") {
      this.counter=1;
      let obj:any = {
        discussion_id: this.deletableDiscussion.id,
        original_discussion_id: this.deletableDiscussion.id,
      };

      const params = this.detailService.GetParams();
      obj.huddle_id = params.id;
      obj.user_id = sessionData.user_current_account.User.id;
        this.appMainService.DeleteDiscussion(obj).subscribe(
          data => {
          let d: any = data;
          if (d.success) {
          }
          this.deSelectDelete();
          this.deletableDiscussion = {};
        },
        error => {
          this.toastr.ShowToastr('error',error.message);
        }
      );
    } else {
      this.toastr.ShowToastr('info',this.translation.discussion_type_delete);
    }
  }
  }

  deSelectDelete(){
    this.ModalRefs.deletableDiscussion.hide();
    let that = this;
    setTimeout(function(){
      that.counter=0;
    },500);
  }

  TriggerTextChange(ev){
    if(this.counter==0){
      if(ev.keyCode==13) {
        this.ConfirmDelete()
      }
    }
    
  }

}
interface GeneralInputs {
	NewFolderName: string,
	Confirmation: string,
	ConfirmationKey: string
}
