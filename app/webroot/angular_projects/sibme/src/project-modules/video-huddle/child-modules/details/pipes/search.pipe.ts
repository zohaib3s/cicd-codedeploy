import { Pipe, PipeTransform } from "@angular/core";
import * as _ from "underscore";

@Pipe({
  name: "search",
  pure: true
})
export class SearchPipe implements PipeTransform {
  transform(items, term, keys?, sortBy?, dirty?): any {
    if (!items) return items;
    return this.filter(
      items,
      term || "",
      keys ? keys.split(",") : false,
      sortBy,
      dirty
    );
  }

  private filter(items, term, keys?, sortBy?, dirty?) {
    const toCompare = term.toLowerCase();

    let filtered_items = items.filter(function(item) {
      
      for (let property in item) {
        
        if (keys && keys.indexOf(property) < 0) {
          continue;
        }
        if (item[property] === null) {
          continue;
        }

        if (
          item[property]
            .toString()
            .replace(/<.*?>/g, "")
            .toLowerCase()
            .includes(toCompare)
        ) {
          return true;
        }
      }
      return false;
    });













    return filtered_items;
  }
}
