import { Component, OnInit, ViewChild, AfterViewInit, Input, OnChanges, SimpleChanges, EventEmitter, OnDestroy, ElementRef } from '@angular/core';
import { AssessmentService } from '../services/assessment.service';
import { BsModalService, ModalDirective, ModalOptions } from 'ngx-bootstrap/modal';
import { ShowToasterService } from '@projectModules/app/services';
import { environment } from 'src/environments/environment';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import * as _ from "underscore";
import io from 'socket.io-client';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Subject, Subscription } from 'rxjs';
import { DetailsHttpService } from '../../details/servic/details-http.service';
import { HeaderService, SocketService } from '@src/project-modules/app/services';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import * as timezone from 'moment-timezone';
import { TreeNode } from 'angular-tree-component';
import { AddEvidenceModalComponent } from '@src/project-modules/shared/modals/add-evidence-modal/add-evidence-modal.component';

declare global {
  interface Window { io: any; }
  interface Window { Echo: any; }
}

declare var Echo: any;

window.io = io;
window.Echo = window.Echo || {};
@Component({
  selector: 'asessee-detail-view',
  templateUrl: './asessee-detail-view.component.html',
  styleUrls: ['./asessee-detail-view.component.css']
})
export class AsesseeDetailViewComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {

  @ViewChild('popUp', { static: false }) popUpModel;
  videoList: any = [];
  @ViewChild('childModal', { static: false }) childModal: ModalDirective;
  @ViewChild('popup_scroll', { static: false }) popup;
  @ViewChild('searchString', { static: false }) searchString: ElementRef;
  @ViewChild('tree',{static:false}) tree:any;
  
  
  @Input('resources') resources: any;
  @Input('resourceAllowed') resource_submission_allowed: any;
  @Input('publishAll') publishAll: any;
  @Input('is_published_feedback') is_published_feedback: any;
 
  
  workspaceVideoLoading = true;
  page: any = 1;
  sessionData: any = [];
  public videoslist: any = [];
  selectedVideo: any;
  artifact: any = {};
  public socket_listener: Subscription = new Subscription();
  public socket_listener_customFields: Subscription = new Subscription();
  basePath = environment.baseUrl;
  public totalRecords;
  public callCount=0;
  public copyVideoNotes: boolean = false;

  public pageBlankView: boolean = false;

  huddleData: any = {};
  isResponseCompleted= false;
  public foldersData:any;

  public addVideoTitle: string = '';
  public removeVideoTitle: string = '';
  isDataAvailable: boolean = false;
  public artifacts: any = []
  huddle_id: any = 0;
  SearchString='';
  isSubmissionValid = true;
  selectedIndex = -1;
  is_submitted: any = true;
  isAssessorOrCreator: boolean = true;
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  assignmentVideos: any[];
  assignmentResources: any[];
  testEelement: any;
  private searchInput: Subject<string> = new Subject();
  VideoForm: boolean = false;
  public translation: any = {};
  private subscription: Subscription;
  userAccountLevelRoleId: any=0;
  params: any;
  public searchPage=1;
  public assessment_custom_fields: any = [];
  public options={
    getChildren: (node: TreeNode) => {
    
      console.log(node);
      this.getChildrenOfFolders(node)
 
    }
   }
  public showDeadlineBar = true;

  constructor(private toastr: ShowToasterService, private detailsService: DetailsHttpService,
    public headerService: HeaderService, private assessmentService: AssessmentService,
    private socketService: SocketService, private arouter:ActivatedRoute, private modalService: BsModalService,) {
      // this.getWorkSpaceVideos();

    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      if(this.translation.artifacts_add_video){
        this.addVideoTitle = this.translation.artifacts_add_video;
        this.removeVideoTitle = this.translation.vd_cancel;
      }
    });
  }
 
  ngOnInit() {
   
    this.SubscribeSearch();
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    }
    this.sessionData = this.headerService.getStaticHeaderData();
    this.userAccountLevelRoleId = this.sessionData.user_permissions.roles.role_id;

    this.isDataAvailable = false;
    this.arouter.parent.params.subscribe(params=>{
      this.params=params
      this.socketPushFunctionDiscussionDetail();
    })
    this.fetchAssessmentFields();
    

  }
  checkPermission(){
    // if(!this.publishAll ||(this.is_published_feedback && this.publishAll) ){
    //   return true;
    // } else
      if(this.publishAll && !this.is_published_feedback){
          return false;
      } else {
          return true;
      }
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.isResponseCompleted= false;
    if(changes.resource_submission_allowed)
    this.resource_submission_allowed=changes.resource_submission_allowed.currentValue;
    this.resources = changes.resources?.currentValue;
    if (this.resources.original) {
      this.artifacts = this.resources?.original.artifects.all;
      this.detailsService.setArtifactlList(this.artifacts);

      this.assignmentVideos = []
      this.assignmentResources = [];

      this.artifacts.forEach(c => {
        if (c.doc_type == 1) {
          this.assignmentVideos.push(c);

        }
        else {
          this.assignmentResources.push(c);


        }


      });
    }

    
  }

  private SubscribeSearch() {
    this.searchInput
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(value => {



        this.getWorkSpaceVideos()


      });
  }

  public fetchAssessmentFields(){
    
    let obj = {
      huddle_id: this.assessmentService.getHuddleId(),
      assessee_id: this.sessionData.user_current_account.User.id
    }
    this.assessmentService.getAssesseeAssessment(obj).subscribe((res: any) => {

      // To remove obj which don't have any data for correct index
      res.forEach(element => {
        if(element.assessment_custom_field_value != null) this.assessment_custom_fields.push(element)
      });
      
    });
  }

  onScroll(event) {
    this.testEelement
    let element = this.popup.nativeElement;
    this.testEelement = element;
    let atBottom = parseInt(element.scrollHeight) - parseInt(element.scrollTop) === element.clientHeight
    if (atBottom) {
      this.page++;
      let records = Math.ceil(this.totalRecords/12) - 1; 
      if(this.callCount<records){
        this.callCount++;this.searchPage++;
        this.getWorkSpaceVideos(true);
      }
      else{
        return;
      }

    }

  }

  public getChildrenOfFolders = (event) => {
  
    let obj = {
      title: this.SearchString,
      page: 1,
      parent_folder_id:event.data.id,
      apply_inner_search:0,
      sort: '',
      doc_type: '1',
      account_id: this.sessionData.user_current_account.users_accounts.account_id,
      user_id: this.sessionData.user_current_account.users_accounts.user_id,
      with_folders: 1
    };
    this.assessmentService.getWorkspaceVideos(obj).subscribe((data: any) => {
      console.log(data);
      let x = [];
      data.data.forEach((element) => {
        element.isVideo = true;
        x.push(element)
      });
      if(data.folders){
        data.folders.forEach((element) => {
          element.isFolder = true;
          element.hasChildren=true
           x.push(element)
         });
      }
     
      x.reverse();
      event.data.children = x;
      if(event.data.children.length>0){
       event.data.isMessage = false;
     }else{
      event.data.isMessage = true;
     }
      this.tree.treeModel.update();
 let node = this.tree.treeModel.getNodeById(event.data.id);
 if(node){
   node.expand()
   

 }
    
      
      }
    )
 
  }

  public getWorkSpaceVideos(onScrollCall: boolean = false) {
    
    if (!onScrollCall) this.workspaceVideoLoading = true;
    let obj = {
      title: this.SearchString,
      page: this.SearchString.length>0 ? this.searchPage: this.page,
      sort: '',
      doc_type: '1',
      account_id: this.sessionData.user_current_account.users_accounts.account_id,
      user_id: this.sessionData.user_current_account.users_accounts.user_id,
      with_folders: 1
    };
    this.assessmentService.getWorkspaceVideos(obj).subscribe((data: any) => {
      console.log(data);
       this.foldersData = data.folders;
       this.foldersData.forEach(element => {
         element.isFolder = true;
        element.hasChildren=true
       });

      if(data.total_records>0){
        this.totalRecords = data.total_records;
      }
      this.videoslist = data.data;
      this.videoList = [...this.videoList, ...this.videoslist];

      this.assessmentService.setvideoList(this.videoList)
      if(this.videoList.length>0)
        {
        this.isResponseCompleted = true;
       }
     else
        this.isResponseCompleted = false;

    }, error => {

    })
 
  }
  ngAfterViewInit() {

    this.assessmentService.getVideoList().subscribe(data => {
      this.videoList = data;
      this.videoList.map(video => {
        if (this.selectedVideo && this.selectedVideo.id === video.id) video.selected = true;
        else video.selected = false;
      });
      this.workspaceVideoLoading = false;

    });

    this.assessmentService.getHuddleData().subscribe((data: any) => {
      if (data) {
        this.huddleData = data;
        this.isDataAvailable = true;
      }
      this.socketPushFunctionDiscussionDetail();
      if (this.huddleData.current_user_huddle_role_id == 200 || this.sessionData.user_current_account.User.id == this.huddleData.created_by)
        this.isAssessorOrCreator = true;
      else
        this.isAssessorOrCreator = false;

    });

    this.huddleData = this.assessmentService.huddleDataLocalSinglton || [];
    var today = new Date();
    var now =timezone.utc(today).tz('America/Chicago').format("YYYY-MM-DD HH:mm:ss");
    let splitdate = this.huddleData.submission_deadline_date.split('/');
    let newdate =  splitdate[0] + '-' +splitdate[1] + '-' + splitdate[2] +" "+ this.huddleData.submission_deadline_time;
    // var submitDate = new Date(newdate);
    console.log('ASSESS before Date', this.huddleData.submission_deadline_time)

    var submittedDate = moment(newdate).format("YYYY-MM-DD HH:mm:ss")
    console.log('ASSESS Date', submittedDate)
   
    if (submittedDate < now) {
      this.isSubmissionValid = false;
    }
    setTimeout(() => {
      this.isDataAvailable = true;

    }, 2000);
    if (this.huddleData.assessees_list)
      this.is_submitted = this.huddleData.assessees_list[0].is_submitted;

  }
public lastNodeRecord = [];
  toggleSelectedVide(item: any,childe?) {
    console.log(item)
   
  if(this.lastNodeRecord.length>=1){
    // console.log(this.lastNodeRecord[0])
    this.lastNodeRecord[0].data.selected  = false;
    this.lastNodeRecord.pop()
  }
   
    if(!childe){
     
      this.selectedVideo = item;
      this.videoList.forEach(video => {
        if (video.id === this.selectedVideo.id){
          video.selected = true;
          console.log(video);
        }
        else video.selected = false;
      });
    }
  
   
    if(childe){
      
      this.childeToggleSelectedVide(item)
    }
  }

   childeToggleSelectedVide(item) {
    this.selectedVideo = item.data;
      item.data.selected = true;
      this.lastNodeRecord.push(item)
  }

  // showChildModal(index): void {
  //   this.workspaceVideoLoading=true;
  //   this.page=1;
  //   this.videoList=[];
  //   this.selectedIndex = index;
  //   this.getWorkSpaceVideos();
  //   this.childModal.show();
  // }
  public selectedVideoModal: any;
  public selectedEvdience:any;
  showChildModal(i,evidenceType): void {
        this.workspaceVideoLoading=true;
    this.page=1;
    this.videoList=[];
    this.selectedIndex = i;
    this.getWorkSpaceVideos();
    let bsModalRef;

    let type = this.getEvidenceType(evidenceType)
    let submittedEvidence = []
    //videos and resource already added -> submitted evidence

    if (type == 1) {
      this.assignmentVideos.forEach(evidence => {
        if (evidence.doc_type == 1 && evidence.source_document_id && evidence.is_dummy != 1) {
          submittedEvidence.push(evidence.source_document_id)
        }
      });
    } else {
      this.assignmentResources.forEach(evidence => {
         if ((evidence.doc_type == 2 || evidence.doc_type == 5) && evidence.source_document_id && evidence.is_dummy != 1) {
          submittedEvidence.push(evidence.source_document_id)
        }
      });
    }
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'evidance-modal-container',
      initialState: {
        evidenceType,
        item_id: 0,
        owner_id: 0,
        goal_id: 0,
        from_asessors: false,
        from_component: 'assessement',
        submitted_evidence: submittedEvidence,
        check_share_permission:true,
      }
    };
    // console.log('config.initialState: ', config.initialState);
    bsModalRef = this.modalService.show(AddEvidenceModalComponent, config)
    bsModalRef.content.onRecordedUpload.subscribe(data => {
      this.selectedVideoModal = data.selectedEvidence;
      this.copyVideoNotes = data.copyVideoNotes;
      
     
      this.toggleSelectedVide(data.selectedEvidence)
      this.selectedEvdience = data.selectedEvidence;
      this.submitForm();
      
    });
    // this.modalService.show(EvidenceModalComponent, config)
  }

  getEvidenceType(evidenceType) {
    if (evidenceType == 'video') {
      return 1
    }
    if (evidenceType == 'resource') {
      return 2
    }
  }



  hideChildModal(): void {
    this.childModal.hide();
    this.resetItems()
  }

  getSelectedArtifact(data) {
    this.artifact = data.artifact;
    if (data.modalName)
      this.popUpModel.showModal(data.modalName);
    else if (data.duplicateArtifact)
      this.popUpModel.DuplicateResource(data.artifact);

    else if (data.downloadArtifact)
      this.popUpModel.DownloadResource(data.artifact);
  }

  submitForm() {
    this.VideoForm = true;
     let selectedVideo = this.selectedEvdience;
    if (selectedVideo) {
      let formData = {
        document_id: selectedVideo.doc_id,
        account_folder_id: [this.huddleData.current_huddle_info.account_folder_id],
        current_huddle_id: selectedVideo.account_folder_id,
        account_id: this.sessionData.user_current_account.accounts.account_id,
        user_id: this.sessionData.user_current_account.User.id, copy_notes: this.copyVideoNotes,
        from_workspace: 1,
        slot_index: this.selectedIndex,
        doc_type: selectedVideo.doc_type
      };
      this.detailsService.DuplicateResource(formData).subscribe((response: any) => {
       
        this.hideChildModal();
        this.VideoForm = false
        if (response.success) {
            if(selectedVideo.doc_type == 2)
            {
                this.toastr.ShowToastr('info',this.translation.artifacts_new_resource_upload_successfully);
            }
            else
            {
                this.toastr.ShowToastr('info',this.translation.artifacts_new_video_uploaded_successfully);
            }
          this.videoList.map(video => {
            video.selected = false;
          });
          this.copyVideoNotes = false;
          this.selectedVideo = null;
        }
        else {
          this.toastr.ShowToastr('info',response.message);
        }
      }, error => {
        this.VideoForm = false        
        this.videoList.map(video => {
          video.selected = false;
        });
        this.copyVideoNotes = false;
        this.selectedVideo = null;
        this.hideChildModal();
        this.toastr.ShowToastr('info',this.translation.sorry_for_inconvenience);
      });
    } else {
    this.VideoForm = false;

      this.toastr.ShowToastr('info',this.translation.select_video_to_upload);
    }
  }


  public OnSearchChange(str) {
    this.searchPage=1;
    this.callCount=0;
    this.isResponseCompleted= false;
    this.workspaceVideoLoading = true;
    this.videoList = [];
    this.SearchString = str;
    this.searchInput.next(str);


  }

  getId(index) {
    this.selectedIndex = -1;

    this.selectedIndex = index;
    this.huddle_id = this.assessmentService.getHuddleId();


    


  }
  public onMediaUpload(event) {

    if (event.from && event.files.length) {
      let that = this;
      for (let file_key in event.files) {
        if (event.files.hasOwnProperty(file_key)) {
          let file = event.files[file_key];
          let obj: any = {};
          let sessionData: any = that.headerService.getStaticHeaderData();
          obj.user_current_account = sessionData.user_current_account;
          obj.account_folder_id = that.huddle_id;
          obj.huddle_id = that.huddle_id;
          // File Stack changes added
          obj.fileStack_handle = file.handle;
          //filestack url added
          obj.fileStack_url = file.url;
          obj.account_id = sessionData.user_current_account.accounts.account_id;
          obj.site_id = sessionData.site_id;
          obj.user_id = sessionData.user_current_account.User.id;
          obj.current_user_role_id = sessionData.user_current_account.roles.role_id;
          obj.current_user_email = sessionData.user_current_account.User.email;
          obj.suppress_render = false;
          obj.suppress_success_email = false;
          obj.workspace = false;
          obj.activity_log_id = "";
          obj.direct_publish = event.from == "Upload";
          obj.video_file_name = file.filename;
          obj.stack_url = file.url;
          obj.video_url = file.key;
          obj.video_id = "";
          obj.video_file_size = file.size;
          obj.direct_publish = true;
          obj.video_title = file.filename.split(".")[0];
          obj.url_stack_check = 1;
          obj.slot_index = this.selectedIndex


          if (event.from == "Resource") {
            this.detailsService.uploadResource(obj).subscribe((response: any) => {
              this.toastr.ShowToastr('info',this.translation.artifacts_new_resource_upload_successfully);
            });
          }
          else {
            this.detailsService.uploadVideo(obj).subscribe((data: any) => {
              this.toastr.ShowToastr('info',this.translation.artifacts_new_video_uploaded_successfully);
            });
          }

        }
      }
    }
  }

  OpenModel(artifact, modalName) {
    this.artifact = artifact;
    if (modalName === 'download')
      this.popUpModel.DownloadResource(artifact);
    else
      this.popUpModel.showModal(modalName);
  }

  goToDetailPage(artifact) {
    let url = `${this.basePath}/video_details/home/${artifact.account_folder_id}/${artifact.doc_id}?assessment=true`
    if (artifact.published == 1){
      localStorage.removeItem('assessee_detail')
      // let obj = {
      //   name: this.sessionData?.user_current_account?.User?.first_name + '' + this.sessionData?.user_current_account?.User?.last_name,
      //   path: this.getPathAndQuery()
      // }
      // localStorage.setItem('assessee_detail' ,JSON.stringify(obj));
      window.open(url.toString(), '_self');
    }
      
  }

  public openResource(artifact) {
    this.detailsService.openResource(artifact);
  }

  isAllResurcesSubmited() {
    let dummy_count = 0;
    this.artifacts = this.resources?.original.artifects.all;
    this.assignmentVideos.forEach(element => {
      if (element.is_dummy == 1)
        dummy_count++

    });
    this.assignmentResources.forEach(element => {
      if (element.is_dummy == 1)
        dummy_count++

    });


    if (dummy_count == this.assignmentResources.length + this.assignmentVideos.length)
      return true;
    else
      false

  }
  public submitAssignment() {

    let c = this.isAllResurcesSubmited()

    if (c) {
      this.toastr.ShowToastr('info',this.translation.assesse_min_video_or_resource_upload_alert)
    }

    else if (confirm(this.translation.assesse_before_submit_assessment_alert)) {
      let obj = {
        account_folder_id: this.assessmentService.getHuddleId(),
        account_id: this.sessionData.user_current_account.accounts.account_id,
        user_id: this.huddleData.assessees_list[0].user_id,
        group_id: this.huddleData.assessees_list[0].group_id,
      }
      this.assessmentService.submitAssignment(obj).subscribe((data: any) => {

        if (data.success) {
          this.toastr.ShowToastr('info',data.message)
          this.is_submitted = true
        }

      }, error => {

      })
    }
  }

  private socketPushFunctionDiscussionDetail() {
    this.socket_listener.add(this.socketService.pushEventWithNewLogic(`huddle-details-${this.params.id}`).subscribe(data => this.processEventSubscriptions(data)));
    this.socket_listener_customFields.add(this.socketService.pushEventWithNewLogic(`huddle-details-${this.params.id}-${this.sessionData.user_current_account.User.id}`).subscribe(data => this.processEventSubscriptions(data)));
    let updatedCommentsChannelName = `publish-channel-${this.params.id}`;
    this.socket_listener_customFields.add(this.socketService.pushEventWithNewLogic(updatedCommentsChannelName).subscribe(data => this.processEventSubscriptions(data)))
  }

  private processEventSubscriptions(data) {
      switch (data.event) {
        case "resource_added":
          if (this.sessionData.user_current_account.User.id == data.data[0].updated_by ) {
            let should_wait = 0;
            if (data.is_video_crop) {
              should_wait = 1;
            }
            this.processResourceAdded(data.data, should_wait);
          }
          break;

        case "resource_renamed":
          this.processResourceRenamed(data.data, data.is_dummy);
          break;

        case "resource_deleted":
          this.processResourceDeleted(data.data, data.deleted_by, data.assignment_unsubmitted);
          break;

        case "comment_deleted": 

          if (this.isAllowed(data.allowed_participants) || (typeof data.allowed_participants === 'undefined') || data.allowed_participants.length == 0 || data.allowed_participants == undefined) {
            this.huddleData.sample_data.forEach(x => {
              if(x.document_id == data.video_id){
                x.total_comments--
              }
            });
            
          }
          break;

        case "user_assessment_custom_field_updated": 
          if(data.assessee_id == this.sessionData.user_current_account.User.id) {
            this.assessment_custom_fields = [];
            // To remove obj which don't have any data for correct index
            data.data.user_assessment_custom_fields.forEach(element => {
              if(element.assessment_custom_field_value != null) this.assessment_custom_fields.push(element)
            });
          }  
          break;
        case "huddle_published":
        this.updatedCustomFields(data.data);
        break;
        default:
          break;
      }

  }
  updatedCustomFields(res){
    let huddleId =  this.assessmentService.getHuddleId();
    if(res == huddleId){
      this.is_published_feedback = this.publishAll =  true;
    }
  }

  private processResourceAdded(resource, should_wait) {
    let that = this;
    let wait_time = 0;
    if (should_wait) {
      wait_time = 10000;
      resource.published = 0;
    }
    const resourceExisted = this.artifacts.find(r => r.id == resource.id);
    if(!resourceExisted) {
      this.artifacts.push(resource);
      if (that.sessionData.user_current_account.User.id != resource.created_by) {
      }
    }

    if (resource.doc_type == 2 || resource.doc_type == 5)
      this.assignmentResources[this.selectedIndex] = resource;
    else if (resource.doc_type == 1)
      this.assignmentVideos[this.selectedIndex] = resource



  }

  private processResourceRenamed(resource, dont_show_toast = 0) {

    var indexOfMyObject = this.assignmentVideos.findIndex(x => x.id == resource.id);
    var indexOfMyObject2 = this.assignmentResources.findIndex(x => x.doc_id == resource.doc_id);
    let objResource = null;

    if (indexOfMyObject > -1) {
      objResource = this.assignmentVideos[indexOfMyObject];
      this.assignmentVideos[indexOfMyObject] = resource;
    }

    else if (indexOfMyObject2 > -1) {
      objResource = this.assignmentResources[indexOfMyObject2];
      this.assignmentResources[indexOfMyObject2] = resource;
    }

    if (objResource) {
      if (dont_show_toast == 0) {
        objResource.title.slice(0, 25)
        if (this.sessionData.user_current_account.User.id != resource.updated_by) {
          if (objResource.title.length > 25) {
            this.toastr.ShowToastr('info',`'${objResource.title}'... ${this.translation.artifacts_renamed_to} '${resource.title}'...`);
          } else {
            this.toastr.ShowToastr('info',`'${objResource.title}' ${this.translation.artifacts_renamed_to} '${resource.title}'`);
          }
        }
      }

    }
  }

  private processResourceDeleted(resource_id, deleted_by, issubmit?) {
    var indexOfMyObject = this.assignmentVideos.findIndex(x => {
      return x.doc_id == resource_id || x.id == resource_id;
    });
    let obj;
    var indexOfMyObject2 = this.assignmentResources.findIndex(x => {
      obj = x;
      return x.doc_id == resource_id || x.id == resource_id;
    });
    if (issubmit) {
      this.is_submitted = false
    }
    if (indexOfMyObject > -1) {
      this.assignmentVideos[indexOfMyObject].is_dummy = 1;
    }
    else if (indexOfMyObject2 > -1) {
      this.assignmentResources[indexOfMyObject2].is_dummy=1;
    }
    if (deleted_by != this.sessionData.user_current_account.User.id) {
    }
  }
  private isAllowed(allowed_participants = []) {
    let that = this;
    let allowed = _.find(allowed_participants, function (item) {
      return parseInt(item.user_id) == parseInt(that.sessionData.user_current_account.User.id);
    });
    if (allowed) {
      return true;
    }
  }
  public resetItems(){
    this.videoList=[];
    this.OnSearchChange('');
    this.page=1;
  }

  getPathAndQuery() {
    let strurl = window.location.href;
    let url = new URL(strurl)
    return url.pathname + url.search;
}

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.socket_listener.unsubscribe();
    this.socket_listener_customFields.unsubscribe();
  }

   public TestEvent = (event) =>{
   if(event.isExpanded){
    if(event.node.data.children){
      if(event.node.data.children.length>0){
        event.node.data.isMessage = false;
      }
      else{
        event.node.data.isMessage = true;
      }
     }else{
      event.node.data.isMessage = true;
     }
   }
   else{
    event.node.data.isMessage = false;
    //  if(event.node.data.children){
    //   if(event.node.data.children.length>0){
    //     event.node.data.isMessage = false;
    //   }
    //   else{
    //     event.node.data.isMessage = true;
    //   }
    //  }else{
    //   event.node.data.isMessage = true;
    //  }
   
   }
    console.log(event);
  }

  public openNewWindows = (item) =>{
    window.open(item.url,'_blank');

  }
}
