import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { __IMPORTS, __DECLARATIONS, __PROVIDERS, __ENTRY_COMPONENTS } from './components.barrel';
import { AppLevelSharedModule } from '../shared/shared.module';
import { GoalsRoutes } from './goals.routes';
import { DragulaModule } from 'ng2-dragula';
import { DragulaService } from 'ng2-dragula';

@NgModule({
  declarations: [__DECLARATIONS],
  imports: [__IMPORTS, GoalsRoutes, AppLevelSharedModule, DragulaModule],
  schemas:[NO_ERRORS_SCHEMA], 
  providers: [__PROVIDERS, DragulaService],
  entryComponents: [__ENTRY_COMPONENTS]
})
export class GoalsModule { }
