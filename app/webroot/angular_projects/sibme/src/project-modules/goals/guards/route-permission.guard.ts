import { Injectable } from '@angular/core';
import { Router, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HeaderService } from '@app/services';
import { GLOBAL_CONSTANTS } from '@constants/constant';

@Injectable()
export class RoutePermissionGuard implements CanActivateChild {

    constructor(private headerService: HeaderService, private router: Router) { }

    canActivateChild(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        const headerData = this.headerService.getStaticHeaderData();
        if (Object.keys(headerData).length != 0 && headerData.constructor === Object) { // if header data exists in app navigation
            return this.verifyPermission(headerData.user_current_account);
        } else { // else wait for header data and perform action
            return this.headerService.headerData$.pipe(map((headerData: any) => {
                return this.verifyPermission(headerData.user_current_account)
            }));
        }
    }

    private verifyPermission(userCurrAcc: any): boolean {
        if (Boolean(Number(userCurrAcc.accounts.enable_goals)) && userCurrAcc.roles.role_id != GLOBAL_CONSTANTS.USER_ROLES.VIEWER.ID) { // goals are enables for current account AND current user is not viewer
            return true;
        } else {
            this.router.navigate(['/profile-page']);
            return false;
        }
    }
}