import { EditGoalComponent } from './../components/edit-goal/edit-goal.component';
import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { HeaderService } from '@src/project-modules/app/services';

// TODO:delete if not using anymore
@Injectable()
export class CanDeactivateGuard implements CanDeactivate<EditGoalComponent> {
    public translation: any = {};
    
    constructor(private headerService: HeaderService) { }
    canDeactivate(component?: EditGoalComponent): Observable<boolean> | boolean {
        this.headerService.languageTranslation$.subscribe(languageTranslation => {
            this.translation = languageTranslation;
          });
         
         console.log("component.isChangeOccur: ", component.isChangeOccur);
         console.log("component.isChangOccurInChild: ", component.isChangOccurInChild);
          
        if (component.isChangeOccur || component.isChangOccurInChild) {
            if (confirm(this.translation.goals_changes_may_not_be_saved)) {
                return true;
            } else {
                return false;
            }
        } else return true;
    }
}