import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { BaseService } from '@app/services';
import { Observable } from 'rxjs';

@Injectable()
export class GoalWorkService {
  private baseUrl: string = `${environment.APIbaseUrl}/goals/`;
  constructor(private baseService: BaseService) { }


  public getEvidence(data: any, evidenceType?:any, switchOn?) {
    if(evidenceType == 'huddle' || switchOn){
    return this.baseService.__post(`${environment.APIbaseUrl}/goals/getHuddlesList`, data)
    }
    return this.baseService.__post(`${environment.APIbaseUrl}/get_workspace_artifects`, data)
  }

  public getEvidenceArtifactsData(data: any, route: string):Observable<any> {
    return this.baseService.__post(`${environment.APIbaseUrl}/${route}`, data)
  }

  public getEvidenceArtifacts(data: any, route: string) {
    
    return this.baseService.__post(`${environment.APIbaseUrl}/${route}`, data)
  }

  public getHuddleDocuments(data:any) {
    console.log(data);
    return this.baseService.__post(`${this.baseUrl}getHuddleDocuments`, data)
  }

  public saveEvidence(data: any) {
    return this.baseService.__post(`${environment.APIbaseUrl}/goals/addEvidence`, data)
  }








  public FormatSeconds(time) {
    if(time==0 || time==null) return "00:00:00";
    let sec_num: any = parseInt(time, 10);
    let hours: any = Math.floor(sec_num / 3600);
    let minutes: any = Math.floor((sec_num - hours * 3600) / 60);
    let seconds: any = sec_num - hours * 3600 - minutes * 60;

    if (hours < 10) {
      hours = "0" + hours;
    }
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    return hours + ":" + minutes + ":" + seconds;
  }
}
