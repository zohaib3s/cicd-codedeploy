import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { BaseService } from '@app/services';
import { Subject } from 'rxjs';

@Injectable()
export class GoalsService {
  private baseUrl: string = `${environment.APIbaseUrl}/goals/`;

  private frameworksSource = new Subject<any[]>();
  public frameworks$ = this.frameworksSource.asObservable();

  constructor(private baseService: BaseService) { }

  public getGoals(data: any) {
    return this.baseService.__post(`${this.baseUrl}getGoals`, data)
  }

  public getGoalStatus(data: any) {
    return this.baseService.__post(`${this.baseUrl}getGoalStatus`, data)
  }

  public addEditCategories(data: any) {
    return this.baseService.__post(`${this.baseUrl}addEditCategories`, data)
  }

  public deleteCategory(data: any) {
    return this.baseService.__post(`${this.baseUrl}deleteCategory`, data)
  }

  public getEvidenceStandards(data: any) {
    return this.baseService.__post(`${this.baseUrl}getEvidenceStandards`, data);
  }

  public addGoalItemStandards(data: any) {
    return this.baseService.__post(`${this.baseUrl}addGoalItemStandards`, data);
  }
  public archiveGoal(data: any) {
    return this.baseService.__post(`${this.baseUrl}archive_unarchive_goal`, data);
  }


  public goalCompletedItems(goalId: number, userId: number) {
    return this.baseService.__get(`${this.baseUrl}goalCompletedItems/${goalId}/${userId}`);
  }

  public updateFrameworksArray(frameworks: any[]) {
    this.frameworksSource.next(frameworks);
  }

}
