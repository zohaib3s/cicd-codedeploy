import { BehaviorSubject } from 'rxjs';
import { GroupDetailModalComponent } from './../modals/group-detail-modal/group-detail-modal.component';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import * as _ from 'lodash';
import { HeaderService } from '@src/project-modules/app/services';


@Injectable()
export class EditGoalService {
  $editGoalGlobalSubject = new Subject();
  editMode = false;
  $canBePusblishedSubject = new BehaviorSubject<boolean>(false);
  goal_id: number = null;
  headerData: any;
  bsModalRef: any;
  allUsers: any;
  constructor(private http: HttpClient, private modalService: BsModalService,
    public headerService: HeaderService) {
    this.headerData = this.headerService.getStaticHeaderData();
  }

  public GetEditorOptionsCom(customProp?) {
    let y = {
      theme: "snow",
      modules: {
        toolbar: {
          container: [
            ["bold", "italic", "underline"],

            [{ header: 1 }, { header: 2 }],
            [{ list: "ordered" }, { list: "bullet" }],
            ["link"],

            [{ color: [] }],
            ["upload-custom"]
          ],
          handlers: {
            "upload-custom": function () {
              document.getElementById("uploaderx_" + customProp).click();
            }
          }
        }
      }
    };
    return y;

  }
  public getGoalDetails(obj) {

    const path = environment.APIbaseUrl + '/goals/getGoalDetails';
    return this.http.post(path, obj);
  }
  public sortActionItems(obj:any){
    const path = environment.APIbaseUrl + '/goals/sortActionItem';
    return this.http.post(path, obj);
  }

  public GetGroupDetail(obj) {
    const path = environment.APIbaseUrl + '/get_group_details';
    return this.http.post(path, obj);

  }
  public updateGoal(obj) {
    const path = environment.APIbaseUrl + '/goals/addEditGoal';
    return this.http.post(path, obj);

  }

  public updateGoalOwners(obj) {
    const path = environment.APIbaseUrl + '/goals/addGoalOwners';
    return this.http.post(path, obj);

  }
  public updateGoalCollaborators(obj) {
    const path = environment.APIbaseUrl + '/goals/addGoalCollaborators';
    return this.http.post(path, obj);
  }

  public updateGoalItem(obj) {
    const path = environment.APIbaseUrl + '/goals/addEditGoalItem';
    return this.http.post(path, obj);
  }
  
  public getActionItemRF(obj) {
    const path = environment.APIbaseUrl + '/goals/getActionItemRF';
    return this.http.post(path, obj);
  }

  public deleteGoalItem(obj) {
    const path = environment.APIbaseUrl + '/goals/deleteActionItem';
    return this.http.post(path, obj);
  }

  public deleteGoal(obj) {
    const path = environment.APIbaseUrl + '/goals/deleteGoal';
    return this.http.post(path, obj);
  }

  public goalItemReview(obj) {
    const path = environment.APIbaseUrl + '/goals/goalItemReview';
    return this.http.post(path, obj);
  }
  public removeEvidence(obj) {
    const path = environment.APIbaseUrl + '/goals/removeEvidence';
    return this.http.post(path, obj);
  }







  // common funcions for collaborator and owner modals
  formatUsers(users, forCollaborators?:boolean, initialState?: boolean) {
    this.allUsers = []
    if (users.length > 0) if (users[0].hasOwnProperty('is_user')) return users;
    let arr = [];

    Object.keys(users).forEach((k) => {

      this.getUserImage(users[k]);
      users[k].is_added = 1;

      if (!users[k].user_id) {

        users[k].is_user = false;

      } else {
        users[k].is_user = true;
      }
      if(forCollaborators){
        let defaultPermisson
      if (!users[k].hasOwnProperty('permission')){
        defaultPermisson = this.checkPermissions(users[k]);
        if(defaultPermisson)
        users[k] = { ...users[k], ...{ permission: [defaultPermisson] } };
      }

      if(initialState){
        arr.push(users[k]);
      } else {
      if(defaultPermisson){
        arr.push(users[k]);
      }
      else if(users[k].role_id==110 || users[k].role_id==100)
      arr.push(users[k]);
      if(users[k].is_user == false){
        users[k].permission = []
        users[k].permission.push(1)
        arr.push(users[k]);
      }
    }
    } else {
      arr.push(users[k]);
    }
this.allUsers.push(users[k]);
    });

    return arr;

  }



  getUserImage(user) {

    if (user.image && user.image != null) {
      user.image = environment.imageBaseUrl + '/' + user.user_id + '/' + user.image;
    } else {
      user.image = false;
    }

  }



  checkPermissions(user){
    if(user.can_be_added_as_goal_collab_with_view_rights ===1) return 1;
    if(user.can_be_added_as_goal_collab_with_edit_rights ===1) return 2;
    if(user.can_be_added_as_goal_collab_with_right_to_add_evidence===1) return 3;
    if(user.can_be_added_as_goal_collab_with_right_to_review_feedback ===1) return 4;
    else return 0;
  }



  public DeleteUser(user, selectedUsers, allUsers) {

    if (user.user_id == -1) {

      let index = _.findIndex(selectedUsers, { email: user.email });


      if (index >= 0) {

        selectedUsers.splice(index, 1);

      }

    } else if (user.is_user) {


      let index = _.findIndex(selectedUsers, { user_id: user.user_id });
      let userIndex = _.findIndex(allUsers,{user_id: user.user_id});
      if (index >= 0) {

        selectedUsers.splice(index, 1);
        if(userIndex >= 0)
        allUsers[userIndex].is_added =1
        // allUsers.push(user);
      }

    } else {



      let index = _.findIndex(selectedUsers, { id: user.id });
      let userIndex = _.findIndex(allUsers,{id: user.id});

      if (index >= 0) {

        selectedUsers.splice(index, 1);
        if(userIndex >= 0)
        allUsers[userIndex].is_added =1
        // allUsers.push(user);
      }

    }

  }


  // public openGroupDetailModal(group_id, selectedUsers, allUsers) {
  //   let obj = {
  //     account_id: this.headerData.user_current_account.accounts.account_id,
  //     group_id
  //   };

  //   this.GetGroupDetail(obj).subscribe((data: any) => {

  //     const config: ModalOptions = {
  //       backdrop: 'static',
  //       keyboard: false,
  //       class: 'modal-container-600',
  //       initialState: {
  //         GroupDetails: {
  //           name: data.group_details[0].name,
  //           users: this.GetGroupUsers(data.group_user_details, selectedUsers, allUsers)
  //         }
  //     }
  //     };
  //     this.bsModalRef = this.modalService.show(GroupDetailModalComponent, config);
  //     // this.headerService.g_modal_data='config';
  //     // this.bsModalRef = this.modalService.show(template, { class: 'modal-md' });

  //   });

  // }



  public GetGroupUsers(users: Array<any>, selectedUsers, allUsers) {

    let arr = [];
    users.forEach((u) => {

      arr.push(this.GetUserById(u.user_id, selectedUsers, allUsers));

    });

    return arr;
  }



  private GetUserById(user_id, selectedUsers, allUsers) {

    let index = _.findIndex(this.allUsers, { user_id: user_id });
    if (index >= 0) {
      return this.allUsers[index];
    } else {
      index = _.findIndex(selectedUsers, { user_id: user_id });
      if (index >= 0)
        return selectedUsers[index];

      else return {};
    }

  }



  public getCopy(obj) {

    return JSON.parse(JSON.stringify(obj));

  }



  public getDateString(date: Date) {
    const year = date.getFullYear();
    let month: number | string = date.getMonth() + 1;
    let day: number | string = date.getDate();

    if (month < 10) month = `0${month}`;
    if (day < 10) day = `0${day}`;

    return `${year}-${month}-${day}`;
  }
}
