
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RootComponent, GoalsListComponent, EditGoalComponent, GoalsWorkComponent, GoalsListDetailsComponent, FrameworkComponent } from './components';
import { SharedPageNotFoundComponent } from '../shared/shared-page-not-found/shared-page-not-found.component';
import { CanDeactivateGuard } from './helpers/can-deactivate.guard';
import { RoutePermissionGuard } from './guards/route-permission.guard';

import {GLOBAL_CONSTANTS} from '@constants/constant';

const pageMode = GLOBAL_CONSTANTS.GOAL_PAGE_MODE;

const routes: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  {
    path: '', component: RootComponent, canActivateChild: [RoutePermissionGuard], children: [
      { path: 'list', component: GoalsListComponent },
      { path: 'create', component: EditGoalComponent, data: { mode: pageMode.CREATE },  canDeactivate:[CanDeactivateGuard] },
      { path: 'view/:goal_id', component: EditGoalComponent, data: { mode: pageMode.VIEW } , canDeactivate:[CanDeactivateGuard]  },
      { path: 'edit/:goal_id', component: EditGoalComponent, data: { mode: pageMode.EDIT }, canDeactivate:[CanDeactivateGuard] },
      { path: 'work/:goal_id/:goal_type/:owner_id', component: GoalsWorkComponent },
      { path: 'work/:goal_id/:goal_type', component: GoalsWorkComponent },
      { path: 'status/:goalId/:goalType', component: GoalsListDetailsComponent },
      { path: 'framework/:goalId/:itemId/:evidenceId/:frameworkId', component: FrameworkComponent }
    ]
  },

  { path: 'page_not_found', component: SharedPageNotFoundComponent },
  { path: '**', redirectTo: 'page_not_found', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GoalsRoutes { }
