import { Component, Input, OnInit, ViewChild, AfterViewInit, OnChanges, SimpleChanges, EventEmitter, OnDestroy, Output } from '@angular/core';
import { HeaderService, AppMainService, SocketService } from '@app/services';
import { GoalsService, EditGoalService } from '@goals/services';
import { Subscription, Subject } from 'rxjs';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import { ShowToasterService } from '@projectModules/app/services';
import { Router, ActivatedRoute } from '@angular/router';
import { SlimScrollEvent, ISlimScrollOptions } from 'ngx-slimscroll';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { BreadCrumbInterface } from '@src/project-modules/app/interfaces';
import * as moment from 'moment';

import { EvidenceModalComponent, FrameWorkEvidenceModalComponent, DeleteComponent } from '@goals/modals';
import { AddEvidenceModalComponent } from '@src/project-modules/shared/modals/add-evidence-modal/add-evidence-modal.component';

@Component({
  selector: 'goals-work-details',
  templateUrl: './work-details.component.html',
  styleUrls: ['./work-details.component.css']
})
export class WorkDetailsComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  bsModalRef: BsModalRef;
  modalOptions: ModalOptions
  public opts: ISlimScrollOptions;
  public scrollEvents: EventEmitter<SlimScrollEvent>;
  today = new Date()

  @ViewChild('error', { static: false }) error;
  @ViewChild('uncheckConfirmation', { static: false }) uncheckConfirmation;
  @ViewChild('checkConfirmation', { static: false }) checkConfirmation;
  @ViewChild('deleteTemplate', { static: false }) deleteTemplate;
  @Input() singleGoalData;
  @Input() reflection;
  @Input() afterDueDate;
  @Input() owner_id;
  @Input() owner;
  @Input() collaborator;
  @Input() goal_id;
  @Input() evidenceAllowed;
  @Input() viewer;
  @Input() allowEditDelete;
  @Input() goal;
  @Input() totalActionItems;
  @Output() deletedItem: EventEmitter<any> = new EventEmitter();
  public userCurrAcc: any;
  public goalAltName: string;
  edit_mode: boolean = false;
  edit_mode_reflection: boolean = false;
  edit_mode_feedback: boolean = false;
  headerData: any;
  private subscriptions: Subscription = new Subscription();
  translation: any;
  itemCopy: any = {};
  evidenceType: any = [];
  socket_listener: any;
  goalUserSettings: any;
  // reflectionFocus:boolean=false;
  public canUseFramework: boolean = false;
  feedbackFocus: boolean = false;
  goalSettings: any;
  defaultReflection: any;
  defaultFeedback: any;
  @Input() addEvidence = false;
  @Input() addReview = false;
  @Input() addMeasurement = false;
  private measurementInput: Subject<string> = new Subject();
  // @Input() ownersList: any;
  OwnerCanNotEdit:boolean = false;
  collababoratorCanNotEdit: boolean = false;
  private frameworks: any = [];
  checkbox: any;
  feedback_data: any;
  reflection_data: any;
  confirmation: any;


  constructor(private modalService: BsModalService, public headerService: HeaderService, private activatedRoute: ActivatedRoute,
    private appMainService: AppMainService, public editGoalService: EditGoalService, private toastr: ShowToasterService,
    private socketService: SocketService, private router: Router, private goalsService: GoalsService) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.evidenceType = [this.translation.artifacts_video, this.translation.artifacts_resource, this.translation.huddle, this.translation.huddle_more_frameworks, this.translation.artifact_scripted_notes];
      this.goalAltName = this.headerService.getGoalsAltName();
      this.headerData = this.headerService.getStaticHeaderData();
      this.goalUserSettings = this.headerService.getStaticHeaderData('goal_users_settings');
      if (this.goalUserSettings.can_use_frameworks_in_goal_evidence == 1) {
        this.canUseFramework = true;
      } else {
        this.canUseFramework = false;
      }
      this.goalSettings = this.headerService.getStaticHeaderData('goal_settings');
    }));
  }

  ngOnInit() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    }

    this.userCurrAcc = this.headerData.user_current_account;
    this.SubscribeMeasurement();
    if(this.afterDueDate && !this.goalSettings.goal_owner_setting && !this.owner) {
      this.OwnerCanNotEdit = true;
    } else {
      this.OwnerCanNotEdit = false
    }
    // setTimeout(() => {
    //   this.openEvidenceModal('video')
    // }, 500);
    this.updateBreadCrumb(this.goalAltName);
    console.log("single goal data",this.singleGoalData);
    console.log("owner",this.owner);
    this.singleGoalData.submitted_evidence = this.singleGoalData.submitted_evidence.filter(e=>e.title != null);
  }

  ngAfterViewInit() {

  }


  private updateBreadCrumb(title: string) {
    const breadCrum = [{ title: title, link: '/goals/list' },{ title: this.goal?.title }]
    this.appMainService.updateBreadCrumb(breadCrum);
  }


  openFrameworkModal() {

    if (this.frameworks.length === 0) {
      this.appMainService.getFrameworks(this.userCurrAcc.accounts.account_id).subscribe((frameworks: any) => {
        this.goalsService.updateFrameworksArray(frameworks);
        this.frameworks = frameworks;
      });
    }


    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'e-framework-modal-container',
      initialState: {
        frameworks: this.frameworks,
        data: {
          goalId: this.goal_id,
          itemId: this.singleGoalData.id,
          evidenceType: 4,
          ownerId: this.owner_id,
          userId: this.userCurrAcc.User.id
        }
      }
    };

    this.modalService.show(FrameWorkEvidenceModalComponent, config);
  }

  getEvidenceType(evidenceType) {
    if (evidenceType == 'video') {
      return 1
    }
    if (evidenceType == 'resource') {
      return 2
    }
    if (evidenceType == 'huddle') {
      return 3
    }
    if (evidenceType == 'scripted_notes') {
      return 5
    }
  }

  public openEvidenceModal(evidenceType) {

    let type = this.getEvidenceType(evidenceType);
 
    let submittedEvidence = []
    if (this.singleGoalData.submitted_evidence) {
    
      this.singleGoalData.submitted_evidence.forEach(evidence => {
       
        if (evidence.evidence_type == type) {
         
          submittedEvidence.push(Number(evidence.evidence_ref_id))
        
        }
      });
    }
    // console.log('submittedEvidence: ', this.singleGoalData.submitted_evidence);
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'evidance-modal-container',
      initialState: {
        evidenceType,
        item_id: this.singleGoalData.id,
        owner_id: this.owner_id,
        goal_id: this.goal_id,
        from_component: 'goals',
        from_asessors: true,
        submitted_evidence: submittedEvidence
      }
    };
    // console.log('config.initialState: ', config.initialState);
    this.modalService.show(AddEvidenceModalComponent, config)
    // this.modalService.show(EvidenceModalComponent, config)
  }
dateFormat(date){
var date_fomate=moment(date).format('MMM D, YYYY');
return date_fomate;
}
  edit(item, type?) {
    // if(this.afterDueDate) {
    //   this.toastr.ShowToastr('info',"You don't have permission to change action item")
    // } else {
    this.feedback_data=item.feedback;
    this.reflection_data=item.reflection;
    if (type) {
      this.edit_mode_reflection = true;
      this.defaultReflection = item.reflection;
    } else {
      this.edit_mode_feedback = true;
      this.defaultFeedback = item.feedback;
    }
    this.itemCopy = _.cloneDeep(item);
  // }
    // this.singleGoalData = _.cloneDeep(item);
  }
  public FormatSeconds(time) {
    if(time==0 || time==null) return "00:00:00";
    let sec_num: any = parseInt(time, 10);
    let hours: any = Math.floor(sec_num / 3600);
    let minutes: any = Math.floor((sec_num - hours * 3600) / 60);
    let seconds: any = sec_num - hours * 3600 - minutes * 60;

    if (hours < 10) {
      hours = "0" + hours;
    }
    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    return hours + ":" + minutes + ":" + seconds;
  }


  saveReflection(item) {
    // if(!this.afterDueDate) {
    item.reflection = this.reflection
    this.save(item)
    // } else {
    //   this.toastr.ShowToastr('info',"You don't have permission to change action item")
    // }
  }
  

  save(item, addFeedback?, measurement?) {
    let obj = {
      item_id: item.id,
      user_id: this.userCurrAcc.User.id,
      // current_value: item.current_value,
      is_done: item.is_done,
      reflection: item.reflection,
      // feedback:item.feedback,
      goal_type: this.activatedRoute.snapshot.params.goal_type,
      goal_id: this.goal_id,
      owner_id: this.owner_id,
    };
    if (measurement) {
      if (isNaN(item.current_value)) {
        this.toastr.ShowToastr('error',this.translation.vd_please_enter_valid_number);
        return;
      }
      if ((item.start_value < item.end_value) && item.current_value < item.start_value) {
        this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.measurement_error_for_input));
        return;
      }
      if ((item.start_value > item.end_value) && item.current_value < item.end_value) {
        this.toastr.ShowToastr('error','Measurement value should not be less then end value');
        return;
      }
      obj['current_value'] = item.current_value;
    }
    if (addFeedback) obj['feedback'] = item.feedback;


    this.editGoalService.goalItemReview(obj).subscribe((res: any) => {
      if (!this.edit_mode_reflection && obj.reflection) { this.edit_mode_reflection = false; }

      this.edit_mode_feedback = false;
      if (res.success) {
        this.headerService.getGoalAltTranslation(res.message)
      }
    }, error => {
      // this.edit_mode = false;
      this.edit_mode_reflection = false;
      this.edit_mode_feedback = false;
    })
  }
  cancelEditMode(val) {
    // if(confirm(this.translation.goals_changes_may_not_be_saved))
    // {
    // this.edit_mode_feedback = false;
    // this.edit_mode_reflection = false;
    // this.singleGoalData = this.itemCopy;
    // }
    if(val==1){
      this.singleGoalData.feedback= this.feedback_data;
      this.edit_mode_feedback=false;
    }
    else{
      this.reflection=this.reflection_data;
      this.edit_mode_reflection=false;

    }
   
  }

  deleteEvidence(evidence, eType) {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'e-delete-modal-container',
      initialState: {
        evidence,
        evidenceType: eType
      }
    };
    this.modalService.show(DeleteComponent, config);

  }

  checkDueDate() {
    if (moment(this.singleGoalData.deadline).toDate().setHours(0, 0, 0, 0) >= moment().toDate().setHours(0, 0, 0, 0))
      return true;
    else
      return false
  }

  actionItemDone(item) {
    // console.log(this.owner, this.goalSettings.goal_owner_setting)
    debugger;
    //|| (!this.owner && !this.goalSettings.goal_owner_setting && !this.addReview)
    if (!this.checkDueDate() && this.owner && this.goalSettings.goal_owner_setting) {
      if(item.checked)
      {
        if (this.singleGoalData.submitted_evidence.length > 0 || (this.singleGoalData.current_value > this.singleGoalData.start_value) || this.singleGoalData.reflection != null) {
          if (isNaN(this.singleGoalData.current_value)) {
            item.checked = false;
            this.singleGoalData.is_done = 0
            this.toastr.ShowToastr('error',this.translation.vd_please_enter_valid_number);
            return;
          }
          if (this.singleGoalData.current_value < this.singleGoalData.start_value) {
            item.checked = false;
            this.singleGoalData.is_done = 0
            this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.measurement_error_for_input));
            return;
          }
          this.checkbox = item
          const config: ModalOptions = {
            backdrop: 'static',
            keyboard: false,
            class: 'modal-md modal-container-500',
          };
          this.bsModalRef = this.modalService.show(this.checkConfirmation, config)
          return;
        } else {
          const config: ModalOptions = {
            backdrop: 'static',
            keyboard: false,
            class: 'modal-md modal-container-500',
          };
          this.bsModalRef = this.modalService.show(this.error, config);
          this.singleGoalData.is_done = 0;
          item.checked = false;
          return;
        }
      }
      else
      {
        this.singleGoalData.is_done = item.checked ? 1 : 0;
        this.singleGoalData.feedback = ''
        this.save(this.singleGoalData, true);
        return;
      }
    }
    if(!this.checkDueDate() && this.owner && !this.goalSettings.goal_owner_setting)
    {
      item.checked = !item.checked
      this.singleGoalData.is_done = !this.singleGoalData.is_done;
      return;
    }
    if ((!this.checkDueDate() || (this.viewer)) && (!this.headerData.goal_settings.review_collaborators && !this.addReview)){
      item.checked = !item.checked
      this.singleGoalData.is_done = !this.singleGoalData.is_done;
      return;
    }
    if (!this.owner && !this.addReview && !item.checked) {
      this.singleGoalData.is_done = 1
      item.checked = true
      return
    }
    // || (!this.owner && !this.goalSettings.goal_owner_setting)
    if((this.addReview && item.checked && !this.addEvidence) && (!this.headerData.goal_settings.review_collaborators && !this.addReview)){
      item.checked = false;
      this.singleGoalData.is_done = 0
      return;
    }
    if (item.checked) {
      // if (((this.singleGoalData.submitted_evidence.length > 0 || this.singleGoalData.reflection) && !this.singleGoalData.add_measurement)
      //   || (this.singleGoalData.submitted_evidence.length > 0 || this.singleGoalData.reflection) || this.singleGoalData.add_measurement ||
      //   this.singleGoalData.current_value) {
        if(this.singleGoalData.submitted_evidence.length > 0 || (this.singleGoalData.current_value > this.singleGoalData.start_value) || this.singleGoalData.reflection!=null) {
        if (isNaN(this.singleGoalData.current_value)) {
          item.checked = false;
          this.singleGoalData.is_done = 0
          this.toastr.ShowToastr('error',this.translation.vd_please_enter_valid_number);
          return;
        }
        if (this.singleGoalData.current_value < this.singleGoalData.start_value) {
          item.checked = false;
          this.singleGoalData.is_done = 0
          this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.measurement_error_for_input));
          return;
        }
        if (this.goalSettings.goal_owners_cannot_uncheck_action_items) {
          this.checkbox = item
          const config: ModalOptions = {
            backdrop: 'static',
            keyboard: false,
            class: 'modal-md modal-container-500',
          };
          this.bsModalRef = this.modalService.show(this.checkConfirmation, config)
          return
        }
        else {
          this.singleGoalData.is_done = 1;
          item.checked = true;
          this.singleGoalData.feedback = ''
          this.save(this.singleGoalData, true);
        }
      }
      else {
        const config: ModalOptions = {
          backdrop: 'static',
          keyboard: false,
          class: 'modal-md modal-container-500',
        };
        this.bsModalRef = this.modalService.show(this.error, config);
        this.singleGoalData.is_done = 0;
        item.checked = false;
      }
    } else {
      if (this.goalSettings.goal_owners_cannot_uncheck_action_items && this.owner && !this.addReview) {
        this.singleGoalData.is_done = 1
        item.checked = true
        return
      }
      if (this.owner) {
        this.singleGoalData.is_done = 0;
        item.checked = false;
        this.singleGoalData.feedback = ''
        this.save(this.singleGoalData, true)
        return
      }
      this.checkbox = item
      const config: ModalOptions = {
        backdrop: 'static',
        keyboard: false,
        class: 'modal-md modal-container-500',
      };
      this.bsModalRef = this.modalService.show(this.uncheckConfirmation, config)
      return

    }

  }

  uncheck(confirmation) {
    if (confirmation) {
      this.singleGoalData.is_done = 0;
      this.checkbox.checked = false;
      this.bsModalRef.hide()
      this.singleGoalData.feedback = ''
      this.save(this.singleGoalData, true)
    } else {
      this.singleGoalData.is_done = 1;
      this.checkbox.checked = true;
      this.bsModalRef.hide()
    }
  }

  check(confirmation) {
    if (confirmation) {
      this.singleGoalData.is_done = 1;
      this.checkbox.checked = true;
      this.bsModalRef.hide()
      this.singleGoalData.feedback = ''
      this.save(this.singleGoalData, true)
    } else {
      this.singleGoalData.is_done = 0;
      this.checkbox.checked = false;
      this.bsModalRef.hide()
    }
  }


  /*public refFocus(val){

    if (val != '') {
      this.reflectionFocus=true;
    }else{
      this.reflectionFocus=false;
    }
}*/
  public fbFocus(val) {
    if (val != '') {
      this.feedbackFocus = true;
      this.edit_mode_feedback = true;
    } else {
      this.feedbackFocus = false;
    }
  }
  public trimSpaces(str) {
    if (str)
      return str.replace(/\s/g, "")
  }
  public measurementChange(str) {
    this.singleGoalData.current_value = str;
    this.measurementInput.next(str)

  }
  private SubscribeMeasurement() {
    this.measurementInput
      .pipe(
        debounceTime(2000),
        distinctUntilChanged()
      )
      .subscribe(value => {
        this.save(this.singleGoalData, false, 1)


      });
  }

  deleteActionItem(item, is_confirmed?) {
    console.log('Items', this.totalActionItems);
    if(this.totalActionItems <= 1){
      this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goals_atleast_one_action_item))
      return;
    }
    // this.deleteItem = _.cloneDeep(item);


    console.log('Item on Delete', item)
    let obj = {
      goal_id: this.goal_id,
      item_id: item,
      user_id: this.userCurrAcc.User.id
    }
    if(is_confirmed){
      this.bsModalRef.hide()
      obj['is_confirmed'] = is_confirmed;
    }
    this.editGoalService.deleteGoalItem(obj).subscribe((res: any) => {
      if(res.success) {
      this.deletedItem.emit(this.singleGoalData);
      this.toastr.ShowToastr('success',this.headerService.getGoalAltTranslation(this.translation.goal_item_deleted_successfully))
      }else
      this.bsModalRef = this.modalService.show(this.deleteTemplate, { class: 'modal-md modal-container-700' });
    });
  }


  ngOnChanges(changes: SimpleChanges) {
    if (this.edit_mode && changes.singleGoalData.currentValue.id != changes.singleGoalData.previousValue.id)
      this.edit_mode_feedback = false;
    this.edit_mode_reflection = false;
    if (this.singleGoalData.is_done != 1 && !this.singleGoalData.reflection) {
      this.edit(this.singleGoalData, 1);
    }
  }
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}


