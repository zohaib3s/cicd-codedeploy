import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription, forkJoin } from 'rxjs';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';

import { HeaderService, AppMainService } from "@app/services";
import { GoalsService } from "@goals/services";
import { BreadCrumbInterface } from '@app/interfaces';

@Component({
  selector: 'goals-framework',
  templateUrl: './framework.component.html',
  styleUrls: ['./framework.component.css']
})
export class FrameworkComponent implements OnInit, OnDestroy {

  public loaded: boolean = false;
  public canEdit: boolean = false;
  public isSubmitingForm: boolean = false;
  public performanceLevelsPreviewView: boolean = false;
  public goalPerformanceLevelEnabled: boolean = false;
  public frameworkPerformanceLevelEnabled: boolean = false;
  public standardsStaticView: boolean = false;
  public searchFramework: string;
  public submitBtnTitle: string;

  public goalAltName: string;
  public frameworkData: any;
  public frameworkDataDeepCopy: any; // to be used for later use for other calculation
  public selectedFrameworks: any[] = [];
  public currentFrameworkForPLS: any;
  public selectedPerformanceLevel: any = 0;
  public evidenceStandardData: any;
  public evidenceStandardDataDeepCopy: any; // to be used for later use for other calculation
  public ratings: any[] = [];
  public standardDataOwnRating: boolean = false;
  public averagePLData: any = { averageRating: '', standards: [] };

  public userCurrAcc: any;

  private goalId: number;
  private itemId: number;
  private evidenceId: number;

  private subscriptions: Subscription = new Subscription();
  public translation: any = {};

  public slimScrollOpts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;

  constructor(private headerService: HeaderService, private appMainService: AppMainService, private toastr: ShowToasterService,
    private goalsService: GoalsService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.subscriptions.add(this.headerService.behaviorialHeaderData$.subscribe(headerData => this.goalPerformanceLevelEnabled = headerData.goal_settings.enable_performance_level));
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;

      // Change button title on socket also
      if (this.goalPerformanceLevelEnabled && this.frameworkPerformanceLevelEnabled) this.submitBtnTitle = this.translation.done_vl;
      else this.submitBtnTitle = this.translation.vd_save;

      this.goalAltName = this.headerService.getGoalsAltName();
      this.updateBreadCrumb(this.goalAltName);
    }));

    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
  }

  ngOnInit() {

    this.prepareSlimScrollOptions();
    this.averagePLData.averageRating = this.translation.no_rating;

    this.activatedRoute.params.subscribe(param => {
      this.goalId = param.goalId;
      this.itemId = param.itemId;
      this.evidenceId = param.evidenceId;
      this.getData(this.goalId, this.evidenceId, param.frameworkId);
    });

  }

  private getData(goalId: number, evidenceId: number, frameworkId: number) {

    const data = { goal_id: goalId, evidence_id: evidenceId, framework_id: frameworkId, user_id: this.userCurrAcc.User.id, account_id: this.userCurrAcc.accounts.account_id };

    const frameworkData = this.appMainService.getFrameworkSettingsById(frameworkId);
    const evidenceStandardData = this.goalsService.getEvidenceStandards(data);

    // Make both requests in parallel and do operation when both are completed
    forkJoin([frameworkData, evidenceStandardData]).subscribe((results: any) => {
      // results[0] is frameworkData
      // results[1] is evidenceStandardData

      if (results[1].is_allowed) {
        this.frameworkData = results[0].data;
        this.frameworkDataDeepCopy = JSON.parse(JSON.stringify(this.frameworkData));

        // Check if performance levels are enabled for current framework
        this.frameworkPerformanceLevelEnabled = Boolean(Number(this.frameworkData.account_framework_settings.enable_performance_level));
        if (this.goalPerformanceLevelEnabled && this.frameworkPerformanceLevelEnabled) this.submitBtnTitle = this.translation.done_vl;
        else this.submitBtnTitle = this.translation.vd_save;

        this.evidenceStandardData = results[1].data;
        this.canEdit = results[1].edit_permission;
        this.evidenceStandardDataDeepCopy = JSON.parse(JSON.stringify(this.evidenceStandardData));

        if (Array.isArray(results[1].performance_levels) && results[1].performance_levels.length > 0) {
          this.standardDataOwnRating = true;
          this.ratings = results[1].performance_levels;
        } else {
          results[1].ratings.forEach(item => {
            let rating: any = {};
            let title = item.meta_data_name.substring(item.meta_data_name.lastIndexOf("_") + 1, item.meta_data_name.length);
            if (title) {
              rating.id = item.account_meta_data_id;
              rating.performance_level = title;
              rating.performance_level_rating = item.meta_data_value;
              this.ratings.push(rating);
            }
          });
        }

        this.formatFrameworkData();

        this.loaded = true;
      } else {
        this.toastr.ShowToastr('info',this.headerService.getGoalAltTranslation(this.translation.you_are_not_participating_in_this_goal));
        this.router.navigate(['/goals/list']);
      }

    });

  }

  private formatFrameworkData() {
    if (this.evidenceStandardData.length > 0) {
      this.evidenceStandardData.forEach(item => {
        const frameworkIndex = this.frameworkData.account_tag_type_0.findIndex(framework => framework.account_tag_id == item.standard_id);
        if (frameworkIndex > -1) {
          this.frameworkData.account_tag_type_0[frameworkIndex].checked = true;
          item.tag_code = this.frameworkData.account_tag_type_0[frameworkIndex].tag_code,
            item.tag_html = this.frameworkData.account_tag_type_0[frameworkIndex].tag_html,
            this.selectedFrameworks.push(item);
        }
      });

      if (this.goalPerformanceLevelEnabled && this.frameworkPerformanceLevelEnabled) {
        this.calculateAveragePerformanceLevel();
        this.performanceLevelsPreviewView = true;
        this.currentFrameworkForPLS = null;
      } else {
        this.standardsStaticView = true;
      }
    } else if (!this.canEdit) this.standardsStaticView = true;
  }

  public change(checked: boolean, framework: any) {
    framework.checked = checked;

    if (checked) {
      const frameworkItem = {
        tag_code: framework.tag_code,
        tag_html: framework.tag_html,
        standard_id: framework.account_tag_id,
        rating_id: null,
        rating_value: null
      };
      this.selectedFrameworks.push(frameworkItem)
    } else {
      const frameworkIndex = this.selectedFrameworks.findIndex(item => item.standard_id == framework.account_tag_id);
      if (frameworkIndex > -1) this.selectedFrameworks.splice(frameworkIndex, 1);

      if (this.currentFrameworkForPLS && framework.account_tag_id == this.currentFrameworkForPLS.standard_id) this.currentFrameworkForPLS = null;
    }

  }

  public openPerformanceSlection(framework: any, event: any) {
    if (event.target.localName != 'input' && event.target.localName != 'span' && this.goalPerformanceLevelEnabled && this.frameworkPerformanceLevelEnabled) { // Do not listen for checkbox click
      const frmwrk = this.selectedFrameworks.find(item => item.standard_id == framework.account_tag_id);
      if (frmwrk) {
        this.currentFrameworkForPLS = frmwrk;
        if (frmwrk.rating_id) {
          const rating = this.ratings.find(item => item.id == frmwrk.rating_id);
          this.selectedPerformanceLevel = rating;
        } else this.selectedPerformanceLevel = 0;
      }
    }
  }

  public frameworkRatingChange() {
    if (this.selectedPerformanceLevel == 0) {
      this.currentFrameworkForPLS.rating_id = null;
      this.currentFrameworkForPLS.rating_value = null;
    } else {
      this.currentFrameworkForPLS.rating_id = this.selectedPerformanceLevel.id;
      this.currentFrameworkForPLS.rating_value = this.selectedPerformanceLevel.performance_level_rating;
    }

  }

  private calculateAveragePerformanceLevel() {
    this.averagePLData.standards = [];
    this.evidenceStandardData.forEach(item => {
      let averagePerformanceLevel: any = {};
      const framework = this.frameworkData.account_tag_type_0.find(framework => framework.account_tag_id == item.standard_id);
      if (framework) averagePerformanceLevel.title = `${framework.tag_code} ${framework.tag_html}`;
      averagePerformanceLevel.ratingValue = item.rating_value;

      const rating = this.ratings.find(rating => rating.id == item.rating_id);
      if (rating) averagePerformanceLevel.ratingTitle = rating.performance_level;

      this.averagePLData.standards.push(averagePerformanceLevel);

    });

    let averageValue = 0;
    let standardsWithRating = 0;

    this.averagePLData.standards.forEach(standard => {
      if (standard.ratingValue) {
        averageValue += standard.ratingValue;
        standardsWithRating += 1;
      }
    });

    if (averageValue) {
      averageValue = Math.round(averageValue / standardsWithRating);
      const averageRating = this.ratings.find(rating => rating.performance_level_rating == averageValue);
      if (averageRating) this.averagePLData.averageRating = `${averageValue} - ${averageRating.performance_level}`;
      else this.averagePLData.averageRating = this.translation.no_rating;
    } else this.averagePLData.averageRating = this.translation.no_rating;


  }

  public cancel() {
    this.frameworkData = this.frameworkDataDeepCopy;
    this.evidenceStandardData = this.evidenceStandardDataDeepCopy;
    this.selectedFrameworks = [];
    if (this.goalPerformanceLevelEnabled && this.frameworkPerformanceLevelEnabled) {
      this.calculateAveragePerformanceLevel();
      this.performanceLevelsPreviewView = true;
      this.currentFrameworkForPLS = null;
    } else this.standardsStaticView = true;
    this.formatFrameworkData();

  }

  public submit() {
    this.isSubmitingForm = true;

    const data = {
      goal_id: this.goalId,
      item_id: this.itemId,
      evidence_id: this.evidenceId,
      standards: this.selectedFrameworks,
      user_id: this.userCurrAcc.User.id
    };

    this.goalsService.addGoalItemStandards(data).subscribe((res: any) => {
      this.isSubmitingForm = false;
      this.evidenceStandardData = res.data;
      this.evidenceStandardDataDeepCopy = JSON.parse(JSON.stringify(res.data));
      if (this.goalPerformanceLevelEnabled && this.frameworkPerformanceLevelEnabled) {
        this.calculateAveragePerformanceLevel();
        this.performanceLevelsPreviewView = true;
        this.currentFrameworkForPLS = null;
      } else {
        this.standardsStaticView = true;
      }
    }, () => {
      this.isSubmitingForm = false;
    })
  }

  private updateBreadCrumb(title: string) {
    const breadCrumb: BreadCrumbInterface[] = [
      { title: title, link: '/goals/list' },
      { title: this.translation.vd_frameworks }
    ];
    this.appMainService.updateBreadCrumb(breadCrumb);
  }

  private prepareSlimScrollOptions() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.slimScrollOpts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '7',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}