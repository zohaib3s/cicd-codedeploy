import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { HeaderService, AppMainService } from "@app/services";
import { BreadCrumbInterface } from "@app/interfaces";
import { ActivatedRoute, NavigationStart, Router, RoutesRecognized, UrlHandlingStrategy } from '@angular/router';
import { decode } from '@src/project-modules/shared/components/common/utility';

@Component({
  selector: 'goals-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnDestroy {

  public loaded: boolean = false;
  public translation: any = {};
  public userName: any = {};
  public breadCrumbs: BreadCrumbInterface[] = [];
  private subscriptions: Subscription = new Subscription();
  fromRoute: any;

  constructor(public headerService: HeaderService, private appMainService: AppMainService, private route: ActivatedRoute, private router: Router) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => this.translation = languageTranslation));

    this.subscriptions.add(this.appMainService.breadCrumb$.subscribe(((breadCrumbs: BreadCrumbInterface[]) => {
      setTimeout(() => {
        this.breadCrumbs = breadCrumbs;
        this.loaded = true;
      }, 100);

    })));
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.loaded = false;
        // for loading 
      }
    });
    if (route.snapshot.firstChild.queryParams.from) {
      this.fromRoute = decode(route.snapshot.firstChild.queryParams.from);
      this.fromRoute.url = decodeURIComponent(this.fromRoute.url);
      this.userName = decodeURIComponent(this.fromRoute.userName)
    }
  }
  routeTo() {
    this.router.navigateByUrl(this.fromRoute.url);
  }
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}
