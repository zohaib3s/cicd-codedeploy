import { Component, OnInit, EventEmitter, HostListener, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { ShowToasterService } from '@projectModules/app/services';

import { HeaderService, AppMainService, SocketService } from '@app/services';
import { EditGoalService } from '@goals/services';

import { CollaboratorModalComponent, OwnerModalComponent, DeleteComponent, AddGoalModalComponent } from '@goals/modals';
import { GLOBAL_CONSTANTS } from "@constants/constant";

@Component({
  selector: 'goals-edit-goal',
  templateUrl: './edit-goal.component.html',
  styleUrls: ['./edit-goal.component.css']
})
export class EditGoalComponent implements OnInit, OnDestroy {

  bsModalRef: BsModalRef;
  goal: any = {};
  socket_listener: any;
  isEditGoal = true;
  isChangeOccur  = false
  userCurrAcc: any;
  private headerData: any;
  users: any = [];
  public today = new Date();
  public minEndDate;
  public start_date;
  public end_date;
  public ownersData = [];
  public collaboratorsData = [];
  public categories = [];
  goalDetailsObj: { account_id: any; user_id: any; goal_id: number; };
  categoryString = '';
  catLength = 0;
  public goalAltName: string;
  public loader = true;
  editMode: boolean;
  public translation: any = {};
  private subscriptions: Subscription = new Subscription();
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  goalUserSettings: any;
  user: any;
  canBePusblished: boolean =true;
  goalCancelled: boolean = false;
  title: any;
  templateId: any;

  private taAddEditGoalsKey: string = '';
  private addGoalLSKey: string = '';
  private editGoalLSKey: string = '';
  private pageMode = GLOBAL_CONSTANTS.GOAL_PAGE_MODE;
  private currentPageMode: string;
  isIndividual = false;
  addGoalModal
  checkActionItemSaved = false
  canEditDelete = true;
  private pagePreviewMode: boolean = false;
  actionItemEditMode: boolean = false;
  isChangOccurInChild= false;

  constructor(private modalService: BsModalService, public editGoalService: EditGoalService,
    public headerService: HeaderService, private toastr: ShowToasterService, private socketService: SocketService,
    private appMainService: AppMainService, private router: Router, private activatedRoute: ActivatedRoute) {
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');

    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.goalAltName = this.headerService.getGoalsAltName();
      
      this.goalUserSettings = this.headerService.getStaticHeaderData('goal_users_settings');
    }));

    this.subscriptions.add(this.headerService.behaviorialHeaderData$.subscribe(headerData => this.headerData = headerData));

    this.goal.desc = '';
    this.goalDetailsObj = {
      account_id: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.users_accounts.user_id,
      goal_id: 0
    };
    this.goal.start_date = this.today;
    this.goal.end_date = this.today;
    this.minEndDate = this.addDays(this.today, 1);
    this.goal.is_published = 0
    this.user = this.userCurrAcc.User;

    this.taAddEditGoalsKey = `${GLOBAL_CONSTANTS.LOCAL_STORAGE.GOAL.ADD_EDIT_TA}${this.userCurrAcc.accounts.account_id}-${this.userCurrAcc.User.id}`;
    this.addGoalLSKey = `${GLOBAL_CONSTANTS.LOCAL_STORAGE.GOAL.ADD}${this.userCurrAcc.accounts.account_id}-${this.userCurrAcc.User.id}`;

  }

  /*  @HostListener('window:beforeunload', ['$event']) unloadHandler(event: Event) {
      let result = confirm('Changes you made may not be saved.');
      if (result) {
        // Do more processing...
      }
      event.returnValue = false; // stay on same page
    }*/

  ngOnInit() {

    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '7',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    };

    this.activatedRoute.params.subscribe(param => {
      if (param.goal_id) this.goalDetailsObj.goal_id = param.goal_id;
    });

    this.currentPageMode = this.activatedRoute.snapshot.data.mode;
    if (this.currentPageMode === this.pageMode.CREATE || this.currentPageMode === this.pageMode.EDIT) this.isEditGoal = true;
    else this.isEditGoal = false;

    this.activatedRoute.queryParams.subscribe(queryParam => {
      if (queryParam.title) {
        this.title = queryParam.title
        this.goal.title = queryParam.title
      }
      if (queryParam.templateId) {
        this.templateId = queryParam.templateId;
        this.goalDetailsObj.goal_id = queryParam.templateId;
        this.goal.title = queryParam.title

      }
      if (queryParam.goal_type) this.goal.goal_type = queryParam.goal_type;
      if (queryParam.previewMode) this.pagePreviewMode = queryParam.previewMode;


      // if (queryParam.goal_id) this.goalDetailsObj.goal_id = queryParam.goal_id
      // if (params.mode) {
      //   if (params.mode === 'edit') this.isEditGoal = true
      //   if (params.mode === 'detail') this.isEditGoal = false
      // }
      // if (_.isEmpty(params)) this.router.navigate(['/goals/list'])
    });

    // add all the users as an owwner when goal type is account
    if (this.goal.goal_type == 4 && this.goalUserSettings.can_create_individual_goals_for_themselves == '1'
      && this.goalUserSettings.can_create_individual_goals_for_others_people == '0' && this.goalDetailsObj.goal_id == 0
      && this.goalUserSettings.role_id !== '100' && this.goalUserSettings.role_id !== '110') {
      this.goal.owners = [Number(this.goalDetailsObj.user_id)];
      this.isIndividual = true;
    }

    // add the goal creator as a collaborator by defualt if creator has a permission to create individual goals for others
    if (this.goal.goal_type == 4 && this.goalUserSettings.can_create_individual_goals_for_themselves == '0'
      && this.goalDetailsObj.goal_id == 0) {
      let obj = {
        email: this.user.email,
        first_name: this.user.first_name,
        last_name: this.user.last_name,
        goal_id: this.goalDetailsObj.goal_id,
        user_id: Number(this.goalDetailsObj.user_id),
        image: this.user.image,
        permission: 4,
        user_type: 1,
        can_be_added_as_goal_collab_with_edit_right: this.goalUserSettings.can_be_added_as_goal_collab_with_edit_rights,
        can_be_added_as_goal_collab_with_right_to_add_evidence: this.goalUserSettings.can_be_added_as_goal_collab_with_right_to_add_evidence,
        can_be_added_as_goal_collab_with_right_to_review_feedback: 1,
        can_be_added_as_goal_collab_with_view_rights: this.goalUserSettings.can_be_added_as_goal_collab_with_view_rights

      }
      this.goal.collaborators = [obj];
    }

    this.getGoalData();
    this.getGoalDetails();
    this.initiateSocket();
    setTimeout(() => {
      this.checkPublishment();
    }, 1000);
  }

  changeOccur(res){
    this.isChangeOccur = res;
  }
  changeOccurInChild(res){
    this.isChangOccurInChild = res;
  }

  isItemSave(x) {
    if (x) {
      this.publish()
    }
    else {
      this.checkActionItemSaved = false
    }
  }
  parseDate(date){
    // parse the date to replace the offset as it is not parsed by safari
     if (date) {
       return date.replace(/\s/g, "T")
     } else return false;
     
   }

  checkPublishment() {
    this.subscriptions.add(this.editGoalService.$canBePusblishedSubject.subscribe(x => {
      this.canBePusblished = x
    }))
  }
  getGoalData() {
    this.subscriptions.add(this.editGoalService.$editGoalGlobalSubject.subscribe((res: any) => {
      if (res.createGoalData) {
        this.goal = res.createGoalData;
      }
      if (res.ownersData) {
        this.goal.owners = res.ownersData.owners;
      }
      if (res.collaboratorsData) {
        this.goal.collaborators = res.collaboratorsData.collaborators
      }
    }));
  }

  checkActionItemEditModeOn(mode){
    this.actionItemEditMode = mode;
  }

  getGoalDetails() {

    if (this.goalDetailsObj.goal_id > 0 && !this.templateId) {
      this.editMode = true;
    }
    this.editGoalService.getGoalDetails(this.goalDetailsObj).subscribe((res: any) => {
      if(res.data == null && this.goalDetailsObj.goal_id != 0) {
        this.toastr.ShowToastr('info',this.translation.do_not_have_permission_goal);
        this.router.navigate(['profile-page']);
      } else if (res.success) {
      for (const iterator in res.all_users) {
        if (res.all_users.hasOwnProperty(iterator)) {
          this.users.push(res.all_users[iterator]);
        }
      }

      if (res.data) {
      //console.log(res.data)
        this.goal = res.data;

        this.goal.id = this.templateId ? 0 : this.goal.id;
        this.goal.title = this.templateId ? this.title : this.goal.title;
        this.title = this.templateId ? this.title : this.goal.title; 
        
        this.goal.start_date = moment(res.data.start_date).toDate();
        this.goal.end_date = moment(res.data.end_date).toDate();
        this.start_date = this.goal.start_date;
        this.end_date = this.goal.end_date;
        this.changeStartDate(this.goal.start_date);

        this.editGoalLSKey = `${GLOBAL_CONSTANTS.LOCAL_STORAGE.GOAL.EDIT}${this.goal.id}`;
        this.updateBreadCrumb(this.goalAltName);
      }
      if (res.all_categories) this.categories = res.all_categories;
      if (this.goal.categories) this.formatCategories();
      this.makeCategoryString();
      this.loader = false;



      // saved goal, edit mode
      if (this.editMode && this.isEditGoal) {
        this.restoreTAEditGoalDataFromLS(this.goal.id);
      } else if (!this.editMode && this.isEditGoal) { // new goal adding mode
        this.appMainService.updateBreadCrumb([{ title: this.goalAltName }]);
        this.restoreTAAddGoalDataFromLS(this.goal.title, this.goal.goal_type);
      }


      if (this.goal.goal_type == 4 && this.goalUserSettings.can_create_individual_goals_for_themselves == '1'
        && this.goalUserSettings.can_create_individual_goals_for_others_people == '0' && this.templateId
        && this.goalUserSettings.role_id !== '100' && this.goalUserSettings.role_id !== '110') {
        this.goal.owners = [Number(this.goalDetailsObj.user_id)];
        this.isIndividual = true;
      }

      if (this.goal.id) this.checkEditDeletePermission(this.goal)
    }
    });
  }

  initiateSocket() {
    let channel_name = `goal-${this.goalDetailsObj.goal_id}`;
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(channel_name).subscribe(data => this.processEventSubscriptions(data)));
  }

  formatCategories() {
    this.categories.forEach(category => {
      this.goal.categories.forEach(cat => {
        if (cat.category_id === category.id) {
          category.check = true;
        }
      });
    });
  }




  checkCategory(category) {
    category.check = !category.check;
    this.makeCategoryString()
    if (this.goalDetailsObj.goal_id > 0 && !this.isEditGoal) {
      this.save();
    }
  }

  openAddGoalModal() {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-container-700',
      initialState: {}
    };
    this.bsModalRef = this.modalService.show(AddGoalModalComponent, config);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  openOwnerModal() {
    let owners = [];
    if (this.goal.hasOwnProperty('owners')) {
      owners = this.editGoalService.getCopy(this.editGoalService.formatUsers(this.goal.owners));
    }

    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'owner-modal-container',
      initialState: {
        users: this.editGoalService.getCopy(this.users),
        selectedUsers: owners,
        editMode: this.editMode,
        goal_id: this.goalDetailsObj.goal_id,
        created_by: this.goal.created_by ? this.goal.created_by : 0,
        goal_type: this.goal.goal_type,
        modal: 'owner'
      }
    };
    this.headerService.g_modal_data = config;
    this.bsModalRef = this.modalService.show(OwnerModalComponent, config);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  openCollaboratorModal() {
    let collaborators = [];
    if (this.goal.hasOwnProperty('collaborators')) {
      collaborators = this.editGoalService.getCopy(this.editGoalService.formatUsers(this.goal.collaborators, true, true));
    }
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'collaborators-modal-container',
      initialState: {
        users: this.editGoalService.getCopy(this.users),
        editMode: this.editMode,
        selectedUsers: collaborators,
        goal_id: this.goalDetailsObj.goal_id,
        modal: 'collaborator'
      }
    };
    this.headerService.g_modal_data = config;
    this.bsModalRef = this.modalService.show(CollaboratorModalComponent, config);
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  makeCategoryString() {
    let catString = [];
    this.catLength = 0;
    this.categoryString = ''
    this.categories.forEach(cat => {
      if (cat.check) catString.push(cat.name)
      if (catString.toString().length > 39) {
        catString.pop();
        this.catLength++;
      }
    })
    this.categoryString = catString.join(',  ');
  }

  private checkEditDeletePermission(goal: any) {
    const userId = this.userCurrAcc.User.id;
    const isCollaborator = goal.collaborators.find(collaborator => collaborator.user_id == userId); // user is collaborator
    const isCollaboratorWithPermissionVFR = isCollaborator && (isCollaborator.permission.indexOf(1) >= 0 || isCollaborator.permission.indexOf(3) >= 0 || isCollaborator.permission.indexOf(4) >= 0); // user is collaborator with view or add evidence or review feedback permission
    const isCollaboratorWithPermissionE = isCollaborator && isCollaborator.permission.indexOf(2) >= 0; // user is collaborator with edit permission
    const isOwner = goal.owners.find(owner => owner.user_id == userId);
    const isCreator = goal.created_by == userId;

    this.canEditDelete = isCreator || isCollaboratorWithPermissionE || (goal.goal_type == 3 && isOwner);

    const gs = this.headerData.goal_settings; // goal settings
    const allGoalsPublic = gs.make_all_goals_public;
    const userCanViewGoalProg = Boolean(Number(this.headerData.goal_users_settings.can_view_goal_progress)); //group_type_goals_public

    const accountGoalsPublic = allGoalsPublic || (goal.goal_type == 2 && gs.account_type_goals_public) || userCanViewGoalProg;
    const groupGoalsPublic = allGoalsPublic || (goal.goal_type == 3 && gs.group_type_goals_public) || userCanViewGoalProg;
    const individualGoalsPublic = allGoalsPublic || (goal.goal_type == 4 && gs.individual_type_goals_public) || userCanViewGoalProg;

    let redirectUrl: string;

    if (goal.goal_type != 1 && goal.is_published && (this.currentPageMode === this.pageMode.VIEW) && !this.pagePreviewMode) { // goal is not template and goal is published
      if (goal.goal_type == 2) { // account goal
        if (isCollaboratorWithPermissionVFR || accountGoalsPublic) {
          redirectUrl = `/goals/status/${goal.id}/${goal.goal_type}`;
        } else {
          redirectUrl = `/goals/work/${goal.id}/${goal.goal_type}/${userId}`;
        }
      } else if (goal.goal_type == 3) { // group goal
        if (isCollaboratorWithPermissionVFR || isOwner || groupGoalsPublic) {
          redirectUrl = `/goals/work/${goal.id}/${goal.goal_type}`;
        }
      } else if (goal.goal_type == 4 && goal.owners.length === 1) { // single individual goal
        if (isCollaboratorWithPermissionVFR || isOwner || individualGoalsPublic) {
          redirectUrl = `/goals/work/${goal.id}/${goal.goal_type}/${goal.owners[0].user_id}`;
        }
      } else if (goal.goal_type == 4 && goal.owners.length !== 1) { // multiple individual goal
        if (isCollaboratorWithPermissionVFR || individualGoalsPublic) {
          redirectUrl = `/goals/status/${goal.id}/${goal.goal_type}`;
        } else if (isOwner) {
          redirectUrl = `/goals/work/${goal.id}/${goal.goal_type}/${isOwner.user_id}`;
        }
      }
    } else if (!this.canEditDelete && this.goal.goal_type != 1) {
      redirectUrl = '/goals/list';
    }

    if (redirectUrl) this.router.navigate([redirectUrl]);

  }

  editModeOn() {
    // this.isEditGoal = true;
    // this.editMode = true;
    // this.restoreTAEditGoalDataFromLS(this.goal.id);
    this.router.navigate(['/goals/edit', this.goal.id]);
  }

  changeDateFormat(item) {
    return this.headerService.changeDateFormat(item);
  }

  deleteGoal(goal) {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-container-600',
      initialState: { goal }
    };
    this.modalService.show(DeleteComponent, config);
  }

  changeStartDate(e) {
    if (e) {
      this.goal.start_date = e;
      if (new Date(e).setHours(0, 0, 0, 0) < new Date(this.today).setHours(0, 0, 0, 0)) this.minEndDate = this.today
      else if (new Date(e).setHours(0, 0, 0, 0) == new Date(this.today).setHours(0, 0, 0, 0)) this.minEndDate = this.addDays(this.today, 1);
      else this.minEndDate = this.addDays(e, 1);
      if (this.end_date && new Date(this.end_date).setHours(0, 0, 0, 0) <= new Date(this.start_date).setHours(0, 0, 0, 0)) {
        this.end_date = this.minEndDate;
        this.goal.end_date = this.end_date;
      }
    }

  }

  changeEndDate(e) {
    if (this.start_date && this.end_date) {
      this.goal.end_date = e;
    } else {
      this.end_date = e;
      if (!this.start_date)
        this.start_date = this.today;
      this.goal.start_date = this.start_date;
      this.goal.end_date = e;
      return;
    }
  }

  addDays(date, days) {
    const copy = new Date(Number(date))
    copy.setDate(date.getDate() + days)
    return copy
  }

  cancel(confirmationPopUp: any) {
    this.bsModalRef = this.modalService.show(confirmationPopUp);
  }

  publish() {
    if (this.canBePusblished) {
      if (!this.checkActionItemSaved) {
        
        this.checkActionItemSaved = true;
      }
      else {
        this.goal.is_published = 1;
        this.save();
      }
    }
    else {
      this.toastr.ShowToastr('clear');
      this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goals_one_action_item_required));
      return;
    }
  }

  save() {
    this.headerService.removeLocalStorage(this.addGoalLSKey);
    this.headerService.removeLocalStorage(this.editGoalLSKey);
    let goalOwners; // will be used in try again ls obj
    this.goal.title = this.title;

    if (!this.goal.title) {
      this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goals_title_required));
      return;
    }
    if (this.goal.goal_type != "1") {

      if (!this.start_date) {
        this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goals_start_date_required));
        return;
      }
      if (!this.end_date) {
        this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goals_end_date_required));
        return;
      }
      if (this.goal.start_date > this.goal.end_date) {
        this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goals_item_deadline_error));
        return;
      }
    }

    if (this.goal.id && this.goal.goal_type !='1') {
      let result = false
      this.goal.action_items.forEach(actionItem => {
        if (!(moment(actionItem.deadline).toDate().setHours(0, 0, 0, 0) >= moment(this.start_date).toDate().setHours(0, 0, 0, 0)
          && moment(actionItem.deadline).toDate().setHours(0, 0, 0, 0) <= moment(this.end_date).toDate().setHours(0, 0, 0, 0))) {
          result = true
          return
        }
      });
      if (result) {
        this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goals_action_item_date_out_of_range));
        return
      }
    }



    if (this.goal.goal_type != "2") {
      this.goal.ownerGroupIds = [];
      if (!this.isIndividual) {
        let owners = this.goal.owners || [];
        if (this.goal.owners) goalOwners = JSON.parse(JSON.stringify(this.goal.owners));
        this.goal.owners = [];
        if (owners.length < 1 && this.goal.goal_type != 1) {
          this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goals_owner_required));
          return
        }
        owners.forEach(x => {
          if (x.hasOwnProperty('is_user')) {
            x.is_user ? this.goal.owners.push(x.user_id) : this.goal.ownerGroupIds.push(x.id);
          } else {
            this.goal.owners.push(x.user_id)
          }
        });
      }
    } else {
      delete this.goal.owners;
    }
    this.goal.collaboratorsGroupIds = [];
    let collaborators = this.goal.collaborators;
    this.goal.collaborators = [];
    if (collaborators) {
      collaborators.forEach(x => {
        x.is_user ? this.goal.collaborators.push({ user_id: x.user_id, permission: x.permission }) :
          this.goal.collaboratorsGroupIds.push({ group_id: x.id, permission: x.permission });
      });
    }

    if (this.templateId) {
      this.goal.template_id = this.templateId;
    }
    this.goal.account_id = this.userCurrAcc.users_accounts.account_id;
    this.goal.user_id = this.userCurrAcc.users_accounts.user_id;
    if (typeof this.goal.start_date != 'string')
      this.goal.start_date = this.goal.start_date ? this.editGoalService.getDateString(this.goal.start_date) : null;
    if (typeof this.goal.end_date != 'string')
      this.goal.end_date = this.goal.end_date ? this.editGoalService.getDateString(this.goal.end_date) : null;
    this.goal.goal_id = this.goal.id;
    this.goal.categories = [];
    this.categories.forEach(x => {
      if (x.check) this.goal.categories.push(x.id)
    })
    this.loader = true;




    this.editGoalService.updateGoal(this.goal).subscribe((res: any) => {

      this.goalDetailsObj.goal_id = res.data.id;
      this.checkActionItemSaved = false
      this.isEditGoal = false;
      this.isChangeOccur = false
      this.isChangOccurInChild = false
      this.loader = false;
      this.router.navigate(['/goals/view', res.data.id], { queryParams: { previewMode: 'goal-saved-successfully' } });
    }, () => {
      setTimeout(() => {
        this.loader = false
        this.goal.tryAgain = true;
        this.checkActionItemSaved = false

        const goalData: any = {
          goal_type: this.goal.goal_type,
          title: this.title,
          desc: this.goal.desc,
          start_date: this.goal.start_date,
          end_date: this.goal.end_date
        };

        if (this.goal.id) goalData.goal_id = this.goal.id;
        else {
          goalData.uuid = new Date().getTime();
          this.goal.uuid = goalData.uuid;
          if (this.goal.goal_type != "2") {
            this.goal.owners = goalOwners;
            goalData.goalOwners = goalOwners;
          }
        }

        const taAddEditGoals = this.headerService.getLocalStorage(this.taAddEditGoalsKey) || [];
        const goalIndex = taAddEditGoals.findIndex(taAddEditGoal => taAddEditGoal.goal_id == this.goal.id);
        if (goalIndex > -1) taAddEditGoals[goalIndex] = goalData;
        else taAddEditGoals.push(goalData);
        this.headerService.setLocalStorage2(this.taAddEditGoalsKey, taAddEditGoals);

      }, 200);

    });
  }

  private updateBreadCrumb(title: string) {
    //Updated Breadcrumb
    let breadCrum = [];
    if(this.currentPageMode != 'edit') {
      breadCrum = [{ title: title, link: '/goals/list' },{ title: this.goal?.title }];
    }
    if(this.currentPageMode == 'edit') {
      breadCrum = [{ title: title }];
    }

    this.appMainService.updateBreadCrumb(breadCrum);
  }

  private processEventSubscriptions(data) {
    if (data.event === 'goal_edited') {
      switch (data.action) {
        case 'updated_owners':
          this.goal.owners = data.data.owners
          break;
        case 'updated_collaborators':
          this.goal.collaborators = data.data.collaborators
          break;
        case 'edit':
          this.goal = data.data
          break;
        default:
          break;
      }
    }
    if (data.event === 'goal_deleted') {
      this.router.navigate(['/goals/list']);
    }
  }

  /** Local storage functions starts */
  private restoreTAEditGoalDataFromLS(goalId: number) {
    const taAddEditGoals = this.headerService.getLocalStorage(this.taAddEditGoalsKey);

    let TAEditGoalFound = false;

    if (Array.isArray(taAddEditGoals)) {
      taAddEditGoals.forEach(taAddEditGoal => {
        if (taAddEditGoal.goal_id == goalId) {
          TAEditGoalFound = true;
          this.title = taAddEditGoal.title;
          this.isChangeOccur = taAddEditGoal.isChangeOccur;
          this.isChangOccurInChild = taAddEditGoal.isChangOccurInChild;

          this.goal.tryAgain = true;
          this.goal.goal_type = taAddEditGoal.goal_type;
          this.goal.desc = taAddEditGoal.desc;
          this.goal.start_date = new Date(taAddEditGoal.start_date);
          this.goal.end_date = new Date(taAddEditGoal.end_date);

          this.start_date = this.goal.start_date;
          this.end_date = this.goal.end_date;
          this.changeStartDate(this.start_date);

          this.goal.uuid = (taAddEditGoal.uuid) ? taAddEditGoal.uuid : null;
        }
      });
    }

    if (!TAEditGoalFound) this.restoreEditGoalDataFromLS();
  }

  private restoreTAAddGoalDataFromLS(title: string, goal_type: number) {
    const taAddEditGoals = this.headerService.getLocalStorage(this.taAddEditGoalsKey);

    let TAAddGoalFound = false;

    if (Array.isArray(taAddEditGoals)) {
      taAddEditGoals.forEach(taAddEditGoal => {
        if (taAddEditGoal.title === title && taAddEditGoal.goal_type == goal_type) {
          TAAddGoalFound = true;
          this.title = taAddEditGoal.title;
          this.isChangeOccur = taAddEditGoal.isChangeOccur;
          this.isChangOccurInChild = taAddEditGoal.isChangOccurInChild;

          this.goal.tryAgain = true;
          this.goal.goal_type = taAddEditGoal.goal_type;
          this.goal.desc = taAddEditGoal.desc;
          this.goal.start_date = new Date(taAddEditGoal.start_date);
          this.goal.end_date = new Date(taAddEditGoal.end_date);

          this.start_date = this.goal.start_date;
          this.end_date = this.goal.end_date;
          this.changeStartDate(this.start_date);

          this.goal.uuid = (taAddEditGoal.uuid) ? taAddEditGoal.uuid : null;
          if (this.goal.goal_type != 2 && taAddEditGoal.goalOwners) this.goal.owners = taAddEditGoal.goalOwners;
        }
      });
    }

    if (!TAAddGoalFound) this.restoreAddGoalDataFromLS();
  }

  private restoreEditGoalDataFromLS() {
    const editGoal = this.headerService.getLocalStorage(this.editGoalLSKey);

    if (editGoal) {

      (editGoal.isChangeOccur) ? this.isChangeOccur = editGoal.isChangeOccur : false;
      (editGoal.isChangOccurInChild) ? this.isChangOccurInChild = editGoal.isChangOccurInChild : false;
      (editGoal.title) ? this.title = editGoal.title : null;
      (editGoal.goal_type) ? this.goal.goal_type = editGoal.goal_type : null;
      (editGoal.desc) ? this.goal.desc = editGoal.desc : null;
      // (editGoal.owners) ? this.goal.owners = editGoal.owners : null;

      if (editGoal.start_date) {
        this.start_date = new Date(editGoal.start_date);
        this.changeStartDate(this.start_date);
      }
      if (editGoal.end_date) {
        this.end_date = new Date(editGoal.end_date);
        this.changeEndDate(this.end_date);
      }
    }
  }

  private restoreAddGoalDataFromLS() {
    const addGoal = this.headerService.getLocalStorage(this.addGoalLSKey);

    if (addGoal) {
      (addGoal.isChangeOccur) ? this.isChangeOccur = addGoal.isChangeOccur : false;
      (addGoal.isChangOccurInChild) ? this.isChangOccurInChild = addGoal.isChangOccurInChild : false;
      (addGoal.title) ? this.title = addGoal.title : null;
      (addGoal.goal_type) ? this.goal.goal_type = addGoal.goal_type : null;
      (addGoal.desc) ? this.goal.desc = addGoal.desc : null;
      (addGoal.owners) ? this.goal.owners = addGoal.owners : null;

      if (addGoal.start_date) {
        this.start_date = new Date(addGoal.start_date);
        this.changeStartDate(this.start_date);
      }

      if (addGoal.end_date) {
        this.end_date = new Date(addGoal.end_date);
        this.changeEndDate(this.end_date);
      }
    }
  }

  private removeEditGoalDataFromLS(id: number, key: string) {
    const taAddEditGoals = this.headerService.getLocalStorage(this.taAddEditGoalsKey);

    if (Array.isArray(taAddEditGoals)) {
      const goalIndex = taAddEditGoals.findIndex(taAddEditGoal => taAddEditGoal[key] == id);
      if (goalIndex > -1) {
        taAddEditGoals.splice(goalIndex, 1);
        this.headerService.setLocalStorage(this.taAddEditGoalsKey, taAddEditGoals);
      }
    }
  }

  public retryGoal(goal: any) {

    this.loader = true;
    if (!this.goal.isProcessing) {
      this.goal.isProcessing = true;

      const goalData: any = {
        goal_id: goal.goal_id,
        uuid: (goal.uuid) ? goal.uuid : null,
        goal_type: goal.goal_type,
        title: goal.title,
        desc: goal.desc,
        start_date: goal.start_date,
        end_date: goal.end_date,
        account_id: this.userCurrAcc.users_accounts.account_id,
        user_id: this.userCurrAcc.users_accounts.user_id
      };

      if (Array.isArray(goal.owners)) {
        goalData.owners = [];
        goalData.ownerGroupIds = [];

        goal.owners.forEach(x => {
          (x.is_user) ? goalData.owners.push(x.user_id) : goalData.ownerGroupIds.push(x.id);
        });
      }

      this.editGoalService.updateGoal(goalData).subscribe((res: any) => {
        this.router.navigate(['/goals/view', res.data.id], { queryParams: { previewMode: 'goal-retry-success' } });

        const key = (goalData.goal_id) ? 'goal_id' : 'uuid';
        this.removeEditGoalDataFromLS(goalData[key], key);

      }, () => {
        setTimeout(() => {
          this.loader = false;
          this.goal.isProcessing = false;
        }, 200);

      });
    }
  }
  /** Local storage functions ends */

  public confirmed() {
    this.goalCancelled = true;
    this.isEditGoal = false;
    this.isChangeOccur = false
    this.isChangOccurInChild = false
    if (this.goal.id) this.router.navigate(['/goals/view', this.goal.id], { queryParams: { previewMode: 'cancel-edit-confirmed' } });
    else this.router.navigate(['/goals/list']);

    this.bsModalRef.hide();
    this.headerService.removeLocalStorage(this.addGoalLSKey);
    this.headerService.removeLocalStorage(this.editGoalLSKey);

    if (this.goal.tryAgain) {
      const key = (this.goal.goal_id) ? 'goal_id' : 'uuid';
      this.removeEditGoalDataFromLS(this.goal[key], key);
    }
  }

  useTemplate() {

    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-container-700',
      initialState: {
        useTemplate: false,
        templateId: this.goalDetailsObj.goal_id
      }
    };

    this.addGoalModal = this.modalService.show(AddGoalModalComponent, config);
    this.addGoalModal.content.closeBtnName = 'Close';
  }

  @HostListener("window:beforeunload")
  ngOnDestroy() {
    this.subscriptions.unsubscribe();


    if (!this.goalCancelled) {

      this.goal.start_date = this.start_date;
      this.goal.end_date = this.end_date;

      this.goal.title = this.title;
      this.goal.isChangeOccur = this.isChangeOccur
      this.goal.isChangOccurInChild = this.isChangOccurInChild
      let goalLSKey;

      if (!this.editMode && this.isEditGoal) goalLSKey = this.addGoalLSKey; // new goal adding mode
      else if (this.editMode && this.isEditGoal) goalLSKey = this.editGoalLSKey; // saved goal, edit mode

      if (goalLSKey && !this.goal.tryAgain) {
        this.headerService.setLocalStorage2(goalLSKey, this.goal);
      }

    }
  }

}

