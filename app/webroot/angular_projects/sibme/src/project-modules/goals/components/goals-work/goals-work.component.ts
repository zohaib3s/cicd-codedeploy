import { GoalWorkService } from './../../services/goal-work.service';
import { Component, OnInit, OnDestroy, Pipe } from '@angular/core';
import { GLOBAL_CONSTANTS } from '@constants/constant';
import { HeaderService, AppMainService, SocketService } from '@app/services';
import { EditGoalService } from '@goals/services/edit-goal.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
import { environment } from "@environments/environment";
import { ShowToasterService } from '@projectModules/app/services';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
import { CollaboratorsListModalComponent, OwnersListModalComponent } from "@goals/modals";
import { MainService } from '@src/project-modules/video-page/services';

@Component({
  selector: 'goals-work',
  templateUrl: './goals-work.component.html',
  styleUrls: ['./goals-work.component.css']
})

export class GoalsWorkComponent implements OnInit, OnDestroy {
  today = new Date();
  public afterDueDate: boolean = false;
  public loaded: boolean = false;
  private userCurrAcc: any;
  public goalAltName: string;
  goal_id: any
  goalDetailsObj: { account_id: any; user_id: any; owner_id: any; goal_id: number; goal_type: any };
  headerData: any;
  goal: any;
  private translationSubscription: Subscription;
  translation: any;
  singleGoalData: any = [];
  singleGoalDataCopy: any = [];
  evidenceAllowed: any = [];
  private subscriptions: Subscription = new Subscription();
  public goalsTypeByValue: any = GLOBAL_CONSTANTS.GOALS_TYPE_BY_VALUE;
  public avatarBasePath: any = GLOBAL_CONSTANTS.AMAZON_AVATAR_BASE_PATH;
  goalUserSettings: any;
  goalSettings: any;
  addEvidence = false;
  addReview = false;
  addMeasurement = false
  owner: any;
  goalOwner: any;
  collaborator: any;
  measurement: boolean = false;
  veiwer: boolean = false;
  public getAccessCheck: any;


  constructor(private mainService: MainService, public headerService: HeaderService, private appMainService: AppMainService, public editGoalService: EditGoalService,
    public goalWorkService: GoalWorkService, private socketService: SocketService, private router: Router,
    private toastr: ShowToasterService,private activatedRoute: ActivatedRoute, private modalService: BsModalService) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.goalAltName = this.headerService.getGoalsAltName();
      this.headerData = this.headerService.getStaticHeaderData();
      this.goalUserSettings = this.headerService.getStaticHeaderData('goal_users_settings');
      this.goalSettings = this.headerService.getStaticHeaderData('goal_settings');
      this.goalDetailsObj = {
        account_id: this.headerData.user_current_account.accounts.account_id,
        user_id: this.headerData.user_current_account.users_accounts.user_id,
        owner_id: this.activatedRoute.snapshot.params.owner_id,
        goal_id: this.activatedRoute.snapshot.params.goal_id,
        goal_type: this.activatedRoute.snapshot.params.goal_type
      };
    }));
  }

  ngOnInit() {
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
    this.getGoalDetails();
    this.initiateSocket();
  }
  initiateSocket() {
    let channel_name = `goal-${this.goalDetailsObj.goal_id}`;
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(channel_name).subscribe(data => {
      this.processEventSubscriptions(data)
    }));
  }



  getGoalDetails() {
    this.editGoalService.getGoalDetails(this.goalDetailsObj).subscribe((res: any) => {
      if (res.data) {
        if(!res.goal_owner_setting) {
          let today = new Date().toISOString().slice(0, 10)
          if(res.data.end_date < today) {
            this.afterDueDate = true;
          } else {
            this.afterDueDate = false;
          }
        }
        this.goal = res.data;
       
         //Updated Breadcrumb
        const breadCrum = [{ title: this.goalAltName, link: '/goals/list' },{ title: this.goal?.title }];
        this.appMainService.updateBreadCrumb(breadCrum);
        this.getAccessCheck = res.data.hide_action_item_details;
        this.checkEntryPermission();
        this.getGoalOwner()
        if((this.goal.goal_type == 4 &&  this.goalOwner.is_archived) || (this.goal.goal_type != 4  &&  this.goal.is_archived)){
          this.goal_redirect(this.translation.goals_error_goal_is_archived);
        }
        this.singleGoalData = this.goal.action_items[0];
        this.allowedEvidence(this.singleGoalData);
        this.checkMeasurement(this.goal.action_items);
        this.singleGoalDataCopy = _.cloneDeep(this.singleGoalData);
        this.checkPermissions();
        this.loaded = true;
      } else {
        this.goal_redirect(this.translation.goals_item_no_longer_available);
      }
    });
  }
  goal_redirect(error_message){
    this.toastr.ShowToastr('error',error_message);
    this.router.navigate(['/goals/list']);
  }

  getGoalOwner() {
    this.goalOwner = this.goal.owners.find(x => x.user_id == this.goalDetailsObj.owner_id)
  }

  public checkEditDeletePermission() {
    const userId = this.userCurrAcc.User.id;
    const isCollaborator = this.goal.collaborators.find(collaborator => collaborator.user_id == userId); // user is collaborator
    const isCollaboratorWithPermissionE = isCollaborator && isCollaborator.permission.indexOf(2) >= 0; // user is collaborator with edit permission
    const isCreator = this.goal.created_by == userId;
    const isOwner = this.goal.owners_updated.find(owner => owner.user_id == userId);

    return isCreator || isCollaboratorWithPermissionE || (this.goal.goal_type == 3 && isOwner);
  }
  checkEntryPermission() {
    this.owner = this.goal.owners.find(x => x.user_id == this.userCurrAcc.User.id)

    if(this.goalDetailsObj.goal_type == 4 && this.owner && (this.owner.user_id != this.goalDetailsObj.owner_id)){
      this.owner = null
    }

    this.collaborator = this.goal.collaborators.find(x => x.user_id == this.userCurrAcc.User.id)
    if (this.owner) {
      return;
    }
    if (this.collaborator) {
      this.collaborator.permission = JSON.parse("[" + this.collaborator.permission + "]");
      if (this.collaborator.permission[0]!=2 || this.collaborator.permission.length>1) {
        return;
      }
    }
    const gs = this.headerData.goal_settings; // goal settings
    const allGoalsPublic = gs.make_all_goals_public;
    const accountTypeGoal = this.goal.goal_type == 2;
    const groupGoalsPublic = this.goal.goal_type == 3 && gs.group_type_goals_public;
    const individualGoalsPublic = this.goal.goal_type == 4 && gs.individual_type_goals_public;

    const userCanViewGoalProg = Boolean(Number(this.headerData.goal_users_settings.can_view_goal_progress));

    const userCanViewGoalPublically = allGoalsPublic || accountTypeGoal || groupGoalsPublic || individualGoalsPublic || userCanViewGoalProg;
    if(!userCanViewGoalPublically)
    this.router.navigate(['/goals/list']);
    this.veiwer = true;

  }


  checkPermissions() {
    if (this.owner) {
      this.addEvidence = true
      this.addReview = false
    }
    if (this.collaborator && this.collaborator.permission.includes(3)) {
      this.addEvidence = true
    }
    if (this.collaborator && this.collaborator.permission.includes(4)) this.addReview = true

    if ((this.goalUserSettings.can_enter_data_for_measurement_where_goal_owners =='1' && this.owner)
      || (this.goalUserSettings.can_enter_data_for_measurement_where_goal_reviewers =='1' &&
        this.collaborator && this.collaborator.permission.includes(4)))
      this.addMeasurement = true
      if(this.collaborator && !this.collaborator.permission.includes(4) && !this.collaborator.permission.includes(3) && !this.owner )
      this.veiwer = true;
  }


  changeDateFormat(item) {
    return this.headerService.changeDateFormat(item);
  }

  updateSingleGoalData(obj, index) {console.log('here')
      if(!obj.reflection)
      {
          const postData = {
              ...this.goalDetailsObj,
              item_id : obj.id
          };
          this.editGoalService.getActionItemRF(postData).subscribe((res: any) => {
              if (res.data) {
                  obj.reflection = res.data.reflection;
                  obj.feedback = res.data.feedback;
                  this.goal.action_items[index].reflection = obj.reflection;
                  this.goal.action_items[index].feedback = obj.feedback;
                  this.updateSingleGoalDataMain(obj)
              }
              else
              {
                  this.updateSingleGoalDataMain(obj)
              }
          });
      }
      else
      {
          this.updateSingleGoalDataMain(obj)
      }
  }
    
    updateSingleGoalDataMain(obj)
    {
        this.singleGoalData = obj;
        this.singleGoalDataCopy = _.cloneDeep(this.singleGoalData)
        this.allowedEvidence(obj);
    }

  checkIncomplete(item) {
    if (moment(item.deadline).toDate().setHours(0, 0, 0, 0) < moment(this.today).toDate().setHours(0, 0, 0, 0))
      return true;
    else return false
  }
  checkPending(item) {
    if (moment(item.deadline).toDate().setHours(0, 0, 0, 0) >= moment(this.today).toDate().setHours(0, 0, 0, 0))
      return true;
    else return false
  }

  getGoalStatus(){
    let progress = this.goal.progress;
    const totalItems = this.goal.action_items.length;
    const share =  (1/totalItems*100).toFixed(0);
    this.goal.action_items.forEach(element => {
      if(element.is_done != 1){
        const itemProgress = this.checkItemProgress(element);
        if(typeof itemProgress !== "boolean" && typeof itemProgress === "number"){
          progress += (parseInt(share)*itemProgress/100);
        }

      }
          });
    return Math.floor(progress);
  }
  checkItemProgress(item){
    if(item.add_measurement == 1){
      if(item.start_value == item.current_value){
      return 0;
      }else if(item.end_value > item.start_value){
        if(item.current_value <= item.start_value){
          return 0;
        } else if(item.current_value >= item.end_value){
          return 100;
        }else{
          let percentage=  Math.floor((item.current_value-item.start_value)/(item.end_value - item.start_value)*100);
          return percentage;
        }

      }else if(item.start_value > item.end_value){
        if(item.current_value >= item.start_value){
          return 0;
        }else if(item.current_value <= item.end_value){
          return 100;
        }else{
          return Math.floor((item.start_value - item.current_value)/(item.start_value - item.end_value) * 100);
        }
      }
      return true;
    }else{
      return false;
    }
  }
  allowedEvidence(obj) {
    this.evidenceAllowed =[];
    for (let i = 0; i < obj?.allowed_evidence?.length; i++) {
      this.evidenceAllowed.push(obj?.allowed_evidence[i].evidence_type)
    }
  }


  private processEventSubscriptions(res) {
    switch (res.event) {
      case 'item_created':
        this.item_created(res.data);
        break;
      case 'item_deleted':
        this.item_deleted(res.data);
        break;
      case 'goal_deleted':
        this.goal_deleted(res.data);
        break;
      case 'goal_edited':
        if (res.action == 'review_added')
          this.reviewAdded(res);
        break;
      case 'evidence_added':
        this.evidenceAdded(res)
        break;
      case 'evidence_removed':
        this.evidenceRemoved(res);
        break;
      default:
        break;
    }
  }


  evidenceAdded(res) {
    if(this.goalDetailsObj.goal_type !=4 || (this.goalDetailsObj.goal_type ==4 && this.goalDetailsObj.owner_id == res.data.owner_id)){
    this.goal.action_items.forEach(x => {
      if (x.id == res.data.goal_item_id) {
        x.submitted_evidence = x.submitted_evidence || []; // incase if there are no submitted_evidence
        x.submitted_evidence.unshift(res.data)
      }
    })
    if (this.singleGoalDataCopy.id == res.data.goal_item_id) {
      this.singleGoalDataCopy.submitted_evidence = this.singleGoalDataCopy.submitted_evidence || []; // incase if there are no submitted_evidence
      this.singleGoalDataCopy.submitted_evidence.unshift(res.data)
    }
  }
  }
evidenceRemoved(res){
  this.goal.action_items.forEach(x => {
  if (x.id == res.item_id) {
    this.singleGoalData.submitted_evidence = this.singleGoalData.submitted_evidence.filter(x => x.id !== res.evidence_id)}
})
if (this.singleGoalDataCopy.id == res.item_id) {
  this.singleGoalDataCopy.submitted_evidence = this.singleGoalDataCopy.submitted_evidence.filter(x => x.id !== res.evidence_id);
}
}
  reviewAdded(res) {
    let actionItem = res.data.action_items.filter(x => x.id == res.goal_item_id);
    if(this.goalDetailsObj.goal_type !=4 || (this.goalDetailsObj.goal_type ==4 && this.goalDetailsObj.owner_id == actionItem[0].owner_id)){
    this.goal.action_items = this.goal.action_items.map(x => (x.id == actionItem[0].id) ? actionItem[0] : x);
    if (this.singleGoalData.id == actionItem[0].id) {
      this.singleGoalData = actionItem[0];
      this.singleGoalDataCopy = _.cloneDeep(this.singleGoalData)
    }
    this.goal.progress = res.data.progress
  }

  }
  item_deleted(res) {
    this.goal.action_items = this.goal.action_items.filter(x => x.id !== res)
  }

  item_created(res) {
    this.goal.action_items.unshift(res)
  }

  goal_deleted(res) {
    this.router.navigate(['/goals/list']);
  }

  public getAvatarPath(image: string, userId: number) {
    if (image) {
      if (image.indexOf('https') === 0) return image;
      else return `${this.avatarBasePath}${userId}/${image}`;
    } else return `${environment.homeUrl}assets/img/photo-default.png`;
  }
  checkMeasurement(actionItem) {
    for (var i = 0; i < actionItem.length; i++) {
      if (actionItem[i].add_measurement == 1) {
        this.measurement = true;
        break;
      }
    }
  }

  public displayCollaborators(collaborators: any[]) {
    if(collaborators.length > 0 ) {
      if (collaborators.length === 1 && collaborators[0].user_id == this.userCurrAcc.User.id) return;
      const initialState = { collaboratorsList: collaborators };
      this.modalService.show(CollaboratorsListModalComponent, { initialState, class: 'collaborators-list-popup' });
    }
  }

  public displayOwners(owners: any[]) {
    if (owners.length === 1 && owners[0].user_id == this.userCurrAcc.User.id) return;
    const initialState = { ownersList: owners };
    this.modalService.show(OwnersListModalComponent, { initialState, class: 'owners-list-popup' });
  }

  public withoutTime(dateTime) {
    if(dateTime)
    {
      var date = new Date(new Date(dateTime).getTime());
      date.setHours(0, 0, 0, 0);
      return date;
    }
    return 0;
  }

  public removeDeletedActionItem(event) {
    console.log('Goals items', this.goal.action_items);
    console.log('Deleted Item', event);

    this.goal.action_items = this.goal.action_items.filter(x => x.id !== event.id)
    // this.router.navigateByUrl(this.router.url);

    this.goal.action_items.length > 0 ? this.updateSingleGoalData(this.goal.action_items[0], 0) : false;
    // console.log('Updated ', this.goal.action_items.filter(x => x.id !== event.id))

    // this.goal.action_items.forEach(x, i => {
    //   if (x.id === event.id) {
    //     this.goal.action_items.splice(i,1)
    //   }
    // })
  }

  public exportGoal(format) {
    const obj = {
      ...this.goalDetailsObj,
      format
    };
    this.mainService.exportActionItems(obj);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}

