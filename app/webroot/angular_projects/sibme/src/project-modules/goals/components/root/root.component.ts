/** Container component for goals module - Used as router outlet */

import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';

@Component({
  selector: "goals-root",
  templateUrl: "./root.component.html",
  styleUrls: ["./root.component.css"]
})
export class RootComponent implements OnInit, OnDestroy {

  /** TODO:goals Delete the following code for dynamically loading and unloading of css files (if not being used due to components based styling) */

  // private styles = [
  //   { id: 'videoDetailGlobal', path: 'assets/video-page/css/global.css' },
  //   { id: 'videoDetailAnimate', path: 'assets/video-page/css/animate.min.css' },
  //   { id: 'videoDetailExport', path: 'assets/video-page/css/export.css' },
  //   { id: 'videoDetailVJSMarkers', path: 'assets/video-page/css/vjsmarkers.min.css' },
  //   { id: 'videoHuddleCustom2', path: 'assets/video-huddle/css/custom-2.css' },
  // ];

  constructor() { }

  ngOnInit() {
    // this.styles.forEach(style => {
    //   this.loadCss(style);
    // });
  }

  ngOnDestroy() {
    // this.styles.forEach(style => {
    //   let element = document.getElementById(style.id);
    //   element.parentNode.removeChild(element);
    // });
  }

  private loadCss(style: any) {
    // let head = document.getElementsByTagName('head')[0];
    // let link = document.createElement('link');
    // link.id = style.id;
    // link.rel = 'stylesheet';
    // link.type = 'text/css';
    // link.href = style.path;
    // head.appendChild(link);
  }


}
