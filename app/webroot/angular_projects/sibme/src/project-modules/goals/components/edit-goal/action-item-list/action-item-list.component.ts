import { Component, OnInit, EventEmitter, Input, OnDestroy, HostListener, ViewChild, SimpleChanges, OnChanges, Output } from '@angular/core';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import * as _ from 'lodash';
import { DragulaService } from 'ng2-dragula';
import { EditGoalService } from '@goals/services';
import { SocketService, HeaderService, AppMainService } from '@app/services';

import { GLOBAL_CONSTANTS } from "@constants/constant";
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'action-item-list',
  templateUrl: './action-item-list.component.html',
  styleUrls: ['./action-item-list.component.css']
})
export class ActionItemListComponent implements OnInit, OnDestroy, OnChanges {
  bsModalRef: BsModalRef;
  deleteModalRef: BsModalRef;
  @Input() actionItems: any = [];
  @Input() goal_id: any;
  @Input() account_id: any;
  @Input() user_id: any;
  @Input() start_date: any;
  @Input() end_date: any;
  @Input() goal_type: any;
  @Input() is_published: any;
  @Input() checkActionItemSaved: any;
  @Output() notifyParent: EventEmitter<any> = new EventEmitter();
  @Output() editModeOn: EventEmitter<any> = new EventEmitter();
  @Output() isChangeOccur: EventEmitter<any> = new EventEmitter();

  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  actionItemModel: any = {};
  title: any;
  addActionItemModel: any = {};
  add_mode: boolean;
  public goalSettings: any;
  public itemAltName: string;
  itemCopy: any = {};
  public translation: any = {};
  public goalAltName: string;
  private subscriptions: Subscription = new Subscription();
  goalUserSettings: any;
  private userCurrAcc: any;
  private addActionItemLSKey: string = '';
  private editActionItemLSKey: string = '';
  private taAddEditActionItemsKey: string = '';
  confirmation: any;
  deleteItem: any;
  template_id = null;
  actionItemBeingEdited: any;
  editAction: any;
  addNew: any;
  @ViewChild('deleteTemplate', { static: false }) deleteTemplate;
  @ViewChild('publishDailogue', { static: false }) publishDailogue;
  @ViewChild('template', { static: false }) template;


  constructor(private modalService: BsModalService, public editGoalService: EditGoalService,
    private socketService: SocketService, private toastr: ShowToasterService, private appMainService: AppMainService,
     public headerService: HeaderService , private activatedRoute: ActivatedRoute,
     private DG: DragulaService, private router: Router) {

    this.add_mode = false;
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.goalSettings = this.headerService.getStaticHeaderData('goal_settings');
      this.itemAltName = this.headerService.getItemAltName();
      this.goalAltName = this.headerService.getGoalsAltName();
      this.goalUserSettings = this.headerService.getStaticHeaderData('goal_users_settings');
    }));
  }

  ngOnInit() {
    
    this.DG.dropModel("Items").subscribe((args:any) => {
      this.actionItems = args.targetModel
      this.actionItems.map(value => value.user_id = this.user_id)

      let payload = {"actionItems" : this.actionItems}
       this.editGoalService.sortActionItems(payload).subscribe((res:any) =>{
        console.log(res)
       })
    })

    this.activatedRoute.queryParams.subscribe(param => {
      this.template_id = param.templateId;
      this.editAction = param.editAction;

      if(param.addNew) {
        this.addNew = param.addNew;
        setTimeout(() => {
          this.openModal(this.template);
          }, 1000);
        
      }
      if (param.editAction && this.actionItems.length > 0) {

        const index = this.actionItems.findIndex(x => x.id === parseInt(param.editAction))

        this.actionItems[index].edit_mode = true;
        this.actionItems[index].preview_mode = false;
        setTimeout(() => {
        this.edit(this.actionItems[index]);
        }, 1000);
      }
    });

    this.actionItemModel.allowed_evidence = [];
    this.addActionItemModel.allowed_evidence = [];
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '7',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    };
    this.initiateSocket();
    this.canPublishGoal();

    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
    this.taAddEditActionItemsKey = `${GLOBAL_CONSTANTS.LOCAL_STORAGE.GOAL.ACTION_ITEM.ADD_EDIT_TA}${this.goal_id}`;
    this.addActionItemLSKey = `${GLOBAL_CONSTANTS.LOCAL_STORAGE.GOAL.ACTION_ITEM.ADD}${this.goal_id}`;
    this.editActionItemLSKey = `${GLOBAL_CONSTANTS.LOCAL_STORAGE.GOAL.ACTION_ITEM.EDIT}${this.goal_id}`;
    this.restoreTAAddEditActionDataFromLS();

  }

  initiateSocket() {
    let channel_name = `goal-${this.goal_id}`;
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(channel_name).subscribe(data => this.processEventSubscriptions(data)));
  }

  addActionItem() {
    this.title =this.title.trim();
    if(this.title.length>0){
    this.addActionItemModel.title = this.title;
    this.add_mode = true;

    this.restoreAddActionItemDataFromLS();
    this.bsModalRef.hide();
    }
    else{
      this.toastr.ShowToastr('info',this.translation.task_not_empty);
    }
  }

  changeOccur(res){
    this.isChangeOccur.emit(res);
  }

  resetValues() {
    this.addActionItemModel = {};
    this.addActionItemModel.allowed_evidence = [];
    this.title = '';
    this.add_mode = false;
  }

  openModal(template) {
    if (this.addNew) {
      console.log('Modal')
      const config: ModalOptions = {
        backdrop: 'static',
        keyboard: false,
        class: 'modal-md modal-container-700',
      };
      this.bsModalRef = this.modalService.show(template, config);
    }
    else if (this.checkIfChangesSaved(false, true)) {
      this.collapsePreviewMode()
      this.resetValues();

      // restoring title from localstorage
      const addActionItemLSObj = this.headerService.getLocalStorage(this.addActionItemLSKey);
      if (addActionItemLSObj && addActionItemLSObj.title) this.title = addActionItemLSObj.title;
      const config: ModalOptions = {
        backdrop: 'static',
        keyboard: false,
        class: 'modal-md modal-container-700',
      };
      this.bsModalRef = this.modalService.show(template, config);
    }
  }

  addEvidence(check, value, model) {
    check == true ? model.allowed_evidence.push(value) : model.allowed_evidence = model.allowed_evidence.filter(x => x !== value)
  }

  canPublishGoal() {
    this.actionItems && this.actionItems.length > 0 ?
      this.editGoalService.$canBePusblishedSubject.next(true) : this.editGoalService.$canBePusblishedSubject.next(false)
  }

  collapseEditMode() {
    this.actionItems.forEach(item => {
      item.edit_mode = false;
    });
  }

  public cancelForm(confirmationPopUp: any, actionItemBeingEdited?: any) {
    if (actionItemBeingEdited ) this.actionItemBeingEdited = actionItemBeingEdited;
    this.bsModalRef = this.modalService.show(confirmationPopUp);
  }

  cancelEditMode(actionItem: any) {
    this.bsModalRef.hide();
    this.collapseEditMode();
    this.editModeOn.emit(false)
    const editActionItemLSObj = this.headerService.getLocalStorage(this.editActionItemLSKey);
    if (editActionItemLSObj && editActionItemLSObj.id == actionItem.id) {
      this.headerService.removeLocalStorage(this.editActionItemLSKey);
    }
    this.editAction ? this.router.navigate(['/goals', 'edit', this.goal_id]) : false;
  }

  cancelAddMode() {
    this.bsModalRef.hide();
    setTimeout(() => this.add_mode = false, 500);
    this.headerService.removeLocalStorage(this.addActionItemLSKey);
  }

  public cancelAddFromPopup() {
    this.bsModalRef.hide();
    this.headerService.removeLocalStorage(this.addActionItemLSKey);
  }

  collapsePreviewMode(item?, click?) {
    if (this.checkIfChangesSaved(false,click)) {
      this.actionItems.forEach(x => {
        if (item && item.id === x.id) item.preview_mode = !item.preview_mode;
        else
          x.preview_mode = false;
      });
    }
  }

  checkIfChangesSaved(showError?: boolean, click?) {
    let result = true;
    let arr = this.actionItems.filter(x => x.edit_mode === true)
    if (arr.length > 0) {
      if ((click === true && this.editAction) || (click === true && !this.editAction))  {
        result = false;
      }else {
        result = true;
      }
    } else
      if (this.add_mode) {
        result = false;
      }
      else result = true;
      if(showError)
      return result
    if (!result)
      this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goals_item_save_changes));
    return result
  }

  edit(item) {
    if (this.checkIfChangesSaved(false, false)) {
      this.itemCopy = _.cloneDeep(item);
      this.collapsePreviewMode()
      item.edit_mode = true
      this.editModeOn.emit(true);

      // check if local storage for this item exists
      const editActionItemLSObj = this.headerService.getLocalStorage(this.editActionItemLSKey);
      if (editActionItemLSObj && editActionItemLSObj.id == item.id) {
        this.actionItemModel = editActionItemLSObj;
        this.actionItemModel.deadline = moment(this.actionItemModel.deadline).toDate();
      } else {
        this.actionItemModel = _.cloneDeep(item);
        this.actionItemModel.deadline = moment(this.actionItemModel.deadline).toDate();
        this.actionItemModel.allowed_evidence = [];
        item.allowed_evidence.forEach(element => {
          this.actionItemModel.allowed_evidence.push(element.evidence_type);
        });;
      }

    }
  }

  checkEvidenceType(model, value) {
    return model.allowed_evidence.includes(value)
  }

  allowedEvidence(evidence, value) {
    return evidence.evidence_type === value
  }

  changeDateFormat(item) {
    return this.headerService.changeDateFormat(item);
  }

  cancel() {
    this.collapseEditMode();
  }

  deleteActionItem(item, is_confirmed?) {

    if(this.is_published == 1 && this.actionItems.length == 1){
      this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goals_atleast_one_action_item))
      return;
    }
    this.deleteItem = _.cloneDeep(item);
    let obj = {
      goal_id: this.goal_id,
      item_id: item.id,
      user_id: this.user_id
    }
    if(is_confirmed){
      this.deleteModalRef.hide()
      obj['is_confirmed'] = is_confirmed;
    }
    this.editGoalService.deleteGoalItem(obj).subscribe((res: any) => {
      if(res.success)
      this.toastr.ShowToastr('success',this.headerService.getGoalAltTranslation(this.translation.goal_item_deleted_successfully))
      else
      this.deleteModalRef = this.modalService.show(this.deleteTemplate, { class: 'modal-md modal-container-700' });
    });
  }
  hideActionItemSaveModal(){
    this.notifyParent.emit(false);
    this.bsModalRef.hide()
  }

  checkIfActionItemSaved(){
        const res = this.checkIfChangesSaved(true)

        if(res){
          console.log('result1: ', res);
          this.notifyParent.emit(true);
        }else{
          const config: ModalOptions = {
            backdrop: 'static',
            keyboard: false,
            class: 'modal-md modal-container-700',
          };
          this.bsModalRef = this.modalService.show(this.publishDailogue, config);
        }
      }

  save(model) {

    if (!model.title) {
      this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goals_item_require_title))
      return
    }

    if (!model.deadline && this.goal_type !=1) {
      this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goals_item_require_deadline))
      return
    }

    if (!this.start_date || !this.end_date) {
      this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goals_require_start_date_and_end_date))
      return
    }

    // if (model.allowed_evidence.length < 1) {
    //   this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goals_item_require_one_item))
    //   return
    // }

    if (model.add_measurement) {
      if (model.start_value <= -1 || (model.end_value < 1 && model.start_value < model.end_value) || (model.end_value=='' && isNaN(model.end_value))
      || model.start_value==null || model.end_value==null) {
        this.toastr.ShowToastr('error',
          this.headerService.getGoalAltTranslation(this.translation.goals_item_measurement_require_starting_ending_value)
        )
        return
      }
       //Jeff RItter asked to remove the check start value is less the end value
      /*if(model.start_value > model.end_value){
        this.toastr.ShowToastr('error',
          this.headerService.getGoalAltTranslation(this.translation.start_end_value_error)
        )
        return
      }*/
    }

    if ((new Date(model.deadline).setHours(0, 0, 0, 0) < new Date(this.start_date).setHours(0, 0, 0, 0)
      || new Date(model.deadline).setHours(0, 0, 0, 0) > new Date(this.end_date).setHours(0, 0, 0, 0)) && this.goal_type !=1) {
      this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.goal_item_deadline_cannot_before_start_end_date));
      return;
    }

    this.collapseEditMode();

    let actionItem: any = JSON.parse(JSON.stringify(model));
    actionItem.deadline = actionItem.deadline ? this.editGoalService.getDateString(new Date(actionItem.deadline)) : null;
    actionItem.account_id = this.account_id;
    actionItem.user_id = this.user_id;
    actionItem.goal_id = this.goal_id;
    actionItem.item_id = actionItem.id || 0;
    this.add_mode = false;
    this.editGoalService.updateGoalItem(actionItem).subscribe(() => {
      this.editAction ? this.router.navigate(['/goals', 'edit', this.goal_id]) : false;
      if(this.editAction) {
        this.appMainService.updateBreadCrumb([{ title: this.goalAltName }]);
      }
      if(this.checkActionItemSaved)
      this.notifyParent.emit(true);
      this.editModeOn.emit(false)
      this.isChangeOccur.emit(false)
      if(this.bsModalRef) this.bsModalRef.hide()
      if (actionItem.item_id === 0) {
        this.headerService.removeLocalStorage(this.addActionItemLSKey);
      } else {
        const editActionItemLSObj = this.headerService.getLocalStorage(this.editActionItemLSKey);
        if (editActionItemLSObj && editActionItemLSObj.id == actionItem.id) {
          this.headerService.removeLocalStorage(this.editActionItemLSKey);
        }
      }
    }, () => {
      if(this.bsModalRef) this.bsModalRef.hide()
      actionItem.tryAgain = true;
      actionItem.edit_mode = false;
      actionItem.uuid = new Date().getTime();
      this.add_mode = false;

      // Insert/Update action item into this.actionItems array
      const AIIndex = this.actionItems.findIndex(item => item.id == actionItem.id);
      if (AIIndex > -1) this.actionItems[AIIndex] = actionItem;
      else this.actionItems.push(actionItem);

      // Insert/Update action item into local storage
      const taAddEditActionItems = this.headerService.getLocalStorage(this.taAddEditActionItemsKey) || [];
      const actionItemIndex = taAddEditActionItems.findIndex(taAddEditActionItem => taAddEditActionItem.id == actionItem.item_id);
      if (actionItemIndex > -1) taAddEditActionItems[actionItemIndex] = actionItem;
      else taAddEditActionItems.push(actionItem);
      this.headerService.setLocalStorage2(this.taAddEditActionItemsKey, taAddEditActionItems);

    });

  }

  private processEventSubscriptions(data) {
    switch (data.event) {
      case 'item_edited':
        this.actionItems = this.actionItems.map(x => (x.id === data.data.id) ? data.data : x)
        this.canPublishGoal();
        break;
      case 'item_created':
        this.processActionItemCreated(data);
        break;
      case 'item_deleted':
        this.actionItems = this.actionItems.filter(x => x.id !== data.data)
        this.canPublishGoal();
        break;
      default:
        break;
    }
  }

  private processActionItemCreated(data: any) {
    if (data.uuid) {
      const actionItemIndex = this.actionItems.findIndex(actionItem => actionItem.uuid == data.uuid);
      if (actionItemIndex > -1) this.actionItems[actionItemIndex] = data.data;
    } else {
      this.actionItems.push(data.data)
    }
    this.canPublishGoal();
  }

  /** Local storage functions starts */
  private restoreAddActionItemDataFromLS() {
    const addActionItemLSObj = this.headerService.getLocalStorage(this.addActionItemLSKey);
    if (addActionItemLSObj) {
      this.addActionItemModel.description = addActionItemLSObj.description;
      if(addActionItemLSObj.allowed_evidence){
        this.addActionItemModel.allowed_evidence = addActionItemLSObj.allowed_evidence
      } else {
        if(this.goal_type==4 && this.goalUserSettings.can_create_individual_goals_for_others_people == '0'
         && this.goalUserSettings.can_create_individual_goals_for_themselves == '1'
          && this.goalUserSettings.can_use_frameworks_in_goal_evidence == '0'){
            this.addActionItemModel.allowed_evidence =  [1, 2, 3, 5];
          } else {
            this.addActionItemModel.allowed_evidence =  [1, 2, 3, 4, 5];
          }
      }
      this.addActionItemModel.add_measurement = addActionItemLSObj.add_measurement;
      this.addActionItemModel.measurement_type = (addActionItemLSObj.measurement_type) ? addActionItemLSObj.measurement_type : 1;
      this.addActionItemModel.start_value = addActionItemLSObj.start_value;
      this.addActionItemModel.end_value = addActionItemLSObj.end_value;
      
      this.addActionItemModel.deadline = (moment(addActionItemLSObj.deadline).toDate()) ? moment(addActionItemLSObj.deadline).toDate() : null;
    } else {
      if(this.goal_type==4 && this.goalUserSettings.can_create_individual_goals_for_others_people == '0'
         && this.goalUserSettings.can_create_individual_goals_for_themselves == '1'
          && this.goalUserSettings.can_use_frameworks_in_goal_evidence == '0'){
            this.addActionItemModel.allowed_evidence =  [1, 2, 3, 5];
          } else {
            this.addActionItemModel.allowed_evidence =  [1, 2, 3, 4, 5];
          }
      this.addActionItemModel.measurement_type = 1;
    }
  }

  private restoreTAAddEditActionDataFromLS() {
    const taAddEditActionItems = this.headerService.getLocalStorage(this.taAddEditActionItemsKey);

    if (Array.isArray(taAddEditActionItems)) {
      taAddEditActionItems.forEach(taAddEditActionItem => {
        const actionItemIndex = this.actionItems.findIndex(actionItem => actionItem.id == taAddEditActionItem.id);
        if (actionItemIndex > -1) this.actionItems[actionItemIndex] = taAddEditActionItem;
        else this.actionItems.push(taAddEditActionItem);
      });
    }
  }

  private removeTAAddEditActionDataFromLS(id: number, key: string) {
    const taAddEditActionItems = this.headerService.getLocalStorage(this.taAddEditActionItemsKey);

    if (Array.isArray(taAddEditActionItems)) {
      const actionItemIndex = taAddEditActionItems.findIndex(taAddEditActionItem => taAddEditActionItem[key] == id);
      if (actionItemIndex > -1) {
        taAddEditActionItems.splice(actionItemIndex, 1);
        this.headerService.setLocalStorage(this.taAddEditActionItemsKey, taAddEditActionItems);
      }
    }
  }

  public retryActionItem(actionItem: any) {
    if (!actionItem.isProcessing) {
      actionItem.isProcessing = true;
      this.editGoalService.updateGoalItem(actionItem).subscribe(() => {
        const key = (actionItem.id) ? 'id' : 'uuid';
        this.removeTAAddEditActionDataFromLS(actionItem[key], key);
      }, () => {
        actionItem.isProcessing = false;
      });
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.checkActionItemSaved?.currentValue){
      this.checkIfActionItemSaved()
    }
  }


  /** Local storage functions ends */

  @HostListener("window:beforeunload")
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    if (this.add_mode) {
      this.headerService.setLocalStorage2(this.addActionItemLSKey, this.addActionItemModel);
    } else if(Array.isArray(this.actionItems)) {
      this.actionItems.forEach(actionItem => {
        if (actionItem.edit_mode) {
          this.headerService.setLocalStorage2(this.editActionItemLSKey, this.actionItemModel);
        }
      });
    }
  }
}


