import { Component, OnInit, OnDestroy, HostListener, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import * as jsPDF from 'jspdf'
import 'jspdf-autotable';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { HeaderService, AppMainService, } from "@app/services";
import { GoalsService } from "@goals/services";
import { GLOBAL_CONSTANTS } from "@constants/constant";
import { environment } from "@environments/environment";
import { MainService } from '@src/project-modules/video-page/services';
import { ShowToasterService } from '@projectModules/app/services';

@Component({
  selector: 'goals-list-details',
  templateUrl: './goals-list-details.component.html',
  styleUrls: ['./goals-list-details.component.css']
})
export class GoalsListDetailsComponent implements OnInit, OnDestroy {

  public loaded: boolean = false;
  public paginationLoading: boolean = false;
  public sortLoading: boolean = false;
  public data: any;
  private goalId: number;
  private goalType: number;
  public userCurrAcc: any;
  public goalAltName: string;
  public usersSortBy: any = JSON.parse(JSON.stringify(GLOBAL_CONSTANTS.GOAL_USER_SORT_BY)); // deep copy, so that constant default value must not be changed
  public goalsTypeByValue: any = GLOBAL_CONSTANTS.GOALS_TYPE_BY_VALUE;
  public avatarBasePath: any = GLOBAL_CONSTANTS.AMAZON_AVATAR_BASE_PATH;
  private currentPage: number = 0;
  private currentPageRecords: number;
  private recordsPerPage: number = 15;
  private sortBy: string = null;
  @Input() params;
  fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  fileExtension = '.xlsx';

  private subscriptions: Subscription = new Subscription();
  public translation: any = {};
  private headerData: any;

  @HostListener("window:scroll", ["$event"])
  onScroll() {
    if (window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2) { // scroll reached on bottom
      const doc = document.documentElement;
      const currentScroll = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
      if (!this.paginationLoading && this.recordsPerPage <= this.currentPageRecords) {
        this.paginationLoading = true;
        this.currentPage++;
        this.onFiltersChage(this.currentPage, true);
        setTimeout(() => window.scroll(0, currentScroll + 100), 100);
      }
    }
  }

  constructor(private headerService: HeaderService, private appMainService: AppMainService,
    private mainService: MainService, private toastr: ShowToasterService,
    private goalsService: GoalsService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.goalAltName = this.headerService.getGoalsAltName();
    
    }));

    this.subscriptions.add(this.headerService.behaviorialHeaderData$.subscribe(headerData => this.headerData = headerData));

    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(param => {
      this.goalId = param.goalId;
      this.goalType = param.goalType;
      this.getGoalStatus(this.goalId, this.goalType);
    });
  }

  private getGoalStatus(goalId: number, goalType: number) {
    const data = {
      account_id: this.userCurrAcc.accounts.account_id,
      goal_id: goalId,
      goal_type: goalType,
      enable_pagination: true
    };

    this.goalsService.getGoalStatus(data).subscribe((res: any) => {
      console.log(res);
      if (res.status) {

        this.currentPageRecords = res.data.user_data.length;
    
        this.data = res.data;
        const all_UsersArchive=true;
        this.data.user_data
        if((goalType == 4 &&  this.currentPageRecords < 1) || (goalType != 4  &&  this.data.goal_data.is_archived)){
          this.goal_redirect(this.translation.goals_error_goal_is_archived);
        }
        this.data.user_data = this.formatGoalsArray(this.data.user_data, this.data.goal_data.total_items,this.data.goal_data);
        const breadCrum = [{ title: this.goalAltName, link: '/goals/list' },{ title: this.data.goal_data?.title }];
        this.appMainService.updateBreadCrumb(breadCrum);
        this.loaded = true;
      } else {
        setTimeout(() => this.router.navigate(['/goals/list']), 500);
      }
    });
  }
  parseDate(date){
    // parse the date to replace the offset as it is not parsed by safari
     if (date) {
       return date.replace(/\s/g, "T")
     } else return false;
     
   }
   goal_redirect(error_message){
    this.toastr.ShowToastr('error',error_message);
    this.router.navigate(['/goals/list']);
  }
  public onFiltersChage(currentPage: number = 0, fromScroll: boolean = false) {
    this.currentPage = currentPage;
    if (!fromScroll) {
      this.sortLoading = true;
    }
    const data = {
      account_id: this.userCurrAcc.accounts.account_id,
      goal_id: this.goalId,
      goal_type: this.goalType,
      enable_pagination: true,
      sort_by: this.sortBy,
      page: this.currentPage
    };

    this.goalsService.getGoalStatus(data).subscribe((res: any) => {
      if (res.status) {
        this.currentPageRecords = res.data.user_data.length;

        const userData = this.formatGoalsArray(res.data.user_data, this.data.goal_data.total_items,this.data.goal_data);
        if (this.currentPage > 0) this.data.user_data = [...this.data.user_data, ...userData];
        else this.data.user_data = userData;

        this.paginationLoading = false;
        this.sortLoading = false;

      } else {
        console.error('Error in getGoalStatus: ', res)
      }
    });

  }

  checkItemProgress(item){
    if(item.add_measurement == 1){
      if(item.start_value == item.current_value){
      return 0;
      }else if(item.end_value > item.start_value){
        if(item.current_value <= item.start_value){
          return 0;
        } else if(item.current_value >= item.end_value){
          return 100;
        }else{
          let percentage=  Math.floor((item.current_value-item.start_value)/(item.end_value - item.start_value)*100);
          return percentage;
        }

      }else if(item.start_value > item.end_value){
        if(item.current_value >= item.start_value){
          return 0;
        }else if(item.current_value <= item.end_value){
          return 100;
        }else{
          return Math.floor((item.start_value - item.current_value)/(item.start_value - item.end_value) * 100);
        }
      }
      return true;
    }else{
      return false;
    }
  }

  private formatGoalsArray(userArray: any, total_items: number, goal:any) {
    userArray.forEach(item => {
      // Calculate completion percentage
      if (total_items && total_items > 0) {
        //item.percentage = Math.round((item.completed_items / total_items) * 100);
        //item.percentage = `${item.percentage}%`;
        item.percentage = 0;
        const share = (100/total_items).toFixed(0);
        goal.action_items.forEach(itm => {
          
          if(itm.itemFeedbacks){
            console.log(itm.itemFeedbacks);
            itm.itemFeedbacks.forEach(feedback => {
              if(item.user_id == feedback.owner_id ){
                if(feedback.is_done == 1){
                  item.percentage += parseInt(share);
                }else{
                  itm.current_value = feedback.current_value;
                  const itemProgress = this.checkItemProgress(itm);
                  if(typeof itemProgress !== "boolean" && typeof itemProgress === "number"){
                    item.percentage += (parseInt(share)*itemProgress/100);
                  }
                }
              }
              
            });
           
          }
        });
        item.percentage = Math.floor(item.percentage);
        item.percentage += "%";

      } else item.percentage = '0%';


      // Calculate completion percentage
      if (goal.total_items && goal.total_items > 0) {
        
      goal.percentage = Math.floor(goal.percentage);
      goal.percentage = `${goal.percentage}%`;
    } else goal.percentage = '0%';





      item.is_incomplete = !item.is_complete && new Date() > new Date(item.end_date);
      item.routerLink = this.getRouterLink(item);
      item.disabled = (!item.routerLink) ? true : false;
    });

    return userArray;
  }

  private getRouterLink(item: any) {
    const userId = this.userCurrAcc.User.id;
    const isCollaborator = this.data.goal_data.collaborators.find(collaborator => collaborator.user_id == userId); // user is collaborator
    const isCollaboratorWithPermissionVFR = isCollaborator && (isCollaborator.permission.indexOf(1) >= 0 || isCollaborator.permission.indexOf(3) >= 0 || isCollaborator.permission.indexOf(4) >= 0); // user is collaborator with view or add evidence or review feedback permission
    const isOwner = item.user_id == userId;

    const gs = this.headerData.goal_settings; // goal settings
    const allGoalsPublic = gs.make_all_goals_public;
    const userCanViewGoalProg = Boolean(Number(this.headerData.goal_users_settings.can_view_goal_progress));

    const goalsArePublic = allGoalsPublic || (this.data.goal_data.goal_type == 2 && gs.account_type_goals_public) || (this.data.goal_data.goal_type == 4 && gs.individual_type_goals_public) || userCanViewGoalProg;

    if (isCollaboratorWithPermissionVFR || isOwner || goalsArePublic) {
      return `/goals/work/${this.data.goal_data.id}/${this.data.goal_data.goal_type}/${item.user_id}`;
    } else {
      return '';
    }

  }

  public getAvatarPath(image: string, userId: number) {
    if (image) return `${this.avatarBasePath}${userId}/${image}`;
    else return `${environment.homeUrl}assets/img/photo-default.png`;
  }

  public onSort(sort: any) {
    this.sortBy = sort.VALUE;
    this.currentPageRecords = 0;
    this.currentPage = 0;
    this.onFiltersChage();

    this.usersSortBy.forEach(item => {
      if (item.VALUE === sort.VALUE) item.active = true;
      else item.active = false;
    });
  }

  public goalsDetailExportData(to) {
    let sessionData: any = this.headerService.getStaticHeaderData();
    var obj = {
      "account_id": this.userCurrAcc.users_accounts.account_id,
      "user_id": this.userCurrAcc.users_accounts.user_id,
      "export_type": to,
      "current_lang": sessionData.language_translation.current_lang,
      "goal_id": this.goalId
    }
    this.mainService.goalsDetailExportData(obj, to);
  }

  public exportGoal(to) {
    let sessionData: any = this.headerService.getStaticHeaderData();

    var obj = {
      "account_id": this.userCurrAcc.accounts.account_id,
      "goal_id": this.goalId,
      "goal_type": this.goalType,
      "export_type": to,
      "current_lang": sessionData.language_translation.current_lang
    }
    this.mainService.getGoalStatusExported(obj).subscribe(res => {
      console.log(res);
    });
  }

  public ExportPdf() {
    var username = this.data.goal_data.title;
    var date = "Start Date: " + this.data.goal_data.start_date + " Due Date: " + this.data.goal_data.end_date
    const doc = new jsPDF();
    var header = function (data) {
      doc.setFontSize(18);
      doc.setTextColor(40);
      doc.setFontStyle('normal');
      doc.text(20, 20, username);
      doc.text(20, 30, date);
      // doc.text("Start Date: " + this.data.goal_data.start_date + " Due Date: " + this.data.goal_data.end_date, data.settings.margin.left, 10)
    }
    var options = {
      beforePageContent: header,
      margin: {
        top: 80
      },
      startY: doc.autoTableEndPosY() + 40
    };
    var col = ["Username", "Email"];
    var rows = [];
    this.data.user_data.forEach(user => {
      var temp = [user.first_name + " " + user.last_name, user.email];
      rows.push(temp);
    });
    doc.autoTable(col, rows, options);
    doc.save('Test.pdf');
  }

  public ExportExcel() {
    var username = this.data.goal_data.title;
    var date = "Start Date: " + this.data.goal_data.start_date + " Due Date: " + this.data.goal_data.end_date
    var rows = [];
    this.data.user_data.forEach(user => {
      var temp = { Username: user.first_name + " " + user.last_name, Email: user.email };
      rows.push(temp);
    });
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(rows);
    const wb: XLSX.WorkBook = { Sheets: { 'data': ws }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(wb, { bookType: 'xlsx', type: 'array' });
    this.saveExcelFile(excelBuffer, "doc");
  }

  private saveExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: this.fileType });
    FileSaver.saveAs(data, fileName + this.fileExtension);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}