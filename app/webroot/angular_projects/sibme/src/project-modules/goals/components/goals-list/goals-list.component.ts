import { Component, OnInit, OnDestroy, HostListener, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { Subject, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { ShowToasterService } from '@projectModules/app/services';
import { esLocale } from "ngx-bootstrap/locale";
import { defineLocale, add } from "ngx-bootstrap/chronos";
import { BsLocaleService } from "ngx-bootstrap/datepicker";
import { BsDropdownDirective } from "ngx-bootstrap/dropdown";
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';

import * as moment from 'moment';
import { HeaderService, AppMainService, SocketService } from "@app/services";
import { GoalsService, EditGoalService } from "@goals/services";
import { GoalsFilters } from "@goals/models";
import { OwnersListModalComponent, ManageCategoriesModalComponent, AddGoalModalComponent, DeleteComponent } from "@goals/modals";
import { GLOBAL_CONSTANTS } from "@constants/constant";
import { environment } from "@environments/environment";
import { MainService } from '@src/project-modules/video-page/services';

@Component({
  selector: 'goals-list',
  templateUrl: './goals-list.component.html',
  styleUrls: ['./goals-list.component.css']
})
export class GoalsListComponent implements OnInit, OnDestroy {

  @ViewChild('categoryDropdown', { static: false }) categoryDropdown: BsDropdownDirective;
  @ViewChild('content') content: ElementRef;
  @ViewChild('checkConfirmation', { static: false }) checkConfirmation;
  public loaded: boolean = false;
  public paginationLoading: boolean = false;
  public filterLoading: boolean = false;
  public searchResult: boolean = false;
  private userCurrAcc: any;
  public goalAltName: string;
  public goalsTypeByValue: any = GLOBAL_CONSTANTS.GOALS_TYPE_BY_VALUE;
  public goalsSortBy: any = JSON.parse(JSON.stringify(GLOBAL_CONSTANTS.GOAL_SORT_BY)); // deep copy, so that constant default value must not be changed
  public avatarBasePath: any = GLOBAL_CONSTANTS.AMAZON_AVATAR_BASE_PATH;
  fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  fileExtension = '.xlsx';
  public goalsFilters: GoalsFilters = new GoalsFilters();
  private searchQuery: Subject<string> = new Subject();
  private currentPage: number = 0;
  private currentPageRecords: number;
  private recordsPerPage: number = 15;
  private headerData: any;
  public goals: any;
  public sortKey: string = '';
  public sortTableKey: string = null;
  canAddGoal = false;
  bsModalRef: BsModalRef;
  defaultGoalFilter: string;
  myGoals = [] as any;
  dropdownSettings: any;
  selectedGoalsTypes = [] as any;
  goalsTypes = [] as any;
  public storageValue : any = {
    sortBy : {},
    goalFilter : {},
    goalType : [],
    goalCategory : [],
    startDate: '',
    endDate: ''
  };
  public sortData:any;
  private subscriptions: Subscription = new Subscription();
  public translation: any = {};
  addGoalModal: any;

  private taAddEditGoalsKey: string = '';

  public canCreateCategories: boolean = false;

  public slimScrollOpts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  goalUserSettings: any;
  sortByAsc: boolean = true;
  allArchive_userid: any;

  @HostListener("window:scroll", ["$event"])
  onScroll() {
    if (window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2) { // scroll reached on bottom
      const doc = document.documentElement;
      const currentScroll = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
      if (!this.paginationLoading && this.recordsPerPage <= this.currentPageRecords) {
        this.paginationLoading = true;
        this.currentPage++;
        this.onFiltersChage(this.currentPage, true);
        setTimeout(() => window.scroll(0, currentScroll + 100), 100);
      }
    }
  }

  constructor(public headerService: HeaderService, private appMainService: AppMainService, private mainService: MainService, private socketService: SocketService,
    private goalsService: GoalsService, public editGoalService: EditGoalService, private modalService: BsModalService,
    private toastr: ShowToasterService, private router: Router, private localeService: BsLocaleService,) {

    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.goalAltName = this.headerService.getGoalsAltName();
      this.updateBreadCrumb(this.goalAltName);
      this.goalUserSettings = this.headerService.getStaticHeaderData('goal_users_settings');

    }));
    this.defaultGoalFilter = this.translation.all_goals;

    this.subscriptions.add(this.headerService.behaviorialHeaderData$.subscribe(headerData => {
      this.headerData = headerData;
      this.canCreateCategories = Boolean(Number(this.headerData.goal_users_settings.can_create_goal_categories));
    }));

    this.searchQuery.pipe(debounceTime(1000), distinctUntilChanged()).subscribe(() => {
      this.onFiltersChage();
    });

  }

  ngOnInit() {
    console.log("Goals list")
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
    this.sortKey = `sortData-${this.userCurrAcc.accounts.account_id}-${this.userCurrAcc.User.id}`;
    this.sortTableKey = `sortTable-${this.userCurrAcc.accounts.account_id}-${this.userCurrAcc.User.id}`;
    if (this.headerData.language_translation.current_lang == 'es') defineLocale(this.headerData.language_translation.current_lang, esLocale);
    this.localeService.use(this.headerData.language_translation.current_lang);
    this.taAddEditGoalsKey = `${GLOBAL_CONSTANTS.LOCAL_STORAGE.GOAL.ADD_EDIT_TA}${this.userCurrAcc.accounts.account_id}-${this.userCurrAcc.User.id}`;

    this.subscribeSockets();
    this.initDropdown();
    var data;
    if(this.headerService.getLocalStorage(this.sortKey) != null || this.headerService.getLocalStorage(this.sortKey) !=undefined){
      this.sortData = this.headerService.getLocalStorage(this.sortKey);
      this.storageValue = this.sortData;
      if(Object.keys(this.sortData.sortBy).length === 0){
        this.storageValue.sortBy = { T_KEY: 'default_view', VALUE: 'default_view', active: true}
      } 
      if(this.sortData.sortBy.VALUE == "default_view"){
        this.goalsFilters.sortBy = ""
      } else {
        this.goalsFilters.sortBy = this.sortData.sortBy.VALUE;
      }
 
      // this.onFiltersChage();
      this.goalsSortBy.forEach(item => {
        if(item.VALUE === this.sortData.sortBy.VALUE){
          item.active = true;
        } else { 
          item.active = false;
        }
      });
      // if(Object.keys(this.sortData.goalFilter).length === 0) {
      //   this.defaultGoalFilter = this.translation.all_goals;
      //   this.myGoals = false;
      //   this.selectedGoalsTypes = [{id:1,goal_type: this.translation.all_goals}];
      // } else {
      //   this.defaultGoalFilter = this.sortData.goalFilter.defaultGoalFilter;
      //   this.myGoals = this.sortData.goalFilter.myGoals;
      //   this.selectedGoalsTypes = [{id: this.myGoals, goal_type: this.sortData.goalFilter.defaultGoalFilter}];
      // }

      if(this.sortData.goalFilter?.length > 0) {
        this.myGoals = this.sortData.goalFilter;
       
          this.goalsTypes.forEach((value)=> {
            if(this.myGoals?.length > 1) {
              if(this.myGoals?.includes(value.id)) {
                this.selectedGoalsTypes.push(value);
              }
            } else {
              if(this.myGoals == value.id) {
                this.selectedGoalsTypes.push(value);
              }
            }
          })
      } else {
        this.selectedGoalsTypes.push({id:1, goal_type: this.translation.all_goals});
        this.myGoals = [1];
      }

      data = {
        account_id: this.userCurrAcc.accounts.account_id,
        user_id: this.userCurrAcc.User.id,
        search: this.goalsFilters.search,
        goal_type: [],
        category_id: [],
        start_date:  (this.sortData.startDate) ? this.getDateString(new Date(this.sortData.startDate)) : null,
        end_date: (this.sortData.endDate) ? this.getDateString(new Date(this.sortData.endDate)) : null,
        sort_by: this.goalsFilters.sortBy,
        page: this.currentPage,
        my_goals: this.myGoals
      };
      this.goalsFilters.startDate = (this.sortData.startDate) ? new Date(this.sortData.startDate) : null;
      this.goalsFilters.endDate = (this.sortData.endDate) ? new Date(this.sortData.endDate) : null;
      
      this.sortData.goalType.forEach(goalType => (goalType.checked) ? data.goal_type.push(goalType.VALUE) : null);
      this.sortData.goalCategory.forEach(category => (category.checked) ? data.category_id.push(category.id) : null);
    } else {
      //setting default value 'All Gaols' of my goals
      this.selectedGoalsTypes.push({id:1, goal_type: this.translation.all_goals});
      this.myGoals = [1];

      this.goalsSortBy[0].active = true; // setting default value to true
      data = { account_id: this.userCurrAcc.accounts.account_id, user_id: this.userCurrAcc.User.id, my_goals: this.myGoals };
    }
    this.goalsService.getGoals(data).subscribe((res: any) => {
      if (res.success) {

        this.goals = res;
        const goalsData = this.formatGoalsArray(res.data);
        this.goals.data = goalsData;
        

        // used for sorting
        this.goals.data.map((obj) => {
          let owner_name = obj.owners_updated.length > 0 ? obj.owners_updated[0].first_name : '';
          obj.owner_name = owner_name;

          let g_type = obj.goal_type == 1 ? 'd-template' : (obj.goal_type == 2 ? 'a-account' : (obj.goal_type == 3 ? 'b-group' : (obj.goal_type == 4 ? 'c-individual' : '')))
          obj.g_type = g_type;

          let published_date = obj.publish_date != null ? obj.publish_date : 'AAA';
          obj.published_date = published_date;

          obj.g_percentage = Number(obj.percentage ? (obj.percentage.toString()).replace('%', '') : -5);
          if (obj.g_percentage && obj.is_incomplete != undefined) {
            if (obj.g_percentage == 0 && obj.is_incomplete) {
              obj.g_percentage = -3;
            }
          } else {
            if (obj.is_incomplete) {
              obj.g_percentage = -2
            } else {
              obj.g_percentage = -4;
            }
          }

        });

        this.currentPageRecords = res.data.length;
        // Prepare filters
        if(Object.keys(this.storageValue.goalType).length === 0) {
          this.goals.goal_types.forEach(item => {
            let goalType = this.goalsTypeByValue[item];
            goalType.checked = false;
            this.goalsFilters.goalTypes.push(goalType);
          });
        } else {
          this.goalsFilters.goalTypes = this.storageValue.goalType;
        }
        
        if(Object.keys(this.storageValue.goalCategory).length === 0){
          this.goalsFilters.categories = this.goals.categories;
        } else {
          this.goalsFilters.categories = this.sortData.goalCategory;
        }
        this.goals.data = this.formatGoalsArray(this.goals.data);

        this.restoreAddEditGoalDataFromLS(this.goals.data);
        // Sort Data according to column header Like Title
        if(this.headerService.getLocalStorage(this.sortTableKey)){
          var sortTableData = this.headerService.getLocalStorage(this.sortTableKey);
          sortTableData.sortByAsc ? this.sortByAsc = false : this.sortByAsc = true;
          this.sortTable(sortTableData.parm);
        }

        this.loaded = true;
      } else {
        this.toastr.ShowToastr('info',res.message);
        setTimeout(() => this.router.navigate(['/profile-page']), 500);
      }
    });

    this.ssOptForCategoryDropdown();
    this.checkAddGoalPermission();
  }

  initDropdown() {
    this.dropdownSettings = {
      "singleSelection": false,
      "enableCheckAll" : false,
      "allowSearchFilter": false,
      "itemsShowLimit": 1,
      "closeDropDownOnSelection": false,
      "idField": "id",
      "textField": "goal_type"
    };

    this.goalsTypes = [
      {id:1, goal_type: this.translation.all_goals},
      {id:2, goal_type: this.translation.my_goals},
      {id:3, goal_type: this.translation.all_collaborations }, 
      {id:4, goal_type: this.translation.all_archived_goals }, 
    ]
  }

  onGoalTypeSelect(evt) {
    console.log('select single')
    let goalType = evt.id;
    this.myGoals = this.myGoals as [];
    this.myGoals = [...this.myGoals, goalType];
    this.onFiltersChage();
  }

  onGoalTypeSelectAll(evt) {  
    console.log('select all' + evt)
    let goalType = evt.map(value => value.id);
    this.myGoals = goalType;
    this.onFiltersChage();

  }

  onGoalTypeDeselect(evt) {
    console.log('deselect' + evt);
    if(this.myGoals?.length > 1) {
      this.myGoals = this.myGoals.filter(id => id != evt.id);
    } else {

     if(this.myGoals == evt.id) {
       this.myGoals = [];
     }
    }
    
    this.onFiltersChage();
  }

  onGoalTypeDeselectAll(evt) {
    console.log('deselect all')
    this.myGoals = [];
    this.onFiltersChage();
  }

  public openAddGoalModal() {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-container-800',
      initialState: {}
    };
    this.addGoalModal = this.modalService.show(AddGoalModalComponent, config);
    this.addGoalModal.content.closeBtnName = 'Close';
  }

  checkItemProgress(item){
    if(item.add_measurement == 1){
      if(item.start_value == item.current_value){
      return 0;
      }else if(item.end_value > item.start_value){
        if(item.current_value <= item.start_value){
          return 0;
        } else if(item.current_value >= item.end_value){
          return 100;
        }else{
          let percentage=  Math.floor((item.current_value-item.start_value)/(item.end_value - item.start_value)*100);
          return percentage;
        }

      }else if(item.start_value > item.end_value){
        if(item.current_value >= item.start_value){
          return 0;
        }else if(item.current_value <= item.end_value){
          return 100;
        }else{
          return Math.floor((item.start_value - item.current_value)/(item.start_value - item.end_value) * 100);
        }
      }
      return true;
    }else{
      return false;
    }
  }

  private formatGoalsArray(data: any) {
    data.forEach(goal => {
      goal.percentage = goal.progress;
      let totalDoneItems = 0;
        if (goal.goal_type === 3 || (goal.goal_type === 4 && goal.owners_updated.length === 1)) {
        // Calculate completion percentage
        if (goal.total_items && goal.total_items > 0) {
            const share = (100/goal.total_items).toFixed(0);
            goal.action_items.forEach(item => {
              if(item.item_feedbacks){
                if(item.item_feedbacks.is_done == 1){
                  //goal.percentage += parseInt(share);
                  totalDoneItems ++;
                }else{
                  item.current_value = item.item_feedbacks.current_value;
                  const itemProgress = this.checkItemProgress(item);
                  if(typeof itemProgress !== "boolean" && typeof itemProgress === "number"){
                    goal.percentage += (parseInt(share)*itemProgress/100);
                  }
                }
              }
            }); 
          if(goal.total_items > 0 && totalDoneItems == goal.total_items && goal.is_complete == 1){
              goal.percentage = 100;
           } 
          goal.percentage = Math.floor(goal.percentage);
          goal.percentage = `${goal.percentage}%`;
        } else goal.percentage = '0%';
      }

      goal.is_incomplete = !goal.is_complete && goal.is_published && moment(goal.end_date).toDate().setHours(0,0,0,0) < moment().toDate().setHours(0,0,0,0); // compare only date, not time
      goal.is_progress = !goal.is_complete &&  goal.percentage !== '0%'&& goal.is_published && moment(goal.end_date).toDate().setHours(0,0,0,0) > moment().toDate().setHours(0,0,0,0); // goal is in progress if goal is not complete and due date should be greater than current data, compare only date, not time
      
      if(goal.is_progress){
        if(goal.action_items.length){
          goal.is_progress = false;
          goal.action_items.forEach(item => {
            if(item.item_feedbacks){
              // if(item.item_feedbacks.current_value || item.item_feedbacks.reflection){
                goal.is_progress=true;
              // } 
            }
            if(item.submitted_evidence?.length){
                goal.is_progress=true; 
            }
          });
        }
    }

      goal.routerLink = this.getRouterLink(goal);
      goal.disabled = (!goal.routerLink) ? true : false;
      goal.canEditDelete = this.checkEditDeletePermission(goal);
//if all action items are completed on time a goal will be considered completed on time
        goal.is_completed_on_time = 1;
        goal.action_items.forEach(item => {
            if(!item.item_feedbacks || !item.item_feedbacks.is_completed_on_time){
                goal.is_completed_on_time = 0;
            }
        });
    });

    return data;
  }


  private formatGoal(goal: any) {
    if (goal.goal_type === 3 || (goal.goal_type === 4 && goal.owners_updated.length === 1)) {
      // Calculate completion percentage
      goal.total_items = goal.total_items || goal.action_items_count;

      // if (goal.progress) {
      //   goal.percentage = `${goal.progress}%`;
      // } else 
      if (goal.total_items && goal.total_items > 0) {
        goal.percentage = Math.round((goal.completed_items / (goal.total_items || goal.action_items_count)) * 100);
        goal.percentage = `${goal.percentage}%`;

        goal.is_complete = goal.total_items == goal.completed_items;

      } else goal.percentage = '0%';
    }


    goal.is_incomplete = !goal.is_complete && new Date(goal.end_date).setHours(0, 0, 0, 0) < new Date().setHours(0, 0, 0, 0); // compare only date, not time
    goal.routerLink = this.getRouterLink(goal);
    goal.disabled = (!goal.routerLink) ? true : false;
    goal.canEditDelete = this.checkEditDeletePermission(goal);
      //if all action items are completed on time a goal will be considered completed on time
      goal.is_completed_on_time = 1;
      goal.action_items.forEach(item => {
          if(!item.item_feedbacks || !item.item_feedbacks.is_completed_on_time){
              goal.is_completed_on_time = 0;
          }
      });

  }

  public displayOwners(owners: any[]) {
    if (owners.length === 1 && owners[0].user_id == this.userCurrAcc.User.id) return;
    const initialState = { ownersList: owners };
    this.modalService.show(OwnersListModalComponent, { initialState, class: 'owners-list-popup' });
  }

  public openManageCategories() {
    const initialState = { categories: JSON.parse(JSON.stringify(this.goals.categories)) };
    this.bsModalRef = this.modalService.show(ManageCategoriesModalComponent, { initialState, class: 'manage-categories-popup' });
    this.bsModalRef.content.onGetCategory = (data) => {
      this.storageValue.goalCategory = data;
    }
    this.headerService.setLocalStorage(this.sortKey, this.storageValue);
  }

  public onSearch(value: string) {
    this.searchQuery.next(value);
  }

  public onSort(sort: any) {
    this.goalsFilters.sortBy = sort.VALUE;
    this.onFiltersChage();
    this.goalsSortBy.forEach(item => {
      if (item.VALUE === sort.VALUE) {
        item.active = true;
        this.storageValue.sortBy = item;
        // this.headerService.setLocalStorage('sort_value', item);
      } else {
        item.active = false;
      }
    });
    this.headerService.setLocalStorage(this.sortKey, this.storageValue);
  }

  // public onSort(sort: any) {
  //   this.goalsSortBy.forEach(item => {
  //     if (item.VALUE === sort.VALUE) {
  //       if(sort.active === true) {
  //         this.goalsFilters.sortBy = "";
  //         this.onFiltersChage();
  //         item.active = false;
  //         this.storageValue.sortBy = {};
  //       } else {
  //         this.goalsFilters.sortBy = sort.VALUE;
  //         this.onFiltersChage();
  //         item.active = true;
  //         this.storageValue.sortBy = item;
  //       }
  //     } else {
  //       item.active = false;
  //     }
  //   });
  //   this.headerService.setLocalStorage("sortData", this.storageValue);
  // }

  public onFiltersChage(currentPage: number = 0, fromScroll: boolean = false) {
    this.searchResult = true;
    this.currentPage = currentPage;
    if (!fromScroll) {
      this.filterLoading = true;
      if (this.categoryDropdown)
        this.categoryDropdown.hide();
    }

    if(this.goalsFilters.sortBy === "default_view" || this.goalsFilters.sortBy == undefined){
      this.goalsFilters.sortBy = "";
      this.storageValue.sortBy = { T_KEY: 'default_view', VALUE: 'default_view', active: true}
    }
    const data = {
      account_id: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id,
      search: this.goalsFilters.search,
      goal_type: [],
      category_id: [],
      start_date: (this.goalsFilters.startDate) ? this.getDateString(this.goalsFilters.startDate) : null,
      end_date: (this.goalsFilters.endDate) ? this.getDateString(this.goalsFilters.endDate) : null,
      sort_by: this.goalsFilters.sortBy,
      page: this.currentPage,
      my_goals: this.myGoals
    };
    this.storageValue.goalFilter = this.myGoals;
    // this.storageValue.goalFilter = { defaultGoalFilter: this.defaultGoalFilter, myGoals: this.myGoals };
    this.storageValue.startDate = this.goalsFilters.startDate ? this.goalsFilters.startDate: null;
    this.storageValue.endDate = this.goalsFilters.endDate ? this.goalsFilters.endDate: null;
    
    this.goalsFilters.goalTypes.forEach(goalType => (goalType.checked) ? data.goal_type.push(goalType.VALUE) : null);
    this.goalsFilters.categories.forEach(category => (category.checked) ? data.category_id.push(category.id) : null);
    
    this.storageValue.goalCategory = [...this.goalsFilters.categories]
    this.storageValue.goalType = [...this.goalsFilters.goalTypes];
    this.headerService.setLocalStorage(this.sortKey, this.storageValue);
    this.goalsService.getGoals(data).subscribe((res: any) => {
      this.currentPageRecords = res.data.length;
      const goalsData = this.formatGoalsArray(res.data);
      if (this.currentPage > 0) this.goals.data = [...this.goals.data, ...goalsData];
      else this.goals.data = goalsData;

      this.goals.data.map((obj) => {
        let owner_name = obj.owners_updated.length > 0 ? obj.owners_updated[0].first_name : '';
        obj.owner_name = owner_name;

        let g_type = obj.goal_type == 1 ? 'd-template' : (obj.goal_type == 2 ? 'a-account' : (obj.goal_type == 3 ? 'b-group' : (obj.goal_type == 4 ? 'c-individual' : '')))
        obj.g_type = g_type;

        let published_date = obj.publish_date != null ? obj.publish_date : 'AAA';
        obj.published_date = published_date;

        obj.g_percentage = Number(obj.percentage ? (obj.percentage.toString()).replace('%', '') : -5);
        if (obj.g_percentage && obj.is_incomplete != undefined) {
          if (obj.g_percentage == 0 && obj.is_incomplete) {
            obj.g_percentage = -3;
          }
        } else {
          if (obj.is_incomplete) {
            obj.g_percentage = -2
          } else {
            obj.g_percentage = -4;
          }
        }
      });
      this.paginationLoading = false;
      this.filterLoading = false;
    });
  }

  private getDateString(date: Date) {
    const year = date.getFullYear();
    let month: number | string = date.getMonth() + 1;
    let day: number | string = date.getDate();

    if (month < 10) month = `0${month}`;
    if (day < 10) day = `0${day}`;

    return `${year}-${month}-${day}`;
  }

  private checkEditDeletePermission(goal: any) {
    const userId = this.userCurrAcc.User.id;
    const isCollaborator = goal.collaborators.find(collaborator => collaborator.user_id == userId); // user is collaborator
    const isCollaboratorWithPermissionE = isCollaborator && isCollaborator.permission.indexOf(2) >= 0; // user is collaborator with edit permission
    const isCreator = goal.created_by == userId;
    const isOwner = goal.owners_updated.find(owner => owner.user_id == userId);

    return isCreator || isCollaboratorWithPermissionE || (goal.goal_type == 3 && isOwner);
  }

  private getRouterLink(goal: any) {

    if (goal.goal_type == 1) { // template goal
      goal.queryParams = { previewMode: 'navigated-from-list' };
      return `/goals/view/${goal.id}`;
    }

    const userId = this.userCurrAcc.User.id;
    const isCollaborator = goal.collaborators.find(collaborator => collaborator.user_id == userId); // user is collaborator
    const isCollaboratorWithPermissionVFR = isCollaborator && (isCollaborator.permission.indexOf(1) >= 0 || isCollaborator.permission.indexOf(3) >= 0 || isCollaborator.permission.indexOf(4) >= 0); // user is collaborator with view or add evidence or review feedback permission
    const isCollaboratorWithPermissionE = isCollaborator && isCollaborator.permission.indexOf(2) >= 0; // user is collaborator with edit permission
    const isOwner = goal.owners_updated.find(owner => owner.user_id == userId);
    const isCreator = goal.created_by == userId;
    
    const gs = this.headerData.goal_settings; // goal settings
    const allGoalsPublic = gs.make_all_goals_public;
    const userCanViewGoalProg = Boolean(Number(this.headerData.goal_users_settings.can_view_goal_progress));

    const accountGoalsPublic = allGoalsPublic || (goal.goal_type == 2 && gs.account_type_goals_public) || userCanViewGoalProg;
    const groupGoalsPublic = allGoalsPublic || (goal.goal_type == 3 && gs.group_type_goals_public) || userCanViewGoalProg;
    const individualGoalsPublic = allGoalsPublic || (goal.goal_type == 4 && gs.individual_type_goals_public) || userCanViewGoalProg;

    if (goal.is_published) {
      if (goal.goal_type == 2) { // account goal
        if (isCollaboratorWithPermissionVFR || accountGoalsPublic) {
          return `/goals/status/${goal.id}/${goal.goal_type}`;
        } else {
          return `/goals/work/${goal.id}/${goal.goal_type}/${userId}`;
        }
      } else if (goal.goal_type == 3) { // group goal
        if (isCollaboratorWithPermissionVFR || isOwner || groupGoalsPublic) {
          return `/goals/work/${goal.id}/${goal.goal_type}/${userId}`;
        } else if (isCollaboratorWithPermissionE || isCreator) {
          goal.queryParams = { previewMode: 'navigated-from-list' };
          return `/goals/view/${goal.id}`;
        }
      } else if (goal.goal_type == 4 && goal.owners_updated.length === 1) { // single individual goal
        if (isCollaboratorWithPermissionVFR || isOwner || individualGoalsPublic) {
          return `/goals/work/${goal.id}/${goal.goal_type}/${goal.owners_updated[0].user_id}`;
        } else if (isCollaboratorWithPermissionE || isCreator) {
          goal.queryParams = { previewMode: 'navigated-from-list' };
          return `/goals/view/${goal.id}`;
        }
      } else if (goal.goal_type == 4 && goal.owners_updated.length !== 1) { // multiple individual goal
        if (isCollaboratorWithPermissionVFR || individualGoalsPublic) {
          return `/goals/status/${goal.id}/${goal.goal_type}`;
        }
        // else if (isOwner) { // I think it is not be possible to land in this if as if user is owner in any goal then owners_updated of that goal will have only one entry with that user
        //   return `/goals/work/${goal.id}/${goal.goal_type}/${isOwner.user_id}`;
        // } 
        else if (isCollaboratorWithPermissionE || isCreator) {
          goal.queryParams = { previewMode: 'navigated-from-list' };
          return `/goals/view/${goal.id}`;
        }
      }
    } else {
      goal.queryParams = { previewMode: 'navigated-from-list' };
      return `/goals/view/${goal.id}`;
    }

    return '';
  }

  /** Socket functionality section start */
  private subscribeSockets() {
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(`goals_${this.userCurrAcc.accounts.account_id}`).subscribe(res => {

      switch (res.event) {

        case "categories":
          this.processCategoriesAddEdit(res.data);
          break;
        case "category_deleted":
          this.processCategoryDelete(res.item_id);
          break;
        case "goal_created":
          this.currentPage = 0;
          this.getGoals();
          break;
        case "goal_edited":
          // this.processGoalCreatedEdited(res);
          this.currentPage = 0;
          this.getGoals();
          break;
        case "goal_deleted":
          this.processGoalDeleted(res.item_id);
          break;
          case "goal_archived_unarchived":
            // this.processGoalArchived(res.data,res.is_archived);
            this.currentPage = 0;
            this.getGoals();
            break;
          
      }
    }));
  }

  private processCategoriesAddEdit(data: any) {
    this.goals.categories = data.all;
    this.goalsFilters.categories = this.goals.categories;
    this.storageValue.goalCategory = data.all;
    this.headerService.setLocalStorage(this.sortKey, this.storageValue);
  }

  private processCategoryDelete(categoryId: number) {
    const index = this.goalsFilters.categories.findIndex(item => item.id === categoryId);

    if (index >= 0) {
      this.goalsFilters.categories.splice(index, 1);
    }
    this.storageValue.goalCategory = this.goalsFilters.categories;
    this.headerService.setLocalStorage(this.sortKey, this.storageValue);
  }

  private processGoalCreatedEdited(socketRes: any) {
    let goalIsQualified: boolean = false;
    let goal = socketRes.data;
    goal.owners_updated = JSON.parse(JSON.stringify(goal.owners));

    /* First check if goal is qualified to add or update in existing goals array start */
    const userId = this.userCurrAcc.User.id;
    const isOwner = goal.owners_updated.find(owner => owner.user_id == userId);
    const isCollaborator = goal.collaborators.find(collaborator => collaborator.user_id == userId); // user is collaborator
    const isCollaboratorWithPermissionE = isCollaborator && isCollaborator.permission.indexOf(2) >= 0; // user is collaborator with edit permission
    const isCreator = goal.created_by == userId;

    const gs = this.headerData.goal_settings; // goal settings
    const allGoalsPublic = gs.make_all_goals_public;
    const accountTypeGoal = goal.goal_type == 2;
    const groupGoalsPublic = goal.goal_type == 3 && gs.group_type_goals_public;
    const individualGoalsPublic = goal.goal_type == 4 && gs.individual_type_goals_public;

    const userCanViewGoalProg = Boolean(Number(this.headerData.goal_users_settings.can_view_goal_progress));

    const userCanViewGoalPublically = allGoalsPublic || accountTypeGoal || groupGoalsPublic || individualGoalsPublic || userCanViewGoalProg;

    if (isCreator || isCollaboratorWithPermissionE) goalIsQualified = true;
    else if (goal.is_published) {
      if (isOwner || isCollaborator) goalIsQualified = true;
      else if (userCanViewGoalPublically) goalIsQualified = true;
    }
    /* First check if goal is qualified to add or update in existing goals array end */

    if (goalIsQualified) {

      if (goal.goal_type == 4 && !allGoalsPublic && !individualGoalsPublic && !userCanViewGoalProg && !isCollaborator && isOwner) {
        goal.owners_updated = [];
        goal.owners_updated.push(isOwner);
      }

      if (goal.goal_type == 4 && goal.owners_updated.length === 1) {
        this.goalsService.goalCompletedItems(goal.id, goal.owners_updated[0].user_id).subscribe((res: any) => {
      
          if (res.success) {
            goal.completed_items = res.complete_items;
            this.addUpdateGoalsInListing(goal, socketRes)
          }
        });
      } else {
        this.addUpdateGoalsInListing(goal, socketRes);
      }

    }

  }

  private addUpdateGoalsInListing(goal, socketRes) {
    let matchingKey = 'id';
    let matchingValue;
    if (socketRes.uuid && (socketRes.uuid != socketRes.data.id)) {
      matchingKey = 'uuid';
      matchingValue = socketRes[matchingKey];
    } else {
      matchingValue = socketRes.data.id;
    }

    this.formatGoal(goal);

    let goalIndex = this.goals.data.findIndex(item => item[matchingKey] == matchingValue);

    if (goalIndex >= 0) this.goals.data[goalIndex] = goal;
    else this.goals.data.unshift(goal);
  }

  private processGoalDeleted(goalId: number) {
    const index = this.goals.data.findIndex(item => item.id === goalId);

    if (index >= 0) {
      this.goals.data.splice(index, 1);
    }
  }
  // private processGoalArchived(goals,is_) {
  //   const index = this.goals.data.findIndex(item => item.id === goalId);
  //   if((this.myGoals == 4 && goals.is_archived) || (this.myGoals != 4 && !goals.is_Archived))
  //   { 
  //     // add goal in array
  //   }
  //   else if((this.myGoals != 4 && goals.is_Archived) || (this.myGoals == 4 && !goals.is_Archived)){ 
  //   if (index >= 0) {
  //     this.goals.data.splice(index, 1);
  //   }
  // }
  // }

  /** Socket functionality section end */

  deleteGoal(goal) {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-container-600',
      initialState: { goal }
    };
    this.modalService.show(DeleteComponent, config);
  }

  private updateBreadCrumb(title: string) {
    this.appMainService.updateBreadCrumb([{ title }]);
  }

  public getAvatarPath(image: string, userId: number) {
    if (image) {
      if (image.indexOf('https') === 0) return image;
      else return `${this.avatarBasePath}${userId}/${image}`;
    } else return `${environment.homeUrl}assets/img/photo-default.png`;
  }

  /** Local storage functions starts */
  private restoreAddEditGoalDataFromLS(goals: any) {
    const taAddEditGoals = this.headerService.getLocalStorage(this.taAddEditGoalsKey);

    if (Array.isArray(taAddEditGoals)) {
      taAddEditGoals.forEach(taAddEditGoal => {
        const goalIndex = goals.findIndex(goal => goal.id == taAddEditGoal.goal_id);

        if (goalIndex > -1) {
          goals[goalIndex].tryAgain = true;
          goals[goalIndex].uuid = (taAddEditGoal.uuid) ? taAddEditGoal.uuid : null;
          goals[goalIndex].goal_id = taAddEditGoal.goal_id;
          goals[goalIndex].goal_type = taAddEditGoal.goal_type;
          goals[goalIndex].title = taAddEditGoal.title;
          goals[goalIndex].desc = taAddEditGoal.desc;
          goals[goalIndex].start_date = new Date(taAddEditGoal.start_date);
          goals[goalIndex].end_date = new Date(taAddEditGoal.end_date);
        } else if (taAddEditGoal.uuid) {
          taAddEditGoal.tryAgain = true;
          taAddEditGoal.uuid = taAddEditGoal.uuid;
          taAddEditGoal.goal_id = taAddEditGoal.goal_id;
          taAddEditGoal.owners_updated = taAddEditGoal.goalOwners || [];
          goals.unshift(taAddEditGoal);
        }
      });
    }
  }

  checkAddGoalPermission() {
    if (this.goalUserSettings.can_create_individual_goals_for_themselves == '1' ||
      this.goalUserSettings.can_create_individual_goals_for_others_people == '1' ||
      this.goalUserSettings.can_create_group_goals == '1' ||
      this.userCurrAcc.roles.role_id === '100' ||
      this.userCurrAcc.roles.role_id === '110' ||
      this.goalUserSettings.can_create_group_templates == '1')
      this.canAddGoal = true
  }

  public retryGoal(goal: any) {
    if (!goal.isProcessing) {
      goal.isProcessing = true;

      const goalData: any = {
        goal_id: goal.goal_id,
        uuid: (goal.uuid) ? goal.uuid : null,
        goal_type: goal.goal_type,
        title: goal.title,
        desc: goal.desc,
        start_date: goal.start_date,
        end_date: goal.end_date,
        account_id: this.userCurrAcc.users_accounts.account_id,
        user_id: this.userCurrAcc.users_accounts.user_id
      };

      if (Array.isArray(goal.goalOwners)) {
        goalData.owners = [];
        goalData.ownerGroupIds = [];

        goal.goalOwners.forEach(x => {
          (x.is_user) ? goalData.owners.push(x.user_id) : goalData.ownerGroupIds.push(x.id);
        });
      }

      this.editGoalService.updateGoal(goalData).subscribe(() => {
        const key = (goalData.goal_id) ? 'goal_id' : 'uuid';
        this.removeAddEditGoalDataFromLS(goalData[key], key);
      }, () => {
        setTimeout(() => {
          goal.isProcessing = false;
        }, 200);

      });
    }
  }

  private removeAddEditGoalDataFromLS(id: number, key: string) {
    const taAddEditGoals = this.headerService.getLocalStorage(this.taAddEditGoalsKey);

    if (Array.isArray(taAddEditGoals)) {
      const goalIndex = taAddEditGoals.findIndex(taAddEditGoal => taAddEditGoal[key] == id);
      if (goalIndex > -1) {
        taAddEditGoals.splice(goalIndex, 1);
        this.headerService.setLocalStorage(this.taAddEditGoalsKey, taAddEditGoals);
      }
    }
  }
  /** Local storage functions ends */

  public useTemplate(goal: any) {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-container-700',
      initialState: {
        useTemplate: false,
        templateId: goal.id
      }
    };

    this.addGoalModal = this.modalService.show(AddGoalModalComponent, config);
  }

  /**
   * Slimscroll options for category dropdown
   */
  private ssOptForCategoryDropdown() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.slimScrollOpts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '7',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    }
  }


  sortTable(parm) {

    if (this.sortByAsc == true) {
      this.sortByAsc = false;
      this.goals.data.sort((a, b) => {
        if (typeof a[parm] != "number") {
          if (a[parm].toLowerCase() < b[parm].toLowerCase())
            return -1;
          if (a[parm] > b[parm])
            return 1;
          return -1;
        }
        else {
          if (a[parm] < b[parm])
            return -1;
          if (a[parm] > b[parm])
            return 1;
          return -1;
        }
      }
      );
    }
    else {
      this.sortByAsc = true;
      this.goals.data.sort((a, b) => {
        if (typeof a[parm] != "number") {
          if (a[parm].toLowerCase() > b[parm].toLowerCase())
            return -1;
          if (a[parm] < b[parm])
            return 1;
          return -1;
        } else {
          if (a[parm] > b[parm])
            return -1;
          if (a[parm] < b[parm])
            return 1;
          return -1;
        }
      });
    }
    // Add to local storage so that user can view data when he re-visited the page
    this.headerService.setLocalStorage(this.sortTableKey, { parm: parm, sortByAsc: this.sortByAsc });
  }

  public goalsExportData(to) {
    let sessionData: any = this.headerService.getStaticHeaderData();
    var obj = {
      "account_id": this.userCurrAcc.users_accounts.account_id,
      "user_id": this.userCurrAcc.users_accounts.user_id,
      "export_type": to,
      "current_lang": sessionData.language_translation.current_lang
    }
    this.mainService.goalsExportData(obj, to);
  }
  archiveAllModal(id){
    this.allArchive_userid=id;
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-md modal-container-500',
    };
    this.bsModalRef = this.modalService.show(this.checkConfirmation, config)
  }
  parseDate(date){
   // parse the date to replace the offset as it is not parsed by safari
    if (date) {
      return date.replace(/\s/g, "T")
    } else return false;
    
  }
  archive_Goal(id,is_acrhive){
    var obj = {
      account_id: this.userCurrAcc.accounts.account_id,    
      user_id: this.userCurrAcc.users_accounts.user_id,
      goal_id: id,
      archive :is_acrhive,

    }
  this.goalsService.archiveGoal(obj).subscribe((res: any) => {
     if(res.success){
      this.toastr.ShowToastr('success',res.message);
      this.bsModalRef.hide();
     }
  })
  }
  can_archive(goal){
    
    if(!goal) return false;
    const userId = this.userCurrAcc.User.id;
    const isCreator = goal.created_by == userId;
    const isOwner = goal.owners_updated.find(owner => owner.user_id == userId);
    const userCanArhive=this.goalUserSettings.can_archive_goals;
    const isCollaborator = goal.collaborators.find(collaborator => collaborator.user_id == userId);
    const isCollaboratorWithPermissionE = isCollaborator && isCollaborator.permission.indexOf(2) >= 0;
   
   
     if(goal.goal_type == 1 && (!isCreator || !isCollaboratorWithPermissionE))return false;
     if(goal.goal_type == 2 && (isCreator || isCollaboratorWithPermissionE)) return true;
     if(userCanArhive == 1 && (isCreator || isOwner || isCollaboratorWithPermissionE) ) return true;
    
  }
  
  CCUGP(goal, type, is_only = 0)//checkCurrentUserGoalPermission
  {
      if(!goal) return false;
      const userId = this.userCurrAcc.User.id;
      const isCreator = goal.created_by == userId;
      const userCanArhive=this.goalUserSettings.can_archive_goals;
      const isOwner = goal.owners_updated.find(owner => owner.user_id == userId);
      const isCollaborator = goal.collaborators.find(collaborator => collaborator.user_id == userId);
      const isCollaboratorWithPermissionE = isCollaborator && isCollaborator.permission.indexOf(2) >= 0;
    
      if(type == 'isCreator')
      {
          if(is_only)
          {
              if(isCreator && !isOwner && !isCollaborator)
              {
                  return true;
              }
          }
          else
          {
              return isCreator;
          }
          
      }
      else if(type == 'isOwner')
      {
          if(is_only)
          {
              if(!isCreator && isOwner && !isCollaborator)
              {
                  return true;
              }
          }
          else
          {
              return isOwner;
          }
      }
      else if(type == 'userCanArhive')
      {
          return userCanArhive;
      }
      else if(type == 'isCollaborator')
      {
          if(is_only)
          {
              if(isCreator && !isOwner && isCollaborator)
              {
                  return true;
              }
          }
          else
          {
              return isCollaborator;
          }
      }
      else if(type == 'isCollaboratorE')
      {
          if(is_only)
          {
              if(!isCreator && !isOwner && isCollaboratorWithPermissionE)
              {
                  return true;
              }
          }
          else
          {
              return isCollaboratorWithPermissionE;
          }
      }
      return  false;
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  private getGoals(){
    var data;
    if(this.headerService.getLocalStorage(this.sortKey) != null || this.headerService.getLocalStorage(this.sortKey) !=undefined){
      this.sortData = this.headerService.getLocalStorage(this.sortKey);
      this.storageValue = this.sortData;
      if(Object.keys(this.sortData.sortBy).length === 0){
        this.storageValue.sortBy = { T_KEY: 'default_view', VALUE: 'default_view', active: true}
      } 
      if(this.sortData.sortBy.VALUE == "default_view"){
        this.goalsFilters.sortBy = ""
      } else {
        this.goalsFilters.sortBy = this.sortData.sortBy.VALUE;
      }
 
      // this.onFiltersChage();
      this.goalsSortBy.forEach(item => {
      
        if(item.VALUE === this.sortData.sortBy.VALUE){
          item.active = true;
        } else { 
          item.active = false;
        }
      });
      // if(Object.keys(this.sortData.goalFilter).length === 0) {
      //   this.defaultGoalFilter = this.translation.all_goals;
      //   this.myGoals = 1;
      // } else {
      //   this.defaultGoalFilter = this.sortData.goalFilter.defaultGoalFilter;

      //   this.myGoals = this.sortData.goalFilter.myGoals;
      // }

      if(this.sortData.goalFilter) {
        this.myGoals = this.sortData.goalFilter;
        this.goalsTypes.forEach((value)=> {
          if(this.myGoals.includes(value.id)) {
            this.selectedGoalsTypes.push(value);
          }
        })
      }

      data = {
        account_id: this.userCurrAcc.accounts.account_id,
        user_id: this.userCurrAcc.User.id,
        search: this.goalsFilters.search,
        goal_type: [],
        category_id: [],
        start_date:  (this.sortData.startDate) ? this.getDateString(new Date(this.sortData.startDate)) : null,
        end_date: (this.sortData.endDate) ? this.getDateString(new Date(this.sortData.endDate)) : null,
        sort_by: this.goalsFilters.sortBy,
        page: this.currentPage,
        my_goals: this.myGoals
      };
      this.goalsFilters.startDate = (this.sortData.startDate) ? new Date(this.sortData.startDate) : null;
      this.goalsFilters.endDate = (this.sortData.endDate) ? new Date(this.sortData.endDate) : null;
      
      this.sortData.goalType.forEach(goalType => (goalType.checked) ? data.goal_type.push(goalType.VALUE) : null);
      this.sortData.goalCategory.forEach(category => (category.checked) ? data.category_id.push(category.id) : null);
    } else {

      this.goalsSortBy[0].active = true; // setting default value to true
      data = { account_id: this.userCurrAcc.accounts.account_id, user_id: this.userCurrAcc.User.id };
    }

    this.goalsService.getGoals(data).subscribe((res: any) => {
      if (res.success) {

        this.goals = res;
        const goalsData = this.formatGoalsArray(res.data);
        this.goals.data = goalsData;
        

        // used for sorting
        this.goals.data.map((obj) => {
          let owner_name = obj.owners_updated.length > 0 ? obj.owners_updated[0].first_name : '';
          obj.owner_name = owner_name;

          let g_type = obj.goal_type == 1 ? 'd-template' : (obj.goal_type == 2 ? 'a-account' : (obj.goal_type == 3 ? 'b-group' : (obj.goal_type == 4 ? 'c-individual' : '')))
          obj.g_type = g_type;

          let published_date = obj.publish_date != null ? obj.publish_date : 'AAA';
          obj.published_date = published_date;

          obj.g_percentage = Number(obj.percentage ? obj.percentage.replace('%', '') : -5);
          if (obj.g_percentage && obj.is_incomplete != undefined) {
            if (obj.g_percentage == 0 && obj.is_incomplete) {
              obj.g_percentage = -3;
            }
          } else {
            if (obj.is_incomplete) {
              obj.g_percentage = -2
            } else {
              obj.g_percentage = -4;
            }
          }

        });

        this.currentPageRecords = res.data.length;
        // Prepare filters
        if(Object.keys(this.storageValue.goalType).length === 0) {
          this.goals.goal_types.forEach(item => {
            let goalType = this.goalsTypeByValue[item];
            goalType.checked = false;
            this.goalsFilters.goalTypes.push(goalType);
          });
        } else {
          this.goalsFilters.goalTypes = this.storageValue.goalType;
        }
        
        if(Object.keys(this.storageValue.goalCategory).length === 0){
          this.goalsFilters.categories = this.goals.categories;
        } else {
          this.goalsFilters.categories = this.sortData.goalCategory;
        }
        this.goals.data = this.formatGoalsArray(this.goals.data);

        this.restoreAddEditGoalDataFromLS(this.goals.data);
        // Sort Data according to column header Like Title
        if(this.headerService.getLocalStorage(this.sortTableKey)){
          var sortTableData = this.headerService.getLocalStorage(this.sortTableKey);
          sortTableData.sortByAsc ? this.sortByAsc = false : this.sortByAsc = true;
          this.sortTable(sortTableData.parm);
        }

        this.loaded = true;
      } else {
        this.toastr.ShowToastr('info',res.message);
        setTimeout(() => this.router.navigate(['/profile-page']), 500);
      }
    });
  }

  getParticipantLimit(participant_Count) {
    let result;
    if (participant_Count > 999 && participant_Count <= 999999) {
        result = Math.floor(participant_Count / 1000) + 'K';
    } else {
        result = participant_Count;
    }
    return result;
  }

}