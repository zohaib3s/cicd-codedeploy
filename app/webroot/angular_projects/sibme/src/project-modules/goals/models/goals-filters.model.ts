export class GoalsFilters {
    public search: string;
    public goalTypes: any[] = [];
    public categories: any[] = [];
    public startDate: Date;
    public endDate: Date;
    public sortBy: string;
}