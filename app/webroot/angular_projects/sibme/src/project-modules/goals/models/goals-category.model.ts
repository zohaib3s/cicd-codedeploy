export class GoalsCategory {
    public id: number = 0;
    public uuid: number = new Date().getTime();
    public name: string;
    public editing: boolean;
}