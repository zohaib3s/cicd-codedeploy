import { Ng2SearchPipeModule } from 'ng2-search-filter'; // TODO:Delete Should be deleted after evidence modal work done
import { CanDeactivateGuard } from './helpers/can-deactivate.guard';
import { NgSlimScrollModule } from 'ngx-slimscroll';

/** Barrel file - used to import and exports components, directives services and other third party modules used by goals module */
import { CommonModule } from '@angular/common';

import { GoalsService, EditGoalService, GoalWorkService } from './services';

import {
  RootComponent,
  BreadcrumbComponent,
  GoalsListComponent,
  EditGoalComponent,
  ActionItemListComponent,
  GoalsListDetailsComponent,
  GoalsWorkComponent,
  WorkDetailsComponent,
  FrameworkComponent
} from './components';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { FormsModule } from '@angular/forms';
import {
  AddGoalModalComponent,
  OwnerModalComponent,
  CollaboratorModalComponent,
  GroupDetailModalComponent,
  OwnersListModalComponent,
  CollaboratorsListModalComponent,
  ManageCategoriesModalComponent,
  DeleteComponent,
  EvidenceModalComponent,
  FrameWorkEvidenceModalComponent
} from './modals';
import { RoutePermissionGuard } from './guards/route-permission.guard';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { QuillModule } from 'ngx-quill';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';


export const __IMPORTS = [
  CommonModule,
  Ng2SearchPipeModule,
  BsDatepickerModule.forRoot(),
  TooltipModule.forRoot(),
  BsDropdownModule.forRoot(),
  ModalModule.forRoot(),
  FormsModule,
  TypeaheadModule.forRoot(),
  TabsModule.forRoot(),
  TooltipModule.forRoot(),
  NgSlimScrollModule,
  NgbModule,
  QuillModule.forRoot(),
  NgMultiSelectDropDownModule.forRoot(),
];

export const __DECLARATIONS = [
  RootComponent,
  BreadcrumbComponent,
  GoalsListComponent,
  EditGoalComponent,
  ActionItemListComponent,
  AddGoalModalComponent,
  OwnerModalComponent,
  CollaboratorModalComponent,
  GroupDetailModalComponent,
  OwnersListModalComponent,
  CollaboratorsListModalComponent,
  ManageCategoriesModalComponent,
  GoalsListDetailsComponent,
  DeleteComponent,
  GoalsWorkComponent,
  WorkDetailsComponent,
  FrameWorkEvidenceModalComponent,
  EvidenceModalComponent,
  FrameworkComponent,
  SafeHtmlPipe
  
];

export const __ENTRY_COMPONENTS = [
  AddGoalModalComponent,
  OwnerModalComponent,
  CollaboratorModalComponent,
  GroupDetailModalComponent,
  DeleteComponent,
  FrameWorkEvidenceModalComponent,
  EvidenceModalComponent
];

export const __PROVIDERS = [
  GoalsService,
  GoalWorkService,
  EditGoalService,
  CanDeactivateGuard,
  RoutePermissionGuard
];


