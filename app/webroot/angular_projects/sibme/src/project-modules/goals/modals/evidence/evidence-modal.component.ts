import { GoalWorkService } from './../../services/goal-work.service';
import { EditGoalService } from '@goals/services/edit-goal.service';
import { Component, OnInit, AfterViewInit, ViewChild, EventEmitter } from '@angular/core';
import { Subscription, Subject } from 'rxjs';
import { HeaderService } from '@src/project-modules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { AssessmentService } from '@src/project-modules/video-huddle/child-modules/assessment-huddle-details/services/assessment.service';
import { distinctUntilChanged, debounceTime, switchMap } from 'rxjs/operators';
import { SlimScrollEvent, ISlimScrollOptions } from 'ngx-slimscroll';
import { DetailsHttpService } from '@src/project-modules/video-huddle/child-modules/details/servic/details-http.service';

@Component({
  selector: 'evidence-modal',
  templateUrl: './evidence-modal.component.html',
  styleUrls: ['./evidence-modal.component.css']
})
export class EvidenceModalComponent implements OnInit {
  @ViewChild('evidence_scroll', { static: false }) workspace;
  public opts: ISlimScrollOptions;
  public scrollEvents: EventEmitter<SlimScrollEvent>;
  public searchPage = 1;
  public page: any = 1;
  switch = 0;
  item_id: any;
  owner_id: any;
  goal_id: any;
  public goalType = 4;
  private subscription: Subscription;
  private evidenceSubscription: Subscription;
  public translation: any = {};
  public evidenceList: any = [];
  public evidenceData: any = [];
  sessionData: any = {};
  evidenceType: any;
  user: { account_id: any; user_id: any; user_current_account: any; user_accounts: any; };
  goalUserSettings: any;
  obj: any;
  SearchString = '';
  workspaceVideoLoading: boolean;
  evidenceLoading: boolean;
  apiLoading: boolean = false;
  totalRecords: any;
  isResponseCompleted: boolean;
  callCount = 0;
  searchText: any;
  tempEvid1: any = [];
  tempEvid2: any = [];
  submitted_evidence: any;
  apiCount = 1;
  private searchInput: Subject<string> = new Subject();
  selectedEvidence: any;
  private userCurrAcc: any;
  constructor(public headerService: HeaderService, public toastr: ShowToasterService, public modalRef: BsModalRef,
    public assessmentService: AssessmentService, public goalWorkService: GoalWorkService, public detailsService: DetailsHttpService,
    public editGoalServie: EditGoalService, private router: Router) {
    this.sessionData = this.headerService.getStaticHeaderData();
    this.user = {
      account_id: this.sessionData.user_current_account.users_accounts.account_id,
      user_id: this.sessionData.user_current_account.users_accounts.user_id,
      user_current_account: this.sessionData.user_current_account,
      user_accounts: this.sessionData.user_accounts,
    };
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.goalUserSettings = this.headerService.getStaticHeaderData('goal_users_settings');
    });
    this.obj = {
      title: "",
      page: 1,
      sort: "",
      account_id: this.user.account_id,
      user_id: this.user.user_id
    }

  }

  ngOnInit() {
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');

    console.log(this.item_id);
    console.log(this.owner_id);
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    }
    this.SubscribeSearch();

    console.log('evidence ', this.evidenceType);
    if (this.evidenceType == 'video') {
      this.searchText = this.translation.search_videos;
    } else if (this.evidenceType == 'resource') {
      this.searchText = this.translation.search_resources;
    } else if (this.evidenceType == 'huddle') {
      this.searchText = this.translation.search_huddles;
    } else if (this.evidenceType == 'scripted_notes') {
      this.searchText = this.translation.search_scripted_notes;
    }
    this.getWorkspaceArtifacts();

  }

  switchTabs(value) {
    if (this.evidenceSubscription)
      this.evidenceSubscription.unsubscribe()
    this.page = 1;
    this.switch = value
    this.callCount = 0;
    this.SearchString = '';
    this.isResponseCompleted = false;
    this.evidenceLoading = true;
    this.evidenceList = [];
    this.selectedEvidence = [];
    this.tempEvid1 = [];
    this.evidenceData = [];
    this.apiCount = 0;
    this.getWorkspaceArtifacts();


  }



  public OnSearchChange(str) {
    this.SearchString = str;
    this.apiLoading = true;
    this.searchPage = 1;
    this.page = 1;
    this.apiCount = 0;
    this.callCount = 0;
    this.isResponseCompleted = false;
    this.evidenceLoading = true;
    this.evidenceList = [];
    this.selectedEvidence = [];
    this.tempEvid1 = [];
    this.searchInput.next(str)

  }


  private SubscribeSearch() {
    this.searchInput.pipe(
      debounceTime(200),
      // distinctUntilChanged(),
      switchMap(() => {
        return this.goalWorkService.getEvidence(this.setObject(), this.evidenceType, this.switch);
      })
    ).subscribe((data: any) => {
      this.formatResForDisplay(data);

    }, () => {
      this.apiLoading = false;
    });
  }





  setObject() {
    let obj = {}

    obj['title'] = this.SearchString;
    obj['page'] = this.SearchString.length > 0 ? this.searchPage : this.page
    obj['account_id'] = this.sessionData.user_current_account.users_accounts.account_id
    obj['user_id'] = this.sessionData.user_current_account.users_accounts.user_id
    obj['from_goals'] = 1
    obj['with_folders'] = 1
    if (this.evidenceType == 'video') {
      obj['doc_type'] = '1'
    }
    if (this.evidenceType == 'resource') {
      obj['doc_type'] = '2'
    }
    if (this.evidenceType == 'scripted_notes') {
      obj['doc_type'] = '3'
    }
    if (!this.switch && this.evidenceType != 'huddle') {
      obj['sort'] = ''
    }
    // else if(this.switch && this.evidenceType !='huddle'){
    //   obj['with_documents'] =1
    //   obj['role_id'] = this.user.user_current_account.roles.role_id
    // }
    if (this.switch || this.evidenceType == 'huddle') {
      obj['role_id'] = this.user.user_current_account.roles.role_id
    }
    return obj;
  }



  public getWorkspaceArtifacts(onScrollCall: boolean = false) {
    this.apiLoading = true;

    if (!onScrollCall) this.evidenceLoading = true;

    this.evidenceSubscription = this.goalWorkService.getEvidence(this.setObject(), this.evidenceType, this.switch).subscribe((data: any) => {
      this.formatResForDisplay(data);

    }, error => {
      this.apiLoading = false;
    })
  }

  private formatResForDisplay(data: any) {
      
      if (this.evidenceType == 'huddle' || this.switch) {
        // this.evidenceLoading = true;
        this.totalRecords = data.huddle_counts;
        this.evidenceData = data.data;
        let check = this.evidenceType == 'huddle'
        this.mapSubmittedEvidences(this.evidenceData, check)
        this.evidenceList = [...this.evidenceList, ...this.evidenceData];
        // this.evidenceLoading = false;
      } else {
        this.evidenceData = data.data;
        if (data.total_records > 0) {
          this.totalRecords = data.total_records;
        }
        this.mapSubmittedEvidences(this.evidenceData)
        this.evidenceList = [...this.evidenceList, ...this.evidenceData];
      }
      
      if (this.evidenceList.length > 0) {
        this.isResponseCompleted = true;
      } else this.isResponseCompleted = false;

      this.apiLoading = false;
      this.evidenceLoading = false;
  }


  mapSubmittedEvidences(evidenceData, check?) {
    if(check){
      evidenceData.map(evidence => {
      if ((this.selectedEvidence && this.selectedEvidence.huddle_id && this.selectedEvidence.huddle_id == evidence.huddle_id) ||
        this.submitted_evidence.includes(evidence.huddle_id)) evidence.selected = true;
      else evidence.selected = false;
    });
    } else {
      evidenceData.map(evidence => {
      if ((this.selectedEvidence && this.selectedEvidence.id && this.selectedEvidence.id == evidence.id) ||
        this.submitted_evidence.includes(evidence.doc_id)) evidence.selected = true;
      else evidence.selected = false;
    });
    }
    
  }

  toggleList(selectedEvidence) {
    selectedEvidence.toggle = !selectedEvidence.toggle;

    if (selectedEvidence.toggle) this.getHuddleDocuments(selectedEvidence);

    this.evidenceList.map(evidence => {
      if (selectedEvidence.huddle_id != evidence.huddle_id)
        evidence.toggle = false;
    });
  }

  private getHuddleDocuments(selectedEvidence: any) {
    if (!Array.isArray(selectedEvidence.documents)) {
      selectedEvidence.loadingDocuments = true;
      selectedEvidence.documents = [];
      let doc_type;
      if (this.evidenceType == 'video') doc_type = 1;
      if (this.evidenceType == 'resource') doc_type = 2;
      if (this.evidenceType == 'scripted_notes') doc_type = 3;

      let htype;
      if (selectedEvidence.type === 'assessment') htype = 3;
      else if (selectedEvidence.type === 'coaching') htype = 2;
      else htype = 1;

      const data: any = {
        huddle_id: selectedEvidence.huddle_id,
        user_id: this.userCurrAcc.User.id,
        role_id: this.userCurrAcc.roles.role_id,
        doc_type: doc_type,
        htype: htype
      };
      this.goalWorkService.getHuddleDocuments(data).subscribe((res: any) => {
        selectedEvidence.documents = res.items;
        this.mapSubmittedEvidences(selectedEvidence.documents);
        selectedEvidence.loadingDocuments = false;
      }, error => {
        selectedEvidence.loadingDocuments = false;
        console.log('error in getHuddleDocuments: ', error);
      });

    }
  }

  onScroll(event?) {
    let element;
    element = this.workspace ? this.workspace.nativeElement : ''

    let atBottom = parseInt(element.scrollHeight) - parseInt(element.scrollTop) <= element.clientHeight + 2;
    //console.log(atBottom);
    if (atBottom && !this.apiLoading) {
      let records;
      if (this.switch) records = Math.ceil(this.totalRecords / 3);
      else records = Math.ceil(this.totalRecords / 12) - 1;
      if (this.callCount < records) {
        this.page++;
        this.callCount++;
        this.searchPage++;
        this.getWorkspaceArtifacts(true);
      }
      else {
        if (this.switch) this.evidenceLoading = false;
        else return;
      }

    }

  }




  toggleSelectedEvidence(item: any) {
    this.selectedEvidence = item;
    if (this.switch && this.SearchString == '')
      this.evidenceList.forEach((evidence: any) => {
        if (Array.isArray(evidence.documents) && evidence.documents.length > 0)
          evidence.documents.forEach(document => {
            if (document.id === this.selectedEvidence.id || this.submitted_evidence.includes(document.doc_id)) document.selected = true;
            else document.selected = false;
          });
      });
    else
      this.evidenceList.forEach(evidence => {
        if (evidence.id === this.selectedEvidence.id || this.submitted_evidence.includes(evidence.doc_id)) evidence.selected = true;
        else evidence.selected = false;
      });
  }



  toggleSelectedhuddle(item: any) {
    this.selectedEvidence = item;
    this.evidenceList.forEach(evidence => {
      if (evidence.huddle_id === this.selectedEvidence.huddle_id ||
        this.submitted_evidence.includes(evidence.doc_id) || this.submitted_evidence.includes(evidence.huddle_id)) evidence.selected = true;
      else evidence.selected = false;
    });
  }

  checkEvidence() {
    if (this.evidenceType == 'video') return 1
    if (this.evidenceType == 'resource') return 2
    if (this.evidenceType == 'huddle') return 3
    if (this.evidenceType == 'scripted_notes') return 5
  }

  save() {
    let obj = {
      item_id: this.item_id,
      user_id: this.user.user_id,
      goal_id: this.goal_id,
      owner_id: this.owner_id,
      evidence_ref_id: this.evidenceType == 'huddle' ? this.selectedEvidence.huddle_id : this.selectedEvidence.doc_id,
      evidence_type: this.checkEvidence()
    }
    this.goalWorkService.saveEvidence(obj).subscribe((res: any) => {

      if (res.success) {
        this.modalRef.hide();
        this.toastr.ShowToastr('success',this.headerService.getGoalAltTranslation(this.translation.evidence_added))
      }
      else
        this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.evidence_duplicate))
    }, error => {
      this.modalRef.hide();
    })
  }
}
