import { EditGoalService } from '../../services/edit-goal.service';
import { Component, OnInit } from '@angular/core';
import { Subscription, Observable, Subscriber, of } from 'rxjs';
import { HeaderService } from '@src/project-modules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { mergeMap } from 'rxjs/operators';
import { environment } from '@environments/environment';
import * as _ from 'lodash';
import { OwnerModalComponent, CollaboratorModalComponent } from '@goals/modals';


@Component({
  selector: 'group-detail-modal',
  templateUrl: './group-detail-modal.component.html',
  styleUrls: ['./group-detail-modal.component.css']
})
export class GroupDetailModalComponent implements OnInit {
  private subscription: Subscription;
  public translation: any = {};
  headerData: any;
  modal_data:any;
  bsModalRef: any;
  user: { account_id: any; user_id: any; user_current_account: any; user_accounts: any; };
  public GroupDetails: any={}
  constructor(public headerService: HeaderService, public toastr: ShowToasterService, public modalRef: BsModalRef,
              private modalService: BsModalService) {
    this.headerData = this.headerService.getStaticHeaderData();
    this.user = {
      account_id: this.headerData.user_current_account.users_accounts.account_id,
      user_id: this.headerData.user_current_account.users_accounts.user_id,
      user_current_account: this.headerData.user_current_account,
      user_accounts: this.headerData.user_accounts
    };
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
  }

  ngOnInit() {
    this.modal_data =this.headerService.g_modal_data;
    console.log('group details ',this.GroupDetails);
  }

  showParentModal(){
    if(this.modal_data){
      if(this.modal_data.initialState.modal=='owner')
      this.bsModalRef = this.modalService.show(OwnerModalComponent, this.modal_data);

      if(this.modal_data.initialState.modal=='collaborator')
      this.bsModalRef = this.modalService.show(CollaboratorModalComponent, this.modal_data);
    }
  }

}
