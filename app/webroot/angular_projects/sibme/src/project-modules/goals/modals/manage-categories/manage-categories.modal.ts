import { Component, OnInit, OnDestroy, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ShowToasterService } from '@projectModules/app/services';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';

import { HeaderService } from "@app/services";
import { GoalsService } from "@goals/services";
import { GoalsCategory } from "@goals/models";
@Component({
  selector: 'manage-categories.modal',
  templateUrl: './manage-categories.modal.html',
  styleUrls: ['./manage-categories.modal.css']
})
export class ManageCategoriesModalComponent implements OnInit, OnDestroy {
  @ViewChild('categoryName') searchElement: ElementRef;

  private userCurrAcc: any;
  public categories: any[] = [];
  public category: GoalsCategory = new GoalsCategory();
  public submitBtnTitle: string = 'Add';
  public isEdit: boolean = false;
  public formSubmitting: boolean = false;
  public isFormDirty: boolean = false;
  public slimScrollOpts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;

  private subscription: Subscription;
  public translation: any = {};

  constructor(private headerService: HeaderService, private goalsService: GoalsService,
    public bsModalRef: BsModalRef, private toastr: ShowToasterService) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.submitBtnTitle = this.translation.myfile_add;
    });
  }

  ngOnInit() {
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
    this.prepareSlimScrollOptions();
  }

  public addUpdateCategory() {
    this.isFormDirty = true;
    this.category.name = (this.category.name) ? this.category.name.trim() : '';
    if (this.category.name) {
      if (this.isEdit) {
        this.isEdit = false;

        // If the editing category is the one from db then matching key will be 'id' otherwise 'uuid'
        let matchingKey = 'id';
        if (this.category.id === 0) matchingKey = 'uuid';

        let category = this.categories.find(category => category[matchingKey] === this.category[matchingKey]);
        if (category) {
          category.name = this.category.name;
          category.editing = false;
        }
      } else {
        this.categories.unshift(this.category);
      }

      this.category = new GoalsCategory();
      this.submitBtnTitle = this.translation.myfile_add;

    } else {
      this.toastr.ShowToastr('info',this.translation.category_name_should_not_be_empty);
    }

  }

  public editCategory(category: GoalsCategory) {
    this.isEdit = true;
    category.editing = true;
    this.submitBtnTitle = this.translation.myfile_update;
    this.category = JSON.parse(JSON.stringify(category));
    setTimeout(() => this.searchElement.nativeElement.focus(), 0);
  }

  onGetCategory(data) {

  }

  public submit() {
    this.onGetCategory(this.categories);
    this.formSubmitting = true;
    if (this.isFormDirty) {
      const data = {
        account_id: this.userCurrAcc.accounts.account_id,
        user_id: this.userCurrAcc.User.id,
        categories: this.categories
      };
      this.goalsService.addEditCategories(data).subscribe(() => {
        this.bsModalRef.hide();
      });
    } else {
      this.bsModalRef.hide();
    }
  }

  public deleteCategory(category: GoalsCategory) {
    // If the deleting category is the one from db then matching key will be 'id' otherwise 'uuid'
    let matchingKey = 'id';
    if (category.id === 0) {
      matchingKey = 'uuid';
      this.removeCategoryFromArray(category, matchingKey);

    } else {
      const data = {
        account_id: this.userCurrAcc.accounts.account_id,
        category_id: category.id
      };

      this.goalsService.deleteCategory(data).subscribe(() => {
        this.toastr.ShowToastr('info',this.translation.category_deleted);
        this.removeCategoryFromArray(category, matchingKey);
      });
    }
  }

  private removeCategoryFromArray(category: GoalsCategory, matchingKey: string) {
    const index = this.categories.findIndex(item => item[matchingKey] === category[matchingKey]);

    if (index >= 0) {
      this.categories.splice(index, 1);
    }
  }

  private prepareSlimScrollOptions() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.slimScrollOpts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '7',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
