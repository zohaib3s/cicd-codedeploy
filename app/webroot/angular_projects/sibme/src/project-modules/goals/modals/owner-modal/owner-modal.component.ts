import { DeleteComponent } from './../delete/delete.component';
import { EditGoalService } from '../../services/edit-goal.service';
import { Component, OnInit, EventEmitter, TemplateRef } from '@angular/core';
import { Subscription, Observable, Subscriber, of } from 'rxjs';
import { HeaderService } from '@src/project-modules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { mergeMap } from 'rxjs/operators';
import { environment } from '@environments/environment';
import * as _ from 'lodash';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { GroupDetailModalComponent } from '../group-detail-modal/group-detail-modal.component';



@Component({
  selector: 'owner-modal',
  templateUrl: './owner-modal.component.html',
  styleUrls: ['./\owner-modal.component.css']
})
export class OwnerModalComponent implements OnInit {
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  public goal_type;
  private subscription: Subscription;
  public translation: any = {};
  headerData: any;
  goalTitle: '';
  currentUser: { account_id: any; user_id: any; user_current_account: any; user_accounts: any; };
  asyncSelected: any;
  dataSource: Observable<any>;
  typeaheadLoading: boolean;
  users: any = [];
  selectedUsers: any;
  editMode: boolean;
  created_by;
  goal_id;
  confirmation: any;
  public GroupDetails: any={}
  deleteModalRef: BsModalRef;
  userToDelete: any = {};
  goalUserSettings: any;
  newlyAddedUsers: any = [];
  bsModalRef: any;
  public ConfirmationKey;

  constructor(public headerService: HeaderService, public toastr: ShowToasterService, public modalRef: BsModalRef,
              private modalService: BsModalService, public editGoalService: EditGoalService) {
    this.headerData = this.headerService.getStaticHeaderData();
    this.currentUser = {
      account_id: this.headerData.user_current_account.users_accounts.account_id,
      user_id: this.headerData.user_current_account.users_accounts.user_id,
      user_current_account: this.headerData.user_current_account,
      user_accounts: this.headerData.user_accounts
    };
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.ConfirmationKey = this.translation.Huddle_confirmation_bit
    });
    this.goalUserSettings = this.headerService.getStaticHeaderData('goal_users_settings');
    this.dataSource = new Observable((observer: Subscriber<string>) => {
      // Runs on every search
      observer.next(this.asyncSelected);
    })
      .pipe(
        mergeMap((token: string) => this.getUsersAsObservable(token))
      );
  }

  ngOnInit() {
     this.users = this.editGoalService.formatUsers(this.users);
     this.scrollEvents = new EventEmitter<SlimScrollEvent>();
     this.opts = {
       position: 'right',
       barBackground: '#C9C9C9',
       barOpacity: '0.8',
       barWidth: '10',
       barBorderRadius: '20',
       barMargin: '0',
       gridBackground: '#D9D9D9',
       gridOpacity: '1',
       gridWidth: '0',
       gridBorderRadius: '20',
       gridMargin: '0',
       alwaysVisible: true,
       visibleTimeout: 1000,
     };
     if(this.selectedUsers.length > 0) {
        this.selectedUsers.forEach(user => {
          let index = _.findIndex(this.users, { user_id: user.user_id });
          if (index >= 0) {
             // this.users.splice(index, 1);
          this.users[index].is_added = 0;
       }
      })
     }
     if(this.goalUserSettings.can_create_individual_goals_for_others_people ==='1' &&
      this.goalUserSettings.can_create_individual_goals_for_themselves ==='0' && this.goal_type == 4
       && this.currentUser.user_current_account.roles.role_id != '100' && this.currentUser.user_current_account.roles.role_id != '110' ){
        let index = _.findIndex(this.users, { user_id: Number(this.currentUser.user_id) });
        if (index >= 0) {
          this.users.splice(index, 1);
    }
     }
     if(this.goal_id<1 && this.selectedUsers.length > 0){
       this.newlyAddedUsers = _.cloneDeep(this.selectedUsers)
     }
  }


  getUsersAsObservable(token: string): Observable<any> {
    const query = new RegExp(token, 'ig');
    return of(
      this.users.filter(user => (query.test(user.first_name) || query.test(user.last_name)
        || query.test(user.first_name + ' ' + user.last_name) || query.test(user.email) || query.test(user.name)) && user.is_added ===1)
    );
  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }
  typeaheadOnSelect(e: any): void {

    let index = _.findIndex(this.users, { id: e.item.id });

    if (index >= 0) {

      if (!this.selectedUsers) this.selectedUsers = [];

      this.selectedUsers.unshift(this.editGoalService.getCopy(this.users[index]));
      this.newlyAddedUsers.unshift(this.editGoalService.getCopy(this.users[index]));

      // this.users.splice(index, 1);
      this.users[index].is_added = 0;

      this.asyncSelected = '';
    }

  }


  openDeleteModal(template: TemplateRef<any>, user) {
    this.confirmation  = ''
    this.userToDelete = {}
    this.userToDelete = user;
    let index;
    user.is_user?index = _.findIndex(this.newlyAddedUsers, { user_id: user.user_id }): index =_.findIndex(this.newlyAddedUsers, { id: user.id })
      if (index >= 0) {
        this.newlyAddedUsers.splice(index, 1);
        this.editGoalService.DeleteUser(this.userToDelete, this.selectedUsers,this.users)
        return
      }
      const config: ModalOptions = {
        backdrop: 'static',
        keyboard: false,
        class: 'owner-modal-container',
      };
      this.deleteModalRef = this.modalService.show(template, config);
      this.modalRef.setClass('fade');
    }

    deleteOwner(){
      this.editGoalService.DeleteUser(this.userToDelete, this.selectedUsers,this.users)
      if(this.editMode){
        this.updateOwners()
      }
      this.showParentModal();
    }

    showParentModal(){
      this.deleteModalRef.hide();
      setTimeout(() => {
        this.modalRef.setClass('owner-modal-container');
      }, 500);
    }

    updateOwners(){
      if(this.editMode) {
      let ownerGroupIds = [];
      let owners = [];

        this.selectedUsers.forEach(x => {
          x.is_user ? owners.push(x.user_id) : ownerGroupIds.push(x.id);
        });
      let obj = {
        goal_id: this.goal_id,
        account_id: this.currentUser.account_id,
        user_id: this.currentUser.user_id,
        owners,
        ownerGroupIds
      }
      this.editGoalService.updateGoalOwners(obj).subscribe(res =>{
        this.toastr.ShowToastr('success',this.headerService.getGoalAltTranslation(this.translation.goals_owners_updated));
      })
    } else {
      this.editGoalService.$editGoalGlobalSubject.next({ ownersData: {owners: this.selectedUsers} });
    }
    }

  save() {
     this.updateOwners();
    this.modalRef.hide();

  }

  public openGroupDetailModal(group_id, selectedUsers, allUsers) {
    let obj = {
      account_id: this.headerData.user_current_account.accounts.account_id,
      group_id
    };

    this.editGoalService.GetGroupDetail(obj).subscribe((data: any) => {

      const config: ModalOptions = {
        backdrop: 'static',
        keyboard: false,
        class: 'modal-container-600',
        initialState: { 
          GroupDetails: {
            name: data.group_details[0].name,
            users: this.editGoalService.GetGroupUsers(data.group_user_details, selectedUsers, allUsers),
           
          },
     
      }
      };
      this.modalRef.hide();
      this.headerService.g_modal_data.modal='owner';
      this.bsModalRef = this.modalService.show(GroupDetailModalComponent, config);

    });

  }

}
