import { EditGoalService } from '../../services/edit-goal.service';
import { Component, OnInit, EventEmitter, TemplateRef, ElementRef, ViewChild } from '@angular/core';
import { Subscription, Observable, Subscriber, of } from 'rxjs';
import { HeaderService } from '@src/project-modules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { mergeMap } from 'rxjs/operators';
import { environment } from '@environments/environment';
import * as _ from 'lodash';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { GroupDetailModalComponent } from '@goals/modals';

@Component({
  selector: 'collaborator-modal',
  templateUrl: './collaborator-modal.component.html',
  styleUrls: ['./collaborator-modal.component.css']
})
export class CollaboratorModalComponent implements OnInit {
  @ViewChild('confirmationDailogue') confirmDailogue:ElementRef;
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  public goalType = 4;
  private subscription: Subscription;
  public translation: any = {};
  headerData: any;
  goalTitle: '';
  user: { account_id: any; user_id: any; user_current_account: any; user_accounts: any; };
  asyncSelected: any;
  dataSource: Observable<any>;
  typeaheadLoading: boolean;
  editMode: boolean;
  users: any[];
  selectedUsers: any;
  public GroupDetails: any;
  confirmation: any;
  goal_id;
  userToDelete: any;
  deleteModalRef: BsModalRef;
  cofirmModalRef: BsModalRef;
  bsModalRef;
  group: number;
  newlyAddedUsers: any=[];
  public ConfirmationKey: any;
  constructor(public headerService: HeaderService, public toastr: ShowToasterService, public modalRef: BsModalRef,
    private modalService: BsModalService, public editGoalService: EditGoalService) {
    this.headerData = this.headerService.getStaticHeaderData();
    this.user = {
      account_id: this.headerData.user_current_account.users_accounts.account_id,
      user_id: this.headerData.user_current_account.users_accounts.user_id,
      user_current_account: this.headerData.user_current_account,
      user_accounts: this.headerData.user_accounts
    };
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.ConfirmationKey = this.translation.Huddle_confirmation_bit
    });
    this.dataSource = new Observable((observer: Subscriber<string>) => {
      // Runs on every search
      observer.next(this.asyncSelected);
    })
      .pipe(
        mergeMap((token: string) => this.getUsersAsObservable(token))
      );
  }

  ngOnInit() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    };
    this.users = this.editGoalService.formatUsers(this.users, true);
    if(this.selectedUsers.length > 0) {
      this.selectedUsers.forEach(user => {
        user.permission = JSON.parse("[" + user.permission + "]");
        let index = _.findIndex(this.users, { user_id: user.user_id });
        if (index >= 0) {
          // this.users.splice(index, 1);
          this.users[index].is_added = 0;
     }
    })
   }
   if(this.goal_id<1 && this.selectedUsers.length > 0){
    this.newlyAddedUsers = _.cloneDeep(this.selectedUsers)
  }
  }



  getUsersAsObservable(token: string): Observable<any> {
    const query = new RegExp(token, 'ig');
    return of(
      this.users.filter(user => (query.test(user.first_name) || query.test(user.last_name)
        || query.test(user.first_name + ' ' + user.last_name) || query.test(user.email) || query.test(user.name)) && user.is_added ===1)
    );
  }

  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }
  typeaheadOnSelect(e: any): void {

    let index = _.findIndex(this.users, { id: e.item.id });

    if (index >= 0) {

      if (!this.selectedUsers) this.selectedUsers = [];
      this.selectedUsers.unshift(this.editGoalService.getCopy(this.users[index]));
      this.newlyAddedUsers.unshift(this.editGoalService.getCopy(this.users[index]));

      // this.users.splice(index, 1);
      this.users[index].is_added = 0;

      this.asyncSelected = '';
    }

  }


  openDeleteModal(template: TemplateRef<any>, user) {
    this.confirmation  = ''
    this.userToDelete = ''
    this.userToDelete = user;
    let index;
    user.is_user?index = _.findIndex(this.newlyAddedUsers, { user_id: user.user_id }): index =_.findIndex(this.newlyAddedUsers, { id: user.id })
      if (index >= 0) {
        this.newlyAddedUsers.splice(index, 1);
        this.editGoalService.DeleteUser(this.userToDelete, this.selectedUsers,this.users)
        return
      }
      const config: ModalOptions = {
        backdrop: 'static',
        keyboard: false,
        class: 'collab-delete-modal-container',
      };
      this.deleteModalRef = this.modalService.show(template,config);
      this.modalRef.setClass('fade');
    }

    deleteCollaborator(){
      this.editGoalService.DeleteUser(this.userToDelete, this.selectedUsers,this.users)
      if(this.editMode){
        this.updateCollaborator()
      }
      this.showParentModal();
    }

    showParentModal(){
      this.deleteModalRef.hide();
      setTimeout(() => {
        this.modalRef.setClass('collaborators-modal-container');
      }, 500);
    }


    updateCollaborator(){
      if (this.editMode) {
      let collaboratorsGroupIds = [];
      let collaborators = [];

      this.selectedUsers.forEach(x => {
        x.is_user ? collaborators.push({ user_id: x.user_id, permission: x.permission })
         : collaboratorsGroupIds.push({ group_id: x.id, permission: x.permission });
      });
      let obj = {
        goal_id: this.goal_id,
        account_id: this.user.account_id,
        user_id: this.user.user_id,
        collaborators,
        collaboratorsGroupIds
      }
      this.editGoalService.updateGoalCollaborators(obj).subscribe(res => {
        this.toastr.ShowToastr('success',this.translation.goals_collab_added);
      })
    }else{
      this.editGoalService.$editGoalGlobalSubject.next({ collaboratorsData: {collaborators: this.selectedUsers}});
    }

    }

  save() {
      this.updateCollaborator();
      this.modalRef.hide();
  }

  public openGroupDetailModal(group_id, selectedUsers, allUsers) {
    let obj = {
      account_id: this.headerData.user_current_account.accounts.account_id,
      group_id
    };

    this.editGoalService.GetGroupDetail(obj).subscribe((data: any) => {

      const config: ModalOptions = {
        backdrop: 'static',
        keyboard: false,
        class: 'modal-container-600',
        initialState: { 
          GroupDetails: {
            name: data.group_details[0].name,
            users: this.editGoalService.GetGroupUsers(data.group_user_details, selectedUsers, allUsers),
           
          },
        
      }
      };
      this.modalRef.hide();
      this.headerService.g_modal_data.modal='collaborator';
      this.bsModalRef = this.modalService.show(GroupDetailModalComponent, config);

    });

  }


  addPermission(user, type){
    let found = user.permission.find(x=>x==type)
    console.log('index: ', found);
    found?
      user.permission = user.permission.filter(x=>x != found)
    : user.permission.push(type)
  
  console.log('user', user)
  }

  checkPermission(user, value) {
    if(Array.isArray(user.permission))
    return user.permission.includes(value)
    let permission = user.permission;
    user.permission = []
    user.permission.push(permission)
    this.checkPermission(user, value)
  }

}
