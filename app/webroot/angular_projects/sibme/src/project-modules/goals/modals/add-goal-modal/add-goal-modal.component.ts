import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';

import { EditGoalService } from '@goals/services';
import { HeaderService } from '@app/services';
import { GLOBAL_CONSTANTS } from "@constants/constant";

@Component({
  selector: 'add-goal-modal',
  templateUrl: './add-goal-modal.component.html',
  styleUrls: ['./add-goal-modal.component.css']
})
export class AddGoalModalComponent implements OnInit, OnDestroy {
  public goalType = 4;
  private subscription: Subscription;
  public translation: any = {};
  sessionData: any = {};
  goalTitle = "";
  public useTemplate = true;
  public templateId: any;
  public goalAltName: string;
  user: { account_id: any; user_id: any; user_current_account: any; user_accounts: any; };
  goalUserSettings: any;

  private userCurrAcc: any;
  private addGoalLSKey: string = '';
  count= 0;

  constructor(public headerService: HeaderService, public toastr: ShowToasterService, public modalRef: BsModalRef,
    public editGoalServie: EditGoalService, private router: Router , public activatedRoute: ActivatedRoute) {

    this.sessionData = this.headerService.getStaticHeaderData();
    this.user = {
      account_id: this.sessionData.user_current_account.users_accounts.account_id,
      user_id: this.sessionData.user_current_account.users_accounts.user_id,
      user_current_account: this.sessionData.user_current_account,
      user_accounts: this.sessionData.user_accounts
    };
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.goalAltName = this.headerService.getGoalsAltName();
      this.goalUserSettings = this.headerService.getStaticHeaderData('goal_users_settings');
    });
  }

  ngOnInit() {
    console.log(this.templateId);
    
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
    this.addGoalLSKey = `${GLOBAL_CONSTANTS.LOCAL_STORAGE.GOAL.ADD}${this.userCurrAcc.accounts.account_id}-${this.userCurrAcc.User.id}`;
    this.restoreAddGoalDataFromLS();
    this.checkPermission()
  }

  createGoal() {
    this.editGoalServie.$editGoalGlobalSubject.next({ createGoalData: { title: this.goalTitle, goal_type: this.goalType } });
    this.updateAddGoalDataInLS();
    this.router.navigate(['/goals/create'], { queryParams: { title: this.goalTitle, goal_type: this.goalType , templateId: this.templateId ? this.templateId: '' } })
    this.modalRef.hide();
  }

  private restoreAddGoalDataFromLS() {
    const addGoal = this.headerService.getLocalStorage(this.addGoalLSKey);

    if (addGoal) {
      (addGoal.title) ? this.goalTitle = addGoal.title : null;
      (addGoal.goal_type) ? this.goalType = addGoal.goal_type : null;
    }
  }

  private updateAddGoalDataInLS() {
    const addGoal = this.headerService.getLocalStorage(this.addGoalLSKey) || {};
    addGoal.title = this.goalTitle;
    addGoal.goal_type = this.goalType;

    this.headerService.setLocalStorage2(this.addGoalLSKey, addGoal);
  }


  checkPermission(){
    if(this.goalUserSettings.can_create_individual_goals_for_themselves == '1' ||
    this.goalUserSettings.can_create_individual_goals_for_others_people == '1')
    {
      this.count++
      this.goalType = 4
    }
    if(this.goalUserSettings.can_create_group_goals == '1')
    {
      this.count++
      this.goalType = 3
    }
    if(this.user.user_current_account.roles.role_id ==='100' || this.user.user_current_account.roles.role_id ==='110')
    {
      this.count++
      this.goalType = 2
    }
    if(this.goalUserSettings.can_create_group_templates == '1')
    {
      this.count++
      this.goalType = 1
    }
    if(this.count==4){
      this.goalType = 4
    }
  }

  public cancel() {
    this.headerService.removeLocalStorage(this.addGoalLSKey);
    this.modalRef.hide();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
