import { EditGoalService } from '../../services/edit-goal.service';
import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { HeaderService } from '@src/project-modules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'delete-modal',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent implements OnInit {
  public goalType = 4;
  private subscription: Subscription;
  public translation: any = {};
  public confirmation;
  public ConfirmationKey;
  sessionData: any = {};
  goal: any;
  evidence:any;
  evidenceType:any;
  user: { account_id: any; user_id: any; user_current_account: any; user_accounts: any; };
  constructor(public headerService: HeaderService, public toastr: ShowToasterService, public modalRef: BsModalRef,
              public editGoalServie: EditGoalService, private router: Router) {
    this.sessionData = this.headerService.getStaticHeaderData();
    this.user = {
      account_id: this.sessionData.user_current_account.users_accounts.account_id,
      user_id: this.sessionData.user_current_account.users_accounts.user_id,
      user_current_account: this.sessionData.user_current_account,
      user_accounts: this.sessionData.user_accounts
    };
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.ConfirmationKey = this.translation.Huddle_confirmation_bit
    });
  }

  ngOnInit() {

console.log('evidenceType',this.evidenceType);
  }

  deletegoal(){
    if(this.confirmation == this.ConfirmationKey){
      let obj= {
        user_id: this.user.user_id,
        account_id: this.user.account_id,
        goal_id: this.goal.id
      }
      this.editGoalServie.deleteGoal(obj).subscribe(x=>
        this.toastr.ShowToastr('success',this.headerService.getGoalAltTranslation(this.translation.goals_deleted_successfully))
        );
      this.modalRef.hide();  
    } else {
      this.toastr.ShowToastr('info',this.translation.huddle_you_typed_in + " '" + this.ConfirmationKey + "' ");
      return;
    }
  }

  deleteEvidence(){
    console.log('evidence: ',this.evidence);
 
   
    let delObj={
      item_id:this.evidence.goal_item_id,
      goal_id:this.evidence.goal_id,
      owner_id:this.evidence.owner_id,
      evidence_ref_id:this.evidence.evidence_ref_id,
      evidence_id:this.evidence.id,
    }
    this.editGoalServie.removeEvidence(delObj).subscribe((res: any) => {
      this.toastr.ShowToastr('success',this.headerService.getGoalAltTranslation(this.translation.goals_evidence_removed_success));
      this.modalRef.hide();
     }) 
  
}
}
