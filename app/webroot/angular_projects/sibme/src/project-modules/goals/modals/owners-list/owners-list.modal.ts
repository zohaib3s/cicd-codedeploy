import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';

import { HeaderService } from "@app/services";

import { GLOBAL_CONSTANTS } from "@constants/constant";
import { environment } from "@environments/environment";

@Component({
  selector: 'owners-list',
  templateUrl: './owners-list.modal.html',
  styleUrls: ['./owners-list.modal.css']
})
export class OwnersListModalComponent implements OnInit, OnDestroy {
  public avatarBasePath: any = GLOBAL_CONSTANTS.AMAZON_AVATAR_BASE_PATH;
  public ownersList: any[] = [];
  public searchOwner: string;
  public slimScrollOpts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;

  private subscription: Subscription;
  public translation: any = {};

  constructor(private headerService: HeaderService, public bsModalRef: BsModalRef) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => this.translation = languageTranslation);
  }

  ngOnInit() {
    this.prepareSlimScrollOptions();
  }

  private prepareSlimScrollOptions() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.slimScrollOpts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '7',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    }
  }

  public getAvatarPath(image: string, userId: number) {
    if (image) return `${this.avatarBasePath}${userId}/${image}`;
    else return `${environment.homeUrl}assets/img/photo-default.png`;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
