import { Component, OnInit, OnDestroy, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';

import { HeaderService, AppMainService } from '@app/services';
import { GoalsService, GoalWorkService } from '@goals/services';

@Component({
  selector: 'framework-evidence-modal',
  templateUrl: './framework-evidence-modal.component.html',
  styleUrls: ['./framework-evidence-modal.component.css']
})
export class FrameWorkEvidenceModalComponent implements OnInit, OnDestroy {
  /** modal initial state data start */
  public frameworks: any;
  public data: any;
  /** modal initial state data end */

  public selectedFrameworkId: number = 0;
  public selectedFrameworkData: any;
  public frameworkStandardLoading: boolean = false;
  public addingFramework: boolean = false;

  public translation: any;
  private subscriptions: Subscription = new Subscription();

  public slimScrollOpts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;

  constructor(public headerService: HeaderService, private goalsService: GoalsService, private goalWorkService: GoalWorkService,
    private appMainService: AppMainService, public modalRef: BsModalRef, private toastr: ShowToasterService) {
    this.subscriptions.add(this.goalsService.frameworks$.subscribe(frameworks => this.frameworks = frameworks));
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => this.translation = languageTranslation));
  }

  ngOnInit() {
    this.prepareSlimScrollOptions();
  }

  public frameworkChange(frameworkId: number) {
    if (frameworkId == 0) this.selectedFrameworkData = null;
    else {
      this.frameworkStandardLoading = true;
      this.appMainService.getFrameworkSettingsById(frameworkId).subscribe((res: any) => {

        if (res.data) this.selectedFrameworkData = res.data;
        else this.selectedFrameworkData = null;

        this.frameworkStandardLoading = false;

      }, () => {
        this.selectedFrameworkData = null
        this.frameworkStandardLoading = false;
      });
    }
  }

  public useFramework() {
    this.addingFramework = true;
    let obj = {
      item_id: this.data.itemId,
      user_id: this.data.userId,
      goal_id: this.data.goalId,
      owner_id: this.data.ownerId,
      evidence_ref_id: this.selectedFrameworkId,
      evidence_type: this.data.evidenceType
    }
    this.goalWorkService.saveEvidence(obj).subscribe((res: any) => {
      this.modalRef.hide();
      if (res.success) this.toastr.ShowToastr('success',this.headerService?.getGoalAltTranslation(this.translation.evidence_added));
      else this.toastr.ShowToastr('error',res.message);
    }, () => {
      this.modalRef.hide();
    })
  }

  private prepareSlimScrollOptions() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.slimScrollOpts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '7',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}
