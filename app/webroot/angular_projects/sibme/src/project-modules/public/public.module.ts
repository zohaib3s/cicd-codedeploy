import { NgModule } from '@angular/core';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

const route:Routes=[

  { path: '',redirectTo:'reset_password' ,pathMatch:'full' },

   { path: 'reset_password', component: ResetPasswordComponent }
]

@NgModule({
  declarations: [ResetPasswordComponent],
  imports: [
  RouterModule.forChild(route),
    FormsModule,
    HttpClientModule
  ],
  exports: [ResetPasswordComponent]
})
export class PublicModule { }
