import { Component, OnInit } from '@angular/core';
import { ShowToasterService } from '@projectModules/app/services';
import { PublicService } from '../../services/public.service';
import { HeaderService } from "@projectModules/app/services";
import * as _ from "lodash"
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  public model = {
    password: '',
    confirm_password: '',
    key: ''

  }

  public showPassword = 'password';
  public showConfirmPassword = 'password';
  public showPasswordFlag = false;
  public showConfirmPasswordFlag = false;

  constructor(private acRoute: ActivatedRoute, private router: Router, private toastr: ShowToasterService, private publicService: PublicService, private headerService: HeaderService) {

    this.acRoute.queryParams.subscribe(params => {
      if (params.key)
        this.model.key = params.key;
      else
        this.router.navigate(["/page_not_found"]);

    })
  }

  ngOnInit() {
  }

  resetPassword() {
    if (this.model.password == '' || this.model.confirm_password == '') {
      this.toastr.ShowToastr('info','Please fill both fileds to continue')
    }
    if (this.model.password.length < 8 || this.model.confirm_password.length < 8) {
      this.toastr.ShowToastr('info','Minimum 8 charaters for password')
    }

    if (this.model.password != this.model.confirm_password) {
      this.toastr.ShowToastr('info','confirm password must match new password')
    }

    if ((this.model.password.length >= 8 && this.model.confirm_password.length >= 8) && (this.model.password == this.model.confirm_password)) {
      this.publicService.resetPassword(this.model).subscribe((data: any) => {
      }, error => {

      })
    }

  }

}
