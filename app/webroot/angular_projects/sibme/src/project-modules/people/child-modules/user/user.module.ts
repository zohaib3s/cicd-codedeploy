import { NgModule } from '@angular/core';

import { __IMPORTS, __DECLARATIONS, __PROVIDERS } from "./components.barrel";

import { UserRoutes } from "./user.routes";
import { UniqueUserPipe } from './pipe/unique-user.pipe';
import { AppLevelSharedModule } from '@shared/shared.module';

@NgModule({
  imports: [__IMPORTS, UserRoutes, AppLevelSharedModule],
  declarations: [__DECLARATIONS, UniqueUserPipe],
  providers: [__PROVIDERS]
})
export class UserModule { }
