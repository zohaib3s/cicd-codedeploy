import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RootComponent } from './components/root/root.component'
import { SharedPageNotFoundComponent } from '../shared/shared-page-not-found/shared-page-not-found.component';

const routes: Routes = [
  { path: '', redirectTo: 'home/people', pathMatch: 'full' },
  {
    path: 'home',
    component: RootComponent, children: [
      { path: '', redirectTo: 'people', pathMatch: 'full' },
      {
        path: 'people',
        loadChildren:()=> import('./child-modules/user/user.module').then(m => m.UserModule),
      },
      {
        path: 'groups',
        loadChildren:()=> import('./child-modules/groups/groups.module').then(m => m.GroupsModule),
      },
    ]
  },
  { path: 'page_not_found', component: SharedPageNotFoundComponent },
  { path: '**', redirectTo: 'page_not_found', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],

  exports: [RouterModule]
})
export class PeopleRoutes { }
