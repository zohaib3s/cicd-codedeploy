import { Component, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'people-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css']
})
export class RootComponent implements OnInit {

  private styles = [
    { id: 'peopleApp', path: 'assets/people/css/app.css' },
    { id: 'peopleCustom', path: 'assets/people/css/custom.css' }
  ];
  
  constructor() {}

  ngOnInit() {
    this.styles.forEach(style => {
      this.loadCss(style);
    });
  }

  ngOnDestroy() {
    this.styles.forEach(style => {
      let element = document.getElementById(style.id);
      element.parentNode.removeChild(element);
    });
  }

  private loadCss(style: any) {
    let head = document.getElementsByTagName('head')[0];
    let link = document.createElement('link');
    link.id = style.id;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = style.path;
    head.appendChild(link);
  }

}
