import { NgModule } from '@angular/core';

import { __IMPORTS, __DECLARATIONS, __PROVIDERS } from "./components.barrel";
import { PeopleRoutes } from './people-routes';
import { AppLevelSharedModule } from '../shared/shared.module';

@NgModule({
  imports: [__IMPORTS, PeopleRoutes, AppLevelSharedModule],
  declarations: [__DECLARATIONS],
  providers: [__PROVIDERS]
})
export class PeopleModule { }
