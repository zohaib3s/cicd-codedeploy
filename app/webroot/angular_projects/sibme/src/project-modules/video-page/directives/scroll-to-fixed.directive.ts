import { Directive, HostListener, ElementRef, OnInit, Renderer2, Input,AfterViewInit, OnChanges } from '@angular/core';
@Directive({
    selector: '[scrollToFixed]'
})
export class ScrollToFixed implements OnInit,AfterViewInit,OnChanges {
 
    
    @Input() helperDivWidth: string ;
    @Input() enable=false;
    @Input() width;
    @Input() divType;
    @Input() tabWidth;
    private offsetTop: number = 0;
    private helperDiv: HTMLElement;

    constructor(private elRef: ElementRef, private renderer: Renderer2) { }

    ngOnInit() {
    
      }
      ngOnChanges(changes){
      if(this.width > 0){
        this.helperDivWidth=this.width +"px";
      }
      if(this.enable){
          this.renderer.removeClass(this.elRef.nativeElement, 'scroll-to-fixed')
          this.renderer.setStyle(this.helperDiv, 'display', 'none');
          this.renderer.removeStyle(this.elRef.nativeElement,"width");
      }
      }
      ngAfterViewInit(){
        if(!this.enable){
          this.helperDivWidth=this.elRef.nativeElement.getBoundingClientRect().width +"px";
        const nativeEl = this.elRef.nativeElement;
         this.offsetTop = nativeEl.offsetTop - (parseFloat(nativeEl.style.marginTop.replace(/auto/, '0')) || 0);
       
        this.helperDiv = this.renderer.createElement('div');
        this.renderer.setStyle(this.helperDiv, 'display', 'none');
        this.renderer.setStyle(this.elRef.nativeElement, 'width', this.helperDivWidth);

        this.renderer.insertBefore(this.renderer.parentNode(this.elRef.nativeElement), this.helperDiv, this.elRef.nativeElement);
      }
    }
      @HostListener('window:scroll', ['$event'])
      onScroll(event) {
          const winScrollTop = event.target.scrollingElement.scrollTop;
          // console.log("offsettop",winScrollTop);
          if(!this.enable){
            if (winScrollTop > 172 && this.divType === "tab") {
              this.renderer.addClass(this.elRef.nativeElement, 'scroll-to-fixed')
              this.renderer.setStyle(this.helperDiv, 'display', 'block');
              //   this.renderer.setAttribute(this.elRef.nativeElement,"width",this.helperDivWidth);
              this.renderer.setStyle(this.elRef.nativeElement, 'width', this.tabWidth+"px");
              this.renderer.setStyle(this.elRef.nativeElement, 'background-color', "white");

            }
            if (winScrollTop > 172 && this.divType === "player") {
                this.renderer.addClass(this.elRef.nativeElement, 'scroll-to-fixed')
                this.renderer.setStyle(this.helperDiv, 'display', 'block');
              //   this.renderer.setAttribute(this.elRef.nativeElement,"width",this.helperDivWidth);
              this.renderer.setStyle(this.elRef.nativeElement, 'width', this.helperDivWidth);
              this.renderer.setStyle(this.elRef.nativeElement, 'top', '38px');
            }
            if (winScrollTop < 172) {
                this.renderer.removeClass(this.elRef.nativeElement, 'scroll-to-fixed')
                this.renderer.setStyle(this.helperDiv, 'display', 'none');
                this.renderer.removeStyle(this.elRef.nativeElement, 'background-color');
                this.renderer.removeStyle(this.elRef.nativeElement,"width");
                this.renderer.removeStyle(this.elRef.nativeElement, 'top');
            }
        }
      
      }
      @HostListener('window:resize', ['$event'])
      onResize(event) {
        if(this.width > 0 && !this.enable){
          this.renderer.setStyle(this.elRef.nativeElement, 'width', this.helperDivWidth);
          //console.log("on resize",this.helperDivWidth);
        }
      }
}