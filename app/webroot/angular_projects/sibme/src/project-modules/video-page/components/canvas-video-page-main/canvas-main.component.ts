import { Component, OnInit, ViewChild, TemplateRef, OnDestroy, Input } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { DeviceDetectorService } from 'ngx-device-detector';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import * as _ from "underscore";

import { HeaderService, SocketService, AppMainService } from "@app/services";
import { MainService, PlayerService, CropPlayerService, VideoPageService } from '@videoPage/services';
import { environment } from "@environments/environment";
import { CommentOrAttachmentCountInterface } from '@videoPage/interfaces';
import { GLOBAL_CONSTANTS } from '@constants/constant';
import { MARKER_COLORS } from '@videoPage/constants';
import { ArtifactShareModelComponent } from "@shared/modals";

type RecordingState = 'comfortZone' | 'recording' | 'resume' | 'uploading' | 'play';
type VideoCommentPlayState = 'on' | 'off';
import { PageType, BreadCrumbInterface } from "@app/interfaces";

@Component({
  selector: 'canvas-video-page-main',
  templateUrl: './canvas-main.component.html',
  styleUrls: ['./canvas-main.component.css']
})
export class CanvasMainComponent implements OnInit, OnDestroy {
  private videoPageType = GLOBAL_CONSTANTS.VIDEO_PAGE_TYPE;
  public description;
  public descShow:boolean = false;

  public loaded: boolean = false;
  public src;
  public newComment;
  public settings;
  public VideoCurrentTime;
  public VideoTotalTime;
  public comments = [];
  public colorClasses: string[] = MARKER_COLORS;
  public CustomMarkers;
  public totalStats: any = {};
  public rubrics;
  public currentTab;
  public selectedRubrics;
  public ShowInfo;
  public VideoInfo;
  public params;
  private currentTimeInSeconds;
  public staticFiles: any[];
  public isPlaying;
  public currentComment;
  public EditMode;
  public editableRubrics;
  public EditableComment;
  public modalRef: BsModalRef;
  private DeletableFile;
  public PLTabData;
  public CopyData;
  public SelectedPLRubric;
  public tagIds: number[] = [];
  public ratingIds: number[] = [];
  public PLcomments;
  public permissions;
  public isAuthenticatedUser;
  public currnetUser;
  public cropRange;
  public cropRangeSliderConfig;
  public cropStartValue;
  public cropEndValue;
  public videoEnd;
  public previousStart;
  public previousEnd;
  public captureCount = 0;
  public IsCropPlayerReady;
  public currentCopyTab;
  public isCroLoading;
  public frameworks;
  public VideoHuddleFramework;
  public rubricPreview;
  public isVideoProcessing;
  public EditChanges;
  public header_color;
  public primery_button_color;
  public secondry_button_color;
  public header_data;
  private userCurrAcc: any;
  public translation: any = {};
  public account_folder_id;
  public video_id;
  public autoSaveAudio = "";
  public categoriesString = "";
  public remainingCatCount = 0;
  private subscriptions: Subscription = new Subscription();
  public thubnail_url;
  public subtitles_data:any=[];
  public subtitles_data_available:boolean = false;
  public trim_copy_comments:boolean = true;
  public isCinemaModeOn = false;
  transCodingStatus = null;
  bsModalRef: BsModalRef;
  public userAccountLevelRoleId: number | string = null;
  @ViewChild('staticTabs', { static: false }) staticTabs: TabsetComponent;
  @ViewChild('cropDialog', { static: true }) cropDialog;
  @Input() huddle_permission;

  isAudio: boolean = false;
  errorStatus: any;
  public isIEOpened = false;
  /**Edit comment variables start */
  public availableHours: number[] = [...Array(24).keys()];
  public availableMntsNScnds: number[] = [...Array(60).keys()];
  public commentEndTime: any = [];
  /**Edit comment variables end */

  /** Comment play variables start */
  public videoCommentPlayAvailable: boolean = false;
  public videoIsLessAutoCommentPlayRange: boolean = true;
  private videoCommentPlayTimeSlots: any[] = [];
  public videoCommentPlayState: VideoCommentPlayState = 'off';
  /** Comment play variables end */

  public pageType: PageType;

  private apiUrl: string;

  public autoPlayTooltip: string;

  constructor(private videoPageService: VideoPageService, private modalService: BsModalService,
    private toastr: ShowToasterService, private headerService: HeaderService, public mainService: MainService,
    private activatedRoute: ActivatedRoute, private playerService: PlayerService, private cropPlayerService: CropPlayerService,
    private deviceService: DeviceDetectorService, private socketService: SocketService, private appMainService: AppMainService, private router: Router) {

    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.VideoCurrentTime = this.translation.all_video;
      if (this.pageType === 'huddle-page') this.autoPlayTooltip = this.translation.comments_autoplay;
      else if (this.pageType === 'workspace-page') this.autoPlayTooltip = this.translation.notes_autoplay;
    }));

    this.playerService.videoCommentPlay$.subscribe(data => this.videoCommentPlayState = data.state);
    this.isIEOpened = this.headerService.isOpenedInIE();

    this.subscriptions.add(this.videoPageService.currentTab$.subscribe((tabValue: number) => {
      this.staticTabs.tabs[tabValue].active = true;
      this.currentTab = tabValue;
    }));

    this.subscriptions.add(this.videoPageService.totalStats$
      .subscribe((totalStats: CommentOrAttachmentCountInterface[]) => totalStats.forEach(item => this.totalStats[item.key] = item.value)));

    this.subscriptions.add(this.videoPageService.staticFiles$.subscribe((staticFiles: any[]) => this.staticFiles = staticFiles));

    this.activatedRoute.parent.data.subscribe(data => {
      if (data) {
        if (data.pageType === this.videoPageType.HUDDLE) {
          this.pageType = 'huddle-page';
          this.apiUrl = 'view_page';
        } else if (data.pageType === this.videoPageType.WORKSPACE) {
          this.pageType = 'workspace-page';
          this.apiUrl = 'workspace_video_page';
        } else if (data.pageType === this.videoPageType.LIBRARY) {
          this.pageType = 'library-page';
          this.apiUrl = 'view_page';
        }
      }
    });
    this.checkCinemaMode();
  }

  ngOnInit() {

    this.isAuthenticatedUser = false;
    this.settings = { EnterToPost: true, PauseWhileTyping: true };
    this.newComment = {};
    this.newComment.files = [];
    this.newComment.timeEnabled = true;
    this.currentTab = 0;
    this.selectedRubrics = [];
    this.ShowInfo = false;
    this.currentComment = {};
    this.EditMode = false;
    this.editableRubrics = [];
    this.EditableComment = {};
    this.CopyData = {};
    this.CopyData.searchHuddleText = "";
    this.CopyData.searchAccountText = "";
    this.CopyData.LibrarySelected = false;
    this.permissions = {};
    this.isCroLoading = false;
    this.VideoHuddleFramework = "";
    this.EditChanges = {};
    this.cropRangeSliderConfig = {
      connect: [false, true, false],
      step: 1,
      range: { min: 1, max: 62 }
    };

    this.header_data = this.headerService.getStaticHeaderData();
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');

    // Dynamic Button Colors Start
    this.header_color = this.header_data.header_color;
    this.primery_button_color = this.header_data.primery_button_color;
    this.secondry_button_color = this.header_data.secondry_button_color;
    // Dynamic Button Colors End

    this.currnetUser = this.header_data.user_current_account.User;
    this.userAccountLevelRoleId = this.header_data.user_permissions.roles.role_id;

    this.appMainService.getFrameworks(this.header_data.user_current_account.accounts.account_id).subscribe((frameworks) => {
      this.frameworks = frameworks;
      if (this.frameworks.length == 1) {
        setTimeout(() => {
          this.VideoHuddleFramework = this.frameworks[0].account_tag_id;
          this.ResolveAssignFramework(1, true);
        }, 3000);
      }
    });

    this.activatedRoute.params.subscribe((p) => {
      console.log('p: ', p);
      this.params = p;
      this.device_detector();
      this.handleParams(p);
      
    });

    this.RunSubscribers();
    window.onbeforeunload = () => this.ngOnDestroy();

    this.account_folder_id = this.params.huddle_id;
    this.video_id = this.params.video_id;
    this.socketPushFunctionComment();

  }

  private RunSubscribers() {
    this.GetVideoDuration();

    this.mainService.PushableRubric.subscribe((r) => {

      if (this.EditMode) {
        this.EditChanges.changed_standards = true;
      }
      if (r.selected) {

        this.selectedRubrics.push(JSON.parse(JSON.stringify(r)));

      } else {

        this.selectedRubrics = this.selectedRubrics.filter((rub) => {

          return r.account_tag_id != rub.account_tag_id;

        });

      }

    });

    this.GetCopyData();

  }

  private GetVideoDuration() {

    this.mainService.GetVideoDuration(this.params.video_id).subscribe((data) => {

      if (data != -1) {
        this.VideoTotalTime = Number(data);
      }


    });

  }

  public Seek(val) {

    if (val == this.translation.vd_all_videos.trim() || val == 0) return;
    this.playerService.Seek.emit(this.FormatToSeconds(val));

  }

  public init_crop() {
    if (this.params.init_crop && this.params.init_crop == 1) {
      setTimeout(() => {
        if (this.src && this.src.path) {
          this.InitiateCropDialog(this.cropDialog);

        }
      });
    }
  }

  public GetCopyData() {
    let obj = {
      user_id: this.userCurrAcc.User.id,
      account_id: this.userCurrAcc.accounts.account_id
    }
    this.mainService.GetCopyData(obj).subscribe((data) => {
      this.CopyData = data;
      if (this.CopyData.all_huddles && this.CopyData.all_huddles.length > 0) this.onSearchHuddleChange("");

      if (this.CopyData.all_accounts && this.CopyData.all_accounts.length > 0) this.onSearchAccountChange("");
    });

  }

  public GetRubricById(framework_id, assign?) {
    if (!framework_id) {
      this.rubricPreview = {};
      return;
    }

    if (framework_id) {
      this.appMainService.getFrameworkSettingsById(framework_id).subscribe((rubrics: any) => {
        this.rubricPreview = rubrics.data;
        if (assign) this.rubrics = this.rubricPreview;
      });
    }
  }

  public onSearchHuddleChange(newVal) {
    if (this.CopyData.searchHuddleText == '' || !this.CopyData.searchHuddleText) {
      this.CopyData.all_huddles.forEach((h) => {
        h.valid = true;
      });
      return;
    }
    this.CopyData.all_huddles.forEach((h) => {
      h.valid = h.name.toLowerCase().indexOf(this.CopyData.searchHuddleText.toLowerCase()) > -1;
    });
    this.UpdateMatches();
  }

  private UpdateMatches() {
    this.CopyData.huddles_matched = _.where(this.CopyData.all_huddles, { valid: true }).length;
    this.CopyData.accounts_matched = _.where(this.CopyData.all_accounts, { valid: true }).length;
  }

  public ResolveCopyVideo(flag) {

    if (flag == 0) {

      if (this.CopyData.all_huddles) {

        this.CopyData.all_huddles.forEach((h) => { h.selected = false; })
      }

      if (this.CopyData.all_accounts) {

        this.CopyData.all_accounts.forEach((ac) => { ac.selected = false; })
      }

      this.CopyData.LibrarySelected = false;

      this.modalRef.hide();


    } else {

      let selectedAccounts = _.where(this.CopyData.all_accounts, { selected: true });

      let selectedHuddles = _.where(this.CopyData.all_huddles, { selected: true });

      if ((selectedHuddles && selectedHuddles.length > 0) || this.CopyData.LibrarySelected) {

        let ids = selectedHuddles.map((ac) => { return ac.account_folder_id; });

        if (this.CopyData.LibrarySelected) { ids.push("-1") }

        let obj = {

          document_id: this.params.video_id,
          account_folder_id: ids,
          current_huddle_id: this.params.huddle_id,
          account_id: this.userCurrAcc.accounts.account_id,
          user_id: this.userCurrAcc.User.id,
          copy_notes: this.CopyData.CopyComments ? 1 : 0
        }
        this.mainService.CopyToHuddlesAndLib(obj).subscribe((data: any) => this.toastr.ShowToastr('info',data.message));

      }

      if (selectedAccounts && selectedAccounts.length > 0) {
        let ids = selectedAccounts.map((ac) => { return ac.account_id; });
        const obj = {
          account_ids: ids,
          document_id: this.params.video_id,
          user_id: this.userCurrAcc.User.id,
          copy_notes: this.CopyData.CopyComments ? 1 : 0
        }
        this.modalRef.hide();
        this.mainService.CopyToAccounts(obj).subscribe((data: any) => this.toastr.ShowToastr('info',data.message));
      }

      if (selectedAccounts.length == 0 && selectedHuddles.length == 0 && !this.CopyData.LibrarySelected)
        this.toastr.ShowToastr('info',this.translation.vd_select_huddle_to_copy);

      this.ResolveCopyVideo(0);
    }

  }

  public onSearchAccountChange(newVal) {
    if (this.CopyData.searchAccountText == '' || !this.CopyData.searchAccountText) {
      this.CopyData.all_accounts.forEach((ac) => ac.valid = true);
      return;
    }

    this.CopyData.all_accounts.forEach((ac) => ac.valid = ac.company_name.toLowerCase().indexOf(this.CopyData.searchAccountText.toLowerCase()) > -1);
    this.UpdateMatches();
  }

  public InitiateCopyDialog(template: TemplateRef<any>, file) {
    this.modalRef = this.modalService.show(template, { class: "modal-md model-copy" });
  }

  public InitiateCropDialog(template: TemplateRef<any>, file?) {
    this.playerService.ModifyPlayingState("pause");

    if (document.getElementsByTagName("modal-container").length < 1) {
      this.isCroLoading = true;
      setTimeout(() => {
        this.isCroLoading = false;
        this.cropRange = [0, 0];
        this.cropStartValue = "00:00";
        this.cropEndValue = "00:00";
        this.modalRef = this.modalService.show(template, { class: "modal-lg model-crop", backdrop: 'static', animated: false });
        (document.querySelector('modal-container') as HTMLElement).addEventListener('click', function () {
        });
        (document.querySelector('.modal-content') as HTMLElement).addEventListener('click', function (e) {
          e.stopPropagation();
        });
      }, 1000);
    } else {
      (document.querySelector('bs-modal-backdrop') as HTMLElement).style.display = "block";
      (document.querySelector('modal-container') as HTMLElement).style.display = "block";
      (document.querySelector('body') as HTMLElement).style.overflow = "hidden";
    }
  }

  public hideCropModal() {
    (document.querySelector('bs-modal-backdrop') as HTMLElement).style.display = "none";
    (document.querySelector('modal-container') as HTMLElement).style.display = "none";
    (document.querySelector('body') as HTMLElement).style.overflow = "visible";
  }

  public TrimVideo() {
    const user_id = this.userCurrAcc.User.id;
    if (this.cropRange[0] == this.cropRange[1]) {
      alert(this.translation.please_select_range);
    } else {
      const account_id = this.userCurrAcc.accounts.account_id;
      let data = {
        copy_comments: this.trim_copy_comments ,
        startVideo: parseFloat(this.cropRange[0] + ".00").toFixed(2),
        endVideo: parseFloat(this.cropRange[1] + ".00").toFixed(2) }
      if (this.pageType == 'library-page') {
        data['library'] = 1;
      }

      this.mainService.TrimVideo(this.params.video_id, this.params.huddle_id, data).subscribe((data: any) => {

        let path: any;
        let user_id = this.userCurrAcc.User.id;
        let is_huddle = this.pageType == 'huddle-page';
        let dynamicFunction = 'afterHuddleTrim';
        let redirectPath = `/video_huddles/huddle/details/${this.params.huddle_id}/artifacts/grid`;


        if (!is_huddle) {
          redirectPath = '/workspace/workspace/home/grid';
          dynamicFunction = 'afterWorkspaceTrim';
        }
          if(this.trim_copy_comments)
          {
              this.headerService[dynamicFunction](this.params.video_id, data["document"].doc_id, parseFloat(this.cropRange[0] + ".00").toFixed(2), parseFloat(this.cropRange[1] + ".00").toFixed(2), user_id, this.params.huddle_id).subscribe(data => {
                  this.router.navigate([redirectPath]);
              });
          }
          else
          {
              this.router.navigate([redirectPath]);
          }
      });
    }
  }

  public CaptureTotalTime(VideoTotalTime) {
    if (this.captureCount == 0) {
      let videoStart = 0;
      let videoEnd = Math.floor(this.VideoTotalTime) || Math.floor(VideoTotalTime);
      this.videoEnd = videoEnd;
      let that = this;
      this.cropRangeSliderConfig.range.min = videoStart;
      this.previousStart = videoStart;
      this.cropRangeSliderConfig.range.max = videoEnd;

      this.previousEnd = videoEnd;
      this.cropRange = [videoStart, videoEnd];
      this.convertValuesToTime([videoStart, videoEnd], 0);
      this.convertValuesToTime([videoStart, videoEnd], 1);
      this.videoEnd = this.cropEndValue;
      setTimeout(() => {
        that.IsCropPlayerReady = true;
      }, 1000);

    }
    this.captureCount++;
  }

  public onCropSliderChange(ev) {
    let handle = 0;
    if (this.previousEnd != ev[1]) handle = 1;
    if (handle == 0) this.cropPlayerService.SeekTo(ev[0], 0);
    else {
      this.cropPlayerService.SeekTo(ev[1], 0);
      this.cropPlayerService.seekEnd(ev[1], 0);
    }

    this.convertValuesToTime(ev, handle);
    this.previousStart = ev[0];
    this.previousEnd = ev[1];
  }

  convertValuesToTime(values, handle) {
    let hours = 0,
      minutes = 0,
      seconds = 0;

    if (handle === 0) {
      hours = this.convertToHour(values[0]);
      minutes = this.convertToMinute(values[0], hours);
      seconds = this.convertToSecond(values[0], minutes, hours);
      this.cropStartValue = this.formatHoursAndMinutes(hours, minutes, seconds);
      return;
    }

    hours = this.convertToHour(values[1]);
    minutes = this.convertToMinute(values[1], hours);
    seconds = this.convertToSecond(values[1], minutes, hours);
    this.cropEndValue = this.formatHoursAndMinutes(hours, minutes, seconds);
  }

  convertToHour(value) {
    return Math.floor(value / 3600);
  }

  convertToMinute(value, hour) {
    return (Math.floor(value / 60) - (hour * 60));
  }

  convertToSecond(value, minute, hour) {
    return (value - (minute * 60) - (hour * 3600));
  }

  formatHoursAndMinutes(hours, minutes, seconds) {
    if (hours.toString().length == 1) hours = '0' + hours;
    if (minutes.toString().length == 1) minutes = '0' + minutes;
    if (seconds.toString().length == 1) seconds = '0' + seconds;
    if (hours == '00' || hours == 0) {
      return minutes + ':' + seconds;
    }
    else {
      return hours + ':' + minutes + ':' + Math.floor(seconds);
    }
  }

  public CropPreviewVideo() {
    this.cropPlayerService.ModifyPlayingState(["play", this.cropRange[0], this.cropRange[1]]);
  }

  public InitiateDeleteResource(template: TemplateRef<any>, file) {
    this.DeletableFile = file;
    this.modalRef = this.modalService.show(template, { class: "modal-md" });
  }

  public ResolveDeleteFile(flag, id?) {
    if (flag == 0) {
      this.modalRef.hide();
    } else {

      if (this.modalRef) this.modalRef.hide();
      let obj:any = {
        huddle_id: this.params.huddle_id,
        video_id: this.params.video_id,
        document_id: id ? id : this.DeletableFile.id,
        user_id: this.userCurrAcc.User.id,
      }
        if (this.pageType == 'library-page') {
            obj.library = 1;
        }
      this.mainService.DeleteResource(obj).subscribe();
    }
  }

  public UploadResource(file, wholeVideo?, commentId?) {

    if (wholeVideo) file.time = 0;
    let obj: any = {
      huddle_id: this.params.huddle_id,
      account_id: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id,
      video_id: this.params.video_id,
      stack_url: file.url,
      video_url: file.key,
      video_file_name: file.filename,
      video_file_size: file.size,
      video_desc: "",
      current_user_role_id: this.userCurrAcc.roles.role_id,
      current_user_email: this.userCurrAcc.User.email,
      url_stack_check: 1,
      account_role_id: this.userCurrAcc.users_accounts.role_id
    };

    if (commentId) {
      let comment = _.findWhere(this.comments, { id: commentId });
      if (comment) obj.time = comment.time;
      obj.comment_id = commentId;
    }
      if (this.pageType == 'library-page') {
          obj.library = 1;
      }

    if (!obj.time && obj.time != 0) obj.time = this.FormatToSeconds(file.time);
    this.appMainService.UploadResource(obj).subscribe((data: any) => {
      file.id = data.document_id;
    });
  }

  private FormatToSeconds(time) {
    if (time == 0) return 0;
    if (typeof (time) == "number") return time;
    let stamp = time.split(":");
    return Number(stamp[0] * 3600) + Number(stamp[1] * 60) + Number(stamp[2]);
  }

  public RemoveSelectedRubric(rubric, i) {

    if (rubric) {

      let index = _.findIndex(this.rubrics.account_tag_type_0, { account_tag_id: rubric.account_tag_id });

      if (index > -1) {

        this.rubrics.account_tag_type_0[index].selected = false;

        this.selectedRubrics.splice(i, 1);

      }

    }

  }

  private GetRubrics(huddle_id, account_id) {

    this.mainService.GetRubrics({ huddle_id: huddle_id, account_id: account_id, video_id: this.params.video_id }).subscribe((data: any) => {

      this.rubrics = data.data;

      /** In case of add comment local storage selected framewors then also select framework rubric start */
      let datafromlS = localStorage.getItem(this.header_data.user_current_account.User.id + '_video_play_comment_' + this.params.video_id);
      let ParsedDataFromLs = JSON.parse(datafromlS);
      if (ParsedDataFromLs && Array.isArray(ParsedDataFromLs.selectedRubrics)) {
        ParsedDataFromLs.selectedRubrics.forEach(lsRubric => {
          let rubricExisted = this.rubrics.account_tag_type_0.find(rubric => rubric.account_tag_id == lsRubric.account_tag_id);
          if (rubricExisted) rubricExisted.selected = true;
        });
      }
      /** In case of add comment local storage selected framewors then also select framework rubric end */

    });

  }

  public TranscribeTab(ev) {
    this.closeInfo();
    this.currentTab = ev;


  }
  public SelectTab(ev) {
    this.closeInfo();
    this.currentTab = ev;

    if (ev == 4) {
      this.LoadRubricsTabData();
    }
  }

  public LoadRubricsTabData(callOnSocket?, selectedStandardId?, selectedRatingId?) {
    if (!callOnSocket) {
      this.SelectedPLRubric = {};
      this.PLcomments = [];
    }

    const obj = {
      account_id: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id,
      huddle_id: this.params.huddle_id,
      video_id: this.params.video_id
    }

    this.appMainService.GetPLTabData(obj).subscribe((data) => {
      this.PLTabData = data;
      if (this.SelectedPLRubric && this.SelectedPLRubric.account_tag_id && selectedStandardId && selectedRatingId) {
        if (this.SelectedPLRubric.account_tag_id == selectedStandardId) this.SelectedPLRubric.selectedRating = selectedRatingId;
      }
    });
  }

  public OnRubricClicked(rubric) {

    this.SelectedPLRubric = rubric;

    this.SelectedPLRubric.selectOptions = this.getPls();

    if (this.getSettings("enable_unique_desc") == 1) {

      this.SelectedPLRubric.descriptions = [];

      this.SelectedPLRubric.selectOptions.forEach((opt) => {

        let obj = _.findWhere(this.SelectedPLRubric.performance_level_descriptions, { performance_level_id: opt.id });

        if (obj)
          this.SelectedPLRubric.descriptions.push({ label: opt.text, text: obj.description });

      });

    }

    if (this.SelectedPLRubric.get_selected_rating)
      this.SelectedPLRubric.selectedRating = this.SelectedPLRubric.get_selected_rating.rating_id;
    else this.SelectedPLRubric.selectedRating = 0;
    this.PLcomments = [];

    const obj = {

      huddle_id: this.params.huddle_id,
      account_id: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id,
      video_id: this.params.video_id,
      account_tag_id: rubric.account_tag_id

    };
    if (Number(this.SelectedPLRubric.get_standard_tagged_count) > 0) {

      this.mainService.GetPLComments(obj).subscribe((data: any) => {

        this.PLcomments = data.videoComments;

      });

    }
  }

  public RatingChanged(id) {
    id = Number(id);
    this.tagIds.push(this.SelectedPLRubric.account_tag_id);
    this.ratingIds.push(id);

    const val = _.findWhere(this.SelectedPLRubric.selectOptions, { id: this.ratingIds[this.ratingIds.length - 1] });

    const obj = {
      standard_ids: [this.tagIds[this.tagIds.length - 1]],
      huddle_id: this.params.huddle_id,
      video_id: this.params.video_id,
      account_id: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id,
      account_role_id: this.userCurrAcc.users_accounts.role_id,
      current_user_email: this.userCurrAcc.User.email
    };

    obj["rating_value_" + this.tagIds[this.tagIds.length - 1]] = val ? val.value : 0;
    obj["rating_id_" + this.tagIds[this.tagIds.length - 1]] = this.ratingIds[this.ratingIds.length - 1];

    this.mainService.SaveRating(obj).subscribe((data: any) => {
      // update local variables for rating
      let selectedPLRubric = this.PLTabData.standards.data.account_tag_type_0.find(rubric => {
        return rubric.account_tag_id == this.tagIds[this.tagIds.length - 1];
      });
      if (selectedPLRubric) {
        if (selectedPLRubric.get_selected_rating)
          selectedPLRubric.get_selected_rating.rating_id = this.ratingIds[this.ratingIds.length - 1];
        else
          selectedPLRubric.get_selected_rating = { rating_id: this.ratingIds[this.ratingIds.length - 1] };
      }
      this.tagIds.shift();
      this.ratingIds.shift();

    });

  }

  public getPls() {
    if (this.getSettings("enable_performance_level") == 1) {
      return this.GetDropdownSettingsFromPL(this.SelectedPLRubric);
    } else {
      return this.PLTabData.old_ratings.map((fr) => {
        return { id: fr.account_meta_data_id, value: fr.meta_data_value, text: fr.meta_data_name.substring(fr.meta_data_name.lastIndexOf("_") + 1, fr.meta_data_name.length) };
      });
    }
  }

  public getSettings(key) {
    return this.PLTabData.standards.data.account_framework_settings[key];
  }

  private GetDropdownSettingsFromPL(rubric: any) {
    return rubric.account_framework_settings_performance_levels.map((r) => {
      return { id: r.id, value: r.performance_level_rating, text: r.performance_level };
    });
  }

  private handleParams(args) {

    const user_id = this.userCurrAcc.User.id;

    this.mainService.GetVideoResources({ video_id: args.video_id, huddle_id: args.huddle_id, user_id: user_id }).subscribe((files: any) => {

      if (files) {
        files.map((file) => {
          if (file.scripted_current_duration) file.time = this.ConvertTime(file.scripted_current_duration);
          else file.time = this.translation.vd_all_videos;
        });
        this.videoPageService.updateStaticFiles(files);
      }
    });

    const account_id = this.userCurrAcc.accounts.account_id;
    let data = {
      "user_id": user_id,
      "video_id": args.video_id,
      "account_id": account_id,
      "huddle_id": args.huddle_id,
      "role_id": this.userCurrAcc.roles.role_id,
      "permission_maintain_folders": 0
    }
    if (this.pageType == 'library-page') {
     data['library'] = 1;

    }
    this.GetRubrics(args.huddle_id, account_id);
    this.mainService.getVideoData(this.apiUrl, data).subscribe((data: any) => {
      this.handleVideo(data);
    });
  }

  public getPLTabPermission() {
    if (this.VideoInfo.h_type == 2) {
      return this.permissions.coaching_perfomance_level;
    } else if (this.VideoInfo.h_type == 3) {
      return this.permissions.assessment_perfomance_level;
    }
  }

  public getCoachingSummaryCheck() {
    if (this.VideoInfo.h_type == 2) {
      if (this.currnetUser.huddle_role_id == '210') {
        if (!this.permissions.coaching_perfomance_level) {
          return false;
        } else {
          if (this.permissions.can_view_summary == "1") {
            return true;
          } else {
            return false;
          }
        }
      } else {
        return true;
      }

    } else if (this.VideoInfo.h_type == 3) {

      if (!this.permissions.assessment_perfomance_level) {
        return false;
      } else {
        return this.permissions.can_view_summary || this.permissions.assessment_summary_check;
      }

    } else {
      return false;
    }
  }

  private handleVideo(data) {
    if (data.h_type == 3 && this.rubrics) {
      if (data.user_huddle_level_permissions == 210 || data.user_huddle_level_permissions == 220) {
        this.rubrics.account_framework_settings.checkbox_level = -1;
      }
    }
    if (!data.success) {
      this.toastr.ShowToastr('info',this.translation.u_dont_permission_vd);
      setTimeout(() => location.href = environment.baseUrl, 1000);
      return;
    }

    this.isAuthenticatedUser = true;
    this.src = { "path": data.static_url, "type": "video/mp4" };
    this.comments = data.comments.Document.comments;
    this.CustomMarkers = data.custom_markers;
    this.videoPageService.updateTotalStats([{ key: 'comment_count', value: data.comments_counts.total_comments }, { key: 'resources_count', value: data.attached_document_numbers }]);
    this.VideoInfo = data.video_detail;
    console.log(' this.VideoInfo: ',  this.VideoInfo);
    this.VideoInfo.videoCategories = data.categories;
    this.makeCategoryString(data.categories);
    this.thubnail_url = data.thubnail_url;
    this.VideoInfo.h_type = data.h_type;
    this.VideoInfo.huddle_type = data.huddle_type;
    if(this.VideoInfo && this.VideoInfo.subtitle_vtt)
    {
    this.getSubtitlesData();
    }
    /** TODO: new code to update settings start */

    this.videoPageService.updateCommentTypingSettings({ key: 'PauseWhileTyping', value: Boolean(data.user_pause_settings) });
    this.videoPageService.updateCommentTypingSettings({ key: 'autoscroll', value: Boolean(data.user_autoscroll_settings) });
    /** TODO: new code to update settings end */
    this.permissions.rubric_check = data.rubric_check;
    this.permissions.can_download = data.can_download;

    this.permissions.can_view_summary = data.can_view_summary;
    this.permissions.coaching_summary_check = data.coaching_summary_check;
    this.permissions.assessment_summary_check = data.assessment_summary_check;


    this.permissions.can_rate = data.can_rate;
    this.permissions.huddle_permission = data.user_huddle_level_permissions;

    this.permissions.AllowCustomMarkers = data.video_markers_check == "1";
    this.permissions.coaching_perfomance_level = data.coaching_perfomance_level == "1";

    this.permissions.assessment_perfomance_level = data.assessment_perfomance_level == "1";

    if (this.pageType === 'workspace-page') {
      this.permissions.performance_level_check = data.coaching_perfomance_level;
      this.permissions.can_comment = true;
      this.permissions.can_reply = true;
      this.permissions.showCopy = true;
      this.permissions.can_crop_video = true;
      this.VideoInfo.defaultFramework = data.account_framework;
    } else {
      this.permissions.performance_level_check = data.performance_level_check;
      this.permissions.can_comment = data.can_comment;
      this.permissions.can_reply = data.can_reply;
      this.permissions.showCopy = data.can_dl_edit_delete_copy_video;
      this.permissions.can_crop_video = !!data.can_crop_video;
      this.VideoInfo.defaultFramework = data.default_framework;
    }

    this.loaded = true;

    data.huddle_info.huddle_title = this.VideoInfo.title;
    this.mainService.breadcrumbs.emit(data.bread_crumb_output);
    this.mainService.huddleInfo.emit(data.huddle_info);
    this.VideoInfo.coaching_link = data.coaching_link;
    this.VideoInfo.assessment_link = data.assessment_link;
    this.permissions.framework_selected_for_video = data.framework_selected_for_video;
    this.permissions.allow_per_video = data.allow_per_video;
    this.permissions.get_account_video_library_permissions = data.get_account_video_library_permissions;
    this.permissions.permission_video_library_upload = this.userCurrAcc.users_accounts.permission_video_library_upload == 1;
    this.permissions.coachee_permission = data.coachee_permission;
    this.currnetUser.huddle_role_id = data.user_huddle_level_permissions;

    this.VideoTotalTime = data.video_duration;
    this.isVideoProcessing = data.video_detail.published == 0;

    this.updateBreadCrumb(this.VideoInfo.title, { id: data.huddle_info.account_folder_id, name: data.huddle_info.name });

    this.transCodingStatus = data.video_detail.transcoding_status;
    this.errorStatus = data.video_detail.encoder_status == 'Error'

    if (this.headerService.isAValidAudio(data.video_detail.file_type)) this.isAudio = true;
    if (this.permissions.can_crop_video) this.init_crop();
    if (this.VideoInfo.defaultFramework != "0" && this.VideoInfo.defaultFramework != "-1") {
      this.VideoHuddleFramework = this.VideoInfo.defaultFramework;
      this.GetRubricById(this.VideoInfo.defaultFramework);
    }

    if (this.VideoInfo.h_type == 2) {
      this.VideoInfo.coachee_name = data.coachee_name;
      this.VideoInfo.coaches_name = data.coaches_name;
    } else if (this.VideoInfo.h_type == 3) {

      this.VideoInfo.assessor_names = data.assessor_names;
      this.VideoInfo.eval_participant_names = data.eval_participant_names;

    }
    if (this.pageType == 'library-page') {
     this.description=data.video_detail.desc;
     this.description ? this.descShow = true : this.descShow = false;
    }
  }

  public makeCategoryString(categories)
  {
      if(categories && categories.length)
      {
          this.remainingCatCount = 0;
          categories.forEach((category) => {
              if(this.categoriesString.length <= 60)
              {
                  this.categoriesString += category.name;
                  if(category.childs && category.childs.length)
                  {
                      this.categoriesString += " > ";
                      for(let subCat of category.childs)
                      {
                          if(this.categoriesString.length <= 60)
                          {
                              this.categoriesString += subCat.name + " | ";
                          }
                          else
                          {
                              this.categoriesString += "...| ";
                              break;
                          }
                      }
                      this.categoriesString = this.categoriesString.substring(0, this.categoriesString.length - 2);
                  }
                  else
                  {
                      this.categoriesString += " > "+this.translation.vl_no_subcategory;
                  }
                  this.categoriesString += ", ";
              }
              else
              {
                  this.remainingCatCount++;
              }
          });
          this.categoriesString = this.categoriesString.substring(0, this.categoriesString.length - 2);
      }
  }

  public InitiateDeleteVideo(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: "modal-md" });
  }

  public getSelectFrameworkPermission() {
    if (this.pageType === 'workspace-page') {
      if (this.permissions.framework_selected_for_video != "1") {
        return true;
      }
    } else {

      if (this.permissions.allow_per_video == "1" && (this.VideoInfo.h_type == 2 || this.VideoInfo.h_type == 1) && this.permissions.framework_selected_for_video != "1") {
        return true;
      }

      if (this.permissions.allow_per_video != "1" && (this.VideoInfo.h_type == 2 || this.VideoInfo.h_type == 1) && this.permissions.framework_selected_for_video != "1") {
        if (this.permissions.huddle_permission == 200) return true;
      }

      if (this.VideoInfo.h_type == 3 && this.permissions.framework_selected_for_video != "1") {
        if (this.permissions.huddle_permission == 200) return true;
      }
    }

  }

  public ResolveDeleteVideo(flag) {
    if (flag == 0) {
      this.modalRef.hide();
    } else {
      this.modalRef.hide();
      this.DeleteVideo();
    }
  }

  public DeleteVideo() {
    const obj = {
      huddle_id: this.params.huddle_id,
      video_id: this.params.video_id,
      user_id: this.userCurrAcc.User.id
    }
    if (this.pageType == 'library-page') {
      obj['library'] = 1;
    }

    this.mainService.DeleteVideo(obj).subscribe((data: any) => {
      if (data.sucess) {
        let msg = this.translation.vd_videos_has_been_deleted;
        let redirectPath = `/video_huddles/huddle/details/${this.params.huddle_id}/artifacts/grid`;
        if (this.pageType === 'workspace-page') {
          msg = this.translation.myfile_videos_has_been_deleted;
          redirectPath = '/workspace/workspace/home/grid';
        }
        if (this.pageType === 'library-page') {
          msg = this.translation.library_videos_has_been_deleted;
          redirectPath = '/VideoLibrary';
        }
        this.toastr.ShowToastr('info',msg);

        setTimeout(() => this.router.navigate([redirectPath]), 2000);
      } else {
        this.toastr.ShowToastr('info',data.message);
      }
    });
  }

  public DownloadVideo() {
    const obj = {
      document_id: this.params.video_id,
      huddle_id: this.params.huddle_id,
      account_id: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id
    }
    this.mainService.DownloadVideo(obj);
  }

  public PointToLink() {
    const link = this.VideoInfo.h_type == "2" ? `${environment.baseUrl}/home/trackers/coaching?date=1month` : this.VideoInfo.h_type == "3" ? `${environment.baseUrl}/home/trackers/assessment` : "";
    if (link) window.open(link, "_blank");

    setTimeout(() => this.updateCurrentTab(this.currentTab));
  }

  public updateCurrentTab(tabValue: number) {
    this.videoPageService.updateCurrentTab(tabValue);
  }

  // TODO: delete
  public onCommentDelete(comment) {
    if (!comment.id || comment.id == "") {
      this.toastr.ShowToastr('info',this.translation.please_select_comment);
      return;
    } else {
      let user_id = this.userCurrAcc.User.id;
      let obj = {
        video_id: this.params.video_id,
        comment_id: comment.id,
        user_id: user_id
      }
      if (this.pageType == 'library-page') {
        obj['library'] = 1;
      }

      this.appMainService.DeleteComment(obj).subscribe()
    };
  }

  public ResolveAssignFramework(flag, is_internal?) {
    if (flag == 0) {
      this.modalRef.hide();
    } else {

      if (!is_internal) this.modalRef.hide();

      const obj = {
        huddle_id: this.params.huddle_id,
        video_id: this.params.video_id,
        framework_id: this.VideoHuddleFramework,
        user_id: this.headerService.getUserId()
      }

      this.mainService.SetFrameworkForVideo(obj).subscribe((data: any) => {
        if (!data.status) {
          if (!is_internal) {
            this.toastr.ShowToastr('info',this.translation.vd_framework_selected_for_video);
          }
          let video_framework_id = data.video_framework_id;
          this.GetRubricById(video_framework_id, true);
          this.permissions.framework_selected_for_video = "1";
        }
      });
    }
  }

  public AssignFramework(template, flag) {
    if (flag == 0) {
      this.rubricPreview = "";
      this.VideoHuddleFramework = this.VideoInfo.defaultFramework;
      this.GetRubricById(this.VideoInfo.defaultFramework);
      return;
    } else if (flag == 1) {
      this.modalRef = this.modalService.show(template, { class: 'modal-md' });
    }
  }

  public ConvertTime(n) {

    if (!n || n == null || n == 0) return this.translation.vd_all_videos;
    let sec_num: any = parseInt(n, 10);
    let hours: any = Math.floor(sec_num / 3600);
    let minutes: any = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds: any = sec_num - (hours * 3600) - (minutes * 60);

    if (hours == 0 && minutes == 0 && seconds == 0) {
      return this.translation.vd_all_videos;
    }

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return hours + ':' + minutes + ':' + seconds;
  }

  public closeInfo(changeTabe = 0, changeToTab = 0, fromTab = 0) {
    this.ShowInfo = false;
    if (changeTabe) {
      if (fromTab == this.currentTab) {
        this.updateCurrentTab(changeToTab);
      }
    }
  }

  device_detector() {
    let huddle_id = this.params.huddle_id;
    let video_id = this.params.video_id;
    let account_id = this.userCurrAcc.accounts.account_id;
    const isMobile = this.deviceService.isMobile();
    if (isMobile) {
      // location.href = "sibme://play_video/?huddleID=" + huddle_id + "&videoID=" + video_id + "&isWorkspace=false&account_id=" + account_id;
    }

  }

  /** Socket functionality starts */
  private socketPushFunctionComment() {
    if (this.params.huddle_id) {
      this.subscriptions.add(this.socketService.pushEventWithNewLogic(`huddle-details-${this.params.huddle_id}`)
        .subscribe(data => this.processEventSubscriptions(data)));
    }

    this.subscriptions.add(this.socketService.pushEventWithNewLogic(`video-details-${this.params.video_id}`)
      .subscribe(data => this.processEventSubscriptions(data)));

    let workspaceChannelName = `workspace-${this.header_data.user_current_account.users_accounts.account_id}-${this.header_data.user_current_account.users_accounts.user_id}`;
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(workspaceChannelName)
      .subscribe(data => this.processEventSubscriptions(data)));
  }

  private processEventSubscriptions(res) {
    switch (res.event) {
      case "framework_selected":
        this.processFrameworkSelected(res.data);
        break;
      case "ratings_updated":
        this.processRatingsUpdated(res.data, res.document_id, res.huddle_id);
        break;
      case "resource_renamed":
        this.processResourceRenamed(res.data, res.is_dummy);
        break;
      case "subtitles_available":
      this.processSubtitles(res.data,res.subtitle_paths,res.subtitles_data);
      break;
    }
  }

  private processFrameworkSelected(frameworkId) {

    this.GetRubricById(frameworkId, true)
    this.permissions.framework_selected_for_video = "1";
    this.toastr.ShowToastr('info',this.translation.framework_selected);
  }

  private processRatingsUpdated(data, document_id, huddle_id) {
    if (document_id == this.params.video_id && huddle_id == this.params.huddle_id) {
      let selectedStandardId = data.input_data.standard_ids[0];
      let selectedRatingId = data.input_data[`rating_id_${selectedStandardId}`];
      this.LoadRubricsTabData(true, selectedStandardId, selectedRatingId);
    }
  }

  private processResourceRenamed(data, is_dummy) {
    if (!is_dummy && (data.id == this.params.video_id || data.doc_id == this.params.video_id)) {
      this.VideoInfo.title = data.title;
      this.mainService.updateVideoTitle(data.title);
      this.toastr.ShowToastr('info',this.translation.title_changed);
    }
  }

  /** Socket functionality end */

  public commentAdded(event: any) {
    if (event.parendCommentAdded) {
      this.LoadRubricsTabData(true);
    }
  }

  public commentEdited(event: any) {
    if (event.parendCommentEdited) {
      this.LoadRubricsTabData(true);
    }
  }

  public commentDeleted(event: any) {

  }

  public playAvailable(value: boolean) {
    this.videoCommentPlayAvailable = value;
  }

  public autoCommentPlayRange(value: boolean) {
    this.videoIsLessAutoCommentPlayRange = value;
  }

  public commentPlayState(state: VideoCommentPlayState) {
    this.videoCommentPlayState = state;
  }

  public videoCmntPlayTimeSlots(videoCmntPlayTimeSlots: any[]) {
    this.videoCommentPlayTimeSlots = videoCmntPlayTimeSlots;
  }

  public toggleVideoCommentPlayState(state: VideoCommentPlayState, videoCommentPlayAvailable: boolean = true) {
    if (videoCommentPlayAvailable) {
      this.videoCommentPlayState = state;
      this.playerService.toggleVideoCommentPlay({ state: state, timeSlots: this.videoCommentPlayTimeSlots });
    }
  }

  public checkPerformanceLevelPermission() {
    if (this.pageType === 'huddle-page')
      return this.permissions.performance_level_check && this.permissions.rubric_check && this.getPLTabPermission() && this.userAccountLevelRoleId != '125';
    else if (this.pageType === 'workspace-page')
      return this.permissions.coaching_perfomance_level && this.permissions.rubric_check && this.userAccountLevelRoleId != '125' && this.userAccountLevelRoleId != '120';
  }

  public getRatingPerm() {
    if (this.pageType === 'huddle-page')
      return this.permissions.can_rate && this.currnetUser.huddle_role_id == 200;
    else if (this.pageType === 'workspace-page')
      return true;
  }

  public getCopyToAccPerm() {
    if (this.pageType === 'huddle-page' && this.CopyData.all_accounts.length > 0) return true;
    else if (this.pageType === 'workspace-page' && this.CopyData.all_accounts.length > 1) return true;
    else return false;
  }

  private updateBreadCrumb(title: string, huddleInfo?: any) {
    const breadCrumb: BreadCrumbInterface[] = [];

    if (this.pageType === 'huddle-page') breadCrumb.push({ title: this.translation.huddle_breadcrumb_huddle, link: '/video_huddles/list' });
    else if (this.pageType === 'workspace-page') breadCrumb.push({ title: this.translation.header_list_worksapce, link: '/workspace/workspace/home/grid' });
    else if (this.pageType === 'library-page') breadCrumb.push({ title: this.translation.video_library_vl, link: '/VideoLibrary' });

    if (huddleInfo && this.pageType === 'huddle-page') breadCrumb.push({ title: huddleInfo.name, link: `/video_huddles/huddle/details/${huddleInfo.id}` });

    breadCrumb.push({ title });
    this.appMainService.updateBreadCrumb(breadCrumb);
  }
  private processSubtitles(data, subtitles,subtitles_data) {
    this.mainService.enable_subtitles.emit(subtitles.vtt);
    // this.mainService.subtitles_data.emit(subtitles_data);
    this.subtitles_data =subtitles_data;
    this.subtitles_data_available =true;
  }

  public openNewShareModal() {
    const artifact = {
      doc_type: this.VideoInfo.doc_type,
      doc_id: this.params.video_id,
      account_folder_id: this.params.huddle_id,
      total_comments: this.totalStats.comment_count
    }
    const initialState = { artifact, pageType: this.pageType, modal_for:'for_artifact_share' };
    this.modalService.show(ArtifactShareModelComponent, { initialState, class: 'share-modal-container' });
  }
  checkCinemaMode(){
    this.subscriptions.add(this.playerService._videoPlayerCinemaMode.subscribe((res) => this.isCinemaModeOn = res));
  }

  public getSubtitlesData(){
       this.mainService.getSubtitlesFromDb(this.VideoInfo.id).subscribe((data: any) => {
        this.subtitles_data = data;
        this.subtitles_data_available = true;
      });
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    if (this.modalRef) this.modalRef.hide();
  }

 
}
