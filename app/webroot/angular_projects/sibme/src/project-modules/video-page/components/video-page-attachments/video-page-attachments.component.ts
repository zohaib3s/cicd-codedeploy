import { Component, OnInit, Input, TemplateRef, ViewChild } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { ShowToasterService } from '@projectModules/app/services';
import { HeaderService, AppMainService } from "@app/services";
import { MainService, VideoPageService } from '@videoPage/services';
import { environment } from '@src/environments/environment';
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import { PdfRendererViewerComponent } from '../pdf-renderer-viewer/pdf-renderer-viewer.component';
@Component({
  selector: 'video-page-attachments',
  templateUrl: './video-page-attachments.component.html',
  styleUrls: ['./video-page-attachments.component.css']
})
export class VideoPageAttachmentsComponent implements OnInit {
  @Input('userAccountLevelRoleId') userAccountLevelRoleId: any;
  @Input('currnetUser') currnetUser: any;
  @Input('huddle_id') huddle_id: number;
  @Input('video_id') video_id: number;
  @Input('pageType') pageType: any;
  @Input('permissions') permissions:any;
  @Input('huddle_type') huddle_type:any;
  @Input('original_account') original_account:any;

  @ViewChild(PdfRendererViewerComponent) PdfRendererViewerComponent: PdfRendererViewerComponent;

  public Inputs: GeneralInputs;
  public showConfirmationDialog = false;
  public videoOptions: any = {
    maxFiles: 20,
    accept: GLOBAL_CONSTANTS.RESOURCE_UPLOAD_EXTENSIONS,
    fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'url', 'onedrive'],
    customText: {
      'File {displayName} is not an accepted file type. The accepted file types are {types}': 'File {displayName} is not an accepted file type. The accepted file types are image, text',
    }
  };

  public userCurrAcc: any;
  public currentSelectedFile: any;
  public translation: any = {};
  private subscriptions: Subscription = new Subscription();
  private DeletableFile: any;
  public staticFiles: any[];
  public modalRef: BsModalRef;
  public isNotCanvasPage = true;

  constructor(private headerService: HeaderService, private toastr: ShowToasterService,  private appMainService: AppMainService, private modalService: BsModalService, public videoPageService: VideoPageService, public mainService: MainService) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
			this.translation = languageTranslation;
			this.Inputs = { NewFolderName: "", Confirmation: "", ConfirmationKey: this.translation.Huddle_confirmation_bit };
		}))
    this.subscriptions.add(this.videoPageService.staticFiles$.subscribe((staticFiles: any[]) => this.staticFiles = staticFiles));
  }

  ngOnInit() {
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
    if(document.location.href.indexOf('/canvas/') >= 0){
      this.isNotCanvasPage = false;
    }
  }
  checkCsv(file: any){
    if (file.original_file_name.split('.')[1] == 'csv' && file.ready_for_download) {
      return true;
    } else  if (file.original_file_name.split('.')[1] != 'csv' && file.ready_for_download) {
      return true;
    }
    else return false
    
  }

  public FileClicked(from, file) {

    // if(file.original_file_name.split('.')[1] == 'csv' && file.ready_for_download){
    //   this.DownloadFile(file);
    //   return
    // }
    // else
     if(file.original_file_name.split('.')[1] == 'csv' && file.ready_for_download==0){
      return
    }

    if (from == "td") {
      if (file.stack_url && file.stack_url != null) {
        //  const path = environment.baseUrl + "/app/view_document" + file.stack_url.substring(file.stack_url.lastIndexOf("/"), file.stack_url.length);
         let path = "/home/document-commenting/pdf-renderer/workspace-video-page/" + file.id+ file.stack_url.substring(file.stack_url.lastIndexOf("/")) + "/" + file.file_type + "/" + file.account_folder_id + "/" + file.parent_folder_id + "/" + true
         window.open(path, "_blank");
         //home is added to the url to work on production. it will not work on localhost
        //this.currentSelectedFile = file;
      } else {
        this.DownloadFile(file);
      }
    } else {
      this.DownloadFile(file);
    }
  }

  private DownloadFile(file) {
    this.mainService.DownloadFile(file.id);
  }

  public InitiateDeleteResource(template: TemplateRef<any>, file) {
    this.DeletableFile = file;
    if(file.created_by !=  this.userCurrAcc.User.id && this.huddle_type == 2){
      this.showConfirmationDialog = true;
    } else {
      this.showConfirmationDialog = false;
    }
    this.modalRef = this.modalService.show(template, { class: "modal-md" });
  }
  public TriggerTextChange(ev) {

		if (ev.keyCode == 13) {
			this.ResolveDeleteFile(true)

		}
	}
  public uploadFiles(event: any) {
    if (event.from === 'resources') {
      event.files.forEach(file => {
        let obj: any = {
          huddle_id: this.huddle_id,
          video_id: this.video_id,
          time: 0,
          stack_url: file.url,
          video_url: file.key,
          video_file_name: file.filename,
          video_file_size: file.size,
          video_desc: "",
          url_stack_check: 1,
          account_id: this.userCurrAcc.accounts.account_id,
          user_id: this.userCurrAcc.User.id,
          current_user_role_id: this.userCurrAcc.roles.role_id,
          current_user_email: this.userCurrAcc.User.email,
          account_role_id: this.userCurrAcc.users_accounts.role_id
        };
          if (this.pageType == 'library-page') {
              obj.library = 1;
          }
        this.appMainService.UploadResource(obj).subscribe();
      });
    }
  }

  public ResolveDeleteFile(flag: boolean) {
    if (this.Inputs.ConfirmationKey != this.Inputs.Confirmation) {
			this.toastr.ShowToastr('info',this.translation.huddle_you_typed_in + " '" + this.Inputs.ConfirmationKey + "' ");
			return;
    } else {
      this.modalRef.hide();
      if (flag) {
        this.modalRef.hide();
        let obj:any = {
          huddle_id: this.huddle_id,
          video_id: this.video_id,
          document_id: this.DeletableFile.id,
          user_id: this.userCurrAcc.User.id,
        };
          if (this.pageType == 'library-page') {
              obj.library = 1;
          }
        this.mainService.DeleteResource(obj).subscribe();
      }
    }
  }
  
  public ResolveDeleteFileWithoutConfig(flag: boolean){
    this.modalRef.hide();
    if (flag) {
      this.modalRef.hide();
      const obj = {
        huddle_id: this.huddle_id,
        video_id: this.video_id,
        document_id: this.DeletableFile.id,
        user_id: this.userCurrAcc.User.id,
      }
     this.mainService.DeleteResource(obj).subscribe();
    }
  }
}
interface GeneralInputs {
	NewFolderName: string,
	Confirmation: string,
	ConfirmationKey: string
}
