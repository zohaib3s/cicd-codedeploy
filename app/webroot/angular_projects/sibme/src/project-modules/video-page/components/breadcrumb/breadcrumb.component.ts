import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { HeaderService, AppMainService } from "@app/services";
import { BreadCrumbInterface } from "@app/interfaces";
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'video-page-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnDestroy {

  public loaded: boolean = false;
  public checkAssessmentParam: boolean = false;
  public checkSampleData = false;
  public assessee: any;
  public translation: any = {};
  public breadCrumbs: BreadCrumbInterface[] = [];
  private subscriptions: Subscription = new Subscription();

  constructor(public headerService: HeaderService,
    private appMainService: AppMainService,
    private activatedRoute: ActivatedRoute) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => this.translation = languageTranslation));
    this.activatedRoute.queryParams.subscribe(params => {
      if (params.assessment) {
        this.checkAssessmentParam = true;
      }
      if (params.sampleData) {
        this.checkSampleData = true;
      }
    });
    this.subscriptions.add(this.appMainService.breadCrumb$.subscribe(((breadCrumbs: BreadCrumbInterface[]) => {
      console.log(breadCrumbs)
      localStorage.setItem('v_pagebreadcrums', JSON.stringify(breadCrumbs));
      this.breadCrumbs = breadCrumbs;
      if(this.breadCrumbs) {
        if (this.checkAssessmentParam && !this.checkSampleData) {
          this.assessee = JSON.parse(localStorage.getItem('assessee_detail'));
          if (this.assessee) {
            if (document.location.href.indexOf('/home') >= 0) {
              this.assessee.path = this.assessee?.path.replace('/home', '');
            }
            let arr = { title: this.assessee?.name, link: this.assessee?.path };
            breadCrumbs.splice(3, 0, arr);
          }


        }
        
        this.loaded = true;
      }  
    })));

  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}
