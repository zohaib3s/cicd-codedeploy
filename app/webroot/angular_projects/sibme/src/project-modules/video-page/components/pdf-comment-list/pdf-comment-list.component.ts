import {
    Component,
    EventEmitter,
    Input,
    Output,
    OnInit,
    OnDestroy,
    SimpleChanges,
    TemplateRef,
    ViewChild
} from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from "ngx-toastr";
import { Subscription } from 'rxjs';
import * as _ from "underscore";

import { HeaderService, AppMainService, SocketService } from "@app/services";
import { MainService, ScrollService, VideoPageService, PlayerService } from '@videoPage/services';
import { CommentOrAttachmentCountInterface, AddNewCommentInterface, CommentTypingSettingsInterface } from '@videoPage/interfaces';
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import { environment } from "@environments/environment";
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { cloneDeep } from 'lodash';
import { PdfCommentComponent } from '../pdf-comment/pdf-comment.component';
import { LessThanEqualValidator } from 'ng2-validation/dist/less-than-equal/directive';

type VideoCommentPlayState = 'on' | 'off';

@Component({
  selector: 'pdf-comment-list',
  templateUrl: './pdf-comment-list.component.html',
  styleUrls: ['./pdf-comment-list.component.css']
})
export class PdfCommentListComponent implements OnInit, OnDestroy {
  private pdfCommentComponent: PdfCommentComponent

  public Inputs: GeneralInputs;
  public showConfirmationDialog: boolean = false;



  @Input('comments') comments;
  @Input('ifNoSearchResult') ifNoSearchResult;
  @Input('allow_evidence_view') allow_evidence_view;
  @Input('permissions') permissions;
  @Input('VideoInfo') VideoInfo;
  @Input('documentInfo') documentInfo;
  @Input('customMarkers') customMarkers;
  @Input('params') params;
  @Input('colorClasses') colorClasses;
  @Input('CustomMarkers') CustomMarkers;
  @Input('original_account') public original_account: boolean;
  @Input('screenSharing') public screenSharing: boolean;
  @Input('publishAll') publishAll: any;
  @Input('is_published_feedback') is_published_feedback: any;
  @Input() from;

  @Output('commentAdded') commentAdded = new EventEmitter<any>();
  @Output('activatedComment') activatedComment = new EventEmitter<any>();
  @Output('commentEdited') commentEdited = new EventEmitter<any>();
  @Output('commentDeleted') commentDeleted = new EventEmitter<any>();
  @Output('hideLine') hideLine = new EventEmitter<any>();

  @Output('playAvailable') playAvailable = new EventEmitter<boolean>(); // videoCommentPlayAvailable
  @Output('autoCommentPlayRange') autoCommentPlayRange = new EventEmitter<boolean>(); // videoIsLessAutoCommentPlayRange
  @Output('commentPlayState') commentPlayState = new EventEmitter<VideoCommentPlayState>(); // videoCommentPlayState
  @Output('videoCmntPlayTimeSlots') videoCmntPlayTimeSlots = new EventEmitter<any[]>(); // videoCommentPlayTimeSlots
  
  /** Local storage variables starts */
  private VIDEO_PAGE_LS_KEYS = GLOBAL_CONSTANTS.LOCAL_STORAGE.VIDEO_PAGE;
  private taCommentsKey: string = '';
  private scriptCommentsKey: string = '';
  private taEditCommentsKey: string = '';
  private taEditRepliesKey: string = '';
  private taEditSubRepliesKey: string = '';
  /** Local storage variables ends */

  private settings: CommentTypingSettingsInterface;
  public staticFiles: any[];
  public totalStats: any = {};
  public currentComment: any = {};
  public enablePublish: boolean = false;
  public modalDeleteCheck: boolean = false;

  private videoPageType = GLOBAL_CONSTANTS.VIDEO_PAGE_TYPE;
  public COMMENT_STATE = GLOBAL_CONSTANTS.COMMENT_STATE;
  private header_data: any;
  public userCurrAcc: any;
  public translation: any = {};
  private subscriptions: Subscription = new Subscription();
  public modalRef: BsModalRef;
  public isCinemaModeOn = false;
  public marker_Search=false;

  public checkSampleData;

  /** Comment play variables start */
  private videoDuration: number = 0;
  private minMntsReqForVCP: number = 1; // Minimum minutes requirred to enable video comment play
  private videoCommentPlayTimeSlots: any[] = [];
  /** Comment play variables end */

  EditSubReplylocalArry: any = [];

  private commentsSortBy: number = 0;
  pageType: string = '';
  noCommentExist: any;
  noMatchCommentFound: any;
  public canvasHeight = "600px";
  public activeComment: any = {};
  currentPage: any;
  public editDocumentComment: any = null;
  userAccountLevelRoleId: any;
  artifactDataId: { id: any; page: any; xPos: any; yPos: any; };
  artifacts_id: any;
  sessionData: any;
  currentUser: any;
  deletableComment: any;
  confirmDeleteMessage: any;
  activeUsers: any;
  activeUsersInterval: NodeJS.Timeout;
  currEditableComment: any;
  @ViewChild('confirmDialog', { static: false }) confirmDialog;

  constructor(private headerService: HeaderService, private appMainService: AppMainService, public mainService: MainService,
    private toastr: ToastrService, private socketService: SocketService, private videoPageService: VideoPageService,
    private modalService: BsModalService, private playerService: PlayerService, private scrollService: ScrollService, private activatedRoute: ActivatedRoute) {

    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.confirmDeleteMessage = this.translation.vd_cnf_delete_comment
    }));
    if (document.location.href.indexOf('/canvas/') >= 0) {
      this.canvasHeight = "300px";
    }

    this.activatedRoute.parent.data.subscribe(data => {
      if (data) {
        this.noCommentExist = this.translation.no_comment_exist;

        this.noMatchCommentFound = this.translation.vd_no_comments_matched

        if (data.pageType === this.videoPageType.HUDDLE) {
          this.pageType = 'huddle-page';
        } else if (data.pageType === this.videoPageType.WORKSPACE) {
          this.pageType = 'workspace-page';
          this.noCommentExist = this.translation.no_notes_exist;
          this.noMatchCommentFound = this.translation.vd_no_comment_found;
          this.confirmDeleteMessage = this.translation.myfile_confirm_alert
        }
        else if (data.pageType === this.videoPageType.LIBRARY) {
          this.pageType = 'library-page';
          this.commentsSortBy = 2;

        }5
      }
    });
    this.subscriptions.add(this.videoPageService.videoDuration$.subscribe(videoDuration => this.checkForVideoCommentPlay(videoDuration)));
    // Only check for comment Autoscroll in case video has started playing i.e videoCurrentTime > 0
    this.subscriptions.add(this.videoPageService.videoCurrentTime$.subscribe(videoCurrentTime => {
      if (videoCurrentTime > 0) {
        if (this.settings.autoscroll) {  
         this.checkForCommentAutoscroll(videoCurrentTime);
        }
      }
    }));
    this.subscriptions.add(this.videoPageService.staticFiles$.subscribe((staticFiles: any[]) => this.staticFiles = staticFiles));
    this.subscriptions.add(this.videoPageService.commentTypingSettings$.subscribe(
      (commentTypingSettings: CommentTypingSettingsInterface) => this.settings = commentTypingSettings));

    this.subscriptions.add(this.videoPageService.totalStats$
      .subscribe((totalStats: CommentOrAttachmentCountInterface[]) => totalStats.forEach(item => this.totalStats[item.key] = item.value)));

    this.subscriptions.add(this.mainService.SearchData.subscribe((data) => this.filterComments(data)));

    this.subscriptions.add(this.videoPageService.newComment$.subscribe((data: AddNewCommentInterface) => {
      this.addNewComment(data)
    }));

    this.subscriptions.add(this.videoPageService.editCommentError$.subscribe((editComment: any) => this.updateEditCommentObject(editComment)));

    this.Inputs = { NewFolderName: "", Confirmation: "", ConfirmationKey: this.translation.Huddle_confirmation_bit };

    this.subscriptions.add(this.mainService.currentEditableComment$.subscribe((currentEditableComment: any) => this.currEditableComment = currentEditableComment ));
  }

  ngOnInit() {
    
    this.activatedRoute.queryParams.subscribe(params => {
      this.checkSampleData = params.sampleData;
    });
    this.sessionData = this.headerService.getStaticHeaderData();
    this.currentUser =  this.sessionData.user_current_account.User;
    this.currentUser.role_id =  this.sessionData.user_current_account.roles.role_id;
    this.header_data = this.headerService.getStaticHeaderData();
    this.userAccountLevelRoleId = this.header_data.user_permissions.roles.role_id;
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
    const userAndVideoId = `${this.headerService.getUserId()}-${this.VideoInfo?.id}`;
    this.taCommentsKey = `${this.VIDEO_PAGE_LS_KEYS.TA_COMMENTS}${userAndVideoId}`;
    this.scriptCommentsKey = `${this.VIDEO_PAGE_LS_KEYS.TA_COMMENTS}${userAndVideoId}`;
    this.taEditCommentsKey = `${this.VIDEO_PAGE_LS_KEYS.TA_EDIT_COMMENTS}${userAndVideoId}`;
    this.taEditRepliesKey = `${this.VIDEO_PAGE_LS_KEYS.TA_EDIT_REPLIES}${userAndVideoId}`;
    this.taEditSubRepliesKey = `${this.VIDEO_PAGE_LS_KEYS.TA_EDIT_SUB_REPLIES}${userAndVideoId}`;

    this.prepareComments(this.comments);
    this.LoadPublishSettings();
    this.restoreCommentsFromLocalStorage();
    this.subscribeSockets();
    this.checkCinemaMode();
    this.activeUsersInterval = setInterval(() => {
      this.getActiveUsers();
    }, 10000);
    this.videoPageService.eventEmitter.subscribe((e:any)=>{
      this.changeActiveComment(e.comment,e.event)
    })
    document.addEventListener('keyup', (event)=>{
      this.captureKey(event, this.confirmDialog);
    })

  }
  checkForSampleData() {
    if (this.checkSampleData || !this.publishAll) {
      return true;
    } else if (this.publishAll) {
      return false;
    }
  }
    
    parseDate(date){
        // parse the date to replace the offset as it is not parsed by safari
        if (date) {
            return date.replace(/\s/g, "T")
        } else return false;
        
    }
  // ngOnChanges(changes:SimpleChanges) {
  //  console.log(' inside comment-list -> ',changes);
  // }

  private restoreCommentsFromLocalStorage() {
    /** Restoring try-again-add-comments from localstorage start */
    const taComments = this.headerService.getLocalStorage(this.taCommentsKey);

    if (Array.isArray(taComments)) {
      taComments.forEach(comment => {
        this.comments.unshift(comment);
      });
    }
    /**Restoring try-again-add-comments from localstorage end */

    /**Restoring try-again-edit-comments from localstorage start */
    const taEditComments = this.headerService.getLocalStorage(this.taEditCommentsKey);
    if (Array.isArray(taEditComments)) {
      taEditComments.forEach(taEditComment => {
        this.updateEditCommentObject(taEditComment);
      });
    }
    /**Restoring try-again-edit-comments from localstorage end */
  }

  public editComment(comment) {
    this.headerService.hideTabRecordIcon(true);
    if (this.from == 'vobody') this.commentEdited.emit(comment);
    else this.videoPageService.changeEditCommentSource(comment);
  }

  onEdit(editData) {
    this.commentEdited.emit(editData);
  }

  public documentCommentEdited(editData){
    if(this.from == 'document') {
      this.editDocumentComment = editData
      this.artifactDataId = {
        id: editData.id,
        page: editData.page,
        xPos: editData.xPos,
        yPos: editData.yPos
      }
    }
  }

  public cancelDocumentComment(){
    this.editDocumentComment = null;
  }

  private updateEditCommentObject(commentObj: any): void {
    const commentIndex = this.comments.findIndex(comment => comment.id == commentObj.comment_id);
    if (commentIndex > -1) {
      this.comments[commentIndex].edittryagain = true;
      this.comments[commentIndex].huddle_id = commentObj.huddle_id;
      this.comments[commentIndex].videoId = commentObj.videoId;
      this.comments[commentIndex].comment_id = commentObj.comment_id;
      this.comments[commentIndex].for = commentObj.for;
      this.comments[commentIndex].time = commentObj.time;
      this.comments[commentIndex].synchro_time = commentObj.synchro_time;
      this.comments[commentIndex].end_time = commentObj.end_time;
      this.comments[commentIndex].ref_type = commentObj.ref_type;
      this.comments[commentIndex].comment = commentObj.comment;
      this.comments[commentIndex].user_id = commentObj.user_id;
      this.comments[commentIndex].account_id = commentObj.account_id;
      this.comments[commentIndex].standards_acc_tags = commentObj.standards_acc_tags;
      this.comments[commentIndex].default_tags = commentObj.default_tags;
      this.comments[commentIndex].default_tags_value = commentObj.default_tags_value;
      this.comments[commentIndex].assessment_value = commentObj.assessment_value;
      this.comments[commentIndex].account_role_id = commentObj.account_role_id;
      this.comments[commentIndex].current_user_email = commentObj.current_user_email;
      this.comments[commentIndex].files = commentObj.files;
    }
  }

  public toggleAutoscroll(event){
    this.ApplySettings(this.settings.sortBy);
  }

  public ApplySettings(settings) {
    if (typeof (settings.sortBy) == "number") {
      this.commentsSortBy = settings.sortBy;
      if (settings.sortBy == 1) {
        this.comments = _.sortBy(this.comments, (c) => { return new Date(c.created_date) });
      } else if (settings.sortBy == 0) {
        this.comments = _.sortBy(this.comments, (c) => { return new Date(c.created_date) });
        this.comments = this.comments.reverse();
      } else if (settings.sortBy == 2) {
        //fix from here
        let wholeComments = this.comments.filter((c) => { return c.time == 0 || c.time == null; });
        let timeComments = this.comments.filter((c) => { return c.time > 0 });
        this.comments = _.sortBy(timeComments, (c) => { return new Date(c.time) }).concat(wholeComments);
      }
    }
    if (typeof (settings.autoscroll) == "boolean") {
      this.settings.autoscroll = settings.autoscroll;
    }
  }

  public getValidCommentCount() {
    if (!this.comments || this.comments.length <= 0) return 1;
    return _.where(this.comments, { valid: true }).length;
  }

  getActiveUsers(){
    this.appMainService.getActiveUsers(this.documentInfo).subscribe((data: any) => {
      this.activeUsers = data;
      this.checkUserId();
      this.videoPageService.updateActiveUsers(this.activeUsers);
    });
  }

  public ResolvePublish(flag) {
    this.modalRef.hide();
    if (flag == 1) {
      const obj = {
        huddle_id: this.params.huddle_id,
        video_id: this.params.video_id,
        user_id: this.header_data.user_current_account.User.id,
        account_id: this.header_data.user_current_account.accounts.account_id,
        account_role_id: this.header_data.user_current_account.users_accounts.role_id,
        current_user_email: this.header_data.user_current_account.User.email
      };

      this.mainService.PublishFeedback(obj).subscribe();
    }
  }

  public Urlify(text) {  
     
    if (text) {
      let urlRegex = /((?:(http|https|Http|Https|rtsp|Rtsp):\/\/(?:(?:[a-zA-Z0-9\$\-\_\.\+\!\*\'\(\)\,\;\?\&\=]|(?:\%[a-fA-F0-9]{2})){1,64}(?:\:(?:[a-zA-Z0-9\$\-\_\.\+\!\*\'\(\)\,\;\?\&\=]|(?:\%[a-fA-F0-9]{2})){1,25})?\@)?)?((?:(?:[a-zA-Z0-9][a-zA-Z0-9\-]{0,64}\.)+(?:(?:aero|arpa|asia|a[cdefgilmnoqrstuwxz])|(?:biz|b[abdefghijmnorstvwyz])|(?:cat|com|gle|coop|c[acdfghiklmnoruvxyz])|d[ejkmoz]|(?:edu|e[cegrstu])|f[ijkmor]|(?:gov|g[abdefghilmnpqrstuwy])|h[kmnrtu]|(?:info|int|i[delmnoqrst])|(?:jobs|j[emop])|k[eghimnrwyz]|l[abcikrstuvy]|(?:mil|mobi|museum|m[acdghklmnopqrstuvwxyz])|(?:name|net|n[acefgilopruz])|(?:org|om)|(?:pro|p[aefghklmnrstwy])|qa|r[eouw]|s[abcdeghijklmnortuvyz]|(?:tel|travel|t[cdfghjklmnoprtvwz])|u[agkmsyz]|v[aceginu]|w[fs]|y[etu]|z[amw]))|(?:(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])))(?:\:\d{1,5})?)(\/(?:(?:[a-zA-Z0-9\;\/\?\:\@\&\=\#\~\-\.\+\!\*\'\(\)\,\_])|(?:\%[a-fA-F0-9]{2}))*)?(?:\b|$)/gi;
      return text.replace(urlRegex, function (url) {
        let add_protocole = false;
        if (!/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig.test(url)) {
          add_protocole = true;
        }
        return add_protocole ? '<a href="' + "//" + url + '" target="_blank">' + url + '</a>' : '<a href="' + url + '" target="_blank">' + url + '</a>';
      })
    } else {
      return '';
    }

  }

  public checkUserId(){
    // remove class "green" from all
    let removeClass = document.querySelectorAll(`.live_user:not(.user_${this.header_data.user_current_account.User.id})`);
    [].forEach.call(removeClass, function(el) {
        el.classList.remove("green");
    });

    //add class "green" on active users
    for (let data = 0; data < this.activeUsers.length; data++) {
      let elems = document.querySelectorAll(".user_"+this.activeUsers[data]);
      for (let i = 0; i < elems.length; i++) 
      {
        elems[i].classList.add('green');
      }
    }
  }

  public PublishFeedback(template) {
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
    this.videoPageService.updateEnablePublishBtn(false);  
  }

  private updateCommentCount() {
    let count = this.comments.length;
    if (count == 0) {
      this.videoPageService.updateTotalStats([{ key: 'comment_count', value: 0 }]);
    } else {
      this.comments.forEach(c => {
        if (c && c.Comment && c.Comment.responses.length > 0) {
          count += c.Comment.responses.length;
          c.Comment.responses.forEach((r) => {
            count += r.responses ? r.responses.length : 0;
          });
        }
      });
      this.videoPageService.updateTotalStats([{ key: 'comment_count', value: count }]);
    }
  }

  /** Socket functionality starts */
  private subscribeSockets() {
    
    
    if (this.params.huddle_id) {
      this.subscriptions.add(this.socketService.pushEventWithNewLogic(`huddle-details-${this.params.huddle_id}`)
        .subscribe(data => this.processEventSubscriptions(data)));
    }
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(`video-details-${this.params.video_id}`)
      .subscribe(data => this.processEventSubscriptions(data)));
    let updatedCommentsChannelName = `publish-channel-${this.params.huddle_id}`;;
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(updatedCommentsChannelName)
      .subscribe(data => this.processEventSubscriptions(data)));
    let workspaceChannelName = `workspace-${this.header_data.user_current_account.users_accounts.account_id}-${this.header_data.user_current_account.users_accounts.user_id}`;
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(workspaceChannelName)
      .subscribe(data => this.processEventSubscriptions(data)));
  }

  
  public getUserImgUrl(comment) {


    if (comment.image && comment.image.length > 0) {

      let img = comment.image;

      return "https://s3.amazonaws.com/sibme.com/static/users/" + comment.user_id + "/" + img;

    }
    return environment.baseUrl + "/img/home/photo-default.png";



  }

  private processEventSubscriptions(res) {
    console.log(res);

    switch (res.event) {
      case "comment_added":
        this.processCommentAdded(res.data, res.from_cake, res.reference_id);
        break;
      case "comment_edited":
        this.processCommentEdited(res.data, res.from_cake, res.reference_id, res);
        break;
      case "comment_deleted":
        this.processCommentDeleted(res.parent_comment_id, res.item_id, res.reference_id, res);
        break;
      case "resource_added":
        this.processResourceAdded(res.data, res.reference_id);
        break;
      case "attachment_deleted":
        this.processAttachmentDeleted(res.data, res.huddle_id);
        break;
      case "feedback_published":
        this.processFeedbackPublished(res.data);
        break;
      case "huddle_published":
        this.updatedComment(res.data);
        break;
    }
  }
  updatedComment(res) {

    if (res == this.params.huddle_id) {
      const commentParamsObj = {

        "user_id": this.header_data.user_current_account.User.id,
        "video_id": this.params.video_id,
        "account_id": this.header_data.user_current_account.accounts.account_id,
        "huddle_id": this.params.huddle_id,
        "role_id": this.header_data.user_current_account.roles.role_id,
        "permission_maintain_folders": this.header_data.user_permissions.UserAccount.permission_maintain_folders
      }
      this.comments = this.publishAll = this.is_published_feedback = null;
      this.mainService.GetVideo(commentParamsObj).subscribe((data: any) => {
        if (data.h_type == 3) {

          //this.huddleLevelPermission = data.user_huddle_level_permissions
          data.publish_all == "1" ? this.publishAll = true : false
          data.huddle_info.is_published_feedback == 1 ? this.is_published_feedback = true : false
        }
        this.comments = cloneDeep([...data.comments.Document.comments]);
        this.comments.map(comment => {
          comment.valid = true;
        });
        this.ApplySettings({ sortBy: 0 });
        this.prepareComments(this.comments);

        this.totalStats.comment_count = data.comments_counts.total_comments;
        this.videoPageService.updateTotalStats([{ key: 'comment_count', value: this.totalStats.comment_count }]);
        this.totalStats.resources_count = data.comments_counts.total_comments;
        this.videoPageService.updateTotalStats([{ key: 'resources_count', value: this.totalStats.resources_count }]);
      });
    }
  }
  private processCommentAdded(comment, from_cake, reference_id) {
    // console.log('inside comment process' , reference_id , this.params.video_id);
    
    // if (reference_id == this.params.video_id && ((this.publishAll && this.is_published_feedback && this.VideoInfo.h_type == 3) || this.checkSampleData || comment.created_by == this.userCurrAcc.User.id ||  this.VideoInfo.role_id ==200)) {
      // if(this.from == 'document') {
   
      // }
    if (reference_id == this.params.video_id) {
      let allowed = true;

      if(this.VideoInfo.h_type == 3) {
        if (this.publishAll && this.VideoInfo.role_id != 200) {
          if (!this.is_published_feedback) allowed = false;
        }
        if(this.VideoInfo.role_id == 210 && comment.created_by != this.userCurrAcc.User.id && comment.active == 0) {
          allowed = false;
        }
      }      
      if (allowed) {
        let parendCommentAdded: boolean = false;
        let commentObj;
        if (from_cake) commentObj = JSON.parse(comment);
        else commentObj = comment;

        if(commentObj.ref_type == 6) commentObj.playable = true;

        /* comment add section start */
        if (!commentObj.parent_comment_id) {
          
          commentObj.valid = true;
          this.LoadPublishSettings();
          this.totalStats.comment_count++;
          this.videoPageService.updateTotalStats([{ key: 'comment_count', value: this.totalStats.comment_count }]);
          this.mainService.CommentAddedNotification.emit(commentObj);

          let commentIndex = this.comments.findIndex(c => c.fake_id == commentObj.Comment.fake_id);
          if(commentIndex >= 0)
          {
              this.comments[commentIndex] = commentObj;
          }
          else
          {
              this.comments.push(commentObj);
          }
          // unique comments
          this.comments = this.comments.filter((comment, i, arr) => {
            return arr.indexOf(arr.find(t => t.id === comment.id)) === i;
          });
          parendCommentAdded = true;
          this.commentAdded.emit(comment);
          /* comment add section end */
        } else {

          /* reply add section start */
          let found = false;

          let commentIndex = this.comments.findIndex(c => c.id == commentObj.parent_comment_id);

          if (commentIndex > -1) {
            found = true;

            this.comments[commentIndex].replyEnabled = false;
            this.comments[commentIndex].replyAdding = true;
            this.comments[commentIndex].replyAdding = false;
            this.comments[commentIndex].replyText = "";
            this.comments[commentIndex].replies_count = commentObj.replies_count; //To update reply count if user add's a new reply

            let replyIndex = this.comments[commentIndex].Comment.responses.findIndex(r => r.uuid == commentObj.uuid);
            if (replyIndex > -1 && !from_cake) {
              this.comments[commentIndex].Comment.responses[replyIndex] = commentObj;
              delete this.comments[commentIndex].Comment.responses[replyIndex].fakeComment;
              delete this.comments[commentIndex].Comment.responses[replyIndex].tryAgain;
            } else {
              if (!this.comments[commentIndex].Comment) this.comments[commentIndex].Comment = {};
              if (!this.comments[commentIndex].Comment.responses) this.comments[commentIndex].Comment.responses = [];
              this.comments[commentIndex].Comment.responses.push(commentObj);
            }
            this.totalStats.comment_count++;
            this.videoPageService.updateTotalStats([{ key: 'comment_count', value: this.totalStats.comment_count }]);
            /* reply add section end */
          }

          /* sub-reply add section start */
          if (!found) {
            this.comments.forEach((c) => {

              if (c.Comment && c.Comment.responses && c.Comment.responses.length > 0) {
                let replyIndex = c.Comment.responses.findIndex(response => response.id == commentObj.parent_comment_id);
                if (replyIndex > -1) {
                  c.Comment.responses[replyIndex].replyText = "";

                  let subReplyIndex = c.Comment.responses[replyIndex].responses.findIndex(sr => sr.uuid == commentObj.uuid);
                  if (subReplyIndex > -1 && !from_cake) {
                    c.Comment.responses[replyIndex].responses[subReplyIndex] = commentObj;
                    delete c.Comment.responses[replyIndex].responses[subReplyIndex].fakeComment;
                    delete c.Comment.responses[replyIndex].responses[subReplyIndex].tryAgain;
                  } else {
                    if (!c.Comment.responses[replyIndex].responses) c.Comment.responses[replyIndex].responses = [];
                    c.Comment.responses[replyIndex].responses.push(commentObj);
                  }
                  this.totalStats.comment_count++;
                  this.videoPageService.updateTotalStats([{ key: 'comment_count', value: this.totalStats.comment_count }]);
                }
              }

            });

          }
          /* sub-reply add section end */
          // }
          let c = this.comments.slice();
          this.comments = [];
          this.comments = [...c];
          if (this.commentsSortBy != 0) this.ApplySettings({ sortBy: this.commentsSortBy });
          // this.commentAdded.emit({ parendCommentAdded });
          this.calculateVideoCommentPlayTimeSlots();
          this.checkForVideoCommentPlay();
        }

      }

      // let taComments = this.headerService.getLocalStorage(this.taCommentsKey);
      // if (Array.isArray(taComments)) {
      //   const commentIndex = taComments.findIndex(tsComment => tsComment.fake_id === comment.Comment.fake_id);
      //   if (commentIndex > -1) {
      //     taComments.splice(commentIndex, 1);
      //     this.headerService.setLocalStorage(this.taCommentsKey, taComments);
      //   }
    }
  }

  private processCommentEdited(comment, from_cake, reference_id, res) {
    if (reference_id == this.params.video_id) {
      let parendCommentEdited: boolean = false;
      let commentObj;
      if (from_cake) commentObj = JSON.parse(comment);
      else commentObj = comment;
      
      this.commentEdited.emit(comment);

      /* comment edit section start */
      if (!commentObj.Comment.parent_comment_id) {
        if(this.currEditableComment && this.currEditableComment.ref_type == 6 &&this.currEditableComment.id == commentObj.id){
          return;
        } else {
          let commentIndex = this.comments.findIndex(c => c.id == commentObj.id);
          if (commentIndex > -1) {
            commentObj.valid = true;
            this.comments[commentIndex] = commentObj;
            console.log(this.comments[commentIndex]);
            this.comments[commentIndex].replies_count = commentObj.replies_count;
          }
          this.AttachFilesToComment(commentObj);
          this.mainService.ReRenderMarkers.emit(true);
          parendCommentEdited = true;
        }
        /* comment edit section end */

      } else {

        /* reply edit section start */
        let found = false;
        let commentIndex = this.comments.findIndex(c => c.id == commentObj.Comment.parent_comment_id);

        // this.comments[commentIndex].replies_count = commentObj.replies_count;

        if (commentIndex > -1) {

          let replyIndex = this.comments[commentIndex].Comment.responses.findIndex(response => response.id == commentObj.id);

          if (replyIndex > -1) {
            found = true;
            this.comments[commentIndex].Comment.responses[replyIndex] = commentObj.Comment;
            this.comments[commentIndex].Comment.responses[replyIndex].EditEnabled = false;
            this.comments[commentIndex].Comment.responses[replyIndex].edittryagain = false;
            this.comments[commentIndex].Comment.responses[replyIndex].state = this.COMMENT_STATE.UPDATED;
            this.comments[commentIndex].Comment.responses[replyIndex].processing = false;
            this.comments[commentIndex].Comment.responses[replyIndex].postingEdit = false;
            this.mainService.ReRenderMarkers.emit(true);
          }

        }
        /* reply edit section end */

        /* sub-reply edit section start */
        if (!found) {

          this.comments.forEach((c) => {

            if (c.Comment.responses) {

              c.Comment.responses.forEach((r) => {

                if (r.responses) {
                  let subReplyIndex = r.responses.findIndex(response => response.id == commentObj.id);
                  if (subReplyIndex > -1) {
                    r.responses[subReplyIndex].comment = commentObj.Comment.comment;
                    r.responses[subReplyIndex].EditEnabled = false;
                    r.responses[subReplyIndex].edittryagain = false;
                    r.responses[subReplyIndex].state = this.COMMENT_STATE.UPDATED;
                    r.responses[subReplyIndex].processing = false;
                  }
                }

              });

            }

          });

        }
        /* sub-reply edit section end */
      }
      // this.commentAdded.emit({ parendCommentEdited });
      this.checkForVideoCommentPlay();
    }
  }

  //Deleting comment on document
  public TriggerTextChange(ev) {
		if (ev.keyCode == 13) {
			this.ResolveDelete(1)
		}
	}

  public ResolveDelete(flag) {
    if (this.Inputs.ConfirmationKey != this.Inputs.Confirmation) {
			this.toastr.info(this.translation.huddle_you_typed_in + " '" + this.Inputs.ConfirmationKey + "' ");
			return;
    } else {
      this.modalRef.hide();
      this.Inputs.Confirmation=null;
      this.modalDeleteCheck = false;
      if (flag == 1) {
        this.onCommentDelete(this.deletableComment);
        this.activeComment = {};
      }
    }
  }

  public ResolveDeleteWithoutConfig(flag) {
    this.modalRef.hide();
    if (flag == 1) {
      this.modalDeleteCheck = false;
      this.onCommentDelete(this.deletableComment);
      this.activeComment = {};
    }
  }

  private processCommentDeleted(parent_comment_id, item_id, reference_id, data) {
    // if(this.from = 'document') this.commentDeleted.emit(item_id)

    if (reference_id == this.params.video_id) {

      this.LoadPublishSettings();

      /** Comment delete section start */
      if (!parent_comment_id) {
        let commentIndex = this.comments.findIndex(c => c.id == item_id);
        if (commentIndex > -1)
        {
            let comment = this.comments[commentIndex];
            this.comments.splice(commentIndex, 1);
            this.commentDeleted.emit(comment)
        }
        
        /** Comment delete section end */

      } else {

        /** Reply delete section start */
        let found = false;
        let commentIndex = this.comments.findIndex(c => c.id == parent_comment_id);

        
        this.comments[commentIndex].replies_count = data.replies_count;
        

        if (commentIndex > -1 && Array.isArray(this.comments[commentIndex].Comment.responses)) {

          let replyIndex = this.comments[commentIndex].Comment.responses.findIndex(response => response.id == item_id);
          if (replyIndex > -1) {
            found = true;
            this.comments[commentIndex].Comment.responses.splice(replyIndex, 1);
          }

        }
        /** Reply delete section end */

        /** Sub-reply delete section start */
        // if (!found) {

        //   this.comments.forEach((c) => {

        //     if (Array.isArray(c.Comment.responses)) {

        //       c.Comment.responses.forEach((r) => {

        //         if (Array.isArray(r.responses)) {
        //           let subReplyIndex = r.responses.findIndex(response => response.id == item_id);
        //           if (subReplyIndex > -1) {
        //             r.responses.splice(subReplyIndex, 1);
        //           }
        //         }

        //       });

        //     }

        //   });

        // }
        /** Sub-reply delete section end */

      }

      this.updateCommentCount();
      this.mainService.ReRenderMarkers.emit(true);
      this.checkForVideoCommentPlay();
      if (this.comments.length < 1) {
        this.toggleVideoCommentPlayState('off');
      }

    }

  }

  private processResourceAdded(resource, reference_id) {
    if (reference_id == this.params.video_id) {
      resource.id = Number(resource.doc_id);
      resource.time = (resource.scripted_current_duration) ? this.ConvertTime(resource.scripted_current_duration) : this.translation.all_video;

      if (!resource.comment_id) {
        if (!resource.total_attachment && isNaN(resource.total_attachment)) {
          this.staticFiles.push(resource);
          this.toastr.info(this.translation.resource_uploaded);
        }
      } else {
        let commentExisted = this.comments.find(comment => comment.id == resource.comment_id);
        if (commentExisted) {
          this.staticFiles.push(resource);
          this.AttachFilesToCommentsOnResourceAddedSocket(this.staticFiles);
        }

      }
      this.videoPageService.updateStaticFiles(this.staticFiles);
    }

  }

  private processAttachmentDeleted(deletedResourceId, huddle_id) {
    if (huddle_id == this.params.huddle_id) {
      let index = this.staticFiles.findIndex(file => file.id == deletedResourceId);
      if (index >= 0) {
        this.toastr.info(this.translation.resource_deleted);
        this.staticFiles.splice(index, 1);
        this.videoPageService.updateStaticFiles(this.staticFiles);
        this.AttachFilesToCommentsOnResourceAddedSocket(this.staticFiles);
      }
    }
  }

  private AttachFilesToCommentsOnResourceAddedSocket(staticFiles: any[]) {

    this.comments.map(comment => {
      comment.files = [];
      comment.files = staticFiles.filter(file => file.comment_id == comment.id);
      comment.files.map((file) => {
        file.time = (file.scripted_current_duration) ? this.ConvertTime(file.scripted_current_duration) : this.translation.all_video;
      });
    });
  }

  private processFeedbackPublished(videoId) {

    if (this.params.video_id == videoId) {
      this.enablePublish = false;
      this.toastr.info(this.translation.feedback_published);
      let account_id = this.header_data.user_current_account.accounts.account_id;
      let user_id = this.header_data.user_current_account.User.id;

      const paramsObj = { video_id: this.params.video_id, huddle_id: this.params.huddle_id, user_id: user_id };
      this.mainService.GetVideoResources(paramsObj).subscribe((data: any) => {

        if (data) {
          data.map((d) => {
            if (d.scripted_current_duration) d.time = this.ConvertTime(d.scripted_current_duration);
            else d.time = this.translation.vd_all_videos;
          });
          this.videoPageService.updateStaticFiles(data);
        }

        const commentParamsObj = {
          "user_id": user_id,
          "video_id": this.params.video_id,
          "account_id": account_id,
          "huddle_id": this.params.huddle_id,
          "role_id": this.header_data.user_current_account.roles.role_id,
          "permission_maintain_folders": this.header_data.user_permissions.UserAccount.permission_maintain_folders
        }
        this.mainService.GetVideo(commentParamsObj).subscribe((data: any) => {

          this.comments = [...data.comments.Document.comments];
          this.comments.map(comment => {
            comment.valid = true;
          });
          this.ApplySettings({ sortBy: 0 });
          this.prepareComments(this.comments);

          this.totalStats.comment_count = data.comments_counts.total_comments;
          this.videoPageService.updateTotalStats([{ key: 'comment_count', value: this.totalStats.comment_count }]);
          this.totalStats.resources_count = data.comments_counts.total_comments;
          this.videoPageService.updateTotalStats([{ key: 'resources_count', value: this.totalStats.resources_count }]);
        });

      });
    }
  }
  /** Socket functionality end */

  public onCommentDelete(comment) {
    const obj = {
      video_id: this.params.video_id,
      comment_id: comment.id,
      user_id: this.header_data.user_current_account.User.id
    }

    this.mainService.ReRenderMarkers.emit(comment);
    if (this.pageType == 'library-page') {
      obj['library'] = 1;
    }
    this.appMainService.DeleteComment(obj).subscribe((data: any) => {
        //commented following code because comment should be deleted by socket
//      if(data.status == 'success') this.commentDeleted.emit(comment)
    });
  }

  private filterComments(criteria) {
    if (!this.comments) return;

    this.prepareComments(this.comments);
    if (criteria.tag_id && criteria.tag_id != -1) {
      this.marker_Search=true;
      this.comments.forEach((c) => {
        c.valid = false;
        c.default_tags.forEach((dt) => {
          if (dt.account_tag_id == criteria.tag_id) c.valid = true;
        });
      });
    }
    else{
      this.marker_Search=false;
    }
    if (criteria.text) {
      this.comments.forEach((c) => {
         let isValidTxt = c.comment && c.comment.toLowerCase().indexOf(criteria.text.toLowerCase()) > -1;
         if(criteria.tag_id) {
          c.default_tags.forEach((dt) => {
            if (dt.account_tag_id == criteria.tag_id && isValidTxt) c.valid = true; else c.valid = false;
          });
         } else {
          c.valid = isValidTxt;
         }
        if (!c.valid) {
          if (c.Comment && c.Comment.responses) {
            c.Comment.responses.forEach((reply) => {
              if (reply.comment && reply.comment.toLowerCase().indexOf(criteria.text.toLowerCase()) > -1) c.valid = true;
              if (reply.responses) {
                reply.responses.forEach((sub_reply) => {
                  if (sub_reply.comment && sub_reply.comment.toLowerCase().indexOf(criteria.text.toLowerCase()) > -1) c.valid = true;
                });
              }
            });
          }
        }
      });
    }
  }

  public AttachFilesToComment(comment) {
    if (((typeof (comment.time) == "number" && comment.time == 0) || comment.time == "0") && !comment.is_new_comment) {
      comment.files = [];
      return;
    }

    comment.files = [];
    if (comment.is_new_comment) {
      comment.files = _.where(this.staticFiles, { comment_id: comment.id });
      return;
    }

    this.staticFiles.forEach((file) => {
      if (!file) return;
      file.time2 = file.time == this.translation.vd_all_videos.trim() ? 0 : this.FormatToSeconds(file.time);
      comment.time2 = this.FormatToSeconds(comment.time);
      if (comment.time2 == file.time2) {
        comment.files.push(file);
      }
    });
  }

  private FormatToSeconds(time) {
    if (time == 0) return 0;
    if (typeof (time) == "number") return time;
    let stamp = time.split(":");

    return Number(stamp[0] * 3600) + Number(stamp[1] * 60) + Number(stamp[2]);
  }

  private prepareComments(comments) {
    comments.forEach((c) => {
      c.valid = true;
      c.ReplyTouched = false;

      if (c.time == null) c.time = 0;
      if (c.default_tags.length > 0) {

        c.default_tags.forEach((dt) => {
          let index = _.findIndex(this.CustomMarkers, { account_tag_id: dt.account_tag_id });
          if (index >= 0) {
            if (!c.customMarkers) c.customMarkers = [];
            c.customMarkers.push(dt);
          } else {
            if (!c.defaultTags) c.defaultTags = [];
            c.defaultTags.push(dt);
          }
        });
      }
    })
  }

  private LoadPublishSettings() {
    const obj = {
      huddle_id: this.params.huddle_id,
      user_id: this.header_data.user_current_account.User.id,
      video_id: this.params.video_id

    };
    this.appMainService.GetPublishSettings(obj).subscribe((data: any) => {
      this.videoPageService.updateEnablePublishBtn(data.show_publish_button);
      this.enablePublish = data.show_publish_button;

    });

  }

  private confirmExit(e) {
    if (this.enablePublish) {
      let event = e || window.event;
      const message = "Your confirmation message goes here.";
      if (event) event.returnValue = message; // For IE and Firefox
      return message; // For Safari
    }
  }

  private ConvertTime(n) {
    if (!n || n == null || n == 0) return this.translation.vd_all_videos;
    let sec_num: any = parseInt(n, 10);
    let hours: any = Math.floor(sec_num / 3600);
    let minutes: any = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds: any = sec_num - (hours * 3600) - (minutes * 60);

    if (hours == 0 && minutes == 0 && seconds == 0) {
      return this.translation.vd_all_videos;
    }

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return hours + ':' + minutes + ':' + seconds;
  }

  /** Comment play functionality start */
  private checkForVideoCommentPlay(videoDuration?: number) {
    if (videoDuration) this.videoDuration = videoDuration;

    if ((Math.floor(this.videoDuration) / 60) > this.minMntsReqForVCP) {

      let timeCommentsExists = this.comments.find(comment => comment.time);
      if (timeCommentsExists) {
        this.calculateVideoCommentPlayTimeSlots();
        this.playAvailable.emit(true);
      } else {
        this.playAvailable.emit(false);
      }

      this.autoCommentPlayRange.emit(false);
    } else {
      this.autoCommentPlayRange.emit(true);
    }

  }
  checkPermission() {
    if (this.VideoInfo.h_type != 3) {
      return true;
    } else {
      if (this.permissions.huddle_permission == 210) {
        if (this.publishAll) {
          if (this.is_published_feedback) {
            return true;
          } else {
            return false;
          }
        } else {
          return true;
        }
      }
      else {
        return true
      }
    }
  }

  /** Calculates video comment play time slots
   *  Description: Steps =>
   *  1. Calculate time range by looping over comments by handling the overlap behavior of comment's time and end time
   *  2. If bydefault ordering of commets some time slots have been missed and overlapped their time with one another then do a force filtering
   *     to remove overlap time ranges.
   *  3. Sort the time range slots so that video will always play from leas start time
   */

  public changeStatus(type, commentObj) {
    let obj = {
      huddle_id: this.params.huddle_id,
      comment_id: commentObj.id,
      videoId: this.params.video_id,
      comment: commentObj.comment,
      comment_status: type,
      user_id: this.userCurrAcc.User.id,
      account_id: this.userCurrAcc.accounts.account_id
    };

    this.appMainService.EditCommentStatus(obj).subscribe((data: any) => {});
  }

  public changeCustomMarker(type, commentObj){
    let obj = {
      huddle_id: this.params.huddle_id,
      comment_id: commentObj.id,
      videoId: this.params.video_id,
      comment: commentObj.comment,
      assessment_value: (type) ? "# " + type : "",
      user_id: this.userCurrAcc.User.id,
      account_id: this.userCurrAcc.accounts.account_id
    };

    this.appMainService.EditCustomMarket(obj).subscribe((data: any) => {});
  }

  public getTimelineBg(comment) {

    if (Array.isArray(comment.default_tags)) {

      let ret = "grey";

      comment.default_tags.forEach((dt) => {

        if (dt) {
          let index = _.findIndex(this.customMarkers, { account_tag_id: dt.account_tag_id });

          if (index > -1) {
            ret = this.colorClasses[index];
          }
        }


      });

      return ret;

    } else {
      return "grey";
    }

  }

  public checkTheMarker(marker, comment) {
    if (Array.isArray(comment.default_tags)) {
      let ret = false;
      
      comment.default_tags.forEach((dt) => {
        if (dt) {
          if(marker.account_tag_id == dt.account_tag_id) {
            ret = true;
          }
        }
      });

      return ret;
    } else {
      return false;
    }
  }

  private calculateVideoCommentPlayTimeSlots() {
    this.videoCommentPlayTimeSlots = [];
    this.comments.forEach(comment => {
      if (comment.time && comment.time != 0) {

        const commentTime = Math.max(0, (comment.time - 10));
        const commentEndTime = (comment.end_time && comment.end_time != 0) ? (comment.end_time + 10) : (comment.time + 10);

        let timeRangeExists = this.videoCommentPlayTimeSlots.find(item => commentTime >= item.start && commentTime <= item.end);
        if (timeRangeExists) {
          if (timeRangeExists.end < commentEndTime) timeRangeExists.end = commentEndTime;
        } else {
          this.videoCommentPlayTimeSlots.push({ start: commentTime, end: commentEndTime });
        }
      }
    });
    this.videoCommentPlayTimeSlots = this.videoCommentPlayTimeSlots.filter(timeRange => {
      let timeRangeExists = this.videoCommentPlayTimeSlots.find(item => timeRange.end >= item.start && timeRange.end < item.end);
      if (timeRangeExists) {
        if (timeRange.start < timeRangeExists.start) timeRangeExists.start = timeRange.start;
      } else {
        return timeRange;
      }
    });

    this.videoCommentPlayTimeSlots.sort((firstIndex, secondIndex) => {
      return firstIndex.start - secondIndex.start;
    });
    let temp = this.videoCommentPlayTimeSlots;
    let that = this;
    for (let index = 0; index < this.videoCommentPlayTimeSlots.length; index++) {
      let slot = this.videoCommentPlayTimeSlots[index];
      for (let inner_index = 0; inner_index < temp.length; inner_index++) {
        let current = temp[inner_index];
        if (index !== inner_index) {
          if (slot.start >= current.start && slot.end <= current.end) {
            that.videoCommentPlayTimeSlots.splice(index, 1);
            break;
          }
        }
      }
    }
    this.videoCmntPlayTimeSlots.emit(this.videoCommentPlayTimeSlots);
  }

  public toggleVideoCommentPlayState(state: VideoCommentPlayState, videoCommentPlayAvailable: boolean = true) {
    if (videoCommentPlayAvailable) {
      this.commentPlayState.emit(state);
      this.playerService.toggleVideoCommentPlay({ state: state, timeSlots: this.videoCommentPlayTimeSlots });
    }
  }
  /** Comment play functionality end */

  public onReplyEdit(commentObj) {
    
    let checkAudioText =  commentObj.reply.EditableText.includes('https://d1ebp8oyqi4ffr.cloudfront.net');
    if(commentObj.reply.ref_type == 6 && checkAudioText) {
      let text = commentObj.reply.EditableText.replace('https://d1ebp8oyqi4ffr.cloudfront.net/', '');
      commentObj.reply.EditableText = text.split('?')[0]
    }

    let obj = {
      huddle_id: this.params.huddle_id,
      comment_id: commentObj.reply.id,
      videoId: this.params.video_id,
      for: "",
      synchro_time: '',
      time: 0,
      ref_type: commentObj.reply.ref_type,
      comment: commentObj.reply.EditableText,
      standards_acc_tags: "",
      default_tags: "",
      assessment_value: "",
      user_id: this.userCurrAcc.User.id,
      account_id: this.userCurrAcc.accounts.account_id,
      account_role_id: this.userCurrAcc.users_accounts.role_id,
      current_user_email: this.userCurrAcc.User.email,
      created_at_gmt: moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS"),
      platform: 'document-commenting'
    };
    commentObj.reply.edittryagain = false;
    if (this.pageType == 'library-page') {
      obj['library'] = 1;
    }

    if(commentObj.reply.ref_type == '6')  obj['audio_duration'] = commentObj.reply.audioDuration;

    this.appMainService.EditComment(obj).subscribe((data: any) => {

      if (data.status == "success") {
        let parentIndex = _.findIndex(this.comments, { id: commentObj.comment.id });
        let found = false;
        if (parentIndex > -1) {

          let index = _.findIndex(this.comments[parentIndex].Comment.responses, { id: commentObj.reply.id });

          if (index > -1) {
            found = true;
            this.comments[parentIndex].Comment.responses[index].EditEnabled = false;
            commentObj.reply.edittryagain = true;
            if (this.comments[parentIndex].Comment.responses[index].edittryagain) {
              this.comments[parentIndex].Comment.responses[index].edittryagain = false;
              this.comments[parentIndex].Comment.responses[index].state = this.COMMENT_STATE.UPDATED;


              const taEditReplies = this.headerService.getLocalStorage(this.taEditRepliesKey);
              if (Array.isArray(taEditReplies) && taEditReplies.length > 0) {
                const taEditReplyIndex = taEditReplies.findIndex(taEditReply => taEditReply.id == this.comments[parentIndex].Comment.responses[index].id);
                if (taEditReplyIndex > -1) {
                  taEditReplies.splice(taEditReplyIndex, 1);
                  this.headerService.setLocalStorage2(this.taEditRepliesKey, taEditReplies);
                }
              }

            }
            this.mainService.ReRenderMarkers.emit(true);
          }

        }

        if (!found) {

          this.comments.forEach((c) => {

            if (c.Comment.responses) {

              c.Comment.responses.forEach((r) => {

                let index = -1;
                if (r.responses)
                  index = _.findIndex(r.responses, { id: commentObj.reply.id });

                if (index > -1) {

                  r.responses[index].comment = commentObj.reply.EditableText;
                  r.responses[index].EditEnabled = false;
                  commentObj.reply.edittryagain = true;
                  if (r.responses[index].edittryagain) {
                    r.responses[index].edittryagain = false;
                    r.responses[index].state = this.COMMENT_STATE.UPDATED;
                    const taEditSubReplies = this.headerService.getLocalStorage(this.taEditSubRepliesKey);
                    if (Array.isArray(taEditSubReplies) && taEditSubReplies.length > 0) {
                      const taEditSubReplyIndex = taEditSubReplies.findIndex(taEditSubReply => taEditSubReply.id == r.responses[index].id);
                      if (taEditSubReplyIndex > -1) {
                        taEditSubReplies.splice(taEditSubReplyIndex, 1);
                        this.headerService.setLocalStorage2(this.taEditSubRepliesKey, taEditSubReplies);
                      }
                    }

                  }
                }

              });

            }

          });

        }

      }


    }, err => {
      let parentIndex = _.findIndex(this.comments, { id: commentObj.comment.id });
      let index = _.findIndex(this.comments[parentIndex].Comment.responses, { id: commentObj.reply.id });
      if (!this.comments[parentIndex].Comment) this.comments[parentIndex]['Comment'] = {};
      if (!this.comments[parentIndex].Comment.responses) this.comments[parentIndex].Comment['responses'] = [];
      if (this.comments[parentIndex].Comment && this.comments[parentIndex].Comment.responses) {
        let parentSubindex = _.findIndex(this.comments[parentIndex].Comment.responses, { id: commentObj.reply.parent_comment_id });
        if (this.comments[parentIndex].Comment.responses[parentSubindex] && this.comments[parentIndex].Comment.responses[parentSubindex].responses) {
          let subreply_index = _.findIndex(this.comments[parentIndex].Comment.responses[parentSubindex].responses, { id: commentObj.reply.id });
          if (subreply_index > -1) {
            setTimeout(() => {
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].processing = false;
              delete this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].state;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].comment = obj.comment;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].EditEnabled = false;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].huddle_id = obj.huddle_id;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].account_id = obj.account_id;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].edittryagain = true;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].videoId = obj.videoId;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].for = obj.for;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].synchro_time = obj.synchro_time;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].time = obj.time;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].ref_type = obj.ref_type;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].user_id = obj.user_id;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].standards_acc_tags = obj.standards_acc_tags;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].default_tags = obj.default_tags;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].assessment_value = obj.assessment_value;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].account_role_id = obj.account_role_id;
              this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].current_user_email = obj.current_user_email;

              const taEditSubReplies = this.headerService.getLocalStorage(this.taEditSubRepliesKey) || [];
              const taEditSubReplyIndex = taEditSubReplies.findIndex(taEditSubReply => taEditSubReply.id == this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index].id);
              if (taEditSubReplyIndex > -1) taEditSubReplies[taEditSubReplyIndex] = this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index];
              else taEditSubReplies.push(this.comments[parentIndex].Comment.responses[parentSubindex].responses[subreply_index]);
              this.headerService.setLocalStorage2(this.taEditSubRepliesKey, taEditSubReplies);

            }, 500);
          }
        }
      }
      if (index > -1) {
        setTimeout(() => {
          this.comments[parentIndex].Comment.responses[index].processing = false;
          delete this.comments[parentIndex].Comment.responses[index].state;
          this.comments[parentIndex].Comment.responses[index].comment = obj.comment;
          this.comments[parentIndex].Comment.responses[index].EditEnabled = false;
          this.comments[parentIndex].Comment.responses[index].huddle_id = obj.huddle_id;
          this.comments[parentIndex].Comment.responses[index].account_id = obj.account_id;
          this.comments[parentIndex].Comment.responses[index].edittryagain = true;
          this.comments[parentIndex].Comment.responses[index].videoId = obj.videoId;
          this.comments[parentIndex].Comment.responses[index].for = obj.for;
          this.comments[parentIndex].Comment.responses[index].synchro_time = obj.synchro_time;
          this.comments[parentIndex].Comment.responses[index].time = obj.time;
          this.comments[parentIndex].Comment.responses[index].ref_type = obj.ref_type;
          this.comments[parentIndex].Comment.responses[index].user_id = obj.user_id;
          this.comments[parentIndex].Comment.responses[index].standards_acc_tags = obj.standards_acc_tags;
          this.comments[parentIndex].Comment.responses[index].default_tags = obj.default_tags;
          this.comments[parentIndex].Comment.responses[index].assessment_value = obj.assessment_value;
          this.comments[parentIndex].Comment.responses[index].account_role_id = obj.account_role_id;
          this.comments[parentIndex].Comment.responses[index].current_user_email = obj.current_user_email;

          const taEditReplies = this.headerService.getLocalStorage(this.taEditRepliesKey) || [];
          const taEditReplyIndex = taEditReplies.findIndex(taEditReply => taEditReply.id == this.comments[parentIndex].Comment.responses[index].id);
          if (taEditReplyIndex > -1) taEditReplies[taEditReplyIndex] = this.comments[parentIndex].Comment.responses[index];
          else taEditReplies.push(this.comments[parentIndex].Comment.responses[index]);
          this.headerService.setLocalStorage2(this.taEditRepliesKey, taEditReplies);
        }, 500);
      }
    });
  }

  /** Add/Edit comment functionality starts */
  private addNewComment(data: AddNewCommentInterface) {
    if (data.tryAgain) {
      let comment = this.comments.find(comment => comment.fake_id === data.comment.fake_id);
      if (comment) comment = data.comment;
    } else {

      this.comments.unshift(data.comment);
      if (this.commentsSortBy != 0) this.ApplySettings({ sortBy: this.commentsSortBy });
        setTimeout(() => {
            this.mainService.updateEditComment(false);
        }, 200);
    }
     
    //this.checkForCommentAutoscroll(data.comment.time)
  }

  /** Retry adding comment from local storage
   * On-success => Upload attached comment files (if any)
   * On-Error => Display button for try again
   */
  public retryAddComment(comment: any) {
    const commentExisted = this.comments.find(cComment => cComment.fake_id === comment.fake_id);
    this.appMainService.AddComment(comment).subscribe((data: any) => {
      if (data.status == "success") {

        let files = [];
        if (comment.files && comment.files.length > 0) {
          files = comment.files.filter(file => !file.id);
          if (files.length > 0) this.uploadCommentResources(comment.files, comment.time, data[0].id);
        }
        let taComments = this.headerService.getLocalStorage(this.taCommentsKey);
        if (Array.isArray(taComments)) {
          const commentIndex = taComments.findIndex(tsComment => tsComment.fake_id === comment.fake_id);

          if (commentIndex > -1) {
            taComments.splice(commentIndex, 1);
            this.headerService.setLocalStorage(this.taCommentsKey, taComments);
          }
        }
        if (data.code == 409) {
          const taComments = this.headerService.getLocalStorage(this.taCommentsKey);

          if (Array.isArray(taComments)) {
            const commentIndex = taComments.findIndex(tsComment => tsComment.created_at_gmt === data[0].created_at_gmt);
            if (commentIndex > -1) {
              taComments.splice(commentIndex, 1);
              this.headerService.setLocalStorage(this.taCommentsKey, taComments);
            }
          }
        }
        let getValues = this.comments.filter(x => x.created_at_gmt == data[0].created_at_gmt);
        if (getValues && getValues.length > 1) {
          let indexToDelete = this.comments.findIndex(x => x.created_at_gmt == data[0].created_at_gmt && x.id == -1);

          if (indexToDelete >= 0) {
            this.comments.splice(indexToDelete, 1);
            this.comments = [...this.comments];

          }
        } else {
          let checkIfAdded = this.comments.findIndex(x => x.created_at_gmt == data[0].created_at_gmt);
          this.comments[checkIfAdded].id = data[0].id;
          this.comments[checkIfAdded].default_tags = data[0].default_tags
          this.comments[checkIfAdded].standard = data[0].standard

        }

        // let checkIfAdded = this.comments.findIndex(x => x.created_at_gmt == data[0].created_at_gmt);
        // if(checkIfAdded<0){
        //   this.comments.push(data[0])
        // }

      } else {
        commentExisted.processing = false;
        commentExisted.tryagain = true;
      }
    }, () => {
      commentExisted.processing = false;
      commentExisted.tryagain = true;
    });

  }

  /** Retry editing comment from local storage
   * On-success => Upload attached comment files (if any)
   * On-Error => Display button for try again
   */
  public retryEditComment(comment: any) {

    let taEditComments = this.headerService.getLocalStorage(this.taEditCommentsKey);
    if (Array.isArray(taEditComments)) {
      const commentIndex = taEditComments.findIndex(tsComment => tsComment.fake_id === comment.fake_id);
      if (commentIndex > -1) {
        comment.created_at_gmt = taEditComments[commentIndex].created_at_gmt
      }

    }
    const commentExisted = this.comments.find(comment => comment.id === comment.comment_id);
    this.appMainService.EditComment(comment).subscribe((data: any) => {
      if (data.status == "success") {

        let files = [];
        if (comment.files && comment.files.length > 0) {
          files = comment.files.filter(file => !file.id);
          if (files.length > 0) this.uploadCommentResources(files, comment.time, comment.comment_id);
        }

        let taEditComments = this.headerService.getLocalStorage(this.taEditCommentsKey);
        if (Array.isArray(taEditComments)) {
          const commentIndex = taEditComments.findIndex(tsComment => tsComment.fake_id === comment.fake_id);
          if (commentIndex > -1) {
            taEditComments.splice(commentIndex, 1);
            this.headerService.setLocalStorage(this.taEditCommentsKey, taEditComments);
          }
        }
        if (data.code == 409) {
          const taComments = this.headerService.getLocalStorage(this.taEditCommentsKey);
          if (Array.isArray(taComments)) {
            const commentIndex = taComments.findIndex(tsComment => tsComment.created_at_gmt === data.updated_comment.created_at_gmt);
            if (commentIndex > -1) {
              taComments.splice(commentIndex, 1);
              this.headerService.setLocalStorage(this.taCommentsKey, taComments);
            }
          }
        }
        let checkIfAdded = this.comments.findIndex(x => x.created_at_gmt == data.updated_comment.created_at_gmt);
        if (checkIfAdded > -1) {
          this.comments[checkIfAdded].EditEnabled = false;
          this.comments[checkIfAdded].edittryagain = false;
          this.comments[checkIfAdded].state = this.COMMENT_STATE.UPDATED;
          this.comments[checkIfAdded].default_tags = data.updated_comment.default_tags
          this.comments[checkIfAdded].standard = data.updated_comment.standard



        }

      } else {
        setTimeout(() => {
          commentExisted.processing = false;
          commentExisted.edittryagain = true;
          commentExisted.state = this.COMMENT_STATE.UPDATING_ERROR;
        }, 500);
      }
    }, () => {
      setTimeout(() => {
        commentExisted.processing = false;
        commentExisted.edittryagain = true;
        commentExisted.state = this.COMMENT_STATE.UPDATING_ERROR;
      }, 500);
    });

  }

  private uploadCommentResources(files: any[], time: number, commentId: number) {
    files.forEach(file => {
      const obj: any = {
        huddle_id: this.params.huddle_id,
        account_id: this.userCurrAcc.accounts.account_id,
        user_id: this.userCurrAcc.User.id,
        video_id: this.VideoInfo.id,
        stack_url: file.url,
        video_url: file.key,
        video_file_name: file.filename || file.title,
        video_file_size: file.size,
        video_desc: "",
        current_user_role_id: this.userCurrAcc.roles.role_id,
        current_user_email: this.userCurrAcc.User.email,
        url_stack_check: 1,
        account_role_id: this.userCurrAcc.users_accounts.role_id,
        time: time,
        comment_id: commentId
      };

      this.appMainService.UploadResource(obj).subscribe();
    });
  }
  /** Add/Edit comment functionality ends */

  public AllowTags(default_tags) {
    if (Array.isArray(default_tags)) return default_tags.filter((dt) => { return dt.ref_type != 2; }).length > 0;
    else return false;
  }

  private checkForCommentAutoscroll(videoCurrentTime: number) {
    
    if (!this.playerService.clicked) {
      if (this.comments) {
        let index = this.comments.findIndex(comment => comment.time == videoCurrentTime);

        if (index >= 0) {
          let getComment = JSON.parse(JSON.stringify(this.comments[index]));
          let zeroIndexComment = JSON.parse(JSON.stringify(this.comments[0]));
          this.comments[0] = getComment;
          this.comments[index] = zeroIndexComment;
          
          if (this.settings.autoscroll) {            
            this.currentComment = this.comments;
            // this.currentComment = this.comments[index];
            // this.scrollService.scrollTo("#slimscroll", "#comment_" + (index - 1));
            this.scrollService.scrollTo("#slimscroll", "#comment_" + 0);
          } else {
            this.currentComment = {};
          }
        }
      }
    } else {
      this.playerService.clicked = false
    }
  }

  checkCinemaMode() {
    this.subscriptions.add(this.playerService._videoPlayerCinemaMode.subscribe((res) => this.isCinemaModeOn = res));
  }

  public test(){
    console.log('hello')
  }

  public changeActiveComment(comment,event?) {
    if(event){
      event.stopPropagation();
    }
    if(this.activeComment && comment.id !== this.activeComment.id) {
      this.activeComment = comment;
      this.currentPage = comment.page; 
      this.editDocumentComment = null;
      this.activatedComment.emit(comment);
      this.mainService.updateEditComment(false);
      let el = document.getElementById(`${this.activeComment.id}-comment`) as any;
      el.scrollIntoViewIfNeeded(true);
        this.closeFrameWorkPane();
    }else{
      this.hideLine.emit();
      this.clearActiveComment();
    }
  }

  public changeActiveCommentEdit(comment) {
    if(this.activeComment && comment.id !== this.activeComment.id) {
        this.closeFrameWorkPane();
      this.activeComment = comment;
      this.currentPage = comment.page; 
      this.editDocumentComment = null;
      this.activatedComment.emit(comment);
    }
  }

  public openEditComment(comment) {
    if(this.activeComment && comment.id !== this.activeComment.id) {
      this.changeActiveCommentEdit(comment); 
      setTimeout(() => {
        this.triggerEditComment(comment)
      }, 200);
    } else {
      this.triggerEditComment(comment)
    }  
  }

  public clearActiveComment(){
    this.activeComment = {}
  }

  public getCommentPositon(commentId){
    return document.getElementById(`${commentId}-comment`).getBoundingClientRect();
  }





  // new edit comment functionality ... remove old function from the pdf-comment component
  public triggerEditComment(comment) {
    this.headerService.hideTabRecordIcon(true);
    this.videoPageService.changeEditCommentSource(comment);
    this.mainService.updateEditComment(comment);

    if(this.from=='document'){
      this.documentCommentEdited(comment);
    }

  }

  public initiateDelete(template: TemplateRef<any>, comment, parent?) {
    this.deletableComment = comment;
    this.modalDeleteCheck = true;
    if(comment.created_by !=  this.sessionData.user_current_account.User.id && this.VideoInfo.h_type == 2){
      this.showConfirmationDialog = true;
    } else {
      this.showConfirmationDialog = false;
    }
    this.modalRef = this.modalService.show(template, { class: "modal-md" });
    
  }

  
   public  captureKey = (event,temp) => {
    if(this.activeComment && this.activeComment.id) {
     if(event.key=='Delete' && !this.currEditableComment){ //don't open delete modal if its in editing state
        if(!this.modalDeleteCheck) this.initiateDelete(temp,this.activeComment);
     }
    }

   }

  ngOnDestroy() {
    localStorage.removeItem(`${this.params.video_id}-searchDataText`);
    localStorage.removeItem(`${this.params.video_id}-LsTagId`);
    this.subscriptions.unsubscribe();
    if (this.activeUsersInterval) {
      clearInterval(this.activeUsersInterval);
    }
  }

  public changeActiveCommentForInputField = (comment,event) => {
    if(event.target.id=='reply_count' || event.target.id=='outBox' || event.target.id== `${comment.id}-comment`  ){
      if(this.activeComment && comment.id !== this.activeComment.id) {
        this.activeComment = comment;
        this.currentPage = comment.page; 
        let el = document.getElementById(`${this.activeComment.id}-comment`) as any;
        el.scrollIntoViewIfNeeded(true);
        this.editDocumentComment = null;
        this.activatedComment.emit(comment);
        this.mainService.updateEditComment(false);
        this.closeFrameWorkPane();
      }else{
        this.hideLine.emit();
        this.clearActiveComment();
      }
    }
   
  }
    public closeFrameWorkPane()
    {
        this.videoPageService.updateCurrentTab(0);
        this.videoPageService.updateCurrentTabOnClose(0);
    }
}



interface GeneralInputs {
	NewFolderName: string,
	Confirmation: string,
	ConfirmationKey: string
}
