import { Component, OnInit, OnDestroy, Input, HostListener, ElementRef, ViewChild, OnChanges, EventEmitter, Output } from '@angular/core';
import { ShowToasterService } from '@projectModules/app/services';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';

import { isEmpty } from "@app/helpers";
import { CommentTypingSettingsInterface, RecordingState, AudioPath } from "@videoPage/interfaces";
import { HeaderService, AppMainService } from "@app/services";
import { VideoPageService, MainService, PlayerService } from "@videoPage/services";
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import { MARKER_COLORS } from '@videoPage/constants';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'video-page-video-add-comment',
  templateUrl: './video-add-comment.component.html',
  styleUrls: ['./video-add-comment.component.css']
})
export class VideoAddCommentComponent implements OnInit,OnChanges ,OnDestroy {
  addCommentTitle: any;
  commentTime: any;
  updateComment: any;
  placeholder(arg0: (languageTranslation: any) => any, placeholder: any): import("rxjs").TeardownLogic {
    throw new Error("Method not implemented.");
  }

  @HostListener('window:beforeunload')
  beforeUnload() {
    this.ngOnDestroy();
  }

  @Input('huddleId') public huddleId: number;
  @Input('VideoInfo') public VideoInfo: any;
  @Input('isVideoProcessing') public isVideoProcessing: boolean;
  @Input('permissions') public permissions: any;
  @Input('CustomMarkers') public CustomMarkers: any;
  @Input('userAccountLevelRoleId') public userAccountLevelRoleId: number | string = null;
  @Input('original_account') public original_account: boolean;
  @Input('currentTab') public currentTab:  number | string;
  @Input('isCinemaModeOn') public cinemaModeOn: boolean;
  @Input('artifactComment') public artifactComment: boolean;
  @Input('from') public from = '';
  @ViewChild('audioPlayer', { static: false }) audioPlayer: ElementRef;
  @ViewChild('commentEndTimePopup', { static: true }) commentEndTimePopup;
  @ViewChild('selectedrubricsTemplate', { static: false }) selectedrubricsTemplate;
  
  pageType: string='huddle-page';
  @Input()public 

  public videoOptions = {
    maxFiles: 20,
    accept: GLOBAL_CONSTANTS.RESOURCE_UPLOAD_EXTENSIONS,
    fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'url', 'onedrive'],
    customText: {
      'File {displayName} is not an accepted file type. The accepted file types are {types}': 'File {displayName} is not an accepted file type. The accepted file types are image, text',
    }
  };

  private videoPageType = GLOBAL_CONSTANTS.VIDEO_PAGE_TYPE;
  private COMMENT_STATE = GLOBAL_CONSTANTS.COMMENT_STATE;
  public header_data: any;
  public userCurrAcc: any;
  public bsModalRef: BsModalRef;
  public EditMode: boolean = false;
  public EditChanges: any = {};
  public newComment: any = {
    commentText: '',
    files: [],
    timeEnabled: true
  };
  public markerColors: string[] = MARKER_COLORS;
  public staticFiles: any[];
  public selectedRubrics: any[] = [];
  public temp_selectedRubrics: any[] = [];
  private selectedTag: any = {};
  private tags: any[] = [];
  private currentTimeInSeconds;
  public formattedVideoCurrentTime: string;

  public audioRecorderState: RecordingState = 'comfortZone';
  public localAudioData: any = {};
  public autoSaveAudio = "";

  public commentTextstatus: boolean = false;
  public tempEditComment: any = {};

  public settings: CommentTypingSettingsInterface;
  public videoDuration: number;

  /**Edit comment variables start */
  public availableHours: number[] = [...Array(24).keys()];
  public availableMntsNScnds: number[] = [...Array(60).keys()];
  public commentStartTime: any = [];
  public commentEndTime: any = [];
  /**Edit comment variables end */

  public translation: any;
  private subscriptions: Subscription = new Subscription();

  /** Local storage keys start */
  private VIDEO_PAGE_LS_KEYS = GLOBAL_CONSTANTS.LOCAL_STORAGE.VIDEO_PAGE;
  private addCommentLSKey: string = '';
  private editCommentLSKey: string = '';
  private taCommentsKey: string = '';
  private taEditCommentsKey: string = '';
  /** Local storage keys end */
  public isCinemaModeOn = false;
  public active_class=0;
  public marker_tooltip=false;

  constructor(
    private headerService: HeaderService, private videoPageService: VideoPageService, private mainService: MainService,
    private appMainService: AppMainService, private playerService: PlayerService, private toastr: ShowToasterService,
    private bsModalService: BsModalService,private activatedRoute:ActivatedRoute) {

    this.subscriptions.add(this.videoPageService.videoDuration$.subscribe(videoDuration => this.videoDuration = videoDuration));
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation =>{

      this.translation = languageTranslation
      // this.placeholder=this.translation.vd_plcholder_add_comment;
      // this.addCommentTitle=this.translation.vd_new_comments;
      // this.commentTime=this.translation.video_comment_time;
      // this.updateComment=this.translation.vd_update_comments


    }));
    this.activatedRoute.parent.data.subscribe(data => {
      if (data) {

        console.log(data.pageType);
        console.log(this.videoPageType.WORKSPACE);
        

        if (data.pageType === this.videoPageType.HUDDLE) {
          this.pageType = 'huddle-page';
          this.placeholder=this.translation.vd_plcholder_add_comment;
          this.addCommentTitle=this.translation.vd_new_comments;
          this.commentTime=this.translation.video_comment_time;
          this.updateComment=this.translation.vd_update_comments
        } else if (data.pageType === this.videoPageType.WORKSPACE) {
          this.pageType = 'workspace-page';
          this.placeholder=this.translation.myfile_add_a_note;
          this.addCommentTitle=this.translation.vd_new_notes;
          this.commentTime=this.translation.vd_comment_time;
          this.updateComment=this.translation.vd_update_note

        } else if(data.pageType === this.videoPageType.LIBRARY) {
          this.pageType = 'library-page';
          this.placeholder=this.translation.vd_plcholder_add_comment;
          this.addCommentTitle=this.translation.vd_new_comments;
          this.commentTime=this.translation.video_comment_time;
          this.updateComment=this.translation.vd_update_comments
          // this.checkCinemaMode();
        }
        this.checkCinemaMode();
      }
    });
    this.subscriptions.add(this.videoPageService.commentTypingSettings$.subscribe(
      (commentTypingSettings: CommentTypingSettingsInterface) => this.settings = commentTypingSettings));

    this.subscriptions.add(this.videoPageService.videoCurrentTime$.subscribe((videoCurrentTime: number) => {
      this.formattedVideoCurrentTime = this.formatTime(videoCurrentTime);
      this.currentTimeInSeconds = videoCurrentTime;
    }));

    this.subscriptions.add(this.videoPageService.staticFiles$.subscribe((staticFiles: any[]) => this.staticFiles = staticFiles));

    this.videoPageService.editComment$.subscribe((comment: any) => this.prepareEditComment(comment));

    (this.mainService.PushableRubric.subscribe((rubric: any) => {
      if (this.EditMode) this.EditChanges.changed_standards = true;

      if (rubric.selected) this.selectedRubrics.push(JSON.parse(JSON.stringify(rubric)));
      else this.selectedRubrics = this.selectedRubrics.filter(rub => rubric.account_tag_id != rub.account_tag_id);
    }));

     this.temp_selectedRubrics=this.selectedRubrics;
    //  console.log(this.selectedRubrics);

  }

  ngOnInit() {
    this.header_data = this.headerService.getStaticHeaderData();
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');

    this.addCommentLSKey = `${this.VIDEO_PAGE_LS_KEYS.COMMENT}${this.headerService.getUserId()}-${this.VideoInfo.id}`;
    this.editCommentLSKey = `${this.VIDEO_PAGE_LS_KEYS.EDIT_COMMENT}${this.headerService.getUserId()}-${this.VideoInfo.id}`;
    this.taCommentsKey = `${this.VIDEO_PAGE_LS_KEYS.TA_COMMENTS}${this.headerService.getUserId()}-${this.VideoInfo.id}`;
    this.taEditCommentsKey = `${this.VIDEO_PAGE_LS_KEYS.TA_EDIT_COMMENTS}${this.headerService.getUserId()}-${this.VideoInfo.id}`;

    this.restoreCommentFromLocalStorage();
  }
  ngOnChanges(changes) {
    console.log(this.active_class,this.currentTab);
    if(this.active_class!= 2 && this.currentTab == 2){
      this.active_class= 2;
    }
    else if(this.active_class != 1 && this.currentTab != 2){
      this.active_class=0;
    }
    
  
 
  }
  public opentagsModal() {
    this.bsModalRef = this.bsModalService.show(this.selectedrubricsTemplate,{ class: "modal-md  maxcls", backdrop: 'static' })
  }
  public textWidth(marker,val){
    const elem = document.getElementById("text-width"+val);
   // console.log( "width" ,elem.offsetWidth);
   //  console.log( "widthscroll" ,elem.scrollWidth)
      if(elem.scrollWidth > 74) {
        // console.log(true);
        marker.tooltipEnable=true;
      }
      else{
        // console.log(false);
        marker.tooltipEnable=false;
      }
        
  }
 
textbox(value){
  const txHeight = 70;
const tx = document.getElementById("editTextBox");
const t2 = document.getElementById("plz_change");
var lagend = document.getElementById("lagend");
lagend.textContent = tx['value'];
// const tx1=document.getElementById("editTextBox").clientHeight;
// console.log("txq",tx1)
var height = lagend.scrollHeight;
if(height <= 70)
{
  height = 70;
}
  if (value == '') {
    tx.setAttribute("style", "height:" + txHeight + "px;overflow-y:hidden;");
    t2.setAttribute("style", "position: inherit;margin-top:0;z-index:9;");
  } else {
    tx.setAttribute("style", "height:" + (height) + "px;overflow-y:hidden;");
    if(tx.scrollHeight > 70 || true)
    {
      t2.setAttribute("style", "position: relative;margin-top:-" + (height-70) + "px;z-index:999;");
      // t2.setAttribute("style", "margin-bottom:-" + (tx.scrollHeight-50) + "px;");
    }
    
  }


}

public focusOut(inOut)
 {
  const tx = document.getElementById("editTextBox");
const t2 = document.getElementById("plz_change");
if(inOut)
{
  tx.setAttribute("style", "height:" + (tx.scrollHeight) + "px;overflow-y:hidden;");
    if(tx.scrollHeight > 70)
    {
      // t2.setAttribute("style", "position:relative;top:-" + (tx.scrollHeight-50) + "px;");
      t2.setAttribute("style", "position:relative;margin-top:-" + (tx.scrollHeight-70) + "px;z-index:999;");
    }
}
else
{
  tx.setAttribute("style", "height:" + 70 + "px;overflow-y:hidden;");
    t2.setAttribute("style", "position: inherit;margin-top:0;z-index:9;");
}
  if(inOut ==2){
    tx.setAttribute("style", "height:" + 70 + "px;overflow-y:hidden;");
    t2.setAttribute("style", "position: inherit;margin-top:0;z-index:9;");
  }
}
 
  public AppendNewFiles(obj) {
    if (obj.from == 'comment') {
      this.newComment.files = this.newComment.files.concat(obj.files);
    }
  }

  public RemoveFileFromComment(file, i) {
    this.newComment.files.splice(i, 1);
    if (file.id) {
      const obj = {
        huddle_id: this.huddleId,
        video_id: this.VideoInfo.id,
        document_id: file.id,
        user_id: this.userCurrAcc.User.id,
        attachment_delete_textarea: true
      }
      this.mainService.DeleteResource(obj).subscribe();
    }
    if(this.newComment.files.length <=0 ){
      this.bsModalRef.hide();
    }
  }

  public closeInfo(changeTabe = 0, changeToTab = 0, fromTab = 0) {
    if (changeTabe) {

      this.videoPageService.updateCurrentTab(changeToTab);
    }
  }

  public TriggerTextChange(ev) {
    console.log(this.settings.PauseWhileTyping)
    if (this.settings.PauseWhileTyping) {
      this.playerService.ModifyPlayingState("pause");
    }
    if (ev.keyCode == 13 && this.settings.EnterToPost) {
         this.focusOut(2);
      ev.preventDefault();
      if (!this.EditMode) {
        this.addComment();
      } else {
        this.editComment(1);
      }
    }
  }

  public SetCustomTags(tags) {
    this.tags = tags;
  }

  public ToggleEnterPost() {
    const value = !this.settings.EnterToPost;
    this.videoPageService.updateCommentTypingSettings({ key: 'EnterToPost', value });
  }

  public TogglePause() {
    const value = !this.settings.PauseWhileTyping;
    this.videoPageService.updateCommentTypingSettings({ key: 'PauseWhileTyping', value });

    const obj = {
      user_id: this.header_data.user_current_account.User.id,
      account_id: this.header_data.user_current_account.accounts.account_id,
      value: Number(value)
    };
    this.mainService.SavePauseSettings(obj).subscribe(() => { }, (error) => {
      const value = !this.settings.PauseWhileTyping;
      this.videoPageService.updateCommentTypingSettings({ key: 'PauseWhileTyping', value });
    });
  }

  public updateCurrentTab(value: number) {
    this.videoPageService.updateCurrentTab(value);
  }

  private GetCustomTags() {
    if (!this.tags || this.tags.length == 0) return "";
    let arr = [];
    this.tags.forEach((t) => { arr.push(t.text) });
    return arr.join();
  }

  public allowToComment() {
    let tags = this.GetCustomTags();
    let result = true;
    if ((!(this.newComment.commentText && this.newComment.commentText.trim()) || this.newComment.commentText.trim() == ""))
      result = true;

    if (this.newComment.commentText && (this.newComment.commentText.trim() || (this.newComment.commentText.trim() != "" && this.newComment.commentText.trim() != undefined)))
      result = false;

    if (!!!isEmpty(this.selectedTag) || (!isEmpty(tags)) || !!(this.newComment.files && this.newComment.files.length > 0) || !!(this.selectedRubrics.length > 0))
      result = false;

    return result;
  }

  private PrepareRubrics() {

    if (!this.selectedRubrics || this.selectedRubrics.length == 0) return "";

    let ret = [];

    this.selectedRubrics.forEach((r) => { ret.push(r.account_tag_id); });

    return ret.join(",");
  }

  private PrepareFakeComment(comment, userCurrAcc, obj?) {
    this.selectedTag = !isEmpty(this.selectedTag) && this.selectedTag !== null && this.selectedTag !== undefined ? this.selectedTag : '';
    let fake_comment: any = {
      "valid": true,
      "tryagain": false,
      "fake_id": comment.fake_id,
      "id": -1,
      "parent_comment_id": comment.parent_comment_id ? comment.parent_comment_id : null,
      "title": null,
      "comment": comment.comment,
      "ref_type": comment.ref_type,
      "ref_id": -1,
      "user_id": comment.user_id,
      "time": comment.time || 0,
      "restrict_to_users": 0,
      "created_by": comment.user_id,
      "created_date": "2017-01-23 06:52:14",
      "last_edit_by": -1,
      "last_edit_date": "2017-02-21 20:42:13",
      "active": "1",
      "audio_duration": 0,
      "published_by": null,
      "first_name": userCurrAcc.User.first_name,
      "last_name": userCurrAcc.User.last_name,
      "image": userCurrAcc.User.image,
      "Comment": {
        "last_edit_date": "2017-02-21T20:42:13+00:00",
        "created_date": "2017-01-23T06:52:14+00:00"
      },
      "created_date_string": "Just Now",
      "synchro_time": comment.synchro_time || 0,
      "standard": this.selectedRubrics,
      "default_tags": this.selectedTag,
    
      is_new_comment: comment.is_new_comment
    };
    return fake_comment;

  }

  public addComment() {
    localStorage.removeItem(this.addCommentLSKey);
    // console.log(this.VideoInfo);
    // console.log(this.artifactComment);
    
    
    if (this.audioRecorderState == "resume") {
      this.autoSaveAudio = "add"; // Sending save event to audio component to auto save audio comment.
      return;
    }

    this.autoSaveAudio = "";
    this.localAudioData = {};

    let allowComment = this.allowToComment();
    if (allowComment) {
      if(this.pageType == 'huddle-page')
      this.toastr.ShowToastr('info',this.translation.please_enter_text_to_comment);
      else
      this.toastr.ShowToastr('info',this.translation.please_enter_text_to_note);

      return;
    } else {
      let comment_time = moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS")
      let obj = {
        videoId: this.VideoInfo.id,
        for: (this.newComment.timeEnabled) ? "synchro_time" : "",
        synchro_time: (this.newComment.timeEnabled) ? Math.floor(this.currentTimeInSeconds) : '',
        time: (this.newComment.timeEnabled) ? Math.floor(this.currentTimeInSeconds) : 0,
        ref_type: this.newComment.ref_type || '2',
        comment: this.newComment.commentText || "",
        audio_duration: this.newComment.audioDuration || null,
        user_id: this.userCurrAcc.User.id,
        standards_acc_tags: this.PrepareRubrics(),
        default_tags: this.GetCustomTags(),
        assessment_value: (!isEmpty(this.selectedTag)) ? "# " + this.selectedTag.tag_title : "",
        fake_id: `${new Date().getTime()}-${this.VideoInfo.id}`,
        first_name: this.userCurrAcc.User.first_name,
        last_name: this.userCurrAcc.User.last_name,
        company_name: this.userCurrAcc.accounts.company_name,
        image: this.userCurrAcc.User.image,
        account_role_id: this.userCurrAcc.users_accounts.role_id,
        current_user_email: this.userCurrAcc.User.email,
        created_at_gmt: comment_time,
        is_new_comment: true
      };

      if (this.artifactComment) {
        obj['xPos'] = this.VideoInfo.xPos;
        obj['yPos'] = this.VideoInfo.yPos;
        obj['page'] = this.VideoInfo.page;
      }

      if (this.pageType == 'library-page') {
        obj['library'] = 1;
        }

      let files = [];
      if (this.newComment.files && this.newComment.files.length > 0) files = this.newComment.files;
      if (this.settings.PauseWhileTyping) this.playerService.PlayerPlayingState.emit("play");

      this.videoPageService.updateCurrentTab(0);
      let preparedCommentObj: any = this.PrepareFakeComment(obj, this.userCurrAcc);
      if (obj.ref_type == 6 && this.newComment.localAudio && this.newComment.audioUrl) {
        preparedCommentObj.comment = this.newComment.audioUrl;
      }

      this.videoPageService.addNewComment({ comment: preparedCommentObj, tryAgain: false });
      this.ResetForm();
      this.updateCurrentTab(0);
      this.active_check(0);

      this.appMainService.AddComment(obj).subscribe((data: any) => {
        if (data.status == "success") {
          let x = {
            original_id:data[0].id,
            fake_id:preparedCommentObj.fake_id
          }
          this.videoPageService.updateCommentStatus(x);
         this.videoPageService.updatepublishComment(data[0]);
          if (files.length > 0) this.uploadCommentResources(files, obj.time, data[0].id);
        } else if (data.status == "failed") this.toastr.ShowToastr('error',data.message);
        else this.toastr.ShowToastr('info',this.translation.something_went_wrong_msg);
      }, () => {
        /** Adding text comment to localstorage for try again start */
        preparedCommentObj.tryagain = true;
        preparedCommentObj.audioDuration = obj.audio_duration;
        preparedCommentObj.videoId = obj.videoId;
        preparedCommentObj.for = obj.for;
        preparedCommentObj.standards_acc_tags = obj.standards_acc_tags;
        preparedCommentObj.default_tags = obj.default_tags;
        preparedCommentObj.assessment_value = obj.assessment_value;
        preparedCommentObj.company_name = obj.company_name;
        preparedCommentObj.account_role_id = obj.account_role_id;
        preparedCommentObj.current_user_email = obj.current_user_email;
        preparedCommentObj.files = files;
        preparedCommentObj.created_at_gmt = comment_time;


        this.videoPageService.addNewComment({ comment: preparedCommentObj, tryAgain: true });
        const taComments = this.headerService.getLocalStorage(this.taCommentsKey) || [];
        taComments.push(preparedCommentObj);
        this.headerService.setLocalStorage2(this.taCommentsKey, taComments);
        /** Adding text comment to localstorage for try again end */
      });
    }

  }

  private uploadCommentResources(files: any[], time: number, commentId: number) {

    files.forEach(file => {
      const obj: any = {
        huddle_id: this.huddleId,
        account_id: this.userCurrAcc.accounts.account_id,
        user_id: this.userCurrAcc.User.id,
        video_id: this.VideoInfo.id,
        stack_url: file.url,
         // File Stack changes added
        fileStack_handle : file.handle,
         //filestack url added
        fileStack_url : file.url,
        video_url: file.key,
        video_file_name: file.filename || file.title,
        video_file_size: file.size,
        video_desc: "",
        current_user_role_id: this.userCurrAcc.roles.role_id,
        current_user_email: this.userCurrAcc.User.email,
        url_stack_check: 1,
        account_role_id: this.userCurrAcc.users_accounts.role_id,
        time: time,
        comment_id: commentId
      };

      this.appMainService.UploadResource(obj).subscribe();
    });
  }

  private ResetForm() {
    this.newComment.commentText = "";
    this.newComment.audioUrl = '';
    this.newComment.localAudio = false;
    this.newComment.ref_type = 2;
    this.newComment.audioDuration = null;
    this.newComment.files = [];
    this.selectedRubrics = [];
    this.localAudioData = {};
    this.mainService.ResetCustomTags();
    this.mainService.ResetSelectedRubrics();
    this.selectedTag = {};
    this.EditMode = false;
  }

  /** Audio functionality starts */
  public currentState(state: RecordingState) {
    this.audioRecorderState = state;
    if (this.audioRecorderState == "recording")
      this.playerService.ModifyPlayingState("pause");
  }

  public audioPath(audioPath: AudioPath) {
    if (audioPath.filePath) {
      this.newComment.commentText = audioPath.filePath;
      this.newComment.audioDuration = audioPath.audioDuration;
      this.newComment.audioUrl = audioPath.audioUrl;
      this.newComment.localAudio = true;
      this.newComment.ref_type = 6;
    } else {
      if (!this.commentTextstatus) {
        this.newComment.commentText = '';
        this.newComment.audioDuration = null;
        this.commentTextstatus = false;
      }
      this.newComment.audioUrl = '';
      this.newComment.localAudio = false;
      if (Object.keys(this.tempEditComment).length) {
        this.newComment = this.tempEditComment;
        this.tempEditComment = {};
      }
      else {
        this.newComment.ref_type = 2;
      }
    }
    if (audioPath.autoSubmitComment == "add") {
      this.addComment();
    }
    else if (audioPath.autoSubmitComment == "edit") {
      this.editComment(1);
    }
  }
  /** Audio functionality end */


  public formatTime(time: number) {

    if (!time) return this.translation.vd_all_videos;
    let sec_num: any = parseInt(String(time), 10);
    let hours: any = Math.floor(sec_num / 3600);
    let minutes: any = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds: any = sec_num - (hours * 3600) - (minutes * 60);

    if (hours == 0 && minutes == 0 && seconds == 0) {
      return this.translation.vd_all_videos;
    }

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return hours + ':' + minutes + ':' + seconds;
  }

  public getMarkerBg(tag, index) {
    if (this.selectedTag == tag) {
      return this.markerColors[index];
     } 
    else {
      return "transparent";
    }
  }
  active_check(val){
    if(val == this.active_class)
    {
      console.log("VAL 2",val);
      this.active_class=0;
      this.updateCurrentTab(0);
     
    }
    else{
      this.active_class=val;
      this.updateCurrentTab(2);

    }
    if(this.active_class == 2){
      this.updateCurrentTab(2);
      if(this.settings.PauseWhileTyping){
        this.playerService.ModifyPlayingState('pause');
      }
    }
    else{
      this.updateCurrentTab(0);
    }
  
  }

  public ChooseCustomTag(tag) {
    
    if (this.selectedTag == tag) {
      this.selectedTag = {};
    } else {
      this.selectedTag = tag;
    }
  }

  public deSelectRubric(rubric: any, index: number) {
    this.selectedRubrics.splice(index, 1);
    this.videoPageService.updateSelectedRubric({ rubric, selected: false });
    if(this.selectedRubrics.length <= 0){
      this.bsModalRef.hide();
    }
  }

  /** Comment edit functionality starts */
  private prepareEditComment(comment: any, fromLocalStorage?: boolean) {
    localStorage.removeItem(this.addCommentLSKey);
    this.mainService.ResetSelectedRubrics();
    this.mainService.isEditComent = true;
    this.EditMode = true;
    this.EditChanges = {};

    this.newComment.id = comment.id;
    this.newComment.time = comment.time;
    this.newComment.timeEnabled = comment.time > 0;
    this.newComment.commentText = comment.comment;
    this.newComment.audioDuration = comment.audioDuration;
    this.newComment.audioUrl = comment.comment;
    this.newComment.ref_type = comment.ref_type;
    this.tempEditComment = this.newComment;
    this.localAudioData = {};
    if (this.EditMode) this.newComment.commentText = comment.comment;

    if (this.newComment.ref_type == 6) this.commentTextstatus = false;
    else this.commentTextstatus = true;

    this.audioRecorderState = "comfortZone";

    this.commentStartTime = this.formatTime(comment.time);
    this.commentEndTime = (comment.end_time) ? this.formatTime(comment.end_time) : this.formatTime(comment.time);
    if (this.commentStartTime == this.translation.vd_all_videos.trim()) {
      this.newComment.timeEnabled = false;
      this.commentEndTime = 0;
    }
    this.commentStartTime = this.commentStartTime == this.translation.vd_all_videos.trim() ? this.commentStartTime : this.commentStartTime.split(":");
    this.commentEndTime = (this.commentEndTime) ? this.commentEndTime.split(":") : null;

    this.newComment.files = comment.files;
    if(this.EditMode && this.isCinemaModeOn){
      let element= (<HTMLElement>document.querySelector(".tab-container"));
      element.scrollIntoView({ behavior: 'smooth' });
    }
    if(!this.isCinemaModeOn)
    window.scrollTo(0, document.body.scrollHeight - 30);

    this.tags = [];

    this.selectedRubrics = [];

    if (!fromLocalStorage) {
      comment.default_tags.forEach((dt) => {
        const index = this.CustomMarkers.findIndex(marker => marker.account_tag_id == dt.account_tag_id);
       // if (index > -1) this.selectedTag = this.CustomMarkers[index];
        if (index > -1) this.selectedTag = this.CustomMarkers[index]; else this.selectedTag = null;
        this.perpareEditableTag(dt);
      });
      comment.standard.forEach((rubric) => {
        this.videoPageService.updateSelectedRubric({ rubric, selected: true });
        this.selectedRubrics.push(rubric);
      });
    } else {
      this.SetCustomTags(comment.customTags);
      this.selectedRubrics = comment.selectedRubrics;

      if (comment.selectedCustomMarker) {
        this.CustomMarkers.forEach(marker => {
          if (marker.tag_title == comment.selectedCustomMarker) {
            this.ChooseCustomTag(marker);
          }
        });
      }
    }
    if(comment.default_tags.length == 0) {
      this.selectedTag = null;
    }
    if (comment.ref_type == 6 && this.audioPlayer) {
      this.audioPlayer.nativeElement.load();
    }
  }

  private AttachFilesToComment(comment) {
    comment.files = [];
    comment.files = this.staticFiles.filter(staticFile => staticFile.comment_id == comment.id);
  }

  private perpareEditableTag(tag) {
    console.log(tag)
    if (tag.tag_type == void 0) this.tags.push({ text: tag.tag_title, id: this.tags.length + 1 });

  }

  public updateCommentStartTime(index: number, value: number) {
    this.commentStartTime[index] = value;
  }

  public updateCommentEndTime(index: number, value: number) {
    this.commentEndTime[index] = value;
  }

  public editComment(flag) {

    
    localStorage.removeItem(this.editCommentLSKey);

    if (flag == 0) {
      this.ResetForm();
      return;
    }

    if (this.audioRecorderState == "resume") {
      this.autoSaveAudio = "edit"; //Sending save event to audio component to auto save audio comment.
      return;
    }

    if (this.newComment.commentText && this.newComment.commentText.indexOf('?Expires') > 0) {
      const toIndex = this.newComment.commentText.indexOf('?Expires');
      const fromIndex = this.newComment.commentText.indexOf('uploads/');
      this.newComment.commentText = this.newComment.commentText.slice(fromIndex, toIndex);
    }

    this.autoSaveAudio = "";
    this.localAudioData = {};

    let allowComment = this.allowToComment();
    if (allowComment) {
      this.toastr.ShowToastr('info',this.translation.please_enter_text_to_new_note);
      return;
    } else {
      const commentStartTimeInSeconds = this.newComment.timeEnabled ? Math.floor(this.timeToSeconds(this.commentStartTime.join(":"))) : 0;
      const commentEndTimeInSeconds = this.newComment.timeEnabled ? Math.floor(this.timeToSeconds(this.commentEndTime.join(":"))) : 0;

      if (this.newComment.timeEnabled && (this.videoDuration < commentStartTimeInSeconds || this.videoDuration < commentEndTimeInSeconds)) {
        this.toastr.ShowToastr('info',this.translation.new_note_time_ahead);
        return;
      }
      if (this.newComment.timeEnabled && commentEndTimeInSeconds < commentStartTimeInSeconds) {
        this.bsModalRef = this.bsModalService.show(this.commentEndTimePopup, { class: 'modal-md comment-end-time-popup' });
        this.commentTextstatus = true;
        return;
      }

      let files = [];

      if (this.newComment.files && this.newComment.files.length > 0) {

        files = this.newComment.files.filter(file => !file.id);
        if (files.length > 0) this.uploadCommentResources(files, this.newComment.time, this.newComment.id);
      }

      let comment_time = moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS")

      let editComment: any = {
        huddle_id: this.huddleId,
        videoId: this.VideoInfo.id,
        comment_id: this.newComment.id,
        for: (this.newComment.timeEnabled) ? "synchro_time" : "",
        time: (this.newComment.timeEnabled) ? commentStartTimeInSeconds : 0,
        synchro_time: (this.newComment.timeEnabled) ? commentStartTimeInSeconds : 0,
        end_time: (this.newComment.timeEnabled && this.commentEndTime) ? commentEndTimeInSeconds : 0,
        ref_type: this.newComment.ref_type || '2',
        comment: this.newComment.commentText,
        audio_duration: this.newComment.audioDuration,
        user_id: this.userCurrAcc.User.id,
        account_id: this.userCurrAcc.accounts.account_id,
        standards_acc_tags: this.PrepareRubrics(),
        default_tags: this.GetCustomTags(),
        assessment_value: (!isEmpty(this.selectedTag)) ? "# " + this.selectedTag.tag_title : "",
        account_role_id: this.userCurrAcc.users_accounts.role_id,
        current_user_email: this.userCurrAcc.User.email,
        created_at_gmt: comment_time
      };

      this.videoPageService.updateCommentState({ id: this.newComment.id, state: this.COMMENT_STATE.UPDATING });
      this.videoPageService.updateCurrentTab(0);
      this.ResetForm();
      this.updateCurrentTab(0);
      this.active_check(0);
    if (this.pageType == 'library-page') {
      editComment['library'] = 1;
      }
      this.appMainService.EditComment(editComment).subscribe((data: any) => {
        if (data.status != "success") {
          this.toastr.ShowToastr('info',this.translation.something_went_wrong_msg);
        }
        this.mainService.ReRenderMarkers.emit(data);
      }, () => {
        editComment.files = files;
        setTimeout(() => {
          this.videoPageService.updateCommentState({ id: this.newComment.id, state: this.COMMENT_STATE.UPDATING_ERROR });
          this.videoPageService.updateEditCommentErrorSource(editComment);
        }, 500);
        const taEditComments = this.headerService.getLocalStorage(this.taEditCommentsKey) || [];
        const commentIndex = taEditComments.findIndex(taEditComment => taEditComment.comment_id == editComment.comment_id);
        if (commentIndex > -1) taEditComments[commentIndex] = editComment;
        else taEditComments.push(editComment);
        this.headerService.setLocalStorage2(this.taEditCommentsKey, taEditComments);
      });
    }

  }

  private timeToSeconds(time) {
    if (time == 0) return 0;
    if (typeof (time) == "number") return time;
    let stamp = time.split(":");
    return Number(stamp[0] * 3600) + Number(stamp[1] * 60) + Number(stamp[2]);
  }

  /** Comment edit functionality end */

  /**
   * Restore add comment from localStorage and if add comment is not existed in localStorage
   * then try to restore edit comment from localStorage and display it in comment box for save or update
   */
  private restoreCommentFromLocalStorage() {
    const addComment = this.headerService.getLocalStorage(this.addCommentLSKey);
    const editComment = this.headerService.getLocalStorage(this.editCommentLSKey);

    if (addComment) {
      this.videoPageService.updateCommentExistedInLocalStorage(true);
      this.newComment.commentText = addComment.comment;
      this.newComment.ref_type = addComment.ref_type;
      this.newComment.timeEnabled = addComment.ref_type;
      this.newComment.audioDuration = addComment.audioDuration;
      this.SetCustomTags(addComment.customTags);
      this.newComment.files = addComment.files;
      this.selectedRubrics = addComment.selectedRubrics;
      this.newComment.timeEnabled = addComment.timeEnabled;

      if (addComment.localAudio) {
        this.newComment.audioUrl = addComment.audioUrl;
        this.newComment.localAudio = addComment.localAudio;
        this.audioRecorderState = 'play';
        this.localAudioData.localAudio = addComment.localAudio;
        this.localAudioData.audioUrl = addComment.audioUrl;
        this.localAudioData.audioRecorderState = this.audioRecorderState;
      }

      if (addComment.selectedCustomMarker) {
        this.CustomMarkers.forEach(marker => {
          if (marker.tag_title == addComment.selectedCustomMarker) {
            this.ChooseCustomTag(marker);
          }
        });
      }
      if (addComment.time) {
        this.formattedVideoCurrentTime = this.formatTime(addComment.time);
        this.currentTimeInSeconds = addComment.time;
      } else {
        this.formattedVideoCurrentTime = this.translation.vd_all_videos.trim();
      }
    } else if (editComment) {
      this.prepareEditComment(editComment, true);
    }

  }

  private saveCommentToLS() {
    let key = this.addCommentLSKey;

    const comment: any = {
      videoId: this.VideoInfo.id,
      for: (this.newComment.timeEnabled) ? "synchro_time" : "",
      synchro_time: (this.newComment.timeEnabled) ? Math.floor(this.currentTimeInSeconds) : '',
      time: (this.newComment.timeEnabled) ? Math.floor(this.currentTimeInSeconds) : 0,
      ref_type: this.newComment.ref_type || 2,
      comment: this.newComment.commentText || "",
      audioDuration: this.newComment.audioDuration || null,
      audioUrl: this.newComment.audioUrl,
      localAudio: this.newComment.localAudio,
      user_id: this.userCurrAcc.User.id,
      standards_acc_tags: this.PrepareRubrics(),
      default_tags: this.GetCustomTags(),
      assessment_value: (!isEmpty(this.selectedTag)) ? "# " + this.selectedTag.tag_title : "",
      first_name: this.userCurrAcc.User.first_name,
      last_name: this.userCurrAcc.User.last_name,
      company_name: this.userCurrAcc.accounts.company_name,
      image: this.userCurrAcc.User.image,
      account_role_id: this.userCurrAcc.users_accounts.role_id,
      current_user_email: this.userCurrAcc.User.email,
      files: this.newComment.files,
      customTags: this.tags,
      selectedRubrics: this.selectedRubrics,
      selectedCustomMarker: (!isEmpty(this.selectedTag)) ? this.selectedTag.tag_title : "",
      timeEnabled: this.newComment.timeEnabled

    };
    if (this.EditMode) {
      key = this.editCommentLSKey;
      comment.id = this.newComment.id;
      comment.time = this.newComment.timeEnabled ? Math.floor(this.timeToSeconds(this.commentStartTime.join(":"))) : 0;
      comment.synchro_time = comment.time;
      comment.end_time = this.newComment.timeEnabled ? Math.floor(this.timeToSeconds(this.commentEndTime.join(":"))) : 0;
    }

    this.headerService.setLocalStorage2(key, comment);
  }
  public cancel() {
    this.newComment.ref_type = 2;
    this.newComment.commentText = '';
  }
  checkCinemaMode(){
    this.subscriptions.add(this.playerService._videoPlayerCinemaMode.subscribe((res) =>
      this.isCinemaModeOn = res));
  }

  ngOnDestroy() {
    if (!this.allowToComment()) this.saveCommentToLS();
    this.subscriptions.unsubscribe();
  }

}
