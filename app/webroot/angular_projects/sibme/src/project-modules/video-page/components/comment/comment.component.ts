import { Component, OnInit, Input, EventEmitter, Output, TemplateRef, OnChanges, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { trigger, style, animate, transition } from '@angular/animations';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import * as _ from "underscore";
import { HeaderService, AppMainService } from "@app/services";
import { PlayerService, MainService, ScrollService, VideoPageService } from "@videoPage/services";
import { CommentTypingSettingsInterface } from '@videoPage/interfaces';
import { environment } from "@environments/environment";
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import * as moment from 'moment';

@Component({
  selector: 'comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css'],
  animations: [
    trigger(
      'enterAnimation', [
      transition(':leave', [
        style({ transform: 'translateY(0)', opacity: 1 }),
        animate('500ms', style({ transform: 'translateY(100%)', opacity: 0 }))
      ])
    ]
    )
  ]
})
export class CommentComponent implements OnInit, OnDestroy, OnChanges {

  @ViewChild('audioPlayer', { static: false }) audioPlayer: ElementRef;

  @Input('fromLiveStream') fromLiveStream: boolean;
  @Input('comment') comment;
  @Input('allow_evidence_view') allow_evidence_view;
  @Input('customMarkers') customMarkers;
  @Input('classes') classes;
  @Input('isActive') isActive;
  @Input('original_account') public original_account:boolean;
  @Input() showCrudOptions = true;
  @Input() params;
  @Input() staticFiles;
  @Input() from;
  @Input() permissions: any = {};
  @Input() index;
  @Input() VideoInfo;

  @Output() onEdit: EventEmitter<any> = new EventEmitter<any>();
  @Output() onEditDocumentComment: EventEmitter<any> = new EventEmitter<any>();
  @Output() onDelete: EventEmitter<any> = new EventEmitter<any>();
  @Output() onReplyEdit: EventEmitter<any> = new EventEmitter<any>();
  @Output() retryAddCommentEvent: EventEmitter<any> = new EventEmitter<any>();
  @Output() retryEditCommentEvent: EventEmitter<any> = new EventEmitter<any>();

  private videoPageType = GLOBAL_CONSTANTS.VIDEO_PAGE_TYPE;
  public COMMENT_STATE = GLOBAL_CONSTANTS.COMMENT_STATE;
  public modalRef: BsModalRef;

  public settings: CommentTypingSettingsInterface;
  private deletableComment;
  public currentUser;
  public header_color;
  public primery_button_color;
  public secondry_button_color;
  public showConfirmationDialog: boolean = false;
  public header_data;
  public translation: any = {};
  private subscriptions: Subscription = new Subscription();
  public userAccountLevelRoleId: number | string = null;
  public fromScriptedOvservations: boolean = false;
  public sessionData;
  Reply_tryagain: boolean = false;
  minusIds: any = -1;
  localReplyArry: any = [];
  SubReply_tryagain: boolean;
  public Inputs: GeneralInputs;
  localsubReplyArry: any = [];
  private VIDEO_PAGE_LS_KEYS = GLOBAL_CONSTANTS.LOCAL_STORAGE.VIDEO_PAGE;
  private addReplyLSKey: string = '';
  private taEditRepliesKey: string = '';
  private taEditSubRepliesKey: string = '';
  pageType: string;
  confirmDeleteMessage: any;
  public isNotCanvasPage = true;

  constructor(
    public scrollService: ScrollService,
    private toastr: ShowToasterService,
    private activatedRoute: ActivatedRoute,
    private headerService: HeaderService,
    private mainService: MainService,
    private playerService: PlayerService,
    private modalService: BsModalService,
    private router: Router,
    private appMainService: AppMainService,
    private videoPageService: VideoPageService) {

    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      this.confirmDeleteMessage = this.translation.vd_cnf_delete_comment
      this.Inputs = { NewFolderName: "", Confirmation: "", ConfirmationKey: this.translation.Huddle_confirmation_bit };
    }
    ));
    if (document.location.href.indexOf('/canvas/') >= 0){
      this.isNotCanvasPage = false;
    }
    this.activatedRoute.parent.data.subscribe(data => {
      if (data) {
        if (data.pageType === this.videoPageType.HUDDLE) {
          this.pageType = 'huddle-page';

        } else if (data.pageType === this.videoPageType.WORKSPACE) {
          this.pageType = 'workspace-page';
          this.confirmDeleteMessage = this.translation.myfile_confirm_alert
        }
        else if(data.pageType === this.videoPageType.LIBRARY) {
          this.pageType = 'library-page';

        }
      }
    });
    this.subscriptions.add(this.videoPageService.commentTypingSettings$.subscribe(
      (commentTypingSettings: CommentTypingSettingsInterface) => this.settings = commentTypingSettings));
    this.subscriptions.add(this.videoPageService.commentState$.subscribe(commentState => {
      if (commentState.id == this.comment.id) this.comment.state = commentState.state;
    }));

    this.activatedRoute.url.subscribe((url) => {
      if (url && url[0] && url[0].path == "scripted_observations") {
        this.fromScriptedOvservations = true;
      }
    });

  }

  ngOnInit() {
    this.sessionData = this.headerService.getStaticHeaderData();
    this.currentUser =  this.sessionData.user_current_account.User;
    this.currentUser.role_id =  this.sessionData.user_current_account.roles.role_id;
    this.header_color =  this.sessionData.header_color;
    this.primery_button_color =  this.sessionData.primery_button_color;
    this.secondry_button_color =  this.sessionData.secondry_button_color;
    this.header_data = this.headerService.getStaticHeaderData();
    this.userAccountLevelRoleId = this.header_data.user_permissions.roles.role_id;
    this.addReplyLSKey = `${this.VIDEO_PAGE_LS_KEYS.REPLY}${this.comment.id}_${this.headerService.getUserId()}`;
    if (this.from !== 'tab') {
      const userAndVideoId = `${this.headerService.getUserId()}-${this.VideoInfo.id}`;
      this.taEditRepliesKey = `${this.VIDEO_PAGE_LS_KEYS.TA_EDIT_REPLIES}${userAndVideoId}`;
      this.taEditSubRepliesKey = `${this.VIDEO_PAGE_LS_KEYS.TA_EDIT_SUB_REPLIES}${userAndVideoId}`;
      this.restoreCommentsFromLocalStorage();
    }

    if (this.comment.ref_type == 6) {
      this.checkIfAduioCommentIsPlayable(this.comment.comment);
    }

  }

  GetReplyText() {
    if (this.comment.Comment.responses != undefined) {
      this.comment.Comment.responses.forEach(reply => {
        if (localStorage.getItem(this.header_data.user_current_account.User.id + '_video_play_reply_edit_' + reply.id) != null && localStorage.getItem(this.header_data.user_current_account.User.id + '_video_play_reply_edit_' + reply.id) != undefined) {
          reply.EditableText = localStorage.getItem(this.header_data.user_current_account.User.id + '_video_play_reply_edit_' + reply.id);
        } else {
          reply.EditableText = reply.comment;
        }
      });
    }
  }
  GetSubReplyText() {
    if (this.comment.Comment.responses != undefined) {
      this.comment.Comment.responses.forEach(x => {
        if (x.responses) {
          x.responses.forEach(sub_reply => {
            if (localStorage.getItem(this.header_data.user_current_account.User.id + '_video_play_sub_reply_edit_' + sub_reply.id) != null && localStorage.getItem(this.header_data.user_current_account.User.id + '_video_play_sub_reply_edit_' + sub_reply.id) != undefined) {
              sub_reply.EditableText = localStorage.getItem(this.header_data.user_current_account.User.id + '_video_play_sub_reply_edit_' + sub_reply.id);
            } else {
              sub_reply.EditableText = sub_reply.comment;
            }
          });
        }
      });
    }
  }
  ngOnChanges(change) {
    if (change.comment && change.comment.firstChange) {
      if (this.staticFiles) {
        this.AttachFilesToComment(this.comment, true);
      }
    }
  }
  public TriggerTextChange(ev) {

		if (ev.keyCode == 13) {
			this.ResolveDelete(1)

		}
	}
  public initiateDelete(template: TemplateRef<any>, comment, parent?) {
    this.deletableComment = comment;
    if(comment.created_by !=  this.sessionData.user_current_account.User.id && this.VideoInfo.h_type == 2){
      this.showConfirmationDialog = true;
    } else {
      this.showConfirmationDialog = false;
    }
    this.modalRef = this.modalService.show(template, { class: "modal-md" });
    
  }

  public getCustomTagsLength(default_tags) {

    let matched = _.where(default_tags, { ref_type: 1 });
    if (matched) return matched.length;
    return false;
  }

  public getUserImgUrl(comment) {


    if (comment.image && comment.image.length > 0) {

      let img = comment.image;

      return "https://s3.amazonaws.com/sibme.com/static/users/" + comment.user_id + "/" + img;

    }

    return environment.baseUrl + "/img/home/photo-default.png";



  }

  public ReplyTextChanged(ev, comment) {

    if (ev.keyCode == 13 && this.settings.EnterToPost) {

      ev.preventDefault();

      this.AddReply(comment);

    }

  }






  public SubReplyTextChanged(ev, comment, reply, sub_reply?) {

   
    if (ev.keyCode == 13 && this.settings.EnterToPost) {
      ev.preventDefault();
      if (sub_reply) {
        sub_reply.replyEnabled = false;
      }
      this.SubmitSubReply(comment, reply);

    }

  }

  public AttachFilesToComment(comment, is_internal?) {

    if (!comment.isExpnded && !is_internal) return;

    if (((typeof (comment.time) == "number" && comment.time == 0) || comment.time == "0") && !comment.is_new_comment) {

      comment.files = [];
      return;

    }

    comment.files = [];

    if (comment.is_new_comment) {
      comment.files = this.staticFiles.filter(file => file.comment_id == comment.id);
      return;
    }

    if (this.staticFiles && this.staticFiles.length > 0)
      this.staticFiles.forEach((file) => {

        if (!file) return;

        file.time2 = file.time == "All Video" ? 0 : this.FormatToSeconds(file.time);

        comment.time2 = this.FormatToSeconds(comment.time);
        if (comment.time2 == file.time2) {

          comment.files.push(file);

        }

      })





  }

  private WhetherScrollOrNot(elementId) {

    let parentDiv = (<HTMLElement>document.querySelector("#slimscroll"));

    if (!parentDiv) return;

    let scrollerHeightInPx = parentDiv.style.height;

    let scrollHeight = Number(scrollerHeightInPx.substring(0, scrollerHeightInPx.indexOf("px")));

    let elementScroll = (document.querySelector(elementId)).offsetTop;

    return elementScroll + 185 > scrollHeight;

  }

  private FormatToSeconds(time) {

    if (time == 0 || !time) return 0;
    if (typeof (time) == "number") return time;
    let stamp = time.split(":");

    return Number(stamp[0] * 3600) + Number(stamp[1] * 60) + Number(stamp[2]);

  }

  public retryEditComment(comment: any) {
    if (comment.processing) return;

    comment.processing = true;
    comment.edittryagain = false;
    comment.state = this.COMMENT_STATE.UPDATING;

    this.retryEditCommentEvent.emit(comment);
  }

  public AddReply(comment, reply?, replyindex?) {
    if (!comment.replyText || comment.replyText == "") {
      this.toastr.ShowToastr('info',this.translation.vd_alert_please_add_text);
      return;
    }
    let sessionData: any = this.headerService.getStaticHeaderData();
    let comment_time = moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS")
    let tmp_uuid = `${new Date().getTime()}-${this.params.video_id}`;
    let obj = {
      parent_id: comment.id,
      account_id: sessionData.user_current_account.accounts.account_id,
      comment: comment.replyText,
      access_level: "nested",
      huddle_id: this.params.huddle_id,
      user_id: sessionData.user_current_account.User.id,
      first_name: sessionData.user_current_account.User.first_name,
      last_name: sessionData.user_current_account.User.last_name,
      company_name: sessionData.user_current_account.accounts.company_name,
      image: sessionData.user_current_account.User.image,
      uuid: tmp_uuid,
      fakeComment: true,
      created_at_gmt: comment_time

    }
    if (this.pageType == 'library-page') {
      obj['library'] = 1;
      }
      
    comment.replyEnabled = false;
    comment.replyAdding = true;
    comment.replyText = "";
    if (!comment.Comment) comment.Comment = {};
    if (!comment.Comment.responses) comment.Comment.responses = [];
   
    comment.Comment.responses.push(obj);
    
    localStorage.removeItem(this.addReplyLSKey);
   

    this.appMainService.AddReply(obj).subscribe((data: any) => {
      localStorage.removeItem('video_reply_' + comment.id + '_' + this.header_data.user_current_account.User.id)
      let lsData = localStorage.getItem(this.header_data.user_current_account.User.id + '_fake_reply_comment_video_' + this.params.video_id);
      let dddata = JSON.parse(lsData);
      if (dddata) {
        dddata.forEach((x, ii) => {
          if (x.id == reply.id) {
            dddata.splice(ii, 1)
          }
        });
      }
      localStorage.setItem(this.header_data.user_current_account.User.id + '_fake_reply_comment_video_' + this.params.video_id, JSON.stringify(dddata))

    }, (err) => {
      localStorage.removeItem('video_reply_' + comment.id + '_' + this.header_data.user_current_account.User.id)
      comment.replyText = '';
      let localdata: any = obj;
      localdata.id = this.minusIds - 1;
      localdata.tryagain = true;
      this.localReplyArry.push(localdata);
      localStorage.setItem(this.header_data.user_current_account.User.id + '_fake_reply_comment_video_' + this.params.video_id, JSON.stringify(this.localReplyArry));
    });
  }
  public ReAddReply(comment, reply?, replyindex?) {
    if (reply.processing) return;

    reply.processing = true;
    reply.tryagain = false;
    if (!comment.replyText || comment.replyText == "") {
      this.toastr.ShowToastr('info',this.translation.vd_alert_please_add_text);
      return;
    }
    let sessionData: any = this.headerService.getStaticHeaderData();
    let obj = {
      parent_id: comment.id,
      account_id: sessionData.user_current_account.accounts.account_id,
      comment: comment.replyText,
      access_level: "nested",
      huddle_id: this.params.huddle_id,
      user_id: sessionData.user_current_account.User.id,
      first_name: sessionData.user_current_account.User.first_name,
      last_name: sessionData.user_current_account.User.last_name,
      company_name: sessionData.user_current_account.accounts.company_name,
      image: sessionData.user_current_account.User.image,
      uuid: reply.uuid
    }
    comment.replyEnabled = false;
    comment.replyAdding = true;
    if (this.pageType == 'library-page') {
      obj['library'] = 1;
      }
    this.appMainService.AddReply(obj).subscribe((data: any) => {
 
      if (data.status == "failed") {
        this.toastr.ShowToastr('error',data.message);
        this.router.navigate(['/'])
      }

      comment.replyAdding = false;
      comment.replyText = "";
      this.comment.replyText = '';
      localStorage.removeItem('video_reply_' + comment.id + '_' + this.header_data.user_current_account.User.id);
      let lsData = localStorage.getItem(this.header_data.user_current_account.User.id + '_fake_reply_comment_video_' + this.params.video_id);
      let dddata = JSON.parse(lsData);
      if (dddata) {
        dddata.forEach((x, ii) => {
          if (x.id == reply.id) {
            dddata.splice(ii, 1)
          }
        });
      }
      localStorage.setItem(this.header_data.user_current_account.User.id + '_fake_reply_comment_video_' + this.params.video_id, JSON.stringify(dddata))
      localStorage.removeItem(this.addReplyLSKey)
      reply.processing = false;
    }, (err) => {
      reply.processing = false;
      reply.tryagain = true;
    });
  }

  retryAddComment(comment: any) {
    if (comment.processing) return;

    comment.processing = true;
    comment.tryagain = false;

    this.retryAddCommentEvent.emit(comment);

  }

  public replyMode(comments: any,comment?: any): any {
      if(comments && comments.responses)
      {
          comments.responses.map(x => {
              x.replyEnabled = false;
          });
      }
     
    comments.replyText = "";
    if(comment){
      comment.replyEnabled = true;
      comment.EditEnabled = false
    }
    
  }
 
  SubmitSubReply(comment, reply, ind?, sub_reply?, fakeComment: boolean = false) {
    if (fakeComment) {
      if (sub_reply.processing) return;

      sub_reply.processing = true;
      sub_reply.tryagain = false;
    }

    if (!reply || reply.replyText == "" || !reply.replyText) {

      this.toastr.ShowToastr('info',this.translation.vd_alert_plesae_enter_text);
      return;
    }
    let sessionData: any = this.headerService.getStaticHeaderData();
    let comment_time = moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS")
    let tmp_uuid = (sub_reply && sub_reply.uuid) ? sub_reply.uuid : `${new Date().getTime()}-${this.params.video_id}`;
    let obj = {
      parent_id: reply.id,
      account_id: sessionData.user_current_account.accounts.account_id,
      comment: reply.replyText,
      access_level: "nested",
      huddle_id: this.params.huddle_id,
      user_id: sessionData.user_current_account.User.id,
      first_name: sessionData.user_current_account.User.first_name,
      last_name: sessionData.user_current_account.User.last_name,
      company_name: sessionData.user_current_account.accounts.company_name,
      image: sessionData.user_current_account.User.image,
      uuid: tmp_uuid,
      fakeComment: true,
      created_at_gmt: comment_time
    };
   
    if (!reply.responses) reply.responses = [];
    if (!fakeComment) reply.responses.push(obj);

    reply.replyEnabled = false;
    reply.replyText = "";
    if (this.pageType == 'library-page') {
      obj['library'] = 1;
      }

    localStorage.removeItem(this.header_data.user_current_account.User.id + '_video_play_sub_reply_comment_' + reply.id);
    this.appMainService.AddReply(obj).subscribe((data: any) => {
      let lsData = localStorage.getItem(this.header_data.user_current_account.User.id + '_fake_sub_reply_comment_video_' + this.params.video_id);
      let dddata = JSON.parse(lsData);
      if (dddata) {
        dddata.forEach((x, ii) => {
          if (x.id == sub_reply.id) {
            dddata.splice(ii, 1)
          }
        });
      }
      localStorage.setItem(this.header_data.user_current_account.User.id + '_fake_sub_reply_comment_video_' + this.params.video_id, JSON.stringify(dddata))
      if (sub_reply) {
        sub_reply.processing = false;
        sub_reply.tryagain = true;
      }
    }, (err) => {
      if (fakeComment) {
        sub_reply.processing = false;
        sub_reply.tryagain = true;
      } else {
        let localdata: any = obj;
        localdata.id = this.minusIds - 1;
        localdata.tryagain = true;
        this.localsubReplyArry.push(localdata);
        if (ind != null && ind != undefined) {
          reply.responses[ind] = localdata;
        } else {
          localStorage.setItem(this.header_data.user_current_account.User.id + '_fake_sub_reply_comment_video_' + this.params.video_id, JSON.stringify(this.localsubReplyArry));
          if (reply.responses == undefined) {
            reply.responses = [];
          }
        }
        localStorage.removeItem(this.header_data.user_current_account.User.id + '_video_play_sub_reply_comment_' + reply.id)
      }
    });
  }
  ReSubmitSubReply(reply, ind, sub_reply) {
    if (sub_reply.processing) return;

    sub_reply.processing = true;
    if (!reply || reply.replyText == "" || !reply.replyText) {

      this.toastr.ShowToastr('info',this.translation.vd_alert_plesae_enter_text);
      return;
    }

    let sessionData: any = this.headerService.getStaticHeaderData();

    let obj = {
      parent_id: reply.id,
      account_id: sessionData.user_current_account.accounts.account_id,
      comment: reply.replyText,
      access_level: "nested",
      huddle_id: this.params.huddle_id,
      user_id: sessionData.user_current_account.User.id,
      first_name: sessionData.user_current_account.User.first_name,
      last_name: sessionData.user_current_account.User.last_name,
      company_name: sessionData.user_current_account.accounts.company_name,
      image: sessionData.user_current_account.User.image
    };

    reply.replyEnabled = false;
    localStorage.removeItem(this.header_data.user_current_account.User.id + '_video_play_sub_reply_comment_' + reply.id);
    if (this.pageType == 'library-page') {
      obj['library'] = 1;
      }
 
    this.appMainService.AddReply(obj).subscribe((data: any) => {
  
      if (data.status == "failed") { sub_reply.processing = false }
      for (let ii = 0; ii < this.comment.Comment.responses.length; ii++) {
        if (sub_reply.tryagain) {
          this.comment.Comment.responses[ii].responses.splice(ind, 1)
        }
      }
      let lsData = localStorage.getItem(this.header_data.user_current_account.User.id + '_fake_sub_reply_comment_video_' + this.params.video_id);
      let dddata = JSON.parse(lsData);
      if (dddata) {
        dddata.forEach((x, ii) => {
          if (x.id == sub_reply.id) {
            dddata.splice(ii, 1)
          }
        });
      }
      localStorage.setItem(this.header_data.user_current_account.User.id + '_fake_sub_reply_comment_video_' + this.params.video_id, JSON.stringify(dddata))
    }, (err) => sub_reply.processing = false);
  }
 
  retrySubComment(obj, reply) {
    if (this.pageType == 'library-page') {
      obj['library'] = 1;
      }
    this.appMainService.AddReply(obj).subscribe((data: any) => {
  
      reply.replyText = "";
      localStorage.removeItem(this.header_data.user_current_account.User.id + '_video_play_sub_reply_comment_' + reply.id);

    });
  }

  public editComment(comment) {
    
    this.headerService.hideTabRecordIcon(true);
    if (this.from == 'vobody' || this.from == 'sync-note' || this.fromLiveStream) this.onEdit.emit(comment);
    else this.videoPageService.changeEditCommentSource(comment);
  }

  public ResolveDelete(flag) {

    if (this.Inputs.ConfirmationKey != this.Inputs.Confirmation) {

			this.toastr.ShowToastr('info',this.translation.huddle_you_typed_in + " '" + this.Inputs.ConfirmationKey + "' ");
			return;
    } else {
      this.modalRef.hide();
      this.Inputs.Confirmation=null;
      if (flag == 1) {
        this.onDelete.emit(this.deletableComment);
      }
    }
 
  }
  public ResolveDeleteWithoutConfig(flag) {

  
    this.modalRef.hide();
    if (flag == 1) {
      this.onDelete.emit(this.deletableComment);
    }
    
  }

  public Seek(comment) {
    this.playerService.SeekTo({ time: comment.time, end_time: comment.end_time });
  }

  public getFormattedTime(time, end_time) {
    if(this.from == 'vobody'){
    
    if (time <= 0 || time == null) {
      let formattedTime = '';
      let sec_num: any = parseInt(this.comment.synchro_time, 10);
      let hours: any = Math.floor(sec_num / 3600);
      let minutes: any = Math.floor((sec_num - (hours * 3600)) / 60);
      let seconds: any = sec_num - (hours * 3600) - (minutes * 60);
      if (hours < 10) { hours = "0" + hours; }
      if (minutes < 10) { minutes = "0" + minutes; }
      if (seconds < 10) { seconds = "0" + seconds; }
      formattedTime += hours + ':' + minutes + ':' + seconds;
      return formattedTime;
    }

    }
    else{
      if (time <= 0 || time == null) return this.translation.vd_all_videos;
          }
    let formattedTime = '';
    let sec_num: any = parseInt(time, 10);
    let hours: any = Math.floor(sec_num / 3600);
    let minutes: any = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds: any = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    formattedTime += hours + ':' + minutes + ':' + seconds;

    if (end_time && end_time > 0 && end_time != time) {
      let sec_num: any = parseInt(end_time, 10);
      let hours: any = Math.floor(sec_num / 3600);
      let minutes: any = Math.floor((sec_num - (hours * 3600)) / 60);
      let seconds: any = sec_num - (hours * 3600) - (minutes * 60);

      if (hours < 10) { hours = "0" + hours; }
      if (minutes < 10) { minutes = "0" + minutes; }
      if (seconds < 10) { seconds = "0" + seconds; }
      formattedTime += ' - ' + hours + ':' + minutes + ':' + seconds;
    }

    return formattedTime;

  }

  public UpdateReply(comment, reply) {
    let comment_time = moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS")
    reply.created_at_gmt = comment_time
    this.retryUpdateReply(comment, reply)
  }
  public retryUpdateReply(comment, reply) {
    if (!reply.EditableText) {
      this.toastr.ShowToastr('info',this.translation.vd_alert_please_add_text);
    } else {
      reply.EditEnabled = false;
      if (this.from === 'video-page') reply.state = this.COMMENT_STATE.UPDATING;
      this.onReplyEdit.emit({ comment: comment, reply: reply });
    }
  }

  public AllowTags(default_tags) {
    if (Array.isArray(default_tags)) return default_tags.filter((dt) => { return dt.ref_type != 2; }).length > 0;
    else return false;
  }

  public Urlify(text) {
    if (text) {
      let urlRegex = /((?:(http|https|Http|Https|rtsp|Rtsp):\/\/(?:(?:[a-zA-Z0-9\$\-\_\.\+\!\*\'\(\)\,\;\?\&\=]|(?:\%[a-fA-F0-9]{2})){1,64}(?:\:(?:[a-zA-Z0-9\$\-\_\.\+\!\*\'\(\)\,\;\?\&\=]|(?:\%[a-fA-F0-9]{2})){1,25})?\@)?)?((?:(?:[a-zA-Z0-9][a-zA-Z0-9\-]{0,64}\.)+(?:(?:aero|arpa|asia|a[cdefgilmnoqrstuwxz])|(?:biz|b[abdefghijmnorstvwyz])|(?:cat|com|gle|coop|c[acdfghiklmnoruvxyz])|d[ejkmoz]|(?:edu|e[cegrstu])|f[ijkmor]|(?:gov|g[abdefghilmnpqrstuwy])|h[kmnrtu]|(?:info|int|i[delmnoqrst])|(?:jobs|j[emop])|k[eghimnrwyz]|l[abcikrstuvy]|(?:mil|mobi|museum|m[acdghklmnopqrstuvwxyz])|(?:name|net|n[acefgilopruz])|(?:org|om)|(?:pro|p[aefghklmnrstwy])|qa|r[eouw]|s[abcdeghijklmnortuvyz]|(?:tel|travel|t[cdfghjklmnoprtvwz])|u[agkmsyz]|v[aceginu]|w[fs]|y[etu]|z[amw]))|(?:(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])))(?:\:\d{1,5})?)(\/(?:(?:[a-zA-Z0-9\;\/\?\:\@\&\=\#\~\-\.\+\!\*\'\(\)\,\_])|(?:\%[a-fA-F0-9]{2}))*)?(?:\b|$)/gi;
      return text.replace(urlRegex, function (url) {
        let add_protocole = false;
        if (!/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig.test(url)) {
          add_protocole = true;
        }
        return add_protocole ? '<a href="' + "//" + url + '" target="_blank">' + url + '</a>' : '<a href="' + url + '" target="_blank">' + url + '</a>';
      })
    } else {
      return '';
    }

  }

  public getTimelineBg(comment) {

    if (Array.isArray(comment.default_tags)) {

      let ret = "transparent";

      comment.default_tags.forEach((dt) => {

        if (dt) {
          let index = _.findIndex(this.customMarkers, { account_tag_id: dt.account_tag_id });

          if (index > -1) {
            ret = this.classes[index];
          }
        }


      });

      return ret;

    } else {
      return "transparent";
    }

  }

  public FileClicked(file) {

    if (file.stack_url && file.stack_url != null) {

      // let path = environment.baseUrl + "/app/view_document" + file.stack_url.substring(file.stack_url.lastIndexOf("/"), file.stack_url.length);
      // window.open(path, "_blank");
      let path = "/home/document-commenting/pdf-renderer/workspace-video-page/" + file.id+ file.stack_url.substring(file.stack_url.lastIndexOf("/")) + "/" + file.file_type + "/" + file.account_folder_id + "/" + file.parent_folder_id + "/" + true
      window.open(path, "_blank");
      //home is added to the url to work on production. it will not work on localhost
    }
  }

  private checkIfAduioCommentIsPlayable(commentText: string) {
    if(this.appMainService.playableAudioExtensions) {
      let extensionExisted = this.appMainService.playableAudioExtensions.find(plExt => {
        let returnedRes = false;
        if (commentText.indexOf(`.${plExt}`) > -1) returnedRes = true;
        return returnedRes;
      });
      if (extensionExisted) {
        this.comment.playable = true;
  
        setTimeout(() => {
          if (this.audioPlayer) this.audioPlayer.nativeElement.load();
        }, 500);
      }
    }
  }

  private restoreCommentsFromLocalStorage() {
    this.comment.replyText = localStorage.getItem(this.addReplyLSKey);
    if (this.comment.Comment && this.comment.Comment.responses != undefined) {
      this.comment.Comment.responses.forEach(x => {
        if (localStorage.getItem(this.header_data.user_current_account.User.id + '_video_play_sub_reply_comment_' + x.id) != null) {
          x.replyText = localStorage.getItem(this.header_data.user_current_account.User.id + '_video_play_sub_reply_comment_' + x.id);
        }
      });
    }


    let localsData = localStorage.getItem(this.header_data.user_current_account.User.id + '_fake_reply_comment_video_' + this.params.video_id)
    if (localsData != null && localsData != undefined) {
      let data = JSON.parse(localsData)
      if (data) {
        data.forEach(x => {
          if (x && this.comment.id == x.parent_id) {
            this.Reply_tryagain = true;
            this.comment.Comment.responses.push(x);
          }
        });
      }
    }

    let localsubrData = localStorage.getItem(this.header_data.user_current_account.User.id + '_fake_sub_reply_comment_video_' + this.params.video_id)
    if (localsubrData != null && localsubrData != undefined) {
      let sdata = JSON.parse(localsubrData)
      if (sdata && Array.isArray(sdata)) {
        sdata.forEach(x => {
          this.comment.Comment.responses.forEach(p => {
            if (p.id == x.parent_id) {
              x.tryagain = true;
              p.responses.push(x);
            }
          });
        });
      }
    }

    /**Restoring try-again-edit-replies from localstorage start */
    const taEditReplies = this.headerService.getLocalStorage(this.taEditRepliesKey);
    if (Array.isArray(taEditReplies) && taEditReplies.length > 0) {
      taEditReplies.forEach(taEditReply => {
        if (this.comment.Comment.responses) {
          const taEditReplyIndex = this.comment.Comment.responses.findIndex(reply => reply.id == taEditReply.id);
          if (taEditReplyIndex > -1) this.comment.Comment.responses[taEditReplyIndex] = taEditReply;
        }
      });
    }
    /**Restoring try-again-edit-replies from localstorage end */

    /**Restoring try-again-edit-sub-replies from localstorage start */
    const taEditSubReplies = this.headerService.getLocalStorage(this.taEditSubRepliesKey);
    if (Array.isArray(taEditSubReplies) && taEditSubReplies.length > 0) {
      taEditSubReplies.forEach(taEditSubReply => {
        if (this.comment.Comment.responses) {
          this.comment.Comment.responses.forEach((replies, i) => {
            if (replies.responses) {
              const taEditSubReplyIndex = replies.responses.findIndex(subReply => subReply.id == taEditSubReply.id);
              if (taEditSubReplyIndex > -1) replies.responses[taEditSubReplyIndex] = taEditSubReply;
            }
          });
        }
      });
    }
    /**Restoring try-again-edit-sub-replies from localstorage end */
  }

  checkCsv(file){
    if (file.original_file_name.split('.')[1] == 'csv') {
      return true;
    } else return false;
    
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    if (this.modalRef) this.modalRef.hide();
  }

}
interface GeneralInputs {
	NewFolderName: string,
	Confirmation: string,
	ConfirmationKey: string
}
