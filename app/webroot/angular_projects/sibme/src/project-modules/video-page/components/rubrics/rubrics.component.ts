import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, OnChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import * as _ from "underscore";

import { HeaderService } from "@projectModules/app/services";
import { MainService, VideoPageService } from "@videoPage/services";
import { SelectedRubricInterface } from '@videoPage/interfaces';

@Component({
  selector: 'rubrics',
  templateUrl: './rubrics.component.html',
  styleUrls: ['./rubrics.component.css']
})
export class RubricsComponent implements OnInit, OnDestroy, OnChanges {

  @Input('data') rubrics;
  @Input('from') from;
  @Input('previewMode') previewMode;
  @Input() term;
  @Input('isCreater') isCreater;
  @Output() RubricClicked: EventEmitter<any> = new EventEmitter<any>();
  public currentRubric: any = {};
  public header_data;
  public translation: any = {};
  private subscriptions: Subscription = new Subscription();
  constructor(private mainService: MainService, private headerService: HeaderService, private videoPageService: VideoPageService) {

    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => this.translation = languageTranslation));
    this.subscriptions.add(this.videoPageService.selectedRubric$.subscribe((selectedRubric: SelectedRubricInterface) => {
      const index = this.rubrics.account_tag_type_0.findIndex(rubric => rubric.account_tag_id == selectedRubric.rubric.account_tag_id);
      if (index > -1) this.rubrics.account_tag_type_0[index].selected = selectedRubric.selected;
    }));

    this.subscriptions.add(this.mainService.ResetRubrics.subscribe(() => {
     if(this.rubrics && Array.isArray(this.rubrics.account_tag_type_0)) this.rubrics.account_tag_type_0.forEach((r) => { r.selected = false; });
    }))
  }

  ngOnInit() {
    if (this.rubrics) {
      if (!this.mainService.isEditComent && this.rubrics.account_tag_type_0) {
        this.rubrics.account_tag_type_0.forEach((r) => { r.selected = false; });
        this.mainService.isEditComent = false;
      }
      
    }

    this.header_data = this.headerService.getStaticHeaderData();
  }

  ngOnChanges(changes) {
    console.log(this.term);
    console.log(this.rubrics);
    
    if (changes.isCreater)
      this.isCreater = changes.isCreater.currentValue;
    else
      this.isCreater = true;
  }

  public getClass(rubric) {

    let classes = ["level_1", "level_2", "level_3", "level_4"];

    let plus = (this.from && this.from == 'tab' && this.currentRubric.account_tag_id == rubric.account_tag_id) ? " ActiveRubric" : "";

    let tabClass = (this.from && this.from == 'tab') ? " paddedRubric" : "";

    return classes[rubric.standard_level - 1] + plus + tabClass;

  }

  public RubricChecked(rubric) {
    this.mainService.UpdateRubricToComment(rubric);
  }

  public BroadcastClick(rubric) {
    if (this.from && this.from == 'tab') {
      if(rubric.hasOwnProperty('selectOptions'))  delete rubric.selectOptions;
      if(rubric.hasOwnProperty('selectedRating')) delete rubric.selectedRating;
      this.currentRubric = rubric;
      this.RubricClicked.emit(rubric);
    }

  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
