import { Component, OnInit, Input, Output, EventEmitter, TemplateRef, OnDestroy } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { HeaderService } from "@projectModules/app/services";
import { MainService, VideoPageService, PlayerService } from '@videoPage/services';
import { CommentTypingSettingsInterface } from "@videoPage/interfaces";
import { GLOBAL_CONSTANTS } from '@src/constants/constant';

@Component({
  selector: 'search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit, OnDestroy {

  @Input('customMarkers') customMarkers;
  @Input('classes') classes;
  @Output('settingsChanged') settingsChanged: EventEmitter<any> = new EventEmitter<any>();
  @Output('autoScrollChanged') autoScrollChanged: EventEmitter<any> = new EventEmitter<any>();
  @Input() params;
  @Input() VideoInfo;
  @Input() options;
  @Input() permissions;
  @Input() from;
  private videoPageType = GLOBAL_CONSTANTS.VIDEO_PAGE_TYPE;
  public searchData;
  public settings: CommentTypingSettingsInterface;
  private selectedTag;
  public modalRef: BsModalRef;
  public EmailData;
  private file;
  public is_workspace: boolean = false;
  public userCurrAcc: any;
  public translation: any = {};
  public isCinemaModeOn = false;
  private subscriptions: Subscription = new Subscription();
  pageType: string;
  placeholder: any;
  marker_tooltip=false;

  constructor(private toastr: ShowToasterService, private modalService: BsModalService, private headerService: HeaderService,
    private mainService: MainService, private activatedRoute: ActivatedRoute, private videoPageService: VideoPageService,
    private playerService: PlayerService) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
    this.translation = languageTranslation;
      this.placeholder = this.translation.vd_search_comments;

    }));
    this.subscriptions.add(this.videoPageService.commentTypingSettings$.subscribe(
      (commentTypingSettings: CommentTypingSettingsInterface) => this.settings = commentTypingSettings));

    this.activatedRoute.parent.data.subscribe(data => {
      if (data) {
        if (data.pageType === this.videoPageType.HUDDLE) {
          this.pageType = 'huddle-page';
          this.placeholder = this.translation.vd_search_comments;

        } else if (data.pageType === this.videoPageType.WORKSPACE) {
          this.is_workspace = true;
          this.pageType = 'workspace-page';
          this.placeholder = this.translation.vd_search_notes;


        } else if (data.pageType === this.videoPageType.LIBRARY) {
          this.pageType = 'library-page';
          this.placeholder = this.translation.vd_search_comments;
        }
      }
      this.settings.sortBy = 2;
    });

  }

  ngOnInit() {
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
    this.searchData = { text: localStorage.getItem(`${this.params.video_id}-searchDataText`), tag_id: localStorage.getItem(`${this.params.video_id}-LsTagId`) };
    this.EmailData = {};
    this.sortBy(2);
    this.initOptions();
    this.checkCinemaMode();
  }

  private initOptions() {
    if (!this.options) {
      this.options = { sorting: true, others: true, export: true };
    }
  }
  public textWidth(marker,val){

    const elem = document.getElementById("text-width"+val);

      if(elem.scrollWidth > 74) {
        marker.tooltipEnable=true;
      } else {
        marker.tooltipEnable=false;
      }
        
  }
  public searchTextChanged(ev) {
    localStorage.setItem(`${this.params.video_id}-searchDataText`, ev)
    if(this.selectedTag){
      this.mainService.TriggerSearchChange({ text: ev, tag_id: this.selectedTag.account_tag_id });
    } else {
      this.mainService.TriggerSearchChange({ text: ev });
    }
    
  }

  public ExportComments(to) {
    const obj = {
      video_id: this.params.video_id,
      huddle_id: this.params.huddle_id,
      account_id: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id,
      sort_by: this.FormatSortingOptions(this.settings.sortBy)
    }
    this.mainService.ExportComments(obj, to);
  }

  private FormatSortingOptions(flag) {
    return ["newest", "oldest", "timestamp"][flag];
  }

  public HandleFile(files, ref) {
    if (files[0].size > 2000000) {
      this.toastr.ShowToastr('info',this.translation.vd_file_to_large);
      ref.value = "";
      return;
    } else {
      this.file = files[0];
    }
  }
  public checkCinemaMode(){
    this.subscriptions.add(this.playerService._videoPlayerCinemaMode.subscribe((res) => {
      this.isCinemaModeOn = res;
      if(this.isCinemaModeOn && this.settings.autoscroll && this.pageType =="library-page"){
        
          this.ToggleAutoScroll();
      }
    }
      ));
  }
  public ResolveEmail(flag) {
    if (flag == 0) {
      this.EmailData = {};
      this.modalRef.hide();
      return;
    } else {
      if (this.EmailData.email.indexOf(",") > -1) {

        let valid = true;

        let arr = this.EmailData.email.split(",");

        arr.forEach((e) => {

          if (!this.isValidEmail(e)) {
            valid = false;
          }


        });

        if (!valid) {
          this.toastr.ShowToastr('info',this.translation.vd_please_provide);
          return;
        }

      } else {
        if (!this.isValidEmail(this.EmailData.email)) {
          this.toastr.ShowToastr('info',this.translation.vd_valid_email_address);
          return;
        }
      }

      if (!this.EmailData.subject) {

        this.toastr.ShowToastr('info',this.translation.vd_please_provide_email);
        return;

      }

      const obj = {
        huddle_id: this.params.huddle_id,
        video_id: this.params.video_id,
        account_id: this.userCurrAcc.accounts.account_id,
        email: this.EmailData.email,
        subject: this.EmailData.subject,
        message: this.EmailData.message ? this.EmailData.message : "",
        additional_attachemnt: this.file ? this.file : "",
        user_id: this.userCurrAcc.User.id,
        sort_by: this.FormatSortingOptions(this.settings.sortBy),
        current_lang: this.userCurrAcc.User.lang
      }
      this.modalRef.hide();
      this.mainService.SendEmail(obj).subscribe((data: any) => this.toastr.ShowToastr('info',data.message));
    }

  }

  private isValidEmail(email) {

    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());

  }

  public InitiateEmailDialog(template: TemplateRef<any>, file) {

    this.EmailData.email = this.VideoInfo.email;
    this.EmailData.subject = this.VideoInfo.title;

    this.modalRef = this.modalService.show(template, { class: "modal-md model-send-email" });
  }

  public sortBy(flag) {

    this.settingsChanged.emit({ sortBy: flag });
    const value = flag;
    this.videoPageService.updateCommentTypingSettings({ key: 'sortBy', value });

  }

  public ToggleAutoScroll() {
    const value = !this.settings.autoscroll;
    this.videoPageService.updateCommentTypingSettings({ key: 'autoscroll', value });
    this.autoScrollChanged.emit(this.settings.autoscroll);
    const obj = {
      user_id: this.userCurrAcc.User.id,
      account_id: this.userCurrAcc.accounts.account_id,
      value: Number(this.settings.autoscroll)
    };
    this.mainService.SaveAutoscrollSettings(obj).subscribe();
  }

  public SelectTag(tag) {

    let tag_id = -1;
    let lsTagid = localStorage.getItem(`${this.params.video_id}-LsTagId`);
    if (this.selectedTag == tag) {

      this.selectedTag = {};
    }
    else {
      this.selectedTag = tag;
      tag_id = tag.account_tag_id;
    }
    if (lsTagid != tag_id.toString()) {

      localStorage.setItem(`${this.params.video_id}-LsTagId`, JSON.stringify(tag_id))
    }
    else {
      localStorage.setItem(`${this.params.video_id}-LsTagId`, '-1')
    }
    this.mainService.TriggerSearchChange({ text: this.searchData.text, tag_id: tag_id })

  }

  public getBg(tag, index) {
    if (this.selectedTag == tag) return this.classes[index];
    if (localStorage.getItem(`${this.params.video_id}-LsTagId`) == tag.account_tag_id) return this.classes[index];
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    if (this.modalRef) this.modalRef.hide();
  }

}
