import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';

import { HeaderService } from "@app/services";
import { MainService, PlayerService, VideoPageService, ScrollService } from "@videoPage/services";

@Component({
  selector: 'video-page-transcription',
  templateUrl: './transcription.component.html',
  styleUrls: ['./transcription.component.css']
})
export class TranscriptionComponent implements OnInit, OnDestroy {

  @Input('showCopy') public showCopy: boolean;
  @Input('videoId') private videoId: number;
  @Input('video_url') private video_url: number;
  @Input('subtitles_data') public subtitles_data: any;
  @Input('currnetUser') public currnetUser: any;
  @Input('VideoCreatedBy') public VideoCreatedBy: any;
  @Input('huddle_type') public huddle_type: any;
  @Input('pageType') public pageType: any;
  @Input('huddleId') public huddle_id: any;
  public userId: any;
  public accountId: any;
  public huddleId: any;
  public header_data: any;
  public searchTranscription: string;
  public currentSubtitleText: string;
  public currentSubtitleId: string; // used to add class for auto-scrolling
  public userAccountLevelRoleId: number;
  public primery_button_color: string;
  public translation: any;
  private subscriptions: Subscription = new Subscription();

  constructor(private headerService: HeaderService, private playerService: PlayerService, private scrollService: ScrollService,
    private videoPageService: VideoPageService, private mainService: MainService, private toastr: ShowToasterService) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => this.translation = languageTranslation));
    this.subscriptions.add(this.videoPageService.videoCurrentTime$.subscribe(videoCurrentTime => (videoCurrentTime > 0) ? this.transcriptionAutoscroll(videoCurrentTime) : null)); // Only check for transcription Autoscroll in case video has started playing i.e videoCurrentTime > 0
  }

  ngOnInit() {
    this.header_data = this.headerService.getStaticHeaderData();
    this.userId = this.header_data.user_current_account.User.id;
    this.accountId = this.header_data.user_current_account.users_accounts.account_id;
    this.huddleId = this.huddle_id;
    this.primery_button_color = this.header_data.primery_button_color;
    this.userAccountLevelRoleId = this.header_data.user_permissions.roles.role_id;
  }

  public seekForTranscription(time) {
    this.playerService.SeekTo({ time: this.headerService.formatToSeconds(time), end_time: null });
  }

  public exportTranscriptions(urlParam: string) {
    const obj = {
      video_id: this.videoId,
      account_id: this.accountId,
      user_id: this.userId,
      huddle_id: this.huddleId
    }
    this.mainService.exportTranscriptions(obj, urlParam);
  }

  public enableEditMode(subtitle: any) {
    if (this.pageType == "library-page") {

      if (this.header_data.user_permissions.UserAccount.permission_video_library_upload == 1) {
        this.currentSubtitleText = subtitle.subtitles;
        this.subtitles_data.forEach(item => {
          if (item.id === subtitle.id) item.editMode = true;
          else item.editMode = false;
        });

      } else {
        return;
      }

    } else if (this.pageType == "workspace-page") {
      //if (this.VideoCreatedBy == this.currnetUser.id) {
        //if (this.header_data.user_permissions.UserAccount.permission_video_library_upload == 1) {
        // check removed after discussing with Saad, Asad 25-09-2020
        this.currentSubtitleText = subtitle.subtitles;
        this.subtitles_data.forEach(item => {
          if (item.id === subtitle.id) item.editMode = true;
          else item.editMode = false;
        });
        //}
      // } else {
      //   return;
      // }
    } else {
      if (this.currnetUser.huddle_role_id == 200 || (this.VideoCreatedBy == this.currnetUser.id && this.huddle_type != 3)) {
        //if (this.header_data.user_permissions.UserAccount.permission_video_library_upload == 1) {
        // check removed after discussing with Saad, Asad 25-09-2020
        this.currentSubtitleText = subtitle.subtitles;
        this.subtitles_data.forEach(item => {
          if (item.id === subtitle.id) item.editMode = true;
          else item.editMode = false;
        });
        //}
      } else {
        return;
      }
    }


  }

  public updateSubtitles(subtitle: any) {
    subtitle.isEditing = true;

    const data = {
      id: subtitle.id,
      document_id: subtitle.document_id,
      subtitles: this.currentSubtitleText,
      video_url: this.video_url,
      user_id: this.header_data.user_current_account.users_accounts.user_id
    };

    this.mainService.updateSubtitles(data).subscribe((res: any) => {
      this.toastr.ShowToastr('info',res.message);
      subtitle.subtitles = this.currentSubtitleText;
      subtitle.editMode = false;
      subtitle.isEditing = false;
    });
  }

  private transcriptionAutoscroll(videoCurrentTime: number) {

    if (this.subtitles_data) {

      const index = this.subtitles_data.findIndex(subTitle => this.headerService.formatToSeconds(subTitle.time_range.slice(0, 8)) == videoCurrentTime);

      if (index >= 0) {
        this.currentSubtitleId = this.subtitles_data[index].id;
        const transcriptId = `#transcript-${index}`
        this.scrollService.scrollTo("#slimscroll-container", transcriptId, 1);
      }
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

}
