import { Component, Input, Output, EventEmitter, ElementRef, OnDestroy, ViewChild, OnInit, AfterViewInit, SimpleChanges, OnChanges } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { trigger, style, animate, transition } from '@angular/animations';
import { Subscription, Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import * as _ from "underscore";

import { HeaderService, AppMainService } from "@projectModules/app/services";
import { MainService, PlayerService, VideoPageService } from '@videoPage/services';
import { PageType } from '@src/project-modules/app/interfaces';
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import { debug } from 'console';

declare var videojs: any;
type VideoCommentPlayState = 'on' | 'off';

@Component({
  selector: 'video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css'],
  animations: [
    trigger(
      'enterAnimation', [
      transition(':leave', [
        style({ transform: 'translateY(0)', opacity: 1 }),
        animate('500ms', style({ transform: 'translateY(100%)', opacity: 0 }))
      ])
    ]
    )
  ]
})
export class VideoPlayerComponent implements OnDestroy, OnInit, AfterViewInit, OnChanges {

  @ViewChild('vidoeTag', { static: true }) vidoeTag: ElementRef;
  @Output() ShowInfoBack = new EventEmitter<any>();
  @Output() IsPlaying: EventEmitter<any> = new EventEmitter<any>();
  @Output("VideoCurrentTime") VideoCurrentTime: EventEmitter<any> = new EventEmitter<any>();
  @Input('fromLiveStream') fromLiveStream: boolean;
  @Input('src') src;
  @Input("VideoInfo") VideoInfo;
  @Input("ShowInfo") ShowInfo;
  @Input() comments;
  @Input() colors;
  @Input() customMarkers;
  @Input() params;
  @Input() isLive;

  private videoPageType = GLOBAL_CONSTANTS.VIDEO_PAGE_TYPE;
  public commentExistedInLocalStorage: boolean;
  public player;
  private fullScreenActive: boolean = false;
  public header_data: any;
  private userCurrentAccount: any;
  public translation: any = {};
  private subscriptions: Subscription = new Subscription();
  private isCinemaMode = false;
  public isCinemaModeOn = false;
  public pageType: PageType;
  public changeStyle:any;

  private mediaForwardEventChange$ = new Subject<string>();
  private mediaBackwardEventChange$ = new Subject<string>();
  private mediaForwardButton: any;
  private mediaBackwardButton: any;
  private mediaForwardButtonTooltip: any;
  private mediaBackwardButtonTooltip: any;
  private fastForwardStarted: boolean = false;
  private fastBackwardStarted: boolean = false;
  private playBackRateBeforeFastForward: number = 1;
  private previousVideoCurrentTime: number;

  public videoCommentPlayState: VideoCommentPlayState = 'off';
  public videoCommentPlayTimeSlots: any = [];
  public videoCommentPlayIteration: number = 0;
  public videoCommentPlayOn: boolean = false;
  public startTime: any;
  public endTime: any;
  public isPlaying: boolean = false;
  public backward_interval: any = false;
  private selctedLibAccount=-1;
  public videoComments: any = [];

  intervalArray: any = [];
  private subTitleAlreadyAdded: boolean = false;

  private vjsTipStyleLeft: string;
  private vjsTipStyleObserver: any;
  private currentMarkerKey: string;

  constructor(
    private videoPageService: VideoPageService, private appMainService: AppMainService, private headerService: HeaderService,
    private mainService: MainService, private playerService: PlayerService,private activatedRoute: ActivatedRoute) {

    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => this.translation = languageTranslation));
    this.subscriptions.add(this.videoPageService.commentExistedInLocalStorage$.subscribe((value: boolean) => this.commentExistedInLocalStorage = value));
    this.subscriptions.add(this.mainService.ReRenderMarkers.subscribe((comment) => {
        let index = this.player.markers.getMarkers().findIndex(marker => marker.time === comment.time && marker.id === comment.id);
        if(index != -1) {
          this.player.markers.remove([index]);
        }
      if(comment.status === "success") {
      let index = this.player.markers.getMarkers().findIndex(marker => marker.id === comment.updated_comment.id);
      let updatedMarker =  [{
          backgroundColor: this.getCueBg(comment.updated_comment),
          time: comment.updated_comment.time,
          text: this.getComment(comment.updated_comment),
          id:comment.updated_comment.id
        }];
        this.player.markers.remove([index]);
        this.player.markers.add(updatedMarker);
      }

      //  this.AddCues();
    }));
    this.activatedRoute.parent.data.subscribe(data => {
      if (data) {
         if (data.pageType === this.videoPageType.LIBRARY) {
          this.pageType = 'library-page';
        }
      }
    });
    this.initiateFastForwardListener();
    this.initiateFastBackwardListener();
    this.initiateVideoCommentAutoplayListener();
    this.initiatePlayerListener();
  }

  ngOnInit() {
    this.header_data = this.headerService.getStaticHeaderData();
    this.userCurrentAccount = this.header_data.user_current_account;
    this.selctedLibAccount =this.headerService.selectedLibAcct;
    window.onbeforeunload = (e) => this.ngOnDestroy(e);
    this.checkCinemaMode();
  }
  ngOnChanges(changes: SimpleChanges){
    // if(this.ShowInfo) {
    //   this.player.markers.removeAll();
    // } else {
    //   this.AddCues();
    // }
  }
  FireEvent( Element, EventName )
  {
    if( Element != null )
    {
      if(Element.fireEvent)
      {
        Element.fireEvent( 'on' + EventName );
      }
      else
      {
        var evObj = document.createEvent( 'Events' );
        evObj.initEvent( EventName, true, true );
        Element.dispatchEvent( evObj );
      }
    }
  }
  ngAfterViewInit() {
    const obj = {
      video_id: this.params.video_id,
      huddle_id: this.params.huddle_id,
      account_id: this.userCurrentAccount.accounts.account_id,
      user_id: this.userCurrentAccount.User.id
    }

    let videoT = this.vidoeTag.nativeElement;
    this.player = videojs(videoT, {
      controls: true,
      autoplay: false,
      preload: false,
      fullscreenToggle: true,
      playbackRates: (!this.fromLiveStream) ? [0.5, 1, 1.25, 1.5, 2, 5] : []
    });
    this.player.one('loadedmetadata', () => this.videoPageService.updateVideoDuration(videoT.duration));
    this.player.on('fullscreenchange', () => this.toggleFullScreen());
    videojs.registerPlugin("markers", (<any>window).vjsMarkers);
    this.changeStyle = document.getElementsByClassName('vjs-break-overlay-text');
    this.addForwardAndBackwardControlInPlayer();

    this.player.src(this.src.path);
    this.player.type = this.src.type;
    this.player.markers({
      breakOverlay:{
        display: false,
        displayTime: 7,
        style:{
          'width':'100%',
          'height': '10%',
          'background-color': 'transparent',
          'visibility':'hidden'
       },
        text: function(marker) {
           return marker.text;
        }
     },
     onMarkerReached: (marker) => {
        if(this.fullScreenActive || this.isCinemaMode) {
          
          const element = document.querySelector("[data-marker-key='"+marker.key+"']");
          const toolbar = document.getElementById('mainVideo');

          this.currentMarkerKey = marker.key;

          if(!toolbar.classList.contains("vjs-user-active")) {
            toolbar.classList.remove("vjs-user-inactive");
            toolbar.classList.add("vjs-user-active");
          }

          if(element) this.FireEvent(element, 'mouseover');

          setTimeout(() => {
            // if(!toolbar.classList.contains("vjs-user-inactive"))
            // {
            //   toolbar.classList.remove("vjs-user-active");
            //   toolbar.classList.add("vjs-user-inactive");
            // }
            if(element && this.currentMarkerKey === marker.key) this.FireEvent(element, 'mouseout');
          },7000);
        }
     }
    });

    if(this.selctedLibAccount>-1){
      obj.account_id=this.selctedLibAccount;
    }
    if (!this.commentExistedInLocalStorage) {
      this.mainService.updateViewCount(obj).subscribe();
      this.player.play();
      if(this.VideoInfo.video_duration){
      this.player.duration= ()=> {
        return this.VideoInfo.video_duration; // the amount of seconds of video
      }
    }

    }

    let cuesAdded = false;
    let time = 0;
    let end_recorded = false;

    /** TODO: Will remove this setInterval code section and use videojs event emitters */
    this.intervalArray[0] = setInterval(() => {
      if (!this.player.paused() && this.player.currentTime() > 0) {
        time++;
        if (time % 5 == 0 && this.params) {
          const obj = {
            user_id: this.userCurrentAccount.User.id, 
            document_id: this.params.video_id, 
            minutes_watched: Math.round(this.player.currentTime()),
            playback_rate: this.player.playbackRate()
          };

          this.appMainService.LogViewerHistory(obj).subscribe();
        }
      }

    }, 1100);

    this.intervalArray[1] = setInterval(() => {
      if (this.player) {
        this.player.on("play", () => {
          this.commentExistedInLocalStorage = false;
        });

        this.player.on("ended", () => {
          if (!end_recorded && time % 5 != 0) {
            end_recorded = true;
            const obj = {
              user_id: this.userCurrentAccount.User.id, 
              document_id: this.params.video_id, 
              minutes_watched: Math.round(this.player.currentTime()),
              playback_rate: this.player.playbackRate()
            };

            this.appMainService.LogViewerHistory(obj).subscribe();
          }
        })

        if (this.player.currentTime() > 0 && !cuesAdded) {
          cuesAdded = true;
          this.AddCues();
        }
      }

      this.IsPlaying.emit(!this.player.paused());

    }, 100);

    let play_button: any = document.getElementsByClassName("vjs-big-play-button");
    play_button[0].onclick = () => {
      this.mainService.updateViewCount(obj).subscribe();
    }
    this.commentTimeSpanPlay();
    this.handleVideoCommentPlay();
    this.srtTrack();

    this.listenVjsTipStyleChange();
  }

  /**
   * Adding forward and backward controls in player and initializing the listerner for these buttons events
   */
  private addForwardAndBackwardControlInPlayer() {
    let playerBkClasses = 'oi player_bk';
    let playerFwdClasses = 'oi player_fwd';

    if (this.fromLiveStream) {
      playerBkClasses = 'fake-player-btn player_bk';
      playerFwdClasses = 'fake-player-btn player_fwd';
    }

    this.mediaBackwardButton = this.player.controlBar.addChild('button', {
      'el': videojs.createEl('button', {
        className: playerBkClasses,
        'role': 'button'
      }, { "data-glyph": "media-skip-backward" })
    });

    if (!this.fromLiveStream) {
      this.mediaBackwardButton.on('mouseup', (event) => this.mediaBackwardEventChange$.next(event.type));
      this.mediaBackwardButton.on('mousedown', (event) => this.mediaBackwardEventChange$.next(event.type));
      this.mediaBackwardButton.on('mouseleave', (event) => this.mediaBackwardEventChange$.next(event.type));
      this.mediaBackwardButton.on('mouseenter', (event) => this.mediaBackwardEventChange$.next(event.type));

      this.mediaBackwardButtonTooltip = this.player.controlBar.addChild('button', {
        'el': videojs.createEl('span', {
          className: 'media-backward-button-tooltip hide-media-backward-button-tooltip',
        }, {}, this.translation.click_and_hold_fast_backward)
      });
    }

    this.mediaForwardButton = this.player.controlBar.addChild('button', {
      'el': videojs.createEl('button', {
        className: playerFwdClasses,
        'role': 'button'
      }, { "data-glyph": "media-skip-forward" })
    });

    if (!this.fromLiveStream) {
      this.mediaForwardButton.on('mouseup', (event) => this.mediaForwardEventChange$.next(event.type));
      this.mediaForwardButton.on('mousedown', (event) => this.mediaForwardEventChange$.next(event.type));
      this.mediaForwardButton.on('mouseleave', (event) => this.mediaForwardEventChange$.next(event.type));
      this.mediaForwardButton.on('mouseenter', (event) => this.mediaForwardEventChange$.next(event.type));

      this.mediaForwardButtonTooltip = this.player.controlBar.addChild('button', {
        'el': videojs.createEl('span', {
          className: 'media-forward-button-tooltip hide-media-forward-button-tooltip',
        }, {}, this.translation.click_and_hold_fast_forward)
      });
    }
  }

  public AddCue(comment?) {
    if (this.player && this.player.markers) {
      if (comment.comment && comment.comment.indexOf("https://") > -1) {
        this.player.markers.add([{
          backgroundColor: this.getCueBg(comment),
          time: comment.time,
          text: this.getComment(comment,'Audio Comment'),
          id:comment.id
        }]);
      } else {
        this.player.markers.add([{
          backgroundColor: this.getCueBg(comment),
          time: comment.time,
          text: this.getComment(comment),
          id:comment.id
        }]);
      }
    }
  }
public getComment(comment,val?){
  let commentText;
  if(comment.comment) {
    commentText = comment.comment.trim();
    (commentText.length > 600) ? commentText = `${commentText.substring(0, 600)}...` : false;
  }

  if(val =='Audio Comment'){
    return `<span class="tooltip_vsjs"><img  src="assets/img/tooltip_time.svg" width="15" height="15">${this.getFormattedTime(comment.time, comment.end_time) } </span> <br> Audio Comment ` ;
  } else if(commentText){
    return `<span class="tooltip_vsjs"><img  src="assets/img/tooltip_time.svg" width="15" height="15">${this.getFormattedTime(comment.time, comment.end_time)} </span> <br> ${commentText}` ;
  } else{
    return `<span class="tooltip_vsjs"><img  src="assets/img/tooltip_time.svg" width="15" height="15">${this.getFormattedTime(comment.time, comment.end_time) } </span>` ;
  }
}

public getFormattedTime(time, end_time) {
  let formattedTime = '';
  let sec_num: any = parseInt(time, 10);
  let hours: any = Math.floor(sec_num / 3600);
  let minutes: any = Math.floor((sec_num - (hours * 3600)) / 60);
  let seconds: any = sec_num - (hours * 3600) - (minutes * 60);

  if (hours < 10) { hours = "0" + hours; }
  if (minutes < 10) { minutes = "0" + minutes; }
  if (seconds < 10) { seconds = "0" + seconds; }
  formattedTime += hours + ':' + minutes + ':' + seconds;

  if (end_time && end_time > 0 && end_time != time) {
    let sec_num: any = parseInt(end_time, 10);
    let hours: any = Math.floor(sec_num / 3600);
    let minutes: any = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds: any = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    formattedTime += ' - ' + hours + ':' + minutes + ':' + seconds;
  }

  return formattedTime;

}
  private AddCues() {
    // console.log("this.comments: ", this.comments);
    // this.videoComments = JSON.parse(JSON.stringify(this.comments));

    // let seen = {};
    // let timeInc = 0;
    // this.videoComments.some(function(currentObject) {
    //   if (seen.hasOwnProperty(currentObject.time)) {
    //     timeInc = timeInc + 1;
    //     console.log(timeInc);
    //     currentObject.time = currentObject.time + timeInc;

    //   } else {
    //     timeInc = 0;
    //   }
    //   return (seen[currentObject.time] = false);
    // });
    // console.log("videoComments ", this.videoComments)

    this.comments.forEach((c) => this.AddCue(c));
  }

  public getCueBg(comment) {
    if (Array.isArray(comment.default_tags) && comment.default_tags.length > 0) {
      let ret = "rgb(255, 35, 0)";
      comment.default_tags.forEach((dt) => {
        if (dt) {
          let index = _.findIndex(this.customMarkers, { account_tag_id: dt.account_tag_id });
          if (index > -1) ret = this.colors[index];
        }
      });
      return ret;
    } else {
      return "rgb(255, 35, 0)";
    }
  }

  public PlayPause(arg) {
    if (!this.player) {
      setTimeout(() => {
        if (arg == "pause") {
          this.player.pause();
        } else if (arg == "play") {
          this.player.play();
        }
      }, 1000);

    } else {
      if (arg == "pause") {
        this.player.pause();
      } else if (arg == "play") {
        this.player.play();
      }
    }

  }

  /** Comment auto play functionality starts */
  private disableVideoCommentPlay() {
    this.videoCommentPlayState = 'off';
    this.videoCommentPlayOn = false;
    this.videoCommentPlayIteration = 0;
    this.playerService.toggleVideoCommentPlay({ state: this.videoCommentPlayState });
  }

  private handleVideoCommentPlay() {

    this.player.on('timeupdate', (playerInstance: any) => {

      this.emitVideoCurrentTime(this.player.currentTime());

      if (!this.commentExistedInLocalStorage) this.VideoCurrentTime.emit(this.player.currentTime());

      if (this.videoCommentPlayState === 'on' && this.videoCommentPlayTimeSlots.length > 0) {
        if (!playerInstance.manuallyTriggered) {
          let currentTimeSlot = this.videoCommentPlayTimeSlots[this.videoCommentPlayIteration];

          if (!this.videoCommentPlayOn) {
            this.videoCommentPlayOn = true;
            this.player.currentTime(currentTimeSlot.start);
          }

          if (Math.floor(this.player.currentTime()) >= currentTimeSlot.end) {
            if (this.videoCommentPlayTimeSlots.length > (this.videoCommentPlayIteration + 1)) {
              this.videoCommentPlayIteration++;
              this.videoCommentPlayOn = false;
            } else {
              this.player.pause();
              this.disableVideoCommentPlay();
            }
          }

        } else {
          let currentSeekTime = Math.floor(playerInstance.target.player_.cache_.currentTime);
          let foundSlot = 0;
          for (let i = (this.videoCommentPlayTimeSlots.length - 1); i >= 0; i--) {
            if (currentSeekTime >= this.videoCommentPlayTimeSlots[i].start) {
              foundSlot = i;
              break;
            }
          }
          this.videoCommentPlayIteration = foundSlot;
          this.videoCommentPlayOn = false;
        }
      }
      if(this.fullScreenActive) {

      }

    });

  }
  /** Comment auto play functionality end */
  private commentTimeSpanPlay() {
    this.player.on('timeupdate', () => {
      if (this.isPlaying && this.endTime != null) {
        if (Math.floor(this.player.currentTime()) >= this.endTime) {
          this.player.pause();
          this.isPlaying = false;
        }
      }
    });

  }

  /** New code to emit video current time start */
  private emitVideoCurrentTime(videoCurrentTime: number) {
    videoCurrentTime = Math.floor(videoCurrentTime);
    if (!this.previousVideoCurrentTime) {
      this.previousVideoCurrentTime = videoCurrentTime;
      this.videoPageService.updateVideoCurrentTime(videoCurrentTime);
    } else if (videoCurrentTime !== this.previousVideoCurrentTime) {
      this.previousVideoCurrentTime = videoCurrentTime;
      this.videoPageService.updateVideoCurrentTime(videoCurrentTime);
    }
  }
  /** New code to emit video current time end */

  /**
   * Fast forward functionality
   * and hide/show tooltip for fast forward based on mouse events
   */
  private initiateFastForwardListener() {
    this.mediaForwardEventChange$.pipe(debounceTime(200)).subscribe((eventType: string) => {
      if (!this.fastForwardStarted && eventType === 'mouseenter') {
        this.mediaForwardButtonTooltip.removeClass('hide-media-forward-button-tooltip');
      } else if (!this.fastForwardStarted && eventType === 'mouseleave') {
        this.mediaForwardButtonTooltip.addClass('hide-media-forward-button-tooltip');
      } else if (!this.fastForwardStarted && eventType === 'mousedown') {
        this.playBackRateBeforeFastForward = this.player.playbackRate();
        this.player.playbackRate(this.playBackRateBeforeFastForward * 2);
        this.mediaForwardButton.addClass('btn-large');
        this.mediaForwardButtonTooltip.addClass('hide-media-forward-button-tooltip');
        this.fastForwardStarted = true;
      } else if (this.fastForwardStarted && (eventType === 'mouseup' || this.fastForwardStarted && eventType === 'mouseleave')) {
        this.player.playbackRate(this.playBackRateBeforeFastForward);
        this.mediaForwardButton.removeClass('btn-large');
        this.fastForwardStarted = false;
      }
    });
  }

  /**
   * Fast backward functionality
   * and hide/show tooltip for fast backward based on mouse events
   */
  private initiateFastBackwardListener() {
    this.mediaBackwardEventChange$.pipe(debounceTime(200)).subscribe((eventType: string) => {
      if (!this.fastBackwardStarted && eventType === 'mouseenter') {
        this.mediaBackwardButtonTooltip.removeClass('hide-media-backward-button-tooltip');
      } else if (!this.fastBackwardStarted && eventType === 'mouseleave') {
        this.mediaBackwardButtonTooltip.addClass('hide-media-backward-button-tooltip');
      } else if (!this.fastBackwardStarted && eventType === 'mousedown') {
        let time_backward = 575;
        if (this.player.playbackRate() == 1) {
          time_backward = 500;
        } else if (this.player.playbackRate() == 1.25) {
          time_backward = 425;
        } else if (this.player.playbackRate() == 1.5) {
          time_backward = 350;
        } else if (this.player.playbackRate() == 2) {
          time_backward = 275;
        } else if (this.player.playbackRate() == 5) {
          time_backward = 200;
        }

        if (!this.player.paused()) this.player.pause();

        this.backward_interval = setInterval(() => {
          this.player.currentTime(this.player.currentTime() - 1);
        }, time_backward);

        this.mediaBackwardButton.addClass('btn-large');
        this.mediaBackwardButtonTooltip.addClass('hide-media-backward-button-tooltip');
        this.fastBackwardStarted = true;

      } else if (this.fastBackwardStarted && (eventType === 'mouseup' || this.fastBackwardStarted && eventType === 'mouseleave')) {
        if (this.backward_interval) {
          clearInterval(this.backward_interval);
          this.player.play();
        }
        this.mediaBackwardButton.removeClass('btn-large');
        this.fastBackwardStarted = false;
      }
    });
  }

  private initiateVideoCommentAutoplayListener() {
    this.playerService.videoCommentPlay$.subscribe(data => {
      this.videoCommentPlayState = data.state;
      this.videoCommentPlayTimeSlots = data.timeSlots;
      if (this.videoCommentPlayState === 'off') {
        this.videoCommentPlayOn = false;
        this.videoCommentPlayIteration = 0;
      }
    });
  }


  private initiatePlayerListener() {

    this.playerService.PlayerPlayingState.subscribe((s) => this.PlayPause(s));
    this.mainService.CommentAddedNotification.subscribe((comment) => this.AddCue(comment));

    this.playerService.Seek.subscribe((comment) => {
      this.isPlaying = true;
      this.startTime = comment.time;
      this.endTime = (comment.end_time && comment.end_time > comment.time) ? comment.end_time : null;
      if (this.videoCommentPlayState === 'on') this.disableVideoCommentPlay();
      this.player.currentTime(this.startTime);
      this.player.play();
    });
    this.mainService.enable_subtitles.subscribe((file_path)=>{
      this.insertSubtitles(file_path);
  });
  }

  srtTrack()
  {
      let document_id = this.params.video_id;
      if(this.VideoInfo && this.VideoInfo.subtitle_vtt)
      {
        let subtitle_path = this.VideoInfo.subtitle_vtt;
          this.player.on('loadedmetadata', () => {
              this.insertSubtitles(subtitle_path);
          });
      }

  }
  insertSubtitles(subtitle_path) {
    if(!this.subTitleAlreadyAdded) {
      this.subTitleAlreadyAdded = true;
      this.player.addRemoteTextTrack({
          src: subtitle_path,
          srclang: 'en',
          label: 'english',
          kind: 'captions',
          mode: 'showing'
      }, true);
    }
  }
  private toggleFullScreen() {

    this.fullScreenActive = !this.fullScreenActive;

      // if (this.fullScreenActive) {
      //   this.changeStyle[0].style["opacity"] = 1;
      //   this.mediaBackwardButton.addClass('bk_full');
      //   this.mediaForwardButton.addClass('fw_full');
      //   this.player.controlBar.playToggle.addClass('play_adjusted');
      // } else {
      //   this.changeStyle[0].style["opacity"] = 0;
      //   this.mediaBackwardButton.removeClass('bk_full');
      //   this.mediaForwardButton.removeClass('fw_full');
      //   this.player.controlBar.playToggle.removeClass('play_adjusted');
      // }
  }

  cinemaModeOn(){
    this.isCinemaMode = !this.isCinemaMode;

      // if (this.isCinemaMode) {
      //   this.changeStyle[0].style["opacity"] = 1;
      //   this.mediaBackwardButton.addClass('bk_full');
      //   this.mediaForwardButton.addClass('fw_full');
      //   this.player.controlBar.playToggle.addClass('play_adjusted');
      // } else {
      //   this.changeStyle[0].style["opacity"] = 0;
      //   this.mediaBackwardButton.removeClass('bk_full');
      //   this.mediaForwardButton.removeClass('fw_full');
      //   this.player.controlBar.playToggle.removeClass('play_adjusted');
      // }
    this.videoPageService.setCinemaMode(this.isCinemaModeOn);
    this.playerService._videoPlayerCinemaMode.next(this.isCinemaMode);
  }
  checkCinemaMode(){
    this.subscriptions.add(this.playerService._videoPlayerCinemaMode.subscribe((res) => this.isCinemaModeOn = res));
  }

  /**
   * Listen for vjs-tip style change event and update style of vjs-tip-arrow according to vjs-tip style
   * In case of player dom has not laoded yet, this will work in interval until finds the vjs-tip dom and then
   * clear interval and listen for style changes
   *
   */
  private listenVjsTipStyleChange() {

    const oneSecInterval = setInterval(() => {
      const vjsTip = document.getElementsByClassName('vjs-tip')[0];
      if(vjsTip) {
        clearInterval(oneSecInterval);
        const config = { attributes: true};

        const callback = (mutationsList) => {
          const c = document.getElementsByClassName('vjs-tip-inner')[0];
           c.innerHTML = c.textContent;
          for(const mutation of mutationsList) {
            if(mutation.type === 'attributes') {
              if(this.vjsTipStyleLeft != mutation.target.style.left) {
                this.vjsTipStyleLeft = mutation.target.style.left;
                const vjsTipArrow:any = document.getElementsByClassName('vjs-tip-arrow')[0];
                vjsTipArrow.style.left = this.vjsTipStyleLeft;

              }
            }
          }
        };

        this.vjsTipStyleObserver = new MutationObserver(callback);
        this.vjsTipStyleObserver.observe(vjsTip, config);
      }
    }, 1000);
  }

  ngOnDestroy(e?) {
    this.player.pause();
    if (!e) {
      this.player.dispose();
    }
    this.intervalArray.forEach(element => {
      clearInterval(element);

    });
    this.subscriptions.unsubscribe();
    if(this.vjsTipStyleObserver) this.vjsTipStyleObserver.disconnect();
  }
}
