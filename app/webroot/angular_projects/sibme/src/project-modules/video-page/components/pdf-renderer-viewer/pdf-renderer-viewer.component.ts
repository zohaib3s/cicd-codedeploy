import { Component, ElementRef, OnInit, ViewChild, Renderer2, ViewChildren, QueryList, Input } from '@angular/core';
import { Subscription } from 'rxjs';

import { HeaderService } from '@app/services';
import { MainService } from "@videoPage/services";

import * as pdfjsLib from 'pdfjs-dist/build/pdf';
import { ToastrService } from 'ngx-toastr';

pdfjsLib.GlobalWorkerOptions.workerSrc = './assets/video-page/js/pdf.worker.min.js';
@Component({
  selector: 'sibme-pdf-renderer-viewer',
  templateUrl: './pdf-renderer-viewer.component.html',
  styleUrls: ['./pdf-renderer-viewer.component.css']
})
export class PdfRendererViewerComponent implements OnInit {

  @Input('currentSelectedFile') public currentSelectedFile: any;

  @ViewChild('spdfContent', { static: true }) spdfContent: ElementRef;
  @ViewChild('spdfBody', { static: true }) spdfBody: ElementRef;
  @ViewChild('spdfRenderer', { static: true }) spdfRenderer: ElementRef;
  @ViewChild('spdfCommentsWrapper', { static: true }) spdfCommentsWrapper: ElementRef;
  @ViewChild('spdfCanvasContainer', { static: true }) spdfCanvasContainer: ElementRef;
  @ViewChild('shadowCanvas', { static: true }) shadowCanvas: ElementRef;
  @ViewChild('spdfFirstHLine', { static: true }) spdfFirstHLine: ElementRef; // first horizontal line
  @ViewChild('spdfVLine', { static: true }) spdfVLine: ElementRef; // vertical line
  @ViewChild('spdfSecondHLine', { static: true }) spdfSecondHLine: ElementRef; // second horizontal line
  @ViewChildren('CommentsDom') commentsDom: QueryList<ElementRef>;

  @ViewChild('selectedrubricsTemplate', { static: false }) selectedrubricsTemplate;


  public loaded: boolean = false;
  public addReply: boolean = false;
  private pdfDoc = null;
  public totalPages: number = 0;
  public currentPage: number = 0;
  private desiredWidth = 600;
  public VideoHuddleFramework;

  public comments;
  public commentsList = [];
  public replyText: string;

  public viewPortHeight: number;

  private subscriptions: Subscription = new Subscription();
  public translations: any = {};
  public artifacts_id: any;
  public myPdfPath: any;
  public filestack_handle: any;
  public activeComment: any = {};
  public userAccountLevelRoleId: number | string = null;
  public activeBtn: number = 0;
  public customMarkers: any;
  public currentFrameworkId: any = null;
  public rubrics: any;
  public permissions: any = {};
  public artifactDataId: any = {};

  constructor(public toastr: ToastrService, public headerService: HeaderService, private mainService: MainService, private renderer: Renderer2) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => this.translations = languageTranslation));

    this.subscriptions.add(this.mainService.PushableRubric.subscribe((rubric: any) => {
      const activeComment = this.comments.id === this.activeComment.id;
      if (activeComment) {
        if (rubric.selected) this.activeComment.standards.push(JSON.parse(JSON.stringify(rubric)));
        else this.activeComment.standards = this.activeComment.standards.filter(rub => rubric.account_tag_id != rub.account_tag_id);
      }
    }));

  }

  ngOnInit() {
    this.loadPdfCanvas();
  }

  public loadPdfCanvas() {
    let path = "https://cdn.filestackcontent.com/output=f:pdf/pdfconvert=pageorientation:portrait,pageformat:a3/"
    if (this.currentSelectedFile.file_type == 'pdf') {
      path = "https://cdn.filestackcontent.com"
    }

    this.filestack_handle = this.currentSelectedFile.stack_url.substring(this.currentSelectedFile.stack_url.lastIndexOf("/"))

    this.myPdfPath = path + this.filestack_handle;
    pdfjsLib.getDocument(this.myPdfPath).promise.then((_pdfDoc) => {
      console.log(_pdfDoc);
      
      this.pdfDoc = _pdfDoc;
      this.totalPages = this.pdfDoc.numPages;

      this.renderPages(this.pdfDoc);
    });
  }

  private renderPages(pdfDoc) {
    for (let num = 1; num <= pdfDoc.numPages; num++) {
      pdfDoc.getPage(num).then(page => this.renderPage(page, num));
    }
    this.currentPage = 1;
    this.loaded = true;
  }

  /**
    * Get page info from document, resize canvas accordingly, and render page.
    * @param page Page
    * @param num Page number
  */
  private renderPage(page: any, num: number) {
    let viewport = page.getViewport({ scale: 1 });
    const scale = this.desiredWidth / viewport.width;
    viewport = page.getViewport({ scale: scale });

    const pdfPageContainer = this.renderer.createElement('div');
    const pdfPageAnnotator = this.renderer.createElement('div');
    const canvas = this.renderer.createElement('canvas');
    const ctx = canvas.getContext('2d');
    // Render PDF page into canvas context
    const renderContext = {
      canvasContext: ctx,
      viewport: viewport
    };

    this.renderer.setProperty(pdfPageContainer, 'id', `pdf-page-${num}`);
    this.renderer.setStyle(pdfPageContainer, 'margin-bottom', '10px');

    this.renderer.setProperty(pdfPageAnnotator, 'id', `pdf-page-${num}-annot`);
    this.renderer.setStyle(pdfPageAnnotator, 'height', `${Math.floor(viewport.height)}px`);
    this.renderer.setStyle(pdfPageAnnotator, 'width', `${Math.floor(viewport.width)}px`);

    this.renderer.setProperty(canvas, 'height', viewport.height);
    this.renderer.setProperty(canvas, 'width', viewport.width);
    this.renderer.setStyle(canvas, 'position', 'absolute');

    this.renderer.appendChild(pdfPageContainer, canvas);
    this.renderer.appendChild(pdfPageContainer, pdfPageAnnotator);
    this.renderer.appendChild(this.spdfCanvasContainer.nativeElement, pdfPageContainer);

    page.render(renderContext);

    if (num === 1) {
      this.viewPortHeight = Math.floor(viewport.height) + 10;
      this.renderer.setStyle(this.spdfRenderer.nativeElement, 'max-height', `${this.viewPortHeight}px`);
    }
  }

  /**
   * Navigate page to next or previous based on param
   * @param navigateTo {'next' | 'previous'}
   */
  public navigatePage(navigateTo: 'next' | 'previous') {
    if (navigateTo === 'next' && this.currentPage < this.totalPages) this.currentPage++;
    if (navigateTo === 'previous' && this.currentPage > 1) this.currentPage--;

    this.scrollToPage(this.currentPage, true);
  }

  public onScrollPdf() {
    this.currentPage = Math.round(this.spdfRenderer.nativeElement.scrollTop / this.viewPortHeight) + 1; // if half page scrolled, caculate as full page scroll
  }

  /**
   * 
   * @param pageNumber 
   * @param hideLineAnnot 
   */
  private scrollToPage(pageNumber: number, hideLineAnnot: boolean) {
    this.spdfRenderer.nativeElement.scrollTo(0, ((pageNumber - 1) * this.viewPortHeight)); // minus 1 from page because page numbering start from 1 and vertical scrolling start from 0
  }

  listIcons() {
    this.commentsList.forEach(comment => {

      const span = this.renderer.createElement('span');
      const img = this.renderer.createElement('img');
      this.renderer.setAttribute(img, 'src', `assets/img/text-comment.svg`);
      this.renderer.setAttribute(img, 'id', `${comment.id}-image`);

      this.renderer.addClass(span, 'absolute-icon');
      this.renderer.setStyle(span, 'left', `${comment.xPos}px`);
      this.renderer.setStyle(span, 'top', `${comment.yPos}px`);
      this.renderer.setAttribute(span, 'id', `${comment.id}-icon`);
      this.renderer.setAttribute(span, 'title', `${comment.text}`);
      this.renderer.appendChild(span, img);
      const annotElm = document.getElementById(`pdf-page-${comment.page}-annot`);
      console.log('annotElm: ', annotElm);
      this.renderer.appendChild(annotElm, span);
      console.log('span', span)
      console.log('renderer', this.renderer)

    });

  }

}