/**
 *********************************************************************************************************************
 ********************************************************************************************************************* 
 * This is for POC purpose, kinldy don't modify this component 
 *********************************************************************************************************************
 *********************************************************************************************************************
 */

import { Component, ElementRef, OnInit, ViewChild, Renderer2, ViewChildren, QueryList, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { HeaderService, AppMainService } from '@app/services';
import { VideoPageService, MainService } from "@videoPage/services";

import { GLOBAL_CONSTANTS } from "@constants/constant";
import { environment } from "@environments/environment";
import { CommentModel, CommentType } from "@videoPage/models";
import { BreadCrumbInterface } from '@app/interfaces';

// import {
//   PDFJSStatic,
//   PDFPageViewport,
//   PDFRenderTask,
//   PDFDocumentProxy,
//   PDFPageProxy,
// } from 'pdfjs-dist';

// const PDFJS: PDFJSStatic = require('pdfjs-dist');

interface DomPosition { xPos: number, yPos: number };
interface LinePosition { startPos: DomPosition, endPos: DomPosition };

import * as pdfjsLib from 'pdfjs-dist/build/pdf';

pdfjsLib.GlobalWorkerOptions.workerSrc = './assets/video-page/js/pdf.worker.min.js';
@Component({
  selector: 'sibme-pdf-renderer-poc',
  templateUrl: './pdf-renderer-poc.component.html',
  styleUrls: ['./pdf-renderer-poc.component.css']
})
export class PdfRendererPOCComponent implements OnInit {

  @ViewChild('spdfContent', { static: true }) spdfContent: ElementRef;
  @ViewChild('spdfBody', { static: true }) spdfBody: ElementRef;
  @ViewChild('spdfRenderer', { static: true }) spdfRenderer: ElementRef;
  @ViewChild('spdfCommentsWrapper', { static: true }) spdfCommentsWrapper: ElementRef;
  @ViewChild('spdfCanvasContainer', { static: true }) spdfCanvasContainer: ElementRef;
  @ViewChild('shadowCanvas', { static: true }) shadowCanvas: ElementRef;
  @ViewChild('spdfFirstHLine', { static: true }) spdfFirstHLine: ElementRef; // first horizontal line
  @ViewChild('spdfVLine', { static: true }) spdfVLine: ElementRef; // vertical line
  @ViewChild('spdfSecondHLine', { static: true }) spdfSecondHLine: ElementRef; // second horizontal line
  @ViewChildren('CommentsDom') commentsDom: QueryList<ElementRef>;

  @ViewChild('selectedrubricsTemplate', { static: false }) selectedrubricsTemplate;

  public loaded: boolean = false;
  private pdfDoc = null;
  public totalPages: number = 0;
  public currentPage: number = 0;
  private desiredWidth = 900;

  public currentAction: CommentType;
  private actionItemIconHeight: number = 25;
  private actionItemIconWidth: number = 25;

  public comments: CommentModel[] = [];

  public viewPortHeight: number;

  private videoPageType = GLOBAL_CONSTANTS.VIDEO_PAGE_TYPE;
  private pageType: string;
  private subscriptions: Subscription = new Subscription();
  public translations: any = {};
  private userName: string;

  public activeComment: any = {};
  public activeBtn: number = 0;
  public customMarkers: any = [
    {
      id: 1,
      tag_title: "Question",
      color: "#5db85b"
    },
    {
      id: 2,
      tag_title: "Good Idea",
      color: "#38a0ff"
    },
    {
      id: 3,
      tag_title: "Bright Spot",
      color: "#FFB308"
    },
    {
      id: 4,
      tag_title: "Light Spots",
      color: "#8145F2"
    }
  ];
  public rubrics: any = {
    "account_tag_type_0": [
      {
        "account_tag_id": 38480,
        "account_id": 2936,
        "parent_account_tag_id": null,
        "tag_type": "0",
        "tag_code": "1",
        "tag_title": "Thoughtful Work",
        "tag_html": "Thoughtful Work",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38480",
        "standard_analytics_label": "Thoughtful Work",
        "standard_level": 1,
        "standard_position": 1,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": []
      },
      {
        "account_tag_id": 38481,
        "account_id": 2936,
        "parent_account_tag_id": 38480,
        "tag_type": "0",
        "tag_code": "1.1.S",
        "tag_title": "Beginning Thoughtful Work (S) : Students demonstrate their learning by completing recall and retell tasks. Most tasks draw on memorization and focus on answering recall-type questions.",
        "tag_html": "Beginning Thoughtful Work (S) : Students demonstrate their learning by completing recall and retell tasks. Most tasks draw on memorization and focus on answering recall-type questions.",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38480\/38481",
        "standard_analytics_label": "Students demonstrate the",
        "standard_level": 2,
        "standard_position": 2,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": [
          {
            "id": 1834,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 1",
            "performance_level_rating": 1,
            "description": null,
            "site_id": 1
          },
          {
            "id": 1828,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 2",
            "performance_level_rating": 2,
            "description": "Global description for Performance Levels - ICLE Framework 1",
            "site_id": 1
          },
          {
            "id": 3368,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL Title 3",
            "performance_level_rating": 3,
            "description": null,
            "site_id": 1
          }
        ]
      },
      {
        "account_tag_id": 38482,
        "account_id": 2936,
        "parent_account_tag_id": 38480,
        "tag_type": "0",
        "tag_code": "1.1.I",
        "tag_title": "Beginning Thoughtful Work (I): Learning tasks include one assigned way for students to demonstrate their thinking.",
        "tag_html": "Beginning Thoughtful Work (I): Learning tasks include one assigned way for students to demonstrate their thinking.",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38480\/38482",
        "standard_analytics_label": "Learning tasks include o",
        "standard_level": 2,
        "standard_position": 3,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": [
          {
            "id": 1834,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 1",
            "performance_level_rating": 1,
            "description": null,
            "site_id": 1
          },
          {
            "id": 1828,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 2",
            "performance_level_rating": 2,
            "description": "Global description for Performance Levels - ICLE Framework 1",
            "site_id": 1
          },
          {
            "id": 3368,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL Title 3",
            "performance_level_rating": 3,
            "description": null,
            "site_id": 1
          }
        ]
      },
      {
        "account_tag_id": 38483,
        "account_id": 2936,
        "parent_account_tag_id": 38480,
        "tag_type": "0",
        "tag_code": "1.2.S.A",
        "tag_title": "Emerging Thoughtful Work (S.A): Students demonstrate their learning by completing tasks that require comprehension.",
        "tag_html": "Emerging Thoughtful Work (S.A): Students demonstrate their learning by completing tasks that require comprehension.",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38480\/38483",
        "standard_analytics_label": "Students demonstrate the",
        "standard_level": 2,
        "standard_position": 4,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": [
          {
            "id": 1834,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 1",
            "performance_level_rating": 1,
            "description": null,
            "site_id": 1
          },
          {
            "id": 1828,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 2",
            "performance_level_rating": 2,
            "description": "Global description for Performance Levels - ICLE Framework 1",
            "site_id": 1
          },
          {
            "id": 3368,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL Title 3",
            "performance_level_rating": 3,
            "description": null,
            "site_id": 1
          }
        ]
      },
      {
        "account_tag_id": 38484,
        "account_id": 2936,
        "parent_account_tag_id": 38480,
        "tag_type": "0",
        "tag_code": "1.2.S.B",
        "tag_title": "Emerging Thoughtful Work (S.B): There are opportunities for students to demonstrate mastery through learning tasks that require them to apply knowledge and comprehend content.",
        "tag_html": "Emerging Thoughtful Work (S.B): There are opportunities for students to demonstrate mastery through learning tasks that require them to apply knowledge and comprehend content.",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38480\/38484",
        "standard_analytics_label": "There are opportunities ",
        "standard_level": 2,
        "standard_position": 5,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": [
          {
            "id": 1834,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 1",
            "performance_level_rating": 1,
            "description": null,
            "site_id": 1
          },
          {
            "id": 1828,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 2",
            "performance_level_rating": 2,
            "description": "Global description for Performance Levels - ICLE Framework 1",
            "site_id": 1
          },
          {
            "id": 3368,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL Title 3",
            "performance_level_rating": 3,
            "description": null,
            "site_id": 1
          }
        ]
      },
      {
        "account_tag_id": 38485,
        "account_id": 2936,
        "parent_account_tag_id": 38480,
        "tag_type": "0",
        "tag_code": "1.2.I",
        "tag_title": "Emerging Thoughtful Work (I): Learning tasks include one or more assigned ways for students to demonstrate their thinking.",
        "tag_html": "Emerging Thoughtful Work (I): Learning tasks include one or more assigned ways for students to demonstrate their thinking.",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38480\/38485",
        "standard_analytics_label": "Learning tasks include o",
        "standard_level": 2,
        "standard_position": 6,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": [
          {
            "id": 1834,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 1",
            "performance_level_rating": 1,
            "description": null,
            "site_id": 1
          },
          {
            "id": 1828,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 2",
            "performance_level_rating": 2,
            "description": "Global description for Performance Levels - ICLE Framework 1",
            "site_id": 1
          },
          {
            "id": 3368,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL Title 3",
            "performance_level_rating": 3,
            "description": null,
            "site_id": 1
          }
        ]
      },
      {
        "account_tag_id": 38486,
        "account_id": 2936,
        "parent_account_tag_id": 38480,
        "tag_type": "0",
        "tag_code": "1.3.S.A",
        "tag_title": "Developed Thoughtful Work (S.A): Students demonstrate their learning by completing tasks that validate their ability to analyze, synthesize, and\/or evaluate new instructional content.",
        "tag_html": "Developed Thoughtful Work (S.A): Students demonstrate their learning by completing tasks that validate their ability to analyze, synthesize, and\/or evaluate new instructional content.",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38480\/38486",
        "standard_analytics_label": "Students demonstrate the",
        "standard_level": 2,
        "standard_position": 7,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": [
          {
            "id": 1834,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 1",
            "performance_level_rating": 1,
            "description": null,
            "site_id": 1
          },
          {
            "id": 1828,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 2",
            "performance_level_rating": 2,
            "description": "Global description for Performance Levels - ICLE Framework 1",
            "site_id": 1
          },
          {
            "id": 3368,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL Title 3",
            "performance_level_rating": 3,
            "description": null,
            "site_id": 1
          }
        ]
      },
      {
        "account_tag_id": 38487,
        "account_id": 2936,
        "parent_account_tag_id": 38480,
        "tag_type": "0",
        "tag_code": "1.3.S.B",
        "tag_title": "Developed Thoughtful Work (S.B): Tasks include the opportunity for students to respond to content through inquiry and interpretation.",
        "tag_html": "Developed Thoughtful Work (S.B): Tasks include the opportunity for students to respond to content through inquiry and interpretation.",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38480\/38487",
        "standard_analytics_label": "Tasks include the opport",
        "standard_level": 2,
        "standard_position": 8,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": [
          {
            "id": 1834,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 1",
            "performance_level_rating": 1,
            "description": null,
            "site_id": 1
          },
          {
            "id": 1828,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 2",
            "performance_level_rating": 2,
            "description": "Global description for Performance Levels - ICLE Framework 1",
            "site_id": 1
          },
          {
            "id": 3368,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL Title 3",
            "performance_level_rating": 3,
            "description": null,
            "site_id": 1
          }
        ]
      },
      {
        "account_tag_id": 38488,
        "account_id": 2936,
        "parent_account_tag_id": 38480,
        "tag_type": "0",
        "tag_code": "1.3.I",
        "tag_title": "Developed Thoughtful Work (I): Learning tasks allow students to self select options to best represent their thinking.",
        "tag_html": "Developed Thoughtful Work (I): Learning tasks allow students to self select options to best represent their thinking.",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38480\/38488",
        "standard_analytics_label": "Learning tasks allow stu",
        "standard_level": 2,
        "standard_position": 9,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": [
          {
            "id": 1834,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 1",
            "performance_level_rating": 1,
            "description": null,
            "site_id": 1
          },
          {
            "id": 1828,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 2",
            "performance_level_rating": 2,
            "description": "Global description for Performance Levels - ICLE Framework 1",
            "site_id": 1
          },
          {
            "id": 3368,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL Title 3",
            "performance_level_rating": 3,
            "description": null,
            "site_id": 1
          }
        ]
      },
      {
        "account_tag_id": 38489,
        "account_id": 2936,
        "parent_account_tag_id": 38480,
        "tag_type": "0",
        "tag_code": "1.4.S.A",
        "tag_title": "Well Developed Thoughtful Work (S.A): Students develop their own learning tasks that stretch their creativity, originality, design, or adaptation.",
        "tag_html": "Well Developed Thoughtful Work (S.A): Students develop their own learning tasks that stretch their creativity, originality, design, or adaptation.",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38480\/38489",
        "standard_analytics_label": "Students develop their o",
        "standard_level": 2,
        "standard_position": 10,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": [
          {
            "id": 1834,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 1",
            "performance_level_rating": 1,
            "description": null,
            "site_id": 1
          },
          {
            "id": 1828,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 2",
            "performance_level_rating": 2,
            "description": "Global description for Performance Levels - ICLE Framework 1",
            "site_id": 1
          },
          {
            "id": 3368,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL Title 3",
            "performance_level_rating": 3,
            "description": null,
            "site_id": 1
          }
        ]
      },
      {
        "account_tag_id": 38490,
        "account_id": 2936,
        "parent_account_tag_id": 38480,
        "tag_type": "0",
        "tag_code": "1.4.S.B",
        "tag_title": "Well Developed Thoughtful Work (S.B): Tasks include the opportunity for students to assess their own learning and move forward to adapt their knowledge to new activities.",
        "tag_html": "Well Developed Thoughtful Work (S.B): Tasks include the opportunity for students to assess their own learning and move forward to adapt their knowledge to new activities.",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38480\/38490",
        "standard_analytics_label": "Tasks include the opport",
        "standard_level": 2,
        "standard_position": 11,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": [
          {
            "id": 1834,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 1",
            "performance_level_rating": 1,
            "description": null,
            "site_id": 1
          },
          {
            "id": 1828,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 2",
            "performance_level_rating": 2,
            "description": "Global description for Performance Levels - ICLE Framework 1",
            "site_id": 1
          },
          {
            "id": 3368,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL Title 3",
            "performance_level_rating": 3,
            "description": null,
            "site_id": 1
          }
        ]
      },
      {
        "account_tag_id": 38491,
        "account_id": 2936,
        "parent_account_tag_id": 38480,
        "tag_type": "0",
        "tag_code": "1.4.I",
        "tag_title": "Well Developed Thoughtful Work (I): Learning tasks extend the learning of students, inspiring them to pursue self-discovery.",
        "tag_html": "Well Developed Thoughtful Work (I): Learning tasks extend the learning of students, inspiring them to pursue self-discovery.",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38480\/38491",
        "standard_analytics_label": "Learning tasks extend st",
        "standard_level": 2,
        "standard_position": 12,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": [
          {
            "id": 1834,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 1",
            "performance_level_rating": 1,
            "description": null,
            "site_id": 1
          },
          {
            "id": 1828,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 2",
            "performance_level_rating": 2,
            "description": "Global description for Performance Levels - ICLE Framework 1",
            "site_id": 1
          },
          {
            "id": 3368,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL Title 3",
            "performance_level_rating": 3,
            "description": null,
            "site_id": 1
          }
        ]
      },
      {
        "account_tag_id": 38492,
        "account_id": 2936,
        "parent_account_tag_id": null,
        "tag_type": "0",
        "tag_code": "2",
        "tag_title": "High-Level Questioning",
        "tag_html": "High-Level Questioning",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38492",
        "standard_analytics_label": "High-Level Questioning",
        "standard_level": 1,
        "standard_position": 13,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": []
      },
      {
        "account_tag_id": 38493,
        "account_id": 2936,
        "parent_account_tag_id": 38492,
        "tag_type": "0",
        "tag_code": "2.1.S.A",
        "tag_title": "Beginning High-Level Questioning (S.A): Students respond to questions that mainly focus on basic recall and retell.",
        "tag_html": "Beginning High-Level Questioning (S.A): Students respond to questions that mainly focus on basic recall and retell.",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38492\/38493",
        "standard_analytics_label": "Students respond to ques",
        "standard_level": 2,
        "standard_position": 14,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": [
          {
            "id": 1834,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 1",
            "performance_level_rating": 1,
            "description": null,
            "site_id": 1
          },
          {
            "id": 1828,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 2",
            "performance_level_rating": 2,
            "description": "Global description for Performance Levels - ICLE Framework 1",
            "site_id": 1
          },
          {
            "id": 3368,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL Title 3",
            "performance_level_rating": 3,
            "description": null,
            "site_id": 1
          }
        ]
      },
      {
        "account_tag_id": 38494,
        "account_id": 2936,
        "parent_account_tag_id": 38492,
        "tag_type": "0",
        "tag_code": "2.1.S.B",
        "tag_title": "Beginning High-Level Questioning (S.B): Few students ask questions, and most questions asked focus on basic recall or retelling of content.",
        "tag_html": "Beginning High-Level Questioning (S.B): Few students ask questions, and most questions asked focus on basic recall or retelling of content.",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38492\/38494",
        "standard_analytics_label": "Few students ask questio",
        "standard_level": 2,
        "standard_position": 15,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": [
          {
            "id": 1834,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 1",
            "performance_level_rating": 1,
            "description": null,
            "site_id": 1
          },
          {
            "id": 1828,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 2",
            "performance_level_rating": 2,
            "description": "Global description for Performance Levels - ICLE Framework 1",
            "site_id": 1
          },
          {
            "id": 3368,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL Title 3",
            "performance_level_rating": 3,
            "description": null,
            "site_id": 1
          }
        ]
      },
      {
        "account_tag_id": 38495,
        "account_id": 2936,
        "parent_account_tag_id": 38492,
        "tag_type": "0",
        "tag_code": "2.1.I",
        "tag_title": "Beginning High-Level Questioning (I): Lesson mainly includes questions at the recall and retell level, and\/or not all students are required to respond to each question.",
        "tag_html": "Beginning High-Level Questioning (I): Lesson mainly includes questions at the recall and retell level, and\/or not all students are required to respond to each question.",
        "created_by": 18737,
        "created_date": "2018-08-27 12:36:20",
        "last_edit_by": 18737,
        "last_edit_date": "2020-06-30 06:08:52",
        "tads_code": null,
        "framework_id": 38479,
        "framework_hierarchy": "\/38492\/38495",
        "standard_analytics_label": "Lesson mainly includes q",
        "standard_level": 2,
        "standard_position": 16,
        "source_standard_id": null,
        "site_id": 1,
        "account_framework_settings_performance_levels": [
          {
            "id": 1834,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 1",
            "performance_level_rating": 1,
            "description": null,
            "site_id": 1
          },
          {
            "id": 1828,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL title 2",
            "performance_level_rating": 2,
            "description": "Global description for Performance Levels - ICLE Framework 1",
            "site_id": 1
          },
          {
            "id": 3368,
            "account_id": 2936,
            "account_framework_setting_id": 1649,
            "performance_level": "PL Title 3",
            "performance_level_rating": 3,
            "description": null,
            "site_id": 1
          }
        ]
      }
    ],
    "account_framework_settings": {
      "id": 1649,
      "account_id": 2936,
      "account_tag_id": 38479,
      "published_at": "2020-12-17 06:52:50",
      "updated_at": "2020-12-17 06:52:39",
      "published": 1,
      "framework_name": "ICLE Framework 1",
      "enable_unique_desc": 1,
      "enable_ascending_order": 0,
      "enable_performance_level": 1,
      "tier_level": 3,
      "checkbox_level": 2,
      "parent_child_share": 1
    }
  };

  public bsModalRef: BsModalRef;

  constructor(public headerService: HeaderService, private appMainService: AppMainService, private videoPageService: VideoPageService, private mainService: MainService,
    private activatedRoute: ActivatedRoute, private renderer: Renderer2, private bsModalService: BsModalService) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => this.translations = languageTranslation));
    this.subscriptions.add(this.headerService.behaviorialHeaderData$.subscribe(headerData => this.userName = `${headerData.user_current_account.User.first_name} ${headerData.user_current_account.User.last_name}`));
    this.subscriptions.add(this.mainService.PushableRubric.subscribe((rubric: any) => {
      const activeComment = this.comments.find(comment => comment.id === this.activeComment.id);
      if (activeComment) {
        if (rubric.selected) this.activeComment.standards.push(JSON.parse(JSON.stringify(rubric)));
        else this.activeComment.standards = this.activeComment.standards.filter(rub => rubric.account_tag_id != rub.account_tag_id);
      }
    }));

    this.pageType = this.activatedRoute.parent.snapshot.data.pageType;
  }

  ngOnInit() {

    setTimeout(() => {
      const breadCrumbs: BreadCrumbInterface[] = [{ title: 'PDF Renderer', link: '' }];

      if (this.pageType === this.videoPageType.WORKSPACE) {
        breadCrumbs.unshift({ title: this.translations.header_list_worksapce, link: '/workspace/workspace/home/grid' });
      } else if (this.pageType === this.videoPageType.HUDDLE) {
        breadCrumbs.unshift({ title: this.translations.huddle_breadcrumb_huddle, link: '/video_huddles/list' });
      }

      this.appMainService.updateBreadCrumb(breadCrumbs);
    }, 1000);

    /**
     * Asynchronously downloads PDF.
     */
    pdfjsLib.getDocument(environment.localPdfFilePath).promise.then((_pdfDoc) => {
      this.pdfDoc = _pdfDoc;
      this.totalPages = this.pdfDoc.numPages;

      this.renderPages(this.pdfDoc);
    });
  }

  private renderPages(pdfDoc) {
    for (let num = 1; num <= pdfDoc.numPages; num++) {
      pdfDoc.getPage(num).then(page => this.renderPage(page, num));
    }
    this.currentPage = 1;
    this.loaded = true;

    // const comment = CommentModel.addComment('text-comment', 1, 'text-comment', 561, 1073, { name: this.userName });
    // const comment = CommentModel.addComment('audio-comment', 1, 'audio-comment', 561, 1073, { name: this.userName });
    // this.comments.push(comment);
    // this.activeComment = comment;
  }

  /**
    * Get page info from document, resize canvas accordingly, and render page.
    * @param page Page
    * @param num Page number
  */
  private renderPage(page: any, num: number) {
    let viewport = page.getViewport({ scale: 1 });
    const scale = this.desiredWidth / viewport.width;
    viewport = page.getViewport({ scale: scale });

    const pdfPageContainer = this.renderer.createElement('div');
    const pdfPageAnnotator = this.renderer.createElement('div');
    const canvas = this.renderer.createElement('canvas');
    const ctx = canvas.getContext('2d');
    // Render PDF page into canvas context
    const renderContext = {
      canvasContext: ctx,
      viewport: viewport
    };

    this.renderer.setProperty(pdfPageContainer, 'id', `pdf-page-${num}`);
    this.renderer.setStyle(pdfPageContainer, 'margin-bottom', '10px');

    this.renderer.setProperty(pdfPageAnnotator, 'id', `pdf-page-${num}-annot`);
    this.renderer.setStyle(pdfPageAnnotator, 'height', `${Math.floor(viewport.height)}px`);
    this.renderer.setStyle(pdfPageAnnotator, 'width', `${Math.floor(viewport.width)}px`);

    this.renderer.setProperty(canvas, 'height', viewport.height);
    this.renderer.setProperty(canvas, 'width', viewport.width);
    this.renderer.setStyle(canvas, 'position', 'absolute');

    this.renderer.appendChild(pdfPageContainer, canvas);
    this.renderer.appendChild(pdfPageContainer, pdfPageAnnotator);
    this.renderer.appendChild(this.spdfCanvasContainer.nativeElement, pdfPageContainer);

    page.render(renderContext);

    if (num === 1) {
      this.viewPortHeight = Math.floor(viewport.height) + 10;
      this.renderer.setStyle(this.spdfRenderer.nativeElement, 'max-height', `${this.viewPortHeight}px`);
    }
  }

  /**
   * Navigate page to next or previous based on param
   * @param navigateTo {'next' | 'previous'}
   */
  public navigatePage(navigateTo: 'next' | 'previous') {
    if(navigateTo === 'next' && this.currentPage < this.totalPages) this.currentPage++;
    if (navigateTo === 'previous' && this.currentPage > 1) this.currentPage--;

    this.scrollToPage(this.currentPage, true);
  }

  /**
   * 
   * @param pageNumber 
   * @param hideLineAnnot 
   */
  private scrollToPage(pageNumber: number, hideLineAnnot: boolean) {
    this.spdfRenderer.nativeElement.scrollTo(0, ((pageNumber - 1) * this.viewPortHeight)); // minus 1 from page because page numbering start from 1 and vertical scrolling start from 0
    if (hideLineAnnot) this.hideLineAnnot();
  }

  public changeActionBtn(actionBtn: CommentType) {
    this.currentAction = actionBtn;
  }

  public shadowCanvasClicked(event) {

    if (this.currentAction) {
      const clickPos = this.getParentPosition(event.currentTarget);
      let xPos = (event.clientX - clickPos.xPos) - (this.actionItemIconWidth / 2);
      let yPos = (event.clientY - clickPos.yPos - (this.actionItemIconHeight / 2)) + this.spdfRenderer.nativeElement.scrollTop;
      // let yPos = (event.clientY - clickPos.yPos - (this.actionItemIconHeight / 2)) + (this.spdfRenderer.nativeElement.scrollTop % this.viewPortHeight); // use this if you want to pdf-page-{pageId}-annot div to be relative
      xPos = (xPos < 0) ? 0 : xPos;
      yPos = (yPos < 0) ? 0 : yPos;

      console.log('clickPos.yPos: ', clickPos.yPos)
      console.log('event.clientY: ', event.clientY)
      console.log('this.spdfRenderer.nativeElement.scrollTop: ', this.spdfRenderer.nativeElement.scrollTop)
      console.log('this.viewPortHeight: ', this.viewPortHeight)
      console.log('this.spdfRenderer.nativeElement.scrollTop % this.viewPortHeight: ', this.spdfRenderer.nativeElement.scrollTop % this.viewPortHeight)
      console.log('yPos: ', yPos);

      const pageId = event.target.parentElement.id.split("pdf-page-")[1];
      const comment = CommentModel.addComment(this.currentAction, pageId, this.currentAction, xPos, yPos, { name: this.userName }, 'asad');

      const span = this.renderer.createElement('span');
      const img = this.renderer.createElement('img');
      this.renderer.setAttribute(img, 'src', `assets/img/${this.currentAction}.svg`);
      this.renderer.setAttribute(img, 'alt', this.currentAction);
      this.renderer.setAttribute(img, 'id', `${comment.id}-image`);

      this.renderer.addClass(span, 'absolute-icon');
      this.renderer.setStyle(span, 'left', `${xPos}px`);
      this.renderer.setStyle(span, 'top', `${yPos}px`);
      this.renderer.setAttribute(span, 'id', `${comment.id}-icon`);
      this.renderer.setAttribute(span, 'title', `${comment.text}`);

      this.renderer.appendChild(span, img);

      // this.renderer.appendChild(event.target, span);
      const annotElm = document.getElementById(`pdf-page-${pageId}-annot`);
      console.log('annotElm: ', annotElm);
      this.renderer.appendChild(annotElm, span);

      this.comments.push(comment);
      this.activeComment = comment;

      setTimeout(() => {
        const linePosition: LinePosition = this.getLinePos(comment.id);
        this.drawLine(linePosition);
      }, 500);

      this.currentAction = null;
    }
  }

  /**
   * Return parent element x and y position
   * @param element Target element
   */
  private getParentPosition(element: HTMLElement | any): DomPosition {
    let xPos = 0;
    let yPos = 0;

    while (element) {
      if (element.tagName == "BODY") {
        // deal with browser quirks with body/window/document and page scroll
        const xScroll = element.scrollLeft || document.documentElement.scrollLeft;
        const yScroll = element.scrollTop || document.documentElement.scrollTop;

        xPos += (element.offsetLeft - xScroll + element.clientLeft);
        yPos += (element.offsetTop - yScroll + element.clientTop);
      } else {
        // for all other non-BODY elements
        xPos += (element.offsetLeft - element.scrollLeft + element.clientLeft);
        yPos += (element.offsetTop - element.scrollTop + element.clientTop);
      }

      element = element.offsetParent;
    }
    return { xPos: xPos, yPos: yPos };
  }

  private getLinePos(commentId: number): LinePosition {
    const iconImageRect = document.getElementById(`${commentId}-image`).getBoundingClientRect();
    const commentRect = document.getElementById(`${commentId}-comment`).getBoundingClientRect();
    const spdfBodyRec = this.spdfBody.nativeElement.getBoundingClientRect();

    const startPos: DomPosition = { xPos: iconImageRect.left - spdfBodyRec.left + this.actionItemIconWidth, yPos: iconImageRect.top - spdfBodyRec.top + (this.actionItemIconHeight / 2) - 2 };
    const endPos: DomPosition = { xPos: commentRect.left - spdfBodyRec.left, yPos: commentRect.top - spdfBodyRec.top };

    return { startPos, endPos };
  }

  private drawLine(linePosition: LinePosition) {
    // console.log('startPos: ', linePosition.startPos);
    // console.log('endPos: ', linePosition.endPos);
    const horizontalDistance = linePosition.endPos.xPos - linePosition.startPos.xPos;
    // console.log('horizontalDistance: ', horizontalDistance);
    const oneForth = Math.round(horizontalDistance / 4);
    const thirdForth = Math.round(horizontalDistance - oneForth);

    // console.log('oneForth: ', oneForth);
    // console.log('thirdForth: ', thirdForth);

    const spdfFirstHLineCSSProps = {
      border: '1px solid',
      width: `${oneForth}px`,
      left: `${linePosition.startPos.xPos}px`,
      top: `${linePosition.startPos.yPos}px`
    };
    this.updateLineAnnotStyle(this.spdfFirstHLine.nativeElement, spdfFirstHLineCSSProps);

    let top: number;
    let height: number;
    if (linePosition.startPos.yPos > linePosition.endPos.yPos) {
      top = linePosition.endPos.yPos;
      height = linePosition.startPos.yPos - linePosition.endPos.yPos;
    } else {
      top = linePosition.startPos.yPos;
      height = linePosition.endPos.yPos - linePosition.startPos.yPos;
    }

    // console.log('top: ', top);
    // console.log('height: ', height);

    const spdfVLineCSSProps = {
      border: '1px solid',
      height: `${height}px`,
      left: `${linePosition.startPos.xPos + oneForth}px`,
      top: `${top}px`
    };
    this.updateLineAnnotStyle(this.spdfVLine.nativeElement, spdfVLineCSSProps);

    const spdfSecondHLineCSSProps = {
      border: '1px solid',
      width: `${thirdForth}px`,
      left: `${linePosition.endPos.xPos - thirdForth}px`,
      top: `${linePosition.endPos.yPos}px`
    };
    this.updateLineAnnotStyle(this.spdfSecondHLine.nativeElement, spdfSecondHLineCSSProps);

  }

  private updateLineAnnotStyle(lineElem: any, cssProps: any) {
    Object.entries(cssProps).forEach(([key, value]) => {
      this.renderer.setStyle(lineElem, key, value);
    });
  }

  private hideLineAnnot() {
    const spdfLineCSSProps = { border: 'none', width: 0, height: 0, left: 0, top: 0 };
    [this.spdfFirstHLine.nativeElement, this.spdfVLine.nativeElement, this.spdfSecondHLine.nativeElement].forEach(element => {
      this.updateLineAnnotStyle(element, spdfLineCSSProps);
    });

  }

  public selectMarker(comment: CommentModel, marker: any) {
    comment.marker = marker;
  }

  public updateCustomTags(tags: any) {
    const activeComment = this.comments.find(comment => comment.id === this.activeComment.id);
    if (activeComment) activeComment.tags = tags;
  }

  public unSelectRubric(comment: CommentModel, rubric: any, index: number) {
    comment.standards.splice(index, 1);
    this.videoPageService.updateSelectedRubric({ rubric, selected: false });
    if (comment.standards.length <= 0) this.bsModalRef.hide();
  }

  public cancelComment(comment: CommentModel, index: number, event: Event) {
    event.stopPropagation();
    this.activeComment = {};
    this.comments.splice(index, 1);
    this.hideLineAnnot();
    const icon = document.getElementById(`${comment.id}-icon`);
    icon.remove();

  }

  public addComment(comment: CommentModel, index: number, event: Event) {
    event.stopPropagation();
    this.hideLineAnnot();
    this.activeComment = {};
  }

  public opentagsModal() {
    this.bsModalRef = this.bsModalService.show(this.selectedrubricsTemplate, { class: "modal-md maxcls", backdrop: 'static' })
  }

  public onScrollPdf() {
    this.hideLineAnnot();
    this.currentPage = Math.round(this.spdfRenderer.nativeElement.scrollTop / this.viewPortHeight) + 1; // if half page scrolled, caculate as full page scroll
  }

  public onScrollComments() {
    this.hideLineAnnot();
  }

  public changeActiveComment(comment: CommentModel) {
    if(comment.id !== this.activeComment.id) {
      this.activeComment = comment;
      this.currentPage = comment.page; 
      this.scrollToPage(this.currentPage, false);
      setTimeout(() => {
        const linePosition: LinePosition = this.getLinePos(comment.id);
        this.drawLine(linePosition);
      }, 200);
    }
  }

}