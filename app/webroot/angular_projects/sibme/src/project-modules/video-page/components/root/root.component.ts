import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import { GLOBAL_CONSTANTS } from '@src/constants/constant';

@Component({
  selector: "video-page-root",
  templateUrl: "./root.component.html",
  styleUrls: ["./root.component.css"],
  encapsulation: ViewEncapsulation.None
})
export class RootComponent implements OnInit, OnDestroy {
  private videoPageType = GLOBAL_CONSTANTS.VIDEO_PAGE_TYPE;
  private projectTitle: string = 'video_details';
  private pageType;
  public showBreadCrumb = true;

  private styles = [
    { id: 'videoDetailGlobal', path: 'assets/video-page/css/global.css' },
    { id: 'videoDetailAnimate', path: 'assets/video-page/css/animate.min.css' },
    { id: 'videoDetailExport', path: 'assets/video-page/css/export.css' },
    { id: 'videoDetailVJSMarkers', path: 'assets/video-page/css/vjsmarkers.min.css' },
    { id: 'videoHuddleCustom2', path: 'assets/video-huddle/css/custom-2.css' },
  ];
  path: string;
  params: any;

  constructor(private activatedRoute: ActivatedRoute, private router: Router) {
      this.activatedRoute.data.subscribe(data => {
          if (data) {
              if (data.pageType === this.videoPageType.HUDDLE) {
                  this.pageType = 'huddle-page';
              } else if (data.pageType === this.videoPageType.WORKSPACE) {
                  this.pageType = 'workspace-page';
              } else if (data.pageType === this.videoPageType.LIBRARY) {
                  this.pageType = 'library-page';
              }
              if (document.location.href.indexOf('/canvas/') >= 0){
                this.showBreadCrumb = false;
              }
          }
      });
  }

  ngOnInit() {
    if(this.pageType == 'library-page' )
    {
        this.styles.push({ id: 'videoDetailLibrary', path: 'assets/video-page/css/library.css' });
    }
    if(document.location.href.indexOf('/canvas/') >= 0){
      this.styles.push({ id: 'videoDetailLibrary', path: 'assets/video-page/css/library.css' });
      this.styles.push({ id: 'videoDetailCanvas', path: 'assets/video-page/css/canvas.css' });
    }
    this.styles.forEach(style => {
      this.loadCss(style);
    });
  }

  ngOnDestroy() {
    this.styles.forEach(style => {
      let element = document.getElementById(style.id);
      element.parentNode.removeChild(element);
    });
  }

  private loadCss(style: any) {
    let head = document.getElementsByTagName('head')[0];
    let link = document.createElement('link');
    link.id = style.id;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = style.path;
    head.appendChild(link);
  }


}
