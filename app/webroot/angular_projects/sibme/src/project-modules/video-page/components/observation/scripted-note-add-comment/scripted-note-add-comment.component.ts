import { Component, OnInit, Input, Output, EventEmitter, OnChanges, OnDestroy, ViewChild } from '@angular/core';
import { MainService } from "@videoPage/services";
import { HeaderService } from "@projectModules/app/services";
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import * as _ from "underscore";
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import { MARKER_COLORS } from '@videoPage/constants';
@Component({
  selector: 'scripted-note-add-comment',
  templateUrl: './scripted-note-add-comment.component.html',
  styleUrls: ['./scripted-note-add-comment.component.css']
})
export class ScriptedNoteAddCommentComponent implements OnInit, OnDestroy, OnChanges {

  public newComment:any={};
  private params;
  public header_color;
  public primery_button_color;
  public secondry_button_color;
  public header_data;
  public translation: any = {};
  private subscription: Subscription;
  public videoOptions: any = {
    maxFiles: 20,
    accept: GLOBAL_CONSTANTS.RESOURCE_UPLOAD_EXTENSIONS,
    fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'url', 'onedrive'],
    customText: {
      'File {displayName} is not an accepted file type. The accepted file types are {types}': 'File {displayName} is not an accepted file type. The accepted file types are image, text',
    }
  };

  @Input() permissions;
  @Input() selectedRubrics;
  @Input() rubrics;
  @Input() settings;
  @Input() presetTags;
  @Input() text;
  @Input() EditMode;
  @Input() selectedFiles:any;
  @Input() huddleCradentials;
  @Input() timerStatus='stopped';
  @Input() isTimerStart;
  @Input() isREcordingOn;
  @Input() customMarkers;
  @Input() currentTab;
  @Output() customTags = new EventEmitter<any>();
  @Output() onTabSelection = new EventEmitter<any>();
  @Output() onRubricDelete = new EventEmitter<any>();
  @Output() onAdd = new EventEmitter<any>();
  @Output() onEdit = new EventEmitter<any>();
  @Output() TypingStarted = new EventEmitter<any>();
  @Output() commentText = new EventEmitter<any>();
  @Output() updateFilesCount = new EventEmitter<any>();
  @Output() chooseCustomTag = new EventEmitter<any>();
  @ViewChild('selectedrubricsTemplate', { static: false }) selectedrubricsTemplate;
  public modalRef: BsModalRef;
  // private selectedTag: any = {};
  @Input() selectedTag: any = {};
  public markerColors: string[] = MARKER_COLORS;
  DeletableFile: any={};
  commentTextForModal=""
  commentTextForModalAdd: string="";
  active_class: number;
  ngOnChanges(change) {
    console.log("this.currentTab",this.currentTab);
    console.log(this.isREcordingOn)
     
    if(this.currentTab == 1 && this.active_class!= 2 && this.isREcordingOn == true) {
      this.active_class= 2;
    }
    else if(this.active_class != 3 && this.currentTab != 1){
      console.log("this.active_class)",this.active_class);
      this.active_class=0;
    }
    
    if (change.text && change.text.currentValue) {
      if (!this.newComment) this.newComment = {};
      this.newComment.commentText = change.text.currentValue;
      this.commentTextForModal=this.newComment.commentText;
      localStorage.setItem('edit_comment',this.commentTextForModal)
    }

    if(this.isREcordingOn)
    localStorage.setItem('live_edit_comment',this.commentTextForModal)
  

  }

  constructor(public mainService: MainService,  private bsModalService: BsModalService,private toastr: ShowToasterService, private headerService: HeaderService) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
  }

  ngOnInit() {
    let sessionData: any = this.headerService.getStaticHeaderData();

    this.header_data = this.headerService.getStaticHeaderData();
    this.header_color = sessionData.header_color;
    this.primery_button_color = sessionData.primery_button_color;
    this.secondry_button_color = sessionData.secondry_button_color;

    this.initVars();

    if (this.params)
      this.newComment.commentText = localStorage.getItem('video_play_synced_comment_' + this.params.video_id);
    if (this.EditMode) this.newComment.commentText = localStorage.getItem('video_play_synced_edit_comment_' + this.params.video_id);
    this.mainService.ResetForm.subscribe((d) => {

      this.ResetForm();

    });
  }
  public opentagsModal() {
    this.modalRef = this.bsModalService.show(this.selectedrubricsTemplate,{ class: "modal-md  maxcls", backdrop: 'static' })
  }
  private ResetForm() {

    this.newComment.commentText = "";
    this.commentTextForModal="";
    this.commentTextForModalAdd=""
    this.newComment.files = [];
    this.mainService.ResetCustomTags();
    this.mainService.ResetSelectedRubrics();
    this.selectedRubrics.forEach((r)=>{r.selected=false;});

    this.EditMode = false;

  }

  private initVars() {


    if (!this.newComment) {
      this.newComment = {};
    }

  }

  public AddTextComment() {
      if(!this.commentTextForModalAdd){

        if(!this.commentTextForModal)
      this.toastr.ShowToastr('info',`${this.translation.vd_pleaseentersometexttocomment}`);
      return;

    }
    let obj = {
      text: this.newComment.commentText=this.commentTextForModalAdd,
      files: this.newComment.files
    }
    this.onAdd.emit(obj);
    this.active_check(0);
  }
  active_check(val){
    console.log(val);
    if(val == this.active_class)
    {
      this.active_class=0;
      this.ActivateTab(0);
    }
    else if(val ==1 ){
      this.ActivateTab(0);
      this.active_class=val;
    }
    else if(val ==2 ){
        this.active_class=val;
        this.ActivateTab(1);
      }
      else if(val ==3 ){
        this.ActivateTab(0);
        this.active_class=val;
      }
      else if(val ==0 ){
        this.ActivateTab(0);
        this.active_class=0;
      }
  
  }

  public getMarkerBg(tag, index) {
    if (this.selectedTag == tag) {
      return this.markerColors[index];
     } 
    else {
      return "transparent";
    }
  }
  public ChooseCustomTag(tag) {

    if (this.selectedTag == tag) {
      this.selectedTag = {};
    } else {
      this.selectedTag = tag;
    }
    this.chooseCustomTag.emit(this.selectedTag);
  }
  public textWidth(marker,val){
    const elem = document.getElementById("text-width"+val);
   // console.log( "width" ,elem.offsetWidth);
   //  console.log( "widthscroll" ,elem.scrollWidth)
      if(elem.scrollWidth > 74) {
        console.log(true);
        marker.tooltipEnable=true;
      }
      else{
        console.log(false);
        marker.tooltipEnable=false;
      }
        
  }
  public SetCustomTags(tags) {

    this.customTags.emit(tags);

  }

  public RemoveSelectedRubric(r, i) {

    this.onRubricDelete.emit({ rubric: r, index: i });

  }

  public ActivateTab(tab_id) {

    this.onTabSelection.emit(tab_id);

  }

  public EditTextComment(flag) {
let text=localStorage.getItem('edit_comment');
let text_live=localStorage.getItem('live_edit_comment');

    if (flag == 0 ) {
      this.mainService.isEditComent = false;
      if(text!==this.commentTextForModal){
        this.commentTextForModal=text || "";
      }
      if(this.isREcordingOn)
      this.commentTextForModalAdd=""
      this.resetFormInCaseEdit()
      this.onEdit.emit(false);
      return;

    }
    this.active_check(0);
    console.log('flag '+flag);
    
    let obj={ flag: flag, text: this.newComment.commentText=this.commentTextForModal,files:this.newComment.files }
    if(this.rubrics)
     this.rubrics.account_tag_type_0.forEach((r)=>{r.selected=false;});
    this.onEdit.emit(obj);    
  }

  public resetFormInCaseEdit(){
    this.mainService.ResetCustomTags();
    this.mainService.ResetSelectedRubrics();
    this.selectedRubrics.forEach((r)=>{r.selected=false;});
  }
  public TriggerTextChange(ev) {

    if (ev.keyCode == 13 && this.settings.EnterToPost) {

      ev.preventDefault();

      this.AddTextComment();

    } else {

      let context = this;
      setTimeout(() => {

        context.commentText.emit(context.newComment.commentText);

      }, 50);

    }

    if (!this.newComment.commentText || this.newComment.commentText.length == 0) {
      this.TypingStarted.emit(true);
    }
    if (!this.commentTextForModal || this.commentTextForModal.length == 0) {
      this.TypingStarted.emit(true);
    }
    if (!this.commentTextForModalAdd || this.commentTextForModalAdd.length == 0) {
      this.TypingStarted.emit(true);
    }

  }

  public AppendNewFiles(obj) {
    console.log(obj);
    
    if (obj.from == 'comment') {
      if (this.newComment.files)
        this.newComment.files = this.newComment.files.concat(obj.files);
    
      else {
        this.newComment.files = [];
        this.newComment.files = this.newComment.files.concat(obj.files);

      }

      this.selectedFiles= [...this.selectedFiles,...obj.files];


    }


  }
  public RemoveFileFromComment(file, i) {
    let index = _.findIndex(this.selectedFiles, {url: file.url});
    
    let index2 = _.findIndex(this.newComment.files, {url: file.url});
    if(index2 >-1){
    this.newComment.files.splice(i, 1);
    }


    if(index >-1){

      let subIndex = _.findIndex(this.selectedFiles, {url:file.url});
      if(file.id)
      this.ResolveDeleteFile(1,file.id);
      else
      this.selectedFiles.splice(index, 1);


    }


  }

  public ResolveDeleteFile(flag, id?){
    let sessionData = this.headerService.getStaticHeaderData();
      let obj = {
        huddle_id:this.huddleCradentials.huddle_id,
        video_id: this.huddleCradentials.video_id,
        document_id:this.DeletableFile.id?this.DeletableFile.id:id,
        user_id: this.header_data.user_current_account.User.id,
      }

      this.mainService.DeleteResource(obj).subscribe((data:any)=>{
         
            let index = _.findIndex(this.selectedFiles, {id: id? id: this.DeletableFile.id});
            if(index >-1){
              this.selectedFiles.splice(index, 1);
              this.updateCount()
            }



      });

    

  }

  updateCount(){
    this.updateFilesCount.emit(this.selectedFiles)
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


}
