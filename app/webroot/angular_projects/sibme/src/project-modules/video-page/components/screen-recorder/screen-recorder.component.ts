import { Component, OnInit, OnDestroy, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ShowToasterService } from '@projectModules/app/services';
import { ModalOptions, BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { HeaderService, AppMainService, HomeService } from '@app/services';
import { FilestackService, MainService } from '@videoPage/services';
import { GLOBAL_CONSTANTS } from "@constants/constant";
import { ScreenRecordingType, ScreenRecordingUrl } from "@app/types";
import { BreadCrumbInterface } from '@app/interfaces';

type BrowserAgent = 'opera' | 'firefox' | 'safari' | 'ie' | 'edge' | 'chrome' | 'chromium';

declare var document: any;
declare var MediaRecorder: any;
declare var navigator: any;
declare var opr: any;
declare var InstallTrigger: any;
declare var safari: any;
// declare var blob: any;

@Component({
  selector: 'video-page-screen-recorder',
  templateUrl: './screen-recorder.component.html',
  styleUrls: ['./screen-recorder.component.css']
})
export class ScreenRecoderComponent implements OnInit, OnDestroy {

  private videoPageType = GLOBAL_CONSTANTS.VIDEO_PAGE_TYPE;
  private screenRecordingUrl: ScreenRecordingUrl;
  private pageType: string;
  public micCamPermError: boolean = false;
  public timeBegan = null;
  public timeStopped: any = null;
  public stoppedDuration: any = 0;
  public started = null;
  public videoRecordTime: number;
  public timer: number = 3;
  public playTimer: boolean = false;
  public interval;
  private subscriptions: Subscription = new Subscription();
  public translation: any = {};
  public video_name = '';
  public permission = true;
  public permissionLoading = false;
  public cam_and_mic_perm = true;
  public window_permission = true;
  public browserSupportedRecording: boolean = false;
  private streamChunks: any = [];
  private availableAudioSrc = [];
  private selectedMicrophones: any;
  private cameraStream: any;
  public uploadProgress: any = 0;
  public modalRef: BsModalRef;
  public header_data: any;
  public is_cancel = false;
  private screenRecordingCamContainer: any; // It will be used as hidden cam view to show picture in picture view to user
  public recordingType: ScreenRecordingType;
  public browserAgent: BrowserAgent;
  private huddleId: number = null;
  private huddleName: string = '';
  private folderId: any;
  public blob: any;
  public screenStream;
  public goalEvidence;
  // private mute = false;

  @ViewChild('sharedScreenPreview', { static: false }) sharedScreenPreview: ElementRef;
  @ViewChild('camView', { static: false }) camView: ElementRef;
  @ViewChild('uploadStreamModal', { static: true }) uploadStreamModal;
  @ViewChild('cancelModal', { static: true }) cancelModal;
  newAudioStream: any;
  deviceCheckForSecondTime: boolean = false;
  screenRecordingState: string;
  mainScreenStream: any;

  constructor(
    public headerService: HeaderService,
    private appMainService: AppMainService,
    private activatedRoute: ActivatedRoute,
    private filestackService: FilestackService,
    private modalService: BsModalService,
    private router: Router,
    private toastr: ShowToasterService,
    public mainService: MainService,
    public windowRef: Window,
    public homeService: HomeService,
    private changeDet:ChangeDetectorRef
  ) {
    this.header_data = this.headerService.getStaticHeaderData();
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => this.translation = languageTranslation));
    this.subscriptions.add(this.headerService.videoRecordTime$.subscribe(videoRecordTime => {
      this.videoRecordTime = videoRecordTime;
    }));

    // this.subscriptions.add(this.headerService.screenRecordingState$.subscribe(screenRecordingState => {
    //   this.screenRecordingState = screenRecordingState;
    // }));  

    this.subscriptions.add(this.activatedRoute.queryParams.subscribe(params => {
      this.huddleId = params.huddleId;
      this.huddleName = params.huddleName;
      this.folderId = params.folderId
    }));

    this.pageType = this.activatedRoute.parent.snapshot.data.pageType;
    
  }

  ngOnInit() {
    this.browserAgent = this.getBrowserAgent();
    // weired syntax is due to IE syntax checker
    if (navigator.mediaDevices && this.browserAgent !== 'safari') {
      this.browserSupportedRecording = true;
    } else this.permission = false;

    this.subscriptions.add(this.headerService.screenRecordingState$.subscribe(screenRecordingState => {
      this.windowRef['screenRecordingState'] = screenRecordingState;
      // if (this.permission && (this.windowRef['screenRecordingState'] === 'comfortZone' || this.windowRef['screenRecordingState'] === 'cancelled')) this.showCamView();
    }));

    var today = new Date() as any;
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    var min = String(today.getUTCMinutes());
    var hour = String(today.getUTCHours());

    today = mm + '_' + dd + '_' + yyyy + '_' + hour + '_' + min;
    this.video_name = 'Screen_recording_' + today

    const breadCrumbs: BreadCrumbInterface[] = [{ title: this.video_name, link: '' }];

    if (this.pageType === this.videoPageType.WORKSPACE) {
      breadCrumbs.unshift({ title: this.translation.header_list_worksapce, link: '/workspace/workspace/home/grid' });
    } else if (this.pageType === this.videoPageType.HUDDLE) {
      breadCrumbs.unshift({ title: this.huddleName, link: `/video_huddles/huddle/details/${this.huddleId}/artifacts/grid` });
      breadCrumbs.unshift({ title: this.translation.huddle_breadcrumb_huddle, link: '/video_huddles/list' });
    }

    this.activatedRoute.queryParams.subscribe(params => {
      if (params.folderId) {

        let obj: any;
        if (this.pageType === this.videoPageType.WORKSPACE) {
          obj = {
            goal_evidence: params.goalEvidence,
            document_folder_id: params.folderId
          };
        } else {
          obj = {
            folder_id: params.huddleId,
            goal_evidence: params.goalEvidence,
            document_folder_id: params.folderId
          };
        }
        const sessionData: any = this.headerService.getStaticHeaderData();

        ({
          User: { id: obj.user_id },
          accounts: { account_id: obj.account_id }
        } = sessionData.user_current_account);

        this.homeService.GetBreadcrumbs(obj).subscribe((data: any) => {
          if (data.success === -1) {
            this.toastr.ShowToastr('info',data.message);

            setTimeout(() => {
              this.router.navigate(['/list']);
            }, 4000);
            this.homeService.updateBreadcrumb(data);

          }
          else {
            const folders = data.folders;
            const docFolder = data.document_folders;

            const tmp = breadCrumbs.pop();
            this.pageType === this.videoPageType.HUDDLE ? breadCrumbs.pop() : false;
            let crumbs = [];

            crumbs = [...folders, ...docFolder];
            const path = [];

            crumbs.forEach((element) => {
              if (this.pageType === this.videoPageType.HUDDLE) {
                if (!element.location) {
                  path.push({
                    link: `/video_huddles/list/${element.folder_id}`,
                    title: element.folder_name
                  });
                }
                else {
                  path.push({
                    link: `/video_huddles/huddle/details/${this.huddleId}/artifacts/grid`,
                    title: element.folder_name
                  });
                }
              } else if (this.pageType === this.videoPageType.WORKSPACE) {
                path.push({
                  link: `/workspace/workspace/home/grid/${element.folder_id}`,
                  title: element.folder_name
                });
              }
            });

            const finalCrumbs = [...breadCrumbs, ...path , tmp];

            this.appMainService.updateBreadCrumb(finalCrumbs);

          }
        });
      } else {
          this.appMainService.updateBreadCrumb(breadCrumbs);

      }
    });

    // this.appMainService.updateBreadCrumb(breadCrumbs);

    this.screenRecordingCamContainer = document.getElementById('screen-recording-cam-container');
    this.getAudioDevices(); 

    navigator.mediaDevices.ondevicechange = this.updateDevices;

    if(this.windowRef['mainStream']) this.mainScreenStream = this.windowRef['mainStream'];
  }

  getAudioDevices(){
    navigator.mediaDevices.enumerateDevices().then(this.gotDevices).catch(this.handleError);
  }

  gotDevices = (deviceInfos) => {
    this.availableAudioSrc = [];
    deviceInfos.forEach(element => {  
      if(element.kind == "audioinput" && (element.deviceId != "communications" && element.deviceId != "default")) {
        this.availableAudioSrc.push(element);
      }
    });
    
    this.selectedMicrophones = this.availableAudioSrc[0].deviceId;
    if (this.permission && (this.windowRef['screenRecordingState'] === 'comfortZone' || this.windowRef['screenRecordingState'] === 'cancelled')) this.showCamView();
  }

  public handleError(error) {
    console.log('navigator.MediaDevices.getUserMedia error: ', error.message, error.name);
  }

  public muteMicrophone() {
    this.mainScreenStream.getAudioTracks()[0].enabled = false;
  }

  public unmuteMicrophone() {
    this.mainScreenStream.getAudioTracks()[0].enabled = true;
  }

  private showCamView() {
    console.log("showCamView")
    if (this.browserSupportedRecording) {
      this.permission = true;
      this.permissionLoading = true;

      this.getMedia().then(stream => {
        this.permissionLoading = false;
        this.cameraStream = stream;

        // console.log("this.cameraStream: ",this.cameraStream.getAudioTracks());

        if (this.camView) {
          this.camView.nativeElement.srcObject = stream;
          this.camView.nativeElement.muted = true;
        }

        this.screenRecordingCamContainer.srcObject = stream;
        this.screenRecordingCamContainer.muted = true;


      }).catch((err) => {
        this.permission = false;
        this.cam_and_mic_perm = false;
        this.permissionLoading = false;
        console.error('Could not get Media: ', err);
      });
    }
  }

  updateDevices = () => { 
    this.subscriptions.add(this.headerService.screenRecordingState$.subscribe(screenRecordingState => {
      this.screenRecordingState = screenRecordingState;
    }));

    if(this.deviceCheckForSecondTime == true) {
      navigator.mediaDevices.enumerateDevices()
      .then((devices) => {
        console.log(this.screenRecordingState);
        console.log(this.windowRef['stopScreenRecording']);
        
        if (this.screenRecordingState === 'started' || this.screenRecordingState === 'resumed' || this.screenRecordingState === 'paused' || (this.screenRecordingState === 'initialized' && this.windowRef['stopScreenRecording'])) {
          this.stopScreenCasting();
          setTimeout(() => {
            alert(this.translation.error_you_disconnected_your_audio);
          }, 1000);
         
        } 
        else if (this.screenRecordingState === 'initialized' && this.windowRef['stopScreenRecording'] == undefined) {
          console.log(this.screenStream);
          window.location.reload();
          // if (this.pageType === this.videoPageType.WORKSPACE) {
          //   this.folderId
          //   ? this.router.navigate([`/workspace/workspace/home/grid/${this.folderId}`])
          //   : this.router.navigate([`/workspace/workspace/home/grid`])
          // }
          // else if (this.pageType === this.videoPageType.HUDDLE) this.router.navigate(this.folderId ? ['/video_huddles/huddle/details', this.huddleId, 'artifacts', 'grid', this.folderId] : ['/video_huddles/huddle/details', this.huddleId, 'artifacts', 'grid']);
        } 
        else {
          if(document.location.href.indexOf('/record-screen') >= 0 && this.screenRecordingState !== 'uploading') {
            this.availableAudioSrc = [];
            devices.forEach(element => {
              if(element.kind == "audioinput" && (element.deviceId != "communications" && element.deviceId != "default")) {
                this.availableAudioSrc.push(element);
              }
            });
            this.selectedMicrophones = this.availableAudioSrc[0].deviceId;
            this.changeDet.detectChanges();
            
            setTimeout(() => {
              this.showCamView();
            }, 1000);
          }
        }  

        // devices.forEach(element => {
        //   if(element.kind == "audioinput" && (element.deviceId != "communications" && element.deviceId != "default")) {
        //     this.availableAudioSrc.push(element);
        //   }

          // if(element.kind == "audioinput" && element.deviceId == "default") {
          //   console.log(element);
            
            // this.selectedMicrophones = element.deviceId;
            // this.getMedia().then(stream => {
            //   this.newAudioStream = stream;
            //   // this.newAudioStream.getVideoTracks().forEach(track => track.stop());
            //   // this.cameraStream.getAudioTracks().forEach(track => track.stop());
            //   // that.cameraStream.getAudioTracks()[0].replaceTrack(screenAudioTrack);

            //   let mediaStream = new MediaStream([this.screenStream.getVideoTracks()[0], this.newAudioStream.getAudioTracks()[0]]);

            //   // this.stream.addTrack(this.newAudioStream.getAudioTracks()[0])

            //   // console.log("newAudioStream: ",this.newAudioStream.getTracks());
              
            //   // console.log("merged mediaStream: ", mediaStream.getTracks());
              
            //   // this.cameraStream = null;
            //   this.stream = mediaStream;
            //   // console.log("stream: ",this.stream.getTracks());
            //   this.windowRef['stopScreenRecording'] = () => this.onStopScreenRecording(this.stream);
            //   this.addEventListenerForScreenStream(this.stream);

            //   this.windowRef['mediaRecorderPlaceholder'] = new MediaRecorder(this.stream);
            //   this.windowRef['mediaRecorderPlaceholder'].addEventListener("dataavailable", event => {
            //     this.streamChunks.push(event.data)
            //     console.log("streamChunks: ",this.streamChunks);
            //     // console.log("event:", event);
                
              // });
            //   this.windowRef['mediaRecorderPlaceholder'].start();
            // });  
          // }

        // });
      });

      this.deviceCheckForSecondTime = false;
    } else {
      this.deviceCheckForSecondTime = true;
    }
  };

  private getMedia() {
    return new Promise((resolve, reject) => {
      // let constraints: any = { audio: true };
      let constraints = {
        audio: {deviceId: this.selectedMicrophones ? {exact: this.selectedMicrophones} : undefined},
        video: true
      } 
      
      if (this.browserAgent !== 'firefox') constraints.video = true;
      navigator.mediaDevices.getUserMedia(constraints)
        .then(str => {
          resolve(str);
        }).catch(err => reject(err));
    });
  }

  public startSCreenCasting(screenRecordingType: ScreenRecordingType) {
    this.permission = true;
    // this.window_permission = true;
    this.cam_and_mic_perm = true;
    this.recordingType = screenRecordingType;

    this.headerService.updateScreenRecordingStateSource('initialized');
    this.captureScreen(screenRecordingType);
  }

  private captureScreen(screenRecordingType: ScreenRecordingType) {
    if (screenRecordingType === 'withoutCam') { 
      this.stopCamVideoView();
    }

    const constraints = { 
      video: true,
      audio: true
    };

    let stream;
    navigator.mediaDevices.getDisplayMedia(constraints).then(async screenStream => {
      this.screenStream = screenStream;
      // if (screenRecordingType === 'withCam') {
      //   this.startPicInPicPreview();
      // }
      
      const notMonitor = screenStream.getVideoTracks()[0].getSettings().displaySurface !== 'monitor';
      if (notMonitor) {
        this.stopCamVideoView();
        // this.stopPicInPicPreview();
        this.toastr.ShowToastr('info',this.translation.rs_camera_preview_not_available);
      } else if(screenRecordingType === 'withCam' ){
        this.startPicInPicPreview();
      }
      
      // screenStream.addTrack(this.cameraStream.getAudioTracks()[0]); // adding audio track in screen stream

      const tracks = [
        ...screenStream.getVideoTracks(), 
        ...this.mergeAudioStreams(screenStream, this.cameraStream)
      ];
 
      stream = new MediaStream(tracks);
      this.windowRef['mainStream'] = this.cameraStream;                                  //Storing stream in window ref for globle use
      this.mainScreenStream = this.windowRef['mainStream'];                   //local variable to use in html and within ts
      
      this.windowRef['stopScreenRecording'] = () => this.onStopScreenRecording(stream);
      this.addEventListenerForScreenStream(stream);

      this.windowRef['mediaRecorderPlaceholder'] = new MediaRecorder(stream);
      this.windowRef['mediaRecorderPlaceholder'].addEventListener("dataavailable", event => this.streamChunks.push(event.data));
      this.windowRef['mediaRecorderPlaceholder'].addEventListener("stop", () => {

        this.blob = new Blob(this.streamChunks, { type: 'video/webm' }); // { type: 'video/webm' }
        const videoDuration = this.headerService.getCurrentVideoRecordTime();
        this.streamChunks = []; // resetting, so that the previous chunks must be cleared

        if (this.windowRef['screenRecordingState'] === 'started' || this.windowRef['screenRecordingState'] === 'resumed' || this.windowRef['screenRecordingState'] === 'paused') this.uploadStream(this.blob, videoDuration, this.huddleId);
        else if (this.windowRef['screenRecordingState'] === 'cancelled') this.headerService.updateScreenRecordingStateSource('comfortZone');
      });

      this.playTimer = true;
      this.interval = setInterval(() => {
        this.timer--;
        // console.log('this.timer: ', this.timer);
        // console.log("this.windowRef['screenRecordingState']: ", this.windowRef['screenRecordingState']);
        if (this.timer == 0) {
          // Setting page state
          clearInterval(this.interval);
          this.playTimer = false;
          this.timer = 3;
          // console.log('in if : ')
          // console.log('this.timer: ', this.timer);
          // console.log("this.windowRef['screenRecordingState']: ", this.windowRef['screenRecordingState']);
          if (this.windowRef['screenRecordingState'] === 'initialized') {
            // TODO: check if width and height are not auto setting after video upload functionality
            // screenStream.width = this.windowRef['screen'].width;
            // screenStream.height = this.windowRef['screen'].height;
            // screenStream.fullcanvas = true;
            // console.log('start : ')
            this.windowRef['mediaRecorderPlaceholder'].start();
            this.headerService.updateScreenRecordingStateSource('started');

            if (this.pageType === this.videoPageType.WORKSPACE) {
              this.folderId
              ? this.screenRecordingUrl = { link: '/workspace_video/record-screen', queryParams: { folderId : this.folderId } }
              : this.screenRecordingUrl = { link: '/workspace_video/record-screen', queryParams: '' };
            }
            else if (this.pageType === this.videoPageType.HUDDLE) {
              this.folderId
               ? this.screenRecordingUrl = {
                  link: '/video_details/record-screen',
                  queryParams: { huddleId: this.huddleId, huddleName: this.huddleName, folderId: this.folderId }
                 }
               : this.screenRecordingUrl = {
                  link: '/video_details/record-screen',
                  queryParams: { huddleId: this.huddleId, huddleName: this.huddleName }
                } ;
            }
            this.headerService.updateScreenRecordingUrlSource(this.screenRecordingUrl);
          }
        }
      }, 1000);
    }, err => {
      this.permission = false;
      this.window_permission = false;
      this.headerService.updateScreenRecordingStateSource('cancelled');
      this.stopPicInPicPreview();
    });
  }

  mergeAudioStreams = (desktopStream, voiceStream) => {
    const context = new AudioContext();
    const destination = context.createMediaStreamDestination();
    let hasDesktop = false;
    let hasVoice = false;
    if (desktopStream && this.screenStream.getAudioTracks().length > 0) {
      // If you don't want to share Audio from the desktop it should still work with just the voice.
      const source1 = context.createMediaStreamSource(desktopStream);
      const desktopGain = context.createGain();
      desktopGain.gain.value = 0.7;
      source1.connect(desktopGain).connect(destination);
      hasDesktop = true;
    }
    
    if (voiceStream && this.cameraStream.getAudioTracks().length > 0) {
      const source2 = context.createMediaStreamSource(voiceStream);
      const voiceGain = context.createGain();
      voiceGain.gain.value = 0.7;
      source2.connect(voiceGain).connect(destination);
      hasVoice = true;
    }
      
    return (hasDesktop || hasVoice) ? destination.stream.getAudioTracks() : [];
  };

  private addEventListenerForScreenStream(screenStream: any) {
    screenStream.addEventListener('inactive', () => {
      if (this.windowRef['stopScreenRecording']) this.windowRef['stopScreenRecording']();
    }, false);

    screenStream.getTracks().forEach(track => {
      track.addEventListener('ended', () => {
        if (this.windowRef['stopScreenRecording']) this.windowRef['stopScreenRecording']();
      });
    });
  }

  private onStopScreenRecording(stream: any) {
    if (this.windowRef['screenRecordingState'] === 'initialized') {
      this.headerService.updateScreenRecordingStateSource('aborted');
      setTimeout(() => this.cancelRecording(), 2000);
    } else {
      this.playTimer = false;
      this.windowRef['stopScreenRecording'] = null;
      this.timer = 3;
      this.stopCamView();
      
      stream.getTracks().forEach(track => track.stop()); // It will also stop the MediaRecorder API
      this.screenStream.getAudioTracks().forEach(track => track.stop());
    }
  }

  private stopCamVideoView() {
    const camViewSrcObject = this.camView ? this.camView.nativeElement.srcObject : null;
    if (this.cameraStream) {
      const tracks = this.cameraStream.getVideoTracks();
      tracks.forEach(track => track.stop());
    }
     if (camViewSrcObject) {
      // reset local cam preview
      const tracks = camViewSrcObject.getVideoTracks();
      tracks.forEach(track => track.stop());
      this.camView.nativeElement.srcObject = null;
    }    

    const globalCamViewSrcObject = this.screenRecordingCamContainer.srcObject;
    if (globalCamViewSrcObject) {
      // reset global app level cam preview
      const tracks = globalCamViewSrcObject.getVideoTracks();
      tracks.forEach(track => track.stop());
      this.screenRecordingCamContainer.srcObject = null;
    }
  }

  private stopCamView() {
    const camViewSrcObject = this.camView ? this.camView.nativeElement.srcObject : null;
    if (camViewSrcObject) {
      // reset local cam preview
      const tracks = camViewSrcObject.getTracks();
      tracks.forEach(track => track.stop());
      this.camView.nativeElement.srcObject = null;
    } else if (this.cameraStream) {
      const tracks = this.cameraStream.getTracks();
      tracks.forEach(track => track.stop());
    }

    const globalCamViewSrcObject = this.screenRecordingCamContainer.srcObject;
    if (globalCamViewSrcObject) {
      // reset global app level cam preview
      const tracks = globalCamViewSrcObject.getTracks();
      tracks.forEach(track => track.stop());
      this.screenRecordingCamContainer.srcObject = null;
    }

    if (camViewSrcObject || globalCamViewSrcObject) this.stopPicInPicPreview();
  }

  private startPicInPicPreview() {
    if (this.screenRecordingCamContainer && document.pictureInPictureEnabled && !this.screenRecordingCamContainer.disablePictureInPicture) {
      try {
        if (document.pictureInPictureElement) document.exitPictureInPicture();
        this.screenRecordingCamContainer.requestPictureInPicture().then(() => console.log("screen recording")).catch((err) => {
          alert("There's some error appearing in webcam view (Picture-in-picture), Please start screen sharing again.");
          console.log('Error:'+ err);
        });
      } catch (err) {
        console.error('picInPic error: ', err);
      }
    }
  }

  private stopPicInPicPreview() {
    if (document.pictureInPictureElement) document.exitPictureInPicture();
  }

  public stopScreenCasting() {
    this.windowRef['stopScreenRecording']();
  }

  getStreamprogress() {
    this.filestackService.streamUploadProgress.subscribe(progress => {
      this.uploadProgress = progress;
    });
  }

  uploadStream(blob: any, videoDuration: number, huddleId: number) {
    this.headerService.updateScreenRecordingStateSource('stopped');
    this.getStreamprogress();
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-md'
    };
 
    this.modalRef = this.modalService.show(this.uploadStreamModal, config);
    this.headerService.updateScreenRecordingStateSource('uploading');
    console.log("blob", blob);
    this.filestackService.uploadToS3(blob, 'mp4',
      this.header_data.user_current_account.accounts.account_id,
      this.header_data.user_current_account.User.id).then((res:any) => {
        console.log("res", res);
        
        let obj: any = {};
        obj.user_current_account = this.header_data.user_current_account;
        obj.account_folder_id = huddleId;
        obj.huddle_id = huddleId;
        //fileStack handle key added in live screen recording
        obj.fileStack_handle = res.handle;
        //filestack url added
        obj.fileStack_url = res.url; 
        obj.account_id = this.header_data.user_current_account.accounts.account_id;
        obj.site_id = this.header_data.site_id;
        obj.user_id = this.header_data.user_current_account.User.id;
        obj.current_user_role_id = this.header_data.user_current_account.roles.role_id;
        obj.current_user_email = this.header_data.user_current_account.User.email;
        obj.suppress_render = false;
        obj.suppress_success_email = false;
        this.huddleId ? obj.workspace = false: obj.workspace = true;
        obj.activity_log_id = "";
        obj.video_file_name = res.filename;
        obj.stack_url = res.url;
        obj.video_url = res.key;
        obj.video_file_size = res.size;
        obj.direct_publish = true;
        obj.video_title = this.video_name;
        obj.url_stack_check = 1;
        obj.video_duration = videoDuration;
        if (this.folderId) {
          obj.parent_folder_id = this.folderId;
        }
        obj.is_screen_recording = true;
        this.mainService.uploadVideo(obj).subscribe((data: any) => {
          this.toastr.ShowToastr('info',this.translation.rs_video_uploaded);
          this.modalRef.hide();
          setTimeout(() => this.headerService.updateScreenRecordingStateSource('cancelled'), 1000);
        }, error => {
          this.modalRef.hide();
          console.log('error in upload video api: ', error);
        }, () => {
          // if (this.pageType === this.videoPageType.WORKSPACE) this.router.navigate([`/workspace/workspace/home/grid/${this.folderId}`]);
          if (this.pageType === this.videoPageType.WORKSPACE) {
            this.folderId
            ? this.router.navigate([`/workspace/workspace/home/grid/${this.folderId}`])
            : this.router.navigate([`/workspace/workspace/home/grid`])
          }
          else if (this.pageType === this.videoPageType.HUDDLE) this.router.navigate(this.folderId ? ['/video_huddles/huddle/details', huddleId, 'artifacts', 'grid', this.folderId] : ['/video_huddles/huddle/details', huddleId, 'artifacts', 'grid']);
        });
      });

  }

  public downloadVideoRecorded(){
    this.downloadBlob(this.blob, 'video.mp4');
  }

  public downloadBlob(blob, name = 'video.mp4') {
    // Convert your blob into a Blob URL (a special url that points to an object in the browser's memory)
    const blobUrl = URL.createObjectURL(blob);

    // Create a link element
    const link = document.createElement("a");
    
    // Set link's href to point to the Blob URL
    link.href = blobUrl;
    link.download = name;

    // Append link to the body
    document.body.appendChild(link);

    // Dispatch click event on the link
    // This is necessary as link.click() does not work on the latest firefox
    link.dispatchEvent(
      new MouseEvent('click', { 
        bubbles: true, 
        cancelable: true, 
        view: window 
      })
    );

    // Remove link from body
    document.body.removeChild(link);
  }

  public pauseScreenRecording() {
    this.windowRef['mediaRecorderPlaceholder'].pause();
    this.headerService.updateScreenRecordingStateSource('paused');
  }

  public resumeScreenRecording() {
    this.windowRef['mediaRecorderPlaceholder'].resume();
    this.headerService.updateScreenRecordingStateSource('resumed');
  }

  public OpenCancelModal() {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-md'
    };
    this.modalRef = this.modalService.show(this.cancelModal, config);
  }

  public cancelRecording(redirect: boolean = false) {
   this.headerService.updateScreenRecordingStateSource('cancelled');
    if (this.windowRef['stopScreenRecording']) {
      this.stopScreenCasting();
      if (this.modalRef) this.modalRef.hide();
      redirect = true;
    } else {
      this.stopCamView();
    }

    if (redirect) {
      if (this.pageType === this.videoPageType.WORKSPACE) {
        this.folderId
        ? this.router.navigate([`/workspace/workspace/home/grid/${this.folderId}`])
        : this.router.navigate([`/workspace/workspace/home/grid`])
      }
      else if (this.pageType === this.videoPageType.HUDDLE) this.router.navigate(this.folderId ? ['/video_huddles/huddle/details', this.huddleId, 'artifacts', 'grid', this.folderId] : ['/video_huddles/huddle/details', this.huddleId, 'artifacts', 'grid']);
    }
  }

  retry() {
    this.permission = true;
    this.cam_and_mic_perm = true;
    this.getAudioDevices();
    this.showCamView();
  }

  private getBrowserAgent(): BrowserAgent {
    // Opera 8.0+
    const isOpera = (!!this.windowRef['opr'] && !!opr.addons) || !!this.windowRef['opera'] || navigator.userAgent.indexOf(' OPR/') >= 0;
    if (isOpera) return 'opera';

    // Firefox 1.0+
    const isFirefox = typeof InstallTrigger !== 'undefined';
    if (isFirefox) return 'firefox';

    // Safari 3.0+ "[object HTMLElementConstructor]"
    const isSafari = /constructor/i.test(this.windowRef['HTMLElement']) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!this.windowRef['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
    if (isSafari) return 'safari';

    // Internet Explorer 6-11
    const isIE = /*@cc_on!@*/false || !!document.documentMode;
    if (isIE) return 'ie';

    // Edge 20+
    const isEdge = !isIE && !!window.StyleMedia;
    if (isEdge) return 'edge';

    // Chrome 1 - 71
    const isChrome = !!this.windowRef['chrome'] && (!!this.windowRef['chrome'].webstore || !!this.windowRef['chrome'].runtime);
    if (isChrome) return 'chrome';

    const isEdgeChromium = isChrome && (navigator.userAgent.indexOf("Edg") != -1);
    if (isEdgeChromium) return 'chromium';
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
    //this.headerService.updateScreenRecordingStateSource('cancelled')
    if (this.cam_and_mic_perm && (this.windowRef['screenRecordingState'] === 'comfortZone' || this.windowRef['screenRecordingState'] === 'cancelled' || this.windowRef['screenRecordingState'] === 'aborted')) this.stopCamView();
  }

}
