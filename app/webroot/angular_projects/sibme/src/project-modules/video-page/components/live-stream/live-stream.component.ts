import { Component, OnInit, ViewChild, TemplateRef, OnDestroy, Input, Renderer2, HostListener } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
import { TabsetComponent } from 'ngx-bootstrap/tabs';
import { ActivatedRoute, Router } from "@angular/router";
import { HeaderService, SocketService, AppMainService, MillicastService, HomeService } from "@app/services";
import * as _ from "underscore";
import { ShowToasterService } from '@projectModules/app/services';
import { MainService, PlayerService, ScrollService, CropPlayerService } from '@videoPage/services';
import { environment } from "@environments/environment";
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Subscription, interval, forkJoin } from 'rxjs';
import { take } from 'rxjs/operators';
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import * as moment from 'moment';
import { MARKER_COLORS } from '@videoPage/constants';
import * as Hls from 'hls.js';
import io from 'socket.io-client';
import { BreadCrumbInterface } from '@app/interfaces';

import { PlUniqueDescriptionModelComponent } from "@videoPage/modals";

import { LiveStreamData } from '@app/types';

declare global {
  interface Window { io: any; }
  interface Window { Echo: any; }
}

declare var window: any;
declare var document: any;
declare var navigator: any;

window.io = io;
window.Echo = window.Echo || {};
@Component({
  selector: 'video-page-live-stream',
  templateUrl: './live-stream.component.html',
  styleUrls: ['./live-stream.component.css'],
  providers: [MillicastService]
})
export class LiveStreamComponent implements OnInit, OnDestroy {
  public loaded: boolean = false;
  public liveStreamStarted: boolean = false;
  public src;
  public url_updated = false;
  public newComment;
  public settings;
  public VideoCurrentTime;
  public VideoTotalTime;
  public comments = [];
  public colorClasses: string[] = MARKER_COLORS;
  public CustomMarkers;
  private selectedTag;
  public totals;
  public rubrics;
  public currentTab;
  public selectedRubrics;
  public ShowInfo;
  public VideoInfo: any = {};
  private params: any = { huddle_id: null, video_id: null, init_crop: null};
  private tags;
  private fakeCommentCount;
  private currentTimeInSeconds = 0;
  public allow_evidence_view = 0;
  public videoOptions = {
    maxFiles: 20,
    accept: GLOBAL_CONSTANTS.RESOURCE_UPLOAD_EXTENSIONS,
    fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'url', 'onedrive']
  };
  public staticFiles;
  public isPlaying;
  public currentComment;
  public EditMode;
  public editableRubrics;
  public EditableComment;
  public modalRef: BsModalRef;
  private DeletableFile;
  public EnablePublish;
  public PLTabData;
  public CopyData;
  public SelectedPLRubric;
  public tagIds: number[] = [];
  public ratingIds: number[] = [];
  public PLcomments;
  public permissions;
  public isAuthenticatedUser: boolean = false;
  public goLiveCheck: boolean = false;
  public currnetUser;
  public cropRange;
  public cropRangeSliderConfig;
  public cropStartValue;
  public cropEndValue;
  public videoEnd;
  public previousStart;
  public previousEnd;
  public captureCount = 0;
  public IsCropPlayerReady;
  public currentCopyTab;
  public isCroLoading;
  public frameworks;
  public VideoHuddleFramework;
  public rubricPreview;
  public isVideoProcessing;
  public EditChanges;
  public header_color;
  public primery_button_color;
  public secondry_button_color;
  public header_data;
  public translation: any = {};
  public account_folder_id;
  public video;
  public hls;
  public viewers_count: any = 0;
  public streaming_platform: any = 1;
  public live_viewer_interval: any;
  public viewer_interval_check: any = 0;
  public baseUrl: any = '';
  public after_saving: any = false;
  public play: boolean = false;
  public time: number = 3;
  public disableGoLive: boolean = false;
  public mainLiveSharingStream: any;
  public mainAudioStream: any;
  public newAudioStream: any;
  private subscriptions: Subscription = new Subscription();
  videoOldTime: any = [];
  transCodingStatus = null
  public userAccountLevelRoleId: number | string = null;
  public isSafari: any;
  public isMacOs: any;
  public checkIfModalIsOpen: boolean = false;
  public folderId

  isAudio: boolean = false;
  artifact: any = {};
  socket_listener: any;
  errorStatus: any;
  redirectedBaseURL: any;

  private commentsSortBy: number = 0;

  private screenRecordingCamContainer: any; // It will be used as hidden cam view to show picture in picture view to user
  public liveStreamVideoTime: number;
  private userCurrAcc: any;
  private streamUrl: string;

  public micCamPermError: boolean = false;
  public browserSupportRecording: boolean = true;
  public live_streamer: boolean = false;
  public huddleName: string = '';
  public active_class;

  @ViewChild('staticTabs', { static: false }) staticTabs: TabsetComponent;
  @ViewChild('cropDialog', { static: true }) cropDialog;
  @ViewChild('liveVideoSave', { static: true }) liveVideoSave;
  @ViewChild('selectedrubricsTemplate', { static: false }) selectedrubricsTemplate;
  @Input() huddle_permission;
  elemWidth: any;

  constructor(private modalService: BsModalService, private scrollToService: ScrollService, private homeService: HomeService,
              private toastr: ShowToasterService, private headerService: HeaderService, public mainService: MainService, private router: Router,
              private activatedRoute: ActivatedRoute, private playerService: PlayerService, private cropPlayerService: CropPlayerService,
              private deviceService: DeviceDetectorService, private socketService: SocketService, private appMainService: AppMainService,
              private sanitizer: DomSanitizer, private millicastService: MillicastService, public windowRef: Window, private renderer: Renderer2) {

    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => this.translation = languageTranslation));
    this.subscriptions.add(this.headerService.liveStreamState$.subscribe(liveStreamState => {
      this.windowRef['liveStreamState'] = liveStreamState
    }));
    this.subscriptions.add(this.headerService.liveStreamVideoTime$.subscribe(liveStreamVideoTime => this.liveStreamVideoTime = liveStreamVideoTime)); 
    this.isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if(navigator.platform.toUpperCase().indexOf('MAC') >= 0 ) {
      this.isMacOs = true;
    } else {
      this.isMacOs = false;
    }
    
  }

  ngOnInit() {
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
    this.settings = { EnterToPost: true, PauseWhileTyping: true };
    this.newComment = {};
    this.newComment.files = [];
    this.newComment.timeEnabled = true;
    this.totals = {};
    this.currentTab = 0;
    this.selectedRubrics = [];
    this.ShowInfo = false;
    this.tags = [];
    this.fakeCommentCount = 0;
    this.staticFiles = [];
    this.currentComment = {};
    this.EditMode = false;
    this.editableRubrics = [];
    this.EditableComment = {};
    this.totals.resources_count = 0;
    this.CopyData = {};
    this.CopyData.searchHuddleText = "";
    this.CopyData.searchAccountText = "";
    this.CopyData.LibrarySelected = false;
    this.permissions = {};
    this.isCroLoading = false;
    this.VideoHuddleFramework = "";
    this.EditChanges = {};
    this.cropRangeSliderConfig = {
      connect: [false, true, false],
      step: 1,
      range: {
        min: 1,
        max: 62
      }
    };
    this.header_data = this.headerService.getStaticHeaderData();

    this.folderId = this.activatedRoute.snapshot.queryParams.folderId;

    this.header_color = this.header_data.header_color;
    this.primery_button_color = this.header_data.primery_button_color;
    this.secondry_button_color = this.header_data.secondry_button_color;

    this.currnetUser = this.header_data.user_current_account.User;

    this.userAccountLevelRoleId = this.header_data.user_permissions.roles.role_id;

    this.appMainService.getFrameworks(this.header_data.user_current_account.accounts.account_id).subscribe((frameworks) => {

      this.frameworks = frameworks;

      if (this.frameworks.length == 1) {

        setTimeout(() => {
          this.VideoHuddleFramework = this.frameworks[0].account_tag_id;
          this.ResolveAssignFramework(1, true);
        }, 3000);
      }
    });

    // get params and query-params of current route
    this.params.huddle_id = this.activatedRoute.snapshot.params.huddle_id;
    this.params.video_id = this.activatedRoute.snapshot.params.video_id;
    this.params.init_crop = this.activatedRoute.snapshot.params.init_crop;
    this.live_streamer = this.activatedRoute.snapshot.queryParams.live_streamer;
    this.huddleName = this.activatedRoute.snapshot.queryParams.huddleName;
    if(this.userAccountLevelRoleId == '125' && this.live_streamer == true ){
        this.router.navigate(['/profile-page'])
    }
    if (this.params.video_id && !this.live_streamer) this.initializePage()
    else if (this.windowRef['liveStreamState'] === 'comfortZone' ) this.checkIfLiveStreamingAllowed();
    else if (this.windowRef['liveStreamState'] === 'started-screen-sharing' || this.windowRef['liveStreamState'] === 'stopped-screen-sharing') {
      const liveStreamData = this.headerService.getLiveStreamDataSource();
      this.params.video_id = liveStreamData.videoId;
      this.params.huddle_id = liveStreamData.huddleId;
      this.initializePage();
    }

    this.screenRecordingCamContainer = document.getElementById('screen-recording-cam-container');  
  }

ngAfterViewInit(): void {
  //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
  //Add 'implements AfterViewInit' to the class.
  setTimeout(() => {
    // let v = document.getElementById('cam-view');
    // console.log("LiveStreamComponent -> ngAfterViewInit -> v", v);
      if(this.windowRef['liveStreamState'] === 'stopped-screen-sharing'){
        this.onStopScreenRecording(this.windowRef['screenStream'])
      }
  }, 1500);
 
}
  private initializePage() {
    this.device_detector();
    this.handleParams(this.params);
    this.RunSubscribers();
    this.account_folder_id = this.params.huddle_id;
    this.socketPushFunctionComment();
  }

  private RunSubscribers() {
    this.GetVideoDuration();

    this.mainService.SearchData.subscribe((data) => {

      this.filterComments(data);

    });

    this.mainService.PushableRubric.subscribe((r) => {

      if (this.EditMode) {
        this.EditChanges.changed_standards = true;
      }
      if (r.selected) {

        this.selectedRubrics.push(JSON.parse(JSON.stringify(r)));

      } else {

        this.selectedRubrics = this.selectedRubrics.filter((rub) => {

          return r.account_tag_id != rub.account_tag_id;

        });

      }

    });

    this.LoadPublishSettings();
    this.GetCopyData();

  }

  private GetVideoDuration() {

    this.mainService.GetVideoDuration(this.params.video_id).subscribe((data) => {

      if (data != -1) {
        this.VideoTotalTime = Number(data);
      }


    });

  }
  public opentagsModal() {
    this.modalRef = this.modalService.show(this.selectedrubricsTemplate,{ class: "modal-md  maxcls", backdrop: 'static' })
  }
  public textWidth(marker,val){
    const elem = document.getElementById("text-width"+val);
   // console.log( "width" ,elem.offsetWidth);
   //  console.log( "widthscroll" ,elem.scrollWidth)
      if(elem.scrollWidth > 74) {
        console.log(true);
        marker.tooltipEnable=true;
      }
      else{
        console.log(false);
        marker.tooltipEnable=false;
      }
        
  }
  textbox(value){
    const txHeight = 70;
  const tx = document.getElementById("editTextBox");
  const t2 = document.getElementById("plz_change");
  var lagend = document.getElementById("lagend");
  
    
  lagend.textContent = tx['value'];
  // const tx1=document.getElementById("editTextBox").clientHeight;
  // console.log("txq",tx1)
  var height = lagend.scrollHeight;
  if(height <= 70)
  {
    height = 70;
  }
    if (value == '') {
      tx.setAttribute("style", "height:" + txHeight + "px;overflow-y:hidden;");
      t2.setAttribute("style", "position: inherit;margin-top:0;z-index:9;");
    } else {
      tx.setAttribute("style", "height:" + (height) + "px;overflow-y:hidden;");
      if(tx.scrollHeight > 70 || true)
      {
        t2.setAttribute("style", "position: relative;margin-top:-" + (height-70) + "px;z-index:999;");
        // t2.setAttribute("style", "margin-bottom:-" + (tx.scrollHeight-50) + "px;");
      }
      
    }
  
  
  }
  
  public focusOut(inOut)
   {
    const tx = document.getElementById("editTextBox");
  const t2 = document.getElementById("plz_change");

  if(inOut)
  {
    tx.setAttribute("style", "height:" + (tx.scrollHeight) + "px;overflow-y:hidden;");
      if(tx.scrollHeight > 70)
      {
        // t2.setAttribute("style", "position:relative;top:-" + (tx.scrollHeight-50) + "px;");
        t2.setAttribute("style", "position:relative;margin-top:-" + (tx.scrollHeight-70) + "px;z-index:999;");
      }
  }
  else
  {
    tx.setAttribute("style", "height:" + 70 + "px;overflow-y:hidden;");
      t2.setAttribute("style", "position: inherit;margin-top:0;z-index:9;");
  }
    if(inOut ==2){
      tx.setAttribute("style", "height:" + 70 + "px;overflow-y:hidden;");
      t2.setAttribute("style", "position: inherit;margin-top:0;z-index:9;");
    }
  }
   
    active_check(val){
    console.log("val",val);
    if(val == this.active_class)
    {
      this.active_class=0;
      this.ActivateTab(0);
    }
    else if(val ==1 ){
      this.ActivateTab(0);
      this.active_class=val;
    }
    else if(val ==2 ){
    
        this.active_class=val;
        this.ActivateTab(2);
      }
      else if(val ==3 ){
      
        this.active_class=val;
        this.ActivateTab(0);
      }
      
      else{
        this.ActivateTab(0);
      }
      
  
  }
  public Seek(val) {

    if (val == "All Video" || val == 0) return;
    this.playerService.Seek.emit(this.FormatToSeconds(val));

  }

  public init_crop() {
    if (this.params.init_crop && this.params.init_crop == 1) {

      let that = this;
      setTimeout(() => {

        if (that.src && that.src.path) {
          that.InitiateCropDialog(that.cropDialog);
        }

      });


    }
  }

  public GetCopyData() {

    let sessionData: any = this.headerService.getStaticHeaderData();

    let obj = {

      user_id: sessionData.user_current_account.User.id,
      account_id: sessionData.user_current_account.accounts.account_id

    }

    this.mainService.GetCopyData(obj).subscribe((data) => {

      this.CopyData = data;

      if (this.CopyData.all_huddles && this.CopyData.all_huddles.length > 0) {
        this.onSearchHuddleChange("");
      }

      if (this.CopyData.all_accounts && this.CopyData.all_accounts.length > 0) {
        this.onSearchAccountChange("");
      }



    })

  }

  public GetRubricById(framework_id, assign?) {

    if (!framework_id) {
      this.rubricPreview = {};
      return;
    }

    if (framework_id) {

      this.appMainService.getFrameworkSettingsById(framework_id).subscribe((rubrics: any) => {

        this.rubricPreview = rubrics.data;
        if (assign) {

          this.rubrics = this.rubricPreview;

        }

      });
    }



  }

  public onSearchHuddleChange(newVal) {

    if (this.CopyData.searchHuddleText == '' || !this.CopyData.searchHuddleText) {

      this.CopyData.all_huddles.forEach((h) => {

        h.valid = true;

      });

      return;

    }

    this.CopyData.all_huddles.forEach((h) => {

      h.valid = h.name.toLowerCase().indexOf(this.CopyData.searchHuddleText.toLowerCase()) > -1;

    });

    this.UpdateMatches();
  }

  private UpdateMatches() {

    this.CopyData.huddles_matched = _.where(this.CopyData.all_huddles, { valid: true }).length;
    this.CopyData.accounts_matched = _.where(this.CopyData.all_accounts, { valid: true }).length;

  }

  public ResolveCopyVideo(flag) {

    if (flag == 0) {

      if (this.CopyData.all_huddles) {

        this.CopyData.all_huddles.forEach((h) => { h.selected = false; })
      }

      if (this.CopyData.all_accounts) {

        this.CopyData.all_accounts.forEach((ac) => { ac.selected = false; })
      }

      this.CopyData.LibrarySelected = false;

      this.modalRef.hide();


    } else {

      let selectedAccounts = _.where(this.CopyData.all_accounts, { selected: true });

      let selectedHuddles = _.where(this.CopyData.all_huddles, { selected: true });

      if ((selectedHuddles && selectedHuddles.length > 0) || this.CopyData.LibrarySelected) {

        let ids = selectedHuddles.map((ac) => { return ac.account_folder_id; });

        let sessionData: any = this.headerService.getStaticHeaderData();

        if (this.CopyData.LibrarySelected) { ids.push("-1") }

        let obj = {

          document_id: this.params.video_id,
          account_folder_id: ids,
          current_huddle_id: this.params.huddle_id,
          account_id: sessionData.user_current_account.accounts.account_id,
          user_id: sessionData.user_current_account.User.id,
          copy_notes: this.CopyData.CopyComments ? 1 : 0

        }

        this.mainService.CopyToHuddlesAndLib(obj).subscribe((data: any) => {

          {

            this.toastr.ShowToastr('info',data.message);

          }

        });

      }

      if (selectedAccounts && selectedAccounts.length > 0) {

        let ids = selectedAccounts.map((ac) => { return ac.account_id; });

        let sessionData: any = this.headerService.getStaticHeaderData();

        let obj = {

          account_ids: ids,
          document_id: this.params.video_id,
          user_id: sessionData.user_current_account.User.id,
          copy_notes: this.CopyData.CopyComments ? 1 : 0

        }
        this.modalRef.hide();
        this.mainService.CopyToAccounts(obj).subscribe((data: any) => {

          {

            this.toastr.ShowToastr('info',data.message);

          }

        });

      }

      if (selectedAccounts.length == 0 && selectedHuddles.length == 0 && !this.CopyData.LibrarySelected) {
        this.toastr.ShowToastr('info',this.translation.vd_select_huddle_to_copy);
      }

      this.ResolveCopyVideo(0);
    }

  }

  public onSearchAccountChange(newVal) {

    if (this.CopyData.searchAccountText == '' || !this.CopyData.searchAccountText) {

      this.CopyData.all_accounts.forEach((ac) => {

        ac.valid = true;

      });

      return;

    }

    this.CopyData.all_accounts.forEach((ac) => {

      ac.valid = ac.company_name.toLowerCase().indexOf(this.CopyData.searchAccountText.toLowerCase()) > -1;;

    });
    this.UpdateMatches();
  }

  private LoadPublishSettings() {

    let sessionData: any = this.headerService.getStaticHeaderData();

    let obj = {

      huddle_id: this.params.huddle_id,
      user_id: sessionData.user_current_account.User.id,
      video_id: this.params.video_id

    };

    this.appMainService.GetPublishSettings(obj).subscribe((data: any) => {

      this.EnablePublish = data.show_publish_button;

    });

  }

  public ResolvePublish(flag) {

    this.modalRef.hide();

    if (flag == 1) {

      let sessionData: any = this.headerService.getStaticHeaderData();

      let obj = {

        huddle_id: this.params.huddle_id,
        video_id: this.params.video_id,
        user_id: sessionData.user_current_account.User.id,
        account_id: sessionData.user_current_account.accounts.account_id,
        account_role_id: sessionData.user_current_account.users_accounts.role_id,
        current_user_email: sessionData.user_current_account.User.email

      };


      this.mainService.PublishFeedback(obj).subscribe((data: any) => {

        if (data.status) {

          this.EnablePublish = false;

          this.toastr.ShowToastr('info',this.translation.feedback_published);

        }

      });

    }

  }

  public PublishFeedback(template) {

    this.modalRef = this.modalService.show(template, { class: 'modal-md' });

  }

  public InitiateCopyDialog(template: TemplateRef<any>, file) {

    this.modalRef = this.modalService.show(template, { class: "modal-md model-copy" });
  }

  public InitiateCropDialog(template: TemplateRef<any>, file?) {

    let that = this;

    this.playerService.ModifyPlayingState("pause");

    if (document.getElementsByTagName("modal-container").length < 1) {
      this.isCroLoading = true;

      setTimeout(() => {

        this.isCroLoading = false;
        this.cropRange = [0, 0];
        this.cropStartValue = "00:00";
        this.cropEndValue = "00:00";
        this.modalRef = this.modalService.show(template, { class: "modal-lg model-crop", backdrop: 'static', animated: false });
        (document.querySelector('modal-container') as HTMLElement).addEventListener('click', function () {
        });
        (document.querySelector('.modal-content') as HTMLElement).addEventListener('click', function (e) {
          e.stopPropagation();
        });

      }, 1000);



    }
    else {
      (document.querySelector('bs-modal-backdrop') as HTMLElement).style.display = "block";
      (document.querySelector('modal-container') as HTMLElement).style.display = "block";
      (document.querySelector('body') as HTMLElement).style.overflow = "hidden";
    }

  }
  public hideCropModal() {
    (document.querySelector('bs-modal-backdrop') as HTMLElement).style.display = "none";
    (document.querySelector('modal-container') as HTMLElement).style.display = "none";
    (document.querySelector('body') as HTMLElement).style.overflow = "visible";

  }
  public TrimVideo() {
    if (this.cropRange[0] == this.cropRange[1]) {
      alert(this.translation.please_select_range);
    }
    else {
      this.mainService.TrimVideo(this.params.video_id, this.params.huddle_id, { startVideo: parseFloat(this.cropRange[0] + ".00").toFixed(2), endVideo: parseFloat(this.cropRange[1] + ".00").toFixed(2) }).subscribe((data: any) => {
        let path: any;
        path = environment.baseUrl + "/Huddles/view/" + this.params.huddle_id;
        location.href = path;
      });
    }
  }

  public CaptureTotalTime(VideoTotalTime) {
    if (this.captureCount == 0) {
      let videoStart = 0;
      let videoEnd = Math.floor(this.VideoTotalTime) || Math.floor(VideoTotalTime);
      this.videoEnd = videoEnd;
      let that = this;
      this.cropRangeSliderConfig.range.min = videoStart;
      this.previousStart = videoStart;
      this.cropRangeSliderConfig.range.max = videoEnd;

      this.previousEnd = videoEnd;
      this.cropRange = [videoStart, videoEnd];
      this.convertValuesToTime([videoStart, videoEnd], 0);
      this.convertValuesToTime([videoStart, videoEnd], 1);
      this.videoEnd = this.cropEndValue;
      setTimeout(() => {
        that.IsCropPlayerReady = true;
      }, 1000);

    }
    this.captureCount++;
  }
  public onCropSliderChange(ev) {
    let handle = 0;
    if (this.previousEnd != ev[1]) {
      handle = 1;
    }
    if (handle == 0) {
      this.cropPlayerService.SeekTo(ev[0], 0);
    }
    else {
      this.cropPlayerService.SeekTo(ev[1], 0);
    }
    this.convertValuesToTime(ev, handle);
    this.previousStart = ev[0];
    this.previousEnd = ev[1];
  }
  convertValuesToTime(values, handle) {
    let hours = 0,
      minutes = 0,
      seconds = 0;

    if (handle === 0) {
      hours = this.convertToHour(values[0]);
      minutes = this.convertToMinute(values[0], hours);
      seconds = this.convertToSecond(values[0], minutes, hours);
      this.cropStartValue = this.formatHoursAndMinutes(hours, minutes, seconds);
      return;
    }

    hours = this.convertToHour(values[1]);
    minutes = this.convertToMinute(values[1], hours);
    seconds = this.convertToSecond(values[1], minutes, hours);
    this.cropEndValue = this.formatHoursAndMinutes(hours, minutes, seconds);

  }

  convertToHour(value) {
    return Math.floor(value / 3600);
  }
  convertToMinute(value, hour) {
    return (Math.floor(value / 60) - (hour * 60));
  }
  convertToSecond(value, minute, hour) {
    return (value - (minute * 60) - (hour * 3600));
  }
  formatHoursAndMinutes(hours, minutes, seconds) {
    if (hours.toString().length == 1) hours = '0' + hours;
    if (minutes.toString().length == 1) minutes = '0' + minutes;
    if (seconds.toString().length == 1) seconds = '0' + seconds;
    if (hours == '00' || hours == 0) {
      return minutes + ':' + seconds;
    }
    else {
      return hours + ':' + minutes + ':' + Math.floor(seconds);
    }
  }
  public CropPreviewVideo() {
    this.cropPlayerService.ModifyPlayingState(["play", this.cropRange[0], this.cropRange[1]]);
  }

  public InitiateDeleteResource(template: TemplateRef<any>, file) {

    this.DeletableFile = file;
    this.modalRef = this.modalService.show(template, { class: "modal-md" });


  }

  public onReplyEdit(commentObj) {

    let sessionData: any = this.headerService.getStaticHeaderData();
    let user_id = sessionData.user_current_account.User.id;
    let account_id = sessionData.user_current_account.accounts.account_id;
    let huddle_id = this.params.huddle_id;
    let obj = {
      huddle_id: huddle_id,
      account_id: account_id,
      comment_id: commentObj.reply.id,
      videoId: this.params.video_id,
      for: "",
      synchro_time: '',
      time: (this.newComment.timeEnabled) ? this.currentTimeInSeconds : 0,
      ref_type: commentObj.reply.ref_type,
      comment: commentObj.reply.EditableText,
      user_id: user_id,
      standards_acc_tags: "",
      default_tags: "",
      assessment_value: "",
      account_role_id: sessionData.user_current_account.users_accounts.role_id,
      current_user_email: sessionData.user_current_account.User.email
    };

    this.appMainService.EditComment(obj).subscribe((data: any) => {

      if (data.status == "success") {

        let parentIndex = _.findIndex(this.comments, { id: commentObj.comment.id });
        let found = false;
        if (parentIndex > -1) {

          let index = _.findIndex(this.comments[parentIndex].Comment.responses, { id: commentObj.reply.id });

          if (index > -1) {
            found = true;
            this.comments[parentIndex].Comment.responses[index].EditEnabled = false;
            this.mainService.ReRenderMarkers.emit(true);
          }

        }

        if (!found) {

          this.comments.forEach((c) => {

            if (c.Comment.responses) {

              c.Comment.responses.forEach((r) => {

                let index = -1;
                if (r.responses)
                  index = _.findIndex(r.responses, { id: commentObj.reply.id });

                if (index > -1) {

                  r.responses[index].comment = commentObj.reply.EditableText;
                  r.responses[index].EditEnabled = false;

                }

              });

            }

          });

        }

      }

    });


  }

  public ResolveDeleteFile(flag, id?) {

    if (flag == 0) {
      this.modalRef.hide();
    } else {
      if (this.modalRef)
        this.modalRef.hide();
      let sessionData = this.headerService.getStaticHeaderData();

      let obj = {
        huddle_id: this.params.huddle_id,
        video_id: this.params.video_id,
        document_id: id ? id : this.DeletableFile.id,
        user_id: sessionData.user_current_account.User.id,
      }

      this.mainService.DeleteResource(obj).subscribe((data: any) => {

        if (!data.error) {
          this.toastr.ShowToastr('info',this.translation.resource_deleted);
          let index = _.findIndex(this.staticFiles, { id: id ? id : this.DeletableFile.id });
          if (index > -1) {
            this.staticFiles.splice(index, 1);
            this.totals.resources_count--;
          }

        }

      });

    }

  }

  public UploadResource(file, wholeVideo?, commentId?) {
    console.log(file)
    debugger;
    if (wholeVideo) file.time = 0;
    let sessionData: any = this.headerService.getStaticHeaderData();
    let obj: any = {
      huddle_id: this.params.huddle_id,
      account_id: sessionData.user_current_account.accounts.account_id,
      user_id: sessionData.user_current_account.User.id,
      video_id: this.params.video_id,
      stack_url: file.url,
      video_url: file.key,
      video_file_name: file.filename,
      video_file_size: file.size,
      video_desc: "",
      current_user_role_id: sessionData.user_current_account.roles.role_id,
      current_user_email: sessionData.user_current_account.User.email,
      url_stack_check: 1,
      account_role_id: sessionData.user_current_account.users_accounts.role_id


    };

    if (commentId) {

      let comment = _.findWhere(this.comments, { id: commentId });

      if (comment)
        obj.time = comment.time;
      obj.comment_id = commentId;
    }

    if (!obj.time && obj.time != 0) obj.time = this.FormatToSeconds(file.time);
    this.appMainService.UploadResource(obj).subscribe((data: any) => {

      file.id = data.document_id;

    });

  }

  private FormatToSeconds(time) {

    if (time == 0) return 0;
    if (typeof (time) == "number") return time;
    let stamp = time.split(":");

    return Number(stamp[0] * 3600) + Number(stamp[1] * 60) + Number(stamp[2]);

  }

  public ApplySettings(settings) {

    if (typeof (settings.sortBy) == "number") {
      this.commentsSortBy = settings.sortBy;

      if (settings.sortBy == 1) {

        this.comments = _.sortBy(this.comments, (c) => { return new Date(c.created_date) });

      } else if (settings.sortBy == 0) {
        this.comments = _.sortBy(this.comments, (c) => { return new Date(c.created_date) });
        this.comments = this.comments.reverse();
      } else if (settings.sortBy == 2) {

        let wholeComments = this.comments.filter((c) => { return c.time == 0 || c.time == null; });
        let timeComments = this.comments.filter((c) => { return c.time > 0 });


        this.comments = _.sortBy(timeComments, (c) => { return new Date(c.time) }).concat(wholeComments);

      }

    }
    if (typeof (settings.autoscroll) == "boolean") {

      this.settings.autoscroll = settings.autoscroll;

    }

  }

  public GetNewFiles(obj) {
    if (obj.from == 'resources') {

      this.AddTimeStampToResources(obj.files);


    }

  }

  public FileClicked(from, file) {

    if (from == "td") {

      if (file.stack_url && file.stack_url != null) {

        // let path = environment.baseUrl + "/app/view_document" + file.stack_url.substring(file.stack_url.lastIndexOf("/"), file.stack_url.length);
        // window.open(path, "_blank");

        let path = "/home/document-commenting/pdf-renderer/workspace-video-page/" + file.id+ file.stack_url.substring(file.stack_url.lastIndexOf("/")) + "/" + file.file_type + "/" + file.account_folder_id + "/" + file.parent_folder_id + "/" + true
        window.open(path, "_blank");
        //home is added to the url to work on production. it will not work on localhost

      } else {

        this.DownloadFile(file);

      }
    } else {
      this.DownloadFile(file);
    }

  }

  private DownloadFile(file) {

    this.mainService.DownloadFile(file.id);

  }

  public AppendNewFiles(obj) {
    if (obj.from == 'comment') {
      this.newComment.files = this.newComment.files.concat(obj.files);
    }


  }

  public RemoveFileFromComment(file, i) {

    let index = _.findIndex(this.staticFiles, { url: file.url });
    this.newComment.files.splice(i, 1);

    if (index > -1) {

      let subIndex = _.findIndex(this.staticFiles, { url: file.url });
      this.staticFiles.splice(index, 1);
      this.ResolveDeleteFile(1, file.id);

    }


  }

  public AttachFilesToComment(comment) {

    if (((typeof (comment.time) == "number" && comment.time == 0) || comment.time == "0") && !comment.is_new_comment) {

      comment.files = [];
      return;

    }

    comment.files = [];

    if (comment.is_new_comment) {
      comment.files = _.where(this.staticFiles, { comment_id: comment.id });
      return;
    }

    this.staticFiles.forEach((file) => {

      if (!file) return;

      file.time2 = file.time == "All Video" ? 0 : this.FormatToSeconds(file.time);

      comment.time2 = this.FormatToSeconds(comment.time);
      if (comment.time2 == file.time2) {

        comment.files.push(file);

      }

    })

  }

  private AddTimeStampToResources(files, isFromComment?, fixTime?, commentId?) {

    this.totals.resources_count += files.length;
    if (this.newComment.timeEnabled) {

      files.forEach((f) => {

        f.created_by = this.currnetUser.id;

        f.time = fixTime != void 0 ? fixTime : this.ConvertTime(this.currentTimeInSeconds);

        if (commentId) {

          let comment = _.findWhere(this.comments, { id: commentId });

          if (comment) {
            f.time = comment.time;
          }

        }

        f.stack_url = f.url;

        f.title = f.filename || f.title;

        if (commentId) {
          f.comment_id = commentId;
        }

        if (!isFromComment)
          this.UploadResource(f, true);
        else
          this.UploadResource(f, false, commentId);

      });
    } else {

      files.forEach((f) => {
        f.time = 0;
        f.stack_url = f.url;
        f.title = f.filename;
        if (commentId) {
          f.comment_id = commentId;
        }
        if (!isFromComment)
          this.UploadResource(f, true);
        else
          this.UploadResource(f, false, commentId);

      });
    }


  }

  public DeleteResource(file) {



  }

  public ActivateTab(tabId) {

    this.staticTabs.tabs[tabId].active = true;
    this.currentTab = tabId;

  }

  public RemoveSelectedRubric(rubric, i) {

    if (rubric) {

      let index = _.findIndex(this.rubrics.account_tag_type_0, { account_tag_id: rubric.account_tag_id });

      if (index > -1) {

        this.rubrics.account_tag_type_0[index].selected = false;

        this.selectedRubrics.splice(i, 1);

      }

    }

  }

  private GetRubrics(huddle_id, account_id) {

    this.mainService.GetRubrics({ huddle_id: huddle_id, account_id: account_id, video_id: this.params.video_id }).subscribe((data: any) => {

      this.rubrics = data.data;

    });

  }
  public SelectTab(ev) {
    this.closeInfo();
    this.currentTab = ev;
    console.log(this.active_class, this.currentTab )
    
    if(this.active_class != 2 && this.currentTab == 2){
      this.active_class=2;
    }
    else if(this.active_class != 3 && this.currentTab != 2 ){
      this.active_class=0;
    }
      
    if (ev == 4) {
      this.LoadRubricsTabData();
    }

  }

  public LoadRubricsTabData(callOnSocket?, selectedStandardId?, selectedRatingId?) {

    if (!callOnSocket) {
      this.SelectedPLRubric = {};
      this.PLcomments = [];
    }
    let sessionData: any = this.headerService.getStaticHeaderData();
    let obj = {
      account_id: sessionData.user_current_account.accounts.account_id,
      user_id: sessionData.user_current_account.User.id,
      huddle_id: this.params.huddle_id,
      video_id: this.params.video_id
    }

    this.appMainService.GetPLTabData(obj).subscribe((data) => {

      this.PLTabData = data;

      if (this.SelectedPLRubric && this.SelectedPLRubric.account_tag_id && selectedStandardId && selectedRatingId) {

        if (this.SelectedPLRubric.account_tag_id == selectedStandardId) this.SelectedPLRubric.selectedRating = selectedRatingId;

      }

    });


  }

  public OnRubricClicked(rubric) {

    this.SelectedPLRubric = rubric;

    this.SelectedPLRubric.selectOptions = this.getPls();

    if (this.getSettings("enable_unique_desc") == 1) {

      this.SelectedPLRubric.plUniqueDescriptions = [];
      
      this.SelectedPLRubric.selectOptions.forEach((opt) => {
        const plUniqueDescription = { title: opt.text, description: '' };
        let obj = _.findWhere(this.SelectedPLRubric.performance_level_descriptions, { performance_level_id: opt.id });
        if (obj) plUniqueDescription.description = obj.description;
        
        this.SelectedPLRubric.plUniqueDescriptions.push(plUniqueDescription);
      });
    }

    if (this.SelectedPLRubric.get_selected_rating)
      this.SelectedPLRubric.selectedRating = this.SelectedPLRubric.get_selected_rating.rating_id;
    else this.SelectedPLRubric.selectedRating = 0;
    this.PLcomments = [];
    let sessionData: any = this.headerService.getStaticHeaderData();

    let obj = {

      huddle_id: this.params.huddle_id,
      account_id: sessionData.user_current_account.accounts.account_id,
      user_id: sessionData.user_current_account.User.id,
      video_id: this.params.video_id,
      account_tag_id: rubric.account_tag_id

    };
    if (Number(this.SelectedPLRubric.get_standard_tagged_count) > 0) {

      this.mainService.GetPLComments(obj).subscribe((data: any) => {

        this.PLcomments = data.videoComments;

      });

    }


  }

  public RatingChanged(id) {

    let sessionData: any = this.headerService.getStaticHeaderData();

    id = Number(id);
    this.tagIds.push(this.SelectedPLRubric.account_tag_id);
    this.ratingIds.push(id);

    let val = _.findWhere(this.SelectedPLRubric.selectOptions, { id: this.ratingIds[this.ratingIds.length - 1] });

    let obj = {
      standard_ids: [this.tagIds[this.tagIds.length - 1]],
      huddle_id: this.params.huddle_id,
      video_id: this.params.video_id,
      account_id: sessionData.user_current_account.accounts.account_id,
      user_id: sessionData.user_current_account.User.id,
      account_role_id: sessionData.user_current_account.users_accounts.role_id,
      current_user_email: sessionData.user_current_account.User.email
    };

    obj["rating_value_" + this.tagIds[this.tagIds.length - 1]] = val ? val.value : 0;
    obj["rating_id_" + this.tagIds[this.tagIds.length - 1]] = this.ratingIds[this.ratingIds.length - 1];


    this.mainService.SaveRating(obj).subscribe((data: any) => {

      if (data.rating_score == 'N/O')
        this.PLTabData.average_rating = '0 - No Rating';
      else {
        let avgRating = this.SelectedPLRubric.selectOptions.find(option => data.rating_score == option.value);
        if (avgRating) this.PLTabData.average_rating = `${avgRating.value} - ${avgRating.text}`;
      }

      let selectedPLRubric = this.PLTabData.standards.data.account_tag_type_0.find(rubric => {
        return rubric.account_tag_id == this.tagIds[this.tagIds.length - 1];
      });
      if (selectedPLRubric) {
        if (selectedPLRubric.get_selected_rating)
          selectedPLRubric.get_selected_rating.rating_id = this.ratingIds[this.ratingIds.length - 1];
        else
          selectedPLRubric.get_selected_rating = { rating_id: this.ratingIds[this.ratingIds.length - 1] };
      }
      this.tagIds.shift();
      this.ratingIds.shift();

    });

  }

  public getPls() {

    if (this.getSettings("enable_performance_level") == 1) {

      return this.GetDropdownSettingsFromPL(this.SelectedPLRubric);

    } else {
      return this.PLTabData.old_ratings.map((fr) => {

        return { id: fr.account_meta_data_id, value: fr.meta_data_value, text: fr.meta_data_name.substring(fr.meta_data_name.lastIndexOf("_") + 1, fr.meta_data_name.length) };

      });
    }



  }

  public getSettings(key) {

    return this.PLTabData.standards.data.account_framework_settings[key];

  }

  private GetDropdownSettingsFromPL(rubric: any) {

    return rubric.account_framework_settings_performance_levels.map((r) => {

      return { id: r.id, value: r.performance_level_rating, text: r.performance_level };

    })

  }

  public getMarkerBg(tag, index) {

    if (this.selectedTag == tag) {

      return this.colorClasses[index];

    } else {
      return "transparent";
    }

  }

  public ChooseCustomTag(tag) {

    this.EditChanges.changed_custom_markers = true;

    if (this.selectedTag == tag) {
      this.selectedTag = {};
    } else {
      this.selectedTag = tag;
    }

  }

  private filterComments(criteria) {

    if (!this.comments) {

      return;
    }

    this.prepareComments(this.comments);

    if (criteria.tag_id && criteria.tag_id != -1) {

      this.comments.forEach((c) => {

        c.valid = false;

        c.default_tags.forEach((dt) => {

          if (dt.account_tag_id == criteria.tag_id) {
            c.valid = true;
          }

        });

      });

    }

    if (criteria.text) {

      this.comments.forEach((c) => {

        c.valid = c.comment && c.comment.toLowerCase().indexOf(criteria.text.toLowerCase()) > -1;

        if (!c.valid) {

          if (c.Comment && c.Comment.responses) {

            c.Comment.responses.forEach((reply) => {

              if (reply.comment && reply.comment.toLowerCase().indexOf(criteria.text.toLowerCase()) > -1) {
                c.valid = true;
              }

              if (reply.responses) {

                reply.responses.forEach((sub_reply) => {

                  if (sub_reply.comment && sub_reply.comment.toLowerCase().indexOf(criteria.text.toLowerCase()) > -1) {

                    c.valid = true;

                  }

                });

              }


            })

          }

        }

      })

    }


  }

  private handleParams(args) {

    let sessionData: any = this.headerService.getStaticHeaderData();

    let user_id = sessionData.user_current_account.User.id;

    this.appMainService.LiveStreamPage.emit({ item_id: args.video_id });

    this.mainService.GetVideoResources({ video_id: args.video_id, huddle_id: args.huddle_id, user_id: user_id }).subscribe((data: any) => {

      if (data)
        data.forEach((d) => {

          d.time = (d.scripted_current_duration) ? this.ConvertTime(d.scripted_current_duration) : this.translation.vd_all_videos;

        })

      if (data)
        this.staticFiles = this.staticFiles.concat(data);

    });

    let that = this;

    let interval = setInterval(() => {

      let headerData: any = that.headerService.getStaticHeaderData();


      if (headerData) {

        clearInterval(interval);

        let account_id = headerData.user_current_account.accounts.account_id;
        let user_id = headerData.user_current_account.User.id;
        let data = {
          "user_id": user_id,
          "video_id": args.video_id,
          "account_id": account_id,
          "huddle_id": args.huddle_id,
          "role_id": headerData.user_current_account.roles.role_id,
          "permission_maintain_folders": headerData.user_permissions.UserAccount.permission_maintain_folders
        }
        that.GetRubrics(args.huddle_id, account_id);
        that.mainService.GetVideo(data).subscribe((data) => {
          that.handleVideo(data);

        });

      }

    });

  }

  public getPLTabPermission() {
    if (this.VideoInfo.h_type == 2) {

      return this.permissions.coaching_perfomance_level;

    } else if (this.VideoInfo.h_type == 3) {

      return this.permissions.assessment_perfomance_level;

    }

  }

  public getCoachingSummaryCheck() {
    if (this.VideoInfo.h_type == 2) {
      if (this.currnetUser.huddle_role_id == '210') {
        if (!this.permissions.coaching_perfomance_level) {
          return false;
        } else {
          if (this.permissions.can_view_summary == "1") {
            return true;
          } else {
            return false;
          }
        }
      } else {
        return true;
      }

    } else if (this.VideoInfo.h_type == 3) {

      if (!this.permissions.assessment_perfomance_level) {
        return false;
      } else {
        return this.permissions.can_view_summary || this.permissions.assessment_summary_check;
      }

    } else {
      return false;
    }

  }

  private handleVideo(data) {

    if (data.h_type == 3 && this.rubrics) {

      if (data.user_huddle_level_permissions == 210 || data.user_huddle_level_permissions == 220) {

        this.rubrics.account_framework_settings.checkbox_level = -1;

      }

    }

    if (!data.success) {

      this.toastr.ShowToastr('info',this.translation.u_dont_permission_vd);

      setTimeout(() => {

        location.href = environment.baseUrl;

      }, 1000);

      return;


    }
    if (data.video_detail.doc_type != 4) {
      this.toastr.ShowToastr('info',this.translation.something_went_wrong_msg);

        this.folderId
        ? this.router.navigate([`/video_huddles/huddle/details/${this.account_folder_id}/artifacts/grid/${this.folderId}`]) 
        : this.router.navigate([`/video_huddles/huddle/details/${this.account_folder_id}/artifacts/grid`]);
    }


    this.isAuthenticatedUser = true;
    this.streamUrl = data.stream_url;
    

    // if (this.windowRef['liveStreamState'] === 'comfortZone' || this.windowRef['liveStreamState'] === 'cancelled' ) {
      if(!this.goLiveCheck  && !this.live_streamer) {                     // only go into this if when user is in view mode
        if(data.is_screen_sharing == 0) {         // if live streaming is without screen sharing on IOS the check will be 0
          this.millicastService.updateStreamName(data.stream_name);
          this.millicastLiveStreaming(data.stream_name);
        } else {                                  // if live streaming is with screen sharing 
          this.millicastService.updateStreamName(data.stream_name);
          this.millicastLiveStreamingSS(data.screen_stream_log.file_name_recorded_video);
        }

        // Check count for the viewer mode
        this.live_viewer_interval = setInterval( () => {
          if(this.viewer_interval_check == 0) {
            this.mainService.GetLiveViewerCount(obj).subscribe((data: any) => {
              this.viewers_count = data.data;
            })
          }
        }, 5000)
      }
    // }
    
    this.comments = data.comments.Document.comments;
    this.ApplySettings({ sortBy: 0 });
    this.CustomMarkers = data.custom_markers;
    this.prepareComments(this.comments);
    this.totals.comment_count = data.comments_counts.total_comments;
    this.totals.resources_count = data.attached_document_numbers;
    this.VideoInfo = data.video_detail;
    this.VideoInfo.stream_name = data.stream_name;
    this.VideoInfo.h_type = data.h_type;
    this.VideoInfo.huddle_type = data.huddle_type;
    this.settings.PauseWhileTyping = Boolean(data.user_pause_settings);
    this.permissions.rubric_check = data.rubric_check;
    this.permissions.performance_level_check = data.performance_level_check;
    this.permissions.can_view_summary = data.can_view_summary;
    this.permissions.coaching_summary_check = data.coaching_summary_check;
    this.permissions.assessment_summary_check = data.assessment_summary_check;
    this.permissions.can_comment = data.can_comment;
    this.permissions.can_reply = data.can_reply;
    this.permissions.can_rate = data.can_rate;
    this.permissions.huddle_permission = data.user_huddle_level_permissions;
    this.permissions.showCopy = data.can_dl_edit_delete_copy_video;
    this.permissions.AllowCustomMarkers = data.video_markers_check == "1";
    this.permissions.coaching_perfomance_level = data.coaching_perfomance_level == "1";
    this.permissions.can_crop_video = !!data.can_crop_video;
    this.permissions.assessment_perfomance_level = data.assessment_perfomance_level == "1";
    this.loaded = true;
    this.liveStreamStarted = true;
    this.permissions.coachee_permission = data.coachee_permission;

    data.huddle_info.huddle_title = this.VideoInfo.title;
    this.mainService.breadcrumbs.emit(data.bread_crumb_output);
    this.mainService.huddleInfo.emit(data.huddle_info);
    this.VideoInfo.coaching_link = data.coaching_link;
    this.VideoInfo.assessment_link = data.assessment_link;
    this.permissions.framework_selected_for_video = data.framework_selected_for_video;
    this.permissions.allow_per_video = data.allow_per_video;
    this.permissions.get_account_video_library_permissions = data.get_account_video_library_permissions;

    this.currnetUser.huddle_role_id = data.user_huddle_level_permissions;

    this.VideoTotalTime = data.video_duration;
    this.VideoInfo.defaultFramework = data.default_framework;
    this.isVideoProcessing = data.video_detail.published == 0;
    this.transCodingStatus = data.video_detail.transcoding_status;
    this.errorStatus = data.video_detail.encoder_status == 'Error';

    this.updateBreadCrumb(this.VideoInfo.title, { id: data.huddle_info.account_folder_id, name: data.huddle_info.name });

    if (this.headerService.isAValidAudio(data.video_detail.file_type)) {
      this.isAudio = true;

    }
    if (this.permissions.can_crop_video) {
      this.init_crop();
    }
    if (data.default_framework != "0" && data.default_framework != "-1") {

      this.VideoHuddleFramework = data.default_framework;
      this.GetRubricById(data.default_framework);

      let sessionData: any = this.headerService.getStaticHeaderData();

      this.permissions.permission_video_library_upload = sessionData.user_current_account.users_accounts.permission_video_library_upload == 1;

    }

    if (this.VideoInfo.h_type == 2) {
      this.VideoInfo.coachee_name = data.coachee_name;
      this.VideoInfo.coaches_name = data.coaches_name;
    } else if (this.VideoInfo.h_type == 3) {

      this.VideoInfo.assessor_names = data.assessor_names;
      this.VideoInfo.eval_participant_names = data.eval_participant_names;

    }
    this.streaming_platform = data.streaming_auth_details.id;
    // console.log('this.streaming_platform: ', this.live_streamer)
    if (this.streaming_platform == 1 && !this.live_streamer) {
      setTimeout(() => {
        if (Hls && Hls.isSupported() && this.loaded) {
          this.video = document.getElementById('live_video');
          this.hls = new Hls({
          });
          if (this.video) {
            this.hls.loadSource(this.src.path);
            this.hls.attachMedia(this.video);
            this.hls.on(Hls.Events.MANIFEST_PARSED, () => {
              this.video.play();
              this.video.play();
              this.video.on("timeupdate", (e) => {
              });
            });
            this.hls.on(Hls.Events.BUFFER_APPENDED, (e) => {
              this.currentTimeInSeconds = this.video.duration;
            });
          }

        }
      }, 50);
    }
    else {
      setInterval(() => {
        this.currentTimeInSeconds++;
      }, 1000);
    }


    let that = this;
    let sessionData: any = this.headerService.getStaticHeaderData();

    let obj = {

      user_id: sessionData.user_current_account.User.id,
      account_id: sessionData.user_current_account.accounts.account_id,
      document_id: this.params.video_id,
      device_type: 2,
      session_id: this.appMainService.parseCookieValue('PHPSESSID')
    };

    this.live_viewer_interval = setInterval( () => {
      if(this.windowRef['liveStreamState'] !== 'comfortZone') {
        this.mainService.GetLiveViewerCount(obj).subscribe((data: any) => {
          this.viewers_count = data.data;
        })
      }
    }, 5000)


  }



  private prepareComments(comments) {

    comments.forEach((c) => {

      c.valid = true;

      c.ReplyTouched = false;

      if (c.time == null) c.time = 0;

      if (c.default_tags.length > 0) {

        c.default_tags.forEach((dt) => {

          let index = _.findIndex(this.CustomMarkers, { account_tag_id: dt.account_tag_id });

          if (index >= 0) {
            if (!c.customMarkers) c.customMarkers = [];
            c.customMarkers.push(dt);
          } else {

            if (!c.defaultTags) c.defaultTags = [];
            c.defaultTags.push(dt);

          }

        });

      }
    })

  }

  public InitiateDeleteVideo(template: TemplateRef<any>) {

    this.modalRef = this.modalService.show(template, { class: "modal-md" });

  }

  public getValidCommentCount() {

    if (!this.comments || this.comments.length <= 0) return 1;

    return _.where(this.comments, { valid: true }).length;

  }

  public getSelectFrameworkPermission() {

    let flag = false;

    if (this.permissions.allow_per_video == "1" && (this.VideoInfo.h_type == 2 || this.VideoInfo.h_type == 1) && this.permissions.framework_selected_for_video != "1") {
      flag = true;
    }

    if (this.permissions.allow_per_video != "1" && (this.VideoInfo.h_type == 2 || this.VideoInfo.h_type == 1) && this.permissions.framework_selected_for_video != "1") {

      if (this.permissions.huddle_permission == 200) {

        flag = true;

      }

    }

    if (this.VideoInfo.h_type == 3 && this.permissions.framework_selected_for_video != "1") {

      if (this.permissions.huddle_permission == 200) {

        flag = true;

      }

    }

    return flag;

  }

  public ResolveDeleteVideo(flag) {
    if (flag == 0) {
      this.modalRef.hide();
    } else {
      this.modalRef.hide();
      this.DeleteVideo();
    }

  }

  public DeleteVideo() {

    let sessionData: any = this.headerService.getStaticHeaderData();

    let obj = {

      huddle_id: this.params.huddle_id,
      video_id: this.params.video_id,
      user_id: sessionData.user_current_account.User.id

    }

    this.mainService.DeleteVideo(obj).subscribe((data: any) => {

      if (data.sucess) {

        this.toastr.ShowToastr('info',this.translation.vd_videos_has_been_deleted);

        setTimeout(() => {

          let path = environment.baseUrl + "/Huddles/view/" + this.params.huddle_id;

          location.href = path;

        }, 2000);

      } else {

        this.toastr.ShowToastr('info',data.message);

      }

    });

  }

  public DownloadVideo() {

    let sessionData: any = this.headerService.getStaticHeaderData();

    let obj = {
      document_id: this.params.video_id,
      huddle_id: this.params.huddle_id,
      account_id: sessionData.user_current_account.accounts.account_id,
      user_id: sessionData.user_current_account.User.id
    }

    this.mainService.DownloadVideo(obj);

  }

  public PointToLink() {

    let link = this.VideoInfo.h_type == "2" ? `${environment.baseUrl}/home/trackers/coaching?date=1month` : this.VideoInfo.h_type == "3" ? `${environment.baseUrl}/home/trackers/assessment` : "";

    if (link) {
      window.open(link, "_blank");
    }

    setTimeout(() => {
      this.ActivateTab(this.currentTab);
    });

  }

  public OnCommentAdded(e) {

    this.totals.comment_count++;

  }
  private updateCommentCount() {

    let count = this.comments.length;

    if (count == 0) {
      this.totals.comment_count = 0;
      return;
    }

    _.each(this.comments, (c) => {

      if (c && c.Comment && c.Comment.responses && c.Comment.responses.length > 0) {

        count += c.Comment.responses.length;

        c.Comment.responses.forEach((r) => {
          count += r.responses ? r.responses.length : 0;
        });

      }


    });

    this.totals.comment_count = count;
  }

  public onCommentDelete(comment) {
    if (!comment.id || comment.id == "") {
      this.toastr.ShowToastr('info',this.translation.please_select_comment);
      return;
    } else {
      let sessionData: any = this.headerService.getStaticHeaderData();
      let user_id = sessionData.user_current_account.User.id;
      let obj = {
        video_id: this.params.video_id,
        comment_id: comment.id,
        user_id: user_id
      }
      this.appMainService.DeleteComment(obj).subscribe((data: any) => {
        if (data.status == "success") {


          this.LoadPublishSettings();

          this.FindAndKill(comment.id);

          this.updateCommentCount();

          this.mainService.ReRenderMarkers.emit(true);

        }
      })
    };
  }

  private FindAndKill(id) {

    let index = -1;
    index = _.findIndex(this.comments, { id: id });

    if (index > -1) {

      this.comments.splice(index, 1);

    } else {

      let parent;
      this.comments.forEach((c) => {

        if (c && c.Comment && c.Comment.responses) {
          let _index = _.findIndex(c.Comment.responses, { id: id });
          if (_index > -1) {
            index = _index;
            parent = c;
          }
        }


      });

      if (index > -1) {

        parent.Comment.responses.splice(index, 1);

      } else {

        this.comments.forEach((c) => {
          if (c && c.Comment && c.Comment.responses) {
            c.Comment.responses.forEach((r, i) => {
              if (r && r.responses) {
                let _index = _.findIndex(r.responses, { id: id });

                if (_index > -1) {
                  r.responses.splice(_index, 1);
                }
              }


            });
          }


        });

      }

    }

  }

  public GetTime(ev) {

    if (this.EditMode) return;
    this.VideoCurrentTime = this.ConvertTime(ev);
    this.currentTimeInSeconds = ev;

    let index = _.findIndex(this.comments, { time: Math.floor(ev) });

    if (index >= 0 && ev != 0) {
      if (this.isPlaying && this.settings.autoscroll) {
        this.currentComment = this.comments[index];
        this.scrollToService.scrollTo("#slimscroll", "#comment_" + index);
      } else {
        this.currentComment = {};
      }

    }
  }

  public ToggleEnterPost() {

    this.settings.EnterToPost = !this.settings.EnterToPost;

  }

  public TogglePause() {

    this.settings.PauseWhileTyping = !this.settings.PauseWhileTyping;

    let sessionData: any = this.headerService.getStaticHeaderData();
    let obj = {
      user_id: sessionData.user_current_account.User.id,
      account_id: sessionData.user_current_account.accounts.account_id,
      value: Number(this.settings.PauseWhileTyping)
    };

    this.mainService.SavePauseSettings(obj).subscribe((data) => { });

  }

  public SetCustomTags(tags) {

    this.tags = tags;

  }

  public onCommentEdit(c) {

    this.EditMode = true;
    this.EditChanges = {};
    this.prepareEditComment(c);

  }

  public confirmExit(e) {

    if (!this.EnablePublish) return;

    var message = "Your confirmation message goes here.",
      e = e || window.event;
    if (e) {
      e.returnValue = message;
    }

    return message;
  }


  public ResolveAssignFramework(flag, is_internal?) {

    if (flag == 0) {

      this.modalRef.hide();

    } else {

      if (!is_internal) {
        this.modalRef.hide();
      }

      let obj = {
        huddle_id: this.params.huddle_id,
        video_id: this.params.video_id,
        framework_id: this.VideoHuddleFramework
      }
      this.mainService.SetFrameworkForVideo(obj).subscribe((data: any) => {

        if (!data.status) {

          if (!is_internal) {
            this.toastr.ShowToastr('info',this.translation.vd_framework_selected_for_video);
          }

          let video_framework_id = data.video_framework_id;
          this.GetRubricById(video_framework_id, true);
          this.permissions.framework_selected_for_video = "1";

        } else {

          this.rubrics = this.rubricPreview;
          this.permissions.framework_selected_for_video = "1";
        }

      });

    }

  }

  public AssignFramework(template, flag) {

    if (flag == 0) {

      this.rubricPreview = "";
      this.VideoHuddleFramework = this.VideoInfo.defaultFramework;
      this.GetRubricById(this.VideoInfo.defaultFramework);
      return;

    } else if (flag == 1) {

      this.modalRef = this.modalService.show(template, { class: 'modal-md' });

    }

  }

  public OnCommentTimeChange(val, flag) {
    if (flag == 's') {

      if (val > 59) {
        this.VideoCurrentTime[1]++;
        this.VideoCurrentTime[2] = 0;
        if (this.VideoCurrentTime[1] >= 59) {
          this.OnCommentTimeChange(this.VideoCurrentTime[1], 'm');
        }
      }

    } else if (flag == 'm') {
      if (val > 59) {
        this.VideoCurrentTime[0]++;
        this.VideoCurrentTime[1] = 0;
      }
    } else if (flag == 'h') {

    }
  }

  private prepareEditComment(comment) {

    this.EditChanges = { changed_standards: false, changed_custom_markers: false };
    this.AttachFilesToComment(comment);
    this.EditableComment = comment;
    this.newComment.timeEnabled = comment.time >= 0;
    this.newComment.commentText = comment.comment;
    this.videoOldTime = this.ConvertTime(comment.time).split(":");

    this.VideoCurrentTime = this.ConvertTime(comment.time);
    if (this.VideoCurrentTime == "All Video") {
      this.newComment.timeEnabled = false;
    }
    this.VideoCurrentTime = this.VideoCurrentTime == "All Video" ? this.VideoCurrentTime : this.VideoCurrentTime.split(":");
    this.newComment.files = comment.files;
    window.scrollTo(0, document.body.scrollHeight - 30);

    this.tags = [];

    this.selectedRubrics = [];

    comment.default_tags.forEach((dt) => {

      let index = _.findIndex(this.CustomMarkers, { account_tag_id: dt.account_tag_id });

      if (index >= 0) {

        this.selectedTag = this.CustomMarkers[index];

      } else {
        this.perpareEditableTag(dt);
        this.selectedTag = null;
      }

    });
    if(comment.default_tags.length == 0) {
      this.selectedTag = null;
    }

    comment.standard.forEach((c) => {

      let index = _.findIndex(this.rubrics.account_tag_type_0, { account_tag_id: c.account_tag_id });

      if (index > -1) {

        this.rubrics.account_tag_type_0[index].selected = true;
        this.selectedRubrics.push(this.rubrics.account_tag_type_0[index]);

      }

    });

  }

  private perpareEditableTag(tag) {
    this.tags.push({ text: tag.tag_title, id: this.tags.length + 1 });

  }

  public TriggerTextChange(ev) {
    if (this.settings.PauseWhileTyping) {

      this.playerService.ModifyPlayingState("pause");

    }
    if (ev.keyCode == 13 && this.settings.EnterToPost) {

      ev.preventDefault();

      if (!this.EditMode) {
        this.AddTextComment();
      } else {
        this.EditTextComment(1);
      }


    }


  }
  public allowToComment() {

    let tags = this.GetCustomTags();
    let result = true;
    if (this.newComment.commentText && (!this.newComment.commentText.trim() || this.newComment.commentText.trim() == ""))
      result = true;

    if ((this.newComment.commentText && (this.newComment.commentText.trim() || (this.newComment.commentText.trim() != "" && this.newComment.commentText.trim() != undefined))))
      result = false;

    if (!!!_.isEmpty(this.selectedTag) || (!_.isEmpty(tags)) || !!(this.newComment.files && this.newComment.files.length > 0) || !!(this.selectedRubrics.length > 0))
      result = false;

    return result;


  }

  public AddTextComment() {
    let allowComment = this.allowToComment()
    if (allowComment) {

      this.toastr.ShowToastr('info',this.translation.please_enter_text_to_comment);
      return;

    } else {

      let sessionData: any = this.headerService.getStaticHeaderData();
      let user_id = sessionData.user_current_account.User.id;
      let comment_time = moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS")

      let obj = {
        videoId: this.params.video_id,
        for: (this.newComment.timeEnabled) ? "synchro_time" : "",
        synchro_time: (this.newComment.timeEnabled) ? Math.floor(this.currentTimeInSeconds) : '',
        time: (this.newComment.timeEnabled) ? Math.floor(this.currentTimeInSeconds) : 0,
        ref_type: '2',
        comment: this.newComment.commentText || "",
        user_id: user_id,
        standards_acc_tags: this.PrepareRubrics(),
        default_tags: this.GetCustomTags(),
        assessment_value: (!_.isEmpty(this.selectedTag)) ? "# " + this.selectedTag.tag_title : "",
        fake_id: this.GetFakeId(),
        first_name: sessionData.user_current_account.User.first_name,
        last_name: sessionData.user_current_account.User.last_name,
        company_name: sessionData.user_current_account.accounts.company_name,
        image: sessionData.user_current_account.User.image,
        account_role_id: sessionData.user_current_account.users_accounts.role_id,
        current_user_email: sessionData.user_current_account.User.email,
        created_at_gmt: comment_time

      };

      let files = [];

      if (this.newComment.files && this.newComment.files.length > 0) {


        files = this.newComment.files;


      }

      if (this.settings.PauseWhileTyping) {

        this.playerService.PlayerPlayingState.emit("play");

      }

      this.ActivateTab(0);
      this.comments.unshift(this.PrepareFakeComment(obj, sessionData));
      if (this.commentsSortBy != 0) this.ApplySettings({ sortBy: this.commentsSortBy });
      this.ResetForm();
      this.active_check(0);
      

      this.appMainService.AddComment(obj).subscribe((data: any) => {
        if (data.status == "success") {

          /** this logic has been moved to socket "comment_added" start */




          /** this logic has been moved to socket "comment_added" end */

          if (files.length > 0) {

            this.AddTimeStampToResources(files, true, obj.time, data[0].id);

          }

        } else {
          this.toastr.ShowToastr('info',this.translation.something_went_wrong_msg);
        }

      });
    }

  }
  findNegativeNumber(timeArray) {
    if (timeArray == "All Video") return false;

    let a = timeArray;
    let result = false;
    for (let index = 0; index < a.length; index++) {
      const element = a[index];
      if (parseInt(element) < 0) {
        this.toastr.ShowToastr('info',this.translation.vd_please_enter_valid_number)
        result = true;
      }

    }
    return result;
  }

  calculateSeconds(timeArray) {
    if (timeArray == "All Video") return true;

    let a = timeArray;

    let newTime = parseInt(a[0]) * 3600;
    newTime += parseInt(a[1]) * 60;
    newTime += parseInt(a[2]);

    return newTime;
  }
  public EditTextComment(flag) {

    if (flag == 0) {
      this.ResetForm();
      return;
    }

    let allowComment = this.allowToComment();
    if (allowComment) {

      this.toastr.ShowToastr('info',this.translation.please_enter_text_to_comment);
      return;

    } else {




      if (this.newComment.files && this.newComment.files.length > 0) {

        this.newComment.files = this.newComment.files.filter((f) => { return !f.id; });

        this.AddTimeStampToResources(this.newComment.files, true, this.EditableComment.time, this.EditableComment.id);

      }

      let sessionData: any = this.headerService.getStaticHeaderData();
      let user_id = sessionData.user_current_account.User.id;
      let account_id = sessionData.user_current_account.accounts.account_id;


      var newTime = this.calculateSeconds(this.VideoCurrentTime);
      var oldTime = this.calculateSeconds(this.videoOldTime);
      var isNagative = this.findNegativeNumber(this.VideoCurrentTime)
      let huddle_id = this.params.huddle_id;

      if (!isNagative) {
        if (oldTime < newTime || !newTime) {
          if (!newTime)
            this.toastr.ShowToastr('info',this.translation.vd_please_enter_valid_number)
          else
            this.toastr.ShowToastr('info',this.translation.comment_time_ahead);
        }
        else {
          let comment_time = moment().utc().format("YYYY-MM-DD hh:mm:ss.SSS")

          let obj = {
            huddle_id: huddle_id,
            account_id: account_id,
            comment_id: this.EditableComment.id,
            videoId: this.params.video_id,
            for: (this.newComment.timeEnabled) ? "synchro_time" : "",
            synchro_time: (this.newComment.timeEnabled) ? Math.floor(this.FormatToSeconds(this.VideoCurrentTime.join(":"))) : '',
            time: (this.newComment.timeEnabled) ? Math.floor(this.FormatToSeconds(this.VideoCurrentTime.join(":"))) : 0,

            ref_type: '2',
            comment: this.newComment.commentText,
            user_id: user_id,
            standards_acc_tags: this.PrepareRubrics(),
            default_tags: this.GetCustomTags(),
            assessment_value: (!_.isEmpty(this.selectedTag)) ? "# " + this.selectedTag.tag_title : "",
            account_role_id: sessionData.user_current_account.users_accounts.role_id,
            current_user_email: sessionData.user_current_account.User.email,
            changed_standards: this.EditChanges.changed_standards,
            changed_custom_markers: this.EditChanges.changed_custom_markers,
            created_at_gmt: comment_time
          };

          this.ResetForm();
          this.appMainService.EditComment(obj).subscribe((data: any) => {

            if (data.status == "success") {
              /** this logic has been moved to comment_edited socket start */


              /** this logic has been moved to comment_edited socket end */

            } else {
              this.toastr.ShowToastr('info',this.translation.something_went_wrong_msg);
            }

          });
          this.ActivateTab(0);
        }
      }
    }

  }

  private ResetForm() {

    this.newComment.commentText = "";
    this.newComment.files = [];
    this.selectedRubrics = [];
    this.mainService.ResetCustomTags();
    this.mainService.ResetSelectedRubrics();
    this.selectedTag = {};
    this.EditMode = false;

  }

  private GetCustomTags() {
    if (!this.tags || this.tags.length == 0) return "";
    let arr = [];
    this.tags.forEach((t) => { arr.push(t.text) });
    return arr.join();
  }
  private PrepareFakeComment(comment, sessionData) {

    let fake_comment = {
      "valid": true,
      "fake_id": comment.fake_id,
      "id": -1,
      "parent_comment_id": comment.parent_comment_id ? comment.parent_comment_id : null,
      "title": null,
      "comment": comment.comment,
      "ref_type": comment.ref_type,
      "ref_id": -1,
      "user_id": comment.user_id,
      "time": comment.time,
      "restrict_to_users": 0,
      "created_by": comment.user_id,
      "created_date": "2017-01-23 06:52:14",
      "last_edit_by": -1,
      "last_edit_date": "2017-02-21 20:42:13",
      "active": "1",
      "audio_duration": 0,
      "published_by": null,
      "first_name": sessionData.user_current_account.User.first_name,
      "last_name": sessionData.user_current_account.User.last_name,
      "image": sessionData.user_current_account.User.image,
      "Comment": {
        "last_edit_date": "2017-02-21T20:42:13+00:00",
        "created_date": "2017-01-23T06:52:14+00:00"
      },
      "created_date_string": "Just Now",
      "standard": this.selectedRubrics,
      "default_tags": [
        this.selectedTag
      ]
    };

    return fake_comment;

  }

  private PrepareSocketComment(comment) {

    let fake_comment = {
      "valid": true,
      "fake_id": comment.fake_id,
      "id": comment.id,
      "parent_comment_id": comment.parent_comment_id ? comment.parent_comment_id : null,
      "title": null,
      "comment": comment.comment,
      "ref_type": comment.ref_type,
      "user_id": comment.user_id,
      "time": comment.time,
      "restrict_to_users": 0,
      "created_by": comment.user_id,
      "created_date": "2017-01-23 06:52:14",
      "last_edit_by": -1,
      "last_edit_date": "2017-02-21 20:42:13",
      "active": "1",
      "audio_duration": 0,
      "published_by": null,
      "first_name": comment.first_name,
      "last_name": comment.last_name,
      "image": comment.image,
      "Comment": {
        "last_edit_date": "2017-02-21T20:42:13+00:00",
        "created_date": "2017-01-23T06:52:14+00:00"
      },
      "created_date_string": "Just Now",
      "standard": this.selectedRubrics,
      "default_tags": [
        this.selectedTag
      ]
    };

    return fake_comment;

  }

  private PrepareEditSocketComment(comment) {
    let fake_comment = {
      "valid": true,
      "id": comment.id,
      "parent_comment_id": comment.parent_comment_id ? comment.parent_comment_id : null,
      "title": null,
      "comment": comment.comment,
      "ref_type": comment.ref_type,
      "user_id": comment.user_id,
      "time": comment.time,
      "restrict_to_users": 0,
      "created_by": comment.user_id,
      "created_date": "2017-01-23 06:52:14",
      "last_edit_by": -1,
      "last_edit_date": "2017-02-21 20:42:13",
      "active": "1",
      "audio_duration": 0,
      "published_by": null,
      "first_name": comment.first_name,
      "last_name": comment.last_name,
      "image": comment.image,
      "Comment": {
        "last_edit_date": "2017-02-21T20:42:13+00:00",
        "created_date": "2017-01-23T06:52:14+00:00"
      },
      "created_date_string": "Just Now",
      "standard": this.selectedRubrics,
    };

    return fake_comment;

  }

  private GetFakeId() {

    return ++this.fakeCommentCount;


  }

  private PrepareRubrics() {

    if (!this.selectedRubrics || this.selectedRubrics.length == 0) return "";

    let ret = [];

    this.selectedRubrics.forEach((r) => { ret.push(r.account_tag_id); });

    return ret.join(",");
  }

  public ConvertTime(n) {

    if (!n || n == null || n == 0) return this.translation.vd_all_videos;
    let sec_num: any = parseInt(n, 10);
    let hours: any = Math.floor(sec_num / 3600);
    let minutes: any = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds: any = sec_num - (hours * 3600) - (minutes * 60);

    if (hours == 0 && minutes == 0 && seconds == 0) {
      return this.translation.vd_all_videos;
    }

    if (hours < 10) { hours = "0" + hours; }
    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return hours + ':' + minutes + ':' + seconds;
  }

  public closeInfo(changeTabe = 0, changeToTab = 0, fromTab = 0) {
    this.ShowInfo = false;
    if (changeTabe) {
      if (fromTab == this.currentTab) {
        this.ActivateTab(changeToTab);
      }
    }
  }


  device_detector() {
    let huddle_id = this.params.huddle_id;
    let video_id = this.params.video_id;
    let sessionData: any = this.headerService.getStaticHeaderData();
    let account_id = sessionData.user_current_account.accounts.account_id;
    const isMobile = this.deviceService.isMobile();
    if (isMobile) {
      location.href = "sibme://play_video/?huddleID=" + huddle_id + "&videoID=" + video_id + "&isWorkspace=false&account_id=" + account_id;
    }

  }

  private socketPushFunctionComment() {
    if (this.params.huddle_id) {
      this.subscriptions.add(this.socketService.pushEventWithNewLogic(`huddle-details-${this.params.huddle_id}`).subscribe(data => {
          this.processEventSubscriptions(data);
        })
      );
    }

    this.subscriptions.add(this.socketService.pushEventWithNewLogic(`video-details-${this.params.video_id}`).subscribe(data => {
        this.processEventSubscriptions(data);
      })
    )

    let workspaceChannelName = `workspace-${this.header_data.user_current_account.users_accounts.account_id}-${this.header_data.user_current_account.users_accounts.user_id}`;
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(workspaceChannelName).subscribe(data => {
        this.processEventSubscriptions(data);
      })
    )
  }

  private processEventSubscriptions(res) {
    // console.log('socket res: ', res);

    switch (res.event) {
      case "comment_added":
        this.processCommentAdded(res.data, res.from_cake, res.reference_id);
        break;
      case "comment_edited":
        this.processCommentEdited(res.data, res.from_cake, res.reference_id);
        break;
      case "comment_deleted":
        this.processCommentDeleted(res.parent_comment_id, res.item_id, res.reference_id);
        break;
      case "resource_added":
        this.processResourceAdded(res.data, res.reference_id);
        break;
      case "attachment_deleted":
        this.processAttachmentDeleted(res.data, res.huddle_id);
        break;
      case "framework_selected":
        this.processFrameworkSelected(res.data);
        break;
      case "feedback_published":
        this.processFeedbackPublished(res.data);
        break;
      case "ratings_updated":
        this.processRatingsUpdated(res.data, res.document_id, res.huddle_id);
        break;
      case "resource_renamed":
        this.processResourceRenamed(res.data, res.is_dummy, res);
        break;
      case "live_screen_sharing":
        if(res.sharing_screen == 0) this.millicastLiveStreaming(this.millicastService.streamName);
        else if(res.sharing_screen == 1) this.millicastLiveStreamingSS(res.file_name_recorded_video);
        break;
    }

  }

  private processFeedbackPublished(videoId) {

    if (this.params.video_id == videoId) {
      this.EnablePublish = false;
      this.toastr.ShowToastr('info',this.translation.feedback_published);
      let account_id = this.header_data.user_current_account.accounts.account_id;
      let user_id = this.header_data.user_current_account.User.id;

      const paramsObj = { video_id: this.params.video_id, huddle_id: this.params.huddle_id, user_id: user_id };
      this.mainService.GetVideoResources(paramsObj).subscribe((data: any) => {

        if (data) {
          data.map((d) => {
            if (d.scripted_current_duration) d.time = this.ConvertTime(d.scripted_current_duration);
            else d.time = this.translation.vd_all_videos;
          });
          this.staticFiles = data;
        }

        const commentParamsObj = {
          "user_id": user_id,
          "video_id": this.params.video_id,
          "account_id": account_id,
          "huddle_id": this.params.huddle_id,
          "role_id": this.header_data.user_current_account.roles.role_id,
          "permission_maintain_folders": this.header_data.user_permissions.UserAccount.permission_maintain_folders
        }
        this.mainService.GetVideo(commentParamsObj).subscribe((data: any) => {

          this.comments = [...data.comments.Document.comments];
          this.comments.map(comment => {
            comment.valid = true;
          });
          this.ApplySettings({ sortBy: 0 });
          this.prepareComments(this.comments);
          this.totals.comment_count = data.comments_counts.total_comments;
          this.totals.resources_count = data.attached_document_numbers;
        });

      });
    }
  }

  private processCommentAdded(comment, from_cake, reference_id) {
    if (reference_id == this.params.video_id) {
      let commentObj;
      if (from_cake) commentObj = JSON.parse(comment);
      else commentObj = comment;

      if (commentObj.created_by == this.header_data.user_current_account.User.id || commentObj.active == 1) {

        /* comment add section start */
        if (!commentObj.parent_comment_id) {
          commentObj.valid = true;
          this.LoadPublishSettings();
          this.totals.comment_count++;
          this.mainService.CommentAddedNotification.emit(commentObj);
          let commentIndex = this.comments.findIndex(c => c.fake_id == commentObj.fake_id);
          delete commentObj.fake_id;
          if (commentIndex > -1 && !from_cake) {
            this.comments[commentIndex] = commentObj;
          }
          else this.comments.unshift(commentObj);
          this.LoadRubricsTabData();

          /* comment add section end */
        } else {

          /* reply add section start */
          let found = false;
          let commentIndex = this.comments.findIndex(c => c.id == commentObj.parent_comment_id);

          if (commentIndex > -1) {
            found = true;

            this.comments[commentIndex].replyEnabled = false;
            this.comments[commentIndex].replyAdding = true;
            this.comments[commentIndex].replyAdding = false;
            this.comments[commentIndex].replyText = "";

            let replyIndex = this.comments[commentIndex].Comment.responses.findIndex(r => r.uuid == commentObj.uuid);
            if (replyIndex > -1 && !from_cake) {
              this.comments[commentIndex].Comment.responses[replyIndex] = commentObj;
              delete this.comments[commentIndex].Comment.responses[replyIndex].fakeComment;
            } else {
              if (!this.comments[commentIndex].Comment) this.comments[commentIndex].Comment = {};
              if (!this.comments[commentIndex].Comment.responses) this.comments[commentIndex].Comment.responses = [];
              this.comments[commentIndex].Comment.responses.push(commentObj);
            }
            this.totals.comment_count++;
            /* reply add section end */
          }

          /* sub-reply add section start */
          if (!found) {
            this.comments.forEach((c) => {

              if (c.Comment && c.Comment.responses && c.Comment.responses.length > 0) {
                let replyIndex = c.Comment.responses.findIndex(response => response.id == commentObj.parent_comment_id);
                if (replyIndex > -1) {
                  c.Comment.responses[replyIndex].replyText = "";

                  let subReplyIndex = c.Comment.responses[replyIndex].responses.findIndex(sr => sr.uuid == commentObj.uuid);
                  if (subReplyIndex > -1 && !from_cake) {
                    c.Comment.responses[replyIndex].responses[subReplyIndex] = commentObj;
                    delete c.Comment.responses[replyIndex].responses[subReplyIndex].fakeComment;
                    delete c.Comment.responses[replyIndex].responses[subReplyIndex].tryAgain;
                  } else {
                    if (!c.Comment.responses[replyIndex].responses) c.Comment.responses[replyIndex].responses = [];
                    c.Comment.responses[replyIndex].responses.push(commentObj);
                  }
                  this.totals.comment_count++;
                }
              }

            });

          }
          /* sub-reply add section end */
        }
        if (this.commentsSortBy != 0) this.ApplySettings({ sortBy: this.commentsSortBy });
      }
    }
  }

  private processCommentEdited(comment, from_cake, reference_id) {
    if (reference_id == this.params.video_id) {
      let commentObj;
      if (from_cake) commentObj = JSON.parse(comment);
      else commentObj = comment;

      /* comment edit section start */
      if (!commentObj.Comment.parent_comment_id) {

        let commentIndex = this.comments.findIndex(c => c.id == commentObj.id);
        if (commentIndex > -1) {
          commentObj.valid = true;
          this.comments[commentIndex] = commentObj;
        }
        this.AttachFilesToComment(commentObj);
        this.mainService.ReRenderMarkers.emit(true);
        this.LoadRubricsTabData(true);

        /* comment edit section end */

      } else {

        /* reply edit section start */
        let found = false;
        let commentIndex = this.comments.findIndex(c => c.id == commentObj.Comment.parent_comment_id);

        if (commentIndex > -1) {

          let replyIndex = this.comments[commentIndex].Comment.responses.findIndex(response => response.id == commentObj.id);

          if (replyIndex > -1) {
            found = true;
            this.comments[commentIndex].Comment.responses[replyIndex].comment = commentObj.Comment.comment;
            this.comments[commentIndex].Comment.responses[replyIndex].EditEnabled = false;
            this.mainService.ReRenderMarkers.emit(true);
          }

        }
        /* reply edit section end */

        /* sub-reply edit section start */
        if (!found) {

          this.comments.forEach((c) => {

            if (c.Comment.responses) {

              c.Comment.responses.forEach((r) => {

                if (r.responses) {
                  let subReplyIndex = r.responses.findIndex(response => response.id == commentObj.id);
                  if (subReplyIndex > -1) {
                    r.responses[subReplyIndex].comment = commentObj.Comment.comment;
                    r.responses[subReplyIndex].EditEnabled = false;
                  }
                }

              });

            }

          });

        }
        /* sub-reply edit section end */
      }

      this.toastr.ShowToastr('success',this.translation.note_updated);
    }
  }

  private processCommentDeleted(parent_comment_id, item_id, reference_id) {

    if (reference_id == this.params.video_id) {

      this.LoadPublishSettings();

      /** Comment delete section start */
      if (!parent_comment_id) {
        let commentIndex = this.comments.findIndex(c => c.id == item_id);
        if (commentIndex > -1) {
          this.comments.splice(commentIndex, 1);
          this.toastr.ShowToastr('info',this.translation.note_deleted);
        }
        /** Comment delete section end */

      } else {

        /** Reply delete section start */
        let found = false;
        let commentIndex = this.comments.findIndex(c => c.id == parent_comment_id);

        if (commentIndex > -1 && Array.isArray(this.comments[commentIndex].Comment.responses)) {

          let replyIndex = this.comments[commentIndex].Comment.responses.findIndex(response => response.id == item_id);
          if (replyIndex > -1) {
            found = true;
            this.comments[commentIndex].Comment.responses.splice(replyIndex, 1);
            this.toastr.ShowToastr('info',this.translation.note_deleted);
          }

        }
        /** Reply delete section end */

        /** Sub-reply delete section start */
        if (!found) {

          this.comments.forEach((c) => {

            if (Array.isArray(c.Comment.responses)) {

              c.Comment.responses.forEach((r) => {

                if (Array.isArray(r.responses)) {
                  let subReplyIndex = r.responses.findIndex(response => response.id == item_id);
                  if (subReplyIndex > -1) {
                    r.responses.splice(subReplyIndex, 1);
                    this.toastr.ShowToastr('info',this.translation.note_deleted);
                  }
                }

              });

            }

          });

        }
        /** Sub-reply delete section end */

      }

      this.updateCommentCount();
      this.mainService.ReRenderMarkers.emit(true);
    }

  }

  private processResourceAdded(resource, reference_id) {
    if (reference_id == this.params.video_id) {
      resource.id = Number(resource.doc_id);
      resource.time = (resource.scripted_current_duration) ? this.ConvertTime(resource.scripted_current_duration) : this.translation.all_video;

      if (!resource.comment_id) {
        this.staticFiles.push(resource);
        this.toastr.ShowToastr('info',this.translation.resource_uploaded);
      } else {
        let commentExisted = this.comments.find(comment => comment.id == resource.comment_id);
        if (commentExisted) {
          this.staticFiles.push(resource);
          this.AttachFilesToCommentsOnResourceAddedSocket();
        }

      }
    }

  }

  private processAttachmentDeleted(deletedResourceId, huddle_id) {
    if (huddle_id == this.params.huddle_id) {
      let index = this.staticFiles.findIndex(file => file.id == deletedResourceId);
      if (index >= 0) {
        this.toastr.ShowToastr('info',this.translation.resource_deleted);
        this.staticFiles.splice(index, 1);
        this.totals.resources_count--;
        this.AttachFilesToCommentsOnResourceAddedSocket();
      }
    }
  }

  private AttachFilesToCommentsOnResourceAddedSocket() {

    this.comments.map(comment => {

      comment.files = [];
      comment.files = this.staticFiles.filter(file => file.comment_id == comment.id);
      comment.files.map((file) => {
        file.time = (file.scripted_current_duration) ? this.ConvertTime(file.scripted_current_duration) : this.translation.all_video;
      });
    });

  }

  private processFrameworkSelected(frameworkId) {

    this.GetRubricById(frameworkId, true)
    this.permissions.framework_selected_for_video = "1";
    this.toastr.ShowToastr('info',this.translation.framework_selected);
  }

  private processRatingsUpdated(data, document_id, huddle_id) {
    if (document_id == this.params.video_id && huddle_id == this.params.huddle_id) {
      let selectedStandardId = data.input_data.standard_ids[0];
      let selectedRatingId = data.input_data[`rating_id_${selectedStandardId}`];
      this.LoadRubricsTabData(true, selectedStandardId, selectedRatingId);
    }
  }

  private processResourceRenamed(data, is_dummy, res?) {
    if (data.doc_type == '1' && (data.id == this.params.video_id || data.doc_id == this.params.video_id)) {
      this.VideoInfo.title = data.title;
      this.mainService.updateVideoTitle(data.title);
      if (res && res.live_video_event && res.live_video_event == "1" && !this.live_streamer) {
        this.toastr.ShowToastr('info',this.translation.live_recording_stopped_redirect);
        setTimeout(() => {
          this.folderId
          ? this.router.navigate([`/video_huddles/huddle/details/${this.account_folder_id}/artifacts/grid/${this.folderId}`]) 
          : this.router.navigate([`/video_huddles/huddle/details/${this.account_folder_id}/artifacts/grid`]);
        }, 4000);
      }
    }
  }

  private millicastLiveStreaming(data: any) {  
    this.millicastService.getJWTAndSocketUrlSub(data).then((res: any) => {
      if (res.status === 200) {
        this.windowRef['rtcPeerConnectionSub'] = new RTCPeerConnection({ iceServers: this.millicastService.iceServers, rtcpMuxPolicy : "require", bundlePolicy: "max-bundle"});

        // Listen for track once it starts playing.
        this.windowRef['rtcPeerConnectionSub'].ontrack = async (event) => {   
          console.log("event.streams[0]: ", event.streams[0])     
          let vidWin = document.getElementById('cam-view-live-stream');
          console.log("vidWin: ", vidWin)  
          vidWin.srcObject = event.streams[0];
          vidWin.controls = true;
        };
        
        const cameraWS = new WebSocket(`${res.wsUrl}?token=${res.jwt}`);
        cameraWS.onopen = () => {
          //Create dummy stream
          const stream = new MediaStream();
          //if this is supported
          if (this.windowRef['rtcPeerConnectionSub'].addTransceiver){            
            //Create all the receiver tracks
            this.windowRef['rtcPeerConnectionSub'].addTransceiver("audio",{
              direction : "recvonly",
              streams : [stream]
            });
            this.windowRef['rtcPeerConnectionSub'].addTransceiver("video",{
              direction : "recvonly",
              streams : [stream]
            });
          }
         
          this.windowRef['rtcPeerConnectionSub'].createOffer({ offerToReceiveAudio: true, offerToReceiveVideo: true }).then(desc => {
            this.windowRef['rtcPeerConnectionSub'].setLocalDescription(desc).then(() => {
              //set required information for media server.
              const payloadData = { name: this.millicastService.streamName, sdp: desc.sdp };
              //create payload
              const payload = { type: "cmd", transId: 0, name: 'view', data: payloadData };
              cameraWS.send(JSON.stringify(payload));
            }).catch(err => console.log('setLocalDescription failed: ', err))
          }).catch(err => console.log('createOffer Failed: ', err));
        }

        cameraWS.addEventListener('message', evt => {
          const msg = JSON.parse(evt.data);
          switch (msg.type) {
            case "response":
              const data = msg.data;
              const answer = new RTCSessionDescription({ type: 'answer', sdp: `${data.sdp}a=x-google-flag:conference\r\n` });
              this.windowRef['rtcPeerConnectionSub'].setRemoteDescription(answer)
                .then(() => console.log('setRemoteDescription Success! '))
                .catch(e => console.log('setRemoteDescription failed: ', e));
          }
        });
      }
    });    
  }

  private millicastLiveStreamingSS(data: any) {  
   this.millicastService.getJWTAndSocketUrlSub(this.millicastService.streamName).then((res: any) => {
      if (res.status === 200) {
        this.windowRef['rtcPeerConnectionSubGetAudio'] = new RTCPeerConnection({ iceServers: this.millicastService.iceServers, rtcpMuxPolicy : "require", bundlePolicy: "max-bundle"});

        // Listen for track once it starts playing.
        this.windowRef['rtcPeerConnectionSubGetAudio'].ontrack = async (event) => {
          this.mainAudioStream = event.streams[0].getAudioTracks();
        };
        
        const cameraWS = new WebSocket(`${res.wsUrl}?token=${res.jwt}`);
        cameraWS.onopen = () => {
          this.windowRef['rtcPeerConnectionSubGetAudio'].createOffer({ offerToReceiveAudio: true, offerToReceiveVideo: true }).then(desc => {
            this.windowRef['rtcPeerConnectionSubGetAudio'].setLocalDescription(desc).then(() => {
              //set required information for media server.
              const payloadData = { name: this.millicastService.streamName, sdp: desc.sdp };
              //create payload
              const payload = { type: "cmd", transId: 0, name: 'view', data: payloadData };
              cameraWS.send(JSON.stringify(payload));
            }).catch(err => console.log('setLocalDescription failed: ', err))
          }).catch(err => console.log('createOffer Failed: ', err));
        }

        cameraWS.addEventListener('message', evt => {
          const msg = JSON.parse(evt.data);
          switch (msg.type) {
            case "response":
              const data = msg.data;
              const answer = new RTCSessionDescription({ type: 'answer', sdp: `${data.sdp}a=x-google-flag:conference\r\n` });
              this.windowRef['rtcPeerConnectionSubGetAudio'].setRemoteDescription(answer)
                .then(() => console.log('setRemoteDescription Success! '))
                .catch(e => console.log('setRemoteDescription failed: ', e));
          }
        });
      }
    });    

    this.millicastService.getJWTAndSocketUrlSub(data).then((res: any) => {
      if (res.status === 200) {
        this.windowRef['rtcPeerConnectionSubSS'] = new RTCPeerConnection({ iceServers: this.millicastService.iceServers, rtcpMuxPolicy : "require", bundlePolicy: "max-bundle"});

        // Listen for track once it starts playing.
        this.windowRef['rtcPeerConnectionSubSS'].ontrack = async (event) => {
          let vidWin = document.getElementById('cam-view-live-stream');
          const tracks = [
            ...event.streams[0].getVideoTracks(), 
            ...this.mainAudioStream
          ];
          let stream = new MediaStream(tracks);
          vidWin.srcObject = stream;
          vidWin.controls = true;
        };
        
        const cameraWS = new WebSocket(`${res.wsUrl}?token=${res.jwt}`);
        cameraWS.onopen = () => {
          this.windowRef['rtcPeerConnectionSubSS'].createOffer({ offerToReceiveAudio: true, offerToReceiveVideo: true }).then(desc => {
            this.windowRef['rtcPeerConnectionSubSS'].setLocalDescription(desc).then(() => {
              //set required information for media server.
              const payloadData = { name: this.millicastService.streamName, sdp: desc.sdp };
              //create payload
              const payload = { type: "cmd", transId: 0, name: 'view', data: payloadData };
              cameraWS.send(JSON.stringify(payload));
            }).catch(err => console.log('setLocalDescription failed: ', err))
          }).catch(err => console.log('createOffer Failed: ', err));
        }

        cameraWS.addEventListener('message', evt => {
          const msg = JSON.parse(evt.data);
          switch (msg.type) {
            case "response":
              const data = msg.data;
              const answer = new RTCSessionDescription({ type: 'answer', sdp: `${data.sdp}a=x-google-flag:conference\r\n` });
              this.windowRef['rtcPeerConnectionSubSS'].setRemoteDescription(answer)
                .then(() => console.log('setRemoteDescription Success! '))
                .catch(e => console.log('setRemoteDescription failed: ', e));
          }
        });
      }
    });  
  }

  getNewserverresponse(path) {

    var xhttp = new XMLHttpRequest();
    let that = this;
    xhttp.onreadystatechange = function () {

      if (this.readyState == 4) {
        let firstindex = this.responseURL.indexOf(".");
        let lastindex = this.responseURL.lastIndexOf(":")
        that.baseUrl = this.responseURL.substring(firstindex - 1, lastindex);
        that.url_updated = true;
        let liveVideoUrl = `http://${that.baseUrl}:1935/liveEdge/smil:${that.VideoInfo.stream_name}.smil/playlist.m3u8`;
        that.src.path = liveVideoUrl;
      }
    };
    xhttp.open("GET", path, true);
    xhttp.send();

  }

  private updateBreadCrumb(title: string, huddleInfo?: any) {
    const breadCrumb: BreadCrumbInterface[] = [
      { title: this.translation.huddle_breadcrumb_huddle, link: '/video_huddles/list' },
      { title: huddleInfo.name, link: `/video_huddles/huddle/details/${huddleInfo.id}` },
      { title }
    ];

    this.activatedRoute.queryParams.subscribe(params => {
      if (params.folderId) {

        let obj: any = {
            folder_id: params.huddleId,
            goal_evidence: params.goalEvidence,
            document_folder_id: params.folderId
          };
        
        const sessionData: any = this.headerService.getStaticHeaderData();

        ({
          User: { id: obj.user_id },
          accounts: { account_id: obj.account_id }
        } = sessionData.user_current_account);

        this.homeService.GetBreadcrumbs(obj).subscribe((data: any) => {
          if (data.success === -1) {
            this.toastr.ShowToastr('info',data.message);

            setTimeout(() => {
              this.router.navigate(['/list']);
            }, 4000);
            this.homeService.updateBreadcrumb(data);

          }
          else {
            const folders = data.folders;
            const docFolder = data.document_folders;

            const tmp = breadCrumb.pop();
            let crumbs = [];

            crumbs = [...folders, ...docFolder];
            const path = [];

            crumbs.forEach((element) => {
                if (!element.location) {
                  path.push({
                    link: `/video_huddles/huddle/details/${params.huddleId}/artifacts/grid/${element.folder_id}`,
                    title: element.folder_name
                  });
                }
            });

            const finalCrumbs = [...breadCrumb, ...path , tmp];

            this.appMainService.updateBreadCrumb(finalCrumbs);

          }
        });
      } else {
          this.appMainService.updateBreadCrumb(breadCrumb);

      }
    });
  }

  private startMilicastBroadcast(streamName: string) {
    this.millicastService.getJWTAndSocketUrl(streamName).then((res: any) => {
      if (res.status === 200) {
        this.windowRef['rtcPeerConnection'] = new RTCPeerConnection({ iceServers: this.millicastService.iceServers, bundlePolicy: "max-bundle" });
        this.windowRef['rtcPeerConnectionSender'] = [];
        var camVideoTrack = this.windowRef['cameraStream'].getVideoTracks()[0];
        var camAudioTrack = this.windowRef['cameraStream'].getAudioTracks()[0];
        this.windowRef['videoSender'] = this.windowRef['rtcPeerConnection'].addTrack(camVideoTrack, this.windowRef['cameraStream']);
        this.windowRef['audioSender'] = this.windowRef['rtcPeerConnection'].addTrack(camAudioTrack, this.windowRef['cameraStream']);
        /*this.windowRef['cameraStream'].getTracks().forEach(track => {
          this.windowRef['rtcPeerConnectionSender'].push(this.windowRef['rtcPeerConnection'].addTrack(track, this.windowRef['cameraStream']))
        });*/
        // console.log("this.windowRef['rtcPeerConnection']", this.windowRef['rtcPeerConnection']);

        const cameraWS = new WebSocket(`${res.wsUrl}?token=${res.jwt}`);
        cameraWS.onopen = () => {
          this.windowRef['rtcPeerConnection'].createOffer({ offerToReceiveAudio: true, offerToReceiveVideo: true }).then(desc => {
            this.liveStreamStarted = true;

            this.windowRef['rtcPeerConnection'].setLocalDescription(desc).then(() => {
              // console.log('setLocalDescription Success !:', this.millicastService.streamName);
              //set required information for media server.
              const payloadData = { name: this.millicastService.streamName, sdp: desc.sdp, codec: 'h264' };
              //create payload
              const payload = { type: "cmd", transId: Math.random() * 10000, name: 'publish', data: payloadData };
              cameraWS.send(JSON.stringify(payload));
              this.headerService.updateLiveStreamStateSource('started');
              let liveStreamData: LiveStreamData;
              liveStreamData =
              this.folderId === 0
              ? {
                  videoId: this.params.video_id, huddleId: this.params.huddle_id, link: `/video_details/live-streaming/${this.params.huddle_id}`,
                  queryParams: { live_streamer: true, huddleName: this.huddleName }
                }
              : {
                videoId: this.params.video_id, huddleId: this.params.huddle_id, link: `/video_details/live-streaming/${this.params.huddle_id}`,
                queryParams: { live_streamer: true, huddleName: this.huddleName, folderId: this.folderId }
              };
              this.headerService.updateLiveStreamDataSource(liveStreamData);
            }).catch(err => console.log('setLocalDescription failed: ', err))
          }).catch(err => console.log('createOffer Failed: ', err));
        }

        cameraWS.addEventListener('message', evt => {
          console.log('cameraWS::message', evt);
          const msg = JSON.parse(evt.data);
          switch (msg.type) {
            case "response":
              const data = msg.data;
              const answer = new RTCSessionDescription({ type: 'answer', sdp: `${data.sdp}a=x-google-flag:conference\r\n` });
              this.windowRef['rtcPeerConnection'].setRemoteDescription(answer)
                .then(() => console.log('setRemoteDescription Success! '))
                .catch(e => console.log('setRemoteDescription failed: ', e));
          }
        });
      }
    })
  }
  private startMilicastBroadcastSS() {  

    if(!this.isSafari) {
      if(!this.isMacOs){  // No need to merge Audio if chrome is on mac
        //Merging MediaUser Audio track and DisplayMedia Audio track for system audio as well on windows chrome
        var audioTrackSS = this.mergeAudioStreams(this.windowRef['screenStream'], this.windowRef['cameraStream']);
        this.newAudioStream = new MediaStream(audioTrackSS);
        var screenAudioTrack = this.newAudioStream.getAudioTracks()[0];
        this.windowRef['audioSender'].replaceTrack(screenAudioTrack);
      } 
      if (this.windowRef['screenStream'].getVideoTracks()[0].getSettings().displaySurface == 'monitor') {
        this.startPicInPicPreview();
      }
    } else {
      this.screenRecordingCamContainer.webkitSetPresentationMode(this.screenRecordingCamContainer.webkitPresentationMode === "picture-in-picture" ? "inline" : "picture-in-picture");
    }

    //Replacing MediaUser Video track by DisplayMedia Video track for screen record
    var screenVideoTrack = this.windowRef['screenStream'].getVideoTracks()[0];
    this.windowRef['videoSender'].replaceTrack(screenVideoTrack);
    this.headerService.updateLiveStreamStateSource('started-screen-sharing');  
  }

  mergeAudioStreams = (desktopStream, mediaStream) => {
    const context = new AudioContext();
    const destination = context.createMediaStreamDestination();
    let hasDesktop = false;
    let hasVoice = false;
    if (desktopStream && this.windowRef['screenStream'].getAudioTracks().length > 0) {
      // If you don't want to share Audio from the desktop it should still work with just the voice.
      const source1 = context.createMediaStreamSource(desktopStream);
      const desktopGain = context.createGain();
      desktopGain.gain.value = 0.7;
      source1.connect(desktopGain).connect(destination);
      hasDesktop = true;
    }
    
    if (mediaStream && this.windowRef['cameraStream'].getAudioTracks().length > 0) {
      const source2 = context.createMediaStreamSource(mediaStream);
      const voiceGain = context.createGain();
      voiceGain.gain.value = 0.7;
      source2.connect(voiceGain).connect(destination);
      hasVoice = true;
    }
      
    return (hasDesktop || hasVoice) ? destination.stream.getAudioTracks() : [];
  };

  private startMilicastBroadcastSS_(streamName: string) {
    this.startPicInPicPreview();

    this.millicastService.getJWTAndSocketUrl(streamName).then((res: any) => {
      if (res.status === 200) {
        this.windowRef['rtcPeerConnectionSS'] = new RTCPeerConnection({ iceServers: this.millicastService.iceServers, bundlePolicy: "max-bundle" });

        this.windowRef['screenStream'].getTracks().forEach(track => {
          this.windowRef['rtcPeerConnectionSenderSS'] = [];
          this.windowRef['rtcPeerConnectionSenderSS'].push(this.windowRef['rtcPeerConnectionSS'].addTrack(track, this.windowRef['screenStream']))
        });

        const screenWS = new WebSocket(`${res.wsUrl}?token=${res.jwt}`);
        screenWS.onopen = () => {
          this.windowRef['rtcPeerConnectionSS'].createOffer({ offerToReceiveAudio: true, offerToReceiveVideo: true }).then(desc => {
            // this.liveStreamStarted = true;

            this.windowRef['rtcPeerConnectionSS'].setLocalDescription(desc).then(() => {
              // console.log('setLocalDescription Success !:', streamName);
              //set required information for media server.
              const payloadData = { name: streamName, sdp: desc.sdp, codec: 'h264' };
              //create payload
              const payload = { type: "cmd", transId: Math.random() * 10000, name: 'publish', data: payloadData };
              screenWS.send(JSON.stringify(payload));
            }).catch(err => console.log('setLocalDescription failed: ', err))
          }).catch(err => console.log('createOffer Failed: ', err));
        }

        screenWS.addEventListener('message', evt => {
          // console.log('screenWS::message', evt);
          const msg = JSON.parse(evt.data);
          switch (msg.type) {
            case "response":
              const data = msg.data;
              const answer = new RTCSessionDescription({ type: 'answer', sdp: `${data.sdp}a=x-google-flag:conference\r\n` });
              this.windowRef['rtcPeerConnectionSS'].setRemoteDescription(answer)
                .then(() => {
                  console.log('setRemoteDescription Success! ')
                  this.headerService.updateLiveStreamStateSource('started-screen-sharing');
                }).catch(e => console.log('setRemoteDescription failed: ', e));
          }
        });
      }
    })
  }

  private showCamView() {
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      const constraints = { audio: true, video: true };
      navigator.mediaDevices.getUserMedia(constraints).then(stream => {
        this.windowRef['cameraStream'] = stream;
        const videoObj: any = document.getElementById('cam-view');
        videoObj.srcObject = this.windowRef['cameraStream'];
        videoObj.muted = true;

        this.screenRecordingCamContainer.srcObject = this.windowRef['cameraStream'];
        this.screenRecordingCamContainer.muted = true;
      }).catch(err => {
        console.error('Could not get Media: ', err);
        this.micCamPermError = true
      });
    } else {
      this.browserSupportRecording = false;
    }

  }

  public save_or_not(template) {
    this.checkIfModalIsOpen = true;
    this.modalRef = this.modalService.show(template, { class: 'modal-md' });
  }

  public save_video() {
    const videoDur = this.headerService.getLiveStreamVideoTimeSource();
    console.log(videoDur);
    
    this.after_saving = false;
    this.modalRef.hide();
    this.stopStreamRecording();
    const data = {
      accountID: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id,
      videoID: this.params.video_id,
      environment_type: 2,
      videoDuration: videoDur,
      huddleID: this.params.huddle_id,
      lang: 'en'
    };

    this.mainService.SaveVideo(data).subscribe(() => {
      this.toastr.ShowToastr('info',this.translation.live_recording_stopped_redirect);
      setTimeout(() => {
        this.folderId
        ? this.router.navigate([`/video_huddles/huddle/details/${this.account_folder_id}/artifacts/grid/${this.folderId}`]) 
        : this.router.navigate([`/video_huddles/huddle/details/${this.account_folder_id}/artifacts/grid`]);
      }, 1000);
    });
  }

  public no_save_video() {
    this.after_saving = false;
    if(this.checkIfModalIsOpen) this.modalRef.hide();
    this.stopStreamRecording();
    const data = {
      accountID: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id,
      video_id: this.params.video_id,
      huddle_id: this.params.huddle_id,
      lang: 'en'
    };

    this.mainService.NoSaveVideo(data).subscribe(() => {
      this.toastr.ShowToastr('info',this.translation.live_recording_stopped_redirect);
      setTimeout(() => {
        this.folderId
        ? this.router.navigate([`/video_huddles/huddle/details/${this.account_folder_id}/artifacts/grid/${this.folderId}`]) 
        : this.router.navigate([`/video_huddles/huddle/details/${this.account_folder_id}/artifacts/grid`]);
      }, 1000);
    });

  }

  public stopStreamRecording() {
    if (this.windowRef['rtcPeerConnection'].connectionState == 'connected') {
      this.headerService.updateLiveStreamStateSource('stopped');
      this.stopLiveRecording();
      this.windowRef['rtcPeerConnectionSender'].forEach(track => this.windowRef['rtcPeerConnection'].removeTrack(track));
      this.windowRef['rtcPeerConnection'].close();
      this.windowRef['cameraStream'].getTracks().forEach(track => track.stop());

      if (this.windowRef['rtcPeerConnectionSS'] && this.windowRef['rtcPeerConnectionSS'].connectionState == 'connected') {
        // this.stopPicInPicPreview();
        this.windowRef['rtcPeerConnectionSenderSS'].forEach(track => this.windowRef['rtcPeerConnectionSS'].removeTrack(track));
        this.windowRef['rtcPeerConnectionSS'].close();
        this.windowRef['screenStream'].getTracks().forEach(track => track.stop());
      }

      const globalCamViewSrcObject = this.screenRecordingCamContainer.srcObject;
      if (globalCamViewSrcObject) {
        // reset global app level cam preview
        const tracks = globalCamViewSrcObject.getTracks();
        tracks.forEach(track => track.stop());
        this.screenRecordingCamContainer.srcObject = null;
        this.stopPicInPicPreview();
      }

    }
    
    if (this.windowRef['liveStreamState'] === 'initialized-screen-sharing' || this.windowRef['liveStreamState'] === 'started-screen-sharing'  || this.windowRef['liveStreamState'] === 'stopped-screen-sharing'  || this.windowRef['liveStreamState'] === 'stopped') {
      this.stopScreenSharing();
    }

    this.headerService.updateLiveStreamStateSource('comfortZone');
  }

  public startTimer() {
    this.goLiveCheck = true;
    this.play = true;
    this.disableGoLive = true;

    const intrvl = interval(1000);
    const waitFor3Seconds = intrvl.pipe(take(3));
    waitFor3Seconds.subscribe(() => this.time--);

    const data: any = {
      LiveRecordingID: 0,
      environment_type: 2,
      videoDuration: 0,
      huddleID: this.params.huddle_id,
      filename: this.millicastService.fileName,
      eventType: 1,
      accountID: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id
    };
    if(this.folderId > 0) {
      data.parent_folder_id = this.folderId
    }

    const createLiveStreamEntry = this.mainService.CreateLiveRecording(data);
    forkJoin([waitFor3Seconds, createLiveStreamEntry]).subscribe((results: any) => {
      const apiRes = results[1];
      this.play = false;
      this.after_saving = true;
      if (apiRes.success == '1') {
        this.millicastService.updateStreamName(apiRes.stream_name);
        this.startMilicastBroadcast(this.millicastService.streamName);
        const newData = data;
        newData.LiveRecordingID = apiRes.video_id;
        newData.eventType = 6;
        this.params.video_id = apiRes.video_id;

         const postData = {
           document_id: apiRes.video_id,
         };
          this.appMainService.liveStreamStatusCheck(postData).subscribe();

          this.mainService.CreateLiveRecording(newData).subscribe((data2) => {
          // console.log('second api call', data2);
          this.device_detector();
          this.handleParams(this.params);
          this.RunSubscribers();

          this.account_folder_id = this.params.huddle_id;

          this.socketPushFunctionComment();

        });

      } else {
        this.toastr.ShowToastr('info',apiRes.message);
        this.disableGoLive = false;
        this.time = 3;
        this.after_saving = false;
      }

    });

  }

  private detectBrowserAndStorePlayableAudioExtensions() {
    // Internet Explorer 6-11
    const isIE = /*@cc_on!@*/false || !!document.documentMode;
    if (isIE) {
      this.browserSupportRecording = false;
    }

    // Edge 20+
    const isEdge = !isIE && !!window.StyleMedia;
    if (isEdge) {
      this.browserSupportRecording = false;
    }

  }

  public stopLiveRecording() {
    const data: any = {
      accountID: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id,
      LiveRecordingID: this.params.video_id,
      environment_type: 2,
      videoDuration: 0,
      huddleID: this.params.huddle_id,
      filename: this.millicastService.fileName,
      eventType: 4
    };
    if(this.folderId > 0) {
      data.parent_folder_id = this.folderId
    }
    this.mainService.CreateLiveRecording(data).subscribe();
  }

  public checkIfLiveStreamingAllowed() {
    const data = {
      account_id: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.User.id,
      site_id: this.header_data.site_id,
      huddle_id: this.params.huddle_id
    };
    this.mainService.liveStreamingAllowed(data).subscribe((data2: any) => {
    // console.log("checkIfLiveStreamingAllowed -> data2", data2)
      if (data2.allowed) {
        this.millicastService.updateFileName(this.makeFileName());
        this.detectBrowserAndStorePlayableAudioExtensions();
        this.loaded = true;
        this.VideoInfo.title = this.millicastService.fileName;
        this.isAuthenticatedUser = true;
        this.updateBreadCrumb(this.VideoInfo.title, { id: this.params.huddle_id, name: this.huddleName });
        this.showCamView();
      } else {
        this.router.navigate(['/page-not-found']);
      }

    });

  }

  public startScreenSharing() {
    this.initializeScreenSharing().then(() => {
      this.startMilicastBroadcastSS();
      /*this.millicastService.generateStreamNameSS();
      this.headerService.updateLiveStreamStateSource('initialized-screen-sharing');

      const data = {
        account_id: this.userCurrAcc.accounts.account_id,
        document_id: this.params.video_id,
        huddle_id: this.params.huddle_id,
        started_at: this.headerService.getLiveStreamVideoTimeSource(),
        file_name_recorded_video: this.millicastService.streamNameSS
      };

      this.mainService.screenShareEvent(data).subscribe((res: any) => {
        if (res.status) {
          this.startMilicastBroadcastSS(this.millicastService.streamNameSS);
        } else {
          console.log('error in creating video for screen sharing');
        }

      });*/
    });

  }

  public stopScreenSharing() {
    if( this.windowRef && this.windowRef['stopScreenRecording']) {
    this.windowRef['stopScreenRecording']();
    }
  }

  private initializeScreenSharing() { 
    return new Promise((resolve, reject) => {

      //System audio is not compatible with MAC. so sending only video param  
      var constraints;
      if(this.isSafari && this.isMacOs) constraints = { video: true };
      if(!this.isSafari && !this.isMacOs) {
        this.toastr.ShowToastr('info',this.translation.ls_select_audio_checkbox_at_end);
        constraints = { video: true, audio: true };
      }
      
      navigator.mediaDevices.getDisplayMedia(constraints).then(screenStream => {
        this.windowRef['screenStream'] = screenStream;
        
        if(!this.isSafari){
          if (this.windowRef['screenStream'].getVideoTracks()[0].getSettings().displaySurface !== 'monitor') {
            this.toastr.ShowToastr('info',this.translation.rs_camera_preview_not_available);
          }
        }
        
        this.addEventListenerForScreenStream(this.windowRef['screenStream']);
        this.windowRef['stopScreenRecording'] = () => this.onStopScreenRecording(this.windowRef['screenStream']);
        resolve(true);
      }, err => {
        console.error('getDisplayMedia error: ', err);
        reject();
      });
    });
  }

  private addEventListenerForScreenStream(screenStream: any) {
    // console.log("addEventListenerForScreenStream: this.windowRef['stopScreenRecording']: ", this.windowRef['stopScreenRecording'])
    screenStream.addEventListener('inactive', () => {
      // console.log("stream is inactive: this.windowRef['stopScreenRecording']: ", this.windowRef['stopScreenRecording'])
      if (this.windowRef['stopScreenRecording']) this.windowRef['stopScreenRecording']();
    }, false);

    screenStream.getTracks().forEach(track => {
      track.addEventListener('ended', () => {
        // console.log("track ended: this.windowRef['stopScreenRecording']: ", this.windowRef['stopScreenRecording'])
        if (this.windowRef['stopScreenRecording']) this.windowRef['stopScreenRecording']();
      });
    });
  }
  private onStopScreenRecording(screenStream: any) {

    if(document.location.href.indexOf('/live-streaming') < 0) {
      var liveStreamDataLink = this.headerService.getLiveStreamDataSource();
      this.router.navigate([liveStreamDataLink.link], { queryParams: liveStreamDataLink.queryParams } as any)
    }

    this.headerService.updateLiveStreamStateSource('stopped-screen-sharing');
    this.stopPicInPicPreview();
    const videoObj: any = document.getElementById('cam-view');
    // console.log("onStopScreenRecording -> videoObj", videoObj)
    if(videoObj){
      videoObj.srcObject = this.windowRef['cameraStream'];
      videoObj.muted = true;
    }
    var camVideoTrack = this.windowRef['cameraStream'].getVideoTracks()[0];
    var camAudioTrack = this.windowRef['cameraStream'].getAudioTracks()[0];
    //changing back to media streams for audio and video if its windows
    if(!this.isSafari && !this.isMacOs) this.windowRef['audioSender'].replaceTrack(camAudioTrack);
    this.windowRef['videoSender'].replaceTrack(camVideoTrack);
    if(this.newAudioStream) this.newAudioStream.getTracks().forEach(track => track.stop());
    screenStream.getTracks().forEach(track => track.stop());
    this.windowRef['stopScreenRecording'] = null;
  }

  private onStopScreenRecording_(screenStream: any) {
    if (this.windowRef['rtcPeerConnectionSS'].connectionState == 'connected') {
      this.headerService.updateLiveStreamStateSource('stopped-screen-sharing');
      this.stopPicInPicPreview();

      const videoObj: any = document.getElementById('cam-view');
      videoObj.srcObject = this.windowRef['cameraStream'];;
      videoObj.muted = true;

      this.windowRef['rtcPeerConnectionSenderSS'].forEach(track => this.windowRef['rtcPeerConnectionSS'].removeTrack(track));
      this.windowRef['rtcPeerConnectionSS'].close();
      screenStream.getTracks().forEach(track => track.stop());

      const data = {
        account_id: this.userCurrAcc.accounts.account_id,
        document_id: this.params.video_id,
        huddle_id: this.params.huddle_id,
        stopped_at: this.headerService.getLiveStreamVideoTimeSource(),
        file_name_recorded_video: this.millicastService.streamNameSS
      };

      this.mainService.screenShareEvent(data).subscribe();

      this.windowRef['rtcPeerConnectionSenderSS'] = [];
      this.windowRef['rtcPeerConnectionSS'] = null;
      this.windowRef['stopScreenRecording'] = null;

    }
  }

  private startPicInPicPreview() {
    if (this.screenRecordingCamContainer && document.pictureInPictureEnabled && !this.screenRecordingCamContainer.disablePictureInPicture) {
      try {
        if (document.pictureInPictureElement) {
          document.exitPictureInPicture()
            .then(() => console.log(""))
            .catch((err) => {
              this.screenRecordingCamContainer.requestPictureInPicture();
              console.log("")
            })
        } 
        this.screenRecordingCamContainer.requestPictureInPicture();
      } catch (err) {
        console.error('picInPic error: ', err);
      }
    }
  }

  private stopPicInPicPreview() {
    if (document.pictureInPictureElement) document.exitPictureInPicture();
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    var elem_width = document.getElementById("col6").offsetWidth;
    
      this.elemWidth = elem_width-30;
     
       //console.log("elemwidth 1",this.elemWidth);
  
  }

  private makeFileName() {
    const date = new Date();
    const day: string | number = (date.getDate() < 10) ? `0${date.getDate()}` : date.getDate();
    const month: string | number = (date.getMonth() + 1 < 10) ? `0${date.getMonth() + 1}` : date.getMonth() + 1;
    const year = date.getFullYear();
    return `Livestream_${month}_${day}_${year}`;
  }

  public openPlUniqueDescModal() {
    this.SelectedPLRubric.plUniqueDescriptions.map(pld => pld.showMore = false);

    let modalClass: string = 'pl-unique-desc-modal-650';
    const plds = this.SelectedPLRubric.plUniqueDescriptions;
    if(plds.length === 3) modalClass = 'pl-unique-desc-modal-900';
    else if (plds.length === 4) modalClass = 'pl-unique-desc-modal-1200';
    else if (plds.length >= 5) modalClass = 'pl-unique-desc-modal-1450';

    const initialState = { plUniqueDescriptions: this.SelectedPLRubric.plUniqueDescriptions };
    this.modalService.show(PlUniqueDescriptionModelComponent, { initialState, class: modalClass, backdrop: 'static' });
  }
  
  ngOnDestroy() {
    if (this.windowRef['liveStreamState'] === 'comfortZone' && this.windowRef['cameraStream']) {
      this.windowRef['cameraStream'].getTracks().forEach(track => track.stop());
    }
    this.viewer_interval_check = 1;
    clearInterval(this.live_viewer_interval)
    this.subscriptions.unsubscribe();
  }

}
