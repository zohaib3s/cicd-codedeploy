import { Component, ElementRef, OnInit, ViewChild, Renderer2, ViewChildren, QueryList, HostListener, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';

import { HeaderService, AppMainService, HomeService, SocketService } from '@app/services';
import { VideoPageService, MainService } from "@videoPage/services";

import { GLOBAL_CONSTANTS } from "@constants/constant";
import { environment } from "@environments/environment";
import { CommentModel, CommentType, ReplyModel } from "@videoPage/models";
import { BreadCrumbInterface } from '@app/interfaces';
import { MARKER_COLORS } from '@videoPage/constants';
import { VideoCropModelComponent, ArtifactShareModelComponent, URLArtifactModalComponent, CategoryModalComponent, DeleteModalComponent, RenameModalComponent, DuplicateResourceModalComponent, DescriptionModalComponent } from "@shared/modals";





// import {
//   PDFJSStatic,
//   PDFPageViewport,
//   PDFRenderTask,
//   PDFDocumentProxy,
//   PDFPageProxy,
// } from 'pdfjs-dist';

// const PDFJS: PDFJSStatic = require('pdfjs-dist');

interface DomPosition { xPos: number, yPos: number };
interface LinePosition { startPos: DomPosition, endPos: DomPosition };

import * as pdfjsLib from 'pdfjs-dist/build/pdf';
import * as pdfjsViewer from 'pdfjs-dist/web/pdf_viewer';
import * as PDFViewerApplication from 'pdfjs-dist/lib/web/app.js';
import { ToastrService } from 'ngx-toastr';
import { isEmpty } from 'lodash';
import { PdfCommentListComponent } from '../pdf-comment-list/pdf-comment-list.component';
import { WorkspaceHttpService } from '@src/project-modules/workspace/services';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { SlimScrollEvent } from 'ngx-slimscroll';
import { start } from 'repl';
import { AnyARecord } from 'dns';
import { PdfAddCommentComponent } from '../pdf-add-comment/pdf-add-comment.component';



pdfjsLib.GlobalWorkerOptions.workerSrc = './assets/video-page/js/pdf.worker.min.js';

// contant values for scale
enum scale {
  DEFAULT_SCALE = 1.0,
  DEFAULT_SCALE_DELTA = 0.05,
  MIN_SCALE = 0.1,
  MAX_SCALE = 5.0
}
const enum Status {
  OFF = 0,
  DOWNRESIZE = 1,
  MOVE = 2,
  UPRESIZE = 3
}
@Component({
  selector: 'sibme-pdf-renderer',
  templateUrl: './pdf-renderer.component.html',
  styleUrls: ['./pdf-renderer.component.css']
})
export class PdfRendererComponent implements OnInit {
  // properties related to shapes
  public width: number;
  public height: number;
  public left: number = 0;
  public top: number = 0;
  public box: ElementRef;
  private boxPosition: { shape: any, left: number, top: number, right: number, bottom: number, height: number, width: number };
  private viewBox: {height: number, width: number};
  private containerPos: { page: HTMLElement, left: number, top: number, right: number, bottom: number, width: number, height: number };
  public mouse: { x: number, y: number }
  public status: Status = Status.OFF;
  private mouseClick: { x: number, y: number, left: number, top: number }




  @ViewChild('resizeUpDot', { static: true }) resizeUpDot: ElementRef;
  @ViewChild('resizeDownDot', { static: true }) resizeDownDot: ElementRef;
  @ViewChild('spdfContent', { static: true }) spdfContent: ElementRef;
  @ViewChild('spdfBody', { static: true }) spdfBody: ElementRef;
  @ViewChild('spdfRenderer', { static: true }) spdfRenderer: ElementRef;
  @ViewChild('spdfCommentsWrapper', { static: true }) spdfCommentsWrapper: ElementRef;
  @ViewChild('spdfCanvasContainer', { static: true }) spdfCanvasContainer: ElementRef;
  @ViewChild('spdfAnnotContainer', { static: true }) spdfAnnotContainer: ElementRef;
  @ViewChild('shadowCanvas', { static: true }) shadowCanvas: ElementRef;
  @ViewChild('spdfFirstHLine', { static: true }) spdfFirstHLine: ElementRef; // first horizontal line
  @ViewChild('spdfVLine', { static: true }) spdfVLine: ElementRef; // vertical line
  @ViewChild('spdfSecondHLine', { static: true }) spdfSecondHLine: ElementRef; // second horizontal line
  @ViewChildren('CommentsDom') commentsDom: QueryList<ElementRef>;

  @ViewChild('selectedrubricsTemplate', { static: false }) selectedrubricsTemplate;
  @ViewChild('fileInfoTemplate', { static: false }) fileInfoTemplate;
  @ViewChild(PdfCommentListComponent) commentListComponent: PdfCommentListComponent;
  @ViewChild(PdfAddCommentComponent) commentAddComponent: PdfAddCommentComponent;


  public loaded: boolean = false;
  public addReply: boolean = false;
  public optionMainCheck: boolean = true;
  public optionCheckValue: boolean = false;
  private pdfDoc = null;
  public totalPages: number = 0;
  public currentPage: number = 0;
  private desiredWidth = 1000;
  public VideoHuddleFramework;
  public comment_status: string = ''
  public add_comment_status = 1
  public page_sort = 7
  public ifNoSearchResult = false;



  public currentAction;
  private actionItemIconHeight: number = 25;
  private actionItemIconWidth: number = 25;

  public comments: any = {};
  public commentsList = [];
  public fileInfo = [];
  public totalReplies:any = 0;
  public fileSizeFileInfo;
  public docApiStatus = false;
  public replyText: string;

  public viewPortHeight: number;

  public colorClasses: string[] = MARKER_COLORS;
  private videoPageType = GLOBAL_CONSTANTS.VIDEO_PAGE_TYPE;
  public pageType: string;
  private subscriptions: Subscription = new Subscription();
  public translations: any = {};
  public userName: string;
  public filestack_handle: any;
  public artifacts_id: any;
  public myPdfPath: any;
  public file_type: any;
  public activeComment: any = {};
  public userAccountLevelRoleId: number | string = null;
  public activeBtn: number = 0;
  public customMarkers: any;
  public currentFrameworkId: any = null;
  public rubrics: any;
  public permissions: any = {};
  public artifactDataId: any = {};
  public pdfPreviewDiv: boolean = false;
  public handToolActivate: boolean = false;
  public pdfCommentDiv: boolean = false;
  public pdfSearchDiv: boolean = false;
  public bsModalRef: BsModalRef;
  public isResizeUp = false;
  public elementToResize:any;
  public isResize = false;
  public huddle_type:any;
  public is_submitted:any;
  public huddle_role:any;
  public coachee_permission:any;
  huddle_id: any;
  user_id: any;
  accountId: any;
  frameworks;
  rubricPreview: {};
  modalRef: BsModalRef;
  userImage: string = null;
  pdf_name: any;
  document_extension:any;
  documentDetail: any;
  headerData: any;
  custom_marker: string = '';
  selectedTag: any = {};
  documentInfo: any = {};
  selectedTagColor: string = 'grey'
  SearchString = ''
  public searchData: any;
  public currEditableCom: any = null;
  public currEditableComRef: any = null;
  public original_mouse_x:any; 
  public original_mouse_y :any;
  private searchInput: Subject<string> = new Subject();
  //declaration of searching in pdf's variables
  eventBus: any
  pdfLinkService: any
  public pdfViewer: any
  pdfFindController: any
  container: HTMLElement;
  viewer: HTMLElement;
  private pdfPageStndrdSize: {width: number, height: number} = {width: 0, height: 0};
  private styles = [
    { id: 'pdf_viewer', path: './assets/video-page/css/pdf_viewer.css' },
    { id: 'text_layer_builder', path: './assets/video-page/css/text_layer_builder.css' },

  ];
  private searchQuery: Subject<string> = new Subject();
  find: { findPrevious: boolean, entireWord: boolean, searchString: string, caseSensitive: boolean, totalMatches: number };
  pdfViewerApplication: any;
  scrollEvents: EventEmitter<any>;
  opts: { position: string; barBackground: string; barOpacity: string; barWidth: string; barBorderRadius: string; barMargin: string; gridBackground: string; gridOpacity: string; gridWidth: string; gridBorderRadius: string; gridMargin: string; alwaysVisible: boolean; visibleTimeout: number; };
  isUpResizingOn: boolean = false;
  public breadCrumbs: any[];
  public parent_folder_id: any;
  public viewer_mode: any;
  private annotationLineOnActiveTimers = [];
  private eventListeners = [];
  public pdfCurrentScale: any = 'page-fit';
  private drawRect:any;
  public pdfAllowedScale: any = ['auto','page-actual','page-fit','page-width','0.1','0.25','0.5','0.75','1','1.25','1.5','2','3','4','5'];
  
    public freeDrawingPath = null;
    public dontFireOnSelfClick = false;//dont fire foreign object comment text change observable when user just clicked on add comment body
    public freeDrawingStrPath;
    public freeDrawingBuffer = []; // Contains the last positions of the mouse cursor
  handToolListeners: any =[];
  handToolMoveListener:any = [];

  constructor(private router: Router, public toastr: ToastrService, public headerService: HeaderService, private appMainService: AppMainService, private modalService: BsModalService,
    private videoPageService: VideoPageService, private socketService: SocketService, private mainService: MainService, public workService: WorkspaceHttpService, private homeService: HomeService,
    private activatedRoute: ActivatedRoute, private renderer: Renderer2, private bsModalService: BsModalService) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => this.translations = languageTranslation));
    this.subscriptions.add(this.headerService.behaviorialHeaderData$.subscribe(headerData => {
      this.userName = `${headerData.user_current_account.User.first_name} ${headerData.user_current_account.User.last_name}`;
      this.user_id = headerData.user_current_account.User.id;
      this.accountId = headerData.user_current_account.accounts.account_id;
      if (headerData.user_current_account.User.image) this.userImage = `${headerData.user_current_account.User.id}/${headerData.user_current_account.User.image}`
      this.userAccountLevelRoleId = headerData.user_permissions.roles.role_id;
    }));

    this.subscriptions.add(this.mainService.PushableRubric.subscribe((rubric: any) => {
      const activeComment = this.comments.id === this.activeComment.id;
      if (activeComment) {
        if (rubric.selected) this.activeComment.standards.push(JSON.parse(JSON.stringify(rubric)));
        else this.activeComment.standards = this.activeComment.standards.filter(rub => rubric.account_tag_id != rub.account_tag_id);
      }
    }));
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
    }));
      this.subscriptions.add(this.mainService.foreignTextComment.subscribe(commentText => this.updateForeignComment(commentText)));
    
    
      // this.breadCrumbsSubscription = this.homeService.breadcrumbs$.subscribe(data => {
    //   this.breadCrumbs = data;
		// });

    // this.pageType = this.activatedRoute.parent.snapshot.data.pageType;

    this.activatedRoute.params.subscribe((p) => {
      this.filestack_handle = p.filestack_handle;
      this.artifacts_id = p.id;
      this.file_type = p.file_type
      this.pageType = p.page_type
      this.huddle_id = p.account_folder_id
      this.parent_folder_id = p.parent_folder_id
      this.viewer_mode = p.viewer_mode
      this.GetRubrics(this.huddle_id, this.accountId);
    });

    //initialization of searching in pdf variables
    this.eventBus = new pdfjsViewer.EventBus();

    this.eventBus.on("pagerendered", (event) => {
      
      this.initPageAnnotations(event.pageNumber);
    });

    this.eventBus.on("pagesinit", () => {
      //this.pdfViewer.currentScaleValue = '1';
       this.pdfViewer.currentScaleValue = this.pdfCurrentScale;

       this.subscriptions.add(this.mainService.currentEditableComment$.subscribe(currentEditableComment => {
        if (currentEditableComment) {
          this.currEditableCom = { ...currentEditableComment };
          this.currEditableComRef = { ...currentEditableComment };
        }
        else
        {
           /* */
        
        }
        if(currentEditableComment === false)
        {
            this.reDrawPreviousShape();
            this.drawLineHelper(this.activeComment.id, this.activeComment.type, (this.pdfViewer.currentPageNumber == this.activeComment.page))
        }
  
        if (currentEditableComment) {
          this.currEditableCom = { ...currentEditableComment };
          this.currEditableComRef = { ...currentEditableComment };
          setTimeout( ()=>{
              this.hideShowResizeHandlers();
          },200)
          //this.changeActiveComment(this.currEditableComRef)
        }
        else {
          this.currEditableCom = null;
          this.currEditableComRef = null;
        }
  
        if (!this.currEditableCom) {
          this.makeAllInActive();
          setTimeout(()=>{
              this.hideShowResizeHandlers(1);
          },100)
          
        }
        
      }));

      Array.prototype.slice.call(document.getElementsByClassName("page")).forEach(element => {
        element.setAttribute('id', `pdf-page-${element.dataset.pageNumber}`);
        element.classList.add('text-layer-pdf-page');
      });

      console.log('pdfViewer: ', this.pdfViewer);

    });


    this.pdfLinkService = new pdfjsViewer.PDFLinkService({
      eventBus: this.eventBus,
    });
    this.pdfFindController = new pdfjsViewer.PDFFindController({
      eventBus: this.eventBus,
      linkService: this.pdfLinkService,
    });

    this.find = { findPrevious: false, entireWord: false, searchString: '', caseSensitive: false, totalMatches: 0 };


  }

  private initPageAnnotations(pageNumber: number) {
    const getPageAnnotContainer = this.getPageAnnotContainer(pageNumber);

    const pageAnnotationsArr = this.commentsList.filter(comment => comment.page == pageNumber);
    if(this.comments && this.activeComment && this.activeComment.id == this.comments.id && this.activeComment.page == pageNumber)
    {
        pageAnnotationsArr.push(this.activeComment)
    }
    pageAnnotationsArr.forEach(comment => {
      // if (comment.type == 'rect-comment') {
        this.renderRectAnnot(getPageAnnotContainer, comment);
      // }
    });
  }
  
  public getPageOriginalHeightWidth(pageNumber, width = 1)
  {
      const pdfPageRef = this.pdfViewer._pages[pageNumber - 1]; // get actual index based pdf page referennce instead of dom reference
      const pdfPageWidth = (pdfPageRef.pdfPage) ? pdfPageRef.pdfPage.view[2] : 350; // view is an array in which 3rd property is width and 4th is height
      const pdfPageHeight = (pdfPageRef.pdfPage) ? pdfPageRef.pdfPage.view[3] : 550;
      if(width)
      {
          return pdfPageWidth;
      }
      else
      {
          return pdfPageHeight;
      }
  }

  /**
   * Create page annot layer (container) if not existed
   * Then return the annot layer aka parent dom element to hold annotations
   * @param pageNumber | number
   */
  private getPageAnnotContainer(pageNumber: number) {
    const page: any = document.getElementById(`pdf-page-${pageNumber}`);
   // console.log("pageNumber: ",pageNumber);
   // console.log("page: ",page);
    
    const pageAnnotLayer = [...page.children].find(element => element.className.includes('custom-annotation-layer'));

    if (pageAnnotLayer) {

      if (pageAnnotLayer.tagName === 'DIV') return pageAnnotLayer.firstChild; // return svg if layer is div
      else return pageAnnotLayer; // return layer elem which is surely svg elem

    } else {
      // Create and return layer

      const pdfPageWidth = this.getPageOriginalHeightWidth(pageNumber, 1)
      const pdfPageHeight = this.getPageOriginalHeightWidth(pageNumber, 0);

      // saving width and height of larger page
      if(pdfPageWidth > this.pdfPageStndrdSize.width) this.pdfPageStndrdSize.width = pdfPageWidth;
      if(pdfPageHeight > this.pdfPageStndrdSize.height) this.pdfPageStndrdSize.height = pdfPageHeight;

      // Annot layer container div and child svg technique
      const customAnnotationLayer = this.renderer.createElement('div');
      this.renderer.addClass(customAnnotationLayer, 'custom-annotation-layer');
      this.renderer.setStyle(customAnnotationLayer, 'position', 'absolute');
      this.renderer.setStyle(customAnnotationLayer, 'top', 0);

      const pageSvg = this.renderer.createElement('svg', 'svg');

      this.renderer.setAttribute(pageSvg, 'width', `${page.offsetWidth}`);
      this.renderer.setAttribute(pageSvg, 'height', `${page.offsetHeight}`);
      this.renderer.setAttribute(pageSvg, 'viewBox', `0 0 ${pdfPageWidth} ${pdfPageHeight}`);
      this.renderer.setAttribute(pageSvg, 'id', `pdf-page-svg-${pageNumber}`);
      if(this.eventListeners && this.eventListeners.length)
      {
          this.eventListeners.forEach(listener => {
              listener();//this will remove previous event listeners
          })
      }
      this.renderer.listen(pageSvg, 'mousedown', (event) => this.svgMouseDown(event, pageSvg));
      this.renderer.listen(pageSvg, 'mousemove', (event) => this.onMouseMove(event, pageSvg));
      this.eventListeners.push(this.renderer.listen(this.spdfRenderer.nativeElement, 'mouseup', (event) => this.handleMouseUp(event)));


      this.renderer.appendChild(customAnnotationLayer, pageSvg);
      this.renderer.appendChild(page, customAnnotationLayer);
      this.registerResizeDotsEvents();
      return pageSvg;

      // Only svg container technique
      // const svgAnnotationLayer = this.renderer.createElement('svg', 'svg');
      // this.renderer.addClass(svgAnnotationLayer, 'custom-annotation-layer');
      // this.renderer.setAttribute(svgAnnotationLayer, 'width', `${page.offsetWidth}`);
      // this.renderer.setAttribute(svgAnnotationLayer, 'height', `${page.offsetHeight}`);
      // this.renderer.setStyle(svgAnnotationLayer, 'position', `absolute`);
      // this.renderer.setStyle(svgAnnotationLayer, 'top', 0);

      // this.renderer.appendChild(page, svgAnnotationLayer);
      // return svgAnnotationLayer;
    }

  }
  public registerResizeDotsEvents()
  {
      this.spdfCanvasContainer.nativeElement.addEventListener('mousedown', (e: any) => {
          // e.preventDefault();
          if(e.target.id=='resize-action-down'){
              this.elementToResize  = document.getElementById(this.activeComment.id+'-'+this.activeComment.type.split('-')[0]);
              this.isResize = true;
          }
          if(e.target.id=='resize-action-up'){
              console.log('up started');
              this.elementToResize  = document.getElementById(this.activeComment.id+'-'+this.activeComment.type.split('-')[0]);
              this.isResizeUp = true;
          }
      })
      this.spdfCanvasContainer.nativeElement.addEventListener('mousemove',(evnt:any)=>{
          evnt.preventDefault();
          if(this.isResize){
              this.doResizingForDot(this.elementToResize,evnt)
          }
          else if(this.isResizeUp){
              this.resizingUp(this.elementToResize,evnt)
          }
      })
    
      window.addEventListener('mouseup', (e)=> {
          this.isResize = false;
          this.isResizeUp = false;
      });
  }
  private svgPoint = (elem, x, y) => {
    let p = elem.createSVGPoint();
    p.x = x;
    p.y = y;
    return p.matrixTransform(elem.getScreenCTM().inverse());
  }
  getMousePosition(svg, evt) {
    var CTM = svg.getScreenCTM();
    if (evt.touches) { evt = evt.touches[0]; }
    return {
      x: (evt.clientX - CTM.e) / CTM.a,
      y: (evt.clientY - CTM.f) / CTM.d
    };
  }
  
  hideShowResizeHandlers(hide = 0)
  {
      
      if(hide)
      {
          //this.makeAllInActive();
          this.renderer.setStyle(this.resizeUpDot.nativeElement, 'display', 'none');
          this.renderer.setStyle(this.resizeDownDot.nativeElement, 'display', 'none');
      }
      else
      {
          let comment = this.activeComment && this.activeComment.id ? this.activeComment : (this.comments ? this.comments : null);
          if(!comment || (comment && comment.type == 'text-comment'))
          {
              return;
          }
          this.updateActiveCommentBBox(comment)
          let pageId = comment.page;
          this.moveDiv("resize-action-up", `pdf-page-svg-${pageId}`);
          this.moveDiv("resize-action-down", `pdf-page-svg-${pageId}`);
    
          this.renderer.setStyle(this.resizeUpDot.nativeElement, 'cx', ((comment.xPos - 10)) );
          this.renderer.setStyle(this.resizeUpDot.nativeElement, 'cy', ((comment.yPos - 10)) );
          
          this.renderer.setStyle(this.resizeDownDot.nativeElement, 'cx', ((comment.xPos + comment.width + 10)));
          this.renderer.setStyle(this.resizeDownDot.nativeElement, 'cy', ((comment.yPos + comment.height + 10)));
          if(this.activeComment.type == 'line-comment')
          {
              let element = document.getElementById(this.activeComment.id+'-line');
              if(element)
              {
                      this.renderer.setStyle(this.resizeUpDot.nativeElement, 'cx', element.getAttribute('x1'));
                      this.renderer.setStyle(this.resizeUpDot.nativeElement, 'cy', element.getAttribute('y1'));
    
                      this.renderer.setStyle(this.resizeDownDot.nativeElement, 'cx', element.getAttribute('x2'));
                      this.renderer.setStyle(this.resizeDownDot.nativeElement, 'cy', element.getAttribute('y2'));
              }
          }
          this.renderer.setStyle(this.resizeUpDot.nativeElement, 'display', 'block');
          this.renderer.setStyle(this.resizeDownDot.nativeElement, 'display', 'block');
          this.makeAllInActive();
         let elem = document.getElementById(comment.id+'-rect');
         if(elem)
         {
             elem.classList.add('active-annot');
         }
         let textBox = document.getElementById(comment.id+ '-foreignObject') 
         if(textBox)  textBox.firstElementChild.setAttribute('contenteditable', 'true')
         
      }
  }
  
  private drawCircle(pageSvg,start,pageId)
  {
      const rect = this.renderer.createElement('ellipse', 'svg');
      this.hideShowResizeHandlers(1);
      this.drawRect = (e) => {
          let p = this.svgPoint(pageSvg, e.clientX, e.clientY);
          let w = Math.abs(p.x - start.x);
          let h = Math.abs(p.y - start.y);
          if (p.x > start.x) {
              p.x = start.x;
          }
        
          if (p.y > start.y) {
              p.y = start.y;
          }
          p.x = p.x+w/2
          p.y = p.y+h/2
          let width = w/2
          let height = h/2
          console.log('width', width)
          console.log('height', height)
          this.addNewComment(pageId, p.x, p.y, 'ellipse-comment',height,width)
          console.log('this is new comment height width', this.comments.height , ' and width', this.comments.width )
          rect.setAttributeNS(null, 'cx', p.x);
          rect.setAttributeNS(null, 'cy', p.y);
          rect.setAttributeNS(null, 'x', p.x);
          rect.setAttributeNS(null, 'y', p.y);
          rect.setAttributeNS(null, 'width', width);
          rect.setAttributeNS(null, 'height', height);
          rect.setAttributeNS(null, 'rx', width);
          rect.setAttributeNS(null, 'ry', height);
          rect.setAttributeNS(null, 'fill', 'rgba(255, 255, 255, 0)');
          this.renderer.addClass(rect, 'resizable-draggable');
          rect.setAttributeNS(null, 'stroke-width', '2');
          rect.setAttributeNS(null, 'id', `${this.comments.id}-ellipse`);
          pageSvg.appendChild(rect);
        
      }
    
      const endDraw = (e) => {
          let p = this.svgPoint(pageSvg, e.clientX, e.clientY);
          this.currEditableCom = this.activeComment = this.comments;
          this.hideShowResizeHandlers();
        
          pageSvg.removeEventListener('mousemove', this.drawRect);
          pageSvg.removeEventListener('mouseup', endDraw);
        
          this.renderer.listen(rect, 'mousedown', (event) => this.setStatus(event, 2));
      }
    
      pageSvg.addEventListener('mousemove', this.drawRect);
      pageSvg.addEventListener('mouseup', endDraw);
  }

  private drawTextBox(pageSvg, start, pageId) {
    const rect = this.renderer.createElement('foreignObject', 'svg');
    const div = this.renderer.createElement('div');
    this.hideShowResizeHandlers(1);
    this.drawRect = (e) => {
      let p = this.svgPoint(pageSvg, e.clientX, e.clientY);
      let w = Math.abs(p.x - start.x);
      let h = Math.abs(p.y - start.y);
      if (p.x > start.x) {
        p.x = start.x;
      }

      if (p.y > start.y) {
        p.y = start.y;
      }
      this.addNewComment(pageId, p.x, p.y, 'foreignObject-comment', h, w)
      rect.setAttributeNS(null, 'x', p.x);
      rect.setAttributeNS(null, 'y', p.y);
      rect.setAttributeNS(null, 'width', w);
      rect.setAttributeNS(null, 'height', h);
      // rect.setAttributeNS(null, 'fill', 'rgba(255, 255, 255, 0)');
      this.renderer.addClass(rect, 'resizable-draggable');
      rect.setAttributeNS(null, 'stroke-width', '2');
      rect.setAttributeNS(null, 'id', `${this.comments.id}-foreignObject`);
      // div.setAttributeNS(null, 'id', `${this.comments.id}-textbox`);
      this.renderer.setStyle(div, 'width', '100%');
      this.renderer.setStyle(div, 'height', '100%');
      this.renderer.setStyle(div, 'padding', '5px');
      this.renderer.addClass(div, 'custom-scrollbar-css');
      this.renderer.setStyle(rect, 'border', '2px solid rgb(3, 149, 66)');
      //this.renderer.setStyle(div, 'border', '1px solid rgb(3, 149, 66)');
      this.renderer.setStyle(div, 'font-size', '24px');
      this.renderer.setStyle(div, 'white-space', 'break-spaces');
      this.renderer.setStyle(div, 'word-break', 'break-all');
      div.setAttributeNS(null, 'contenteditable', 'true');
      this.renderer.addClass(div, 'title');
      this.renderer.addClass(div, 'text-tool-div');
      div.setAttributeNS(null, 'data-placeholder', 'Insert your text here');
      // div.innerHTML = 'Insert your text here';
      pageSvg.appendChild(rect);
      rect.appendChild(div)

    }

    const endDraw = (e) => {
      let p = this.svgPoint(pageSvg, e.clientX, e.clientY);
      this.currEditableCom = this.activeComment = this.comments;
      this.hideShowResizeHandlers();

      pageSvg.removeEventListener('mousemove', this.drawRect);
      pageSvg.removeEventListener('mouseup', endDraw);

      this.renderer.listen(rect, 'mousedown', (event) => this.setStatus(event, 2));
      this.renderer.listen(rect, 'keyup', (event) => this.handleMouseUp(event));
    }

    pageSvg.addEventListener('mousemove', this.drawRect);
    pageSvg.addEventListener('mouseup', endDraw);
    pageSvg.addEventListener("paste", e => {
      // console.log('data types ', (e.originalEvent || e).clipboardData.types)
      if((e.originalEvent || e).clipboardData.types.includes('Files')) e.preventDefault();
    });
  }
  
  private drawArrow(pageSvg,start,pageId)
  {
      const rect = this.renderer.createElement('line', 'svg');
      this.hideShowResizeHandlers(1);
      this.drawRect = (e) => {
          let p = this.svgPoint(pageSvg, e.clientX, e.clientY);
          let w = Math.abs(p.x - start.x);
          let h = Math.abs(p.y - start.y);
          // if (p.x > start.x) {
          //     p.x = start.x;
          // }
        
          // if (p.y > start.y) {
          //     p.y = start.y;
          // }
          // p.x = p.x+w/2
          // p.y = p.y+h/2
          // w = w/2
          // h = h/2
          this.addNewComment(pageId, p.x, p.y, 'line-comment',h,w)
          rect.setAttributeNS(null, 'x1', start.x);
          rect.setAttributeNS(null, 'y1', start.y);
          rect.setAttributeNS(null, 'x', start.x);
          rect.setAttributeNS(null, 'y', start.y);
          rect.setAttributeNS(null, 'width', w);
          rect.setAttributeNS(null, 'height', h);
          rect.setAttributeNS(null, 'x2', p.x);
          rect.setAttributeNS(null, 'y2', p.y);
          rect.setAttributeNS(null, 'marker-end', 'url(#arrow)');
          rect.setAttributeNS(null, 'fill', 'rgba(255, 255, 255, 0)');
          this.renderer.addClass(rect, 'resizable-draggable');
          rect.setAttributeNS(null, 'stroke-width', '2');
          rect.setAttributeNS(null, 'id', `${this.comments.id}-line`);
          pageSvg.appendChild(rect);
        
      }
    
      const endDraw = (e) => {
          let p = this.svgPoint(pageSvg, e.clientX, e.clientY);
          this.currEditableCom = this.activeComment = this.comments;
          this.hideShowResizeHandlers();
        
          pageSvg.removeEventListener('mousemove', this.drawRect);
          pageSvg.removeEventListener('mouseup', endDraw);
        
          this.renderer.listen(rect, 'mousedown', (event) => this.setStatus(event, 2));
      }
    
      pageSvg.addEventListener('mousemove', this.drawRect);
      pageSvg.addEventListener('mouseup', endDraw);
  }
  
  public freeDrawing(e, pageSvg, pageId)
  {
      // this.freeDrawingPath = document.getElementById('asad');
      // if(!this.freeDrawingPath)
      // {
        const g = document.createElementNS('http://www.w3.org/2000/svg', 'g');
        this.freeDrawingPath = document.createElementNS('http://www.w3.org/2000/svg', 'path');
        this.freeDrawingPath.setAttribute("fill", "none");
        this.freeDrawingPath.setAttribute("stroke", "#000");
        this.freeDrawingPath.setAttribute("stroke-width", 2);
        this.freeDrawingBuffer = [];
        let pt = this.svgPoint(pageSvg, e.clientX, e.clientY);
        this.addNewComment(pageId, pt.x, pt.y, 'path-comment',0,0)
        g.setAttribute("id", `${this.comments.id}-path`);
        this.appendToBuffer(pt);
        this.freeDrawingStrPath = "M" + pt.x + " " + pt.y;
        this.freeDrawingPath.setAttribute("d", this.freeDrawingStrPath);
        pageSvg.appendChild(g);
        g.appendChild(this.freeDrawingPath)
        this.renderer.listen(g, 'mousedown', (event) => this.setStatus(event, 2));
      // }
  }
    public appendToBuffer(pt) {
        this.freeDrawingBuffer.push(pt);
        while (this.freeDrawingBuffer.length > 1) {
            this.freeDrawingBuffer.shift();
        }
    };

// Calculate the average point, starting at offset in the this.freeDrawingBuffer
    public getAveragePoint(offset) {
        let len = this.freeDrawingBuffer.length;
        if (len % 2 === 1 || len >= 1) {
            var totalX = 0;
            var totalY = 0;
            var pt, i;
            var count = 0;
            for (i = offset; i < len; i++) {
                count++;
                pt = this.freeDrawingBuffer[i];
                totalX += pt.x;
                totalY += pt.y;
            }
            return {
                x: totalX / count,
                y: totalY / count
            }
        }
        return null;
    };
    
    public updateSvgPath() {
        let pt = this.getAveragePoint(0);
        
        if (pt) {
            // Get the smoothed part of the this.freeDrawingPath that will not change
            this.freeDrawingStrPath += " L" + pt.x + " " + pt.y;
            
            // Get the last part of the this.freeDrawingPath (close to the current mouse position)
            // This part will change if the mouse moves again
            let tmpPath = "";
            for (var offset = 2; offset < this.freeDrawingBuffer.length; offset += 2) {
                pt = this.getAveragePoint(offset);
                tmpPath += " L" + pt.x + " " + pt.y;
            }
            
            // Set the complete current this.freeDrawingPath coordinates
            this.freeDrawingPath.setAttribute("d", this.freeDrawingStrPath + tmpPath);
        }
    }


  private svgMouseDown(event, pageSvg) {
    if(this.activeComment && this.activeComment.fakeComment && this.comments && this.comments.id == this.activeComment.id){
      console.log('Already a shape exist so returning')
    return;
}
    const start = this.svgPoint(pageSvg, event.clientX, event.clientY);
    let pageAnnotId = pageSvg.id.split('pdf-page-svg-')[1];
    
    pageAnnotId = pageAnnotId ? pageAnnotId.split('-')[0] : null;
    const pageId = pageAnnotId || this.activeComment.page;

    if (!pageId) return;
    if (this.currentAction == 'rect-comment') {


      const rect = this.renderer.createElement('rect', 'svg');
      this.hideShowResizeHandlers(1);
      this.drawRect = (e) => {
        let p = this.svgPoint(pageSvg, e.clientX, e.clientY);
        let w = Math.abs(p.x - start.x);
        let h = Math.abs(p.y - start.y);
        if (p.x > start.x) {
          p.x = start.x;
        }

        if (p.y > start.y) {
          p.y = start.y;
        }
        this.addNewComment(pageId, p.x, p.y, 'rect-comment',h,w)
        rect.setAttributeNS(null, 'x', p.x);
        rect.setAttributeNS(null, 'y', p.y);
        rect.setAttributeNS(null, 'width', w);
        rect.setAttributeNS(null, 'height', h);
        rect.setAttributeNS(null, 'fill', 'rgba(255, 255, 255, 0)');
        this.renderer.addClass(rect, 'resizable-draggable');
        rect.setAttributeNS(null, 'stroke-width', '2');
        rect.setAttributeNS(null, 'id', `${this.comments.id}-rect`);
        pageSvg.appendChild(rect);

      }

      const endDraw = (e) => {
        let p = this.svgPoint(pageSvg, e.clientX, e.clientY);
          this.currEditableCom = this.activeComment = this.comments;
          this.hideShowResizeHandlers();
       
       
       pageSvg.removeEventListener('mousemove', this.drawRect);
        pageSvg.removeEventListener('mouseup', endDraw);
        
        this.renderer.listen(rect, 'mousedown', (event) => this.setStatus(event, 2));
      }

      pageSvg.addEventListener('mousemove', this.drawRect);
      pageSvg.addEventListener('mouseup', endDraw);
    }
    if(this.currentAction == 'ellipse-comment'){
        this.drawCircle(pageSvg, start, pageId);
    }
    if(this.currentAction == 'line-comment'){
        this.drawArrow(pageSvg, start, pageId);
    }
    if(this.currentAction == 'freeDrawing-comment'){
        this.freeDrawing(event, pageSvg,pageId);
    }
    if (this.currentAction == 'textbox-comment') {
      this.drawTextBox(pageSvg, start, pageId);
    }
    if(this.currentAction == 'text-comment'){
      // check the xPos and Ypos with page width, lef and top, and not let it exceed it from the limit of page
    // so that icon can be placed within the page 
    let element = event.target;
    let pageWidth = this.getPageOriginalHeightWidth(pageId, 1);
    let pageHeight = this.getPageOriginalHeightWidth(pageId, 0);
    start.x = start.x < (pageWidth-48) ? start.x : (pageWidth-48)
    start.y = start.y < (pageHeight-48) ? start.y : (pageHeight-48)
      this.addNewComment(pageId, start.x, start.y, 'text-comment',48,48)
      const img = this.renderer.createElement('image', 'svg');
      this.renderer.setAttribute(img, 'href', `assets/img/text-comment.svg`);
      this.renderer.setAttribute(img, 'alt', this.currentAction);
      this.renderer.setAttribute(img, 'id', `${this.comments.id}-text`);
      this.renderer.addClass(img, 'absolute-icon');
      this.renderer.setStyle(img, 'position', `absolute`);
      this.renderer.setAttribute(img, 'x', `${start.x-20}`);
      this.renderer.setAttribute(img, 'y', `${start.y-20}`);
      this.renderer.listen(img, 'mousedown', (event) => this.setStatus(event, 2));
      pageSvg.appendChild(img);
    }
  };
    
    public resizingUp = (element,e) => {
        e.preventDefault();
        if(this.isResizeUp){
            let bPosition = element.getBBox();
            let pointOriginX = bPosition.x;
            let pointOriginY = bPosition.y;
            let original_width =  bPosition.width;
            let original_height = bPosition.height;
            const pageSvg = document.getElementById(element.parentElement.id)
            const end = this.svgPoint(pageSvg, e.clientX, e.clientY);
            
            let changeInX = end.x - pointOriginX;
            let changeInY = end.y - pointOriginY;
            
            if(end.x>0 && end.y>0){
                let h = original_height - changeInY;
                let w = original_width - changeInX;
                if(h>0 && w>0){
                    this.resizeUpDot.nativeElement.style.cssText = `cx:${end.x-10};cy:${end.y-10};display:block;`
                    if(this.activeComment.type == 'rect-comment' || this.activeComment.type ==  'foreignObject-comment')
                    {
                        element.setAttribute('x', end.x);
                        element.setAttribute('y', end.y);
                        element.setAttribute('width',w);
                        element.setAttribute('height',h);
                        
                    }
                    else if(this.activeComment.type == 'ellipse-comment')
                    {
                        let rx = ((w)/2).toFixed(2);
                        let ry = ((h)/2).toFixed(2);
                        let cx = (parseFloat(element.getAttribute('cx')) + (changeInX/2)).toFixed(2);
                        let cy = (parseFloat(element.getAttribute('cy')) + (changeInY/2)).toFixed(2);
                        this.renderer.setAttribute(element,'rx',`${rx}`);
                        this.renderer.setAttribute(element,'ry',`${ry}`);
                        this.renderer.setAttribute(element,'width',`${rx}`);
                        this.renderer.setAttribute(element,'height',`${ry}`);
                        this.renderer.setAttribute(element,'cx',`${cx}`);
                        this.renderer.setAttribute(element,'cy',`${cy}`);
                    }
                }
                if(this.activeComment.type == 'line-comment')
                {
                    this.resizeUpDot.nativeElement.style.cssText = `cx:${end.x-3};cy:${end.y-3};display:block;`
                    this.renderer.setAttribute(element,'x1',`${end.x-3}`);
                    this.renderer.setAttribute(element,'y1',`${end.y-3}`);
                }
            }
        }
    }


  public moveDiv(source, destination) {
    var fragment = document.createDocumentFragment();
    let a = document.getElementById(source);
    if(a)
    {
        fragment.appendChild(a);
        let d = document.getElementById(destination);
        if(d)
        {
            d.appendChild(fragment);
        }
    }
  }

  public addNewComment(pageId, xPos, yPos, type, height, width) {
    if (!isEmpty(this.comments)) {
      this.cancelComment(this.comments)
    }
    const comment: any = CommentModel.addComment(this.currentAction, pageId, this.currentAction, xPos, yPos, { name: this.userName }, this.artifacts_id);
    comment.type = type;
    comment.height = height;
    comment.width= width
    this.comments = comment;
    this.currEditableCom = this.activeComment = comment;
    this.activeComment = comment;
    // this.currentPage = comment.page;
      /*if(this.pdfViewer.currentPageNumber != comment.page)
    this.pdfViewer.currentPageNumber = +comment.page;*/

    this.artifactDataId = {
      id: this.artifacts_id,
      page: comment.page,
      xPos: comment.xPos,
      yPos: comment.yPos,
      width: comment.width,
      height: comment.height,
      type: comment.type,
      fake_id: comment.id,
      page_height: this.getPageOriginalHeightWidth(comment.page, 0),
      page_width: this.getPageOriginalHeightWidth(comment.page, 1),
    }
    this.updateArtifactData(comment);
    return comment;
  }

  private renderRectAnnot(containerElm: HTMLElement, comment: any) {
    if(comment.annotation_data){
      let id = comment.id;
      let commentAnnotation = comment.annotation_data;
      if(typeof commentAnnotation !== 'object')
      {
         commentAnnotation = JSON.parse(comment.annotation_data);
      }
    if (comment.type !== 'text-comment')
    {
        let commentId =`${id}-${commentAnnotation.node_name}`;
        this.removeElementFromDom(commentId)
        const rect = this.renderer.createElement(`${commentAnnotation.node_name}`, 'svg');
        let notAttributes = ['page_height', 'page_width', 'node_name', 'page', 'type', 'xPos', 'yPos', 'id']
        for (const [key, value] of Object.entries(commentAnnotation)) {
          if(!notAttributes.includes(key)){
            if(key == 'innerHTML') {
              rect.innerHTML = value
              rect.firstChild.setAttributeNS(null,'contenteditable','false')
              
            }
            else rect.setAttributeNS(null, `${key}`, `${value}`);
          }
        }
        rect.setAttributeNS(null, 'id', commentId);
        this.renderer.listen(rect, 'mousedown', (event) => this.setStatus(event, 2));
        if(comment.type == 'ellipse-comment') console.log('this is the ellipse comment ', comment);
        
        if (comment.type == 'foreignObject-comment'){
          this.renderer.listen(rect, 'paste', (e) => {
            if((e.originalEvent || e).clipboardData.types.includes('Files')) e.preventDefault()
          });
          this.renderer.listen(rect, 'keyup', (event) => this.handleMouseUp(event));
        } 
        this.renderer.setStyle(rect, 'position', `absolute`);
        this.renderer.addClass(rect, 'resizable-draggable');
        this.renderer.appendChild(containerElm, rect);
        return;
    }
    }

    if (comment.type == 'rect-comment')
    {
        let commentId =`${comment.id}-rect`;
        this.removeElementFromDom(commentId)
        const rect = this.renderer.createElement('rect', 'svg');


        rect.setAttributeNS(null, 'x', `${comment.xPos}`);
        rect.setAttributeNS(null, 'y', `${comment.yPos}`);
        rect.setAttributeNS(null, 'width', `${comment.width}`);
        rect.setAttributeNS(null, 'height', `${comment.height}`);
        rect.setAttributeNS(null, 'fill', 'rgba(255, 255, 255, 0)');
        rect.setAttributeNS(null, 'stroke-width', '2');
        rect.setAttributeNS(null, 'id', commentId);
        this.renderer.listen(rect, 'mousedown', (event) => this.setStatus(event, 2));
        this.renderer.setStyle(rect, 'position', `absolute`);
        this.renderer.addClass(rect, 'resizable-draggable');
        this.renderer.appendChild(containerElm, rect);
    }
    else if (comment.type == 'text-comment')
    {
        let commentId = `${comment.Comment ? comment.Comment.id : comment.id}-text`;
        this.removeElementFromDom(commentId);
        const img = this.renderer.createElement('image', 'svg');
        this.renderer.setAttribute(img, 'href', `assets/img/text-comment.svg`);
        this.renderer.setAttribute(img, 'alt', comment.type);
        this.renderer.setAttribute(img, 'id', commentId);
        this.renderer.setStyle(img, 'position', `absolute`);
        this.renderer.addClass(img, 'absolute-icon');
        this.renderer.setAttribute(img, 'x', `${comment.xPos}`);
        this.renderer.setAttribute(img, 'y', `${comment.yPos}`);
        this.renderer.listen(img, 'mousedown', (event) => this.setStatus(event, 2));
        this.renderer.appendChild(containerElm, img);

    }
  }


  webViewerResize() {
      this.moveDiv("resize-action-up", `resize-dots-container`);
      this.moveDiv("resize-action-down", `resize-dots-container`);
    // Note: the scale is constant for 'page-actual'.
    this.pdfViewer.currentScaleValue = scale.DEFAULT_SCALE;
    this.pdfViewer.update();
  }

  private loadCss(style: any) {
    let head = document.getElementsByTagName('head')[0];
    let link = document.createElement('link');
    link.id = style.id;
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = style.path;
    head.appendChild(link);
  }

  checkSubscriptions() {
    this.subscriptions.add(this.videoPageService.currentTab$.subscribe((tabValue: number) => {
      // this.staticTabs.tabs[tabValue].active = true;
      console.log(tabValue);
      
      this.activeBtn = tabValue;
      if(this.activeBtn == 2) {       
        this.drawLineHelper(this.activeComment.id, this.activeComment.type)
      }
    }));
    this.subscriptions.add(this.videoPageService.newComment$.subscribe((comment: any) => {
        if(comment && comment.comment)
        {
            this.clearDrawRectEvent(comment.comment);
        }
      
    }));

    this.subscriptions.add(this.appMainService.getFrameworks(this.accountId).subscribe((frameworks) => {
      this.frameworks = frameworks;
      if (this.frameworks.length == 1) {
        // setTimeout(() => {
        //   this.VideoHuddleFramework = this.frameworks[0].account_tag_id;
        //   this.ResolveAssignFramework(1, true);
        // }, 3000);
      }
    }));
  }
    
    public doResizingForDot = (element:any,e:any) => {
        e.stopPropagation();
        if(this.isResize){
            const pageSvg = document.getElementById(element.parentElement.id) as any
            const end = this.svgPoint(pageSvg, e.clientX, e.clientY);
            let box = pageSvg.viewBox.baseVal;  
            if(end.x > box.width || end.y > box.height || end.x < 0 || end.y < 0 ) return;
            let bPosition = element.getBBox();
            let x = bPosition.x;
            let y = bPosition.y
            let w = end.x-x-3;
            let h = end.y-y-3;
            if(w>0 && h>0){
                this.resizeDownDot.nativeElement.style.cssText = `cx:${end.x+7};cy:${end.y+7};display:block;`
                if(this.activeComment.type == 'rect-comment' || this.activeComment.type == 'foreignObject-comment')
                {
                    this.renderer.setAttribute(element,'width',`${w}`);
                    this.renderer.setAttribute(element,'height',`${h}`);
                }
                else if(this.activeComment.type == 'ellipse-comment')
                {
                    let newWidth = end.x - (bPosition.x + bPosition.width);
                    let newHeight = end.y - (bPosition.y + bPosition.height);
    
                    let rx = ((newWidth + bPosition.width)/2).toFixed(2);
                    let ry = ((newHeight + bPosition.height)/2).toFixed(2);
                    let cx = (parseFloat(element.getAttribute('cx')) + (newWidth/2)).toFixed(2);
                    let cy = (parseFloat(element.getAttribute('cy')) + (newHeight/2)).toFixed(2);
                    this.renderer.setAttribute(element,'rx',`${rx}`);
                    this.renderer.setAttribute(element,'ry',`${ry}`);
                    this.renderer.setAttribute(element,'width',`${rx}`);
                    this.renderer.setAttribute(element,'height',`${ry}`);
                    this.renderer.setAttribute(element,'cx',`${cx}`);
                    this.renderer.setAttribute(element,'cy',`${cy}`);
                }
            }
            else{
                console.log('Error');
            }
            if(this.activeComment.type == 'line-comment')
            {
                this.resizeDownDot.nativeElement.style.cssText = `cx:${end.x-3};cy:${end.y-3};display:block;`
                this.renderer.setAttribute(element,'x2',`${end.x-3}`);
                this.renderer.setAttribute(element,'y2',`${end.y-3}`);
            }
        }
  }
  ngOnInit() {
      this.spdfCanvasContainer.nativeElement.style.height = window.innerHeight - 230+"px";
    this.documentInfo = {
      document_id: this.artifacts_id,
      account_id: this.accountId,
      user_id: this.user_id
    }

    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '7',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    };
    this.styles.forEach(style => {
      this.loadCss(style);
    });
    this.container = document.getElementById("viewerContainer");
    this.viewer = document.getElementById("viewer");
    localStorage.removeItem(`comment-${this.user_id}-${this.artifacts_id}`)
    localStorage.removeItem(`edit-comment-${this.user_id}-${this.artifacts_id}`)
    this.checkSubscriptions();
    this.headerData = this.headerService.getStaticHeaderData();
    this.subscribeSockets();
    /**
     * Asynchronously downloads PDF.
     */

    // let pdfUrlCode = "GNevseQSjGraTV9Fg55e"; // 5BlbY5JZSSyXJJmREcZZ

    let path = "https://cdn.filestackcontent.com/output=f:pdf/pdfconvert=pageorientation:portrait,pageformat:a3/"
    if (this.file_type == 'pdf') {
      path = "https://cdn.filestackcontent.com/"
    }

    this.myPdfPath = path + this.filestack_handle;
    this.pdfViewer = new pdfjsViewer.PDFViewer({
      container: this.container,
      eventBus: this.eventBus,
      linkService: this.pdfLinkService,
      findController: this.pdfFindController,
    });
    this.pdfLinkService.setViewer(this.pdfViewer);



    pdfjsLib.getDocument(this.myPdfPath).promise.then((_pdfDoc) => {
      this.pdfDoc = _pdfDoc;
        this.pdfDoc.getMetadata().then(function(stuff) {
            console.log(stuff); // Metadata object here you can get real Author name from this variable
        }).catch(function(err) {
            console.log('Error getting meta data');
            console.log(err);
        });
    
        this.pdfDoc.loadingParams.disableAutoFetch = true
      this.totalPages = this.pdfDoc.numPages;

      this.pdfViewer.setDocument(this.pdfDoc)
      this.pdfLinkService.setDocument(this.pdfDoc, null);


      // this.currentPage = 1;
      this.loaded = true;
      this.getDocumentComments();
      this.permissions.can_comment = true;
      // this.pdfPreviewRender(this.pdfDoc);
    }, (error)=>{
        this.toastr.error(this.translations.dc_currept_file);
        if (this.pageType === 'workspace-video-page') {
            this.router.navigate(["/workspace/workspace/home/grid"])
        } else if (this.pageType === 'huddle-video-page') {
            this.router.navigate(["/video_huddles/list"])
        }
    })

    this.updateLineAnnotStyle(this.spdfFirstHLine.nativeElement, { display: 'none' });
    document.addEventListener('click', (e: any) => {
      if (e.target) {
        if (e.target.classList.contains('resizable-draggable')) {
          this.makeAllInActive();
          let currentId = this.getCommentIdFromString(e.target.id);
          if (this.currEditableCom && currentId == this.currEditableCom.id) {
            e.target.classList.add('active-annot');
          }
        }
      }
    });

     
    
  
     window.addEventListener('mouseup', (e)=> {this.isResize = false;});
     document.addEventListener('keyup', (event)=>{
      this.captureKey(event);
    })
      this.SubscribeSearch()

    // document.addEventListener('mousedown',(e:any)=>{
    //     if(e.target)
    //     {
    //         if(e.target.classList.contains('active-annot')){
    //             console.log("active annot",e.target);
    //             this.setStatus(e, 2);
    //         }
    //         else if(e.target.parentElement && e.target.parentElement.classList.contains('active-annot') && e.target.classList.contains('resize-action-down'))
    //         {
    //             console.log("resize down",e.target);
    //             this.setStatus(e, 1);
    //         }
    //         else if(e.target.parentElement && e.target.parentElement.classList.contains('active-annot') && e.target.classList.contains('resize-action-up'))
    //         {
    //             console.log("resize up",e.target);
    //             this.setStatus(e, 3);
    //         }
    //     }

    // });

    /*shape.addEventListener('mousedown', ($event) => {
    
    });
    resizeDivDown.addEventListener('mousedown', ($event) => {
        this.setStatus($event, 1);
    });
    resizeDivUp.addEventListener('mousedown', ($event) => {
        this.setStatus($event, 3);
    });*/
  }

  public captureKey(event) {
    if(this.comments && Object.keys(this.comments).length) {
      if(event.key=='Delete'){
         this.cancelNewComment()
      }
    }
  }
  
  getCommentIdFromString(id_string)
  {
      return id_string.substr(0, id_string.lastIndexOf("-"));
  }
  makeAllInActive() {
    document.querySelectorAll(".resizable-draggable.active-annot").forEach(elem => elem.classList.remove('active-annot'));
  }
  makeAllUnHighlight() {
    document.querySelectorAll("svg .highlightShape").forEach(elem => elem.classList.remove('highlightShape'));
    document.querySelectorAll(".comment-body.highlightShape").forEach(elem => elem.classList.remove('highlightShape'));
  }
  makeHighlight(id, type){
    this.makeAllUnHighlight();
    let shape = document.getElementById(`${id}-${type}`);
    if(shape)
    {
        shape.classList.add('highlightShape')
    }
    let comment = document.getElementById(`${id}-comment`)
    if(comment)
    {
        comment.classList.add('highlightShape')
    }
    
  }

  public updateCurrentTab(value: number) {
    this.videoPageService.updateCurrentTab(value);
  }

  getDocumentComments() {
    let obj = {
      huddle_id: this.huddle_id,
      user_id: this.user_id,
      document_id: this.artifacts_id,
      account_id: this.accountId,
      page_sort: this.page_sort,
      comment_status: this.comment_status,
      search_text : this.searchData
    }
    if (this.custom_marker) obj['custom_marker'] = this.custom_marker
    this.appMainService.GetDocumentComments(obj).subscribe((data: any) => {
      if(data.success) {
        this.breadCrumbsAssign(data);
        this.commentsList = data.comments.Document.comments;
        this.customMarkers = data.custom_markers;
        this.documentDetail = data.document_detail
        this.documentDetail.is_workspace = data.is_workspace
        this.permissions.AllowCustomMarkers = data.video_markers_check;
        this.permissions.rubric_check = data.rubric_check;
        this.permissions.can_reply = false;
        this.pdf_name = data.document_detail.title;
        this.document_extension = data.document_extension;
        this.coachee_permission = data.coachee_permission;
        this.huddle_role = data.huddle_role;
        this.permissions.huddle_role = data.huddle_role;
        this.permissions.allow_per_video_to_coachee = data.allow_per_video_to_coachee;
        this.huddle_type = data.huddle_type;
        this.is_submitted = data.is_submitted;
        this.fileSizeFileInfo = this.formatBytes(this.documentDetail['file_size']);
        this.docApiStatus = true;
        if (data.framework_assigned) this.currentFrameworkId = data.framework_id;
        // this.listIcons(this.commentsList);
        this.commentsList.forEach(function(itm){
          itm.valid = true;
         });
        if (this.viewer_mode == 'true' || this.huddle_role == 220) {
          this.viewer_mode = 'true';
        }
        this.checkForPermitions();
        let renderPages = [1];
        if (this.totalPages > 1) {
          renderPages = [1, 2];
        }
        renderPages.forEach(pageNum => this.initPageAnnotations(pageNum)); // load annotations for first two pages because on page init only two pdf pages are loaded

        this.commentsList.forEach(element => {
          this.totalReplies += element.Comment.responses.length;
        });
        console.log(this.totalReplies);
        
      } else {
        if (this.pageType === 'workspace-video-page') {
          this.router.navigate(["/workspace/workspace/home/grid"])
        } else if (this.pageType === 'huddle-video-page') {
          this.router.navigate(["/video_huddles/list"])
        }
      }
    })

  }
  
  public formatBytes(a,b=2){if(0===a)return"0 Bytes";const c=0>b?0:b,d=Math.floor(Math.log(a)/Math.log(1024));return parseFloat((a/Math.pow(1024,d)).toFixed(c))+" "+["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"][d]}

  public breadCrumbsAssign(data){
    let breadCrumbs: BreadCrumbInterface[] = [];
    //Breadcrumbs when user opens PDF through Workspace
    if (this.pageType === this.videoPageType.WORKSPACE) {
      breadCrumbs.push({ title: this.translations.header_list_worksapce, link: '/workspace/workspace/home/grid' });
      if(data.breadcumbs) {
        data.breadcumbs.forEach(element => {
          breadCrumbs.push({ title: element.folder_name, link: '/workspace/workspace/home/grid/'+ element.folder_id });
        });
      }
    }
    //Breadcrumbs when user opens PDF through Huddle
    else if (this.pageType === this.videoPageType.HUDDLE) {
      breadCrumbs.push({ title: this.translations.huddle_breadcrumb_huddle, link: '/video_huddles/list' });

      if(data.bread_crumb_output){
        data.bread_crumb_output.forEach(element => {
          breadCrumbs.push({ title: element.folder_name, link: '/video_huddles/list/'+ element.folder_id });
        });
      }

      breadCrumbs.push({ title: data.huddle_detail.name, link: '/video_huddles/huddle/details/'+ data.huddle_detail.account_folder_id +'/artifacts/grid/' });

      if(data.breadcumbs){
        data.breadcumbs.forEach(element => {
          breadCrumbs.push({ title: element.folder_name, link: '/video_huddles/huddle/details/'+ element.account_folder_id +'/artifacts/grid/'+ element.id });
        });
      }
    }
    
    breadCrumbs.push({ title: data.document_detail.title, link: '' });      //default unclickable breadcrumb on pdf page
    
    //pdating breadcrums on main service
    this.appMainService.updateBreadCrumb(breadCrumbs);
  }

  public ChooseCustomTag(tag, index) {

    if (tag == '') {
      this.selectedTag = {};
      this.selectedTagColor = 'grey'
    } else {
      this.selectedTag = tag;
      this.selectedTagColor = this.colorClasses[index]
    }
  }


  public pdfPreviewRender(pdfDoc) {
    for (let num = 1; num <= pdfDoc.numPages; num++) {
      pdfDoc.getPage(num).then(page => this.renderPage(page, num));
      let previewDiv = document.getElementById('pdfPreview');
      let div = document.createElement("div");
      div.addEventListener('click', ($event: any) => {
        if ($event.target.dataset.pageid)
          this.scrollToPage2($event.target.dataset.pageid)
      });
      div.setAttribute('data-pageId', num.toString());
      let span = document.createElement("span");
      span.classList.add('page_number');
      span.innerHTML = num.toString();

      previewDiv.appendChild(div);
      //uncomment this for pdf preview
      pdfDoc.getPage(num).then(this.makeThumb)
        .then(function (canvas) {
          canvas.setAttribute('data-pageId', num.toString());

          div.appendChild(canvas);
          div.appendChild(span);
        });
    }
  }

  /** Socket functionality starts */
  private subscribeSockets() {
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(`workspace-${this.accountId}-${this.user_id}`)
      .subscribe(data => this.processEventSubscriptions(data)));
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(`huddle-details-${this.huddle_id}`)
      .subscribe(data => this.processEventSubscriptions(data)));
    this.subscriptions.add(this.socketService.pushEventWithNewLogic(`video-details-${this.artifacts_id}`)
      .subscribe(data => this.processEventSubscriptionsFramwork(data)));  
  }

  private processEventSubscriptions(res) { 
    console.log(res);
    switch (res.event) {
      case "resource_renamed":
        if(res.data.document_id == this.artifacts_id){
            this.mainService.updateVideoTitle(res.video_file_name);
            this.pdf_name = res.video_file_name;
            this.changeResourceData(res);
        }
        break;
      case "comment_added":
        this.changeResourceData(res);
        if(res.is_reply == 1) this.totalReplies = this.totalReplies + 1;
        break; 
      case "comment_deleted":
          if(res.parent_comment_id) this.totalReplies = this.totalReplies - 1;
          break;    
    }
  }

  private processEventSubscriptionsFramwork(res) { 
    console.log(res);
    switch (res.event) {  
      case "framework_selected":
        this.changeAssignedFramework(res);
        break;  
    }
  }

  public changeAssignedFramework(res){
    this.GetRubricById(res.data, true)
    this.currentFrameworkId = res.data;
    if (this.pageType != 'library-page') {
      this.toastr.info(this.translations.framework_selected);
    }
  }

  public changeResourceData(res, last_edit_date?){
    if(!last_edit_date) this.documentDetail.last_edit_date = res.data.last_edit_date;
    if(last_edit_date) this.documentDetail.last_edit_date = last_edit_date;
    if(res.data.title) this.documentDetail.title = res.data.title;
  }

  private renderPages(pdfDoc) {
    //this.pdfPreviewRender(pdfDoc);
    this.currentPage = 1;
    this.loaded = true;
    this.getDocumentComments();
    this.permissions.can_comment = true;

    // const comment = CommentModel.addComment('text-comment', 1, 'text-comment', 561, 1073, { name: this.userName });
    // const comment = CommentModel.addComment('audio-comment', 1, 'audio-comment', 561, 1073, { name: this.userName });
    // this.comments.push(comment);
    // this.activeComment = comment;
  }

  makeThumb(page) {
    // draw page to fit into 96x96 canvas
    let vp = page.getViewport({ scale: 1, });
    let canvas = document.createElement("canvas");
    let scalesize = 1;
    // canvas.width = vp.width * scalesize;
    // canvas.height = vp.height * scalesize;
    canvas.width = 130;
    canvas.height = 180;
    let scale = Math.min(canvas.width / vp.width, canvas.height / vp.height);
    // console.log(vp.width, vp.height, scale);
    return page.render({ canvasContext: canvas.getContext("2d"), viewport: page.getViewport({ scale: scale }) }).promise.then(function () {
      return canvas;
    });
  }


  /**
    * Get page info from document, resize canvas accordingly, and render page.
    * @param page Page
    * @param num Page number
  */
  private renderPage(page: any, num: number) {
    let viewport = page.getViewport({ scale: 1 });
    const scale = this.desiredWidth / viewport.width;
    viewport = page.getViewport({ scale: scale });

    // const pdfPageContainer = this.renderer.createElement('div');
    const pdfPageAnnotator = this.renderer.createElement('div');
    // const canvas = this.renderer.createElement('canvas');
    // const ctx = canvas.getContext('2d');
    // Render PDF page into canvas context
    // const renderContext = {
    //   canvasContext: ctx,
    //   viewport: viewport
    // };

    // this.renderer.setProperty(pdfPageContainer, 'id', `pdf-page-${num}`);
    // this.renderer.setStyle(pdfPageContainer, 'margin-bottom', '10px');

    this.renderer.setProperty(pdfPageAnnotator, 'id', `pdf-page-${num}-annot`);
    // this.renderer.setStyle(pdfPageAnnotator, 'height', `${Math.floor(viewport.height)}px`);
    // this.renderer.setStyle(pdfPageAnnotator, 'width', `${Math.floor(viewport.width)}px`);

    // this.renderer.setProperty(canvas, 'height', viewport.height);
    // this.renderer.setProperty(canvas, 'width', viewport.width);
    // this.renderer.setStyle(canvas, 'position', 'absolute');

    // this.renderer.appendChild(pdfPageContainer, canvas);
    // this.renderer.appendChild(pdfPageContainer, pdfPageAnnotator);
    this.renderer.appendChild(this.spdfCanvasContainer.nativeElement, pdfPageAnnotator);

    // page.render(renderContext);

    if (num === 1) {
      this.viewPortHeight = Math.floor(viewport.height) + 10;
      this.renderer.setStyle(this.spdfRenderer.nativeElement, 'max-height', `${this.viewPortHeight}px`);
    }
  }

  /**
   * Navigate page to next or previous based on param
   * @param navigateTo {'next' | 'previous'}
   */
  public navigatePage(navigateTo: 'next' | 'previous') {
    // if (navigateTo === 'next' && this.currentPage < this.totalPages) this.currentPage++;
    // if (navigateTo === 'previous' && this.currentPage > 1) this.currentPage--;
    if (navigateTo === 'next' && this.pdfViewer.currentPageNumber < this.totalPages) this.pdfViewer.currentPageNumber++;
    if (navigateTo === 'previous' && this.pdfViewer.currentPageNumber > 1) this.pdfViewer.currentPageNumber--;
    // this.scrollToPage2(this.pdfViewer.currentPageNumber);
  }

  handTool(){
    console.log(this.spdfRenderer);
    if(this.handToolActivate){
      let ele = document.getElementById('viewer')
        document.documentElement.style.setProperty('--some-cursor', 'grab');
        document.documentElement.style.setProperty('--foreign-cursor', 'grab');
      ele.style.cursor = 'grab';
      this.handToolListeners.push(this.renderer.listen(ele, 'mousedown', (event) => this.mouseDownHandler(event)));
      this.handToolListeners.push(this.renderer.listen(ele, 'mouseup', (event) => this.mouseUpHandler(event)));
    } else {
      this.clearHandToolListeners();
    }

  }

  clearHandToolListeners(){
    this.handToolActivate = false
    let ele = document.getElementById('viewer')
    ele.style.cursor = 'context-menu';
      document.documentElement.style.setProperty('--some-cursor', 'pointer');
      document.documentElement.style.setProperty('--foreign-cursor', 'scroll-all');
      if(this.handToolListeners && this.handToolListeners.length)
      {
          this.handToolListeners.forEach(listener => {
              listener();//this will remove previous event listeners
          })
      }
  }

   mouseDownHandler(e) {    
    let ele = document.getElementById('viewer')
   ele.style.cursor = 'grabbing';
       document.documentElement.style.setProperty('--some-cursor', 'grabbing');
       document.documentElement.style.setProperty('--foreign-cursor', 'grabbing');
    let pos = {
      left: this.spdfRenderer.nativeElement.scrollLeft,
      top: this.spdfRenderer.nativeElement.scrollTop,
      x: e.clientX,
      y: e.clientY,
  };

  ele.style.userSelect = 'none';
    this.handToolMoveListener.push(this.renderer.listen(this.spdfRenderer.nativeElement, 'mousemove', (event) => this.mouseMoveHandler(event,pos)));

    console.log('mouse down')
};

mouseMoveHandler(e,pos) {
  let ele = document.getElementById('viewer')
  // How far the mouse has been moved
  const dx = e.clientX - pos.x;
  const dy = e.clientY - pos.y;
  console.log('pos',pos);

  // Scroll the element
  this.spdfRenderer.nativeElement.scrollTop = pos.top - dy;
  this.spdfRenderer.nativeElement.scrollLeft = pos.left - dx;
  console.log('this.spdfRenderer.nativeElement.scrollTop',this.spdfRenderer.nativeElement.scrollTop);
  
};


mouseUpHandler(e) {
  let ele = document.getElementById('viewer')
  ele.style.cursor = 'grab';
    document.documentElement.style.setProperty('--some-cursor', 'grab');
    document.documentElement.style.setProperty('--foreign-cursor', 'grab');
  ele.style.removeProperty('user-select');
  console.log('this.eventListeners', this.eventListeners)
  if(this.handToolMoveListener && this.handToolMoveListener.length)
      {
          this.handToolMoveListener.forEach(listener => {
              listener();//this will remove previous event listeners
          })
      }
  console.log('this.eventListeners', this.eventListeners)
};  

  public getUserImgUrl() {
    if (this.userImage) {

      return "https://s3.amazonaws.com/sibme.com/static/users/" + this.userImage;
    }
    return environment.baseUrl + "/img/home/photo-default.png";
  }

  /**
   * 
   * @param pageNumber 
   * @param hideLineAnnot 
   */
  private scrollToPage(pageNumber: number, hideLineAnnot: boolean) {
    this.spdfRenderer.nativeElement.scrollTo(0, ((pageNumber - 1) * this.viewPortHeight)); // minus 1 from page because page numbering start from 1 and vertical scrolling start from 0
    if (hideLineAnnot) this.hideLineAnnot();
  }

  public changeActionBtn(actionBtn) {
    if(this.handToolActivate) this.clearHandToolListeners();
      this.mainService.ResetSelectedRubrics(); // reset selected performance levels
    if( this.activeBtn != 0) this.updateCurrentTab(0); // close the framework tab if its open 
    this.currentAction = actionBtn;
    this.addReply = false;
      if (!isEmpty(this.comments)) {
      this.cancelComment(this.comments, true)
    }
    this.commentListComponent.activeComment = {}
    this.commentListComponent.editDocumentComment = null;
    this.reDrawPreviousShape();
    this.currEditableCom = null;
  }

  commentAdded(comment) {
      if(comment && comment.fake_id)
      {
          this.removeElementFromDom(`${comment.fake_id}-${comment.type.split('-')[0]}`);
      }
    let addComment = this.commentsList.find(x => x.id == comment.id);
    if(!addComment) this.commentsList.push({ ...comment })
    // this.listIcons([comment]) // Todo write logic to add annotation on comment added
    const getPageAnnotContainer = this.getPageAnnotContainer(comment.page);
    this.renderRectAnnot(getPageAnnotContainer, comment);
    if(comment.created_by == this.user_id)
    {
        this.drawLineHelper(comment.id, comment.type)
    }
  }

  reDrawPreviousShape() {
    if (this.currEditableCom ) {
      if (!isEmpty(this.comments)) {
        this.cancelCommentMain(this.comments)
      }
      if(this.currEditableComRef)
      this.commentEdited(this.currEditableComRef);
    }
  }
  
  public clearDrawRectEvent(comment)
  {
      let pageSvg = document.getElementById(`pdf-page-svg-${comment.page}`)
      if(pageSvg && this.drawRect)
      {
          pageSvg.removeEventListener('mousemove', this.drawRect);
      }
  }

  public cancelNewComment(){
    this.removeElementFromDom(`${this.comments.id}-icon`);
    this.hideLineAnnot();
    this.removeElementFromDom(`${this.comments.id}-${this.comments.type.split('-')[0]}`)
    this.clearDrawRectEvent(this.comments);
    this.comments = {};
    this.currEditableCom = null;
  }
  
  commentEdited(comment) {
    this.commentsList.forEach(element => {
      if (element.id == comment.id) {
        element.height = comment.height;
        element.width = comment.width;
        element.xPos = comment.xPos;
        element.yPos = comment.yPos;

        element.default_tags = comment.default_tags;
        if(element.Comment && element.Comment)
        {
            element.Comment.default_tags = comment.Comment.default_tags;
        }
        
        this.removeElementFromDom(`${element.id}-${element.type.split('-')[0]}`)

        // this.listIcons([element])  // Todo write logic to add annotation on comment added
        const getPageAnnotContainer = this.getPageAnnotContainer(comment.page);
        this.renderRectAnnot(getPageAnnotContainer, comment);
      }
    });
  }
  
  removeElementFromDom(elementId)
  {
      let element = document.getElementById(elementId)
      if(element)
      this.renderer.removeChild(element.parentElement, element);
      let elementSillExists = document.getElementById(elementId)
      if(elementSillExists) elementSillExists.remove();
  }

  removePx(value) {
    return parseInt(value, 10);
  }

  listIcons(comments) {
    comments.forEach(comment => {
      if (comment.type != 'rect-comment') {
        return
        const span = this.renderer.createElement('span');
        const img = this.renderer.createElement('img');
        this.renderer.setAttribute(img, 'src', `assets/img/text-comment.svg`);
        this.renderer.setAttribute(img, 'alt', this.currentAction);
        this.renderer.setAttribute(img, 'id', `${comment.id}-image`);
        this.renderer.addClass(span, 'absolute-icon');
        this.renderer.setAttribute(span, 'x', `${comment.xPos}`);
        this.renderer.setAttribute(span, 'y', `${comment.yPos}`);
        this.renderer.setAttribute(span, 'id', `${comment.id}-icon`);
        this.renderer.appendChild(span, img);
        const annotElm = document.getElementById(`pdf-page-${comment.page}-annot`);
        this.renderer.appendChild(annotElm, span);
      }
      if (comment.type == 'rect-comment') {
        const shape = this.renderer.createElement('rect');
        this.renderer.setAttribute(shape, 'height', `${comment.height}`);
        this.renderer.setAttribute(shape, 'width', `${comment.width}`);
        this.renderer.setStyle(shape, 'position', `absolute`);
        this.renderer.setStyle(shape, 'border', `1px solid`);
        this.renderer.setAttribute(shape, 'x', `${comment.xPos}`);
        this.renderer.setAttribute(shape, 'y', `${comment.yPos}`);
        this.renderer.addClass(shape, 'resizable-draggable');
        this.renderer.setAttribute(shape, 'id', `${comment.id}-rect`);
        this.renderer.setAttribute(shape, 'fill', `rgba(255, 255, 255, 0.3) `);
        // this.renderer.setStyle(shape, 'transform', `translate3d(${xPos}px,${yPos}px,0px)`);
        // const resizeDivDown = this.renderer.createElement('div');
        // this.renderer.addClass(resizeDivDown, 'resize-action-down');
        // const resizeDivUp = this.renderer.createElement('div');
        // this.renderer.addClass(resizeDivUp, 'resize-action-up');
        // shape.addEventListener('mousedown', ($event) => {
        //   this.setStatus($event, 2);
        // });
        // resizeDivDown.addEventListener('mousedown', ($event) => {
        //   this.setStatus($event, 1);
        // });
        // resizeDivUp.addEventListener('mousedown', ($event) => {
        //   this.setStatus($event, 3);
        // });
        let anotationDiv = document.getElementById(`pdf-page-${comment.page}-annot`)
        this.renderer.appendChild(anotationDiv, shape);
        // this.renderer.appendChild(shape, resizeDivDown)
        // this.renderer.appendChild(shape, resizeDivUp)
      }
    });
  }

  getXposYPosAndPageIdNew(event) {
    // const pageId = event.target.parentElement.id.split("pdf-page-")[1] || this.activeComment.page // Arif-change
    let pageAnnotId = event.target.id.split('pdf-page-')[1];
    pageAnnotId = pageAnnotId ? pageAnnotId.split('-')[0] : null;
    const pageId = pageAnnotId || this.activeComment.page;

    if (!pageId) return;

    let xPos = event.offsetX;
    let yPos = event.offsetY;

    return { xPos, yPos, pageId };

    // const clickPos = this.getParentPosition(event.target);
    // let xPos = (event.clientX - clickPos.xPos) + this.spdfRenderer.nativeElement.scrollLeft;
    // let yPos = (event.clientY - clickPos.yPos) + this.spdfRenderer.nativeElement.scrollTop;

    // // check the xPos and Ypos with page width, lef and top, and not let it exceed it from the limit of page 
    // // so that icon can be placed within the page 
    // let element = event.target.parentElement
    // xPos = (xPos <= 0 || xPos <= element.clientLeft) ? element.clientLeft : (xPos >= (element.clientWidth - element.clientLeft)) ? (element.clientWidth - element.clientLeft) : xPos;
    // yPos = (yPos <= 0 || yPos <= element.clientTop) ? element.clientTop : yPos;


    // return { xPos, yPos, pageId }
  }

  getXposYPosAndPageId(event) {
    const pageId = event.target.parentElement.id.split("pdf-page-")[1] || this.activeComment.page
    if (!pageId) return;

    const clickPos = this.getParentPosition(event.currentTarget);
    let xPos = (event.clientX - clickPos.xPos) + this.spdfRenderer.nativeElement.scrollLeft;
    let yPos = (event.clientY - clickPos.yPos) + this.spdfRenderer.nativeElement.scrollTop;

    // check the xPos and Ypos with page width, lef and top, and not let it exceed it from the limit of page 
    // so that icon can be placed within the page 
    let element = event.target.parentElement
    xPos = (xPos <= 0 || xPos <= element.clientLeft) ? element.clientLeft : (xPos >= (element.clientWidth - element.clientLeft)) ? (element.clientWidth - element.clientLeft) : xPos;
    yPos = (yPos <= 0 || yPos <= element.clientTop) ? element.clientTop : yPos;


    return { xPos, yPos, pageId }
  }

  annotationBuilder(shapeType, id, idPostFix, xPos, yPos, pageId, elementType) {
    const shape = this.renderer.createElement(elementType);
    const img = this.renderer.createElement('img');
    this.renderer.addClass(shape, 'absolute-icon');
    this.renderer.setStyle(shape, 'left', `${xPos}px`);
    this.renderer.setStyle(shape, 'top', `${yPos}px`);
    this.renderer.setAttribute(shape, 'id', `${id}-${idPostFix}`);
    // this.renderer.setAttribute(span, 'title', `${comment.text}`);

    const annotElm = document.getElementById(`pdf-page-${pageId}-annot`);
    this.renderer.appendChild(annotElm, shape);

    if (shapeType == 'text-comment') {
      this.renderer.setAttribute(img, 'src', `assets/img/${shapeType}.svg`);
      this.renderer.setAttribute(img, 'alt', shapeType);
      this.renderer.setAttribute(img, 'id', `${id}-image`);
      this.renderer.appendChild(shape, img);
    }
    if (shapeType == 'rect-comment') {
      this.renderer.setStyle(shape, 'height', `0px`);
      this.renderer.setStyle(shape, 'width', `0px`);
      this.renderer.setStyle(shape, 'border', `1px solid`);
      this.renderer.addClass(shape, 'resizable-draggable');
      const resizeDivDown = this.renderer.createElement('div');
      this.renderer.addClass(resizeDivDown, 'resize-action-down');
      const resizeDivUp = this.renderer.createElement('div');
      this.renderer.addClass(resizeDivUp, 'resize-action-up');
      shape.addEventListener('mousedown', ($event) => {
        this.setStatus($event, 2);
      });
      resizeDivDown.addEventListener('mousedown', ($event) => {
        this.setStatus($event, 1);
      });
      resizeDivUp.addEventListener('mousedown', ($event) => {
        this.setStatus($event, 3);
      });
      this.renderer.appendChild(shape, resizeDivDown)
      this.renderer.appendChild(shape, resizeDivUp)
    }
  }


  public shadowCanvasClicked(event) {
    if (this.currentAction == null || this.currentAction != 'text-comment') return;
    // const { xPos, yPos, pageId } = this.getXposYPosAndPageId(event) Arif change
    const { xPos, yPos, pageId } = this.getXposYPosAndPageIdNew(event);

    if (!pageId) return;
    if (!isEmpty(this.comments)) {
      this.cancelComment(this.comments)
    }
    const comment = CommentModel.addComment(this.currentAction, pageId, this.currentAction, xPos, yPos, { name: this.userName }, this.artifacts_id);


    this.annotationBuilder(this.currentAction, comment.id, 'icon', xPos, yPos, pageId, 'span')
    this.comments = comment;
    this.currEditableCom = { ...comment };
    this.currEditableComRef = { ...this.currEditableCom };
    this.changeActiveComment(comment)

    this.currentAction = null;
  }

  /**
   * Return parent element x and y position
   * @param element Target element
   */
  private getParentPosition(element: HTMLElement | any): DomPosition {
    let xPos = 0;
    let yPos = 0;

    while (element) {
      if (element.tagName == "BODY") {
        // deal with browser quirks with body/window/document and page scroll
        const xScroll = element.scrollLeft || document.documentElement.scrollLeft;
        const yScroll = element.scrollTop || document.documentElement.scrollTop;

        xPos += (element.offsetLeft - xScroll + element.clientLeft);
        yPos += (element.offsetTop - yScroll + element.clientTop);
      } else {
        // for all other non-BODY elements
        xPos += (element.offsetLeft - element.scrollLeft + element.clientLeft);
        yPos += (element.offsetTop - element.scrollTop + element.clientTop);
      }

      element = element.offsetParent;
    }
    return { xPos: xPos, yPos: yPos };
  }
  
  private splitType(type)
  {
      return type.split('-')[0];
  }

  private getLinePos(commentId: number, type): LinePosition {
      if(!commentId || !type)
      {
          return;
      }
    let iconImageRect;
    let selector = document.getElementById(`${commentId}-${type.split('-')[0]}`) as any;
    this.makeHighlight(commentId,`${type.split('-')[0]}`)
    // selector.classList.add('highlightShape')

    iconImageRect = selector.getBoundingClientRect();
      selector.scrollIntoViewIfNeeded(true);
    let commentRect = document.getElementById(`${commentId}-comment`).getBoundingClientRect();
    if (!commentRect) commentRect = this.commentListComponent.getCommentPositon(commentId);
    const spdfBodyRec = this.spdfBody.nativeElement.getBoundingClientRect();
    // document.getElementById(`${commentId}-comment`).classList.add('highlightShape');
    const startPos: DomPosition = { xPos: iconImageRect.left - spdfBodyRec.left + iconImageRect.width, yPos: iconImageRect.top - spdfBodyRec.top + (iconImageRect.height / 2) - 2 };
    const endPos: DomPosition = { xPos: commentRect.left - spdfBodyRec.left, yPos: commentRect.top - spdfBodyRec.top };

    return { startPos, endPos };
  }


  private drawLine(linePosition: LinePosition) {
      if(!linePosition)
      {
          return;
      }
      if(linePosition.endPos.yPos<0) linePosition.endPos.yPos = 0
    // console.log('startPos: ', linePosition);
    // console.log('endPos: ', linePosition.endPos);
    const horizontalDistance = linePosition.endPos.xPos - linePosition.startPos.xPos;
    // console.log('horizontalDistance: ', horizontalDistance);
    const oneForth = Math.round(horizontalDistance / 4);
    const thirdForth = Math.round(horizontalDistance - oneForth);

    // console.log('oneForth: ', oneForth);
    // console.log('thirdForth: ', thirdForth);

    const spdfFirstHLineCSSProps = {
      display: 'block',
      border: '0.7px solid',
      width: `${oneForth-9}px`, // 9 is subtracted as a constant of variation of arrow head
      left: `${linePosition.startPos.xPos +10}px`, //  10 is added as a constant of variation of arrow head
      top: `${linePosition.startPos.yPos}px`
    };
    this.updateLineAnnotStyle(this.spdfFirstHLine.nativeElement, spdfFirstHLineCSSProps);

    let top: number;
    let height: number;
    if (linePosition.startPos.yPos > linePosition.endPos.yPos) {
      top = linePosition.endPos.yPos;
      height = linePosition.startPos.yPos - linePosition.endPos.yPos;
    } else {
      top = linePosition.startPos.yPos;
      height = linePosition.endPos.yPos - linePosition.startPos.yPos;
    }

    // console.log('top: ', top);
    // console.log('height: ', height);

    const spdfVLineCSSProps = {
      display: 'block',
      border: '0.7px solid',
      height: `${height + 1}px`, // 1 is added as a constant of variation
      left: `${linePosition.startPos.xPos + oneForth}px`,
      top: `${top}px`
    };
    this.updateLineAnnotStyle(this.spdfVLine.nativeElement, spdfVLineCSSProps);

    const spdfSecondHLineCSSProps = {
      display: 'block',
      border: '0.7px solid',
      width: `${thirdForth}px`,
      left: `${(linePosition.endPos.xPos - thirdForth) + 1}px`, // 1 is added as a constant of variation
      top: `${linePosition.endPos.yPos}px`
    };
    this.updateLineAnnotStyle(this.spdfSecondHLine.nativeElement, spdfSecondHLineCSSProps);
    this.currentAction = null;
  }

  private updateLineAnnotStyle(lineElem: any, cssProps: any) {
    Object.entries(cssProps).forEach(([key, value]) => {
      this.renderer.setStyle(lineElem, key, value);
    });
  }

  public hideLineAnnot(fromCommentList = 0) {
      this.hideShowResizeHandlers(1);
    const spdfLineCSSProps = { display: 'none', border: 'none', width: 0, height: 0, left: 0, top: 0 };
    [this.spdfFirstHLine.nativeElement, this.spdfVLine.nativeElement, this.spdfSecondHLine.nativeElement].forEach(element => {
      this.updateLineAnnotStyle(element, spdfLineCSSProps);
    });
    if(fromCommentList)
    {
        this.clearTimerIfExist();
        this.makeAllUnHighlight()
    }
  }
  
  public clearTimerIfExist()
  {
      if(this.annotationLineOnActiveTimers && this.annotationLineOnActiveTimers.length)
      {
          //dont load annotation line if user collapsed comment in comment l
          for (let i=0; i<this.annotationLineOnActiveTimers.length; i++) {
              clearTimeout(this.annotationLineOnActiveTimers[i]);
          }
      }
  }

  public selectMarker(comment: CommentModel, marker: any) {
    comment.marker = marker;
  }

  public updateCustomTags(tags: any) {
    const activeComment = this.comments.id === this.activeComment.id;
    if (activeComment) this.comments.tags = tags;
  }

  public unSelectRubric(comment: CommentModel, rubric: any, index: number) {
    comment.standards.splice(index, 1);
    this.videoPageService.updateSelectedRubric({ rubric, selected: false });
    if (comment.standards.length <= 0) this.bsModalRef.hide();
  }

  public cancelComment(comment, deleteFromList?) {
    // event.stopPropagation();
    this.hideShowResizeHandlers(1);
    this.activeComment = {};
    if(deleteFromList)
    {
        this.hideLineAnnot();
    }
    this.cancelCommentMain(comment, deleteFromList);
    this.commentsList = this.commentsList.filter(function(el) { return  el.id != comment.id; }); //updating comment list on deleteing comment
  }

  public cancelCommentMain(comment, deleteFromList?) {
      if(deleteFromList)
      {
          this.removeElementFromDom(`${comment.id}-${comment.type.split('-')[0]}`)
          this.clearDrawRectEvent(comment);
      }
    if (!deleteFromList) {
      this.comments = {}
      this.selectedTag = {}
      this.selectedTagColor = 'grey'
      this.add_comment_status = 1
    }
  }

  public addComment(comment: CommentModel, index: number, event: Event, addReply) {
    event.stopPropagation();
    this.hideLineAnnot();
    this.activeComment = {};

    if (addReply) {
      let reply = ReplyModel.addReply(this.currentAction, comment.page, this.currentAction, comment.xPos, comment.yPos, { name: this.userName })
      reply.text = JSON.parse(JSON.stringify(this.replyText));
      comment.replies.push({ ...reply })
      this.replyText = ''
    }
  }

  public opentagsModal() {
    this.bsModalRef = this.bsModalService.show(this.selectedrubricsTemplate, { class: "modal-md maxcls", backdrop: 'static' })
  }

  public onScrollPdf() {
    // console.log('current page number',this.pdfViewer.currentPageNumber)
    this.hideLineAnnot();
    // // let pageHeight = document.getElementById(`pdf-page-${this.currentPage}`)
    // // console.log('page height', pageHeight.offsetHeight)
    // // console.log('calculated page', Math.round(this.spdfRenderer.nativeElement.scrollTop / pageHeight.offsetHeight) + 1)
    // // this.currentPage = Math.round(this.spdfRenderer.nativeElement.scrollTop / pageHeight.offsetHeight) + 1; // if half page scrolled, caculate as full page scroll
    // this.currentPage = this.pdfViewer.currentPageNumber
  }

  public onScrollComments() {
    this.hideLineAnnot();
  }
  public triggerActiveCommentById(commentId)
  {
      this.pdfCommentDiv = true;
      setTimeout(()=>{
          let comment = this.commentsList.find(x => x.id == commentId);
    
          if(!comment && this.comments)
          {
              comment = this.comments;
          }
          if(comment){
              this.triggerActiveComment(comment);
              this.commentListComponent.changeActiveComment(comment)
          }
      },100)
  }

  public closeFrameworkDiv() {
    this.updateCurrentTab(0);
    this.changeFramworkSettingTab(0);
    this.drawLineHelper(this.activeComment.id, this.activeComment.type);
  }

 public changeFramworkSettingTab(val){
  this.videoPageService.updateCurrentTabOnClose(val);
 }

  public triggerActiveComment(comment, fromDivClick = false) {
    this.commentListComponent.clearActiveComment();
    this.commentListComponent.editDocumentComment = null
      if(fromDivClick)
      {
          this.dontFireOnSelfClick = true;
      }
    this.changeActiveComment(comment)
  }

  public changeActiveComment(comment) {

    if(this.comments && this.comments.id && !comment.hasOwnProperty('fakeComment')){
      this.cancelComment(this.comments, true)
    } 
      if(this.currEditableComRef)
      {
          //this.reDrawPreviousShape();
      }
    
    // let shape = document.getElementById(`${comment.id}-${comment.type.split('-')[0]}`)
    // if (shape) shape.classList.add('active-annot');

    // if (comment.id !== this.activeComment.id) {
    this.activeComment = comment;
    // this.currentPage = comment.page;
    this.pdfViewer.currentPageNumber = +comment.page;
    //this.scrollToPage2(this.currentPage);
      this.scrollToAnnotation(comment.id);
    //added timeout because sometime annotations are on a page which is not loaded yet
      let timeout = 20;
      if(this.pdfViewer.currentPageNumber != 1)
      {
          timeout = 500;
      }
      this.annotationLineOnActiveTimers.push(setTimeout(()=>{
        if (comment.type == 'rect-comment') {
            this.loadContainer(comment.page)
            this.loadBox()
        }
        this.drawLineHelper(comment.id, comment.type)
    },timeout));

    this.addReply = (this.activeComment.text !== "") ? true : false;

    this.artifactDataId = {
      id: this.artifacts_id,
      page: comment.page,
      xPos: comment.xPos,
      yPos: comment.yPos,
      width: comment.width,
      height: comment.height,
      type: comment.type,
      fake_id: comment.id,
        page_height: this.getPageOriginalHeightWidth(comment.page, 0),
        page_width: this.getPageOriginalHeightWidth(comment.page, 1),
    }
    this.updateArtifactData(comment);
    // }
  }
  
  private updateArtifactData(comment)
  {
      let shape = this.getShapeFromCommentId(comment.id, comment.type);
      if(shape)
      {
          let latestAttributes = this.getSvgAttributes(shape);
          console.log('latestAttributes', latestAttributes);
          
          this.artifactDataId = {...this.artifactDataId, ...latestAttributes};
          if(this.activeComment && this.activeComment.fakeComment && this.activeComment.id == comment.id)
          {
              this.activeComment.annotation_data = this.artifactDataId;
          }
      }
  }
  
  private getShapeFromCommentId(id, type)
  {
      let correctType = this.splitType(type);
      let element = document.getElementById(`${id}-${correctType}`) as any;
      return element;
  }


  private GetRubrics(huddle_id, account_id) {

    this.mainService.GetRubrics({ huddle_id: huddle_id, account_id: account_id, video_id: this.artifacts_id }).subscribe((data: any) => {

      this.rubrics = data.data;

      /** In case of add comment local storage selected framewors then also select framework rubric start */
      // let datafromlS = localStorage.getItem(this.header_data.user_current_account.User.id + '_video_play_comment_' + this.params.video_id);
      // let ParsedDataFromLs = JSON.parse(datafromlS);
      // if (ParsedDataFromLs && Array.isArray(ParsedDataFromLs.selectedRubrics)) {
      //   ParsedDataFromLs.selectedRubrics.forEach(lsRubric => {
      //     let rubricExisted = this.rubrics.account_tag_type_0.find(rubric => rubric.account_tag_id == lsRubric.account_tag_id);
      //     if (rubricExisted) rubricExisted.selected = true;
      //   });
      // }
      /** In case of add comment local storage selected framewors then also select framework rubric end */

    });

  }


  public GetRubricById(framework_id, assign?) {
    if (!framework_id) {
      this.rubricPreview = {};
      return;
    }

    if (framework_id) {
      this.appMainService.getFrameworkSettingsById(framework_id).subscribe((rubrics: any) => {
        this.rubricPreview = rubrics.data;
        if (assign) this.rubrics = this.rubricPreview;
      });
    }
  }


  public AssignFramework(template, flag) {
    if (flag == 0) {
      this.rubricPreview = "";
      this.VideoHuddleFramework = "";
      this.GetRubricById("");
      return;
    } else if (flag == 1) {
      this.modalRef = this.modalService.show(template, { class: 'modal-md', backdrop: 'static' });
    }
  }



  public ResolveAssignFramework(flag, is_internal?) {
    if (flag == 0) {
      this.modalRef.hide();
    } else {

      if (!is_internal) this.modalRef.hide();

      const obj = {
        huddle_id: this.huddle_id,
        video_id: this.artifacts_id,
        framework_id: this.VideoHuddleFramework,
        user_id: this.headerService.getUserId()
      }

      this.mainService.SetFrameworkForVideo(obj).subscribe((data: any) => {
        if (!data.status) {
          if (!is_internal) {
            this.toastr.info(this.translations.vd_framework_selected_for_video);
          }
          let video_framework_id = data.video_framework_id;
          this.GetRubricById(video_framework_id, true);
          // this.permissions.framework_selected_for_video = "1";
        } else {
          this.currentFrameworkId = this.VideoHuddleFramework;
          this.GetRubrics(this.huddle_id, this.accountId);
        }
      });
    }
  }

  openRenameModal() {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'rename-modal-container',
      initialState: {
        artifact: this.documentDetail,
        pageType: this.pageType,
      }
    };

    this.bsModalRef = this.modalService.show(RenameModalComponent, config);
  }

  OpenDeleteModal() {
    let pageTypeData;

    if (this.pageType === 'workspace-video-page') {
      pageTypeData = 'workspace-page'
    } else if (this.pageType === 'huddle-video-page') {
      pageTypeData = 'huddle-page'
    }

    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'delete-modal-container',
      initialState: {
        artifact: this.documentDetail,
        pageType: pageTypeData,
        page: "videoPage",
        huddle_type: this.huddle_type
      }
    };

    this.bsModalRef = this.modalService.show(DeleteModalComponent, config);
  }

  DownloadResource(res) {
    if (res.static_url && false) {
    } else {
      let obj: any = {};
      obj.document_id = res.doc_id;
      obj.account_id = res.account_id;
      ({
        User: {
          id: obj.user_id
        }
      } = this.headerData.user_current_account);

      this.workService.DownloadResource(obj);
    }
    this.toastr.info(this.translations.artifacts_your_file_is_downloading)
  }

  DownloadResourceAnnotation(res) {
    if (res.static_url && false) {
    } else {
      let obj: any = {};

      obj.document_id = res.doc_id;
      obj.account_id = res.account_id;
      obj.user_id = this.user_id;
      obj.huddle_id = res.account_folder_id;
      obj.page_width = this.pdfPageStndrdSize.width;
      obj.page_height = this.pdfPageStndrdSize.height;

      this.workService.DownloadResourceAnn(obj);
    }
    this.toastr.info(this.translations.artifacts_your_file_is_downloading)
  }

  public openNewShareModal() {
    const initialState = { artifact: this.documentDetail, pageType: this.pageType, modal_for: 'for_artifact_share' };
    this.modalService.show(ArtifactShareModelComponent, { initialState, class: 'share-modal-container', backdrop: 'static' });
  }

  OpenDuplicateResourceModal(artifact) {

    if (this.commentsList.length > 0 ) {
      const config: ModalOptions = {
        backdrop: 'static',
        keyboard: false,
        class: 'duplicate-modal-container',
        initialState: {
          artifact: artifact,
          pageType: this.pageType,
          parent_folder_id: this.parent_folder_id
        }
      };

      this.bsModalRef = this.modalService.show(DuplicateResourceModalComponent, config);
    }
    else this.DuplicateResource(artifact);
  }


  DuplicateResource(artifact: any) {
    let obj: any = {};

    obj.document_id = artifact.doc_id;
    obj.account_folder_id = [artifact.account_folder_id];
    obj.current_huddle_id = artifact.account_folder_id;
    obj.account_id = artifact.account_id;
    obj.copy_notes = 1;
    obj.is_duplicated = true;
    obj.shareAssets = false;
    obj.doc_type = artifact.doc_type;
    obj.parent_folder_id = this.parent_folder_id;

    ({ User: { id: obj.user_id } } = this.headerData.user_current_account);

    if (this.pageType === 'workspace-video-page') {
      obj.workspace = true,
        obj.from_workspace = 1;
    }

    this.workService.DuplicateResource(obj).subscribe(data => {
      let d: any = data;
      let type = this.headerService.isAValidAudio(d.data.file_type);
      if (type == false) {
        this.toastr.info(d.message);
      }
      else {
        this.toastr.info(this.translations.audio_duplicate_successfully);
      }
    }, error => {
      this.toastr.error(error.message)

    });
  }

onSearch(operation = "find") {
    this.pdfViewer.findController.executeCommand(operation, {
      caseSensitive: this.find.caseSensitive,
      findPrevious: this.find.findPrevious,
      entireWord: this.find.entireWord,
      phraseSearch: true,
      query: this.find.searchString,

    });
    setTimeout(() => {
      this.find.totalMatches = this.pdfViewer.findController._matchesCountTotal
    }, 1000);
    let page = this.pdfViewer.findController.selected.pageIdx;
    if (page >= 0) this.scrollToPage2(page + 1)
  }

  clearSearch() {
    this.find.caseSensitive = false;
    this.find.entireWord = false;
    this.find.findPrevious = false;
    this.find.searchString = '';

    this.onSearch();
  }

  scrollToPage2(page) {
    this.pdfViewer.currentPageNumber = page

    // if (this.currentPage != page) {
    // this.currentPage = page
    // let element = document.getElementById(`pdf-page-${page}`);
    // if (element)
    //   element.scrollIntoView(false);
    // }
  }
  
  scrollToAnnotation(id) {
    let element = document.querySelector("[id^='"+id+"']") as any;
    if (element)
      element.scrollIntoViewIfNeeded(true);
  }
  nextMatch() {
    this.pdfViewer.findController._nextMatch()
    let page = this.pdfViewer.findController.selected.pageIdx
    setTimeout(() => {
      this.find.totalMatches = this.pdfViewer.findController._matchesCountTotal
    }, 1000);
    if (page >= 0) this.scrollToPage2(page + 1)
  }


  previousSearch() {
    if (!this.find.findPrevious) {
      this.find.findPrevious = true;
        this.onSearch("findagain")
    } else {
      this.nextMatch();
    }
  }

  nextSearch() {
    if (this.find.findPrevious) {
      this.find.findPrevious = false;
        this.onSearch("findagain")
    } else {
      this.nextMatch();
    }
  }
  
  updateScaleSelectElement(newScale)
  {
      this.pdfCurrentScale = newScale;
  }
  
  updatePDFScale(scale)
  {
      this.moveDiv("resize-action-up", `resize-dots-container`);
      this.moveDiv("resize-action-down", `resize-dots-container`);
      this.hideLineAnnot();
      this.pdfViewer.currentScaleValue = scale;
  }

  zoomIn() {
    this.hideLineAnnot();
      this.moveDiv("resize-action-up", `resize-dots-container`);
      this.moveDiv("resize-action-down", `resize-dots-container`);
    let newScale = this.pdfViewer.currentScale;
    if (newScale < scale.MAX_SCALE) {
      newScale = (newScale + scale.DEFAULT_SCALE_DELTA).toFixed(2);
      //newScale = Math.ceil(newScale * 10) / 10;
      newScale = Math.min(scale.MAX_SCALE, newScale);
    }
    this.pdfViewer.currentScaleValue = newScale;
      this.updateScaleSelectElement(newScale);
    // document.documentElement.style.setProperty('--some-scale', newScale);

  }
  zoomOut() {
    this.hideLineAnnot();
      this.moveDiv("resize-action-up", `resize-dots-container`);
      this.moveDiv("resize-action-down", `resize-dots-container`);
    let newScale = this.pdfViewer.currentScale;
    if (newScale > scale.MIN_SCALE) {
        newScale = (newScale - scale.DEFAULT_SCALE_DELTA).toFixed(2);
      //newScale = Math.floor(newScale * 10) / 10;
      newScale = Math.max(scale.MIN_SCALE, newScale);
    }
    this.pdfViewer.currentScaleValue = newScale;
      this.updateScaleSelectElement(newScale);
    // document.documentElement.style.setProperty('--some-scale', newScale);

  }

  drawShapes(xPos, yPos, pageId) {
    this.loadContainer(pageId)
    if (!isEmpty(this.comments)) {
      this.cancelComment(this.comments)
    }
    const comment: any = CommentModel.addComment(this.currentAction, pageId, this.currentAction, xPos, yPos, { name: this.userName }, this.artifacts_id);
    this.comments = comment;
    this.currEditableCom = this.activeComment = comment;
    this.annotationBuilder(this.currentAction, comment.id, 'rect', xPos, yPos, pageId, 'div')
    this.loadBox();
  }

  private loadBox() {
      if(!this.activeComment || !this.activeComment.type)
      {
          return;
      }
    let shape = document.getElementById(`${this.activeComment.id}-${this.activeComment.type.split('-')[0]}`) as any;
    if (!shape) {//no comment is active
      return;
    }
    shape.style.zIndex = '1000'

    let width = +shape.getAttribute('width');
    let height = +shape.getAttribute('height');
    let left = +shape.getAttribute('x');
    let top = +shape.getAttribute('y');
      const {right, bottom } = shape.getBoundingClientRect();
      this.boxPosition = { shape, left, top, right, bottom, height, width };
    let box = shape.parentElement.viewBox.baseVal;
    this.viewBox = {height: box.height,width: box.width }
  }

  private loadContainer(pageId) {
    let page = document.getElementById(`pdf-page-${pageId}`);
    if(!page)
    {
        return;
    }
    const { left, top, right, bottom, width, height } = page.getBoundingClientRect();
    // const right = left + width;
    // const bottom = top + height;
    this.containerPos = { page, left, top, right, bottom, width, height };
  }

  setStatus(event: any, status: number) {
      if(this.handToolActivate) {
          return;
      }
    if (status === 1 || status === 3) event.stopPropagation();
    else if (status === 2) {
      //console.log(document.elementsFromPoint(event.x, event.y));
      this.hideShowResizeHandlers(1);
      let clickedElement = this.getClickedElement(event);
      let clickedCommentId = event.currentTarget.id;
      if(clickedElement)
      {
          clickedCommentId = clickedElement.getAttribute('id');
      }
      let commentId = this.getCommentIdFromString(clickedCommentId);
      // if(this.currEditableCom && this.currEditableCom.id != commentId) this.triggerActiveCommentById(commentId);

      // ---------------- Edit comment option opens if user selects on hand icon ---------------- 
      /*if(this.handToolActivate) {
        this.commentsList.forEach(element => {
          if(element.id == commentId) {
            this.commentListComponent.openEditComment(element);
          }
        });
        this.handToolActivate = false;
      }*/
      // ---------------- Edit comment option opens if user selects on hand icon ---------------- 

      if(!this.currEditableCom && !this.currentAction) this.triggerActiveCommentById(commentId);
      if(!this.currEditableCom || this.currEditableCom.id != this.activeComment.id)
      {
          return;
      }

      this.loadContainer(this.activeComment.page)
      this.loadBox()
      if (!this.boxPosition || !this.boxPosition.shape) {
        return;//no active comment
      }
      let mouse = this.getMousePosition(this.boxPosition.shape, event)
      // mouse.x -= parseFloat(this.boxPosition.shape.getAttributeNS(null, "x"));
      // mouse.y -= parseFloat(this.boxPosition.shape.getAttributeNS(null, "y"));
      // let bbox = this.boxPosition.shape.getBBox();
      // this.mouseClick = { x: mouse.x, y: mouse.y, left: bbox.x, top: bbox.y };
      // let left = +this.boxPosition.shape.getAttribute('cx');
      // let top = +this.boxPosition.shape.getAttribute('cy');
      // let bbox = this.boxPosition.shape.getBBox();
      this.mouseClick = { x: mouse.x, y: mouse.y, left: this.boxPosition.left, top: this.boxPosition.top };

    }
    // else this.loadBox();
    
    this.status = status;
  }

  handleMouseDown(event) {
    if (this.currentAction == 'rect-comment') {
      // const { xPos, yPos, pageId } = this.getXposYPosAndPageId(event) // Arif change
      const { xPos, yPos, pageId } = this.getXposYPosAndPageIdNew(event)
      this.drawShapes(xPos, yPos, pageId)
      this.setStatus(event, 1);

    }
  }
  
  drawLineHelper(commentId, type, condition = true)
  {//this will draw a line from comment div to its annotation
      if(commentId && type)
      {
          this.pdfCommentDiv = true;
          this.annotationLineOnActiveTimers.push(setTimeout(()=>{
              let el = document.getElementById(`${commentId}-comment`) as any;
              if(el)
              {
                  el.scrollIntoViewIfNeeded(true);
                  this.annotationLineOnActiveTimers.push(setTimeout(()=>{
                      if(condition)
                      {
                          let linePosition: LinePosition = this.getLinePos(commentId, type);
                          //this.scrollToAnnotation(commentId);
                          this.drawLine(linePosition);
                      }
                  },50));
              }
              else
              {
                  console.log('Not drawing arrow line as comment div not found')
              }
          },200));
      }
      else
      {
          console.log('commentId and type are required to draw arrow line')
      }
  }
  
  getClickedElement(e) {
    let main = null;
    if (document.elementsFromPoint)
    {
        let elements = document.elementsFromPoint(e.x, e.y);
        // console.log(elements);
        let onlyRects = elements.filter(ele => (ele.classList.contains('absolute-icon')  || ele.classList.contains('resizable-draggable')));
        //console.log(onlyRects);
        if(onlyRects && onlyRects.length)
        {
            main = onlyRects.shift();
            onlyRects.forEach((el:any) => {
                
                let mainBox = main.getBBox();
                let elBox = el.getBBox();
                let mainContainEl = this.areRectanglesOverlap(mainBox,elBox);
                if(mainContainEl)
                {
                    console.log('element found')
                    main = el;
                }
                //main = el;
            })
        }
        
    }
    return main;
  }
    areRectanglesOverlap = (a, b) => {
        return !(
            b.x < a.x ||
            b.y < a.y ||
            b.x+b.height > a.x+a.height ||
            b.y+b.width > a.y + a.width
        );
    }

  handleMouseUp($event) {
      if(this.handToolActivate) {
          return;
      }
      this.setStatus($event, 0);
      if (this.freeDrawingPath) {
          this.freeDrawingPath = null;
      }
      if(!this.activeComment || !this.activeComment.type )
      {
          console.log("active comment not found so returning");
          return;
      }
      if(this.pdfViewer.currentPageNumber == this.activeComment.page) {
        let el = document.getElementById(`${this.activeComment.id}-comment`) as any;
        el.scrollIntoViewIfNeeded(true);
    }
      setTimeout(() => {
          if(!this.activeComment || !this.activeComment.type)
          {
              console.log("active comment not found so returning");
              return;
          }
          let shouldDrawLine = (this.pdfViewer.currentPageNumber == this.activeComment.page || ($event.target && ($event.target.classList.contains('resizable-draggable') || $event.target.classList.contains('absolute-icon'))));
        this.drawLineHelper(this.activeComment.id, this.activeComment.type, shouldDrawLine);
     
          this.updateActiveCommentBBox(this.activeComment)
          this.updateArtifactData(this.activeComment);
      if(((this.comments && this.activeComment  && this.comments.id == this.activeComment.id) || (this.currEditableCom && this.activeComment  && this.currEditableCom.id == this.activeComment.id)) && this.activeComment.type != 'text-comment')
      {
          this.hideShowResizeHandlers();
      }

    }, 100);
    if(this.currentAction)
    {
      this.currentAction = null
    }
    
  }
  
  updateActiveCommentBBox(comment)
  {
      let shape = this.getShapeFromCommentId(comment.id, comment.type);
      if(!shape)
      {
          console.log("active comment shape not found so returning");
          return;
      }
      let bbox = shape.getBBox();
      this.activeComment.yPos = bbox.y
      this.activeComment.xPos = bbox.x
      this.activeComment.height = bbox.height
      this.activeComment.width = bbox.width
      this.artifactDataId.yPos = bbox.y
      this.artifactDataId.xPos = bbox.x
      this.artifactDataId.height = bbox.height
      this.artifactDataId.width = bbox.width;
      this.artifactDataId = {...this.artifactDataId};
  }
  
  getSvgAttributes(element)
  {
      let attributes=[];
      let exclude = ['id', 'class', 'style', 'stroke', 'border']
      for (let att, i = 0, atts = element.attributes, n = atts.length; i < n; i++){
          att = atts[i];
          if(!exclude.includes(att.nodeName))
          {
              attributes[att.nodeName] = att.nodeValue;
          }
      }
      attributes['node_name'] = element.nodeName;
      if(element.nodeName === "foreignObject") {
        attributes['innerHTML'] = element.innerHTML
        attributes['text'] = element.childNodes[0].innerText;
        if(!this.dontFireOnSelfClick)
        {
            this.mainService.updateForeignTextComment({commentText:element.firstChild.innerHTML.replace(/ /g, '&nbsp;'), id:this.activeComment.id, from:'pdf-renderer'});
        }
        else
        {
            this.dontFireOnSelfClick = false;
        }
      }
      console.log(attributes);
      return attributes;
  }

  onMouseMove(event: MouseEvent, pageSvg) {
      if (this.freeDrawingPath) {
          this.appendToBuffer(this.svgPoint(pageSvg, event.clientX, event.clientY));
          this.updateSvgPath();
      }
    if (!this.status) return;
    event.preventDefault();
    let mouse = this.getMousePosition(this.boxPosition.shape, event)
    this.mouse = { x: mouse.x, y: mouse.y };
    if (this.status === Status.DOWNRESIZE) this.resizeDown();
    else if (this.status === Status.UPRESIZE) this.resizeUp();
    else if (this.status === Status.MOVE) this.move();
  }

  private resizeDown() {
    if (this.resizeCondMeet()) {
      let page = document.getElementById(`pdf-page-${this.activeComment.page}`)
      let width = Number(this.mouse.x > this.boxPosition.left) ? this.mouse.x - this.boxPosition.left : 0;
      let height = Number(this.mouse.y > this.boxPosition.top) ? this.mouse.y - (this.boxPosition.top > 0 ? this.boxPosition.top : 0) : 0;
      this.boxPosition.shape.style.height = `${height}px`;
      this.boxPosition.shape.style.width = `${width}px`;
    }
  }
  private resizeUp() {

    if (this.resizeUpCondMeet()) {
      if (!this.isUpResizingOn) {

        let bottom = this.containerPos.height - (this.boxPosition.shape.offsetHeight + this.boxPosition.shape.offsetTop)
        let right = this.containerPos.width - (this.boxPosition.shape.offsetWidth + this.boxPosition.shape.offsetLeft)
        this.renderer.setStyle(this.boxPosition.shape, 'bottom', `${bottom}px`);
        this.renderer.setStyle(this.boxPosition.shape, 'right', `${right}px`);
        this.renderer.removeStyle(this.boxPosition.shape, 'top');
        this.renderer.removeStyle(this.boxPosition.shape, 'left');
        this.isUpResizingOn = true
      }

      let width = this.boxPosition.right - this.mouse.x;
      let height = this.boxPosition.bottom - this.mouse.y;
      this.renderer.setStyle(this.boxPosition.shape, 'height', `${height}px`);
      this.renderer.setStyle(this.boxPosition.shape, 'width', `${width}px`);

    }
  }

  private resizeCondMeet() {
    return (this.mouse.x < this.containerPos.right && this.mouse.y < this.containerPos.bottom);
  }
  private resizeUpCondMeet() {

    return (this.mouse.x > this.containerPos.left &&
      this.mouse.y + this.containerPos.top + ((this.containerPos.height * this.activeComment.page) - this.containerPos.height) > (this.containerPos.page.offsetTop - this.containerPos.top));
  }


  private move() {
    let left = +this.boxPosition.shape.getAttribute('x1');
    let top = +this.boxPosition.shape.getAttribute('y1');
    let left2 = +this.boxPosition.shape.getAttribute('x2')
    let top2 = +this.boxPosition.shape.getAttribute('y2')
    let diffx = left - left2;
    let diffy = top - top2;
    let dx = this.mouseClick.left + (this.mouse.x - this.mouseClick.x)
    let dy = this.mouseClick.top + (this.mouse.y - this.mouseClick.y)
    if(dx<0) dx = 0 
    if(dy<0) dy = 0
    console.log('dx', dx);
    console.log('dy', dy);
    console.log('this.boxPosition.width', this.boxPosition.width);
    console.log('this.boxPosition.height', this.boxPosition.height);
    if(this.boxPosition.shape.nodeName == 'ellipse'){
      
      if(dx<parseFloat(this.boxPosition.shape.getAttribute('rx'))) dx = parseFloat(this.boxPosition.shape.getAttribute('rx'))
      if(dy<parseFloat(this.boxPosition.shape.getAttribute('ry'))) dy = parseFloat(this.boxPosition.shape.getAttribute('ry'))
      if ((dx+parseFloat(this.boxPosition.shape.getAttribute('rx'))) > (this.viewBox.width))dx = (this.viewBox.width-parseFloat(this.boxPosition.shape.getAttribute('rx')));
      
      if ((dy+parseFloat(this.boxPosition.shape.getAttribute('ry'))) > (this.viewBox.height)) dy = (this.viewBox.height-parseFloat(this.boxPosition.shape.getAttribute('ry')));
    } else
    if(this.boxPosition.shape.nodeName == 'line'){
      if(diffx>0){
        if(dx>this.viewBox.width) dx = this.viewBox.width
        if ((dx-diffx) <= 0)  dx = diffx
      }
      if(diffy>0){
        if(dy>this.viewBox.height) dy = this.viewBox.height
        if ((dy-diffy) <= 0)  dy = diffy
        if ((dx-diffx) > (this.viewBox.width))  dx = (this.viewBox.width+diffx)
      } else {
        if ((dx-diffx) > (this.viewBox.width))  dx = (this.viewBox.width+diffx);
        if ((dy-diffy) > (this.viewBox.height)) dy = (this.viewBox.height+diffy);
      }
    } 
    else {

      if ((dx+this.boxPosition.width) > (this.viewBox.width))  dx = (this.viewBox.width-this.boxPosition.width);
      
      if ((dy+this.boxPosition.height) > (this.viewBox.height)) dy = (this.viewBox.height-this.boxPosition.height);
      
    }




    // console.log('dx======',dx);
    // console.log('dy======',dy);
    

  

    // this.boxPosition.shape.setAttributeNS(null, 'transform',`translate(${dx.toString()},${dy.toString()})`);
    this.boxPosition.shape.setAttributeNS(null, 'x', dx.toString());
    this.boxPosition.shape.setAttributeNS(null, 'y', dy.toString());
    this.boxPosition.shape.setAttributeNS(null, 'cx', dx.toString());
    this.boxPosition.shape.setAttributeNS(null, 'cy', dy.toString());
    this.boxPosition.shape.setAttributeNS(null, 'x1', dx.toString());
    this.boxPosition.shape.setAttributeNS(null, 'y1', dy.toString());
    this.boxPosition.shape.setAttributeNS(null, 'x2', (dx-diffx).toString());
    this.boxPosition.shape.setAttributeNS(null, 'y2', (dy-diffy).toString());



    // } 
  }

  private moveCondMeet() {  
    // if(!this.boxPosition.shape.classList.contains('active-annot'))
    // {
    //     return false;
    // }
    const offsetLeft = this.mouseClick.x - this.boxPosition.left;
    const offsetRight = this.boxPosition.width - offsetLeft;
    const offsetTop = this.mouseClick.y - this.boxPosition.top;
    const offsetBottom = this.boxPosition.height - offsetTop;
    return (
      this.mouse.x > this.containerPos.left + offsetLeft &&
      this.mouse.x < this.containerPos.right - offsetRight &&
      this.mouse.y > this.containerPos.top + offsetTop &&
      this.mouse.y < this.containerPos.bottom - offsetBottom
    );
  }

  public checkForPermitions(){
    this.documentPermissions('share');
    this.documentPermissions('rename');
    this.documentPermissions('download');
    this.documentPermissions('downloadWithAnnotation');
    this.documentPermissions('duplicate');
    this.documentPermissions('delete');
    this.documentPermissions('checkVal');
  }

  public documentPermissions(type)
  {
    // hiding the button if not a single option is available for user
    if(type == 'checkVal') {
      if(this.optionCheckValue){
        this.optionMainCheck = true;
      } else {
        this.optionMainCheck = false;
      }
    }

    if(this.documentDetail.is_workspace )
    {
      this.optionCheckValue = true; 
      return true;
    }
    else if(this.huddle_type == '3' && (type == 'share' || type == 'duplicate'))
    {
      return false;
    }
    else if(this.huddle_type == '3' && this.huddle_role == '210' && (type == 'rename' || type == 'delete') && this.is_submitted == 1)
    {
      return false;
    }
    else if(this.huddle_type == '3' && type == 'rename' && (this.documentDetail.created_by == this.user_id || (this.documentDetail.assessment_sample == 1 && this.huddle_role == '200')))
    {
      this.optionCheckValue = true;
      return true;
    }
    else if(this.huddle_type == '3' && type == 'rename' && this.documentDetail.created_by != this.user_id)
    {
      return false;
    }  
    else if(this.huddle_type == '3' && (type == 'download' || type == 'downloadWithAnnotation'))
    {
      this.optionCheckValue = true;
      return true;
    }
    else if(this.huddle_type == '3' && this.huddle_role == '200' && (type == 'delete' || type == 'download' || type == 'downloadWithAnnotation'))
    {
      this.optionCheckValue = true;
      return true;
    }
    else if(this.documentDetail.created_by == this.user_id && this.huddle_type != '2')
    {
      this.optionCheckValue = true;
      return true;
    }
    else if(this.huddle_type == '1' && this.huddle_role == '200')
    {
      this.optionCheckValue = true;
      return true;
    }
    else if(this.huddle_type == '2' && this.huddle_role == '200')
    {
      this.optionCheckValue = true;
      return true;
    }
    else if(this.coachee_permission == '1' && this.huddle_type == '2' && this.documentDetail.created_by == this.user_id )
    {
      this.optionCheckValue = true;
      return true;
    }
    else
    {
        return false;
    }

  }

  public openFileInfoModal() {
    if (this.docApiStatus) {
    this.bsModalRef = this.bsModalService.show(this.fileInfoTemplate, { class: "modal-md maxcls", backdrop: 'static' })
    }
  }

/*  public searchTextChanged(ev) {
    this.ifNoSearchResult = true;
    if (ev) {
      this.commentsList.forEach((c) => {
        c.valid = false;
        if (c.ref_type != 6){
          let isValidTxt = c.comment && c.comment.toLowerCase().indexOf(ev.toLowerCase()) > -1;
          c.valid = isValidTxt;
          console.log('c.valid', c.valid);
          if (!c.valid) {
          if (c.Comment && c.Comment.responses) {
            c.Comment.responses.forEach((reply) => {
              if (reply.comment && reply.comment.toLowerCase().indexOf(ev.toLowerCase()) > -1) c.valid = true;
            });
            }
          }
        }
        if (c.valid) {
          this.ifNoSearchResult = false;
        }
      });
    } else {
      this.commentsList.forEach((c) => {
        c.valid = true;
      });
      this.ifNoSearchResult = false
    }
  }*/
    
    public updateForeignComment(commentText)
    {
        //console.trace()
       //console.log('foreign subscript', commentText)
        if(commentText.from != 'pdf-renderer' && this.activeComment && this.activeComment.type == 'foreignObject-comment' && commentText.id == this.activeComment.id)
        {
            //console.log('foreign matched')
            let element = this.getShapeFromCommentId(this.activeComment.id, this.activeComment.type)
            if(element && element.firstChild && element.firstChild.innerHTML.replace(/ /g, '&nbsp;') != commentText.commentText)
            {
                //console.log('foreign element found')
                element.firstChild.innerHTML = commentText.commentText;
                this.updateArtifactData(this.activeComment);
            }
        }
    }
    public getSelectFrameworkPermission() {
        if (this.pageType === 'workspace-video-page') {
            if (!this.currentFrameworkId) {
                return true;
            }
        } else {
            
            if (this.permissions.allow_per_video_to_coachee == "1" && (this.huddle_type == 2 || this.huddle_type == 1) && !this.currentFrameworkId) {
                return true;
            }
            
            if (this.permissions.allow_per_video_to_coachee != "1" && (this.huddle_type == 2 || this.huddle_type == 1) && !this.currentFrameworkId) {
                if (this.permissions.huddle_role == 200) return true;
            }
            
            if (this.huddle_type == 3 && this.currentFrameworkId) {
                if (this.permissions.huddle_role == 200) return true;
            }
        }
        
    }
    public OnSearchChange(event) {
        this.searchInput.next(event);
    }
    
    private SubscribeSearch() {
        
        this.searchInput
        .pipe(debounceTime(1000), distinctUntilChanged())
        .subscribe(value => {
            this.getDocumentComments();
        });
        
        
        
    }
}




