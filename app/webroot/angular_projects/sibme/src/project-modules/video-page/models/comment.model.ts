export class CommentModel {
    constructor() { }
    public id: any;
    public fakeComment = true;
    public type: CommentType;
    public page: number;
    public icon: string;
    public user: any;
    public time: number;
    public text: string;
    public xPos: number;
    public yPos: number;
    public tags: any[];
    public standards: any[];
    public replies: ReplyModel[] = [];
    public marker: any;

    static addComment(type: CommentType, page: number, icon: string, xPos: number, yPos: number, user: any = {}, video_id : any): CommentModel {
        const date = new Date();
        return {id: `${date.getTime()}-${video_id}`, type, page, icon, user, time: date.getTime(), text: '', xPos, yPos, tags: [], standards: [], replies: [], marker: {},fakeComment:true};
    }
}

export class ReplyModel {
    constructor() { }
    public id: number;
    public type: CommentType;
    public page: number;
    public icon: string;
    public user: any;
    public time: number;
    public text: string;
    public xPos: number;
    public yPos: number;
    public tags: any[];
    public standards: any[];
    public marker: any;

    static addReply(type: CommentType, page: number, icon: string, xPos: number, yPos: number, user: any = {}): ReplyModel {
        const date = new Date();
        return {id: date.getTime(), type, page, icon, user, time: date.getTime(), text: '', xPos, yPos, tags: [], standards: [], marker: {}};
    }
}

export type CommentType = 'text-comment' | 'audio-comment' | 'rect-comment';