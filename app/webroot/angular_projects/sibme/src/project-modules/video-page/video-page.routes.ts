import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  RootComponent, VoBodyComponent, MainComponent, SyncNotesComponent,
  ScreenRecoderComponent, CanvasMainComponent, LiveStreamComponent, PdfRendererPOCComponent, PdfRendererComponent
} from './components';
import { CanDeactivateGuard, ScreenRecordingGuard, CheckViewerGuard, PublishCommentsGuard, CanSaveVideoGuard } from "./helpers";

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: '', component: RootComponent, children: [
      { path: 'home/:huddle_id/:video_id', component: MainComponent, canDeactivate: [PublishCommentsGuard] },
      { path: 'home/canvas/:huddle_id/:video_id', component: CanvasMainComponent },
      { path: 'home/:huddle_id/:video_id/:init_crop', component: MainComponent },
      { path: 'video_observation/:huddle_id/:video_id', component: SyncNotesComponent },
      { path: 'live-streaming/:huddle_id', component: LiveStreamComponent, canDeactivate: [CanSaveVideoGuard] },
      { path: 'live-streaming/:huddle_id/:video_id', component: LiveStreamComponent, canDeactivate: [CanSaveVideoGuard] },
      { path: 'scripted_observations/:huddle_id/:video_id', component: VoBodyComponent, canDeactivate: [CanDeactivateGuard] },
      { path: 'record-screen', component: ScreenRecoderComponent, canActivate: [CheckViewerGuard], canDeactivate: [ScreenRecordingGuard] },
      { path: 'pdf-renderer-poc', component: PdfRendererPOCComponent },
      { path: 'pdf-renderer/:page_type/:id/:filestack_handle/:file_type/:account_folder_id/:parent_folder_id/:viewer_mode', component: PdfRendererComponent }
    ]
  },

  { path: '**', redirectTo: 'page_not_found', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VideoPageRoutes { }