import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { ScreenRecoderComponent } from '@videoPage/components';

@Injectable()
export class ScreenRecordingGuard implements CanDeactivate<ScreenRecoderComponent> {
    constructor() { }
    canDeactivate(component: ScreenRecoderComponent): Observable<boolean> | boolean {
        
        if (component.windowRef['screenRecordingState'] === 'initialized') {
            component.headerService.updateScreenRecordingStateSource('aborted');
            setTimeout(() => component.cancelRecording(), 500);
        }
        return true;
    }
}