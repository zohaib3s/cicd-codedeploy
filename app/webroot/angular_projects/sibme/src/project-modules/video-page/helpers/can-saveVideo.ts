import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { LiveStreamComponent } from '@videoPage/components';

@Injectable()
export class CanSaveVideoGuard implements CanDeactivate<LiveStreamComponent> {

    canDeactivate(component?: LiveStreamComponent): Observable<boolean> | boolean {
        // console.log('in can deactivate: ', component['windowRef']['liveStreamState'])
        if (component['windowRef'] && (component['windowRef']['liveStreamState'] === 'started' || component['windowRef']['liveStreamState'] === 'stopped-screen-sharing')) {
            if (confirm('Live streaming is on, Your changes will be lost')) {
                component.no_save_video();
                return false;
            }
            
        } else return true;
    }
}