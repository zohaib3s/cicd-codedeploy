import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { MainComponent } from '../components/main/main.component';
import { Observable } from 'rxjs';

@Injectable()
export class PublishCommentsGuard implements CanDeactivate<MainComponent> {

    canDeactivate(component?: MainComponent): Observable<boolean> | boolean {
      if ((component.enablePublishBtn && !component.publishAll)) {
        if (confirm("You're about to leave without publishing comments. Are you sure you want to leave this page?")) {
          return true;
        } else {
          return false;
        }
      }
      return true;
    }
  }