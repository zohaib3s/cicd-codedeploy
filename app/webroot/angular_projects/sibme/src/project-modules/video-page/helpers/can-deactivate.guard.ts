import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { VoBodyComponent } from '@videoPage/components';
import { HeaderService } from '@src/project-modules/app/services';

@Injectable()
export class CanDeactivateGuard implements CanDeactivate<VoBodyComponent> {
    public translation: any = {};
    constructor(private headerService: HeaderService) { }
    canDeactivate(component?: VoBodyComponent): Observable<boolean> | boolean {
        this.headerService.languageTranslation$.subscribe(languageTranslation => {
            this.translation = languageTranslation;
          });
        if (component.options.timerStarted && !component.options.script_published) {
            if (confirm(this.translation.goals_changes_may_not_be_saved)) {
                return true;
            } else {
                return false;
            }
        } else return true;
    }
}