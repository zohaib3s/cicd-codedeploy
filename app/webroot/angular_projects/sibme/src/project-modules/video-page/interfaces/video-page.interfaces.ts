/**
 * BreadCrumbInterface
 * @key {title} Title:  To display on screen
 * @key {link} Link:  If the provided title is of link/route type then provide its link 
 */

export interface CommentTypingSettingsInterface {
    PauseWhileTyping: boolean,
    EnterToPost: boolean,
    autoscroll:boolean,
    sortBy: number
}

export type RecordingState = 'comfortZone' | 'recording' | 'resume' | 'uploading' | 'play';
export type VideoCommentPlayState = 'on' | 'off';
export type AudioPath = { filePath: string, audioUrl: string, autoSubmitComment: string, audioDuration: number };


type CommentOrAttachmentCountType = 'comment_count' | 'resources_count';
export interface CommentOrAttachmentCountInterface {
    key: CommentOrAttachmentCountType,
    value: number
}

export interface SelectedRubricInterface {
    rubric: any,
    selected: boolean
}

export interface AddNewCommentInterface {
    tryAgain: boolean
    comment: any,
}

export interface CommentStateInterface {
    id: number
    state: string,
}