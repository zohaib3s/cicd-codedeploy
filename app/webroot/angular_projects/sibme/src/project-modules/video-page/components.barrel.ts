import { CanvasMainComponent } from './components/canvas-video-page-main/canvas-main.component';
import { CommonModule } from '@angular/common';
import { MomentModule } from 'ngx-moment';
import { NouisliderModule } from 'ng2-nouislider';
import { UiSwitchModule } from 'ngx-toggle-switch';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';


import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ModalModule } from "ngx-bootstrap/modal";
import { TabsModule } from "ngx-bootstrap/tabs";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ScrollToFixed } from './directives';
import {AutosizeModule} from '@techiediaries/ngx-textarea-autosize';

import {
  PlayerService, CropPlayerService, MainService, ObservationService, ScrollService, VideoPageService,FilestackService
} from "./services";

import {
  RootComponent, BreadcrumbComponent, PageLoaderAnimationComponent, VideoPlayerComponent, NotFoundComponent, CommentComponent,
  SearchFormComponent, CustomTagsComponent, RubricsComponent, FsUploaderComponent, CoachingSummaryComponent, VoBodyComponent, AddCommentComponent,
  CropVideoPlayerComponent, ScriptedObservationComponent, ScriptedNoteAddCommentComponent, VideoAddCommentComponent, MainComponent,
  SeperateVideoPlayerComponent, CommentListComponent, VideoPageAttachmentsComponent, SyncNotesComponent, TranscriptionComponent, ScreenRecoderComponent, LiveStreamComponent,
  PdfRendererPOCComponent, PdfRendererComponent
} from "./components";

import { PlUniqueDescriptionModelComponent } from "./modals";

import { CommentPlayOffSVG, CommentPlayOnSVG, InfoRedSVG } from "./svg-components";

import { CanDeactivateGuard } from "./helpers";

import { CanSaveVideoGuard } from './helpers/can-saveVideo';

import { SlimScroll } from './directives';
import { QuillModule } from 'ngx-quill';

import { CustomsearchPipe, DoubleDigitPipe, SibmeTimeFormatPipe } from './pipes';
import { PublishCommentsGuard } from './helpers/publish-comment.guard';
import { ScreenRecordingGuard } from './helpers/screen-recording.guard';
import { CheckViewerGuard } from './helpers/check-viewer.guard';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { PdfRendererViewerComponent } from './components/pdf-renderer-viewer/pdf-renderer-viewer.component';
import { PdfAddCommentComponent } from './components/pdf-add-comment/pdf-add-comment.component';
import { PdfCommentListComponent } from './components/pdf-comment-list/pdf-comment-list.component';
import { PdfCommentComponent } from './components/pdf-comment/pdf-comment.component';

export const __IMPORTS = [
  CommonModule,
  TooltipModule.forRoot(),
  BsDropdownModule.forRoot(),
  ButtonsModule.forRoot(),
  BsDatepickerModule.forRoot(),
  TabsModule.forRoot(),
  LoadingBarHttpClientModule,
  MomentModule,
  UiSwitchModule,
  ModalModule.forRoot(),
  AmChartsModule,
  NouisliderModule,
  DeviceDetectorModule.forRoot(),
  AutosizeModule,
  QuillModule.forRoot()
];

export const __DECLARATIONS = [
  RootComponent,
  BreadcrumbComponent,
  CanvasMainComponent,
  PageLoaderAnimationComponent,
  VideoPlayerComponent,
  NotFoundComponent,
  CommentComponent,
  SearchFormComponent,
  SlimScroll,
  CustomTagsComponent,
  RubricsComponent,
  FsUploaderComponent,
  CoachingSummaryComponent,
  VoBodyComponent,
  AddCommentComponent,
  PdfAddCommentComponent,
  PdfCommentListComponent,
  PdfCommentComponent,
  CropVideoPlayerComponent,
  CustomsearchPipe,
  DoubleDigitPipe,
  ScriptedObservationComponent,
  CommentPlayOffSVG,
  CommentPlayOnSVG,
  InfoRedSVG,
  VideoAddCommentComponent,
  MainComponent,
  SeperateVideoPlayerComponent,
  CommentListComponent,
  VideoPageAttachmentsComponent,
  SyncNotesComponent,
  ScriptedNoteAddCommentComponent,
  TranscriptionComponent,
  ScreenRecoderComponent,
  ScrollToFixed,
  SibmeTimeFormatPipe,
  LiveStreamComponent,
  SafeHtmlPipe,
  PlUniqueDescriptionModelComponent,
  PdfRendererPOCComponent,
  PdfRendererComponent,
  PdfRendererViewerComponent
];

export const __PROVIDERS = [
  PlayerService,
  CropPlayerService,
  MainService,
  ScrollService,
  ObservationService,
  CanDeactivateGuard,
  VideoPageService,
  PublishCommentsGuard,
  CanSaveVideoGuard,
  FilestackService,
  ScreenRecordingGuard,
  CheckViewerGuard
];

export const __ENTRY_COMPONENTS = [
  PlUniqueDescriptionModelComponent
];
