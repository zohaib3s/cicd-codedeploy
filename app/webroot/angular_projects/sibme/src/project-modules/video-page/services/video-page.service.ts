import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

import {
    CommentTypingSettingsInterface, CommentOrAttachmentCountInterface, SelectedRubricInterface,
    AddNewCommentInterface, CommentStateInterface
} from "@videoPage/interfaces";

@Injectable()
export class VideoPageService {
    public eventEmitter:Subject<any> = new Subject();
    private cinemaModeSource = new Subject<boolean>();
    public cinemaMode$ = this.cinemaModeSource.asObservable();

    private commentTypingSettings = { PauseWhileTyping: true, EnterToPost: true, autoscroll: false, sortBy: 0 };
    private commentTypingSettingsSource = new BehaviorSubject<CommentTypingSettingsInterface>(this.commentTypingSettings);
    public commentTypingSettings$ = this.commentTypingSettingsSource.asObservable();

    private currentTabSource = new Subject<number>();
    public currentTab$ = this.currentTabSource.asObservable();

    
    private currentTabSourceClose = new Subject<number>();
    public currentTabOnCross$ = this.currentTabSourceClose.asObservable();

    public postingStatus:Subject<any> = new Subject();
 

    private videoCurrentTimeSource = new BehaviorSubject<number>(0);
    public videoCurrentTime$ = this.videoCurrentTimeSource.asObservable();

    private activeUsersSource = new BehaviorSubject<any>([]);
    public activeUsers$ = this.activeUsersSource.asObservable();

    private totalStatsSource = new BehaviorSubject<CommentOrAttachmentCountInterface[]>([{ key: 'comment_count', value: 0 }, { key: 'resources_count', value: 0 }]);
    public totalStats$ = this.totalStatsSource.asObservable();

    private videoDurationSource = new Subject<number>();
    public videoDuration$ = this.videoDurationSource.asObservable();

    private newCommentSource = new Subject<AddNewCommentInterface>();
    public newComment$ = this.newCommentSource.asObservable();

    private editCommentSource = new Subject<any>();
    public editComment$ = this.editCommentSource.asObservable();

    private editCommentErrorSource = new Subject<any>();
    public editCommentError$ = this.editCommentErrorSource.asObservable();

    private commentStateSource = new Subject<CommentStateInterface>();
    public commentState$ = this.commentStateSource.asObservable();

    private selectedRubricSource = new Subject<SelectedRubricInterface>();
    public selectedRubric$ = this.selectedRubricSource.asObservable();

    private commentExistedInLocalStorageSource = new Subject<boolean>();
    public commentExistedInLocalStorage$ = this.commentExistedInLocalStorageSource.asObservable();

    private staticFilesSource = new BehaviorSubject<any[]>([]);
    public staticFiles$ = this.staticFilesSource.asObservable();

    private enablePublishBtn = new Subject<boolean>();
    enablePublish$ = this.enablePublishBtn.asObservable();

    private publishComment = new Subject<any>();
    publishComment$ = this.publishComment.asObservable();

    updateEnablePublishBtn(enablePublish: boolean) {
        this.enablePublishBtn.next(enablePublish);
    }

    setCinemaMode(isCinema: boolean)
    {
        this.cinemaModeSource.next(isCinema);
    }

    constructor() { }

    updateCommentTypingSettings(data: any) { // TODO: implement interface
        const currentValue = this.commentTypingSettingsSource.getValue();
        currentValue[data.key] = data.value;
        this.commentTypingSettingsSource.next(currentValue);
    }
    public updateCommentStatus = (data:any) =>{
        this.postingStatus.next(data)
    }

    updateCurrentTab(currentTab: number) {
        this.currentTabSource.next(currentTab);
    }

    updateCurrentTabOnClose(currentTab: number) {
        this.currentTabSourceClose.next(currentTab);
    }

    updateVideoCurrentTime(currentTime: number) {
        this.videoCurrentTimeSource.next(currentTime);
    }

    updateActiveUsers(users: any) {
        this.activeUsersSource.next(users);
    }

    updateTotalStats(totalStats: CommentOrAttachmentCountInterface[]) {
        this.totalStatsSource.next(totalStats);
    }

    updateVideoDuration(videoDuration: number) {
        this.videoDurationSource.next(videoDuration);
    }

    addNewComment(data: AddNewCommentInterface) {
        this.newCommentSource.next(data);
    }

    changeEditCommentSource(editComment: any) {
        this.editCommentSource.next(editComment);
    }

    updateCommentState(data: CommentStateInterface) {
        this.commentStateSource.next(data);
    }

    updateEditCommentErrorSource(editComment: any) {
        this.editCommentErrorSource.next(editComment);
    }

    updateSelectedRubric(selectedRubric: SelectedRubricInterface) {
        this.selectedRubricSource.next(selectedRubric);
    }

    updateCommentExistedInLocalStorage(commentExistedInLocalStorage: boolean) {
        this.commentExistedInLocalStorageSource.next(commentExistedInLocalStorage);
    }

    updateStaticFiles(staticFiles: any[]) {
        this.staticFilesSource.next(staticFiles);
    }

    updatepublishComment(data: any) {
        this.publishComment.next(data);
    }

}
