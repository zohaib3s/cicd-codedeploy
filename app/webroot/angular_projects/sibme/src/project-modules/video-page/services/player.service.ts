import { Injectable, EventEmitter } from '@angular/core';
import { Subject,BehaviorSubject } from 'rxjs';

@Injectable()
export class PlayerService {

  public PlayerPlayingState: EventEmitter<any> = new EventEmitter<any>();
  public Seek: EventEmitter<any> = new EventEmitter<any>();

  private videoCommentPlaySource = new Subject<any>();
  public videoCommentPlay$ = this.videoCommentPlaySource.asObservable();
  public _videoPlayerCinemaMode = new Subject<any>();
  public clicked: boolean = false;


  constructor() { 
  }

  public ModifyPlayingState(state) {
    this.PlayerPlayingState.emit(state);
  }

  public SeekTo(comment) {
    this.clicked = true;  //If user click on comment(prevent scroll check)
    this.Seek.emit(comment);
  }

  public toggleVideoCommentPlay(data: any) {
    this.videoCommentPlaySource.next(data);
  }

}
