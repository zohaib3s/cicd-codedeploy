import { Injectable, EventEmitter } from '@angular/core';
import * as filestack from 'filestack-js';
import { environment } from "@environments/environment";
import { HeaderService } from "@projectModules/app/services";
import { BehaviorSubject } from 'rxjs';

import { GLOBAL_CONSTANTS } from '@src/constants/constant';
@Injectable()
export class FilestackService { 

	private client;
	private options = {
		location: "s3",
		path: '/tempupload',
		access: 'public',
		container: environment.container,
		region: 'us-east-1',
		maxSize: GLOBAL_CONSTANTS.FILESTACK_MAXSIZE,
		
	}

	public FilesUploaded:EventEmitter<any> = new EventEmitter<any>();
	private progress = new BehaviorSubject([]);
	streamUploadProgress = this.progress.asObservable();

	private fileProgress = new BehaviorSubject({});
	streamUploadFileProgress = this.fileProgress.asObservable();

  constructor(private headerService:HeaderService) {
  	

  }
  public uploadToS3(audioBlob: Blob, audioExtension: string, accountId: number, userId: number) {
	this.InitFileStack();
	const date = new Date();
	this.options.path = '/tempupload'; // Think and find a way that why we reset it to /uploads
	this.options.path = this.makeUploadPath(accountId, userId, date);
	delete this.options.maxSize;
	return this.client.upload(audioBlob, {onProgress :this.uploadProgress}, { ...this.options, filename: `ScreenRecording_${accountId}_${userId}_${date.getTime()}.${audioExtension}` });
}

public uploadProgress = (evt) => {
	this.progress.next(evt.totalPercent);
};

  public uploadFiles(audioBlob: Blob, audioExtension: string, accountId: number, userId: number, filename, key,token) {
	this.InitFileStack();
	const date = new Date();
	this.options.path = '/tempupload'; // Think and find a way that why we reset it to /uploads
	this.options.path = this.makeUploadPath(accountId, userId, date);
	//this.options.onUploadDone: (res) => console.log('albert',res)
	delete this.options.maxSize;
	return this.client.upload(audioBlob, {onProgress :evt => this.uploadFileProgress(evt.totalPercent, key)}, { ...this.options, filename: filename },token);
}


public uploadFilesForMultiple(audioBlob: Blob, audioExtension: string, accountId: number, userId: number, filename, key,token,progressCallback) {
	this.InitFileStack();
	const date = new Date();
	this.options.path = '/tempupload'; // Think and find a way that why we reset it to /uploads
	this.options.path = this.makeUploadPath(accountId, userId, date);
	//this.options.onUploadDone: (res) => console.log('albert',res)
	delete this.options.maxSize;
	return this.client.upload(audioBlob, {onProgress :evt => progressCallback(evt, key)}, { ...this.options, filename: filename },token);
}

	public uploadFileProgress(totalPercent,key){
		let obj = {
			totalPercent,
			key
		}
		this.fileProgress.next(obj);
	};

private makeUploadPath(accountId: number, userId: number, date: Date) {
	return `${this.options.path}/${accountId}/screen_sharing/${date.getFullYear()}/${this.getTwoDigitNumber(date.getMonth() + 1)}/${this.getTwoDigitNumber(date.getDate())}/`;
}
private getTwoDigitNumber(number: number) {
	if (number < 10) return `0${number}`;
	else return number;
}
  public InitFileStack(){

		let key = environment.fileStackAPIKey;
	 	this.client = filestack.init(key);
  }

  public showPicker(options){
	 
  	
  	options = options || {};
  	options.storeTo = {
                location: "s3",
                path: this.getUploadPath(),
                access: 'public',
                container: environment.container,
                region: 'us-east-1'
            }
    options.fromSources = ['local_file_system','dropbox','googledrive','box','onedrive']
  	options.onUploadDone =(res)=> {this.FilesUploaded.emit(res.filesUploaded)};
      let sessionData:any = this.headerService.getStaticHeaderData();
      let language = sessionData.language_translation.current_lang;
      options.lang =  language;
  	return this.client.picker(options).open();

  }

  private getUploadPath(){
	let temp = "/tempupload/";

  	let sessionData:any = this.headerService.getStaticHeaderData();

  	let account_id = sessionData.user_current_account.accounts.account_id;

  	temp = temp+account_id;

  	let date = new Date(); 

  	temp+="/"+date.getFullYear()+"/"+this.padNumber(date.getMonth()+1)+"/"+this.padNumber(date.getDate())+"/";

  	return temp;

  }

  private padNumber(n){

  	let number = Number(n);

  	if(number <=9){
  		return "0"+""+number;
  	}else{
  		return n;
  	}

  }
}
