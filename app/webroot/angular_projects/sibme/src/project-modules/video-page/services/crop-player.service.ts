import {EventEmitter, Injectable} from '@angular/core';

@Injectable()
export class CropPlayerService {

    public PlayerPlayingState:EventEmitter<any> = new EventEmitter<any>();
    public Seek:EventEmitter<any> = new EventEmitter<any>();
    public SeekEndTime:EventEmitter<any> = new EventEmitter<any>(); //new

    constructor() { }


    public ModifyPlayingState(state){

        this.PlayerPlayingState.emit(state);

    }

    public SeekTo(time , play=1){

        this.Seek.emit({time:time,play:play});

    }

    public seekEnd(time , play=1){

        this.SeekEndTime.emit({time:time,play:play});

    }
    

}
