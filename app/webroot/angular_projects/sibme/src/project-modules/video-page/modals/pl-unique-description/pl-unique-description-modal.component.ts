import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';

import { HeaderService } from "@app/services";

@Component({
  selector: 'video-page-pl-unique-description-modal',
  templateUrl: './pl-unique-description-modal.component.html',
  styleUrls: ['./pl-unique-description-modal.component.css']
})
export class PlUniqueDescriptionModelComponent implements OnInit, OnDestroy {

  public plUniqueDescriptions: any = [];
  public plUniqTitleHeight: number = 0;
  public maxDescCharacters: number = 180;

  public translation: any = {};
  private subscriptions: Subscription = new Subscription();

  constructor(public headerService: HeaderService, private modalService: BsModalService, public bsModalRef: BsModalRef) {
    this.subscriptions = this.headerService.languageTranslation$.subscribe(languageTranslation => this.translation = languageTranslation);
  }

  ngOnInit() {
    this.subscriptions.add(this.modalService.onShown.subscribe(() => {
      setTimeout(() => {
        const titleDomArr = document.getElementsByClassName('pl-unique-item-title');
        if (titleDomArr && titleDomArr.length > 0) {
          Array.prototype.forEach.call(titleDomArr, (titleDom) => {
            (titleDom.clientHeight > this.plUniqTitleHeight) ? this.plUniqTitleHeight = titleDom.clientHeight : false;
          });
        }
      }); 
    }));
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}