import { TrialGuard } from './guards/trial.guard';
import { AccountSettingsRoutingModule } from './account-settings-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppLevelSharedModule } from '@shared/shared.module';
import { FormsModule } from '@angular/forms';
import { MainComponent } from './components/main/main.component'
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ColorPickerModule } from 'primeng/colorpicker';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { DeviceDetectorModule } from 'ngx-device-detector';

import {
  CancelAccountComponent,
  GeneralSettingsComponent,
  ColorsLogoComponent,
  GlobalExportComponent,
  ArchivingComponent
} from '@projectModules/account-settings';
import { PlansGuard } from '../shared/guards/plans.guard';
import { ArchivingGuard } from '../shared/guards/archiving.guard';
import { GlobalExportGuard } from '../shared/guards/global-export.guard';
import { GlobalExportPopup } from './guards/global-export-popup.guard';
import { ColorsLogoPopup } from './guards/colors-n-logo-popup.guard';
import { ConfirmDeactivateGuard } from './guards/ConfirmDeactivateGuard';
import { GoalsSettings } from './components/goals-settings/goals-settings.component';
import { CustomFieldsComponent } from './components/custom-fields/custom-fields.component';
import { customFieldGuard } from './guards/customFieldGuard.guard';

@NgModule({
  declarations: [
    MainComponent,
    CancelAccountComponent,
    GeneralSettingsComponent,
    ColorsLogoComponent,
    GlobalExportComponent,
    ArchivingComponent,
    GoalsSettings,
    CustomFieldsComponent
  ],
  imports: [
    CommonModule,
    Ng2SearchPipeModule,
    AccountSettingsRoutingModule,
    LoadingBarHttpClientModule,
    ColorPickerModule,
    BsDatepickerModule.forRoot(),
    DeviceDetectorModule,
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule,
    AppLevelSharedModule,
    BsDropdownModule.forRoot(),
  ],
  providers: [PlansGuard, TrialGuard, ArchivingGuard, GlobalExportGuard, GlobalExportPopup, ColorsLogoPopup ,ConfirmDeactivateGuard, customFieldGuard]
})
export class AccountSettingsModule {
}
