export class plansModel {
    first_name: String;
    last_name: String;
    email: String;
    address: String;
    extendedAddress: String;
    state: String;
    city: String;
    zip_code: String;
    account_id: String;
    company_name: String;
    description: String;
    card_number: String;
    card_expiration: String;
    card_cvv: String;
    country: String;
    user_id: String;
    plan_id: String;
    plan_price: String;
    plan_quantity: String;
    old_card: Boolean;
    auth_token:string;
}