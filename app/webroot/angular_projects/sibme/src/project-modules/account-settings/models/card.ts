export class Card {
    first_name: String;
    last_name: String;
    email: String;
    address: String;
    extendedAddress: String;
    state: String;
    city: String;
    zip_code: String;
    account_id: String;
    company_name: String;
    card_number: String;
    card_expiration: String;
    card_cvv: String;
    country: String;
}