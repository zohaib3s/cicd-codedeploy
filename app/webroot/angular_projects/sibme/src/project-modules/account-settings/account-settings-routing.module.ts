import { PublicAuthGuard } from './../shared/guards/public-auth.guard';
import { MainComponent } from './components/main/main.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GeneralSettingsComponent, ColorsLogoComponent, CancelAccountComponent, GlobalExportComponent, ArchivingComponent } from './index';
import { SharedPageNotFoundComponent } from '@shared/shared-page-not-found/shared-page-not-found.component';
import { GlobalExportGuard } from '../shared/guards/global-export.guard';
import { ArchivingGuard } from '../shared/guards/archiving.guard';
import { PlansGuard } from '../shared/guards/plans.guard';
import { GlobalExportPopup } from './guards/global-export-popup.guard';
import { ColorsLogoPopup } from './guards/colors-n-logo-popup.guard';
import { ConfirmDeactivateGuard } from './guards/ConfirmDeactivateGuard';
import { GoalsSettings } from './components/goals-settings/goals-settings.component';
import { CustomFieldsComponent } from './components/custom-fields/custom-fields.component';
import { customFieldGuard } from './guards/customFieldGuard.guard';

const routes: Routes = [
    { path: '', component: MainComponent, pathMatch: 'full' },
    { path: 'general-settings', component: GeneralSettingsComponent},
    { path: 'goals-settings' , component: GoalsSettings},
    { path: 'colors-logo', component: ColorsLogoComponent, canDeactivate: [ColorsLogoPopup] },
    { path: 'cancel-account',component: CancelAccountComponent },
    { path: 'global-export', component: GlobalExportComponent, canActivate: [GlobalExportGuard], canDeactivate: [GlobalExportPopup] },
    { path: 'archiving', component: ArchivingComponent , canActivate: [ArchivingGuard]},
    { path: 'plans',  loadChildren: './plans-billings/plans-billings.module#PlansBillingsModule', canActivate:[PublicAuthGuard, PlansGuard], canDeactivate:[ConfirmDeactivateGuard]},
    { path: 'custom-fields',component: CustomFieldsComponent , canDeactivate: [customFieldGuard] },
    { path: 'rubrics',  loadChildren: './rubrics/rubrics.module#RubricsModule',},
    { path: 'page_not_found', component: SharedPageNotFoundComponent  },
    { path: '**', redirectTo: 'page_not_found'}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountSettingsRoutingModule {
}
