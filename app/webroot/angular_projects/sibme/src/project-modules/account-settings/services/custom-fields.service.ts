import {HttpClient} from '@angular/common/http';
import {environment} from '@src/environments/environment';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomFieldsService {


  constructor(private http: HttpClient) {

  }

  public getCustomFieldData(account_id) {
    let obj = {account_id: account_id};
    let path = environment.APIbaseUrl + '/get_custom_fields';
    return this.http.post(path, obj);
  }

  public saveCustomFields(Obj) {
    let path = environment.APIbaseUrl + '/save_custom_fields';
    return this.http.post(path, Obj);
  }

  public deleteAssessmentFields(Obj) {
    let path = environment.APIbaseUrl + '/delete_custom_field';
    return this.http.post(path, Obj);
  }
}
