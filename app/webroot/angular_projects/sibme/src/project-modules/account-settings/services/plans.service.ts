import { Card } from './../models/card';
import { plansModel } from './../models/plans';
import { HttpClient } from '@angular/common/http';
import { environment } from '@src/environments/environment';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PlansService {


  constructor(private http: HttpClient) {}

  public fetchAccountPlan(Obj){
    let path = environment.APIbaseUrl + '/fetch_account_plan_details_angular';
    return this.http.post(path,Obj);
  }
  public purchasePlan(Plan :plansModel ){
    let path = environment.APIbaseUrl + '/purchase_new_plan';
    return this.http.post(path,Plan);
  }
  public updateCard(card: Card){
    let path = environment.APIbaseUrl + '/update_customer_info';
    return this.http.post(path,card);
  }
  public updateHeaderSession() {
    let obj = {};
    let path = environment.appPath + '/api/update_session_from_lumen';
    return this.http.post(path, obj);
  }

}
