import {HttpClient} from '@angular/common/http';
import {environment} from '@src/environments/environment';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ColorsLogoService {


  constructor(private http: HttpClient) {

  }

  public GetColorsLogoData(account_id) {
    let obj = {account_id};
    let path = environment.APIbaseUrl + '/get_colors_logos_api';
    return this.http.post(path, obj);
  }

  public updateColorLogo(obj) {
    let path = environment.APIbaseUrl + '/colors_and_logo';
    return this.http.post(path, obj);
  }

  public resetColorsLogos(account_id) {
    let obj = {account_id};
    let path = environment.APIbaseUrl + '/reset_colors_logos';
    return this.http.post(path, obj);
  }
  public updateHeaderSession() {
    let obj = {};
    let path = environment.appPath + '/api/update_session_from_lumen';
    return this.http.post(path, obj);
  }

}
