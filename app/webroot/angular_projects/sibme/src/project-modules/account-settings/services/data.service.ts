import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class DataService {

  private planId = new BehaviorSubject(0);
  private selectedUsers = new BehaviorSubject(0);
  private receipt = new BehaviorSubject([]);
  private previousPlanData = new BehaviorSubject([]);

  currentId = this.planId.asObservable();
  currentUsers = this.selectedUsers.asObservable();
  currentReceipt = this.receipt.asObservable();
  planData = this.previousPlanData.asObservable();

  constructor(){}

  changeId(id: number) {
    this.planId.next(id);
  }

  changeSelectedUsers(users: number){
    this.selectedUsers.next(users);
  }

  updateReceipt(receipt:any){
    this.receipt.next(receipt);
  }

  updatePlanData(data:any){
    this.previousPlanData.next(data);
  }
}