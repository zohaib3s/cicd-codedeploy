import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AccountSettingsService {

  constructor(private http: HttpClient) {

  }

  public AccountUserStorageStats(account_id) {
    let obj = {
      account_id: account_id
    };
    let path = environment.APIbaseUrl + '/account_users_storage_stats';
    return this.http.post(path,obj);
  }

}
