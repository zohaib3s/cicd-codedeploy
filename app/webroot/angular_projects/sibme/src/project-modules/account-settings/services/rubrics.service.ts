import { HttpClient } from '@angular/common/http';
import { environment } from '@src/environments/environment';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RubricsService {

  public performanceData = new BehaviorSubject<any>({});

  constructor(private http: HttpClient) {}

  public fetchRubrics(Obj){
    const path = environment.APIbaseUrl + '/rubrics/get_rubrics_list';
    return this.http.post(path,Obj);
  }
  public publishFramework(payload){
    const path = environment.APIbaseUrl + '/rubrics/publish_framework';
    return this.http.post(path,payload);
  }
  public unpublishFramekwork(payload){
    const path = environment.APIbaseUrl + '/rubrics/unpublish_framework';
    return this.http.post(path,payload);
  }
  public deleteFramekwork(payload){
    const path = environment.APIbaseUrl + '/rubrics/delete_framework';
    return this.http.post(path,payload);
  }
  public deletePerformance(payload){
    const path = environment.APIbaseUrl + '/rubrics/delete_all_performance_levels';
    return this.http.post(path,payload);
  }
  public performance(payload){
    const path = environment.APIbaseUrl + '/rubrics/create_performance_levels';
    return this.http.post(path,payload);
  }
  public getStandards(payload){
    const path = environment.APIbaseUrl + '/rubrics/get_framework_standards_with_pl_levels';
    return this.http.post(path,payload);
  }
  public saveUniqueDescription(payload){
    const path = environment.APIbaseUrl + '/rubrics/save_unique_description';
    return this.http.post(path,payload);
  }

  public createRubric(obj){
    const path = environment.APIbaseUrl + '/rubrics/save_frame_work_first_level';
    return this.http.post(path , obj);
  }

  public getFramworkSettings(obj){
    const path = environment.APIbaseUrl + '/rubrics/get_framework_settings';
    return this.http.post(path, obj);
  }

  public saveFramworkStandards(obj){
    let path = environment.APIbaseUrl + '/rubrics/save_rubric_standards';
    return this.http.post(path, obj);
  }

  public getGlobalPerformances(obj){
    let path = environment.APIbaseUrl + '/rubrics/get_performance_levels_for_framework';
    return this.http.post(path, obj);
  }

  public getUniquePerformances(obj){
    let path = environment.APIbaseUrl + '/rubrics/get_standards_with_unique_performance_levels';
    return this.http.post(path, obj);
  }
  
  public getFrameStandards(obj){
    let path = environment.APIbaseUrl + '/rubrics/get_framework_standards_with_pl_levels';
    return this.http.post(path , obj)
  }

  public duplicatePerformanceLevels(obj){
    let path = environment.APIbaseUrl + '/rubrics/duplicate_performance_levels';
    return this.http.post(path , obj)
  }

  public setPerfData(obj){
    this.performanceData.next(obj);
  }

  public getPerfData() {
    return this.performanceData.asObservable();
  }
}
