import {HttpClient} from '@angular/common/http';
import {environment} from '@src/environments/environment';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GeneralSettingsService {


  constructor(private http: HttpClient) {

  }

  public GetGeneralAccountSettings(account_id, user_id) {
    let obj = {account_id: account_id, user_id: user_id};
    let path = environment.APIbaseUrl + '/get_general_account_settings';
    return this.http.post(path, obj);
  }

  public updateGeneralSettings(Obj) {
    let path = environment.APIbaseUrl + '/update_general_account_settings';
    return this.http.post(path, Obj);
  }

  public changeAccountOwner(Obj) {
    let path = environment.APIbaseUrl + '/change_account_owner';
    return this.http.post(path, Obj);
  }

  public updateHeaderSession() {
    let obj = {};
    let path = environment.appPath + '/api/update_session_from_lumen';
    return this.http.post(path, obj);
  }
}
