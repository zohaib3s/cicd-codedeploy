import {HttpClient} from '@angular/common/http';
import {environment} from '@src/environments/environment';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GlobalExportService {


  constructor(private http: HttpClient) {

  }

  public updateGlobalExport(Obj) {
    let path = environment.APIbaseUrl + '/global_export';
    return this.http.post(path, Obj);
  }


}
