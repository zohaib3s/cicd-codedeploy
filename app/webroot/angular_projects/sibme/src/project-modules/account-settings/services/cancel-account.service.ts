import {HttpClient} from '@angular/common/http';
import {environment} from '@src/environments/environment';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CancelAccountService {


  constructor(private http: HttpClient) {

  }

  public cancelAccount(obj) {
    let path = environment.appPath + '/api/cancel_account_api';
    return this.http.post(path, obj);
  }

}
