import {HttpClient} from '@angular/common/http';
import {environment} from '@src/environments/environment';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GoalSettingsService {


  constructor(private http: HttpClient) {

  }

  public GetGoalSettings(obj) {
    return this.http.get(`${environment.APIbaseUrl}/goals/getGoalsSettings/${obj.account_id}`);
  }

  public updateGoalSettings(obj){
    return this.http.post(`${environment.APIbaseUrl}/goals/updateGoalsSettings` , obj);
  }

  
}
