import {HttpClient} from '@angular/common/http';
import {environment} from '@src/environments/environment';
import {Injectable} from '@angular/core';
import {BaseService} from '@src/project-modules/app/services';

@Injectable({
  providedIn: 'root'
})
export class ArchivingService {


  constructor(private http: HttpClient, private baseService: BaseService) {
  }

  public AddComment(data: object) {
  }

  public getUnarchiving(obj) {
    let path = environment.APIbaseUrl + '/get_unarchive_huddles';
    return this.http.post(path, obj);
  }

  public getArchiving(obj) {
    let path = environment.APIbaseUrl + '/get_unarchive_huddles';
    return this.http.post(path, obj);
  }

// done the archiving
  public archiveData(obj) {
    let path = environment.APIbaseUrl + '/archive';
    return this.http.post(path, obj);
  }

  public unArchiveData(obj) {
    let path = environment.APIbaseUrl + '/unarchive';
    return this.http.post(path, obj);
  }

  public archivingModalUpdate(obj) {
    let path = environment.APIbaseUrl + '/update_getting_started_archiving';
    return this.http.post(path, obj);
  }


}
