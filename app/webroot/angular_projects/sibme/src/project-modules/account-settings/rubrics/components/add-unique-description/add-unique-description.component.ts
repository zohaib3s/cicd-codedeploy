import { Unique } from './../../models/unique';
import { RubricsService } from './../../../services/rubrics.service';
import { Component, OnInit, OnDestroy, TemplateRef, HostListener, EventEmitter, ElementRef, ViewChild, AfterViewChecked } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';

@Component({
    selector: 'add-unique-description',
    templateUrl: './add-unique-description.component.html',
    styleUrls: ['./add-unique-description.component.css']
})
export class AddUniqueDescription implements OnInit, OnDestroy, AfterViewChecked  {
    public modalRef: BsModalRef;
    @ViewChild('scroll1') elementView1: ElementRef;
    public translations;
    public height1:number = 300;
    public height2:number = 0;
    public loading: boolean = true;
    public sub = new Subscription();
    public account_tag_id: number = 0;
    public standards: any;
    public standards_obj;
    public index: number = null;
    public framework_settings: any;
    public unique_description: Unique;
    public performance_levels: any;
    public selected_name: string = null;
    public selected_index_of_array: number = 0;
    public no_standards: boolean = false;
    public maxCharsDesc = 1000;
    public maxCharDescCheck: boolean = false;
    public disable: boolean = false;
    public isEdited: boolean = false;
    public payload = [];
    public checkIndexArr: any;
    public performanceData: any;
    public name: string = null;
    public is_check = true;
    opts: ISlimScrollOptions;
    scrollEvents: EventEmitter<SlimScrollEvent>;
    constructor(

        private headerService: HeaderService,
        private toastr: ShowToasterService,
        private router: Router,
        private route: ActivatedRoute,
        private rubricsService: RubricsService,
        private modalService: BsModalService,

    ) {
        this.sub.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
            this.translations = languageTranslation;
        }));
        this.sub.add(this.route.params.subscribe(params => {
            this.account_tag_id = +(params.id);
        }))
        this.sub.add(this.route.queryParams.subscribe(params => {
            if (params.is_edited) {
                this.isEdited = JSON.parse(params.is_edited);
            }
        }))
    }
    ngOnInit() {
        this.scrollEvents = new EventEmitter<SlimScrollEvent>();
        this.opts = {
            position: 'right',
            barBackground: '#C9C9C9',
            barOpacity: '0.8',
            barWidth: '7',
            barBorderRadius: '20',
            barMargin: '0',
            gridBackground: '#D9D9D9',
            gridOpacity: '1',
            gridWidth: '0',
            gridBorderRadius: '20',
            gridMargin: '0',
            alwaysVisible: true,
            visibleTimeout: 1000,
          };
        this.fetch();
        this.rubricsService.getPerfData().subscribe(info => {
            this.performanceData = info;
            if (Object.keys(this.performanceData).length == 0) {
                this.is_check = false;
                this.navigate();
            }
        })
    }
    ngAfterViewChecked () {
        this.height1 = this.elementView1.nativeElement?.offsetHeight
    }
    public loadFilterDialogUpdate(template: TemplateRef<any>) {
        this.is_check = false;
        this.modalRef = this.modalService.show(template, { class: "modal-md  maxcls", backdrop: 'static' });
    }
    // pervious() {
    //     this.router.navigate(['/account-settings/rubrics/add-performace'], { queryParams: { framework_id: this.account_tag_id, is_edited: true, name: this.name } });
    // }
    fetch() {
        const payload = {
            framework_id: this.account_tag_id
        }
        if (this.isEdited == true) {
            this.sub.add(this.rubricsService.getUniquePerformances(payload).subscribe(res => {
                if (res['standards_with_unique_descriptions'].length == 0) {
                    this.no_standards = true;
                }
                this.standards = res['standards_with_unique_descriptions'];
                this.performance_levels = this.performanceData.performance_levels;
                this.framework_settings = res['get_framework_settings'];
                this.name = this.framework_settings.framework_name;
                this.loading = false;
                this.index = this.standards.findIndex((obj => obj.standard_level == this.framework_settings.checkbox_level));
                this.onChange(this.standards[this.index], this.standards[this.index].standard_level, this.index);
            },
                error => {
                    this.toastr.ShowToastr('error',error.statusText);
                }))
        }
        else {
            this.sub.add(this.rubricsService.getStandards(payload).subscribe(res => {
                if (res['data'].standards.length == 0) {
                    this.no_standards = true;
                }
                this.standards = res['data'].standards;
                this.performance_levels = this.performanceData.performance_levels;
                this.framework_settings = res['data'].framework_settings
                this.name = this.framework_settings.framework_name;
                this.index = this.standards.findIndex((obj => obj.standard_level == this.framework_settings.checkbox_level));
                this.onChange(this.standards[this.index], this.standards[this.index].standard_level, this.index);
                this.loading = false;
            },
                error => {
                    this.toastr.ShowToastr('error',error.statusText);
                }))
        }

    }
    preventSpaces(event:any, outerIndex?, innerIndex?) {
        this.payload[outerIndex].performance_levels[innerIndex].pl_desc = event.replace(/^\s+/, "")
    }
    preventSpacesFromPaste(event: any, outerIndex?, innerIndex?) {
        this.payload[outerIndex].performance_levels[innerIndex].pl_desc = event.srcElement.value.replace(/^\s+/, "")
    }
    onChange(data, level, index?) {
        if (level == this.framework_settings.checkbox_level) {
            this.selected_index_of_array = data.account_tag_id
            this.index = index;
            let selectedIndex = this.payload.findIndex((obj => obj.id == data.account_tag_id));
            this.selected_name = data.tag_title;

            if (selectedIndex == -1) {
                let array = [];
                if (this.isEdited == true && this.standards[this.index].unique_performance_level_desc.length > 0) {
                    this.performance_levels.forEach(element => {
                        if (element.id) {
                            this.standards[this.index].unique_performance_level_desc.forEach((value, secondKey) => {
                                if (element.id == value.performance_level_id) {
                                    if (value.description == null) {
                                        value.description = "";
                                    }
                                    let obj = {
                                        performance_level: element.name,
                                        pl_id: element.id,
                                        rating_value: element.rating_value,
                                        pl_desc: value.description
                                    };
                                    array.push(obj);
                                }
                            });
                        } else {
                            let obj = {
                                performance_level: element.name,
                                pl_id: -1,
                                rating_value: element.rating_value,
                                pl_desc: ""
                            };
                            array.push(obj);
                        }
                    });
                } else {
                    this.performance_levels.forEach(element => {
                        let obj = {
                            performance_level: element.name,
                            pl_id: -1,
                            rating_value: element.rating_value,
                            pl_desc: ''
                        };
                        array.push(obj)
                    });
                }
                this.unique_description = { id: data.account_tag_id, tag_title: data.tag_title, account_tag_id: data.account_tag_id, performance_levels: array }
                this.payload.push(this.unique_description);
            }
        } else {
            return;
        }
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
    onBlurDecp() {
        this.maxCharDescCheck = false;
    }
    onFocusDesc(j) {
        this.checkIndexArr = j;
        this.maxCharDescCheck = true;
    }
    submit() {
        this.is_check = false;
        let obj = JSON.parse(localStorage.getItem('framework'))
        if (obj) {
            this.standards_obj = {
                standards: this.payload,
                do_publish: obj.do_publish,
                framework_id: this.account_tag_id,
                account_framework_setting_id: this.performanceData.account_framework_setting_id
            }
        } else {
            this.standards_obj = {
                standards: this.payload,
                account_framework_setting_id: this.performanceData.account_framework_setting_id
            }
        }
        this.sub.add(this.rubricsService.performance(this.performanceData).subscribe(res => {
            if ((res['success'])) {
                this.sub.add(this.rubricsService.saveUniqueDescription(this.standards_obj).subscribe(res => {
                    if (res['success']) {
                        localStorage.removeItem('framework');
                        this.navigate();
                    }
                },
                    error => {
                        this.toastr.ShowToastr('error',error.statusText);
                    }))
            }
        },
            error => {
                this.toastr.ShowToastr('error',error.statusText);
            }));

    }
    navigate() {
        this.router.navigate(['account-settings/rubrics']);
    }

    @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
        if (this.is_check) {
            if (confirm(this.translations.you_are_about_to_leave_pl_angular_rubrics)) {
                window.onbeforeunload = () => {
                    this.ngOnDestroy();
                }
            }
            event.returnValue = false;
        }
    }
}
