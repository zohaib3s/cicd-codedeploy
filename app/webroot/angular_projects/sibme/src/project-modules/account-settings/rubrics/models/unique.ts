export interface Unique {
    id?:number;
    tag_title: string;
    account_tag_id: string;
    prefix?: string;
    performance_levels: any;
}