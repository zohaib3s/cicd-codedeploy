import { Performace } from './../../models/performance';
import { RubricsService } from './../../../services/rubrics.service';
import { Component, OnInit, OnDestroy, TemplateRef, HostListener } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DragulaService } from 'ng2-dragula';
import { trim } from 'lodash';
import { cloneDeep } from 'lodash';

@Component({
  selector: 'add-performance-level',
  templateUrl: './add-performance.component.html',
  styleUrls: ['./add-performance.component.css']
})
export class AddPerformance implements OnInit, OnDestroy {

  public modalRef: BsModalRef;
  public translations;
  public sub = new Subscription();
  public account_id: any;
  public user_id: any;
  public header_data: any;
  public performance_level: Performace;
  public performance_array = [];
  public maxCharsTitle = 35;
  public maxCharDescp = 1000;
  public account_tag_id;
  public index: number = null;
  public toggle: boolean = false;
  public do_publish: boolean = false;
  public maxCharDescCheck: boolean = false;
  public maxCharTitleCheck: boolean = false;
  public payload;
  // toggle false = uniq_decs off else on
  public selectedIndex: number = undefined;
  public framework_name;
  public length: number = 0;
  public loading: boolean = true;
  public isEdited: boolean = false;
  public framework_id;
  public framework_settings;
  public account_framework_setting_id;
  public delete_pls = [];
  public name: string = null;
  public no_data: boolean = true;
  public checkEmpty: boolean = false;
  public textShowError: boolean = false;
  public is_check = true;
  constructor(

    private headerService: HeaderService,
    private toastr: ShowToasterService,
    private router: Router,
    private route: ActivatedRoute,
    private rubricsService: RubricsService,
    private modalService: BsModalService,
    private DG: DragulaService,
    private activatedRoute: ActivatedRoute,

  ) {
    this.sub.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
    }));
    this.sub.add(this.route
      .queryParams
      .subscribe(params => {
        this.account_tag_id = params['framework_id'];
        if (params.is_edited) {
          this.isEdited = JSON.parse(params.is_edited);
        }

        this.name = params['name'];
      }))


    this.header_data = this.headerService.getStaticHeaderData();
    this.account_id = this.header_data.user_current_account.users_accounts.account_id;
    this.user_id = this.header_data.user_current_account.users_accounts.user_id;
    this.performance_level = { name: "", description: "" }
  }
  ngOnInit() {
    this.getFramworkSettings();
    if (this.isEdited == true) {
      this.activatedRoute.queryParams.subscribe(param => {
        this.framework_id = param.framework_id;

        this.getGlobal_performance();
      });
    } else {
      setTimeout(() => {
        this.loading = false;
      }, 800);
    }

    this.sub.add(this.DG.dropModel("standards").subscribe(args => {
      this.performance_level = { name: "", description: "" }
      this.index = null;
      this.selectedIndex = undefined;
    }))

  }

  getFramworkSettings() {
    this.rubricsService.getFramworkSettings({ framework_id: this.account_tag_id, account_id: this.account_id, user_id: this.user_id, is_duplicate: false }).subscribe((data: any) => {
      if (data.success === false) {
        this.toastr.ShowToastr('error',data.message);
        this.navigateTo();
      } else {
        this.name = data.framework_settings.framework_name
        this.account_framework_setting_id = data.framework_settings.id;
      }
    });
  }
  public loadFilterDialogUpdate(template: TemplateRef<any>) {
    this.is_check = false;
    this.modalRef = this.modalService.show(template, { class: "modal-md  maxcls", backdrop: 'static' });
  }

  getGlobal_performance() {
    this.sub.add(this.rubricsService.getGlobalPerformances({ framework_id: this.framework_id }).subscribe((data: any) => {

      this.framework_settings = data.framework_settings;
      this.performance_array = data.performance_levels;
      this.performance_level = data.performance_levels[0];
      
      if (this.performance_level.description == null) {
        this.performance_level.description = ""
      }
      this.index = 0;
      this.selectedIndex = 0;
      if (this.performance_array.length > 0) {
        this.no_data = false;
      }
      this.length = data.performance_levels.length;

      if (this.framework_settings.enable_unique_desc == 1) {
        this.toggle = true;
      } else {
        this.toggle = false;
      }
      this.loading = false;
    }));
  }

  navigateTo() {
    this.is_check = false;
    this.router.navigate(['account-settings/rubrics']);
    localStorage.removeItem('framework')
  }
  add_performance_level() {
    this.performance_array = cloneDeep(this.performance_array);
    if (this.performance_array.length == 0) {
      this.no_data = false;
    }
    if (this.performance_array.length > 9) {
      this.toastr.ShowToastr('info',this.translations.cant_add_more_pl_angular_rubrics)
    } else {
      if (this.selectedIndex == undefined && this.index == null) {
        // here it will push at the buttom
        let performance_level_data = { name: "PL Title", description: "PL description" }
        this.performance_array.unshift(performance_level_data);
        this.performance_level = { name: "", description: "" }
        this.length = this.performance_array.length;
        // this.index = null
        this.selected(performance_level_data, 0);
      } else {
        // here it will push at the selected Index
        let performance_level_data = { name: "PL Title", description: "PL description" }
        //this.performance_array.splice(this.selectedIndex + 1, 0, performance_level_data);
        /*
        Changed as per the feedback from TJ.
        */ 
        this.performance_array.unshift(performance_level_data);
        this.length = this.performance_array.length;
        // this.performance_level = { name: "PL Title", description: "PL description" }
        //this.selectedIndex = this.index = null;
        this.selected(performance_level_data, 0);
      }
    }
  }
  preventSpaces(event){
  this.performance_level.description = event.replace(/^\s+/,"")
  }
  preventSpacesFromPaste(event:any){
    this.performance_level.description = event.srcElement.value.replace(/^\s+/,"")
  }
  preventSpacesInName(event){
  this.performance_level.name = event.srcElement.value.replace(/^\s+/,"")
  }
  preventSpacesFromPasteInName(event:any){
    this.performance_level.name = event.target.value.replace(/^\s+/,"")
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  selected(d, index) {    
    this.index = index;
    this.performance_level = d;
    if (this.performance_level.description == null) {
      this.performance_level.description = ""
    }
    this.framework_name = d.name;
    this.selectedIndex = index;
  }
  onBlurDecp() {
    this.maxCharDescCheck = false;
  }
  onFocusDesc() {
    this.maxCharDescCheck = true;
  }
  onBlurTitle() {
    this.maxCharTitleCheck = false;
  }
  onFocusTitle() {
    this.maxCharTitleCheck = true;
  }
  moveUp() {
    //let index = this.performance_array.findIndex((obj => obj.id == this.performance_level.id))
    if (this.index === null || this.index === -1) {
      return;
    } else {
      if (this.index > 0) {
        var element = this.performance_array[this.index];
        this.performance_array.splice(this.index, 1);
        this.performance_array.splice(this.index - 1, 0, element)
        this.index--;
        this.selectedIndex--;
      } else {
        return false;
      }
    }
  }
  moveDown() {
    //let index = this.performance_array.findIndex((obj => obj.id == this.performance_level.id))
    let arrLength = this.performance_array.length;
    arrLength = arrLength - 1;
    if (this.index === null || this.index === arrLength) {
      return false;
    } else {
      if (this.index < this.performance_array.length) {
        if (this.index >= 0) {
          var element = this.performance_array[this.index];
          this.performance_array.splice(this.index, 1);
          this.performance_array.splice(this.index + 1, 0, element)
          this.index++;
          this.selectedIndex++;
        }
      } else {
        return
      }
    }
  }
  remove_performance_level() {
    if (this.index != null) {
      this.length = 0;
      if (this.isEdited == true) {
        let delete_index = this.performance_array[this.index];
        let deleteObj = {};
        if (delete_index == undefined) {
          return;
        } else {
          deleteObj = delete_index.id
          this.delete_pls.push(deleteObj)
        }
      }

      if (this.index != null) {
        if (this.index >= 0) {
          this.performance_array.splice(this.index, 1);
          this.index = null
          this.selectedIndex = undefined;
        } else {
          this.toastr.ShowToastr('error',"Following action is not allowed")
        }
        this.length = this.performance_array.length
        if (this.length == 0) {
          this.no_data = true;
          this.selectedIndex = undefined;
        }
      }
      this.performance_level = { name: "", description: "" }
    } else {
      return false;
    }
  }
  togglePlanMode(e) {
    this.toggle = e
  }
  addRating() {
    let number = this.performance_array.length;
    for (let i = 0; i < this.performance_array.length; i++) {
      // checking if any level heading or descp is empty
      if (trim(this.performance_array[i].name) == '') {
        this.checkEmpty = true;
        this.textShowError = true;
        return false;
      }
      this.performance_array[i].name = this.performance_array[i].name.trim();
      this.performance_array[i].rating_value = number;
      if (this.isEdited == false) {
        delete this.performance_array[i].id;
      }
      number--;
    }
  }
  submit() {
    this.is_check = false;
    this.addRating();
    if (this.checkEmpty == false) {   // If level heading or descp is empty
      if (this.performance_array.length > 0) {
        let uniq_dec = this.toggle ? 1 : 0;
        let obj = JSON.parse(localStorage.getItem('framework'))
        if (obj) {
          if (this.isEdited == true) {
            this.payload = {
              account_id: this.account_id,
              framework_id: this.account_tag_id,
              unique_description: uniq_dec,
              do_publish: obj.do_publish,
              performance_levels: this.performance_array,
              delete_pls: this.delete_pls
            }
          } else {
            this.payload = {
              account_id: this.account_id,
              framework_id: this.account_tag_id,
              unique_description: uniq_dec,
              do_publish: obj.do_publish,
              performance_levels: this.performance_array

            }
          }
        } else {
          if (this.isEdited == true) {
            this.payload = {
              account_id: this.account_id,
              framework_id: this.account_tag_id,
              unique_description: uniq_dec,
              performance_levels: this.performance_array,
              delete_pls: this.delete_pls
            }
          } else {
            this.payload = {
              account_id: this.account_id,
              framework_id: this.account_tag_id,
              unique_description: uniq_dec,
              performance_levels: this.performance_array

            }
          }
        }
        this.sub.add(this.rubricsService.performance(this.payload).subscribe(res => {
          if (res['success']) {
            localStorage.removeItem('framework')
            this.navigateTo();
          }
        },
          error => {
            this.toastr.ShowToastr('error',error.statusText);
          }));
      } else {
        this.toastr.ShowToastr('error',this.translations.you_need_to_add_atleast_one_pl_angular_rubrics);
      }
    } else {
      this.toastr.ShowToastr('error',this.translations.please_enter_performance_name_angular_rubrics);
      this.checkEmpty = false;
    }
  }
  submit_uniq() {
    this.is_check = false;
    this.addRating();

    if (this.checkEmpty == false) { // If level heading is empty
      if (this.performance_array.length > 0) {
        let uniq_dec = this.toggle ? 1 : 0;
        if (this.isEdited == true) {
          this.payload = {
            account_id: this.account_id,
            framework_id: this.account_tag_id,
            unique_description: uniq_dec,
            performance_levels: this.performance_array,
            delete_pls: this.delete_pls,
            account_framework_setting_id: this.account_framework_setting_id
          }
        } else {
          this.payload = {
            account_id: this.account_id,
            framework_id: this.account_tag_id,
            unique_description: uniq_dec,
            performance_levels: this.performance_array,
            account_framework_setting_id: this.account_framework_setting_id
          }
        }

        this.sub.add(this.rubricsService.setPerfData(this.payload));
        if (this.isEdited == false) {
          this.router.navigate(['account-settings/rubrics/unique-performace/', this.account_tag_id]);
        } else {
          this.router.navigate(['account-settings/rubrics/unique-performace/', this.account_tag_id], { queryParams: { is_edited: 'true' } });
        }
        // this.sub.add(this.rubricsService.performance(this.payload).subscribe(res => {
        //   if ((res['success']) && this.isEdited == false) {
        //     this.router.navigate(['account-settings/rubrics/unique-performace/', this.account_tag_id]);
        //   } else {
        //     this.router.navigate(['account-settings/rubrics/unique-performace/', this.account_tag_id], { queryParams: { is_edited: 'true' } });
        //   }
        // },
        //   error => {
        //     this.toastr.ShowToastr('error',error.statusText);
        //   }));
      } else {
        this.toastr.ShowToastr('error',this.translations.you_need_to_add_atleast_one_pl_angular_rubrics);
      }
    } else {
      this.toastr.ShowToastr('error',this.translations.please_enter_performance_name_angular_rubrics);
      this.checkEmpty = false;
    }
  }

  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    if (this.is_check) {
      if (confirm(this.translations.you_are_about_to_leave_pl_angular_rubrics)) {
        window.onbeforeunload = () => {
          this.ngOnDestroy();
        }
      }
      event.returnValue = false;
    }
  }

}
