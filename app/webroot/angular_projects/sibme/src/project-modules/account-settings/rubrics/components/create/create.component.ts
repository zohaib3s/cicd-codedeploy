import { Component, OnInit, ViewChild, OnDestroy, ElementRef, TemplateRef, HostListener } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { Subscription } from 'rxjs';
import { ShowToasterService } from '@projectModules/app/services';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { RubricsService } from '@src/project-modules/account-settings/services/rubrics.service';
import { trim } from 'lodash';

@Component({
    selector: 'rubrics-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css']
})

export class CreateComponent implements OnInit, OnDestroy {

    public modalRef: BsModalRef
    public header_data: any;
    private subscription: Subscription;
    public translations;
    public tier_level = 4;
    public check_box_level = 4;
    public maxChars = 65;
    public rubric_name_check: boolean = false;
    public child_check: boolean;
    public rubric_name: any = "";
    public parent_sharing = false;
    user_id: any;
    account_id: any;
    public loading: boolean = true;
    framework_id: any;
    is_duplicate = false;
    dublicate_check;
    check_new;
    public is_check = true;
    first_step_data: any;
    @ViewChild('framework_sample') framework_sample: ElementRef;
    constructor(
        public headerService: HeaderService,
        public rubricService: RubricsService,
        private toastr: ShowToasterService,
        private modalService: BsModalService,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {
        this.header_data = this.headerService.getStaticHeaderData();
        this.user_id = this.header_data.user_current_account.users_accounts.user_id;
        this.account_id = this.header_data.user_current_account.users_accounts.account_id;
        this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
            this.translations = languageTranslation;
        });
    }
    preventSpacesFromPaste(event: any) {
        this.rubric_name = event.srcElement.value.replace(/^\s+/, "")
    }
    inputValue(event:any){
        this.rubric_name = event.srcElement.value.replace(/^\s+/, "")
    }
    ngOnInit() {
        this.activatedRoute.queryParams.subscribe(param => {
            this.framework_id = param.framework_id;
            this.is_duplicate = param.is_duplicate;
        });
        if (this.framework_id) {
            this.getFramworkSettings();
        }
        this.first_step_data = JSON.parse(localStorage.getItem('first_step_data'));
        this.dublicate_check=JSON.parse(localStorage.getItem('duplicate_framework_data'));
        this.check_new=localStorage.getItem('check_new');
        this.child_check = this.header_data.child_accounts_exist;
        
        setTimeout(() => {
            this.loading = false;
        }, 800);
    }
    public loadFilterDialogUpdate(template: TemplateRef<any>) {
        this.is_check = false;
        this.modalRef = this.modalService.show(template, { class: "modal-md  maxcls", backdrop: 'static' });
    }

    getFramworkSettings() {
        let obj = {
            framework_id: this.framework_id,
            account_id: this.account_id,
            user_id: this.user_id
        } as any;

        if (this.is_duplicate || localStorage.getItem('duplicate_framework_data')) {
            obj.is_duplicate = true;
        } else {
            obj.is_duplicate = false;
        }
        this.rubricService.getFramworkSettings(obj).subscribe((data: any) => {
           if (data.success) {
            if (this.first_step_data) {
                this.rubric_name = this.first_step_data.rubric_name;
                this.parent_sharing = this.first_step_data.parent_sharing;
                this.tier_level = this.first_step_data.tier_level;
                this.check_box_level = this.first_step_data.checkbox_level;
            } else {
                this.parent_sharing = data.framework_settings.parent_child_share;
                this.tier_level = data.framework_settings.tier_level;
                this.check_box_level = data.framework_settings.checkbox_level;
                this.rubric_name = this.is_duplicate ? this.translations.copy_of_angular_rubrics + " " + data.framework_settings.framework_name : data.framework_settings.framework_name;

                if (this.rubric_name.length > this.maxChars) {
                    let length = this.rubric_name.length - this.maxChars;
                    this.rubric_name = this.rubric_name.substring(0, this.rubric_name.length - length);
                }
            }
           } else if (data.success === false) {
               this.is_check = false;
               this.toastr.ShowToastr('error',data.message);
               this.router.navigate(['/account-settings/rubrics']);
           }
            
        });
    }

    selectTierLevel(value) {
        this.tier_level = value;
        if (this.tier_level < this.check_box_level) {
            this.check_box_level = this.tier_level
        }

    }
    onBlurTitle() {
        this.rubric_name_check = false;
    }
    onFocusTitle() {
        this.rubric_name_check = true;
    }
    checkBoxLevel(value) {
        if (this.tier_level < value) {
            return false;
        } else {
            this.check_box_level = value;
        }
    }

    // shareChidAccounts(event){
    //     if (event.target.value === 'on') {
    //         this.parent_sharing = true;
    //         console.log( this.parent_sharing )
    //     }else{
    //         this.parent_sharing = false;
    //         console.log( this.parent_sharing )
    //     }
    //     console.log( this.parent_sharing );
    // }
    navigateTo() {
        localStorage.removeItem('first_step_data');
        this.router.navigate(['account-settings/rubrics']);
    }

    saveFramework() {
        this.is_check = false;
        // this.rubric_name=this.rubric_name.trim()
        if (trim(this.rubric_name) == '') {
            this.toastr.ShowToastr('error',this.translations.please_enter_framework_name_angular_rubrics);
            return false;
        }
        this.rubric_name=this.rubric_name.trim();
        let obj = {
            parent_sharing: this.parent_sharing,
            checkbox_level: this.check_box_level,
            tier_level: this.tier_level,
            rubric_name: this.rubric_name,
            framework_sample: this.framework_sample.nativeElement.innerHTML,
            user_id: this.user_id,
            account_id: this.account_id
        } as any;


        if (this.framework_id && !this.is_duplicate) {
            obj.framework_id = this.framework_id;
            const source_id = localStorage.getItem('source_id');
            let queryParams = {} as any;
            if (source_id) {
                queryParams.framework_id = source_id;
                queryParams.is_duplicate = true;
            } else {
                queryParams.framework_id = this.framework_id;
            }
            
            localStorage.setItem('first_step_data', JSON.stringify(obj));
            this.router.navigate(['/account-settings/rubrics/create-step-two'], { queryParams: queryParams });

        } else {
            this.rubricService.createRubric(obj).subscribe((data: any) => {
                if (data.success) {

                    let params = {
                        framework_id: this.is_duplicate ? this.framework_id : data.framework_settings.account_tag_id
                    } as any;

                    if (this.is_duplicate) {
                        let duplicate_framework_data = {
                            duplicate_framework_id: data.framework_settings.account_tag_id,
                            duplicate_framework_name: data.framework_settings.framework_name
                        } as any;

                        localStorage.setItem('duplicate_framework_data', JSON.stringify(duplicate_framework_data));
                        params.is_duplicate = true
                    }
                    this.router.navigate(['/account-settings/rubrics/create-step-two'], { queryParams: params });
                }
            });
        }
    }

    @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
        if (this.is_check) {
            if (confirm(this.translations.you_are_about_to_leave_framework_angular_rubrics)) {
                window.onbeforeunload = () => {
                    this.ngOnDestroy();
                }
            }
            event.returnValue = false;
        }
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}