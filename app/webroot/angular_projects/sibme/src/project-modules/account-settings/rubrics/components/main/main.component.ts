import { RubricsService } from './../../../services/rubrics.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PageAnimation } from '@app/helpers/page.animation';
@Component({
  selector: 'rubrics-main',
  animations: [PageAnimation],
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy {

  config = {
    currentPage: 1,
    itemsPerPage: 12,
    totalItems: 0
  };
  public translations;
  public sub = new Subscription();
  public account_id: any;
  public header_data: any;
  public rubrics_data: any;
  deleteModalRef: BsModalRef;
  editdeleteModalRef: BsModalRef;
  unpublishModalRef: BsModalRef;
  addPerformanceLevel: BsModalRef;
  editPerformanceLevel: BsModalRef;
  public pageState = 'active';
  public unpublish_confirmation = '';
  public confirmation;
  public ConfirmationKey;
  public dataToDelete: any = {};
  public performanceDelete: any = {};
  public dataToUnpublish: any = {};
  public loading: boolean = true;
  public no_data: boolean = false;
  public framework: any;
  public dub_WithPerformance:any;


  @ViewChild('deleteTemplate', { static: false }) deleteTemplate;
  @ViewChild('editdeleteTemplate', { static: false }) editdeleteTemplate;
  @ViewChild('unpublishTemplate', { static: false }) unpublishTemplate;
  @ViewChild('addPerformanceLevelTemplate', { static: false }) addPerformanceLevelTemplate;
  @ViewChild('editPerformanceLevelTemplate', { static: false }) editPerformanceLevelTemplate;

  public accountData = [];

  constructor(
    private headerService: HeaderService,
    private toastr: ShowToasterService,
    private router: Router,
    private modalService: BsModalService,
    private rubricsService: RubricsService
  ) {
    this.sub.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
      this.ConfirmationKey = this.translations.Huddle_confirmation_bit
    }));
    this.header_data = this.headerService.getStaticHeaderData();
    this.account_id = this.header_data.user_current_account.users_accounts.account_id;
  }
  ngOnInit() {

    this.fetchRubrics(this.account_id);
    this.framework = JSON.parse(localStorage.getItem('framework'));
    this.dub_WithPerformance=JSON.parse(localStorage.getItem('dub_withperformance'));
    this.removeLocalStorage();
    if (this.framework ) {
      setTimeout(() => {
        this.addPerformanceLevel = this.modalService.show(this.addPerformanceLevelTemplate,{ class: "modal-md  maxcls", backdrop: 'static' });
      }, 2000);
    }
    else if(this.dub_WithPerformance){
      setTimeout(() => {
        this.editPerformanceLevel = this.modalService.show(this.editPerformanceLevelTemplate,{ class: "modal-md  maxcls", backdrop: 'static' });
      }, 2000);
      
    }

  }
  fetchRubrics(id) {
    const payload = {
      account_id: id,
      page: 1
    }
    this.sub.add(this.rubricsService.fetchRubrics(payload).subscribe(res => {
      if(res['framework_list'].length == 0){
        this.no_data = true;
      }
      this.rubrics_data = res['framework_list'];
      this.config.totalItems = res['frameworks_count'];

      this.loading = false;

    }))
  }
  fetchNext(pageNumber: number) {
    this.loading = true;
    const payload = {
      account_id: this.account_id,
      page: pageNumber
    }
    this.sub.add(this.rubricsService.fetchRubrics(payload).subscribe(res => {
      if(res['success'] == true){
      this.rubrics_data = res['framework_list'];
      this.loading = false;
      }
    }))
  }
  openDeleteModal(data: any) {
    this.confirmation='';
    this.dataToDelete = data;
    this.deleteModalRef = this.modalService.show(this.deleteTemplate,{ class: "modal-md  maxcls", backdrop: 'static' })
  }
  editDeleteModal(data: any) {
    this.confirmation='';
    this.performanceDelete = data;
    this.editdeleteModalRef = this.modalService.show(this.editdeleteTemplate,{ class: "modal-md  maxcls", backdrop: 'static' })
  }
  openUnpublishModal(data: any) {
    this.unpublish_confirmation ='';
    this.dataToUnpublish = data;
    this.unpublishModalRef = this.modalService.show(this.unpublishTemplate,{ class: "modal-md  maxcls", backdrop: 'static' })
  }
  enterPressed(e) {
    if (e.keyCode == 13) {
      this.confirmDelete();
    }

  }
  enter_edit(e){
    if (e.keyCode == 13) {
      this.performance_Delete();
    }
  }
  confirmDelete() {
    if (this.ConfirmationKey != this.confirmation) {
      this.toastr.ShowToastr('info',this.translations.huddle_you_typed_in + " '" + this.ConfirmationKey + "' ");
      return;
    } else {
      
      this.loading = true;
      this.deleteModalRef.hide();
      const payload = {
        framework_id: this.dataToDelete.account_tag_id
      }
      this.rubricsService.deleteFramekwork(payload).subscribe(res => {

        if (res['success'] == true) {
          let id = this.dataToDelete.id;
          const updatedData = this.rubrics_data.filter(function (item) {
            return item.id != id;
          });


          this.rubrics_data = updatedData;
      
          this.config.totalItems = this.config.totalItems - 1;
          if(this.rubrics_data==0 && this.config.totalItems > 0){
            this.onPageChange(this.config.currentPage - 1);
          
          }
          else if(this.config.totalItems==0){
        
          this.no_data=true;
          setTimeout(() => {this.loading=false;}, 1000);
  
          }
          else {
            
             this.fetchNext(this.config.currentPage);
          
          }
        
        } else {
          this.confirmation = "";
          this.toastr.ShowToastr('error',res['message'])
          this.loading = false;
        }
      })
    }
    
  }
  closeModal() {
    this.deleteModalRef.hide();
    this.confirmation = "";
  }
  performance_Delete() {
    if (this.ConfirmationKey != this.confirmation) {
      this.toastr.ShowToastr('info',this.translations.huddle_you_typed_in + " '" + this.ConfirmationKey + "' ");
      return;
    } else {
      this.editdeleteModalRef.hide();
      this.loading = true;
      const payload = {
        framework_id: this.performanceDelete.account_tag_id
      }
      this.rubricsService.deletePerformance(payload).subscribe(res => {
        if (res['success'] == true) {
          this.fetchNext(this.config.currentPage);
          this.toastr.ShowToastr('success',this.translations.performance_level_successfully_delete_angular_rubrics);
          this.loading = false;
          this.editdeleteModalRef.hide();
        } else {
          this.confirmation = "";
          this.toastr.ShowToastr('error',res['message'])
          this.loading = false;
        }
      })
    }
  }
  publish(id: number) {
    this.loading = true;
    const payload = {
      framework_id: id
    }
    this.sub.add(this.rubricsService.publishFramework(payload).subscribe(res => {
      let objIndex = this.rubrics_data.findIndex((obj => obj.id == res['updated_framework'].id));
      this.rubrics_data[objIndex] = res['updated_framework'];
      this.fetchNext(this.config.currentPage);
      this.loading = false;
    }));
  }
  confirmUnpublish(id: number) {
    if (this.unpublish_confirmation != this.translations.unpublished_capital_angular_rubrics) {
      this.toastr.ShowToastr('info',this.translations.huddle_you_typed_in + " '" + this.translations.unpublish_angular_rubrics.toUpperCase() + "' ");
      return;
    } else {
      this.unpublishModalRef.hide();
      this.loading = true;
      const payload = {
        framework_id: id,
        account_id: this.account_id
      };
      this.sub.add(this.rubricsService.unpublishFramekwork(payload).subscribe(res => {
        let index = this.rubrics_data.findIndex((obj => obj.id == res['updated_framework'].id));
        this.rubrics_data[index] = res['updated_framework'];
        // this.rubrics_data[index].is_from_parent=false;
        this.fetchNext(this.config.currentPage);
        this.loading = false;
      }));
      this.unpublishModalRef.hide();
    }
  }

  closePermanceLevelModal() {
    const framework = JSON.parse(localStorage.getItem('framework'));
    if (framework.do_publish) {
      this.rubricsService.publishFramework({ framework_id: framework.framework_id }).subscribe((data: any) => {
        localStorage.removeItem('framework');
        this.fetchRubrics(this.account_id);
        this.addPerformanceLevel.hide();

      });
    } else {
      localStorage.removeItem('framework');
      this.addPerformanceLevel.hide();
    }

  }

  addPerformance() {
    this.addPerformanceLevel.hide();
    this.router.navigate(['/account-settings/rubrics/add-performace'], { queryParams: { framework_id: this.framework.framework_id } });
  }
  editPerformance() {
    this.editPerformanceLevel.hide();
    this.router.navigate(['/account-settings/rubrics/add-performace'], { queryParams: { framework_id: this.dub_WithPerformance.framework_id, is_edited:true,name:this.dub_WithPerformance.name } });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  goToEdit(id: number) {
    this.router.navigate(['/account-settings/rubrics/edit-performace'], { queryParams: { framework_id: id } });
  }
  onPageChange(event) {
    this.fetchNext(event);
    this.config.currentPage = event;
    
  }

  removeLocalStorage(){
    // localStorage.clear();
    localStorage.removeItem('dub_withperformance');
    localStorage.removeItem('duplicate_framework_data');
    localStorage.removeItem('first_step_data');
    localStorage.removeItem('check_new');
    localStorage.removeItem('standards');
    localStorage.removeItem('source_id');
  }
}
