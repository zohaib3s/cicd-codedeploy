import { RubricsService } from './../services/rubrics.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RubricsRoutingModule } from './rubrics-routing.module'
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppLevelSharedModule } from '@shared/shared.module';
import { FormsModule } from '@angular/forms';
import { MainComponent, AddPerformance, AddUniqueDescription, CreateComponent, CreateStepTwoComponent } from './components/index';
import { NgxPaginationModule } from 'ngx-pagination';
import { DragulaModule } from 'ng2-dragula';
import { DragulaService } from 'ng2-dragula';
import {CanDeactivateGuard} from '@src/project-modules/account-settings/rubrics/guards/can-deactivate.guard'
import { FrameworkGuard } from './guards/framework.guard';
import { StepTwoGuard } from './guards/step-two.guard';
import { PerformanceGuard } from './guards/add-performance.guard';
import { NgSlimScrollModule } from 'ngx-slimscroll';
@NgModule({
  declarations: [
    MainComponent,
    AddPerformance,
    CreateComponent,
    CreateStepTwoComponent,
    AddUniqueDescription,
  ],
  imports: [
    CommonModule,
    RubricsRoutingModule,
    DragulaModule,
    LoadingBarHttpClientModule,
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule,
    NgSlimScrollModule,
    AppLevelSharedModule,
    BsDropdownModule.forRoot(),
    NgxPaginationModule,
  ],
  providers: [RubricsService, DragulaService, CanDeactivateGuard , FrameworkGuard, StepTwoGuard , PerformanceGuard]
})
export class RubricsModule {
}
