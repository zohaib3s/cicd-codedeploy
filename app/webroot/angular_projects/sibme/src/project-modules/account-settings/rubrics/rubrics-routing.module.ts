import { MainComponent, AddPerformance, AddUniqueDescription, CreateComponent, CreateStepTwoComponent } from './components/index';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CanDeactivateGuard} from '@src/project-modules/account-settings/rubrics/guards/can-deactivate.guard'
import { FrameworkGuard } from './guards/framework.guard';
import { StepTwoGuard } from './guards/step-two.guard';
import { PerformanceGuard } from './guards/add-performance.guard';
const routes: Routes = [
  { path: '', component: MainComponent, pathMatch: 'full' },
  { path: 'create', component: CreateComponent , canDeactivate: [FrameworkGuard] },
  { path: 'create-step-two', component: CreateStepTwoComponent , canDeactivate: [StepTwoGuard]},
  { path: 'add-performace', component: AddPerformance , canDeactivate: [PerformanceGuard]},
  { path: 'unique-performace/:id', component: AddUniqueDescription, canDeactivate: [CanDeactivateGuard]},
  { path: '**', redirectTo: 'page_not_found' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RubricsRoutingModule {
}
