import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { HeaderService } from '@src/project-modules/app/services';
import { AddPerformance } from '../components';

// TODO:delete if not using anymore
@Injectable()
export class PerformanceGuard implements CanDeactivate<AddPerformance> {
    public translation: any = {};
    
    constructor(private headerService: HeaderService) { }
    canDeactivate(component?: AddPerformance): Observable<boolean> | boolean {
        this.headerService.languageTranslation$.subscribe(languageTranslation => {
            this.translation = languageTranslation;
        });
        if (component.is_check) {
            if (confirm(this.translation.you_are_about_to_leave_pl_angular_rubrics)) {
                return true;
            } else {
                return false;
            }
        } else return true;
        
    }
}