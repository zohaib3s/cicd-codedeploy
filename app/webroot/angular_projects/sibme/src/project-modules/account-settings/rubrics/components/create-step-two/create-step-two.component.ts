import { Component, OnInit, ViewChild, OnDestroy, ElementRef, TemplateRef, HostListener, EventEmitter } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { Subscription } from 'rxjs';
import { ShowToasterService } from '@projectModules/app/services';
import { ActivatedRoute, Router } from '@angular/router';
import { RubricsService } from '@src/project-modules/account-settings/services/rubrics.service';
import { DragulaService } from 'ng2-dragula';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { trim } from 'lodash';
import { cloneDeep } from 'lodash';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';

@Component({
    selector: 'rubrics-create-step-two',
    templateUrl: './create-step-two.component.html',
    styleUrls: ['./create-step-two.component.css']
})

export class CreateStepTwoComponent implements OnInit, OnDestroy {
    opts: ISlimScrollOptions;
    scrollEvents: EventEmitter<SlimScrollEvent>;
    public modalRef: BsModalRef
    public prefix_maxChars = 10;
    public maxChars = 1000;
    public analytic_maxChars = 24;
    public header_data: any;
    private subscription: Subscription;
    public translations;
    public tier_level;
    public checkbox_level;
    public framework_id;
    index = null;
    public loading: boolean = true;
    public framework_settings = {} as any;
    public standards = [] as any;
    public deleted_standards = [] as any;
    public prefix = "";
    public standard_text = "";
    public standard_analytics_label = "";
    user_id: any;
    account_id: any;
    analytics_label: boolean;
    is_duplicate = false;
    duplicate_framework_data: any;
    edit_mode = false;
    do_publish: boolean;
    prefix_check = false;
    standard_check = false;
    analytic_check = false;
    first_step_data: any;
    check_new = false;
    public is_check = true;

    constructor(
        public headerService: HeaderService,
        private toastr: ShowToasterService,
        private activatedRoute: ActivatedRoute,
        private rubricsService: RubricsService,
        private router: Router,
        private DG: DragulaService,
        private modalService: BsModalService,
    ) {
        this.header_data = this.headerService.getStaticHeaderData();
        this.user_id = this.header_data.user_current_account.users_accounts.user_id;
        this.account_id = this.header_data.user_current_account.users_accounts.account_id;
        this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
            this.translations = languageTranslation;
        });

    }

    ngOnInit() {
        this.scrollEvents = new EventEmitter<SlimScrollEvent>();
        this.opts = {
            position: 'right',
            barBackground: '#C9C9C9',
            barOpacity: '0.8',
            barWidth: '10',
            barBorderRadius: '20',
            barMargin: '0',
            gridBackground: '#D9D9D9',
            gridOpacity: '1',
            gridWidth: '0',
            gridBorderRadius: '20',
            gridMargin: '0',
            alwaysVisible: true,
            visibleTimeout: 1000,
          };
        localStorage.removeItem('source_id')
        this.DG.dropModel("standard").subscribe((args: any) => {

            this.index = args.targetIndex;

            this.arraymove(this.standards, args.sourceIndex, args.targetIndex);

            if (args.targetIndex === 0) {
                this.standards[args.targetIndex].standard_level = 1;
            }
            else {
                const upperIndex = args.targetIndex - 1;
                this.standards[args.targetIndex].standard_level = this.standards[upperIndex].standard_level;
            }
        });
        this.activatedRoute.queryParams.subscribe(param => {
            this.framework_id = param.framework_id;
            this.is_duplicate = param.is_duplicate;


            this.getFrameworkStandars();
        });
        this.duplicate_framework_data = JSON.parse(localStorage.getItem('duplicate_framework_data'));
        if (this.is_duplicate) {
            this.duplicate_framework_data = JSON.parse(localStorage.getItem('duplicate_framework_data'));

        }
        setTimeout(() => {
            this.loading = false;
        }, 800);
    }
    @HostListener("click", ["$event"])
    hashCheck(event: any) {
        event.preventDefault();
        event.stopPropagation();
    }

    arraymove(arr, fromIndex, toIndex) {
        var element = arr[fromIndex];
        arr.splice(fromIndex, 1);
        arr.splice(toIndex, 0, element);

        // this.standards = this.standards;

    }
    public loadFilterDialogUpdate(template: TemplateRef<any>) {
        this.is_check = false;
        this.modalRef = this.modalService.show(template, { class: "modal-md  maxcls", backdrop: 'static' });

    }
    navigateTo() {
        localStorage.removeItem('first_step_data');
        this.router.navigate(['account-settings/rubrics']);
    }
    preventSpacesFromPaste(event: any) {
        if (event.target.id === "prefix") {
            this.prefix = event.target.value.trim();
        }
        if (event.target.id === "standard_text") {
            this.standard_text = event.target.value.trim();
        }
        if (event.target.id === "analytics_label") {
            this.standard_analytics_label = event.target.value.trim();
        }
    }
    prefixCheck() {
        this.prefix_check = true;
        this.standard_check = false;
        this.analytic_check = false;
        if (this.prefix == null) {

            this.prefix = "";
        }

    }
    standardCheck() {
        this.standard_check = true;
        this.analytic_check = false;
        this.prefix_check = false;
        if (this.standard_text == null) {
            this.standard_text = "";
        }

    }
    analyticCheck() {
        this.analytic_check = true;
        this.prefix_check = false;
        this.standard_check = false;
        if (this.standard_analytics_label == null) {
            this.standard_analytics_label = "";
        }
    }


    getFrameworkStandars() {
        let obj = {
            framework_id: this.framework_id,
            account_id: this.account_id,
            user_id: this.user_id
        } as any;

        if (this.is_duplicate || localStorage.getItem('duplicate_framework_data')) {
            obj.is_duplicate = true;
        } else {
            obj.is_duplicate = false;
        }
        this.rubricsService.getFrameStandards(obj).subscribe((data: any) => {
            if (data.success) {
                this.framework_settings = data.data.framework_settings;
                this.tier_level = this.framework_settings.tier_level;
                this.checkbox_level = this.framework_settings.checkbox_level;

                const check_standards = JSON.parse(localStorage.getItem('standards'));
                if (check_standards) {
                    if (this.check_new === false) {
                        this.edit_mode = true;
                    }

                    if (localStorage.getItem('check_new')) {
                        this.check_new = true;
                        this.edit_mode = false;
                    }
                    this.standards = check_standards;
                    this.first_step_data = JSON.parse(localStorage.getItem('first_step_data'));
                    if (this.first_step_data) {
                        this.standards.forEach(element => {
                            if (element.standard_level > this.first_step_data.tier_level) {
                                element.standard_level = this.first_step_data.tier_level;
                            }
                        });
                        this.tier_level = this.first_step_data.tier_level;
                        this.checkbox_level = this.first_step_data.checkbox_level;
                        this.framework_settings.framework_name = this.first_step_data.rubric_name;


                    }

                } else {
                    if (data.data.standards.length === 0) {
                        this.check_new = true;
                        for (let i = 1; i <= this.framework_settings.tier_level; i++) {
                            let obj = {
                                prefix: '1.' + i,
                                standard_level: i,
                                standard_text: this.translations.sample_data_angular_rubrics,
                                standard_analytics_label: '1.' + i,
                            }
                            this.standards.push(obj);
                        }

                    }
                    else {
                        this.edit_mode = true;

                        this.standards = data.data.standards;
                        this.first_step_data = JSON.parse(localStorage.getItem('first_step_data'));
                        if (this.first_step_data) {
                            this.standards.forEach(element => {
                                if (element.standard_level > this.first_step_data.tier_level) {
                                    element.standard_level = this.first_step_data.tier_level;
                                }
                            });
                            this.tier_level = this.first_step_data.tier_level;
                            this.checkbox_level = this.first_step_data.checkbox_level;
                            this.framework_settings.framework_name = this.first_step_data.rubric_name;


                        } else {

                        }
                        this.index = 0;
                        this.populateData(this.index)
                    }
                }
            } else if (data.success == false) {
                this.is_check = false;
                this.toastr.ShowToastr('error',data.message);
                this.router.navigate(['/account-settings/rubrics']);
            }
        })
    }

    inputValue(event) {
     
        if (this.prefix != null) {
         
            if (event.target.id === "prefix") {
               
                this.prefix = event.target.value.replace(/^\s+/, "")
            }
            this.standards[this.index].prefix = this.prefix;


        }
        if (this.standard_text != null) {
         
            if (event.target.id === "standard_text") {
              
                this.standard_text = event.target.value.replace(/^\s+/, "")
            }

            this.standards[this.index].standard_text = this.standard_text;

        }

        if (this.standard_analytics_label != null) {
            
            if (event.target.id === "analytics_label") {
               
                this.standard_analytics_label = event.target.value.replace(/^\s+/, "")
            }
            this.standards[this.index].standard_analytics_label = this.standard_analytics_label;

        }
    }
    @HostListener('window:keydown', ['$event'])
    handleKeyboardEvent(event: KeyboardEvent) {

        if (event.keyCode == 40) {

            //down

            let arrLength = this.standards.length;
            arrLength = arrLength - 1;

            if (this.index === arrLength) {
                // this.index = 0;
                return false;

            } else if (this.index === null) {
                this.index = 0;

            } else {
                if (this.index < this.standards.length) {
                    if (this.index >= 0) {
                        this.index = this.index + 1;
                    }
                }
            }

        }
        else if (event.keyCode == 38) {
            //up
            if (this.index === null || this.index === -1) {
                this.index = 0;
                return false;
            } else {
                if (this.index > 0) {
                    this.index = this.index - 1;
                } else {
                    return false;
                }
            }
        }

        this.populateData(this.index)
    }

    addrow() {


        if (this.index !== null) {
            let standard_level = this.standards[this.index].standard_level;
            let obj = {
                prefix: '1.1',
                standard_level: standard_level,
                standard_text: this.translations.sample_data_angular_rubrics,
                standard_analytics_label: '1.0',
            }
            // this.standards.push(obj);

            this.standards.splice(this.index + 1, 0, obj);
            this.selectRow(this.index + 1);
            // this.arraymove(this.standards , this.index , this.index);
        } else {


            let obj = {
                prefix: '1.1',
                standard_level: 1,
                standard_text: this.translations.sample_data_angular_rubrics,
                standard_analytics_label: '1.0'
            }
            this.standards.push(obj);
            this.selectRow(this.standards.length-1);
        }

        this.standards = cloneDeep(this.standards);
    }

    selectRow(i) {
        this.index = i;
        this.populateData(this.index);

    }

    deleteRow() {
        if (this.index >= 0 && this.index != undefined) {
            if (this.standards[this.index].account_tag_id && !this.is_duplicate) {
                this.deleted_standards.push(this.standards[this.index].account_tag_id);
            }


            this.standards.splice(this.index, 1);
            this.index = null;
            this.prefix = "";
            this.standard_text = "";
            this.standard_analytics_label = "";
        } else {
            return false;
        }
    }

    indentRow() {

        if (this.index >= 0 && this.index != undefined) {

            if (this.standards[this.index].standard_level > 1) {
                this.standards[this.index].standard_level = this.standards[this.index].standard_level - 1;

                if (this.checkbox_level === this.standards[this.index].standard_level) {
                    this.analytics_label = true
                }
                else {
                    this.analytics_label = false;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

    outdentRow() {
        if (this.index >= 0 && this.index != undefined) {

            if (this.standards[this.index].standard_level < this.tier_level) {
                this.standards[this.index].standard_level = this.standards[this.index].standard_level + 1;

                if (this.checkbox_level === this.standards[this.index].standard_level) {
                    this.analytics_label = true;
                }
                else {
                    this.analytics_label = false;
                }

            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    downRow() {

        let arrLength = this.standards.length;
        arrLength = arrLength - 1;

        if (this.index === null || this.index === arrLength) {
            return false;
        }
        else {
            if (this.index < this.standards.length) {
                if (this.index >= 0) {
                    var element = this.standards[this.index];
                    this.standards.splice(this.index, 1);
                    this.standards.splice(this.index + 1, 0, element);
                    this.index = this.index + 1;

                } else {
                    return false;
                }
            }
        }


    }

    upRow() {
        if (this.index === null || this.index === -1) {
            return false;
        } else {

            if (this.index > 0) {
                var element = this.standards[this.index];
                this.standards.splice(this.index, 1);
                this.standards.splice(this.index - 1, 0, element)
                this.index = this.index - 1;

            } else {
                return false;
            }
        }

    }

    addStandardPosition() {
        // let number = this.standards.length
        for (let i = 0; i < this.standards.length; i++) {
            this.standards[i].standard_position = i + 1;
            //   number--;
        }
    }

    save() {
        if (this.standards.length == 0) {
            this.toastr.ShowToastr('error',this.translations.please_add_atleast_one_standard_angular_rubrics);
            return false;
        }

        const standard_text = this.standards.some(el => trim(el.standard_text) == '');
        if (standard_text) {
            this.toastr.ShowToastr('error',this.translations.please_enter_standard_text_angular_rubrics);
            return false;
        }

        if (this.framework_settings.enable_performance_level === 0) {
            let obj = {
                do_publish: false,
                framework_id: this.is_duplicate ? this.duplicate_framework_data.duplicate_framework_id : this.framework_id,
            }
            localStorage.setItem('framework', JSON.stringify(obj));
        } else {
            if (this.is_duplicate) {
                let obj = {
                    do_publish: false,
                    framework_id: this.is_duplicate ? this.duplicate_framework_data.duplicate_framework_id : this.framework_id,
                    name: this.duplicate_framework_data.duplicate_framework_name,
                }
                localStorage.setItem('dub_withperformance', JSON.stringify(obj));
            }

        }

        if (this.edit_mode) {
            this.do_publish = false;
        }

        localStorage.removeItem('check_new');
        this.saveFramework();
    }

    publish() {
        if (this.standards.length == 0) {
            this.toastr.ShowToastr('error',this.translations.please_add_atleast_one_standard_angular_rubrics);
            return false;
        }

        const standard_text = this.standards.some(el => trim(el.standard_text) == '');
        if (standard_text) {
            this.toastr.ShowToastr('error',this.translations.please_enter_standard_text_angular_rubrics);
            return false;
        }

        if (this.framework_settings.enable_performance_level === 0) {
            let obj = {
                do_publish: true,
                framework_id: this.is_duplicate ? this.duplicate_framework_data.duplicate_framework_id : this.framework_id,
            }
            localStorage.setItem('framework', JSON.stringify(obj));

        }
        if (this.is_duplicate) {
            let obj = {
                do_publish: true,
                framework_id: this.is_duplicate ? this.duplicate_framework_data.duplicate_framework_id : this.framework_id,
                name: this.duplicate_framework_data.duplicate_framework_name,
            }
            localStorage.setItem('dub_withperformance', JSON.stringify(obj));
        }
        if (this.edit_mode) {
            this.do_publish = true;
        }

        this.saveFramework();
    }


    saveFramework() {
        this.is_check = false;
        this.addStandardPosition();
        localStorage.removeItem('standards');
        if (this.is_duplicate) {
            this.standards.forEach(element => {
                element.source_account_tag_id = element.account_tag_id;
                delete element.account_tag_id;
            });
        }
        let obj = {
            account_id: this.account_id,
            user_id: this.user_id,
            framework_id: this.is_duplicate ? this.duplicate_framework_data.duplicate_framework_id : this.framework_id,
            standards: this.standards,
            delete_standards: this.deleted_standards
        } as any;

        if (this.edit_mode && this.framework_settings.enable_performance_level === 1) {
            obj.do_publish = this.do_publish,
                obj.edit_mode = this.edit_mode
        }

        if (this.first_step_data) {

            this.rubricsService.createRubric(this.first_step_data).subscribe((data: any) => {
                if (data.success) {
                    this.rubricsService.saveFramworkStandards(obj).subscribe((data: any) => {
                        if (data.success) {
                            if (this.is_duplicate) {
                                let duplicate_performance_levels = {
                                    original_framework_id: this.framework_id,
                                    duplicate_framework_id: this.duplicate_framework_data.duplicate_framework_id
                                }

                                this.rubricsService.duplicatePerformanceLevels(duplicate_performance_levels).subscribe((data: any) => {
                                    // if (data.success) {
                                    //     localStorage.removeItem('duplicate_framework_data');
                                    //     this.router.navigate(['/account-settings/rubrics'])
                                    // }
                                });
                            }
                            setTimeout(() => {
                                localStorage.removeItem('duplicate_framework_data');
                                this.router.navigate(['/account-settings/rubrics'])
                            }, 1000);
                        }
                        // localStorage.removeItem('first_step_data');
                        // this.router.navigate(['/account-settings/rubrics']);
                    });
                }
            });
        } else {
            this.rubricsService.saveFramworkStandards(obj).subscribe((data: any) => {
                if (data.success) {
                    if (this.is_duplicate) {
                        let duplicate_performance_levels = {
                            original_framework_id: this.framework_id,
                            duplicate_framework_id: this.duplicate_framework_data.duplicate_framework_id
                        }

                        this.rubricsService.duplicatePerformanceLevels(duplicate_performance_levels).subscribe((data: any) => {
                            // if (data.success) {
                            //     localStorage.removeItem('duplicate_framework_data');
                            //     this.router.navigate(['/account-settings/rubrics'])
                            // }
                        });
                    }
                    setTimeout(() => {
                        localStorage.removeItem('duplicate_framework_data');
                        this.router.navigate(['/account-settings/rubrics'])
                    }, 1000);
                }
            });
        }

    }


    pervious() {
        this.is_check = false;
        if (this.check_new && !this.duplicate_framework_data) {

            localStorage.setItem('check_new', 'new');
        }

        if (this.is_duplicate) {
            localStorage.setItem('source_id', this.framework_id);
        }
        this.addStandardPosition();
        localStorage.setItem('standards', JSON.stringify(this.standards));

        this.router.navigate(['/account-settings/rubrics/create'], { queryParams: { framework_id: this.is_duplicate ? this.duplicate_framework_data.duplicate_framework_id : this.framework_id } });
    }


    populateData(index) {
        this.prefix = this.standards[index].prefix;
        this.standard_text = this.standards[index].standard_text;
        this.standard_analytics_label = this.standards[index].standard_analytics_label;

        if (this.checkbox_level === this.standards[index].standard_level) {
            this.analytics_label = true;
        }
        else {
            this.analytics_label = false;
        }
    }



    @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
        if (this.is_check) {
            if (confirm(this.translations.you_are_about_to_leave_framework_angular_rubrics)) {
                window.onbeforeunload = () => {
                    this.ngOnDestroy();
                }
            }
            event.returnValue = false;
        }
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}