import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { HeaderService } from '@src/project-modules/app/services';
import {CreateComponent } from '../components';

// TODO:delete if not using anymore
@Injectable()
export class FrameworkGuard implements CanDeactivate<CreateComponent> {
    public translation: any = {};
    
    constructor(private headerService: HeaderService) { }
    canDeactivate(component?: CreateComponent): Observable<boolean> | boolean {
        this.headerService.languageTranslation$.subscribe(languageTranslation => {
            this.translation = languageTranslation;
        });
        if (component.is_check) {
            if (confirm(this.translation.you_are_about_to_leave_framework_angular_rubrics)) {
                return true;
            } else {
                return false;
            }
        } else return true;
        
    }
}