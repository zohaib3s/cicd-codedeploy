export interface Performace {
    id?: number;
    name?: string;
    description?: string;
    rating_value?: number;
  }