import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { HeaderService } from '@src/project-modules/app/services';
import { CustomFieldsComponent } from '../components/custom-fields/custom-fields.component';

// TODO:delete if not using anymore
@Injectable()
export class customFieldGuard implements CanDeactivate<CustomFieldsComponent> {
    public translation: any = {};
    
    constructor(private headerService: HeaderService) { }
    canDeactivate(component?: CustomFieldsComponent): Observable<boolean> | boolean {
        this.headerService.languageTranslation$.subscribe(languageTranslation => {
            this.translation = languageTranslation;
        });
        if (!component.is_check) {
            if (confirm(this.translation?.cf_confirm_leave_customfields)) {
                return true;
            } else {
                return false;
            }
        } else return true;
        
    }
}