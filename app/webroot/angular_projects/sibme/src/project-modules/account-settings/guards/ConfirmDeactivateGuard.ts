import { Injectable } from '@angular/core';
import { CanDeactivate } from "@angular/router";
import { MainComponent } from '../plans-billings/components/main/main.component';
import { HeaderService } from '@src/project-modules/app/services';
@Injectable()
export class ConfirmDeactivateGuard implements CanDeactivate<MainComponent> {
    public translations;

constructor(private headerService: HeaderService) {
    this.headerService.languageTranslation$.subscribe(languageTranslation => {
        this.translations = languageTranslation;
      });
}
canDeactivate(component: MainComponent) {
        return confirm(this.translations.you_have_unsaved_changes);
    }

}