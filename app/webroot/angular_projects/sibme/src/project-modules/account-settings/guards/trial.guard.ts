import { Injectable } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '@src/environments/environment';
import { Router, CanActivate } from "@angular/router";
@Injectable()
export class TrialGuard implements CanActivate {

    public account_id: any;
    public header_data: any;
    constructor(private headerService: HeaderService, private router: Router) {
        // Dev : Aqib
        // Description: To check if the user is in Trial or not
    }
    canActivate(): boolean | Observable<boolean> {

        let base_url = environment.baseHeaderUrl + `/angular_header_api/`;
        return this.headerService.getHeaderData(base_url).pipe(map(res => {
            // console.log('trial guard');
            // debugger;
            if (res) {
                this.header_data = res
                if(this.header_data.user_current_account.accounts.in_trial == true){
                    this.router.navigate(['account-settings/plans/change']);
                } else {
                    return true;
                }
                
            }
        }))
    }

}
