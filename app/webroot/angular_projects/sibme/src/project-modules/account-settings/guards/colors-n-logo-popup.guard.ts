import { Injectable } from '@angular/core';
import { CanDeactivate } from "@angular/router";
import { ColorsLogoComponent } from '../components/colors-logo/colors-logo.component';
@Injectable()
export class ColorsLogoPopup implements CanDeactivate<ColorsLogoComponent> {

    constructor() {
    }
    canDeactivate(component: ColorsLogoComponent) {
       if (component.changesSaved) {
            component.saveChangesModal.show();
            return false;
       } else {
           return true
       }
    }

}
