import { Injectable } from '@angular/core';
import { CanDeactivate } from "@angular/router";
import { GlobalExportComponent } from '../components/global-export/global-export.component';
@Injectable()
export class GlobalExportPopup implements CanDeactivate<GlobalExportComponent> {

    constructor() {
    }
    canDeactivate(component: GlobalExportComponent) {
       if (component.changesSaved) {
            component.saveChangesModal.show();
            return false;
       } else {
           return true
       }
    }

}
