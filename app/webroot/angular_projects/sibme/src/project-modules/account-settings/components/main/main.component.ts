import { PlansService } from './../../services/plans.service';
import { AccountSettingsService } from '../../services/account-settings.service'
import { Component, OnInit } from '@angular/core';
import { HeaderService } from '@app/services';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import { environment } from '@src/environments/environment';

@Component({
  selector: 'app-account-settings-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
})
export class MainComponent implements OnInit {
  public archivingPermission: boolean;
  public GlobalExportPermission: boolean;
  public rubricPermission: boolean;
  public plansPermission: boolean;
  public header_data: any;
  public companyName: any = '';
  public account_id: any;
  public showCancel : boolean = false;
  public translations;
  public hideForSuperAdmin: boolean = false;
  public hideCustomFields: boolean = false;
  public user_role:any;
  public stats: any = {};
  public storageProgressBar: any;
  public usersProgressBar: any;
  public skelton_loading: boolean = true;
  public enable_goals: boolean;
  public goalAltName: any = '';
  public breadcrumb : boolean= false;
  private subscription: Subscription;
  constructor(private headerService: HeaderService,
              private toastr: ShowToasterService,
              private accountSettingsService: AccountSettingsService,
              private planService : PlansService) {
    this.headerService.behaviorialHeaderData$.subscribe(headerData => {
      this.header_data = headerData;
      this.proChecks();
      this.account_id = this.header_data.user_current_account.users_accounts.account_id;
      this.companyName = this.header_data.user_current_account.accounts.company_name;
      this.user_role = this.header_data.user_current_account.roles.role_id; 
      
      if(this.user_role == 100){
        this.hideForSuperAdmin = true;
      }

      if(this.user_role == 100 || this.user_role == 110) this.hideCustomFields = true;
    });
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
    });
  }
  ngOnInit(): void {

    this.getUserAccountStorageStats();
  this.fetchAccount(this.account_id);
  setTimeout(() => {
    this.breadcrumb=true;
  }, 1000);
    }
    fetchAccount(accountId) {
      let obj = {
        account_id: accountId
      };
      this.planService.fetchAccountPlan(obj).subscribe(details => {
        if(details['plan_name'] == 'Trial Account'){
         this.showCancel = true;
        }
      })
    }
  getUserAccountStorageStats() {
    this.accountSettingsService.AccountUserStorageStats(this.account_id).subscribe((data: any) => {
      if (data.success) {

        this.stats = data.stats;
        this.storageProgressBar = { width: this.stats.total_storage_used / this.stats.allowed_storage * 100 + '%', background: '#1486e3' };
        this.usersProgressBar = {
          width: this.stats.total_users_used / this.stats.allowed_users * 100 + '%',
          background: '#28A745'
        };
        this.skelton_loading = false;

      }
    }, error1 => {
      console.log(error1);
    });

  }

  proChecks() {
    this.archivingPermission = this.header_data.archiving_permission;
    this.rubricPermission = this.header_data.rubric_authoring_permission;
    this.GlobalExportPermission = this.header_data.global_export_permission;
    this.plansPermission = this.header_data.plans_permission;
    this.enable_goals = this.header_data.user_current_account.accounts.enable_goals;
    this.goalAltName = this.header_data.goal_settings.goal_alt_name;

  }


  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
