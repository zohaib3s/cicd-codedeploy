import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ColorsLogoService } from '../../services/colors-logo.service';
import { HeaderService } from '@src/project-modules/app/services';
import { HttpClient } from '@angular/common/http';
import { ShowToasterService } from '@projectModules/app/services';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { environment } from '@src/environments/environment';


@Component({
  selector: 'app-colors-logo',
  templateUrl: './colors-logo.component.html',
  styleUrls: ['./colors-logo.component.css']
})
export class ColorsLogoComponent implements OnInit, OnDestroy {

  public header_data;
  public img;
  public translations;
  public account_id;
  public header_background_color;
  public nav_bg_color;
  public logoBgColor;
  public defaultColor = "#FFFFFF";
  public headerBgColor;
  public url: any;
  public body = new FormData();
  public changesSaved = false;
  private subscription: Subscription;
  public not_loading_save: boolean = true;
  @ViewChild('save_changes', { static: false }) saveChangesModal: ModalDirective;

  constructor(private colorsLogoService: ColorsLogoService,
    private headerService: HeaderService, private http: HttpClient,
    private toastr: ShowToasterService, protected senitizer: DomSanitizer,
    private router: Router) {


    this.header_data = this.headerService.getStaticHeaderData();
    this.account_id = this.header_data.user_current_account.users_accounts.account_id;
    // this.header_background_color = this.head
    console.log(this.header_data)
    this.img = this.header_data.logo_image;
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
      // this.huddle_data.title = this.translation.huddle_title;
      // this.LoadHuddleTypes();
    });

  }


  ngOnInit() {
    this.getColorsAndLogo();
  }

  cancel_colors_logo() {
    this.getColorsAndLogo();
  }

  getColorsAndLogo() {
    this.colorsLogoService.GetColorsLogoData(this.account_id).subscribe((data: any) => {

      // this.headerBgColor = data.header_background_color;
      // if (this.headerBgColor == '#fff' || this.headerBgColor == '#ffffff') {
      //   this.headerBgColor = '#dffffe';
      // } else if (this.headerBgColor == '#000' || this.headerBgColor == '#000000') {
      //   this.headerBgColor = '#100000';
      // }
      // this.logoBgColor = data.usernav_bg_color;
      // if (this.logoBgColor == '#fff' || this.logoBgColor == '#ffffff') {
      //   this.logoBgColor = '#dffffe';
      // } else if (this.logoBgColor == '#000' || this.logoBgColor == '#000000') {
      //   this.logoBgColor = '#100000';
      // }
      if (data.usernav_bg_color == "#fff" || data.usernav_bg_color == "##fff") {
        this.logoBgColor = "#FFFFFF";
      } else {
        this.logoBgColor = data.usernav_bg_color;
      }
      this.headerBgColor = data.header_background_color;
      this.headerBgColor = this.headerBgColor.split('#');
      if (this.headerBgColor.length == 2) {
        this.headerBgColor = "#" + this.headerBgColor[1];

      } else {
        this.headerBgColor = "#" + this.headerBgColor[2];

      }

      this.logoBgColor = this.logoBgColor.split('#');

      if (this.logoBgColor.length == 2) {
        this.logoBgColor = "#" + this.logoBgColor[1];

      } else {
        this.logoBgColor = "#" + this.logoBgColor[2];

      }


      this.headerService.header_background_color.next(this.headerBgColor);
      this.headerService.logo_background_color.next(this.logoBgColor);
      this.header_background_color = this.headerBgColor;
      this.nav_bg_color = this.logoBgColor;


    });

  }

  selectFile() {
    document.getElementById('file').click();
  }

  // onFileChanged(files: FileList) {
  //   this.fileToUpload = files.item(0);
  //   console.log(this.fileToUpload);
  //   const reader = new FileReader();
  //   reader.readAsDataURL(files.item(0));
  //   console.log(reader.result);
  //
  // }
  onFileChanged(files: FileList) {
    let fileType = files.item(0).type;
    if (fileType == "image/jpeg" || fileType == "image/png") {

      this.changesSaved = true;

      let reader = new FileReader();

      reader.readAsDataURL(files.item(0)); // read file as data url

      reader.onload = (event: any) => {
        // called once readAsDataURL is completed
        this.url = this.senitizer.bypassSecurityTrustUrl(event.target.result as string);

        this.headerService.logo_image.next(event.target.result);

      };
      let fileToUpload: File = files.item(0);
      this.body.append('image_logo', fileToUpload, fileToUpload.name);
      console.log(this.body);
    }
    else {
      this.toastr.ShowToastr('error',"Sorry, the file type can only be image!");
    }

  }

  saveChanges() {
    this.not_loading_save = false;
    if ((this.header_background_color != undefined || this.header_background_color != null) && (this.nav_bg_color != undefined || this.nav_bg_color != null)) {

      this.body.append('header_background_color', this.header_background_color);

      this.body.append('account_id', this.account_id);

      this.body.append('nav_bg_color', this.nav_bg_color);

      this.colorsLogoService.updateColorLogo(this.body).subscribe((data: any) => {
        this.headerService.logo_image.next(data.image_url);
        this.toastr.ShowToastr('success',this.translations.changes_have_been_successfully_saved_new_settings);
        this.changesSaved = false;
        this.colorsLogoService.updateHeaderSession().subscribe((data) => {
          this.not_loading_save = true;
        });

      }, err => {
        this.toastr.ShowToastr('error','Something went wrong!');
      });

    }
  }

  onHeaderColorChanged(event) {
    this.changesSaved = true;
    this.headerBgColor = event;
    this.headerService.header_background_color.next(this.headerBgColor);
    this.header_background_color = this.headerBgColor.split('#');
    this.header_background_color = this.header_background_color[1];
  }

  onLogoBgColorChanged(event) {
    this.changesSaved = true;
    this.logoBgColor = event;
    this.headerService.logo_background_color.next(this.logoBgColor);
    this.nav_bg_color = this.logoBgColor.split('#');
    this.nav_bg_color = this.nav_bg_color[1];
  }

  resetColorsLogo() {

    this.colorsLogoService.resetColorsLogos(this.account_id).subscribe((data: any) => {
      console.log(data)
      if (data.success) {
        this.body = new FormData();
        this.toastr.ShowToastr('success',this.translations.settings_reset_to_default);
        this.headerService.header_background_color.next(data.header_background_color);
        this.headerService.logo_background_color.next(data.nav_bg_color);
        let imageUrl;
        // if (this.header_data.site_id == 1) imageUrl = "https://s3.amazonaws.com/sibme.com/static/companies/2936/logo-3.png";
        if (this.header_data.site_id == 1) imageUrl = "assets/dashboard/assets/img/account-settings/logo-3.png";
        else if (this.header_data.site_id == 2) imageUrl = environment.baseUrl + "/img/logo_3.svg";
        //const imageUrl = this.img;
        this.headerService.logo_image.next(imageUrl);
        this.url = imageUrl;
        this.logoBgColor = "FFFFFF";
        this.headerBgColor = data.header_background_color;
        this.nav_bg_color = this.logoBgColor;
        this.header_background_color = this.headerBgColor;
        this.colorsLogoService.updateHeaderSession().subscribe((data) => {
        });
      } else {
        this.toastr.ShowToastr('error',data.message);
      }
    },
      error1 => {
        this.toastr.ShowToastr('error',error1);
      });
  }
  hideSaveChangesModal() {
    this.saveChangesModal.hide();
  }

  showSaveChangesModal() {
    this.changesSaved ? this.saveChangesModal.show() : this.router.navigate(["/account-settings"]);
  }

  goBack() {
    this.restorePreviousImg();
    this.changesSaved = false;
  }
  restorePreviousImg() {
    console.log(this.img)
    this.headerService.logo_image.next(this.img);
    this.url = this.img;
    this.colorsLogoService.updateHeaderSession().subscribe((data) => {
    });

  }
  ngOnDestroy(): void {
    // throw new Error("Method not implemented.");
  }
}
