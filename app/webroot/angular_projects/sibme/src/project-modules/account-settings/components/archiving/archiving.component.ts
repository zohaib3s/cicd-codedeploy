import { Component, OnInit, OnDestroy, ViewChild, AfterViewInit, HostListener } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { HttpClient } from '@angular/common/http';
import { ShowToasterService } from '@projectModules/app/services';
import { ArchivingService } from '../../services/archiving.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { BsLocaleService } from "ngx-bootstrap/datepicker";
import { esLocale } from "ngx-bootstrap/locale";
import { defineLocale } from "ngx-bootstrap/chronos";
import * as moment from 'moment';
import { Subscription, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import * as _ from 'lodash';

@Component({
  selector: 'app-archiving',
  templateUrl: './archiving.component.html',
  styleUrls: ['./archiving.component.css'],
  providers: []
})
export class ArchivingComponent implements OnInit, OnDestroy, AfterViewInit {


  public header_data;
  public userObject: any = {};
  public huddles: any = [];
  public unarchivingData: any = null;
  public currentDataObject: any = {};
  public scrolledDataObject: any = {};
  public searchText: Subject<string> = new Subject();
  public searchString = '';
  public date;
  public startDate;
  public endDate;
  public skeletonLoading: boolean = true;
  public searchFound: boolean = false;
  public isCoaching: boolean = false;
  public isCollaboration: boolean = false;
  public isAssessment: boolean = false;
  public isTitle: boolean = true;
  public isDateCreated: boolean = false;
  public isDateModified: boolean = false;
  public onlyHuddles: boolean = true;
  public findSomething: boolean = false;
  public isSelectedAll: boolean = false;
  public isHuddleChecked: boolean = false;
  public isArchived: boolean = false;
  public huddleLength:number = 0;
  public testEelement: any;
  public selectedItems: any = [];
  public prev: String;
  public gs_archiving;
  public showArchiving = false;
  private subscription: Subscription;
  public translations;
  public folder: {
    account_folder_id: any,
    account_id: any,
    folder_type: any,
    parent_folder_id: null,
    name: any,
    children: null,
  };
  public huddle: {
    account_folder_id: any,
    account_id: any,
    folder_type: any,
    parent_folder_id: null,
    name: any,
    total_videos: number,
    total_docs: number,
    last_edit_date: any,
    last_created_date
  };
  public current_state;
  public archivedFolders: folderModel[];
  public list: folderModel[];
  public isIEOpened=false;
  @ViewChild('openPopup', { static: false }) openPopup: ModalDirective;


  @HostListener('window:scroll', ['$event'])

  onScroll() {
    this.huddleLength = this.unarchivingData.total_huddles;
    let currentHuddleLength = this.huddles.length;
    // console.log("current huddle length: ", currentHuddleLength);
    // console.log("total huddles length: ", this.huddleLength);

    // if (((window.innerHeight + window.scrollY) >= document.body.offsetHeight - 2)
    //   && (currentHuddleLength <= this.huddleLength)) {
    //     console.log("IF")
    //   let doc = document.documentElement;
    //   let currentScroll = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
      
    //   if (this.skeletonLoading == false) {
    //     this.skeletonLoading = true;
    //   // window.scroll(0, currentScroll - 1000);
    
    //   this.getNextPageitems();
    //   setTimeout(() => window.scroll(0, currentScroll + 100), 100);
    //   }
    // } 
 if (window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2) { // scroll reached on bottom
      const doc = document.documentElement;
     // console.log("END")
     // console.log(this.skeletonLoading)
      const currentScroll = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
      if (!this.skeletonLoading && currentHuddleLength < this.huddleLength) {
        this.skeletonLoading = true;
         this.userObject.page++;
         this.getNextPageitems();
        setTimeout(() => window.scroll(0, currentScroll + 100), 100);
      }
    }
 

  }

  getNextPageitems() {
    this.userObject.folder_id = '';
    this.archivingService.getUnarchiving(this.userObject).subscribe((data: any) => {
      this.skeletonLoading = false;
      this.huddleLength = data.total_huddles;
      if (data.huddles != undefined && data.huddles.length != 0) {
        data.huddles.forEach(element => {
          this.huddles.push(element);
          if(this.isSelectedAll){
          element.checked=true;
          this.selectedItems.push(element);
           this.selectedItems=[...new Set(this.selectedItems)];
          }
        });
      }

      if (data.folders != undefined && data.folders.length != 0) {
        data.folders.forEach(element => {
          this.list.push(element);
          this.list=[...new Set(this.list)];
        });
      }
    });




  }

  constructor(private archivingService: ArchivingService,
    private headerService: HeaderService, private http: HttpClient,
    private toastr: ShowToasterService, private localeService: BsLocaleService,) {

    this.isIEOpened=this.headerService.isOpenedInIE();
    this.header_data = this.headerService.getStaticHeaderData();
    this.userObject = {
      account_id: this.header_data.user_current_account.users_accounts.account_id,
      user_id: this.header_data.user_current_account.users_accounts.user_id,
      role_id: this.header_data.user_current_account.users_accounts.role_id,
      page: 1,
      user_current_account: this.header_data.user_current_account,
      huddle_type: 0,
      huddle_sort: 0,
      title: '',
      folder_id: '',
      start_date: this.startDate,
      end_date: this.endDate,
      switch: '',

    };

    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
      // this.huddle_data.title = this.translation.huddle_title;
      // this.LoadHuddleTypes();
    });
    let sessionData: any = this.headerService.getStaticHeaderData();
    if (sessionData.language_translation.current_lang == 'es') defineLocale(sessionData.language_translation.current_lang, esLocale);
    this.localeService.use(sessionData.language_translation.current_lang);
  }


  ngOnInit() {
    this.unarchivingData = null;
    //console.log('this.unarchivingData ',this.unarchivingData );
     //this.getUnarchivingData();
    this.setDate();
    this.userObject.start_date = this.startDate;
    this.userObject.end_date = this.endDate;
    this.date=[this.startDate,this.endDate];
    this.getHuddlesOfThisDate(this.date);

  }

  getUnarchived() {

    this.skeletonLoading = true;
    this.startDate = '';
    this.endDate = '';
     this.setDate();
    // this.userObject.start_date = '';
    // this.userObject.end_date = '';
    this.userObject.switch = '';
    this.userObject.folder_id = '';
    this.userObject.huddle_type = 0;
    this.isCoaching = false;
    this.isCollaboration = false;
    this.isAssessment = false;
    this.isArchived = false;
    this.selectedItems = [];
    this.huddles = [];
    this.list = [];
    this.userObject.page = 1;
    this.getUnarchivingData();
    this.onlyHuddles = true;

  }

  getArchived() {
    this.skeletonLoading = true;
    // this.startDate = '';
    // this.endDate = '';
    // this.setDate();
    // this.userObject.start_date = '';
    // this.userObject.end_date = '';
    this.userObject.folder_id = '';
    this.userObject.huddle_type = 0;
    this.userObject.switch = 'archived';
    this.isCoaching = false;
    this.isCollaboration = false;
    this.isAssessment = false;
    this.isArchived = true;
    this.selectedItems = [];
    this.huddles = [];
    this.list = [];
    this.userObject.page = 1;

    this.getUnarchivingData();
    this.onlyHuddles = true;


  }

  getChild(folder, index) {
    // this.list[index].collapsed = !this.list[index].collapsed;
    this.userObject.folder_id = folder.account_folder_id;
    this.archivingService.getUnarchiving(this.userObject).subscribe((data: any) => {
      if (data.folders && data.folders.length > 0) {
        let folders = JSON.stringify(data.folders, ['account_folder_id',
          'account_id',
          'folder_type',
          'parent_folder_id',
          'name','child_count'], 2);
        // console.log('vodasdklnhasdflknsd;fkl', folders);
        let obj = JSON.parse(folders);
        // console.log('child folders', obj);
        let obj2 = obj.map(obj => ({ ...obj, children: [], collapsed: false, checked: false, indeterminate:false }));
        // console.log('second object ', obj2);
        let res = this.FindParent(this.list, obj2[0]);
        if (res !== undefined) {
          res.indeterminate=false;
          res.collapsed = !res.collapsed;
          if (!res.collapsed) {
            res.children = [];
          } else {
            obj2.forEach((item) => {
              res.children.push(item);
            });
            if (data.huddles.length > 0) {
              let hud = data.huddles.map(huddle => ({ ...huddle, children: [], checked: false }));
              // console.log('hduddle ha ye ', hud);

              let res2 = this.FindParent(this.list, hud[0]);
              if (res2 !== undefined) {
                hud.forEach((item) => {
                  res2.children.push(item);
                });
              }
            }
          }
        }

        // this.list = null;
        // this.list = [{"account_folder_id":339684,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"AAAAAAAA","children":[{"account_folder_id":339684,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"AAAAAAAA","children":null}]},{"account_folder_id":278876,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"Test 17 Sep Folder","children":null},{"account_folder_id":204220,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"fwefewfwe","children":null},{"account_folder_id":339669,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"AAA","children":null},{"account_folder_id":339682,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"test24","children":null},{"account_folder_id":339681,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"test 24","children":null},{"account_folder_id":337633,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"Yes I m New","children":null},{"account_folder_id":327196,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"test123","children":null},{"account_folder_id":327186,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"umar folder","children":null},{"account_folder_id":327185,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"umar","children":null},{"account_folder_id":318755,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"Test 14 Nov Folder","children":null},{"account_folder_id":317701,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"Test 14  Nov","children":null},{"account_folder_id":196361,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"ttttt12","children":null},{"account_folder_id":314094,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"123456789","children":null},{"account_folder_id":309244,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"31st OCT Folder","children":null},{"account_folder_id":309178,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"test folder 11111","children":null},{"account_folder_id":244114,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"22 April","children":null},{"account_folder_id":144914,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"test1 987","children":null},{"account_folder_id":269939,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"vsdvsd","children":null},{"account_folder_id":269900,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"jkhkjn132165","children":null},{"account_folder_id":265921,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"ASDASDASD","children":null},{"account_folder_id":257724,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"abc1234567890","children":null},{"account_folder_id":257723,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"new folder","children":null},{"account_folder_id":252968,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"Test 27 May Folder","children":null},{"account_folder_id":225355,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"TEST 11 MARCH","children":null},{"account_folder_id":219215,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"TEST FOLDER  26 FEB","children":null},{"account_folder_id":204278,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"ytytytytytyt","children":null},{"account_folder_id":204199,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"test folder one","children":null},{"account_folder_id":199686,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"abc tested test","children":null},{"account_folder_id":173098,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"test folder1","children":null},{"account_folder_id":166974,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"test folder","children":null},{"account_folder_id":164922,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"group func","children":null},{"account_folder_id":157810,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"Evolution Folder 1","children":null},{"account_folder_id":160443,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"mmmmmmmm","children":null},{"account_folder_id":128519,"account_id":2936,"folder_type":5,"parent_folder_id":null,"name":"htest","children":null}];
        // console.log('ye ha folders', this.list);
        // this.date = this.huddles[0].created_date;
      } else if (data.huddles && data.huddles.length > 0) {
        let hud = data.huddles.map(huddle => ({ ...huddle, children: [], checked: false, indeterminate:false }));
        // console.log('hduddle ha ye ', hud);

        let res2 = this.FindParent(this.list, hud[0]);
        if (res2 !== undefined) {
          res2.collapsed = !res2.collapsed;
          if (!res2.collapsed) {
            res2.children = [];
          } else {
            hud.forEach((item) => {
              res2.children.push(item);
            });
          }
        }
      }
    });
  }


  check(list, el) {
    let newState = !el.checked;
    // console.log(newState);
    this.current_state=newState;
    if (newState) {
      this.selectedItems.push(el);
      this.selectedItems=[...new Set(this.selectedItems)];
      let res = list.find(x => x.account_folder_id === el.account_folder_id);
      this.checkRecursive(res, newState);
      this.IndeterminateRecursive(this.list,el,newState)
    } else {
      this.selectedItems=[...new Set(this.selectedItems)];
      let index=this.selectedItems.indexOf(el);
      if(index>-1) this.selectedItems.splice(index, 1);
      let result = this.selectedItems.find(x => x.account_folder_id === el.account_folder_id);

      if (result) {
        this.selectedItems.splice(this.selectedItems.indexOf(el.account_folder_id), 1);
        for (let item of this.selectedItems) {
          if (item.parent_folder_id === result.account_folder_id) {
            this.selectedItems.splice(this.selectedItems.indexOf(item), 1);
          }
        }
      }

      let res = list.find(x => x.account_folder_id === el.account_folder_id);
      this.checkRecursive(res, newState);

      this.IndeterminateRecursive(this.list,el,newState)
    }
  }

  IndeterminateRecursive(list, el, newState, checkedElement ? ) {
    let item = this.FindParent(list, el);
    if (item) {
        if (newState) {
            let checkedElement = 0;
            item.children.forEach(element => {
                if (element.checked)
                    checkedElement++;
            });
            if (checkedElement == item.child_count) {
                item.checked = newState;
                item.indeterminate = !newState;
                this.selectedItems.push(item);
                if (!item.parent_folder_id) item.indeterminate = !newState;

            } else {

                item.indeterminate = newState;
            }
        } else {
            let checkedElement = 0;
            item.children.forEach(element => {
                if (element.checked)
                    checkedElement++;
            });
            if (checkedElement < item.child_count) {
                item.indeterminate = !newState;
                item.checked = newState;
                if (checkedElement == 0 && item.parent_folder_id) {
                    item.indeterminate = newState;
                    item.checked = newState;
                } else if (checkedElement == 0 && !item.parent_folder_id) {
                    item.indeterminate = newState;
                } else if (checkedElement == item.child_count) {
                    item.indeterminate = newState;
                    item.checked = !newState;
                }
                let index = this.selectedItems.indexOf(item);
                if (index > -1) this.selectedItems.splice(index, 1);
            } else if (checkedElement == item.child_count) {
                item.indeterminate = !newState;
                item.checked = newState;
            }
        }
        this.IndeterminateRecursive(list, item, newState)
    }
}
  checkRecursive(list, state) {
    list.checked = state;
    if(list.folder_type==5) list.indeterminate = false;
    list.children.forEach(d => {
      d.checked = state;
      if(state) {this.selectedItems.push(d);  this.selectedItems=[...new Set(this.selectedItems)];} else this.selectedItems.splice(this.selectedItems.indexOf(d), 1);
      if(d.folder_type==5)   d.indeterminate=false;
      // console.log('this is recursive child', d);
      this.checkRecursive(d, state);
    });

  }

  checkHuddles(el) {
    let newState = !el.checked;
    this.current_state=newState;
    let res = this.huddles.find(x => x.account_folder_id === el.account_folder_id);
    res.checked = newState;
    if (newState) {
      this.selectedItems.push(el);
      this.selectedItems=[...new Set(this.selectedItems)];
    } else {
      this.selectedItems.splice(this.selectedItems.indexOf(el), 1);
    }
    // console.log('this.selectedItems', this.selectedItems);
  }

  FindParent(list, el) {
    if (list && list.length > 0) {
      let res = list.find(x => x.account_folder_id === el.parent_folder_id);
      if (res) {
        return res;
      } else {
        let _res = undefined;
        for (const li of list) {
          _res = this.FindParent(li.children, el);
          if (_res) {
            return _res;
          }
        }
        return _res;
      }
    } else {
      return undefined;
    }
  }


  getUnarchivingData() {
    this.getSearched();
    // this.setDate();

    
      // this.userObject.start_date = this.startDate;
      // this.userObject.end_date = this.endDate;
   
    
    this.userObject.folder_id = '';
    // setTimeout(()=>{
    //   this.skeletonLoading =false;
    // }, 4000);
    //console.log('this is user object', this.userObject);
    // if (this.userObject.title == '') {
      this.archivingService.getUnarchiving(this.userObject).subscribe((data: any) => {
        this.unarchivingData = data;
        this.skeletonLoading = false;
        if (this.findSomething == true) {


          if (data.folders.length == 0 && data.huddles.length == 0 && this.userObject.title != 0) {
            // this.toastr.ShowToastr('info',"No results found matching your search.");
            this.searchFound = true;
          }
          else {
            this.searchFound = false;
          }
          this.findSomething = false;
        }

        // console.log('my Data:');
        // console.log(this.unarchivingData);
        this.huddleLength = data.total_huddles;
        if (data.folders) {
          let folders = JSON.stringify(data.folders, ['account_folder_id',
            'account_id',
            'folder_type',
            'parent_folder_id',
            'name','child_count'], 2);
          //console.log('folder', folders);
          let obj = JSON.parse(folders);
          this.archivedFolders = obj.map(obj => ({ ...obj, children: [], collapsed: false, checked: false,indeterminate:false }));
          this.list = [];
          this.list = obj.map(obj => ({ ...obj, children: [], indeterminate:false }));
          // console.log(JSON.stringify(this.archivedFolders));
          // console.log('ye ha folders', this.archivedFolders);
          // console.log('list', this.list);
        }
        if (data.huddles) {
          this.huddles = data.huddles.map(huddle => ({ ...huddle, checked: false }));
           // console.log('these are the huddles ', this.huddles);
          this.huddleLength = data.total_huddles;
          // console.log('total huddle length', this.huddleLength);

        }
      });
   // }


  }

  archive() {

    let obj = { huddle_ids: [...new Set(this.selectedItems.map(x => x.account_folder_id))] };
    // console.log('so these are the selected item ', obj);
    if(this.selectedItems.length>0)
    {
    this.archivingService.archiveData(obj).subscribe((data:any) => {
    //  console.log(this.current_state);
    this.userObject.page = 1;
    if(data.success){
      this.toastr.ShowToastr('success',this.translations.select_successfully_archive);
      this.getUnarchivingData();
    }

      // console.log(data);
  });
  }
  else{
  this.toastr.ShowToastr('error',this.translations.please_select_huddle_to_archive);
  this.userObject.page=1
}
    this.current_state=false;
  }

  unArchive() {

    let obj = { huddle_ids: this.selectedItems.map(x => x.account_folder_id) };

    // console.log('so these are the selected item for unarchive ', obj);
    this.archivingService.unArchiveData(obj).subscribe((data) => {

      if(this.current_state==true)
      {

       this.toastr.ShowToastr('success',this.translations.select_successfully_unarchive);
       this.userObject.page = 1;
       this.getUnarchivingData();

      }
      else{
        this.toastr.ShowToastr('error',this.translations.please_select_huddle_to_unarchive);

      }
   this.current_state=false;



    });
  }

  selectAll() {
    let newState = !this.isSelectedAll;
    this.isSelectedAll = newState;
    this.current_state=newState;
    this.selectedItems = [];
    if(this.huddles){
      this.huddles.forEach(x => {
        x.checked=newState;
      this.selectedItems.push(x);
      // this.selectedItems=[...new Set(this.selectedItems)];
      });
    }
    if(this.userObject.huddle_type==0){
    this.list.forEach(x => {
      this.checkRecursive(x, newState);
      this.selectedItems.push(x);
      // this.selectedItems=[...new Set(this.selectedItems)];
    });
  }

    if (!newState) {
      this.selectedItems = [];
    }
    // console.log('all items are selected now' + this.selectedItems);
    // this.checkRecursive(this.list, newState);
  }

  getHuddlesOfThisDate(event) {
    let check = Date.parse(event[0]);
    if (check) {
      // this.userObject.start_date = '';
      // this.userObject.end_date = '';
      this.userObject.huddle_type = 0;
      this.userObject.folder_id = '';
      this.list = [];
      this.huddles = [];
      this.userObject.page = 1;
      this.date = event;
      let startDate = moment(this.date[0]).format('D MMM YYYY');
      let endDate = moment(this.date[1]).format('D MMM YYYY');
      this.onlyHuddles = true;
      this.isCoaching = false;
      this.isSelectedAll=false;
      this.isCollaboration = false;
      this.isAssessment = false;
      this.skeletonLoading = true;
      this.userObject.start_date = startDate;
      this.userObject.end_date = endDate;
     // console.log('getHuddlesOfThisDate = > userObject',this.userObject);
      this.getUnarchivingData();
    }
    else {
      return this.toastr.ShowToastr('info',"Date is invalid!")

    }
  }

  setDate() {
    let myDate;
    let date = moment(myDate).format('MM/DD/YYYY');
    let today = moment();
    let lastYear = moment().subtract(1, 'year')
      .isoWeek(today.isoWeek())
      .isoWeekday(today.isoWeekday());
    let formattedLastYear = moment().subtract(1, 'year').format('MM/DD/YYYY');
    this.startDate = formattedLastYear;
   
    this.endDate = date;
    console.log("date are ->", this.startDate,this.endDate,this.date)
  }

  findHuddle(event) {
    this.skeletonLoading = true;
    this.userObject.huddle_type = 0;
    // this.startDate = '';
    // this.endDate = '';
    this.findSomething = true
    // this.userObject.start_date = '';
    // this.userObject.end_date = '';
    this.userObject.folder_id = '';
    this.list = [];
    this.huddles = [];
    this.userObject.page = 1;
    this.isCoaching = false;
    this.isSelectedAll=false;
    this.isCollaboration = false;
    this.isAssessment = false;
    this.onlyHuddles = true;
    this.searchText.next(event);

  }

  private getSearched() {
    this.searchText
      .pipe(
        debounceTime(2000),
        distinctUntilChanged()
      )
      .subscribe(value => {
        if (this.prev !== value) {
          this.prev = value;
          this.userObject.title = value;
          this.getUnarchivingData();
        }

      });

  }

  getCoachingHuddles() {
    this.skeletonLoading = true;
    this.onlyHuddles = false;
    this.isCoaching = true;
    this.isSelectedAll=false;
    this.isCollaboration = false;
    this.isAssessment = false;
    this.userObject.huddle_type = 2;
    this.list = [];
    this.huddles = [];
    this.userObject.page = 1;

    this.getUnarchivingData();


  }

  getCollaborationHuddles() {
    this.skeletonLoading = true;

    this.isCollaboration = true;
    this.isSelectedAll=false;
    this.isCoaching = false;
    this.isAssessment = false;
    this.onlyHuddles = false;

    this.userObject.huddle_type = 1;
    this.list = [];
    this.huddles = [];
    this.userObject.page = 1;


    this.getUnarchivingData();


  }


  getAssessmentHuddles() {
    this.skeletonLoading = true;

    this.isAssessment = true;
    this.isSelectedAll=false;
    this.isCoaching = false;
    this.isCollaboration = false;
    this.onlyHuddles = false;
    this.userObject.huddle_type = 3;
    this.list = [];
    this.huddles = [];
    this.userObject.page = 1;


    this.getUnarchivingData();


  }

  getAllHuddles() {
    this.skeletonLoading = true;
    this.userObject.huddle_type = 0;
    this.isAssessment = false;
    this.isCoaching = false;
    this.isCollaboration = false;
    this.onlyHuddles = true;
    this.list = [];
    this.huddles = [];
    this.userObject.page = 1;
    this.getUnarchivingData();
  }


  closeModal() {
    this.openPopup.hide();
  }

  sortByTitle() {
    this.skeletonLoading = true;

    this.isDateCreated = false;
    this.isDateModified = false;
    this.isTitle = true;
    this.userObject.huddle_sort = 0;
    this.list = [];
    this.huddles = [];
    this.userObject.page = 1;


    this.getUnarchivingData();


  }

  sortBYDateCreated() {
    this.skeletonLoading = true;

    this.isTitle = false;
    this.isDateModified = false;
    this.isDateCreated = true;
    this.userObject.huddle_sort = 2;
    this.list = [];
    this.huddles = [];
    this.userObject.page = 1;


    this.getUnarchivingData();


  }

  sortByDateModified() {
    this.skeletonLoading = true;

    this.isTitle = false;
    this.isDateCreated = false;
    this.isDateModified = true;
    this.userObject.huddle_sort = 3;
    this.list = [];
    this.huddles = [];
    this.userObject.page = 1;

    this.getUnarchivingData();


  }

  ngAfterViewInit() {

    this.archivingService.getUnarchiving(this.userObject).subscribe((data: any) => {
      this.gs_archiving = data.gs_archiving;
      if (this.gs_archiving == 1) {
        this.openPopup.show();
      }
    });
  }


  ngOnDestroy() {
  }

  boxChecked(event) {
    this.showArchiving = event;
  }

  archivingModal() {
    if (this.showArchiving) {
      const obj = {
        user_id: this.userObject.user_id
      };
      this.archivingService.archivingModalUpdate(obj).subscribe((data: any) => {
        // this.toastr.ShowToastr('success',data.message);
      }, error1 => {
        console.log(error1);
      });
    }
    this.closeModal();
  }

}


class folderModel {
  public account_folder_id: number;
  public account_id: number;
  public folder_type: number;
  public parent_folder_id: any;
  public name: string;
  public children: folderModel[];
  public collapsed: boolean;
  public checked: boolean;
  public child_count:number;
  public indeterminate:boolean;
}
