import {Component, OnInit, ViewChild, OnDestroy, TemplateRef} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {HeaderService} from '@src/project-modules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import {GeneralSettingsService} from '../../services/general-settings.service';
import {environment} from '@src/environments/environment';
import {ModalDirective, BsModalRef, BsModalService} from 'ngx-bootstrap/modal';
import * as _ from 'lodash';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-general-settings',
  templateUrl: './general-settings.component.html',
  styleUrls: ['./general-settings.component.css']
})
export class GeneralSettingsComponent implements OnInit, OnDestroy {


  public header_data;
  public translations;
  public userObject: any = {};
  public generalSettingsObject: any = {};
  public skelton_loading: boolean = true;
  public accountName = '';
  public site_id = '';
  public analyticsDuration;
  public image = '';
  public default_framework = '';
  public usersPrivileges: any = {};
  public frameworks: any = {};
  public custumMarkChecked: boolean;
  public assessmentRatingsChecked: boolean;
  public updatedRatingName = '';
  public currentValue: any;
  public currentMark: any;
  public targetId: any;
  public currentratingId: any;
  public ratingValues = [1, 2, 3, 4, 5];
  public assessment_ratings = [];
  public custom_marker_tags = [];
  public owners = [];
  public ownersArray = [];
  public newAccountOwnerId: number;
  public newAccountOwnerName: any;
  public proFeatures: boolean;
  public metric_in_account_settings:boolean;
  public accountOwnerId: any;
  public analyticsDurationList: any = [{key: 12, value: '12 Hours'},
  {key: 24, value: '24 Hours'},
  {key: 36, value: '36 Hours'},
  {key: 48, value: '48 Hours'},
  {key: 60, value: '60 Hours'},
  {key: 72, value: '72 Hours'},
  {key: 120, value: '5 Days'},
  {key: 168, value: '1 Week'},
  {key: 336, value: '2 Weeks'}];
  public changedAccountOwner: any = '';
  public change_account_owner_check:boolean = false;
  private subscription: Subscription;
  modalRef: BsModalRef;
  private users_count;
  private admins_count;
  private tempSettingObj: any = {};
  public isEditMode= false;
  public settingsModalObj: any ={};

  @ViewChild('customMarker', {static: false}) customMarker: ModalDirective;
  @ViewChild('assessmentRatings', {static: false}) assessmentRatings: ModalDirective;
  @ViewChild('changeAccount', {static: false}) changeAccount: ModalDirective;
  @ViewChild('no_user_in_account', {static: false}) template;
  @ViewChild('forced_SAML_alert', {static: false}) forced_SAML_alert;


  constructor(private generalSettingsService: GeneralSettingsService,
              private headerService: HeaderService, private http: HttpClient,
              private toastr: ShowToasterService, private router: Router, private modalService: BsModalService,
              private bsModalService: BsModalService) {


    this.header_data = this.headerService.getStaticHeaderData();
    

    this.userObject = {
      account_id: this.header_data.user_current_account.users_accounts.account_id,
      user_id: this.header_data.user_current_account.users_accounts.user_id,
    };

    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
      // this.huddle_data.title = this.translation.huddle_title;
      // this.LoadHuddleTypes();
    });

  }

  ngOnInit() {
    this.getGeneralAccountSettings();
  }

  getGeneralAccountSettings() {
    this.proChecks();

    this.generalSettingsService.GetGeneralAccountSettings(this.userObject.account_id, this.userObject.user_id).subscribe((data: any) => {
        if (data.success) {
          this.users_count=data.users_count
          this.admins_count =data.admins_count;
          this.generalSettingsObject = data;
          this.tempSettingObj = _.cloneDeep(this.generalSettingsObject);
          this.skelton_loading = false;
          this.accountName = this.generalSettingsObject.account_detail.company_name;
          this.site_id = this.generalSettingsObject.account_detail.site_id;
          this.analyticsDuration = this.generalSettingsObject.get_analytics_duration_value;
          this.getProfileImage();
          this.custumMarkChecked = this.generalSettingsObject.enable_custom_markers_in_account;
          this.assessmentRatingsChecked = this.generalSettingsObject.enable_assessment_tracker;
          this.custom_marker_tags = _.cloneDeep(this.generalSettingsObject.custom_marker_tags);
          this.assessment_ratings = _.cloneDeep(this.generalSettingsObject.assessment_ratings);
          this.accountOwnerId = this.generalSettingsObject.account_owner_id;
          this.headerService.libraryPermissionSubject.next(this.generalSettingsObject.enable_video_library ? 1 : 0);
          this.headerService.analyticsPermissionSubject.next(this.generalSettingsObject.enable_analytics ? 1 : 0);
          this.headerService.accountNameSubject.next(this.generalSettingsObject.account_detail.company_name);

          this.accountOwnerId = this.generalSettingsObject.account_owner_id;
          this.owners = _.cloneDeep(this.generalSettingsObject.account_owner_list);

          this.checkingCustomMarkers();
          this.checkRatings();
          // let defaultFramework = this.generalSettingsObject.frameworks_list.filter((item) => item.account_tag_id == this.generalSettingsObject.default_framework);
          this.default_framework = this.generalSettingsObject.default_framework;


        } else {
          this.toastr.ShowToastr('error',data.message);
          this.router.navigate(['/dashboard_angular/home']);
        }
      }
    );


  }

  public confirmForceDelete(){
    this.generalSettingsObject.force_auto_delete = true;
  }

  updateAccountfeatures(event, modal?) {

    for (const item in this.generalSettingsObject) {
      if (event.target.name === item) {
        if (event.target.checked) {
          if (item == "is_evaluation_activated" || item == "assessment_perfomance_level"){
            this.generalSettingsObject[item] = 1;
          } else if (item === 'force_auto_delete'){
            event.target.checked = false;
            this.modalRef = this.bsModalService.show(modal, { class: "modal-md  maxcls", backdrop: 'static' });
          }
          else{
            this.generalSettingsObject[item] = true;
          }
        } else {
          // console.log('After uncheck value:');
          if (item == "is_evaluation_activated" || item == "assessment_perfomance_level") {
            this.generalSettingsObject[item] = 0;
          }
          else{
          this.generalSettingsObject[item] = false;
          }
          // console.log(this.generalSettingsObject[item]);
        }
      }
    }

    if (this.generalSettingsObject.enable_custom_markers_in_account === true) {
      this.custumMarkChecked = true;
    } else {
      this.custumMarkChecked = false;

    }

    if (this.generalSettingsObject.enable_assessment_tracker === true) {
      this.assessmentRatingsChecked = true;
    } else {
      this.assessmentRatingsChecked = false;

    }

  }

  getProfileImage() {
    if (this.generalSettingsObject.user_detail.image != null) {
      this.image = environment.imageBaseUrl + '/' + this.userObject.user_id + '/' + this.generalSettingsObject.user_detail.image;
    }
  }

  updateAccountUserPrivileges(event) {

    for (let item in this.generalSettingsObject.users_privileges_check) {
      if (event.target.name === item) {
        console.log(item);

        if (event.target.checked) {
          console.log(this.generalSettingsObject.users_privileges_check[item]);
          console.log('After check value:');

          this.generalSettingsObject.users_privileges_check[item] = true;
          console.log(this.generalSettingsObject.users_privileges_check[item]);
          this.generalSettingsObject.enable_goals = 1;
        } else {
          console.log('After uncheck value:');

          this.generalSettingsObject.users_privileges_check[item] = false;
          this.generalSettingsObject.enable_goals = 0;
          console.log(this.generalSettingsObject.users_privileges_check[item]);
        }
      }
    }
  }

  updateAccountAdminPrivileges(event) {

    for (let item in this.generalSettingsObject.admins_privileges_check) {
      if (event.target.name === item) {
        console.log(item);

        if (event.target.checked) {
          console.log('value entered in checked:');
          console.log('value before change:');

          console.log(this.generalSettingsObject.admins_privileges_check[item]);
          console.log('After check value:');
          this.generalSettingsObject.admins_privileges_check[item] = true;
          console.log(this.generalSettingsObject.admins_privileges_check[item]);
        } else {
          console.log('value entered in unchecked:');
          console.log('value before change:');
          console.log(this.generalSettingsObject.admins_privileges_check[item]);

          console.log('After uncheck value:');
          this.generalSettingsObject.admins_privileges_check[item] = false;
          console.log(this.generalSettingsObject.admins_privileges_check[item]);
        }
      }
    }
  }

  selectDefaultFramework(framework: any) {
    let defaultFramework = this.generalSettingsObject.frameworks_list.filter((item) => item.account_tag_id == framework.target.value);
    this.default_framework = defaultFramework[0].account_tag_id;
    console.log(this.default_framework);
  }

  editRating(value) {
    this.currentValue = value;
    console.log(this.currentValue);
  }

  editMarking(value) {
    this.currentMark = value;
  }

  async validateSSOFields() {
    let urlPattern = new RegExp('^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$', 'i');
    if (this.generalSettingsObject.account_detail.is_saml_enabled) {
      if (!this.generalSettingsObject.account_detail.sso_url) {
        this.toastr.ShowToastr('error',this.translations.sso_url_cannot_empty);
        return false;
      } else {
        if (!urlPattern.test(this.generalSettingsObject.account_detail.sso_url)) {
          this.toastr.ShowToastr('error',this.translations.sso_url_invalid);
          return false;
        }
      }
      if (!this.generalSettingsObject.account_detail.sso_certificate) {
        this.toastr.ShowToastr('error',this.translations.sso_certificate_empty);
        return false;
      }
      if (!this.generalSettingsObject.account_detail.idp_entityId) {
        this.toastr.ShowToastr('error',this.translations.idp_id_cannot_empty);
        return false;
      }
    }
    return true;
  }

  async updateChanges() {
    let isSSOFieldsValid = await this.validateSSOFields();
    if (!isSSOFieldsValid) {
      return false;
    }

    // if(!this.generalSettingsObject.enable_analytics_enableleaderboard) {
    //   this.generalSettingsObject.admins_privileges_check.admins_can_view_leaderboard = false;
    // }
    // console.log(this.generalSettingsObject);
    // return false;
    let Obj = {
      is_saml_enabled: this.generalSettingsObject.account_detail.is_saml_enabled, //SSO
      sso_url: (this.generalSettingsObject.account_detail.sso_url) ? this.generalSettingsObject.account_detail.sso_url : '', //SSO
      sso_certificate: (this.generalSettingsObject.account_detail.sso_certificate) ? this.generalSettingsObject.account_detail.sso_certificate : '', //SSO
      is_force_sso_enabled: this.generalSettingsObject.account_detail.is_force_sso_enabled, //SSO
      idp_entityId: (this.generalSettingsObject.account_detail.idp_entityId) ? this.generalSettingsObject.account_detail.idp_entityId : '', //SSO
      default_account: this.generalSettingsObject.default_account,
      company_name: this.generalSettingsObject.account_detail.company_name,
      account_id: this.userObject.account_id,
      user_id: this.userObject.user_id,
      get_analytics_duration_value: this.analyticsDuration,
      enable_video_library: this.generalSettingsObject.enable_video_library,
      enable_analytics: this.generalSettingsObject.enable_analytics,
      enable_analytics_enableleaderboard: this.generalSettingsObject.enable_analytics_enableleaderboard,
      enable_assessment_tracker: this.generalSettingsObject.enable_assessment_tracker,
      enable_coaching_tracker: this.generalSettingsObject.enable_coaching_tracker,
      default_framework: this.default_framework,
      enable_custom_markers_in_account: this.generalSettingsObject.enable_custom_markers_in_account,
      enable_custom_markers_in_workspace: this.generalSettingsObject.enable_custom_markers_in_workspace,
      enable_framework_in_account: this.generalSettingsObject.enable_framework_in_account,
      enable_framework_in_workspace: this.generalSettingsObject.enable_framework_in_workspace,
      is_evaluation_activated: this.generalSettingsObject.is_evaluation_activated,
      assessment_perfomance_level: this.generalSettingsObject.assessment_perfomance_level,
      custom_marker_tags: this.custom_marker_tags,
      assessment_ratings: this.assessment_ratings,
      enable_live_rec:this.generalSettingsObject.enable_live_rec,
      transcribe_library_videos:this.generalSettingsObject.transcribe_library_videos,
      enable_goals: this.generalSettingsObject.enable_goals,
      transcribe_workspace_videos : this.generalSettingsObject.transcribe_workspace_videos,
      transcribe_huddle_videos : this.generalSettingsObject.transcribe_huddle_videos,
      // assessment_ratings: this.generalSettingsObject.assessment_ratings,
      allow_auto_delete_on_mobile: this.generalSettingsObject.allow_auto_delete_on_mobile,
      force_auto_delete: this.generalSettingsObject.force_auto_delete,

      users_privileges_check: {
        folders_check: this.generalSettingsObject.users_privileges_check.folders_check,
        permission_access_video_library: this.generalSettingsObject.users_privileges_check.permission_access_video_library,
        permission_video_library_upload: this.generalSettingsObject.users_privileges_check.permission_video_library_upload,
        permission_administrator_user_new_role: this.generalSettingsObject.users_privileges_check.permission_administrator_user_new_role,
        parmission_access_my_workspace: this.generalSettingsObject.users_privileges_check.parmission_access_my_workspace,
        manage_collab_huddles: this.generalSettingsObject.users_privileges_check.manage_collab_huddles,
        manage_coach_huddles: this.generalSettingsObject.users_privileges_check.manage_coach_huddles,
        manage_evaluation_huddles: this.generalSettingsObject.users_privileges_check.manage_evaluation_huddles,
        permission_view_analytics: this.generalSettingsObject.users_privileges_check.permission_view_analytics,
        huddle_to_workspace: this.generalSettingsObject.users_privileges_check.huddle_to_workspace,
        permission_start_synced_scripted_notes: this.generalSettingsObject.users_privileges_check.permission_start_synced_scripted_notes,
        permission_video_library_download: this.generalSettingsObject.users_privileges_check.permission_video_library_download,
        permission_video_library_comments: this.generalSettingsObject.users_privileges_check.permission_video_library_comments,
        permission_share_library: this.generalSettingsObject.users_privileges_check.permission_share_library,
      },
      admins_privileges_check: {
        admin_permission_video_library_upload: this.generalSettingsObject.admins_privileges_check.admin_permission_video_library_upload,
        admin_permission_administrator_user_new_role: this.generalSettingsObject.admins_privileges_check.admin_permission_administrator_user_new_role,
        admin_view_analytics: this.generalSettingsObject.admins_privileges_check.admin_view_analytics,
        admins_can_view_leaderboard: this.generalSettingsObject.enable_analytics_enableleaderboard ?  this.generalSettingsObject.admins_privileges_check.admins_can_view_leaderboard :false,
        admin_permission_video_library_download: this.generalSettingsObject.admins_privileges_check.admin_permission_video_library_download,
        admin_permission_video_library_comments: this.generalSettingsObject.admins_privileges_check.admin_permission_video_library_comments,
        admin_permission_access_video_library: this.generalSettingsObject.admins_privileges_check.admin_permission_access_video_library,
        admin_permission_share_library: this.generalSettingsObject.admins_privileges_check.admin_permission_share_library,
      },
    };
    if (!Obj.enable_assessment_tracker) {
      Obj.assessment_ratings = _.cloneDeep(this.generalSettingsObject.assessment_ratings);
    }
    if (!Obj.enable_custom_markers_in_account) {
      Obj.custom_marker_tags = _.cloneDeep(this.generalSettingsObject.custom_marker_tags);
    }

    if (Obj.company_name.trim() == '') {

      this.toastr.ShowToastr('error','Account Name cannot be empty!');
    } else{
      let users_privileges_check =_.isEqual(Obj.users_privileges_check, this.tempSettingObj.users_privileges_check);
      let admins_privileges_check =_.isEqual(Obj.admins_privileges_check, this.tempSettingObj.admins_privileges_check);
      let is_force_sso_enabled = this.tempSettingObj.account_detail.is_force_sso_enabled;
      if((this.users_count===0 && !users_privileges_check) || (this.admins_count===0 && !admins_privileges_check)){
        this.showNoUserModal(this.template);
      }

        if(!this.generalSettingsObject.account_detail.is_force_sso_enabled || (this.generalSettingsObject.account_detail.is_force_sso_enabled && is_force_sso_enabled == this.generalSettingsObject.account_detail.is_force_sso_enabled))
        this.updateSetting(Obj)
        if(is_force_sso_enabled!= this.generalSettingsObject.account_detail.is_force_sso_enabled && this.generalSettingsObject.account_detail.is_force_sso_enabled){
          this.settingsModalObj = Obj;
          this.showNoUserModal(this.forced_SAML_alert);
        }
    }
  }

  updateSetting(Obj){
    this.generalSettingsService.updateGeneralSettings(Obj).subscribe((data: any) => {
      this.getGeneralAccountSettings();
      if (data.success) {
        this.toastr.ShowToastr('success',this.translations.general_setting_updated_new_settings);
        if(data.is_logout_required_for_sso){
          // window.location.href = environment.appPath + '/users/logout';
          //In case of SSO logout
          this.headerService.logoutUser().subscribe((data: any) => {
            if(this.headerService.getLocalStorage("auth_token")){        //Removing Auth Token if remember me is selected
              this.headerService.removeLocalStorage("auth_token")
            }
            this.router.navigate([data.redirect_to])
          });
        } else {
          this.generalSettingsService.updateHeaderSession().subscribe((data) => {
          });
          this.headerService.getStaticHeaderData();
          // this.toastr.ShowToastr('info',data.message);
          // this.toastr.ShowToastr('success',this.translations.general_setting_updated_new_settings);
        }

      }
    }, error => {
      this.toastr.ShowToastr('error','General Settings could not updated.');
    });


  }
  showNoUserModal(template :TemplateRef<any>){
    this.modalRef = this.modalService.show(template, { class: 'modal-container-600' });

  }

  showCustomMarker(): void {
    this.customMarker.show();
  }

  hideCustomMarker(): void {
    this.customMarker.hide();
  }

  cancelCustomMarker(): void {
    this.customMarker.hide();
    this.isEditMode=false;
    this.custom_marker_tags = _.cloneDeep(this.generalSettingsObject.custom_marker_tags);
    this.checkingCustomMarkers();
    this.currentMark = 876;
  }

  assessmentEditingDone() {
    this.currentValue = 88;

  }

  customMarkEditingDone() {
    this.currentMark = 99;
    

  }

  getChangedRatingName(event) {
    // this.updatedRatingName = event.target.value;
    // this.targetId = event.target.id;
    // console.log(this.updatedRatingName);
    // console.log(this.targetId);

    this.assessment_ratings.forEach(element => {
      if (element.meta_data_value == event.target.id) {
        if (element.fake) {

          element.rating_name = event.target.value;
          element.is_new = true;
          element.is_delete = false;
          element.is_update = false;
        } else {
          element.rating_name = event.target.value;
          element.is_new = false;
          element.is_delete = false;
          element.is_update = true;
        }
        console.log('Rating name:');
        console.log(element.rating_name);
      }
    });


  }

  getChangedMarker(event) {
    this.custom_marker_tags.forEach(element => {
      console.log(event.target);
      if (element.account_tag_id == event.target.id) {
        if (element.fake) {

          element.tag_title = event.target.value;
          element.is_new = true;
          element.is_delete = false;
          element.is_update = false;
        } else {

          element.tag_title = event.target.value;
          element.is_new = false;
          element.is_delete = false;
          element.is_update = true;
        }
        console.log('Rating name:');
        console.log(element.tag_title);
      }
    });


  }

  saveChangedRatings() {
    this.assessmentRatings.hide();
    this.currentValue = 87597;
    this.updateChanges();
  }

  saveChangedMarkings() {
    this.customMarker.hide();
    this.currentMark = 98685;
    this.updateChanges();
  }

  checkRatings() {
    let ratingIds = this.assessment_ratings.map(x => x.meta_data_value);
    // let result = this.assessment_ratings.filter(e => !this.ratingValues.includes(e));
    let result = this.getArrayDifference(this.ratingValues, ratingIds);
    if (result) {
      result.forEach((x) => {
        this.assessment_ratings.push(
          {
            meta_data_value: x,
            rating_name: '',
            is_new: false,
            is_update: false,
            is_delete: false,
            fake: true
          });
      });
    }
    this.assessment_ratings.sort((a, b) => {
      return a.meta_data_value - b.meta_data_value;
    });
    this.checkingCustomMarkers();
  }

  checkingCustomMarkers() {
    let size = this.custom_marker_tags.length;
    if (size < 4) {
      size = 4 - size;
      for (let i = 0; i < size; i++) {
        this.custom_marker_tags.push(
          {
            tag_title: '',
            is_new: false,
            is_update: false,
            is_delete: false,
            fake: true,
            account_tag_id: i
          });
      }
    }
  }

  getArrayDifference(a1, a2) {
    let a = [], diff = [];
    for (let i = 0; i < a1.length; i++) {
      a[a1[i]] = true;
    }
    for (let i = 0; i < a2.length; i++) {
      if (a[a2[i]]) {
        delete a[a2[i]];
      } else {
        a[a2[i]] = true;
      }
    }
    for (let k in a) {
      diff.push(k);
    }
    return diff;
  }

  deleteRating(value) {
    this.currentratingId = value;
    this.assessment_ratings.forEach(element => {
      if (element.meta_data_value == value) {
        element.rating_name = '';
        element.is_delete = true;
      }

    });

  }

  deleteMarking(value) {
    this.custom_marker_tags.forEach(element => {
      if (element.account_tag_id == value) {
        element.tag_title = '';
        element.is_new = false;
        element.is_delete = true;
        element.is_update = false;
        console.log('Mark name:');
        console.log(element.tag_title);
      }
    });
  }

  showAssessmentRatings(): void {
    this.assessmentRatings.show();
  }

  hideAssessmentRatings(): void {
    this.assessmentRatings.hide();
  }

  cancelAssessmentRatings(): void {
    this.assessmentRatings.hide();
    this.isEditMode=false;
    this.assessment_ratings = _.cloneDeep(this.generalSettingsObject.assessment_ratings);
    this.checkRatings();
    this.currentValue = 8365;
  }

  changeAccountOwner(ownerId) {
    this.newAccountOwnerId = ownerId.target.value;
    Object.keys(this.owners).forEach(key => {
      if (key == ownerId.target.value) {
        let splittedName = this.owners[key].split("(");
        this.changedAccountOwner = splittedName[0] ? splittedName[0].trim() :splittedName[0];
        console.log('changed Account Owner');
        console.log(this.changedAccountOwner);
      }
    });
  }

  changeNewAccountOwner() {
    let accountOwnerName = this.changedAccountOwner.replace(" (Super Admin)","");
    let project_title = environment.project_title;
    let base_url = environment.baseHeaderUrl + `/angular_header_api/${project_title}`;
    let obj = {
      account_owner_user_id: this.accountOwnerId,
      user_id: this.newAccountOwnerId,
      account_id: this.userObject.account_id
    } as any;
    this.generalSettingsService.changeAccountOwner(obj).subscribe((data: any) => {
      if (data.success) {
        this.toastr.ShowToastr('success',data.message);
        // this.showChangeAccount();
        this.generalSettingsService.updateHeaderSession().subscribe((data) => {
          console.log('success');
          localStorage.setItem('newAccountOwner', accountOwnerName);
          this.headerService.getHeaderData(base_url).subscribe((res: any)=> {
            this.headerService.account_change.emit(res)
          });
          setTimeout(() => {
            this.router.navigate(['/']);
          }, 2000);

        });
      }
      else{
        this.toastr.ShowToastr('error',data.message);
      }
    }, error => {
      if (obj.user_id == undefined || obj.account_owner_user_id == obj.user_id) {
      this.toastr.ShowToastr('error',"You are already Account owner");

      }
      else{
        this.toastr.ShowToastr('error',"Oops! Something went wrong");

      }

    });
  }

  getSelectedId(id) {
    this.newAccountOwnerId = id;
    console.log(id);
  }

  redirectToHome() {
    //  this.router.navigate(['/dashboard_angular/home']);
    window.location.href = '/dashboard_angular/home';

  }

  showChangeAccount() {
    this.changeAccount.show();
  }

  cancelChanges() {
    this.getGeneralAccountSettings();
    console.log('cancelled');
  }

  proChecks()
  {
    let base_url = environment.baseHeaderUrl + `/angular_header_api/huddle_list`;
    this.headerService.getHeaderData(base_url).subscribe((res: any)=> {
      // console.log('general setting component');
      // debugger;
    if (this.proFeatures == undefined) {

    this.proFeatures = res.pro_features;
    this.metric_in_account_settings = res.metric_in_account_settings;
  }
  });
  }

  public changeAccountCheck()
  {
      this.change_account_owner_check = true;
  }

  public changeNewAccountOwnerBack()
  {
    this.change_account_owner_check = false;
  }

  changeGoalsPremission(event){
    if (event.target.checked) {
      this.generalSettingsObject.enable_goals = 1;
    }
    else{
      this.generalSettingsObject.enable_goals = 0;
    }
  }

  checkForcedSSO(){
    if(this.generalSettingsObject.account_detail.is_saml_enabled){
      this.generalSettingsObject.account_detail.is_force_sso_enabled = false;
    }

  }


  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
