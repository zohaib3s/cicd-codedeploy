import { Component, OnInit, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { HttpClient } from '@angular/common/http';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { GoalSettingsService } from '../../services/goals-settings.service';
import {ModalDirective} from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-goals-settings',
  templateUrl: './goals-settings.component.html',
  styleUrls: ['./goals-settings.component.css']
})
export class GoalsSettings implements OnInit, OnDestroy {

  private subscription: Subscription;
  public checkGoal: any;
  public header_data;
  public translations;
  public userObject: any = {};
  public goalsSettingsObject: any = {};
  public usersPrivilegesObject: any = {};
  public adminsPrivilegesObject: any = {};
  public skelton_loading: boolean = true;
  public makeAllGoalsPublic: any;
  public accountTypeGoalsPublic: any;
  public enableMeasurement: any;
  public enablePerformanceLevel: any;
  public groupTypeGoalsPublic: any;
  public individualTypeGoalsPublic: any;
  public goalAltName: any;
  changeCount: number = 0;
  viewMode = 'general';
  tabClick= 'general';

  public currentGoalsPublicKey: string;
  private goalsPublicKeys: any = {
    general: {
      all: 'make_all_goals_public',
      group: 'group_type_goals_public',
      individual: 'individual_type_goals_public',
      account: 'account_type_goals_public'
    },
    admin_super_owner: {
      all: 'make_all_goals_public_admin_super_owner',
      group: 'group_type_goals_public_admin_super_owner',
      individual: 'individual_type_goals_public_admin_super_owner',
      account: 'account_type_goals_public_admin_super_owner'
    },
    super_owner: {
      all: 'make_all_goals_public_super_owner',
      group: 'group_type_goals_public_super_owner',
      individual: 'individual_type_goals_public_super_owner',
      account: 'account_type_goals_public_super_owner'
    },
  }

  @ViewChild('makeAllGoalPublicModal', {static: false}) makeAllGoalPublicModal: ModalDirective;
  @ViewChild('switchTabwithoutChange', {static: false}) switchTabwithoutChange: ModalDirective;

  constructor(public headerService: HeaderService, private http: HttpClient,
    private toastr: ShowToasterService, private router: Router, private goalService: GoalSettingsService) {

    this.header_data = this.headerService.getStaticHeaderData();
    this.checkGoal = this.header_data.user_current_account.accounts.enable_goals;

    this.userObject = {
      account_id: this.header_data.user_current_account.users_accounts.account_id,
      user_id: this.header_data.user_current_account.users_accounts.user_id,
    };

    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
      // console.log("this,translatons: ",this.translations);
      // this.huddle_data.title = this.translation.huddle_title;
      // this.LoadHuddleTypes();
    });
  }

  ngOnInit() {
    this.goalSettings();
  }

  goalSettings() {

    this.goalService.GetGoalSettings(this.userObject).subscribe((data: any) => {
      if (data.success) {
        this.goalsSettingsObject = data;
        this.goalAltName = this.goalsSettingsObject.data.goal_alt_name;
        this.usersPrivilegesObject = this.goalsSettingsObject.users_privileges_check;
        this.adminsPrivilegesObject = this.goalsSettingsObject.admins_privileges_check;
        // if (this.goalsSettingsObject.data.enable_measurement === 1) {
        //   this.goalsSettingsObject.data.enable_measurement = true;
        // }
        // if (this.goalsSettingsObject.data.enable_performance_level === 1) {
        //   this.goalsSettingsObject.data.enable_performance_level = true;
        // }
        // console.log('Goal data===',this.goalsSettingsObject);

        this.skelton_loading = false;
      }
      else {
        this.toastr.ShowToastr('error',data.message);
        this.router.navigate(['/dashboard_angular/home']);
      }
    });
  }

  adminsPrivileges(event) {
    for (let item in this.goalsSettingsObject.admins_privileges_check) {
      if (event.target.name === item) {
        if (event.target.checked) {
          this.goalsSettingsObject.admins_privileges_check[item] = true;
        } else {
          this.goalsSettingsObject.admins_privileges_check[item] = false;
        }
      }
    }
  }

  userPrivileges(event){
    for (let item in this.goalsSettingsObject.users_privileges_check) {
      if (event.target.name === item) {
        if (event.target.checked) {
          this.goalsSettingsObject.users_privileges_check[item] = true;
        } else {
          this.goalsSettingsObject.users_privileges_check[item] = false;
        }
      }
    }
  }
  /**On click of tab */
  changeTab(tab){
    if(this.changeCount > 0){
      this.switchTabwithoutChange.show();
      this.tabClick =tab;
      return;
    }
    this.viewMode =tab;

  }
  /**Switch tab via model alert */
  switchTab(change?){
    this.switchTabwithoutChange.hide();
    if (change){
       this.viewMode = this.tabClick;
       this.changeCount = 0;
  }
}
  saveChanges(viewMode){
    let obj = {
      account_id: this.userObject.account_id,
      user_id: this.userObject.user_id,
      opened_tab: viewMode
    }
    if(viewMode == 'general'){
      obj['make_all_goals_public']= this.goalsSettingsObject.data.make_all_goals_public;
      obj['group_type_goals_public']= this.goalsSettingsObject.data.group_type_goals_public;
      obj['individual_type_goals_public']= this.goalsSettingsObject.data.individual_type_goals_public;
      obj['account_type_goals_public']= this.goalsSettingsObject.data.account_type_goals_public;

      obj['make_all_goals_public_admin_super_owner']= this.goalsSettingsObject.data.make_all_goals_public_admin_super_owner;
      obj['group_type_goals_public_admin_super_owner']= this.goalsSettingsObject.data.group_type_goals_public_admin_super_owner;
      obj['individual_type_goals_public_admin_super_owner']= this.goalsSettingsObject.data.individual_type_goals_public_admin_super_owner;
      obj['account_type_goals_public_admin_super_owner']= this.goalsSettingsObject.data.account_type_goals_public_admin_super_owner;

      obj['make_all_goals_public_super_owner']= this.goalsSettingsObject.data.make_all_goals_public_super_owner;
      obj['group_type_goals_public_super_owner']= this.goalsSettingsObject.data.group_type_goals_public_super_owner;
      obj['individual_type_goals_public_super_owner']= this.goalsSettingsObject.data.individual_type_goals_public_super_owner;
      obj['account_type_goals_public_super_owner']= this.goalsSettingsObject.data.account_type_goals_public_super_owner;

      obj['enable_measurement']= this.goalsSettingsObject.data.enable_measurement;
      obj['enable_performance_level']= this.goalsSettingsObject.data.enable_performance_level;

      obj['review_collaborators']= this.goalsSettingsObject.data.review_collaborators;
      obj['goal_owner_setting']= this.goalsSettingsObject.data.goal_owner_setting;
      obj['goal_owners_cannot_uncheck_action_items']= this.goalsSettingsObject.data.goal_owners_cannot_uncheck_action_items;
    } else if(viewMode == 'privileges'){
      obj['users_privileges_check']= this.goalsSettingsObject.users_privileges_check;
      obj['admins_privileges_check']= this.goalsSettingsObject.admins_privileges_check;
    }else{
      obj['goal_alt_name']= this.goalsSettingsObject.data.goal_alt_name;
      obj['item_alt_name']= this.goalsSettingsObject.data.item_alt_name;
      obj['evidence_alt_name']= this.goalsSettingsObject.data.evidence_alt_name;
      obj['measurement_alt_name']= this.goalsSettingsObject.data.measurement_alt_name;
    }
    this.changeCount =0;

    this.goalService.updateGoalSettings(obj).subscribe((data: any) => {
      this.goalSettings();
      console.log('success');
      this.toastr.ShowToastr('success',this.headerService.getGoalAltTranslation(this.translations.goal_setting_update));

    }, error => {
      this.toastr.ShowToastr('error',this.translations.goal_setting_not_update);
    });
  }

  checkEnableMeasurement(event){
    if (event.target.checked) {
      this.goalsSettingsObject.data.enable_measurement = 1;
    }
    else{
      this.goalsSettingsObject.data.enable_measurement = 0;
    }
  }

  checkEnabelPerformanceLevel(event){
    if (event.target.checked) {
      this.goalsSettingsObject.data.enable_performance_level = 1;
    }
    else{
      this.goalsSettingsObject.data.enable_performance_level = 0;
    }
  }
  checkReviewCollaborators(event){
    if (event.target.checked) {
      this.goalsSettingsObject.data.review_collaborators = 1;
    }
    else{
      this.goalsSettingsObject.data.review_collaborators = 0;
    }
  }

  checkGoalOwnerSetting(event) {
    if (event.target.checked) {
      this.goalsSettingsObject.data.goal_owner_setting = 1;
    }
    else{
      this.goalsSettingsObject.data.goal_owner_setting = 0;
    }
  }

  checkActionItemCompletetion(event){
    if (event.target.checked) {
      this.goalsSettingsObject.data.goal_owners_cannot_uncheck_action_items = 1;
    }
    else{
      this.goalsSettingsObject.data.goal_owners_cannot_uncheck_action_items = 0;
    }
  }

  checkMakeAllGoalPublic(checked: boolean, key: string) {
    this.currentGoalsPublicKey = key;
    if (checked) {
      this.makeAllGoalPublicModal.show();
    } else {
      this.goalsSettingsObject.data[this.goalsPublicKeys[key].all] = 0;
      this.goalsSettingsObject.data[this.goalsPublicKeys[key].group] = 0;
      this.goalsSettingsObject.data[this.goalsPublicKeys[key].individual] = 0;
      this.goalsSettingsObject.data[this.goalsPublicKeys[key].account] = 0;
    }
  }

  public checkPublicValues(checked: boolean, key: string) {
    let allgpKey = 'make_all_goals_public', ggpKey = 'group_type_goals_public', igpKey = 'individual_type_goals_public', agpKey = 'account_type_goals_public';

    if(key === 'admin_super_owner') {
      allgpKey = 'make_all_goals_public_admin_super_owner';
      ggpKey = 'group_type_goals_public_admin_super_owner';
      igpKey = 'individual_type_goals_public_admin_super_owner';
      agpKey = 'account_type_goals_public_admin_super_owner';
    }else if (key === 'super_owner') {
      allgpKey = 'make_all_goals_public_super_owner';
      ggpKey = 'group_type_goals_public_super_owner';
      igpKey = 'individual_type_goals_public_super_owner';
      agpKey = 'account_type_goals_public_super_owner';
    }

    const { [ggpKey]: ggp, [igpKey]: igp, [agpKey]: agp } = this.goalsSettingsObject.data;
    console.log(`ggpKey: ${ggpKey}, igpKey: ${igpKey}, agpKey: ${agpKey}`)
    console.log(`ggp: ${ggp}, igp: ${igp}, agp: ${agp}`)
    if (checked) {
      if(ggp && igp && agp) this.goalsSettingsObject.data[allgpKey] = 1;
      else this.checkMakeAllGoalPublic(checked, key);

    } else {
      if (!ggp || !igp || !agp) this.goalsSettingsObject.data[allgpKey] = 0;
    }
  }

  doneMakeAllGoalPublic(key: string) {
    this.goalsSettingsObject.data[this.goalsPublicKeys[key].all] = 1;
    this.goalsSettingsObject.data[this.goalsPublicKeys[key].group] = 1;
    this.goalsSettingsObject.data[this.goalsPublicKeys[key].individual] = 1;
    this.goalsSettingsObject.data[this.goalsPublicKeys[key].account] = 1;

    this.makeAllGoalPublicModal.hide();

  }

  cancelMakeAllGoalPublic(key: string) {
    this.goalsSettingsObject.data[this.goalsPublicKeys[key].all] = 0;
    this.makeAllGoalPublicModal.hide();
  }

  cancelChanges() {
    this.goalSettings();
    console.log('cancelled');
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
