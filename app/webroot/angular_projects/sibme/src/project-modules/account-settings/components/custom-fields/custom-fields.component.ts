import { Component, ViewChild, OnDestroy, OnInit, HostListener } from '@angular/core';
import { HeaderService } from '@app/services';
import { HttpClient } from '@angular/common/http';
import { ShowToasterService } from '@projectModules/app/services';
import { Router } from '@angular/router';
import { CustomFieldsService } from '../../services/custom-fields.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs/internal/Subscription';
@Component({
  selector: 'app-custom-fields',
  templateUrl: './custom-fields.component.html',
  styleUrls: ['./custom-fields.component.css']
})
export class CustomFieldsComponent implements OnInit, OnDestroy {

  public userSummaryData: any;
  public parentSummaryData: any = [];
  public userAssessment: any;
  public parentAssessment: any = [];
  public arrLengthCheckSum: any;
  public arrLengthCheckAsse: any;
  public temp_type: any;
  public tempArr: any;
  public tab_toggle: boolean = true;
  public temp_check: any;
  public finalObj: any;
  public childIndex: any;
  public childArrSelected: any;
  public dropdownValues: any = [{ dropdown: "1" }, { dropdown: "2" }, { dropdown: "3" }, { dropdown: "4" }];
  public assessmentDropdownValues: any = [{ dropdown: "1" }, {dropdown: "5"}, { dropdown: "2" }, { dropdown: "3" }, { dropdown: "4" }];
  public adminCustomCheck: boolean = false;
  public userCustomCheck: boolean = false;
  private subscriptions: Subscription = new Subscription();
  public translation: any = {};
  userCurrAcc: any;
  public tempObjDeleteId: any;
  public plusButtonCheckSum: boolean = true;
  public plusButtonCheckAsse: boolean = true;
  deleteModalRef: BsModalRef;
  shareChildModal: BsModalRef;
  public confirmation;
  public childExistCheck: boolean;
  public index_delete;
  public skeletonLoading: boolean = true;
  public saveBtnCheck: boolean = true;
  public is_check = true;

  @ViewChild('deleteTemplate', { static: false }) deleteTemplate;
  @ViewChild('shareChild', { static: false }) shareChild;

  constructor(
    private headerService: HeaderService, private http: HttpClient,
    private toastr: ShowToasterService,
    private router: Router,
    private customFieldsService: CustomFieldsService,
    private modalService: BsModalService
  ) {

    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
    this.childExistCheck = this.headerService.getStaticHeaderData('child_accounts_exist');

    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
      
    }));

  }

  ngOnInit() {
    this.getUserSummary();
  }

  getUserSummary() {
    this.customFieldsService.getCustomFieldData(this.userCurrAcc.accounts.account_id).subscribe((res: any) => {
      this.userSummaryData = res.custom_fields.user_summary.custom_fields;
      this.parentSummaryData = res.custom_fields.user_summary.parent_custom_fields;
      this.userAssessment = res.custom_fields.assessment.custom_fields;
      this.parentAssessment = res.custom_fields.assessment.parent_custom_fields;
      this.arrLengthCheckAsse = this.userAssessment.length - 1; 

      if (this.userAssessment.length == 10) this.plusButtonCheckAsse = false;
      else this.plusButtonCheckAsse = true;

      if(res.admins_can_edit_cf == 1) {
        this.adminCustomCheck = true;
      } else {
        this.adminCustomCheck = false;
      }  

      if(res.users_can_edit_cf == 1) {
        this.userCustomCheck = true;
      } else {
        this.userCustomCheck = false;
      } 
      this.arrLengthCheckSum = this.userSummaryData.length - 1;
      if (this.userSummaryData.length == 10) this.plusButtonCheckSum = false;
      else this.plusButtonCheckSum = true;

      this.skeletonLoading = false;
      this.saveBtnCheck = true;
    });
  }

  minusRow(arr, i, id, type) {
    if (id != undefined) {
      this.openDeleteModal(arr, i, type, true, id);
    } else {
      this.minusRowConfirm(arr, i, type, true);
    }
  }

  enterPressed(e) {
    if (e.keyCode == 13) {
      this.confirmDelete();
    }
  }

  openDeleteModal(arr, i, type, check, id) {
    this.confirmation = '';
    this.index_delete = i;
    this.temp_type = type;
    this.temp_check = check;
    this.tempArr = arr;
    this.tempObjDeleteId = id;
    this.deleteModalRef = this.modalService.show(this.deleteTemplate, { class: "modal-md  maxcls", backdrop: 'static' })
  }

  confirmDelete() {
    if (this.confirmation != this.translation?.Huddle_confirmation_bit) {
      this.toastr.ShowToastr('info',this.translation?.type_delete_new_settings);
      return;
    } else {
      this.minusRowConfirm(this.tempArr, this.index_delete, this.temp_type, this.temp_check);
      this.deleteModalRef.hide();
      this.deleteCustomField(this.temp_type);
    }
  }

  deleteCustomField(type) {
    let obj = {
      user_current_account: this.userCurrAcc,
      custom_field_id: this.tempObjDeleteId
    }
    this.customFieldsService.deleteAssessmentFields(obj).subscribe((res: any) => {
      if (res.success == true) {
        this.toastr.ShowToastr('success',res.messages);

        this.getUserSummary();
      }
    });
  }

  minusRowConfirm(arr, i, type, check) {
    arr.splice(i, 1);
    if (type == 'Summary') this.arrLengthCheckSum = arr.length - 1;
    else if (type == 'Assessment') this.arrLengthCheckAsse = arr.length - 1;

    if (type == 'Summary' && arr.length < 10) this.plusButtonCheckSum = true;
    else if (type == 'Assessment' && arr.length < 10) this.plusButtonCheckAsse = true;

    if (check == false && type == 'Summary') arr[0] = { field_label: "", field_type: "", is_shared: false, ref_type: 1 }
    if (check == false && type == 'Assessment') arr[0] = { field_label: "", field_type: "", is_shared: false, ref_type: 2 }
  }

  plusRow(arr, type) {
    if (type == 'Summary') {
      arr.push({ field_label: "", field_type: "", is_shared: false, ref_type: 1 });
      this.arrLengthCheckSum = arr.length - 1;
    }
    else if (type == 'Assessment') {
      arr.push({ field_label: "", field_type: "", is_shared: false, ref_type: 2 });
      this.arrLengthCheckAsse = arr.length - 1;
    }

    if (type == 'Summary' && arr.length == 10) this.plusButtonCheckSum = false;
    else if (type == 'Assessment' && arr.length == 10) this.plusButtonCheckAsse = false;
  }

  onChildAcc(i, type, arr) {
    if (type == false) {
      this.childIndex = i;
      this.childArrSelected = arr;
      this.shareChildModal = this.modalService.show(this.shareChild, { class: "modal-md  maxcls", backdrop: 'static' });
    }
  }

  confirmShareChild() {
    this.childArrSelected[this.childIndex].is_shared = true;
    this.shareChildModal.hide();
  }

  notConfirmShareChild() {
    this.childArrSelected[this.childIndex].is_shared = false;
    this.shareChildModal.hide();
  }

  checkNameField(d, i, arr) {
    arr.forEach((value, index) => {
      if (index != i) {
        if (d.field_label == value.field_label) {
          this.toastr.ShowToastr('error',this.translation?.cf_custom_field_exists);
          arr[i] = { field_label: "", field_type: "", is_shared: d.is_shared, ref_type: d.ref_type };
        }
      }
    });
  }

  update() {
    this.saveBtnCheck = false;

    // Check if custom fields have space
    for (let i = 0; i < this.userSummaryData.length; i++) {
      this.userSummaryData[i].field_label = this.userSummaryData[i].field_label.trim();
    }
    for (let i = 0; i < this.userAssessment.length; i++) {
      this.userAssessment[i].field_label = this.userAssessment[i].field_label.trim();
    }

    // Check if custom label in array is empty
    const userSummaryData = this.userSummaryData.some(el => el.field_label == "" || el.field_type == "");
    const userAssessment = this.userAssessment.some(el => el.field_label == "" || el.field_type == "");

    // Check if same custom label exist in array
    let sameValCheckSummary = false;
    this.userSummaryData.forEach((val, key) => {
      this.userSummaryData.forEach((childVal, childKey) => {
        if(key != childKey) {
          if(childVal.field_label.toLowerCase() == val.field_label.toLowerCase()){
            sameValCheckSummary = true;
          }
        }
      });
    });

    let sameValCheckAsses = false;
    this.userAssessment.forEach((val, key) => {
      this.userAssessment.forEach((childVal, childKey) => {
        if(key != childKey) {
          if(childVal.field_label.toLowerCase() == val.field_label.toLowerCase()){
            sameValCheckAsses = true;
          }
        }
      });
    });
    
    let newArr = [];    
    this.userAssessment.forEach(element => {
      newArr.push(element);
    });
    this.userSummaryData.forEach(element => {
      newArr.push(element);
    });

    this.finalObj = {
      custom_fields: newArr,
      admins_can_edit_cf: this.adminCustomCheck,
      users_can_edit_cf: this.userCustomCheck,
      account_id: this.userCurrAcc.accounts.account_id,
      user_id: this.userCurrAcc.users_accounts.user_id
    }

    if (userSummaryData != false || userAssessment != false) {
      this.toastr.ShowToastr('error',this.translation?.cf_fill_all_fields);
      this.saveBtnCheck = true;
    }
    else if (sameValCheckSummary != false || sameValCheckAsses != false) {
      this.toastr.ShowToastr('error',this.translation?.cf_custom_field_exists);
      this.saveBtnCheck = true;
    } else {
      this.customFieldsService.saveCustomFields(this.finalObj).subscribe((res: any) => {
        if (res.success == true) {
          this.toastr.ShowToastr('success',res.message);
          this.getUserSummary();
          this.is_check = true;
        }
      });
    }
  }

  checkChangeOnPage(){
    this.is_check = false;
    
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    if (!this.is_check) {
        if (confirm("You are about to leave custom fields before you have saved your changes. Your custom fields changes will not be saved. Are you sure?")) {
            window.onbeforeunload = () => {
                this.ngOnDestroy();
            }
        }
        event.returnValue = false;
    }
  }
}
