import { Component, ViewChild } from '@angular/core';
import { GlobalExportService } from '../../services/global-export.service';
import { ColorsLogoService } from '../../services/colors-logo.service';
import { HeaderService } from '@app/services';
import { HttpClient } from '@angular/common/http';
import { ShowToasterService } from '@projectModules/app/services';
import { DomSanitizer } from '@angular/platform-browser';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-global-export',
  templateUrl: './global-export.component.html',
  styleUrls: ['./global-export.component.css']
})
export class GlobalExportComponent {
  public export = {
    comment: false,
    time_stamp: false,
    date_stamp: false,
    framework_name: false,
    tagged_standards: false,
    custom_marker_tags: false,
    attachment_file_names: false,
  };
  viewMode = 'general';
  private account_id: any;
  private header_data: any;
  public changesSaved = false;
  private subscription: Subscription;
  public translations;
  @ViewChild('save_changes', {static: false}) saveChangesModal: ModalDirective;

  constructor(private colorsLogoService: ColorsLogoService,
              private headerService: HeaderService, private http: HttpClient,
              private toastr: ShowToasterService, private globalExportService: GlobalExportService,
              public router: Router) {

    this.header_data = this.headerService.getStaticHeaderData();
    this.account_id = this.header_data.user_current_account.users_accounts.account_id;
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
    });
  }

  updateGlobalExport() {
    let obj = {
      account_id: this.account_id, export_type: this.viewMode,
      export_fields: this.export
    };
    console.log(obj);
    this.globalExportService.updateGlobalExport(obj).subscribe((data: any) => {
      this.changesSaved = false;
      if (data.success) {
        this.toastr.ShowToastr('success',data.message);
      } else {
        this.toastr.ShowToastr('error',data.message);
      }
    }, error1 => {
      console.log(error1);
    });
  }

  hideSaveChangesModal() {
    this.saveChangesModal.hide();
  }

  showSaveChangesModal() {
    this.changesSaved ? this.saveChangesModal.show() : this.router.navigate(['/account-settings']);
  }
  goBack()
  {
    this.changesSaved = false;
  }
}
