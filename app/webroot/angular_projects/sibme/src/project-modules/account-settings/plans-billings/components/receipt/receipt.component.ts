import { DataService } from '@src/project-modules/account-settings/services/data.service';
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ShowToasterService } from '@projectModules/app/services';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import domtoimage from 'dom-to-image';
import { HeaderService } from '@src/project-modules/app/services';

@Component({
  selector: 'plans-receipt',
  templateUrl: './receipt.component.html',
  styleUrls: ['./receipt.component.css']
})
export class ReceiptComponent implements OnInit, OnDestroy {
  public receiptData: any;
  public header_data: any;
  public translations;
  public sub = new Subscription();
  public skeletonLoading: boolean = true;
  @ViewChild('pdf_Table', { static: false }) pdf_Table: ElementRef;
  public showReceipt = false;
  base_url: any;
  constructor(
    private dataService: DataService,
    private router: Router,
    private headerService: HeaderService,
  ) { }
  ngOnInit() {
    this.sub.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
    }));
    this.header_data = this.headerService.getStaticHeaderData();
    this.base_url=this.header_data.base_url;
    setTimeout(() => {
      this.skeletonLoading = false;
    }, 3000);
    this.sub.add(this.dataService.currentReceipt.subscribe(receipt => {
      if (Object.keys(receipt).length > 0) {
        this.receiptData = receipt;
        this.showReceipt = true;
      } else {
        this.showReceipt = false;
      }

    }))
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  goBack() {
    this.router.navigate(['account-settings/plans']);
  }
  download() {
    // const pdfTable = this.pdfTable.nativeElement;
    // html2canvas(pdfTable).then(canvas => {
    //   // Few necessary setting options  
    //   var imgWidth = 208;
    //   var pageHeight = 295;
    //   var imgHeight = canvas.height * imgWidth / canvas.width;
    //   var heightLeft = imgHeight;

    //   const contentDataURL = canvas.toDataURL('image/png')
    //   let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
    //   var position = 0;
    //   pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)
    //   pdf.save('sibme_receipt.pdf'); // Generated PDF   
      
    // });
    const pdfTable = this.pdf_Table.nativeElement;
    const options = { background: 'white', height: 1000, width: 1200 };
    domtoimage.toPng(pdfTable, options).then((dataUrl) => {
      //Initialize JSPDF
      const doc = new jsPDF('l', 'mm', 'letter');
      const imgProps = doc.getImageProperties(dataUrl);
      const pdfWidth = doc.internal.pageSize.getWidth();
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      //Add image Url to PDF
      doc.addImage(dataUrl, 'PNG', 0, 0, pdfWidth + 5, pdfHeight);
      doc.save('sibme_receipt.pdf');
    })
  }
}