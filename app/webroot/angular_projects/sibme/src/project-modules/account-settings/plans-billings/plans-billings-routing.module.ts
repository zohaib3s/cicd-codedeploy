import { TrialGuard } from '../guards/trial.guard';
import { ReceiptComponent } from './components/receipt/receipt.component';
import { PaymentComponent } from './components/payment/payment.component';
import { ChangePlanComponent } from './components/change-plan/change-plan.component';
import { MainComponent } from './components/main/main.component';
import { CancelAccountComponent } from './components/cancel-account/cancel-account.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicAuthGuard } from '@shared/guards/public-auth.guard';
import { SharedPageNotFoundComponent } from '@shared/shared-page-not-found/shared-page-not-found.component';
import { ConfirmDeactivateGuard } from '../guards/ConfirmDeactivateGuard';
const routes: Routes = [
  { path: '', component: MainComponent, pathMatch: 'full', canActivate:[TrialGuard] },
  { path: 'change', component: ChangePlanComponent },
  { path: 'payment', component: PaymentComponent },
  { path: 'receipt', component: ReceiptComponent, canActivate:[PublicAuthGuard]},
  { path: 'cancel', component: CancelAccountComponent},
  { path: 'page_not_found', component: SharedPageNotFoundComponent  },
  { path: '**', redirectTo: 'page_not_found'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlansBillingsRoutingModule {
}
