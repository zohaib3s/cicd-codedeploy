import { Component, ViewChild, OnInit } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ColorsLogoService } from '../../../services/colors-logo.service';
import { HeaderService } from '@app/services';
import { HttpClient } from '@angular/common/http';
import { ShowToasterService } from '@projectModules/app/services';
import { GlobalExportService } from '../../../services/global-export.service';
import { CancelAccountService } from '../../../services/cancel-account.service';
import { Router } from '@angular/router';
import { environment } from '@src/environments/environment';
import {Subscription} from 'rxjs';
@Component({
  selector: 'app-cancel-account',
  templateUrl: './cancel-account.component.html',
  styleUrls: ['./cancel-account.component.css']
})
export class CancelAccountComponent implements OnInit {

  @ViewChild('cancel_account', {static: false}) cancelAccount: ModalDirective;
  private account_id: any;
  private header_data: any;
  public deleteString = 'DELETE';
  private subscription: Subscription;
  public translations;
  public reasons;
  public reasonsDetails;
  public deleteConfirm = '';

  // public confirmDelete = '';

  constructor(private colorsLogoService: ColorsLogoService,
              private headerService: HeaderService, private http: HttpClient,
              private toastr: ShowToasterService,
              private cancelAccountService: CancelAccountService,
              private router: Router) {

    this.header_data = this.headerService.getStaticHeaderData();
    this.account_id = this.header_data.user_current_account.users_accounts.account_id;

    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
      // this.huddle_data.title = this.translation.huddle_title;
      // this.LoadHuddleTypes();
    });
    
  }

  ngOnInit(){
    this.reasons = [{
      key: this.translations.its_expensive_new_settings, checked: false
    }, {
      key: this.translations.dont_justify_cost_new_settings, checked: false
    }, {
      key: this.translations.dont_have_features_new_settings, checked: false
    }, {
      key: this.translations.doesnt_work_well_new_settings, checked: false
    }];
 
  }

  deleteAccount() {
    let obj = {
      reasons_for_leaving_sibme: '',
      reasons_for_leaving_sibme_detail: this.reasonsDetails,
      account_id: this.account_id
    };
    this.reasons.forEach(x => {
      if (x.checked) {
        obj.reasons_for_leaving_sibme = obj.reasons_for_leaving_sibme + `${x.key}|`;
      }
    });
    this.cancelAccountService.cancelAccount(obj).subscribe((data) => {
      window.location.href = environment.appPath + '/users/login';
    }, error1 => {
      console.log(error1);
    });
  }

  showCancelAccount() {
    this.cancelAccount.show();
  }

  hideCancelAccount() {
    this.cancelAccount.hide();
    this.reasonsDetails='';
    this.deleteConfirm='';
    this.reasons = [{
      key: this.translations.its_expensive_new_settings, checked: false
    }, {
      key: this.translations.dont_justify_cost_new_settings, checked: false
    }, {
      key: this.translations.dont_have_features_new_settings, checked: false
    }, {
      key: this.translations.doesnt_work_well_new_settings, checked: false
    }];

  }

}
