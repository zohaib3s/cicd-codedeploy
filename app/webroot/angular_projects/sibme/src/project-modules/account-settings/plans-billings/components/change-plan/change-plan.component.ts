import { PlansService } from './../../../services/plans.service';
import { Component, OnInit, OnDestroy, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeaderService } from '@src/project-modules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataService } from '@src/project-modules/account-settings/services/data.service';

// Dev :  Saad Zia, Aqib , Umer
// Date : 27-02-2020
@Component({
  selector: 'plans-change',
  templateUrl: './change-plan.component.html',
  styleUrls: ['./change-plan.component.css']
})
export class ChangePlanComponent implements OnInit, OnDestroy {
  public translations;
  public subscription: Subscription;
  // section 1 Booleans flags
  public disableToggle: boolean = false;
  public toggle: boolean = true;
  public ccCheck: boolean = false;
  public skeletonLoading: boolean = true;
  public transactionCheck: boolean = false;
  public featureSwitch: boolean = false;
  public disableBasic: boolean = false;
  public notAllowed: boolean = true;
  public showAccountSettingsBackButton: boolean = false;
  // section 1 ends
  public previousPlanData;
  public accountData = [];
  public userDetails : any = [];
  public type: String = "YR";
  public userCost: number = 0;
  public userCost2: number = 0;
  public startIndex: number = 0;
  public userArray = [];
  public selectedUsers: number = 0;
  public selectedUsers2: number = 0;
  public roleId: number;
  public account_id: any;
  public header_data: any;
  public sub = new Subscription();
  constructor(
    private headerService: HeaderService,
    private http: HttpClient,
    private toastr: ShowToasterService,
    private router: Router,
    private shareData: DataService,
    private plansService: PlansService,
  ) {
    this.selectedUsers = 0;
  }
  ngOnInit() {
    this.sub.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
    }));
    this.header_data = this.headerService.getStaticHeaderData();
    this.account_id = this.header_data.user_current_account.users_accounts.account_id;
    this.fetchAccount(this.account_id);
  }
  navigateTo() {
    this.router.navigate(['/account-settings/plans']);
  }
  fetchAccount(accountId) {
    let obj = {
      account_id: accountId
    };
    this.sub.add(
      this.plansService.fetchAccountPlan(obj).subscribe(details => {
        this.userDetails = details;
        if(details['plan_name'] == 'Trial Account'){
          this.showAccountSettingsBackButton = true;
        }
        if(details['plan_cycle'] == ''){
          //here if we are getting plan cycle undefined that means there is no plan purchased so toggling to monthly.
          //togle false = monthly; toggle true = yearly
          this.selectedUsers = 0;
          this.toggle = false;
          this.type = "MO"
          for (let i = this.startIndex; i < 100; i++) {
            this.userArray.push(i);
          }
        } else {
          if (details['plan_cycle'] == 'yearly') {
            
            this.disableToggle = true;
            if((this.type='YR') && (this.userDetails['plan_name'] == 'PRO') && (this.userDetails['plan_cycle'] == 'yearly')){
              this.disableBasic = true;
              this.selectedUsers = this.userCost = 0;
            } else {
              this.selectedUsers = (details['number_of_users_in_plan'] + 1);
              this.userCost = ((details['number_of_users_in_plan'] + 1) * 99) 
            }
            this.toggle = true;
            this.type = "YR"
            this.startIndex = details['number_of_users_in_plan'];
            //this.userCost = (details['number_of_users_in_plan'] * 99)
            //this.selectedUsers = details['number_of_users_in_plan'];
            this.userArray.push(0);
            if(details['number_of_users_in_plan'] > 99){
              this.userCost = this.selectedUsers = this.selectedUsers2 = this.startIndex = 0;
            } else {
              for (let i = this.startIndex; i < 100; i++) {
                this.userArray.push(i);
              }
            }
            
          } else {
            if((this.type='MO') && (this.userDetails['plan_name'] == 'PRO') && (this.userDetails['plan_cycle'] == 'monthly')){
              this.disableBasic = true;
              this.selectedUsers = this.userCost = 0;
            } else {
              this.selectedUsers = (details['number_of_users_in_plan'] + 1);
              this.userCost = ((details['number_of_users_in_plan'] + 1) * 10) 
            }
            this.toggle = false;
            this.type = "MO";
            this.startIndex = details['number_of_users_in_plan'];
            this.userArray.push(0)
            if(details['number_of_users_in_plan'] > 99){
              this.userCost = this.selectedUsers = this.selectedUsers2 = this.startIndex = 0;
            } else{
              for (let i = this.startIndex; i < 100; i++) {
                this.userArray.push(i);
              }
            }
            
          }
        }
        
        this.skeletonLoading = false;
      })
    )

  }
  userSelected(e) {
    // Toggle: false (Monthly)
    // Toggle : True (Yearly)
    // selectedUsers : BASIC plan dropdown value
    // selectedUsers2 : PRO plan dropdown value
    this.notAllowed = true
    if(this.selectedUsers == 0 && this.selectedUsers2 == 0){
      this.toastr.ShowToastr('error',"Number of license can't be 0")
      this.userCost = this.userCost2 = 0;
    } else {
      if (e.target.id == "selectedUsers") {
        this.selectedUsers2 = 0;
        this.userCost2 = 0;
        if (this.toggle == false) {
          // BASIC monthly
          if((this.userDetails['number_of_users_in_plan'] == this.selectedUsers) && (this.userDetails['plan_name'] == 'BASIC') && (this.userDetails['plan_cycle'] == 'monthly')){
            //this.toastr.ShowToastr('error',"Not Allowd");
            this.userCost = 0;
            this.notAllowed = false;
            //this.userCost = this.selectedUsers = 0;
          } else {
            this.selectedUsers2 = 0;
            this.userCost2 = 0;
            this.userCost = this.selectedUsers * 10
          }
        } else if (this.toggle == true) {
          // BASIC Yearly
          if((this.userDetails['number_of_users_in_plan'] == this.selectedUsers) && (this.userDetails['plan_name'] == 'BASIC') && (this.userDetails['plan_cycle'] == 'yearly')){
            //this.toastr.ShowToastr('error',"Not Allowd");
            this.userCost = 0;
            this.notAllowed = false;
            //this.userCost = this.selectedUsers = 0;
          } else {
            this.selectedUsers2 = 0;
            this.userCost2 = 0;
            this.userCost = (this.selectedUsers * 99) 
          }
        }
      }
      if (e.target.id == "selectedUsers2") {
        this.selectedUsers = 0;
        this.userCost = 0;
        if (this.toggle == false) {
          // PRO Monthly
          if((this.userDetails['number_of_users_in_plan'] == this.selectedUsers2) && (this.userDetails['plan_name'] == 'PRO') && (this.userDetails['plan_cycle'] == 'monthly')){
            //this.toastr.ShowToastr('error',"Not Allowd");
            this.userCost = 0;
            this.notAllowed = false;
            //this.userCost = this.selectedUsers2 = 0;
          } else {
            this.userCost2 = this.selectedUsers2 * 15
          }
        } else if (this.toggle == true) {
          // PRO Yearly
          if((this.type=='YR') && (this.userDetails['plan_name'] == 'PRO') && (this.userDetails['plan_cycle'] == 'yearly')){
            this.disableBasic = true;
            this.selectedUsers = this.userCost = 0
          } else {
            this.disableBasic = false;
          }
          if((this.userDetails['number_of_users_in_plan'] == this.selectedUsers2) && (this.userDetails['plan_name'] == 'PRO') && (this.userDetails['plan_cycle'] == 'yearly')){
            //this.toastr.ShowToastr('error',"Not Allowd");
            this.notAllowed = false;
            this.userCost = 0;
            //this.userCost = this.selectedUsers2 = 0;
          } else {
            this.selectedUsers = this.userCost = 0
            this.userCost2 = (this.selectedUsers2 * 149) 
          }
        }
      }
    }
    

  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  togglePlanMode(e) {
    if(!this.notAllowed){
      this.notAllowed = true;
    } else {
      this.notAllowed = false;
    }
    if((e==true) && (this.userDetails['plan_name'] == 'PRO') && (this.userDetails['plan_cycle'] == 'monthly')){
      this.disableBasic = true;
      this.selectedUsers = this.userCost = 0;
    } else {
      this.disableBasic = false;
    }
    if(this.disableToggle == false){
      if(this.selectedUsers != 0){
        this.userCost2 = 0;
        if (e == true) {
          this.userCost = this.selectedUsers * 10
        } else if (e == false) {
          this.userCost = (this.selectedUsers * 99)
        }
      }
      if (this.selectedUsers2 != 0){
        this.userCost = 0;
        if (e == true) {
          //monthly
          this.userCost2 = this.selectedUsers2 * 15
        } else if (e == false) {
          //yearly
          this.userCost2 = (this.selectedUsers2 * 149)
        }
      }
    }
      
    
    if(this.disableToggle == true){
      this.toastr.ShowToastr('info',this.translations.currently_you_are_using_yealy_new_settings  )
    }
    if(this.disableToggle == false){
      if (e == true) {
        this.type = "MO"
      } else {
        this.type = "YR"
      }
    }
  }

  subplan(e) {
    if (e == 9 || e == 10) {
      if (this.selectedUsers == 0) {
        this.toastr.ShowToastr('info',"Please select the users first")
      } else if(this.notAllowed) {
        this.shareData.changeId(e);
        this.shareData.changeSelectedUsers(this.selectedUsers);
        this.router.navigate(['account-settings/plans/payment']);
      } else {
        this.toastr.ShowToastr('error',"Not Allowd");
      }
    }
    if (e == 11 || e == 12) {
      if (this.selectedUsers2 == 0) {
        this.toastr.ShowToastr('info',"Please select the users first")
      } else if(this.notAllowed) {
        this.shareData.changeId(e);
        this.shareData.changeSelectedUsers(this.selectedUsers2);
        this.router.navigate(['account-settings/plans/payment']);
      } else {
        this.toastr.ShowToastr('error',"Not Allowd");
      }
    }

  }
  hiddenSubplan(e, toggle) {
    if (toggle == true) {
      // Yearly plan
      if (e.target.id == 'basic') {
        if (this.selectedUsers == 0) {
          this.toastr.ShowToastr('info',"Please select the users first")
        } else if(this.notAllowed) {
          // Yearly BASIC checks
          if((this.userDetails['number_of_users_in_plan'] == this.selectedUsers) && (this.userDetails['plan_name'] == 'BASIC') && (this.userDetails['plan_cycle'] == 'yearly')){
            this.toastr.ShowToastr('error',"Not Allowd");
            this.notAllowed = false;
            this.userCost = 0;
            //this.userCost = this.selectedUsers = 0;
          } else {
            this.shareData.changeId(9);
            this.shareData.changeSelectedUsers(this.selectedUsers);
            this.router.navigate(['account-settings/plans/payment']); 
          }
        }
      } else if (e.target.id == 'pro') {
        if (this.selectedUsers2 == 0) {
          this.toastr.ShowToastr('info',"Please select the users first")
        } else {
          // Yearly PRO checks
          if((this.type=='YR') && (this.userDetails['plan_name'] == 'PRO') && (this.userDetails['plan_cycle'] == 'yearly')){
            this.disableBasic = true;
            this.selectedUsers = this.userCost = 0
          } else {
            this.disableBasic = false;
          }
          if((this.userDetails['number_of_users_in_plan'] == this.selectedUsers2) && (this.userDetails['plan_name'] == 'PRO') && (this.userDetails['plan_cycle'] == 'yearly')){
            this.toastr.ShowToastr('error',"Not Allowd");
            this.notAllowed = false;
            this.userCost = 0;
            //this.userCost = this.selectedUsers2 = 0;
          } else if(this.notAllowed) {
            this.shareData.changeId(11);
            this.shareData.changeSelectedUsers(this.selectedUsers2);
            this.router.navigate(['account-settings/plans/payment']); 
          }
        }
      }
    } else {
      // Monthly plan
      if (e.target.id == 'basic') {
        if (this.selectedUsers == 0) {
          this.toastr.ShowToastr('info',"Please select the users first")
        } else {
          // Monthly BASIC Checks
          if((this.userDetails['number_of_users_in_plan'] == this.selectedUsers) && (this.userDetails['plan_name'] == 'BASIC') && (this.userDetails['plan_cycle'] == 'monthly')){
            this.toastr.ShowToastr('error',"Not Allowd");
            this.notAllowed = false;
            this.userCost = 0;
            //this.userCost = this.selectedUsers = 0;
          } else if(this.notAllowed) {
          this.shareData.changeId(10);
          this.shareData.changeSelectedUsers(this.selectedUsers);
          this.router.navigate(['account-settings/plans/payment']);
          }
        }
      } else if (e.target.id == 'pro') {
        if (this.selectedUsers2 == 0) {
          this.toastr.ShowToastr('info',"Please select the users first")
        } else {
          // Monthly PRO Checks
          if((this.userDetails['number_of_users_in_plan'] == this.selectedUsers2) && (this.userDetails['plan_name'] == 'PRO') && (this.userDetails['plan_cycle'] == 'monthly')){
            this.toastr.ShowToastr('error',"Not Allowd");
            this.notAllowed = false;
            this.userCost = 0;
            //this.userCost = this.selectedUsers2 = 0;
          } else if(this.notAllowed) {
            this.shareData.changeId(12);
            this.shareData.changeSelectedUsers(this.selectedUsers2);
            this.router.navigate(['account-settings/plans/payment']);
          }
        }
      }
    }
  }
}