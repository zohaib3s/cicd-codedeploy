import { DataService } from '@src/project-modules/account-settings/services/data.service';
import { Card } from '../../../models/card';
import { PlansService } from '../../../services/plans.service';
import { Component, OnInit, ViewChild, OnDestroy, ElementRef, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeaderService } from '@src/project-modules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import csc from 'country-state-city'
import * as jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import domtoimage from 'dom-to-image';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

// Dev :  Saad Zia, Aqib , Umer
// package used for country and state : https://www.npmjs.com/package/country-state-city
@Component({
  selector: 'plans-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit, OnDestroy {
  public translations;
  public receiptData: any = {};
  public sub = new Subscription();
  public account_id: any;
  public account_name: any;
  public header_data: any;
  public accountData = [];
  public countires = [];
  public states = [];
  public arr = [];
  private projectTitle: string = 'plans';
  public card = new Card();

  public modalRef: BsModalRef
  // section 1: Bool flags
  public skeletonLoading: boolean = true;
  public ccCheck: boolean = false;
  public showNA: boolean = true;
  public showPDF: boolean = false;
  public showForm: boolean = false;
  public hidden: boolean = false;
  // section ends
  public transactionCheck: boolean = false;
  @ViewChild('pdfTable', { static: false }) pdfTable: ElementRef;
  base_url: any;
  constructor(
    private headerService: HeaderService,
    private http: HttpClient,
    private toastr: ShowToasterService,
    private router: Router,
    private plansService: PlansService,
    private dataService: DataService,
    private modalService: BsModalService

  ) {
    this.sub.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
    }));
   }
  ngOnInit() {

    this.states = [
      { name: "New York" }, { name: "Alaska" }, { name: "Arizona" }, { name: "Arkansas" }, { name: "California" }, { name: "Colorado" }, { name: "Connecticut" },
      { name: "Delaware" }, { name: "Florida" }, { name: "Georgia" }, { name: "Hawaii" }, { name: "Idaho" }, { name: "Illinois" }, { name: "Indiana" },
      { name: "Iowa" }, { name: "Kansas" }, { name: "Kentucky" }, { name: "Louisiana" }, { name: "Maine" }, { name: "Maryland" }, { name: "Massachusetts" },
      { name: "Michigan" }, { name: "Minnesota" }, { name: "Mississippi" }, { name: "Missouri" }, { name: "Montana" }, { name: "Nebraska" }, { name: "Nevada" }, { name: "New Hampshire" },
      { name: "New Jersey" }, { name: "New Mexico" }, { name: "Alabama" }, { name: "North Carolina" }, { name: "North Dakota" }, { name: "Ohio" }, { name: "Oklahoma" }, { name: "Oregon" },
      { name: "Pennsylvania" }, { name: "Rhode Island" }, { name: "South Carolina" }, { name: "South Dakota" }, { name: "Tennessee" }, { name: "Texas" }, { name: "Utah" }, { name: "Vermont" }
      , { name: "Virginia" }, { name: "Washington" }, { name: "West Virginia" }, { name: "Wisconsin" }, { name: "Wyoming" }
    ]
    this.card.state = this.states[0].name
    this.header_data = this.headerService.getStaticHeaderData();
    this.base_url=this.header_data.base_url;
    this.account_name=this.header_data.user_current_account.accounts.company_name;
    this.account_id = this.header_data.user_current_account.users_accounts.account_id;
    this.fetchAccount(this.account_id);
    this.countires = csc.getAllCountries();
    this.card.country = this.countires[230].name
  }
  public loadFilterDialogUpdate(template: TemplateRef<any>) {

    this.modalRef = this.modalService.show(template, { class: "modal-md  maxcls", backdrop: 'static' });
  }
  countrySelected(event) {
    this.countires.forEach(x => {
      if (x.name == event) {
        this.fetchStates(x.id)
      }
    })
  }
  fetchStates(countryId) {
    this.states = csc.getStatesOfCountry(countryId);
  }
  fetchAccount(accountId) {
    let obj = {
      account_id: accountId
    };
    this.plansService.fetchAccountPlan(obj).subscribe(details => {
      if (details["creditCardNumber"].length == 0) {
        this.showNA = true;
      } else {
        this.showNA = false;
      }
      if (details["creditCardNumber"]) {
        this.ccCheck = true;
      }
      if (details["transactions"].length > 0) {
        this.transactionCheck = true;
      }
      this.accountData = [];
      this.skeletonLoading = false;
      this.accountData.push(details);
      this.pushDetailsToModel(details['customer_details']);
      this.arr = details['customer_details'];
    })
  }
  pushDetailsToModel(arr: []) {
    // this method will patch the values to the card model.
    if (arr) {
      this.states = [];
      this.card.first_name = arr['firstName'];
      this.card.last_name = arr['lastName'];
      this.card.email = arr['email'];
      this.card.address = arr['streetAddress'];
      this.card.extendedAddress = arr['extendedAddress'];
      this.card.zip_code = arr['postalCode'];
      this.card.company_name = arr['company'];
      this.card.city = arr['locality'];
      this.states.push({
        name: arr['region']
      })
      this.card.state = this.states[0].name
      this.card.country = arr['countryName'];
    }
  }
  resetFormToBack(){
    this.states = [];
    this.card.first_name = this.arr['firstName'];
    this.card.last_name = this.arr['lastName'];
    this.card.email = this.arr['email'];
    this.card.address = this.arr['streetAddress'];
    this.card.extendedAddress = this.arr['extendedAddress'];
    this.card.zip_code = this.arr['postalCode'];
    this.card.company_name = this.arr['company'];
    this.card.city = this.arr['locality'];
    this.states.push({
      name: this.arr['region']
    })
    this.card.card_number = this.card.card_expiration = this.card.card_cvv = ''
    this.card.state = this.states[0].name
    this.card.country = this.arr['countryName'];
  }
  updateCardInfo() {
    this.card.account_id = this.account_id;
    this.card.card_expiration = this.card.card_expiration.replace(/\s+/g, '');
    if (this.card.card_number && this.card.card_cvv && this.card.card_expiration) {
      this.sub.add(
        this.plansService.updateCard(this.card).subscribe(res => {
          if (res['success'] == true) {
            this.toastr.ShowToastr('success',res['message']);
            this.showForm = false;
            this.skeletonLoading = true;
            this.card = new Card();
            this.fetchAccount(this.account_id);
          } else {
            this.toastr.ShowToastr('error',res['message']);
          }
        })
      )
    }


  }
  cancelAccount() {
    this.router.navigate(['account-settings/plans/cancel']);
  }
  navigateTo() {
    this.router.navigate(['account-settings']);
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  download() {
    const pdfTable = this.pdfTable.nativeElement;
    const options = { background: 'white', height: 1000, width: 1200 };
    domtoimage.toPng(pdfTable, options).then((dataUrl) => {
      //Initialize JSPDF
      const doc = new jsPDF('l', 'mm', 'letter');
      const imgProps = doc.getImageProperties(dataUrl);
      const pdfWidth = doc.internal.pageSize.getWidth();
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      //Add image Url to PDF
      doc.addImage(dataUrl, 'PNG', 0, 0, pdfWidth + 5, pdfHeight);
      doc.save('sibme_receipt.pdf');
    })

  }
  toggle(obj) {
    this.showPDF = !this.showPDF
    console.log(obj);
    this.receiptData = obj;
  }
}
