import { ChangePlanComponent } from './components/change-plan/change-plan.component';
import { ReceiptComponent } from './components/receipt/receipt.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlansBillingsRoutingModule } from './plans-billings-routing.module'
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CancelAccountComponent } from './components/cancel-account/cancel-account.component';
import { AppLevelSharedModule } from '@shared/shared.module';
import { FormsModule } from '@angular/forms';
import { MainComponent } from './components/main/main.component'
import { PaymentComponent } from './components/payment/payment.component';
import { DataService } from '../services/data.service';
import { CreditCardDirectivesModule } from 'angular-cc-library';

@NgModule({
  declarations: [
    MainComponent,
    ChangePlanComponent,
    PaymentComponent,
    ReceiptComponent,
    CancelAccountComponent,
  ],
  imports: [
    CommonModule,
    PlansBillingsRoutingModule,
    LoadingBarHttpClientModule,
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule,
    CreditCardDirectivesModule,
    AppLevelSharedModule,
    BsDropdownModule.forRoot(),
  ],
  providers: [DataService]
})
export class PlansBillingsModule {
}
