import { plansModel } from './../../../models/plans';
import { PlansService } from './../../../services/plans.service';
import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HeaderService } from '@src/project-modules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataService } from '@src/project-modules/account-settings/services/data.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import csc from 'country-state-city'
// Dev :  Aqib
// Date : 03-03-2020
@Component({
  selector: 'plans-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit, OnDestroy {
  public translations;
  public account_id: any;
  public plan_id: any;
  public user_id: any;
  public selected_number_of_users: any;
  public header_data: any;
  public sub = new Subscription();
  // section 1: Customs flags for hiding and showing forms
  public noPlanFlag: boolean = false;
  public hideCC: boolean = false;
  public skeletonLoading: boolean = true;
  public showUpdateAndPay: boolean = false;
  public inTrial: boolean = false;
  // section 1 ends
  public planObj = new plansModel();
  public countires = [];
  public states = [];
  public accountData = [];
  public arr = [];
  public modalRef: BsModalRef;
  constructor(
    private headerService: HeaderService,
    private http: HttpClient,
    private toastr: ShowToasterService,
    private router: Router,
    private dataService: DataService,
    private plansService: PlansService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.sub.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translations = languageTranslation;
    }));
    this.countires = csc.getAllCountries();
    this.states = [
      { name: "New York" }, { name: "Alaska" }, { name: "Arizona" }, { name: "Arkansas" }, { name: "California" }, { name: "Colorado" }, { name: "Connecticut" },
      { name: "Delaware" }, { name: "Florida" }, { name: "Georgia" }, { name: "Hawaii" }, { name: "Idaho" }, { name: "Illinois" }, { name: "Indiana" },
      { name: "Iowa" }, { name: "Kansas" }, { name: "Kentucky" }, { name: "Louisiana" }, { name: "Maine" }, { name: "Maryland" }, { name: "Massachusetts" },
      { name: "Michigan" }, { name: "Minnesota" }, { name: "Mississippi" }, { name: "Missouri" }, { name: "Montana" }, { name: "Nebraska" }, { name: "Nevada" }, { name: "New Hampshire" },
      { name: "New Jersey" }, { name: "New Mexico" }, { name: "Alabama" }, { name: "North Carolina" }, { name: "North Dakota" }, { name: "Ohio" }, { name: "Oklahoma" }, { name: "Oregon" },
      { name: "Pennsylvania" }, { name: "Rhode Island" }, { name: "South Carolina" }, { name: "South Dakota" }, { name: "Tennessee" }, { name: "Texas" }, { name: "Utah" }, { name: "Vermont" }
      ,{ name: "Virginia" }, { name: "Washington" }, { name: "West Virginia" }, { name: "Wisconsin" }, { name: "Wyoming" }
    ]
    this.planObj.country = this.countires[230].name
    this.planObj.state = this.states[0].name
    this.header_data = this.headerService.getStaticHeaderData();
    this.planObj.auth_token = this.header_data.auth_token;
    // if(this.header_data.user_current_account.accounts.in_trial == true){
    //   this.inTrial = true;
    // }
    this.account_id = this.header_data.user_current_account.users_accounts.account_id;
    //console.log(this.header_data.user_current_account.User.id)
    this.planObj.user_id = this.header_data.user_current_account.User.id;
    this.dataService.currentId.subscribe(x => {
      this.plan_id = x;
      this.dataService.currentUsers.subscribe(y => {
        this.selected_number_of_users = y;
      })

    })
    if (!this.plan_id || !this.selected_number_of_users) {
      this.noPlanFlag = true;
    }
    this.planObj.company_name = '';
    this.planObj.account_id = this.account_id;
    this.planObj.plan_id = this.plan_id;
    this.planObj.plan_quantity = this.selected_number_of_users;

    let obj = {
      account_id: this.account_id,
      selected_plan_id: this.plan_id,
      selected_number_of_users: this.selected_number_of_users
    }

    this.sub.add(this.plansService.fetchAccountPlan(obj).subscribe(response => {
      this.skeletonLoading = false;
      this.accountData.push(response);
      this.planObj.plan_price = response['selected_plan_price'];
      this.planObj.description = response['description'];
      if (response['creditCardNumber']) {
        this.hideCC = true;
      }
      this.pushDetailsToModel(response['customer_details']);
      this.arr = response['customer_details'];
    })

    )

  }

  fetchStates(countryId) {
    this.states = csc.getStatesOfCountry(countryId);
  }
  pushDetailsToModel(arr: []) {
    if ((this.planObj.first_name != "" || this.planObj.first_name != undefined) || (this.planObj.last_name != "" || this.planObj.last_name != undefined)
      || (this.planObj.email != "" || this.planObj.email != undefined) || (this.planObj.address != "" || this.planObj.address != undefined)
      || (this.planObj.company_name != "" || this.planObj.company_name != undefined)) {

      // this method will patch the values to the model.
      if (arr) {
        this.states = [];
        this.planObj.first_name = arr['firstName'];
        this.planObj.last_name = arr['lastName'];
        this.planObj.email = arr['email'];
        this.planObj.address = arr['streetAddress'];
        this.planObj.extendedAddress = arr['extendedAddress'];
        this.planObj.zip_code = arr['postalCode'];
        this.planObj.company_name = arr['company'];
        this.planObj.city = arr['locality'];
        this.states.push({
          name: arr['region']
        })
        this.planObj.state = this.states[0].name
        this.planObj.country = arr['countryName'];
      }
    }
  }
  countrySelected(event) {
    this.countires.forEach(x => {
      if (x.name == event) {
        this.fetchStates(x.id)
      }
    })
  }
  resetFormToBack(){
    this.states = [];
        this.planObj.first_name = this.arr['firstName'];
        this.planObj.last_name = this.arr['lastName'];
        this.planObj.email = this.arr['email'];
        this.planObj.address = this.arr['streetAddress'];
        this.planObj.extendedAddress = this.arr['extendedAddress'];
        this.planObj.zip_code = this.arr['postalCode'];
        this.planObj.company_name = this.arr['company'];
        this.planObj.city = this.arr['locality'];
        this.planObj.card_number = this.planObj.card_expiration = this.planObj.card_cvv = ''
        this.states.push({
          name: this.arr['region']
        })
        this.planObj.state = this.states[0].name
        this.planObj.country = this.arr['countryName'];
  }
  submit() {

        // code to run for first form filling

      if (!this.showUpdateAndPay) {
      if (this.noPlanFlag) {
        this.toastr.ShowToastr('error',this.translations.please_select_the_new_plan_new_settings)
      } else {
        this.planObj.old_card = false;
        if (this.planObj.card_expiration != undefined) {
          this.planObj.card_expiration = this.planObj.card_expiration.replace(/\s+/g, '');
        }
        this.sub.add(this.plansService.purchasePlan(this.planObj).subscribe(response => {
          if (response['success'] == true) {
            this.headerService.isActive.next(true);
            this.dataService.updateReceipt(response['transaction_receipt_data']);
            this.plansService.updateHeaderSession().subscribe(res =>{
              this.headerService.isActive.next(true);
              this.toastr.ShowToastr('success',this.translations.success_new_settings)
              this.router.navigate(['account-settings/plans/receipt']);
            })

          } else {
            this.toastr.ShowToastr('info',response['error_message'])
          }
        }))
      }
    }

    // code to run when there is to be updated

    if (this.showUpdateAndPay) {
      if (this.noPlanFlag) {
        this.toastr.ShowToastr('error',this.translations.please_select_the_new_plan_new_settings)
      } else {
        // console.log("@@@")
        this.planObj.old_card = false;
        if (this.planObj.card_expiration != undefined) {
          this.planObj.card_expiration = this.planObj.card_expiration.replace(/\s+/g, '');
        }
        this.sub.add(this.plansService.purchasePlan(this.planObj).subscribe(response => {
          if (response['success'] == true) {
            this.plansService.updateHeaderSession().subscribe(res =>{
              this.headerService.isActive.next(true);
            this.toastr.ShowToastr('success',this.translations.success_new_settings)
            this.dataService.updateReceipt(response['transaction_receipt_data']);
            this.router.navigate(['account-settings/plans/receipt']);
            })
          } else {
            this.toastr.ShowToastr('info',response['error_message'])
          }
        }))
      }
    }

  }
  public loadFilterDialog(template: TemplateRef<any>, file) {

    this.modalRef = this.modalService.show(template, { class: "modal-md  maxcls", backdrop: 'static' });
    this.showUpdateAndPay = false;
  }
  loadFilterDialogUpdate(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, { class: "modal-md  maxcls", backdrop: 'static' });
  }

  ngOnDestroy() {
    this.sub.unsubscribe()
  }
  payWithCurrentCard(): void {
    this.planObj.old_card = true;
    if (this.noPlanFlag) {
      this.toastr.ShowToastr('error',this.translations.please_select_the_new_plan_new_settings)
    } else {
      this.sub.add(this.plansService.purchasePlan(this.planObj).subscribe(response => {
        if (response['success'] == true) {
          this.dataService.updateReceipt(response['transaction_receipt_data']);
          this.plansService.updateHeaderSession().subscribe(res =>{
            this.headerService.isActive.next(true);
            this.toastr.ShowToastr('success',this.translations.success_new_settings)
            this.router.navigate(['account-settings/plans/receipt']);
          })
        } else {
          this.toastr.ShowToastr('info',response['error_message'])
        }
      }))
    }

  }
  // updateAndPay(): void {

  //     if (this.noPlanFlag) {
  //       this.toastr.ShowToastr('error',this.translations.please_select_the_new_plan_new_settings)
  //     } else {
  //       console.log("@@@")
  //       this.planObj.old_card = false;
  //       if (this.planObj.card_expiration != undefined) {
  //         this.planObj.card_expiration = this.planObj.card_expiration.replace(/\s+/g, '');
  //       }
  //       this.sub.add(this.plansService.purchasePlan(this.planObj).subscribe(response => {
  //         if (response['success'] == true) {
  //           this.toastr.ShowToastr('success',this.translations.success_new_settings)
  //           this.dataService.updateReceipt(response['transaction_receipt_data']);
  //           this.router.navigate(['account-settings/plans/receipt']);
  //         } else {
  //           this.toastr.ShowToastr('info',response['error_message'])
  //         }
  //       }))
  //     }
  // }

  changePlan(): void {
    this.modalRef.hide();
    this.router.navigate(['account-settings/plans/change']);
  }
  cancelAccount(): void {
    this.router.navigate(['account-settings/plans/cancel']);
  }
  cancelPressed(): void {
    this.modalRef.hide();
    this.router.navigate(['account-settings/plans']);
  }
}
