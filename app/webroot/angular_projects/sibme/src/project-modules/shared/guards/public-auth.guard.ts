import { Injectable } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '@src/environments/environment';
import { Router, CanActivate } from "@angular/router";
@Injectable()
export class PublicAuthGuard implements CanActivate {

    public account_id: any;
    public header_data: any;
    constructor(private headerService: HeaderService, private router: Router) {
        // Dev : Aqib
        // Here we are calling the header api to check the role Id,
        // We can also set it to localStorage and then check from there
        // We can also use behaviorSubjects as well in future.
    }
    canActivate(): boolean | Observable<boolean> {

        let base_url = environment.baseHeaderUrl + `/angular_header_api/`;
        return this.headerService.getHeaderData(base_url).pipe(map(res => {
            // console.log('public auth');
            // debugger;
            if (res) {
                this.header_data = res
                this.account_id = this.header_data.user_current_account.roles.role_id;
                if (this.account_id == 100 || this.account_id == 110) {
                    return true
                } else {
                    this.router.navigate(['/page-not-found']);
                    return false
                }
            }
        }))
    }

}
