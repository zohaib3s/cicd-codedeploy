import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { HeaderService } from '@src/project-modules/app/services';

@Injectable()
export class GlobalExportGuard implements CanActivate {

    public header_data: any;
    constructor(private headerService: HeaderService, private router: Router) {
        
    }
    canActivate(): boolean | Observable<boolean> {

        this.header_data = this.headerService.getStaticHeaderData();
            
            if (this.header_data) {
              
                if (this.header_data.global_export_permission == true) {
                    return true
                } else {
                    this.router.navigate(['/page-not-found']);
                    return false
                }
            }
    }

}