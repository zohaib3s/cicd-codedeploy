import { EventEmitter, Injectable, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Subject, BehaviorSubject } from 'rxjs';
import { HeaderService } from '@src/project-modules/app/services';

@Injectable({
  providedIn: 'root'
})
export class TrackerFiltersService {

  @Output() filtersKeys: EventEmitter<any> = new EventEmitter();
  constructor(private http: HttpClient, private headerService: HeaderService) { }

  
  getReports(obj) {
    let url = `${environment.APIbaseUrl}/get_reports`;
    return this.http.post(url, obj);
  }
  
  updateReports(obj) {
    let url = `${environment.APIbaseUrl}/update_reports`;
    return this.http.post(url, obj);
  }
  

  getFrameWork(obj){
    let url = `${environment.APIbaseUrl}/getFramework/${obj.account_id}/${obj.framework_id}`;
    return this.http.post(url ,{});
  }

  getReport(obj, endPoint){
    console.log("in the service endpoint",endPoint);
    console.log("in the service object",obj)
    let url = `${environment.APIbaseUrl}/${endPoint}`;
    return this.http.post(url, obj);
  }

  exportReports(obj, reportType){
    console.log('obj: ',obj)
    // return false;
    let export_obj = obj;
    if(Boolean(obj.active)) {
      export_obj.search = "";
    }
    let path = `${environment.APIbaseUrl}/${reportType}`;
    return this.http.post(path, export_obj);
    
    // let form = document.createElement("form");
    // form.setAttribute("action", path);
    // form.setAttribute("method", "post");
    // document.body.appendChild(form);
    // this.appendInputToForm(form, obj);
    // console.log("TrackerFiltersService -> exportReports -> form", form);
    // let sessionData: any = this.headerService.getStaticHeaderData();
    // this.appendInputToForm(form, { "current_lang": sessionData.language_translation.current_lang });
    // form.submit();
    // document.body.removeChild(form);
  }

  exportDetailReports(obj, reportType){
    console.log('obj: ',obj)
    // return false;
    let path = `${environment.APIbaseUrl}/${reportType}`;
    // return this.http.post(path, obj);
    
    let form = document.createElement("form");
    form.setAttribute("action", path);
    form.setAttribute("method", "post");
    document.body.appendChild(form);
    this.appendInputToForm(form, obj);
    console.log("TrackerFiltersService -> exportReports -> form", form);
    let sessionData: any = this.headerService.getStaticHeaderData();
    this.appendInputToForm(form, { "current_lang": sessionData.language_translation.current_lang });
    form.submit();
    document.body.removeChild(form);
  }
  private appendInputToForm(form, obj) {
    Object.keys(obj).forEach((key) => {
      let input = document.createElement("input");
      if(key =="account_ids") input.setAttribute("value", JSON.stringify(obj[key]));
      else if(key =="huddle_ids") input.setAttribute("value", JSON.stringify(obj[key]));
      else if(key == "header"){
         input.setAttribute("value", JSON.stringify(obj[key]));
      }
      else input.setAttribute("value", obj[key]);
      input.setAttribute("name", key);
      form.appendChild(input);
    });
  }

  public deleteReport(obj){
    let url = `${environment.APIbaseUrl}/delete_report`;
    return this.http.post(url, obj);
  }

  public detailView(obj){
    let url = `${environment.APIbaseUrl}/detail_view`;
    return this.http.post(url, obj);
  }
  public getHuddleUserData(payload) {
    let path =  `${environment.APIbaseUrl}/huddle_report_users`;
    return this.http.post(path, payload);
  }
  public getAssessesForpage(payload) {
    let path =  `${environment.APIbaseUrl}/assessment_report_assessee`;
    return this.http.post(path, payload);
  }
}
