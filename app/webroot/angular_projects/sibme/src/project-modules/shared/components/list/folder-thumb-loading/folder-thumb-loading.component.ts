import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'shared-folder-thumb-loading',
  templateUrl: './folder-thumb-loading.component.html',
  styleUrls: ['./folder-thumb-loading.component.css']
})
export class SharedFolderThumbLoadingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
