import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'shared-folder-list-loading',
  templateUrl: './folder-list-loading.component.html',
  styleUrls: ['./folder-list-loading.component.css']
})
export class SharedFolderListLoadingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
