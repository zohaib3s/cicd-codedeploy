import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedFolderListLoadingComponent } from './folder-list-loading.component';

describe('FolderListLoadingComponent', () => {
  let component: SharedFolderListLoadingComponent;
  let fixture: ComponentFixture<SharedFolderListLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedFolderListLoadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedFolderListLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
