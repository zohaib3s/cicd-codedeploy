import { Component, Input, OnInit, OnDestroy, Output, EventEmitter, OnChanges } from '@angular/core';
import { Subscription } from 'rxjs';

import { HeaderService, AppMainService, HomeService } from "@app/services";
import { PageType } from "@app/interfaces";
import { isEmpty } from "@app/helpers";
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { MainService } from '@videoPage/services';
import { VideoCropModelComponent, ArtifactShareModelComponent, URLArtifactModalComponent, CategoryModalComponent, DeleteModalComponent, RenameModalComponent, DuplicateResourceModalComponent, DescriptionModalComponent } from "@shared/modals";

import { environment } from "@environments/environment";
import { ShowToasterService } from '@projectModules/app/services';
import { WorkspaceHttpService } from '@workspace/services';
// import { DescriptionModalComponent } from '../description-modal/description-modal.component';
import { ActivatedRoute, Router } from '@angular/router';
import { MoveArtifactDialogComponent } from '@src/project-modules/shared/modals/move-artifacts-dialog/move-artifacts-dialog.component';
import { DetailsHttpService } from '@src/project-modules/video-huddle/child-modules/details/servic/details-http.service';

@Component({
  selector: 'shared-artifact-grid-box',
  templateUrl: './artifact-grid-box.component.html',
  styleUrls: ['./artifact-grid-box.component.css']
})
export class ArtifactGridBoxComponent implements OnInit, OnDestroy, OnChanges {

  @Input('artifacts') artifacts: any;
  @Input('loading') loading: boolean = true;
  @Input('additionalData') additionalData: any;
  @Input('pageType') pageType: PageType;
  @Input('goalEvidence') goalEvidence: boolean;
  @Input('original_account') original_account: boolean;
  @Input('folderPath') folderPath: any;
  @Input('haveFolders') haveFolders: boolean;
  @Output() OnMoveShareFolder = new EventEmitter();

 // @Input() changeBackgroundColorArtifact:boolean;

  private headerData: any;
  public userAccountLevelRoleId: number | string = null;

  public translation: any = {};
  private subscription: Subscription;
  viewerObj: any = {}
  public isDropdownOpen: boolean = false;
  bsModalRef: BsModalRef;
  private userCurrAcc: any;
  private apiUrl: string;
  public isIEOpened=false;
  public permission_share_library:string = '1';
  public user_id_streamer: any;
  public folderId;
  sessionData: any;
  public notRequiredToChangeBackground:boolean =false;
  huddle_id_parent: any = null;
  
  private subscriptions: Subscription = new Subscription();

  public huddleHeaderData: any;
  public loaded: boolean = false;
  //public userAccountLevelRoleId: number | string = null;

  public changeBackgroundondrop:boolean;

  constructor(private router: Router, public headerService: HeaderService, private appMainService: AppMainService, private modalService: BsModalService, public mainService: MainService,
    public toastr: ShowToasterService, public workService: WorkspaceHttpService, private activateRoute: ActivatedRoute, private homeService: HomeService,public detailsService: DetailsHttpService,) {
    this.sessionData = this.headerService.getStaticHeaderData();
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
    this.isIEOpened=this.headerService.isOpenedInIE();

    this.subscriptions.add(this.detailsService.huddleHeaderData$.subscribe(data => {
      this.huddleHeaderData = data;
      console.log('Data in artifact grid box', data)
      this.loaded = true;
    }));

    //console.log('data to find',this.detailsService.staticHuddleHeaderData);
  }

  

  ngOnChanges(){
    this.folderPath ? this.homeService.setFolderPath(this.folderPath) : false;


  }

  ngOnInit() {


    
    this.activateRoute.params.subscribe(param =>{
      this.folderId = param.folderId
      this.huddle_id_parent = param.id
    })
    if (this.pageType == 'huddle-page') {
      this.apiUrl = 'view_page';
    }

    if (this.pageType == 'workspace-page') {
      this.apiUrl = 'workspace_video_page';
    }
    this.headerData = this.headerService.getStaticHeaderData();
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');

    if (Object.keys(this.headerData).length != 0 && this.headerData.constructor === Object) {
      this.userAccountLevelRoleId = this.headerData.user_permissions.roles.role_id;
      this.permission_share_library = this.headerData.user_permissions.UserAccount.permission_share_library;
      this.user_id_streamer = this.headerData.user_current_account.User.id;
    }

  }

  public getPermission(artifact: any, action: string, opp?:boolean) {
    if (this.pageType === 'workspace-page') return true;

    let obj = this.additionalData;
    let userId = this.headerData.user_current_account.User.id;
    if(this.pageType === 'huddle-page')
    {
        if (obj.huddle_type == '3') {
            if (this.additionalData.users_accounts.role_id != 125) {
                if (artifact.published && (userId == artifact.created_by || this.additionalData.role_id == 200) && (action == 'delete' || action == 'edit')) {
                    if (obj.dis_mem_del_video == 1) {
                        return true
                    }
                }

                if ((userId == artifact.created_by || this.additionalData.role_id == 200) && action == 'copy') {
                    return true;
                }

                if ((window.navigator.userAgent.search("Firefox") === -1) && artifact.published && (userId == artifact.created_by || obj.is_evaluator) && action == 'crop') {
                    return true;
                }

                if (action == 'download') {
                    return true;
                }

                if ((this.additionalData.role_id == 200 || userId == artifact.created_by) && action == 'rename') {
                    return true;
                }

                if ((userId == artifact.created_by || this.additionalData.role_id == 200) && action == 'duplicate') {
                    return true;
                }

            }
            else {
                return false;

            }


        }
        else if (obj.huddle_type == '2') {


            if (this.additionalData.users_accounts.role_id !== 125) {
              if (this.additionalData.role_id === 200 || (this.additionalData.role_id === 210 && userId === artifact.created_by)) {
                if (action === 'delete') {
                  return true;
                }
              }
                if (artifact.published && this.additionalData.role_id == 200 || (!!+obj.coachee_permissions && this.additionalData.role_id == 210 && userId == artifact.created_by)) {
                    if (obj.dis_mem_del_video == 1 && action == 'delete') {
                        return true;
                    }
                    if (action == 'rename') {
                        return true;
                    }
                    if (action == 'duplicate') {
                        return true;
                    }
                    if (artifact.published == 1 && action == 'copy') {
                        return true;
                    }
                    if (action == 'download') {
                        return true;
                    }

                    if ((window.navigator.userAgent.search("Firefox") === -1) && artifact.published || (this.additionalData.role_id == 210 && userId == artifact.created_by) || this.additionalData.role_id == 200) {
                        if ((userId == artifact.created_by || obj.is_evaluator) && action == 'crop') {
                            return true;
                        }

                    }

                }
                // else if( opp == true && userId == artifact.created_by  && artifact.published == 0 && action == 'delete') {
                //   return true;
                // }
            }
            else {
                return false;
            }
        }
        else {
            if (this.additionalData.users_accounts.role_id != 125) {
              if (this.additionalData.role_id == 200 || (this.additionalData.role_id == 210 && userId == artifact.created_by)) {
                    return true;
              }
            }
        }
    }
    else if(this.pageType === 'library-page')
    {
        if (this.additionalData.users_accounts.role_id != 125) {
            if((this.userAccountLevelRoleId == 115 || this.userAccountLevelRoleId == 120) && action == 'download' && this.additionalData.users_accounts.permission_video_library_download != 1)
            {
                return false;
            }
            if( action == 'duplicate' )
            {
                return false;
            }
            if(action == 'copy')
            {
               return true;
            }
            else if (this.userAccountLevelRoleId == 100 || this.userAccountLevelRoleId == 110 || (this.additionalData.users_accounts.permission_video_library_upload == 1 && action != 'download') || (this.additionalData.users_accounts.permission_video_library_download == 1 && action == 'download')) {
                return true;
            }
        }
    }
    return false;
  }

  /** Return permission for video, resource and scripted-note action buttons */
  public getActionPermission(artifact: any) {
    // if(artifact.doc_type == 1 && artifact.created_by == this.user_id_streamer && artifact.published == 0 ) {
    //   return true;
    // }
    if (this.pageType === 'workspace-page') {
      if (artifact.doc_type == 1 || (artifact.doc_type == 3 && (artifact.is_processed == 4 || artifact.is_processed == 5))) {// in case of video or sync-note
        return this.userAccountLevelRoleId != 125 && artifact.published == 1 || (artifact.transcoding_status == 5 || artifact.encoder_status == 'Error');
      } else if (artifact.doc_type == 2 || artifact.doc_type == 5 || (artifact.doc_type == 3 && artifact.is_processed < 4)) { // in case of resource or scripted-note or url artifact
        return this.userAccountLevelRoleId != 125
      } else return false;
    }

    else if (this.pageType === 'huddle-page') {
      if (artifact.doc_type == 1 || artifact.doc_type == 4) { // in case of video or live-stream
        return this.getPermission(artifact, 'delete') && (artifact.video_duration != 0 || artifact.video_is_saved == 0) && artifact.published == 1 || (artifact.transcoding_status == 5 || artifact.encoder_status == 'Error');
      } else if (artifact.doc_type == 2 || artifact.doc_type == 5) { // in case of resource or url artifact
        const userId = this.headerData.user_current_account.User.id;
        return this.userAccountLevelRoleId != 125 && ( (artifact.created_by == userId && this.additionalData.huddle_type != '2' ) || this.additionalData.role_id == 200 || this.getPermission(artifact, 'delete') );
      } else if (artifact.doc_type == 3) { // in case of scripted-note
        return this.getPermission(artifact, 'delete')
      } else return false;
    }
    else if (this.pageType === 'library-page')
    {
        if (artifact.doc_type == 1) {// in case of video
            return (this.userAccountLevelRoleId != 125 && artifact.published == 1 || (artifact.transcoding_status == 5 || artifact.encoder_status == 'Error')) && (this.additionalData.users_accounts.permission_video_library_upload == 1 || this.additionalData.users_accounts.permission_video_library_download == 1);
        } else return false;
    }
  }

  public userOwnStream() {
    this.toastr.ShowToastr('info',this.translation.ls_cannot_see_own_livestream)
  }

  public isOpen(artifact: any, flag) {
    this.isDropdownOpen = flag;
    if (flag) {
      artifact.mouseEntered = true;
    }
  }

  public getArtifactUrl(artifact: any) {

    if(artifact.doc_type == 5) { // in case of url artifact
      if(artifact.url.indexOf('http') < 0) return `http://${artifact.url}`;
      else return artifact.url;
    }

    if (artifact.doc_type == 2 && artifact.stack_url){ // in case of resource, huddle and workspace link is same

      if(this.headerData.enable_document_commenting == '0') {
        let path = environment.baseUrl + "/app/view_document" + artifact.stack_url.substring(artifact.stack_url.lastIndexOf("/"), artifact.stack_url.length);
        window.open(path, "_blank");
      } else {
        if (this.pageType === 'workspace-page') {
          let path = "/document-commenting/pdf-renderer/workspace-video-page/" + artifact.doc_id + artifact.stack_url.substring(artifact.stack_url.lastIndexOf("/")) + "/" + artifact.file_type + "/" + artifact.account_folder_id + "/" + artifact.parent_folder_id + "/" + false;
          this.router.navigate([path]);
        } if (this.pageType === 'huddle-page') {
          let path = "/document-commenting/pdf-renderer/huddle-video-page/" + artifact.doc_id + artifact.stack_url.substring(artifact.stack_url.lastIndexOf("/")) + "/" + artifact.file_type + "/" + artifact.account_folder_id + "/" + artifact.parent_folder_id + "/" + false;
          this.router.navigate([path]);
        }
      }
    }
    else if (artifact.doc_type == 2 && !artifact.stack_url){ // if there is no stack_url of a resource, download that particular resource instead of view in new tab
      return environment.baseUrl +"/Huddles/download/" + artifact.doc_id;
    }

    const urlParams = `${artifact.account_folder_id}/${artifact.doc_id}`;
    if (this.pageType === 'huddle-page') {
      if (artifact.doc_type == 1) return `/video_details/home/${urlParams}`;
      else if (artifact.doc_type == 3) return `/video_details/scripted_observations/${urlParams}`;
      else if (artifact.doc_type == 4) return `/video_details/live-streaming/${urlParams}`;
    } else if (this.pageType === 'workspace-page') {
      if (artifact.doc_type == 1) return `/workspace_video/home/${urlParams}`;
      else if (artifact.doc_type == 3 && artifact.is_processed < 4) return `/video_details/scripted_observations/${urlParams}`;
      else if (artifact.doc_type == 3 && artifact.published == 0 &&  artifact.doc_id && artifact.upload_progress <= 100) // in case of sync-note
        return `/workspace_video/video_observation/${urlParams}`;
      else if (artifact.doc_type == 3 && (artifact.published == 1) && artifact.upload_progress <= 100 && artifact.video_duration > 0)
        return `/workspace_video/home/${urlParams}`;
    }  else if (this.pageType === 'library-page') {
        if (artifact.doc_type == 1) return `/library_video/home/${urlParams}`;
    }
  }

   /** Set query-params workspace in case of workspace scripted note for the appropriate return url from scripted note detail page */
   public getArtifactQueryParams(artifact: any) {
    let queryParams: any = {};
    if (artifact.doc_type == 3 && artifact.is_processed < 4 && this.pageType === 'workspace-page') queryParams.workspace = true;
    if(this.goalEvidence) queryParams.goalEvidence = true;

    if(isEmpty(queryParams)) return '';
    else return queryParams;
  }
  checkPreview(artifact: any){
    if(artifact.file_type == 'csv' && artifact.ready_for_download == 1 ){
      return true;
    } else {
      return false;

    }
  }
  DownloadResource(res) {
    if (res.static_url && false) {
    } else {
      let obj: any = {};
      obj.document_id = res.doc_id;
      obj.account_id = res.account_id;
      ({
        User: {
          id: obj.user_id
        }
      } = this.headerData.user_current_account);

      this.workService.DownloadResource(obj);
    }
    this.toastr.ShowToastr('info',this.translation.artifacts_your_file_is_downloading)
  }

  public getArtifectTooltip(artifect){
    return `${artifect.title}.${artifect.file_type}`
  }


  DuplicateResource(artifact: any) {
    let obj: any = {};

    obj.document_id = artifact.doc_id;
    obj.account_folder_id = [artifact.account_folder_id];
    obj.current_huddle_id = artifact.account_folder_id;
    obj.account_id = artifact.account_id;
    obj.copy_notes = 1;
    obj.is_duplicated = true;
    obj.shareAssets = false;
    obj.doc_type = artifact.doc_type;
    obj.parent_folder_id = (this.folderId) ? this.folderId : 0;

    ({ User: {id: obj.user_id} } = this.headerData.user_current_account);

    if(this.pageType === 'workspace-page' ){
      obj.workspace=true,
      obj.from_workspace=1;
    }

    this.workService.DuplicateResource(obj).subscribe(data => {
      let d: any = data;
      let type = this.headerService.isAValidAudio(d.data.file_type);
      if (type == false) {
        this.toastr.ShowToastr('info',d.message);
      }
      else {
        this.toastr.ShowToastr('info',this.translation.audio_duplicate_successfully);
      }
    }, error => {
      this.toastr.ShowToastr('error',error.message)

    });
  }

  openRenameModal(artifact) {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'rename-modal-container',
      initialState: {
        artifact: artifact,
        pageType: this.pageType,
      }
    };

    this.bsModalRef = this.modalService.show(RenameModalComponent, config);
  }
  openDescriptionModal(artifact) {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'description-modal-container',
      initialState: {
        artifact: artifact,
        pageType: this.pageType,
      }
    };

    this.bsModalRef = this.modalService.show(DescriptionModalComponent, config);
  }
  openCategoryModal(artifact) {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'catergory-modal-container',
      initialState: {
        artifact: artifact,
        pageType: this.pageType,
      }
    };

    this.bsModalRef = this.modalService.show(CategoryModalComponent, config);
  }


  OpenDeleteModal(artifact) {
    console.log('call here');
    //debugger;
    //return;
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'delete-modal-container',
      initialState: {
        artifact: artifact,
        pageType: this.pageType,
        huddle_type: this.additionalData.huddle_type
      }
    };

    this.bsModalRef = this.modalService.show(DeleteModalComponent, config);
  }

  OpenDuplicateResourceModal(artifact) {

  if(artifact.total_comments > 0 || artifact.total_attachment > 0 || artifact.doc_type == 2){
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'duplicate-modal-container',
      initialState: {
        artifact: artifact,
        pageType: this.pageType,
        parent_folder_id: (this.folderId) ? this.folderId : 0
      }
    };

    this.bsModalRef = this.modalService.show(DuplicateResourceModalComponent, config);
  }
  else this.DuplicateResource(artifact);
}

  public openCropModal(artifact: any) {
    const initialState = {
      artifact: artifact,
      pageType: this.pageType,
      parent_folder_id: (this.folderId) ? this.folderId : 0
    };

    this.modalService.show(VideoCropModelComponent, { initialState, class: 'crop-modal-container',backdrop:'static' });
  }

  public openNewShareModal(artifact: any) {
    const initialState = { artifact, pageType: this.pageType, modal_for:'for_artifact_share' };
    this.modalService.show(ArtifactShareModelComponent, { initialState, class: 'share-modal-container',backdrop:'static' });
  }

  public openURLArtifactModal(artifact: any) {
    const initialState = {
      pageType: this.pageType,
      title: artifact.title,
      url: artifact.url,
      docomentId: artifact.document_id || artifact.doc_id,
      pageMode: 'edit'
     };
    this.modalService.show(URLArtifactModalComponent, { initialState, class: 'url-artifact-modal-container',backdrop:'static'});
  }

  public updateURLViews(artifact: any) {
    const data = {
      document_id: artifact.document_id || artifact.doc_id,
      account_id: this.headerData.user_current_account.users_accounts.account_id,
      user_id: this.headerData.user_current_account.User.id
    }
    this.appMainService.updateURLViews(data).subscribe();
  }

  onMoveFolder(artifact) {
    console.log('test12222222');

    let huddleId;
    this.activateRoute.params.subscribe(param => {
      huddleId = param.id;
    })
    const initialState = { artifactId: artifact.document_id ? artifact.document_id : artifact.doc_id, huddleId: huddleId, folderId: this.folderId, haveFolders: this.haveFolders }
    this.bsModalRef = this.modalService.show(MoveArtifactDialogComponent, { ariaDescribedby: 'my-modal-description', ariaLabelledBy: 'my-modal-title', initialState });
  }

  callTochange(event){
    event.preventDefault();
    console.log('event',event);
    let catchEvent = event.dataTransfer.effectAllowed;
    console.log('finalevent',event.dataTransfer.effectAllowed)
    if(this.pageType !== 'library-page' && catchEvent == 'all'){

      this.changeBackgroundondrop = true;
    }
      
    
    //this.changeBackgroundondrop = true;

  }

  changeBackgroundDefault(event){
    event.preventDefault();
    if(!event._dndDropzoneActive){
      this.changeBackgroundondrop = false;
    }

  }

  // tocher(event){
  //   //event.preventDefault();
  //   console.log('here come');
  //   this.notRequiredToChangeBackground = true;
  //   this.changeBackgroundondrop = false;
  // }

  uploadFiles(event){

    //console.log('additiondata',this.additionalData);

    //return;

    if(this.pageType === 'library-page'){

      return;
    }

    this.changeBackgroundondrop = false;

   if(this.userAccountLevelRoleId != 125 && ((this.userAccountLevelRoleId == 120 && this.sessionData.user_permissions.UserAccount.permission_video_workspace_upload == 1) || this.userAccountLevelRoleId != 120)){

   
   }else{
    this.toastr.ShowToastr('error', 'You do not have permissions to upload the files');

     return false;

   }

   if(this.userAccountLevelRoleId !='125' && !(this.additionalData.role_id==220 && this.additionalData.huddle_type=='1' )){

    //console.log('permission access 123');
  }else{
    console.log('permission denied 3456789');
    this.toastr.ShowToastr('error', 'You do not have permissions to upload the files');
    return false;
  }

    var files;
    event.preventDefault();
    var data = event.dataTransfer.items;
    files = data;
    //console.log('files',files);
    let that = this;
    let obj: any = {};
    obj.user_current_account = that.sessionData.user_current_account;
    obj.account_folder_id = null;
    obj.huddle_id = null;
    // filestack handle key added
    // filestack url added
    obj.account_id = that.sessionData.user_current_account.accounts.account_id;
    obj.site_id = that.sessionData.site_id;
    obj.user_id = that.sessionData.user_current_account.User.id;
    obj.current_user_role_id = that.sessionData.user_current_account.roles.role_id;
    obj.current_user_email = that.sessionData.user_current_account.User.email;
    obj.suppress_render = false;
    obj.suppress_success_email = false;
    obj.workspace = this.pageType=='workspace-page'?true : false;
    obj.activity_log_id = "";
    obj.video_id = "";
    obj.direct_publish = true;
    obj.url_stack_check = 1;
    obj.parent_folder_id = this.folderId ? this.folderId : 0

    if(this.huddle_id_parent) {
      obj.account_folder_id = this.huddle_id_parent;
      obj.huddle_id = this.huddle_id_parent;
    }

    this.headerService.UpdateUploadFilesList({files, obj})

    // if(this.huddle_id_parent) {
    //   obj.huddle_id = this.huddle_id_parent;
    // }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
