import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeaderService } from "@projectModules/app/services";
import { ArtifactShareModelComponent } from '@src/project-modules/shared/modals';
import { Subscription } from 'rxjs';
import { PageType } from "@app/interfaces";
import { BsModalService } from 'ngx-bootstrap/modal';
@Component({
  selector: 'shared-folder-list-view',
  templateUrl: './folder-list-view.component.html',
  styleUrls: ['./folder-list-view.component.css']
})
export class SharedFolderListViewComponent implements OnInit, OnDestroy {

  @Input() isHuddle;
	@Input() data;
  @Input() block_move;
  @Output() OnEdit = new EventEmitter();
  @Output() OnMove = new EventEmitter();
  @Output() OnDelete = new EventEmitter();
  @Input('pageType') pageType: PageType;
  @Input('additionalData') additionalData: any;

  @Input() changeBackgroundColor:boolean;
  @Input() dragFolderId:any;

  public header_data;
  public displayStyle;
  public huddleId;
  public translation: any = {};
  private subscription: Subscription;
	public getHuddlesCount(stats){

		return Number(stats.collaboration) + Number(stats.coaching) + Number(stats.assessment);

	}

  constructor(public headerService:HeaderService, private activatedRoute: ActivatedRoute, private modalService: BsModalService) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
			this.translation = languageTranslation;
		});
   }

  ngOnInit() {
    this.header_data = this.headerService.getStaticHeaderData();
    this.activatedRoute.params.subscribe((p) => {
      this.displayStyle = p.displayStyle;
      this.huddleId = p.id;
    });
  }

  public EditFolder(id){
    this.OnEdit.emit(id);
  }

  public getPermission() {
    if(this.pageType == 'workspace-page') {
      return true
    } else {
      let userId = this.header_data.user_current_account.User.id;
      if (this.additionalData?.users_accounts.role_id != 125) {
        if (userId == this.data.created_by || this.additionalData.role_id == 200) {
          if (this.additionalData.dis_mem_del_video == 1) {
              return true
          } 
        } 
      } else {
        return false;
      }
    }
  }

  public shareFolderModal(artifact){
    const initialState = { artifact, pageType: this.pageType, modal_for:'for_folder_share' };
    this.modalService.show(ArtifactShareModelComponent, { initialState, class: 'share-modal-container',backdrop:'static' });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }


}
