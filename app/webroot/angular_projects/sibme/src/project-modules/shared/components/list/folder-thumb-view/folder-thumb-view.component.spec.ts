import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedFolderThumbViewComponent } from './folder-thumb-view.component';

describe('FolderThumbViewComponent', () => {
  let component: SharedFolderThumbViewComponent;
  let fixture: ComponentFixture<SharedFolderThumbViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedFolderThumbViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedFolderThumbViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
