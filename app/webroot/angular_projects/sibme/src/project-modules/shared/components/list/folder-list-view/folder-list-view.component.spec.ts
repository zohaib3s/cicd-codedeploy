import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedFolderListViewComponent } from './folder-list-view.component';

describe('FolderListViewComponent', () => {
  let component: SharedFolderListViewComponent;
  let fixture: ComponentFixture<SharedFolderListViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedFolderListViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedFolderListViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
