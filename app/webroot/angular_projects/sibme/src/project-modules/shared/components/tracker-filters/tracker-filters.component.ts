import { Component, OnInit, OnDestroy, ViewChild, Output, EventEmitter, Input, SimpleChanges, OnChanges, ElementRef } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { forkJoin, Subject, Subscription } from 'rxjs';
import { MainService } from '@src/project-modules/trackers/services';
import { BsDatepickerConfig, BsDaterangepickerDirective } from 'ngx-bootstrap/datepicker';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { BsModalService, BsModalRef, ModalDirective, ModalOptions } from 'ngx-bootstrap/modal';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { debounceTime, distinctUntilChanged, filter, startWith } from "rxjs/operators";
import * as moment from "moment";

import { TrackerFiltersService } from '../../services/tracker-filters.service';
import { DragulaService } from 'ng2-dragula';
import { LessThanEqualValidator } from 'ng2-validation/dist/less-than-equal/directive';
import { startCase } from 'lodash';
import { ShowToasterService } from '@projectModules/app/services';
import { isEmpty } from "lodash"
import { Location } from '@angular/common';
import * as _ from 'lodash';
import autoScroll from 'dom-autoscroller';
import { deepCopy, monthDiff, daysBetween } from '../common/utility';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { listLocales } from 'ngx-bootstrap/chronos';
import { BodyService } from 'src/project-modules/analytics/services/body.service';

@Component({
  selector: "trackers-filters",
  templateUrl: "./tracker-filters.component.html",
  styleUrls: ["./tracker-filters.component.css"]
})
export class TrackerFiltersComponent implements OnInit, OnChanges, OnDestroy {
  locale = 'en';
  locales = listLocales();
  private subscriptions: Subscription = new Subscription();
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  @ViewChild('configModal', { static: false }) configModal: ModalDirective;
  @ViewChild('standardsModal', { static: false }) standardsModal: ModalDirective;
  @ViewChild('deleteReportModal', { static: false }) deleteReportModal: ModalDirective;
  @ViewChild('search') stickyDiv: ElementRef;
  @ViewChild("loadingModal", { static: false }) loadingModal;
  bsModalRef: BsModalRef;
  deleteModalRef: BsModalRef;
  SearchString = ''
  @Output() SearchEvent = new EventEmitter
  @Output() isLoading = new EventEmitter
  public userCurrAcc;
  @Input() report_type: any;
  @Input() isWorSpaceAllow: boolean
  @Input() exportReport: any;
  @Input() currentPage: any;
  @Input() itemsPerPage: any;
  @Input() callsFunctionFormFilter: boolean = true;
  @Input() routeOnDuration: boolean = true;
  @Input() isSingleSelection: boolean = false;
  @Input() fromAnalytics: boolean = false;
  public translation: any = {};
  public account_id: any;
  public user_id: any;
  public chlidAccounts = [] as any;
  public huddles = [] as any;
  public frameworks = [] as any;
  public standards = [] as any;
  public framework_id = '' as any;
  public bsConfig: Partial<BsDatepickerConfig>;
  public reportConfigs = []
  public selectedReport = {} as any;
  public reportName
  public header_data: any;
  bsRangeValue: Date[];
  maxDate = new Date();
  bsValue = new Date();
  date = '1month';
  account = '' as any;
  huddleType = '1' as any;
  dropdownList: any;
  selectedItems = [];
  dropdownSettings;
  dropdownSettingsSingle;
  standardSettings;
  selectedAccounts = [] as any;
  selected_accounts = [] as any;
  selectedFramework = [] as any
  standard_ids = [] as any;
  public filters_obj = {
    account_ids: []
  } as any;
  index: any;
  reportEditMode = false;
  indexToUpdate = null;
  searchStandards = '';
  search: String;
  // checkBoxes: any;
  loading = true;
  deletedReportIndex: any;
  isButtonDisable = false;
  private searchQuery: Subject<string> = new Subject();


  @Output() filterData = new EventEmitter;
  @Output() filterKeyEmitter = new EventEmitter();
  @Output() isDataLoad = new EventEmitter();
  attributes: any;
  reportSubscription: Subscription;
  duration = "1";
  queryParams = null;
  dropdownSettingsHuddle;
  selectedHuddles = [] as any;
  byDefaultHuddle = [{ id: 2 }] as any;
  reportsLoading: boolean;
  playCardUserId: null;
  playCardAccountId = null;
  queryParamsSubscription: Subscription = new Subscription();
  @ViewChild(BsDaterangepickerDirective, { static: false }) dateRangePickerObj: BsDaterangepickerDirective;
  @ViewChild('dp', { static: false }) dp;
  dropdownSettingsFramework: {};
  singleUserPlayer = [];
  role_id: any;
  private loadingModalRef: BsModalRef;
  constructor(
    public headerService: HeaderService,
    public trackerMainService: MainService,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    private modalService: BsModalService,
    private tracketFiltersService: TrackerFiltersService,
    private DG: DragulaService,
    private toastr: ShowToasterService,
    private location: Location,
    private localeService: BsLocaleService,
    private bodyService: BodyService,
  ) {
    this.header_data = this.headerService.getStaticHeaderData();
    if (Object.keys(this.header_data).length != 0 && this.header_data.constructor === Object) {
      this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');


      this.account_id = this.userCurrAcc.users_accounts.account_id;
      this.user_id = this.userCurrAcc.User.id;
      this.role_id = this.userCurrAcc.roles.role_id;


      this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
        this.translation = languageTranslation;
        this.locale = this.translation?.language_setting_js;


        this.localeService.use(this.locale);
      }));



      this.searchQuery.pipe(debounceTime(1000), distinctUntilChanged()).subscribe(() => {
        this.update();
      });
      // this.huddles = [
      //   { id: 1, value: 'Coach View' }, { id: 2, value: 'Coachee View' }, { id: 3, value: 'Assessor View' },
      //   { id: 4, value: 'Assessee View' }, { id: 5, value: 'Collaboration' }, { id: 6, value: 'Workspace' },
      //   { id: 8, value: 'Assessment Huddles' }
      // ]

      this.huddles = [
        { id: 6, value: 'Workspace' },
        { id: 5, value: 'Collaboration' },
        { id: 1, value: 'Coach View' },
        { id: 2, value: 'Coachee View' },
        { id: 3, value: 'Assessor View' },
        { id: 4, value: 'Assessee View' },
        { id: 8, value: 'Assessment Huddles'}
      ]
      this.router.events
        .pipe(
          filter((event) => event instanceof NavigationEnd),
          startWith(this.router)
        )
        .subscribe((event: NavigationEnd) => {
          if (activatedRoute.snapshot.firstChild && activatedRoute.snapshot.firstChild) {
            let params = activatedRoute.snapshot.firstChild.params;
            if (this.router.url.includes("playercard")) {
              this.playCardAccountId = params['account_id'];
              this.playCardUserId = params['user_id'];
              // this.getFiltersData();
            }
          }
        })
    }
  }

  ngOnInit() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    };
    this.filters_obj.page = this.currentPage || 1;
    this.filters_obj.limit = this.itemsPerPage || 10;
    this.DG.dropModel("checkbox").subscribe((args: any) => {
      this.arraymove(this.reportConfigs[this.index].report_attributes, args.sourceIndex, args.targetIndex);
    });


    if (this.fromAnalytics) {


      this.maxDate.setFullYear(this.bsValue.getFullYear() - 1);
      this.duration = "12";
      this.date = "12months";
      this.bsRangeValue = [this.maxDate, this.bsValue];
    } else {
      this.maxDate.setDate(this.maxDate.getDate() - 30);
      this.bsRangeValue = [this.maxDate, this.bsValue];
    }
    if (this.activatedRoute.firstChild) {
      this.subscriptions.add(this.activatedRoute.firstChild.params.subscribe(activatedRouteParams => {

        if (activatedRouteParams['start_date'] && activatedRouteParams['start_date']) {
          this.maxDate = moment(activatedRouteParams['start_date']).toDate();
          this.bsValue = moment(activatedRouteParams['end_date']).toDate();
          this.duration = monthDiff(this.maxDate, this.bsValue);
          this.date = this.duration + "months";
          this.bsRangeValue = [this.maxDate, this.bsValue];
        }
      }));
    }
    if (Object.keys(this.header_data).length != 0 && this.header_data.constructor === Object) {
      this.init();
      this.filters_obj.account_id = this.account_id;
      this.filters_obj.user_id = this.user_id;
      this.filters_obj.user_role = this.userCurrAcc.roles.role_id
    }
    // this.getChildAccounts();
    // this.getFiltersData();




    // if (this.callsFunctionFormFilter) {
    //   this.update();
    // }
    // if (this.routeOnDuration) {
    //   this.activatedRoute.queryParams.subscribe((param: any) => {
    //     ['1month', '3months', '6months', '12months'].includes(param.date) ? this.setParams(param.date) : ''
    //   })
    // }

  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.queryParamsSubscription.add(this.activatedRoute.queryParams.subscribe((queryParams: any) => {

      if (queryParams.search) {
        this.search = queryParams.search
      }

      if (queryParams && typeof queryParams['account_ids'] === 'string' || queryParams['account_ids'] instanceof String) {
        queryParams = Object.assign({}, queryParams);
        queryParams['account_ids'] = [queryParams['account_ids']];
      }
      if (queryParams && typeof queryParams['standard_ids'] === 'string' || queryParams['standard_ids'] instanceof String) {
        queryParams = Object.assign({}, queryParams);
        queryParams['standard_ids'] = [queryParams['standard_ids']];
      }

      if (queryParams && typeof queryParams['huddle_ids'] === 'string' || queryParams['huddle_ids'] instanceof String) {
        queryParams = Object.assign({}, queryParams);
        queryParams['huddle_ids'] = [queryParams['huddle_ids']];
      }
      this.queryParams = Object.assign({}, queryParams);
      if (this.queryParams.hasOwnProperty('from')) {
        delete this.queryParams['from']
      }

      if (isEmpty(this.queryParams)) {
        this.getApiCAllFilterData();
        setTimeout(() => {
          this.update();
        }, 50);
      } else {
        this.getApiCAllFilterData();

      };
      this.queryParamsSubscription.unsubscribe();
    }));
  }

  arraymove(arr, fromIndex, toIndex) {
    let element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);

    // this.standards = this.standards;

  }


  public getChildAccounts() {
    this.trackerMainService.getChildAccounts({ account_id: this.account_id }).subscribe((data: any) => {
      this.chlidAccounts = data;
    });
  }



  getApiCAllFilterData() {

    let account_ids = !isEmpty(this.queryParams) && this.queryParams.account_ids ? this.queryParams.account_ids : this.getFields(this.selectedAccounts, 'id');
    // account_ids = account_ids.length > 0 ? account_ids.length : [this.account_id]
    const requests = [
      this.trackerMainService.getChildAccounts({ account_id: this.account_id }),
      this.trackerMainService.getFiltersData(this.account_id, { account_ids: account_ids, user_id: this.playCardUserId })
    ];

    forkJoin(requests).subscribe((results: any) => {

      if (Array.isArray(results) && results.length > 0) {
        this.chlidAccounts = results[0];
        this.getChildAccountsData();
      }

      if (Array.isArray(results) && results.length > 1) {
        const huddlesIndexes = [];
        this.ObjectToArray(results[1].huddles_filters).forEach(x => {
          huddlesIndexes.push(x.id);
        });
        const huddles = [
          { id: 7, value: 'All' },
          { id: 6, value: 'Workspace' },
          { id: 5, value: 'Collaboration' },
          { id: 1, value: 'Coaching Huddles - Coach View' },
          { id: 2, value: 'Coaching Huddles - Coachee View' },
          { id: 3, value: 'Assessment Huddles - Assessor View' },
          { id: 4, value: 'Assessment Huddles - Assessee View' },
          { id: 8, value: 'Assessment Huddles'}
        ];
        this.huddles = huddles.filter(h => {
          return huddlesIndexes.indexOf(h.id.toString()) !== -1;
        });
        // this.huddles = this.ObjectToArray(results[1].huddles_filters);
        // this.huddles.splice(0, 0, this.huddles.splice(5, 1)[0]);
        // this.huddles.splice(1, 0, this.huddles.splice(5, 1)[0]);
        // this.huddles.splice(4, 0, this.huddles.splice(5, 1)[0]);
  
        this.makeHuddleDropDowns();

        this.selectHuddle();
        this.frameworks = results[1].frameworks;
      }
      this.isDataLoad.emit(true);
      if (!isEmpty(this.queryParams)) {
        this.setValuesFormQueryParams();
        // if (this.routeOnDuration) {
        //   ['1month', '3months', '6months', '12months'].includes(this.queryParams.date) ? this.setParams(this.queryParams.date) : ''
        // }
        this.queryParams = null;
      }
    });
  }

  makeHuddleDropDowns() {
    console.log(this.report_type);

    // this.huddles = this.huddles.reverse();
    let deepArray = deepCopy(this.huddles);
    let indexAll = deepArray.findIndex(x => x.id == 7);
    if (indexAll != -1) {
      let allObject = deepArray[indexAll];

      deepArray.splice(indexAll, 1)
      deepArray.push(allObject);
      this.huddles = [...deepArray];
      // this.huddles = this.huddles;
    }
    if (this.report_type == 'coaching_report') {
      this.huddles = this.huddles.filter(x => {
        // return x.value !== 'Workspace' && x.value !== 'Assessor View' && x.value !== 'Assessee View' && x.value !== 'Collaboration'
        return x.id != 3 && x.id != 4 && x.id != 5 && x.id != 6 && x.id != 7 && x.id != 8
      });

    }

    if (this.report_type == 'huddle_report') {
      this.huddles = this.huddles.filter(x => {
        return x.id != 6 && x.id != 7
      });
     
    }
    if (this.fromAnalytics) {
      let indexAll = this.huddles.findIndex(x => x.id == 7);
      if (indexAll != -1) {
        this.huddles.splice(indexAll, 1);
        this.selectedHuddles = this.huddles;
       
      }
      

    }

    // if (this.fromAnalytics && this.report_type !== 'user_summary_report' || this.report_type !== 'playercard-user-summary') {
    //   let workSpaceIndex = this.huddles.findIndex(x => x.id == 6);
    //   if (workSpaceIndex !== -1) {
    //     this.huddles.splice(workSpaceIndex, 1)
    //   }
    // }
    // if(this.huddles[0].id != 6 && this.report_type != 'coaching_report'){
    //   this.huddles.splice(0, 0, this.huddles.splice(5, 1)[0]);
    //     this.huddles.splice(1, 0, this.huddles.splice(5, 1)[0]);
    //     this.huddles.splice(4, 0, this.huddles.splice(5, 1)[0]);
    // }
  

  }


  getChildAccountsData() {
    if (this.playCardAccountId) {
      this.singleUserPlayer = [...this.chlidAccounts.filter(x => x.id == this.playCardAccountId)];

      return this.singleUserPlayer;
    } else {
      return this.chlidAccounts;
    }
  }

  public getFiltersData() {
    this.getChildAccountsData();
    let account_ids = this.getFields(this.selectedAccounts, 'id');
    // account_ids = account_ids.length > 0 ? account_ids.length : [this.account_id]
    this.trackerMainService.getFiltersData(this.account_id, { account_ids: account_ids, user_id: this.playCardUserId }).subscribe((data: any) => {
      this.huddles = this.ObjectToArray(data.huddles_filters);
      this.makeHuddleDropDowns();
      this.frameworks = data.frameworks;
      if (this.framework_id) {
        const frameWork = this.frameworks.find(f => f.account_tag_id == +this.framework_id);
        if (frameWork) {
          this.framework_id = frameWork.account_tag_id;
        } else {
          this.framework_id = "";
        }
        if (data.standards) {
          this.standards = data.standards.map(obj => ({ ...obj, value: obj.account_tag_id, label: obj.tag_title }))
        }

        this.standard_ids = [];
        this.standards.forEach(element => {
          element.status = true;
          this.standard_ids.push(element.value)
        });
      }
      else {
        this.standards = [];
        this.standard_ids = [];
      }
    });
  }



  setParams(param: string) {

    this.date = param;
    this.maxDate = new Date();
    if (['1month', '3months', '6months', '12months'].includes(this.date)) {
      let m = this.date.split("(?<=\d)(?=\D)")
      this.maxDate.setDate(this.maxDate.getDate() - parseInt(m[0]) * 30)
      this.bsRangeValue = [this.maxDate, this.bsValue]
    }
    this.router.navigate(['/trackers/coaching-report'], { queryParams: { date: param } });

  }

  onDuration(dur) {
    this.duration = dur;
    if (dur) {
      let date_range = [moment().subtract(dur, 'months'), moment()];
      this.bsRangeValue = [date_range[0].toDate(), date_range[1].toDate()];
      this.update(true)
      return;
    }
    if (!this.routeOnDuration) {
      return false;
    }


    // this.router.navigate(
    //   [],
    //   {
    //     relativeTo: this.activatedRoute,
    //     queryParams: { date: prams },
    //     queryParamsHandling: 'merge'
    //   });
  }

  setDate(value) {
    this.date = value;
  }
  getReports() {
    this.reportsLoading = true
    this.tracketFiltersService.getReports({
      report_type: this.report_type,
      account_id: this.account_id
    })
      .subscribe((res: any) => {
        if (res.success) {
          this.reportConfigs = res.report_configuration;
          if (!this.selectedReport?.id) {
            this.selectedReport = this.reportConfigs[0];
            this.index = 0;

            // this.selectedReport = {} as any;
          }
          this.attributes = res.Report.attributes
          this.reportsLoading = false
        }
      });
  }

  showConfigModal() {
    this.isButtonDisable = false;

    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-container-700',
    };
    this.getReports()
    this.reportName = '';
    this.bsModalRef = this.modalService.show(this.configModal, config);


  }

  getReportTypeInString(reportType) {
    reportType = reportType.split('_')


    return startCase(reportType[0]) + " " + startCase(reportType[1])
  }

  showStandardModal() {
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-container-700',
    };
    this.bsModalRef = this.modalService.show(this.standardsModal, config);
    if (this.standard_ids) {
      this.standards.map(item => {
        this.standard_ids.includes(item.value) ? item.status = true : item.status = false
      })
    }


  }

  selectFramework(event) {
    // this.framework_id = event.target.value;
    this.framework_id = this.selectedFramework[0].account_tag_id
    if (this.framework_id) {
      this.tracketFiltersService.getFrameWork({ account_id: this.playCardAccountId ? this.playCardAccountId : this.account_id, framework_id: this.framework_id }).subscribe((data: any) => {

        this.standard_ids = [];
        this.standards = data;

        this.standards.forEach(element => {
          element.status = true;
          this.standard_ids.push(element.value)
        });

      });
    } else {
      this.standards = [];
      this.standard_ids = [];
    };



  }

  onFrameworkDeselect() {
    this.framework_id = '';
    this.standards = [];
    this.standard_ids = [];
  }

  selectStandard(value, checked) {
    value.status = checked;
    !checked ? this.standard_ids = this.standard_ids.filter(x => x !== value.value) : this.standard_ids.push(value.value);

  }

  getSelectedReport(config, index) {


    this.index = index;
    this.reportConfigs.map(x => {
      x.active = false
    })
    config.active = true;
    this.selectedReport = deepCopy(config);
    this.filters_obj.report_id = this.selectedReport.id
    let divScroll = document.getElementById("dragg-scroll")
    divScroll.scrollTop = 0
    // for movement of scroll on drag and drop
    setTimeout(() => {
      autoScroll([
        divScroll], {
        margin: 35,
        maxSpeed: 4,
        scrollWhenOutside: false,
        autoScroll() {
          return this.down
        }
      });
    }, 1000);

  }

sortTable(key, order){
this.filters_obj.sort_by = key;
this.filters_obj.sort_by_order  = order;
this.update();
}

  update(updateClick = false) {
    if (this.report_type == 'coaching_report') {
      this.selectedHuddles = this.selectedHuddles.filter(x => {
        // return x.value !== 'Workspace' && x.value !== 'Assessor View' && x.value !== 'Assessee View' && x.value !== 'Collaboration'
        return x.id != 3 && x.id != 4 && x.id != 5 && x.id != 6 && x.id != 7 && x.id != 8
      });

    }
    if (this.report_type == 'huddle_report') {
      this.selectedHuddles = this.selectedHuddles.filter(x => {
        return x.id != 6 && x.id != 7
      });
    }

    if (this.fromAnalytics) {
      let indexAll = this.huddles.findIndex(x => x.id == 7);
      if (indexAll != -1) {
        this.huddles.splice(indexAll, 1)
      }
    }

    // this.loading = true;
    if (!this.bsRangeValue) {
      this.toastr.ShowToastr('error',this.translation.reporting_please_select_date_range || ' Please select a date range.');
      return;
    }
    if (this.dateRangePickerObj && this.dateRangePickerObj._bsValue.length > 0 && !this.dateRangePickerObj._bsValue[0]) {
      this.filters_obj.start_date = "";
      this.filters_obj.end_date = "";
      this.onDuration(1)
    }
    this.isLoading.emit(true)
    this.filters_obj['huddle_type'] = this.huddleType;
    this.filters_obj.start_date = this.convertDate(this.bsRangeValue[0]);
    this.filters_obj.end_date = this.convertDate(this.bsRangeValue[1]);

    // let accounts = this.selectedAccounts.map(a => a.id);
    if (Array.isArray(this.selectedAccounts)) {
      this.selectedAccounts.forEach(element => {
        const checkAccount = this.selected_accounts.some(x => x == element.id);
        if (!checkAccount) {
          this.selected_accounts.push(element.id);
        }
      });
    }
    this.filters_obj.account_id = this.account_id;
    this.filters_obj.user_id = this.user_id;
    this.filters_obj.user_role = this.role_id
    this.filters_obj.role_id = this.role_id;
    this.filters_obj.account_ids = this.selected_accounts;
    // this.filters_obj.isAllAccount = this.chlidAccounts.length == this.filters_obj.account_ids ? 7 : 0;
    this.filters_obj.huddle_ids = this.getFields(this.selectedHuddles, "id");
    this.filters_obj.framework_id = this.framework_id;
    this.filters_obj.active = false

    if (this.standard_ids.length > 0) {
      this.filters_obj.standard_ids = this.standard_ids;
    } else delete this.filters_obj.standard_ids;


    this.filters_obj.duration = this.duration;
    if (this.reportSubscription) this.reportSubscription.unsubscribe();
    if (updateClick) {
      this.filters_obj['updateClick'] = true;
    } else {
      this.filters_obj['updateClick'] = false;
    }
    this.filterKeyEmitter.emit(this.filters_obj);
    this.setQueryParamsInUrl(this.filters_obj);

    if (this.callsFunctionFormFilter && this.report_type) {
      this.reportSubscription = this.tracketFiltersService.getReport(this.filters_obj, this.report_type).subscribe((data: any) => {
        this.loading = false;
        this.filterData.emit(data);
      });
    }
    if (!this.report_type) {
      this.loading = false;
    }
    if (!this.callsFunctionFormFilter) {
      this.loading = false;
    }
  }

  onSearch(value: string) {

    this.filters_obj.search = value
    this.valueChanged(value);
  }

  valueChanged(value) {
    this.filters_obj.search = value
    this.searchQuery.next(value);
  }

  addReport() {
    let newReport = {
      report_type: this.report_type,
      account_id: this.account_id,
      report_attributes: this.attributes,
      report_name: this.reportName,
      created_by: this.user_id

    }

    this.reportConfigs.push(newReport)
    this.selectedReport = this.reportConfigs.slice(-1).pop();
    this.index = this.reportConfigs.length - 1;
    this.getSelectedReport(this.reportConfigs[this.index], this.index)

    this.reportName = ''


  }

  duplicateReport(config) {
    this.index = null;
    this.selectedReport = {};
  
    let duplicateReport = {
      ...config
    };
    duplicateReport.account_id = this.account_id;
    duplicateReport.created_by = this.user_id;
    duplicateReport.is_default === 1 || duplicateReport.is_default === 2 ? duplicateReport.is_default = 2 : duplicateReport.is_default = 0;
    duplicateReport.report_name = 'Copy of' + ' ' + config.report_name;
    delete duplicateReport.id

    this.reportConfigs.push(deepCopy(duplicateReport));
    this.index = this.reportConfigs.length - 1;

    this.getSelectedReport(this.reportConfigs[this.index], this.index)


  }

  updateReports() {
    this.isButtonDisable = true;
    let obj = {
      report_configurations: this.reportConfigs,
      user_id: this.userCurrAcc.users_accounts.user_id,
      account_id: this.userCurrAcc.users_accounts.account_id,
      report_type: this.report_type
    }

    if (this.reportSubscription) this.reportSubscription.unsubscribe();
    this.reportSubscription = this.tracketFiltersService.updateReports(obj).subscribe((res: any) => {
      res.report_configuration.forEach(report => {
        if (report.report_name == this.selectedReport.report_name) {
          this.selectedReport.id = report.id
          this.filters_obj.report_id = this.selectedReport.id
        }
      });
      this.update(true);
      this.toastr.ShowToastr('success',this.translation.reporting_configuration_success)
      this.bsModalRef.hide();

    })
  }

  check(value, checked) {

    value.status = checked

  }

  public init() {
    this.dropdownSettings = {
      "singleSelection": false,
      "selectAllText": this.translation.workspace_all,
      "unSelectAllText": this.translation.workspace_all,
      "allowSearchFilter": false,
      "itemsShowLimit": 1,
      "closeDropDownOnSelection": false,
      "idField": "id",
      "textField": "company_name"
    };

    this.dropdownSettingsSingle = {
      "singleSelection": true,
      "selectAllText": this.translation.workspace_all,
      "unSelectAllText": this.translation.workspace_all,
      "allowSearchFilter": false,
      "itemsShowLimit": 1,
      "closeDropDownOnSelection": false,
      "idField": "id",
      "textField": "company_name"
    };

    this.dropdownSettingsHuddle = {
      "singleSelection": false,
      "selectAllText": this.translation.workspace_all,
      "unSelectAllText": this.translation.workspace_all,
      "allowSearchFilter": false,
      "itemsShowLimit": 1,
      "closeDropDownOnSelection": false,
      "idField": "id",
      "textField": "value"
    };
    this.dropdownSettingsFramework = {
      "singleSelection": true,
      "allowSearchFilter": false,
      "itemsShowLimit": 1,
      "closeDropDownOnSelection": true,
      "maxHeight": 50,
      "idField": "account_tag_id",
      "textField": "tag_title"
    };
    this.selectedAccounts = [
      { id: parseInt(this.account_id), company_name: this.userCurrAcc.accounts.company_name },
    ];

    // this.selectedHuddles = [...this.byDefaultHuddle];
    this.selectHuddle();
    this.bsConfig = { containerClass: "theme-default", rangeInputFormat: 'MM/DD/YYYY', dateInputFormat: 'MM/DD/YYYY', };
  }

  editReport(config, index) {


    this.reportName = config.report_name
    this.indexToUpdate = index

  }
  updateReportName() {
    this.reportConfigs[this.indexToUpdate].report_name = this.reportName
    this.reportName = ''
    this.indexToUpdate = null
  }

  onAccountSelect(obj) {
    // if (this.fromAnalytics) {
    this.getFiltersData();
    // }
  }

  onAccountSelectAll(arr) {
    // if (this.fromAnalytics) {
    setTimeout(() => {

      this.getFiltersData();
    }, 50);

    // }
  }

  onAccountDeselect(obj) {
    this.selected_accounts = this.selected_accounts.filter(element => element !== obj.id);
    // if (this.fromAnalytics) {
    this.getFiltersData();
    // }
  }

  onAccountDeselectAll(event) {
    this.selected_accounts = [];
    // if (this.fromAnalytics) {
    setTimeout(() => {

      this.getFiltersData();
    }, 50);
    // }
  }



  convertDate(str) {
    let date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("/");
  }

  openDeleteModal(obj, deletedReportIndex) {
    // this.bsModalRef.hide();
    this.bsModalRef.setClass('fade');
    this.deletedReportIndex = deletedReportIndex
    const config: ModalOptions = {
      backdrop: 'static',
      keyboard: false,
      class: 'modal-container-700',
    };
    this.selectedReport = obj;
    // this.bsModalRef.hide();
    this.deleteModalRef = this.modalService.show(this.deleteReportModal, config);
  }

  deleteReport() {

    let deleteIndex = this.reportConfigs.findIndex(x => x.id == this.selectedReport.id);

    if (this.reportSubscription) this.reportSubscription.unsubscribe();
    this.reportSubscription = this.tracketFiltersService.deleteReport({ report_id: this.selectedReport.id }).subscribe((data: any) => {
      if (this.filters_obj.report_id == this.selectedReport.id) {
        delete this.filters_obj['report_id'];
        this.setQueryParamsInUrl(this.filters_obj);
      }
      this.selectedReport = {} as any;

      if (this.index == deleteIndex) {

        this.index = null;
        this.selectedReport = {};



      }

      if (this.selectedReport.id) {
        this.reportConfigs = this.reportConfigs.filter(x => x.id !== this.selectedReport.id);
      } else {

        this.reportConfigs.splice(this.deletedReportIndex, 1)
      }



      this.cancelDeleteModal();
    });

  }

  onSearchStandard() {

  }

  setPage(page, callUpdateFunction = true) {
    this.currentPage = page;
    this.filters_obj.page = this.currentPage;
    if (callUpdateFunction) {
      this.update()
    }

  }

  onValueChange(dateRange) {

    if (dateRange && dateRange[0] && dateRange[1]) {
      if (moment(dateRange[0]).toDate().setHours(0, 0, 0, 0) > moment(dateRange[1]).toDate().setHours(0, 0, 0, 0)) {
        this.toastr.ShowToastr('error',this.translation.analytics_strt_date_check);
      }
      // let months = monthDiff(dateRange[0], dateRange[1])
      // if(months && [1,3,6,12].includes(months)){
      //   this.duration = months
      // }   
      // else this.duration = null

      let days = daysBetween(dateRange[0], dateRange[1])
      if (days == 29 || days == 30 || days == 31) {
        this.duration = '1'
      } else if (days == 90 || days == 91 || days == 92) {
        this.duration = '3'
      } else if (days == 181 || days == 182 || days == 183) {
        this.duration = '6'
      } else if (days == 364 || days == 365 || days == 366) {
        this.duration = '12'
      } else {
        this.duration = null;
      }
    }
  }

  setItemsPerPage(items, callUpdateFunction = true) {
    this.itemsPerPage = items
    this.filters_obj.limit = this.itemsPerPage
    if (callUpdateFunction) {
      this.update()
    }
  }

  setExportReport(exportType, active) {
    this.loadingModalRef = this.modalService.show(this.loadingModal, { backdrop: 'static' });
    this.filters_obj.search = this.search;
    this.filters_obj.active = active
    this.tracketFiltersService.exportReports(this.filters_obj, exportType).subscribe((data: any) => {
      // console.log(data);
      this.exportFiles(data.filename)
    });


  }

  exportFiles(fileName) {
    let obj: any = {};

    let file_name = fileName;
    let interval_id = setInterval(() => {
      obj.file_name = file_name;

      this.bodyService.is_download_ready(obj).subscribe((data: any) => {
        let download_status = data.download_status;
        if (download_status) {
          clearInterval(interval_id);
          obj.download_url = data.download_url;
          obj.file_name = file_name;
          this.loadingModalRef.hide();
          this.bodyService.download_analytics_standards_to_excel(obj);
        }
      });
    }, 3000);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.report_type && changes.report_type.currentValue) {
      this.report_type = changes.report_type.currentValue;
    }
    if (changes && changes.itemsPerPage && changes.itemsPerPage.currentValue) {
      this.itemsPerPage = changes.itemsPerPage.currentValue;
      this.filters_obj.limit = this.itemsPerPage;
    }
    // if (changes && changes.isSingleSelection && !this.isEmpty(changes.isSingleSelection.currentValue)) {
    //   this.isSingleSelection = changes.isSingleSelection.currentValue;
    //   this.setSingleSelection();
    // }


  }

  isEmpty(variable) {
    const type = typeof variable
    if (variable === null) return true
    if (type === 'undefined') return true
    if (type === 'boolean') return false
    if (type === 'string') return !variable
    if (type === 'number') return false
    if (Array.isArray(variable)) return !variable.length
    if (type === 'object') return !Object.keys(variable).length
    return !variable
  }

  setSingleSelection() {
    this.dropdownSettingsHuddle = {
      "singleSelection": this.isSingleSelection,
      "selectAllText": this.translation.workspace_all,
      "unSelectAllText": this.translation.analytics_un_select_all,
      "allowSearchFilter": false,
      "itemsShowLimit": 1,
      "closeDropDownOnSelection": false,
      "idField": "id",
      "textField": "value"
    };
    // this.selectedHuddles = [...this.byDefaultHuddle];
    this.selectHuddle();
    this.dropdownSettingsHuddle = Object.assign({}, this.dropdownSettingsHuddle);

  }


  ngOnDestroy() {
  }

  setValuesFormQueryParams() {
    this.setFilterObj(this.queryParams)
    this.setDates();
    this.setHuddleAndFrameWork();
    this.setValueAfterDropDownValuesLoad();
    this.update(this.queryParams?.updateClick);
  }

  setHuddleAndFrameWork() {
    if (this.filters_obj['huddle_type']) {
      this.huddleType = this.filters_obj['huddle_type'];
    }

    if (this.filters_obj.huddle_ids) {
      let that = this;
      this.filters_obj.huddle_ids = [...this.filters_obj.huddle_ids.map(x => +x)];
      this.huddles.map(x => {
        x.id = +x.id;
        return x;
      })
      this.selectedHuddles = this.huddles.filter((item) => {
        return that.filters_obj.huddle_ids.indexOf(item.id) !== -1;
      });
    }
    if (this.filters_obj['framework_id']) {
      if (this.filters_obj.standard_ids) {
        this.filters_obj.standard_ids = this.filters_obj.standard_ids.map(x => +x);
        this.standard_ids = this.filters_obj.standard_ids;
      }
      const frameWork = this.frameworks.find(frameWork => frameWork.account_tag_id == +this.filters_obj['framework_id'])
      this.selectedFramework.push(frameWork)
      this.framework_id = this.filters_obj['framework_id'];
      this.tracketFiltersService.getFrameWork({ account_id: this.account_id, framework_id: this.framework_id }).subscribe((data: any) => {
        let that = this;
        this.standard_ids = this.filters_obj.standard_ids.map(x => +x);
        this.standards = data;
        this.standards.forEach(element => {
          element.status = that.filters_obj.standard_ids.indexOf(element.value) != -1 ? true : false;
        });
      });
    }

  }

  setDates() {
    if (this.filters_obj['start_date'] && this.filters_obj['end_date']) {
      this.maxDate = new Date(this.filters_obj['start_date']);
      this.bsValue = new Date(this.filters_obj['end_date']);
      this.bsRangeValue = [this.maxDate, this.bsValue];
    }
    if (this.filters_obj['duration']) {
      this.duration = <any>parseInt(this.filters_obj['duration']);
    }
  }

  setFilterObj(event) {
    this.filters_obj = Object.assign({}, event);
  }

  setAccounts() {
    let that = this;
    that.filters_obj.account_ids = [...this.filters_obj.account_ids.map(x => +x)];//
    this.selectedAccounts = [];
    if (this.filters_obj.account_ids) {
      for (let i = 0; i < this.filters_obj.account_ids.length; i++) {
        this.selectedAccounts.push({ id: this.filters_obj.account_ids[i] });
      }
      this.selected_accounts = this.filters_obj.account_ids;
    }
  }

  setValueAfterDropDownValuesLoad() {
    let that = this;
    this.filters_obj.account_ids = this.filters_obj.account_ids ? this.filters_obj.account_ids : []

    this.filters_obj.account_ids = [...this.filters_obj.account_ids.map(x => +x)];
    this.selectedAccounts = this.chlidAccounts.filter((item) => {
      return that.filters_obj.account_ids.indexOf(item.id) !== -1;
    });
  }

  setQueryParamsInUrl(event) {
    if (event && event.updateClick) {
      const urlTree = this.router.createUrlTree([], {
        relativeTo: this.activatedRoute,
        queryParams: event,
        queryParamsHandling: 'merge',
      });
      this.location.go(urlTree.toString());
    }
  }

  setUrl(url) {
    this.report_type = url;
  }

  private ObjectToArray(object) {
    let arr = [];
    Object.keys(object).forEach((k) => {
      arr.push({ id: k, value: object[k] });
    });
    if (arr[2] && arr[3]) {
      let t = arr[2];
      arr[2] = arr[3];
      arr[3] = t;
    }
    return arr;
  }

  private selectHuddle() {
    if (!this.huddles || this.huddles.length == 0) {
      return;
    }


    if (this.filters_obj.folder_type) {

      let index = _.findIndex(this.huddles, { id: this.filters_obj.folder_type });
      this.selectedHuddles = this.huddles;
      return;

    } else {

      let coaching_index_1 = _.findIndex(this.huddles, { id: "2" });



      if (coaching_index_1 > -1) {

        this.filters_obj.folder_type = "2";
        this.selectedHuddles = this.huddles;
        console.log(this.selectedHuddles);


      } else {
        let assessment_index = _.findIndex(this.huddles, { id: "3" });

        if (assessment_index > -1) {
          this.filters_obj.folder_type = "3";
          this.selectedHuddles = this.huddles;
        } else {
          this.filters_obj.folder_type = "1";
          this.selectedHuddles = this.huddles;
        }
      }
    }
  }

  getFields(input, field) {
    return input.map((o) => {
      return o[field];
    });
  }

  no_backspaces(event) {
    let backspace = 8;
    if (event.keyCode == backspace) event.preventDefault();
  }
  resetPage(page) {
    this.filters_obj.page = page;
  }

  isToDisable() {
    if (this.report_type == 'playercard-tags' || this.report_type == 'playercard-user-summary' || this.report_type == 'playercard-overview') {
      return true;
    }
    return false;
  }

  cancelDeleteModal() {
    this.deleteModalRef.hide();
    setTimeout(() => {
      this.bsModalRef.setClass('modal-container-700');
    }, 500);

  }

  trackByAccountTagId(index: number, frameWork: any): string {
    return frameWork.account_tag_id;
  }

  public showdp() {

    this.dp.nativeElement.click();

  }

  findTranslation(value) {
    let name = value.toLowerCase();
    name = name.split(' ').join('-');


    if (this.translation[name]) {

      return this.translation[name];
    } else return value;

    // return name;
  }
}
