import { Component, Input, OnChanges, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { ShowToasterService } from '@projectModules/app/services';
import { NewfolderService } from '@src/project-modules/workspace/services/newfolder.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'shared-rename-folder-modal',
  templateUrl: './rename-folder-modal.component.html',
  styleUrls: ['./rename-folder-modal.component.css']
})
export class SharedFolderModal implements OnInit, OnDestroy {
  public translation: any = {};
  public NewFolderName: string;
  subscription: Subscription;
  public folderId;
  public parentFolderId;
  public huddleId;
  public pageType;
  constructor(private modalService: BsModalService, public bsModalRef: BsModalRef, private headerService: HeaderService,
    private toastr: ShowToasterService, private newFolderService: NewfolderService, private activeRoute: ActivatedRoute
  ) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
  }
  ngOnInit() {

  }

  createFolder() {
    if (!this.NewFolderName || !this.NewFolderName.trim()) {
      this.toastr.ShowToastr('info',this.translation.huddle_please_provide_folder_name)
      return;
    }
    let sessiondata = this.headerService.getStaticHeaderData();
    // Edit/Rename Folder in artifacts
    if(this.parentFolderId) {
      let obj = {
        folder_id: this.parentFolderId,
        user_id: sessiondata.user_current_account.User.id,
        account_id: sessiondata.user_current_account.accounts.account_id,
        name: this.NewFolderName,
        site_id: 1,
        parent_folder_id: this.folderId,
        account_folder_id: this.huddleId ? this.huddleId : 0,
      }
      this.subscription = this.newFolderService.renameFolder(obj).subscribe((res: any) => {
        if(res.success) {
          this.toastr.ShowToastr('info',res.message);
          this.bsModalRef.hide();
        } else {
          this.toastr.ShowToastr('info',res.message);
          this.bsModalRef.hide();
        }
      })
      return;
    }
    // Create New Folder in Artifacts
    let obj: any = {
      user_id: sessiondata.user_current_account.User.id,
      account_id: sessiondata.user_current_account.accounts.account_id,
      name: this.NewFolderName,
      site_id: 1,
      account_folder_id: this.huddleId ? this.huddleId : 0,
      parent_folder_id: this.folderId ? this.folderId : 0
    }

    this.subscription = this.newFolderService.createNewFolder(obj).subscribe((res: any) => {
      if(res.success) {
        this.toastr.ShowToastr('info',res.message);
        this.bsModalRef.hide();
      } else {
        this.toastr.ShowToastr('info',res.message);
        this.bsModalRef.hide();
      }
    })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
