import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, OnChanges, HostListener } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeaderService } from "@projectModules/app/services";
import { ArtifactShareModelComponent } from '@src/project-modules/shared/modals';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { PageType } from "@app/interfaces";
@Component({
  selector: 'shared-folder-thumb-view',
  templateUrl: './folder-thumb-view.component.html',
  styleUrls: ['./folder-thumb-view.component.css']
})
export class SharedFolderThumbViewComponent implements OnInit, OnDestroy, OnChanges {

  @Input() isHuddle;
	@Input() data;
  @Input() block_move;
  @Input() isExpanded;
  public displayStyle;
	@Output() OnEdit = new EventEmitter();
  @Output() OnDelete = new EventEmitter();
  @Output() OnShare = new EventEmitter();
  @Input('pageType') pageType: PageType;
  @Input('additionalData') additionalData: any;

  @Input() changeBackgroundColor:boolean;
  @Input() dragFolderId:any;

    // @HostListener('dragenter', ['$event'])
    // onDragenter(event: MouseEvent) {
    //     //this.mousedown.emit(event);
    //     console.log('dragenter'); 
        
    //     //return false; // Call preventDefault() on the event
    // }

  // @Output() shareFolderModal = new EventEmitter();
  public assessment_permissions;
  public isDropdownOpen:boolean;
  public huddleId;
  public header_data;
  public pointerEvent:boolean = false;
  public translation: any = {};
  private subscription: Subscription;

  @Output() buttonDragStart:EventEmitter<boolean> = new EventEmitter<boolean>();
  public state:boolean = true;

  constructor(public headerService:HeaderService, private activatedRoute: ActivatedRoute, private modalService: BsModalService) { 
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
			this.translation = languageTranslation;
		});
  }

  ngOnInit() {
    this.isDropdownOpen = false;
    this.header_data = this.headerService.getStaticHeaderData();
    this.Assessment_huddle_permissions();
    this.activatedRoute.params.subscribe((p) => {
      this.displayStyle = p.displayStyle;
      this.huddleId = p.id;
    });

    

    //console.log('changebackgroundcolor in child',this.changeBackgroundColor);
  }

  ngOnChanges(){

      //console.log('isexpanded',this.isExpanded);
    //console.log('changebackgroundcolor on changes',this.changeBackgroundColor);
    //console.log('drag folder id',this.dragFolderId);

    if(this.changeBackgroundColor == true){

    }
  }

    public isOpen(flag){

    this.isDropdownOpen = flag;
    if(flag){
      this.data.mouseEntered = true;
    }

  }

  // stopDrag(event){
  
  //   console.log('find',event);
  //   console.log('eventemitter',this.buttonDragStart);
  //   this.buttonDragStart.emit(this.state);

  //   //this.headerService.UpdateMouseEvent(true);
    
  // }

 public EditFolder(id){
   this.OnEdit.emit({id: id, parentFolderId: this.data?.parent_folder_id});

  }

  public getPermission() {
    if(this.pageType == 'workspace-page') {
      return true
    } else {
      let userId = this.header_data.user_current_account.User.id;
      if (this.additionalData?.users_accounts.role_id != 125) {
        if (userId == this.data.created_by || this.additionalData.role_id == 200) {
          if (this.additionalData.dis_mem_del_video == 1) {
              return true
          } 
        } 
      } else {
        return false;
      }
    }
  }

  public shareHuddle() {
    this.OnShare.emit();
  }

  public shareFolderModal(artifact){
    const initialState = { artifact, pageType: this.pageType, modal_for:'for_folder_share' };
    this.modalService.show(ArtifactShareModelComponent, { initialState, class: 'share-modal-container',backdrop:'static' });
  }

  public Assessment_huddle_permissions() {
    let data: any = this.headerService.getStaticHeaderData();
		this.assessment_permissions = data.user_permissions.UserAccount.manage_evaluation_huddles == 1;
		return this.assessment_permissions;
	}

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
