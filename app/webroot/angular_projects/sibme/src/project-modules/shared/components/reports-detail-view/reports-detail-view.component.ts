import { Component, OnInit, Input, EventEmitter, ViewChild } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { KeyValue } from '@angular/common';
import { HeaderService, LocalStorageService } from '@src/project-modules/app/services';
import { TrackerFiltersComponent } from '@src/project-modules/shared/components';
import { ActivatedRoute, Router } from '@angular/router';
import { TrackerFiltersService } from '../../services/tracker-filters.service';
import { decode, encodeCompress } from '../common/utility';
import { BsDatepickerConfig, BsDaterangepickerDirective } from 'ngx-bootstrap/datepicker';
import { Location } from '@angular/common';
import * as moment from "moment";
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { deepCopy, monthDiff } from '../common/utility';
import { ShowToasterService } from '@projectModules/app/services';

@Component({
  selector: 'reports-detail-view',
  templateUrl: './reports-detail-view.component.html',
  styleUrls: ['./reports-detail-view.component.css']
})
export class ReportsDetailView implements OnInit {
  @ViewChild(TrackerFiltersComponent) trackerFiltersComponent: TrackerFiltersComponent
  @ViewChild(BsDaterangepickerDirective, { static: false }) dateRangePickerObj: BsDaterangepickerDirective;
  @ViewChild('dp', { static: false }) dp;

  private subscription: Subscription;
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  p: number = 1;
  translation: any;
  headers = [] as any;
  attributes = [] as any;
  sortByAsc: boolean;
  SearchString = '';
  imageStaticUrl: any;
  baseUrl: any;
  public header_data;
  imageBaseUrl: any;
  config = {
    currentPage: 1,
    itemsPerPage: 10,
  };

  isLoading = true;
  type: any;
  filterObj = {} as any;
  account_id: any;
  user_id: any;
  huddle_ids: any;
  documnet_ids = [];
  queryParams = {};
  dropdownSettings;
  path: any;

  // huddleTypes = [{ id: 1, name: 'Videos uploaded to Huddle' }, { id: 2, name: 'Videos Shared to Huddle' }, { id: 3, name: 'Hours Uploaded' }, { id: 4, name: 'Hours Viewed' },];
  huddleTypes = [];
  selectedHuddleTypes = [];
  public bsConfig: Partial<BsDatepickerConfig>;
  bsRangeValue: Date[];
  maxDate = new Date();
  bsValue = new Date();
  duration = 1;
  private searchQuery: Subject<string> = new Subject();
  constructor(
    private headerService: HeaderService,
    private activatedRoute: ActivatedRoute,
    private trackerService: TrackerFiltersService,
    public location: Location,
    private localStorage: LocalStorageService,
    private toastr: ShowToasterService,
    private tracketFiltersService: TrackerFiltersService,
    public router: Router,
  ) {
    this.dropDownConfiguration();
    this.type = this.activatedRoute.snapshot.paramMap.get('report_type');
    this.header_data = this.headerService.getStaticHeaderData();
    this.account_id = this.header_data.user_current_account.users_accounts.account_id;
    this.user_id = this.header_data.user_current_account.users_accounts.user_id;
    // console.log(this.header_data);
    this.headerService.reportsDetailData$.subscribe((data: any) => {
      this.huddle_ids = data.huddle_ids;
      this.documnet_ids = data.document_ids;

    });
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
  }

  ngOnInit() {
    this.maxDate.setDate(this.maxDate.getDate() - 30);
    this.bsRangeValue = [this.maxDate, this.bsValue];

    this.activatedRoute.queryParams.subscribe((queryParams: any) => {
      this.setFirstQueryParams(queryParams);
      this.setSecondQuery2ParamsFilter(queryParams);


      this.searchQuery.pipe(debounceTime(1000), distinctUntilChanged()).subscribe(() => {
        // console.log('this called searchQuery');
        this.onUpdate();
      });
    });

    this.setScrollOptions();
    this.bsConfig = { containerClass: "theme-default", rangeInputFormat: 'MM/DD/YYYY', dateInputFormat: 'MM/DD/YYYY',};

  }

  setScrollOptions() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    };
  }

  setFirstQueryParams(queryParams) {
    if (queryParams.query) {
      this.queryParams = decode(queryParams.query);
      this.queryParams = Object.assign({}, this.queryParams);
      // console.log(this.queryParams);

      if (this.queryParams['report_type']) {
        this.huddleTypes = this.localStorage.get(this.queryParams['report_type']);
        this.huddleTypes = this.huddleTypes.filter(x => x.detail !== false);
      }
      if (this.queryParams['path']) {
        this.path = decodeURIComponent(this.queryParams['path']);
      }
      if (this.queryParams['header']) {
        this.selectedHuddleTypes = [...this.queryParams['header']];
      }
      if (this.queryParams['start_date'] || this.queryParams['end_date'] || this.queryParams['duration']) {
        this.setDates(this.queryParams);
      }
    }
  }

  setSecondQuery2ParamsFilter(queryParams) {
    if (queryParams.query2) {
      let query2 = decode(queryParams.query2);
      query2 = Object.assign({}, query2);
      if (query2 && typeof query2['header'] === 'string' || query2['header'] instanceof String) {
        query2['header'] = [query2['header']];
      }
      if (query2['header']) {
        this.selectedHuddleTypes = [...query2['header']];
        delete this.queryParams['header']
      }
      this.filterObj = query2;
      if (this.filterObj.start_date || this.filterObj.start_date || this.filterObj.duration) {
        this.setDates(this.filterObj);
      }
      this.setSearch();
      // this.setValueAfterDropDownValuesLoad();
    }
  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.setFilterObject();
    this.detailView();

  }

  originalOrder = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => {
    return 0;
  }

  setFilterObject() {

    let selectedHeaders = []
    // console.log(this.selectedHuddleTypes, 'selectedHuddleTypes');
    let that = this;
    var anotherOne = this.getFields(this.selectedHuddleTypes, 'fake_id');;
    var filteredArray = this.huddleTypes.filter(x => anotherOne.indexOf(x.fake_id) >= 0);
    // console.log("ReportsDetailView -> setFilterObject -> filteredArray", filteredArray)
    this.filterObj = {
      // account_id: this.account_id,
      // user_id: this.user_id,
      huddle_ids: this.huddle_ids,
      document_ids: this.documnet_ids,
      header: filteredArray,
      search: this.SearchString,
      duration: this.duration,
      start_date: this.convertDate(this.bsRangeValue[0]),
      end_date: this.convertDate(this.bsRangeValue[1]),
    };

  }

  onValueChange(dateRange) {
    if (dateRange && dateRange[0] && dateRange[1]) {
      if (moment(dateRange[0]).toDate().setHours(0, 0, 0, 0) > moment(dateRange[1]).toDate().setHours(0, 0, 0, 0)) {
        this.toastr.ShowToastr('error',this.translation.analytics_strt_date_check);
      }
      let months = monthDiff(dateRange[0], dateRange[1])
      if (months && [1, 3, 6, 12].includes(months)) {
        this.duration = months
      }
      else this.duration = null
    }
  }

  detailView() {
    let obj = { ...this.queryParams, ...this.filterObj };

    this.trackerService.detailView(obj).subscribe((data: any) => {
      // console.log(data);
      this.headers = Array.isArray(data.header) ? data.header : []
      this.attributes = data.activity_detail
      this.isLoading = false
      // this.huddleTypes = this.ObjectToArray(data.dropdown_data);
      // console.log("ReportsDetailView -> detailView -> this.huddleTypes", this.huddleTypes)
      // this.setValueAfterDropDownValuesLoad();
    });
  }


  onSearch(searchString) {
    this.SearchString = searchString
    //  this.searchQuery.next(this.SearchString); 
  }

  sortTable(j) {

    if (this.sortByAsc) {
      this.sortByAsc = !this.sortByAsc;
      this.attributes.sort((a, b) => {

        return isNaN(a[j] - b[j]) ? (a[j] === b[j]) ? 0 : (a[j] < b[j]) ? -1 : 1 : a[j] - b[j];
      }
      );
    }
    else {
      this.sortByAsc = !this.sortByAsc;

      this.attributes.sort((a, b) => {
        return isNaN(b[j] - a[j]) ? (b[j] === a[j]) ? 0 : (b[j] < a[j]) ? -1 : 1 : b[j] - a[j];
      });
    }

  }

  getImage(userId, image) {
    if (image) {
      return this.imageBaseUrl = 'https://s3.amazonaws.com/sibme.com/static/users/' + userId + '/' + image;
    } else return this.header_data.base_url + "/img/home/photo-default.png";
  }

  isNumeric(value) {
    return isNaN(value);
  }

  onPageChange(event) {
    // console.log("cureent page", event)
    this.config.currentPage = event;
  }

  onPerPageLimitChange(event) {
    // console.log("count per page ", this.config.itemsPerPage)
    // this.config.currentPage = 1;
    // this.detailView();
  }

  dropDownConfiguration() {
    this.dropdownSettings = {
      "singleSelection": false,
      "selectAllText": 'All',
      "unSelectAllText": 'All',
      "allowSearchFilter": false,
      "itemsShowLimit": 1,
      "closeDropDownOnSelection": false,
      "idField": "fake_id",
      "textField": "name"
    };
  }

  onDuration(dur) {
    this.duration = dur;
    if (dur) {
      let date_range = [moment().subtract(dur, 'months'), moment()];
      this.bsRangeValue = [date_range[0].toDate(), date_range[1].toDate()];
      return;
    }
  }

  exportReport(exportType, active) {
    this.filterObj.active = active
    let obj = { ...this.queryParams, ...this.filterObj };
    this.tracketFiltersService.exportDetailReports(obj, exportType);
  }

  convertDate(str) {
    let date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("/");
  }

  getFields(input, field) {
    return input.map((o) => {
      return o[field];
    });
  }

  setQueryParamsInUrl(event) {
    // if (event && event.updateClick) {
    const urlTree = this.router.createUrlTree([], {
      relativeTo: this.activatedRoute,
      queryParams: event,
      queryParamsHandling: 'merge',
    });
    this.location.go(urlTree.toString());
    // }
  }

  onUpdate() {
    if (!this.bsRangeValue) {
      this.toastr.ShowToastr('error',this.translation.reporting_please_select_date_range || ' Please select a date range.');
      return;
    }
    if (this.dateRangePickerObj && this.dateRangePickerObj._bsValue.length > 0 && !this.dateRangePickerObj._bsValue[0]) {
      this.filterObj.start_date = "";
      this.filterObj.end_date = "";
      this.onDuration(1)
    }
    this.filterObj['updateClick'] = true;
    this.setFilterObject();
    
    let queryObject = { query: encodeCompress(this.queryParams), query2: encodeCompress(this.filterObj) };
    this.setQueryParamsInUrl((queryObject));
    this.detailView();
  }

  setDates(inputObject) {
    if (inputObject['start_date'] && inputObject['end_date']) {
      this.maxDate = new Date(inputObject['start_date']);
      this.bsValue = new Date(inputObject['end_date']);
      this.bsRangeValue = [this.maxDate, this.bsValue];
    }
    if (inputObject['duration']) {
      this.duration = <any>parseInt(inputObject['duration']);
    }
  }

  setSearch() {
    if (this.filterObj['search']) {
      this.SearchString = <any>(this.filterObj['search']);
    }
  }

  setValueAfterDropDownValuesLoad() {
    let that = this;
    // this.filterObj.selected_types = [...this.filterObj.selected_types.map(x => +x)];
    this.selectedHuddleTypes = this.filterObj.selected_types;
    // this.selectedHuddleTypes = this.huddleTypes.filter((item) => {
    //   return that.filterObj.selected_types.indexOf(item.key) !== -1;
    // });
  }

  createBreadCrumb(string) {
    string = string.replace(/_/g, " ");
    const word = string.toLowerCase().replace(/^\w|\s\w/g, function (letter) {
      return letter.toUpperCase();
    });
    
    
    if(word == 'Coaching Report') return this.translation?.reporting_coaching_report;
    
    if(word == 'Assessment Report') return this.translation?.reporting_assessment_report;
    
    if(word == 'Huddle Report') return this.translation?.reporting_huddle_report;
    
    if(word == 'User Summary Report') return this.translation?.analytics_user_summary;
    
    // return word;
  }

  decodeUrl(url) {
    // console.log(decodeURIComponent(url));

    return decodeURIComponent(url)
  }

  back(){
    
    if (document.location.href.indexOf('/home') > 0) {

      this.path = this.path.replace('/home' , '');
      this.router.navigateByUrl(this.path);

    } else{
      this.router.navigateByUrl(this.path)
    }
    
  }

  public showdp() {

    this.dp.nativeElement.click();

  }
}

