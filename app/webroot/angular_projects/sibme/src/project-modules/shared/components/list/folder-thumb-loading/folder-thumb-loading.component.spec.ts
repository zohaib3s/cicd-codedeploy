import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedFolderThumbLoadingComponent } from './folder-thumb-loading.component';

describe('SharedFolderThumbLoadingComponent', () => {
  let component: SharedFolderThumbLoadingComponent;
  let fixture: ComponentFixture<SharedFolderThumbLoadingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SharedFolderThumbLoadingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedFolderThumbLoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
