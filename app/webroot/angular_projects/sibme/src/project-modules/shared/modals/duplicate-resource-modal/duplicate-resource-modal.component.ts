import { Component, OnInit } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { Subscription } from 'rxjs';
import { ShowToasterService } from '@projectModules/app/services';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { WorkspaceHttpService } from '@src/project-modules/workspace/services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-duplicate-resource-modal',
  templateUrl: './duplicate-resource-modal.component.html',
  styleUrls: ['./duplicate-resource-modal.component.css']
})
export class DuplicateResourceModalComponent implements OnInit {
  duplicateIsinProcess = false;
  doShareAssets: any = false;
  sessionData: any = {};
  private subscription: Subscription;
  public translation: any = {};
  artifact;
  pageType;
  page;
  parent_folder_id;

  constructor(public headerService: HeaderService,
     public workService: WorkspaceHttpService, private router: Router,
     public toastr: ShowToasterService, public bsModalRef: BsModalRef) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });

   }

  ngOnInit() {
  this.sessionData = {};
  this.sessionData = this.headerService.getStaticHeaderData();

  }



  duplicateResourceNow(value) {
    this.duplicateIsinProcess = true;
    if (value == 1)
      this.doShareAssets = true;
    this.DuplicateResource(this.artifact)
    this.hideAssetsShare();
  }

  DuplicateResource(res: any) {
    let obj: any = {};

    obj.document_id = res.doc_id;
    obj.account_folder_id = [res.account_folder_id];
    obj.current_huddle_id = res.account_folder_id;
    obj.account_id = res.account_id;
    obj.copy_notes = 1;
    obj.is_duplicated = true;
    obj.shareAssets = this.doShareAssets
    obj.doc_type = res.doc_type;
    obj.parent_folder_id = this.parent_folder_id;
    ({ User: {id: obj.user_id} } = this.sessionData.user_current_account);

    if(this.pageType === 'workspace-page' || this.pageType === 'workspace-video-page'  ){
      obj.workspace=true,
      obj.from_workspace=1;
    }

    this.workService.DuplicateResource(obj).subscribe(data => {
      let d: any = data;
      this.duplicateIsinProcess = false;

      let type = this.headerService.isAValidAudio(d.data.file_type);
      if (type == false) {
        this.toastr.ShowToastr('info',d.message);
      }
      else {
        this.toastr.ShowToastr('info',this.translation.audio_duplicate_successfully);
      }
      
      if(this.pageType === 'workspace-page' && this.page=='videoPage'){
        setTimeout(() => {
          if(this.parent_folder_id) {
            this.router.navigate(['/workspace/workspace/home/grid/' + this.parent_folder_id]);
          } else {
            this.router.navigate(['/workspace/workspace/home/grid']);
          }
        }, 1000);
      }
      else if(this.pageType === 'huddle-page' && this.page=='videoPage'){
        setTimeout(() => {
          if(this.parent_folder_id) {
            this.router.navigate([`/video_huddles/huddle/details/${obj.account_folder_id}/artifacts/grid/${this.parent_folder_id}`]);
          } else {
            this.router.navigate([`/video_huddles/huddle/details/${obj.account_folder_id}/artifacts/grid`]);
          }
        }, 1000);
      }
    }, error => {
      this.toastr.ShowToastr('error',error.message)

    });
  }

  hideAssetsShare(): void {
    this.bsModalRef.hide()
    this.doShareAssets = false;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
