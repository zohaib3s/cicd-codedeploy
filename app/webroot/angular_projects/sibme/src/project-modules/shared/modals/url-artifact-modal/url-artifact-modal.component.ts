import { Component, OnDestroy, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';

import { HeaderService, AppMainService } from "@projectModules/app/services";
import { PageType } from "@app/interfaces";

@Component({
  selector: 'shared-url-artifact-modal',
  templateUrl: './url-artifact-modal.component.html',
  styleUrls: ['./url-artifact-modal.component.css']
})
export class URLArtifactModalComponent implements OnInit, OnDestroy {

  private pageType: PageType;
  public title: string;
  public url: string;
  public folderId;
  private huddleId: number;
  private docomentId: number;
  public pageMode: 'add' | 'edit' = 'add';

  private userCurrAcc: any;
  public translation: any = {};
  private subscription: Subscription;
  public validButton:any;
  constructor(public headerService: HeaderService, public appMainService: AppMainService, public bsModalRef: BsModalRef,
    private toastr: ShowToasterService) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
  }

  ngOnInit() {
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');

  }

  public submitURLArtifact() {
    if (this.title) this.title = this.title.trim();
    if (this.url) this.url = this.url.trim();

    if (!this.title) {
      this.toastr.ShowToastr('info',this.translation.url_title_should_not_be_empty);
    } else if (!this.url) {
      this.toastr.ShowToastr('info',this.translation.url_should_not_be_empty);
    } else if (!this.isValidUrl(this.url)) {
      this.toastr.ShowToastr('info',this.translation.invalid_url);
    } else {
      const data: any = {
        title: this.title,
        url: this.url,
        account_id: this.userCurrAcc.accounts.account_id,
        user_id: this.userCurrAcc.User.id,
        current_user_role_id: this.userCurrAcc.roles.role_id,
        current_user_email: this.userCurrAcc.User.email,
        parent_folder_id: this.folderId ? this.folderId : 0
      };
      if (this.pageMode === 'edit') data.document_id = this.docomentId;
      if (this.pageType === 'huddle-page') data.huddle_id = this.huddleId;

      const apiURL: string = this.pageMode === 'add' ? 'add_url' : 'edit_url';
      this.validButton = true;
      this.appMainService.submitURLArtifact(apiURL, data).subscribe((data: any) => {
        if (data.success === false) {
          this.toastr.ShowToastr('info',data.message);
          this.bsModalRef.hide();
          this.validButton = false;
        }
        else {
          if (this.pageMode === 'add') this.toastr.ShowToastr('info',this.translation.url_added);
          else this.toastr.ShowToastr('info',this.translation.url_updated);
          this.bsModalRef.hide();
        }
      });
    }
  }

  private isValidUrl(url: string) {
    return true;
    //  front end validation for some valid urls so this is code commented because validation is on backend;
    // const urlPattern = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
    // return urlPattern.test(url);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
