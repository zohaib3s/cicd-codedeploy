import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Output, EventEmitter, HostListener } from '@angular/core';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';

import { HeaderService, AppMainService, MillicastService } from "@app/services";
import { InternetState } from "@app/types";

import { GLOBAL_CONSTANTS } from "@constants/constant";

type RecordingState = 'comfort-zome' | 'startring' | 'started' | 'stopping' | 'stopped';

declare var navigator: any;

@Component({
  selector: 'shared-millicast-record-video-modal',
  templateUrl: './millicast-record-video-modal.component.html',
  styleUrls: ['./millicast-record-video-modal.component.css'],
  providers: [MillicastService]
})
export class MillicastRecordVideoModelComponent implements OnInit, OnDestroy {

  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    if (this.recordingState !== 'comfort-zome' && this.recordingState !== 'startring' && this.recordingState !== 'stopped' && this.internetState === 'online' && !this.recordingStoppedDueToNetworkLost) event.returnValue = false;
  }

  @ViewChild('confirmModal', { static: false }) confirmModal: ModalDirective;
  @ViewChild('videoSaveDiscardModal', { static: false }) videoSaveDiscardModal: ModalDirective;
  @ViewChild('camView', { static: false }) camView: ElementRef;
  @Output() onRecordedUpload: EventEmitter<any> = new EventEmitter<any>();

  public browserSupportedRecording: boolean = false;
  public permissionLoading: boolean = true;
  public cam_and_mic_perm: boolean = true;
  public window_permission: boolean = true;
  public internetState: InternetState = 'online';
  public backToOnline: boolean;
  private offlineSince: number = 0;
  public connectionState: string = "You're offline";
  private recordingStoppedDueToNetworkLost: boolean = false;
  private shownVideoSaveDiscardModal: boolean = false;

  // public recordingState: RecordingState = 'uploading';
  public recordingState: RecordingState = 'comfort-zome';
  private stream: any;
  private recordedTime: number = 0;
  private showRecIcon: boolean = true;
  private oneSecInterval: any = null;
  private halfSecInterval: any = null;
  public confirmModalRef: BsModalRef;
  public videoSaveDiscardModalRef: BsModalRef;

  public uploadProgress: number = 0;

  private millicast_rec_id: number;
  private rtcPeerConnection: any;
  private rtcPeerConnectionSender: any;

  public translation: any = {};
  private subscriptions: Subscription = new Subscription();

  constructor(public headerService: HeaderService, public appMainService: AppMainService, public bsModalRef: BsModalRef, private toastr: ShowToasterService,
    private millicastService: MillicastService, private modalService: BsModalService) {
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    }));

    this.subscriptions.add(this.appMainService.internetState$.subscribe(internetState => {
      if (this.internetState !== 'online' && internetState === 'online') {
        console.log('this.internetState: ', this.internetState)
        this.backToOnline = true;
        this.connectionState = 'Back online';
        if (!this.recordingStoppedDueToNetworkLost && this.recordingState === 'started') this.startTimer();
        this.offlineSince = 0;
        setTimeout(() => {
          this.internetState = internetState;
          this.backToOnline = false;
          this.connectionState = "You're offline";
        }, 500);
      } else if (internetState === 'offline' && !this.recordingStoppedDueToNetworkLost) {
        console.log('this.internetState: ', this.internetState)
        this.internetState = internetState;
        if (this.recordingState === 'started') {
          this.stopTimer();
          this.offlineSince++;

          if (this.offlineSince === 3) {
            this.recordingStoppedDueToNetworkLost = true;
            this.stopRecording();
            if (this.stream) this.stream.getTracks().forEach(track => track.stop());
            // this.toastr.ShowToastr('info','You were offline since a while so that connection to recording has been lost');
          }
        }
      }

    }));
  }

  ngOnInit() {
    this.browserSupportedRecording = true; // as it is only for safari
    setTimeout(() => this.showCamView(), 1000);
  }

  public startRecording() {
    this.recordingState = 'startring';
    this.millicast_rec_id = new Date().getTime();
    this.millicastService.updateFileName(this.makeFileName());
    this.millicastService.updateStreamName(this.makeStreamName(this.millicast_rec_id));
    this.startMilicastBroadcast(this.millicastService.streamName);
  }

  private startMilicastBroadcast(streamName: string) {
    this.millicastService.getJWTAndSocketUrl(streamName).then((res: any) => {
      if (res.status === 200) {
        this.rtcPeerConnection = new RTCPeerConnection({ iceServers: this.millicastService.iceServers, bundlePolicy: "max-bundle" });
        this.rtcPeerConnectionSender = [];
        this.stream.getTracks().forEach(track => this.rtcPeerConnectionSender.push(this.rtcPeerConnection.addTrack(track, this.stream)));

        const cameraWS = new WebSocket(`${res.wsUrl}?token=${res.jwt}`);
        cameraWS.onopen = () => {
          this.rtcPeerConnection.createOffer({ offerToReceiveAudio: true, offerToReceiveVideo: true }).then(desc => {
            // this.recordingState = 'started';

            this.rtcPeerConnection.setLocalDescription(desc).then(() => {
              //set required information for media server.
              const payloadData = { name: this.millicastService.streamName, sdp: desc.sdp, codec: 'h264' };
              //create payload
              const payload = { type: "cmd", transId: Math.random() * 10000, name: 'publish', data: payloadData };
              cameraWS.send(JSON.stringify(payload));
              this.recordingState = 'started';
              this.startTimer();
            }).catch(err => console.log('setLocalDescription failed: ', err))
          }).catch(err => console.log('createOffer Failed: ', err));
        }

        cameraWS.addEventListener('message', evt => {
          console.log('cameraWS::message+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++', evt);
          const msg = JSON.parse(evt.data);
          switch (msg.type) {
            case "response":
              const data = msg.data;
              const answer = new RTCSessionDescription({ type: 'answer', sdp: `${data.sdp}a=x-google-flag:conference\r\n` });
              this.rtcPeerConnection.setRemoteDescription(answer)
                .then(() => console.log('setRemoteDescription Success! '))
                .catch(e => console.log('setRemoteDescription failed: ', e));
              break
          }
        });
      }
    })
  }

  public stopStreamRecording(action: 'save' | 'discard') {
    this.recordingState = 'stopping';
    if(this.rtcPeerConnection.connectionState == 'connected') {
      this.rtcPeerConnectionSender.forEach(track => this.rtcPeerConnection.removeTrack(track));
      this.rtcPeerConnection.close();
    }
    this.recordingState = 'stopped';
    this.cleanSubscription();

    if (action === 'save') {
      if (this.internetState === 'online') {
        const millicastFileObj = { file_key: 'from-millicast', filename: this.millicastService.fileName, streamName: this.millicastService.streamName, videoDuration: this.recordedTime, millicast_rec_id: this.millicast_rec_id, size: 0, video_source: GLOBAL_CONSTANTS.RECORDING_SOURCE.MILLICAST };
        this.onRecordedUpload.emit({ from: 'Record', files: [millicastFileObj] });
        if (this.videoSaveDiscardModalRef) this.videoSaveDiscardModalRef.hide();
        this.bsModalRef.hide();
      }
    } else {
      if (this.videoSaveDiscardModalRef) this.videoSaveDiscardModalRef.hide();
      this.bsModalRef.hide();
    }
  }

  public retry() {
    this.permissionLoading = true;
    setTimeout(() => this.showCamView(), 1000);
  }

  private showCamView() {
    this.cam_and_mic_perm = true;

    const constraints = { audio: true, video: true };
    navigator.mediaDevices.getUserMedia(constraints).then(stream => {
      this.stream = stream
      this.camView.nativeElement.srcObject = stream;
      this.camView.nativeElement.muted = true;

      setTimeout(() => this.permissionLoading = false, 500);

    }).catch((err) => {
      this.cam_and_mic_perm = false;
      this.permissionLoading = false;
      console.error('Could not get Media: ', err);
    });
  }

  private startTimer() {
    this.oneSecInterval = setInterval(() => this.recordedTime++, 1000);
    this.halfSecInterval = setInterval(() => this.showRecIcon = !this.showRecIcon, 500);
  }

  private stopTimer() {
    clearInterval(this.oneSecInterval);
    clearInterval(this.halfSecInterval);
  }

  public stopRecording() {
    if(!this.shownVideoSaveDiscardModal) {
      this.shownVideoSaveDiscardModal = true;
      this.videoSaveDiscardModalRef = this.modalService.show(this.videoSaveDiscardModal, { class: 'modal-container-500', backdrop: 'static' });
    }
  }

  /**
   * Create and return a nice looking file name with date and time
   */
  private makeFileName() {
    const date = new Date();
    const month = GLOBAL_CONSTANTS.MONTH_NAMES[date.getMonth()];
    const day = date.getDate();
    const year = date.getFullYear();
    let hours = date.getHours();
    let min: number | string = date.getMinutes();
    let AMPM = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    min = min < 10 ? `0${min}` : min;

    return `Recording_${month}_${day}_${year}_${hours}_${min}_${AMPM}`;
  }

  private makeStreamName(millicast_rec_id: number) {
    const date = new Date();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const year = date.getFullYear();

    return `Recording_${month}_${day}_${year}_${millicast_rec_id}_safari`;
  }

  public closeModal() {
    console.log('this.recordingState: ', this.recordingState, 'this.internetState: ', this.internetState)
    if (this.recordingState !== 'comfort-zome' && this.recordingState !== 'startring' && this.recordingState !== 'stopped' && this.internetState === 'online' && !this.recordingStoppedDueToNetworkLost) {
      this.confirmModalRef = this.modalService.show(this.confirmModal, { backdrop: 'static' });

    } else this.bsModalRef.hide();
  }

  public closeVideoSaveDiscardModal() {
    this.videoSaveDiscardModalRef.hide();
    if (this.recordingStoppedDueToNetworkLost) this.bsModalRef.hide();
  }

  public confirmed() {
    this.confirmModalRef.hide();
    this.stopStreamRecording('discard');
  }

  private cleanSubscription() {
    if (this.stream) this.stream.getTracks().forEach(track => track.stop());
    this.stopTimer();
  }

  ngOnDestroy() {
    this.cleanSubscription();
    this.subscriptions.unsubscribe();
  }
}