import { Component, OnDestroy, OnInit } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { NewfolderService } from '@src/project-modules/workspace/services/newfolder.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'shared-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.css']
})
export class ConfirmationDialogComponent implements OnInit, OnDestroy {
  public translation: any = {};
  subscription: Subscription;
  confirmation: any;
  public title;
  public folderData;
  constructor(public bsModalRef: BsModalRef, private headerService: HeaderService, private toastr: ShowToasterService, private newFolderService: NewfolderService) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
  }

  ngOnInit(): void {
  }

  confirmDelete() {
    if (this.confirmation !== this.translation.Huddle_confirmation_bit) {
      this.toastr.ShowToastr('info',this.translation.huddle_you_typed_in + " '" + this.translation.Huddle_confirmation_bit + "' ");
      return;
    }
    let sessiondata = this.headerService.getStaticHeaderData();
    let obj = {
      folder_id: this.folderData.id,
      site_id: 1,
      user_id: sessiondata.user_current_account.User.id,
      account_id: this.folderData.account_id,
      account_folder_id: this.folderData.account_folder_id
    }
    this.subscription = this.newFolderService.deleteFolder(obj).subscribe((res: any) => {
      if(res.success) {
        this.toastr.ShowToastr('info',res.message);
        this.bsModalRef.hide();
      } else {
        this.toastr.ShowToastr('info',res.message);
        this.bsModalRef.hide();
      }
    })
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
