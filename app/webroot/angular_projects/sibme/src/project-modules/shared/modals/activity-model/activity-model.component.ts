import { Component, OnInit, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { HeaderService } from "@projectModules/app/services";
import { WorkspaceHttpService } from '@src/project-modules/workspace/services';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { DetailsHttpService } from '@src/project-modules/video-huddle/child-modules/details/servic/details-http.service';

@Component({
  selector: 'app-activity-model',
  templateUrl: './activity-model.component.html',
  styleUrls: ['./activity-model.component.css']
})
export class ActivityModelComponent implements OnInit {
  @ViewChild('modalContentBody', { static: true }) modalContentBody: ElementRef;
  public translation: any = {};
  actvities: any = [];
  pageType: string;
  activityData:any;
  folder_id:any;
  popupisloading:boolean=true;
  private goalEvidence: boolean = false;
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;

  private subscription: Subscription;
  constructor( public headerService: HeaderService,
     private WdetailService:WorkspaceHttpService,
     public detailsService: DetailsHttpService,
     public bsModalRef: BsModalRef) { 

    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
   
  }

  ngOnInit() {

    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right', 
      barBackground: '#C9C9C9', 
      barOpacity: '0.8', 
      barWidth: '10', 
      barBorderRadius: '20',
      barMargin: '0', 
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    }

    this.getActivities();

    this.actvities =this.activityData;

  }


  hideModal(value): void {

  }

  getActivities() {
    let x: any = this.headerService.getStaticHeaderData()
    let obj
    if(this.pageType=='workspace'){
      obj= {
        user_id:x.user_current_account.User.id,
        account_id: x.user_current_account.users_accounts.account_id,
        user_current_account: x.user_current_account,
        workspace: 1
      };
      this.WdetailService.GetAcivities(obj).subscribe(data => {
        this.popupisloading= false;
        this.actvities = data;
      }, error => {
  
      })
  }
  if(this.pageType=='huddle'){
    obj= {
      folder_id: this.folder_id,
      account_id: x.user_current_account.accounts.account_id,
      user_id: x.user_current_account.User.id,
      role_id: x.user_current_account.roles.role_id,
      goal_evidence: Boolean(this.goalEvidence)
    };
    this.detailsService.getActivities(obj).subscribe(data => {
      this.popupisloading= false;
      this.actvities = data;
    }, error => {

    })

  }


  }

  timedateSpanish(timedate, index) {
    let d = timedate.split(',')
    if (index == 1) return d[0]
    if (index == 2) {
      let dd = timedate.split(' ')
      return dd[3] + ' ' + dd[4].toLowerCase();
    }
  }

}
