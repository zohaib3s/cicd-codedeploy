import { Component, EventEmitter, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { NewfolderService } from '@src/project-modules/workspace/services/newfolder.service';
import { TreeComponent } from 'angular-tree-component';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-move-folder-modal',
  templateUrl: './share-folder-modal.component.html',
  styleUrls: ['./share-folder-modal.component.css']
})
export class ShareFolderModalComponent implements OnInit, OnDestroy {
  
  public folderId;
  public translation;
  public isActive: string = 't1';
  public search_Huddle_Input;
  private subscription: Subscription;
  public workspaceFolderData;
  public workspaceFolderLength;
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  @ViewChild('tree') huddleFolderTree: TreeComponent;
  
  constructor(public bsModalRef: BsModalRef, private headerService: HeaderService, public folderService: NewfolderService) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
			this.translation = languageTranslation;
		});
  }

  ngOnInit(): void {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    }

    this.getFolderData();
  }

  public getFolderData() {
    let sessionData: any = this.headerService.getStaticHeaderData();
		let obj: any = {
			account_id: sessionData.user_current_account.accounts.account_id,
      site_id: 1,
      user_id: 18737,
      append_root: 0
		};
    this.subscription = this.folderService.folderTreeViewData(obj).subscribe((data: any) => {
      this.workspaceFolderLength = data.length
      this.workspaceFolderData = data;
    })
  }

  public setIsActive(value) {
    this.isActive = value;
  }

  clearSearchFilter(filter: any) {
    filter.value = '';
    this.search_Huddle_Input = '';
  }

  shareFolder() {
    console.log("We are working on Share folder. Please be patience. We will let you know when are done...hahahahaha!");
    this.bsModalRef.hide();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
