import { Component, OnDestroy, OnInit } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import { NewfolderService } from '@src/project-modules/workspace/services/newfolder.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'share-move-artifacts-dialog',
  templateUrl: './move-artifacts-dialog.component.html',
  styleUrls: ['./move-artifacts-dialog.component.css']
})
export class MoveArtifactDialogComponent implements OnInit, OnDestroy {

  translation: any;
  subscription: Subscription;
  FoldersTreeData;
  artifactId;
  folderId;
  haveFolders;
  public huddleId;

  constructor(private headerService: HeaderService, private activatedRoute: ActivatedRoute, private toastr: ShowToasterService, public bsModalRef: BsModalRef, private newfolderService: NewfolderService) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
  }

  ngOnInit(): void {
    let sessionData: any = this.headerService.getStaticHeaderData();
    let obj: any = {
			account_id: sessionData.user_current_account.accounts.account_id,
			user_id: sessionData.user_current_account.User.id,
      site_id: 1,
      append_root: 1
    };
    if(this.huddleId) {
      obj.account_folder_id = this.huddleId;
    }

    if(this.folderId && !this.haveFolders) {
      obj.skip_folder_id = this.folderId
    }
    this.newfolderService.folderTreeViewData(obj).subscribe((data) => {
      this.FoldersTreeData = this.list_to_tree(data, 'parent');
    });
  }

  private list_to_tree(list, parentProp) {    
		var map = {}, node, roots = [], i;
		for (i = 0; i < list.length; i += 1) {
			map[list[i].id] = i;
			list[i].children = [];
		}
		for (i = 0; i < list.length; i += 1) {
			node = list[i];
			node.name = node.text;
			if (node[parentProp] !== "#") {
				if (list[map[node[parentProp]]]) {
					list[map[node[parentProp]]].children.push(node);
				} else {
				}

			} else {
				roots.push(node);
			}
    }

    if(!this.folderId) {
      roots = roots.filter(function( obj ) {
        return obj.id !== "0";
      });
    } 
		return roots;
	}

  public getMovePath(tree) {
    if (!tree || !tree.treeModel || !tree.treeModel.getActiveNode() || !tree.treeModel.getActiveNode().data) {
      return;
    }
    let head = tree.treeModel.getActiveNode();
    if (tree.treeModel.getActiveNode().id == -1) return [tree.treeModel.getActiveNode().data];
    let arr = [];
    while (head.parent != null) {
      if (head.data) {
        arr.push(head.data);
      }
      else if (head.treeModel.getActiveNode()) {
        arr.push(head.treeModel.getActiveNode().data);
      }
      head = head.parent;
    }
    if (arr.length == 0) return [tree.treeModel.getActiveNode().data];
    return arr.reverse();
  }

  MoveItem(tree) {
    if (!tree.treeModel.getActiveNode()) {
      this.toastr.ShowToastr('info',this.translation.huddle_no_traget_folder);
      return;
    }
    let sessionData: any = this.headerService.getStaticHeaderData();
    let target = tree.treeModel.getActiveNode().data;
    let obj = {
			folder_id: target.id,
      object_id: this.artifactId, //Artifacts id
      site_id: 1,
      account_id: sessionData.user_current_account.accounts.account_id,
      user_id: sessionData.user_current_account.User.id,
      account_folder_id: target.account_folder_id,
      workspace: target.workspace
    }

    this.newfolderService.moveDocumentFolder(obj).subscribe((data: any) => {
        this.toastr.ShowToastr('info',data.message);
    })
    this.bsModalRef.hide();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
