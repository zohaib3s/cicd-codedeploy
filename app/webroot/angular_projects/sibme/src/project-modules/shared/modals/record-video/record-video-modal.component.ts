import { Component, OnInit, OnDestroy, ViewChild, ElementRef, Output, EventEmitter, HostListener } from '@angular/core';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';

import { HeaderService, AppMainService, AppLevelFilestackService } from "@app/services";

import { GLOBAL_CONSTANTS } from "@constants/constant";
import { Router } from '@angular/router';

type RecordingState = 'comfort-zome' | 'startring' | 'started' | 'stopping' | 'stopped' | 'uploading' | 'uploaded';
type CancelType = 'file-cancel' | 'modal-close';

declare var window: any;
declare var document: any;
declare var MediaRecorder: any;
declare var navigator: any;

@Component({
  selector: 'shared-record-video-modal',
  templateUrl: './record-video-modal.component.html',
  styleUrls: ['./record-video-modal.component.css']
})
export class RecordVideoModelComponent implements OnInit, OnDestroy {

  @HostListener("window:beforeunload", ["$event"]) unloadHandler(event: Event) {
    if (this.recordingState !== 'comfort-zome' && this.recordingState !== 'uploaded') event.returnValue = false;
  }

  @ViewChild('confirmModal', { static: false }) confirmModal: ModalDirective;
  @ViewChild('camView', { static: false }) camView: ElementRef;
  @Output() onRecordedUpload: EventEmitter<any> = new EventEmitter<any>();

  public browserSupportedRecording: boolean = false;
  public permissionLoading = true;
  public cam_and_mic_perm = true;
  public window_permission = true;

  // public recordingState: RecordingState = 'uploading';
  public recordingState: RecordingState = 'comfort-zome';
  private mediaRecorder: any;
  private stream: any;
  private streamChunks: any = [];
  private blob: any;
  private recordedTime: number = 0;
  private showRecIcon: boolean = true;
  private oneSecInterval: any = null;
  private halfSecInterval: any = null;
  private cancelType: CancelType;
  public confirmModalRef: BsModalRef;

  public uploadProgress: number = 0;
  private fileStackToken: any;
  // public filename: string = this.makeFileName();
  public filename: string = '';

  private userCurrAcc: any;
  public translation: any = {};
  private subscription: Subscription;
  private uploadProgressSubscription: Subscription;

  constructor(public headerService: HeaderService, public appMainService: AppMainService, public bsModalRef: BsModalRef, public router: Router,
    private appLevelFilestackService: AppLevelFilestackService, private modalService: BsModalService) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
  }

  ngOnInit() {
    if (navigator.mediaDevices && window.MediaRecorder) {
      this.browserSupportedRecording = true;
      setTimeout(() => this.showCamView(), 1000);
    } else {
      setTimeout(() => this.permissionLoading = false, 1000);
    }

    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
  }

  public startRecording() {
    this.recordingState = 'startring';

    this.mediaRecorder = new MediaRecorder(this.stream);
    this.mediaRecorder.addEventListener("dataavailable", (event: any) => this.streamChunks.push(event.data));
    this.mediaRecorder.addEventListener("stop", () => {
      this.recordingState = 'stopped';
      this.filename = this.makeFileName();
      this.blob = new Blob(this.streamChunks);

      this.cleanSubscription();
    });

    setTimeout(() => {
      this.recordingState = 'started';
      this.mediaRecorder.start();
      this.startTimer();
    }, 1000);
  }

  public uploadStream() {
    this.getStreamprogress();

    this.recordingState = 'uploading';
    this.fileStackToken = {};
    this.appLevelFilestackService.upload(this.blob, this.filename, this.userCurrAcc.accounts.account_id, this.fileStackToken).then((uploadedFile: any) => {
      uploadedFile.videoDuration = this.recordedTime;
      this.recordingState = 'uploaded';
      this.onRecordedUpload.emit({ from: 'Record', files: [uploadedFile], video_source: GLOBAL_CONSTANTS.RECORDING_SOURCE.CUSTOM });
      this.bsModalRef.hide();
    }).catch(err => console.error('File stack upload error: ', err));

  }

  private getStreamprogress() {
    this.uploadProgressSubscription = this.appLevelFilestackService.uploadProgress$.subscribe(uploadProgress => this.uploadProgress = uploadProgress);
  }

  public stopRecording() {
    this.recordingState = 'stopping';
    setTimeout(() => this.mediaRecorder.stop(), 1000);
  }

  public retry() {
    this.permissionLoading = true;
    setTimeout(() => this.showCamView(), 1000);
  }

  private showCamView() {
    this.cam_and_mic_perm = true;

    const constraints = { audio: true, video: true };
    navigator.mediaDevices.getUserMedia(constraints).then(stream => {
      this.stream = stream
      this.camView.nativeElement.srcObject = stream;
      this.camView.nativeElement.muted = true;

      setTimeout(() => this.permissionLoading = false, 500);

    }).catch((err) => {
      this.cam_and_mic_perm = false;
      this.permissionLoading = false;
      console.error('Could not get Media: ', err);
    });
  }

  public downloadVideoRecorded() {
    const blobUrl = URL.createObjectURL(this.blob);
    const link = document.createElement("a");
    link.href = blobUrl;
    link.download = this.filename;

    document.body.appendChild(link);

    link.dispatchEvent(new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
      view: window
    }));

    // Remove link from body
    document.body.removeChild(link);
  }

  private startTimer() {
    this.oneSecInterval = setInterval(() => this.recordedTime++, 1000);
    this.halfSecInterval = setInterval(() => this.showRecIcon = !this.showRecIcon, 500);
  }

  /**
   * Create and return a nice looking file name with date and time
   */
  private makeFileName() {
    const date = new Date();
    const month = GLOBAL_CONSTANTS.MONTH_NAMES[date.getMonth()];
    const day = date.getDate();
    const year = date.getFullYear();
    let hours = date.getHours();
    let min: number | string = date.getMinutes();
    let AMPM = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    min = min < 10 ? `0${min}` : min;

    return `Recording-${month}_${day}_${year}_${hours}_${min}_${AMPM}.webm`;
  }

  public cancelFile() {
    this.cancelType = 'file-cancel';
    this.confirmModalRef = this.modalService.show(this.confirmModal, { backdrop: 'static' });
  }

  public closeModal() {
    if (this.recordingState !== 'comfort-zome' && this.recordingState !== 'uploaded') {
      this.cancelType = 'modal-close';
      this.confirmModalRef = this.modalService.show(this.confirmModal, { backdrop: 'static' });

    } else this.bsModalRef.hide();
  }

  private cancelFileStackFileUpload() {
    if (this.fileStackToken) this.fileStackToken.cancel();
    if (this.uploadProgressSubscription) {
      this.appLevelFilestackService.resetUploadProgress();
      this.uploadProgressSubscription.unsubscribe();
    }
  }

  public confirmed() {
    this.cancelFileStackFileUpload();
    this.confirmModalRef.hide();

    if (this.cancelType === 'file-cancel') {
      this.recordingState = 'comfort-zome';
      this.blob = null;
      this.streamChunks = [];
      this.recordedTime = 0;

      this.permissionLoading = true;
      setTimeout(() => this.showCamView(), 1000);
    } else this.bsModalRef.hide();
  }

  private cleanSubscription() {
    if (this.stream) this.stream.getTracks().forEach(track => track.stop());
    clearInterval(this.oneSecInterval);
    clearInterval(this.halfSecInterval);
  }

  ngOnDestroy() {
    this.cleanSubscription();
    this.appLevelFilestackService.resetUploadProgress();
    if (this.uploadProgressSubscription) this.uploadProgressSubscription.unsubscribe();
    this.subscription.unsubscribe();
  }
}