import { Component, OnInit, EventEmitter } from '@angular/core';
import { HeaderService, AppMainService } from '@src/project-modules/app/services';
import { DetailsHttpService } from '@src/project-modules/video-huddle/child-modules/details/servic/details-http.service';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { WorkspaceHttpService } from '@src/project-modules/workspace/services';
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { MainService } from '@src/project-modules/video-page/services';

@Component({
    selector: 'app-category-modal',
    templateUrl: './category-modal.component.html',
    styleUrls: ['./category-modal.component.css']
})
export class CategoryModalComponent implements OnInit {

    description: any = '';
    public translation: any = {};
    private subscription: Subscription;
    artifact;
    pageType;
    private artifactList: any = [];
    sessionData: any = {};
    public categories: any = [];
    public subCategories: any = [];
    public selectedCategories: any = [];
    public loadingCategory = true;
    opts: ISlimScrollOptions;
    scrollEvents: EventEmitter<SlimScrollEvent>;
    public disableBtn = false;


    constructor(public headerService: HeaderService, private detailService: DetailsHttpService, public appMainService: AppMainService,
        public toastr: ShowToasterService, public bsModalRef: BsModalRef, public workService: WorkspaceHttpService,
        public mainService: MainService,) {

        this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
            this.translation = languageTranslation;
        });
        this.sessionData = this.headerService.getStaticHeaderData('user_current_account');
    }

    ngOnInit() {
        this.scrollEvents = new EventEmitter<SlimScrollEvent>();
        this.opts = {
            position: 'right',
            barBackground: '#C9C9C9',
            barOpacity: '0.8',
            barWidth: '10',
            barBorderRadius: '20',
            barMargin: '0',
            gridBackground: '#D9D9D9',
            gridOpacity: '1',
            gridWidth: '0',
            gridBorderRadius: '20',
            gridMargin: '0',
            alwaysVisible: true,
            visibleTimeout: 1000,
        };
        this.description = this.artifact.desc;

        this.artifactList = this.workService.list;

        let obj = {
            account_id: this.artifact.account_id,
            account_folder_id: this.artifact.account_folder_id,
            user_id: this.sessionData.User.id,
        };

        this.appMainService.getCategories(obj).subscribe((res: any) => {
            if (res.data) {
                let categories = res.data;
                this.categories = categories.map(x => ({ ...x, isChecked: false, nocategoryCheck:false, nocategoryName:this.translation.vl_no_subcategory }));
                this.categories.forEach(parent => {
                    if (parent.childs.length > 0) {
                        parent.childs.forEach(child => {
                            child.isChecked = false;
                            child.parentId = parent.subject_id;
                        });
                    }
                });
            }
            if (res.video_categories) {
                res.video_categories.forEach(category => {
                    this.categories.forEach(x => {
                        if (category.subject_id === x.subject_id) {
                            x.isChecked = true;
                            x.nocategoryCheck = true; 
                            this.selectedCategories.push(x.subject_id);
                            if (category.childs.length > 0) {
                                x.nocategoryCheck = false; 
                                category.childs.forEach(subCategory => {
                                    x.childs.forEach(child => {
                                        if (child.subject_id === subCategory.subject_id) {
                                            child.isChecked = true;
                                            this.selectedCategories.push(child.subject_id);
                                        }
                                    });
                                });
                            }
                        }
                    });
                });
            }

            this.loadingCategory = false;
        });
    }
    // change_video_description
    // params : user_id, account_folder_id, document_id, description

    SubmitRename() {
        let obj: any = {};
        let sessionData: any = this.headerService.getStaticHeaderData();

        obj = {
            //account_folder_id, description

            document_id: String(this.artifact.id),
            description: String(this.description),
            account_folder_id: String(this.artifact.account_folder_id),
            user_id: sessionData.user_current_account.User.id,
            account_id: sessionData.user_current_account.accounts.account_id,

        };


        if (this.pageType === 'workspace-page') {
            obj.workspace = 1
        }
        else if (this.pageType === 'library-page') {
            obj.library = 1
        }
        let strObj = JSON.stringify(obj)

        this.detailService.changeDescripton(strObj).subscribe(data => {
            let d: any = data;
            

            let message = d.message || this.translation.artifacts_title_changed + `\n ${d.title}`;
            if (d.success) {
                message.slice(0, 25)
                if (message.length > 25) {
                    this.toastr.ShowToastr('success',message + '.');
                } else {
                    this.toastr.ShowToastr('success',message)
                }
                this.bsModalRef.hide();
            }
            else {
                this.toastr.ShowToastr('info',d.message);

            }
        }, error => {
            this.toastr.ShowToastr('info',error.message);
        });

    }


   

    checkCategory(category,  parentCategory?) {
        category.isChecked = !category.isChecked;
        if (category.isChecked) {
            this.selectedCategories.push(category.subject_id);
            if (category.hasOwnProperty('parentId')) {
            if(parentCategory) parentCategory.nocategoryCheck = false;
                if (!this.selectedCategories.includes(category.parentId)) {
                    let parent= this.categories.find(x => x.subject_id === category.parentId);
                    parent.isChecked=category.isChecked;
                    parent.nocategoryCheck=false;
                    this.selectedCategories.push(parent.subject_id)
                }
            }
          else {
            category.nocategoryCheck = !category.nocategoryCheck;
            category.isChecked = category.nocategoryCheck;
          }
        } else {
             this.selectedCategories = this.selectedCategories.filter(id => id !== category.subject_id);
              if (category.hasOwnProperty('childs')) {
            category.nocategoryCheck = false;
                if (category.childs.length > 0) {
                      category.childs.forEach(child => {
                         this.selectedCategories = this.selectedCategories.filter(id => id !== child.subject_id);
                        child.isChecked = false;
                      });
                }
            }
          if (category.hasOwnProperty('parentId')) {
            if (parentCategory.childs.length > 0) {
              let result = parentCategory.childs.filter(x=> x.isChecked)
              if(result.length <= 0 && parentCategory.isChecked) parentCategory.nocategoryCheck = true
        }
      }
      
        }
    }


    checkNoCategory(category, ischecked){
        category.isChecked =ischecked;
        category.nocategoryCheck = ischecked;
      
        if(ischecked && category.childs.length > 0){
          category.childs.forEach(child => {
            this.selectedCategories = this.selectedCategories.filter(id => id !== child.subject_id);
            child.isChecked = false;
          });
        }
      
        if (category.isChecked) {    
          this.selectedCategories.push(category.subject_id);
        }
        else{
          this.selectedCategories = this.selectedCategories.filter(id => id !== category.subject_id);
        }
      
      }
    updateVideoCategory() {
        this.disableBtn = true;
        let obj = {
            user_id: this.sessionData.User.id,
            account_id: this.sessionData.accounts.account_id,
            account_folder_id: this.artifact.account_folder_id,
            document_id: this.artifact.document_id,
            categories: this.selectedCategories
        };
        this.appMainService.changeVideoCategory(obj).subscribe(res => {
            let data: any = res;
            if(data.status){
                this.mainService.vlSortOnRename.emit(this.artifact.document_id);
            }
            this.bsModalRef.hide();
        });
    }

    updateTitle(art) {
        let item = this.artifactList.findIndex(d => d.id == art.document_id);
        if (this.artifactList[item] && this.artifactList[item].title) {
            this.artifactList[item].title = art.title;
            this.workService.setArtifact(this.artifactList);
        }
    }



}
