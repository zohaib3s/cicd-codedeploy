import { Component, OnDestroy, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ShowToasterService } from '@projectModules/app/services';
import { Subject, Subscription } from 'rxjs';

import { HeaderService, AppMainService, HomeService } from "@projectModules/app/services";
import { ISlimScrollOptions, SlimScrollEvent } from 'ngx-slimscroll';
import { PageType } from "@app/interfaces";
import { ITreeOptions, TreeComponent, TreeModel, TreeNode } from 'angular-tree-component';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'shared-artifact-share-modal',
  templateUrl: './artifact-share-modal.component.html',
  styleUrls: ['./artifact-share-modal.component.css']
})
export class ArtifactShareModelComponent implements OnInit, OnDestroy {

  public artifact: any;
  public modal_for: any;
  public pageType: PageType;

  public huddlesrecord: any;
  public notResourceShare: boolean;
  public isitNotes: boolean;
  public isActive: string = 't1';
  public search_Huddle_Input: Subject<string> = new Subject();
  public searchHuddleString: string;
  public search_Account_Input: string;
  public search_videos_Input: string;
  public videoShareFlag: boolean = false;
  public selected_library = false;
  public copy_comments = false;
  public categories: any = [];
  public subCategories: any = [];
  public huddlesFoldersTreeData: any;
  public accountWorkSpaceTreeData: any;
  private userCurrAcc: any;
  private header_data: any;
  public translation: any = {};
  private subscription: Subscription;
  opts: ISlimScrollOptions;
  scrollEvents: EventEmitter<SlimScrollEvent>;
  public selectedCategories: any = [];
  selectedHuddleId: any;
  selectedAccountId: any;
  @ViewChild('tree') huddleFolderTree: TreeComponent;
  @ViewChild('accountTree') accountFolderTree: TreeComponent;
  constructor(public headerService: HeaderService, public appMainService: AppMainService, public bsModalRef: BsModalRef,
    private toastr: ShowToasterService, private homeService: HomeService) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
  }

  ngOnInit() {

    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '10',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      visibleTimeout: 1000,
    }
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
    this.notResourceShare = (this.artifact.doc_type == 1 || (this.artifact.doc_type == 3 && (this.artifact.is_processed == 4 || this.artifact.is_processed == 5))) ? true : false; // in case of video or sync-note
    this.isitNotes = (this.artifact.doc_type == 3 && this.artifact.is_processed < 4) ? true : false;
    this.header_data = this.headerService.getStaticHeaderData();
    this.searchFolder();
    this.getHuddlesDataForShare();
    this.prepareCategoryData(this.appMainService.CategoriesDataForShare);
    this.getCategoriesData();

  }

  private prepareDataForSharing(huddlesDataForShare: any) {

    if (huddlesDataForShare) {
      // Getting deep copy of `huddlesDataForShare` so that checkbox will not be conflicted for one another
      const copyOfDataToShare = JSON.parse(JSON.stringify(huddlesDataForShare));

      if (this.pageType === 'huddle-page' || this.pageType === 'huddle-video-page') {
        let index = copyOfDataToShare.all_huddles.findIndex(item => item.account_folder_id == this.artifact.account_folder_id);
        if (index > -1) copyOfDataToShare.all_huddles.splice(index, 1);
      } else if ((this.pageType === 'workspace-page' || this.pageType === 'workspace-video-page') && copyOfDataToShare.all_accounts) {
        copyOfDataToShare.all_accounts = copyOfDataToShare.all_accounts
          .filter(account => this.userCurrAcc.users_accounts.account_id != account.account_id && account.parmission_access_my_workspace == 1);
      }
      this.huddlesrecord = copyOfDataToShare;
      console.log('Huddles Data', this.huddlesrecord);
    }
  }

  selectAccountTreeNode(evt) {
    let selectedEventData = evt.node.data;
    if (selectedEventData.account_id) {
      this.selectedAccountId = selectedEventData.account_id;
    }
    this.accountFolderTree.treeModel.selectedLeafNodes.map((node: TreeNode) => {

      if (selectedEventData.account_id) {
        if (selectedEventData?.account_id === node.data.account_id) {
        }
        else {
          node.setIsSelected(false);
          node.collapseAll();
        }
      }
      if (selectedEventData.parent) {
        if (selectedEventData === node.data) {
        } else {
          node.setIsSelected(false);
        }
      }
    })
    if (!evt.node.data.account_id) return;
    let user_id = this.header_data.user_current_account.users_accounts.user_id;
    let obj = {
      account_id: 2936,
      // account_id: evt.node.data.account_id,
      site_id: this.header_data.user_current_account.users_accounts.site_id,
      user_id: user_id,
      append_root: 0
    }
    let ref = this.homeService.GetFolderTreeView(obj).subscribe((data) => {
      this.accountWorkSpaceTreeData = this.list_to_tree(data, 'parent');
      if (!evt.node.data.children) {
        evt.node.data.children = [];
        evt.node.data.children = this.accountWorkSpaceTreeData;
      }
      this.accountFolderTree.treeModel.update();
      ref.unsubscribe();
    });
  }

  selectTreeNode(evt) {
    let selectedEventData = evt.node.data;
    if (selectedEventData.account_folder_id) {
      this.selectedHuddleId = selectedEventData.account_folder_id;
      // this.huddleFolderTree.treeModel.collapseAll();
    }
    //  this.getHuddlesFoldersTree(evt.node.data.account_folder_id);
    this.huddleFolderTree.treeModel.selectedLeafNodes.map((node: TreeNode) => {
      if (selectedEventData.id) {
        if (selectedEventData?.id === node.data.id) {
        }
        else {
          node.setIsSelected(false);
          // node.collapseAll();
        }
      }
      if (selectedEventData.parent) {
        if (selectedEventData === node.data) {
        } else {
          node.setIsSelected(false);
        }
      }
    })
}


  filterTreeHuddles(filterValue) {
    this.huddleFolderTree.treeModel.filterNodes(filterValue.value);
  }

  public OnSearchChange(name) {
    this.search_Huddle_Input.next(name);
  }

  public searchFolder() {
    this.search_Huddle_Input
      .pipe(
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(value => {
        if(this.searchHuddleString == '') {
          this.getHuddlesDataForShare();
        } else{
          let obj = {
            user_id: this.userCurrAcc.User.id,
            account_id: this.userCurrAcc.accounts.account_id,
            site_id: this.userCurrAcc.users_accounts.site_id,
            name: this.searchHuddleString,
            from_web: 1,
            is_sharing_folder: 0
          };
          if(this.modal_for == 'for_folder_share') obj.is_sharing_folder = 1;

          this.appMainService.getSearchCategories(obj).subscribe((res: any) => {
            if (res) {
              this.appMainService.updateHuddlesDataForShare(res);
              this.prepareDataForSharing(this.appMainService.huddlesDataForShare);
            }
          });
        }
      });
  }

  clearSearchFilter(filter: any) {
    // filter.value = '';
    // this.searchHuddleString = '';
    // this.huddleFolderTree.treeModel.clearFilter();
    // this.search_Huddle_Input.next('');
    this.getHuddlesDataForShare();
  }
  private list_to_tree(list, parentProp) {
    var map = {}, node, roots = [], i;
    for (i = 0; i < list.length; i += 1) {
      map[list[i].id] = i;
      list[i].children = [];
    }
    for (i = 0; i < list.length; i += 1) {
      node = list[i];
      node.name = node.text;
      if (node[parentProp] !== "#") {
        if (list[map[node[parentProp]]]) {
          list[map[node[parentProp]]].children.push(node);
        } else {
        }

      } else {
        roots.push(node);
      }
    }
    return roots;
  }

  options: ITreeOptions = {
    useCheckbox: true,
    getChildren: this.getChildren.bind(this),
    useTriState: false,
    allowDrag: false,
    allowDrop: false,
    allowDragoverStyling: false,
  };

  public getChildren(data : any){
    if (!data.data.account_folder_id) return;
    this.selectedHuddleId = data.data.account_folder_id;
    let user_id = this.header_data.user_current_account.users_accounts.user_id;
    let obj = {
      account_id: data.data.account_id,
      site_id: this.header_data.user_current_account.users_accounts.site_id,
      user_id: user_id,
      account_folder_id: data.data.account_folder_id,
      append_root: 0
    }
    let ref = this.homeService.GetFolderTreeView(obj).subscribe((res: any) => {
      this.huddlesFoldersTreeData = this.list_to_tree(res, 'parent');

      if (!data.data.children) {
        data.data.children = [];
        data.data.children = this.huddlesFoldersTreeData;
      }
      this.huddleFolderTree.treeModel.update();
      // this.huddleFolderTree.treeModel.collapseAll();
      // ref.unsubscribe();
      return new Promise((resolve, reject) => {
        setTimeout(() => resolve(data.data.children), 1000);
      });

    });
  }

  public getHuddlesDataForShare() {
    const obj = {
      user_id: this.userCurrAcc.User.id,
      account_id: this.userCurrAcc.accounts.account_id,
      artifact_tree_view: true
    };
    this.appMainService.getHuddlesForCopy(obj).subscribe((data: any) => {
      data.all_huddles = data.all_huddles.filter(huddles => huddles.meta_data_value != 3);

      this.appMainService.updateHuddlesDataForShare(data);

      this.prepareDataForSharing(this.appMainService.huddlesDataForShare);

    });
  }

  public setIsActive(value) {
    this.isActive = value;
  }

  public getLibPerm() {
    return this.notResourceShare && this.userCurrAcc.users_accounts.permission_access_video_library == 1 && this.header_data.user_permissions.UserAccount.permission_video_library_upload == 1;
  }

  public copyVideo() {
    if (this.categories.length > 0) {
      if (this.isActive === 't2') {
        this.selected_library = true;
      } else {
        this.selected_library = false;
      }
    }
    this.videoShareFlag = true;

    if (this.isActive === 't3') {
      let selected_account = [];

      for (let i = 0; i < this.huddlesrecord.all_accounts.length; i++) {
        if (this.huddlesrecord.all_accounts[i].selected) {
          selected_account.push(this.huddlesrecord.all_accounts[i].account_id)
        }
      }

      if (selected_account.length > 0) {
        let sessionData: any = this.headerService.getStaticHeaderData();
        var ids = selected_account;
        let obj = {
          account_ids: ids,
          document_id: this.artifact.doc_id,
          user_id: sessionData.user_current_account.User.id,
          copy_notes: this.copy_comments
        }
        if (this.isitNotes) {
          obj.copy_notes = true;
        }
        this.appMainService.copyVideoToAccounts(obj).subscribe((data: any) => {
          this.toastr.ShowToastr('info',data.message);
          if (data.success) this.bsModalRef.hide();
          else this.videoShareFlag = false;
        });
      }

      if ( selected_account.length == 0) {
        if (!this.notResourceShare) {
          this.toastr.ShowToastr('info',this.translation.artifacts_please_select_huddle);
          this.videoShareFlag = false;
        } else {
          if (this.pageType == 'library-page') {
            this.toastr.ShowToastr('info',this.translation.artifacts_please_select_huddle_or_account);
          }
          else {
            this.toastr.ShowToastr('info',this.translation.artifacts_please_select_huddle_or_library);
          }
          this.videoShareFlag = false;
        }
      }
    } else {
      let selected_huddle_folder_id: any;

      for (let i = 0; i < this.huddleFolderTree?.treeModel.selectedLeafNodes.length; i++) {
        if (this.huddleFolderTree.treeModel.selectedLeafNodes[i].data.parent) {
          selected_huddle_folder_id = this.huddleFolderTree.treeModel.selectedLeafNodes[i].data.id;
        }
      }

      if (this.selectedHuddleId || this.selected_library) {
        let sessionData: any = this.headerService.getStaticHeaderData();
        var ids = [];
        if(!this.selected_library) ids.push(this.selectedHuddleId);
        
        if (this.selected_library) {
          ids.push("-1");
        }
        this.selectedCategories = [...new Set(this.selectedCategories.map(x => x))];
        let obj: any = {
          document_id: this.artifact.doc_id,
          account_folder_id: ids,
          parent_folder_id: selected_huddle_folder_id,
          current_huddle_id: this.artifact.account_folder_id,
          account_id: sessionData.user_current_account.accounts.account_id,
          user_id: sessionData.user_current_account.User.id,
          copy_notes: this.copy_comments,
          doc_type: this.artifact.doc_type,
          categories: this.selectedCategories
        }

        if (this.isitNotes) {
          obj.copy_notes = true;
          obj.is_scripted_note = true;
        }

        if (this.pageType == 'workspace-page') {
          obj.from_workspace = 1;
        }

        this.appMainService.copyArtifact(obj).subscribe((data: any) => {
          {
            this.toastr.ShowToastr('info',data.message);
            if (data.success) this.bsModalRef.hide();
            else this.videoShareFlag = false;
          }
        });
      }

      if (!this.selectedHuddleId && !this.selectedAccountId && !this.selected_library) {
        if (!this.notResourceShare) {
          this.toastr.ShowToastr('info',this.translation.artifacts_please_select_huddle);
          this.videoShareFlag = false;
        } else {
          if (this.pageType == 'library-page') {
            this.toastr.ShowToastr('info',this.translation.artifacts_please_select_huddle_or_account);
          }
          else {
            this.toastr.ShowToastr('info',this.translation.artifacts_please_select_huddle_or_library);
          }
          this.videoShareFlag = false;
        }
      }
    }
  }

  public copyFolder() {
    this.videoShareFlag = true;
    let selected_huddle_folder_id: any;

    for (let i = 0; i < this.huddleFolderTree?.treeModel.selectedLeafNodes.length; i++) {
      if (this.huddleFolderTree.treeModel.selectedLeafNodes[i].data.parent) {
        selected_huddle_folder_id = this.huddleFolderTree.treeModel.selectedLeafNodes[i].data.id;
      }
    }

    if (!this.selectedHuddleId && this.isActive == 't1') {
      if (!this.notResourceShare) {
        this.toastr.ShowToastr('info',this.translation.artifacts_please_select_huddle);
        this.videoShareFlag = false;
      } else {
        this.toastr.ShowToastr('info',this.translation.artifacts_please_select_huddle_or_library);
        this.videoShareFlag = false;
      }
    } else {
      let sessionData: any = this.headerService.getStaticHeaderData();
      let obj: any = {
        user_id: sessionData.user_current_account.User.id,
        account_id: sessionData.user_current_account.accounts.account_id,
        copy_notes: this.copy_comments,
        site_id: sessionData.site_id,
        workspace: 0,
        folder_id: this.artifact.id,
        dest_folder_id:selected_huddle_folder_id,
        dest_huddle_id:this.selectedHuddleId
      }

      if (this.isActive === 't3') {
        for (let i = 0; i < this.huddlesrecord.all_accounts.length; i++) {
          if (this.huddlesrecord.all_accounts[i].selected == true) {
            obj.dest_account_id = this.huddlesrecord.all_accounts[i].account_id;
          }
        }
        obj.workspace = 1;
        obj.dest_huddle_id = 0;
      }

      if (this.pageType == 'huddle-page') {
        obj.from_workspace = 0;
        obj.account_folder_id = this.artifact.account_folder_id;
      }

      if (this.pageType == 'workspace-page') {
        obj.from_workspace = 1;
        obj.account_folder_id = 0;
      }

      this.appMainService.shareFolders(obj).subscribe((data: any) => {
        {
          this.toastr.ShowToastr('info',data.message);
          if (data.status) this.bsModalRef.hide();
          else this.videoShareFlag = false;
        }
      });
    }
  }

  public checkboxCheckedPrevent(id){
    for (let i = 0; i < this.huddlesrecord.all_accounts.length; i++) {
      if (this.huddlesrecord.all_accounts[i].id != id) {
        if(this.huddlesrecord.all_accounts[i].selected) this.huddlesrecord.all_accounts[i].selected = false;
      }
    }
  }

  private getCategoriesData() {
    const obj = {
      user_id: this.userCurrAcc.User.id,
      account_id: this.userCurrAcc.accounts.account_id,
    };
    this.appMainService.getCategories(obj).subscribe((res: any) => {
      if (res.data) {
        let categories = res.data;
        categories = categories.map(x => ({ ...x, isChecked: false, nocategoryCheck: false, nocategoryName: this.translation.vl_no_subcategory }));
        categories.forEach(parent => {
          if (parent.childs.length > 0) {
            parent.childs.forEach(child => {
              child.isChecked = false;
              child.parentId = parent.subject_id;
            });
          }
        });

        this.appMainService.updateCategoriesDataForShare(categories);
        this.prepareCategoryData(this.appMainService.CategoriesDataForShare);
      }
    });
  }

  private prepareCategoryData(categoriesData) {
    // if (categoriesData) {
    this.categories = categoriesData;
    // } else {
    //this.getCategoriesData();
    //}
  }

  checkCategory(category, parentCategory?) {
    category.isChecked = !category.isChecked;
    if (category.isChecked) {
      this.selectedCategories.push(category.subject_id);
      if (category.hasOwnProperty('parentId')) {
        if (parentCategory) parentCategory.nocategoryCheck = false;
        if (!this.selectedCategories.includes(category.parentId)) {

          let parent = this.categories.find(x => x.subject_id === category.parentId);
          parent.isChecked = category.isChecked;
          parent.nocategoryCheck = false;
          this.selectedCategories.push(parent.subject_id)
        }
      }
      else {
        category.nocategoryCheck = !category.nocategoryCheck;
        category.isChecked = category.nocategoryCheck;
      }
    } else {
      this.selectedCategories = this.selectedCategories.filter(id => id !== category.subject_id);
      if (category.hasOwnProperty('childs')) {
        category.nocategoryCheck = false;
        if (category.childs.length > 0) {
          category.childs.forEach(child => {
            this.selectedCategories = this.selectedCategories.filter(id => id !== child.subject_id);
            child.isChecked = false;
          });
        }
      }
      if (category.hasOwnProperty('parentId')) {
        if (parentCategory.childs.length > 0) {
          let result = parentCategory.childs.filter(x => x.isChecked)
          if (result.length <= 0 && parentCategory.isChecked) parentCategory.nocategoryCheck = true
        }
      }

    }
  }

  checkNoCategory(category, ischecked) {
    category.isChecked = ischecked;
    category.nocategoryCheck = ischecked;

    if (ischecked && category.childs.length > 0) {
      category.childs.forEach(child => {
        this.selectedCategories = this.selectedCategories.filter(id => id !== child.subject_id);
        child.isChecked = false;
      });
    }

    if (category.isChecked) {
      this.selectedCategories.push(category.subject_id);
    }
    else {
      this.selectedCategories = this.selectedCategories.filter(id => id !== category.subject_id);
    }

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
