import { Component, OnInit } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { DetailsHttpService } from '@src/project-modules/video-huddle/child-modules/details/servic/details-http.service';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { WorkspaceHttpService } from '@src/project-modules/workspace/services';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { MainService } from '@src/project-modules/video-page/services';

@Component({
  selector: 'app-description-modal',
  templateUrl: './description-modal.component.html',
  styleUrls: ['./description-modal.component.css']
})
export class DescriptionModalComponent implements OnInit {
  
  maxChars=150;
  description: any = '';
  public translation: any = {};
  private subscription: Subscription;
  artifact;
  pageType;
  private artifactList:any = [];
  sessionData: any = {};
  

  constructor(public headerService: HeaderService, private detailService: DetailsHttpService,  public mainService: MainService,
     public toastr: ShowToasterService,public bsModalRef: BsModalRef, public workService: WorkspaceHttpService) { 

      this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
        this.translation = languageTranslation;
      });
     }

  ngOnInit() {
    var value;
    this.description = this.artifact.desc;
    this.artifactList = this.workService.list;
    this.sessionData = this.headerService.getStaticHeaderData();
  
  }
  // change_video_description
  // params : user_id, account_folder_id, document_id, description

  SubmitRename() {
    let obj: any = {};
    let sessionData: any = this.headerService.getStaticHeaderData();
   
    obj = { 
      //account_folder_id, description
     
      document_id: String(this.artifact.document_id),
      description :String(this.description),
      account_folder_id: String(this.artifact.account_folder_id), 
      user_id: sessionData.user_current_account.User.id,
      account_id: sessionData.user_current_account.accounts.account_id,
     };
    
    
    if(this.pageType === 'workspace-page' ){
      obj.workspace=1
    }
    else if (this.pageType === 'library-page') {
        obj.library=1
    }
    // if(String(this.description).replace(/\s/g, "").length == 0)
    // {
    //   this.toastr.ShowToastr('error','Description cannot be empty.');
    // }
    if(String(this.description).replace(/\s/g, "").length > 150)
    {
      this.toastr.ShowToastr('error',this.translation.vl_description_limit);
    }
    else
    {
    let strObj = JSON.stringify(obj)

      this.detailService.changeDescripton(strObj).subscribe(data => {
        let d: any = data;
        // this.updateTitle(obj)

        // let message = d.message || this.translation.artifacts_title_changed + `\n ${d.title}`
        if (d.status) {
          if(this.pageType === 'library-page'){
            this.mainService.vlSortOnRename.emit(this.artifact.document_id);
          }
          // message.slice(0, 25)
          // if (message.length > 25) {
          //   this.toastr.ShowToastr('success',message + '.')
          // } else {
          //   this.toastr.ShowToastr('success',message)
          // }

          this.toastr.ShowToastr('success',this.translation.artifacts_title_changed)
          this.bsModalRef.hide()
        }
        else {
          this.toastr.ShowToastr('info',d.message)

        }
      }, error => {
        this.toastr.ShowToastr('info',error.message)
      })


    }
    
    // else {
    //   if (this.artifact.doc_type == 1) {
    //     this.toastr.ShowToastr('info',this.translation.artifacts_video_name_Should_not_be_empty);
    //   } else if (this.artifact.doc_type == 2) {
    //     this.toastr.ShowToastr('info',this.translation.artifacts_resource_name_should_not_be_empty);
    //   } else {
    //     this.toastr.ShowToastr('info',this.translation.artifacts_scripted_note_name_should_not_be_empty);
    //   }
    // }



  }


  updateTitle(art) {
    let item = this.artifactList.findIndex(d => d.id == art.document_id);
    if(this.artifactList[item] && this.artifactList[item].title){
      this.artifactList[item].title = art.title;
      this.workService.setArtifact(this.artifactList);
    }
  }



}
