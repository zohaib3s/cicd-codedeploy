import { Component, OnInit } from '@angular/core';
import { HeaderService } from '@src/project-modules/app/services';
import { ShowToasterService } from '@projectModules/app/services';
import { DetailsHttpService } from '@src/project-modules/video-huddle/child-modules/details/servic/details-http.service';
import { Subscription } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { WorkspaceHttpService } from '@src/project-modules/workspace/services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.css']
})
export class DeleteModalComponent implements OnInit {
  confirmDelete: any = '';
  artifact;
  pageType;
  huddle_type;
  page;
  sessionData: any = {};
  public showConfirmationDialog: boolean = false;
  public showEdtpaConfirmationDialog: boolean = false;
  public assignmentCon_Msg = '';
  public Inputs: GeneralInputs;
  private subscription: Subscription;
  public translation: any = {};
  constructor(public headerService: HeaderService, public toastr: ShowToasterService,
    private detailService: DetailsHttpService, public bsModalRef: BsModalRef,
    public workService: WorkspaceHttpService, private router:Router) {
      this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
        this.translation = languageTranslation;
        this.Inputs = { NewFolderName: "", Confirmation: "", ConfirmationKey: this.translation.Huddle_confirmation_bit };
      });

  }

  ngOnInit() {

    // console.log('check');
    // debugger;
  this.sessionData = {};
  this.sessionData = this.headerService.getStaticHeaderData();
  if(this.sessionData.user_current_account.User.id != this.artifact.created_by && this.huddle_type == 2){
    this.showConfirmationDialog = true;
  } else {
    this.showConfirmationDialog = false;
  }
  if(!this.artifact.doc_id)
  this.artifact.doc_id=this.artifact.id;
  }

  public TriggerTextChange(ev) {

		if (ev.keyCode == 13) {
			this.SubmitDelete()

		}
	}
  SubmitDelete(is_confirmed?) {

    // console.log('delete');
    // debugger;
    // return;
    if (this.Inputs.ConfirmationKey != this.Inputs.Confirmation) {

			this.toastr.ShowToastr('info',this.translation.huddle_you_typed_in + " '" + this.Inputs.ConfirmationKey + "' ");
			return;
    }
    let obj: any = {};
    this.confirmDelete = 'DELETE';
    obj = {
      document_id: this.artifact.doc_id,
      huddle_id: this.artifact.account_folder_id,
      user_id: 0,
      doc_type: this.artifact.doc_type,

    };
    ({
      User: {
        id: obj.user_id
      }
    } = this.sessionData.user_current_account);

    if(is_confirmed) {
      obj.is_confirmed = true;
    }
    if (this.pageType === 'workspace-page') {
      obj.account_id = this.sessionData.user_current_account.accounts.account_id,
        obj.workspace = 1
    }
    if (this.confirmDelete === 'DELETE') {

      this.detailService.DeleteResource(obj).subscribe(data => {

        let d: any = data;
        if(d?.is_assignment) {
          this.showEdtpaConfirmationDialog = true;
          this.assignmentCon_Msg = d.message;
        }
        this.confirmDelete = '';
        if (d.success) {
          this.bsModalRef.hide();
          if(this.pageType === 'workspace-page' && this.page=='videoPage'){
            this.router.navigate(['/workspace/workspace/home/grid'])
          }
          else if(this.pageType === 'huddle-page' && this.page=='videoPage'){
            this.router.navigate([`/video_huddles/huddle/details/${this.artifact.account_folder_id}/artifacts/grid`]);
          }
          this.removeItem(this.artifact.id);
          let type = this.headerService.isAValidAudio(this.artifact.file_type);
          if (this.artifact.doc_type == 1 && type == false) {
            this.toastr.ShowToastr('info',`${this.translation.workspace_videohasbeendeleted}`);
          } else if (this.artifact.doc_type == 1 && type == true) {
            this.toastr.ShowToastr('info',`${this.translation.workspace_audio_hasbeendeleted}`);
          } else if (this.artifact.doc_type == 2) {
            this.toastr.ShowToastr('info',`${this.translation.workspace_resourcehasbeendeleted}`);
          } else if (this.artifact.doc_type == 3 && this.artifact.is_processed < 4) {
            this.toastr.ShowToastr('info',`${this.translation.workspace_scriptednotehasbeendeleted}`);
          } else if (this.artifact.doc_type == 3 && this.artifact.is_processed >= 4) {
            this.toastr.ShowToastr('info',`${this.translation.workspace_videohasbeendeleted}`);
          } else if (this.artifact.doc_type == 5) {
            this.toastr.ShowToastr('info',`${this.translation.url_deleted}`);
          }
        }
        else {
          if(!d?.is_assignment) {
            this.bsModalRef.hide();
            this.toastr.ShowToastr('info',d.message)
          }
        }
      

      }, error => {
        this.toastr.ShowToastr('error',error.message)
      })
    }
    else
      this.toastr.ShowToastr('info',this.translation.artifacts_please_enter_delete);
  }

  SubmitDeleteWithoutConfirmation(is_confirmed?){

    // console.log('submit without delete');

    // debugger;

    let obj: any = {};
    this.confirmDelete = 'DELETE';
     obj = {
      document_id: this.artifact.doc_id,
      huddle_id: this.artifact.account_folder_id,
      user_id: 0,
      doc_type: this.artifact.doc_type,
      account_id: this.sessionData.user_current_account.accounts.account_id,
    };
    ({
      User: {
        id: obj.user_id
      }
    } = this.sessionData.user_current_account);

    if(is_confirmed) {
      obj.is_confirmed = true;
    }

    if(this.pageType === 'workspace-page' ){
      obj.workspace=1
    }else if (this.pageType === 'library-page') {
        obj.library=1
    }
    if (this.confirmDelete === 'DELETE') {
    
      this.detailService.DeleteResource(obj).subscribe(data => {
        let d: any = data;
        this.confirmDelete = '';
        if(d?.is_assignment) {
          this.showEdtpaConfirmationDialog = true;
          this.assignmentCon_Msg = d.message;
        }
        if (d.success) {
          this.bsModalRef.hide();
          if(this.pageType === 'workspace-page' && this.page=='videoPage'){
            this.router.navigate([`/workspace/workspace/home/grid/${this.artifact.parent_folder_id}`])
          }
          else if(this.pageType === 'huddle-page' && this.page=='videoPage'){
            if(this.artifact.parent_folder_id !== 0 && this.artifact.parent_folder_id) {
              this.router.navigate([`/video_huddles/huddle/details/${this.artifact.account_folder_id}/artifacts/grid/${this.artifact.parent_folder_id}`]);
            } else {
              this.router.navigate([`/video_huddles/huddle/details/${this.artifact.account_folder_id}/artifacts/grid`]);
            }
          }
          else if(this.pageType === 'library-page' && this.page=='videoPage'){
            this.router.navigate([`/VideoLibrary`]);
          }
          this.removeItem(this.artifact.id);
          let type = this.headerService.isAValidAudio(this.artifact.file_type);
          if (this.artifact.doc_type == 1 && type == false) {
            this.toastr.ShowToastr('info',`${this.translation.workspace_videohasbeendeleted}`);
          }
          else if (this.artifact.doc_type == 1 && type == true) {
            this.toastr.ShowToastr('info',`${this.translation.workspace_audio_hasbeendeleted}`);
          }

          else if (this.artifact.doc_type == 2) {
            this.toastr.ShowToastr('info',`${this.translation.workspace_resourcehasbeendeleted}`);
          } else if (this.artifact.doc_type == 3 && this.artifact.is_processed < 4) {
            this.toastr.ShowToastr('info',`${this.translation.workspace_scriptednotehasbeendeleted}`);
          } else if (this.artifact.doc_type == 3 && this.artifact.is_processed >= 4) {
            this.toastr.ShowToastr('info',`${this.translation.workspace_videohasbeendeleted}`);
          }
        }
        else {
          if(!d?.is_assignment) {
            this.bsModalRef.hide();
            this.toastr.ShowToastr('info',d.message)
          }
         
        }
       

      }, error => {
        console.log(error.message)
        //this.toastr.ShowToastr('error',error.message)
      })
    }
    else
      this.toastr.ShowToastr('info',this.translation.artifacts_please_enter_delete);
  }

  removeItem(value) {

    let arr = this.workService.list;
    for (let index = 0; index < arr.length; index++) {
      const element = arr[index];
      if (arr[index].id === value) {
        arr.splice(index, 1);
      }
    }
  }

}


interface GeneralInputs {
	NewFolderName: string,
	Confirmation: string,
	ConfirmationKey: string
}
