import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppMainService, HeaderService } from '@src/project-modules/app/services';
import { DetailsHttpService } from '@src/project-modules/video-huddle/child-modules/details/servic/details-http.service';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { WorkspaceHttpService } from '@src/project-modules/workspace/services';
import { MainService } from '@src/project-modules/video-page/services';
import { BreadCrumbInterface } from '@src/project-modules/app/interfaces';

@Component({
  selector: 'app-rename-modal',
  templateUrl: './rename-modal.component.html',
  styleUrls: ['./rename-modal.component.css']
})
export class RenameModalComponent implements OnInit , OnDestroy {
  rename: any = '';
  public translation: any = {};
  private subscriptions: Subscription = new Subscription();
  artifact;
  pageType;
  private artifactList: any = [];
  sessionData: any = {};
  private breadCrumbs : BreadCrumbInterface[] = [];

  constructor(public headerService: HeaderService, private detailService: DetailsHttpService, public mainService: MainService,
    public toastr: ShowToasterService, public bsModalRef: BsModalRef, public workService: WorkspaceHttpService, private appMainService: AppMainService) {

    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    }));

    this.breadCrumbs = JSON.parse(localStorage.getItem('v_pagebreadcrums'));
  }

  ngOnInit() {
    this.rename = this.artifact.title;
    this.artifactList = this.workService.list;
    this.sessionData = this.headerService.getStaticHeaderData();
  }
  getRenameData(name) {

  }

  SubmitRename() {
    let obj: any = {};
    let sessionData: any = this.headerService.getStaticHeaderData();
    let vidId = this.artifact.id;
    if (this.artifact.id == this.artifact.doc_id) {
      vidId = this.artifact.document_id;
    }
 
    obj = {
      title: this.rename.trim(),
      document_id: String(vidId),
      doc_id: String(this.artifact.doc_id),
      huddle_id: String(this.artifact.account_folder_id),
      user_id: sessionData.user_current_account.User.id,
      account_id: this.sessionData.user_current_account.accounts.account_id
    };


    if (this.pageType === 'workspace-page' || this.pageType === 'workspace-video-page') {
      obj.workspace = 1
    }
    else if (this.pageType === 'library-page') {
      obj.library = 1
    } else if (this.pageType === 'video-page') {
      obj.library = 1
    }
    let strObj = JSON.stringify(obj)
    if (obj.title.length !== 0) {
      this.detailService.RenameResourceTitle(strObj).subscribe(data => {

        let d: any = data;
        this.updateTitle(obj);
        if(this.breadCrumbs) {
          //Breadcrumbs Updation
          this.breadCrumbs.forEach((item)=> {
            if(!item.link) {
              item.title = obj.title;
            }
          })
          this.appMainService.updateBreadCrumb(this.breadCrumbs);
        }
    
        let message = d.message || this.translation.artifacts_title_changed + `\n ${d.title}`

        if (d.success) {
          if (this.pageType === 'video-page') {
            this.getRenameData(this.rename);
            this.bsModalRef.hide();
            return;
          }
          if (this.pageType === 'library-page') {
            this.mainService.vlSortOnRename.emit(this.artifact.doc_id);
          }
          message.slice(0, 25)
          if (message.length > 25) {
            this.toastr.ShowToastr('success',message + '.')
          } else {
            this.toastr.ShowToastr('success',message)
          }
          this.getRenameData(this.rename);
          this.bsModalRef.hide()
        }
        else {
          this.toastr.ShowToastr('info',d.message)

        }

      }, error => {
        this.toastr.ShowToastr('info',error.message)
      })

    }
    else {
      if (this.artifact.doc_type == 1) {
        this.toastr.ShowToastr('info',this.translation.artifacts_video_name_should_not_be_empty);
      } else if (this.artifact.doc_type == 2) {
        this.toastr.ShowToastr('info',this.translation.artifacts_resource_name_should_not_be_empty);
      } else {
        this.toastr.ShowToastr('info',this.translation.artifacts_scripted_note_name_should_not_be_empty);
      }
    }

  }


  updateTitle(art) {
    let item = this.artifactList.findIndex(d => d.id == art.document_id);
    if (this.artifactList[item] && this.artifactList[item].title) {
      this.artifactList[item].title = art.title;
      this.workService.setArtifact(this.artifactList);
    }
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }


}
