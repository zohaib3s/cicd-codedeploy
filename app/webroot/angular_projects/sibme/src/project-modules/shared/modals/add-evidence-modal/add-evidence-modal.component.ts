import { Component, OnInit, ViewChild, EventEmitter, OnDestroy, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ShowToasterService } from '@projectModules/app/services';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { SlimScrollEvent } from 'ngx-slimscroll';
import { debounceTime, switchMap } from 'rxjs/operators';
import { Subscription, Subject } from 'rxjs';

import { HeaderService } from '@app/services';
import { EditGoalService, GoalWorkService } from '@goals/services';
import { DetailsHttpService } from '@videoHuddle/child-modules/details/servic/details-http.service';
import { WorkspaceHttpService } from '@workspace/services';
import { GLOBAL_CONSTANTS } from '@src/constants/constant';
import {TreeNode } from 'angular-tree-component';


type EvidenceTab = 'huddle' | 'workspace' | 'computer';

@Component({
  selector: 'goals-add-evidence-modal',
  templateUrl: './add-evidence-modal.component.html',
  styleUrls: ['./add-evidence-modal.component.css']
})
export class AddEvidenceModalComponent implements OnInit, OnDestroy {
  @ViewChild('evidence_scroll', { static: false }) workspace;
  @ViewChild('tree',{static:false}) tree:any;
  @ViewChild('tree2',{static:false}) tree_2:any;
  @Output() onRecordedUpload: EventEmitter<any> = new EventEmitter<any>();
  public evidenceType: string;
  item_id: any;
  owner_id: any;
  goal_id: any;
  from_component: any;
  from_asessors: boolean;
  public check_share_permission:any;

  public loaded: boolean = false;

  public apiInProgress: boolean = false;
  public searching: boolean = false;
  public switchingTab: boolean = false;
  public EmptyMessage =false;
  public searchString = '';
  public searchPlaceholder: string;
  public noDataFoundText: string;
  public huddleFolders:any = [];
  public disabled = false;

  public activeTab: EvidenceTab = 'workspace';
  private page: any = 1;
  private totalRecords: number = 0;
  public evidenceList: any = [];
  public artifectsList: any = [];
  public evidenceData: any = [];
  submitted_evidence: any;
  public copyVideoNotes: boolean = false;
  selectedEvidence: any;
  selectedFolderInfo: any;
  private subscriptions: Subscription = new Subscription();
  public translation: any = {};
  private userCurrAcc: any;
  isCallIn = false;
  private sessionData: any;

  private evDynamicKeys = {
    video: { searchString: 'search_videos', noData: 'goals_no_video_found', docType: 1 },
    resource: { searchString: 'search_resources', noData: 'goals_no_resource_found', docType: 2 },
    scripted_notes: { searchString: 'search_scripted_notes', noData: 'goals_no_scripted_notes_found', docType: 3 },
    huddle: { searchString: 'search_huddles', noData: 'workspace_nohuddlefound', docType: null }
  };

  public opts: any;
  public scrollEvents: EventEmitter<SlimScrollEvent>;

  private searchQuery: Subject<string> = new Subject();
  private makeHttpCall: Subject<'latest'> = new Subject();
  public FoldersTreeData:any = [];
  private huddleId: any;
  public itemList = [];

  constructor(
    public headerService: HeaderService,
    public toastr: ShowToasterService,
    public modalRef: BsModalRef,
    public goalWorkService: GoalWorkService, public detailsService: DetailsHttpService,
    private workService: WorkspaceHttpService,
    public editGoalServie: EditGoalService, private router: Router) {
      this.disabled = false;
    this.subscriptions.add(this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    }));

    this.subscriptions.add(this.searchQuery.pipe(debounceTime(1000)).subscribe((searchString) => {
      if(!this.switchingTab) {
        // if (searchString) this.searching = true;
        // else this.searching = false;
       
       
        if(this.searchString.length >= 3 || this.searchString.length == 0) {
          this.page = 1;
          this.searching = false;
          this.loaded = false;
        
          this.makeHttpCall.next('latest');
       
        }
      
      }
    }));
    
    if (this.activeTab !== 'computer'){

      this.subscriptions.add(this.makeHttpCall.pipe(switchMap(() => {
        // const route: string = (this.activeTab === 'huddle' || this.evidenceType === 'huddle') ? 'get_huddles' : 'get_workspace_artifects';
        let route = '';
        let postData:any = this.postData();
        if ((this.activeTab === 'huddle' || this.evidenceType === 'huddle') && this.searchString) {
            postData.from_goals = 1;
            // postData.page = 0;
            postData.without_assessment = 1;
          route = 'get_huddles';
        } else if (this.activeTab === 'huddle' || this.evidenceType === 'huddle' && !this.searchString) {
          route = 'get_huddles';
            postData.sort = 0;
            // postData.page = 0;
            if(this.activeTab === 'huddle') {
              postData.without_assessment = 1;
            }
            postData.user_current_account = this.userCurrAcc;
        } else {
          route = 'get_workspace_artifects';
          postData.apply_inner_search = 0;
        }
        let x:any = this.postData();
      
        if(this.evidenceType=='huddle'){
          x.with_assessment=1;
        }
        return this.goalWorkService.getEvidenceArtifacts(postData, route);
      })).subscribe((data: any) => {
      



        this.formatResForDisplay(data);
        this.loaded = true;
        this.apiInProgress = false;
        this.switchingTab = false;
      }));
    }
  }


  ngOnInit() {
    this.sessionData = this.headerService.getStaticHeaderData();
    this.userCurrAcc = this.headerService.getStaticHeaderData('user_current_account');
    this.prepareSlimScrollOptions();

    this.searchPlaceholder = this.translation[this.evDynamicKeys[this.evidenceType].searchString];
    this.noDataFoundText = this.translation[this.evDynamicKeys[this.evidenceType].noData];
    this.makeHttpCall.next('latest');
  }

  public switchTab(tab: EvidenceTab) {
   
    // if(tab == 'computer'){
    //   let x =  document.getElementById('closeBtn');
    //   if(x){
    //     x.click();
    //   }
    // }
    this.switchingTab = true;
    this.activeTab = tab;

    this.loaded = false;
    this.page = 1;
    this.searchString = '';
    this.searching = false;
    this.makeHttpCall.next('latest');
  }

  public onSearch(value: string) {
    if(value.length >= 3) {
      this.searchQuery.next(value);
    } else if(value.length == 0) {
      this.searchQuery.next(value);
    }
  }

  private postData() {
    if (this.searchString) {
      return {
          sort: 'video_title',user_current_account: this.userCurrAcc, from_goals: 1, title: this.searchString, page: this.page, doc_type: this.evDynamicKeys[this.evidenceType].docType,
        user_id: this.userCurrAcc.users_accounts.user_id, account_id: this.userCurrAcc.users_accounts.account_id, role_id: this.userCurrAcc.roles.role_id, with_folders: 1
      };
    } else {
        return {
          sort: 'video_title', from_goals: 1, title: this.searchString, page: this.page, doc_type: this.evDynamicKeys[this.evidenceType].docType,
          user_id: this.userCurrAcc.users_accounts.user_id, account_id: this.userCurrAcc.users_accounts.account_id, role_id: this.userCurrAcc.roles.role_id, with_folders: 1
        };
      }
  }

  private formatResForDisplay(data: any) {
    if (this.page === 1) this.totalRecords = data.total_records || data.huddle_counts || data.total_huddles;
    this.evidenceData = data.data || data.huddles;
    const check = this.evidenceType === 'huddle';


    if (this.page > 1) {
       this.evidenceList = [...this.evidenceList, ...this.evidenceData];
      if(this.searchString) {
        this.FoldersTreeData = data?.folders ? [...this.FoldersTreeData, ...data?.folders] : this.FoldersTreeData;
      } 

      this.artifectsList = data?.artifects ? [...this.artifectsList, ...data?.artifects] : this.artifectsList;
    }
    else {
      this.evidenceList = this.evidenceData;
 
        this.FoldersTreeData = data.folders;
           if(this.FoldersTreeData){
            this.FoldersTreeData.forEach(element => {
              element.hasChildren = true;
           });
      }
      this.artifectsList = data.artifects;
    }

    if(this.activeTab === 'huddle' || this.evidenceType === 'huddle') {
      this.evidenceList.forEach(element => {
        element.hasChildren = true;
     });
    }

    this.mapSubmittedEvidences(this.evidenceData, check);
  }

  mapSubmittedEvidences(evidenceData, check?) {

    if (check) { 
      evidenceData.forEach(evidence => {
        if ((this.selectedEvidence && this.selectedEvidence.huddle_id && this.selectedEvidence.huddle_id == evidence.huddle_id) ||
          this.submitted_evidence.includes(evidence.huddle_id)) {
            evidence.selected = true;
          } 
        else evidence.selected = false;
      });

    } else {
      evidenceData.forEach(evidence => {
        if ((this.selectedEvidence && this.selectedEvidence.id && this.selectedEvidence.id == evidence.id) ||
          this.submitted_evidence.includes(evidence.doc_id)) { 
            evidence.selected = true;
          } 
        else evidence.selected = false;
      });

      this.artifectsList?.forEach(evidence => {
        if (evidence.id === this.selectedEvidence?.id || this.submitted_evidence?.includes(evidence.doc_id)) evidence.selected = true;
        else evidence.selected = false;
      });
    }

  }

  toggleList(selectedEvidence,is_withfoler? : boolean, node? : TreeNode) { 
    
    if(node) {
      
     if(node.data.children?.length > 0) {
      if(selectedEvidence.toggle) {
        selectedEvidence.toggle = false;
        node.collapse();
        return;
      } 
      if(!selectedEvidence.toggle) {
        selectedEvidence.toggle = true;
        node.expand();
        return;
      }
     }
    }
  
   

   
     // remove selected evidence artifact.
   
    selectedEvidence.toggle = !selectedEvidence.toggle;


    if(selectedEvidence.toggle && is_withfoler && node) {
        node.expand();
    } 
    if(!selectedEvidence.toggle && is_withfoler) {
        node.data.isMessage = false;
        node.collapse();
    }



    if (selectedEvidence.toggle) this.getHuddleDocuments(selectedEvidence,is_withfoler,node, false);


  }


  private getHuddleDocuments(selectedEvidence: any, is_withfoler? : boolean, node? : TreeNode, withHuddle?: boolean) {

      this.huddleId = selectedEvidence.huddle_id;
      selectedEvidence.loadingDocuments = true;
      selectedEvidence.documents = [];
      let doc_type;
      if (this.evidenceType == 'video') doc_type = 1;
      if (this.evidenceType == 'resource') doc_type = 2;
      if (this.evidenceType == 'scripted_notes') doc_type = 3;

      let htype;
      if (selectedEvidence.type === 'assessment') htype = 3;
      else if (selectedEvidence.type === 'coaching') htype = 2;
      else htype = 1;
      let data:any;

       data = {
        account_id: this.userCurrAcc.users_accounts.account_id,
        huddle_id: selectedEvidence.huddle_id,
        user_id: this.userCurrAcc.User.id,
        role_id: this.userCurrAcc.roles.role_id,
        doc_type: doc_type,
        htype: htype,
        page: 1,
        apply_inner_search : 0,
        title: "",
        sort: "video_title",
        with_folders: 1,
        from_resources:true,
        parent_folder_id: selectedEvidence.folder_id ? selectedEvidence.folder_id : ''
      };
      // if(is_withfoler) {
      //   data.parent_folder_id = "";
      // }
      if(this.searchString || withHuddle){
        withHuddle ? data.parent_folder_id = selectedEvidence.folder_id : false;
        this.detailsService.GetArtifacts(data).subscribe(res => {
      
          selectedEvidence.loadingDocuments = false;
          let parent_folder = selectedEvidence.folder_id;
          res['message'] ? selectedEvidence.documents = [] : selectedEvidence.documents = res['artifects']['all'];
          selectedEvidence.folders = res['folders'];
          if(selectedEvidence.folders) {
            this.huddleFolders = selectedEvidence.folders
          
          }

      

          if(this.huddleFolders || selectedEvidence.documents) {
            let x = [];

            // added artifact in huddles 
            if(this.huddleFolders) { 
  
              this.huddleFolders.forEach((element) => {
                  element.hasChildren=true;
                  delete element.id;
                  x.push(element);
              })
  
            }
         
            if(selectedEvidence.documents) {
              selectedEvidence.documents.forEach((element) => {
            
                x.push(element)
              });
             }
            
              // x.reverse();
              node.data.children = [];
              node.data.children = x;
              if(node.data.children.length>0){
                node.data.isMessage = false;
              }else{
                node.data.isMessage = true;
              }
              let node1 = null;
              if(is_withfoler) {
                this.tree_2.treeModel.update();
                node1 = this.tree_2.treeModel.getNodeById(selectedEvidence.id);
              } else {
                this.tree.treeModel.update();
                node1 = this.tree.treeModel.getNodeById(selectedEvidence.id);
              }
              
              if(node1){
                node1.expand();
              }
          }

          this.mapSubmittedEvidences(selectedEvidence.documents);
         
        });
      } else {
        this.goalWorkService.getHuddleDocuments(data).subscribe((res: any) => {
          
        
          
       //huddles new design root
       let x = [];
       res.items.forEach((element) => {
         x.push(element);
       });
       if(res.folders) {
        res.folders.forEach((element) => {
          delete element.id;
          element.hasChildren=true;
          x.push(element);
      })
      }
       x.reverse();
       node.data.children = [];
       node.data.children = x;
       if(node.data.children.length>0){
         node.data.isMessage = false;
       }else{
         node.data.isMessage = true;
       }

       this.tree_2.treeModel.update();
       let node1 = this.tree_2.treeModel.getNodeById(selectedEvidence.id);
       if(node1){
         node1.expand();
       }
       this.mapSubmittedEvidences(res.items);
        }, error => {
          selectedEvidence.loadingDocuments = false;
        });
      }
 



  }


  onScroll(event?) {
  
    let element;
    element = this.workspace ? this.workspace.nativeElement : ''

    let atBottom = parseInt(element.scrollHeight) - parseInt(element.scrollTop) <= element.clientHeight + 2;
    if (atBottom && !this.apiInProgress) {

      this.apiInProgress = true;
      if (this.evidenceList.length < this.totalRecords) {

        if(this.searchString) {
          if(this.activeTab === 'huddle' || this.evidenceType === 'huddle') {
            // if huddles with search no call
          } else {
            this.page++;
            this.makeHttpCall.next('latest');   
          }
        } else {
          this.page++;
          this.makeHttpCall.next('latest');   
        }

     
      }

    }

  }

  public topscroll(event){
   
  }

public selected_list=[]
  toggleSelectedEvidence(item: any,node?:any) {

      // if(node && this.selectedEvidence) {
      //   this.selectedEvidence.selected = false;
      // }

    if(this.itemList.length>0){     //enable the Done button by default
      this.isCallIn=false;
    }
    let selectedindex = this.itemList.findIndex(x => x.id == item.id);
    if( item.selected==true && this.itemList[selectedindex]?.selected==true){
      this.itemList[selectedindex].selected=false;
      item.selected=false;
      this.itemList.splice(selectedindex, 1);
      if(this.itemList.length==0){
        this.isCallIn=true;
      }else{
        this.isCallIn=false;
      }
      return;     //on deselect return from here otherwise following code adds new object in this array or we have to rewrite whole logic
    }
     
    if(!this.submitted_evidence.includes(item.doc_id)) {
      this.selectedEvidence = item;
    } else {
      return;
    }
     

    if(!this.from_asessors) {
        if(this.selected_list.length>0){
        this.selected_list[0].selected = false;
      }
    }
    
      if(!node && this.from_asessors){

        if (this.activeTab === 'huddle' && this.searchString == '') {
         this.evidenceList.forEach((evidence: any) => {
        if (Array.isArray(evidence.documents) && evidence.documents.length > 0)
          evidence.documents.forEach(document => {
          
            if (document.id === this.selectedEvidence.id || this.submitted_evidence.includes(document.doc_id)) {
            
              if(!document.selected) {
               
                document.selected = !document.selected;
                const index = this.itemList.indexOf(document);
                this.itemList?.length > 0 && index != -1 ? this.itemList[index] = document : this.itemList.push(document);      
              }
          
            }
            // else document.selected = false;
          });
      });
    } else if (this.activeTab === 'huddle' && this.searchString) {
      this.evidenceList.forEach((evidence: any) => {
        if (Array.isArray(evidence.documents) && evidence.documents.length > 0)
          evidence.documents.forEach(document => {
            if (document.id === this.selectedEvidence.id || this.submitted_evidence.includes(document.doc_id)) {
              if(!document.selected) {

              document.selected = !document.selected;
              const index = this.itemList.indexOf(document);
              this.itemList?.length > 0 && index != -1 ? this.itemList[index] = document : this.itemList.push(document);
              }
            }
          
          });
      });


      this.artifectsList.forEach(evidence => {
        if (evidence.id === this.selectedEvidence.id || this.submitted_evidence.includes(evidence.doc_id)) {
         
          if(!evidence.selected) { 
            evidence.selected = !evidence.selected;
            const index = this.itemList.indexOf(evidence);
            this.itemList?.length > 0 && index != -1 ? this.itemList[index] = evidence : this.itemList.push(evidence);
          }
       
        };
        // else evidence.selected = false;
      });

      this.FoldersTreeData.forEach((evidence: any) => {
            if (Array.isArray(evidence.documents) && evidence.documents.length > 0)
              evidence.documents.forEach(document => {
                if (document.id === this.selectedEvidence.id || this.submitted_evidence.includes(document.doc_id)) {
                  if(!evidence.selected) {
                  
                    document.selected = !document.selected;
                    const index = this.itemList.indexOf(document);
                    this.itemList?.length > 0 && index != -1 ? this.itemList[index] = document : this.itemList.push(document);
                   }
                }
                // else document.selected = false;
              });
              });

        } else {
          this.evidenceList.forEach(evidence => {
            if (evidence.id === this.selectedEvidence.id || this.submitted_evidence.includes(evidence.doc_id)) {
              if(!evidence.selected) {
            
                evidence.selected = !evidence.selected;
                const index = this.itemList.indexOf(evidence);
                this.itemList?.length > 0 && index != -1 ? this.itemList[index] = evidence : this.itemList.push(evidence);
               }
            }
            // else evidence.selected = false;
          });
        }
      }
      else if(!node && !this.from_asessors) {
        if (this.activeTab === 'huddle' && this.searchString == '') {
          this.evidenceList.forEach((evidence: any) => {
         if (Array.isArray(evidence.documents) && evidence.documents.length > 0)
           evidence.documents.forEach(document => {
             if (document.id === this.selectedEvidence.id || this.submitted_evidence.includes(document.doc_id)) document.selected = true;
             else document.selected = false;
           });
       });
     } else if (this.activeTab === 'huddle' && this.searchString) {
       this.evidenceList.forEach((evidence: any) => {
         if (Array.isArray(evidence.documents) && evidence.documents.length > 0)
           evidence.documents.forEach(document => {
             if (document.id === this.selectedEvidence.id || this.submitted_evidence.includes(document.doc_id)) document.selected = true;
             else document.selected = false;
           });
       });
   
 
       this.artifectsList.forEach(evidence => {
         if (evidence.id === this.selectedEvidence.id || this.submitted_evidence.includes(evidence.doc_id)) evidence.selected = true;
         else evidence.selected = false;
       });
      
       this.FoldersTreeData.forEach((evidence: any) => {
             if (Array.isArray(evidence.documents) && evidence.documents.length > 0)
               evidence.documents.forEach(document => {
                 if (document.id === this.selectedEvidence.id || this.submitted_evidence.includes(document.doc_id)) document.selected = true;
                 else document.selected = false;});
               });

         } else {
           this.evidenceList.forEach(evidence => {
             if (evidence.id === this.selectedEvidence.id || this.submitted_evidence.includes(evidence.doc_id)) evidence.selected = true;
             else evidence.selected = false;
           });
          
         }
  
      }
    else{
   
      if(!this.from_asessors) {
        if(this.activeTab === 'huddle') {
          this.evidenceList.forEach((evidence: any) => {
            if (Array.isArray(evidence.documents) && evidence.documents.length > 0)
              evidence.documents.forEach(document => {
                if (document.id === this.selectedEvidence.id || this.submitted_evidence.includes(document.doc_id)) document.selected = true;
                else document.selected = false;
              });
          });
        } else {
          this.evidenceList.forEach(evidence => {
            if (evidence.id === this.selectedEvidence.id || this.submitted_evidence.includes(evidence.doc_id)) evidence.selected = true;
            else evidence.selected = false;
          });
        }
      
       
     if(this.selected_list.length == 0){
       this.selected_list[0] = item;
       item.selected = true;
       this.tree.treeModel.update();
     } else {
      this.selected_list[0].selected = false;
       this.selected_list[0] = item;
       item.selected = true;
       this.tree.treeModel.update();
     }
     if(this.searchString) {
      this.artifectsList.forEach(evidence => {
        if (evidence.id === this.selectedEvidence.id || this.submitted_evidence.includes(evidence.doc_id)) {
          evidence.selected = true;
        }
        else evidence.selected = false;
      });
     }
  
      } else {
        item.selected = true;
        this.itemList.push(item);
         this.tree.treeModel.update();
      }
    }
   

    
    if(this.itemList.length>0){   //enable Done button if there is any item in item list
      this.isCallIn=false;
      item.selected=true;
    }
  
  }
  
  toggleSelectedhuddle(item: any) {

    if(item.selected) return;
    this.selectedEvidence = item;
    this.evidenceList.forEach(evidence => {
      if (evidence.huddle_id === this.selectedEvidence.huddle_id ||
        this.submitted_evidence.includes(evidence.doc_id) || this.submitted_evidence.includes(evidence.huddle_id)) {

          if(this.submitted_evidence.includes(this.selectedEvidence.huddle_id))
          return;
        } 
         if(evidence.huddle_id == item.huddle_id) { 
            evidence.selected = true;
          } else if(!this.submitted_evidence.includes(evidence.huddle_id)) {

            evidence.selected = false;
          }
    });
    
  }

  checkEvidence() {
    if (this.evidenceType == 'video') return 1
    if (this.evidenceType == 'resource') return 2
    if (this.evidenceType == 'huddle') return 3
    if (this.evidenceType == 'scripted_notes') return 5
  }

  save() {
    let docIds = [];
    this.itemList.forEach(element => {
      docIds.push(element.doc_id);
     
    });
    
    this.isCallIn = true;

    if(this.from_component == 'assessement'){
      if(this.itemList?.length<=0 && !this.selectedEvidence){   // do not emit if there is nothing in item list
        return;
      }
      
      this.onRecordedUpload.emit({evidenceType: this.evidenceType , selectedEvidence: this.selectedEvidence, copyVideoNotes: this.copyVideoNotes, selectedDocIds : docIds.toString()});
      this.modalRef.hide();
      this.copyVideoNotes = false;
    } else {
      if(this.itemList.length ==0 && docIds.length == 0) {
        if(this.selectedEvidence) {
            docIds.push(this.selectedEvidence.doc_id);
        }
      }

      const obj = {
        item_id: this.item_id,
        user_id: this.userCurrAcc.User.id,
        goal_id: this.goal_id,
        owner_id: this.owner_id,
        
        evidence_ref_id: this.evidenceType === 'huddle' ? this.selectedEvidence.huddle_id : docIds.toString(),
       
        evidence_type: this.checkEvidence()
      };
    

      this.goalWorkService.saveEvidence(obj).subscribe((res: any) => {
        if (res.success) {
          this.modalRef.hide();
          this.toastr.ShowToastr('success',this.headerService.getGoalAltTranslation(this.translation.evidence_added))
        }
        else {
          this.toastr.ShowToastr('error',this.headerService.getGoalAltTranslation(this.translation.evidence_duplicate))
        }
      }, error => {
        this.modalRef.hide();
      });

    }
  }
public opts_2:any
  private prepareSlimScrollOptions() {
    this.scrollEvents = new EventEmitter<SlimScrollEvent>();
    this.opts = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '6',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      scrollSensitivity: 0,
      visibleTimeout: 1000,

    };
    this.opts_2 = {
      position: 'right',
      barBackground: '#C9C9C9',
      barOpacity: '0.8',
      barWidth: '6',
      barBorderRadius: '20',
      barMargin: '0',
      gridBackground: '#D9D9D9',
      gridOpacity: '1',
      gridWidth: '0',
      gridBorderRadius: '20',
      gridMargin: '0',
      alwaysVisible: true,
      scrollSensitivity: 0,
      visibleTimeout: 1000,

    }
  }

  public onMediaUpload(event: any) {
    this.disabled = true;
    this.itemList = [];
 
    if (event.from && event.files.length) {
    


      const that = this;
      for (const fileKey in event.files) {
        if (event.files.hasOwnProperty(fileKey)) {
          const file = event.files[fileKey];
          const obj: any = {};
          obj.user_current_account = that.userCurrAcc;
          obj.account_folder_id = null;
          obj.huddle_id = null;
          obj.fileStack_handle = file.handle;
          obj.fileStack_url = file.url;
          obj.account_id = that.userCurrAcc.accounts.account_id;
          obj.site_id = that.sessionData.site_id;
          obj.user_id = that.userCurrAcc.User.id;
          obj.current_user_role_id = that.userCurrAcc.roles.role_id;
          obj.current_user_email = that.userCurrAcc.User.email;
          obj.suppress_render = false;
          obj.suppress_success_email = false;
          obj.workspace = true;
          obj.activity_log_id = '';
          obj.direct_publish = event.from === 'Upload';
          obj.video_file_name = file.filename;
          obj.stack_url = file.url;
          obj.video_url = file.key;
          obj.video_id = '';
          obj.video_file_size = file.size;
          obj.direct_publish = true;
          obj.video_title = file.filename.split('.')[0];
          obj.url_stack_check = 1;
          if (event.from === 'Resource') {
            this.workService.uploadResource(obj).subscribe((resource: any) => {
              this.selectedEvidence = resource.data;
              this.toastr.ShowToastr('info',`${this.translation.workspace_newresourceuploaded}`);
              this.save();

            }, error => {

            });
          }
          else {
            file.videoDuration ? obj.video_duration = file.videoDuration : false;
            obj.video_source = (file.video_source) ? file.video_source : GLOBAL_CONSTANTS.RECORDING_SOURCE.FILESTACK;
            obj.millicast_rec_id = (file.millicast_rec_id) ? file.millicast_rec_id : null;
            obj.streamName = (file.streamName) ? file.streamName : null;
            this.workService.uploadVideo(obj).subscribe((uploaded: any) => {
              this.selectedEvidence = uploaded.data;

              const d = uploaded;
              const type = this.headerService.isAValidAudio(d.data.file_type);
              if (type === false) {
                this.toastr.ShowToastr('info',`${this.translation.workspace_newvideouploaded}`);
              }
              else {
                this.toastr.ShowToastr('info',this.translation.workspace_new_audio_uploaded);
              }

              this.save();

            }, error => {
            });
          }

        }
      }
    }
  }
  public OnFolderClick(folder_id) {



  }
  private list_to_tree(list, parentProp) {
    var map = {}, node, roots = [], i;
    for (i = 0; i < list.length; i += 1) {
      map[list[i].id] = i;
      list[i].children = [];
    }
    for (i = 0; i < list.length; i += 1) {
      node = list[i];
      node.name = node.text;
      if (node[parentProp] !== "#") {
        // if you have dangling branches check that map[node[parentProp]] exists
        if (list[map[node[parentProp]]]) {
          list[map[node[parentProp]]].children.push(node);
        }

      } else {
        roots.push(node);
      }
    }
    return roots;
  }

  public getMovePath(tree) {

    if (!tree || !tree.treeModel || !tree.treeModel.getActiveNode() || !tree.treeModel.getActiveNode().data) {
      return;
    }
    let head = tree.treeModel.getActiveNode();

    if (tree.treeModel.getActiveNode().id == -1) return [tree.treeModel.getActiveNode().data];

    let arr = [];


    while (head.parent != null) {

      if (head.data) {

        arr.push(head.data);

      }
      else if (head.treeModel.getActiveNode()) {

        arr.push(head.treeModel.getActiveNode().data);

      }

      head = head.parent;
    }

    if (arr.length == 0) return [tree.treeModel.getActiveNode().data];

    return arr.reverse();

  }

  public onEvent(event){
    event.data.loading = true;
    if(event.data.stats){
      if(this.selectedFolderInfo == event.data.folder_id) {
        return;
      } 
        this.selectedFolderInfo = event.data.folder_id;
     
        this.getFolderRecord(event.data.folder_id,event.data.folder_id,event)

    }
 }

 public options_2={
  getChildren: (node: TreeNode) => {

    
     this.onEventHuddles(node)
  }
 }

 public onEventHuddles = (node) => {

  
   this.getDataForHuddles(node.data,node)
 }

  public getDataForHuddles = (selectedEvidence,event) =>{

    let doc_type;
    if (this.evidenceType == 'video') doc_type = 1;
    if (this.evidenceType == 'resource') doc_type = 2;
    if (this.evidenceType == 'scripted_notes') doc_type = 3;


    let data:any;

     data = {
      huddle_id: selectedEvidence.huddle_id,
      user_id: this.userCurrAcc.User.id,
      role_id: this.userCurrAcc.roles.role_id,
      doc_type: doc_type,
      apply_inner_search:0,
      htype: selectedEvidence.htype,
      account_id: this.userCurrAcc.users_accounts.account_id,
      parent_folder_id:selectedEvidence.folder_id
    };



    this.goalWorkService.getHuddleDocuments(data).subscribe((res: any) => {
     
         let x = [];
         res.items.forEach((element) => {

           x.push(element)
         });
         res.folders.forEach((element) => {
          delete element.id;
          element.hasChildren=true
           x.push(element)
         });
         x.reverse();

         event.data.children = x;
         if(event.data.children.length>0){
          event.data.isMessage = false;
        }else{
         event.data.isMessage = true;
        }
         this.tree_2.treeModel.update();
    let node = this.tree_2.treeModel.getNodeById(event.data.id);
    if(node){
      node.expand()


    }


    }, error => {
      selectedEvidence.loadingDocuments = false;
    });

  }


  public getFolderRecord(id,folder,event){

    if(event.data.huddle_id) {
      this.getHuddleDocuments(event.data, false, event, true );
    }else {

    
    let x:any = this.postData();
    x.page=1;
    x.apply_inner_search = 0;
    x.parent_folder_id = id;
    x.title = '';
    const route: string = (this.activeTab === 'huddle' || this.evidenceType === 'huddle') ? `get_huddles` : `get_workspace_artifects`;
    if(this.activeTab === 'huddle' || this.evidenceType === 'huddle') {
      x.user_current_account = this.userCurrAcc; 
      x.apply_inner_search = undefined;
      x.huddle_sort = 3;
      x.folder_id = event.data.account_folder_id;
      x.parent_folder_id = undefined;
      x.huddle_type = 0;
      x.with_folders = 0;
      if(this.activeTab === 'huddle') {
        x.without_assessment = 1;
      }
    }
   let ref = this.goalWorkService.getEvidenceArtifactsData(x, route).subscribe((res)=>{
     
       let x = [];
       if(this.activeTab === 'huddle' || this.evidenceType === 'huddle') {
        res.huddles.forEach((element) => {
          element.hasChildren=true;
          x.push(element)
        });
       } else {
        if(res.data.length>0){
          res.data.forEach((element) => {
            if (this.submitted_evidence.includes(element.doc_id)) element.selected = true;
            else element.selected = false;
            x.push(element)
          });
         }
       }

    
       if(res.folders){
        if(res.folders.length>0){
          res.folders.forEach((element) => {
            element.hasChildren=true;
            delete element.id;
            x.push(element)
          });
         }
       }
       x.reverse();

       event.data.children = [];
       event.data.children = x;
       if(event.data.children.length>0){
         event.data.isMessage = false;
       }else{
        event.data.isMessage = true;
       }
       this.tree.treeModel.update();
    let node = this.tree.treeModel.getNodeById(folder);
    if(node){
      node.expand();
    }
    event.data.loading = false;
 
    ref.unsubscribe();

     })
    }
  }
  public TestEvent = (event) =>{
   if(event.isExpanded){
    if(event.node.data.children){
      if(event.node.data.children.length>0){
        event.node.data.isMessage = false;
      }
      else{
        event.node.data.isMessage = true;
      }
     }else{
      event.node.data.isMessage = true;
     }
   }
   else{
    event.node.data.isMessage = false;
 

   }

    this.selectedFolderInfo = '';
  }
  options = {
    getChildren: (node: TreeNode) => {
      // fake request here
  
       this.onEvent(node)
    },


  };

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }


}
