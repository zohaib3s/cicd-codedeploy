import { Component, OnDestroy, AfterViewInit, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ShowToasterService } from '@projectModules/app/services';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from "@angular/router";

import { HeaderService, AppMainService } from "@projectModules/app/services";

@Component({
  selector: 'shared-video-crop-modal',
  templateUrl: './video-crop-modal.component.html',
  styleUrls: ['./video-crop-modal.component.css']
})
export class VideoCropModelComponent implements AfterViewInit, OnDestroy, OnInit {

  public artifact: any;

  public isitNotes: boolean = false;
  public isTrimming: boolean = false;
  public sliderValues: any = { min: 0, max: 10, range: [0, 10] }
  public translation: any = {};
  public copy_comments = true;
  private subscription: Subscription;
  pageType;
  parent_folder_id;
  disableCrop: boolean = true;


  constructor(public headerService: HeaderService, public appMainService: AppMainService, public bsModalRef: BsModalRef,
    private toastr: ShowToasterService, private router: Router) {
    this.subscription = this.headerService.languageTranslation$.subscribe(languageTranslation => {
      this.translation = languageTranslation;
    });
  }

  ngOnInit() {
    if (!this.artifact.doc_id) {
      this.artifact.doc_id = this.artifact.video_detail.id;
      this.artifact.account_folder_id = this.artifact.huddle_info.account_folder_id;
    }
   let duration =Number(this.artifact.video_duration);
   this.sliderValues = { min: 0, max: duration, range: [0, duration], duration: duration };
  }

  ngAfterViewInit() {
    document.getElementById(`artifact_${this.artifact.doc_id}`).addEventListener('canplay', (e: any) => {
      this.disableCrop = false;
      let duration = Math.floor(e.target.duration);
      if (duration === Infinity) duration = Number(this.artifact.video_duration);
      if (this.sliderValues.max === duration) return;
      this.sliderValues = { min: 0, max: duration, range: [0, duration], duration: duration };
    });
  }

  public getFormattedTime(totalSeconds) {
    let [hours, minutes, seconds] = [0, 0, 0];
    hours = this.getPadding(Math.floor(totalSeconds / 3600));
    totalSeconds %= 3600;
    minutes = this.getPadding(Math.floor(totalSeconds / 60));
    seconds = this.getPadding(totalSeconds % 60);

    return `${hours}:${minutes}:${seconds}`;
  }

  private getPadding(n) {
    return n.toString().length == 1 ? `0${n}` : n;
  }

  public playImageRemover() {
    document.getElementById("ply_butn").style.display = "none"
  }

  public onPreviewVideo(range) {
    let selector: any = document.getElementById(`artifact_${this.artifact.doc_id}`);

    [selector.currentTime] = range;
    selector.play();

    this.sliderValues.range = range;

    let timer = setInterval(() => {
      if (!selector.paused) {
        if (selector.currentTime > range[1]) {
          selector.pause();
          selector.currentTime = range[0];
          clearInterval(timer);
        }
      }
    });
  }

  public onTrim(range) {
    let obj: any;
    this.isTrimming = true;
    let data: any = this.headerService.getStaticHeaderData();
    
    if(data.user_permissions.accounts.active_transcoder_type == "2") {
      obj = { 
        startTime: parseFloat(range[0] + ".00").toFixed(2),
        endTime: parseFloat(range[1] + ".00").toFixed(2),
        user_id :data.user_current_account.User.id,
        site_id: data.site_id, 
        account_id: data.user_current_account.users_accounts.account_id,
        huddle_id: this.artifact.account_folder_id,
        video_id: this.artifact.doc_id,
        parent_folder_id: this.parent_folder_id
        };
    } else {
      obj = { startVideo: parseFloat(range[0] + ".00").toFixed(2), endVideo: parseFloat(range[1] + ".00").toFixed(2) };
    }
    
    if (this.pageType === 'workspace-page') obj.workspace = 1;
    obj.copy_comments = this.copy_comments;
    this.appMainService.cropVideo(obj, data.user_permissions.accounts.active_transcoder_type, this.artifact.account_folder_id, this.artifact.doc_id).subscribe((data: any) => {
      this.isTrimming = false;
      if(this.getFileExtention(data["document"]) == 'mp4')
      this.toastr.ShowToastr('info',this.translation.artifacts_video_request_has_been_submitted);
      else if (this.getFileExtention(data["document"]) == 'mp3')
      this.toastr.ShowToastr('info',this.translation.artifacts_audio_request_has_been_submitted);
      let sessionData: any = this.headerService.getStaticHeaderData();
      let user_id = sessionData.user_current_account.User.id;
      if (this.copy_comments) {
        if (this.pageType === 'workspace-page') {
          this.headerService.afterWorkspaceTrim(this.artifact.doc_id, data["document"].id, parseFloat(range[0] + ".00").toFixed(2), parseFloat(range[1] + ".00").toFixed(2), user_id).subscribe((data: any) => {
            if(this.parent_folder_id) {
              this.router.navigate(['/workspace/workspace/home/grid/' + this.parent_folder_id]);
            } else {
              this.router.navigate(['/workspace/workspace/home/grid']);
            }
          });
        }
        else {
          this.headerService.afterHuddleTrim(this.artifact.doc_id, data["document"].id, parseFloat(range[0] + ".00").toFixed(2), parseFloat(range[1] + ".00").toFixed(2), user_id, this.artifact.account_folder_id, (this.pageType === 'library-page' ? 1 : 0)).subscribe((data2: any) => {

            if (this.pageType === 'library-page') {
              this.router.navigate(['/VideoLibrary']);
            }
            else {
              if(this.parent_folder_id) {
                this.router.navigate(['/video_huddles/huddle/details/' + this.artifact.account_folder_id + '/artifacts/grid/' + this.parent_folder_id]);
              } else {
                this.router.navigate(['/video_huddles/huddle/details/' + this.artifact.account_folder_id + '/artifacts/grid']);
              }
            }
          });
        }

      }
      if (!this.copy_comments) {
        if (this.pageType === 'library-page') {
          this.router.navigate(['/VideoLibrary']);
        }

      }
      this.bsModalRef.hide();
    });
  }
  public getFileExtention(file:any){
    return file.original_file_name.split('.')[1];
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
