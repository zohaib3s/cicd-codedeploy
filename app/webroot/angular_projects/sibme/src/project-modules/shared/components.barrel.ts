import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from "@angular/router";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { ModalModule } from "ngx-bootstrap/modal";
import { TabsModule } from "ngx-bootstrap/tabs";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { NgSlimScrollModule, SLIMSCROLL_DEFAULTS } from 'ngx-slimscroll';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { RangeSliderModule } from 'ngx-rangeslider-component';

import { LanguageInterceptor } from "@projectModules/app/helpers";
import { BaseService } from '@app/services';
import { MainService } from '@videoPage/services';

import { ArtifactGridBoxComponent, ArtifactListBoxComponent , TrackerFiltersComponent} from './components';

import { TreeModule } from 'angular-tree-component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DragulaModule, DragulaService } from 'ng2-dragula';
import { SearchPipe } from '../trackers/pipe/search.pipe';

import {
    VideoCropModelComponent, ArtifactShareModelComponent, URLArtifactModalComponent, RecordVideoModelComponent, MillicastRecordVideoModelComponent,
    CategoryModalComponent, ActivityModelComponent, RenameModalComponent, DeleteModalComponent, DuplicateResourceModalComponent, DescriptionModalComponent
} from "./modals";

import { CustomSearchPipe, SibmeSlicePipe, SharedTimeFormatPipe } from './pipes';

import { NumberDirective } from './number.directive';
import { ReportsDetailView } from './components/reports-detail-view/reports-detail-view.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgScrollbarModule } from 'ngx-scrollbar';
import { AddEvidenceModalComponent } from './modals/add-evidence-modal/add-evidence-modal.component';
import { FileDropUploadDirective, FSUploaderDirective } from './directives';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale,enGbLocale } from 'ngx-bootstrap/locale';
import { BodyService } from 'src/project-modules/analytics/services/body.service';
defineLocale('es', esLocale);
defineLocale('en', enGbLocale);
import { SharedFolderListLoadingComponent } from './components/list/folder-list-loading/folder-list-loading.component';
import { SharedFolderListViewComponent } from './components/list/folder-list-view/folder-list-view.component'
import { SharedFolderThumbLoadingComponent } from './components/list/folder-thumb-loading/folder-thumb-loading.component'
import { SharedFolderThumbViewComponent } from './components/list/folder-thumb-view/folder-thumb-view.component'
import { SharedFolderModal } from '@src/project-modules/shared/components/list/rename-folder-modal/rename-folder-modal.component'
import { ConfirmationDialogComponent } from './modals/confirmation-dialog/confirmation-dialog.component'
import { MoveArtifactDialogComponent } from './modals/move-artifacts-dialog/move-artifacts-dialog.component'
import { ShareFolderModalComponent } from './modals/share-folder-modal/share-folder-modal.component';
import { DndModule } from "ngx-drag-drop";

export const __IMPORTS = [
    CommonModule,
    RouterModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    TooltipModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    NgSlimScrollModule,
    Ng2SearchPipeModule,
    RangeSliderModule,
    TreeModule.forRoot(),
    DndModule,
    BsDatepickerModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    DragulaModule,
    NgxPaginationModule,
    NgSlimScrollModule,
    NgScrollbarModule

];

export const __DECLARATIONS = [
    ArtifactGridBoxComponent,
    ArtifactListBoxComponent,
    VideoCropModelComponent,
    ArtifactShareModelComponent,
    URLArtifactModalComponent,
    ActivityModelComponent,
    CustomSearchPipe,
    SibmeSlicePipe,
    RenameModalComponent,
    DeleteModalComponent,
    DuplicateResourceModalComponent,
    DescriptionModalComponent,
    CategoryModalComponent,
    TrackerFiltersComponent,
    SearchPipe,
    NumberDirective,
    ReportsDetailView,
    SharedFolderListLoadingComponent,
    SharedFolderListViewComponent,
    SharedFolderThumbLoadingComponent,
    SharedFolderThumbViewComponent,
    SharedFolderModal,
    ConfirmationDialogComponent,
    MoveArtifactDialogComponent,
    ShareFolderModalComponent,
    RecordVideoModelComponent,
    MillicastRecordVideoModelComponent,
    SharedTimeFormatPipe,
    AddEvidenceModalComponent,
    FSUploaderDirective,
    FileDropUploadDirective
];

export const __ENTRY_COMPONENTS = [
    VideoCropModelComponent,
    ArtifactShareModelComponent,
    URLArtifactModalComponent,
    CategoryModalComponent,
    RecordVideoModelComponent,
    MillicastRecordVideoModelComponent,
    ActivityModelComponent,
    RenameModalComponent,
    DeleteModalComponent,
    DuplicateResourceModalComponent,
    DescriptionModalComponent,
    AddEvidenceModalComponent
];

export const __PROVIDERS = [
    MainService,
    DragulaService,
    BodyService,
    BaseService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LanguageInterceptor,
      multi: true
    }
];
