import {Component, OnInit, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from '@src/project-modules/app/services';
import { environment } from '@src/environments/environment';


@Component({
  selector: 'plans-billings-shared',
  templateUrl: './plans-billings-shared.component.html',
  styleUrls: ['./plans-billings-shared.component.css']
})
export class PlansBillingsSharedComponent implements OnInit {

  public cardExpired: boolean;
  public cardFailure: boolean;
   
constructor(
    private router: Router,
    private headerService: HeaderService
    ){
    }

ngOnInit(){
     this.getExpiredCardFlag();
    }

getExpiredCardFlag()
{
  let base_url = environment.baseHeaderUrl + `/angular_header_api/huddle_list`;

  this.headerService.getHeaderData(base_url).subscribe((res: any)=> {
    this.headerService.account_change.emit(res)
    this.cardExpired = res.cardExpired;
    this.cardFailure = res.cardFailure;
    
    if (this.cardFailure == true) {
      this.router.navigate(['account-settings/plans']);
      this.cardExpired = false;
    }
    else{
      this.cardExpired = res.cardExpired;
    }
  });
}
    
}