import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "search",
  pure: true 
})
export class CustomSearchPipe implements PipeTransform {
  transform(items, term, keys?): any {
    if (!items) return items;
    return this.filter(
      items,
      term || "",
      keys ? keys.split(",") : false
    );
  }

  private filter(items, term, keys?) {
    const toCompare = term.toLowerCase();
    let filtered_items = items.filter(item => {
      for (let property in item) {
        if (keys && keys.indexOf(property) < 0) continue;
        if (item[property] === null) continue;
        
        if (item[property].toString().replace(/<.*?>/g, "").toLowerCase().includes(toCompare)) return true;
      }
      return false;
    });
    return filtered_items;
  }
}