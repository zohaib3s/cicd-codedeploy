import { Pipe, PipeTransform } from '@angular/core';

/**
 * Converts number of seconds into time format hh:mm:ss
 * 
 * How to use :  
 * 1. Import Shared module into the module in which you want to use sibmeTimeFormat pipe
 * 2. Use as {{time | sibmeTimeFormat}}
 */

@Pipe({
    name: 'sharedTimeFormatPipe'
})
export class SharedTimeFormatPipe implements PipeTransform {
    transform(time: number = 0): string {
        let hours: any = Math.floor(time / 3600);
        let minutes: any = Math.floor((time - (hours * 3600)) / 60);
        let seconds: any = time - (hours * 3600) - (minutes * 60);

        if (hours < 10) { hours = `0${hours}`; }
        if (minutes < 10) { minutes = `0${minutes}`; }
        if (seconds < 10) { seconds = `0${seconds}`; }
        
        return `${hours}:${minutes}:${seconds}`;
    }
}
