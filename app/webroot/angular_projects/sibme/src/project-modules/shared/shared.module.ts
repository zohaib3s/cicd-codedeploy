import { NgModule } from '@angular/core';

import { __IMPORTS, __DECLARATIONS, __PROVIDERS, __ENTRY_COMPONENTS } from './components.barrel';
import { SharedPageNotFoundComponent } from './shared-page-not-found/shared-page-not-found.component';
import { PlansBillingsSharedComponent } from './plans-billing-shared/plans-billings-shared.component';
import { RouterModule } from '@angular/router';
import { TreeModule } from 'angular-tree-component';


@NgModule({
  imports: [__IMPORTS, RouterModule, TreeModule.forRoot()],
  declarations: [__DECLARATIONS, SharedPageNotFoundComponent, PlansBillingsSharedComponent],
  exports: [__DECLARATIONS,SharedPageNotFoundComponent, PlansBillingsSharedComponent],
  providers: [__PROVIDERS],
  entryComponents: [__ENTRY_COMPONENTS]
})
export class AppLevelSharedModule { }
