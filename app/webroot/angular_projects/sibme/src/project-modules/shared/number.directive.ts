import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[numberOnly]'
})
export class NumberDirective {

  constructor(private el: ElementRef) {
  }
  @HostListener('keypress', ['$event'])
  onKeyDown(event) {
    console.log(this.el.nativeElement.value);
    // Allow Backspace, tab, end, and home keys
 const pattern = /^[0-9\+\-\.]{1,9}$/;

    let inputChar = String.fromCharCode(event.charCode);


    if (event.keyCode != 8 && !pattern.test(inputChar))
      event.preventDefault();

    if (event.charCode == 46 && event.target.value.indexOf('.') >= 0)//has one dot
      event.preventDefault();

    if ((event.charCode == 43 || event.charCode == 45) && event.target.value.toString().length >= 1) //+ or - only comes in the first
      event.preventDefault();
      

    return null;
  }
}