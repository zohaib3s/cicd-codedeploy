export const GLOBAL_CONSTANTS = {
    VIDEO_PAGE_TYPES: {
        SCREEN_SHARING_PREVIEW: 'screen-sharing-preview'
    },
    LAUNCHPAD: {
        LAUNCHPAD_URL: 'launchpad'
    },
    VIDEO_PAGE_TYPE: {
        WORKSPACE: 'workspace-video-page',
        HUDDLE: 'huddle-video-page',
        LIBRARY: 'library-video-page'
    },
    USER_ROLES: {
        VIEWER: { ID: 125 },
        USER: { ID: 120 }
    },
    COMMENT_STATE: {
        POSTED: 'posted',
        UPDATED: 'updated',
        POSTING: 'posting',
        UPDATING: 'updating',
        POSTING_ERROR: 'posting-error',
        UPDATING_ERROR: 'updating-error'
    },
    NAV_ITEMS: { WORKSPACE: 'workspace', HUDDLE: 'huddle', ANALYTICS: 'analytics', PEOPLE: 'people', LIBRARY: 'library', GOALS: 'Goals' },
    // ANALYTICS_TABS: {
    //     OVERVIEW: { label: 'overview', value: 0 },
    //     TAGS_N_PL: { label: 'tags-and-performance-level', value: 1 },
    //     USER_SUMMARY: { label: 'user-summary', value: 2 },
    // }
    ANALYTICS_TABS: [
        { label: 'overview', value: 0 },
        { label: 'tags-and-performance-level', value: 1 },
        { label: 'user-summary', value: 2 },
    ],
    ANALYTICS_PC_TABS: [
        { label: 'overview', value: 0 },
        { label: 'tags-and-performance-level', value: 1 }
    ],
    PAGE_MODE: { ADD: 'add', EDIT: 'edit' },

    QUILL_CONFIGURATION: {
        MODULES: {
            toolbar: {
                container: [
                    [{ 'size': ['small', false, 'large', 'huge'] }],
                    ["bold", "italic", "underline"],
                    [{ header: 1 }, { header: 2 }],
                    [{ list: "ordered" }, { list: "bullet" }],
                    ["link"],
                    [{ color: [] }]
                ]
            }/*,
            imageDropAndPaste: {
                handler: {},
            }*/
            //if we uncomment above code(imageDropAndPaste) all places where quill editor is being used will stop working until we register this module
        },
        CSS_STYLE: { height: '100px' }
    },
    ASSSESSMENT_HUDDLE_FORM_FIELDS: {
        VIDEO: {
            OPTIONS: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
            DEFAULT: 1
        },
        RESOURCE: {
            OPTIONS: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
            DEFAULT: 0
        }
    },
    /*
    RESOURCE_UPLOAD_EXTENSIONS: [
        // As per FileStack conversation, the individual file extensions can work with dot. For example, ['.jpg', '.jpeg', '.png'] is a valid and working solution as well.
        'image/jpg', 'image/jpeg', 'image/png', 'image/gif', 'image/bmp', 'image/svg', 'pdf', 'doc', 'docx', 'odt',
        'ppt', 'pptx', 'potx', 'xls', 'xlsx', 'xlsm', 'rtf', 'csv', 'txt', 'tex'
    ],
    */
    RESOURCE_UPLOAD_EXTENSIONS: [
        '.jpg', '.jpeg', '.png', '.gif', '.bmp', '.svg', '.pdf', '.doc', '.docx', '.odt',
        '.ppt', '.pptx', '.potx', '.xls', '.xlsx', '.xlsm', '.rtf', '.csv', '.txt', '.tex', '.tiff'
    ],
    RESOURCE_UPLOAD_VIDEO_EXTENSIONS:[
        '.3gp','.mkv','.mpg', '.mp2', '.mpeg', '.mpe', '.mpv', '.ogg', '.mp4', '.m4p', '.m4v', '.avi', '.wmv', '.mov', '.qt', '.flv', '.swf','.f4v', 'video/*', 'audio/*'
    ],
    LOCAL_STORAGE: {
        WORKSPACE: {
            VIDEO_COMMENT: 'workspace_video_comment_',
            SYNC_NOTE_COMMENT: 'workspace_sync_comment_',
            VIDEO_COMMENT_TA: 'workspace_video_comment_ta_', // TA sf Try Again
            VIDEO_EDIT_COMMENT: 'workspace_video_edit_comment_',
            SYNC_NOTE_EDIT_COMMENT: 'workspace_sync_edit_comment_',
            VIDEO_EDIT_COMMENT_TA: 'workspace_video_edit_comment_ta_',
            VIDEO_REPLY_TA: 'workspace_play_reply_ta_',
            REPLY: 'workspace_video_reply_',
            SUB_REPLY: 'workspace_video_sub_reply_'
        },
        VIDEO_PAGE: { // TODO: must be change to 'VIDEO'
            COMMENT: 'comment-', // will be formed as 'comment-userId-videoId'
            EDIT_COMMENT: 'edit-comment-', // will be formed as 'edit-comment-userId-videoId'
            TA_COMMENTS: 'ta-comments-', // will be formed as 'ta-comments-userId-videoId'
            TA_EDIT_COMMENTS: 'ta-edit-comments-', // will be formed as 'ta-edit-comments-userId-videoId'
            REPLY: 'reply-', // will be formed as 'reply-userId-commentId'
            TA_EDIT_REPLIES: 'ta-edit-replies-', // will be formed as 'ta-edit-replies-userId-commentId'
            TA_EDIT_SUB_REPLIES: 'ta-edit-sub-replies-' // will be formed as 'ta-edit-sub-replies-userId-videoId'
        },
        VIDEO: {
            REPLY: 'video_reply_',
            SUB_REPLY: 'video_sub_reply_'
        },
        DISCUSSION: {
            ADD: 'add_discussion_',
            EDIT: 'edit_discussion_',
            EDIT_D_TITLE: 'edit_discussion_title_',
            EDIT_D_COMMENT: 'edit_discussion_comment_',
            ADD_TA: 'add_discussion_ta_',
            EDIT_TA: 'edit_discussion_ta_',
            ADD_COMMENT: 'discussion_add_comment_',
            ADD_COMMENT_TA: 'discussion_add_comment_ta_',
            EDIT_COMMENT: 'discussion_edit_comment_',
            EDIT_COMMENT_TA: 'discussion_edit_comment_ta_'
        },
        GOAL: {
            ADD: 'goals-add-', // will be formed as goals-add-accountId-userId
            EDIT: 'goals-edit-', // will be formed as goals-edit-goalId
            ADD_EDIT_TA: 'goals-add-edit-ta-', // will be formed as goals-edit-ta-accountId-userId
            ACTION_ITEM: {
                ADD: 'goals-action-item-add-', // will be formed as goals-action-item-add-goalId
                EDIT: 'goals-action-item-edit-', // will be formed as goals-action-item-edit-goalId
                ADD_EDIT_TA: 'goals-action-item-add-edit-ta-' // will be formed as goals-action-item-add-edit-ta-goalId
            }
        },
    },
    GOALS_TYPE_BY_VALUE: {
        1: { T_KEY: 'template', CLASS: 'template-color', VALUE: 1 },
        2: { T_KEY: 'vd_account', CLASS: 'account-color', VALUE: 2 },
        3: { T_KEY: 'group', CLASS: 'group-color', VALUE: 3 },
        4: { T_KEY: 'individual', CLASS: 'individual-color', VALUE: 4 }
    },
    GOAL_SORT_BY: [
        { T_KEY: 'default_view', VALUE: 'default_view' },
        { T_KEY: 'not_updated', VALUE: 'not_updated' },
        { T_KEY: 'complete', VALUE: 'complete' },
        { T_KEY: 'incomplete', VALUE: 'incomplete' },
        // { T_KEY: 'due_date_all', VALUE: 'due_date' },
        // { T_KEY: 'last_update', VALUE: 'last_updated' },
        // { T_KEY: 'title', VALUE: 'title' },
        { T_KEY: 'in_progress', VALUE: 'in_progress' },
        // { T_KEY: 'pdf_export', VALUE: 'pdf_export' },
        // { T_KEY: 'excel_export', VALUE: 'excel_export' },
    ],
    GOAL_USER_SORT_BY: [
        { T_KEY: 'name_ng', VALUE: 'name' },
        { T_KEY: 'status_all', VALUE: 'status' },
        { T_KEY: 'last_modified_ng', VALUE: 'last_modified' }
    ],
    GOAL_PAGE_MODE: {
        CREATE: 'create',
        VIEW: 'view',
        EDIT: 'edit'
    },
    AMAZON_AVATAR_BASE_PATH: 'https://s3.amazonaws.com/sibme.com/static/users/',
    // added filestack maxSize property 1000GB, this limit should be passed BYTES
    // https://convertlive.com/u/convert/gigabytes/to/bytes#1000
    FILESTACK_MAXSIZE: 1073741824000,

    MONTH_NAMES: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'], 

    BROWSER_PLAYABLE_AUDIO_EXTENSION: {
        opera: ['mp3', 'm4a', 'wav', 'webm', 'ogg'],
        firefox: ['mp3', 'm4a', 'wav', 'webm', 'ogg'],
        safari: ['mp3', 'm4a', 'wav'],
        ie: ['mp3', 'm4a'],
        edge: ['mp3', 'm4a', 'wav', 'ogg'],
        chrome: ['mp3', 'm4a', 'wav', 'webm', 'ogg'],
        chromium: ['mp3', 'm4a', 'wav', 'webm', 'ogg'],
    }, 
    RECORDING_SOURCE: {
        CUSTOM: 'custom_recording_web', 
        FILESTACK: 'filestack_recording_web',
        MILLICAST: 'millicast_recording_web'
    }
}
