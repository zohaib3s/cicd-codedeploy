# Sibme
## _Steps to configure local development environment for Sibme’s Angular project_ 

1. Clone project from repo
    - Replace arifsami3s with your own username. You can also use SSH protocol for cloning
        ```sh
        git clone https://arifsami3s@bitbucket.org/ksaleem81/sibme_prod_optimization.git
        ```
2.  Move to cloned project folder in terminal
    - ```sh
        cd sibme_prod_optimization
        ```
3. Switch to desired branch
    - If you cloned the project for first time it will fetch “master” branch by default, now you must switch branch in which you want to do work, for documentation purpose I am assuming you are working with Hotfixes, so you have to switch branch to Hotfixes 
        ```sh
        git checkout --track origin/HotFixes
        ```
4.	Move to angular project folder
    - ```sh
        cd webroot/angular_projects/sibme
        ```
5.	Install dependencies
    - ```sh
        npm install
        ```
6.	Run angular project
    - Use the below command to compile code instead of “ng serve” because this command will maximize heap memory size which may give error in case of “ng serve” 
        ```sh
        npm start
        ```
    - If you’re running it for first time it will give you an error 
    `ERROR in Maximum call stack size exceeded`
    Nothings to worry, its only compiler warning, you only have to update any file inside angular project e.g. add space character at any line in app.component.ts file, save it and it will compile the code successfully
    - Don’t forget to remove your change i.e. space character from app.component.ts so that it should not go to git commit 
7. Chrome setup
    - Open chrome with security disabled feature (so that there is no CORS issue for localhost) with one of below command
     - For Windows: Write below command in "Run" dialog which can be opened by pressing Windows + r key
     ```sh chrome.exe --disable-web-security --user-data-dir="D:/temp/chrome-dev" ```
     - For Linux: Execute below command in terminal
     ```sh google-chrome --disable-web-security --user-data-dir="/temp/chrome-dev" ```
     - For Mac: Execute below command in terminal
     ```sh open -n -a /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --args --user-data-dir="/tmp/chrome_dev_sess_1" --disable-web-security ```
    - Open http://localhost:4200/login on newly opened chrome instance and login with either these credentials ${username: testinglandingpage9@gmail.com, password: testinglanding1} or any other of your choice and do not forget to mark “Remember me” checkbox so that session on backend works for local

**Note: Kindly use OS’s native terminal for compilation instead of git-bash terminal.**