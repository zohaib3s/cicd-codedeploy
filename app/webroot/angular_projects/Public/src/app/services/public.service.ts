import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class PublicService {

  constructor(private http:HttpClient) { }

  public resetPassword(data){
    // page = page || 1;
    let cookies=document.cookie;;
    let value='LANG'
  const headers = new HttpHeaders({'current-lang':this.parseCookieValue(cookies,value)});
  	let path = environment.APIbaseUrl + "/change_password";
  	return this.http.post(path, data,{headers:headers});

  }


  parseCookieValue(cookieStr, name) {
    name = encodeURIComponent(name);
    for (const cookie of cookieStr.split(';')) {
        /** @type {?} */
        const eqIndex = cookie.indexOf('=');
        const [cookieName, cookieValue] = eqIndex == -1 ? [cookie, ''] : [cookie.slice(0, eqIndex), cookie.slice(eqIndex + 1)];
        if (cookieName.trim() === name) {
            return decodeURIComponent(cookieValue);
        }
    }
    return null;
}

  public getTranslations(){
    let path = environment.baseUrl + "/api/get_translation_settings/change_password";
  	return this.http.post(path,null);
  }

  getLangFromLuman(){
    let path = environment.APIbaseUrl + "/get_language_from_lumen";
  	return this.http.get(path);
  }

  getUserSettingLan(obj){

    let path = environment.baseUrl + "/app/session_update_lumen_side";
  	return this.http.post(path,obj);

  }




}
