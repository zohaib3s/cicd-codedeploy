import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PublicService } from 'src/app/services/public.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  public translations: any = {};
  public model = {
    password: '',
    confirm_password: '',
    key: ''
  }

  loading=true;
  color = '';
  public siteId = 1;
  baseUrl=environment.baseUrl;
  loginUrl=environment.baseUrl+'/users/login'

  public showPassword = 'password';
  public showConfirmPassword = 'password';
  public showPasswordFlag = false;
  public showConfirmPasswordFlag = false;

  constructor(private acRoute: ActivatedRoute, private router: Router,
    private toastr: ToastrService, private publicService: PublicService) {

    this.acRoute.queryParams.subscribe(params => {
      if (params.key) {
        this.model.key = params.key;
        this.siteId = params.site
        if (params.site == 2)
          this.color = '#848688'

        console.log("key:", params);

        this.getLanFromLuman()

      }
      else{
        // window.open(this.loginUrl,'_self')

      }

      // this.router.navigate([this.loginUrl]);

    })
    // let sessionData: any = this.headerService.getStaticHeaderData();
    // if(!_.isEmpty(sessionData))
  }

  ngOnInit() {

  }

  toggleNewPassword(){
    this.showPasswordFlag=!this.showPasswordFlag;
    if(this.showPassword=='password')
    this.showPassword='text';
    else
    this.showPassword='password'

  }

  toggleConfirmPassword(){
    this.showConfirmPasswordFlag=!this.showConfirmPasswordFlag;
    if(this.showConfirmPassword=='password')
    this.showConfirmPassword='text';
    else
    this.showConfirmPassword='password'

  }

  toggleConfirmpassword(){

  }
  getTranslations() {
    this.publicService.getTranslations().subscribe((data: any) => {
      this.translations = data;
      this.loading=false;
      // console.log('translations', data)

    })
  }

  getLanFromLuman() {
    this.publicService.getLangFromLuman().subscribe((data: any) => {
      this.getUserSettingLang(data)

    })
  }
  getUserSettingLang(obj) {
    this.publicService.getUserSettingLan(obj).subscribe((data: any) => {
      this.getTranslations();
    },error=>{
      this.getTranslations();

    })
  }
  showhidePassword(flag, option) {


  }

  resetPassword() {
    // console.log('model', this.model)
    if (this.model.password == '') {
      this.toastr.info(this.translations.new_password_field_required_cp)
    }
    else if (this.model.confirm_password == '') {
      this.toastr.info(this.translations.confirm_password_field_required_cp)
    }
    else if ((this.model.password.length > 0 && this.model.confirm_password.length > 0) && (this.model.password.length < 8 || this.model.confirm_password.length < 8)) {
      this.toastr.info(this.translations.password_limit_cp)
    }

    else if (this.model.password != this.model.confirm_password) {
      this.toastr.info(this.translations.password_dont_match_cp)
    }

    if ((this.model.password.length >= 8 && this.model.confirm_password.length >= 8) && (this.model.password == this.model.confirm_password)) {
      this.publicService.resetPassword(this.model).subscribe((data: any) => {
        // console.log('reset password data', data)
        if (!data.status)
          this.toastr.info(data.message);
        else {
          this.toastr.info(data.message);
          this.model = {
            password: '',
            confirm_password: '',
            key: ''
          }
         window.open(this.loginUrl,'_self')
        }
      }, error => {

      })
    }

  }

}
