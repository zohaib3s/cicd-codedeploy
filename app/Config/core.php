<?php

/**
 * This is core configuration file.
 *
 * Use it to configure core behavior of Cake.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * CakePHP Debug Level:
 *
 * Production Mode:
 *      0: No error messages, errors, or warnings shown. Flash messages redirect.
 *
 * Development Mode:
 *      1: Errors and warnings shown, model caches refreshed, flash messages halted.
 *      2: As in 1, but also with full debug messages and SQL output.
 *
 * In production mode, flash messages redirect after a time interval.
 * In development mode, you need to click the flash message to continue.
 */
Configure::write('debug', 0);

/**
 * Configure the Error handler used to handle errors for your application. By default
 * ErrorHandler::handleError() is used. It will display errors using Debugger, when debug > 0
 * and log errors with CakeLog when debug = 0.
 *
 * Options:
 *
 * - `handler` - callback - The callback to handle errors. You can set this to any callable type,
 *   including anonymous functions.
 *   Make sure you add App::uses('MyHandler', 'Error'); when using a custom handler class
 * - `level` - int - The level of errors you are interested in capturing.
 * - `trace` - boolean - Include stack traces for errors in log files.
 *
 * @see ErrorHandler for more information on error handling and configuration.
 */
Configure::write('Error', array(
    'handler' => 'ErrorHandler::handleError',
    'level' => E_ALL & ~E_DEPRECATED,
    'trace' => true
));

/**
 * Configure the Exception handler used for uncaught exceptions. By default,
 * ErrorHandler::handleException() is used. It will display a HTML page for the exception, and
 * while debug > 0, framework errors like Missing Controller will be displayed. When debug = 0,
 * framework errors will be coerced into generic HTTP errors.
 *
 * Options:
 *
 * - `handler` - callback - The callback to handle exceptions. You can set this to any callback type,
 *   including anonymous functions.
 *   Make sure you add App::uses('MyHandler', 'Error'); when using a custom handler class
 * - `renderer` - string - The class responsible for rendering uncaught exceptions. If you choose a custom class you
 *   should place the file for that class in app/Lib/Error. This class needs to implement a render method.
 * - `log` - boolean - Should Exceptions be logged?
 *
 * @see ErrorHandler for more information on exception handling and configuration.
 */
Configure::write('Exception', array(
    'handler' => 'ErrorHandler::handleException',
    'renderer' => 'ExceptionRenderer',
    'log' => false
));

/**
 * Application wide charset encoding
 */
Configure::write('App.encoding', 'UTF-8');

/**
 * To configure CakePHP *not* to use mod_rewrite and to
 * use CakePHP pretty URLs, remove these .htaccess
 * files:
 *
 * /.htaccess
 * /app/.htaccess
 * /app/webroot/.htaccess
 *
 * And uncomment the App.baseUrl below. But keep in mind
 * that plugin assets such as images, CSS and Javascript files
 * will not work without url rewriting!
 * To work around this issue you should either symlink or copy
 * the plugin assets into you app's webroot directory. This is
 * recommended even when you are using mod_rewrite. Handling static
 * assets through the Dispatcher is incredibly inefficient and
 * included primarily as a development convenience - and
 * thus not recommended for production applications.
 */
//Configure::write('App.baseUrl', env('SCRIPT_NAME'));

/**
 * Uncomment the define below to use CakePHP prefix routes.
 *
 * The value of the define determines the names of the routes
 * and their associated controller actions:
 *
 * Set to an array of prefixes you want to use in your application. Use for
 * admin or other prefixed routes.
 *
 *      Routing.prefixes = array('admin', 'manager');
 *
 * Enables:
 *      `admin_index()` and `/admin/controller/index`
 *      `manager_index()` and `/manager/controller/index`
 *
 */
//Configure::write('Routing.prefixes', array('admin'));

/**
 * Turn off all caching application-wide.
 *
 */
//Configure::write('Cache.disable', true);

/**
 * Enable cache checking.
 *
 * If set to true, for view caching you must still use the controller
 * public $cacheAction inside your controllers to define caching settings.
 * You can either set it controller-wide by setting public $cacheAction = true,
 * or in each action using $this->cacheAction = true.
 *
 */
//Configure::write('Cache.check', true);

/**
 * Enable cache view prefixes.
 *
 * If set it will be prepended to the cache name for view file caching. This is
 * helpful if you deploy the same application via multiple subdomains and languages,
 * for instance. Each version can then have its own view cache namespace.
 * Note: The final cache file name will then be `prefix_cachefilename`.
 */
//Configure::write('Cache.viewPrefix', 'prefix');

/**
 * Session configuration.
 *
 * Contains an array of settings to use for session configuration. The defaults key is
 * used to define a default preset to use for sessions, any settings declared here will override
 * the settings of the default config.
 *
 * ## Options
 *
 * - `Session.cookie` - The name of the cookie to use. Defaults to 'CAKEPHP'
 * - `Session.timeout` - The number of minutes you want sessions to live for. This timeout is handled by CakePHP
 * - `Session.cookieTimeout` - The number of minutes you want session cookies to live for.
 * - `Session.checkAgent` - Do you want the user agent to be checked when starting sessions? You might want to set the
 *    value to false, when dealing with older versions of IE, Chrome Frame or certain web-browsing devices and AJAX
 * - `Session.defaults` - The default configuration set to use as a basis for your session.
 *    There are four builtins: php, cake, cache, database.
 * - `Session.handler` - Can be used to enable a custom session handler. Expects an array of callables,
 *    that can be used with `session_save_handler`. Using this option will automatically add `session.save_handler`
 *    to the ini array.
 * - `Session.autoRegenerate` - Enabling this setting, turns on automatic renewal of sessions, and
 *    sessionids that change frequently. See CakeSession::$requestCountdown.
 * - `Session.ini` - An associative array of additional ini values to set.
 *
 * The built in defaults are:
 *
 * - 'php' - Uses settings defined in your php.ini.
 * - 'cake' - Saves session files in CakePHP's /tmp directory.
 * - 'database' - Uses CakePHP's database sessions.
 * - 'cache' - Use the Cache class to save sessions.
 *
 * To define a custom session handler, save it at /app/Model/Datasource/Session/<name>.php.
 * Make sure the class implements `CakeSessionHandlerInterface` and set Session.handler to <name>
 *
 * To use database sessions, run the app/Config/Schema/sessions.php schema using
 * the cake shell command: cake schema create Sessions
 *
 */
Configure::write('Session', array(
    'cookie' => 'SibmeApp',
    'defaults' => 'php',
    'cookieTimeout' => 0,
    'timeout' => 90
));

/**
 * A random string used in security hashing methods.
 */
Configure::write('Security.salt', 'DYhG93b0qyJfIxfs2guVoUubWwvniR2G5GgaC9mi');

/**
 * A random numeric string (digits only) used to encrypt/decrypt strings.
 */
Configure::write('Security.cipherSeed', '76859309657453542495449683645');

/**
 * Apply timestamps with the last modified time to static assets (js, css, images).
 * Will append a query string parameter containing the time the file was modified. This is
 * useful for invalidating browser caches.
 *
 * Set to `true` to apply timestamps when debug > 0. Set to 'force' to always enable
 * timestamping regardless of debug value.
 */
//Configure::write('Asset.timestamp', true);

/**
 * Compress CSS output by removing comments, whitespace, repeating tags, etc.
 * This requires a/var/cache directory to be writable by the web server for caching.
 * and /vendors/csspp/csspp.php
 *
 * To use, prefix the CSS link URL with '/ccss/' instead of '/css/' or use HtmlHelper::css().
 */
//Configure::write('Asset.filter.css', 'css.php');

/**
 * Plug in your own custom JavaScript compressor by dropping a script in your webroot to handle the
 * output, and setting the config below to the name of the script.
 *
 * To use, prefix your JavaScript link URLs with '/cjs/' instead of '/js/' or use JavaScriptHelper::link().
 */
//Configure::write('Asset.filter.js', 'custom_javascript_output_filter.php');

/**
 * The class name and database used in CakePHP's
 * access control lists.
 */
Configure::write('Acl.classname', 'DbAcl');
Configure::write('Acl.database', 'default');

/**
 * Uncomment this line and correct your server timezone to fix
 * any date & time related errors.
 */
date_default_timezone_set('America/Chicago');

/**
 *
 * Cache Engine Configuration
 * Default settings provided below
 *
 * File storage engine.
 *
 *       Cache::config('default', array(
 *              'engine' => 'File', //[required]
 *              'duration' => 3600, //[optional]
 *              'probability' => 100, //[optional]
 *              'path' => CACHE, //[optional] use system tmp directory - remember to use absolute path
 *              'prefix' => 'cake_', //[optional]  prefix every cache file with this string
 *              'lock' => false, //[optional]  use file locking
 *              'serialize' => true, [optional]
 *      ));
 *
 * APC (http://pecl.php.net/package/APC)
 *
 *       Cache::config('default', array(
 *              'engine' => 'Apc', //[required]
 *              'duration' => 3600, //[optional]
 *              'probability' => 100, //[optional]
 *              'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 *      ));
 *
 * Xcache (http://xcache.lighttpd.net/)
 *
 *       Cache::config('default', array(
 *              'engine' => 'Xcache', //[required]
 *              'duration' => 3600, //[optional]
 *              'probability' => 100, //[optional]
 *              'prefix' => Inflector::slug(APP_DIR) . '_', //[optional] prefix every cache file with this string
 *              'user' => 'user', //user from xcache.admin.user settings
 *              'password' => 'password', //plaintext password (xcache.admin.pass)
 *      ));
 *
 * Memcache (http://www.danga.com/memcached/)
 *
 *       Cache::config('default', array(
 *              'engine' => 'Memcache', //[required]
 *              'duration' => 3600, //[optional]
 *              'probability' => 100, //[optional]
 *              'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 *              'servers' => array(
 *                      '127.0.0.1:11211' // localhost, default port 11211
 *              ), //[optional]
 *              'persistent' => true, // [optional] set this to false for non-persistent connections
 *              'compress' => false, // [optional] compress data in Memcache (slower, but uses less memory)
 *      ));
 *
 *  Wincache (http://php.net/wincache)
 *
 *       Cache::config('default', array(
 *              'engine' => 'Wincache', //[required]
 *              'duration' => 3600, //[optional]
 *              'probability' => 100, //[optional]
 *              'prefix' => Inflector::slug(APP_DIR) . '_', //[optional]  prefix every cache file with this string
 *      ));
 */
/**
 * Configure the cache handlers that CakePHP will use for internal
 * metadata like class maps, and model schema.
 *
 * By default File is used, but for improved performance you should use APC.
 *
 * Note: 'default' and other application caches should be configured in app/Config/bootstrap.php.
 *       Please check the comments in bootstrap.php for more info on the cache engines available
 *       and their settings.
 */
$engine = 'File';

// In development mode, caches should expire quickly.
$duration = '+999 days';
if (Configure::read('debug') > 0) {
    $duration = '+10 seconds';
}

// Prefix each application on the same server with a different string, to avoid Memcache and APC conflicts.
$prefix = 'myapp_';

/**
 * Configure the cache used for general framework caching. Path information,
 * object listings, and translation cache files are stored with this configuration.
 */
Cache::config('_cake_core_', array(
    'engine' => $engine,
    'prefix' => $prefix . 'cake_core_',
    'path' => CACHE . 'persistent' . DS,
    'serialize' => ($engine === 'File'),
    'duration' => $duration
));

/**
 * Configure the cache for model and datasource caches. This cache configuration
 * is used to store schema descriptions, and table listings in connections.
 */
Cache::config('_cake_model_', array(
    'engine' => $engine,
    'prefix' => $prefix . 'cake_model_',
    'path' => CACHE . 'models' . DS,
    'serialize' => ($engine === 'File'),
    'duration' => $duration
));

define('IS_PROD', true);
define('IS_QA', false);
define('IS_LOCAL', false);

if (IS_PROD) {
/**/
//    Configure::write('db_host', "staging-aurora-24-feb-sw5538.cluster-cxhfyn4myjkc.us-east-1.rds.amazonaws.com");
    Configure::write('db_host', "sibme-prod-20-aug-2021-cluster.cluster-c2djmavjxkzh.us-east-1.rds.amazonaws.com");
//    Configure::write('db_host', "staging-aurora-06-jan.cluster-cxhfyn4myjkc.us-east-1.rds.amazonaws.com");
    Configure::write('db_login', "prodsibmeDBA");
    Configure::write('db_password', "Sibmedev123");
/**/

    Configure::write('db_name', "sibmeproduction");
    Configure::write('MinifyAsset', true);

    Configure::write('sibme_base_url', 'https://devops.sibme.com/');
    Configure::write('sibme_api_url', 'https://devopsapi.sibme.com/');

    Configure::write('amazon_base_url', 'http://sibme-production.s3.amazonaws.com/');
    Configure::write('bucket_name', 'sibme-production');
    Configure::write('bucket_name_cdn', 'sibme.com');
    Configure::write('access_key_id', 'AKIAJ4ZWDR5X5JKB7CZQ');
    Configure::write('secret_access_key', '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');
    Configure::write('filepicker_access_key', 'A3w6JbXR2RJmKr3kfnbZtz');
    Configure::write('encoder_provider', '2'); //1 is Zencoder, 2 is Amazon
    Configure::write('trial_duration', 30); // 30 days

    Configure::write('smtp_direct_host', 'smtp.sendgrid.net');
    Configure::write('smtp_direct_host_port', '2525');
//    Configure::write('smtp_direct_user_name', 'davew@sibme.com');
//    Configure::write('smtp_direct_password', 'Sibme20!9!');
    Configure::write('smtp_direct_user_name', 'apikey');
    Configure::write('smtp_direct_password', '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');
    Configure::write('smtp_direct_from', 'info@sibme.com');
    Configure::write('smtp_direct_from_name', 'Sibme');

    Configure::write('smtp_host', 'smtp.sendgrid.net');
    Configure::write('smtp_host_port', '2525');
//    Configure::write('smtp_user_name', 'davew@sibme.com');
//    Configure::write('smtp_password', 'Sibme20!9!');
    Configure::write('smtp_user_name', 'apikey');
    Configure::write('smtp_password', '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');
    Configure::write('smtp_from', 'info@sibme.com');
    Configure::write('smtp_from_name', 'Sibme');

    Configure::write('use_cloudfront', true); // enable/disable cloud front
    Configure::write('cloudfront_keypair', 'APKAJ64SUURDPDPPLA5Q'); // enable/disable cloud front
    Configure::write('cloudfront_host_url', 'https://d1ebp8oyqi4ffr.cloudfront.net'); // enable/disable cloud front
    Configure::write('sibmecdn_host_url', 'https://d1c4hn49irt1g3.cloudfront.net'); // enable/disable cloud front
    Configure::write('cloudfront_host_http_url', 'http://d1ebp8oyqi4ffr.cloudfront.net'); // enable/disable cloud front
    Configure::write('distribution_id', 'E2Y2BO1ZAWQUFG');

    Configure::write('use_ffmpeg_path', '1'); //ffmpeg settings
    Configure::write('ffmpeg_binaries', '/root/bin/ffmpeg'); //ffmpeg settings
    Configure::write('ffprobe_binaries', '/root/bin/ffprobe'); //ffprobe settings

    Configure::write('use_ssl', '1'); //Use SSL 1 Yes , 0 No.
    Configure::write('send_error_email', '1'); //Send Error Email 1 Yes , 0 No.
    Configure::write('to','khurri.saleem@gmail.com');
    Configure::write('email_address', array(
        '1' => 'ks@3ssolutions.net',
        '2' => 'khurrams@sibme.com',
    ));
    Configure::write('sample_huddle', '3258,13547');
    Configure::write('use_local_file_store', true);
    Configure::write('use_job_queue', true);
    Configure::write('amazon_assets_url', 'https://d1c4hn49irt1g3.cloudfront.net/');
    Configure::write('user_id_allowed_impersonation', "621,585,6221,117,17335");
    Configure::write('cdn_ver', '03142018');
    Configure::write('reply_to_email','@sibme.com');

    //sendgrid templates for onboarding emails
    //welcome to sibme
    Configure::write('day1_all', '4c889195-8bc0-4166-b0f4-7c58a68ccd63');
    //Day 7 active account
    Configure::write('day7_active_user', '289a5594-760a-41e7-89cc-d90abc56540a');
    Configure::write('day7_active_superuser', '63586a0a-47ea-4c8f-be58-13b591861ca3');
    Configure::write('day7_active_accountowner', '0813b068-256a-42ca-88d4-cbfa100f06d7');
    //Day 7 inactive account
    Configure::write('day7_inactive_user', '44eb19ad-dd15-4dab-997b-406c2b1c6c60');
    Configure::write('day7_inactive_superuser', '39be86d0-933d-4b9f-9db2-59fd679b2582');
    Configure::write('day7_inactive_accountowner', '7c459f44-3954-4559-a5ea-507e554ad267');
    //Day 14 inactive account
    Configure::write('day14_inactive_user', '25581b0b-e2e4-4b26-af26-2c04cf895876');
    Configure::write('day14_inactive_superuser', '85b19be0-2833-4148-89e9-d648e2cade44');
    Configure::write('day14_inactive_accountowner', '5a042979-b51b-44be-8022-a6176c4541ef');
    //Day 18 active account
    Configure::write('day18_active_user', 'c5b1f899-86b8-49b9-92d2-b7b572532ebc');
    Configure::write('day18_active_superuser', 'c5b1f899-86b8-49b9-92d2-b7b572532ebc');
    Configure::write('day18_active_accountowner', 'c5b1f899-86b8-49b9-92d2-b7b572532ebc');
    //Day 25 All users
    Configure::write('day25_all_users', '12ab7830-ef9d-41ec-a8c7-38b807a8bfbf');
    //Day 26 All users
    Configure::write('day26_all_users', '6175f39b-167b-4945-b122-a3bd694dbf02');
    //Day 30 All users
    Configure::write('day30_all_users', '7d5b1da3-5a12-410e-9d73-aa3323d4f915');
    //Day 31 All users
    Configure::write('day31_all_users', 'e0ddd141-45b3-4a1e-8e3a-246001f3111d');
    //Day 40 All users
    Configure::write('day40_all_users', '078e3304-2aaf-4d99-b9b5-a11a4445de1a');
    //Day 45 All users
    Configure::write('day45_all_users', 'ac799adf-d1d9-4d6b-b6b4-af7acab1574e');
    //Day 46 All users
    Configure::write('day46_all_users', 'b2ea22ef-6e5a-4b9b-954a-9bce6bbd071e');

    //URL config for EDTPA
    Configure::write('edtpa_base_url', 'https://partners.nesinc.com/ws/seam/resource/v1/Import/');
    Configure::write('edtpa_provider_name', 'Sibme');
    /*Configure::write('edtpa_shared_secret', 'z3ol8fvb739u');
    Configure::write('edtpa_shared_key', '888080d8b6b443d4de7e4f35e91cca9543c57fe3b72483f9450a75fb9c67f47d');*/
    Configure::write('edtpa_shared_secret', 'z3ol8fvb739u');
    Configure::write('edtpa_shared_key', 'cfb1bb092c907fb8db23456b7c7a5411c4059796f582902e7cd34add57b9064a');

   Configure::write('analytical_report_test', '0');
   Configure::write('analytical_report_test_to', 'khurri.saleem@gmail.com');
   Configure::write('live_recording_url', 'https://59ec70ba7b9df.streamlock.net/livecf/');

   Configure::write('intercom_access_token', 'dG9rOjJmMGIwZTU5XzViMzhfNDExZF85MjAwXzE0ODI5Yjk3ZGU2NToxOjA=');
   Configure::write('intercom_app_id', 'uin0il18');
   Configure::write('intercom_app_id_link', 'https://widget.intercom.io/widget/uin0il18');

}

if (IS_QA) {

    Configure::write('db_host', "127.0.0.1");
    Configure::write('db_login', "root");
    Configure::write('db_password', "Tiger123");
    Configure::write('db_name', "sibme_new_2");
    Configure::write('MinifyAsset', true);
    Configure::write('sibme_base_url', 'http://qa.sibme.com/');
    Configure::write('amazon_base_url', 'http://sibme-development.s3.amazonaws.com/');
    Configure::write('bucket_name', 'sibme-development');
    Configure::write('bucket_name_cdn', 'sibme.com');
    Configure::write('access_key_id', 'AKIAJ4ZWDR5X5JKB7CZQ');
    Configure::write('secret_access_key', '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');
    Configure::write('filepicker_access_key', 'A3w6JbXR2RJmKr3kfnbZtz');
    Configure::write('encoder_provider', '2'); //1 is Zencoder, 2 is Amazon
    Configure::write('trial_duration', 30); // 30 days

    Configure::write('smtp_host', 'smtp.sendgrid.net');
    Configure::write('smtp_host_port', '25');
    Configure::write('smtp_user_name', 'davew@sibme.com');
    Configure::write('smtp_password', 'Sibme20!9!');
    Configure::write('smtp_from', 'info@sibme.com');
    Configure::write('smtp_from_name', 'Sibme');

    Configure::write('use_cloudfront', true); // enable/disable cloud front
    Configure::write('cloudfront_keypair', 'APKAJ64SUURDPDPPLA5Q'); // enable/disable cloud front
    Configure::write('cloudfront_host_url', 'http://dxyc8ojgw5jhg.cloudfront.net'); // enable/disable cloud front
    Configure::write('sibmecdn_host_url', 'http://d1c4hn49irt1g3.cloudfront.net'); // enable/disable cloud front
    Configure::write('distribution_id', 'E2Y2BO1ZAWQUFG');

    Configure::write('use_ffmpeg_path', '0'); //ffmpeg settings
    Configure::write('ffmpeg_binaries', ''); //ffmpeg settings
    Configure::write('ffprobe_binaries', ''); //ffprobe settings

    Configure::write('use_ssl', '0'); //Use SSL 1 Yes , 0 No.
    Configure::write('send_error_email', '0'); //Send Error Email 1 Yes , 0 No.
    Configure::write('to','khurri.saleem@gmail.com');
    Configure::write('email_address', array(
        '1' => 'khurri.saleem@gmail.com',
        '2' => 'khurri.saleem@gmail.com',
        '3' => 'khurri.saleem@gmail.com')
    );

    Configure::write('sample_huddle', 3258);
    Configure::write('use_local_file_store', true);
    Configure::write('use_job_queue', false);


}

if (IS_LOCAL) {

    Configure::write('db_host', "127.0.0.1");
    Configure::write('db_login', "root");
    Configure::write('db_password', "");
    Configure::write('db_name', "sibme");
    Configure::write('MinifyAsset', true);
    Configure::write('sibme_base_url', 'http://qalcl.sibme.com/');
    Configure::write('amazon_base_url', 'http://sibme-development.s3.amazonaws.com/');
    Configure::write('bucket_name', 'sibme-development');
    Configure::write('bucket_name_cdn', 'sibme.com');
    Configure::write('access_key_id', 'AKIAJ4ZWDR5X5JKB7CZQ');
    Configure::write('secret_access_key', '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');
    Configure::write('filepicker_access_key', 'A3w6JbXR2RJmKr3kfnbZtz');
    Configure::write('encoder_provider', '2'); //1 is Zencoder, 2 is Amazon
    Configure::write('trial_duration', 30); // 30 days

    Configure::write('use_cloudfront', false); // enable/disable cloud front
    Configure::write('cloudfront_keypair', 'APKAJ64SUURDPDPPLA5Q'); // enable/disable cloud front
    Configure::write('cloudfront_host_url', 'http://dxyc8ojgw5jhg.cloudfront.net'); // enable/disable cloud front
    Configure::write('sibmecdn_host_url', 'http://d1c4hn49irt1g3.cloudfront.net'); // enable/disable cloud front
    Configure::write('distribution_id', 'E2Y2BO1ZAWQUFG');

    Configure::write('use_ffmpeg_path', '0'); //ffmpeg settings
    Configure::write('ffmpeg_binaries', ''); //ffmpeg settings
    Configure::write('ffprobe_binaries', ''); //ffprobe settings

    Configure::write('use_ssl', '0'); //Use SSL 1 Yes , 0 No.
    Configure::write('send_error_email', '0'); //Send Error Email 1 Yes , 0 No.
    Configure::write('to','khurri.saleem@gmail.com');
    Configure::write('email_address', array(
        '1' => 'khurri.saleem@gmail.com',
        '2' => 'khurri.saleem@gmail.com',
        '3' => 'hamidbscs8@gmail.com')
    );

    Configure::write('sample_huddle', 3258);
    Configure::write('use_local_file_store', true);
    Configure::write('use_job_queue', false);


}



$path = dirname(__DIR__) . '/Config/local/core.php';
if (file_exists($path)) include_once $path;

require_once dirname(__DIR__) . '/Vendor/autoload.php';

