<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'Dashboard', 'action' => 'home'));
Router::connect('/users/login', array('controller' => 'Users', 'action' => 'login'));
Router::connect('/users/sign_up', array('controller' => 'Users', 'action' => 'signup'));
Router::connect('/home/forgot_username', array('controller' => 'Api', 'action' => 'forgot_username'));
Router::connect('/Api/home/retreive_username.json', array('controller' => 'Api', 'action' => 'forgot_username'));

Router::connect('/external/*', array('controller' => 'External', 'action' => 'anyvideos'), array('account_id' => '[0-9]+', 'account_folder_id' => '[0-9]+'));
Router::connect('/Folder/:folder_id', array('controller' => 'Folder', 'action' => 'index'), array('folder_id' => '[0-9]+'));
Router::connect('/view/*', array('controller' => 'Huddles', 'action' => 'view'));


/**
 * ...and connect the rest of 'Pages' controller's urls.
 */
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
