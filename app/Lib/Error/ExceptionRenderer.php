<?php

/**
 * Exception Renderer
 *
 * Provides Exception rendering features. Which allow exceptions to be rendered
 * as HTML pages.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Error
 * @since         CakePHP(tm) v 2.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Sanitize', 'Utility');
App::uses('Router', 'Routing');
App::uses('CakeResponse', 'Network');
App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
App::uses('Session', 'Component');
App::uses('CakeEmail', 'Network/Email');
App::uses('AuditErrors', 'Model');

/**
 * Exception Renderer.
 *
 * Captures and handles all unhandled exceptions. Displays helpful framework errors when debug > 1.
 * When debug < 1 a CakeException will render 404 or 500 errors. If an uncaught exception is thrown
 * and it is a type that ExceptionHandler does not know about it will be treated as a 500 error.
 *
 * ### Implementing application specific exception rendering
 *
 * You can implement application specific exception handling in one of a few ways:
 *
 * - Create a AppController::appError();
 * - Create a subclass of ExceptionRenderer and configure it to be the `Exception.renderer`
 *
 * #### Using AppController::appError();
 *
 * This controller method is called instead of the default exception handling. It receives the
 * thrown exception as its only argument. You should implement your error handling in that method.
 *
 * #### Using a subclass of ExceptionRenderer
 *
 * Using a subclass of ExceptionRenderer gives you full control over how Exceptions are rendered, you
 * can configure your class in your core.php, with `Configure::write('Exception.renderer', 'MyClass');`
 * You should place any custom exception renderers in `app/Lib/Error`.
 *
 * @package       Cake.Error
 */
class ExceptionRenderer {

    /**
     * Controller instance.
     *
     * @var Controller
     */
    public $controller = null;

    /**
     * template to render for CakeException
     *
     * @var string
     */
    public $template = '';

    /**
     * The method corresponding to the Exception this object is for.
     *
     * @var string
     */
    public $method = '';

    /**
     * The exception being handled.
     *
     * @var Exception
     */
    public $error = null;

    /**
     * Creates the controller to perform rendering on the error response.
     * If the error is a CakeException it will be converted to either a 400 or a 500
     * code error depending on the code used to construct the error.
     *
     * @param Exception $exception Exception
     * @return mixed Return void or value returned by controller's `appError()` function
     */
    var $components = array('Session');

    public function __construct(Exception $exception) {

        $this->controller = $this->_getController($exception);

        if (method_exists($this->controller, 'apperror')) {
            return $this->controller->appError($exception);
        }
        $method = $template = Inflector::variable(str_replace('Exception', '', get_class($exception)));
        $code = $exception->getCode();

        $methodExists = method_exists($this, $method);

        if ($exception instanceof CakeException && !$methodExists) {
            $method = '_cakeError';
            if (empty($template) || $template === 'internalError') {
                $template = 'error500';
            }
        } elseif ($exception instanceof PDOException) {
            $method = 'pdoError';
            $template = 'pdo_error';
            $code = 500;
        } elseif (!$methodExists) {
            $method = 'error500';
            if ($code >= 400 && $code < 500) {
                $method = 'error400';
            }
        }

        $isNotDebug = !Configure::read('debug');
        if ($isNotDebug && $method === '_cakeError') {
            $method = 'error400';
        }
        if ($isNotDebug && $code == 500) {
            $method = 'error500';
        }
        $this->template = $template;
        $this->method = $method;
        $this->error = $exception;

        
       $this->save_errors();
        
    }

    public function save_errors() {

        $AuditErrors = new AuditErrors();
        $error = $this->error;
        $error_data = '<div style="font-weight:bold;"> Error: ' . $error->getMessage() . "</div>";
        $error_data .= '<h3>Stack Trace</h3>';
        $error_data .= '<ul class="cake-stack-trace">';
        foreach ($error->getTrace() as $i => $stack):
            $error_data .= '<li>';
            $excerpt = $arguments = '';
            $params = array();
            if (isset($stack['file']) && isset($stack['line'])):
                $error_data .= sprintf('<a href="#" onclick="traceToggle(event, \'file-excerpt-%s\')">%s line %s</a>', $i, Debugger::trimPath($stack['file']), $stack['line']);
                $error_data .= sprintf('<div id="file-excerpt-%s" class="cake-code-dump" style="display:none;"><pre>', $i);
                $error_data .= implode("\n", Debugger::excerpt($stack['file'], $stack['line'] - 1, 2));
                $error_data .= '</pre></div> ';
            else:
                $error_data .= '<a href="#">[internal function]</a>';
            endif;
            $error_data .=' &rarr; ';
            if ($stack['function']):
                $args = array();
                if (!empty($stack['args'])):
                    foreach ((array) $stack['args'] as $arg):
                        $args[] = Debugger::getType($arg);
                        $params[] = Debugger::exportVar($arg, 2);
                    endforeach;
                endif;
                $called = isset($stack['class']) ? $stack['class'] . $stack['type'] . $stack['function'] : $stack['function'];
                $error_data .= sprintf('<a href="#" onclick="traceToggle(event, \'trace-args-%s\')">%s(%s)</a> ', $i, $called, h(implode(', ', $args)));
                $arguments = sprintf('<div id="trace-args-%s" class="cake-code-dump" style="display: none;"><pre>', $i);
                $arguments .= h(implode("\n", $params));
                $arguments .= '</pre></div>';
            endif;
            $error_data .= $excerpt;
            $error_data .= $arguments;
            $error_data .= "</li>";
        endforeach;
        $error_data .= "</ul>";
        $error_data .= "<script type='text/javascript'>
        function traceToggle(event, id) {
        var el = document.getElementById(id);
        el.style.display = (el.style.display === 'block') ? 'none' : 'block';
        event.preventDefault();
        return false;
    }
    </script>";
        $form_fields = '';
        if (isset($_POST['data']) && $_POST['data'] != '') {
            $form_fields = var_export($_POST['data'], true);
        }

        $data = array(
            'form_fields' => ($form_fields != '' ? $form_fields : ''),
            'created_date' => date("Y-m-d H:i:s"),
            'error_detail' => $error_data,
            'path' => $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
        );
        $error_id = $AuditErrors->set_error_data($data);
        if ($error_id) {
            $data['error_no'] = $error_id;

            if ($data['path'] == 'app.sibme.com/index.php/cron/uploadNewFiles') return;

            $send_error_email = Configure::read('send_error_email');

            if ($send_error_email == '1') {
                $this->send_error($data);
            }
        }
    }

    public function send_error($data) {
        $user = $this->controller->Session->read('user_current_account');
        $message = '<div style="padding-bottom:10px;"><strong>Error Number : </strong>' . $data['error_no'] . '</div>';
        if (isset($user['User']['id']) && $user['User']['id'] != '') {
            $message .='<div style="padding-bottom:10px;"><strong>Created By : </strong>' . $user['User']['first_name'] . " " . $user['User']['last_name'] . '</div>';
        }
        $message .='<div style="padding-bottom:10px;"><strong>Path: </strong><a href="' . $data['path'] . '"/>' . $data['path'] . '</a></div>';
        $message .='<div style="padding-bottom:10px;"><strong>View this error on site please <a href="https://' . $_SERVER['HTTP_HOST'] . '/app/Errors/view_exception/' . $data['error_no'] . '"><span style="color:blue;">Click here.</span></a></strong></div>';
        $message .= $data['error_detail'];
        if ($data['form_fields'] != '') {
            $message .='<div><strong>Form Fields </strong><pre>' . $data['form_fields'] . "</pre>";
        }

        $this->controller->Email->delivery = 'smtp';
        $this->controller->Email->from = 'do-not-reply@sibme.com';
        $this->controller->Email->to = Configure::read('email_address');
        $this->controller->Email->subject = "Sibme Production Website Exception";
        $this->controller->Email->template = 'default';
        $this->controller->Email->sendAs = 'html';
        $this->controller->Email->send($message);
        $this->controller->Session->write('error_id', $data['error_no']);
        $this->controller->redirect('/Errors/show');
    }

    /**
     * Get the controller instance to handle the exception.
     * Override this method in subclasses to customize the controller used.
     * This method returns the built in `CakeErrorController` normally, or if an error is repeated
     * a bare controller will be used.
     *
     * @param Exception $exception The exception to get a controller for.
     * @return Controller
     */
    protected function _getController($exception) {
        App::uses('AppController', 'Controller');
        App::uses('CakeErrorController', 'Controller');
        if (!$request = Router::getRequest(true)) {
            $request = new CakeRequest();
        }
        $response = new CakeResponse();

        if (method_exists($exception, 'responseHeader')) {
            $response->header($exception->responseHeader());
        }

        if (class_exists('AppController')) {
            try {
                $controller = new CakeErrorController($request, $response);
                $controller->startupProcess();
            } catch (Exception $e) {
                if (!empty($controller) && $controller->Components->enabled('RequestHandler')) {
                    $controller->RequestHandler->startup($controller);
                }
            }
        }
        if (empty($controller)) {
            $controller = new Controller($request, $response);
            $controller->viewPath = 'Errors';
        }
        return $controller;
    }

    /**
     * Renders the response for the exception.
     *
     * @return void
     */
    public function render() {


        if ($this->method) {
            call_user_func_array(array($this, $this->method), array($this->error));
        }
    }

    /**
     * Generic handler for the internal framework errors CakePHP can generate.
     *
     * @param CakeException $error
     * @return void
     */
    protected function _cakeError(CakeException $error) {
        $url = $this->controller->request->here();
        $code = ($error->getCode() >= 400 && $error->getCode() < 506) ? $error->getCode() : 500;
        $this->controller->response->statusCode($code);
        $this->controller->set(array(
            'code' => $code,
            'url' => h($url),
            'name' => h($error->getMessage()),
            'error' => $error,
            '_serialize' => array('code', 'url', 'name')
        ));
        $this->controller->set($error->getAttributes());
        $this->_outputMessage($this->template);
    }

    /**
     * Convenience method to display a 400 series page.
     *
     * @param Exception $error
     * @return void
     */
    public function error400($error) {
        $message = $error->getMessage();
        if (!Configure::read('debug') && $error instanceof CakeException) {
            $message = __d('cake', 'Not Found');
        }
        $url = $this->controller->request->here();
        $this->controller->response->statusCode($error->getCode());
        $this->controller->set(array(
            'name' => h($message),
            'url' => h($url),
            'error' => $error,
            '_serialize' => array('name', 'url')
        ));
        $this->_outputMessage('error400');
    }

    /**
     * Convenience method to display a 500 page.
     *
     * @param Exception $error
     * @return void
     */
    public function error500($error) {
        $message = $error->getMessage();
        if (!Configure::read('debug')) {
            $message = __d('cake', 'An Internal Error Has Occurred.');
        }
        $url = $this->controller->request->here();
        $code = ($error->getCode() > 500 && $error->getCode() < 506) ? $error->getCode() : 500;
        $this->controller->response->statusCode($code);
        $this->controller->set(array(
            'name' => h($message),
            'message' => h($url),
            'error' => $error,
            '_serialize' => array('name', 'message')
        ));
        $this->_outputMessage('error500');
    }

    /**
     * Convenience method to display a PDOException.
     *
     * @param PDOException $error
     * @return void
     */
    public function pdoError(PDOException $error) {
        $url = $this->controller->request->here();
        $code = 500;
        $this->controller->response->statusCode($code);
        $this->controller->set(array(
            'code' => $code,
            'url' => h($url),
            'name' => h($error->getMessage()),
            'error' => $error,
            '_serialize' => array('code', 'url', 'name', 'error')
        ));
        $this->_outputMessage($this->template);
    }

    /**
     * Generate the response using the controller object.
     *
     * @param string $template The template to render.
     * @return void
     */
    protected function _outputMessage($template) {
        try {
            $this->controller->render($template);
            $this->controller->afterFilter();
            $this->controller->response->send();
        } catch (MissingViewException $e) {
            $attributes = $e->getAttributes();
            if (isset($attributes['file']) && strpos($attributes['file'], 'error500') !== false) {
                $this->_outputMessageSafe('error500');
            } else {
                $this->_outputMessage('error500');
            }
        } catch (Exception $e) {
            $this->_outputMessageSafe('error500');
        }
    }

    /**
     * A safer way to render error messages, replaces all helpers, with basics
     * and doesn't call component methods.
     *
     * @param string $template The template to render
     * @return void
     */
    protected function _outputMessageSafe($template) {
        $this->controller->layoutPath = null;
        $this->controller->subDir = null;
        $this->controller->viewPath = 'Errors/';
        $this->controller->layout = 'error';
        $this->controller->helpers = array('Form', 'Html', 'Session');

        $view = new View($this->controller);
        $this->controller->response->body($view->render($template, 'error'));
        $this->controller->response->type('html');
        $this->controller->response->send();
    }

}
