<?php

require_once 'Braintree.php';
require_once "Braintree/CreditCardNumbers/CardTypeIndicators.php";
require_once "Braintree/CreditCardDefaults.php";
require_once "Braintree/CreditCard.php";

APP::import('Model', 'Transaction');



class Subscription {
    var $envoirment;
    var $gateway;
    

    public function __construct() {
        $this->envoirment =  Configure::read('subscription_config');
        $this->gateway = $this->integrationMerchantConfig();
        date_default_timezone_set("UTC");
    }

    function integrationMerchantConfig() {
        if ($this->envoirment == 'sandbox') {
            $config = new Braintree\Configuration([
                'environment' => 'sandbox',
                'merchantId' => '2zhny5z63p96qnpv',
                'publicKey' => '7m8srqdvts26gc4n',
                'privateKey' => 'a5cc6489fd6230be94f3fd1787ccbbe0'
            ]);
            $conf = new Braintree\Gateway($config);
            return $conf;
        }else{
            $config = new Braintree\Configuration([
                'environment' => 'production',
                'merchantId' => 'tk7ph5dyz33hccq8',
                'publicKey' => '44g8y8krfcrdyygn',
                'privateKey' => '9d9c0a674920bc6e4462280cd17d55a3'
            ]);
            $conf = new Braintree\Gateway($config);
            return $conf;
        }
        
    }

     function create_subscription($planId, $customerData, $price, $monthly_bool, $subscription_id) 
     {
        /*
         * Step 1 . First Create Cutomer and get Customer Id.
         * Step 2 . Create Credit Card and return Token.
         * Step 3 . Create Subscriptions return subscription id.
         */
       //  Braintree_Subscription::cancel($subscription_id);
         $customer_data = array();
        //$result = Braintree_Customer::create($customerData);
        $result = $this->gateway->customer()->create($customerData);
        if ($result->success) {
            $result1 = $this->gateway->subscription()->create([
                        'paymentMethodToken' => $result->customer->creditCards[0]->token,
                        'planId' => $planId,
                        'trialDuration' => 60,
                        'trialDurationUnit' => 'month',
                        'price' => $price
            ]);
            
            $subscription_type = 0 ;
            
            if($monthly_bool == 1)
            {
                $subscription_type = 0;
                
                
                  $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 month", $time));
                
            }
            else
            {
                
                $subscription_type = 1;
                $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 year", $time));
              
              
                
            }
            
            
            
            $customer_data = array(
                'success' => TRUE,
                'braintree_customer_id' => $result->customer->id,
                'braintree_subscription_id' => $result1->subscription->id,
                'braintree_plan_id' => $result1->subscription->planId,
                'subscription_type' => $subscription_type,
                'subscription_status' => $result1->subscription->status,
                'payment_status' =>  (isset($result1->subscription->transactions[0]->status))? $result1->subscription->transactions[0]->status : '',
                'start_date' => $start_date ,
                'end_date' => $end_date,
                'db_check' => 'new'
            );
            return $customer_data;
        } else {
            return $result;
        }
    }
    
    function create_subscription_with_existing_customer($planId, $customerData,$price,$monthly_bool,$subscription_id,$customer_id) {
        /*
         * Step 1 . First Create Cutomer and get Customer Id.
         * Step 2 . Create Credit Card and return Token.
         * Step 3 . Create Subscriptions return subscription id.
         */
       //  Braintree_Subscription::cancel($subscription_id);
         $customer_data = array();
        $result = $this->gateway->customer()->update($customer_id,$customerData);
        if ($result->success) {
            $result1 = $this->gateway->subscription()->create(array(
                        'paymentMethodToken' => $result->customer->creditCards[0]->token,
                        'planId' => $planId,
                        'trialDuration' => 60,
                        'trialDurationUnit' => 'month',
                        'price' => $price
            ));
            
            $subscription_type = 0 ;
            
            if($monthly_bool == 1)
            {
                $subscription_type = 0;
                
                
                  $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 month", $time));
                
            }
            else
            {
                
                $subscription_type = 1;
                $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 year", $time));
              
              
                
            }
            
            
            
            $customer_data = array(
                'success' => TRUE,
                'braintree_customer_id' => $result->customer->id,
                'braintree_subscription_id' => $result1->subscription->id,
                'braintree_plan_id' => $result1->subscription->planId,
                'subscription_type' => $subscription_type,
                'subscription_status' => $result1->subscription->status,
                'payment_status' =>  (isset($result1->subscription->transactions[0]->status))? $result1->subscription->transactions[0]->status : '',
                'start_date' => $start_date ,
                'end_date' => $end_date,
                'db_check' => 'new'
            );
            return $customer_data;
        } else {
            return $result;
        }
    }
    
    function update_customer_subscription($planId, $customerData,$customer_id,$subscription_id,$price,$monthly_bool,$yearly_bool,$quantity,$current_planId ,$current_quantity,$current_price,$start_date,$end_date) {
        /*
         * Step 1 . First Create Cutomer and get Customer Id.
         * Step 2 . Create Credit Card and return Token.
         * Step 3 . Create Subscriptions return subscription id.
         */
        
        $db_check = '';
         $subscription = new Subscription();
        $subscriptions = $subscription->subscriptions_detail($subscription_id);
       
        $current_plan_price = ($subscriptions->nextBillAmount);
        
        if(!empty($subscriptions)){

        
        $nextBillingDate = $subscriptions->nextBillingDate;
        $nextBillingDate = (array)$nextBillingDate;
        
        $billingPeriodStartDate = $subscriptions->billingPeriodStartDate;
        if(!empty($billingPeriodStartDate))
        {
        $billingPeriodStartDate = (array)$billingPeriodStartDate;
        }
        else{
            $billingPeriodStartDate = $nextBillingDate;
        }
        
         $billingPeriodEndDate = $subscriptions->billingPeriodEndDate;
         if(!empty($billingPeriodEndDate)){
        $billingPeriodEndDate = (array)$billingPeriodEndDate;
        
         }
         else{
             $time = strtotime($nextBillingDate['date']);
             if($monthly_bool)
             {
             $billingPeriodEndDate['date'] = date("Y-m-d", strtotime("+1 month", $time));
             }
             else{
                 $billingPeriodEndDate['date'] = date("Y-m-d", strtotime("+1 year", $time));
             }
         }   
            
            
            
            
        
        $date1 = new DateTime($end_date);
        $date2 = new DateTime($start_date);

        
       $divider = $date2->diff($date1)->format("%a");
        
        
        
           $date1 = new DateTime(date('Y-m-d'));
        //    $date1 = new DateTime('2017-01-20');
            if($monthly_bool)
            {
        
           $date2 = new DateTime($end_date);
            }
            else
            {
            $date2 = new DateTime($end_date);    
            }

           $diff = $date2->diff($date1)->format("%a");
          
        
        
        }
            if($divider == 0)
            {
                if($monthly_bool)
                {
                    $divider = 30;
                }
                else{
                    $divider = 365; 
                }
            }
            $update_price = $price;
            
            if(($yearly_bool == 1 && $monthly_bool == 0 ) || ($yearly_bool == 0 && $monthly_bool == 1 ))
        {
                
         if($planId == $current_planId && $quantity > $current_quantity)
            {
                $price = $price/$quantity;
                $price = ($price)*($quantity - $current_quantity);
            }        
        
                
        
      //  Braintree_Subscription::cancel($subscription_id);
        $customer_data = array();
        //$result = Braintree_Customer::update($customer_id,$customerData);
        $result = $this->gateway->customer()->update($customer_id,$customerData);
        if ($result->success) {
            
            if($quantity > $current_quantity || ( $planId != $current_planId && $planId > $current_planId  ) )
            {
               if($planId != $current_planId && $planId > $current_planId)
                { 
                   $transaction_result = $this->gateway->transaction()->sale([
                  'amount' => (round(($price/$divider)*$diff,2) - round(($current_price/$divider)*$diff,2)),
                  'customerId' => $customer_id,
                  'paymentMethodToken' => $result->customer->creditCards[0]->token,
                  'options' => [
                    'submitForSettlement' => True
                  ]
             ]);
                   
               }  
             else{
         //   echo $price . ' ' . $divider . ' ' . $diff . ' ' . round(($price/$divider)*$diff,2);die;
         $transaction_result = $this->gateway->transaction()->sale([
                  'amount' => round(($price/$divider)*$diff,2),
                  'customerId' => $customer_id,
                  'paymentMethodToken' => $result->customer->creditCards[0]->token,
                  'options' => [
                    'submitForSettlement' => True
                  ]
             ]);
         
             }

                $db = new Transaction();
                $transaction = array(
                        'transaction_id' => $transaction_result->transaction->id,
                        'transaction_type' => $transaction_result->transaction->type,
                        'status' => $transaction_result->transaction->status,
                        'student_payment_id'=>$planId,
                        'created_date' => date('Y-m-d H:i:s')
                        );
                
                $db->save($transaction);
                
            }
            
            
            
            
         if(($planId < $current_planId) || ( $planId == $current_planId && $quantity < $current_quantity))  
         {
            $this->gateway->subscription()->cancel($subscription_id);
              $result1 = $this->gateway->subscription()->create(array(
                        'paymentMethodToken' => $result->customer->creditCards[0]->token,
                        'planId' => $planId,
                        'trialDuration' => 60,
                        'firstBillingDate' => $end_date,
                        'trialDurationUnit' => 'month',
                        'price' => $update_price,
                    ));
              $db_check = 'new';
             
         }
            
        else{
        $result1 = $this->gateway->subscription()->update($subscription_id,array(
                        'paymentMethodToken' => $result->customer->creditCards[0]->token,
                        'planId' => $planId,
                    //    'trialDuration' => 60,
                    //    'trialDurationUnit' => 'month',
                        'price' => $update_price
            ));
        
            $db_check = 'update';
        
        }
            
            
            $subscription_type = 0 ;
            
            if($yearly_bool == 1)
            {
                $subscription_type = 1;
                
                $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 year", $time));
                
            }
            else
            {
                
                $subscription_type = 0;
                
                $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 month", $time));
              
                
            }
            
            
            
            
             if(($planId < $current_planId) || ( $planId == $current_planId && $quantity < $current_quantity))
            {
                if($monthly_bool == 1) {
                $customer_data = array(
                'success' => TRUE,
                'braintree_customer_id' => $result->customer->id,
                'braintree_subscription_id' => $result1->subscription->id,
                'braintree_plan_id' => $result1->subscription->planId,
                'subscription_type' => $subscription_type,
                'subscription_status' => $result1->subscription->status,
                'payment_status' =>  (isset($result1->subscription->transactions[0]->status))? $result1->subscription->transactions[0]->status : '',
                'start_date' => $end_date ,
                'end_date' => date("Y-m-d", strtotime("+1 month", strtotime($end_date))) ,
                'db_check' => $db_check
            );
                }
                else{
                    
                     $customer_data = array(
                'success' => TRUE,
                'braintree_customer_id' => $result->customer->id,
                'braintree_subscription_id' => $result1->subscription->id,
                'braintree_plan_id' => $result1->subscription->planId,
                'subscription_type' => $subscription_type,
                'subscription_status' => $result1->subscription->status,
                'payment_status' =>  (isset($result1->subscription->transactions[0]->status))? $result1->subscription->transactions[0]->status : '',
                'start_date' => $end_date ,
                'end_date' => date("Y-m-d", strtotime("+1 year", strtotime($end_date))) ,
                'db_check' => $db_check
            );
                    
                }
                
            }
            else{
            
            $customer_data = array(
                'success' => TRUE,
                'braintree_customer_id' => $result->customer->id,
                'braintree_subscription_id' => $result1->subscription->id,
                'braintree_plan_id' => $result1->subscription->planId,
                'subscription_type' => $subscription_type,
                'subscription_status' => $result1->subscription->status,
                'payment_status' =>  (isset($result1->subscription->transactions[0]->status))? $result1->subscription->transactions[0]->status : '',
                'start_date' => $start_date ,
                'end_date' => $end_date,
                'db_check' => $db_check
            );
            }
        
        
           
            return $customer_data;
            
            
            
            
            
        } else {
            return $result;
        }
        
        }
        
        else{
        $this->gateway->subscription()->cancel($subscription_id);
        $customer_data = array();
        $result = $this->gateway->customer()->update($customer_id,$customerData);
        if ($result->success) {
            
            if(!$yearly_bool)
            {
               $discount_price = round(($current_plan_price/$divider)*$diff,2);
            }
            
            if(isset($discount_price))
            {
                 $result1 = $this->gateway->subscription()->create(array(
                        'paymentMethodToken' => $result->customer->creditCards[0]->token,
                        'planId' => $planId,
                        'trialDuration' => 60,
                        'trialDurationUnit' => 'month',
                        'price' => $price ,
                        'discounts' => [
        'add' => [
            [
                'inheritedFromId' => '745g',
                'amount' => $discount_price,
                'numberOfBillingCycles' => 1
            ]
        ],
        ]
                     //   'firstBillingDate' => $nextBillingDate['date']
               
            ));
                
            }
            
            else{
            
            $result1 = $this->gateway->subscription()->create(array(
                        'paymentMethodToken' => $result->customer->creditCards[0]->token,
                        'planId' => $planId,
                        'trialDuration' => 60,
                        'trialDurationUnit' => 'month',
                        'price' => $price 
            ));
            
            }
            
            
            $subscription_type = 1 ;
            
            if($monthly_bool == 1)
            {
                $subscription_type = 0;
                
                $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 month", $time));
                
            }
            else
            {
                
                $subscription_type = 1;
                
                $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 year", $time));
                
            }
            
            
            $customer_data = array(
                'success' => TRUE,
                'braintree_customer_id' => $result->customer->id,
                'braintree_subscription_id' => $result1->subscription->id,
                'braintree_plan_id' => $result1->subscription->planId,
                'subscription_type' => $subscription_type,
                'subscription_status' => $result1->subscription->status,
                'payment_status' =>  (isset($result1->subscription->transactions[0]->status))? $result1->subscription->transactions[0]->status : '',
                'start_date' => $start_date ,
                'end_date' => $end_date,
                'db_check' => 'new'
            );
            return $customer_data;
        } else {
            return $result;
        }
            
        }
        
    }
    
    function update_customer($customerData,$customer_id,$subscription_id) {
        /*
         * Step 1 . First Create Cutomer and get Customer Id.
         * Step 2 . Create Credit Card and return Token.
         * Step 3 . Create Subscriptions return subscription id.
         */
      //  Braintree_Subscription::cancel($subscription_id);
        $customer_data = array();
        $result = $this->gateway->customer()->update($customer_id,$customerData);
        if ($result->success) {
           
            $customer_data = array(
                'success' => TRUE,
                'braintree_customer_id' => $result->customer->id,
            );
            return $customer_data;
        } else {
            return $result;
        }
    }

    function edit_plane($subscripiton_id, $data,$planId,$monthly_bool,$yearly_bool,$quantity,$current_planId ,$current_quantity,$current_price,$start_date,$end_date) {
        $subscription = new Subscription();
        $subscriptions = $subscription->subscriptions_detail($subscripiton_id);
        $current_plan_price = ($subscriptions->nextBillAmount);
        $db_check = '';
        
        if(!empty($subscriptions)){
        
        $nextBillingDate = $subscriptions->nextBillingDate;
        $nextBillingDate = (array)$nextBillingDate;
        
        $billingPeriodStartDate = $subscriptions->billingPeriodStartDate;
        if(!empty($billingPeriodStartDate))
        {
        $billingPeriodStartDate = (array)$billingPeriodStartDate;
        }
        else{
            $billingPeriodStartDate = $nextBillingDate;
        }
        
         $billingPeriodEndDate = $subscriptions->billingPeriodEndDate;
         if(!empty($billingPeriodEndDate)){
        $billingPeriodEndDate = (array)$billingPeriodEndDate;
        
         }
         else{
             $time = strtotime($nextBillingDate['date']);
             if($monthly_bool)
             {
             $billingPeriodEndDate['date'] = date("Y-m-d", strtotime("+1 month", $time));
             }
             else{
                 $billingPeriodEndDate['date'] = date("Y-m-d", strtotime("+1 year", $time));
             }
         }   
            
            
            
            
        
        $date1 = new DateTime($end_date);
        $date2 = new DateTime($start_date);

        
       $divider = $date2->diff($date1)->format("%a");
        
        
        
            $date1 = new DateTime(date('Y-m-d'));
         //   $date1 = new DateTime('2017-01-25');
            
            if($monthly_bool)
            {
        
           $date2 = new DateTime($end_date);
            }
            else
            {
            $date2 = new DateTime($end_date);    
            }

           $diff = $date2->diff($date1)->format("%a");
          
        
        
        }
            if($divider == 0 && $divider != 30 && $divider != 31 )
            {
                if($monthly_bool)
                {
                    $divider = 30;
                }
                else{
                    $divider = 365; 
                }
            }
           
            $update_price = $data['price'];
                     
        if(($yearly_bool == 1 && $monthly_bool == 0 ) || ($yearly_bool == 0 && $monthly_bool == 1 ))
        {
            
            if($planId == $current_planId && $quantity > $current_quantity)
            {
                
                $data['price'] = $data['price']/$quantity;
                $data['price'] = ($data['price'])*($quantity - $current_quantity);
            }
            
            if($quantity > $current_quantity || ( $planId != $current_planId && $planId > $current_planId  ) )
            {
                
                if($planId != $current_planId && $planId > $current_planId)
                {   
                    $transaction_result =  $this->gateway->transaction()->sale([
                  'amount' => (round(($data['price']/$divider)*$diff,2) - round(($current_price/$divider)*$diff,2) ) ,
                  'customerId' => $data['customer_id'],
                  'paymentMethodToken' => $data['paymentMethodToken'],
                  'options' => [
                    'submitForSettlement' => True
                  ]
                ]);
                    
                }
                else{
           // echo $data['price'] . ' ' . $diff . ' ' . $divider.' '.round(($data['price']/$divider)*$diff,2);die;
           
          $transaction_result =  $this->gateway->transaction()->sale([
                  'amount' => round(($data['price']/$divider)*$diff,2),
                  'customerId' => $data['customer_id'],
                  'paymentMethodToken' => $data['paymentMethodToken'],
                  'options' => [
                    'submitForSettlement' => True
                  ]
                ]);
                }
                $array = $transaction_result->transaction->_attributes;
                $db = new Transaction();
                $transaction = array(
                        'transaction_id' => $transaction_result->transaction->id,
                        'transaction_type' => $transaction_result->transaction->type,
                        'status' => $transaction_result->transaction->status,
                        'student_payment_id'=>$planId,
                        'created_date' => date('Y-m-d H:i:s')
                        );
                
                $db->save($transaction);
            
            }
         if(($planId < $current_planId) || ( $planId == $current_planId && $quantity < $current_quantity))  
         {
            $this->gateway->subscription()->cancel($subscripiton_id);
              $result1 = $this->gateway->subscription()->create(array(
                        'paymentMethodToken' => $data['paymentMethodToken'],
                        'planId' => $planId,
                        'trialDuration' => 60,
                        'firstBillingDate' => $end_date,
                        'trialDurationUnit' => 'month',
                        'price' => $update_price,
                    ));
              $db_check = 'new';
             
         }
            
        else{
        $result1 = $this->gateway->subscription()->update($subscripiton_id,array(
                        'paymentMethodToken' => $data['paymentMethodToken'],
                        'planId' => $planId,
                    //    'trialDuration' => 60,
                    //    'trialDurationUnit' => 'month',
                        'price' => $update_price
            ));
        
            $db_check = 'update';
        
        }
           
            
            $subscription_type = 0 ;
            
            if($yearly_bool == 1)
            {
                $subscription_type = 1;
                
                $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 year", $time));
                
            }
            else
            {
                
                $subscription_type = 0;
                
                $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 month", $time));
              
                
            }
            
            //if($quantity < $current_quantity && $planId < $current_planId || $planId == $current_planId  )
            if(($planId < $current_planId) || ( $planId == $current_planId && $quantity < $current_quantity))
            {
                if($monthly_bool == 1) {
                $customer_data = array(
                'success' => TRUE,
                'braintree_customer_id' => $data['customer_id'],
                'braintree_subscription_id' => $result1->subscription->id,
                'braintree_plan_id' => $result1->subscription->planId,
                'subscription_type' => $subscription_type,
                'subscription_status' => $result1->subscription->status,
                'payment_status' =>  (isset($result1->subscription->transactions[0]->status))? $result1->subscription->transactions[0]->status : '',  
                'start_date' => $end_date ,
                'end_date' => date("Y-m-d", strtotime("+1 month", strtotime($end_date))) ,
                'db_check' => $db_check
            );
                }
                else{
                    
                     $customer_data = array(
                'success' => TRUE,
                'braintree_customer_id' => $data['customer_id'],
                'braintree_subscription_id' => $result1->subscription->id,
                'braintree_plan_id' => $result1->subscription->planId,
                'subscription_type' => $subscription_type,
                'subscription_status' => $result1->subscription->status, 
                'payment_status' =>  (isset($result1->subscription->transactions[0]->status))? $result1->subscription->transactions[0]->status : '',        
                'start_date' => $end_date ,
                'end_date' => date("Y-m-d", strtotime("+1 year", strtotime($end_date))) ,
                'db_check' => $db_check
            );
                    
                }
                
            }
            else{
            
            $customer_data = array(
                'success' => TRUE,
                'braintree_customer_id' => $data['customer_id'],
                'braintree_subscription_id' => $result1->subscription->id,
                'braintree_plan_id' => $result1->subscription->planId,
                'subscription_type' => $subscription_type,
                'subscription_status' => $result1->subscription->status,
                'payment_status' =>  (isset($result1->subscription->transactions[0]->status))? $result1->subscription->transactions[0]->status : '',
                'start_date' => $start_date ,
                'end_date' => $end_date,
                'db_check' => $db_check
            );
            }
        
        
           
            return $customer_data;
            
        }
        
        else{
            
            
            
            $this->gateway->subscription()->cancel($subscripiton_id);
            
            if(!$yearly_bool)
            {
               $discount_price = round(($current_plan_price/$divider)*$diff,2);
            }
            
            if(isset($discount_price))
            {
            
            $result1 = $this->gateway->subscription()->create(array(
                        'paymentMethodToken' => $data['paymentMethodToken'],
                        'planId' => $planId,
                        'trialDuration' => 60,
                        'trialDurationUnit' => 'month',
                        'price' => $data['price'],
                        'discounts' => [
        'add' => [
            [
                'inheritedFromId' => '745g',
                'amount' => $discount_price,
                'numberOfBillingCycles' => 1
            ]
        ],
        ]
                     //   'firstBillingDate' => $nextBillingDate['date']
               
            ));
            
            }
            
            else{
                $result1 = $this->gateway->subscription()->create(array(
                        'paymentMethodToken' => $data['paymentMethodToken'],
                        'planId' => $planId,
                        'trialDuration' => 60,
                        'trialDurationUnit' => 'month',
                        'price' => $data['price'],
                    ));
                
            }
//            echo $planId;
//            echo $discount_price;
//            echo $result1->message;die;
            
             $subscription_type = 1 ;
            
            if($monthly_bool == 1)
            {
                $subscription_type = 0;
                
                $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 month", $time));
                
            }
            else
            {
                
                $subscription_type = 1;
                
                $start_date = date('Y-m-d');
                $time = strtotime($start_date);
                $end_date =  date("Y-m-d", strtotime("+1 year", $time));
                
            }
            
            
            
            
            
            
            $customer_data = array(
                'success' => TRUE,
                'braintree_customer_id' => $data['customer_id'],
                'braintree_subscription_id' => $result1->subscription->id,
                'braintree_plan_id' => $result1->subscription->planId,
                'subscription_type' => $subscription_type,
                'subscription_status' => $result1->subscription->status,
                'payment_status' =>  (isset($result1->subscription->transactions[0]->status))? $result1->subscription->transactions[0]->status : '',
                'start_date' => $start_date ,
                'end_date' => $end_date,
                'db_check' => 'new'
            );
        
        
           
            return $customer_data;
            
            
            
        }
    }

    function cancel_subscription($subscription_id) {
        try {
            $result = $this->gateway->subscription()->cancel($subscription_id);
            return $result;
        } catch (Exception $e) {
            return false;
        }
    }

    function update_subscription() {
        $this->gateway->subscription()->update($subscriptionId, $attributes);
    }

    function get_customer_information($customer_id) {
        try {
            $customer = $this->gateway->customer()->find($customer_id);
            return $customer;
        }catch(Braintree_Exception_NotFound $e) {
            return array();
        }
    }
    
    function create_transaction($amount, $customerData,$city,$state,$zip_code,$address) {
         $result = $this->gateway->customer()->create($customerData);         
          if ($result->success) {
                    $result1 = $this->gateway->transaction()->sale([
                    'amount' => $amount,
                     'billing' => array(
                        'locality'=> $city,
                        'region' => $state,
                        'postalCode' => $zip_code,
                        'streetAddress' =>$address,
                        ), 
                        'options' => array(
                        'submitForSettlement' => true
                      ),  
                    'paymentMethodToken' => $result->customer->creditCards[0]->token,
                    'customerId' => $result->customer->id
                  ]);
                      
              return $result1;
          }
          else{
              $error_array = array ('status'=>0,
                  'message'=>$result->message
                 );
              return $error_array;
              
          }
    }
    
    function transactions_detail($customer_id) {
        try {
            $collection = $this->gateway->transaction()->search([
                Braintree_TransactionSearch::customerId()->is($customer_id),
            ]);
            return $collection;
        }catch(Braintree_Exception_NotFound $e) {
            return array();
        }
    }
    
      function subscriptions_detail($subscription_id) {
        try {
            $collection = $this->gateway->subscription()->find($subscription_id);
            return $collection;
        }catch(Braintree_Exception_NotFound $e) {
            return array();
        }

    }
    
      function find_transaction_detail($transaction_id) {
        try {
            $collection = $this->gateway->transaction()->find($transaction_id);
            return $collection ;
        }catch(Braintree_Exception_NotFound $e) {
            return array();
        }

    }
    
     function getCardVerification($customer_id)
    {
        try {
            $collection = $this->gateway->creditCardVerification()->search([
                    Braintree_CreditCardVerificationSearch::customerId()->is($customer_id),
                  ]);
            return $collection ;
             }
        catch(Braintree_Exception_NotFound $e) {
            return array();
        }
    }
    
}

?>