/*
SQLyog Ultimate v9.50 
MySQL - 5.5.53-log : Database - sibmeproduction
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `account_framework_settings` */

CREATE TABLE `account_framework_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) DEFAULT NULL,
  `account_tag_id` int(11) DEFAULT NULL,
  `published_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `published` int(11) DEFAULT NULL,
  `framework_name` varchar(255) DEFAULT NULL,
  `enable_unique_desc` int(11) DEFAULT NULL,
  `enable_ascending_order` int(11) DEFAULT NULL,
  `enable_performance_level` int(11) DEFAULT NULL,
  `tier_level` int(11) DEFAULT NULL,
  `checkbox_level` int(11) DEFAULT NULL,
  `framework_sample` text,
  `parent_child_share` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
