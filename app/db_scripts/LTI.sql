CREATE TABLE `assignmentlti2_consumer` (
  `consumer_pk` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `consumer_key256` varchar(256) NOT NULL,
  `consumer_key` text,
  `account_id` int(11) DEFAULT NULL,
  `secret` varchar(1024) NOT NULL,
  `lti_version` varchar(10) DEFAULT NULL,
  `consumer_name` varchar(255) DEFAULT NULL,
  `consumer_version` varchar(255) DEFAULT NULL,
  `consumer_guid` varchar(1024) DEFAULT NULL,
  `profile` text,
  `tool_proxy` text,
  `settings` text,
  `protected` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `enable_from` datetime DEFAULT NULL,
  `enable_until` datetime DEFAULT NULL,
  `last_access` date DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`consumer_pk`),
  UNIQUE KEY `assignmentlti2_consumer_consumer_key_UNIQUE` (`consumer_key256`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1

CREATE TABLE `assignmentlti2_context` (
  `context_pk` int(11) NOT NULL AUTO_INCREMENT,
  `consumer_pk` int(11) NOT NULL,
  `lti_context_id` varchar(255) NOT NULL,
  `settings` text,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`context_pk`),
  KEY `assignmentlti2_context_consumer_id_IDX` (`consumer_pk`),
  CONSTRAINT `assignmentlti2_context_lti2_consumer_FK1` FOREIGN KEY (`consumer_pk`) REFERENCES `assignmentlti2_consumer` (`consumer_pk`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1


CREATE TABLE `assignmentlti2_nonce` (
  `consumer_pk` int(11) NOT NULL,
  `value` varchar(32) NOT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`consumer_pk`,`value`),
  CONSTRAINT `assignmentlti2_nonce_lti2_consumer_FK1` FOREIGN KEY (`consumer_pk`) REFERENCES `assignmentlti2_consumer` (`consumer_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

CREATE TABLE `assignmentlti2_resource_link` (
  `resource_link_pk` int(11) NOT NULL AUTO_INCREMENT,
  `context_pk` int(11) DEFAULT NULL,
  `consumer_pk` int(11) DEFAULT NULL,
  `lti_resource_link_id` varchar(255) NOT NULL,
  `settings` text,
  `primary_resource_link_pk` int(11) DEFAULT NULL,
  `share_approved` tinyint(1) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`resource_link_pk`),
  KEY `assignmentlti2_resource_link_lti2_resource_link_FK1` (`primary_resource_link_pk`),
  KEY `assignmentlti2_resource_link_consumer_pk_IDX` (`consumer_pk`),
  KEY `assignmentlti2_resource_link_context_pk_IDX` (`context_pk`),
  CONSTRAINT `assignmentlti2_resource_link_lti2_resource_link_FK1` FOREIGN KEY (`primary_resource_link_pk`) REFERENCES `assignmentlti2_resource_link` (`resource_link_pk`),
  CONSTRAINT `assignmentlti2_resource_link_lti2_context_FK1` FOREIGN KEY (`context_pk`) REFERENCES `assignmentlti2_context` (`context_pk`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1

CREATE TABLE `assignmentlti2_share_key` (
  `share_key_id` varchar(32) NOT NULL,
  `resource_link_pk` int(11) NOT NULL,
  `auto_approve` tinyint(1) NOT NULL,
  `expires` datetime NOT NULL,
  PRIMARY KEY (`share_key_id`),
  KEY `assignmentlti2_share_key_resource_link_pk_IDX` (`resource_link_pk`),
  CONSTRAINT `assignmentlti2_share_key_lti2_resource_link_FK1` FOREIGN KEY (`resource_link_pk`) REFERENCES `assignmentlti2_resource_link` (`resource_link_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

CREATE TABLE `assignmentlti2_tool_proxy` (
  `tool_proxy_pk` int(11) NOT NULL AUTO_INCREMENT,
  `tool_proxy_id` varchar(32) NOT NULL,
  `consumer_pk` int(11) NOT NULL,
  `tool_proxy` text NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`tool_proxy_pk`),
  UNIQUE KEY `assignmentlti2_tool_proxy_tool_proxy_id_UNIQUE` (`tool_proxy_id`),
  KEY `assignmentlti2_tool_proxy_consumer_id_IDX` (`consumer_pk`),
  CONSTRAINT `assignmentlti2_tool_proxy_lti2_consumer_FK1` FOREIGN KEY (`consumer_pk`) REFERENCES `assignmentlti2_consumer` (`consumer_pk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

CREATE TABLE `assignmentlti2_user_result` (
  `user_pk` int(11) NOT NULL AUTO_INCREMENT,
  `resource_link_pk` int(11) NOT NULL,
  `lti_user_id` varchar(255) NOT NULL,
  `lti_result_sourcedid` varchar(1024) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`user_pk`),
  KEY `assignmentlti2_user_result_resource_link_pk_IDX` (`resource_link_pk`),
  CONSTRAINT `assignmentlti2_user_result_lti2_resource_link_FK1` FOREIGN KEY (`resource_link_pk`) REFERENCES `assignmentlti2_resource_link` (`resource_link_pk`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1

CREATE TABLE `assignment_submission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_id` int(11) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `assignment_id` int(11) DEFAULT NULL,
  `huddle_id` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=latin1

CREATE TABLE `huddles_assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `huddle_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date_added` datetime DEFAULT NULL,
  `course_id` varchar(255) DEFAULT NULL,
  `account_id` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=latin1
