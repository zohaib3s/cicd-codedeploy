<?php
$user = $this->Session->read('Auth');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$user_id = $user_current_account['User']['id'];
$permission_maintain_folders = $user_current_account['users_accounts']['permission_maintain_folders'];
$user_role_id = $user_current_account['roles']['role_id'];
$account_id = $user_current_account['accounts']['account_id'];
?>
<?php
$month_arr = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
if (date('m') >= $accAnalyticsDuration) {
    $yr = date('Y');
} else {
    $yr = date('Y') - 1;
}
?>

<div class="counterBg analyticsdiv sibme_container">
    <style type="text/css">
        .sel_dur_grph{
            width:200px;
            float:left;
        }
        .cl_token_field{
            width:200px;
            float:left;
            margin-left:5px;
            margin-right:5px;
        }
        .filter_box select{
            -moz-appearance: tab-scroll-arrow-forward;
            vertical-align: top;
        }
        .filter_box .tokenfield{
            display:inline-block;
        }
        .token .close{
            margin-top:0px;
            margin-right:0px;
        }
        .token .token-label{
            max-width: 60px !important;
        }
        .tokenfield{
            min-height: 37px !important;
        }
        .tokenfield .token-input{
            height: 20px !important;
        }
        .colCell {position: sticky;
                  left: 0;
                  width: 165px !important;
                  z-index: 9;
                  background: #fff;}
        #grid_account_name {position: sticky;
                            left: 0;
                            width: 165px !important;
                            z-index: 9;
                            background: #e0e0e0;}

        .search_mod_class
        {
            position: sticky;
            left: 0;
            width: 165px !important;
            z-index: 9;
            background: #e0e0e0;

        }

        #ui-datepicker-div
        {
            z-index:9 !important;
        }





    </style>
    <div class=" clrcls1"></div>
    <section class="company_detail">
        <h1>Analytics</h1>
        <!--<h2><?php echo $user_current_account['accounts']['company_name']; ?></h2>-->
    </section>
    <a href="<?php echo $this->base . '/Dashboard/home' ?>" style="cursor:pointer;text-decoration:underline;color:#6588a6;font:14px 'Segoe UI';padding:0 0 0 17px;margin:0 0 25px 0;">Back to Dashboard</a>
    <div class="container analyticsdiv-inner">
        <div>
            <div class="clrcls1"></div>
            <div id="masterAccAnalytics">
                <?php echo $masterAccountAnalytics; ?>
            </div>
        </div>
        <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
            <?php if (!empty($frameworks_added) && count($frameworks_added) > 0): ?>
                <section class="clearfix main_data">
                    <h1>Frequency of Tagged Standards for Coaching Huddle video sessions.</h1>
                    <div class="filter_box clearfix">
                        <div class="filter_detail_box">
                            <label>Frequency</label>
                            <select id="durationGrph" onchange="changeDuration();" style="width:175px;">
                                <option value="3" selected="selected">Monthly</option>
                                <option value="2">Quarterly</option>
                                <option value="1">Yearly</option>
                            </select>
                        </div>
                        <div class="filter_detail_box">
                            <label>Account</label>
                            <select id="subAccountGrph" style="width:175px;">
                                <?php if (is_array($account_sub_users) && count($account_sub_users) > 0): ?>
                                    <option value="" selected="selected">All Accounts</option>
                                <?php endif; ?>
                                <option value="<?php echo $user_current_account['accounts']['account_id']; ?>"><?php echo $user_current_account['accounts']['company_name']; ?>
                                </option>
                                <?php if (is_array($account_sub_users) && count($account_sub_users) > 0): ?>
                                    <?php foreach ($account_sub_users as $sub_user): ?>
                                        <option value="<?php echo $sub_user['a']['id']; ?>"><?php echo $sub_user['a']['company_name']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="filter_detail_box">
                            <?php
                            $month_arr = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
                            if (date('m') >= $accAnalyticsDuration) {
                                $yr = date('Y');
                            } else {
                                $yr = date('Y') - 1;
                            }
                            ?>
                            <div class="accountselect" id="fromMonthDiv" style="float: left;margin-right:8px;">
                                <label>Month From:</label>
                                <select id="monthFrom" name="monthFrom" style="width: 85px;">
                                    <?php
                                    for ($m = 1; $m <= 12; $m++) {
                                        $month = "<option value=$m";
                                        if ($accAnalyticsDuration == $m)
                                            $month .= " selected=selected";
                                        $month .=">" . $month_arr[$m - 1] . "</option>";
                                        echo $month;
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="accountselect"  id="fromQuarterDiv" style="float: left;margin-right:8px;display: none;">
                                <label>Quarter From:</label>
                                <select id="quarterFrom" name="quarterFrom" style="width: 102px;">
                                    <?php
                                    for ($q = 1; $q <= 4; $q++) {
                                        echo "<option value=$q>Quarter $q</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="accountselect" style="float: left;margin-right:8px;">
                                <label><span id="yr_from" style="display: none;margin-bottom: -20px;">Year From:</span>&nbsp;</label>
                                <select id="yearFrom" name="yearFrom" style="width: 85px;">
                                    <?php
                                    $curr_year = date('Y');
                                    for ($y = 2012; $y <= $curr_year; $y++) {
                                        $year = "<option value=$y";
                                        if ($yr == $y)
                                            $year .= " selected=selected";
                                        $year .=">$y</option>";
                                        echo $year;
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="accountselect"  id="toMonthDiv" style="float: left;margin-right:8px;">
                                <label>Month To:</label>
                                <select id="monthTo" name="monthTo" style="width: 85px;">
                                    <?php
                                    for ($m = 1; $m <= 12; $m++) {
                                        $month = "<option value=$m";
                                        if ($accAnalyticsDuration == $m)
                                            $month .= " selected=selected";
                                        $month .=">" . $month_arr[$m - 1] . "</option>";
                                        echo $month;
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="accountselect"  id="toQuarterDiv" style="float: left;margin-right:8px;display: none;">
                                <label>Quarter To:</label>
                                <select id="quarterTo" name="quarterTo" style="width: 102px;">
                                    <?php
                                    for ($q = 1; $q <= 4; $q++) {
                                        echo "<option value=$q>Quarter $q</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="accountselect" style="float: left;">
                                <label><span id="yr_to" style="display: none;margin-bottom: -20px;">Year To:</span>&nbsp;</label>
                                <?php
                                if (date('m') >= $accAnalyticsDuration) {
                                    $curr_year = date('Y') + 1;
                                } else {
                                    $curr_year = date('Y');
                                }
                                ?>
                                <select id="yearTo" name="yearTo" style="width: 85px;">
                                    <?php
                                    for ($y = 2012; $y <= $curr_year; $y++) {
                                        if ($y == $curr_year)
                                            $s = 'selected=selected';
                                        echo "<option value=$y $s>$y</option>";
                                    }
                                    ?>
                                </select>
                            </div>

                            <div class="" style="float:left;margin-left: 10px;">
                                <?php if (!empty($frameworks) && count($frameworks) > 0): ?>
                                    <label>Select Framework:</label>
                                    <select name="frameworks" id="frameworks">
                                        <?php foreach ($frameworks as $framework) { ?>
                                            <option value="<?php echo $framework['AccountTag']['account_tag_id'] ?>"><?php echo $framework['AccountTag']['tag_title'] ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                <?php endif; ?>
                            </div>



                        </div>
                        <div class="clear"></div>
                        <div style="margin-top:5px;" class="filter_div">
                            <style>
                                ::-webkit-input-placeholder {
                                    color: #999;
                                }

                                :-moz-placeholder { /* Firefox 18- */
                                    color: #999;
                                }

                                ::-moz-placeholder {  /* Firefox 19+ */
                                    color: #999;
                                }

                                :-ms-input-placeholder {
                                    color: #999;
                                }

                            </style>
                            <div style="float: right;">
                                <label style="height: 6px;"></label>
                                <input type="text" class="form-control" id="tokenfield" name="search_by_standards" placeholder="Filter Standards" value style="width: 250px;" />
                                <button style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?> ; margin-right:52px;" type="button" class="btn btn-green" >Filter</button>

                            </div>
                        </div>
                        <form accept-charset="UTF-8" action="<?php echo $this->base . '/Dashboard/masterAccountAnalyticsGraph_export' ?>"  enctype="multipart/form-data" id="form_export_graph" method="post">
                            <input type="hidden" name="subAccount" id="subAccount_export">
                            <input type="hidden" name="duration" id="duration_export">
                            <input type="hidden" name="startDate" id="startDate_export">
                            <input type="hidden" name="endDate" id="endDate_export">
                            <input type="hidden" name="search_by_standards" id="search_by_standards_export">
                            <input type="hidden" name="framework_id" id="framework_id_export">
                            <input type="hidden" name="coaching_eval" value = '1' >
                        </form>
                        <button style="margin-right:52px; background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button" class="btn btn-green">Export</button>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $('#tokenfield').tokenfield({
                                    autocomplete: {
                                        //source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
                                        source: <?php echo $standards; ?>,
                                        delay: 100,
                                        minWidth: 10,
                                        createTokensOnBlur: true,
                                    },
                                    showAutocompleteOnFocus: true
                                });
                                $('#tokenfield').on('tokenfield:createdtoken', function (e) {
                                    $("#tokenfield-tokenfield").blur();
                                }).tokenfield()

                            });
                        </script>

                    </div>
                    <div>
                        <div id="masterAccAnalyticsGraph">
                            <?php echo $masterAccAnalyticsGraph; ?>
                        </div>
                    </div>
                </section>
            <?php endif; ?>
        <?php endif; ?>
        <?php if ($this->Custom->check_if_eval_huddle_active($account_id)): ?>
            <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                <?php if (!empty($frameworks_added) && count($frameworks_added) > 0): ?>
                    <section class="clearfix main_data">
                        <h1>Frequency of Tagged Standards and ratings for Assessment Huddles.</h1>
                        <div class="filter_box clearfix">
                            <div class="filter_detail_box">
                                <label>Frequency</label>
                                <select id="durationGrph_eval" onchange="changeDuration();" style="width:175px;">
                                    <option value="3" selected="selected">Monthly</option>
                                    <option value="2">Quarterly</option>
                                    <option value="1">Yearly</option>
                                </select>
                            </div>
                            <div class="filter_detail_box">
                                <label>Account</label>
                                <select id="subAccountGrph_eval" style="width:175px;">
                                    <?php if (is_array($account_sub_users) && count($account_sub_users) > 0): ?>
                                        <option value="" selected="selected">All Accounts</option>
                                    <?php endif; ?>
                                    <option value="<?php echo $user_current_account['accounts']['account_id']; ?>"><?php echo $user_current_account['accounts']['company_name']; ?>
                                    </option>
                                    <?php if (is_array($account_sub_users) && count($account_sub_users) > 0): ?>
                                        <?php foreach ($account_sub_users as $sub_user): ?>
                                            <option value="<?php echo $sub_user['a']['id']; ?>"><?php echo $sub_user['a']['company_name']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="filter_detail_box">
                                <?php
                                $month_arr = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
                                if (date('m') >= $accAnalyticsDuration) {
                                    $yr = date('Y');
                                } else {
                                    $yr = date('Y') - 1;
                                }
                                ?>
                                <div class="accountselect" id="fromMonthDiv" style="float: left;margin-right:8px;">
                                    <label>Month From:</label>
                                    <select id="monthFrom_eval" name="monthFrom_eval" style="width: 85px;">
                                        <?php
                                        for ($m = 1; $m <= 12; $m++) {
                                            $month = "<option value=$m";
                                            if ($accAnalyticsDuration == $m)
                                                $month .= " selected=selected";
                                            $month .=">" . $month_arr[$m - 1] . "</option>";
                                            echo $month;
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="accountselect"  id="fromQuarterDiv" style="float: left;margin-right:8px;display: none;">
                                    <label>Quarter From:</label>
                                    <select id="quarterFrom_eval" name="quarterFrom_eval" style="width: 102px;">
                                        <?php
                                        for ($q = 1; $q <= 4; $q++) {
                                            echo "<option value=$q>Quarter $q</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="accountselect" style="float: left;margin-right:8px;">
                                    <label><span id="yr_from" style="display: none;margin-bottom: -20px;">Year From:</span>&nbsp;</label>
                                    <select id="yearFrom_eval" name="yearFrom_eval" style="width: 85px;">
                                        <?php
                                        $curr_year = date('Y');
                                        for ($y = 2012; $y <= $curr_year; $y++) {
                                            $year = "<option value=$y";
                                            if ($yr == $y)
                                                $year .= " selected=selected";
                                            $year .=">$y</option>";
                                            echo $year;
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="accountselect"  id="toMonthDiv" style="float: left;margin-right:8px;">
                                    <label>Month To:</label>
                                    <select id="monthTo_eval" name="monthTo_eval" style="width: 85px;">
                                        <?php
                                        for ($m = 1; $m <= 12; $m++) {
                                            $month = "<option value=$m";
                                            if ($accAnalyticsDuration == $m)
                                                $month .= " selected=selected";
                                            $month .=">" . $month_arr[$m - 1] . "</option>";
                                            echo $month;
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="accountselect"  id="toQuarterDiv" style="float: left;margin-right:8px;display: none;">
                                    <label>Quarter To:</label>
                                    <select id="quarterTo_eval" name="quarterTo_eval" style="width: 102px;">
                                        <?php
                                        for ($q = 1; $q <= 4; $q++) {
                                            echo "<option value=$q>Quarter $q</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="accountselect" style="float: left;">
                                    <label><span id="yr_to" style="display: none;margin-bottom: -20px;">Year To:</span>&nbsp;</label>
                                    <?php
                                    if (date('m') >= $accAnalyticsDuration) {
                                        $curr_year = date('Y') + 1;
                                    } else {
                                        $curr_year = date('Y');
                                    }
                                    ?>
                                    <select id="yearTo_eval" name="yearTo_eval" style="width: 85px;">
                                        <?php
                                        for ($y = 2012; $y <= $curr_year; $y++) {
                                            if ($y == $curr_year)
                                                $s = 'selected=selected';
                                            echo "<option value=$y $s>$y</option>";
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="" style="float:left;margin-left: 10px;">
                                    <?php if (!empty($frameworks) && count($frameworks) > 0): ?>
                                        <label>Select Framework:</label>
                                        <select name="frameworks_eval" id="frameworks_eval">
                                            <?php foreach ($frameworks as $framework) { ?>
                                                <option value="<?php echo $framework['AccountTag']['account_tag_id'] ?>"><?php echo $framework['AccountTag']['tag_title'] ?></option>
                                            <?php }
                                            ?>
                                        </select>
                                    <?php endif; ?>
                                </div>



                            </div>
                            <div class="clear"></div>
                            <div style="margin-top:5px;" class="filter_div">
                                <style>
                                    ::-webkit-input-placeholder {
                                        color: #999;
                                    }

                                    :-moz-placeholder { /* Firefox 18- */
                                        color: #999;
                                    }

                                    ::-moz-placeholder {  /* Firefox 19+ */
                                        color: #999;
                                    }

                                    :-ms-input-placeholder {
                                        color: #999;
                                    }

                                    /*                        .filter_div .token-input{
                                                                min-width: 60px !important;
                                                                border: 1px solid rgb(237, 237, 237) !important;
                                                                padding: 16px !important;
                                                                font-weight: normal !important;
                                                                color: rgb(0, 0, 0) !important;
                                                                width: 252px !important;
                                                                box-shadow: none !important;
                                                            }
                                                            .filter_div .focus{
                                                                box-shadow: none !important;
                                                            }*/
                                </style>
                                <div style="float: right;">
                                    <label style="height: 6px;"></label>
                                    <input type="text" class="form-control" id="tokenfield_1" name="search_by_standards" placeholder="Filter Standards" value style="width: 250px;" />
                                    <button style="margin-right:52px; background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button" class="btn btn-green" onclick="ajaxMasterAccountGraph_for_evaluation();">Filter</button>

                                </div>
                            </div>

                            <form accept-charset="UTF-8" action="<?php echo $this->base . '/Dashboard/masterAccountAnalyticsGraph_export/3' ?>"  enctype="multipart/form-data" id="form_export_graph_1" method="post">
                                <input type="hidden" name="subAccount" id="subAccount_export_1">
                                <input type="hidden" name="duration" id="duration_export_1">
                                <input type="hidden" name="startDate" id="startDate_export_1">
                                <input type="hidden" name="endDate" id="endDate_export_1">
                                <input type="hidden" name="search_by_standards" id="search_by_standards_export_1">
                                <input type="hidden" name="framework_id" id="framework_id_export_1">
                                <input type="hidden" name="coaching_eval" value = '2' >


                            </form>
                            <button style="margin-right:52px; background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button" class="btn btn-green" onclick="ajaxMasterAccountGraph_for_evaluation_export();">Export</button>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('#tokenfield_1').tokenfield({
                                        autocomplete: {
                                            //source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
                                            source: <?php echo $standards; ?>,
                                            delay: 100,
                                            minWidth: 10,
                                            createTokensOnBlur: true,
                                        },
                                        showAutocompleteOnFocus: true
                                    });
                                    $('#tokenfield_1').on('tokenfield_1:createdtoken', function (e) {
                                        $("#tokenfield_1-tokenfield_1").blur();

                                    }).tokenfield()
                                });
                            </script>

                        </div>
                        <div>
                            <div id="masterAccAnalyticsGraph1">
                                <?php echo $masterAccAnalyticsGraphForEvaluation; ?>
                            </div>
                        </div>
                    </section>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
        <div class=" clrcls1"></div>
        <section class="clearfix main_data">
            <h1>User Summary</h1>
            <div class="filter_box clearfix">
                <div class="filter_detail_box">
                    <label>Start Date</label>
                    <input type="text" id="fromUsr" style="width:132px;" />
                </div>
                <div class="filter_detail_box">
                    <label>End Date</label>
                    <input type="text" id="toUsr" style="width:132px;"/>
                </div>
                <div class="filter_detail_box">
                    <label>Select Account</label>
                    <select id="subAccountUsers" style="width: 175px;">
                        <?php if (is_array($account_sub_users) && count($account_sub_users) > 0): ?>
                            <option value="" selected="selected">All Accounts</option>
                        <?php endif; ?>
                        <option value="<?php echo $user_current_account['accounts']['account_id']; ?>"><?php echo $user_current_account['accounts']['company_name']; ?></option>
                        <?php if (is_array($account_sub_users) && count($account_sub_users) > 0): ?>
                            <?php foreach ($account_sub_users as $sub_user): ?>
                                <option value="<?php echo $sub_user['a']['id']; ?>"><?php echo $sub_user['a']['company_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </div>
                <div class="filter_search_box">
                    <button type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="btn btn-green" onclick="ajaxAccountUsers();">Search</button>
                </div>
            </div>
        </section>
    </div>
    <!--/end container-->
    <div id="accUsersAnalytics" class="data_grid_green data_grid_adjs">
        <?php echo $accUsersAnalytics; ?>
    </div>
</div>

<div class="clrcls1"></div>

<script type="text/javascript">
    $(document).ready(function () {
        App.initCounter();
    });
</script>
<script type="text/javascript">
    $(function () {
        var st_m = <?php echo $accAnalyticsDuration - 1; ?>;
        var st_d = 1;
        var st_y = <?php echo $yr; ?>;
        var sDate = new Date(st_y, st_m, st_d);
        var eDate = new Date();
        eDate.setDate(eDate.getDate());
        $('#from').val($.datepicker.formatDate('mm/dd/yy', sDate));
        $('#to').val($.datepicker.formatDate('mm/dd/yy', eDate));
        $('#fromGrph').val($.datepicker.formatDate('mm/dd/yy', sDate));
        $('#toGrph').val($.datepicker.formatDate('mm/dd/yy', eDate));
        $('#fromUsr').val($.datepicker.formatDate('mm/dd/yy', sDate)
                );
        $('#toUsr').val($.datepicker.formatDate('mm/dd/yy', eDate));

        $("#from").datepicker({
            changeMonth: true,
            changeYear: true,
            showOn: "button",
            buttonImage: '<?php echo $this->webroot . "img/icons/calendar_icon.png"; ?>',
            onClose: function (selectedDate) {
                $("#to").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#to").datepicker({
            changeMonth: true,
            changeYear: true,
            showOn: "button",
            buttonImage: '<?php echo $this->webroot . "img/icons/calendar_icon.png"; ?>',
        });

        $("#fromGrph").datepicker({
            changeMonth: true, changeYear: true,
            showOn: "button",
            buttonImage: '<?php echo $this->webroot . "img/icons/calendar_icon.png"; ?>',
            onClose: function (selectedDate) {
                $("#toGrph").datepicker("option", "minDate", selectedDate);
            }
        });

        $("#toGrph").datepicker({
            changeMonth: true,
            changeYear: true,
            showOn: "button",
            buttonImage: '<?php echo $this->webroot . "img/icons/calendar_icon.png"; ?>',
        });

        $("#fromUsr").datepicker({
            changeMonth: true,
            changeYear: true,
            showOn: "button",
            buttonImage: '<?php echo $this->webroot . "img/icons/calendar_icon.png"; ?>',
            onClose: function (selectedDate) {
                $("#toUsr").datepicker("option", "minDate", selectedDate);
            },
        });

        $("#toUsr").datepicker({
            changeMonth: true,
            changeYear: true,
            showOn: "button",
            buttonImage: '<?php echo $this->webroot . "img/icons/calendar_icon.png"; ?>',
        });
        $("#subAccount").change(function () {
            $.ajax({
                type: 'POST',
                url: home_url + '/Dashboard/subAccountAnalytics',
                dataType: 'html',
                data: {subAccount: $('#subAccount').val(), startDate: $('#from').val(), endDate: $('#to').val()},
                success: function (res) {
                    $('#subAccountName').html($("#subAccount").children("option").filter(":selected").text());
                    $('#subAccountAnalytics').html(res);
                    ajaxAccountUsers();
                }
            });
        });

    });
    function ajaxMasterAccount() {
        $.ajax({
            type: 'POST',
            url: home_url + '/Dashboard/masterAccountAnalytics',
            dataType: 'html',
            success: function (res) {
                $('#masterAccAnalytics').html(res);
                App.initCounter();
            }
        });
    }
    function ajaxMasterAccountGraph() {
        if ($('#durationGrph').val() == 1) {
            var durationFrom = $('#yearFrom').val();
            var durationTo = $('#yearTo').val();
        }
        if ($('#durationGrph').val() == 2) {
            var durationFrom = $('#yearFrom').val() + '-' + $('#quarterFrom').val();
            var durationTo = $('#yearTo').val() + '-' + $('#quarterTo').val();
        }
        if ($('#durationGrph').val() == 3) {
            var durationFrom = $('#yearFrom').val() + '-' + $('#monthFrom').val();
            var durationTo = $('#yearTo').val() + '-' + $('#monthTo').val();
        }
        if ($('#frameworks').val() > 0) {
            var framework = $('#frameworks').val();
        }
        $.ajax({
        type: 'POST',
                url: home_url + '/Dashboard/masterAccountAnalyticsGraph',
                dataType: 'html',
                //data: {subAccount:$('#subAccountGrph').val(),duration:$('#durationGrph').val(),startDate:$('#fromGrph').val(),endDate:$('#toGrph').val()},
                data: {subAccount: $('#subAccountGrph').val(), duration: $('#durationGrph').val(), startDate: durationFrom, endDate: durationTo, search_by_standards: $('#tokenfield').val(), framework_id: framework},
                success: function (res) {
                    $('#masterAccAnalyticsGraph').html(res);
                    //callAjx($('#frameworks').val());             }
                });
                var url = '';
        if ($('#subAccountGrph').val() == '')
        {
            url = home_url + '/Dashboard/masterAccountAnalytics/2';
        }
        else {
            url = home_url + '/Dashboard/masterAccountAnalytics/1'
        }
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'html',
            //data: {subAccount:$('#subAccountGrph').val(),duration:$('#durationGrph').val(),startDate:$('#fromGrph').val(),endDate:$('#toGrph').val()},
            data: {subAccount: $('#subAccountGrph').val(), duration: $('#durationGrph').val(), startDate: durationFrom, endDate: durationTo, search_by_standards: $('#tokenfield').val(), framework_id: framework},
            success: function (res) {
                $('#masterAccAnalytics').html(res);
                //callAjx($('#frameworks').val());
            }
        });

        }
    }
    function ajaxMasterAccountGraph_export() {
        if ($('#durationGrph').val() == 1) {
            var durationFrom = $('#yearFrom').val();
            var durationTo = $('#yearTo').val();
        }
        if ($('#durationGrph').val() == 2) {
            var durationFrom = $('#yearFrom').val() + '-' + $('#quarterFrom').val();
            var durationTo = $('#yearTo').val() + '-' + $('#quarterTo').val();
        }
        if ($('#durationGrph').val() == 3) {
            var durationFrom = $('#yearFrom').val() + '-' + $('#monthFrom').val();
            var durationTo = $('#yearTo').val() + '-' + $('#monthTo').val();
        }
        if ($('#frameworks').val() > 0) {
            var framework = $('#frameworks').val();
        }


        $('#subAccount_export').val($('#subAccountGrph').val());
        $('#duration_export').val($('#durationGrph').val());
        $('#startDate_export').val(durationFrom);
        $('#endDate_export').val(durationTo);
        $('#search_by_standards_export').val($('#tokenfield').val());
        $('#framework_id_export').val(framework);

        $("#form_export_graph").submit();





    }



    function ajaxMasterAccountGraph_for_evaluation() {
        if ($('#durationGrph_eval').val() == 1) {
            var durationFrom = $('#yearFrom_eval').val();
            var durationTo = $('#yearTo_eval').val();
        }
        if ($('#durationGrph_eval').val() == 2) {
            var durationFrom = $('#yearFrom_eval').val() + '-' + $('#quarterFrom_eval').val();
            var durationTo = $('#yearTo_eval').val() + '-' + $('#quarterTo_eval').val();
        }
        if ($('#durationGrph_eval').val() == 3) {
            var durationFrom = $('#yearFrom_eval').val() + '-' + $('#monthFrom_eval').val();
            var durationTo = $('#yearTo_eval').val() + '-' + $('#monthTo_eval').val();
        }
        if ($('#frameworks_eval').val() > 0) {
            var framework = $('#frameworks_eval').val();
        }
        $.ajax({
            type: 'POST',
            url: home_url + '/Dashboard/masterAccountAnalyticsGraph/3',
            dataType: 'html',
            //data: {subAccount:$('#subAccountGrph').val(),duration:$('#durationGrph').val(),startDate:$('#fromGrph').val(),endDate:$('#toGrph').val()},
            data: {subAccount: $('#subAccountGrph_eval').val(), duration: $('#durationGrph_eval').val(), startDate: durationFrom, endDate: durationTo, search_by_standards: $('#tokenfield_1').val(), framework_id: framework},
            success: function (res) {

                $('#masterAccAnalyticsGraph1').html(res);
                //callAjx($('#frameworks').val());
            }
        });
        var url = '';
        if ($('#subAccountGrph').val() == '')
        {
            url = home_url + '/Dashboard/masterAccountAnalytics/2';
        } else {
            url = home_url + '/Dashboard/masterAccountAnalytics/1'
        }
        $.ajax({
            type: 'POST',
            url: url,
            dataType: 'html',
            //data: {subAccount:$('#subAccountGrph').val(),duration:$('#durationGrph').val(),startDate:$('#fromGrph').val(),endDate:$('#toGrph').val()},
            data: {subAccount: $('#subAccountGrph').val(), duration: $('#durationGrph').val(), startDate: durationFrom, endDate: durationTo, search_by_standards: $('#tokenfield').val(), framework_id: framework},
            success: function (res) {
                $('#masterAccAnalytics').html(res);
                //callAjx($('#frameworks').val());
            }
        });

    }

    function ajaxMasterAccountGraph_for_evaluation_export() {
        if ($('#durationGrph_eval').val() == 1) {
            var durationFrom = $('#yearFrom_eval').val();
            var durationTo = $('#yearTo_eval').val();
        }
        if ($('#durationGrph_eval').val() == 2) {
            var durationFrom = $('#yearFrom_eval').val() + '-' + $('#quarterFrom_eval').val();
            var durationTo = $('#yearTo_eval').val() + '-' + $('#quarterTo_eval').val();
        }
        if ($('#durationGrph_eval').val() == 3) {
            var durationFrom = $('#yearFrom_eval').val() + '-' + $('#monthFrom_eval').val();
            var durationTo = $('#yearTo_eval').val() + '-' + $('#monthTo_eval').val();
        }
        if ($('#frameworks_eval').val() > 0) {
            var framework = $('#frameworks_eval').val();
        }



        $('#subAccount_export_1').val($('#subAccountGrph_eval').val());
        $('#duration_export_1').val($('#durationGrph_eval').val());
        $('#startDate_export_1').val(durationFrom);
        $('#endDate_export_1').val(durationTo);
        $('#search_by_standards_export_1').val($('#tokenfield_1').val());
        $('#framework_id_export_1').val(framework);

        $("#form_export_graph_1").submit();



    }



    $("#frameworks").change(function () {
        $.ajax({
            type: 'POST',
            url: home_url + '/Dashboard/getFramework',
            dataType: 'json',
            data: {framework_id: $('#frameworks').val()},
            success: function (res) {
                //console.log(res);
                $('#tokenfield').data('bs.tokenfield').$input.autocomplete({source: res})
            }
        });
    });
    function ajaxSubAccount() {
        $.ajax({
            type: 'POST',
            url: home_url + '/Dashboard/subAccountAnalytics',
            dataType: 'html',
            data: {subAccount: $('#subAccount').val(), startDate: $('#from').val(), endDate: $('#to').val()},
            success: function (res) {
                $('#subAccountAnalytics').html(res);
            }
        });
    }
    function ajaxAccountUsers() {
        $.ajax({
            type: 'POST',
            url: home_url + '/Dashboard/accountUsersAnalytics',
            dataType: 'html',
            data: {subAccount: $('#subAccountUsers').val(), startDate: $('#fromUsr').val(), endDate: $('#toUsr').val()},
            success: function (res) {
                $('#accUsersAnalytics').html(res);
            }
        });

        var url = '';
        if ($('#subAccountUsers').val() == '')
        {
            url = home_url + '/Dashboard/masterAccountAnalytics/2';
        }
        else {
            url = home_url + '/Dashboard/masterAccountAnalytics/1'
        }
        $.ajax({
            type: 'POST', url: url,
            dataType: 'html',
            //data: {subAccount:$('#subAccountGrph').val(),duration:$('#durationGrph').val(),startDate:$('#fromGrph').val(),endDate:$('#toGrph').val()},
            data: {subAccount: $('#subAccountUsers').val(), startDate: $('#fromUsr').val(), endDate: $('#toUsr').val()},
            success: function (res) {
                $('#masterAccAnalytics').html(res);
                //callAjx($('#frameworks').val());
            }
        });


    }
    $("#durationGrph").change(function () {
        if ($(this).val() == 1) {
            $('#fromMonthDiv').css('display', 'none');
            $('#fromQuarterDiv').css('display', 'none');
            $('#toMonthDiv').css('display', 'none');
            $('#toQuarterDiv').css('display', 'none');
            $('#yr_from').css('display', 'block');
            $('#yr_to').css('display', 'block');
        }
        if ($(this).val() == 2) {
            $('#fromMonthDiv').css('display', 'none');
            $('#fromQuarterDiv').css('display', 'block');
            $('#toMonthDiv').css('display', 'none');
            $('#toQuarterDiv').css('display', 'block');
            $('#yr_from').css('display', 'none');
            $('#yr_to').css('display', 'none');
        }
        if ($(this).val() == 3) {
            $('#fromMonthDiv').css('display', 'block');
            $('#fromQuarterDiv').css('display', 'none');
            $('#toMonthDiv').css('display', 'block');
            $('#toQuarterDiv').css('display', 'none');
            $('#yr_from').css('display', 'none');
            $('#yr_to').css('display', 'none');
        }
    });

    var myGrid = $("#list");
// ...
    $(document).ready(function () {
        $('.ui-search-toolbar th:nth-child(3)').addClass('search_mod_class');
    });


</script>
<style>
    .desk-cta.desk-sidetab{
        display: none !important;
    }
</style>