<style>
    .modal-content form {
        padding: 0px;
    }
</style>
    <div class="header" style="padding: 10px 10px 0px 15px;margin-bottom: 10px;">
        <h4 class="header-title nomargin-vertical smargin-bottom"><?=$language_based_content['video_session_detail'];?></h4>
        <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">x</a>
    </div>
<div class="video_dialog analytics_new">
    <div class="video_left">
        <?php
        $document_files_array = $this->Custom->get_document_url($document['Document']);

        $thumbnail_image_path = $document_files_array['thumbnail'];
        ?>
        <a target="_blank" href="<?php echo $this->base . '/Huddles/view/' . $video_detail['afd']['account_folder_id'] . '/1/' . $video_detail['Document']['id']; ?>" >
            <img src="<?php echo $thumbnail_image_path; ?>" width="223" height="148" alt=""/>
            <div class="play-icon" style="top: 170px !important;left: 120px;"></div>
        </a>
        <h3><?php //echo $video_detail['Document']['original_file_name'];                                                                                                                                                                ?></h3>
        <h1 class="tracker_name"><?php echo $coachee_name['User']['first_name'] . ' ' . $coachee_name['User']['last_name']; ?></h1>
        <div class="video_upload_date"><span><?=$language_based_content['session_date'];?> </span>
            <?php
            $created_at = explode(' ', $video_detail['Document']['recorded_date']);
            $created_date = explode('-', $created_at[0]);
            echo $created_date[1] . '-' . $created_date[2] . '-' . $created_date[0] . ' ' . date('h:i A', strtotime($video_detail['Document']['recorded_date']));
            ?>
        </div>
    </div>
    <form method="post" action="<?php echo $this->base; ?>/Dashboard/remove_tracking" name="remove_tracking_form" id="remove_tracking_form" style="display:none;">
        <input type="hidden" name="remove_from_tracking" value="remove_assessment_tracking">
        <input type="hidden" name="document_id" value="<?php echo $video_detail['Document']['id']; ?>">
        <input type="hidden" name="load_back" value="<?php echo $load_back; ?>">
        <input type="hidden" name="huddle_id" value="<?php echo $video_detail['afd']['account_folder_id']; ?>">
    </form>
    <form method="post" action="<?php echo $this->base; ?>/Dashboard/assessment_note">
        <input type="hidden" name="load_back" value="<?php echo $load_back; ?>">
        <input type="hidden" name="coach_feedback_id" value="<?php
        if (count($assessment_feedback) > 0) {
            echo $assessment_feedback['Comment']['id'];
        }
        ?>">
        <input type="hidden" name="document_id" value="<?php echo $video_detail['Document']['id']; ?>">
        <input type="hidden" name="huddle_id" value="<?php echo $video_detail['afd']['account_folder_id']; ?>">
        <div class="video_right">
            <div class="green_count_box" style="min-height: 92px;">
                <div class="green_count_box_number"><?php echo $coach_comments; ?></div>
                <?php if ($this->Custom->check_if_eval_huddle($video_detail['afd']['account_folder_id'])): ?>
                    <b><?=$language_based_content['Assessor_Comments_Added '];?></b>
                <?php else: ?>
                    <b><?=$language_based_content['Coach_Comments_Added_tracker'];?></b>
                <?php endif; ?>
            </div>
            <div class="green_count_box">
                <div class="green_count_box_number"><?php echo $coachee_comments; ?></div>
                <?php if ($this->Custom->check_if_eval_huddle($video_detail['afd']['account_folder_id'])): ?>
                    <b><?=$language_based_content['Assessed_Participant_Comments_Added']; ?></b>
                <?php else: ?>
                    <b><?=$language_based_content['Coachee_Comments_Added_tracker']; ?></b>
                <?php endif; ?>
            </div>
            <?php if ($this->Custom->check_if_eval_huddle($video_detail['afd']['account_folder_id'])): ?>
                <div class="green_count_box" style="height: 91px;">
                    <div class="green_count_box_number"><?php echo $resources; ?></div>
                    <b><?=$language_based_content['resources_attached']; ?></b>
                </div>
            <?php else: ?>
                <div class="green_count_box">
                    <div class="green_count_box_number"><?php echo $resources; ?></div>
                    <b><?=$language_based_content['resources_attached']; ?></b>
                </div>
            <?php endif; ?>
            <div class="clear"></div>
            <div class="comment_box"><?=$language_based_content['Assessors_Session_Summary']; ?></div>
            <textarea class="dialog_text_area" style="height: 80px;" name="assessment_note"><?php
                if (count($assessment_feedback) > 0) {
                    echo stripcslashes($assessment_feedback['Comment']['comment']);
                }
                ?></textarea>
            <style>
                .graph_btn{
                    background: #e74c3c;
                    width: 139px;
                    color: #fff;
                    border-radius: 5px;
                    float: left;
                    margin: 7px 0px -8px 16px;
                    padding: 10px;
                    cursor: pointer;
                    text-align: center;
                }
                .amChartsLegend{top: 30px;}
            </style>
        </div>
        <div class="clear"></div>
        <!--        <div class="graph_btn" id="graph_rating_link" style="display: none;">Graph of Standards</div>
                <div class="graph_btn" id="graph_rubric_link">Graph of Ratings</div>-->
        <div id="coaching_graph" style="display: none;" >
            <div id="chartCoachVideo" style="width: 100%; height: 430px;"></div>
        </div>
        <!--        <div id="assessment_graph">
                    <div id="chartdiv" style="width: 100%; height: 330px;"></div>
                </div>-->

        <hr/>
        <div class="clear"></div>
        <div class="btns dialog_btn_box">
            <div class="remove_tracker_button"><a href="#"><?=$language_based_content['remove_from_tracker'];?></a></div>
            <?php if ($load_back > 0) { ?>
                <div class="btn" style="margin-left: 10px;border: 1px solid #ded7d7;"><a style="color: #80a0bb;font-weight: 600;" href="<?php echo $url; ?>"> <?=$language_based_content['Back_to_Assessment_Tracker']; ?></a></div>
            <?php } ?>
            <div class="actions_btn_box">
                <input class="sessionDateSave btn btn-green" type="submit" value="<?=$language_based_content['save_popup']; ?>" style="background-color:<?php echo $this->Custom->get_site_settings('primary_bg_color')?>; border-color:<?php echo $this->Custom->get_site_settings('primary_border_color')?>"/>
                <input class="btn btn-white" type="reset" value="<?=$language_based_content['cancel_popup']; ?>" style="top: -2px;position: relative;" onclick="$('#assessment_modal').modal('hide');"/>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </form>
</div>
<?php // var_dump($video_tags); ?>
<script>
//    $("#graph_rubric_link").click(function (e) {
//        $("#assessment_graph").hide();
    $("#coaching_graph").show();
    $("#graph_rating_link").show();

//        $("#graph_rubric_link").hide();
    var chart = AmCharts.makeChart("chartCoachVideo", {
        "type": "serial",
        "theme": "light",
        "categoryField": "tag_title",
        "rotate": false,
        "marginTop": 60,
        "marginBottom": 60,
        "startEffect": 'easeInSine',
        "startDuration": 0.5,
        "autoMargins": true,
        "dataProvider": <?php echo $video_tags; ?>,
        "categoryAxis": {
            "position": "left",
            "gridPosition": "start",
            "gridAlpha": 0,
            "tickPosition": "start",
            "tickLength": 20,
            "labelRotation": 45,
            "labelFunction": function (label) {
                if (typeof (label) != 'undefined' && label.length > 0) {
                    if (label.length > 15)
                        return label.substr(0, 14) + '...';
                    return label;
                }

            }

        },
        "trendLines": [],
        "graphs": [
            {
                "valueAxis": "ValueAxis-1",
                "colorField": "color",
                "fixedColumnWidth": 10,
                "balloonText": "[[category]]: <b>[[value]]</b> <br> Average PL : [[label]]",
                "fillAlphas": 0.8,
                "id": "AmGraph-1",
                "lineAlpha": 0.2,
                "type": "column",
                "title": "[[tag_title]]",
                "valueField": "total_tags"
            },
            {
                "valueAxis": "ValueAxis-r",
                "colorField": "color_rating",
                "balloonText": "[[Assessment]]",
                "fixedColumnWidth": 10,
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "title": "red line",
                "valueField": "average_rating",
            },
        ],
        "legend": {
            "equalWidths": false,
            "autoMargins": false,
            "position": "top",
            "valueAlign": "left",
            "markerType": "square",
            "valueWidth": 2,
            "verticalGap": 2,
            divId: "legenddiv_1",
            "data": [{
                    "title": "<?php echo $standard_title . ': ' . $total_standards; ?>",
                    "color": "<?php echo $standard_color; ?>"
                },
                {
                    "title": "<?php echo $ratting_title . ': ' . $total_ratting; ?>",
                    "color": "<?php echo $ratting_color; ?>"
                }
            ]
        },
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "valueAxes": [
            {
                "id": "ValueAxis-1",
                "position": "left",
                "axisAlpha": 1,
                "labelRotation": 45,
            },
            {
                "id": "ValueAxis-r",
                "axisAlpha": 1,
                "integersOnly": true,
                "dashLength": 1,
                "inside": false,
                "labelRotation": 45,
                "position": "right",
                "maximum": <?php echo count($assessment_array); ?>,
                "valueText": "assessment_point",
                "labelFunction": formatValue,
            }

        ],
        "balloon": {},
        "titles": [],
        "export": {
            "enabled": true,
            "menu": [{
                    "class": "export-main",
                    "menu": ["PNG", "JPG", "SVG", "PDF"]
                }]
        },
    });


    var chart1 = AmCharts.makeChart("chartCoachVideo1", {
        "pathToImages": '<?php echo $this->webroot; ?>amcharts/images/',
        "type": "serial",
        "theme": "light",
        "dataProvider": <?php echo $video_tags; ?>,
        "graphs": [{
                "id": "g2",
                "balloonText": "[[category]]: <b>[[value]]</b> <br> Rating : [[label]]",
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "tag_title",
            },
            {
                "id": "g1",
                "balloonText": "[[Assessment]]",
                "bullet": "round",
//                "bulletBorderAlpha": 1,
                "fixedColumnWidth": 10,
                "fillAlphas": 0.8,
                "lineAlpha": 0.2,
                "bulletColor": "#FFFFFF",
                "hideBulletsCount": 50,
                "title": "red line",
                "valueField": "assessment_point",
                "useLineColorForBulletBorder": true,
                "balloon": {
                    "drop": true
                }
            }
        ],
        "valueAxes": [
            {
                "id": "ValueAxis-<?php echo $count; ?>",
                "position": "left",
                "axisAlpha": 1,
                "title": "Number of Standard Tags",
                "titleBold": false,
                "titleFontSize": "10",
            },
            {
                "id": "ValueAxis-r",
                "axisAlpha": 1,
                "autoGridCount": true,
                "gridCount": 6,
                "integersOnly": true,
                "dashLength": 1,
                "position": "right",
                /*"showLastLabel": true,
                 "showFirstLabel": true,*/
                //"labelFrequency": 5,
                "minimum": 0,
                "maximum": 6,
                "valueText": "standard_rating_avg",
                "labelFunction": formatValue,
                "title": "Performance Level",
                "titleBold": false,
                "titleFontSize": "10",
                "minVerticalGap": 1,
            }

        ],
//        "valueAxes": [
//            {
//                "id": "ValueAxis-1",
//                "axisAlpha": 0,
//                "integersOnly": true,
//                "position": "left",
////                "gridColor": "#FFFFFF",
////                "dashLength": 1
//            },
//            {
//                "id": "ValueAxis-r",
//                "axisAlpha": 1,
//                "integersOnly": true,
//                "dashLength": 1,
//                "position": "right",
//                "maximum": <?php echo count($assessment_array); ?>,
//                "valueText": "assessment_point",
//                "labelFunction": formatValue,
//                "inside": true,
//                "title": "Ratings"
//            }
//        ],
        "gridAboveGraphs": true,
        "startDuration": 1,
        "chartCursor": {
            "categoryBalloonEnabled": false,
            "cursorAlpha": 0,
            "zoomable": false
        },
        "categoryField": "tag_title",
        "categoryAxis": {
            "gridPosition": "start",
            "gridAlpha": 0,
            "tickPosition": "start",
            "tickLength": 20,
            "position": "left",
            "labelFunction": function (label) {
                if (typeof (label) != 'undefined' && label.length > 0) {
                    if (label.length > 10)
                        return label.substr(0, 10) + '...';
                    return label;
                } else {

                    return ' ';
                }
            }
        },
        "export": {
            "enabled": true,
            "menu": [{
                    "class": "export-main",
                    "menu": ["PNG", "JPG", "SVG", "PDF"]
                }]
        }

    });
    AmCharts.checkEmptyData = function (chart) {
        if (0 == chart.dataProvider.length) {
            // set min/max on the value axis
            chart.valueAxes[0].minimum = 0;
            chart.valueAxes[0].maximum = 100;
            // add dummy data point
            var dataPoint = {
                dummyValue: 0
            };
            dataPoint[chart.categoryField] = '';
            chart.dataProvider = [dataPoint];
            // add label
            chart.addLabel(0, '50%', '<?php echo $language_based_content['chart_contains_no_data']; ?>', 'center');
            // set opacity of the chart div
            chart.chartDiv.style.opacity = 0.5;
            // redraw it
            chart.validateNow();
        }
    }

    AmCharts.checkEmptyData(chart);
//    });
//    $("#graph_rating_link").click(function (e) {
//        $("#coaching_graph").hide();
//        $("#assessment_graph").show();
//        $("#graph_rating_link").hide();
//        $("#graph_rubric_link").show();
//    });
//    var assessments = ['no value', 'bad', 'average', 'good'];

//    var chart = AmCharts.makeChart("chartdiv", {
//        "type": "serial",
//        "pathToImages": '<?php // echo $this->webroot;                                                                                                                                           ?>amcharts/images/',
//        "theme": "light",
//        "dataProvider": <?php // echo $assessment_graph;                                                                                                                                           ?>,
//        "valueAxes": [{
//                "axisAlpha": 1,
//                "integersOnly": true,
//                "dashLength": 1,
//                "position": "left",
//                "maximum": <?php // echo count($assessment_array);                                                                                                                                           ?>,
//                "valueText": "assessment_point",
//                "labelFunction": formatValue,
//                "inside": true,
//                "title": "Ratings"
//            }],
//        "gridAboveGraphs": true,
//        "startDuration": 1,
//        "showLastLabel": false,
//        "chartScrollbar": {
//        },
//        "graphs": [
//    {
//    "id": "g1",
//            "balloonText": "[[Assessment]]",
//            "bullet": "round",
//            "bulletBorderAlpha": 1,
//            "bulletColor": "#FFFFFF",
//            "hideBulletsCount": 50,
//            "title": "red line",
//            "valueField": "assessment_point",
//            "useLineColorForBulletBorder": true,
//            "balloon": {
//            "drop": true
//            }
//    }
//            ],
//        "chartCursor": {
//            "limitToGraph": "g1"
//        },
//        "categoryField": "created_date",
//        "categoryAxis": {
//            "axisColor": "#DADADA",
//            "labelRotation": 45,
//            //   "dashLength": 1,
//            //  "parseDates": true,
//            //  "minPeriod": "DD",
//            //  "equalSpacing": true,
//            //  "ignoreAxisWidth": true,
//            //   "inside": true
//        },
//        "export": {
//            "enabled": true,
//            "menu": [{
//                    "class": "export-main",
//                    "menu": ["PNG", "JPG", "SVG", "PDF"]
//                }]
//        }
//    });

    AmCharts.checkEmptyData = function (chart) {
        if (0 == chart.dataProvider.length) {
            // set min/max on the value axis
            chart.valueAxes[0].minimum = 0;
            chart.valueAxes[0].maximum = 100;
            // add dummy data point
            var dataPoint = {
                dummyValue: 0
            };
            dataPoint[chart.categoryField] = '';
            chart.dataProvider = [dataPoint];
            // add label
            chart.addLabel(0, '50%', '<?php echo $language_based_content['chart_contains_no_data']; ?>', 'center');
            // set opacity of the chart div
            chart.chartDiv.style.opacity = 0.5;
            // redraw it
            chart.validateNow();
        }
    }
    AmCharts.checkEmptyData(chart);
    chart.addListener("dataUpdated", zoomChart);
    zoomChart();
    function zoomChart() {
        if (chart) {
            if (chart.zoomToIndexes) {
                // chart.zoomToIndexes(130, chartData.length - 1);
            }
        }
    }
    function formatValue(value, formattedValue, valueAxis) {
        var assessment_array = new Array();
<?php foreach ($assessment_array as $key => $val) { ?>
            assessment_array.push("<?php echo $val; ?>");
<?php } ?>
//
        if (value == 0) {
            return typeof (assessment_array[0]) != 'undefined' ? assessment_array[0] : "No Assesment";
        }
        else if (value == 1) {
            return typeof (assessment_array[1]) != 'undefined' ? assessment_array[1] : "";
        }
        else if (value == 2) {
            return typeof (assessment_array[2]) != 'undefined' ? assessment_array[2] : "";
        }
        else if (value == 3) {
            return typeof (assessment_array[3]) != 'undefined' ? assessment_array[3] : "";
        }
        else if (value == 4) {
            return typeof (assessment_array[4]) != 'undefined' ? assessment_array[4] : "";
        }
        else if (value == 5) {
            return typeof (assessment_array[5]) != 'undefined' ? assessment_array[5] : "";
        } else {
            return '';
        }
    }


</script>
