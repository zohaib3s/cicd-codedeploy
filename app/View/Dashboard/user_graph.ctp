<div class="counterBg analyticsdiv box" style="width: 1024px; padding: 10px 0px 0px 0px;">
    <section style="margin-top:-9px;" class="company_detail">
        <h1>Analytics</h1>
        <!--<h2><?php //echo $user_current_account['accounts']['company_name'];          ?></h2>-->
    </section>
    <a href="<?php echo $this->base . '/analytics' ?>" style="cursor:pointer;text-decoration:underline;color:#6588a6;font:14px 'Segoe UI';padding:0 0 0 17px;margin:0 0 25px 0;">Back to Analytics</a>
    <div class=" clrcls1"></div>
    <div class="container analyticsdiv-inner">
        <section class="clearfix account_basic">
            <h1>Evidence of standards in <?php echo $user_name; ?>'s Coaching Huddle video sessions</h1>
            <form method="post" id="user_detail_form" action="<?php echo $this->base . "/Dashboard/userGraph/$user_id/$account_id/$st_date/$en_date/2"; ?>" accept-charset="UTF-8">
                <input type="hidden" name="filter_check" value="1">
                <div class="filter_box clearfix">
                    <div class="accountselect1 filter_detail_box">

                        <style type="text/css">
                            .sel_dur_grph{
                                width:200px;
                                float:left;
                            }
                            .cl_token_field{
                                width:200px;
                                float:left;
                                margin-left:5px;
                                margin-right:5px;
                            }
                            .filter_box select{
                                -moz-appearance: tab-scroll-arrow-forward;
                                vertical-align: top;
                            }
                            .filter_box .tokenfield{
                                display:inline-block;
                            }
                            .token .close{
                                margin-top:0px;
                                margin-right:0px;
                            }
                            .token .token-label{
                                max-width: 60px !important;
                            }
                            .tokenfield{
                                min-height: 37px !important;
                            }
                            .tokenfield .token-input{
                                height: 20px !important;
                            }
                            ::-webkit-input-placeholder {
                                color: #999;
                            }

                            :-moz-placeholder { /* Firefox 18- */
                                color: #999;
                            }

                            ::-moz-placeholder {  /* Firefox 19+ */
                                color: #999;
                            }

                            :-ms-input-placeholder {
                                color: #999;
                            }
                        </style>

                        <div class="filter_detail_box">
                            <label>Frequency</label>
                            <select id="durationGrph" name="durationGrph" style="width: 200px;">
                                <option value="3" <?php if ($duration == 3) echo 'selected=selected'; ?>>Monthly</option>
                                <option value="2" <?php if ($duration == 2) echo 'selected=selected'; ?>>Quarterly</option>
                                <option value="1" <?php if ($duration == 1) echo 'selected=selected'; ?>>Yearly</option>
                            </select>
                        </div>
                        <div class="filter_detail_box">
                            <label>User Type </label>
                            <select id="userType" name="userType" style="width: 200px;">
                                <option value="coach" <?php if ($uType == 'coach') echo 'selected=selected'; ?>>Coach</option>
                                <option value="coachee" <?php if ($uType == 'coachee') echo 'selected=selected'; ?>>Coachee</option>
                                <option value="all" <?php if ($uType == 'all') echo 'selected=selected'; ?>>All</option>
                            </select>
                        </div>
                        <?php if (!empty($frameworks) && count($frameworks) > 0): ?>
                            <div class="filter_detail_box">
                                <label>Select Framework:</label>
                                <select name="framework_id" id="frameworks">
                                    <?php foreach ($frameworks as $framework) { ?>
                                        <option value="<?php echo $framework['AccountTag']['account_tag_id'] ?>"><?php echo $framework['AccountTag']['tag_title'] ?></option>
                                    <?php }
                                    ?>
                                </select>
                            </div>
                        <?php endif; ?>
                        <div class="filter_detail_box">
                            <label style="height: 18px;"></label>
                            <input type="text" class="form-control" id="tokenfield" name="search_by_standards" placeholder="Filter Standards" value="<?php // if(isset($search_by_standard)){ echo $standards;}     ?>" style="width: 250px;" />
                        </div>
                        <div class="filter_search_box"><input type="submit" class="btn btn-green" value="Filter"></div>

                        <script type="text/javascript">
                            $(document).ready(function () {
                                $('#tokenfield').tokenfield({
                                    autocomplete: {
                                        //source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
                                        source: <?php echo $standards; ?>,
                                        delay: 100,
                                        minWidth: 10
                                    },
                                    showAutocompleteOnFocus: true
                                });
                                $('#tokenfield').tokenfield(
                                        'setTokens', <?php echo $saved_standard_values; ?>
                                );
                                $('#tokenfield').on('tokenfield:createdtoken', function (e) {
                                    $("#tokenfield-tokenfield").blur();
                                }).tokenfield()
                            });
                        </script>
                    </div>

            </form>
    </div>
    <form accept-charset="UTF-8" action="<?php echo $this->base . "/Dashboard/userGraph_export/$user_id/$account_id/$st_date/$en_date/2"; ?>"  enctype="multipart/form-data" id="form_export_graph" method="post">
        <input type="hidden" name="durationGrph" id="durationGrph_export">
        <input type="hidden" name="userType" id="userType_export">
        <input type="hidden" name="search_by_standards" id="tokenfield_export">
        <input type="hidden" name="coaching_eval" value = '1' >
        <input type="hidden" name="user_name" value = '<?php echo $user_name; ?>' >


    </form>
    <button style="margin-right:52px; background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button" class="btn btn-green" onclick="ajax_coach_graph_export(2);">Export</button>


    <div class="gra-div">

        <div id="chartdivCoach" style="width: 100%; height: 500px;"></div>

    </div> <!--- ga -->

    <h1>Evidence of standards in <?php echo $user_name; ?>'s Assessment Huddle</h1>
    <form method="post" id="user_detail_form" action="<?php echo $this->base . "/Dashboard/userGraph/$user_id/$account_id/$st_date/$en_date/3"; ?>" accept-charset="UTF-8">
        <input type="hidden" name="filter_check" value="2">
        <div class="filter_box clearfix">
            <div class="accountselect1 filter_detail_box">

                <style type="text/css">
                    .sel_dur_grph{
                        width:200px;
                        float:left;
                    }
                    .cl_token_field{
                        width:200px;
                        float:left;
                        margin-left:5px;
                        margin-right:5px;
                    }
                    .filter_box select{
                        -moz-appearance: tab-scroll-arrow-forward;
                        vertical-align: top;
                    }
                    .filter_box .tokenfield{
                        display:inline-block;
                    }
                    .token .close{
                        margin-top:0px;
                        margin-right:0px;
                    }
                    .token .token-label{
                        max-width: 60px !important;
                    }
                    .tokenfield{
                        min-height: 37px !important;
                    }
                    .tokenfield .token-input{
                        height: 20px !important;
                    }
                    ::-webkit-input-placeholder {
                        color: #999;
                    }

                    :-moz-placeholder { /* Firefox 18- */
                        color: #999;
                    }

                    ::-moz-placeholder {  /* Firefox 19+ */
                        color: #999;
                    }

                    :-ms-input-placeholder {
                        color: #999;
                    }
                </style>

                <div class="filter_detail_box">
                    <label>Frequency</label>
                    <select id="durationGrph_1" name="durationGrph" style="width: 200px;">
                        <option value="3" <?php if ($duration == 3) echo 'selected=selected'; ?>>Monthly</option>
                        <option value="2" <?php if ($duration == 2) echo 'selected=selected'; ?>>Quarterly</option>
                        <option value="1" <?php if ($duration == 1) echo 'selected=selected'; ?>>Yearly</option>
                    </select>
                </div>
                <div class="filter_detail_box">
                    <label>User Type </label>
                    <select id="userType_1" name="userType" style="width: 200px;">
                        <option value="coach" <?php if ($uType == 'coach') echo 'selected=selected'; ?>>Assessor</option>
                        <option value="coachee " <?php if ($uType == 'coachee') echo 'selected=selected'; ?>>Assessed Participant</option>
                        <option value="all" <?php if ($uType == 'all') echo 'selected=selected'; ?>>All</option>
                    </select>
                </div>
                <?php if (!empty($frameworks) && count($frameworks) > 0): ?>
                    <div class="filter_detail_box">
                        <label>Select Framework:</label>
                        <select name="framework_id" id="frameworks">
                            <?php foreach ($frameworks as $framework) { ?>
                                <option value="<?php echo $framework['AccountTag']['account_tag_id'] ?>"><?php echo $framework['AccountTag']['tag_title'] ?></option>
                            <?php }
                            ?>
                        </select>
                    </div>
                <?php endif; ?>
                <div class="filter_detail_box">
                    <label style="height: 18px;"></label>
                    <input type="text" class="form-control" id="tokenfield_1" name="search_by_standards" placeholder="Filter Standards" value style="width: 250px;" />
                </div>
                <div class="filter_search_box"><input type="submit" class="btn btn-green" value="Filter"></div>

                <script type="text/javascript">
                    $(document).ready(function () {
                        $('#tokenfield_1').tokenfield({
                            autocomplete: {
                                //source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
                                source: <?php echo $standards; ?>,
                                delay: 100,
                                minWidth: 10
                            },
                            showAutocompleteOnFocus: true
                        });
                        $('#tokenfield_1').tokenfield(
                                'setTokens', <?php echo $saved_standard_values_1; ?>
                        );
                        $('#tokenfield_1').on('tokenfield_1:createdtoken', function (e) {
                            $("#tokenfield_1-tokenfield_1").blur();
                        }).tokenfield()
                    });
                </script>
            </div>

    </form>
</div>


<form accept-charset="UTF-8" action="<?php echo $this->base . "/Dashboard/userGraphForEvaluationHuddle_export/$user_id/$account_id/$st_date/$en_date/3"; ?>"  enctype="multipart/form-data" id="form_export_graph_1" method="post">
    <input type="hidden" name="durationGrph" id="durationGrph_export_1">
    <input type="hidden" name="userType" id="userType_export_1">
    <input type="hidden" name="search_by_standards" id="tokenfield_export_1">
    <input type="hidden" name="coaching_eval" value = '2' >
    <input type="hidden" name="user_name" value = '<?php echo $user_name; ?>' >

</form>
<button style="margin-right:52px; background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button" class="btn btn-green" onclick="ajax_eval_graph_export(2);">Export</button>



<div class="gra-div">

    <div id="chartdivEvaluation" style="width: 100%; height: 500px;"></div>

</div> <!--- ga -->
</section>

</div>
</div>

<script>
    $("#frameworks").change(function () {
        $.ajax({
            type: 'POST',
            url: home_url + '/Dashboard/getFramework',
            dataType: 'json',
            data: {framework_id: $('#frameworks').val()},
            success: function (res) {
                $('#tokenfield').data('bs.tokenfield').$input.autocomplete({source: res})
            }
        });
    });

    var chart = AmCharts.makeChart("chartdivCoach", {
        "pathToImages": '<?php echo $this->webroot; ?>amcharts/images/',
        "type": "serial",
        "theme": "dark",
        "legend": {
            "useGraphSettings": true,
            "valueText": "[[value]]",
            "fontSize": 10
        },
        "balloon": {
            "textAlign": "left",
            "maxWidth": 400,
            "verticalPadding": 8
        },
        "dataProvider": <?php echo $total_coaching_tags; ?>,
        "valueAxes": [{
                "id": "v1",
                "integersOnly": true,
                "axisColor": "#FF6600",
                "axisThickness": 2,
                "gridAlpha": 0,
                "axisAlpha": 1,
                "position": "left",
                "title": "Total Tags"
            }, {
                "id": "v11",
                "integersOnly": true,
                "axisColor": "#FCD202",
                "axisThickness": 2,
                "gridAlpha": 0,
                "axisAlpha": 1,
                "position": "right",
                "title": "Total Sessions"
            }],
        "graphs": [{
                "valueAxis": "v1",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_0]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_0",
                "valueField": "tag0",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v2",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_1]]: [[value]]</span>",
                //"legendValueText":"[[tag_title_1]]",
                "valueField": "tag1",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_1",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v3",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_2]]: [[value]]</span>",
                //"legendValueText":"[[tag_title_2]]",
                "valueField": "tag2",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_2",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v4",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_3]]: [[value]]</span>",
                "valueField": "tag3",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_3",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v5",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_4]]: [[value]]</span>",
                //"legendValueText":"tag_title_0",
                "valueField": "tag4",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_4",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v6",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_5]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_5",
                "valueField": "tag5",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v7",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_6]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_6",
                "valueField": "tag6",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v8",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_7]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_7",
                "valueField": "tag7",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v9",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_8]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_8",
                "valueField": "tag8",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v10",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_9]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_9",
                "valueField": "tag9",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v11",
                "bullet": "square",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "title": "Total Sessions",
                "dashLength": 5,
                "showBalloon": false,
                "valueField": "total_sessions",
                "fillAlphas": 0
            }],
        "chartScrollbar": {},
        "chartCursor": {
            "cursorPosition": "mouse"
        },
        "categoryField": "date",
        "categoryAxis": {
            "axisColor": "#DADADA",
            "minorGridEnabled": true,
        },
        "export": {
            "enabled": true,
            "position": "bottom-right"
        }
    });


    AmCharts.checkEmptyData = function (chart) {
        if (0 == chart.dataProvider.length) {
            // set min/max on the value axis
            chart.valueAxes[0].minimum = 0;
            chart.valueAxes[0].maximum = 100;

            // add dummy data point
            var dataPoint = {
                dummyValue: 0
            };
            dataPoint[chart.categoryField] = '';
            chart.dataProvider = [dataPoint];

            // add label
            chart.addLabel(0, '50%', 'The chart contains no data', 'center');

            // set opacity of the chart div
            chart.chartDiv.style.opacity = 0.5;

            // redraw it
            chart.validateNow();
        }
    }

    AmCharts.checkEmptyData(chart);
//chart.addListener("init", handleClick);
    AmCharts.handleClick = function (chart) {
//function handleClick(event){
        console.log(chart.graphs);
        var d = chart.dataProvider[0];
        var garr = [];
        for (var i in chart.graphs) {
            var g = chart.graphs[i];
            //var j = 'tag_title_'+i;
            if (typeof d['tag_title_' + i] != 'undefined') {
                g.visibleInLegend = true;
                if (d['tag_title_' + i].length > 30)
                    g.title = d['tag_title_' + i].substr(0, 30) + '..';
                else
                    g.title = d['tag_title_' + i];
                garr.push(d['tag_title_' + i]);
            } else {
                g.visibleInLegend = false;
            }

        }

        var g = chart.graphs[10];
        //console.log(g);
        g.visibleInLegend = true;
        g.title = 'Total Sessions';
        g.showBalloon = true;
        var html = "<b><span style='font-size:10px;padding-bottom: 4px;display: inline-block;'>Total Sessions: [[value]]</span><br>";
        //console.log(tag_title_0);
        for (var i in garr) {
            html += "<span style='font-size:10px;padding-bottom: 4px;display: inline-block;'>" + garr[i] + ": [[tag" + i + "]]</span><br>"
        }
        //html +="</span>";
        g.balloonText = html;
        chart.validateNow();
    }
    AmCharts.handleClick(chart);


    var chart_eval = AmCharts.makeChart("chartdivEvaluation", {
        "pathToImages": '<?php echo $this->webroot; ?>amcharts/images/',
        "type": "serial",
        "theme": "dark",
        "legend": {
            "useGraphSettings": true,
            "valueText": "[[value]]",
            "fontSize": 10
        },
        "balloon": {
            "textAlign": "left",
            "maxWidth": 400,
            "verticalPadding": 8
        },
        "dataProvider": <?php echo $total_coaching_tags_eval; ?>,
        "valueAxes": [{
                "id": "v1",
                "integersOnly": true,
                "axisColor": "#FF6600",
                "axisThickness": 2,
                "gridAlpha": 0,
                "axisAlpha": 1,
                "position": "left",
                "title": "Total Tags"
            }, {
                "id": "v11",
                "integersOnly": true,
                "axisColor": "#FCD202",
                "axisThickness": 2,
                "gridAlpha": 0,
                "axisAlpha": 1,
                "position": "right",
                "title": "Total Sessions"
            }],
        "graphs": [{
                "valueAxis": "v1",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_0]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_0",
                "valueField": "tag0",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v2",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_1]]: [[value]]</span>",
                //"legendValueText":"[[tag_title_1]]",
                "valueField": "tag1",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_1",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v3",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_2]]: [[value]]</span>",
                //"legendValueText":"[[tag_title_2]]",
                "valueField": "tag2",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_2",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v4",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_3]]: [[value]]</span>",
                "valueField": "tag3",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_3",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v5",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_4]]: [[value]]</span>",
                //"legendValueText":"tag_title_0",
                "valueField": "tag4",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_4",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v6",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_5]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_5",
                "valueField": "tag5",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v7",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_6]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_6",
                "valueField": "tag6",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v8",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_7]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_7",
                "valueField": "tag7",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v9",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_8]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_8",
                "valueField": "tag8",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v10",
                "bullet": "round",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "color": "#FF6600",
                "showBalloon": false,
                "balloonText": "<b><span style='font-size:14px;'>[[tag_title_9]]: [[value]]</span>",
                "legendValueText": "[[value]]",
                "descriptionField": "tag_title_9",
                "valueField": "tag9",
                "visibleInLegend": false,
                "fillAlphas": 0
            }, {
                "valueAxis": "v11",
                "bullet": "square",
                "bulletBorderThickness": 1,
                "hideBulletsCount": 30,
                "title": "Total Sessions",
                "dashLength": 5,
                "showBalloon": false,
                "valueField": "total_sessions",
                "fillAlphas": 0
            }],
        "chartScrollbar": {},
        "chartCursor": {
            "cursorPosition": "mouse"
        },
        "categoryField": "date",
        "categoryAxis": {
            "axisColor": "#DADADA",
            "minorGridEnabled": true,
        },
        "export": {
            "enabled": true,
            "position": "bottom-right"
        }
    });


    AmCharts.checkEmptyData = function (chart_eval) {
        if (0 == chart_eval.dataProvider.length) {
            // set min/max on the value axis
            chart_eval.valueAxes[0].minimum = 0;
            chart_eval.valueAxes[0].maximum = 100;

            // add dummy data point
            var dataPoint = {
                dummyValue: 0
            };
            dataPoint[chart_eval.categoryField] = '';
            chart_eval.dataProvider = [dataPoint];

            // add label
            chart_eval.addLabel(0, '50%', 'The chart contains no data', 'center');

            // set opacity of the chart div
            chart_eval.chartDiv.style.opacity = 0.5;

            // redraw it
            chart_eval.validateNow();
        }
    }

    AmCharts.checkEmptyData(chart_eval);
//chart.addListener("init", handleClick);
    AmCharts.handleClick = function (chart_eval) {
//function handleClick(event){
        console.log(chart_eval.graphs);
        var d = chart_eval.dataProvider[0];
        var garr = [];
        for (var i in chart_eval.graphs) {
            var g = chart_eval.graphs[i];
            //var j = 'tag_title_'+i;
            if (typeof d['tag_title_' + i] != 'undefined') {
                g.visibleInLegend = true;
                if (d['tag_title_' + i].length > 30)
                    g.title = d['tag_title_' + i].substr(0, 30) + '..';
                else
                    g.title = d['tag_title_' + i];
                garr.push(d['tag_title_' + i]);
            } else {
                g.visibleInLegend = false;
            }

        }

        var g = chart_eval.graphs[10];
        //console.log(g);
        g.visibleInLegend = true;
        g.title = 'Total Sessions';
        g.showBalloon = true;
        var html = "<b><span style='font-size:10px;padding-bottom: 4px;display: inline-block;'>Total Sessions: [[value]]</span><br>";
        //console.log(tag_title_0);
        for (var i in garr) {
            html += "<span style='font-size:10px;padding-bottom: 4px;display: inline-block;'>" + garr[i] + ": [[tag" + i + "]]</span><br>"
        }
        //html +="</span>";
        g.balloonText = html;
        chart_eval.validateNow();
    }
    AmCharts.handleClick(chart_eval);



    function ajax_coach_graph_export() {



        $('#durationGrph_export').val($('#durationGrph').val());
        $('#userType_export').val($('#userType').val());
        $('#tokenfield_export').val($('#tokenfield').val());


        $("#form_export_graph").submit();







    }

    function ajax_eval_graph_export() {



        $('#durationGrph_export_1').val($('#durationGrph_1').val());
        $('#userType_export_1').val($('#userType_1').val());
        $('#tokenfield_export_1').val($('#tokenfield_1').val());


        $("#form_export_graph_1").submit();







    }





</script>
<style>
    .desk-cta.desk-sidetab{
        display: none !important;
    }
</style>