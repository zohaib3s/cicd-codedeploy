<?php $breadcrumb_language_based_content = $this->Custom->get_page_lang_based_content('BreadCrumb'); ?>
<?php
$user = $this->Session->read('Auth');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$user_id = $user_current_account['User']['id'];
$permission_maintain_folders = $user_current_account['users_accounts']['permission_maintain_folders'];
$user_role_id = $user_current_account['roles']['role_id'];
?>

<div class="virtual_tracker_container">
    <div class="virtual_header_box">
        <h1><?=$language_based_content['coaching_tracker_title']?></h1>
        <form method="post" <?php echo $this->base . '/dashboard/coach_tracker'; ?>>
            <div class="virtual_legend_box">
                 <span class="dots_img">
            <img src="/app/img/red_legend.png" width="21" height="21" alt=""/> <?=$language_based_content['no_feedback'];?>
            <img src="/app/img/yellow_legend.png" width="21" height="21" alt=""/> <?=$language_based_content['late_feedback'];?>
            <img src="/app/img/green_label.png" width="21" height="21" alt=""/> <?=$language_based_content['on_time_feedback'];?>
            <img src="/app/img/blue_label.png" width="21" height="21" alt=""/> <?=$language_based_content['coachee_comments'];?>
            <img src="/app/img/published.png" width="21" height="21" alt=""/> <?=$language_based_content['published_observations'];?>
            <img src="/app/img/unpublished.png" width="21" height="21" alt=""/> <?=$language_based_content['unpublished_observations'];?>
        </span>

                <select name="duration" onchange="this.form.submit();">
                    <?php foreach ($duration as $d): ?>
                        <option value="<?php echo $d['vl']; ?>" <?php
                        if ($sel_duration == $d['vl']) {
                            echo 'selected';
                        }
                        ?>><?php echo $d['dt']; ?></option>
                            <?php endforeach; ?>
                </select>
                 <div class="clear"></div>
            </div>
             <div class="clear"></div>
        </form>
        <div class="clear"></div>
   
        
    </div>
    <?php foreach ($account_coaches as $coach): ?>
        <div class="coach_container">
        <?php if($user_role_id!=115){ ?>
            <div class="coach_container_header"> <span><?=$language_based_content['coach'];?></span>
                <h1><?php echo $coach['User']['first_name'] . ' ' . $coach['User']['last_name']; ?></h1>
        <?php } ?>
                    <?php
                       $video_count = 0;
                      foreach ($coach['Coachees'] as $c_v):
                      foreach ($c_v['Videos'] as $videos):
                             

                                if ($videos['Documents']['is_associated'] == 1 && !count($videos['Feedback']) > 0 && !count($videos['Comments']) > 0) {
                                    $video_count++;
                                } elseif ($videos['Documents']['is_associated'] > 1 && $videos['Documents']['current_duration'] != 0 && $videos['Documents']['is_processed'] == 4 && $videos['Documents']['published'] == 1) {
                                  
                                } elseif ($videos['Documents']['doc_type'] != 3) {
                                    $video_count++;
                                   
                                } else {
                                 $video_count++;
                                }
                                ?>
                            <?php endforeach; ?>
                            <?php endforeach; ?>    
                <?php
//                $video_count = 0;
//                foreach ($coach['Coachees'] as $c_v):
//                    $video_count = $video_count + count($c_v['Videos']);
//                endforeach;
                ?>
                <?php if($user_role_id!=115){ ?>
                <span class="video_session"><?php echo $video_count; ?> <?=$language_based_content['video_sessions'];?></span>
                <div class="clear"></div>
            </div>
                <?php } ?>
            <div class="virtual_header">
                <div class="coachee_box"><?=$language_based_content['coachee'];?></div>
                <div class="videos_box"><?=$language_based_content['video_sessions'];?></div>
                <div class="clear"></div>
            </div>
            <div class="virtual_data">

                <div class="virtual_data_Left">
                    <?php $cnt = 0 ?>
                    <?php foreach ($coach['Coachees'] as $coachees): ?>
                         <?php
                         
                          $class1 = '';
                            if (count($coachees['Videos']) >= 11) {
                                $class1 = ' scrool-cl_1';
                            } else {
                                $class1 = '';
                            } 
                         
                         
                         ?>
                    
                        <div class="coachee_box_inner<?php echo $class1;  ?>"><?php echo $coachees['User']['first_name'] . ' ' . $coachees['User']['last_name']; ?></div>
                    <?php endforeach; ?>
                </div>
                <div class="virtual_data_Right">
                    <?php foreach ($coach['Coachees'] as $coachees): ?>
                           <?php
                         
                          $class2 = '';
                            if (count($coachees['Videos']) >= 11) {
                                $class2 = ' scrool-cl_2';
                            } else {
                                $class2 = '';
                            } 
                         
                         
                         ?>
                    
                    
                        <div class="videos_box_container<?php echo $class2;  ?>">
                            <?php if (count($coachees['Videos']) == 0) { ?>
                                <div class="video_sub_box" style="height: 39px;"></div>
                            <?php }
                            ?>
                            <style type="">
                                .video_sub_box_late {
                                    position: relative;
                                    z-index: 9;
                                    background: #ccf2fd !important;
                                    width: 500px;
                                    height: 200px;
                                }
                                .video_sub_box_late:before {
                                    content: "";
                                    position: absolute;
                                    z-index: -1;  top: 0;
                                    right: 50%;
                                    bottom: 0;
                                    left: 0;
                                    background: #fffaa5 !important;
                                }
                                .video_sub_box_ontime {
                                    position: relative;
                                    z-index: 9;
                                    background: #ccf2fd !important;
                                    width: 500px;
                                    height: 200px;
                                }
                                .video_sub_box_ontime:before {
                                    content: "";
                                    position: absolute;
                                    z-index: -1;  top: 0;
                                    right: 50%;
                                    bottom: 0;
                                    left: 0;
                                    background: #b3eaa2 !important;
                                }
                                .video_sub_box_no {
                                    position: relative;
                                    z-index: 9;
                                    background: #ccf2fd !important;
                                    width: 500px;
                                    height: 200px;
                                }
                                .video_sub_box_no:before {
                                    content: "";
                                    position: absolute;
                                    z-index: -1;  top: 0;
                                    right: 50%;
                                    bottom: 0;
                                    left: 0;
                                    background: #fcd2bb !important;
                                }
                                .video_sub_box_un_publish{
                                    position: relative;
                                    z-index: 9;
                                    background: #e74c3c !important;
                                    width: 500px;
                                    height: 200px;
                                }
                                .video_sub_box_publish{
                                    position: relative;
                                    z-index: 9;
                                    background: #2ecc71 !important;
                                    width: 500px;
                                    height: 200px;
                                }

                                .video_sub_box_publish_half{
                                    position: relative;
                                    z-index: 9;
                                    background: #ccf2fd !important;
                                    width: 500px;
                                    height: 200px;
                                }

                                .video_sub_box_publish_half:before {
                                    content: "";
                                    position: absolute;
                                    z-index: -1;  top: 0;
                                    right: 50%;
                                    bottom: 0;
                                    left: 0;
                                    background: #2ecc71  !important;
                                }


.scrool-cl_1 {    padding: 19.5px 9px!important;}
.scrool-cl_2 {    height: 59px!important;}

                            </style>
                            <?php
                            $class = '';
                            if (count($coachees['Videos']) >= 11) {
                                $class = 'scrool-cl';
                            } else {
                                $class = '';
                            }
                            foreach ($coachees['Videos'] as $videos):
                                $display = '';
                                $phpdate = strtotime($videos['Documents']['recorded_date']);
                                $color = '';
                                $modified_date = strtotime('+' . $tracking_duration . ' hours', $phpdate);
                                $new_date = strtotime('Now');
                                if($_SESSION['LANG'] == 'es')
                                {
                                 $mysqldate = $this->Custom->SpanishDate($phpdate,'trackers');
                                }
                                else 
                                {
                                $mysqldate = date('d-M', $phpdate);
                                }


                                if ($videos['Documents']['is_associated'] == 1 && !count($videos['Feedback']) > 0 && !count($videos['Comments']) > 0) {
                                    $cls = 'video_sub_box_publish';
                                } elseif ($videos['Documents']['is_associated'] > 1 && $videos['Documents']['current_duration'] != 0 && $videos['Documents']['is_processed'] == 4 && $videos['Documents']['published'] == 1) {
                                    $display = 'style = "display:none;"';
                                } elseif ($videos['Documents']['doc_type'] != 3) {
                                    if (is_array($videos['Comments']) && count($videos['Comments']) > 0) {
                                        $cmntdate = strtotime($videos['Comments'][0]['Comment']['created_date']);
                                        if ($modified_date >= $cmntdate) {
                                            $cls = 'green_legend_bg';
                                            $c = 1;
                                        } else {
                                            $cls = 'yellow_legend_bg';
                                            $c = 2;
                                        }
                                    } else {
                                        if ($modified_date <= $new_date) {
                                            $cls = 'red_legend_bg';
                                            $c = 3;
                                        } else {
                                            $cls = '';
                                            $c = '';
                                        }
                                    }
                                    if (is_array($videos['Feedback']) && count($videos['Feedback']) > 0) {
                                        if ($c == 1) {
                                            $cls = 'video_sub_box_ontime';
                                        } elseif ($c == 2) {
                                            $cls = 'video_sub_box_late';
                                        } elseif ($c == 3) {
                                            $cls = 'video_sub_box_no';
                                        } else {
                                            $cls = 'blue_legend_bg';
                                            if ($videos['Documents']['is_associated'] == 1) {
                                                $cls = 'video_sub_box_publish_half';
                                            }
                                        }
                                        $c = 4;
                                    }
                                } else {
                                    if ($videos['Documents']['is_associated'] == 0) {
                                        $cls = 'video_sub_box_un_publish';
                                        $color = 'color:white;';
                                    } else {
                                        $cls = 'video_sub_box_publish';
                                    }
                                }
                                ?>
                                <div <?php echo $display; ?> class="video_sub_box <?php echo $class; ?>"><div class="<?php echo $cls; ?>"><a href="<?php echo $this->base . '/dashboard/video_detail/' . $videos['Documents']['id'] . '/' . $coach['User']['id'] . '/' . $coachees['User']['id'] . '/0/' . $c; ?>" rel="tooltip" data-toggle="modal" data-target="#coach_modal" data-remote="false" style="font-weight: 600;<?php echo $color; ?>"><?php echo $mysqldate; ?> </a></div></div>
                            <?php endforeach; ?>
                            <div class="clear"></div>
                        </div>
                        <?php endforeach; ?>
                </div>
                <div class="clear"></div>
            </div>
        </div>
                    <?php endforeach; ?>
</div>

<div id="coach_modal" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 810px;">
        <div class="modal-content">
        </div>
    </div>
</div>

<script>
    $("#coach_modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        console.log(link.attr("href"));
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $(document).on("click", ".remove_tracker_button", function () {
        var r = confirm('<?php echo $language_based_content['are_you_sure_want_to_del_this']; ?>');
        if (r === true)
        {
            $('#remove_tracking_form').submit();
        }
    });
</script>
<style type="text/css">

    .virtual_data_Right{ overflow: hidden;}
    .videos_box_container{    overflow: auto;overflow-y: hidden;}
  
</style>

<script>
     $(document).ready(function (e) {
         
                        var bread_crumb_data =  '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><span href="#"><?php echo $breadcrumb_language_based_content['coaching_tracker_breadcrumb']; ?></span></div>';

                        $('.breadCrum').html(bread_crumb_data);
         
                        });
</script>
