<?php $breadcrumb_language_based_content = $this->Custom->get_page_lang_based_content('BreadCrumb'); ?>
<?php
$user = $this->Session->read('Auth');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$user_id = $user_current_account['User']['id'];
$account_id = $user_current_account['accounts']['account_id'];
$permission_maintain_folders = $user_current_account['users_accounts']['permission_maintain_folders'];
$user_role_id = $user_current_account['roles']['role_id'];
?>

<div class="virtual_tracker_container">
    <div class="virtual_header_box">
        <h1><?=$language_based_content['assessment_tracker_traker'];?></h1>
        <form method="post" <?php echo $this->base . '/dashboard/coach_tracker'; ?>>
            <div class="virtual_legend_box">
                <span class="dots_img">
                <img src="/app/img/dark_green_label.png" width="21" height="21" alt=""/> <?=$language_based_content['On_time_Submission'];?>
                <img src="/app/img/red_legend.png" width="21" height="21" alt=""/> <?=$language_based_content['Late_Submission'];?>
                <img src="/app/img/green_label.png" width="21" height="21" alt=""/> <?=$language_based_content['Scored_On_time'];?>
                <img src="/app/img/orange_label.png" width="21" height="21" alt=""/> <?=$language_based_content['Scored_late'];?>
                </span>
                <select name="duration" onchange="this.form.submit();">
                    <?php foreach ($duration as $d): ?>
                        <option value="<?php echo $d['vl']; ?>" <?php
                        if ($sel_duration == $d['vl']) {
                            echo 'selected';
                        }
                        ?>><?php echo $d['dt']; ?></option>
                            <?php endforeach; ?>
                </select>
                 <div class="clear"></div>
            </div>
        </form>
        <div class="clear"></div>
    </div>
    <?php foreach ($account_coaches as $coach): ?>
        <?php
        $account_folder_id = $coach['AccountFolder']['account_folder_id'];

        $submission_date = $this->Custom->get_submission_date($account_folder_id);
        ?>
        <div class="coach_container">
            <div class="coach_container_header"> <span><?=$language_based_content['Assessor'];?></span>
                <h1><?php echo $coach['User']['first_name'] . ' ' . $coach['User']['last_name']; ?></h1>
                <div class="clear"></div>
            </div>
            <?php
            $huddle_ids = array();
            $participant_ids = array();
            $all_huddles = $this->Custom->get_evaluator_huddles($account_id, $coach['User']['id']);
            foreach ($all_huddles as $row) {
                $huddle_ids[] = $row['AccountFolder']['account_folder_id'];
            }
            $evaluated_participants = $this->Custom->get_huddle_participants($huddle_ids, $coach['User']['id']);
            if ($evaluated_participants) {
                foreach ($evaluated_participants as $row) {
//                    echo "<pre>";
//                    print_r($evaluated_participants);
                    $participant_ids[] = $row['User']['id'];
                }
            }
            ?>
            <div class="virtual_header video_data_cls">
                <table>
                    <tr>
                        <td class="huddle-rows-td left_br">
                            <table>
                                <tr>
                                    <td><b> <?=$language_based_content['Assessments'];?> </b> </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b><?=$language_based_content['Assessed_Participants'];?></b>
                                    </td>
                                </tr>

                                <?php $cnt = 0 ?>
                                <?php
                                foreach ($coach['Coachees'] as $coachees):
                                    if (isset($participant_ids) && is_array($participant_ids) && !in_array($coachees['User']['id'], $participant_ids)) {
                                        continue;
                                    }
                                    ?>
                                    <tr>
                                        <td>
                                            <div><?php echo $coachees['User']['first_name'] . ' ' . $coachees['User']['last_name']; ?></div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>

                        </td>
                        <?php
                        if ($all_huddles) {
                            foreach ($all_huddles as $row_h):
                                ?>
                                <td class="huddle-rows-td">
                                    <table>
                                        <tr>
                                            <td> <b><?php echo $row_h['AccountFolder']['name'] ?></b></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <?php if ($coachees) { ?>
                                            <?php foreach ($coach['Coachees'] as $coachees): ?>
                                                <?php
                                                if (isset($participant_ids) && is_array($participant_ids) && !in_array($coachees['User']['id'], $participant_ids)) {
                                                    continue;
                                                }
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php
                                                        $class = '';
                                                        if (count($coachees['Videos']) >= 11) {
                                                            $class = 'scrool-cl';
                                                        } else {
                                                            $class = '';
                                                        }
                                                        foreach ($coachees['Videos'] as $videos):
                                                            if ($videos['afd']['account_folder_id'] != $row_h['AccountFolder']['account_folder_id']) {
                                                                continue;
                                                            }
                                                            $phpdate = strtotime($videos['Documents']['recorded_date']);
                                                            //     $modified_date = strtotime('+' . $tracking_duration . ' hours', $phpdate);
                                                            $modified_date = $phpdate;
                                                            $new_date = strtotime($this->Custom->get_submission_date($row_h['AccountFolder']['account_folder_id']));
                                                            //   $new_date = strtotime('Now');
                                                            $mysqldate = date('d-M', $phpdate);
                                                            if (is_array($videos['Comments']) && count($videos['Comments']) > 0) {
                                                                $cmntdate = strtotime($videos['Comments'][0]['Comment']['created_date']);
                                                                if ($modified_date <= $new_date) {
                                                                    $cls = 'dark_green_class';
                                                                    $c = 1;
                                                                } else {
                                                                    $cls = 'red_legend_bg';
                                                                    $c = 2;
                                                                }
                                                            } else {
                                                                if ($modified_date >= $new_date) {
                                                                    $cls = 'red_legend_bg';
                                                                    $c = 3;
                                                                } else {
                                                                    $cls = 'dark_green_class';
                                                                    $c = '';
                                                                }
                                                            }


                                                            $new_date_2 = strtotime('+' . $tracking_duration . ' hours', $new_date);
                                                            $comment_details = $this->Custom->get_video_comments($videos['Documents']['id']);
                                                            $class1 = '';
                                                            if (is_array($comment_details) && count($comment_details) > 0) {
                                                                $cmntdate = strtotime($comment_details[0]['Comment']['created_date']);

                                                                if ($cmntdate <= $new_date_2) {
                                                                    $class1 = 'light_green_class';
                                                                    $c = 1;
                                                                } else {
                                                                    $class1 = 'orange_class';
                                                                    $c = 2;
                                                                }
                                                            }


//                                                            if (is_array($videos['Feedback']) && count($videos['Feedback']) > 0) {
//                                                                if ($c == 1) {
//                                                                    $cls = 'video_sub_box_ontime';
//                                                                } elseif ($c == 2) {
//                                                                    $cls = 'video_sub_box_late';
//                                                                } elseif ($c == 3) {
//                                                                    $cls = 'video_sub_box_no';
//                                                                } else {
//                                                                    $cls = 'video_sub_box_ontime';
//                                                                }
//                                                                $c = 4;
//                                                            }
//                                                            echo "Folder id afd " . $videos['afd']['account_folder_id'] . "<br/>";
//                                                            echo "Folder id " . $row_h['AccountFolder']['account_folder_id'];
                                                            ?>
                                                            <div class="video_sub_box <?php echo $class; ?>">
                                                                <div class="<?php echo $cls; ?>">
                                                                    <span class="<?php echo $class1; ?>"></span>
                                                                    <a class="outer-tooltip"  href="<?php echo $this->base . '/dashboard/assessment_detail/' . $videos['Documents']['id'] . '/' . $coach['User']['id'] . '/' . $coachees['User']['id'] . '/0/' . $c; ?>"  data-toggle="modal" data-target="#assessment_modal" data-remote="false" style="font-weight: 600;position: relative;"><div class="inner-tooltp"><?php echo $mysqldate; ?></div></a>
                                                                </div>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php } ?>
                                    </table>
                                </td>
                            <?php endforeach; ?>
                        <?php } ?>
                    </tr>
                </table>

                <div class="clear"></div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<div id="assessment_modal" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width: 810px;">
        <div class="modal-content">
        </div>
    </div>
</div>
<script>
    $("#assessment_modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $(document).on("click", ".remove_tracker_button", function () {
        var r = confirm('<?php echo $language_based_content['are_you_sure_want_to_del_this']; ?>');
        if (r === true)
        {
            $('#remove_tracking_form').submit();
        }
    });
</script>

<style type="text/css">

    .virtual_data_Right{ overflow: hidden;}
    .videos_box_container{    overflow: auto;overflow-y: hidden;}
    .video_sub_box.scrool-cl > div{padding: 0 8px;}
    .video_data_cls{ overflow:auto;}
    .video_data_cls td{    
                           text-overflow: ellipsis;
                           white-space: nowrap;     padding: 0px 5px;
                           height: 24px;       vertical-align: middle;  }
    .video_data_cls td.huddle-rows-td{border-left: 1px solid #ddd;
                                      border-right: 1px solid #ddd; width: 150px; padding-left: 0;
                                      padding-right: 0;}
    .huddle-rows-td b{     width: 150px;
                           display: block;
                           overflow: hidden;
                           text-overflow: ellipsis;
                           white-space: nowrap;     font-size: 12px;
                           text-align: center;
                           padding: 0 5px;}
    .video_data_cls .left_br b{ padding: 0; text-align: left;}
    .video_data_cls td .coachee_box{}
    .video_data_cls .video_sub_box  { display: table-cell;}
    .video_data_cls .green_legend_bg{    margin: 0 auto;}
    .video_data_cls table{    width: auto;}

    .video_sub_box_late {
        position: relative;
        z-index: 9;
        background: #ccf2fd !important;
        width: 500px;
        height: 200px;
    }
    .video_sub_box_late:before {
        content: "";
        position: absolute;
        z-index: -1;  top: 0;
        right: 50%;
        bottom: 0;
        left: 0;
        background: #fffaa5 !important;
    }
    .video_sub_box_ontime {
        position: relative;
        z-index: 9;
        background: #6ab752 !important;
        width: 500px;
        height: 200px;
    }
    .video_sub_box_ontime:before {
        content: "";
        position: absolute;
        z-index: -1;  top: 0;
        right: 50%;
        bottom: 0;
        left: 0;
        background: #b3eaa2 !important;
    }
    .video_sub_box_no {
        position: relative;
        z-index: 9;
        background: #b3eaa2 !important;
        width: 500px;
        height: 200px;
    }
    .video_sub_box_no:before {
        content: "";
        position: absolute;
        z-index: -1;  top: 0;
        right: 50%;
        bottom: 0;
        left: 0;
        background: #fcd2bb !important;
    }
    .dark_green_class{
        width: 70px;
        height: 37px;
        padding: 8px;
        box-sizing: border-box;
        white-space: normal;
        border: solid 1px #dddddd;
        border-bottom: 0px;
        border-left: 0px;
        text-align: center;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        font-size: 13px;
        color: #696969;
        background: #6ab752 !important;
        position:relative;
    }

    .light_green_class{
        position: absolute;
        background: #b3eaa2;
        width: 50%;
        top: 0px;
        bottom: 0px;
        right: 0px;
    }

    .orange_class{
        position: absolute;
        background: #f7dfa7;
        width: 50%;
        top: 0px;
        bottom: 0px;
        right: 0px;
    }
    .red_legend_bg{

        position: relative;
    }
.outer-tooltip { position: relative;     display: block;
    height: 15px;}
.inner-tooltp {       top: -21px;
    background: #000;
    padding: 0px 2px 2px 2px;
    width: 49px;
    color: #fff;
    border-radius: 4px;
    line-height: 16px;
    position: absolute;
    font-size: 10px;
    z-index: 9;
    left: -9px; display: none;
}
.outer-tooltip:hover .inner-tooltp { display: block;}

.inner-tooltp:before{    pointer-events: none;
    position: absolute;
    content: '';
    width: 0;
    height: 0;
    border-left: 6px solid transparent;
    border-right: 6px solid transparent;
    border-top: 6px solid #000;
    top: 17px;
    left: 20px; }
.video_sub_box > div{       width: 36px;
    height: 16px;
    padding: 0px;}


</style>

<script>
     $(document).ready(function (e) {
         
                        var bread_crumb_data =  '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><span href="#"><?php echo $breadcrumb_language_based_content['assessment_tracker_breadcrumb']; ?></span></div>';

                        $('.breadCrum').html(bread_crumb_data);
         
                        });
</script>

