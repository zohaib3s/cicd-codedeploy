<?php
$user = $this->Session->read('Auth');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
?>

<style>
    #chartdiv {
        width    : 100%;
        height    : 500px;
    }
    .ui-widget-content a{
        text-decoration: underline;
    }
    .ui-widget-content a:hover{
        text-decoration: underline;
    }
    .ui-state-highlight a, .ui-widget-content .ui-state-highlight a, .ui-widget-header .ui-state-highlight a{
        text-decoration: underline;
    }
</style>

<style>
    td{
        padding: 10px 10px;
    }
    th.ui-th-column div{
        white-space:normal !important;
        height:auto !important;
        padding:2px;
    }
    .ui-jqgrid .ui-jqgrid-resize {height:100% !important;}
</style>

<style>
    .multiselect {
        width: 200px;
    }
    .selectBox {
        position: relative;
    }
    .selectBox select {
        width: 100%;
        font-weight: bold;
    }
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    #checkboxes {
        display: none;
        border: 1px #dadada solid;
    }
    #checkboxes label {
        display: block;
    }
    #checkboxes label:hover {
        background-color: #1e90ff;
    }
    .filter_dropDown{
        position: absolute;
        background: #fff;
        padding: 8px;
        z-index: 10;
        width: 100%;
        border-radius: 3px;
    }
    .filter_dropDown label{
        padding: 5px;
        margin-bottom: 0px;
    }
    .filter_dropDown label:hover{
        color:#fff;
    }
</style>

<script>

    $(document).ready(function () {
        $('.selectBox').click(function (e) {
            $('.filter_dropDown').toggle();
            e.stopPropagation();
        });

        $(document.body).click(function () {
            $('.filter_dropDown').hide();
        });

        $('.filter_dropDown').click(function (e) {
            e.stopPropagation();
        });
    });

</script>

<?php
$result_array = array(
    '12'=>'Accessed a Huddle',
    '5'=>'Huddle Video Comments',
    '5'=>'Workspace Video Notes',
    '3'=>'Resources Uploaded',
    '13'=>'Resources Viewed',
    '1'=>'Huddles Created',
    '9'=>'Logged In',
    '8'=>'Reply to Comments',
    '15'=>'Standards Added',
    '14'=>'Tags Added',
    '4'=>'Videos Uploaded to Huddles',
    '4'=>'Videos Uploaded to Workspace',
    '11'=>'Huddles Videos Viewed',
    '11'=>'Workspace Videos Viewed',
    '22'=>'Videos Shared to Huddles',
    '23'=>'Scripted Observations',
    '20'=>'Scripted Video Observations',
    '24'=>'Total Video Hours Uploaded',
    '25'=>'Total Video Hours Viewed'
);
$result_array_keys = array(12,5,5,3,13,1, 9, 8,15,14,4,4, 11, 11, 22,23,20,24,25);
$selected_rows = array(
    '12'=> $access_huddle,
    '5'=> $huddles_comment_uploaded,
    '5'=> $workspace_comment_uploaded,
    '3'=> $document_uploaded,
    '13'=> $document_viewed,
    '1'=> $huddle_created,
    '9'=> $logged_in,
    '8'=> $reply_comment,
    '15'=> $standard_added,
    '14'=> $tag_added,
    '4'=> $huddles_video_uploaded,
    '4'=> $workspace_video_uploaded,
    '11'=> $huddles_video_viewed,
    '11'=> $workspace_video_viewed,
    '22'=> $huddles_video_shared,
    '23'=> $scripted_observations,
    '20'=> $scripted_video_observations,
    '24'=> $total_video_hours_uploaded,
    '25'=> $total_video_hours_viewed,
);
$title =false;
foreach($selected_rows as $key=>$val){
    if($val){
        if(is_array($result_array) && in_array($key,$result_array_keys)){
            $title[]= $result_array[$key];
        }
    }

}
$selected_titles = implode(',  ',$title);
?>
<form method="post" action="<?php echo $this->base . "/Dashboard/excelExport"; ?>">
    <input type="hidden" name="csvBuffer" id="csvBuffer" value="" />
    <input type="hidden" name="acctName" id="acctName" value="" />
</form>
<div class="counterBg analyticsdiv box" style="width: 1024px; padding: 10px 0px 0px 0px;">

    <section style="margin-top:-9px;" class="company_detail">
        <h1>Analytics</h1>
        <!--<h2><?php //echo $user_current_account['accounts']['company_name'];     ?></h2>-->
    </section>
    <a href="<?php echo $this->base . '/analytics' ?>" style="cursor:pointer;text-decoration:underline;color:#6588a6;font:14px 'Segoe UI';padding:0 0 0 17px;margin:0 0 25px 0;">Back to Analytics</a>
    <br>
    <div class=" clrcls1"></div>
    <div class="container analyticsdiv-inner">
        <section class="clearfix account_basic">
            <h1><?php echo $user_name; ?>'s Activity</h1>
            <form method="post" id="user_detail_form" action="<?php echo $this->base . "/Dashboard/userDetail/$user_id/$account_id"; ?>" accept-charset="UTF-8">
                <div class="filter_box clearfix">
                    <div class="accountselect1 filter_detail_box frmCls">
                        <label>Start Date</label>
                        <input type="text" name="startDate" id="fromTbl" style="width:132px;" />
                    </div>
                    <div class="accountselect1 filter_detail_box toCls">
                        <label>End Date</label>
                        <input type="text" name="endDate" id="toTbl" style="width:132px;" />
                    </div>


                    <div class="accountselect1 filter_detail_box">
                        <label>Filters</label>

                        <div class="multiselect">
                            <div class="selectBox">
                                <select style=" box-shadow: 0 1px 2px rgba(0,0,0,0.09) inset;">
                                    <option>Select an option</option>
                                </select>
                                <div class="overSelect"></div>
                            </div>
                            <div id="checkboxes" class="filter_dropDown" style="max-height: 400px; overflow: auto;">
                                <label for="access_huddle"><input type="checkbox" name="access_huddle" id="access_huddle" value="12" <?php if (isset($access_huddle)) echo 'checked=checked'; ?>> &nbsp;Accessed a Huddle</label>
                    <!--            <label for="comment_uploaded"><input type="checkbox" name="comment_uploaded" id="comment_uploaded" value="5" <?php //if(isset($comment_uploaded)) echo 'checked=checked';     ?>> &nbsp;Video Comments Added</label>-->
                                <label for="huddles_comment_uploaded"><input type="checkbox" name="huddles_comment_uploaded" id="huddles_comment_uploaded" value="5" <?php if (isset($huddles_comment_uploaded)) echo 'checked=checked'; ?>> &nbsp;Huddle Video Comments</label>
                                <label for="workspace_comment_uploaded"><input type="checkbox" name="workspace_comment_uploaded" id="workspace_comment_uploaded" value="5" <?php if (isset($workspace_comment_uploaded)) echo 'checked=checked'; ?>> &nbsp;Workspace Video Notes</label>
                                <label for="document_uploaded"><input type="checkbox" name="document_uploaded" id="document_uploaded" value="3" <?php if (isset($document_uploaded)) echo 'checked=checked'; ?>> &nbsp;Resources Uploaded</label>
                                <label for="document_viewed"><input type="checkbox" name="document_viewed" id="document_viewed" value="13" <?php if (isset($document_viewed)) echo 'checked=checked'; ?>> &nbsp;Resources Viewed</label>
                                <label for="huddle_created"><input type="checkbox" name="huddle_created" value="1" id="huddle_created" <?php if (isset($huddle_created)) echo 'checked=checked'; ?>>&nbsp;Huddles Created</label>
                                <label for="logged_in"><input type="checkbox" name="logged_in" id="logged_in" value="9" <?php if (isset($logged_in)) echo 'checked=checked'; ?>> &nbsp;Logged In</label>
                                <label for="reply_comment"><input type="checkbox" name="reply_comment" id="reply_comment" value="8" <?php if (isset($reply_comment)) echo 'checked=checked'; ?>> &nbsp;Reply to Comments</label>
                                <label for="standard_added"><input type="checkbox" name="standard_added" id="standard_added" value="15" <?php if (isset($standard_added)) echo 'checked=checked'; ?>> &nbsp;Standards Added</label>
                                <label for="tag_added"><input type="checkbox" name="tag_added" id="tag_added" value="14" <?php if (isset($tag_added)) echo 'checked=checked'; ?>>&nbsp;Tags Added</label>
                    <!--            <label for="video_uploaded"><input type="checkbox" name="video_uploaded" id="video_uploaded" value="4" <?php //if(isset($video_uploaded)) echo 'checked=checked';     ?>>&nbsp;Videos Uploaded</label>-->
                                <label for="huddles_video_uploaded"><input type="checkbox" name="huddles_video_uploaded" id="huddles_video_uploaded" value="4" <?php if (isset($huddles_video_uploaded)) echo 'checked=checked'; ?>>&nbsp;Videos Uploaded to Huddles</label>
                                <?php if ($this->Custom->get_account_video_permissions($account_id)): ?>
                                    <label for="library_video_uploaded"><input type="checkbox" name="library_video_uploaded" id="library_video_uploaded" value="4" <?php if (isset($library_video_uploaded)) echo 'checked=checked'; ?>>&nbsp;Videos Uploaded to Library</label>
                                <?php endif; ?>
                                <label for="workspace_video_uploaded"><input type="checkbox" name="workspace_video_uploaded" id="workspace_video_uploaded" value="4" <?php if (isset($workspace_video_uploaded)) echo 'checked=checked'; ?>>&nbsp;Videos Uploaded to Workspace</label>
                 <!--            <label for="video_viewed"><input type="checkbox" name="video_viewed" id="video_viewed" value="11" <?php //if(isset($video_viewed)) echo 'checked=checked';     ?>>&nbsp;Videos Viewed</label>-->
                                <label for="huddles_video_viewed"><input type="checkbox" name="huddles_video_viewed" id="huddles_video_viewed" value="11" <?php if (isset($huddles_video_viewed)) echo 'checked=checked'; ?>>&nbsp;Huddles Videos Viewed</label>
                                <?php if ($this->Custom->get_account_video_permissions($account_id)): ?>
                                    <label for="library_video_viewed"><input type="checkbox" name="library_video_viewed" id="library_video_viewed" value="11" <?php if (isset($library_video_viewed)) echo 'checked=checked'; ?>>&nbsp;Library Videos Viewed</label>
                                <?php endif; ?>
                                <label for="workspace_video_viewed"><input type="checkbox" name="workspace_video_viewed" id="workspace_video_viewed" value="11" <?php if (isset($workspace_video_viewed)) echo 'checked=checked'; ?>>&nbsp;Workspace Videos Viewed</label>
                                <label for="huddles_video_shared"><input type="checkbox" name="huddles_video_shared" id="huddles_video_shared" value="22" <?php if (isset($huddles_video_shared)) echo 'checked=checked'; ?>>&nbsp;Videos Shared to Huddles</label>
                                <?php if ($this->Custom->get_account_video_permissions($account_id)): ?>
                                    <label for="library_video_shared"><input type="checkbox" name="library_video_shared" id="library_video_shared" value="22" <?php if (isset($library_video_shared)) echo 'checked=checked'; ?>>&nbsp;Videos Shared to Library</label>
                                <?php endif; ?>
                                <label for="scripted_observations"><input type="checkbox" name="scripted_observations" id="scripted_observations" value="23" <?php if (isset($scripted_observations)) echo 'checked=checked'; ?>>&nbsp;Scripted Observations</label>
                                <label for="scripted_video_observations"><input type="checkbox" name="scripted_video_observations" id="scripted_video_observations" value="20" <?php if (isset($scripted_video_observations)) echo 'checked=checked'; ?>>&nbsp;Scripted Video Observations</label>
                                <label for="total_video_hours_uploaded"><input type="checkbox" name="total_video_hours_uploaded" id="total_video_hours_uploaded" value="24" <?php if (isset($total_video_hours_uploaded)) echo 'checked=checked'; ?>>&nbsp;Total Video Hours Uploaded</label>
                                <label for="total_video_hours_viewed"><input type="checkbox" name="total_video_hours_viewed" id="total_video_hours_viewed" value="25" <?php if (isset($total_video_hours_viewed)) echo 'checked=checked'; ?>>&nbsp;Total Video Hours Viewed</label>
                            </div>
                        </div>

                    </div>
                    <?php if($title):?>
                    <div>
                        <div>Selected Filters</div>
                        <div style="width:447px; font-weight: bold;float: left;background: #fff;padding: 7px;border: solid 1px #fff;margin-top: 5px;border-radius: 3px;color: #555555;"><?php echo $selected_titles;?></div>
                    </div>
                    <?php endif;?>

                </div>


                <input type="submit" class="btn btn-green" value="Search" style="margin-top: 10px;">

            </form>
            <div class="data_grid_green data_grid_adjs gra-div" style="margin-top: 15px;height:537px;">
                <table id="grid2"></table>
                <div id="pager2"></div>

            </div> <!--- ga -->
    </div>
</section>
</div>
<script type="text/javascript">
    $(function () {
        var sDate = new Date('<?php echo $st_date; ?>');
        var eDate = new Date('<?php echo $en_date; ?>');
        $('#fromTbl').val($.datepicker.formatDate('mm/dd/yy', sDate));
        $('#toTbl').val($.datepicker.formatDate('mm/dd/yy', eDate));

        $("#toTbl").datepicker({
            changeMonth: true,
            changeYear: true,
            showOn: "button",
            buttonImage: '<?php echo $this->webroot . "img/icons/calendar_icon.png"; ?>',
            yearRange: '2012:2026'
        });
        $('.toCls img').click(function () {
            $('.filter_dropDown').hide();
        });
        $("#fromTbl").datepicker({
            changeMonth: true,
            changeYear: true,
            showOn: "button",
            buttonImage: '<?php echo $this->webroot . "img/icons/calendar_icon.png"; ?>',
            onClose: function (selectedDate) {
                $("#toTbl").datepicker("option", "minDate", selectedDate);
            },
            yearRange: '2012:2026'
        });
        $('.frmCls img').click(function () {
            $('.filter_dropDown').hide();
        });
    });

    $(function () {

        var data = <?php echo $user_detail; ?>,
                $grid = $("#grid2");
        var library_check = <?php echo (!empty($this->Custom->get_account_video_permissions($account_id))) ? $this->Custom->get_account_video_permissions($account_id) : 0; ?>;

        if (library_check)
        {
            $grid.jqGrid({
                data: data,
                datatype: "local",
                colNames: ['Action', 'Name', 'Action Date', 'Workspace', 'Huddle', 'Library', 'Detail', 'Video Title', 'Coach', 'Coachee', 'Session Date', 'Minutes Uploaded/Viewed'],
                colModel: [
                    {name: "action_type", width: 130, search: false},
                    {name: "username", width: 160, search: false},
                    {name: "date_added", width: 140, search: false},
                    {name: "workspace", width: 130, search: false},
                    {name: "desc", width: 130, search: false},
                    {name: "library", width: 130, search: false},
                    /*{ name: "url", width: 130, search:false,formatter: function (cellvalue, options, rowObject) {
                     if(rowObject.action_type == 'Logged In' || rowObject.action_type == 'Video Viewed' || rowObject.action_type == 'Document Viewed' || rowObject.action_type == 'Tag Added' || rowObject.action_type == 'Standard Added')
                     return rowObject.url;
                     else
                     return '<a id=url_'+options.rowId+' href=javascript:paramLink("' + rowObject.url + '")>' + cellvalue + "</a>";
                     }},*/
                    {name: "url", width: 130, search: false},
                    {name: "video", width: 130, search: false},
                    {name: "coach", width: 130, search: false},
                    {name: "coachee", width: 130, search: false},
                    {name: "session_date", width: 130, search: false},
                    {name: "minutes", width: 130, search: false}
                ],
                pager: '#pager2',
                rowNum: 20,
                rowList: [10, 20, 30, 50, 100],
                viewrecords: true,
                shrinkToFit: false,
                //rownumbers: true,
                ignoreCase: true,
                caption: "Activity",
                height: 391,
                width: 960,
                sortname: "date_added",
                sortorder: 'desc',
                gridview: true,
            });

        }

        else
        {
            $grid.jqGrid({
                data: data,
                datatype: "local",
                colNames: ['Action', 'Name', 'Action Date', 'Workspace', 'Huddle', 'Detail', 'Video Title', 'Coach', 'Coachee', 'Session Date', 'Minutes Uploaded/Viewed'],
                colModel: [
                    {name: "action_type", width: 130, search: false},
                    {name: "username", width: 160, search: false},
                    {name: "date_added", width: 140, search: false},
                    {name: "workspace", width: 130, search: false},
                    {name: "desc", width: 130, search: false},
                    /*{ name: "url", width: 130, search:false,formatter: function (cellvalue, options, rowObject) {
                     if(rowObject.action_type == 'Logged In' || rowObject.action_type == 'Video Viewed' || rowObject.action_type == 'Document Viewed' || rowObject.action_type == 'Tag Added' || rowObject.action_type == 'Standard Added')
                     return rowObject.url;
                     else
                     return '<a id=url_'+options.rowId+' href=javascript:paramLink("' + rowObject.url + '")>' + cellvalue + "</a>";
                     }},*/
                    {name: "url", width: 130, search: false},
                    {name: "video", width: 130, search: false},
                    {name: "coach", width: 130, search: false},
                    {name: "coachee", width: 130, search: false},
                    {name: "session_date", width: 130, search: false},
                    {name: "minutes", width: 130, search: false}
                ],
                pager: '#pager2',
                rowNum: 20,
                rowList: [10, 20, 30, 50, 100],
                viewrecords: true,
                shrinkToFit: false,
                //rownumbers: true,
                ignoreCase: true,
                caption: "Activity",
                height: 391,
                width: 960,
                sortname: "date_added",
                sortorder: 'desc',
                gridview: true,
            });
        }

        $grid.jqGrid('filterToolbar', {});
        $grid.jqGrid('navGrid', '#pager2', {refresh: false, search: false, view: false, del: false, add: false, edit: false, excel: true})
                .navButtonAdd('#pager2', {
                    caption: "&nbsp;<img src='<?php echo $this->webroot . "img/icons/excel_icon.png"; ?>' /> Export to Excel",
                    buttonicon: "ui-icon-save",
                    onClickButton: function () {
                        exportExcel();
                    },
                    position: "last"
                });
    });

    function exportExcel() {
        var library_check = <?php echo (!empty($this->Custom->get_account_video_permissions($account_id))) ? $this->Custom->get_account_video_permissions($account_id) : 0; ?>;
        var mya = new Array();
        mya = $("#grid2").getDataIDs();  // Get All IDs
        var myData = $("#grid2").jqGrid('getGridParam', 'data');
        var data = $("#grid2").getRowData(mya[0]);     // Get First row to get the labels
        var colNames = new Array();
        var ii = 0;
        for (var i in data) {
            colNames[ii++] = i;
        }    // capture col names
        var html = "";
        for (k = 0; k < 1; k++)
        {
            //html=html+colNames[k]+"\t";     // output each Column as tab delimited
            html = html + 'Action' + '|' + "\t";
            html = html + 'Name' + '|' + "\t";
            html = html + 'Action Date' + '|' + "\t";
            html = html + 'Workspace' + '|' + "\t";
            html = html + 'Huddle' + '|' + "\t";
            if (library_check)
            {
                html = html + 'Library' + '|' + "\t";
            }
            html = html + 'Detail' + '|' + "\t";
            html = html + 'Video Title' + '|' + "\t";
            html = html + 'Coach' + '|' + "\t";
            html = html + 'Coachee' + '|' + "\t";
            html = html + 'Session Date' + '|' + "\t";
            html = html + 'Minutes Uploaded/Viewed' + '|' + "\t";
        }
        html = html + "\t\r";
        for (var i = 0; i < myData.length; i++) {
            html = html + $.trim(myData[i].action_type) + '|' + "\t";
            html = html + $.trim(myData[i].username) + '|' + "\t";
            html = html + $.trim(myData[i].date_added) + '|' + "\t";
            html = html + $.trim(myData[i].workspace) + '|' + "\t";
            html = html + $.trim(myData[i].desc) + '|' + "\t";
            if (library_check)
            {
                html = html + $.trim(myData[i].library) + '|' + "\t";
            }
            html = html + $.trim(myData[i].url) + '|' + "\t";
            html = html + $.trim(myData[i].video) + '|' + "\t";
            html = html + $.trim(myData[i].coach) + '|' + "\t";
            html = html + $.trim(myData[i].coachee) + '|' + "\t";

            html = html + $.trim(myData[i].session_date) + '|' + "\t";
            html = html + $.trim(myData[i].minutes) + '|' + "\t\r";
        }

        html = html + "\t\r";  // end of line at the end
        document.forms[0].csvBuffer.value = html;
        var acc_name = myData[0].username;
        document.forms[0].acctName.value = acc_name;
        document.forms[0].method = 'POST';
        if (library_check)
        {
            document.forms[0].action = '<?php echo $this->base . "/Dashboard/excelExport/12/1"; ?>';  // send it to server which will open this contents in excel file
        }
        else
        {
            document.forms[0].action = '<?php echo $this->base . "/Dashboard/excelExport/11/1"; ?>';
        }
        //document.forms[0].target='_blank';
        document.forms[0].submit();
    }

    function paramLink(url) {
        var l = '<?php echo $this->base; ?>';
        var link = l + url;
        window.open(link);
    }

    $(window).click(function () {
//Hide the menus if visible
        var checkboxes = document.getElementById("checkboxes");
        checkboxes.style.display = "none";
    });

    $('.multiselect').click(function (event) {
        event.stopPropagation();
        var checkboxes = document.getElementById("checkboxes");
        checkboxes.style.display = "block";

    });

</script>
<style>
    .desk-cta.desk-sidetab{
        display: none !important;
    }
</style>