<?php
$user = $this->Session->read('user_current_account');
$account_id = $user['accounts']['account_id'];
?>
<style>
    .modal-content form {
        padding: 0px;
    }

    .amcharts-chart-div {
        top: 14px;
        margin-top: 19px;
    }

    .analytics_new {
        padding-bottom: 0;
    }

    .amcharts-export-menu-top-right {
        top: -13px;
    }
</style>
    <div class="header" style="padding: 10px 10px 0px 15px;margin-bottom: 10px;">
        <h4 class="header-title nomargin-vertical smargin-bottom"><?=$language_based_content['video_session_detail'];?></h4>
        <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">x</a>
    </div>
<div class="video_dialog analytics_new">
    <div class="video_left">

        <?php
        $document_files_array = $this->Custom->get_document_url($document['Document']);

        $thumbnail_image_path = $document_files_array['thumbnail'];
        ?>
        <?php if ($video_detail['Document']['doc_type'] != 3): ?>
            <a target="_blank"
               href="<?php echo $this->base . '/Huddles/view/' . $video_detail['afd']['account_folder_id'] . '/1/' . $video_detail['Document']['id']; ?>">
               <?php else: ?>
                   <?php if ($video_detail['Document']['current_duration'] > 0) { ?>
                    <a target="_blank"
                       href="<?php echo $this->base . '/Huddles/observation_details_3/' . $video_detail['afd']['account_folder_id'] . '/' . $video_detail['Document']['id']; ?>">
                       <?php } else { ?>
                        <a target="_blank"
                           href="<?php echo $this->base . '/Huddles/observation_details_1/' . $video_detail['afd']['account_folder_id'] . '/' . $video_detail['Document']['id']; ?>">
                           <?php } ?>
                       <?php endif; ?>
                    <img src="<?php echo $thumbnail_image_path; ?>" width="223" height="148" alt=""/>
                    <div class="play-icon" style="top: 170px !important;left: 120px;"></div>
                </a>
                <h3><?php //echo $video_detail['Document']['original_file_name'];                           ?></h3>
                <h1 class="tracker_name"><?php echo $coachee_name['User']['first_name'] . ' ' . $coachee_name['User']['last_name']; ?></h1>
                <div class="video_upload_date"><span><?=$language_based_content['session_date'];?> </span>

                    <?php
                    $created_at = explode(' ', $video_detail['Document']['recorded_date']);
                    $created_date = explode('-', $created_at[0]);
                    echo $created_date[1] . '-' . $created_date[2] . '-' . $created_date[0] . ' ' . date('h:i A', strtotime($video_detail['Document']['recorded_date']));
                    ?>
                    <!--<br><?php if (!empty($class)): ?>
                        <?php if ($class == '1'): ?>
                                                                                                                                                                                                                                                    <div style="margin-right: -5px;" class="virtual_legend_box2"> <img src="/app/img/green_label.png" width="21" height="21" alt=""> On-Time Feedback </div>
                        <?php elseif ($class == '2'): ?>
                                                                                                                                                                                                                                                    <div  style="margin-right: -5px;" class="virtual_legend_box2"> <img src="/app/img/yellow_legend.png" width="21" height="21" alt=""> Late Feedback </div>
                        <?php elseif ($class == '3'): ?>
                                                                                                                                                                                                                                                    <div style="margin-right: -5px;" class="virtual_legend_box2"> <img src="/app/img/red_legend.png" width="21" height="21" alt=""> No Feedback </div>
                        <?php elseif ($class == '4'): ?>
                                                                                                                                                                                                                                                    <div style="margin-right: -5px;" class="virtual_legend_box2"> <img src="/app/img/blue_label.png" width="21" height="21" alt=""> Coachee Comments </div>
                        <?php endif; ?>
                    <?php endif; ?>-->
                </div>


                <form method="post" action="<?php echo $this->base; ?>/Dashboard/remove_tracking"
                      name="remove_tracking_form" id="remove_tracking_form">
                    <input type="hidden" name="document_id" value="<?php echo $video_detail['Document']['id']; ?>">
                    <input type="hidden" name="load_back" value="<?php echo $load_back; ?>">
                    <input type="hidden" name="huddle_id"
                           value="<?php echo $video_detail['afd']['account_folder_id']; ?>">
                    <input type="hidden" name="remove_from_tracking" value="remove_coaching_tracking">

                </form>
                </div>
                <form method="post" action="<?php echo $this->base; ?>/Dashboard/coaching_tracker_note">
                    <input type="hidden" name="load_back" value="<?php echo $load_back; ?>">
                    <input type="hidden" name="coach_feedback_id" value="<?php
                    if (count($coach_feedback) > 0) {
                        echo $coach_feedback['Comment']['id'];
                    }
                    ?>">
                    <input type="hidden" name="document_id" value="<?php echo $video_detail['Document']['id']; ?>">
                    <input type="hidden" name="huddle_id" value="<?php echo $video_detail['afd']['account_folder_id']; ?>">
                    <div class="video_right">
                        <div class="green_count_box">
                            <div class="green_count_box_number"><?php echo $coach_comments; ?></div>
                            <b><?=$language_based_content['coach_comments_added'];?></b>
                        </div>
                        <div class="green_count_box">
                            <div class="green_count_box_number"><?php echo $coachee_comments; ?></div>
                            <b><?=$language_based_content['coachee_comments_added'];?></b>
                        </div>
                        <div class="green_count_box">
                            <div class="green_count_box_number"><?php echo $resources; ?></div>
                            <b><?=$language_based_content['resources_attached'];?></b>
                        </div>
                        <div class="clear"></div>
                        <div class="comment_box"><?=$language_based_content['coachs_session_summary'];?></div>
                        <textarea class="dialog_text_area" name="coach_response"><?php
                            if (count($coach_feedback) > 0) {
                                echo stripcslashes($coach_feedback['Comment']['comment']);
                            }
                            ?></textarea>
                    </div>
                    <div class="clear"></div>
                    <div>
                        <div id="chartCoachVideo" style="width: 100%; height: 220px;"></div>

                    </div>
                    <hr/>
                    <div class="clear"></div>
                    <div class="btns dialog_btn_box">
                        <div class="remove_tracker_button"><a href="#"><?=$language_based_content['remove_from_tracker'];?></a></div>
                        <?php if ($load_back > 0) { ?>
                            <div class="btn" style="margin-left: 10px;border: 1px solid #ded7d7;"><a
                                    style="color: #80a0bb;font-weight: 600;" href="<?php echo $url; ?>"> <?=$language_based_content['back_to_coaching_tracker']; ?></a></div>
                        <?php } ?>
                        <div class="actions_btn_box">
                            <input class="sessionDateSave btn btn-green" type="submit" value="<?=$language_based_content['save_popup'];?>" style="background-color:<?php echo $this->Custom->get_site_settings('primary_bg_color')?>; border-color:<?php echo $this->Custom->get_site_settings('primary_border_color')?>" />
                            <input class="btn btn-white" type="reset" value="<?=$language_based_content['cancel_popup'];?>" style="top: -2px;position: relative;"
                                   onclick="$('#coach_modal').modal('hide');"/>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </form>
                </div>
                <script>

                    var coaching_ratings = '<?php echo $this->Custom->coaching_perfomance_level_without_role($account_id); ?>';

                    if (coaching_ratings == '1') {
                        var chart = AmCharts.makeChart("chartCoachVideo", {
                            "pathToImages": '<?php echo $this->webroot; ?>amcharts/images/',
                            "type": "serial",
                            "theme": "light",
                            "dataProvider": <?php echo $video_tags; ?>,
                            "valueAxes": [
                                {
                                    "id": "ValueAxis-1",
                                    "position": "left",
                                    "axisAlpha": 1
                                },
                                {
                                    "id": "ValueAxis-r",
                                    "axisAlpha": 1,
                                    "integersOnly": true,
                                    "dashLength": 1,
                                    "inside": true,
                                    "position": "right",
                                    "maximum": <?php echo count($assessment_array); ?>,
                                    "valueText": "assessment_point",
                                    "labelFunction": formatValue,
                                }


                            ],
                            "gridAboveGraphs": true,
                            "startDuration": 1,
                            "graphs": [
                                {
                                    "valueAxis": "ValueAxis-1",
                                    "colorField": "color",
                                    "fixedColumnWidth": 10,
                                    "balloonText": "[[category]]: <b>[[value]]</b> <br> Rating : [[label]]",
                                    "fillAlphas": 0.8,
                                    "id": "AmGraph-1",
                                    "lineAlpha": 0.2,
                                    "type": "column",
                                    "title": "[[tag_title]]",
                                    "valueField": "total_tags"
                                },
                                {
                                    "valueAxis": "ValueAxis-r",
                                    "colorField": "color_rating",
                                    "balloonText": "[[Assessment]]",
                                    "fixedColumnWidth": 10,
                                    "fillAlphas": 0.8,
                                    "lineAlpha": 0.2,
                                    "type": "column",
                                    "title": "red line",
                                    "valueField": "average_rating",
                                },
                            ],
                            "chartCursor": {
                                "categoryBalloonEnabled": false,
                                "cursorAlpha": 0,
                                "zoomable": false
                            },
                            "categoryField": "tag_title",
                            "export": {
                                "enabled": true,
                                "menu": [{
                                        "class": "export-main",
                                        "menu": ["PNG", "JPG", "SVG", "PDF"]
                                    }]
                            },
                            "categoryAxis": {
                                "gridPosition": "start",
                                "gridAlpha": 0,
                                "tickPosition": "start",
                                "tickLength": 20,
                                "labelFunction": function (label) {
                                    if (typeof (label) != 'undefined' && label.length > 15) {
                                        return label.substr(0, 14) + '...';
                                    } else if (typeof (label) != 'undefined' && label.length <= 15) {
                                        return label;
                                    } else {
                                        return 'None';
                                    }
                                }
                            }
                        });

                    }
                    else {

                        var chart = AmCharts.makeChart("chartCoachVideo", {
                            "pathToImages": '<?php echo $this->webroot; ?>amcharts/images/',
                            "type": "serial",
                            "theme": "light",
                            "dataProvider": <?php echo $video_tags; ?>,
                            "valueAxes": [
                                {
                                    "id": "ValueAxis-1",
                                    "position": "left",
                                    "axisAlpha": 1
                                },
                                //                            {
                                //                                "id": "ValueAxis-r",
                                //                                "axisAlpha": 1,
                                //                                "integersOnly": true,
                                //                                "dashLength": 1,
                                //                                "inside": true,
                                //                                "position": "right",
                                //                                "maximum": <?php //echo count($assessment_array);          ?>,
                                //                                "valueText": "assessment_point",
                                //                                "labelFunction": formatValue,
                                //                            }


                            ],
                            "gridAboveGraphs": true,
                            "startDuration": 1,
                            "graphs": [
                                {
                                    "valueAxis": "ValueAxis-1",
                                    "colorField": "color",
                                    "fixedColumnWidth": 10,
                                    "balloonText": "[[category]]: <b>[[value]]</b>",
                                    "fillAlphas": 0.8,
                                    "id": "AmGraph-1",
                                    "lineAlpha": 0.2,
                                    "type": "column",
                                    "title": "[[tag_title]]",
                                    "valueField": "total_tags"
                                },
                                //                            {
                                //                                "valueAxis": "ValueAxis-r",
                                //                                "colorField": "color_rating",
                                //                                "balloonText": "[[Assessment]]",
                                //                                "fixedColumnWidth": 10,
                                //                                "fillAlphas": 0.8,
                                //                                "lineAlpha": 0.2,
                                //                                "type": "column",
                                //                                "title": "red line",
                                //                                "valueField": "average_rating",
                                //                            },


                            ],
                            "chartCursor": {
                                "categoryBalloonEnabled": false,
                                "cursorAlpha": 0,
                                "zoomable": false
                            },
                            "categoryField": "tag_title",
                            "export": {
                                "enabled": true,
                                "menu": [{
                                        "class": "export-main",
                                        "menu": ["PNG", "JPG", "SVG", "PDF"]
                                    }]
                            },
                            "categoryAxis": {
                                "gridPosition": "start",
                                "gridAlpha": 0,
                                "tickPosition": "start",
                                "tickLength": 20,
                                "labelFunction": function (label) {
                                    console.log(label);
                                    if (typeof (label) != 'undefined' && label.length > 15) {
                                        return label.substr(0, 14) + '...';
                                    } else if (typeof (label) != 'undefined' && label.length <= 15) {
                                        return label;
                                    } else {
                                        return 'None';
                                    }

                                }
                            }
                        });


                    }
                    AmCharts.checkEmptyData = function (chart) {
                        if (0 == chart.dataProvider.length) {
                            // set min/max on the value axis
                            chart.valueAxes[0].minimum = 0;
                            chart.valueAxes[0].maximum = 100;
                            // add dummy data point
                            var dataPoint = {
                                dummyValue: 0
                            };
                            dataPoint[chart.categoryField] = '';
                            chart.dataProvider = [dataPoint];
                            // add label
                            chart.addLabel(0, '50%', '<?php echo $language_based_content['chart_contains_no_data']; ?>', 'center');
                            // set opacity of the chart div
                            chart.chartDiv.style.opacity = 0.5;
                            // redraw it
                            chart.validateNow();
                        }
                    }

                    AmCharts.checkEmptyData(chart);

                    function formatValue(value, formattedValue, valueAxis) {
                        var assessment_array = new Array();
<?php foreach ($assessment_array as $key => $val) { ?>
                            assessment_array.push("<?php echo $val; ?>");
<?php } ?>

                        if (value == 0) {
                            if (typeof (assessment_array[0]) === "undefined") {
                                return false;
                            } else {
                                return assessment_array[0];

                            }
                        }
                        else if (value == 1) {
                            if (typeof (assessment_array[1]) === "undefined") {
                                return false;
                            } else {
                                return assessment_array[1];

                            }
                        }
                        else if (value == 2) {
                            if (typeof (assessment_array[2]) === "undefined") {
                                return false;
                            } else {
                                return assessment_array[2];

                            }
                        }
                        else if (value == 3) {
                            if (typeof (assessment_array[3]) === "undefined") {
                                return false;
                            } else {
                                return assessment_array[3];

                            }
                        }
                        else if (value == 4) {
                            if (typeof (assessment_array[4]) === "undefined") {
                                return false;
                            } else {
                                return assessment_array[4];

                            }
                        } else if (value == 5) {
                            if (typeof (assessment_array[5]) === "undefined") {
                                return false;
                            } else {
                                return assessment_array[5];
                            }

                        } else {
                            return false;
                        }
                    }


                </script>;