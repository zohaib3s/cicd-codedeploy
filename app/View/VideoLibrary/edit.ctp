<style type="text/css">
    #txtVideoTags_tagsinput {
        border-left: 1px solid #CCC;
        border-bottom: 1px solid #CCC;
    }

</style>
<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<?php
$video = $video[0];
$amazon_base_url = Configure::read('amazon_base_url');
$videoLibraryID = $library['AccountFolder']['account_folder_id'];
$videoFilePath = pathinfo($video['Document']['url']);
$videoFileName = $videoFilePath['filename'];
?>
<?php
$tags_string = '';
if (count($tags) > 0) {


    for ($i = 0; $i < count($tags); $i++) {

        $tag = $tags[$i];
        if (!empty($tags_string))
            $tags_string .= ", ";
        $tags_string .= $tag['account_folders_meta_data']['meta_data_value'];
    }
}

function has_subject_selected($sel_subjects, $subject_id) {

    for ($i = 0; $i < count($sel_subjects); $i++) {

        $subject = $sel_subjects[$i];
        if ($subject['Subject']['id'] == $subject_id)
            return true;
    }

    return false;
}

function has_topic_selected($sel_topics, $topic_id) {

    for ($i = 0; $i < count($sel_topics); $i++) {

        $topic = $sel_topics[$i];
        if ($topic['Topic']['id'] == $topic_id)
            return true;
    }

    return false;
}
?>
<style>
    .string-text-wrap{
        width:255px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    #txtVideoTags_tagsinput{
        width: 450px !important;
    }
</style>
<div class="VideoLibrary">
    <div class="cntr" style="width: 100%;">

        <div class="top-head" style="width: 100%;">
            <h1>Edit Video Library</h1>
        </div>

        <div id="videoLibraryComponent">

            <div class="upload-detail-container">
                <form id="frmVideoLibrary" name="frmVideoLibrary">
                    <div class="inner">
                        <div class="cnt-left">
                            <div class="section">
                                <div class="upload-row">
                                    <div class="upload-row-left">
                                        <?php
                                        $document_files_array = $this->Custom->get_document_url($video['Document']);
                                        if (empty($document_files_array['url'])) {
                                            $video['Document']['published'] = 0;
                                            $document_files_array['url'] = $video['Document']['original_file_name'];
                                            $video['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                        } else {
                                            $video['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                        }
                                        $thumbnail_image_path = $document_files_array['thumbnail'];

                                        if (!empty($thumbnail_image_path)):
                                            ?>
                                            <img src="<?php echo $thumbnail_image_path; ?>" width="105px"/>
                                        <?php endif; ?>
                                    </div>
                                    <div class="upload-row-right" style="display:none;">
                                        <h2><?php echo $video['Document']['original_file_name']; ?></h2>
                                        <a href="javascript:OpenFilePicker();" class="edit-link-videolib">Click here to change the Video.</a>
                                    </div>
                                    <input id="txtUploadedFilePath" type="hidden" value="<?php echo $video['Document']['url']; ?>" />
                                    <input id="txtUploadedFileName" type="hidden" value="<?php echo $video['Document']['original_file_name']; ?>" />
                                    <input id="txtUploadedFileMimeType" type="hidden" value="<?php echo $video['Document']['content_type']; ?>" />
                                    <input id="txtUploadedFileSize" type="hidden" value="<?php echo $video['Document']['file_size']; ?>" />
                                    <input id="txtRandomNumber" type="hidden" value="<?php echo strtotime('now'); ?>" />
                                </div>
                                <div class="video-title-row row">
                                    <input type="text" name="txtVideoTitle" id="txtVideoTitle" value="<?php echo $library['AccountFolder']['name']; ?>" placeholder="<?php echo $language_based_content['video_title_vl']; ?>" required/>
                                </div>
                                <div class="video-description-row row">
                                    <textarea type="text" name="txtVideoDescription" id="txtVideoDescription" placeholder="<?php echo $language_based_content['video_description_vl']; ?>"  ><?php echo $library['AccountFolder']['desc']; ?></textarea>
                                </div>
                                <div class="video-tags-row row">
                                    <label><?php echo $language_based_content['tags_vl']; ?>:</label>
                                    <input type="text" name="txtVideoTags" id="txtVideoTags" value="<?php echo $tags_string; ?>" placeholder="" />
                                </div>
                            </div>
                        </div>
                        <div class="cnt-right">
                            <div class="cross" onclick="location.href = '<?php echo $this->base . '/VideoLibrary/view/' . $library['AccountFolder']['account_folder_id'] . '/' . $library['AccountFolder']['account_id']; ?>';">&nbsp;</div>
                            <?php
                            $subjects_title = array();
                            foreach ($subjects as $sub) {
                                if (has_subject_selected($sel_subjects, $sub['Subject']['id'])) {
                                    $subjects_title[] = $sub['Subject']['name'];
                                }
                            }
                            $subjects_title = implode(',', $subjects_title);

// for topics
                            $tops_title = array();
                            foreach ($topics as $topic) {
                                if (has_topic_selected($sel_topics, $topic['Topic']['id'])) {
                                    $tops_title[] = $topic['Topic']['name'];
                                }
                            }
                            $tops_title = implode(',', $tops_title);
                            ?>
                            <div class="section">

                                <div id="cmbSubjectsRow" class="subject-row row">
                                    <span class="label"><?php echo $language_based_content['choose_subject_vl']; ?></span>
                                    <div id="cmbSubjects" class="video-dropdown string-text-wrap"><?php echo isset($subjects_title) && $subjects_title != '' ? $subjects_title : $language_based_content['subject_name_vl'] ?></div>
                                    <div class="video-dropdown-container">
                                        <div class="wrapper">
                                            <div class="container">
                                                <?php if ($subjects): ?>
                                                    <?php foreach ($subjects as $subject): ?>

                                                        <div id="chkContainer<?php echo $subject['Subject']['id'] ?>" class="check-container">
                                                            <input title-data="<?php echo $subject['Subject']['name'] ?>" id="subject_ids<?php echo $subject['Subject']['id'] ?>" name="subject_ids<?php echo $subject['Subject']['id'] ?>" <?php echo (has_subject_selected($sel_subjects, $subject['Subject']['id']) ? "checked='checked'" : "") ?> class="checkbox-3 subject_checkbox subject-checks" type="checkbox" value="<?php echo $subject['Subject']['id'] ?>">

                                                            <label title-data-lbl="<?php echo $subject['Subject']['name'] ?>" for="subject_ids<?php echo $subject['Subject']['id'] ?>" class="title subject-checks-lbl"><?php echo $subject['Subject']['name'] ?></label>
                                                            <input type="text" id="txtsubject_ids<?php echo $subject['Subject']['id'] ?>" class="edit-field" value="<?php echo $subject['Subject']['name'] ?>"/>
                                                            <div class="controls controls<?php echo $subject['Subject']['name'] ?>">
                                                                <?php
                                                                $imgedit_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/playlist8x8.png');
                                                                $imgdel_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/delete-icon8x9.png');
                                                                $imgsave_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/save8x8.png');
                                                                $imgundo_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/undo8x8.png');
                                                                ?>
                                                                <?php echo $this->Html->image($imgedit_path, array('id' => "imgEdit" . $subject['Subject']['id'], 'class' => 'edit-controls edit-controls' . $subject['Subject']['id'])); ?>
                                                                <?php echo $this->Html->image($imgdel_path, array('id' => "imgDelete" . $subject['Subject']['id'], 'class' => 'edit-controls edit-controls' . $subject['Subject']['id'])); ?>
                                                                <?php echo $this->Html->image($imgsave_path, array('id' => "imgSave" . $subject['Subject']['id'], 'class' => 'save-controls save-controls' . $subject['Subject']['id'])); ?>
                                                                <?php echo $this->Html->image($imgundo_path, array('id' => "imgUndo" . $subject['Subject']['id'], 'class' => 'save-controls save-controls' . $subject['Subject']['id'])); ?>
                                                            </div>

                                                            <script type="text/javascript">

                                                                $(document).ready(function () {

                                                                    $('.save-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                                                                    $('#imgEdit<?php echo $subject['Subject']['id'] ?>').click(function () {

                                                                        $('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').css('display', 'none');
                                                                        $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').css('display', 'block');

                                                                        $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').val($('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').html());

                                                                        $('.save-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'block');

                                                                        $('.edit-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'none');
                                                                    });

                                                                    $('#imgUndo<?php echo $subject['Subject']['id'] ?>').click(function () {

                                                                        $('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').css('display', 'block');
                                                                        $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                                                                        $('.save-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                                                                        $('.edit-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'block');
                                                                    });

                                                                    $('#imgSave<?php echo $subject['Subject']['id'] ?>').click(function () {

                                                                        var subject_id = $(this).attr('id').replace('imgSave', '');

                                                                        $.ajax({
                                                                            type: 'POST',
                                                                            data: {
                                                                                subject: $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').val()
                                                                            },
                                                                            url: home_url + '/VideoLibrary/updateSubject/' + subject_id,
                                                                            success: function (response) {

                                                                                $('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').html($('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').val());

                                                                                $('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').css('display', 'block');
                                                                                $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                                                                                $('.save-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                                                                                $('.edit-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'block');

                                                                            },
                                                                            errors: function (response) {
                                                                                alert(response.contents);
                                                                            }

                                                                        });

                                                                    });

                                                                    $('#imgDelete<?php echo $subject['Subject']['id'] ?>').click(function () {

                                                                        var subject_id = $(this).attr('id').replace('imgDelete', '');

                                                                        $.ajax({
                                                                            type: 'POST',
                                                                            data: {
                                                                                subject: $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').val()
                                                                            },
                                                                            url: home_url + '/VideoLibrary/deleteSubject/' + subject_id,
                                                                            success: function (response) {

                                                                                $('#chkContainer<?php echo $subject['Subject']['id'] ?>').remove();

                                                                            },
                                                                            errors: function (response) {
                                                                                alert(response.contents);
                                                                            }

                                                                        });

                                                                    });

                                                                });

                                                            </script>

                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <input type="hidden" name="selected_subjects" id="selected_subjects" value=""/>
                                        <div class="video-dropdown-container-bottom">
                                            <input type="button" style='background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>' class="btnDoneVideoLibrary_1 btn btn-green" value="<?php echo $language_based_content['done_vl'] ?>"/>
                                            <input type="button" class="btnCancelVideoLibrary btn btn-grey" value="<?php echo $language_based_content['cancel_vl'] ?>"/>
                                            <div class="create-area">

                                                <input type="text" style="width: 200px;" id="txtNewSubject" name="new_subject" placeholder="<?php echo $language_based_content['subject_name_vl']; ?>.." />
                                                <a href="javascript:doNewSubject();" class="btn-creat"><?php echo $language_based_content['create_vl'] ?></a>

                                                <script type="text/javascript">

                                                    $('#txtNewSubject').bind('keypress', function (e) {
                                                        var code = (e.keyCode ? e.keyCode : e.which);
                                                        if (code == 13) { //Enter keycode
                                                            doNewSubject();
                                                            return false;
                                                        }
                                                    });

                                                    function doNewSubject() {

                                                        if ($("#txtNewSubject").val() == '') {
                                                            $("#txtNewSubject").addClass('error');
                                                        } else {
                                                            $("#txtNewSubject").removeClass('error');
                                                            $.ajax({
                                                                url: '<?php echo $this->base . "/VideoLibrary/addSubject" ?>',
                                                                method: 'POST',
                                                                data: {
                                                                    subject: $("#txtNewSubject").val()
                                                                },
                                                                success: function (data) {
                                                                    $("#txtNewSubject").val("");
                                                                    $('#cmbSubjectsRow .video-dropdown-container .container').html(data);
                                                                }
                                                            });
                                                        }
                                                    }

                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="cmbTopicsRow" class="topic-row row">
                                    <span class="label"><?php echo $language_based_content['choose_topic_vl']; ?></span>
                                    <div id="cmbTopics" class="video-dropdown"><?php echo isset($tops_title) && $tops_title != '' ? $tops_title : $language_based_content['topic_name_vl']; ?></div>
                                    <div class="video-dropdown-container">
                                        <div class="wrapper">
                                            <div class="container">
                                                <?php if ($topics): ?>
                                                    <?php foreach ($topics as $topic): ?>

                                                        <div id="chkContainerTopic<?php echo $topic['Topic']['id'] ?>" class="check-container">

                                                            <input id="topic_ids<?php echo $topic['Topic']['id'] ?>" name="topic_ids<?php echo $topic['Topic']['id'] ?>" class="checkbox-3 topic_checkbox" <?php echo (has_topic_selected($sel_topics, $topic['Topic']['id']) ? "checked='checked'" : "") ?>  type="checkbox" value="<?php echo $topic['Topic']['id'] ?>">

                                                            <label for="topic_ids<?php echo $topic['Topic']['id'] ?>" class="title"><?php echo $topic['Topic']['name'] ?></label>
                                                            <input type="text" id="txttopic_ids<?php echo $topic['Topic']['id'] ?>" class="edit-field" value="<?php echo $topic['Topic']['name'] ?>"/>
                                                            <div class="controls controls<?php echo $topic['Topic']['name'] ?>">
                                                                <?php
                                                                $imgedit_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/playlist8x8.png');
                                                                $imgdel_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/delete-icon8x9.png');
                                                                $imgsave_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/save8x8.png');
                                                                $imgundo_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/undo8x8.png');
                                                                ?>
                                                                <?php echo $this->Html->image($imgedit_path, array('id' => "imgEditTopic" . $topic['Topic']['id'], 'class' => 'edit-controls edit-controlsTopic' . $topic['Topic']['id'])); ?>
                                                                <?php echo $this->Html->image($imgdel_path, array('id' => "imgDeleteTopic" . $topic['Topic']['id'], 'class' => 'edit-controls edit-controlsTopic' . $topic['Topic']['id'])); ?>
                                                                <?php echo $this->Html->image($imgsave_path, array('id' => "imgSaveTopic" . $topic['Topic']['id'], 'class' => 'save-controls save-controlsTopic' . $topic['Topic']['id'])); ?>
                                                                <?php echo $this->Html->image($imgundo_path, array('id' => "imgUndoTopic" . $topic['Topic']['id'], 'class' => 'save-controls save-controlsTopic' . $topic['Topic']['id'])); ?>
                                                            </div>
                                                        </div>

                                                        <script type="text/javascript">

                                                            $(document).ready(function () {

                                                                $('.save-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

                                                                $('#imgEditTopic<?php echo $topic['Topic']['id'] ?>').click(function () {

                                                                    $('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').css('display', 'none');
                                                                    $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').css('display', 'block');

                                                                    $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').val($('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').html());

                                                                    $('.save-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'block');

                                                                    $('.edit-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'none');
                                                                });

                                                                $('#imgUndoTopic<?php echo $topic['Topic']['id'] ?>').click(function () {

                                                                    $('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').css('display', 'block');
                                                                    $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

                                                                    $('.save-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

                                                                    $('.edit-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'block');
                                                                });

                                                                $('#imgSaveTopic<?php echo $topic['Topic']['id'] ?>').click(function () {

                                                                    var topic_id = $(this).attr('id').replace('imgSaveTopic', '');

                                                                    $.ajax({
                                                                        type: 'POST',
                                                                        data: {
                                                                            topic: $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').val()
                                                                        },
                                                                        url: home_url + '/VideoLibrary/updateTopic/' + topic_id,
                                                                        success: function (response) {

                                                                            $('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').html($('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').val());

                                                                            $('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').css('display', 'block');
                                                                            $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

                                                                            $('.save-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

                                                                            $('.edit-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'block');

                                                                        },
                                                                        errors: function (response) {
                                                                            alert(response.contents);
                                                                        }

                                                                    });

                                                                });

                                                                $('#imgDeleteTopic<?php echo $topic['Topic']['id'] ?>').click(function () {

                                                                    var topic_id = $(this).attr('id').replace('imgDeleteTopic', '');

                                                                    $.ajax({
                                                                        type: 'POST',
                                                                        data: {
                                                                            topic: $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').val()
                                                                        },
                                                                        url: home_url + '/VideoLibrary/deleteTopic/' + topic_id,
                                                                        success: function (response) {

                                                                            $('#chkContainerTopic<?php echo $topic['Topic']['id'] ?>').remove();

                                                                        },
                                                                        errors: function (response) {
                                                                            alert(response.contents);
                                                                        }

                                                                    });

                                                                });

                                                            });

                                                        </script>

                                                    <?php endforeach; ?>
                                                <?php endif; ?>

                                            </div>
                                        </div>
                                        <input type="hidden" name="selected_topics" id="selected_topics" value=""/>
                                        <div class="video-dropdown-container-bottom">
                                            <input type="button" style='background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>' class="btnDoneVideoLibrary_1 btn btn-green" value="<?php echo $language_based_content['done_vl'] ?>"/>
                                            <input type="button" class="btnCancelVideoLibrary btn btn-grey" value="<?php echo $language_based_content['cancel_vl'] ?>"/>
                                            <div class="create-area">

                                                <input type="text" style="width: 200px;" id="txtNewTopic" name="new_topic" placeholder="<?php echo $language_based_content['topic_name_vl']; ?>.." />
                                                <a href="javascript:doNewTopic();" class="btn-creat"><?php echo $language_based_content['create_vl'] ?></a>

                                                <script type="text/javascript">

                                                    $('#txtNewTopic').bind('keypress', function (e) {

                                                        var code = (e.keyCode ? e.keyCode : e.which);
                                                        if (code == 13) { //Enter keycode
                                                            doNewTopic();
                                                            return false;
                                                        }
                                                    });

                                                    function doNewTopic() {

                                                        if ($("#txtNewTopic").val() == '') {
                                                            $("#txtNewTopic").addClass('error');
                                                        } else {
                                                            $("#txtNewTopic").removeClass('error');
                                                            $.ajax({
                                                                url: '<?php echo $this->base . "/VideoLibrary/addTopic" ?>',
                                                                method: 'POST',
                                                                data: {
                                                                    topic: $("#txtNewTopic").val()
                                                                },
                                                                success: function (data) {
                                                                    $("#txtNewTopic").val("");
                                                                    $('#cmbTopicsRow .video-dropdown-container .container').html(data);
                                                                }
                                                            });
                                                        }
                                                    }

                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="cnt-bottom">
                            <input type="button" id="btnAddToVideoLibrary btn btn-green" style='background: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>; border-radius: 3px;' class="btnAddToVideoLibrary" value="<?php echo $language_based_content['edit_library_vl']; ?>" onclick="PostVideoLibrary()"/>
                            <a href="<?php echo $this->base . '/VideoLibrary/view/' . $videoLibraryID . '/' . $library['AccountFolder']['account_id']; ?>" class="btn btn-grey"><?php echo $language_based_content['cancel_vl'] ?></a>
                        </div>
                    </div>
                </form>
            </div>

        </div>


        <script type="text/javascript">

            var uploadPath = '<?php echo "/tempupload/" . $accounts['account_id'] . "/" . date('Y') . "/" . date('m') . "/" . date('d') . "/"; ?>';

            function OpenFilePicker() {

                filepicker.setKey('<?php echo Configure::read('filepicker_access_key'); ?>');
                filepicker.pickAndStore(
                        {
                            multiple: false,
                            services: ['COMPUTER', 'DROPBOX', 'GOOGLE_DRIVE', 'SKYDRIVE'],
                            extensions: ['3g2', '3gp', '3gp2', '3gpp', '3gpp2', 'aac', 'ac3', 'eac3', 'ec3', 'f4a', 'f4b', 'f4v', 'flv', 'highwinds', 'm4a', 'm4b', 'm4r', 'm4v', 'mkv', 'mov', 'mp3', 'mp4', 'oga', 'ogg', 'ogv', 'ogx', 'ts', 'webm', 'wma', 'wmv', 'divx', 'avi', 'dv', 'mjpeg', 'mpg', 'm2ts', 'mts', 'mxf']
                        },
                {
                    location: "S3",
                    path: uploadPath,
                    access: 'public',
                    container: bucket_name
                },
                function (inkBlob) {


                    if (inkBlob && inkBlob.length > 0) {

                        var blob = inkBlob[0];
                        $('#txtUploadedFilePath').val(blob.key);
                        $('#txtUploadedFileName').val(blob.filename);
                        $('#txtUploadedFileMimeType').val(blob.mimetype);
                        $('#txtUploadedFileSize').val(blob.size);
                        $('#filepicker_dialog_container a').trigger("click");
                    }

                },
                        function (FPError) {

                        }
                );
            }


            $(document).ready(function () {

                $('.ui-checkbox input').on({
                    'change': function () {
                        $(this).parent()[$(this).prop('checked') ? 'addClass' : 'removeClass']('active');
                    },
                    'focus blur': function (e) {
                        $(this).parent()[e.type == 'focus' ? 'addClass' : 'removeClass']('focused');
                    }
                }).trigger('change');

                $("#frmVideoLibrary").validate({
                    errorPlacement: function (error, element) {
                    }
                });




                $('#txtVideoTags').tagsInput({
                    defaultText: '<?php echo $language_based_content['add_tags_here_vl']; ?>'
                });


                $('.video-dropdown').click(function () {
                    if ($(this).hasClass('video-dropdown-selected')) {
                        $(this).removeClass('video-dropdown-selected');
                        $($(this).siblings('.video-dropdown-container')[0]).css('display', 'none');
                    } else {
                        CloseAllDropDown();
                        $(this).addClass('video-dropdown-selected');
                        $($(this).siblings('.video-dropdown-container')[0]).css('display', 'block');
                    }
                });


                $('.btnDoneVideoLibrary_1').click(CloseDropDown);

                $('.btnCancelVideoLibrary').click(CloseAllDropDown);
            });




            function getCheckedSubjects() {

                var subjects_id = '';

                $.each($('.subject_checkbox'), function (index, value) {

                    if ($(this).prop('checked') == true) {
                        if (subjects_id != '')
                            subjects_id += ',';
                        subjects_id += $(this).attr('value');
                    }

                });

                return subjects_id;
            }

            function getCheckedTopics() {

                var topics_id = '';

                $.each($('.topic_checkbox'), function (index, value) {

                    if ($(this).prop('checked') == true) {
                        if (topics_id != '')
                            topics_id += ',';

                        topics_id += $(this).attr('value');

                    }

                });
                return topics_id;
            }

            function PostVideoLibrary() {
                var selected_subjects = getCheckedSubjects();
                var selected_topics = getCheckedTopics();

                if ($('#txtUploadedFilePath').val() == '') {
                    alert('<?php echo $alert_messages['please_wait_while_file_uploading']; ?>');
                    return;
                }

                /*if ($('#txtVideoTags').val() == '') {
                 $('#txtVideoTags_tagsinput').addClass('tagsinputerror');
                 } else {
                 $('#txtVideoTags_tagsinput').removeClass('tagsinputerror');
                 }*/

                if (selected_subjects == '') {
                    $('#cmbSubjects').addClass('video-dropdown-error');
                } else {
                    $('#cmbSubjects').removeClass('video-dropdown-error');
                }

                $('#selected_subjects').val(selected_subjects);

                if (selected_topics == '') {
                    $('#cmbTopics').addClass('video-dropdown-error');
                } else {
                    $('#cmbTopics').removeClass('video-dropdown-error');
                }

                $('#selected_topics').val(selected_topics);

                if ($("#frmVideoLibrary").valid() && selected_subjects != '' && selected_topics != '') {

                    $("#btnAddToVideoLibrary").val("<?php echo $language_based_content['uploading_library']; ?>");
                    $("#btnAddToVideoLibrary").prop("disabled", true);
                    // $.blockUI({message: "Uploading please wait."});

                    $.ajax({
                        url: home_url + '/VideoLibrary/edit/<?php echo $videoLibraryID; ?>',
                        method: 'POST',
                        success: function (data) {

                            $(document).ajaxStop($.unblockUI);

                            if (parseInt(data) == 0) {
                                alert('<?php echo $alert_messages['unable_to_save_data']; ?>');
                            } else {
                                $(".inner").unmask();
                                location.href = home_url + '/VideoLibrary/view/<?php echo $videoLibraryID . '/' . $library['AccountFolder']['account_id']; ?>';
                            }
                        },
                        data: {
                            video_title: $('#txtVideoTitle').val(),
                            video_desc: $('#txtVideoDescription').val(),
                            video_url: $('#txtUploadedFilePath').val(),
                            video_file_name: $('#txtUploadedFileName').val(),
                            video_mime_type: $('#txtUploadedFileMimeType').val(),
                            video_file_size: $('#txtUploadedFileSize').val(),
                            rand_folder: $('#txtRandomNumber').val(),
                            tags: $('#txtVideoTags').val(),
                            subjects: $('#selected_subjects').val(),
                            topics: $('#selected_topics').val()
                        }
                    });

                }

            }

            $('.subject-checks').on('click', function (e) {
                if ($(this).is(":checked")) {
                    setAllSubjects();
                } else {
                    setAllSubjects();
                }
            });
            $('.topic_checkbox').on('click', function (e) {
                if ($(this).is(":checked")) {
                    setAllTopics();
                } else {
                    setAllTopics();
                }
            });
            function CloseDropDown() {
                setAllSubjects();
                setAllTopics();
                $($(this).parent().parent().prev()[0]).trigger("click");
            }

            function CloseAllDropDown() {

                var btnDones = $('.btnDoneVideoLibrary_1');

                for (var i = 0; i < btnDones.length; i++) {
                    var btnDone = $(btnDones[i]);
                    var current_dropdwn = $(btnDone.parent().parent().prev()[0]);
                    if (current_dropdwn.hasClass('video-dropdown-selected')) {
                        current_dropdwn.removeClass('video-dropdown-selected');
                        $(current_dropdwn.siblings('.video-dropdown-container')[0]).css('display', 'none');
                    }
                }
            }
            function setAllSubjects() {

                if ($('.subject-checks:checked').length > 0) {
                    var chkId = '';
                    $('.subject-checks:checked').each(function () {
                        chkId += $('#txtsubject_ids' + $(this).val()).val() + ",";
                    });
                    chkId = chkId.slice(0, -1);
                    $('#cmbSubjects').html('<div style="width:255px; white-space: nowrap;overflow: hidden;    text-overflow: ellipsis;">' + chkId + '</div>');
                } else {
                    $('#cmbSubjects').html('<?php echo $language_based_content['subject_name_vl']; ?>');
                }


            }
            function setAllTopics() {

                if ($('.topic_checkbox:checked').length > 0) {
                    var chkId2 = '';
                    $('.topic_checkbox:checked').each(function () {
                        chkId2 += $('#txttopic_ids' + $(this).val()).val() + ",";
                    });
                    chkId2 = chkId2.slice(0, -1);
                    $('#cmbTopics').html('<div style="width:255px; white-space: nowrap;overflow: hidden;    text-overflow: ellipsis;">' + chkId2 + '</div>');
                } else {
                    $('#cmbTopics').html('<?php $language_based_content['topic_name_vl']; ?>');
                }


            }



        </script>
        <div style="clear:both;"></div>
    </div>
</div>
