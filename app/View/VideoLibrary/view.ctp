<style type="text/css">
    .vjs-tooltip {
        display: none !important;
    }
    .btn-success{
    background-color: rgb(243, 98, 82) !important;
    border-color: rgb(243, 98, 82) !important;
    border: 0px !important;
    margin-left: 340px;
    color: #fff;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: 4px;
    }
    .exp_notes { background:#FFEBEB; padding:20px; margin: -20px -20px 0 -20px;}

    .exp_notes p { margin:0; color:#616161;}
    @font-face {
    font-family: 'VideoJS';
    src: url('https://vjs.zencdn.net/f/1/vjs.eot');
    src: url('https://vjs.zencdn.net/f/1/vjs.eot?#iefix') format('embedded-opentype'), 
      url('https://vjs.zencdn.net/f/1/vjs.woff') format('woff'),     
      url('https://vjs.zencdn.net/f/1/vjs.ttf') format('truetype');
  }

    .video-js .vjs-play-control.vjs-playing .vjs-icon-placeholder:before, .vjs-icon-pause:before {
        content: "\f103";
        font-family: 'VideoJS';
    }

    .video-js .vjs-mute-control .vjs-icon-placeholder:before, .vjs-icon-volume-high:before {
        content: "\f107";
        font-family: 'VideoJS';
    }

    .video-js .vjs-big-play-button .vjs-icon-placeholder:before, .video-js .vjs-play-control .vjs-icon-placeholder:before, .vjs-icon-play:before {
        content: "\f101";
        font-family: 'VideoJS';
    }

    .video-js .vjs-picture-in-picture-control .vjs-icon-placeholder:before, .vjs-icon-picture-in-picture-enter:before {
        content: "\f121";
        font-family: 'VideoJS';
    }



     .video-js .vjs-fullscreen-control .vjs-icon-placeholder:before, .vjs-icon-fullscreen-enter:before {
        content: "\f108";
        font-family: 'VideoJS';
    }
</style>
<?php
$amazon_base_url = Configure::read('amazon_base_url');
$user_permissions = $this->Session->read('user_permissions');
$user_current_account = $this->Session->read('user_current_account');

$account_id = $user_current_account['accounts']['account_id'];
$user_id = $user_current_account['User']['id'];
if (isset($library) && !empty($library)) {
    ?>

    <div id="flashMessage2" class="message error" style="display:none;"></div>

    <div class="top top-videoview" style="display: none;">&nbsp;</div>
    <div class="VideoLibraryView box" style="width: 100%;">
        <div class="header header-huddle">
            <h2 class="title"><?php echo $library['AccountFolder']['name']; ?></h2>
        </div>
        <?php if($cardExpired){ ?> 
            <div class="exp_notes">
           <p>Please update the expiration date for your credit card or change the credit card.
              <button onclick="window.location.href = '<?php echo Configure::read('sibme_base_url') . 'home/account-settings/plans' ; ?>';" type="button" class="btn-success">Update Now</button>
           </p>
          </div>
        <?php } ?>
        <div class="warper" style="margin: 0px -30px;">

            <div class="top-head" style="display: none;">
                <div class="left">
                    <h2 style="font-size: 18px;margin-top: 10px;">Video</h2>
                </div>
            </div>
            <div class="left_right_main">
                <div class="left_col" style="padding-top: 0px;">
                    <!--  <div class="back_to">
                         <p><a href="<?php echo $selected_account == $account_id ? $this->base . "/VideoLibrary/" : $this->base . "/VideoLibrary/index/" . $selected_account; ?>">Back to Video Library </a> </p>
                     </div> -->
                    <div class="video_title">
                        <h4><?php echo $library['AccountFolder']['name']; ?></h4>

                        <div class="toolbar">
                            <?php //echo $this->Html->image('message.png');   ?>
                            <a  onMouseOut="hide_sidebar()" href="#" class="appendix right lmargin-top" style="margin-top: 5px;">?</a>
                            <div class="appendix-content appendix-narrow" style="display: none;">
                                <p style="word-wrap: break-word; padding: 10px; margin-top: 0px;">If your video is not loading or playing, please upgrade your browser to the most recent version.<?php $this->Custom->get_site_settings('site_title') ?> is compatible with <a style="float: none;" target="blank" href="http://windows.microsoft.com/en-US/internet-explorer/downloads/ie-9/worldwide-languages">Internet Explorer</a>,<a style="float: none;" target="blank" href="https://www.google.com/intl/en/chrome/browser/?&brand=CHMB&utm_campaign=en&utm_source=en-ha-na-us-sk&utm_medium=ha">Google Chrome</a>, and <a style="float: none;" href="http://www.apple.com/safari/" target="blank">Safari</a>. If your video is still not playing in one of these browsers, please contact &nbsp; <a style="float: none;" href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.</p>
                            </div>
                            <?php //echo $user_current_account['roles']['role_id'];?>
                            <?php if (($user_current_account['roles']['role_id'] == '110' || $user_current_account['roles']['role_id'] == '100' || $user_permissions['UserAccount']['permission_video_library_upload'] == '1') && $account_id == $selected_account): ?>
                               <?php if($user_current_account['roles']['role_id'] <= 110):?>
                               <a href="<?php echo $this->base . '/videoLibrary/edit/' . $video_id ?>" rel="tooltip" title="edit"><?php
                                    $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/playlist2.png');
                                    echo $this->Html->image($chimg);
                                    ?></a>

                                <form id="delete-video-document<?php echo $video['Document']['id']; ?>" action="<?php echo $this->base . '/VideoLibrary/delete/' . $video_id ?>" accept-charset="UTF-8">
                                    <a rel="nofollow tooltip" title="delete" data-confirm="Are you sure you want to delete this Video?" id="delete-main-video" href="javascript:$('#delete-video-document<?php echo $video['Document']['id']; ?>').submit()"><?php
                                        $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/delet_icon.png');
                                        echo $this->Html->image($chimg);
                                        ?></a>
                                </form>
                                <?php endif; ?>
                            <?php endif; ?>

                        </div>
                        <div class="clear"> </div>
                    </div>
                    <?php
                    $videoLibraryID = $library['AccountFolder']['account_folder_id'];

                    $document_files_array = $this->Custom->get_document_url($video['Document']);

                    if (empty($document_files_array['url'])) {
                        $video['Document']['published'] = 0;
                        $document_files_array['url'] = $video['Document']['original_file_name'];
                        $video['Document']['encoder_status'] = $document_files_array['encoder_status'];
                    } else {
                        $video['Document']['encoder_status'] = $document_files_array['encoder_status'];
                    }

                    $videoFilePath = pathinfo($document_files_array['url']);
                    //$videoFilePath = pathinfo($this->Custom->get_document_url($video['Document']['id'],$video['Document']['url']));
                    $videoFileName = $videoFilePath['filename'];
                    ?>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $(".vjs-big-play-button").on('click', function () {
                                $.ajax({
                                    type: 'POST',
                                    url: home_url + '/VideoLibrary/update_view_count/<?php echo $video['Document']['id']; ?>',
                                    success: function (response) {

                                    },
                                    errors: function (response) {

                                    }

                                });
                            });


                        });
                    </script>
                    <style>
                        .vjs-tooltip{display: none !important; }
                        .v_control button.fast{    background: #3498DB;
                        }
                        .v_control button {    cursor: pointer; border: 0;
                                               padding: 7px;
                                               border-radius: 5px;
                                               color: #fff;}
                        .v_control button.wrin{    background: #3a79a4;
                        }
                        .v_control {margin-top:10px;}

                        .v_control button img{width:35px;}

                    </style>
                    <?php
                    $document_files_array = $this->Custom->get_document_url($video['Document']);
                    $transcoding_status = $this->Custom->transcoding_status($video['Document']['id']);
                    if (empty($document_files_array['url'])) {
                        $video['Document']['published'] = 0;
                        $document_files_array['url'] = $video['Document']['original_file_name'];
                        $video['Document']['encoder_status'] = $document_files_array['encoder_status'];
                    } else {
                        $video['Document']['encoder_status'] = $document_files_array['encoder_status'];
                    }

                    if (isset($video['Document']['published']) && $video['Document']['published'] == 1 && $transcoding_status != 5):
                        ?>

                        <div oncontextmenu="return false;" class="video_detail" >

                            <?php
                            $thumbnail_image_path = $document_files_array['thumbnail'];
                            $video_path = $document_files_array['url'];
                            ?>

                            <video id="<?php echo $videoLibraryID ?>" class="video-js vjs-default-skin disable-vide-comments" controls preload="metadata" width="629" height="405" poster="<?php echo $thumbnail_image_path; ?>" data-markers="[]" data-setup="{}" >
                                <source src="<?php echo $video_path; ?>" type='video/mp4'>
                            </video>
                            <!--                            <div class="v_control">
                                             <button onclick="skip(-10)" class="wrin"><img src="<?php //echo  $this->webroot.'img/fast_forward_icon2.png'                ?>"> Rewind</button>
                                                    <button onclick="skip(10)" class="fast">Fast Forward <img src="<?php //echo  $this->webroot.'img/fast_forward_icon.png'                ?>"></button>
                            </div>-->
                            <?php
                            $commentDate = $library['AccountFolder']['created_date'];
                            $commentsDate = $this->Custom->formateDate($commentDate);
                            ?>
                            <p>
                                <span><?php
                                    $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/upload_icon.png');
                                    echo $this->Html->image($chimg);
                                    ?>Uploaded <?php echo $commentsDate; ?></span>
                                <?php $extraPermission = (int) $user_permissions['UserAccount']['permission_video_library_upload']; ?>
                                <?php $isCreator = $this->Custom->is_creator($user_current_account['User']['id'], $video['Document']['created_by']); ?>
                                <?php if ($user_current_account['roles']['role_id'] == '110'   || $extraPermission): ?>
                                    <?php if($user_current_account['roles']['role_id'] <= 110):?>
                                    <?php $filePath = $video_path; ?>
                                    <?php                                    
                                    $downloadUrl = $video_path;
                                    ?>
                                     <?php if(empty($can_download_video)):?>                                                                                                                                                           <!--<a href="<?php echo $downloadUrl; ?>" target="_blank" download="<?php echo $videoFileName . "_enc.mp4"; ?>">-->
                                        <a href="<?php echo $this->webroot . 'Huddles/download/' . $video['Document']['id'] ?>">
                                            <img alt="Download" class="right videos-library-detail-download" rel="tooltip" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download.png'); ?>" title="download video"/>
                                        </a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>

                                <?php
                                $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/video_icon.png');
                                echo $this->Html->image($chimg);
                                ?>Views: <?php echo $video['Document']['view_count']; ?>

                            </p>
                            <div class="clear"> </div>


                        </div>

                    <?php else: ?>

                        <div  style="width: 629px; height: 405px; background: #000; float: left; text-align: center; color: #fff;">
                            <div class="empty_video_box" style="width: 100%;padding: 165px 80px;">
                                <?php if ($video['Document']['encoder_status'] == 'Error' || $transcoding_status == 5): ?>
                                    Video failed to process successfully. Please try again or contact <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>" style="color: blue;text-decoration: underline;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.

                                <?php else: ?>
                                    <img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;" /><br>Your video is currently processing. You will receive an email notification when your video is ready to be viewed.
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="img_thum">

                        <?php
                        
                        $profile_image = $library['User']['image'];
                        $profile_image_real_path = realpath(dirname(__FILE__) . '/../../../') . "/app/webroot/img/users/$profile_image";

                        //if (!empty($profile_image) && file_exists($profile_image_real_path)) {
                        if (!empty($profile_image)) {
                            //$profile_image = $this->webroot . "img/users/$profile_image";
                            $profile_image = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $library['AccountFolder']['created_by'] . "/" . $library['User']['image']);
                        } else {
                            //$profile_image = $this->webroot . "img/profile.jpg";
                            $profile_image = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/profile.jpg');
                        }
                        ?>

                        <img src="<?php echo $profile_image; ?>" width="50px"/>
                        <p><span><?php echo $library['User']['first_name'] . ' ' . $library['User']['last_name']; ?> - </span><?php echo $videoCount; ?> videos </p>
                        <p><?php echo $roles = $library['AccountFolder']['name']; ?></p>
                        <div class="clear"> </div>
                    </div>
                    <div class="description">
                        <h4>Description </h4>
                        <p>
                            <?php echo $library['AccountFolder']['desc']; ?> <br> <span style="visibility:hidden;">More<?php
                            $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/more_icon.png');
                            echo $this->Html->image($chimg);
                            ?></span></p>
                    </div>
                    <div class="description2">
                        <div class="description-2-row">
                            <h5>Tags:</h5>
                            <?php
                            echo "&nbsp";
                            if (count($tags) > 0) {

                                $tags_string = '';
                                for ($i = 0; $i < count($tags); $i++) {

                                    $tag = $tags[$i];
                                    if (!empty($tags_string))
                                        $tags_string .= ", ";
                                    $tags_string .= $tag['account_folders_meta_data']['meta_data_value'];
                                }
                                ?>
                                <?php if ($user_current_account['roles']['role_id'] == '110' || $user_current_account['roles']['role_id'] == '100' || $user_permissions['UserAccount']['permission_video_library_upload'] == '1'): ?>
                                    <?php echo $tags_string; ?>
                                <?php else: ?>
                                    <?php echo $tags_string; ?>
                                <?php endif; ?>
                                <?php
                            } else {
                                echo "No tags added";
                            }

                            if (!empty($library['videoLibrary']['tags'])) {
                                echo $library['videoLibrary']['tags'];
                            } else {

                            }
                            ?>
                        </div>

                        <div class="description-2-row">
                            <h5>Subject: </h5>
                            <?php
                            echo "&nbsp";
                            $subjects_string = '';
                            if (!empty($subjects)) {
                                foreach ($subjects as $subject) {
                                    if (!empty($subjects_string))
                                        $subjects_string .= ", ";
                                    $subjects_string .= $subject['Subject']['name'];
                                }
                                ?>
                                <?php if ($user_current_account['roles']['role_id'] == '110' || $user_current_account['roles']['role_id'] == '100' || $user_permissions['UserAccount']['permission_video_library_upload'] == '1'): ?>
                                    <?php echo $subjects_string; ?>
                                <?php else: ?>
                                    <?php echo $subjects_string; ?>
                                <?php endif; ?>
                                <?php
                            } else {
                                echo "No subjects associated";
                            }
                            ?>

                        </div>

                        <div class="description-2-row">
                            <h5>Topic: </h5>
                            <?php
                            echo "&nbsp";
                            $topics_string = '';
                            if (!empty($topics)) {
                                foreach ($topics as $topic) {
                                    if (!empty($topics_string))
                                        $topics_string .= ", ";
                                    $topics_string .= $topic['Topic']['name'];
                                }
                                ?>
                                <?php if ($user_current_account['roles']['role_id'] == '110' || $user_current_account['roles']['role_id'] == '100' || $user_permissions['UserAccount']['permission_video_library_upload'] == '1'): ?>
                                    <?php echo $topics_string; ?>
                                <?php else: ?>
                                    <?php echo $topics_string; ?>
                                <?php endif; ?>
                                <?php
                            } else {
                                echo "No topics associated ";
                            }
                            ?>


                        </div>
                    </div>
                </div>
                <div class="right_col">
                    <p>Supporting Attachments
                        <?php if ($user_permissions['UserAccount']['permission_video_library_upload'] == '1' && $selected_account == $account_id): ?>
                           <?php if($user_current_account['roles']['role_id'] <= 110):?>
                                <span><a id="video-library-doc-upload" class="upload" >Upload</a> </span>
                            <?php endif; ?>
                        <?php endif; ?>
                    </p>

                    <input id="txtUploadedFilePath" type="hidden" value="" />
                    <input id="txtUploadedFileName" type="hidden" value="" />
                    <input id="txtUploadedFileMimeType" type="hidden" value="" />
                    <input id="txtUploadedFileSize" type="hidden" value="" />
                    <input id="txtUploadedDocType" type="hidden" value="" />
                    <input id="txtUploadedUrl" type="hidden" value="" />
                    <input id="video-id" type="hidden" value="<?php echo $videoLibraryID ?>" />
                    <input id="user_id" type="hidden" value="<?php echo $user_id ?>" />
                    <input id="synchro_time" type="hidden" value="" />

                    <ul>
                        <?php
                        if (!empty($documents)) {
                            foreach ($documents as $document) {

                                $documentID = $document['Document']['id'];
                                //$documentFilePath = pathinfo($document['Document']['url']);
                                //$documentFilePath = pathinfo($this->Custom->get_document_url($documentID,$document['Document']['url']));

                                $document_files_array = $this->Custom->get_document_url($document['Document']);
                                if (empty($document_files_array['url'])) {
                                    $document['Document']['published'] = 0;
                                    $document_files_array['url'] = $document['Document']['original_file_name'];
                                    $document['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                } else {
                                    $document['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                }

                                $documentFilePath = pathinfo($document_files_array['url']);

                                $documentFileName = $documentFilePath['filename'];
                                $documentFileNameWExt = $documentFilePath['filename'] . "." . $documentFilePath['extension'];

                                $documentOrgFilePath = pathinfo($document['Document']['original_file_name']);
                                $documentOrgFileName = $documentOrgFilePath['filename'];
                                ?>
                                <li class="video-docs"  id="li_video_doc_<?php echo $document['Document']['id']; ?>">
                                    <span class="<?php echo $this->Custom->getIcons($documentFileNameWExt); ?>"></span>
                                    <?php if (($user_current_account['roles']['role_id'] == '110' || $user_current_account['roles']['role_id'] == '100' || $user_permissions['UserAccount']['permission_video_library_upload'] == '1') && $selected_account == $account_id && $user_current_account['roles']['role_id'] <= 110): ?>
                                        <form id="delete-video-document<?php echo $document['Document']['id']; ?>" data-async="" action="<?php echo $this->base; ?>/VideoLibrary/deleteVideoDocument/<?php echo $document['Document']['id']; ?>/<?php echo $videoLibraryID; ?>" accept-charset="UTF-8">
                                            <a rel="nofollow" data-confirm="Are you sure you want to delete this Document?" id="delete-main-comments" style=" margin-top: 5px; display: block; width: 16px; float: left; " class="del" href="javascript:$('#delete-video-document<?php echo $document['Document']['id']; ?>').submit()">
                                                <img src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/del-icon.gif'); ?>"/>
                                            </a>

                                            <script type="text/javascript">
                                                $(document).ready(function () {
                                                    $('#delete-video-document<?php echo $document['Document']['id']; ?>').on('submit', function (event) {
                                                        var $form = $(this);
                                                        var $target = $($form.attr('data-target'));
                                                        $.ajax({
                                                            type: $form.attr('method'),
                                                            url: $form.attr('action'),
                                                            data: $form.serialize(),
                                                            success: function (data, status) {
                                                                $('#li_video_doc_<?php echo $document['Document']['id']; ?>').remove();
                                                                if ($('ul.video-docs li').length == 0) {
                                                                    $('ul.video-docs').html('<li style="clear:left;">No Attachments have been added.</li>');
                                                                }
                                                            }
                                                        });
                                                        event.preventDefault();

                                                    });

                                                });</script>
                                        </form>
                                    <?php endif; ?>
                                    <?php if (isset($document['Document']['stack_url']) && $document['Document']['stack_url'] != '') {
                                        ?>
                                        <?php
                                        $url_code = explode('/', $document['Document']['stack_url']);

                                        $url_code = $url_code[3];
                                        ?>
                                        <?php if($user_current_account['roles']['role_id'] <= 110):?>
                                            <a target="_blank"  href="<?php echo $this->base . '/VideoLibrary/view_document/' . $url_code; ?>"><?php echo $documentOrgFileName; ?> </a>
                                        <?php else:?>
                                            <a   href="#"><?php echo $documentOrgFileName; ?> </a>
                                        <?php endif;?>

                                    <?php } else { ?>
                                        <?php if($user_current_account['roles']['role_id'] <= 110):?>
                                                <a href="<?php echo $this->base . '/VideoLibrary/download/' . $document['Document']['id'] . '/' . $videoLibraryID ?>"><?php echo $documentOrgFileName; ?> </a>
                                             <?php else:?>
                                                <a  href="#"><?php echo $documentOrgFileName; ?> </a>
                                             <?php endif;?>
                                        <?php } ?>
                                </li>

                                <?php
                            }
                        } else {
                            echo "No Attachments have been added.";
                        }
                        ?>
                    </ul>
                </div>
                <div class="clear"> </div>
            </div>
        </div>
    </div>
    <div class="btm bottom-videoview">&nbsp;</div>
    <script type="text/javascript">
        $(document).ready(function () {
            setInterval(function () {
                $('.vjs-tooltip').remove();
            }, 500);
            $('.vjs-tooltip').css('display', 'none !important');

            $('.vjs-load-progress').hover(function (e) {
                $('.vjs-tooltip').remove()
            });
        });

        //        function OpenFilePicker() {
        //            filepicker.setKey('<?php //echo Configure::read('filepicker_access_key');          ?>');
        //            var uploadPath = '<?php //echo "/tempupload/" . $accounts['account_id'] . "/" . date('Y') . "/" . date('m') . "/" . date('d') . "/";          ?>';
        //            $('#txtUploadedFilePath').val("");
        //            $('#txtUploadedFileName').val("");
        //            $('#txtUploadedFileMimeType').val("");
        //            $('#txtUploadedFileSize').val("");
        //            $('#txtUploadedDocType').val('doc');
        //
        //            filepicker.pickAndStore(
        //                    {
        //                        multiple: false,
        //                        extensions: ['bmp', 'gif', 'jpeg', 'jpg', 'png', 'tif', 'tiff', 'swf', 'pdf', 'txt', 'docx', 'ppt', 'pptx', 'potx', 'xls', 'xlsx', 'xlsm', 'rtf', 'odt', 'doc'],
        //                        services: ['COMPUTER', 'DROPBOX', 'GOOGLE_DRIVE', 'SKYDRIVE', 'BOX']
        //                    },
        //            {
        //                location: "S3",
        //                path: uploadPath,
        //                access: 'public',
        //                container: bucket_name
        //            },
        //            function(inkBlob) {
        //                if (inkBlob && inkBlob.length > 0) {
        //                    var blob = inkBlob[0];
        //                    $('#txtUploadedFilePath').val(blob.key);
        //                    $('#txtUploadedFileName').val(blob.filename);
        //                    $('#txtUploadedFileMimeType').val(blob.mimetype);
        //                    $('#txtUploadedFileSize').val(blob.size);
        //                    $('#txtUploadedUrl').val(blob.url);
        //                    $('#filepicker_dialog_container a').trigger("click");
        //                    PostHuddleDocument();
        //                }
        //            },
        //                    function(FPError) {
        //                    }
        //            );
        //        }



        function OpenFilePicker() {
            // filepicker.setKey('<?php //echo Configure::read('filepicker_access_key');          ?>');
            var client = filestack.init('<?php echo Configure::read('filepicker_access_key'); ?>');
            var uploadPath = '<?php echo "/tempupload/" . $accounts['account_id'] . "/" . date('Y') . "/" . date('m') . "/" . date('d') . "/"; ?>';
            $('#txtUploadedFilePath').val("");
            $('#txtUploadedFileName').val("");
            $('#txtUploadedFileMimeType').val("");
            $('#txtUploadedFileSize').val("");
            $('#txtUploadedDocType').val('doc');

            client.pick({
                maxFiles: 1000,
                accept: ['application/*', '.bmp', '.gif', '.jpeg', '.jpg', '.png', '.tif', '.tiff', '.swf', '.pdf', '.txt', '.docx', '.ppt', '.pptx', '.potx', '.xls', '.xlsx', '.xlsm', '.rtf', '.odt', '.doc'],
                fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive'],
                storeTo: {
                    location: "s3",
                    path: uploadPath,
                    access: 'public',
                    container: bucket_name,
                    region: 'us-east-1'
                }
            }).then(
                    function (inkBlob) {
                        inkBlob = inkBlob.filesUploaded;
                        console.log(JSON.stringify(inkBlob.filesUploaded));
                        if (inkBlob && inkBlob.length > 0) {
                            var blob = inkBlob[0];
                            $('#txtUploadedFilePath').val(blob.key);
                            $('#txtUploadedFileName').val(blob.filename);
                            $('#txtUploadedFileMimeType').val(blob.mimetype);
                            $('#txtUploadedFileSize').val(blob.size);
                            $('#txtUploadedUrl').val(blob.url);
                            $('#filepicker_dialog_container a').trigger("click");
                            PostHuddleDocument();
                        }
                    });

        }


        function PostHuddleDocument() {
            var video_id_url = 'undefined';
            var value = 1;

            $.ajax({
                url: home_url + '/VideoLibrary/uploadDocuments' + '/<?php echo $videoLibraryID; ?>/<?php echo $account_id; ?>/<?php echo $user_id; ?>/' + video_id_url + '/' + value,
                method: 'POST',
                success: function (data) {
                    if (parseInt(data) == 0) {
                        alert('Unable to save data, please try again');
                    } else {

                        location.href = '<?php echo $this->base; ?>/VideoLibrary/view/<?php echo $videoLibraryID . '/' . $account_id; ?>';
                                        }
                                    },
                                    data: {
                                        video_title: $('#txtUploadedFileName').val(),
                                        video_desc: "",
                                        video_url: $('#txtUploadedFilePath').val(),
                                        video_file_name: $('#txtUploadedFileName').val(),
                                        video_mime_type: $('#txtUploadedFileMimeType').val(),
                                        video_file_size: $('#txtUploadedFileSize').val(),
                                        url: $('#txtUploadedUrl').val(),
                                        rand_folder: "0"
                                    }
                                });
                            }
                            $(document).ready(function (e) {
                                $('.vjs-play-progress').css('padding-right', '8px');
                            });

                            function hide_sidebar() {
                                $(".appendix-content").delay(500).fadeOut(500);
                                // $("#collab_help").delay(500).fadeOut(500);


                            }
                            function skip(value) {
                                var video = videojs('<?php echo $videoLibraryID ?>');
                                var current_time = video.currentTime();
                                video.currentTime(current_time + value);
                            }


                            $('#video-library-doc-upload').click(function (e) {
                                $.ajax({
                                    url: '<?php echo $this->base; ?>/Huddles/check_storage_full',
                                    type: 'POST',
                                    success: function (response) {
                                        if (response != '') {
                                            $('#flashMessage2').css('display', 'block');
                                            $('#flashMessage2').html(response);

                                        } else {
                                            OpenFilePicker();
                                        }
                                    }
                                });
                            });

                            $('#flashMessage2').click(function (e) {

                                $('#flashMessage2').fadeOut();
                            });


                            $(document).ready(function (e) {

                                var bread_crumb_data = '<div class="container"><a href = "/dashboard/home">Home</a><a href="/VideoLibrary">Video Library</a> <span>Video</span></div>';

                                $('.breadCrum').html(bread_crumb_data);

                            });

                            $(document).ready(function (e) {
                                function document_viewer_history() {
                                    /* Add Viewer History ---*/
                                    $.ajax({
                                        url: home_url + '/app/add_viewer_history',
                                        method: 'POST',
                                        data: {
                                            document_id: $('#video-id').val(),
                                            user_id: $('#user_id').val(),
                                            minutes_watched: $('#synchro_time').val()
                                        },
                                        dataType: 'json',
                                        success: function (res) {
                                            console.log(res);
                                        }
                                    });
                                }

                                var video = videojs('<?php echo $videoLibraryID ?>');
                                if (video.play_seconds == undefined) {
                                    video.play_seconds = 0;
                                }

                                video.on('ended', function () {
                                    video.play_state = false;
                                    clearInterval(video.play_interval);
                                });

                                video.on('pause', function () {
                                    video.play_state = false;
                                });


                                video.on('play', function () {
                                    // setInterval(document_viewer_history, 5000);

                                    video.play_state = true;

                                    /*
                                     if (video.play_interval != null) {
                                     clearInterval(video.play_interval);
                                     }
                                     */

                                    video.play_interval = setInterval(function () {

                                        if (video.play_state == true) {
                                            video.play_seconds += 1;

                                            console.log(video.play_seconds);

                                            if ((video.play_seconds % 5) === 0) {

                                                $.ajax({
                                                    url: home_url + '/app/add_viewer_history',
                                                    method: 'POST',
                                                    data: {
                                                        document_id: $('#video-id').val(),
                                                        user_id: $('#user_id').val(),
                                                        minutes_watched: video.play_seconds
                                                    },
                                                    dataType: 'json',
                                                    success: function (res) {
                                                        console.log(res);
                                                    }
                                                });
                                            }
                                        }

                                    }, 1000);





                                });


                            });


    </script>

    <?php
} else {
    echo "no record found";
}
?>
