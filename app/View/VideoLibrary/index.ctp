<?php $breadcrumb_language_based_content = $this->Custom->get_page_lang_based_content('BreadCrumb'); ?>
<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<?php
$amazon_base_url = Configure::read('amazon_base_url');
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$parent_library_check = $this->Custom->get_show_parent_video_library($users['accounts']['account_id']);
$parent_account_id = $this->Custom->get_account_details($users['accounts']['account_id']);
$login_account_id = $users['account']['account_id'];

?>
<?php
if (isset($get_videos['total'])) {
    $remaining_videos = $get_videos['total'] - $rows;
    unset($get_videos['total']);
}
?>
<style type="text/css">
  .btn-success{
    background-color: rgb(243, 98, 82) !important;
    border-color: rgb(243, 98, 82) !important;
    border: 0px !important;
    margin-left: 340px;
    color: #fff;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: 4px;
    }
.exp_notes { background:#FFEBEB; padding:20px; margin: 0 -20px;}

.exp_notes p { margin:0; color:#616161;}
    
</style>    
<div id="flashMessage2" class="message error" style="display:none;"></div>
<div class="box videos-library">

    <div class="header header--large header-huddle">

        <h2 class="header__title"><?php echo $language_based_content['video_library_vl']; ?> <?php echo!empty($user_accounts) ? ' - ' . $account_info['Account']['company_name'] : ''; ?><span class="blue_count"><?php echo $total_videos; ?></span></h2>
    </div>
   <?php if($cardExpired){ ?> 
    <div class="exp_notes">
     <p>Please update the expiration date for your credit card or change the credit card.
        <button onclick="window.location.href = '<?php echo Configure::read('sibme_base_url') . 'home/account-settings/plans' ; ?>';" type="button" class="btn-success">Update Now</button>
     </p>
    </div>
   <?php } ?>
    <div class="videos-library__main">
        <aside class="videos-library__sidebar">
            <?php if (!empty($user_accounts)): ?>
                <?php if ($parent_account_id === 0 || !$parent_library_check): ?>
                    <h4 class="videos-library__sidebar-title"><?php echo $language_based_content['switch_library']; ?></h4>
                    <ul class="videos-library__sidebar-categories" style="max-height: 400px;overflow: hidden;overflow-y: auto;">
                        <?php
                        foreach ($user_accounts as $user_account):
                            if ($user_account['Account']['id'] == $user_current_account['accounts']['account_id'] && empty($selected_account)) {
                                ?>
                                <li class="js-show-all-videos all_color">
                                    <a href="<?php echo $this->webroot . 'VideoLibrary/index/' . $user_account['Account']['id']; ?>" data-account_id="<?php echo $user_account['Account']['id']; ?>" data-parent_id="<?php echo $user_account['Account']['parent_account_id']; ?>" class="bold active"><?php echo $user_account['Account']['company_name']; ?></a>
                                </li>
                                <?php
                            } else if (!empty($selected_account) && $selected_account == $user_account['Account']['id']) {
                                ?>
                                <li class="js-show-all-videos all_color">
                                    <a href="<?php echo $this->webroot . 'VideoLibrary/index/' . $user_account['Account']['id']; ?>" data-account_id="<?php echo $user_account['Account']['id']; ?>" data-parent_id="<?php echo $user_account['Account']['parent_account_id']; ?>" class="bold active"><?php echo $user_account['Account']['company_name']; ?></a>
                                </li>
                                <?php
                            } else {
                                ?>
                                <li class="js-show-all-videos all_color">
                                    <a href="<?php echo $this->webroot . 'VideoLibrary/index/' . $user_account['Account']['id']; ?>" data-account_id="<?php echo $user_account['Account']['id']; ?>" data-parent_id="<?php echo $user_account['Account']['parent_account_id']; ?>" class="bold"><?php echo $user_account['Account']['company_name']; ?></a>
                                </li>
                                <?php
                            }
                        endforeach;
                        ?>

                    </ul>
                <?php endif; ?>
            <?php endif; ?>
            <h4 class="videos-library__sidebar-title"><?php echo $language_based_content['uncatergorized_vl']; ?></h4>
            <ul class="videos-library__sidebar-categories" style="max-height: 400px;overflow: hidden;overflow-y: auto;">
                <li class="js-show-all-videos all_color"><a href="javascript:setAllUncat()" class="allVidSubject bold"><?php echo $language_based_content['all_vl']; ?> (<?php echo $uncat_videos; ?>)</a></li>
            </ul>
            <h4 class="videos-library__sidebar-title"><?php echo $language_based_content['subject_vl']; ?></h4>
            <ul class="videos-library__sidebar-categories" style="max-height: 400px;overflow: hidden;overflow-y: auto;">
                <li class="js-show-all-videos all_color"><a href="javascript:setAllSubject()" class="allVidSubject bold"><?php echo $language_based_content['all_vl']; ?> (<?php echo $subject_videos; ?>)</a></li>
                <?php
                if (is_array($subjects) && count($subjects) > 0) {
                    foreach ($subjects as $subject) {
                        ?>
                        <li>
                            <a class="vidCategsSubject" href="javascript:getSubjectVideos('<?php echo $subject['subject_id']; ?>',0,<?php echo $rows; ?>,<?php echo $account_info['Account']['id'] ?>)">
                                <?php echo $subject['name'] . " (" . $subject['count'] . ")"; ?>
                            </a>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
            <h4><?php echo $language_based_content['topics_vl']; ?> </h4>
            <ul class="videos-library__sidebar-categories" style="max-height:400px;overflow: hidden;overflow-y: auto;">
                <li class="js-show-all-videos all_color"><a href="javascript:setAllTopic()" class="allVidTopic bold"><?php echo $language_based_content['all_vl']; ?> (<?php echo $total_topics; ?>)</a></li>
                <?php
                if (is_array($topics) && count($topics) > 0) {
                    foreach ($topics as $topic) {
                        ?>
                        <li>
                            <a class="vidCategsTopic" href="javascript:getTopicVideos('<?php echo $topic['topic_id']; ?>',0,<?php echo $rows; ?>,<?php echo $account_info['Account']['id']; ?>)">
                                <?php echo $topic['name'] . " (" . $topic['count'] . ")"; ?>
                            </a>
                        </li>
                        <?php
                    }
                }
                ?>

            </ul>
            <div id="clearAllFilter" style="display: none;">
                <a id="clearFilter" class="js-clear-filter videos-library__sidebar-clear-filter" onclick="return false;" href="javascript:void(0);"><span>Clear all filter</span></a>
            </div>
        </aside>
        <div class="videos-library__content">

            <div class="videos-library__content-header">
                <?php if ($user_permissions['UserAccount']['permission_video_library_upload'] == '1' && empty($selected_account) && $video_upload_button): ?>
                    <div class="header__nav" style="margin-left:12px;">
                        <a id ="upload_video_library" href="<?php //echo $this->webroot . 'VideoLibrary/add';                              ?>javascript:void(0)" class="btn btn-orange icon2-video" style="background-color: #ee9828; border-color:#ee9828; "><?php echo $language_based_content['upload_video_library_btn_vl']; ?></a>
                    </div>
                <?php endif; ?>
                <div class="videos-library__search search-form">
                    <input class="search-form__btn" type="image" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/serch_icn.png'); ?>">
                    <input id="videoLibrarySearch" class="search-form__input" type="text" placeholder="<?php echo $language_based_content['search_videos_vl']; ?>">
                    <span id="clearSearchVideoLibrary"  style="display: none;" class="clear-video-input-box cross-search search-x-videolibrary">X</span>
                </div>

                <?php
                $title = 'Video';
                if ($total_videos != 1) {
                    $title = 'Videos';
                }
                ?>

                <h2 id='total_video_label' class="videos-library__content-header-title"><?php echo $total_videos . "&nbsp;" . $title; ?></h2>

                <input id="searchVideoSubjectID" type="hidden" value="-1">
                <input id="searchVideoTopicID" type="hidden" value="-1">
                <input id="searchVideoSubjectDomain" type="hidden" value="n">
                <input id="searchVideoSubjectRecent" type="hidden" value="n">
                <input id="videoLibrary" type="hidden" value="1"/>
            </div>
            <p class="videos-library__filter text-right">
                <a id="most_recent" href="javascript:void(0);" class="js-show-most-recent active"><?php echo $language_based_content['most_recent_vl']; ?></a>
                <a id="most_viewed" href="javascript:void(0);" class="js-show-most-watched"><?php echo $language_based_content['most_watched_vl']; ?></a>
            </p>
            <ul class="videos-library__list videos-library__list--videos">
                <?php
                $video_inreview = array();
                ?>
                <?php foreach ($get_videos as $videos) { ?>
                    <?php
                    $transcoding_status = $this->Custom->transcoding_status($videos['document_id']);
                    $video_library_id = $videos['id'];
                    $videoFilePath = pathinfo($videos['video_url']);
                    $videoFileName = $videoFilePath['filename'];

                    $commentDate = $videos['created_date'];
                    $commentsDate = $this->Custom->formateDate($commentDate);
                    ?>
                    <li class="videos-library__list-item" data-itemid="<?php echo $video_library_id ?>">
                        <div class="video-library-header">
                            <h4 class="videos-library__list-title" ><?php echo (strlen($videos['video_title']) > '40') ? '<a rel="tooltip"  data-original-title="' . $videos['video_title'] . '" href="javascript:void(0);" >' . $videos['video_title'] . "..." . "</a>" : $videos['video_title']; ?></h4>
                        </div>
                        <div class="videos-library__list-thumb" id="published-message-<?php echo $videos['document_id']; ?>">
                            <a class="videos-library__list-item-play" href="<?php echo $this->base . "/VideoLibrary/view/" . $video_library_id . "/" . $account_info['Account']['id']; ?>"></a>
                            <span class="videos-library__list-item-time" style="visibility:hidden;">33:10</span>
                            <?php
                            if (!isset($videos['url']) || empty($videos['url']))
                                $videos['url'] = $videos['video_url'];

                            $videos['id'] = $videos['document_id'];

                            $document_files_array = $this->Custom->get_document_url($videos);

                            if (empty($document_files_array['url'])) {
                                $videos['published'] = 0;
                                $document_files_array['url'] = $videos['original_file_name'];
                                $videos['encoder_status'] = $document_files_array['encoder_status'];
                            } else {
                                $videos['encoder_status'] = $document_files_array['encoder_status'];
                                @$row['Document']['duration'] = $document_files_array['duration'];
                            }

                            if (isset($videos['published']) && $videos['published'] == '1' && $transcoding_status != 5) :
                                $video_url = $document_files_array['url'];
                                $thumbnail_image_path = $document_files_array['thumbnail'];

                                $seconds = $row['Document']['duration'] % 60;
                                $minutes = ($row['Document']['duration'] / 60) % 60;
                                $hours = gmdate("H", $row['Document']['duration']);
                                ?>
                                <img src="<?php echo $thumbnail_image_path; ?>" width="225" height="148" alt=""/>
                                <div style="font-size: 12px;color: white;background: rgba(0,0,0, 0.9);position: absolute;top: 128px;right: 0px;padding: 2px;border-radius: 3px;"><?php printf("%02d:%02d:%02d", $hours, $minutes, $seconds); ?></div>
                            <?php else: ?>
                                <?php if ($videos['encoder_status'] == 'Error' || $transcoding_status == 5): ?>
                                    <div class="video-unpublished-videolib"><span>Video failed to process successfully. Please try again or contact <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['support']; ?>" style="color: blue;text-decoration: underline;"><?php echo $this->custom->get_site_settings('static_emails')['support']; ?></a>.</span></div>
                                <?php else : ?>
                                    <?php $video_inreview[] = $videos['document_id']; ?>
                                    <div class="video-unpublished-videolib" id="processing-message-<?php echo $videos['document_id']; ?>">
                                        <span class="video-unpublished" style="margin-top: 27px;">
                                            <img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;" /><br><?=$alert_messages["Your_video_is_currently_processing"]; ?>
                                        </span>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                        <div class="videos-library__list-footer">
                            <span class="videos-library__list-views"><?php echo $videos['view_count'] ?> <?php echo $language_based_content['views_vl']; ?></span>
                            <?php $extraPermission = (int) $user_permissions['UserAccount']['permission_video_library_upload']; ?>
                            <?php $isCreator = $this->Custom->is_creator($user_current_account['User']['id'], $videos['created_by']); ?>
                            <?php
                            
                           

                            if ($videos['published'] == '1' && (
                                    ($user_permissions['roles']['role_id'] == '100' || $user_permissions['roles']['role_id'] == '110' ||$user_permissions['roles']['role_id'] == '115' || $user_permissions['roles']['role_id'] == '120') && ($extraPermission))):
                                ?>
                                <?php
                                $filePath = $document_files_array['url'];
                                ?>
                                <?php $downloadUrl = $filePath; ?>
                                <?php if($login_account_id == $account_id):?>                                                                                                                                                                                <!--<a href="<?php echo $downloadUrl; ?>" target="_blank" download="<?php echo $videoFileName . "_enc.mp4"; ?>">-->
                                <a href="<?php echo $this->webroot . 'Huddles/download/' . $videos['document_id'] ?>">
                                    <img alt="Download" class="right videos-library__list-download" height="14" rel="tooltip" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download.png'); ?>" title="<?php echo $language_based_content['download_video_vl']; ?>" width="14" />
                                </a>
                            <?php endif;?>

                            <?php elseif ($videos['published'] == '1' && (($user_permissions['roles']['role_id'] == '120'||$user_permissions['roles']['role_id'] == '110') && ($extraPermission))): ?>
                                <?php
                                $filePath = $document_files_array['url'];
                                ?>
                                <?php $downloadUrl = $filePath; ?>
                                <?php if($login_account_id == $account_id):?>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              <!--<a href="<?php echo $downloadUrl; ?>" target="_blank" download="<?php echo $videoFileName . "_enc.mp4"; ?>">-->
                                    <a href="<?php echo $this->webroot . 'Huddles/download/' . $videos['document_id'] ?>">
                                        <img alt="Download" class="right videos-library__list-download" height="14" rel="tooltip" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/download.png'); ?>" title="<?php echo $language_based_content['download_video_vl']; ?>" width="14" />
                                    </a>
                                <?php endif; ?>
                            <?php endif; ?>

                            <span class="videos-library__list-date"><?php echo $commentsDate; ?></span>
                        </div>
                    </li>

                <?php } ?>

            </ul>
            <p class="text-center" style="display:<?php echo ($remaining_videos > 0 ? 'block' : 'none'); ?>">
                <?php
                if ($remaining_videos > 0) {
                    ?>
                    <a id="loadMoreBtn" class="btn btn-green" href="javascript:LoadMoreVideos(<?php echo $rows; ?>,<?php echo $rows; ?>,'all');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>
                    <?php
                }
                ?>
            </p>
        </div>
    </div>
</div>

<script type="text/javascript">
    function doLibrarySearchAjax() {
        $('#searchVideoSubjectID').val(-1);
        $('#searchVideoTopicID').val(-1);
        $('#searchVideoSubjectDomain').val('title');
        $('#clearSearchVideoLibrary').css('display', 'block');

        $('.vidCategsTopic').each(function () {
            $(this).removeClass('active');
        });
        $('.allVidTopic').removeClass('active');

        $('.vidCategsSubject').each(function () {
            $(this).removeClass('active');
        });
        $('.allVidSubject').removeClass('active');

        vidTitle = ($('#videoLibrarySearch').val() == '') ? 'e1a2c3d4b5' : $('#videoLibrarySearch').val();
        order = $('#searchVideoSubjectRecent').val();
        load_publish_videos(vidTitle, order, 0,<?php echo $rows; ?>,<?php echo $account_info['Account']['id']; ?>);
    }
    (function poll() {

        setTimeout(function () {
            var processing_video_ids = [];
            $('[id^=processing-message] .video-unpublished').each(function (idx, el) {
                var parent = $(this).parent();
                var id = parent.attr('id');
                id = parseInt(id.split('-')[2]);
                processing_video_ids.push(id);
            });
            $.ajax({
                url: home_url + '/Huddles/sysRefreash',
                type: 'post',
                data: {
                    processing_video_ids: processing_video_ids
                },
                dataType: 'json'
            }).success(function (res) {
                //console.log( res );
                if (res.ok && res.data != undefined) {
                    doLibrarySearchAjax();
                    for (var id in res.data) {
                        var image = res.data[id];
                        $('#processing-message-' + id).html(image).append('<div class="play-icon" style="background-size: 50px;height: 50px;margin: -60px 0 0 -25px;width: 50px;"></div>');
                    }
                }
            }).always(function () {
                poll();
            });
        }, 30000);
    })();



    function load_publish_videos(vidTitle, order, start, rows, account_id) {
        $('#searchVideoSubjectDomain').val('title');
        // $(".videos-library__content").mask("Please wait...");
        $.ajax({
            dataType: 'JSON',
            type: 'POST',
            url: '<?php echo $this->base ?>/VideoLibrary/getVideosByTitle/' + vidTitle + '/' + order + '/' + start + '/' + rows + '/' + account_id,
            success: function (response) {
                //  $(".videos-library__content").unmask();
                if (start == 0) {
                    $('.videos-library__list').html(response.contents);
                    $('#total_video_label').html(response.total_count + ' Videos');
                } else {
                    $('.videos-library__list').prepend(response.contents);
                }


                if (parseInt(response.remaining) >= 0) {
                    if ($('#searchVideoSubjectID').val() == -1) {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' + (start + rows) + ',' +<?php echo $rows; ?> + ',' + "'all'" + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    } else {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:getVideoByTitle(' + '\'' + vidTitle + '\'' + ',' + '\'' + order + '\'' + ',' + (start + rows) + ',' +<?php echo $rows; ?> + ',' +<?php echo $account_info['Account']['id']; ?> + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    }
                    $('p.text-center').css('display', 'block');
                } else {
                    $('p.text-center').css('display', 'none');
                    $('p.text-center').css('display', 'none');
                }
                if (parseInt(response.total_count) <= 0) {
                    if ($('#searchVideoSubjectID').val() == -1) {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' + (start + rows) + ',' +<?php echo $rows; ?> + ',' + "'all'" + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    } else {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:getVideoByTitle(' + '\'' + vidTitle + '\'' + ',' + '\'' + order + '\'' + ',' + (start + rows) + ',' +<?php echo $rows; ?> + ',' +<?php echo $account_info['Account']['id']; ?> + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    }
                    $('p.text-center').css('display', 'none');
                }

                if (response.total_count == 0) {
                    $('p.text-center').append('<?php echo $language_based_content['no_videos_matched_this_search']; ?>');
                    $('p.text-center').css('display', 'block');
                    $('p.text-center .btn-green').css('display', 'none');
                }

            },
            errors: function (response) {
                alert(response.contents);
            }
        })
    }

    function LoadMoreVideos(start, rows, vtype) {
        if (vtype != '') {
            vtype = vtype;
        } else {
            vtype = 'all'
        }

        $.ajax({
            dataType: 'JSON',
            type: 'POST',
            url: '<?php echo $this->base ?>/VideoLibrary/getAllVideos/' + start + '/' + rows + '/' + $('#searchVideoSubjectRecent').val() + '/' +<?php echo $account_info['Account']['id']; ?> + '/' + vtype,
            success: function (response) {
                if (start == 0) {
                    $('.videos-library__list').html(response.contents);
                } else {
                    $('.videos-library__list').prepend(response.contents);
                }
                if (parseInt(response.remaining) >= 0) {
                    $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' + (start + rows) + ',' +<?php echo $rows; ?> + ',' + "'" + vtype + "'" + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    $('p.text-center').css('display', 'block');
                } else {
                    $('p.text-center').css('display', 'none');
                    $('p.text-center').css('display', 'none');
                }
                if (parseInt(response.total_count) <= 0) {
                    $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' + (start + rows) + ',' +<?php echo $rows; ?> + ',' + "'" + vtype + "'" + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    $('p.text-center').css('display', 'none');
                }



            },
            errors: function (response) {
                alert(response.contents);
            }
        })
    }

    function getUncatVideos(subjectID, start, rows) {

        $('#clearAllFilter').show();
        $('#searchVideoSubjectID').val(subjectID);

        $('#searchVideoTopicID').val(-1);
        topicID = $('#searchVideoTopicID').val();

        $('.vidCategsTopic').each(function () {
            $(this).removeClass('active');
        });

        $('.allVidTopic').addClass('active');

        $('#searchVideoSubjectDomain').val('subject');
        sOrder = $('#searchVideoSubjectRecent').val();
        $('#clearAllFilter').show();
        $(".videos-library__content").mask("<?php echo $language_based_content['please_wait_library']; ?>");
        $.ajax({
            dataType: 'JSON',
            type: 'POST',
            url: '<?php echo $this->base ?>/VideoLibrary/getUncatVideos/' + subjectID + '/' + sOrder + '/' + start + '/' + rows + '/' + topicID + '/' + <?php echo!empty($selected_account) ? $selected_account : $account_info['Account']['id']; ?>,
            success: function (response) {
                $(".videos-library__content").unmask();
//                console.log('start  = '+start);
                if (start == 0) {
                    $('.videos-library__list').html(response.contents);
                    $('#total_video_label').html(response.total_count + ' Videos');
                } else {
                    $('.videos-library__list').prepend(response.contents);
                }


//                console.log('response.total_count  = '+response.total_count);
//                console.log('response.remaining  = '+response.remaining);
                if (parseInt(response.remaining) >= 0) {
                    if ($('#searchVideoSubjectID').val() == -1) {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' + (start + rows) + ',' +<?php echo $rows; ?> + ',' + "'uncat'" + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    } else {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:getSubjectVideos(' + subjectID + ',' + (start + rows) + ',' +<?php echo $rows; ?> + ',' + <?php echo $account_info['Account']['id'] ?> + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    }
                    $('p.text-center').css('display', 'block');
                } else {
                    $('p.text-center').css('display', 'none');
                    $('p.text-center').css('display', 'none');
                }
                if (parseInt(response.total_count) <= 0) {
                    if ($('#searchVideoSubjectID').val() == -1) {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' + (start + rows) + ',' +<?php echo $rows; ?> + ',' + "'uncat'" + ');">Load more videos...</a>');
                    } else {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:getSubjectVideos(' + subjectID + ',' + (start + rows) + ',' +<?php echo $rows; ?> + ',' + <?php echo $account_info['Account']['id'] ?> + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    }
                    $('p.text-center').css('display', 'none');
                }

            },
            errors: function (response) {
                alert(response.contents);
            }
        })
    }

    function getSubjectVideos(subjectID, start, rows, account_id) {

        $('#clearAllFilter').show();
        $('#searchVideoSubjectID').val(subjectID);

        $('#searchVideoTopicID').val(-1);
        topicID = $('#searchVideoTopicID').val();

        $('.vidCategsTopic').each(function () {
            $(this).removeClass('active');
        });

        $('.allVidTopic').addClass('active');

        $('#searchVideoSubjectDomain').val('subject');
        sOrder = $('#searchVideoSubjectRecent').val();
        $('#clearAllFilter').show();
        $(".videos-library__content").mask("<?php echo $language_based_content['please_wait_library']; ?>");
        $.ajax({
            dataType: 'JSON',
            type: 'POST',
            url: '<?php echo $this->base ?>/VideoLibrary/getSubjectVideos/' + subjectID + '/' + sOrder + '/' + start + '/' + rows + '/' + topicID + '/' + account_id,
            success: function (response) {
                $(".videos-library__content").unmask();
//                console.log('start  = '+start);
                if (start == 0) {
                    $('.videos-library__list').html(response.contents);
                    $('#total_video_label').html(response.total_count + ' Videos');
                } else {
                    $('.videos-library__list').prepend(response.contents);
                }


//                console.log('response.total_count  = '+response.total_count);
//                console.log('response.remaining  = '+response.remaining);
                if (parseInt(response.remaining) >= 0) {
                    if ($('#searchVideoSubjectID').val() == -1) {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' + (start + rows) + ',' +<?php echo $rows; ?> + ',' + "'subject'" + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    } else {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:getSubjectVideos(' + subjectID + ',' + (start + rows) + ',' +<?php echo $rows; ?> + ',' +<?php echo $account_info['Account']['id']; ?> + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    }
                    $('p.text-center').css('display', 'block');
                } else {
                    $('p.text-center').css('display', 'none');
                    $('p.text-center').css('display', 'none');
                }
                if (parseInt(response.total_count) <= 0) {
                    if ($('#searchVideoSubjectID').val() == -1) {
//                                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' + (start + rows) + ',' +<?php echo $rows; ?> + ',' +<?php echo $account_info['Account']['id'] ?> + ');">Load more videos...</a>');
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' + (start + rows) + ',' +<?php echo $rows; ?> + ',' + "'subject'" + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    } else {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:getSubjectVideos(' + subjectID + ',' + (start + rows) + ',' +<?php echo $rows; ?> + ',' +<?php echo $account_info['Account']['id'] ?> + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    }
                    $('p.text-center').css('display', 'none');
                }

            },
            errors: function (response) {
                alert(response.contents);
            }
        })
    }

    function getTopicVideos(topicID, start, rows, account_id) {

        $('#clearAllFilter').show();
        $('#searchVideoTopicID').val(topicID);

        $('#searchVideoSubjectID').val(-1);
        subjectID = $('#searchVideoSubjectID').val();

        $('.vidCategsSubject').each(function () {
            $(this).removeClass('active');
        });

        $('.allVidSubject').addClass('active');

        $('#searchVideoSubjectDomain').val('topic');
        tOrder = $('#searchVideoSubjectRecent').val();
        $(".videos-library__content").mask("<?php echo $language_based_content['please_wait_library']; ?>");
        $.ajax({
            dataType: 'JSON',
            type: 'POST',
            url: '<?php echo $this->base ?>/VideoLibrary/getTopicVideos/' + topicID + '/' + tOrder + '/' + start + '/' + rows + '/' + subjectID + '/' + account_id,
            success: function (response) {
                $(".videos-library__content").unmask();

                if (start == 0) {
                    $('.videos-library__list').html(response.contents);
                    $('#total_video_label').html(response.total_count + ' Videos');
                } else {
                    $('.videos-library__list').prepend(response.contents);
                }



                if (parseInt(response.remaining) >= 0) {
                    if ($('#searchVideoTopicID').val() == -1) {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' + (start + rows) + ',' +<?php echo $rows; ?> + ',' + "'topic'" + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    } else {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:getTopicVideos(' + topicID + ',' + (start + rows) + ',' +<?php echo $rows; ?> + ',' +<?php echo $account_info['Account']['id']; ?> + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    }
                    $('p.text-center').css('display', 'block');
                } else {
                    $('p.text-center').css('display', 'none');
                    $('p.text-center').css('display', 'none');
                }

                if (parseInt(response.total_count) <= 0) {
                    if ($('#searchVideoTopicID').val() == -1) {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' + (start + rows) + ',' +<?php echo $rows; ?> + ',' + "'topic'" + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    } else {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:getTopicVideos(' + topicID + ',' + (start + rows) + ',' +<?php echo $rows; ?> + ',' +<?php echo $account_info['Account']['id']; ?> + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    }
                    $('p.text-center').css('display', 'none');
                }


            },
            errors: function (response) {
                alert(response.contents);
            }
        })
    }
    function checkPublishLibrary() {

    }

    function getVideoByTitle(vidTitle, order, start, rows, account_id) {

        $('#searchVideoSubjectDomain').val('title');

        $(".videos-library__content").mask("<?php echo $language_based_content['please_wait_library']; ?>");
        $.ajax({
            dataType: 'JSON',
            type: 'POST',
            url: '<?php echo $this->base ?>/VideoLibrary/getVideosByTitle/' + vidTitle + '/' + order + '/' + start + '/' + rows + '/' + account_id,
            success: function (response) {
                $(".videos-library__content").unmask();
                if (start == 0) {
                    $('.videos-library__list').html(response.contents);
                    $('#total_video_label').html(response.total_count + ' Videos');
                } else {
                    $('.videos-library__list').prepend(response.contents);
                }


                if (parseInt(response.remaining) >= 0) {
                    if ($('#searchVideoSubjectID').val() == -1) {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' + (start + rows) + ',' +<?php echo $rows; ?> + ',' + "'all'" + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    } else {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:getVideoByTitle(' + '\'' + vidTitle + '\'' + ',' + '\'' + order + '\'' + ',' + (start + rows) + ',' +<?php echo $rows; ?> + ',' +<?php echo $account_info['Account']['id']; ?> + ');"><?php echo $language_based_content['load_more_videos_vl']; ?>.</a>');
                    }
                    $('p.text-center').css('display', 'block');
                } else {
                    $('p.text-center').css('display', 'none');
                }
                if (parseInt(response.total_count) <= 0) {
                    if ($('#searchVideoSubjectID').val() == -1) {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' + (start + rows) + ',' +<?php echo $rows; ?> + ',' + "'all'" + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    } else {
                        $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:getVideoByTitle(' + '\'' + vidTitle + '\'' + ',' + '\'' + order + '\'' + ',' + (start + rows) + ',' +<?php echo $rows; ?> + ',' +<?php echo $account_info['Account']['id']; ?> + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    }
                    $('p.text-center').css('display', 'none');
                }

                if (response.total_count == 0) {
                    $('p.text-center').append('<?php echo $language_based_content['no_videos_matched_this_search']; ?>');
                    $('p.text-center').css('display', 'block');
                    $('p.text-center .btn-green').css('display', 'none');
                }

            },
            errors: function (response) {
                alert(response.contents);
            }
        })
    }

//                    $('#clearSearchVideoLibrary').click(function() {
    $(document).on('click', '#clearSearchVideoLibrary', function () {

        $('#videoLibrarySearch').val("");
        $('#clearSearchVideoLibrary').css('display', 'none');
        vidTitle = ($('#videoLibrarySearch').val() == '') ? 'e1a2c3d4b5' : $('#videoLibrarySearch').val();
        order = $('#searchVideoSubjectRecent').val();
        getVideoByTitle(vidTitle, order, 0,<?php echo $rows; ?>,<?php echo $account_info['Account']['id']; ?>);

    });

    $('#videoLibrarySearch').keypress(function (e) {
//                    $(document).on('keypress', '#videoLibrarySearch', function(e) {
        if (e.which == 13) {

            $('#searchVideoSubjectID').val(-1);
            $('#searchVideoTopicID').val(-1);
            $('#searchVideoSubjectDomain').val('title');
            $('#clearSearchVideoLibrary').css('display', 'block');

            $('.vidCategsTopic').each(function () {
                $(this).removeClass('active');
            });
            $('.allVidTopic').removeClass('active');

            $('.vidCategsSubject').each(function () {
                $(this).removeClass('active');
            });
            $('.allVidSubject').removeClass('active');

            vidTitle = ($('#videoLibrarySearch').val() == '') ? 'e1a2c3d4b5' : $('#videoLibrarySearch').val();
            order = $('#searchVideoSubjectRecent').val();
            getVideoByTitle(vidTitle, order, 0,<?php echo $rows; ?>,<?php echo $account_info['Account']['id']; ?>);

        }
    });

//                    $('#clearFilter').click(function() {
    $(document).on('click', '#clearFilter', function () {
        $('#searchVideoSubjectID').val(-1);
        $('#searchVideoSubjectDomain').val('n');
        $('#searchVideoSubjectRecent').val('n');
        $('#videoLibrarySearch').val('');
        $('#most_recent').css('color', '#a4a4a4');

        $('.vidCategsSubject').each(function () {
            $(this).removeClass('active');
        });

        $('.vidCategsTopic').each(function () {
            $(this).removeClass('active');
        });


        $('.allVidSubject').addClass('active');
        $('.allVidTopic').addClass('active');
        $(".videos-library__content").mask("<?php echo $language_based_content['please_wait_library']; ?>");
        $.ajax({
            dataType: 'JSON',
            type: 'POST',
            url: '<?php echo $this->base ?>/VideoLibrary/getAllVideos/0' + '/' + <?php echo $rows; ?> + '/n/' +<?php echo $account_info['Account']['id']; ?>,
            success: function (response) {
                $(".videos-library__content").unmask();
                $('.videos-library__list').html(response.contents);

                $('#total_video_label').html(response.total_count + ' Videos');
                if (parseInt(response.remaining) >= 0) {
                    $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' +<?php echo $rows; ?> + ',' +<?php echo $rows; ?> + ',' + "'all'" + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    $('p.text-center').css('display', 'block');
                } else {
                    $('p.text-center').css('display', 'none');
                    $('p.text-center').css('display', 'none');
                }
                if (parseInt(response.total_count) <= 0) {
                    $('p.text-center').html('<a class="btn btn-green" id="loadMoreBtn" href="javascript:LoadMoreVideos(' +<?php echo $rows; ?> + ',' +<?php echo $rows; ?> + ',' + "'all'" + ');"><?php echo $language_based_content['load_more_videos_vl']; ?></a>');
                    $('p.text-center').css('display', 'none');
                }

                $('#clearAllFilter').css('display', 'none');
            },
            errors: function (response) {
                alert(response.contents);
            }
        })

    });

//                    $('.vidCategsSubject').click(function() {
    $(document).on('click', '.vidCategsSubject', function () {
        $('.allVidSubject').removeClass('active');
        $('.vidCategsSubject').each(function () {
            $(this).removeClass('active');
        });

        $(this).addClass('active');
    });

//                    $('.vidCategsTopic').click(function() {
    $(document).on('click', '.vidCategsTopic', function () {
        $('.allVidTopic').removeClass('active');
        $('.vidCategsTopic').each(function () {
            $(this).removeClass('active');
        });

        $(this).addClass('active');
    });

//                    $('#most_recent').click(function() {
    $(document).on('click', '#most_recent', function () {
        $(this).css('color', '#333');
        $('#most_viewed').css('color', '#a4a4a4');
        $('#searchVideoSubjectRecent').val(' account_folders.created_date');

        if ($('#searchVideoSubjectDomain').val() == 'title') {

            vidTitle = ($('#videoLibrarySearch').val() == '') ? 'e1a2c3d4b5' : $('#videoLibrarySearch').val();
            getVideoByTitle(vidTitle, ' account_folders.created_date', 0,<?php echo $rows; ?>,<?php echo $account_info['Account']['id']; ?>);

        } else if ($('#searchVideoSubjectDomain').val() == 'topic') {
            getTopicVideos($('#searchVideoTopicID').val(), 0,<?php echo $rows; ?>,<?php echo $account_info['Account']['id']; ?>);
        } else if ($('#searchVideoSubjectDomain').val() == 'subject') {
            getSubjectVideos($('#searchVideoSubjectID').val(), 0,<?php echo $rows; ?>,<?php echo $account_info['Account']['id'] ?>);
        } else {
            LoadMoreVideos(0, <?php echo $rows; ?>, 'all');
        }

    });

//                    $('#most_viewed').click(function() {
    $(document).on('click', '#most_viewed', function () {
        $(this).css('color', '#333');
        $('#most_recent').css('color', '#a4a4a4');
        $('#searchVideoSubjectRecent').val('view_count');


        if ($('#searchVideoSubjectDomain').val() == 'title') {

            vidTitle = ($('#videoLibrarySearch').val() == '') ? 'e1a2c3d4b5' : $('#videoLibrarySearch').val();
            getVideoByTitle(vidTitle, 'view_count', 0,<?php echo $rows; ?>,<?php echo $account_info['Account']['id']; ?>);

        } else if ($('#searchVideoSubjectDomain').val() == 'topic') {
            getTopicVideos($('#searchVideoTopicID').val(), 0,<?php echo $rows; ?>,<?php echo $account_info['Account']['id']; ?>);
        } else if ($('#searchVideoSubjectDomain').val() == 'subject') {
            getSubjectVideos($('#searchVideoSubjectID').val(), 0,<?php echo $rows; ?>,<?php echo $account_info['Account']['id']; ?>);
        } else {
            LoadMoreVideos(0, <?php echo $rows; ?>, 'all');
        }
    });

    function setAllUncat() {

        $('#searchVideoSubjectID').val(-1);
        $('#searchVideoSubjectDomain').val('uncat');

        $('.vidCategsSubject').each(function () {
            $(this).removeClass('active');
        });

        $('.allVidSubject').addClass('active');

        getUncatVideos($('#searchVideoSubjectID').val(), 0, <?php echo $rows; ?>);
    }

    function setAllSubject() {

        $('#searchVideoSubjectID').val(-1);
        $('#searchVideoSubjectDomain').val('subject');

        $('.vidCategsSubject').each(function () {
            $(this).removeClass('active');
        });

        $('.allVidSubject').addClass('active');

        getSubjectVideos($('#searchVideoSubjectID').val(), 0, <?php echo $rows; ?>,<?php echo $account_info['Account']['id'] ?>);
    }

    function setAllTopic() {

        $('#searchVideoTopicID').val(-1);
        $('#searchVideoSubjectDomain').val('topic');

        $('.vidCategsTopic').each(function () {
            $(this).removeClass('active');
        });

        $('.allVidTopic').addClass('active');

        getTopicVideos($('#searchVideoTopicID').val(), 0, <?php echo $rows; ?>,<?php echo $account_info['Account']['id']; ?>);
    }

    $(document).on('click', '#upload_video_library', function () {


        $.ajax({
            url: '<?php echo $this->base; ?>/Huddles/check_storage_full',
            type: 'POST',
            success: function (response) {
                if (response != '') {
                    $('#flashMessage2').css('display', 'block');
                    $('#flashMessage2').html(response);

                } else {
                    var uploadPath = '<?php echo "/tempupload/" . $user_current_account['accounts']['account_id'] . "/" . date('Y') . "/" . date('m') . "/" . date('d') . "/"; ?>';

                    //  filepicker.setKey('<?php //echo Configure::read('filepicker_access_key');                              ?>');
                    var client = filestack.init('<?php echo Configure::read('filepicker_access_key'); ?>');
                    client.pick({
                        maxFiles: 1000,
                        fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive', 'video'],
                        accept: ['3gp', '3gpp', 'avi', 'divx', 'dv', 'flv', 'm4v', 'mjpeg', 'mkv', 'mod', 'mov', 'mp4', 'mpeg', 'mpg', 'm2ts', 'mts', 'mxf', 'ogv', 'wmv', 'aif', 'mp3', 'm4a', 'ogg', 'wav', 'wma'],
                        lang: '<?php echo $alert_messages['langauage_settings_alert']; ?>',
                        //extensions: ['3g2', '3gp', '3gp2', '3gpp', '3gpp2', 'aac', 'ac3', 'eac3', 'ec3', 'f4a', 'f4b', 'f4v', 'flv', 'highwinds', 'm4a', 'm4b', 'm4r', 'm4v', 'mkv', 'mov', 'mp3', 'mp4', 'oga', 'ogg', 'ogv', 'ogx', 'ts', 'webm', 'wma', 'wmv', 'divx', 'avi', 'dv', 'mjpeg', 'mpg', 'm2ts', 'mts', 'mxf']


                        storeTo: {
                            location: "s3",
                            path: uploadPath,
                            access: 'public',
                            container: bucket_name,
                            region: 'us-east-1'
                        }
                    }).then(
                            function (inkBlob) {
                                inkBlob = inkBlob.filesUploaded;
                                if (inkBlob && inkBlob.length > 0) {




                                    var blob = inkBlob[0];
                                    $.ajax({
                                        dataType: 'JSON',
                                        type: 'POST',
                                        url: '<?php echo $this->base ?>/VideoLibrary/session_values_put/',
                                        data: {key_lib: blob.key, filename_lib: blob.filename, mimetype_lib: blob.mimetype, size_lib: blob.size},
                                        success: function (response) {
                                            location.href = home_url + '/VideoLibrary/add';
                                        },
                                        errors: function (response) {
                                            alert(response.contents);
                                        }
                                    })


//                        $('#txtUploadedFilePath').val(blob.key);
//                        $('#txtUploadedFileName').val(blob.filename);
//                        $('#txtUploadedFileMimeType').val(blob.mimetype);
//                        $('#txtUploadedFileSize').val(blob.size);
//
//                        $('.VideoLibrary .cntr .upload-detail-container .inner .cnt-left .section  .upload-row .upload-row-right h2').html(blob.filename);
//                        $('.VideoLibrary .cntr .upload-detail-container .inner .cnt-left .section  .upload-row .upload-row-right h3').html('Your Video is uploaded.');
//                        $('.VideoLibrary .cntr .upload-detail-container .inner .cnt-left .section  .upload-row .upload-row-right div.bar .actualprogress').css('width', '100%');
//                        $('.VideoLibrary .cntr .upload-detail-container .inner .cnt-left .section  .upload-row .upload-row-right .progressText').html('100%');
//
//                        $('#imgSampleImage').attr('src', '<?php //echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/question_mark.png')                              ?>');
//
//                        $('.VideoLibrary .cntr .upload-detail-container').css('display', 'block');
//                        $('#filepicker_dialog_container a').trigger("click");
                                }
                            }), function () {
                        location.href = home_url + '/VideoLibrary/';
                    };
                }
            }
        });




    });

    $('#flashMessage2').click(function (e) {

        $('#flashMessage2').fadeOut();
    });

    $(document).ready(function (e) {
        console.log("dkdkd");
        var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><span href="#"><?php echo $breadcrumb_language_based_content['video_library_breadcrumb']; ?></span></div>';

        $('.breadCrum').html(bread_crumb_data);

    });


    $(document).on("click", "div.fsp-source-list__item:nth-last-child(1)", function () {
        alert("<?php echo $alert_messages['warning_if_your_internet_is_unstable']; ?>");
    });
</script>
