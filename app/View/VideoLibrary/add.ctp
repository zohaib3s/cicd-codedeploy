<style type="text/css">
    #txtVideoTags_tagsinput {
        border-left: 1px solid #CCC;
        border-bottom: 1px solid #CCC;
        width: 450px !important;
    }
</style>
<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<div class="VideoLibrary">
    <div class="cntr">

        <div class="top-head">
            <h1><?php echo $language_based_content['add_to_video_library_vl']; ?></h1>
        </div>

        <div id="videoLibraryComponent">

            <div class="upload-container" style="display:none">
                <div class="cross" onclick="location.href = '<?php $this->base . '/VideoLibrary/'; ?>';">&nbsp;</div>
                <div class="inner">
                    <ul class="mnu">
                        <li class="mycomputer active">My Computer</li>
                        <li class="googledrive">Google Drive</li>
                        <li class="dropbox">Dropbox</li>
                    </ul>
                    <div class="dropzone-box fade well accordion-panels" style="display:none;">Drop files here <input id="fileupload" type="file" name="files[]"/></div>
                </div>
            </div>

            <div class="upload-detail-container" style="">
                <form id="frmVideoLibrary" name="frmVideoLibrary">
                    <div class="inner">
                        <div class="cnt-left">
                            <div class="section">
                                <div class="upload-row">
                                    <div class="upload-row-left">
                                        <img id="imgSampleImage" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/videolibrary/sample-video-snapshot.jpg'); ?>"/>
                                    </div>
                                    <div class="upload-row-right">
                                        <h2><?php echo $language_based_content['video_title_goes_here']; ?></h2>
                                        <h3><?php echo $language_based_content['uploading_your_video_library']; ?></h3>
                                        <div class="bar">
                                            <div class="actualprogress" style="background:<?php echo $this->Custom->get_site_settings('primary_bg_color') ?>"></div>
                                        </div>
                                        <span class="progressText">0%</span>
                                    </div>
                                    <input id="txtUploadedFilePath" type="hidden" value="" />
                                    <input id="txtUploadedFileName" type="hidden" value="" />
                                    <input id="txtUploadedFileMimeType" type="hidden" value="" />
                                    <input id="txtUploadedFileSize" type="hidden" value="" />
                                    <input id="txtRandomNumber" type="hidden" value="<?php echo strtotime('now'); ?>" />
                                </div>
                                <div class="video-title-row row">
                                    <input type="text" name="txtVideoTitle" id="txtVideoTitle" value="" placeholder="<?php echo $language_based_content['video_title_vl']; ?>" required/>
                                </div>
                                <div class="video-description-row row">
                                    <textarea type="text" name="txtVideoDescription" id="txtVideoDescription" placeholder="<?php echo $language_based_content['video_description_vl']; ?>"  ></textarea>
                                </div>
                                <div class="video-tags-row row">
                                    <label><?php echo $language_based_content['tags_vl']; ?>:</label>
                                    <input type="text" name="txtVideoTags" id="txtVideoTags" value="" placeholder="" />
                                </div>

                            </div>
                        </div>
                        <div class="cnt-right">
                            <div class="cross" onclick="location.href = '<?php echo $this->base . '/VideoLibrary/'; ?>';">&nbsp;</div>

                            <div class="section">

                                <div id="cmbSubjectsRow" class="subject-row row">
                                    <span class="label"><?php echo $language_based_content['choose_subject_vl']; ?></span>
                                    <div id="cmbSubjects" class="video-dropdown"><?php echo $language_based_content['subject_name_vl']; ?></div>
                                    <div class="video-dropdown-container">
                                        <div class="wrapper">
                                            <div class="container">
                                                <?php if ($subjects): ?>
                                                    <?php foreach ($subjects as $subject): ?>

                                                        <div id="chkContainer<?php echo $subject['Subject']['id'] ?>" class="check-container">
                                                            <input id="subject_ids<?php echo $subject['Subject']['id'] ?>" name="subject_ids<?php echo $subject['Subject']['id'] ?>" class="checkbox-3 subject_checkbox" type="checkbox" value="<?php echo $subject['Subject']['id'] ?>">

                                                            <label for="subject_ids<?php echo $subject['Subject']['id'] ?>" class="title"><?php echo $subject['Subject']['name'] ?></label>
                                                            <input type="text" id="txtsubject_ids<?php echo $subject['Subject']['id'] ?>" class="edit-field" value="<?php echo $subject['Subject']['name'] ?>"/>
                                                            <div class="controls controls<?php echo $subject['Subject']['name'] ?>">
                                                                <?php
                                                                $imgedit_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/playlist8x8.png');
                                                                $imgdel_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/delete-icon8x9.png');
                                                                $imgsave_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/save8x8.png');
                                                                $imgundo_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/undo8x8.png');
                                                                ?>
                                                                <?php echo $this->Html->image($imgedit_path, array('id' => "imgEdit" . $subject['Subject']['id'], 'class' => 'edit-controls edit-controls' . $subject['Subject']['id'])); ?>
                                                                <?php echo $this->Html->image($imgdel_path, array('id' => "imgDelete" . $subject['Subject']['id'], 'class' => 'edit-controls edit-controls' . $subject['Subject']['id'])); ?>
                                                                <?php echo $this->Html->image($imgsave_path, array('id' => "imgSave" . $subject['Subject']['id'], 'class' => 'save-controls save-controls' . $subject['Subject']['id'])); ?>
                                                                <?php echo $this->Html->image($imgundo_path, array('id' => "imgUndo" . $subject['Subject']['id'], 'class' => 'save-controls save-controls' . $subject['Subject']['id'])); ?>
                                                            </div>

                                                            <script type="text/javascript">

                                                                $(document).ready(function () {

                                                                    $('.save-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                                                                    $('#imgEdit<?php echo $subject['Subject']['id'] ?>').click(function () {

                                                                        $('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').css('display', 'none');
                                                                        $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').css('display', 'block');

                                                                        $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').val($('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').html());

                                                                        $('.save-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'block');

                                                                        $('.edit-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'none');
                                                                    });

                                                                    $('#imgUndo<?php echo $subject['Subject']['id'] ?>').click(function () {

                                                                        $('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').css('display', 'block');
                                                                        $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                                                                        $('.save-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                                                                        $('.edit-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'block');
                                                                    });

                                                                    $('#imgSave<?php echo $subject['Subject']['id'] ?>').click(function () {

                                                                        var subject_id = $(this).attr('id').replace('imgSave', '');

                                                                        $.ajax({
                                                                            type: 'POST',
                                                                            data: {
                                                                                subject: $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').val()
                                                                            },
                                                                            url: home_url + '/VideoLibrary/updateSubject/' + subject_id,
                                                                            success: function (response) {

                                                                                $('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').html($('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').val());

                                                                                $('label[for="subject_ids<?php echo $subject['Subject']['id'] ?>"]').css('display', 'block');
                                                                                $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                                                                                $('.save-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'none');

                                                                                $('.edit-controls<?php echo $subject['Subject']['id'] ?>').css('display', 'block');

                                                                            },
                                                                            errors: function (response) {
                                                                                alert(response.contents);
                                                                            }

                                                                        });

                                                                    });

                                                                    $('#imgDelete<?php echo $subject['Subject']['id'] ?>').click(function () {

                                                                        var subject_id = $(this).attr('id').replace('imgDelete', '');

                                                                        $.ajax({
                                                                            type: 'POST',
                                                                            data: {
                                                                                subject: $('#txtsubject_ids<?php echo $subject['Subject']['id'] ?>').val()
                                                                            },
                                                                            url: home_url + '/VideoLibrary/deleteSubject/' + subject_id,
                                                                            success: function (response) {

                                                                                $('#chkContainer<?php echo $subject['Subject']['id'] ?>').remove();

                                                                            },
                                                                            errors: function (response) {
                                                                                alert(response.contents);
                                                                            }

                                                                        });

                                                                    });

                                                                });

                                                            </script>

                                                        </div>

                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <input type="hidden" name="selected_subjects" id="selected_subjects" value=""/>
                                        <div class="video-dropdown-container-bottom">
                                            <input type="button" class="btnDoneVideoLibrary_btn btn btn-green" style="background-color:<?php echo $this->Custom->get_site_settings('primary_bg_color') ?>; border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>" value="<?php echo $language_based_content['done_vl']; ?>"/>
                                            <input type="button" class="btnCancelVideoLibrary btn btn-grey" value="<?php echo $language_based_content['cancel_vl']; ?>"/>
                                            <div class="create-area">

                                                <input type="text" style="width: 200px;" id="txtNewSubject" name="new_subject" placeholder="<?php echo $language_based_content['subject_name_vl']; ?>.." />
                                                <a href="javascript:doNewSubject();" class="btn-creat"><?php echo $language_based_content['create_vl']; ?></a>

                                                <script type="text/javascript">

                                                    $('#txtNewSubject').bind('keypress', function (e) {
                                                        var code = (e.keyCode ? e.keyCode : e.which);
                                                        if (code == 13) { //Enter keycode
                                                            doNewSubject();
                                                            return false;
                                                        }
                                                    });

                                                    function doNewSubject() {

                                                        if ($("#txtNewSubject").val() == '') {
                                                            $("#txtNewSubject").addClass('error');
                                                        } else {
                                                            $("#txtNewSubject").removeClass('error');
                                                            $.ajax({
                                                                url: '<?php echo $this->base . "/VideoLibrary/addSubject" ?>',
                                                                method: 'POST',
                                                                data: {
                                                                    subject: $("#txtNewSubject").val()
                                                                },
                                                                success: function (data) {
                                                                    $("#txtNewSubject").val("");
                                                                    $('#cmbSubjectsRow .video-dropdown-container .container').html(data);
                                                                }
                                                            });
                                                        }
                                                    }

                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="cmbTopicsRow" class="topic-row row">
                                    <span class="label"><?php echo $language_based_content['choose_topic_vl']; ?></span>
                                    <div id="cmbTopics" class="video-dropdown"><?php echo $language_based_content['topic_name_vl']; ?></div>
                                    <div class="video-dropdown-container">
                                        <div class="wrapper">
                                            <div class="container">

                                                <?php if ($topics): ?>
                                                    <?php foreach ($topics as $topic): ?>

                                                        <div id="chkContainerTopic<?php echo $topic['Topic']['id'] ?>" class="check-container">

                                                            <input id="topic_ids<?php echo $topic['Topic']['id'] ?>" name="topic_ids<?php echo $topic['Topic']['id'] ?>" class="checkbox-3 topic_checkbox"  type="checkbox" value="<?php echo $topic['Topic']['id'] ?>">

                                                            <label for="topic_ids<?php echo $topic['Topic']['id'] ?>" class="title"><?php echo $topic['Topic']['name'] ?></label>
                                                            <input type="text" id="txttopic_ids<?php echo $topic['Topic']['id'] ?>" class="edit-field" value="<?php echo $topic['Topic']['name'] ?>"/>
                                                            <div class="controls controls<?php echo $topic['Topic']['name'] ?>">
                                                                <?php
                                                                $imgedit_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/playlist8x8.png');
                                                                $imgdel_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/delete-icon8x9.png');
                                                                $imgsave_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/save8x8.png');
                                                                $imgundo_path = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/undo8x8.png');
                                                                ?>
                                                                <?php echo $this->Html->image($imgedit_path, array('id' => "imgEditTopic" . $topic['Topic']['id'], 'class' => 'edit-controls edit-controlsTopic' . $topic['Topic']['id'])); ?>
                                                                <?php echo $this->Html->image($imgdel_path, array('id' => "imgDeleteTopic" . $topic['Topic']['id'], 'class' => 'edit-controls edit-controlsTopic' . $topic['Topic']['id'])); ?>
                                                                <?php echo $this->Html->image($imgsave_path, array('id' => "imgSaveTopic" . $topic['Topic']['id'], 'class' => 'save-controls save-controlsTopic' . $topic['Topic']['id'])); ?>
                                                                <?php echo $this->Html->image($imgundo_path, array('id' => "imgUndoTopic" . $topic['Topic']['id'], 'class' => 'save-controls save-controlsTopic' . $topic['Topic']['id'])); ?>
                                                            </div>
                                                        </div>

                                                        <script type="text/javascript">

                                                            $(document).ready(function () {

                                                                $('.save-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

                                                                $('#imgEditTopic<?php echo $topic['Topic']['id'] ?>').click(function () {

                                                                    $('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').css('display', 'none');
                                                                    $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').css('display', 'block');

                                                                    $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').val($('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').html());

                                                                    $('.save-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'block');

                                                                    $('.edit-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'none');
                                                                });

                                                                $('#imgUndoTopic<?php echo $topic['Topic']['id'] ?>').click(function () {

                                                                    $('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').css('display', 'block');
                                                                    $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

                                                                    $('.save-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

                                                                    $('.edit-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'block');
                                                                });

                                                                $('#imgSaveTopic<?php echo $topic['Topic']['id'] ?>').click(function () {

                                                                    var topic_id = $(this).attr('id').replace('imgSaveTopic', '');

                                                                    $.ajax({
                                                                        type: 'POST',
                                                                        data: {
                                                                            topic: $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').val()
                                                                        },
                                                                        url: home_url + '/VideoLibrary/updateTopic/' + topic_id,
                                                                        success: function (response) {

                                                                            $('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').html($('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').val());

                                                                            $('label[for="topic_ids<?php echo $topic['Topic']['id'] ?>"]').css('display', 'block');
                                                                            $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

                                                                            $('.save-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'none');

                                                                            $('.edit-controlsTopic<?php echo $topic['Topic']['id'] ?>').css('display', 'block');

                                                                        },
                                                                        errors: function (response) {
                                                                            alert(response.contents);
                                                                        }

                                                                    });

                                                                });

                                                                $('#imgDeleteTopic<?php echo $topic['Topic']['id'] ?>').click(function () {

                                                                    var topic_id = $(this).attr('id').replace('imgDeleteTopic', '');

                                                                    $.ajax({
                                                                        type: 'POST',
                                                                        data: {
                                                                            topic: $('#txttopic_ids<?php echo $topic['Topic']['id'] ?>').val()
                                                                        },
                                                                        url: home_url + '/VideoLibrary/deleteTopic/' + topic_id,
                                                                        success: function (response) {

                                                                            $('#chkContainerTopic<?php echo $topic['Topic']['id'] ?>').remove();

                                                                        },
                                                                        errors: function (response) {
                                                                            alert(response.contents);
                                                                        }

                                                                    });

                                                                });

                                                            });

                                                        </script>

                                                    <?php endforeach; ?>
                                                <?php endif; ?>

                                            </div>
                                        </div>
                                        <input type="hidden" name="selected_topics" id="selected_topics" value=""/>
                                        <div class="video-dropdown-container-bottom">
                                            <input type="button" class="btnDoneVideoLibrary_btn btn btn-green" style="background-color:<?php echo $this->Custom->get_site_settings('primary_bg_color') ?>; border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>" value="<?php echo $language_based_content['done_vl']; ?>"/>
                                            <input type="button" class="btnCancelVideoLibrary btn btn-grey" value="<?php echo $language_based_content['cancel_vl']; ?>"/>
                                            <div class="create-area">

                                                <input type="text" style="width: 200px;" id="txtNewTopic" name="new_topic" placeholder="<?php echo $language_based_content['topic_name_vl']; ?>.." />
                                                <a href="javascript:doNewTopic();" class="btn-creat"><?php echo $language_based_content['create_vl']; ?></a>

                                                <script type="text/javascript">

                                                    $('#txtNewTopic').bind('keypress', function (e) {

                                                        var code = (e.keyCode ? e.keyCode : e.which);
                                                        if (code == 13) { //Enter keycode
                                                            doNewTopic();
                                                            return false;
                                                        }
                                                    });

                                                    function doNewTopic() {

                                                        if ($("#txtNewTopic").val() == '') {
                                                            $("#txtNewTopic").addClass('error');
                                                        } else {
                                                            $("#txtNewTopic").removeClass('error');
                                                            $.ajax({
                                                                url: '<?php echo $this->base . "/VideoLibrary/addTopic" ?>',
                                                                method: 'POST',
                                                                data: {
                                                                    topic: $("#txtNewTopic").val()
                                                                },
                                                                success: function (data) {
                                                                    $("#txtNewTopic").val("");
                                                                    $('#cmbTopicsRow .video-dropdown-container .container').html(data);
                                                                }
                                                            });
                                                        }
                                                    }

                                                </script>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        <div class="cnt-bottom">
                            <input type="button" id="btnAddToVideoLibrary" class="btnAddToVideoLibrary1 btn btn-green" style="background-color:<?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_bg_color') ?>" value="<?php echo $language_based_content['add_to_library_vl']; ?>" onclick="PostVideoLibrary()"/>
                            <a href="<?php echo $this->base . '/VideoLibrary/'; ?>" class="btn btn-grey"><?php echo $language_based_content['cancel_vl']; ?></a>
                        </div>
                    </div>
                </form>
            </div>

        </div>


        <script type="text/javascript">

            var uploadPath = '<?php echo "/tempupload/$account_id/" . date('Y') . "/" . date('m') . "/" . date('d') . "/"; ?>';

            $(document).ready(function () {

                $('.ui-checkbox input').on({
                    'change': function () {
                        $(this).parent()[$(this).prop('checked') ? 'addClass' : 'removeClass']('active');
                    },
                    'focus blur': function (e) {
                        $(this).parent()[e.type == 'focus' ? 'addClass' : 'removeClass']('focused');
                    }
                }).trigger('change');

                $("#frmVideoLibrary").validate({
                    errorPlacement: function (error, element) {
                    }
                });
                $('#txtVideoTags').tagsInput({
                    defaultText: '<?php echo $language_based_content['add_tags_here_vl']; ?>'
                });

//                filepicker.setKey('<?php //echo Configure::read('filepicker_access_key');        ?>');
//                filepicker.pickAndStore({
//                    multiple: false,
//                    services: ['COMPUTER', 'DROPBOX', 'GOOGLE_DRIVE', 'SKYDRIVE', 'BOX'],
//                    extensions: ['3gp', '3gpp', 'avi', 'divx', 'dv', 'flv', 'm4v', 'mjpeg', 'mkv', 'mod', 'mov', 'mp4', 'mpeg', 'mpg', 'm2ts', 'mts', 'mxf', 'ogv', 'wmv', 'aif', 'mp3', 'm4a', 'ogg', 'wav', 'wma']
//                            //extensions: ['3g2', '3gp', '3gp2', '3gpp', '3gpp2', 'aac', 'ac3', 'eac3', 'ec3', 'f4a', 'f4b', 'f4v', 'flv', 'highwinds', 'm4a', 'm4b', 'm4r', 'm4v', 'mkv', 'mov', 'mp3', 'mp4', 'oga', 'ogg', 'ogv', 'ogx', 'ts', 'webm', 'wma', 'wmv', 'divx', 'avi', 'dv', 'mjpeg', 'mpg', 'm2ts', 'mts', 'mxf']
//                }, {
//                    location: "S3",
//                    path: uploadPath,
//                    access: 'public',
//                    container : bucket_name
//                }, function(inkBlob) {
//                    if (inkBlob && inkBlob.length > 0) {
//                        var blob = inkBlob[0];
                $('#txtUploadedFilePath').val('<?php echo $this->Session->read('key_lib'); ?>');
                $('#txtUploadedFileName').val('<?php echo $this->Session->read('filename_lib'); ?>');
                $('#txtUploadedFileMimeType').val('<?php echo $this->Session->read('mimetype_lib'); ?>');
                $('#txtUploadedFileSize').val('<?php echo $this->Session->read('size_lib'); ?>');

                $('.VideoLibrary .cntr .upload-detail-container .inner .cnt-left .section  .upload-row .upload-row-right h2').html('<?php echo $this->Session->read('filename_lib'); ?>');
                $('.VideoLibrary .cntr .upload-detail-container .inner .cnt-left .section  .upload-row .upload-row-right h3').html('<?php echo $language_based_content['your_video_is_uploaded_vl'] ?>');
                $('.VideoLibrary .cntr .upload-detail-container .inner .cnt-left .section  .upload-row .upload-row-right div.bar .actualprogress').css('width', '100%');
                $('.VideoLibrary .cntr .upload-detail-container .inner .cnt-left .section  .upload-row .upload-row-right .progressText').html('100%');

                $('#imgSampleImage').attr('src', '<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/question_mark.png') ?>');

                $('.VideoLibrary .cntr .upload-detail-container').css('display', 'block');
                $('#filepicker_dialog_container a').trigger("click");
//                    }
//                }, function() {
//                        location.href = home_url + '/VideoLibrary/';
//                });

                $('.video-dropdown').click(function () {
                    if ($(this).hasClass('video-dropdown-selected')) {
                        $(this).removeClass('video-dropdown-selected');
                        $($(this).siblings('.video-dropdown-container')[0]).css('display', 'none');
                    } else {
                        CloseAllDropDown();
                        $(this).addClass('video-dropdown-selected');
                        $($(this).siblings('.video-dropdown-container')[0]).css('display', 'block');
                    }
                });

                $('.btnDoneVideoLibrary_btn').click(CloseDropDown);
                $('.btnCancelVideoLibrary').click(CloseAllDropDown);
            });




            function getCheckedSubjects() {

                var subjects_id = '';

                $.each($('.subject_checkbox'), function (index, value) {

                    if ($(this).prop('checked') == true) {
                        if (subjects_id != '')
                            subjects_id += ',';
                        subjects_id += $(this).attr('value');
                    }

                });

                return subjects_id;
            }

            function getCheckedTopics() {

                var topics_id = '';

                $.each($('.topic_checkbox'), function (index, value) {

                    if ($(this).prop('checked') == true) {
                        if (topics_id != '')
                            topics_id += ',';
                        topics_id += $(this).attr('value');
                    }

                });

                return topics_id;
            }

            function PostVideoLibrary() {
                var selected_subjects = getCheckedSubjects();
                var selected_topics = getCheckedTopics();

                if ($('#txtUploadedFilePath').val() == '') {
                    alert('<?php echo $alert_messages['please_wait_while_file_uploading']; ?>');
                    return;
                }

                if (selected_subjects == '') {
                    $('#cmbSubjects').addClass('video-dropdown-error');
                } else {
                    $('#cmbSubjects').removeClass('video-dropdown-error');
                }

                $('#selected_subjects').val(selected_subjects);

                if (selected_topics == '') {
                    $('#cmbTopics').addClass('video-dropdown-error');
                } else {
                    $('#cmbTopics').removeClass('video-dropdown-error');
                }

                $('#selected_topics').val(selected_topics);

                if ($("#frmVideoLibrary").valid() && selected_subjects != '' && selected_topics != '') {

                    $("#btnAddToVideoLibrary").val("<?php echo $language_based_content['uploading_library']; ?>");
                    $("#btnAddToVideoLibrary").prop("disabled", true);
                    $.blockUI({message: "<?php echo $language_based_content['uploading_please_wait_library']; ?>"});

                    var fileExt = getFileExtension($('#txtUploadedFileName').val()).toLowerCase();
                    var direct_publish = (fileExt == 'mp4' ? true : false);

                    $.ajax({
                        url: home_url + '/VideoLibrary/add',
                        method: 'POST',
                        success: function (data) {

                            $(document).ajaxStop($.unblockUI);

                            if (parseInt(data) == 0) {
                                alert('<?php echo $alert_messages['unable_to_save_data']; ?>');
                            } else {
                                $(".inner").unmask();

                                location.href = home_url + '/VideoLibrary/';

                            }
                        },
                        data: {
                            video_title: $('#txtVideoTitle').val(),
                            video_desc: $('#txtVideoDescription').val(),
                            video_url: $('#txtUploadedFilePath').val(),
                            video_file_name: $('#txtUploadedFileName').val(),
                            video_mime_type: $('#txtUploadedFileMimeType').val(),
                            video_file_size: $('#txtUploadedFileSize').val(),
                            rand_folder: $('#txtRandomNumber').val(),
                            tags: $('#txtVideoTags').val(),
                            subjects: $('#selected_subjects').val(),
                            topics: $('#selected_topics').val(),
                            direct_publish: (direct_publish == true ? 1 : 0)
                        }
                    });

                }

            }

            function getFileExtension(filename) {

                return filename.substr(filename.lastIndexOf('.') + 1)

            }

            function CloseAllDropDown() {

                var btnDones = $('.btnDoneVideoLibrary_btn');

                for (var i = 0; i < btnDones.length; i++) {
                    var btnDone = $(btnDones[i]);
                    var current_dropdwn = $(btnDone.parent().parent().prev()[0]);
                    if (current_dropdwn.hasClass('video-dropdown-selected')) {
                        current_dropdwn.removeClass('video-dropdown-selected');
                        $(current_dropdwn.siblings('.video-dropdown-container')[0]).css('display', 'none');
                    }
                }
            }

            $('.subject_checkbox').on('click', function (e) {
                if ($(this).is(":checked")) {
                    setAllSubjects();
                } else {
                    setAllSubjects();
                }
            });
            $('.topic_checkbox').on('click', function (e) {
                if ($(this).is(":checked")) {
                    setAllTopics();
                } else {
                    setAllTopics();
                }
            });
            function CloseDropDown() {
                setAllSubjects();
                setAllTopics();
                $($(this).parent().parent().prev()[0]).trigger("click");
            }
            function setAllSubjects() {

                if ($('.subject_checkbox:checked').length > 0) {
                    var chkId = '';
                    $('.subject_checkbox:checked').each(function () {
                        chkId += $('#txtsubject_ids' + $(this).val()).val() + ",";
                    });
                    chkId = chkId.slice(0, -1);
                    $('#cmbSubjects').html('<div style="width:255px; white-space: nowrap;overflow: hidden;    text-overflow: ellipsis;">' + chkId + '</div>');
                } else {
                    $('#cmbSubjects').html('<?php echo $language_based_content['subject_name_vl']; ?>');
                }
            }
            function setAllTopics() {
                if ($('.topic_checkbox:checked').length > 0) {
                    var chkId2 = '';
                    $('.topic_checkbox:checked').each(function () {
                        chkId2 += $('#txttopic_ids' + $(this).val()).val() + ",";
                    });
                    chkId2 = chkId2.slice(0, -1);
                    $('#cmbTopics').html('<div style="width:255px; white-space: nowrap;overflow: hidden;    text-overflow: ellipsis;">' + chkId2 + '</div>');
                } else {
                    $('#cmbTopics').html('<?php echo $language_based_content['topic_name_vl']; ?>');
                }
            }

        </script>
        <div style="clear:both;"></div>
    </div>
</div>
