<?php
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];
?>
<style type="text/css">
    #HudDesc {
        min-height: 20px;
    }
    #HudTitle {
        min-height: 20px;
    }
    .wiz-container{
        padding-top:5px !important;
    }
</style>
<div class="container box newCoachingHuddle">
    <h1 id="wiz-heading">New Huddle Folder</h1>
    <script type="text/javascript">

        (function ($) {
            jQuery.expr[':'].Contains = function (a, i, m) {
                return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
            };

            function listFilter(header, list) {
                var form = $("<div>").attr({"class": "filterform"}),
                        input = $("<input id='input-filter' placeholder='Search people or group'><div  id='cancel-btn' type='text' style=' width: 10px;  position: absolute; right: 93px; margin-top: 7px; display:none;cursor: pointer; '>X</div>").attr({"class": "filterinput", "type": "text"});
                $(form).append(input).appendTo(header);
                $('.filterform input').css({'width': '100%', 'float': 'right'});
                $(input).change(function () {
                    var filter = $(this).val();
                    $('#liNodataFound').remove();
                    if (filter) {
                        $search_count = $(list).find("a:Contains(" + filter + ")").length;
                        if ($search_count > 4) {
                            $('div.thumb').css('top', '0px');
                            $('div.overview').css('top', '0px');
                            $('.scrollbar').css('display', 'block');
                        } else {
                            $('div.thumb').css('top', '0px');
                            $('div.overview').css('top', '0px');
                            $('.scrollbar').css('display', 'none');
                        }
                        $(list).find("a:not(:Contains(" + filter + "))").parent().parent().slideUp(400, function () {
                            $('.widget-scrollable').tinyscrollbar_update();
                        });
                        $(list).find("a:Contains(" + filter + ")").parent().parent().slideDown(400, function () {
                            $('.widget-scrollable').tinyscrollbar_update();
                        });
                        jQuery('#cancel-btn').css('display', 'block');
                        if ($(list).find("a:Contains(" + filter + ")").length == 0) {
                            $('#liNodataFound').remove();
                            $("#list-containers").append('<li id="liNodataFound">No people or groups match this search. Please try again.</li>');
                        } else {
                            $('#liNodataFound').remove();
                        }

                    } else {
                        jQuery('#cancel-btn').css('display', 'none');
                        $('.scrollbar').css('display', 'block');
                        $(list).find("li").slideDown(400, function () {
                            $('.widget-scrollable').tinyscrollbar_update();
                        });

                    }
                    return false;
                }).keyup(function () {
                    $(this).change();
                });

                $('#cancel-btn').click(function (e) {
                    jQuery('#cancel-btn').css('display', 'none');
                    $('div.thumb').css('top', '0px');
                    $('div.overview').css('top', '0px');
                    $('.scrollbar').css('display', 'block');
                    jQuery('#input-filter').val('');
                    $(list).find("li").slideDown(400, function () {
                        $('.widget-scrollable').tinyscrollbar_update();
                    });
                })
            }

            $(function () {
                listFilter($("#header-container"), $("#list-containers"));
            });
        }(jQuery));
        $(document).ready(function () {

            $('#caoch-checkbox').click(function (e) {
                if ($(this).is(':checked') == true) {
                    $(".caoch-checkbox input[type='checkbox']").prop('checked', true);
                } else {
                    $(".caoch-checkbox input[type='checkbox']").prop('checked', false);
                }
            })

            $("span.mentee-checkbox input[type=radio]").click(function () {

                var huddle_row_selector = $(this).parent().parent().parent().find("span.caoch-checkbox input[type='checkbox']");
                if (huddle_row_selector.length > 0 && !huddle_row_selector.is(":checked")) {
                    huddle_row_selector.trigger('click');
                    $("#chk_is_coachee").val("1");
                }

            });

            $("span.caoch-checkbox input[type=radio]").click(function () {


                var huddle_row_selector = $(this).parent().parent().parent().find("span.caoch-checkbox input[type='checkbox']");
                if (huddle_row_selector.length > 0 && !huddle_row_selector.is(":checked")) {
                    huddle_row_selector.trigger('click');
                }


            });


            $('#select-all-none').click(function (e) {
                if ($('#select-all').is(':checked')) {
                    $('#select-all-label').html('Select All');
                    $('#select-all').prop('checked', true);
                    $(".member-user").prop('checked', true);
                    $(".super-user").prop('checked', true);
                    $(".viewer-user").prop('checked', true);
                    $(".huddle_edit_cls_chkbx").prop('checked', true);
                } else {
                    $('#select-all-label').html('Select None');
                    $('#select-all').prop('checked', false);
                    $(".member-user").prop('checked', false);
                    $(".super-user").prop('checked', false);
                    $(".viewer-user").prop('checked', false);
                    $(".huddle_edit_cls_chkbx").prop('checked', false);
                }

            });
        })

    </script>
    <script>
        $(document).ready(function () {

            $('#collab-huddle').click(function () {
                $('#wiz-heading').text('New Collaboration Huddle');
                $('#step2-label').text("Invite other people in your account to participate with you in the huddle. You can always do this later. You're defaulted to be an Admin in the Huddle.");
                $('#video_huddle_name').prop("disabled", false);
                $('#video_huddle_description').prop("disabled", false);
                $('#video_huddle_name').css("background-color", 'white');
                $('#video_huddle_description').css("background-color", 'white');
                $('.coach_mentee').css('display', 'none');
                $('.coach_perms').css('display', 'block');
                $('#select-all-none-cnt').css('display', 'block');
                $('.groups-table-header').css('padding-bottom', '0px');
                $("#Coaches_info").css("display", "block");
                $("#Coachee_info").css("display", "none");

                if ($('.coach_mentee_hidden').length > 0) {
                    $('.coach_mentee_hidden').remove();
                }

                if ($('.coach_coach_hidden').length > 0) {
                    $('.coach_coach_hidden').remove();
                }

            });
            $('#coach-huddle').click(function () {
                $('#wiz-heading').text('New Coaching / Mentoring Huddle');
                $('#step2-label').text('Coaching Huddle creator is defaulted to be a coach in the Huddle. You may add another coach to the huddle, but you must select a participant to be coached.');
                $('#video_huddle_name').prop("disabled", true);
                $('#video_huddle_description').prop("disabled", true);
                $('#video_huddle_name').css("background-color", '#F0EEE3');
                $('#video_huddle_description').css("background-color", '#F0EEE3');
                $('.coach_mentee').css('display', 'block');
                $('.coach_perms').css('display', 'none');
                $('.coach_mentee').css('float', 'none');
                $('.coach_mentee').attr('style', 'float: right !important');
                $('#select_all_coaches_panel').css('display', 'none');
                $('#video_huddle_name').val('');
                $('#video_huddle_description').val('');
                $('#select-all-none-cnt').css('display', 'none');
                $('.groups-table-header').css('padding-bottom', '10px');
                $("#Coaches_info").css("display", "none");
                $("#Coachee_info").css("display", "block");



                $("span.mentee-checkbox input[type=radio]").each(function () {

                    if ($(this).is(":checked")) {
                        var radio_id = $(this).attr('id').split('_');
                        //$('#super_admin_ids_'+radio_id[1]).prop('checked', true);
                        //$('#new_checkbox_'+radio_id[1]).prop('checked', true);
                        var html = '<input type="checkbox" class="coach_mentee_hidden" value="1" id="is_mentor_' + radio_id[1] + '" name="is_mentor_' + radio_id[1] + '" style="display:none" checked="checked" />';
                        $(this).parent().parent().append(html);
                    }

                });

                $("span.caoch-checkbox input[type=radio]").each(function () {

                    if ($(this).is(":checked")) {
                        var radio_id = $(this).attr('id').split('_');
                        //$('#super_admin_ids_'+radio_id[1]).prop('checked', true);
                        //$('#new_checkbox_'+radio_id[1]).prop('checked', true);
                        var html = '<input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_' + radio_id[1] + '" name="is_coach_' + radio_id[1] + '" style="display:none"  checked="checked">';
                        $(this).parent().parent().append(html);
                    }

                });


            });
            $('.step2').click(function () {

                $("#tabs").tabs("option", "active", 1);
                $("#step-2").css("display", "block");
                setTimeout(function () {
                    $('#input-filter').trigger('change');
                }, 200);

                $('#HudTitle').css("display", "block");
                $('#HudDesc').css("display", "block");
                if ($('#collab-huddle').is(':checked')) {
                    if ($('#video_huddle_name').val() != '') {
                        $("#huddle-name-last").text($('#video_huddle_name').val());
                        $("#huddle-name-last").css('width', '250px');
                        $("#name-id").val($('#video_huddle_name').val());
                        $("#ajaxInputName").val($('#video_huddle_name').val());
                        $(".editArea").css("display", "none");
                    } else {
                        $(".editArea").css("display", "block");
                    }
                    if ($('#video_huddle_description').val() != '') {
                        $("#huddle-desc-last").text($('#video_huddle_description').val());
                        $("#huddle-desc-last").css('width', '250px');
                        $("#desc-id").val($('#video_huddle_description').val());
                        $("#ajaxInputDesc").val($('#video_huddle_description').val());
                        $(".editAread").css("display", "none");
                    } else {
                        $(".editAread").css("display", "block");
                    }
                }
                if ($('#coach-huddle').is(':checked')) {
                    $(".editArea").css("display", "none");
                    $(".editAread").css("display", "none");
                }

            });
            /*$('.secondone').click(function(){

             $( "#tabs" ).tabs("option", "active", 1 );
             setTimeout(function(){
             $('#input-filter').trigger('change');
             }, 200);

             $('#HudTitle').css("display", "block");
             $('#HudDesc').css("display", "block");
             if($('#collab-huddle').is(':checked')) {
             if($('#video_huddle_name').val()!=''){
             $("#huddle-name-last").text($('#video_huddle_name').val());
             $("#huddle-name-last").css('width','250px');
             $("#name-id").val($('#video_huddle_name').val());
             $("#ajaxInputName").val($('#video_huddle_name').val());
             $(".editArea").css("display", "none");
             }else{
             $(".editArea").css("display", "block");
             }
             if($('#video_huddle_description').val()!=''){
             $("#huddle-desc-last").text($('#video_huddle_description').val());
             $("#huddle-desc-last").css('width','250px');
             $("#desc-id").val($('#video_huddle_description').val());
             $("#ajaxInputDesc").val($('#video_huddle_description').val());
             $(".editAread").css("display", "none");
             }else{
             $(".editAread").css("display", "block");
             }
             }
             if($('#coach-huddle').is(':checked')) {
             $(".editArea").css("display", "none");
             $(".editAread").css("display", "none");
             }

             });*/
            $('.step3').click(function () {
                $("#tabs").tabs("option", "active", 2);
                $("#step-3").css("display", "block");
                if ($('#coach-huddle').is(':checked'))
                    var h_type = 2
                else
                    var h_type = 1
                var participants = ['<?php echo $users['User']['id']; ?>||200||' + h_type];
                var mentor_name = '';
                var huddle_type = 1;
                $(".editArea").css("display", "none");
                $(".editAread").css("display", "none");
                $('#HudTitle').css("display", "block");
                $('#HudDesc').css("display", "block");
                if ($('#collab-huddle').is(':checked')) {
                    if ($('#video_huddle_name').val() != '') {
                        $("#huddle-name-last").text($('#video_huddle_name').val());
                        $("#huddle-name-last").css('width', '250px');
                        $("#name-id").val($('#video_huddle_name').val());
                        $("#ajaxInputName").val($('#video_huddle_name').val());
                        $(".editArea").css("display", "none");
                    } else {
                        $(".editArea").css("display", "block");
                    }
                    if ($('#video_huddle_description').val() != '') {
                        $("#huddle-desc-last").text($('#video_huddle_description').val());
                        $("#huddle-desc-last").css('width', '250px');
                        $("#desc-id").val($('#video_huddle_description').val());
                        $("#ajaxInputDesc").val($('#video_huddle_description').val());
                        $(".editAread").css("display", "none");
                    } else {
                        $(".editAread").css("display", "block");
                    }
                }

                $('input[name="super_admin_ids[]"]:checked').each(function () {
                    if ($('#coach-huddle').is(':checked')) {

                        if ($('input[name=is_coach_' + this.value + ']').is(':checked')) {
                            var $radios = $('input:radio[name=user_role_' + this.value + ']');
                            $radios.filter('[value=200]').prop('checked', true);
                        } else {
                            if ($('input[name=is_mentor_' + this.value + ']').is(':checked')) {
                                var $radios = $('input:radio[name=user_role_' + this.value + ']');
                                $radios.filter('[value=210]').prop('checked', true);
                            }
                        }
                    }
                    var role = $('input[name=user_role_' + this.value + ']:checked').val();
                    if ($('input[name=is_coach_' + this.value + ']').is(':checked') || $('input[name=is_mentor_' + this.value + ']').is(':checked')) {
                        role = role + '||2';
                    } else {
                        role = role + '||1';
                    }
                    if (this.value < 0) {
                        role = role + '||' + $("#super_admin_fullname_" + this.value).val();
                    }
                    participants.push(this.value + '||' + role);
                    if ($('input[name="is_mentor_' + this.value + '"]').is(':checked')) {
                        if (mentor_name == '')
                            mentor_name = $('#lblsuper_admin_ids_' + this.value).html();
                    }
                });
                $('input[name="group_ids[]"]:checked').each(function () {

                    if ($('#coach-huddle').is(':checked')) {
                        if ($('input[name=is_coach_' + this.value + ']').is(':checked')) {
                            var $radios = $('input:radio[name=group_role_' + this.value + ']');
                            $radios.filter('[value=200]').prop('checked', true);
                        } else {
                            if ($('input[name=is_mentor_' + this.value + ']').is(':checked')) {
                                var $radios = $('input:radio[name=group_role_' + this.value + ']');
                                $radios.filter('[value=210]').prop('checked', true);
                            }
                        }
                    }
                    var role = $('input[name=group_role_' + this.value + ']:checked').val();
                    if ($('input[name=is_coach_' + this.value + ']').is(':checked') || $('input[name=is_mentor_' + this.value + ']').is(':checked')) {
                        role = role + '||2';
                    } else {
                        role = role + '||1';
                    }
                    participants.push(this.value + '||' + role);
                    if ($('input[name="is_mentor_' + this.value + '"]').is(':checked')) {
                        if (mentor_name == '')
                            mentor_name = $('#lblsuper_admin_ids_' + this.value).html();
                        //console.log($('#lblsuper_admin_ids_'+this.value).html());
                    }
                });
                if ($('#coach-huddle').is(':checked')) {
                    huddle_type = 2;
                    if (typeof mentor_name == "undefined" || mentor_name == '')
                        mentor_name = 'Coaching Huddle 2015';

                    $("#huddle-name-last").text(mentor_name);
                    $("#name-id").val(mentor_name);
                    $("#ajaxInputName").val(mentor_name);
                    $("#huddle-desc-last").html('&nbsp;');
                    $("#desc-id").val(' ');
                    $("#ajaxInputDesc").val(' ');
                }
                $.ajax({
                    type: 'POST',
                    data: {
                        participants: participants,
                        type: huddle_type,
                    },
                    url: home_url + '/Huddles/get_people_ajax/',
                    success: function (res) {
                        $('#participants').html(res);
                    }
                });

            });
            /*$('.thirdone').click(function(){
             $( "#tabs" ).tabs("option", "active", 2 );
             var participants = [];
             var mentor_name = ''

             $(".editArea").css("display", "none");
             $(".editAread").css("display", "none");
             $('#HudTitle').css("display", "block");
             $('#HudDesc').css("display", "block");

             $('input[name="super_admin_ids[]"]:checked').each(function() {
             if($('#coach-huddle').is(':checked')) {
             if($('input[name=is_coach_'+this.value+']').is(':checked')){
             var $radios = $('input:radio[name=user_role_'+this.value+']');
             $radios.filter('[value=200]').prop('checked', true);
             }else{
             if($('input[name=is_mentor_'+this.value+']').is(':checked')){
             var $radios = $('input:radio[name=user_role_'+this.value+']');
             $radios.filter('[value=210]').prop('checked', true);
             }
             }
             }
             var role = $('input[name=user_role_'+this.value+']:checked').val();
             participants.push(this.value+'||'+role);
             if($('input[name="is_mentor_'+this.value).is(':checked')) {
             if(mentor_name == '')
             mentor_name = $('#lblsuper_admin_ids_'+this.value).html();
             //console.log($('#lblsuper_admin_ids_'+this.value).html());
             }
             });
             $('input[name="group_ids[]"]:checked').each(function() {
             var role = $('input[name=group_role_'+this.value+']:checked').val();
             participants.push(this.value+'||'+role);
             if($('input[name="is_mentor_'+this.value).is(':checked')) {
             if(mentor_name == '')
             mentor_name = $('#lblsuper_admin_ids_'+this.value).html();
             }
             });
             if($('#coach-huddle').is(':checked')) {
             if(mentor_name == '')
             mentor_name = 'Coaching Huddle 2015';

             $("#huddle-name-last").text(mentor_name);
             $("#name-id").val(mentor_name);
             $("#ajaxInputName").val(mentor_name);
             $("#huddle-desc-last").html('&nbsp;');
             $("#desc-id").val(' ');
             $("#ajaxInputDesc").val(' ');
             }
             $.ajax({
             type: 'POST',
             data: {
             participants: participants,
             },
             url: home_url + '/Huddles/get_people_ajax/',
             success: function (res) {
             $('#participants').html(res);
             }
             });

             });*/
        });
        $(function () {
            ;

            $('.coach_mentee').css('display', 'none');
            $("#tabs").tabs();
            $(document).on("change", '.huddle_edit_cls [type="checkbox"]', function () {
                if ($(this).is(":checked")) {
                    $(this).parent().parent().find('label [type="checkbox"]').prop("checked", true);
                }
                else {
                    $(this).parent().parent().find('label [type="checkbox"]').prop("checked", false);
                }
            });

            $("#coaching_trig_2").click(function () {
                $(".cls_coaching_trig_2").trigger("click");
            });
            $("#cls_coaching_trig").click(function () {
                $(".cls_coaching_trig").trigger("click");
            });

            $(document).on("change", '.coach_mentee [type="radio"]', function () {
                var radio_val = $(this).val();
                var radio_name = $(this).attr('name');
                var get_radio_id = radio_name.split("_");
                var radio_id = get_radio_id[1];
                var html = '';
                $(this).parent().parent().find('[type="checkbox"]').remove();
                if (radio_val == "1") {
                    html = '<input type="checkbox" class="coach_coach_hidden" value="1" id="is_coach_' + radio_id + '" name="is_coach_' + radio_id + '" style="display:none" checked="checked" />';
                    $(this).parent().parent().append(html);
                }
                else {
                    html = '<input type="checkbox" class="coach_mentee_hidden" value="1" id="is_mentor_' + radio_id + '" name="is_mentor_' + radio_id + '" style="display:none" checked="checked" />';
                    $(this).parent().parent().append(html);
                }

            });
        });
        $(document).on("click", '#ui-id-1', function () {
            $(".appendix-content").hide();
        });
    </script>
    <form method="post" id="new_video_huddle" enctype="multipart/form-data" class="new_video_huddle"
          action="<?php echo $this->base . '/Folder/create'; ?>" accept-charset="UTF-8">
        <div id="tabs">

            <div class="wiz-container">
                <div id="step-1">
                    <div style="margin:0;padding:0;display:inline"><input type="hidden" value="✓" name="utf8">
                        <input name="data[folder_id]" type="hidden" value="<?php echo $folder_id; ?>"/></div>

                    <a class="close-reveal-modal"></a>
                    <div class="span5">
                        <div class="row"  style="margin-bottom: 20px;">
                            <span class="wiz-step1-radio"><input type="radio" id="collab-huddle" name="type" class="cls_coaching_trig_2" checked="checked" value="1" style="display: none;" checked="checked"></span>
                            <div style="clear: both;height:10px;"></div>
                        </div>
                        <div class="row" style="margin-bottom: 20px;">
                            <input name="data[hname]" id="video_folder_name" class="size-big huddle-name "  placeholder="Folder Name" size="30" type="text" required/>
                            <label for="video_folder_name" class="error" style="display:none;background-color: none !important;">This field is required.</label>
                        </div>
                        <div style="clear: both;"></div>
                        <div class="row" style="display:none;">
                            <?php echo $this->Form->textarea('hdescription', array('rows' => '4', 'placeholder' => 'Folder Description (Optional)', 'id' => 'video_huddle_description', 'cols' => '40', 'class' => 'size-big')); ?>
                        </div>
                        <?php echo $this->Form->input('created_by', array('value' => $user_id, 'type' => 'hidden', 'id' => 'video_huddle_created_by')); ?>
                    </div>
                    <div style="clear: both;height:10px;"></div>
                    <!--  <div class="row-fluid">
                        <div class="groups-table span12">

                            <div class="span4 huddle-span4" style="margin-left:0px;">
                                <div class="groups-table-header groups-table-header-create">
                                    <div style="width: 200px; float: left; margin-left: 8px;">
                    <?php if ($user_permissions['UserAccount']['permission_maintain_folders'] == 1 && $user_permissions['UserAccount']['permission_administrator_user_new_role'] == 1): ?>
                                                            <a style="margin-left: -8px;" id="pop-up-btn" data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="btn btn-small btn-green" href="#"><span class="plus">+</span>
                                                            </a>
                    <?php else: ?>
                                                            <a style="margin-left: -8px;"><span class="">&nbsp;&nbsp;</span>
                                                            </a>
                    <?php endif; ?>
                                    </div>
                                    <div style="width: 248px; padding: 0px;float: left;margin-left: 426px;" class="search-box">
                                        <div  id="header-container" class="filterform">
                                        </div>
                                    </div>
                                    <a class="appendix right" href="#" id="Coaches_info" >?</a>
                                    <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                        <h3>Info</h3>
                                        <p>Admin: Can add/remove users; upload/download/copy/delete all videos and documents; clip all videos; add/edit/delete all video annotations and comments; create/participate/edit all Folder discussions; delete Huddle.</p>
                                        <p>Members: Can upload/download/copy/delete videos and documents they add to the Huddle; clip their videos; add/edit/delete their video annotations and comments; create/participate in Huddle discussions.</p>
                                        <p>Viewers: View videos and documents only.</p>
                                    </div>
                                    <a class="appendix right" href="#" id="Coachee_info" style="display:none;">?</a>
                                    <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                        <h3>Info</h3>
                                        <p>Coach(es):  Can edit coaching Huddle, by adding or removing other coaches in the Huddle; upload/download/copy all videos and supporting resources; clip/edit videos; add/edit/delete all video annotations; enable/disable tags/account framework; create/participate/edit all discussions; delete the coaching Huddle.</p>
                                        <p>Coachee: Can upload/download/copy/delete videos and resources they add to the Huddle; clip/edit their own videos; add/edit/delete their video annotations and comments; create/participate in Huddle discussions.</p>
                                    </div>
                                    <div style="clear:both"></div>
                    <?php if (count($super_users) > 1 || (isset($folderUsers) && count($folderUsers) > 1) || (isset($users_groups) && count($users_groups) > 0)): ?>
                                                        <div id="select-all-none-cnt" class="select-all-none selectnone"  style="float: left;margin-left: 454px;margin-top: 1border.pngpx;min-width: 123px;max-width: 200px;">
                                                            <label id="select-all-none" for="select-all"><input type="checkbox" name="select-all" id="select-all" style="display:inline-block;" /> <span id="select-all-label">Select None</span></label>
                                                        </div>
                    <?php endif; ?>
                    <?php if (1 == 1): ?>
                                                        <div id="select_all_coaches_panel" class="huddles-select-all-block coach_mentee">
                                                            <span class="caoch-checkbox" >
                                                                <input type="checkbox" id="caoch-checkbox" name="is_coach"> Coach</span>
                                                            <span class="mentee-checkbox"><input type="checkbox" id="mentee-checkbox" name="is_mentor" >Coachee</span>
                                                        </div>
                    <?php endif; ?>

                                    <div class="huddles-select-all-block coach_perms">
                                        <span class="admin-radio"><input type="radio" id="admin-radio" name="select-all"> Admin</span>
                                        <span class="member-radio"><input type="radio" id="member-radio" name="select-all" style="margin-left: -8px;"> Member</span>
                                        <span class="viewer-radio"><input type="radio" id="viewers-radio" name="select-all"> Viewer</span>
                                    </div>
                                    <div style="clear:both"></div>
                                </div>
                                <div class="groups-table-content">
                                    <div class="widget-scrollable">
                                        <div class="scrollbar" style="height: 155px;">
                                            <div class="track" style="height: 155px;">
                                                <div class="thumb" style="top: 0px; height: 90.3195px;">
                                                    <div class="end"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="viewport short">
                                            <div class="overview" style="top: 0px;">
                                                <div  id="people-lists">
                                                    <ul id="list-containers">
                                                        <input type="hidden" id="chk_is_coachee" value="0" name="chk_is_coachee"  />
                    <?php
                    if (count($users_record) > 0):
                        ?>
                        <?php if ($users_record): ?>
                            <?php foreach ($users_record as $row): ?>
                                <?php
                                if ($row['id'] == $user_id)
                                    continue;
                                ?>
                                <?php if ($row['is_user'] == 'admin'): ?>
                                                                                                                                        <li style="display:none;">
                                                                                                                                            <label  class="huddle_permission_editor_row" for="super_admin_ids_<?php echo $row['id'] ?>"><input class="super-user" type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>" style="display: none;"> <a style="color: #757575; font-weight: normal;" id="lblsuper_admin_ids_<?php echo $row['id'] ?>"><?php echo trim($row['first_name'] . " " . $row['last_name']); ?> </a></label>
                                                                                                                                            <input type="hidden" value="<?php echo $row['email'] ?>" name="super_admin_email_<?php echo $row['id'] ?>" id="super_admin_email_<?php echo $row['id'] ?>">

                                    <?php if (1 == 1): ?>
                                                                                                                                                                <div class="permissions coach_mentee">
                                                                                                                                                                    <span class="caoch-checkbox" ><input type="radio" value="1" id="caoch-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>"><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>" >Coach</label></span>
                                                                                                                                                                    <span class="mentee-checkbox"><input value="2" type="radio" id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" class="chk_is_coachee" checked="checked"> <span id="mentee-chk"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span></span>
                                                                                                                                                                </div>
                                    <?php endif; ?>
                                                                                                                                            <div class="permissions coach_perms">
                                                                                                                                                <label for="user_role_<?php echo $row['id'] ?>_200"><input  class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200"/>Admin
                                                                                                                                                </label>
                                                                                                                                                <label for="user_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn"  type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" checked="checked"/>Member
                                                                                                                                                </label>
                                                                                                                                                <label for="user_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220"/>Viewer
                                                                                                                                                </label>
                                                                                                                                            </div>
                                                                                                                                            <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="<?php echo $row['id'] ?>" id="new_checkbox_<?php echo $row['id'] ?>"  name="" class="huddle_edit_cls_chkbx"><label class="cls_sp_label" for="new_checkbox_<?php echo $row['id'] ?>">Folder Participant</label></span>
                                                                                                                                        </li>

                                <?php elseif ($row['is_user'] == 'member'): ?>
                                                                                                                                        <li>
                                                                                                                                            <label  class="huddle_permission_editor_row" for="super_admin_ids_<?php echo $row['id'] ?>"><input class="member-user"  type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>" style="display: none;"> <a style="color: #757575; font-weight: normal;" id="lblsuper_admin_ids_<?php echo $row['id'] ?>"> <?php echo $row['first_name'] . " " . $row['last_name'] ?></a></label>

                                    <?php if (1 == 1): ?>
                                                                                                                                                                <div class="permissions coach_mentee">
                                                                                                                                                                    <span class="caoch-checkbox" >
                                                                                                                                                                        <input type="radio" id="caoch-checkbox_<?php echo $row['id'] ?>" value="1"  name="coach_<?php echo $row['id'] ?>"><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>">Coach</label></span>
                                                                                                                                                                    <span class="mentee-checkbox"><input type="radio" value="2"   id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" class="chk_is_coachee" checked="checked"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span>
                                                                                                                                                                </div>
                                    <?php endif; ?>
                                                                                                                                            <div class="permissions coach_perms">
                                                                                                                                                <label for="user_role_<?php echo $row['id'] ?>_200"><input class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200"/>Admin
                                                                                                                                                </label>
                                                                                                                                                <label for="user_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" checked="checked"/>Member
                                                                                                                                                </label>
                                                                                                                                                <label for="user_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220"/>Viewer
                                                                                                                                                </label>
                                                                                                                                            </div>
                                                                                                                                            <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="<?php echo $row['id'] ?>" id="new_checkbox_<?php echo $row['id'] ?>"  name="" class="huddle_edit_cls_chkbx"><label class="cls_sp_label" for="new_checkbox_<?php echo $row['id'] ?>">Folder Participant</label></span>
                                                                                                                                        </li>

                                <?php elseif ($row['is_user'] == 'group'): ?>
                                                                                                                                        <li>
                                                                                                                                            <label  class="huddle_permission_editor_row"><input class="viewer-user" type="checkbox" value="<?php echo $row['id']; ?>" name="group_ids[]" id="group_ids_<?php echo $row['id'] ?>" style="display: none;"> <a style="color: #757575; font-weight: normal;" id="lblsuper_admin_ids_<?php echo $row['id'] ?>"><?php echo $row['name']; ?></a></label>

                                    <?php if (1 == 1): ?>
                                                                                                                                                                <div class="permissions coach_mentee">
                                                                                                                                                                    <span class="caoch-checkbox" >
                                                                                                                                                                        <input type="radio" id="caoch-checkbox_<?php echo $row['id'] ?>" value="1"  name="coach_<?php echo $row['id'] ?>"><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>">Coach</label></span>
                                                                                                                                                                    <span class="mentee-checkbox"><input type="radio" value="2" id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" class="chk_is_coachee" checked="checked"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span>
                                                                                                                                                                </div>
                                    <?php endif; ?>
                                                                                                                                            <div class="permissions coach_perms">
                                                                                                                                                <label for="group_role_<?php echo $row['id'] ?>_200"><input  class="admin-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_200" value="200"/>Admin
                                                                                                                                                </label>
                                                                                                                                                <label for="group_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_210" value="210" checked="checked"/>Member
                                                                                                                                                </label>
                                                                                                                                                <label for="group_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_220" value="220"/>Viewer
                                                                                                                                                </label>
                                                                                                                                            </div>
                                                                                                                                            <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="1" id="new_checkbox_<?php echo $row['id'] ?>"  name="" class="huddle_edit_cls_chkbx"><label class="cls_sp_label" for="new_checkbox_<?php echo $row['id'] ?>">Folder Participant</label></span>
                                                                                                                                        </li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    <?php else: ?>
                                                                            <li>
                                                                                To Invite users into the account <a data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="" href="#"><span class="">click here</span></a>
                                                                            </li>
                    <?php endif; ?>

                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="form-actions" style="text-align:left;">
                        <input type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" id="btnSaveHuddle" value="Create Folder" name="commit" onclick="beforeHuddleAdd();" class="btn btn-green"  >
                        <a id="cancel" class="btn btn-transparent" href="javascript:void(null)">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="addSuperAdminModal" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="header">
                    <h4 class="header-title nomargin-vertical smargin-bottom"><img alt="Admin" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/add-user.png'); ?>" /> New User</h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                </div>
                <form accept-charset="UTF-8" action="<?php echo $this->base . '/Huddles/addUsers' ?>" enctype="multipart/form-data" method="post" name="admin_form" onsubmit="return false;" style="padding-top:0px;">
                    <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                    <div id="flashMessage2" class="message error" style="display:none;"></div>
                    <div class="way-form">
                        <h3>Add name and email</h3>
                        <ol class="autoadd autoadd-sfont fmargin-list-left">
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name" name="users[][name]" required placeholder="Full Name" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email" name="users[][email]" required placeholder="Email Address" type="email" value="" /></label>
                                <a href="#" class="close">x</a>
                            </li>
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name" name="users[][name]" placeholder="Full Name" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email" name="users[][email]" placeholder="Email Address" type="email" value="" /></label>
                                <a href="#" class="close">x</a>
                            </li>
                        </ol>
                        <input id="controller_source" name="controller_source" type="hidden" value="video_huddles" />
                        <input id="action_source" name="action" type="hidden" value="add" />
                        <input id="action_source" name="user_type" type="hidden" value="110" />
                        <button style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" id="btnAddToAccount_addHuddle" class="btn btn-green fmargin-left" type="button">Add to Account</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clear"></div>

    <script type="text/javascript">
        $(document).ready(function () {

            $(document).on("click", "#cancel", function () {
                $('#createfolder').modal('hide');
            });


            $("#HudTitle").mouseover(function () {
                $(this).css("backgroundColor", "#FAFABE");
            });
            $("#HudTitle").mouseout(function () {
                $(this).css("backgroundColor", "#ffffff");
            });
            $(document).on('click', "#HudTitle", function () {

                $(this).css("display", "none");
                $(".editArea").css("display", "block");
                $("#ajaxInputName").val($(this).text().trim());
                $("#ajaxInputName").focus();

            });

            $("#HudDesc").mouseover(function () {
                $(this).css("backgroundColor", "#FAFABE")
            });
            $("#HudDesc").mouseout(function () {
                $(this).css("backgroundColor", "#ffffff")
            });
            $("#HudDesc").click(function () {
                //         if($('#collab-huddle').is(':checked')) {
                $(this).css("display", "none");
                $(".editAread").css("display", "block");
                $("#ajaxInputDesc").val($(this).text().trim());
                $("#ajaxInputDesc").focus();
                //         }
            });

            $("#admin-radio").click(function () {
                if ($(this).is(':checked')) {
                    $(".member-btn").removeAttr("checked");
                    $(".viewer-btn").removeAttr("checked");
                    $(".admin-btn").prop('checked', true);
                }

            });
            $("#member-radio").click(function () {
                if ($(this).is(':checked')) {
                    $(".admin-btn").removeAttr("checked");
                    $(".viewer-btn").removeAttr("checked");
                    $(".member-btn").prop('checked', true);

                }
            });
            $("#viewers-radio").click(function () {
                if ($(this).is(':checked')) {
                    $(".admin-btn").removeAttr("checked");
                    $(".member-btn").removeAttr("checked");
                    $(".viewer-btn").prop('checked', true);
                }
            });
        });

        var posted_data = [];
        var new_ids = 0;

        $(document).ready(function () {
            $('#btnAddToAccount_addHuddle').click(function () {
                addToInviteList();
            });

            $('#pop-up-btn').click(function () {
                $('.fmargin-left').attr('disabled', false);
            });

            $('#video_folder_name').keypress(function (e) {
                if (e.keyCode == 13) {
                    $(this).closest('form').find('textarea').eq(0).focus();
                    return false;
                }
                $(this).removeClass('error').next().hide();
            });

        });

        function beforeHuddleAdd() {
            $('#btnSaveHuddle').attr('disabled', true);

//            if ($("#ajaxInputName").val().trim().length > 0) {
//                var video_huddle_name = $("#ajaxInputName");
//            } else {
//                if ($("#ajaxInputName").val().trim().length > 0) {
//                    $("#ajaxInputName").val($("#name-id").val());
//                    var video_huddle_name = $("#name-id");
//                } else {
//                    var video_huddle_name = $("#ajaxInputName");
//                }
//            }


            var video_huddle_name = $("#video_folder_name");
            if (video_huddle_name.val().trim().length > 0) {
                console.log(video_huddle_name.val());

                if ($("#coach-huddle").is(':checked')) {

                    $("#chk_is_coachee").val('0');
                    $("span.mentee-checkbox input[type=radio]").each(function () {

                        var huddle_row_selector = $(this).parent().parent().parent().find("span.caoch-checkbox input[type='checkbox']");
                        if (huddle_row_selector.length > 0 && huddle_row_selector.is(":checked") && $(this).is(":checked")) {
                            $("#chk_is_coachee").val('1');
                        }
                    });

                    var get_chk = $("#chk_is_coachee").val();
                    if (get_chk != '1') {
                        alert("You must select a participant to be coached before creating a coaching/mentoring Huddle.");
                        $('#btnSaveHuddle').attr('disabled', false);
                    } else {
                        $('#new_video_huddle').submit();
                    }

                }
                else {
                    $('#new_video_huddle').submit();
                }
            } else {
                $("#video_folder_name").addClass('error');
                $('#btnSaveHuddle').attr('disabled', false);
                $('#HudTitle').hide();
                $('.editArea').css('display', 'block');

                //            video_huddle_name.next().css('display', 'block');
            }
            //        console.log('before huddle add ' + (new Date()).getTime());
            return false;
        }

        function is_email_exists(email) {
            for (var i = 0; i < posted_data.length; i++) {
                var posted_user = posted_data[i];
                if (posted_user[1] == email)
                    return true;
            }
            return false;
        }

        function addToInviteList() {
            var lposted_data = [];
            var userNames = document.getElementsByName('users[][name]');
            var userEmails = document.getElementsByName('users[][email]');
            var i = 0;
            for (i = 0; i < userNames.length; i++) {
                var userEmail = $(userEmails[i]);
                if (is_email_exists(userEmail.val())) {
                    alert('A new user with this email is already added.');
                    return false;
                }
            }

            for (i = 0; i < userNames.length; i++) {
                var userName = $(userNames[i]);
                var userEmail = $(userEmails[i]);
                if (userName.val() != '' || userEmail.val() != '') {
                    if (userName.val() == '') {
                        alert('Please enter a valid Full Name.');
                        userName.focus();
                        return false;
                    }
                    if (userEmail.val() == '') {
                        alert('Please enter a valid Email.');
                        userEmail.focus();
                        return false;
                    }
                    if (is_email_exists(userEmail.val())) {
                        alert('A new user with this email is already added.');
                        return false;
                    }
                    var newUser = [];
                    newUser[newUser.length] = '';
                    newUser[newUser.length] = userName.val();
                    newUser[newUser.length] = userEmail.val();
                    lposted_data[lposted_data.length] = newUser;
                }
            }

            if (lposted_data.length == 0) {
                alert('Please enter atleast one Full name or email.');
                return;
            }
            $.ajax({
                type: 'POST',
                data: {
                    user_data: lposted_data,
                },
                url: '<?php echo $this->base; ?>/Huddles/verifyNewUsers',
                success: function (response) {
                    if (response != '') {
                        $('#flashMessage2').css('display', 'block');
                        $('#flashMessage2').html(response);
                    } else {

                        $('#flashMessage2').css('display', 'none');

                        new_ids -= 1;

                        for (var i = 0; i < lposted_data.length; i++) {

                            var data = lposted_data[i];
                            var html = '';

                            html += '<li>';
                            html += '<label class="huddle_permission_editor_row" for="super_admin_ids_' + new_ids + '"><input type="checkbox" checked="checked" value="' + new_ids + '" name="super_admin_ids[]" id="super_admin_ids_' + new_ids + '" style="display:none;"><a id="lblsuper_admin_ids_' + new_ids + '" style="color: #757575; font-weight: normal;">' + data[1] + '</a></label>';
                            html += '<input type="hidden" value="' + data[1] + '" name="super_admin_fullname_' + new_ids + '" id="super_admin_fullname_' + new_ids + '">';
                            html += '<input type="hidden" value="' + data[2] + '" name="super_admin_email_' + new_ids + '" id="super_admin_email_' + new_ids + '">';
                            if ($('#coach-huddle').is(':checked')) {
                                html += '<div class="permissions coach_mentee" style="float:right !important;">';
                            } else {
                                html += '<div class="permissions coach_mentee" style="display:none;float:right !important;">';
                            }

                            html += '<span class="caoch-checkbox" style="margin-right: 5px;">';
                            html += '<input type="radio" id="caoch-checkbox_' + new_ids + '" value="1" name="coach_' + new_ids + '"><label for="caoch-checkbox_' + new_ids + '" class="cls_sp_label">Coach</label></span>';

                            if ($('#coach-huddle').is(':checked')) {

                                html += '<input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_' + new_ids + '" name="is_coach_' + new_ids + '" style="display:none">';
                            }

                            if ($('#coach-huddle').is(':checked')) {

                                html += '<span class="mentee-checkbox"><input type="radio" value="2" id="mentee-checkbox_' + new_ids + '" name="coach_' + new_ids + '" class="chk_is_coachee" checked="checked"><label for="mentee-checkbox_' + new_ids + '" class="cls_sp_label">Coachee</label></span>';
                                $("#chk_is_coachee").val("1");

                            } else {

                                html += '<span class="mentee-checkbox"><input type="radio" value="2" id="mentee-checkbox_' + new_ids + '" name="coach_' + new_ids + '" class="chk_is_coachee" ><label for="mentee-checkbox_' + new_ids + '" class="cls_sp_label">Coachee</label></span>';
                            }

                            if ($('#coach-huddle').is(':checked')) {
                                html += '<input type="checkbox" value="1" class="coach_mentee_hidden" id="is_mentor_' + new_ids + '" name="is_mentor_' + new_ids + '" style="display:none" checked="checked">';
                            }

                            html += '</div>';

                            if ($('#collab-huddle').is(':checked')) {
                                html += '<div class="permissions coach_perms">';
                            } else {
                                html += '<div class="permissions coach_perms" style="display:none;">';
                            }
                            html += '<label for="user_role_' + new_ids + '_200"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_200" value="200">Admin</label>';
                            html += '<label for="user_role_' + new_ids + '_210"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_210" value="210" checked="checked">Member</label>';
                            html += '<label for="user_role_' + new_ids + '_220"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_220" value="220">Viewer</label>';
                            html += '</div>';
                            html += '<span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="' + new_ids + '" id="new_checkbox_' + new_ids + '"  name="" class="huddle_edit_cls_chkbx" checked="checked"><label class="cls_sp_label" for="new_checkbox_' + new_ids + '">Folder Participant</label></span>';
                            html += '</li>';
                            $('.groups-table .huddle-span4 .groups-table-content ul').prepend(html);
                            new_ids -= 1;
                            var newUser = [];
                            newUser[newUser.length] = data[0];
                            newUser[newUser.length] = data[1];
                            posted_data[posted_data.length] = newUser;
                        }

                        var $overview = $('.widget-scrollable.horizontal .overview');
                        $.each($overview, function () {

                            var width = $.map($(this).children(), function (child) {
                                return $(child).outerWidth() +
                                        $(child).pixels('margin-left') + $(child).pixels('margin-right');
                            });
                            $(this).width($.sum(width));

                        });

                        $('.widget-scrollable').tinyscrollbar();
                        $('#addSuperAdminModal').modal('hide');


                    }
                },
                errors: function (response) {
                    alert(response.contents);
                }

            });

        }


    </script>
