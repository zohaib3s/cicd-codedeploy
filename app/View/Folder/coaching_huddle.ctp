<?php
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$user_role = $this->Custom->get_user_role_name($users['users_accounts']['role_id']);
$account_id = $users['accounts']['account_id'];
?>
<style type="text/css">
    #HudDesc {
        min-height: 20px;
    }
    #HudTitle {
        min-height: 20px;
    }
</style>
<div class="container box newCoachingHuddle">
    <h1 id="wiz-heading">New Collaboration Huddle</h1>
    <script type="text/javascript">

        (function ($) {
            jQuery.expr[':'].Contains = function (a, i, m) {
                return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
            };

            function listFilter(header, list) {
                var form = $("<div>").attr({"class": "filterform"}),
                        input = $("<input id='input-filter' placeholder='Search people or group'><div  id='cancel-btn' type='text' style=' width: 10px;  position: absolute; right: 93px; margin-top: 7px; display:none;cursor: pointer; '>X</div>").attr({"class": "filterinput", "type": "text"});
                $(form).append(input).appendTo(header);
                $('.filterform input').css({'width': '100%', 'float': 'right'});
                $(input).change(function () {
                    var filter = $(this).val();
                    $('#liNodataFound').remove();
                    if (filter) {
                        $search_count = $(list).find("a:Contains(" + filter + ")").length;
                        if ($search_count > 4) {
                            $('div.thumb').css('top', '0px');
                            $('div.overview').css('top', '0px');
                            $('.scrollbar').css('display', 'block');
                        } else {
                            $('div.thumb').css('top', '0px');
                            $('div.overview').css('top', '0px');
                            $('.scrollbar').css('display', 'none');
                        }
                        $(list).find("a:not(:Contains(" + filter + "))").parent().parent().slideUp(400, function () {
                            $('.widget-scrollable').tinyscrollbar_update();
                        });
                        $(list).find("a:Contains(" + filter + ")").parent().parent().slideDown(400, function () {
                            $('.widget-scrollable').tinyscrollbar_update();
                        });
                        jQuery('#cancel-btn').css('display', 'block');
                        if ($(list).find("a:Contains(" + filter + ")").length == 0) {
                            $('#liNodataFound').remove();
                            $("#list-containers").append('<li id="liNodataFound">No people or groups match this search. Please try again.</li>');
                        } else {
                            $('#liNodataFound').remove();
                        }

                    } else {
                        jQuery('#cancel-btn').css('display', 'none');
                        $('.scrollbar').css('display', 'block');
                        $(list).find("li").slideDown(400, function () {
                            $('.widget-scrollable').tinyscrollbar_update();
                        });

                    }
                    return false;
                }).keyup(function () {
                    $(this).change();
                });

                $('#cancel-btn').click(function (e) {
                    jQuery('#cancel-btn').css('display', 'none');
                    $('div.thumb').css('top', '0px');
                    $('div.overview').css('top', '0px');
                    $('.scrollbar').css('display', 'block');
                    jQuery('#input-filter').val('');
                    $(list).find("li").slideDown(400, function () {
                        $('.widget-scrollable').tinyscrollbar_update();
                    });
                })
            }

            /*$(function () {
             listFilter($("#header-container"), $("#list-containers"));
             });*/
        }(jQuery));


        $(document).ready(function () {

            $('#caoch-checkbox').click(function (e) {
                if ($(this).is(':checked') == true) {
                    $(".caoch-checkbox input[type='checkbox']").prop('checked', true);
                } else {
                    $(".caoch-checkbox input[type='checkbox']").prop('checked', false);
                }
            })
            /*
             $("span.participant-checkbox1 input[type=radio]").click(function () {
             var huddle_row_selector = $(this).parent().parent().parent().find("span.eval-checkbox input[type='checkbox']");
             coachee_check_count = 0;
             $('.eval_huddle_edit_cls [type="checkbox"]:checked').each(function () {
             if ($('input[name=is_eval_participant_' + this.value + ']').is(':checked')) {
             var $radios = $('input:radio[name=user_role_' + this.value + ']');
             $radios.filter('[value=210]').prop('checked', true);
             coachee_check_count++;
             }
             });
             if (coachee_check_count > 0) {
             $(this).prop("checked", false);
             var $radioName = $(this).attr('id');
             console.log($radioName);
             var strCoach = $radioName.replace('participant', 'eval');
             $('#' + strCoach).prop("checked", true);
             //$(this).parent().parent().find('label [type="checkbox"]').prop("checked", false);
             alert('You can only select one Coachee. If you want this to be your Coachee, please unselect the Coachee you have already selected.');
             } else {
             $(this).parent().parent().find('label [type="checkbox"]').prop("checked", true);
             }
             if (huddle_row_selector.length > 0 && !huddle_row_selector.is(":checked")) {
             //huddle_row_selector.trigger('click');
             //$("#chk_is_coachee").val("1");

             }

             });
             $("span.mentee-checkbox input[type=radio]").click(function () {

             var huddle_row_selector = $(this).parent().parent().parent().find("span.caoch-checkbox input[type='checkbox']");
             coachee_check_count = 0;
             $('.huddle_edit_cls [type="checkbox"]:checked').each(function () {
             if ($('input[name=is_mentor_' + this.value + ']').is(':checked')) {
             var $radios = $('input:radio[name=user_role_' + this.value + ']');
             $radios.filter('[value=210]').prop('checked', true);
             coachee_check_count++;
             }
             });
             if (coachee_check_count > 0) {
             $(this).prop("checked", false);
             var $radioName = $(this).attr('id');
             var strCoach = $radioName.replace('mentee', 'caoch');
             $('#' + strCoach).prop("checked", true);
             //$(this).parent().parent().find('label [type="checkbox"]').prop("checked", false);
             alert('You can only select one Coachee. If you want this to be your Coachee, please unselect the Coachee you have already selected.');
             } else {
             $(this).parent().parent().find('label [type="checkbox"]').prop("checked", true);
             }
             if (huddle_row_selector.length > 0 && !huddle_row_selector.is(":checked")) {
             //huddle_row_selector.trigger('click');
             //$("#chk_is_coachee").val("1");

             }

             });
             */
            $("span.caoch-checkbox input[type=radio]").click(function () {
                var huddle_row_selector = $(this).parent().parent().parent().find("span.caoch-checkbox input[type='checkbox']");
                if (huddle_row_selector.length > 0 && !huddle_row_selector.is(":checked")) {
                    //huddle_row_selector.trigger('click');
                }
            });
            $("span.eval-checkbox input[type=radio]").click(function () {
                var huddle_row_selector = $(this).parent().parent().parent().find("span.caoch-checkbox input[type='checkbox']");
                if (huddle_row_selector.length > 0 && !huddle_row_selector.is(":checked")) {
                    //huddle_row_selector.trigger('click');
                }
            });


            $('#select-all-none').click(function (e) {
                if ($('#select-all').is(':checked')) {
                    $('#select-all-label').html('Select None');
                    $('#select-all').prop('checked', true);
                    $(".member-user").prop('checked', true);
                    $(".super-user").prop('checked', true);
                    $(".viewer-user").prop('checked', true);
                    $(".huddle_edit_cls_chkbx").prop('checked', true);
                } else {
                    $('#select-all-label').html('Select All');
                    $('#select-all').prop('checked', false);
                    $(".member-user").prop('checked', false);
                    $(".super-user").prop('checked', false);
                    $(".viewer-user").prop('checked', false);
                    $(".huddle_edit_cls_chkbx").prop('checked', false);
                }

            });
        })

    </script>
    <script>
        $(document).ready(function () {
            base_url = "<?php echo $this->base . '/Folder/createhuddle/' . $folder_id; ?>"

            $('#wiz-heading').text('New Coaching / Mentoring Huddle');
            //$('#step2-label').text('Coaching Huddle creator is defaulted to be a coach in the Huddle. You may add another coach to the huddle, but you must select a participant to be coached.');
            $('#step2-label').text('You\'re defaulted to be the coach in the Huddle. You may add another participant to help coach, but you must select a coachee to be coached.');
            /*$('#video_huddle_name').prop("disabled", true);
             $('#video_huddle_description').prop("disabled", true);*/

            $('#coaching-huddle-groups').css("display", "block");
            $('#video_huddle_name').css("display", "none");
            $('#video_huddle_description').css("display", "none");
            $('#video_huddle_name').css("background-color", '#F0EEE3');
            $('#video_huddle_description').css("background-color", '#F0EEE3');
            $('.coach_mentee').css('display', 'block');
            $('.grpClass').css('display', 'none');
            $('.coach_perms').css('display', 'none');
            $('.coach_mentee').css('float', 'none');
            $('.coach_mentee').attr('style', 'float: right !important');
            $('#select_all_coaches_panel').css('display', 'none');
            $('#video_huddle_name').val('');
            $('#video_huddle_description').val('');
            $('#select-all-none-cnt').css('display', 'none');
            $('.groups-table-header').css('padding-bottom', '10px');
            $("#Coaches_info").css("display", "none");
            $("#evaluation_info").css("display", "none");
            $("#Coachee_info").css("display", "block");
            $("#chkcoacheepermissions").show();
            $("#chkcoacheepermissionslabel").show();
            $('.coach_evaluation').css('display', 'none');
            $('#select_all_evaluation_panel').css('display', 'none');
            $(".eval_huddle_edit_cls").css('display', 'none');
            $(".huddle_edit_cls").css('display', 'block');
            $('#eval_submission_deadline').css('display', 'none');
            $('.huddle_edit_cls_chkbx').removeAttr('checked');
            $('#select-all').removeAttr('checked');
            $("span.mentee-checkbox input[type=radio]").each(function () {
                if ($(this).is(":checked")) {
                    $(this).parent().parent().find('[type="checkbox"]').remove();
                    var radio_id = $(this).attr('id').split('_');
                    //$('#super_admin_ids_'+radio_id[1]).prop('checked', true);
                    //$('#new_checkbox_'+radio_id[1]).prop('checked', true);
                    var html = '<input type="checkbox" class="coach_mentee_hidden" value="1" id="is_mentor_' + radio_id[1] + '" name="is_mentor_' + radio_id[1] + '" style="display:none" checked="checked" />';
                    $(this).parent().parent().append(html);
                }

            });

            $("span.caoch-checkbox input[type=radio]").each(function () {
                if ($(this).is(":checked")) {
                    $(this).parent().parent().find('[type="checkbox"]').remove();
                    var radio_id = $(this).attr('id').split('_');
                    //$('#super_admin_ids_'+radio_id[1]).prop('checked', true);
                    //$('#new_checkbox_'+radio_id[1]).prop('checked', true);
                    var html = '<input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_' + radio_id[1] + '" name="is_coach_' + radio_id[1] + '" style="display:none"  checked="checked">';
                    $(this).parent().parent().append(html);
                }

            });

            $('#eval-huddle').click(function () {

                window.location.href = base_url + '/3';
            });
            $('#collab-huddle').click(function () {
                window.location.href = base_url + '/1';
            });

            $('.firstone').click(function () {
                $('.thirdone').removeClass('ui-tabs-active');
                $('.thirdone').removeClass('ui-state-active');
                $('.firstone').addClass('ui-tabs-active');
                $('.firstone').addClass('ui-state-active');
                $("#step-3").css("display", "none");
                $("#step-1").css("display", "block");
            });
            $('.step2').click(function () {
                $("#tabs").tabs("option", "active", 1);
                $('.thirdone').removeClass('ui-tabs-active');
                $('.thirdone').removeClass('ui-state-active');
                $('.secondone').addClass('ui-tabs-active');
                $('.secondone').addClass('ui-state-active');
                $("#step-3").css("display", "none");
                $("#step-2").css("display", "block");
                setTimeout(function () {
                    $('#input-filter').trigger('change');
                }, 200);

                $('#HudTitle').css("display", "block");
                $('#HudDesc').css("display", "block");
                if ($('#collab-huddle').is(':checked')) {
                    $('#eval_required_deadline').css('display', 'none');
                    if ($('#video_huddle_name').val() != '') {
                        $("#huddle-name-last").text($('#video_huddle_name').val());
                        $("#huddle-name-last").css('width', '250px');
                        $("#name-id").val($('#video_huddle_name').val());
                        $("#ajaxInputName").val($('#video_huddle_name').val());
                        $(".editArea").css("display", "none");
                    } else {
                        $(".editArea").css("display", "block");
                    }
                    if ($('#video_huddle_description').val() != '') {
                        $("#huddle-desc-last").text($('#video_huddle_description').val());
                        $("#huddle-desc-last").css('width', '250px');
                        $("#desc-id").val($('#video_huddle_description').val());
                        $("#ajaxInputDesc").val($('#video_huddle_description').val());
                        $(".editAread").css("display", "none");
                    } else {
                        $(".editAread").css("display", "block");
                    }
                } else if ($('#eval-huddle').is(':checked')) {
                    $('#eval_required_deadline').css('display', 'block');
                    $('#eval-submission-date').val($('#date').val());
                    $('#eval-submission-time').val($('#time').val());

                }
                if ($('#coach-huddle').is(':checked')) {
                    $('#eval_required_deadline').css('display', 'none');
                    $(".editArea").css("display", "none");
                    $(".editAread").css("display", "none");
                }

            });
            /*$('.secondone').click(function(){

             $( "#tabs" ).tabs("option", "active", 1 );
             setTimeout(function(){
             $('#input-filter').trigger('change');
             }, 200);

             $('#HudTitle').css("display", "block");
             $('#HudDesc').css("display", "block");
             if($('#collab-huddle').is(':checked')) {
             if($('#video_huddle_name').val()!=''){
             $("#huddle-name-last").text($('#video_huddle_name').val());
             $("#huddle-name-last").css('width','250px');
             $("#name-id").val($('#video_huddle_name').val());
             $("#ajaxInputName").val($('#video_huddle_name').val());
             $(".editArea").css("display", "none");
             }else{
             $(".editArea").css("display", "block");
             }
             if($('#video_huddle_description').val()!=''){
             $("#huddle-desc-last").text($('#video_huddle_description').val());
             $("#huddle-desc-last").css('width','250px');
             $("#desc-id").val($('#video_huddle_description').val());
             $("#ajaxInputDesc").val($('#video_huddle_description').val());
             $(".editAread").css("display", "none");
             }else{
             $(".editAread").css("display", "block");
             }
             }
             if($('#coach-huddle').is(':checked')) {
             $(".editArea").css("display", "none");
             $(".editAread").css("display", "none");
             }

             });*/
            $('.step3').click(function () {
                if ($('#coach-huddle').is(':checked')) {
                    var h_type = 2
                    $('#eval_required_deadline').css('display', 'none');
                }
                else {
                    var h_type = 1
                }
                var participants = ['<?php echo $users['User']['id']; ?>||200||' + h_type];
                var mentor_name = '';
                var huddle_type = 1;
                $(".editArea").css("display", "none");
                $(".editAread").css("display", "none");
                $('#HudTitle').css("display", "block");
                $('#HudDesc').css("display", "block");
                if ($('#collab-huddle').is(':checked')) {
                    $('#eval_required_deadline').css('display', 'none');
                    if ($('#video_huddle_name').val() != '') {
                        $("#huddle-name-last").text($('#video_huddle_name').val());
                        $("#huddle-name-last").css('width', '250px');
                        $("#name-id").val($('#video_huddle_name').val());
                        $("#ajaxInputName").val($('#video_huddle_name').val());
                        $(".editArea").css("display", "none");
                    } else {
                        $(".editArea").css("display", "block");
                    }
                    if ($('#video_huddle_description').val() != '') {
                        $("#huddle-desc-last").text($('#video_huddle_description').val());
                        $("#huddle-desc-last").css('width', '250px');
                        $("#desc-id").val($('#video_huddle_description').val());
                        $("#ajaxInputDesc").val($('#video_huddle_description').val());
                        $(".editAread").css("display", "none");
                    } else {
                        $(".editAread").css("display", "block");
                    }
                }
                var coachee_count = 0;
                $('input[name="super_admin_ids[]"]:checked').each(function () {
                    if ($('#coach-huddle').is(':checked')) {

                        if ($('input[name=is_coach_' + this.value + ']').is(':checked')) {
                            var $radios = $('input:radio[name=user_role_' + this.value + ']');
                            $radios.filter('[value=200]').prop('checked', true);
                        } else {
                            if ($('input[name=is_mentor_' + this.value + ']').is(':checked')) {
                                console.log($('input[name=new_checkbox_' + this.value + ']').is(':checked'));
                                var $radios = $('input:radio[name=user_role_' + this.value + ']');
                                $radios.filter('[value=210]').prop('checked', true);
                                coachee_count++;

                            }
                        }
                    }

                    var role = $('input[name=user_role_' + this.value + ']:checked').val();
                    if ($('input[name=is_coach_' + this.value + ']').is(':checked') || $('input[name=is_mentor_' + this.value + ']').is(':checked')) {
                        role = role + '||2';
                    }
                    if ($('input[name=is_coach_' + this.value + ']').is(':checked') || $('input[name=is_mentor_' + this.value + ']').is(':checked')) {
                        role = role + '||2';
                    } else if ($('input[name=is_evaluator_' + this.value + ']').is(':checked') || $('input[name=is_eval_participant_' + this.value + ']').is(':checked')) {
                        role = role + '||3';
                    }
                    else {
                        role = role + '||1';
                    }
                    if (this.value < 0) {
                        role = role + '||' + $("#super_admin_fullname_" + this.value).val();
                    }
                    participants.push(this.value + '||' + role);
                    if ($('input[name="is_mentor_' + this.value + '"]').is(':checked')) {
                        if (mentor_name == '')
                            mentor_name = $('#lblsuper_admin_ids_' + this.value).html();
                    }
                });

                if (h_type == 2 && coachee_count == 0) {
                    alert('You must select a coachee to be coached. Please select the Huddle participant to add a coachee.');
                    return;
                }
                if (h_type == 3 && coachee_count == 0) {
                    alert('You must select a assessed participant to be assessed. Please select the Huddle participant to add a assessed participant.');
                    return;
                }

                if (h_type == 2 && coachee_count > 1) {
                    alert('You can only select one Coachee. If you want this to be your Coachee, please unselect the Coachee you have already selected.');
                    return;
                }
                $('.step3 a').attr('href', '#step-3');
                $('.thirdone').addClass('ui-tabs-active');
                $('.thirdone').addClass('ui-state-active');
                $('.secondone').removeClass('ui-tabs-active');
                $('.secondone').removeClass('ui-state-active');
                $('.firstone').removeClass('ui-tabs-active');
                $('.firstone').removeClass('ui-state-active');
                $("#tabs").tabs("option", "active", 2);
                $("#step-1").css("display", "none");
                $("#step-2").css("display", "none");
                $("#step-3").css("display", "block");
                $('input[name="group_ids[]"]:checked').each(function () {
                    if ($('#coach-huddle').is(':checked')) {
                        if ($('input[name=is_coach_' + this.value + ']').is(':checked')) {
                            var $radios = $('input:radio[name=group_role_' + this.value + ']');
                            $radios.filter('[value=200]').prop('checked', true);
                        } else {
                            if ($('input[name=is_mentor_' + this.value + ']').is(':checked')) {
                                var $radios = $('input:radio[name=group_role_' + this.value + ']');
                                $radios.filter('[value=210]').prop('checked', true);
                            }
                        }
                    }
                    var role = $('input[name=group_role_' + this.value + ']:checked').val();
                    if ($('input[name=is_coach_' + this.value + ']').is(':checked') || $('input[name=is_mentor_' + this.value + ']').is(':checked')) {
                        role = role + '||2';
                    } else if ($('input[name=is_evaluator_' + this.value + ']').is(':checked') || $('input[name=is_eval_participant_' + this.value + ']').is(':checked')) {
                        role = role + '||3';
                    } else {
                        role = role + '||1';
                    }
                    participants.push(this.value + '||' + role);
                    if ($('input[name="is_mentor_' + this.value + '"]').is(':checked')) {
                        if (mentor_name == '')
                            mentor_name = $('#lblsuper_admin_ids_' + this.value).html();
                        //console.log($('#lblsuper_admin_ids_'+this.value).html());
                    }
                });
                if ($('#coach-huddle').is(':checked')) {
                    huddle_type = 2;
                    if (typeof mentor_name == "undefined" || mentor_name == '')
                        mentor_name = 'Coaching Huddle 2015';

                    $("#huddle-name-last").text(mentor_name);
                    $("#name-id").val(mentor_name);
                    $("#ajaxInputName").val(mentor_name);
                    $("#huddle-desc-last").html('&nbsp;');
                    $("#desc-id").val('');
                    $("#ajaxInputDesc").val('');
                    $('.huddle_edit_cls_chkbx').removeAttr('checked');
                }
                else if ($('#eval-huddle').is(':checked')) {
                    huddle_type = 3;

                }
                $.ajax({
                    type: 'POST',
                    data: {
                        participants: participants,
                        type: huddle_type,
                    },
                    url: "<?php echo $this->base . '/Huddles/get_people_ajax' ?>",
                    success: function (res) {
                        $('#participants').html(res);
                    }
                });

            });
            /*$('.thirdone').click(function(){
             $( "#tabs" ).tabs("option", "active", 2 );
             var participants = [];
             var mentor_name = ''

             $(".editArea").css("display", "none");
             $(".editAread").css("display", "none");
             $('#HudTitle').css("display", "block");
             $('#HudDesc').css("display", "block");

             $('input[name="super_admin_ids[]"]:checked').each(function() {
             if($('#coach-huddle').is(':checked')) {
             if($('input[name=is_coach_'+this.value+']').is(':checked')){
             var $radios = $('input:radio[name=user_role_'+this.value+']');
             $radios.filter('[value=200]').prop('checked', true);
             }else{
             if($('input[name=is_mentor_'+this.value+']').is(':checked')){
             var $radios = $('input:radio[name=user_role_'+this.value+']');
             $radios.filter('[value=210]').prop('checked', true);
             }
             }
             }
             var role = $('input[name=user_role_'+this.value+']:checked').val();
             participants.push(this.value+'||'+role);
             if($('input[name="is_mentor_'+this.value).is(':checked')) {
             if(mentor_name == '')
             mentor_name = $('#lblsuper_admin_ids_'+this.value).html();
             //console.log($('#lblsuper_admin_ids_'+this.value).html());
             }
             });
             $('input[name="group_ids[]"]:checked').each(function() {
             var role = $('input[name=group_role_'+this.value+']:checked').val();
             participants.push(this.value+'||'+role);
             if($('input[name="is_mentor_'+this.value).is(':checked')) {
             if(mentor_name == '')
             mentor_name = $('#lblsuper_admin_ids_'+this.value).html();
             }
             });
             if($('#coach-huddle').is(':checked')) {
             if(mentor_name == '')
             mentor_name = 'Coaching Huddle 2015';

             $("#huddle-name-last").text(mentor_name);
             $("#name-id").val(mentor_name);
             $("#ajaxInputName").val(mentor_name);
             $("#huddle-desc-last").html('&nbsp;');
             $("#desc-id").val(' ');
             $("#ajaxInputDesc").val(' ');
             }
             $.ajax({
             type: 'POST',
             data: {
             participants: participants,
             },
             url: base_url + '/Huddles/get_people_ajax/',
             success: function (res) {
             $('#participants').html(res);
             }
             });

             });*/
        });
        $(function () {

            $('.coach_mentee').css('display', 'block');
            $('.grpClass').css('display', 'block');
            $("#tabs").tabs();



            $(document).on("change", '.huddle_edit_cls [type="checkbox"]', function () {

                if ($(this).is(":checked")) {
                    coachee_check_count = 0;
                    if ($('#coach-huddle').is(':checked')) {
                        $('.huddle_edit_cls [type="checkbox"]:checked').each(function () {
                            if ($('input[name=is_mentor_' + this.value + ']').is(':checked')) {
                                var $radios = $('input:radio[name=user_role_' + this.value + ']');
                                $radios.filter('[value=210]').prop('checked', true);
                                coachee_check_count++;
                            }
                        });
                        if (coachee_check_count > 1) {
                            $(this).parent().parent().find('label [type="checkbox"]').prop("checked", false);
                            $(this).prop("checked", false);

                            alert('You can only select one Coachee. If you want this to be your Coachee, please unselect the Coachee you have already selected.');
                        } else {

                            $(this).parent().parent().find('label [type="checkbox"]').prop("checked", true);
                        }
                    } else {

                        $(this).parent().parent().find('label [type="checkbox"]').prop("checked", true);
                    }
                }
                else {

                    $(this).parent().parent().find('label [type="checkbox"]').prop("checked", false);
                }
            });

            $("#eval_trig").click(function () {
                $(".cls_eval_trig").trigger("click");
            });
            $("#coaching_trig_2").click(function () {
                $(".cls_coaching_trig_2").trigger("click");
            });
            $("#cls_coaching_trig").click(function () {
                $(".cls_coaching_trig").trigger("click");
            });

            $(document).on("change", '.coach_mentee [type="radio"]', function () {
                var radio_val = $(this).val();
                var radio_name = $(this).attr('name');
                var get_radio_id = radio_name.split("_");
                var radio_id = get_radio_id[1];
                var html = '';
                $(this).parent().parent().find('[type="checkbox"]').remove();
                if (radio_val == "1") {
                    html = '<input type="checkbox" class="coach_coach_hidden" value="1" id="is_coach_' + radio_id + '" name="is_coach_' + radio_id + '" style="display:none" checked="checked" />';
                    $(this).parent().parent().append(html);
                }
                else {
                    html = '<input type="checkbox" class="coach_mentee_hidden" value="1" id="is_mentor_' + radio_id + '" name="is_mentor_' + radio_id + '" style="display:none" checked="checked" />';
                    $(this).parent().parent().append(html);
                }


            });
            $(document).on("change", '.coach_evaluation [type="radio"]', function () {
                var radio_val = $(this).val();
                var radio_name = $(this).attr('name');
                var get_radio_id = radio_name.split("_");
                var radio_id = get_radio_id[1];
                var html = '';
                $(this).parent().parent().find('[type="checkbox"]').remove();
                if (radio_val == "1") {
                    html = '<input type="checkbox" class="eval_coach_hidden" value="1" id="is_evaluator_' + radio_id + '" name="is_evaluator_' + radio_id + '" style="display:none" checked="checked" />';
                    $(this).parent().parent().append(html);
                }
                else {
                    html = '<input type="checkbox" class="eval_participant_hidden" value="1" id="is_eval_participant_' + radio_id + '" name="is_eval_participant_' + radio_id + '" style="display:none" checked="checked" />';
                    $(this).parent().parent().append(html);
                }
            });
        });
        $(document).on("click", '#ui-id-1', function () {
            $(".appendix-content").hide();
        });



    </script>
    <style>
        input[type="search"]{
            border: 1px solid #c5c5c5;
            border-radius: 3px;
            box-shadow: 0 1px 2px rgba(0,0,0,0.09) inset;
            font-size: 13px;
            font-weight: bold;
            color: #222;
            padding: 8px 4px;
        }
        .scrolable_box {
            height:200px;
            overflow-y:auto;
            padding-left: 12px;
        }
    </style>
    <form method="post" id="new_video_huddle" enctype="multipart/form-data" class="new_video_huddle"
          action="<?php echo $this->base . '/Folder/createhuddle/' . $folder_id; ?>" accept-charset="UTF-8">
        <div id="tabs">
            <ul class="wizard-steps">
                <li class="firstone"><a href="#step-1"><span>1</span> Huddle Types</a></li>
                <li class="secondone step2"><a href="#step-2"><span>2</span> Participants</a></li>
                <li class="thirdone step3"><a><span>3</span> Review</a></li>
                <div style="clear: both;"></div>
            </ul>
            <div class="wiz-container">
                <div id="step-1">
                    <div style="margin:0;padding:0;display:inline"><input type="hidden" value="✓" name="utf8"></div>

                    <a class="close-reveal-modal"></a>
                    <div class="span5">
                        <div class="row">
                            <?php if ($user_permissions['UserAccount']['manage_coach_huddles'] == '1'): ?>
                                <span class="wiz-step1-radio"><input type="radio" id="coach-huddle" class="cls_coaching_trig" checked="checked" name="type" value="2"><label  id="cls_coaching_trig"> Coaching / Mentoring Huddle</label></span>
                                <a  onMouseOut="hide_sidebar()" class="appendix appendix2" href="#">?</a>
                                <div id="coachee_help" class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                    <p>Coaching/Mentoring Huddles enhance and personalize 1:1 coaching and mentoring.  These Huddles are private and secure and help establish trust between professionals.</p>
                                </div>
                            <?php endif; ?>

                            <div style="clear: both;height:10px;"></div>
                            <?php if ($user_permissions['UserAccount']['manage_collab_huddles'] == '1'): ?>
                                <?php if ($user_permissions['UserAccount']['manage_coach_huddles'] == '1') { ?>
                                    <span class="wiz-step1-radio"><input type="radio" id="collab-huddle" name="type" class="cls_coaching_trig_2"  value="1"><label id="coaching_trig_2"> Collaboration Huddle</label></span>
                                <?php } else { ?>
                                    <span class="wiz-step1-radio"><input type="radio" id="collab-huddle" name="type" class="cls_coaching_trig_2" checked="checked"  value="1"><label id="coaching_trig_2"> Collaboration Huddle</label></span>
                                <?php } ?>
                                <a onMouseOut="hide_sidebar()" class="appendix appendix2" href="#">?</a>
                                <div id="collab_help"  class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                    <p>A Collaboration Huddle is a great way to conduct blended professional development workshops or trainings, professional learning communities, peer observations, or any other learning environment where videos and related resources can be shared, annotated, and discussed.</p>
                                </div>
                            <?php endif; ?>
                            <div style="clear: both;height:10px;"></div>
                            <?php if ($this->Custom->check_if_eval_huddle_active($user_permissions['UserAccount']['account_id']) && $user_permissions['UserAccount']['manage_evaluation_huddles'] == '1'): ?>
                                <?php if ($users['roles']['role_id'] == 120): ?>
                                    <?php if ($user_permissions['UserAccount']['manage_evaluation_huddles'] == '1') { ?>
                                        <span class="wiz-step1-radio"><input type="radio" id="eval-huddle" name="type" class="cls_eval_trig"  value="3"><label id="eval_trig"> Assessment Huddle</label></span>
                                    <?php } else { ?>
                                        <span class="wiz-step1-radio"><input type="radio" id="eval-huddle" name="type" class="cls_eval_trig" checked="checked"  value="3"><label id="eval_trig"> Assessment Huddle</label></span>
                                    <?php } ?>
                                <?php else: ?>
                                    <span class="wiz-step1-radio"><input type="radio" id="eval-huddle" name="type" class="cls_eval_trig"  value="3"><label id="eval_trig"> Assessment Huddle</label></span>
                                <?php endif; ?>
                                <a onMouseOut="hide_sidebar()" class="appendix appendix2" href="#">?</a>
                                <div id="collab_help"  class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                    <p>Assessment Huddles are designed specifically for not only giving feedback to performance task, but also adding customizable ratings in reference to standards or competencies on a rubric or framework. </p>
                                </div>
                            <?php endif; ?>

                        </div>
                        <div id="collab_name" class="row" style="margin-bottom: 20px;display:none;">
                            <input name="data[name]" id="video_huddle_name" class="size-big huddle-name "  placeholder="Huddle Name" size="30" type="text"  style="margin-top: 20px;" required/>
                            <label for="video_huddle_name" class="error" style="display:none;background-color: none !important;">This field is required.</label>
                        </div>
                        <div style="clear: both;"></div>
                        <div id="collab_desc" class="row" style="display:none;">
                            <textarea name="data[description]" rows="4" placeholder="Huddle Description (Optional)" id="video_huddle_description" cols="40" class="size-big" style="display: block; background-color: rgb(255, 255, 255);margin-bottom: 20px;"></textarea>
                        </div>
                        <div id="eval_submission_deadline" class="row" style="display:none;">
                            <label class="" style="display: block;">Submission Deadline:</label>
                            <input type="text" id="date"  class="size-small"  placeholder="Date" required>
                            <input type="text" id="time"  class="size-small"  placeholder="Time" required>
                        </div>

                        <?php echo $this->Form->input('created_by', array('value' => $user_id, 'type' => 'hidden', 'id' => 'video_huddle_created_by')); ?>
                        <br/>
                    </div>


                    <div class="row-fluid">
                    </div>
                    <?php if ($this->Custom->is_enabled_framework_and_standards($account_id) && !$this->Custom->check_trial($account_id)): ?>
                        <input type="checkbox" id="chkenableframework" name="chkenableframework" checked="checked" value="1"><label for="chkenableframework" style="padding-left: 7px;">Enable Framework/Rubric in this Huddle</label>
                        <br/><br/>
                        <?php if (!empty($frameworks) && count($frameworks) > 0): ?>
                            <div id="frmDiv">
                                <select name="frameworks" id="frameworks">
                                    <option value="">Select Framework</option>
                                    <?php foreach ($frameworks as $framework) { ?>
                                        <option <?php
                                        if ($default_framework == $framework['AccountTag']['account_tag_id']) {
                                            echo 'selected';
                                        }
                                        ?> value="<?php echo $framework['AccountTag']['account_tag_id'] ?>"><?php echo $framework['AccountTag']['tag_title'] ?></option>
                                        <?php }
                                        ?>
                                </select>
                                <br/><br/>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if ($this->Custom->is_enable_tags($account_id) && !$this->Custom->check_trial($account_id)): ?>
                        <input type="checkbox" id="chkenabletags" name="chkenabletags" checked="checked" value="1"><label for="chkenabletags" style="padding-left: 7px;">Enable custom Video Markers in this Huddle</label>
                    <?php endif; ?>
                    <br/><br/>
                    <input type="checkbox" id="chkcoacheepermissions" name="chkcoacheepermissions" checked="checked" value="1"><label id="chkcoacheepermissionslabel" for="chkcoacheepermissions" style="padding-left: 7px;">Enable the Coachee permission to download, copy, edit, and delete all videos in this Huddle.</label>
                    <br/><br/>
                    <input type="checkbox" id="coach_hud_feedback" name="coach_hud_feedback" checked="checked" value="1"><label for="coach_hud_feedback" style="padding-left: 7px;">Publish comments for coachee after completing observation</label>
                    <div class="form-actions" style="text-align:left;">
                        <a href="#" class="btn btn-green step2" id="next-step2">Next</a>
                        <a class="btn btn-transparent" href="<?php echo $this->base . '/Huddles' ?>">Cancel</a>
                    </div>
                </div>
                <div id="step-2" style="display: none;">
                    <h3 id="step2-label" style="float: left;margin-top: 5px;margin-bottom: 10px;font-size: 14px;">Invite other people in your account to participate with you in the huddle. You can always do this later. You're defaulted to be an Admin in the Huddle.</h3>
                    <div class="row-fluid">
                        <div class="groups-table span12">

                            <div class="span4 huddle-span4" style="margin-left:0px;">
                                <div class="groups-table-header groups-table-header-create">
                                    <div style="width: 200px; float: left; margin-left: 8px;top: 10px;position: relative;padding-bottom: 18px;">
                                        <?php if ($user_permissions['UserAccount']['permission_maintain_folders'] == 1 && $user_permissions['UserAccount']['permission_administrator_user_new_role'] == 1): ?>
                                            <a style="margin-left: -8px;" id="pop-up-btn" data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="btn btn-small btn-green" href="#"><span class="plus">+</span>
                                            </a>
                                        <?php else: ?>
                                            <a style="margin-left: -8px;"><span class="">&nbsp;&nbsp;</span>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                    <div style="width: 248px; padding: 0px;float: left;margin-left: 426px;" class="search-box">
                                        <div  id="header-container" class="filterform">
                                        </div>
                                    </div>

                                    <a onMouseOut="hide_sidebar()" class="appendix right" href="#" id="Coachee_info" style="display:none;">?</a>
                                    <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                        <h3>Info</h3>
                                        <p>Coach(es):  Can edit coaching Huddle, by adding or removing other coaches in the Huddle; upload/download/copy all videos and supporting resources; clip/edit videos; add/edit/delete all video annotations; enable/disable tags/account framework; create/participate/edit all discussions; delete the coaching Huddle.</p>
                                        <p>Coachee: Can upload/download/copy/delete videos and resources they add to the Huddle; clip/edit their own videos; add/edit/delete their video annotations and comments; create/participate in Huddle discussions.</p>
                                    </div>

                                    <div style="clear:both"></div>
                                    <div id="searchdiv" class="filterform" style="<?php echo $user_permissions['UserAccount']['permission_maintain_folders'] == 1 && $user_permissions['UserAccount']['permission_administrator_user_new_role'] == 1 ? "top: -20px;" : "top: -8px;"; ?>position: relative;margin-bottom: -1px;">
                                        <fieldset>
                                            <input type="search" name="search" value="" id="id_search" placeholder='Search people or group' class="filterinput" style="margin-right: 48px;width: 238px; float: right; margin-top: -32px;">
                                            <div id='cancel-srch' style='width:10px;position:absolute; right:60px; margin-top:-27px;cursor:pointer;display:none;'>X</div>
                                        </fieldset>

                                    </div>
                                    <?php if (count($super_users) > 1 || (isset($huddleUsers) && count($huddleUsers) > 1) || (isset($users_groups) && count($users_groups) > 0)): ?>
                                        <div id="select-all-none-cnt" class="select-all-none selectnone"  style="float: left;margin-left: 442px;margin-top: -13px;min-width: 123px;max-width: 200px;">
                                            <label id="select-all-none" for="select-all"><input type="checkbox" name="select-all" id="select-all" style="display:inline-block;" /> <span id="select-all-label">Select All</span></label>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (1 == 1): ?>
                                        <div id="select_all_coaches_panel" class="huddles-select-all-block coach_mentee1">
                                            <span class="caoch-checkbox" >
                                                <input type="checkbox" id="caoch-checkbox" name="is_coach"> Coach</span>
                                            <span class="mentee-checkbox"><input type="checkbox" id="mentee-checkbox" name="is_mentor" >Coachee</span>
                                        </div>
                                    <?php endif; ?>


                                    <div style="clear:both"></div>
                                </div>
                                <div class="groups-table-content">
                                    <div class="scrolable_box">
                                        <div>
                                            <div style="top: 0px;">
                                                <div  id="people-lists">
                                                    <ul id="list-containers">
                                                        <input type="hidden" id="chk_is_coachee" value="0" name="chk_is_coachee" />
                                                        <?php
                                                        if (count($users_record) > 0):
                                                            ?>
                                                            <?php if ($users_record): ?>
                                                                <?php foreach ($users_record as $row): ?>
                                                                    <?php
                                                                    if ($row['id'] == $user_id)
                                                                        continue;
                                                                    ?>
                                                                    <?php if ($row['is_user'] == 'admin'): ?>
                                                                        <li style="display:none;">
                                                                            <label  class="huddle_permission_editor_row" for="super_admin_ids_<?php echo $row['id'] ?>"><input class="super-user" type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>" style="display: none;"> <a style="color: #757575; font-weight: normal;" id="lblsuper_admin_ids_<?php echo $row['id'] ?>"><?php echo trim($row['first_name'] . " " . $row['last_name']); ?> </a></label>
                                                                            <input type="hidden" value="<?php echo $row['email'] ?>" name="super_admin_email_<?php echo $row['id'] ?>" id="super_admin_email_<?php echo $row['id'] ?>">

                                                                            <?php if (1 == 1): ?>
                                                                                <div class="permissions coach_mentee">
                                                                                    <span class="caoch-checkbox" ><input type="radio" value="1" id="caoch-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>"><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>" >Coach</label></span>
                                                                                    <span class="mentee-checkbox"><input value="2" type="radio" id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" class="chk_is_coachee" checked="checked"> <span id="mentee-chk"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span></span>
                                                                                </div>

                                                                            <?php endif; ?>
                                                                            <div class="permissions coach_perms" style="display: none;">
                                                                                <label for="user_role_<?php echo $row['id'] ?>_200"><input  class="admin-btn admin-role" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200"/>Admin
                                                                                </label>
                                                                                <label for="user_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn member-role"  type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" checked="checked"/>Member
                                                                                </label>
                                                                                <label for="user_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn viewer-role" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220"/>Viewer
                                                                                </label>
                                                                            </div>
                                                                            <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="<?php echo $row['id'] ?>" id="new_checkbox_<?php echo $row['id'] ?>"  name="" class="huddle_edit_cls_chkbx"><label class="cls_sp_label" for="new_checkbox_<?php echo $row['id'] ?>">Huddle Participant</label></span>
                                                                        </li>

                                                                    <?php elseif ($row['is_user'] == 'member'): ?>
                                                                        <li>
                                                                            <label  class="huddle_permission_editor_row" for="super_admin_ids_<?php echo $row['id'] ?>"><input class="member-user"  type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>" style="display: none;"> <a style="color: #757575; font-weight: normal;" id="lblsuper_admin_ids_<?php echo $row['id'] ?>"> <?php echo $row['first_name'] . " " . $row['last_name'] ?></a></label>

                                                                            <?php if (1 == 1): ?>
                                                                                <div class="permissions coach_mentee">
                                                                                    <span class="caoch-checkbox" >
                                                                                        <input type="radio" id="caoch-checkbox_<?php echo $row['id'] ?>" value="1"  name="coach_<?php echo $row['id'] ?>"><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>">Coach</label>
                                                                                    </span>
                                                                                    <span class="mentee-checkbox"><input type="radio" value="2"   id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" class="chk_is_coachee" checked="checked"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span>
                                                                                </div>

                                                                            <?php endif; ?>
                                                                            <div class="permissions coach_perms" style="display: none;">
                                                                                <label for="user_role_<?php echo $row['id'] ?>_200"><input class="admin-btn admin-role" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200"/>Admin
                                                                                </label>
                                                                                <label for="user_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn member-role" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" checked="checked"/>Member
                                                                                </label>
                                                                                <label for="user_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn viewer-role" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220"/>Viewer
                                                                                </label>
                                                                            </div>
                                                                            <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="<?php echo $row['id'] ?>" id="new_checkbox_<?php echo $row['id'] ?>"  name="" class="huddle_edit_cls_chkbx"><label class="cls_sp_label" for="new_checkbox_<?php echo $row['id'] ?>">Huddle Participant</label></span>

                                                                        </li>

                                                                    <?php elseif ($row['is_user'] == 'group'): ?>
                                                                        <!--                                                                        <li style="margin: 0;border: none;padding: 0;">
                                                                                                                                                    <div class="grpClass" style="border-bottom: 1px solid #ededed;float: left;width: 100%;padding-bottom: 10px;padding-top: 6px;margin-bottom: 15px;">
                                                                                                                                                        <label  class="huddle_permission_editor_row"><input class="viewer-user" type="checkbox" value="<?php //echo $row['id'];     ?>" name="group_ids[]" id="group_ids_<?php //echo $row['id']     ?>" style="display: none;"> <a style="color: #757575; font-weight: normal;" id="lblsuper_admin_ids_<?php //echo $row['id']     ?>"><?php //echo $row['name'];     ?></a></label>

                                                                        <?php //if (1 == 1):   ?>
                                                                                                                                                            <div class="permissions coach_mentee">
                                                                                                                                                                <span class="caoch-checkbox" >
                                                                                                                                                                    <input type="radio" id="caoch-checkbox_<?php //echo $row['id']       ?>" value="1"  name="coach_<?php //echo $row['id']       ?>"><label class="cls_sp_label" for="caoch-checkbox_<?php //echo $row['id']       ?>">Coach</label></span>
                                                                                                                                                                <span class="mentee-checkbox"><input type="radio" value="2" id="mentee-checkbox_<?php //echo $row['id']       ?>" name="coach_<?php //echo $row['id']       ?>" class="chk_is_coachee" checked="checked"><label class="cls_sp_label" for="mentee-checkbox_<?php //echo $row['id']       ?>">Coachee</label></span>
                                                                                                                                                            </div>

                                                                        <?php // endif;   ?>
                                                                                                                                                        <div class="permissions coach_perms" style="display: none;">
                                                                                                                                                            <label for="group_role_<?php //echo $row['id']       ?>_200"><input  class="admin-btn admin-role" type="radio" name="group_role_<?php //echo $row['id']       ?>" id="group_role_<?php //echo $row['id']       ?>_200" value="200"/>Admin
                                                                                                                                                            </label>
                                                                                                                                                            <label for="group_role_<?php //echo $row['id']       ?>_210"><input style="margin-right: 5px;" class="member-btn member-role" type="radio" name="group_role_<?php //echo $row['id']       ?>" id="group_role_<?php //echo $row['id']       ?>_210" value="210" checked="checked"/>Member
                                                                                                                                                            </label>
                                                                                                                                                            <label for="group_role_<?php //echo $row['id']       ?>_220"><input class="viewer-btn viewer-role" type="radio" name="group_role_<?php //echo $row['id']       ?>" id="group_role_<?php //echo $row['id']       ?>_220" value="220"/>Viewer
                                                                                                                                                            </label>
                                                                                                                                                        </div>
                                                                                                                                                        <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="1" id="new_checkbox_<?php //echo $row['id']       ?>"  name="" class="huddle_edit_cls_chkbx"><label class="cls_sp_label" for="new_checkbox_<?php //echo $row['id']       ?>">Huddle Participant</label></span>

                                                                                                                                                    </div>
                                                                                                                                                </li>-->
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        <?php else: ?>
                                                            <li>
                                                                To Invite users into the account <a data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="" href="#"><span class="">click here</span></a>
                                                            </li>
                                                        <?php endif; ?>
                                                        <li id="noresults">No people or groups match this search. Please try again.</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="form-actions" style="text-align:left;">
                        <a href="#" class="btn btn-green step3" id="next-step3">Next</a>
                        <a class="btn btn-transparent" href="<?php echo $this->base . '/Huddles' ?>">Cancel</a>
                    </div>
                </div>
                <div id="step-3" style="display: none;">
                    <div>
                        <label style="font-weight: bold;">Huddle Name:</label>
                        <div id="HudTitle" class="mmargin-top" name="click to edit title" style="font-weight: bold; width: 410px">
                            <h4 id="huddle-name-last" class="last-step-labels" style="cursor:pointer;"></h4>
                        </div>
                        <div class="editArea">
                            <input id="ajaxInputName" type="text" name="hname" style="width: 250px;">
                            <input id="name-id" type="hidden"/>
                        </div>
                        <label style="font-weight: bold;">Description:</label>
                        <div id="HudDesc" class="mmargin-top" name="click to edit desc" style="font-weight: bold; width: 410px">
                            <h4 id="huddle-desc-last" class="last-step-labels" style="cursor:pointer;"></h4>
                        </div>
                        <div class="editAread">
                            <!--<input id="ajaxInputDesc" type="text" name="hdescription" style="width: 250px;">-->
                            <textarea rows="3" id="ajaxInputDesc" name="hdescription" style="width: 250px;"></textarea>
                            <input id="desc-id" type="hidden"/>
                        </div>
                        <div id="eval_required_deadline" class="editAread1" style="display: block;">
                            <label class="" style="display: block;">Submission Deadline:</label>
                            <input type="text" name="submission_deadline_date" id="eval-submission-date" class="size-small" placeholder="Date" required="">
                            <input type="text" name="submission_deadline_time" id="eval-submission-time" class="size-small" placeholder="Time" required="">
                        </div>
                    </div>
                    <div id='participants'>
                    </div>
                    <div class="input-group mmargin-top">
                        <div id="editor2-toolbar" class="editor-toolbar" style="display: none;">
                            <a data-wysihtml5-command="bold">bold</a>
                            <a data-wysihtml5-command="italic">italic</a>
                            <a data-wysihtml5-command="insertOrderedList">ol</a>
                            <a data-wysihtml5-command="insertUnorderedList">ul</a>
                            <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
                        </div>
                        <?php
                        echo $this->Form->textarea('message', array('id' => 'editor-2', 'class' => 'editor-textarea larg-input',
                            'style' => 'color: #696565 !important;width:470px;text-indent:5px;padding-right:0px;padding-left:0px;height:150px;',
                            'placeholder' => 'Message to User(s) participating in Huddle...(Optional)'));
                        ?>
                        <?php echo $this->Form->error('videoHuddle.message'); ?>
                    </div>
                    <div class="form-actions" style="text-align:left;">
                        <input type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" id="btnSaveHuddle" value="Create Huddle" name="commit" onclick="beforeHuddleAdd();" class="btn btn-green"  >
                        <a class="btn btn-transparent" href="<?php echo $this->base . '/Huddles' ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div id="addSuperAdminModal" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="header">
                    <h4 class="header-title nomargin-vertical smargin-bottom"><img alt="Admin" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/add-user.png'); ?>" /> New User</h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                </div>
                <form accept-charset="UTF-8" action="<?php echo $this->base . '/Huddles/addUsers' ?>" enctype="multipart/form-data" method="post" name="admin_form" id="inviteFrm" onsubmit="return false;" style="padding-top:0px;">
                    <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                    <div id="flashMessage2" class="message error" style="display:none;"></div>
                    <div class="way-form">
                        <h3>Add name and email</h3>
                        <ol class="autoadd autoadd-sfont fmargin-list-left">
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name" name="users[][name]" required placeholder="Full Name" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email" name="users[][email]" required placeholder="Email Address" type="email" value="" /></label>
                                <a href="#" class="close">x</a>
                            </li>
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name" name="users[][name]" placeholder="Full Name" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email" name="users[][email]" placeholder="Email Address" type="email" value="" /></label>
                                <a href="#" class="close">x</a>
                            </li>
                        </ol>
                        <input id="controller_source" name="controller_source" type="hidden" value="video_huddles" />
                        <input id="action_source" name="action" type="hidden" value="add" />
                        <input id="action_source" name="user_type" type="hidden" value="110" />
                        <button id="btnAddToAccount_addHuddle" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="btn btn-green fmargin-left" type="button">Add to Account</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="clear"></div>

    <script type="text/javascript">
        $(document).ready(function () {
            if ($('#coach-huddle').is(':checked')) {
                $("#coach-huddle").trigger("click");
            }

            if ($('#collab-huddle').is(':checked')) {
                $("#collab-huddle").trigger("click");
            }
            if ($('#eval-huddle').is(':checked')) {
                $("#eval-huddle").trigger("click");
            }

            $('#chkenableframework').click(function () {
                if ($("#chkenableframework").is(':checked') ? 1 : 0 === 1)
                {
                    $('#frmDiv').show();
                }
                else {
                    $('#frmDiv').hide();
                }
            });


            $("#id_search").quicksearch("#list-containers li", {
                noResults: '#noresults',
                stripeRows: ['odd', 'even'],
                loader: 'span.loading',
                minValLength: 2
            });
            $('.close-reveal-modal').click(function (e) {
                $("#inviteFrm").trigger("reset");
            });
            $('#cancel-srch').click(function (e) {
                $("#searchdiv").triggerHandler("focus");
                $("#id_search").val('').trigger('keyup');
            });
            $("#id_search").keyup(function () {
                if ($('#id_search').val().length > 0)
                    $('#cancel-srch').css('display', 'block');
                else
                    $('#cancel-srch').css('display', 'none');
            });

            $("#HudTitle").mouseover(function () {
                $(this).css("backgroundColor", "#FAFABE");
            });
            $("#HudTitle").mouseout(function () {
                $(this).css("backgroundColor", "#ffffff");
            });
            $(document).on('click', "#HudTitle", function () {
                if ($("#coach-huddle").is(':checked')) {
                    $("#ajaxInputName").focus();
                } else {
                    $(this).css("display", "none");
                    $(".editArea").css("display", "block");
                    $("#ajaxInputName").val($(this).text().trim());
                    $("#ajaxInputName").focus();
                }

            });

            $("#HudDesc").mouseover(function () {
                $(this).css("backgroundColor", "#FAFABE")
            });
            $("#HudDesc").mouseout(function () {
                $(this).css("backgroundColor", "#ffffff")
            });
            $("#HudDesc").click(function () {
                //         if($('#collab-huddle').is(':checked')) {
                $(this).css("display", "none");
                $(".editAread").css("display", "block");
                $("#ajaxInputDesc").val($(this).text().trim());
                $("#ajaxInputDesc").focus();
                //         }
            });
            function select_all_evaluator(radio_val) {
                $('.coach_evaluation [type="radio"]').each(function () {
//                    var radio_val = $(this).val();
                    var radio_name = $(this).attr('name');
                    var get_radio_id = radio_name.split("_");
                    var radio_id = get_radio_id[1];
                    var html = '';
                    $(this).parent().parent().find('[type="checkbox"]').remove();

                    if (radio_val == "1") {
                        html = '<input type="checkbox" class="eval_coach_hidden" value="1" id="is_evaluator_' + radio_id + '" name="is_evaluator_' + radio_id + '" style="display:none" checked="checked" />';
                        $(this).parent().parent().append(html);
                    }
                    else {
                        html = '<input type="checkbox" class="eval_participant_hidden" value="1" id="is_eval_participant_' + radio_id + '" name="is_eval_participant_' + radio_id + '" style="display:none" checked="checked" />';
                        $(this).parent().parent().append(html);
                    }
                });
            }


            $("#eval-checkbox").click(function () {
                if ($(this).is(':checked')) {
                    $(".participant-check").removeAttr("checked");
                    $("#all_particapant").removeAttr("checked");
                    $(".evaluator-check").prop('checked', true);
                    select_all_evaluator(1);
                } else {
                    $(".evaluator-check").removeAttr("checked");
                    $(".eval_coach_hidden").removeAttr("checked");

                }

            });
            $("#participant-checkbox").click(function () {
                if ($(this).is(':checked')) {
                    $(".evaluator-check").removeAttr("checked");
                    $("#eval-checkbox").removeAttr("checked");
                    $(".participant-check").prop('checked', true);
                    select_all_evaluator(2);
                } else {
                    $(".participant-check").removeAttr("checked");
                    $(".eval_participant_hidden").removeAttr("checked");
                }

            });
            $("#admin-radio").click(function () {
                if ($(this).is(':checked')) {
                    $(".member-btn").removeAttr("checked");
                    $(".viewer-btn").removeAttr("checked");
                    $(".admin-btn").prop('checked', true);
                }

            });
            $("#member-radio").click(function () {
                if ($(this).is(':checked')) {
                    $(".admin-btn").removeAttr("checked");
                    $(".viewer-btn").removeAttr("checked");
                    $(".member-btn").prop('checked', true);

                }
            });
            $("#viewers-radio").click(function () {
                if ($(this).is(':checked')) {
                    $(".admin-btn").removeAttr("checked");
                    $(".member-btn").removeAttr("checked");
                    $(".viewer-btn").prop('checked', true);
                }
            });
        });

        var posted_data = [];
        var new_ids = 0;
        $(document).ready(function () {
            $('#btnAddToAccount_addHuddle').click(function () {
                addToInviteList();
            });

            $('#pop-up-btn').click(function () {
                $('.fmargin-left').attr('disabled', false);
            });

            $('#video_huddle_name').keypress(function (e) {
                if (e.keyCode == 13) {
                    $(this).closest('form').find('textarea').eq(0).focus();
                    return false;
                }
                $(this).removeClass('error').next().hide();
            });

        });
        function hide_sidebar() {
            $("#coachee_help").delay(500).fadeOut(500);
            $("#collab_help").delay(500).fadeOut(500);
            $(".appendix-content").delay(500).fadeOut(500);

        }
        function beforeHuddleAdd() {
<?php if (IS_QA): ?>
                var metadata = {
                    huddle_created: 'Coaching Huddle Created',
                    user_role: '<?php echo $user_role; ?>'

                };
                Intercom('trackEvent', 'huddle-created', metadata);
<?php endif; ?>

            $.each($(".huddle_edit_cls_chkbx"), function (index, value) {

                if ($(this).prop('checked') == true) {

                } else {

                    var unselected_id = $(this).attr('id');
                    var unselected_id_array = $(this).attr('id').split('_');
                    var unselected_user_id = unselected_id_array[2];

                    $('[name="super_admin_email_' + unselected_user_id + '"]').remove();
                    $('[name="coach_' + unselected_user_id + '"]').remove();
                    $('[name="is_mentor_' + unselected_user_id + '"]').remove();
                    $('[name="iuser_role_' + unselected_user_id + '"]').remove();
                }

            });

            $('#btnSaveHuddle').attr('disabled', true);
            if ($("#ajaxInputName").val().trim().length > 0) {
                if ($("#eval-huddle").is(':checked')) {
                    var evaluation_submission_date = $('#eval-submission-date');
                    var evaluation_submission_time = $('#eval-submission-time');
                }
                var video_huddle_name = $("#ajaxInputName");
            } else {
                if ($("#ajaxInputName").val().trim().length > 0) {
                    $("#ajaxInputName").val($("#name-id").val());
                    var video_huddle_name = $("#name-id");
                } else {
                    var video_huddle_name = $("#ajaxInputName");
                }
            }
            if (video_huddle_name.val().trim().length > 0) {
                if ($("#coach-huddle").is(':checked')) {
                    $("#chk_is_coachee").val('0');
                    $("span.mentee-checkbox input[type=radio]").each(function () {
                        var huddle_row_selector = $(this).parent().parent().parent().find("span.caoch-checkbox input[type='checkbox']");
                        if (huddle_row_selector.length > 0 && huddle_row_selector.is(":checked") && $(this).is(":checked")) {
                            $("#chk_is_coachee").val('1');
                        }
                    });
                    var get_chk = $("#chk_is_coachee").val();

//                    if (get_chk != '1') {
//                        alert("You must select a participant to be coached before creating a coaching/mentoring Huddle.");
//                        $('#btnSaveHuddle').attr('disabled', false);
//                    } else {
                    $('#new_video_huddle').submit();
//                    }
                }
                else {
                    $('#new_video_huddle').submit();
                }
            } else {
                $("#ajaxInputName").addClass('error');
                $('#btnSaveHuddle').attr('disabled', false);
                $('#HudTitle').hide();
                $('.editArea').css('display', 'block');

            }
            return false;
        }

        function is_email_exists(email) {
            for (var i = 0; i < posted_data.length; i++) {
                var posted_user = posted_data[i];
                if (posted_user[1] == email)
                    return true;
            }
            return false;
        }
        function ValidateEmail(mail)
        {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
            {
                return (true);
            }
            return (false);
        }
        function addToInviteList() {
            var lposted_data = [];
            var userNames = document.getElementsByName('users[][name]');
            var userEmails = document.getElementsByName('users[][email]');
            var i = 0;
            for (i = 0; i < userNames.length; i++) {
                var userEmail = $(userEmails[i]);
                if (is_email_exists(userEmail.val())) {
                    alert('A new user with this email is already added.');
                    return false;
                }
            }

            for (i = 0; i < userNames.length; i++) {
                var userName = $(userNames[i]);
                var userEmail = $(userEmails[i]);
                if (userName.val() != '' || userEmail.val() != '') {
                    if (userName.val() == '') {
                        alert('Please enter a valid Full Name.');
                        userName.focus();
                        return false;
                    }
                    if (userEmail.val() == '' || !ValidateEmail(userEmail.val())) {
                        alert('Please enter a valid Email.');
                        userEmail.focus();
                        return false;
                    }
                    if (is_email_exists(userEmail.val())) {
                        alert('A new user with this email is already added.');
                        return false;
                    }
                    var newUser = [];
                    newUser[newUser.length] = '';
                    newUser[newUser.length] = userName.val();
                    newUser[newUser.length] = userEmail.val();
                    lposted_data[lposted_data.length] = newUser;
                }
            }

            if (lposted_data.length == 0) {
                alert('Please enter atleast one Full name or email.');
                return;
            }
            $("#inviteFrm").trigger("reset");
            $.ajax({
                type: 'POST',
                data: {
                    user_data: lposted_data,
                },
                url: '<?php echo $this->base; ?>/Huddles/verifyNewUsers',
                success: function (response) {
                    if (response != '') {
                        $('#flashMessage2').css('display', 'block');
                        $('#flashMessage2').html(response);
                    } else {

                        $('#flashMessage2').css('display', 'none');

                        new_ids -= 1;

                        for (var i = 0; i < lposted_data.length; i++) {

                            var data = lposted_data[i];
                            var html = '';

                            html += '<li>';
                            html += '<label class="huddle_permission_editor_row" for="super_admin_ids_' + new_ids + '"><input type="checkbox" checked="checked" value="' + new_ids + '" name="super_admin_ids[]" id="super_admin_ids_' + new_ids + '" style="display:none;"><a id="lblsuper_admin_ids_' + new_ids + '" style="color: #757575; font-weight: normal;">' + data[1] + '</a></label>';
                            html += '<input type="hidden" value="' + data[1] + '" name="super_admin_fullname_' + new_ids + '" id="super_admin_fullname_' + new_ids + '">';
                            html += '<input type="hidden" value="' + data[2] + '" name="super_admin_email_' + new_ids + '" id="super_admin_email_' + new_ids + '">';
                            if ($('#coach-huddle').is(':checked')) {
                                html += '<div class="permissions coach_mentee" style="float:right !important;">';
                            } else if ($('#eval-huddle').is(':checked')) {
                                html += '<div class="permissions coach_evaluation" style="float: right !important; margin-left: 20px !important">';
                            } else {
                                html += '<div class="permissions coach_mentee" style="display:none;float:right !important;">';
                            }

                            if ($('#coach-huddle').is(':checked')) {
                                html += '<span class="caoch-checkbox" style="margin-right: 5px;">';
                                html += '<input type="radio" id="caoch-checkbox_' + new_ids + '" value="1" name="coach_' + new_ids + '"><label for="caoch-checkbox_' + new_ids + '" class="cls_sp_label">Coach</label></span>';
                            } else if ($('#eval-huddle').is(':checked')) {
                                html += '<span class="eval-checkbox" style="margin-right: 5px;">';
                                html += '<input type="radio" id="caoch-checkbox_' + new_ids + '" value="1" name="coach_' + new_ids + '"><label for="caoch-checkbox_' + new_ids + '" class="cls_sp_label">Assessor</label></span>';
                            }
                            if ($('#coach-huddle').is(':checked')) {
                                html += '<input type="checkbox" value="1" class="coach_coach_hidden" id="is_coach_' + new_ids + '" name="is_coach_' + new_ids + '" style="display:none">';
                            }
                            if ($('#coach-huddle').is(':checked')) {
                                html += '<span class="mentee-checkbox"><input type="radio" value="2" id="mentee-checkbox_' + new_ids + '" name="coach_' + new_ids + '" class="chk_is_coachee" checked="checked"><label for="mentee-checkbox_' + new_ids + '" class="cls_sp_label">Coachee</label></span>';
                                $("#chk_is_coachee").val("1");
                            } else if ($('#eval-huddle').is(':checked')) {
                                html += '<span class="mentee-checkbox"><input type="radio" value="2" id="mentee-checkbox_' + new_ids + '" name="coach_' + new_ids + '" class="chk_is_coachee" checked="checked"><label for="mentee-checkbox_' + new_ids + '" class="cls_sp_label">Evaluated Particpant</label></span>';
                                $("#chk_is_coachee").val("1");
                            } else {
                                html += '<span class="mentee-checkbox"><input type="radio" value="2" id="mentee-checkbox_' + new_ids + '" name="coach_' + new_ids + '" class="chk_is_coachee" ><label for="mentee-checkbox_' + new_ids + '" class="cls_sp_label">Coachee</label></span>';
                            }

                            if ($('#coach-huddle').is(':checked')) {
                                html += '<input type="checkbox" value="1" class="coach_mentee_hidden" id="is_mentor_' + new_ids + '" name="is_mentor_' + new_ids + '" style="display:none" checked="checked">';
                            } else if ($('#eval-huddle').is(':checked')) {
                                html += '<input type="checkbox" value="1" class="coach_mentee_hidden" id="is_mentor_' + new_ids + '" name="is_mentor_' + new_ids + '" style="display:none" checked="checked">';
                            }

                            html += '</div>';

                            if ($('#collab-huddle').is(':checked')) {
                                html += '<div class="permissions coach_perms">';
                            } else if ($('#eval-huddle').is(':checked')) {
                                html += '<div class="permissions coach_perms" style="display:none;">';
                            } else {
                                html += '<div class="permissions coach_perms" style="display:none;">';
                            }
                            html += '<label for="user_role_' + new_ids + '_200"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_200" value="200">Admin</label>';
                            html += '<label for="user_role_' + new_ids + '_210"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_210" value="210" checked="checked">Member</label>';
                            html += '<label for="user_role_' + new_ids + '_220"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_220" value="220">Viewer</label>';
                            html += '</div>';
                            html += '<span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="' + new_ids + '" id="new_checkbox_' + new_ids + '"  name="" class="huddle_edit_cls_chkbx" checked="checked"><label class="cls_sp_label" for="new_checkbox_' + new_ids + '">Huddle Participant</label></span>';
                            html += '</li>';
                            $('.groups-table .huddle-span4 .groups-table-content ul').prepend(html);
                            new_ids -= 1;
                            var newUser = [];
                            newUser[newUser.length] = data[0];
                            newUser[newUser.length] = data[1];
                            posted_data[posted_data.length] = newUser;
                        }

                        var $overview = $('.widget-scrollable.horizontal .overview');
                        $.each($overview, function () {

                            var width = $.map($(this).children(), function (child) {
                                return $(child).outerWidth() +
                                        $(child).pixels('margin-left') + $(child).pixels('margin-right');
                            });
                            $(this).width($.sum(width));

                        });

                        $('.widget-scrollable').tinyscrollbar();
                        $('#addSuperAdminModal').modal('hide');


                    }
                },
                errors: function (response) {
                    alert(response.contents);
                }

            });

        }


    </script>
