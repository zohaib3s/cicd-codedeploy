<?php $breadcrumb_language_based_content = $this->Custom->get_page_lang_based_content('BreadCrumb'); ?>
<?php
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];
?>
<div id="showflashmessage1"></div>
<link rel="stylesheet" href="/app/js/dist/themes/default/style.min.css" />
<script src="/app/js/dist/jstree.min.js"></script>

<div id="createfolder" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog"  style="width: 400px;">
        <div class="modal-content">
            <div class="header" style="padding-left: 25px;padding-bottom: 0px;margin-bottom: 0px;padding-top: 15px;">
                <h4 class="header-title nomargin-vertical smargin-bottom">New Huddle Folder</h4>
                <a id="cross1" class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">x</a>
            </div>
            <form method="post" id="new_video_huddle2" enctype="multipart/form-data" class="new_video_huddle"
                  action="<?php echo $this->base . '/Folder/create'; ?>" accept-charset="UTF-8">
                <div id="tabs">

                    <div>
                        <div id="step-1" style="margin-left: -20px;">
                            <div style="margin:0;padding:0;display:inline"><input type="hidden" value="✓" name="utf8">
                                <input name="data[folder_id]" id="hidden_folder_id" type="hidden" value="<?php echo $folder_id; ?>"/></div>

                            <a class="close-reveal-modal"></a>
                            <div class="span5">
                                <div class="row"  style="margin-bottom: 20px;">
                                    <span class="wiz-step1-radio"><input type="radio" id="collab-huddle" name="type" class="cls_coaching_trig_2" checked="checked" value="1" style="display: none;" checked="checked"></span>
                                    <div style="clear: both;height:10px;"></div>
                                </div>
                                <div class="row" style="margin-bottom: 20px;margin-left: 0px;">
                                    <input name="data[hname]" id="video_folder_name" class="size-big huddle-name "  placeholder="Folder Name" size="30" type="text" style="width: 350px;" required/>
                                    <label id="error" for="video_folder_name" class="error" style="display:none;background-color: none !important;">This field is required.</label>
                                </div>
                                <div style="clear: both;"></div>
                                <div class="row" style="display:none;">
                                    <?php echo $this->Form->textarea('hdescription', array('rows' => '4', 'placeholder' => 'Folder Description (Optional)', 'id' => 'video_huddle_description', 'cols' => '40', 'class' => 'size-big')); ?>
                                </div>
                                <?php echo $this->Form->input('created_by', array('value' => $user_id, 'type' => 'hidden', 'id' => 'video_huddle_created_by')); ?>
                            </div>
                            <div style="clear: both;height:10px;"></div>
                            <!--  <div class="row-fluid">
                                <div class="groups-table span12">

                                    <div class="span4 huddle-span4" style="margin-left:0px;">
                                        <div class="groups-table-header groups-table-header-create">
                                            <div style="width: 200px; float: left; margin-left: 8px;">
                            <?php if ($user_permissions['UserAccount']['permission_maintain_folders'] == 1 && $user_permissions['UserAccount']['permission_administrator_user_new_role'] == 1): ?>
                                                                                                                                                    <a style="margin-left: -8px;" id="pop-up-btn" data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="btn btn-small btn-green" href="#"><span class="plus">+</span>
                                                                                                                                                    </a>
                            <?php else: ?>
                                                                                                                                                    <a style="margin-left: -8px;"><span class="">&nbsp;&nbsp;</span>
                                                                                                                                                    </a>
                            <?php endif; ?>
                                            </div>
                                            <div style="width: 248px; padding: 0px;float: left;margin-left: 426px;" class="search-box">
                                                <div  id="header-container" class="filterform">
                                                </div>
                                            </div>
                                            <a class="appendix right" href="#" id="Coaches_info" >?</a>
                                            <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                                <h3>Info</h3>
                                                <p>Admin: Can add/remove users; upload/download/copy/delete all videos and documents; clip all videos; add/edit/delete all video annotations and comments; create/participate/edit all Folder discussions; delete Huddle.</p>
                                                <p>Members: Can upload/download/copy/delete videos and documents they add to the Huddle; clip their videos; add/edit/delete their video annotations and comments; create/participate in Huddle discussions.</p>
                                                <p>Viewers: View videos and documents only.</p>
                                            </div>
                                            <a class="appendix right" href="#" id="Coachee_info" style="display:none;">?</a>
                                            <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                                                <h3>Info</h3>
                                                <p>Coach(es):  Can edit coaching Huddle, by adding or removing other coaches in the Huddle; upload/download/copy all videos and supporting resources; clip/edit videos; add/edit/delete all video annotations; enable/disable tags/account framework; create/participate/edit all discussions; delete the coaching Huddle.</p>
                                                <p>Coachee: Can upload/download/copy/delete videos and resources they add to the Huddle; clip/edit their own videos; add/edit/delete their video annotations and comments; create/participate in Huddle discussions.</p>
                                            </div>
                                            <div style="clear:both"></div>
                            <?php if (count($super_users) > 1 || (isset($folderUsers) && count($folderUsers) > 1) || (isset($users_groups) && count($users_groups) > 0)): ?>
                                                                                                                                                <div id="select-all-none-cnt" class="select-all-none selectnone"  style="float: left;margin-left: 454px;margin-top: 1border.pngpx;min-width: 123px;max-width: 200px;">
                                                                                                                                                    <label id="select-all-none" for="select-all"><input type="checkbox" name="select-all" id="select-all" style="display:inline-block;" /> <span id="select-all-label">Select None</span></label>
                                                                                                                                                </div>
                            <?php endif; ?>
                            <?php if (1 == 1): ?>
                                                                                                                                                <div id="select_all_coaches_panel" class="huddles-select-all-block coach_mentee">
                                                                                                                                                    <span class="caoch-checkbox" >
                                                                                                                                                        <input type="checkbox" id="caoch-checkbox" name="is_coach"> Coach</span>
                                                                                                                                                    <span class="mentee-checkbox"><input type="checkbox" id="mentee-checkbox" name="is_mentor" >Coachee</span>
                                                                                                                                                </div>
                            <?php endif; ?>

                                            <div class="huddles-select-all-block coach_perms">
                                                <span class="admin-radio"><input type="radio" id="admin-radio" name="select-all"> Admin</span>
                                                <span class="member-radio"><input type="radio" id="member-radio" name="select-all" style="margin-left: -8px;"> Member</span>
                                                <span class="viewer-radio"><input type="radio" id="viewers-radio" name="select-all"> Viewer</span>
                                            </div>
                                            <div style="clear:both"></div>
                                        </div>
                                        <div class="groups-table-content">
                                            <div class="widget-scrollable">
                                                <div class="scrollbar" style="height: 155px;">
                                                    <div class="track" style="height: 155px;">
                                                        <div class="thumb" style="top: 0px; height: 90.3195px;">
                                                            <div class="end"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="viewport short">
                                                    <div class="overview" style="top: 0px;">
                                                        <div  id="people-lists">
                                                            <ul id="list-containers">
                                                                <input type="hidden" id="chk_is_coachee" value="0" name="chk_is_coachee"  />
                            <?php
                            if (count($users_record) > 0):
                                ?>
                                <?php if ($users_record): ?>
                                    <?php foreach ($users_record as $row): ?>
                                        <?php
                                        if ($row['id'] == $user_id)
                                            continue;
                                        ?>
                                        <?php if ($row['is_user'] == 'admin'): ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <li style="display:none;">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <label  class="huddle_permission_editor_row" for="super_admin_ids_<?php echo $row['id'] ?>"><input class="super-user" type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>" style="display: none;"> <a style="color: #757575; font-weight: normal;" id="lblsuper_admin_ids_<?php echo $row['id'] ?>"><?php echo trim($row['first_name'] . " " . $row['last_name']); ?> </a></label>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <input type="hidden" value="<?php echo $row['email'] ?>" name="super_admin_email_<?php echo $row['id'] ?>" id="super_admin_email_<?php echo $row['id'] ?>">

                                            <?php if (1 == 1): ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <div class="permissions coach_mentee">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <span class="caoch-checkbox" ><input type="radio" value="1" id="caoch-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>"><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>" >Coach</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <span class="mentee-checkbox"><input value="2" type="radio" id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" class="chk_is_coachee" checked="checked"> <span id="mentee-chk"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </div>
                                            <?php endif; ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="permissions coach_perms">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="user_role_<?php echo $row['id'] ?>_200"><input  class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200"/>Admin
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="user_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn"  type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" checked="checked"/>Member
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="user_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220"/>Viewer
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="<?php echo $row['id'] ?>" id="new_checkbox_<?php echo $row['id'] ?>"  name="" class="huddle_edit_cls_chkbx"><label class="cls_sp_label" for="new_checkbox_<?php echo $row['id'] ?>">Folder Participant</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                </li>

                                        <?php elseif ($row['is_user'] == 'member'): ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <li>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <label  class="huddle_permission_editor_row" for="super_admin_ids_<?php echo $row['id'] ?>"><input class="member-user"  type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>" style="display: none;"> <a style="color: #757575; font-weight: normal;" id="lblsuper_admin_ids_<?php echo $row['id'] ?>"> <?php echo $row['first_name'] . " " . $row['last_name'] ?></a></label>

                                            <?php if (1 == 1): ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <div class="permissions coach_mentee">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <span class="caoch-checkbox" >
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <input type="radio" id="caoch-checkbox_<?php echo $row['id'] ?>" value="1"  name="coach_<?php echo $row['id'] ?>"><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>">Coach</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <span class="mentee-checkbox"><input type="radio" value="2"   id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" class="chk_is_coachee" checked="checked"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </div>
                                            <?php endif; ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="permissions coach_perms">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="user_role_<?php echo $row['id'] ?>_200"><input class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200"/>Admin
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="user_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" checked="checked"/>Member
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="user_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220"/>Viewer
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="<?php echo $row['id'] ?>" id="new_checkbox_<?php echo $row['id'] ?>"  name="" class="huddle_edit_cls_chkbx"><label class="cls_sp_label" for="new_checkbox_<?php echo $row['id'] ?>">Folder Participant</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                </li>

                                        <?php elseif ($row['is_user'] == 'group'): ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <li>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <label  class="huddle_permission_editor_row"><input class="viewer-user" type="checkbox" value="<?php echo $row['id']; ?>" name="group_ids[]" id="group_ids_<?php echo $row['id'] ?>" style="display: none;"> <a style="color: #757575; font-weight: normal;" id="lblsuper_admin_ids_<?php echo $row['id'] ?>"><?php echo $row['name']; ?></a></label>

                                            <?php if (1 == 1): ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <div class="permissions coach_mentee">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <span class="caoch-checkbox" >
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <input type="radio" id="caoch-checkbox_<?php echo $row['id'] ?>" value="1"  name="coach_<?php echo $row['id'] ?>"><label class="cls_sp_label" for="caoch-checkbox_<?php echo $row['id'] ?>">Coach</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <span class="mentee-checkbox"><input type="radio" value="2" id="mentee-checkbox_<?php echo $row['id'] ?>" name="coach_<?php echo $row['id'] ?>" class="chk_is_coachee" checked="checked"><label class="cls_sp_label" for="mentee-checkbox_<?php echo $row['id'] ?>">Coachee</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </div>
                                            <?php endif; ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <div class="permissions coach_perms">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="group_role_<?php echo $row['id'] ?>_200"><input  class="admin-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_200" value="200"/>Admin
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="group_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_210" value="210" checked="checked"/>Member
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        <label for="group_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_220" value="220"/>Viewer
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </label>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <span class="caoch-checkbox huddle_edit_cls" style="margin-right: 5px;"><input type="checkbox" value="1" id="new_checkbox_<?php echo $row['id'] ?>"  name="" class="huddle_edit_cls_chkbx"><label class="cls_sp_label" for="new_checkbox_<?php echo $row['id'] ?>">Folder Participant</label></span>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                </li>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                                                                                                                                                    <li>
                                                                                                                                                                        To Invite users into the account <a data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="" href="#"><span class="">click here</span></a>
                                                                                                                                                                    </li>
                            <?php endif; ?>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>-->
                            <div class="form-actions" style="text-align: left;margin-left: 22px;margin-top:0px;">
                                <input id="creatingfolder" type="button" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>; border-color:<?php echo $this->Custom->get_site_settings('primary_border_color'); ?>" value="Create Folder" name="commit" onclick="CreateFolder1();" class="btn btn-green"  >
                                <a id="cancel" class="btn btn-white" style="font-size: 14px;" onclick="$('#createfolder').modal('hide');">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        var hudd_id = '';
        $(document).on("click", ".move_huddle", function () {
            var huddle_id = $(this).attr("huddle_id");
            $('#tree-container').jstree({
                'plugins': ['wholerow'],
                'core': {
                    'data': {
                        "url": "<?php echo $this->base . '/folder/treeview_detail'; ?>",
                        "dataType": "json" // needed only if you do not supply JSON headers
                    }
                }
            })
            $('#tree-container').jstree(true).settings.core.data.url = home_url + '/folder/treeview_detail/' + huddle_id;
            $('#tree-container').jstree('refresh');
            $("#txthuddleid").val($(this).attr("huddle_id"));
            $("#txtfoldid").val($(this).attr("fold_id"));
            hudd_id = $(this).attr("id");
        });
        $(document).on("click", ".moveFolder", function () {
            var user_id = "<?php echo $user_id; ?>";
            var view_mode = "<?php echo $view_mode; ?>";
            var account_id = "<?php echo $account_id; ?>";
            var sort = "<?php echo $sort; ?>";

            $('#moveFolderBtn').prop('disabled', true);
            var present_folder_id = $("#txtfolderid").val();
            var move_to_folder_id = $("#txtdestfolderid").val();
            //var move_to_folder_id = $(this).attr("id").replace('_anchor','');
            var huddle = $("#txthuddleid").val();
            var fold_id = $("#txtfoldid").val();
            if (move_to_folder_id == present_folder_id) {
                $('#moveFolderBtn').prop('disabled', false);
                $('#movefolderto').modal('hide');
                return true;
            }

            $.ajax({
                url: home_url + '/Folder/movehuddletofolder',
                data: {huddle: huddle, present_id: present_folder_id, move_to_folder: move_to_folder_id},
                dataType: 'JSON',
                type: 'POST',
                success: function (response) {
                    $('#moveFolderBtn').prop('disabled', false);
                    $('#movefolderto').modal('hide');
                    $(document).ajaxStop($.unblockUI);
                    if (response == true) {
                        $("#flashMessage").hide();
                        $("#showflashmessage1").fadeIn();
                        //  $('#huddle-listings').html(response.contents);
                        // $('.page-title__counter').html($('#search-huddle-count').html());
                        var show_msg = '';
                        if (fold_id > 0)
                            show_msg = '<div id="flashMessage" class="message success" style="cursor: pointer;">Folder moved Successfully.</div>';
                        else
                            show_msg = '<div id="flashMessage" class="message success" style="cursor: pointer;">Huddle moved Successfully.</div>';
                        $("#showflashmessage1").prepend(show_msg);
                        $("#txtSearchFolderHuddles").val("");
                        $("#clearSearchHuddlesfolder").hide();
                        $('#huddle_row_' + huddle).hide();
                        if (view_mode == 'grid')
                        {
                            $.ajax({
                                url: home_url + '/Folder/refresh_folders_stats/' + move_to_folder_id,
                                data: {huddle: huddle, present_id: present_folder_id, move_to_folder: move_to_folder_id, user_id: user_id, sort: sort, account_id: account_id},
                                dataType: 'JSON',
                                type: 'POST',
                                success: function (response) {
                                    $('#huddle_row_' + move_to_folder_id).html(response);
                                }

                            });
                        }
                    } else {
                        //   $('.page-title__counter').html($('#search-huddle-count').html());
                        //   $('#huddle-listings').html(response.contents);
                    }
                },
                error: function () {
                    $('#moveFolderBtn').prop('disabled', false);
                }
            });

        });

    });
</script>

<!--<h1 class="page-title nomargin-top"> <span class="page-title__counter"></span></h1>
<hr class="style2">-->
<style>
    .cross-search{
        margin-left: 6px;
        position: absolute;
        right: 54px;
        width: 327px;
        cursor: pointer;
    }
    .tree_scroll_box ul li:first-child a i{
        background: url(/app/img/crhomeicon.png) no-repeat !important;
        top: 2px;
    }
    .tree_scroll_box ul li ul li:first-child a i{

        background-image: url(/app/js/dist/themes/default/32px.png) !important;
        background-position: -218px -67px !important;
        top:0px !important;
    }

</style>

<?php // echo "<pre>";print_r($folder_tree); echo "</pre>";  ?>
<div id="movefolderto" class="modal in" role="dialog" aria-hidden="false">

    <div class="modal-dialog">
        <div class="modal-content">
            <div class="header">
                <h4 class="header-title nomargin-vertical smargin-bottom">Choose a destination Folder</h4>
                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">x</a>
            </div>
            <div class="foldrmn">
                <input type="hidden" id="txthuddleid" name="txthuddleid" value="" />
                <input type="hidden" id="txtfoldid" name="txtfoldid" value="" />
                <input type="hidden" id="txtdestfolderid" name="txtfolderid" value="" />
                <div class="tree_scroll_box">
                    <div id="tree-container"></div>
                </div>
                <div class="clear"></div>
                <div class="btns" style="float: right;position: relative;margin-bottom: 10px;">
                    <input class="moveFolder btn btn-green" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" id="moveFolderBtn" type="button" value="Move" />
                    <input class="btn btn-white" type="reset" value="Cancel" onclick="$('#movefolderto').modal('hide');"/>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


    function CreateFolder1() {
        folder_id = $("#hidden_folder_id").val();
        folder_name = $("#video_folder_name").val();
        if (folder_name === '')
        {
            $("#error").css("display", "inline-block");

//             $('#createfolder').modal('hide');
//             $("#flashMessage").hide();
//             $("#showflashmessage1").fadeIn();
//             var show_msg = '<div id="flashMessage" class="message error" style="cursor: pointer;">Please enter folder name.</div>';
//             $("#showflashmessage1").prepend(show_msg);
//             $("#txtSearchHuddles:input").val('');
//            dofolderHuddleSearch();

        }
        else {
            $.ajax({url: home_url + '/Folder/create',
                data: {hname: $("#video_folder_name").val(), hdescription: '', folder_id: folder_id},
                type: 'post',
                success: function (output) {
                    $('#createfolder').modal('hide');
                    $("#flashMessage").hide();
                    $("#showflashmessage1").fadeIn();
                    if (output == 'folderexists')
                    {
                        var show_msg = '<div id="flashMessage" class="message error" style="cursor: pointer;">A folder with same name already exist, please try another name. You might not see the folder with name "' + folder_name + '" because you are not participating in that.</div>';
                    }
                    else {
                        var show_msg = '<div id="flashMessage" class="message success" style="cursor: pointer;">' + folder_name + ' has been saved successfully.</div>';
                    }
                    $("#showflashmessage1").prepend(show_msg);
                    $("#txtSearchHuddles:input").val('');
                    dofolderHuddleSearch();

                }
            });

        }

    }
    $(document).on("click", "#showflashmessage1", function () {
        $("#showflashmessage1").fadeOut(200);
    });
    $(document).on("click", "#cancel", function () {
        $("#video_folder_name").val('');
        $("#error").hide();

    });
    $(document).on("click", "#cross1", function () {
        $("#video_folder_name").val('');
        $("#error").hide();

    });
    $(document).on("click", "#creatingfolder", function () {
        $("#video_folder_name").val('');
    });

    $(document).on("click", "#flashMessage", function () {
        $("#flashMessage").fadeOut(200);
    });


    $('#tree-container')
            .on("changed.jstree", function (e, data) {
                if (data.selected.length) {
                    $("#txtdestfolderid").val(data.instance.get_node(data.selected[0]).id);
                }
            });
    $('#new_video_huddle2').submit(function (evt) {
        evt.preventDefault();

    });

    $("#video_folder_name").keyup(function (e) {
        var str_length = $("#video_folder_name").val().trim();
        if (str_length.length > 0 && e.which == 13) {
            $("#creatingfolder").trigger('click');
        }
    });

</script>


<?php
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$loggedInUserRole = '';
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];
$btn_1_color = $this->Custom->get_site_settings('primary_bg_color');
$btn_2_color = $this->Custom->get_site_settings('secondary_bg_color');

$users_shown = array();
$groups_shown = array();
$is_group = '';
echo $head;
?>
<script type="text/javascript">

    $(document).ready(function () {



        dragndrop();

    });/*--------Ready End----------*/
    function dragndrop() {
        $(".card").draggable({
            appendTo: "body",
            cursor: "move",
            helper: 'clone',
            revert: "invalid",
            handle: ".handle"
        });


        $("#launchPad2").droppable({
            tolerance: "intersect",
            accept: ".card",
            activeClass: "ui-state-default",
            hoverClass: "ui-state-hover",
            drop: function (event, ui) {
                console.log('launchpad2');
                var $huddle_id = ui.draggable.attr("huddle_id");
                var $folder_id = $(this).attr("folder_id");
                $(this).append($(ui.draggable));
                savedragtofolder($huddle_id, $folder_id);
            }
        });

        $(".stackDrop1").droppable({
            tolerance: "intersect",
            accept: ".card",
            activeClass: "ui-state-default",
            hoverClass: "ui-state-hover",
            drop: function (event, ui) {
                console.log('2');
                var $huddle_id = ui.draggable.attr("huddle_id");
                var $folder_id = $(this).attr("folder_id");
                console.log('drop1');
                console.log($huddle_id);
                console.log($folder_id);
                $(this).append($(ui.draggable));
                savedragtofolder($huddle_id, $folder_id);
            }
        });

        $(".stackDrop2").droppable({
            tolerance: "intersect",
            accept: ".card",
            activeClass: "ui-state-default",
            hoverClass: "ui-state-hover",
            drop: function (event, ui) {
                console.log('drop2');
                var $huddle_id = ui.draggable.attr("huddle_id");
                var $folder_id = $(this).attr("folder_id");
                $(this).append($(ui.draggable));
                savedragtofolder($huddle_id, $folder_id);
            }
        });
    }

    function savedragtofolder($huddle_id, $folder_id) {
        var user_id = "<?php echo $user_id; ?>";
        var view_mode = "<?php echo $view_mode; ?>";
        var account_id = "<?php echo $account_id; ?>";
        var sort = "<?php echo $sort; ?>";
        $.ajax({
            url: home_url + '/Folder/draghuddletofolder',
            data: {huddle_id: $huddle_id, folder_id: $folder_id},
            dataType: 'JSON',
            type: 'POST',
            success: function (response) {
                if (view_mode == 'grid')
                {
                    $.ajax({
                        url: home_url + '/Folder/refresh_folders_stats/' + $folder_id,
                        data: {huddle: $huddle_id, folder_id: $folder_id, user_id: user_id, sort: sort, account_id: account_id},
                        dataType: 'JSON',
                        type: 'POST',
                        success: function (response) {
                            $('#huddle_row_' + $folder_id).html(response);
                        }

                    });
                }

            },
            error: function () {

            }
        });

    }
</script>
<style>
    #launchPad {
        min-height:500px;
    }
    #launchPad2 {
        min-height:500px;
    }
    .dropZone {

    }
    .card {
        cursor: move;
        background: #fff;

    }
    .stack {
        width: 100%;
        height: 353px;
    }
    .stackHdr {

    }
    .topsection {
        min-width:285px;
    }
    /*.stackDrop2 {
        width: 100%;
        height: 55px;
    }*/
    .clear{
        clear:both;
    }
    .huddle_container{
        margin:20px;
        padding:0px;
        list-style:none;
    }
    .huddle_container li{
        float:left;
        list-style:none;
        width:100%;
    }
    .huddle{
        background-color: #FFF;
        border: 3px solid #dddad2;
        border-radius: 3px 3px 3px 3px;
        box-sizing:border-box;
        height: 353px;
        width: 328px;
        margin:15px;
        float:left;
    }

    .huddle_Mfolder .card{
        display:none;
    }
    /*.huddle_Mfolder{
        background:url(/app/img/folder_bg.jpg) repeat-x;
        border: 3px solid transparent;
        border-radius: 3px 3px 3px 3px;
        box-sizing:border-box;
        height: 353px;
        width: 328px;
        margin:11px 5px;
        float:left;
        position:relative;
    }*/
    .box.style2.compact{
        margin:11px 5px;
    }
    .box .box-wrapper {
        min-height: 275px;
    }
    .box {
        width: 100%;
        min-height: 353px;
    }
    .col-4sm {
        width: 306px;
    }
    /*.huddle_Mfolder::before{
        content: "";
        width:127px;
        height:362px;
        color:#e0a203;
        background:url(/app/img/folder_left.jpg) no-repeat left;
        position: relative;
        display:table;
        left: -3px;
        top: -15px;
        float:left;
    }
    .huddle_Mfolder::after{
        content: "";
        width:9px;
        height:362px;
        color:#e0a203;
        background: url(/app/img/folder_right.jpg) no-repeat left;
        position: relative;
        display:table;
        right: -3px;
        top: -8px;
        float:right;
        padding-top: 12px;
    }*/
    .folder_inner{
        position:absolute;
        margin:0px 20px;
        width:87%;
    }
    .huddle_Mfolder h4{
        top: 10px;
        position: relative;
    }
    .huddle_Mfolder h4 a{
        color:#fff;
    }
    .huddle_Mfolder h5{
        color:#574502;
    }
    .huddle_Mfolder p i{
        color:#725b03;
    }
    .folder_comment{
        margin-top:20px;
    }
    .huddle_Mfolder hr{
        border-top: 1px dashed #e3b80c;
    }
    .huddle_Mfolder .action-buttons{
        background:#fff;
    }
    .folder_icon{
        width:63px;
        height:46px;
        background: url(/app/img/folder_icon.png) no-repeat left;
        float:left;
        margin-right: 10px;
        z-index: 10;
        position: relative;
    }
    /*.list_folder .huddle_list_left{
        width: 645px;
    }*/
    .list_folder .box{
        display:none;

    }
    .col-4sm {
        /* width:310px;*/
    }
    .lmargin-bottom{
        margin: -10px -15px 0px -15px;
        margin-bottom:22px;
    }
    .col-4sm {

        min-height: 275px;
    }
    .col-4sm:hover .handle a{
        color:#f6701b !important;
    }
    .col-4sm:hover a{
        color:#333;
    }
</style>

<div class="clear"></div>

<div class="box container">
    <div class="header header-huddle">
        <h2 class="title"><?php echo $page_title ? $page_title : ''; ?> <span class="blue_count"><?php echo $total_huddles ?></span></h2></div>

    <div style="    height: 38px;">
        <?php
        $params = $this->request->pass;

        if (isset($this->request->data["txtSearchHuddles"]))
            $huddleSearch = $this->request->data["txtSearchHuddles"];
        else if (isset($params[2]))
            $huddleSearch = $params[2];
        else
            $huddleSearch = '';
        ?>
        <?php if ($user_permissions['UserAccount']['manage_collab_huddles'] == '1' || $user_permissions['UserAccount']['manage_coach_huddles'] == '1' || $user_permissions['UserAccount']['manage_evaluation_huddles'] == '1'): ?>
            <a id="btn-new-huddle" href="<?php echo $this->base . '/add_huddle_angular/home/' . $folder_id ?>" class="btn btn-green right"><?php echo $new_huddle_button ? $new_huddle_button : ''; ?> </a>
        <?php endif; ?>
        <?php if (($user_current_account['roles']['role_id'] == '120' && $user_permissions['UserAccount']['folders_check'] == '1' ) || $user_current_account['roles']['role_id'] == '110' || $user_current_account['roles']['role_id'] == '100' || ($user_current_account['roles']['role_id'] == '115' && $user_permissions['UserAccount']['folders_check'] == '1')): ?>
            <a id="btn-new-huddle" data-toggle="modal" data-target="#createfolder" style="margin-right:5px; background-color: <?php echo $btn_1_color ?>; border-color:<?php echo $btn_1_color ?>;" href="<?php //echo $this->base . '/Folder/create/' . $folder_id                        ?>" class="btn btn-orange right">New Folder </a>
        <?php endif; ?>
        <div id="sort-mode" class="dropdown right mmargin-right" style="width: 200px !important;    margin-top: -2px;">
            <ul>
                <li><a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/' . $view_mode . '/' . $sort . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" title="View all" class="active"><?php
                        if ($sort == 'name') {
                            echo 'Title';
                        } elseif ($sort == 'date') {
                            echo 'Date Created';
                        } elseif ($sort == 'coaching') {
                            echo 'Coaching Huddles';
                        } elseif ($sort == 'collaboration') {
                            echo 'Collaboration Huddles';
                        } elseif ($sort == 'evaluation') {
                            echo 'Assessment Huddles';
                        } elseif ($sort == 'folders' && !empty($folders)) {
                            echo 'Folders';
                        } else {
                            echo 'Sort';
                        }
                        ?></a></li>
                <li <?php
                if ($sort == 'name') {
                    echo 'style="display:none;"';
                }
                ?> ><a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/' . $view_mode . '/name' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Title</a></li>
                <li <?php
                if ($sort == 'date') {
                    echo 'style="display:none;"';
                }
                ?> ><a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/' . $view_mode . '/date' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Date Created</a></li>
                <li <?php
                if ($sort == 'coaching' || empty($coach_huddles_dropdown)) {
                    echo 'style="display:none;"';
                }
                ?> ><a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/' . $view_mode . '/coaching' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Coaching Huddles</a></li>
                <li <?php
                if ($sort == 'collaboration' || empty($collab_huddles_dropdown)) {
                    echo 'style="display:none;"';
                }
                ?> ><a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/' . $view_mode . '/collaboration' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Collaboration Huddles</a></li>
                    <?php
                    if ($this->Custom->check_if_eval_huddle_active($account_id)) {
                        ?>
                    <li <?php
                    if ($sort == 'evaluation' || empty($eval_huddles_dropdown)) {
                        echo 'style="display:none;"';
                    }
                    ?> ><a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/' . $view_mode . '/evaluation' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Assessment Huddles</a></li>
                    <?php } ?>
                <li <?php
                if ($sort == 'folders' || empty($folders_check_dropdown)) {
                    echo 'style="display:none;"';
                }
                ?> ><a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/' . $view_mode . '/folders' . ((isset($huddleSearch) ? ('/' . $huddleSearch) : '')) ?>" class="">Folders</a></li>
            </ul>
        </div>

        <input type="hidden" id="txtfolderid" name="txtfolderid" value="<?php echo $folder_id; ?>" />
        <div id="view" class="mmargin-right right">
            <a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/list' ?>" class="icon-list <?php
            if ($view_mode == 'list') {
                echo 'active';
            }
            ?>" rel="tooltip" title="List View"></a>
            <a href="<?php echo $this->base . '/Folder/index/' . $folder_id . '/grid' ?>" class="icon-grid <?php
            if ($view_mode != 'list') {
                echo 'active';
            }
            ?>" rel="tooltip" title="Grid View"></a>
        </div>

        <div class="search-box12" style="width: 335px;float: right;position: relative;margin-top: 4px;">
            <input type="hidden" id="search-mode" action="" class="btn-search" value="<?php echo $view_mode; ?>"/>
            <input type="button" id="btnHuddleSearchfolder" action="" class="btn-search" value="" style="width:27px;"/>
            <input class="text-input" name="txtSearchFolderHuddles" id="txtSearchFolderHuddles" type="text" value="" placeholder="Search ..." />
            <span id="clearSearchHuddlesfolder"  style="display: none; margin-right: -15px;" class="clear-video-input-box cross-search search-x-huddles">X</span>
        </div>
    </div>

    <hr class="style2">
    <?php //echo $breadcrumb;  ?>

    <div class="lmargin-bottom" id="huddle-listings">

        <?php if (isset($video_huddles) && (!empty($video_huddles) || !empty($folders))): ?>
            <?php if (isset($view_mode) && $view_mode == 'grid'): ?>
                <style>
                    .row{
                        margin-left:0px;
                    }
                    .bridcrm{
                        margin:4px 0px 2px 0px;
                    }
                </style>
                <div class="row huddles-list huddles-list--grid">
                    <?php if (isset($folders) && !empty($folders)): ?>
                        <?php
                        foreach ($folders as $row):
                            $invitedUser = '';
                            $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                            $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                            $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $huddleGroups, $user_current_account['User']['id']);
                            ?>
                            <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                <div class="huddle_Mfolder compact" style="margin: 0px;">
                                    <div class="stackDrop1 col-4sm" id="huddle_row_<?php echo $row['AccountFolder']['account_folder_id']; ?>"  folder_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>">
                                        <div class="topsection">
                                            <span class="ficoncls"><?php echo $this->Html->image('icons_folder.png'); ?></span>
                                            <span class="namecls"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Folder/' . $row['AccountFolder']['account_folder_id'] ?>">
                                                    <?php echo $row['AccountFolder']['name']; ?>
                                                </a></span>
                                            <span class="optioncls">
                                                <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1'))): ?>
                                                    <a id="editfolder1" huddle_id="<?php echo $row['AccountFolder']['account_folder_id'] ?> " rel="tooltip" data-original-title="Edit"  href="#<?php //echo $this->base . '/Folder/edit/' . $row['AccountFolder']['account_folder_id']                             ?>"> <?php echo $this->Html->image('icons_pen.png'); ?> </a>
                                                <?php endif; ?>
                                                <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1'))): ?>
                                                    <a id="deletefolder1" huddle_id_del="<?php echo $row['AccountFolder']['account_folder_id'] ?>" rel="tooltip" data-original-title="Delete" data-method="delete"  href="#<?php //echo $this->base . '/Folder/delete/' . $row['AccountFolder']['account_folder_id']                             ?>"> <?php echo $this->Html->image('icons_del.png'); ?> </a>
                                                <?php endif; ?>
                                                <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1'))): ?>
                                                    <a href="#" rel="tooltip" data-original-title="Move" data-toggle="modal" data-target="#movefolderto" class="move_huddle" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>" fold_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>"> <?php echo $this->Html->image('icons_mov.png'); ?> </a>
                                                <?php endif; ?>
                                            </span>
                                        </div><!--top-->
                                        <style>
                                            .huddle_date{
                                                font-weight: 100;
                                                color: #333;
                                                margin:0px 0px 8px 0px;
                                            }
                                            .folder_details{
                                                margin: 0px;
                                                padding:0px;
                                                height:150px;
                                            }
                                            .folder_details li{
                                                padding: 14px 0px;
                                                color: #333;
                                                font-weight: 600;
                                                border-bottom: solid 1px #c4c4c4;
                                            }
                                            .folder_details li:last-child{
                                                border-bottom:0px;
                                            }
                                            .huddle_createrName{
                                                color:#858585;
                                                font-size: 14px;
                                                position: absolute;
                                                bottom: 2px;
                                            }
                                            .count_huddle{
                                                float:right;
                                                margin-right: 10px;

                                            }
                                            .col-4sm{
                                                position: relative;
                                            }
                                            .folder_link{
                                                font-weight: normal;
                                            }
                                            .topsection{
                                                height: 30px;
                                            }
                                        </style>


                                        <div class="huddle_date"><?php echo date('M d, Y', strtotime($row['AccountFolder']['created_date'])); ?><br></div>

                                        <a href="<?php echo $this->base . '/Folder/' . $row['AccountFolder']['account_folder_id'] ?>" class="folder_link">
                                            <ul class="folder_details">
                                                <?php if ($this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id, 2) > 0) { ?> <li><img src="/app/img/coaching_huddle.png" />  Coaching Huddles<div class="count_huddle"><?php echo $this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id, 2); ?></div></li> <?php } ?>
                                                <?php if ($this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id, 1) > 0) { ?> <li><img src="/app/img/Collaboration_huddle.png" />  Collaboration Huddles<div class="count_huddle"><?php echo $this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id, 1); ?></div></li><?php } ?>
                                                <?php if ($this->Custom->check_if_eval_huddle_active($account_id)) { ?>
                                                        <?php if ($this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id, 3) > 0) { ?> <li><img src="/app/img/evaluation.png" />  Assessment Huddles<div class="count_huddle"><?php echo $this->Custom->count_coaching_colab_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id, 3); ?></div></li><?php } ?>
                                                    <?php } ?>
                                                    <?php if ($this->Custom->count_folders_in_folder($row['AccountFolder']['account_folder_id'], $user_id, $account_id) > 0) { ?><li><img src="/app/img/folder_inside.png" />  Folders<div class="count_huddle"><?php echo $this->Custom->count_folders_in_folder($row['AccountFolder']['account_folder_id'], $user_id, $account_id); ?></div></li></li><?php } ?>
                                            </ul>
                                        </a>
                                        <div class="huddle_createrName">  <?php
                                            echo 'Created By: ';
                                            echo $this->Custom->user_name_from_id($row['AccountFolder']['created_by']);
                                            ?></div>

                                    </div>
                                </div><!---->
                            <?php else: ?>

                                <div class="col-4sm card" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>">
                                    <div class="topsection">
                                        <span class="datecls"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $row['AccountFolder']['account_folder_id'] ?>">
                                                <?php echo $row['AccountFolder']['name']; ?>
                                            </a></span>
                                        <span class="optioncls">
                                            <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                                <a href="<?php echo $this->base . '/add_huddle_angular/edit/' . $row['AccountFolder']['account_folder_id'] ?>"> <?php echo $this->Html->image('icons_pen.png'); ?> </a>
                                            <?php endif; ?>
                                            <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>
                                                <a rel="tooltip" data-method="delete" data-confirm="Are you sure you want to permanently delete this huddle?" href="<?php echo $this->base . '/Huddles/delete/' . $row['AccountFolder']['account_folder_id'] ?>"
                                                   data-original-title="Delete"> <?php echo $this->Html->image('icons_del.png'); ?> </a>
                                               <?php endif; ?>
                                            <a href="#" data-toggle="modal" data-target="#movefolder" > <?php echo $this->Html->image('icons_mov.png'); ?> </a>
                                        </span>

                                    </div><!--top-->

                                    <div class="postdate"><?php echo date('M d, Y', strtotime($row['AccountFolder']['created_date'])); ?></div>
                                    <div class="postedfile">
                                        <span><?php echo $this->Html->image('icons_vdo.png'); ?> <?php echo $row['AccountFolder']['total_videos']; ?></span>
                                        <span><?php echo $this->Html->image('icons_fle.png'); ?><?php echo $row['AccountFolder']['total_docs']; ?></span>

                                    </div>
                                    <?php
                                    $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                                    $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                                    $userGroups = $AccountFolderGroup->getHuddleGroups($row['AccountFolder']['account_folder_id']);

                                    if (isset($huddleGroups[0]['Group']['name']) && $huddleGroups[0]['Group']['name'] != '') {
                                        $is_group = TRUE;
                                    } else {
                                        $is_group = FALSE;
                                    }

                                    $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $userGroups, $user_current_account['User']['id']);
                                    ?>
                                    <div class="partcls" style="min-height: 160px;">
                                        <?php
                                        if ($row['AccountFolder']['meta_data_value'] == '2') {
                                            $part_height = '';
                                            ?>
                                            <label>Coach</label>
                                            <?php
                                        } elseif ($row['AccountFolder']['meta_data_value'] == '3') {
                                            $part_height = '';
                                            echo "<label>Assessor</label>";
                                        } else {
                                            if ($is_group) {
                                                $part_height = '';
                                            } else {
                                                $part_height = 'height:130px';
                                            }
                                            ?>
                                            <label>Participants in the Huddle</label>
                                        <?php } ?>
                                        <?php if ($huddleUsers): ?>
                                            <div class="widget-scrollable nopadding-left nopadding-top" style="margin-top: 4px !important;margin-bottom: 4px !important;">
                                                <div class="scrollbar disable" style="height: 44px;">
                                                    <div class="track" style="height: 44px;">
                                                        <div class="thumb" style="top: 0px; height: 15.3651px;">
                                                            <div class="end"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="viewport vshort2" style="<?php echo $part_height; ?>">
                                                    <div class="overview" style="top: 0px;">
                                                        <?php
                                                        $users_shown = array();
                                                        $coach_html = '';
                                                        $coachee_html = '';
                                                        ?>
                                                        <?php if ($row['AccountFolder']['meta_data_value'] == '2') {
                                                            ?>
                                                            <?php foreach ($huddleUsers as $invitedUsers): ?>
                                                                <?php
                                                                $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $invitedUsers['User']['id'] . "/" . $invitedUsers['User']['image'], $invitedUsers['User']['image']);
                                                                ?>
                                                                <?php if ($invitedUsers['huddle_users']["is_coach"] == '1') { ?>
                                                                    <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                                    <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                        <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                        <?php
                                                                    else:
                                                                        $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                        $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                        ?>

                                                                    <?php endif; ?>
                                                                <?php }elseif ($invitedUsers['huddle_users']["is_mentee"] == '1') { ?>
                                                                    <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                                    <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                        <?php $coachee_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                        <?php
                                                                    else:
                                                                        $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                        $coachee_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                        ?>
                                                                    <?php endif; ?>
                                                                <?php }else { ?>
                                                                    <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                                    <?php if ($invitedUsers['huddle_users']["role_id"] == '200') { ?>
                                                                        <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                            <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                            <?php
                                                                        else:
                                                                            $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                            $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                            ?>
                                                                        <?php endif; ?>
                                                                    <?php }else { ?>
                                                                        <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                            <?php $coachee_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                            <?php
                                                                        else:
                                                                            $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                            $coachee_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                            ?>
                                                                        <?php endif; ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                <?php
                                                                $users_shown[] = $invitedUsers['User']["id"];
                                                            endforeach;
                                                        }elseif ($row['AccountFolder']['meta_data_value'] == '3') {
                                                            foreach ($huddleUsers as $invitedUsers):
                                                                ?>
                                                                <?php
                                                                $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $invitedUsers['User']['id'] . "/" . $invitedUsers['User']['image'], $invitedUsers['User']['image']);
                                                                ?>
                                                                <?php if ($invitedUsers['huddle_users']["is_coach"] == '1') { ?>
                                                                    <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                                    <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                        <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                        <?php
                                                                    else:
                                                                        $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                        $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                        ?>

                                                                    <?php endif; ?>
                                                                <?php }elseif ($invitedUsers['huddle_users']["is_mentee"] == '1') { ?>
                                                                    <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                                    <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                        <?php $coachee_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                        <?php
                                                                    else:
                                                                        $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                        $coachee_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                        ?>
                                                                    <?php endif; ?>
                                                                <?php }else { ?>
                                                                    <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                                    <?php if ($invitedUsers['huddle_users']["role_id"] == '200') { ?>
                                                                        <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                            <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                            <?php
                                                                        else:
                                                                            $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                            $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                            ?>
                                                                        <?php endif; ?>
                                                                    <?php }else { ?>
                                                                        <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                            <?php $coachee_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                            <?php
                                                                        else:
                                                                            $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                            $coachee_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                            ?>
                                                                        <?php endif; ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                <?php
                                                                $users_shown[] = $invitedUsers['User']["id"];
                                                            endforeach;
                                                        }else {
                                                            foreach ($huddleUsers as $invitedUsers):
                                                                ?>
                                                                <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                                <?php
                                                                $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $invitedUsers['User']['id'] . "/" . $invitedUsers['User']['image'], $invitedUsers['User']['image']);
                                                                ?>
                                                                <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                    <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                    <?php
                                                                else:
                                                                    $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                    $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                    ?>
                                                                <?php endif; ?>
                                                                <?php
                                                                $users_shown[] = $invitedUsers['User']["id"];
                                                            endforeach;
                                                        }
                                                        echo $coach_html;
                                                        if ($coachee_html != '' && $row['AccountFolder']['meta_data_value'] == '2') {
                                                            echo '</div></div></div>';
                                                            echo '<label>Coachee</label>';
                                                            ?>
                                                            <div class="widget-scrollable nopadding-left nopadding-top" style="margin-top: 4px !important;margin-bottom: 4px !important;">
                                                                <div class="scrollbar disable" style="height: 44px;">
                                                                    <div class="track" style="height: 44px;">
                                                                        <div class="thumb" style="top: 0px; height: 15.3651px;">
                                                                            <div class="end"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="viewport vshort2">
                                                                    <div class="overview" style="top: 0px;">
                                                                        <?php
                                                                        echo $coachee_html;
                                                                    } elseif ($coachee_html != '' && $row['AccountFolder']['meta_data_value'] == '3') {
                                                                        echo '</div></div></div>';
                                                                        echo '<label>Assessor</label>';
                                                                        ?>
                                                                        <div class="widget-scrollable nopadding-left nopadding-top" style="margin-top: 4px !important;margin-bottom: 4px !important;">
                                                                            <div class="scrollbar disable" style="height: 44px;">
                                                                                <div class="track" style="height: 44px;">
                                                                                    <div class="thumb" style="top: 0px; height: 15.3651px;">
                                                                                        <div class="end"></div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="viewport vshort2">
                                                                                <div class="overview" style="top: 0px;">
                                                                                    <?php
                                                                                    echo $coachee_html;
                                                                                }
                                                                                $users_shown = array();
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php else: ?>
                                                                    <div style="height: 60px;"> No People in the Huddle.</div>
                                                                <?php endif; ?>
                                                                <?php if (($is_group && $row['AccountFolder']['meta_data_value'] != '3')): // || ($is_group && $row['AccountFolder']['meta_data_value'] == '3' && $this->Custom->check_if_evalutor($row['AccountFolder']['account_folder_id'], $user_id) ) ): ?>
                                                                    <label>Groups in huddle</label>
                                                                    <div class="widget-scrollable nopadding-left nopadding-vertical">
                                                                        <div class="scrollbar disable" style="height: 50px;">
                                                                            <div class="track" style="height: 50px;">
                                                                                <div class="thumb" style="top: 0px; height: 50px;">
                                                                                    <div class="end"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="viewport vshort3">
                                                                            <div class="overview">
                                                                                <?php
                                                                                $counter = 0;
                                                                                $groups_shown = array();
                                                                                foreach ($huddleGroups as $grp):
                                                                                    ?>
                                                                                    <?php if (isset($grp['huddle_groups']['group_id']) && in_array($grp['huddle_groups']['group_id'], $groups_shown)) continue; ?>
                                                                                    <?php
                                                                                    if ($counter > 0)
                                                                                        echo ", ";
                                                                                    echo $grp['Group']['name'];
                                                                                    ?>
                                                                                    <?php
                                                                                    $counter +=1;

                                                                                    $groups_shown[] = $grp['huddle_groups']['group_id'];

                                                                                endforeach;

                                                                                $groups_shown = array();
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php endif; ?>

                                                            </div>

                                                        </div> <!---->
                                                    <?php endif; ?>
                                                <?php endforeach; ?>


                                            <?php endif; ?>
                                            <?php if (isset($video_huddles) && !empty($video_huddles)): ?>

                                                <?php
                                                foreach ($video_huddles as $row):
//                                                if ($this->Custom->check_if_eval_huddle_active($account_id) == false) {
//                                                        if ($row[0]['folderType'] == 'evaluation') {
//                                                            continue;
//                                                        }
//                                                    }

                                                    $invitedUser = '';
                                                    $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                                                    $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                                                    $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $huddleGroups, $user_current_account['User']['id']);
                                                    ?>
                                                    <div class="col-4sm <?php if (isset($folders) && !empty($folders)): ?><?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>card<?php endif; ?><?php endif; ?>" id="huddle_row_<?php echo $row['AccountFolder']['account_folder_id']; ?>" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>">
                                                        <div class="handle" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>">
                                                            <div class="topsection">
                                                                <span class="datecls"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $row['AccountFolder']['account_folder_id'] ?>">
                                                                        <?php echo $row['AccountFolder']['name']; ?>
                                                                    </a></span>
                                                                <span class="optioncls">
                                                                    <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']))): ?>
                                                                        <a rel="tooltip" data-original-title="Edit" href="<?php echo $this->base . '/add_huddle_angular/edit/' . $row['AccountFolder']['account_folder_id'] ?>"> <?php echo $this->Html->image('icons_pen.png'); ?> </a>
                                                                    <?php endif; ?>
                                                                    <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']))): ?>
                                                                        <a rel="tooltip" data-method="delete" data-confirm="Are you sure you want to permanently delete this huddle?" href="<?php echo $this->base . '/Huddles/delete/' . $row['AccountFolder']['account_folder_id'] ?>"
                                                                           data-original-title="Delete"> <?php echo $this->Html->image('icons_del.png'); ?> </a>
                                                                       <?php endif; ?>
                                                                       <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']))): ?>
                                                                           <?php //if ($this->custom->check_folders_in_account($account_id)):     ?>
                                                                        <a href="#" rel="tooltip" data-original-title="Move" data-toggle="modal" data-target="#movefolderto" class="move_huddle" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>"> <?php echo $this->Html->image('icons_mov.png'); ?> </a>
                                                                        <?php //endif;    ?>
                                                                    <?php endif; ?>
                                                                </span>

                                                            </div><!--top-->
                                                        </div>
                                                        <a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $row['AccountFolder']['account_folder_id'] ?>">
                                                            <div class="postdate" style="font-weight: 100;"><?php echo date('M d, Y', strtotime($row['AccountFolder']['created_date'])); ?></div>
                                                            <div class="postedfile" style="font-weight: 100;">
                                                                <span><?php echo $this->Html->image('icons_vdo.png'); ?> <?php echo $row['AccountFolder']['total_videos']; ?></span>
                                                                <span><?php echo $this->Html->image('icons_fle.png'); ?><?php echo $row['AccountFolder']['total_docs']; ?></span>

                                                            </div>
                                                            <?php
                                                            $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                                                            $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                                                            $userGroups = $AccountFolderGroup->getHuddleGroups($row['AccountFolder']['account_folder_id']);

                                                            if (isset($huddleGroups[0]['Group']['name']) && $huddleGroups[0]['Group']['name'] != '') {
                                                                $is_group = TRUE;
                                                            } else {
                                                                $is_group = FALSE;
                                                            }

                                                            $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $userGroups, $user_current_account['User']['id']);
                                                            ?>
                                                            <div class="partcls" style="min-height: 160px;">
                                                                <?php
                                                                if ($row['AccountFolder']['meta_data_value'] == '2') {
                                                                    $part_height = '';
                                                                    ?>
                                                                    <label>Coach</label>
                                                                    <?php
                                                                } elseif ($row['AccountFolder']['meta_data_value'] == '3') {
                                                                    $part_height = '';
                                                                    echo "<label>Assessor</label>";
                                                                } else {
                                                                    if ($is_group) {
                                                                        $part_height = '';
                                                                    } else {
                                                                        $part_height = 'height:130px';
                                                                    }
                                                                    ?>
                                                                    <label>Participants in the Huddle</label>
                                                                <?php } ?>
                                                                <?php if ($huddleUsers): ?>
                                                                    <div class="widget-scrollable nopadding-left nopadding-top" style="margin-top: 4px !important;margin-bottom: 4px !important;">
                                                                        <div class="scrollbar disable" style="height: 44px;">
                                                                            <div class="track" style="height: 44px;">
                                                                                <div class="thumb" style="top: 0px; height: 15.3651px;">
                                                                                    <div class="end"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="viewport vshort2" style="<?php echo $part_height; ?>">
                                                                            <div class="overview" style="top: 0px;">
                                                                                <?php
                                                                                $users_shown = array();
                                                                                $coach_html = '';
                                                                                $coachee_html = '';
                                                                                ?>
                                                                                <?php if ($row['AccountFolder']['meta_data_value'] == '2') {
                                                                                    ?>
                                                                                    <?php foreach ($huddleUsers as $invitedUsers): ?>
                                                                                        <?php
                                                                                        $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $invitedUsers['User']['id'] . "/" . $invitedUsers['User']['image'], $invitedUsers['User']['image']);
                                                                                        ?>
                                                                                        <?php if ($invitedUsers['huddle_users']["is_coach"] == '1') { ?>
                                                                                            <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                                                            <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                                                <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                                                <?php
                                                                                            else:
                                                                                                $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                                                $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                                                ?>

                                                                                            <?php endif; ?>
                                                                                        <?php }elseif ($invitedUsers['huddle_users']["is_mentee"] == '1') { ?>
                                                                                            <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                                                            <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                                                <?php $coachee_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                                                <?php
                                                                                            else:
                                                                                                $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                                                $coachee_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                                                ?>
                                                                                            <?php endif; ?>
                                                                                        <?php }else { ?>
                                                                                            <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                                                            <?php if ($invitedUsers['huddle_users']["role_id"] == '200') { ?>
                                                                                                <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                                                    <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                                                    <?php
                                                                                                else:
                                                                                                    $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                                                    $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                                                    ?>
                                                                                                <?php endif; ?>
                                                                                            <?php }else { ?>
                                                                                                <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                                                    <?php $coachee_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                                                    <?php
                                                                                                else:
                                                                                                    $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                                                    $coachee_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                                                    ?>
                                                                                                <?php endif; ?>
                                                                                            <?php } ?>
                                                                                        <?php } ?>
                                                                                        <?php
                                                                                        $users_shown[] = $invitedUsers['User']["id"];
                                                                                    endforeach;
                                                                                }elseif ($row['AccountFolder']['meta_data_value'] == '3') {
                                                                                    ?>
                                                                                    <?php foreach ($huddleUsers as $invitedUsers): ?>
                                                                                        <?php
                                                                                        $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $invitedUsers['User']['id'] . "/" . $invitedUsers['User']['image'], $invitedUsers['User']['image']);
                                                                                        ?>
                                                                                        <?php if ($invitedUsers['huddle_users']["is_coach"] == '1') { ?>
                                                                                            <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                                                            <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                                                <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                                                <?php
                                                                                            else:
                                                                                                $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                                                $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                                                ?>

                                                                                            <?php endif; ?>
                                                                                        <?php }elseif ($invitedUsers['huddle_users']["is_mentee"] == '1') { ?>
                                                                                            <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                                                            <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                                                <?php $coachee_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                                                <?php
                                                                                            else:
                                                                                                $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                                                $coachee_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                                                ?>
                                                                                            <?php endif; ?>
                                                                                        <?php }else { ?>
                                                                                            <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                                                            <?php if ($invitedUsers['huddle_users']["role_id"] == '200') { ?>
                                                                                                <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                                                    <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                                                    <?php
                                                                                                else:
                                                                                                    $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                                                    $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                                                    ?>
                                                                                                <?php endif; ?>
                                                                                            <?php }else { ?>
                                                                                                <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                                                    <?php $coachee_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                                                    <?php
                                                                                                else:
                                                                                                    $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                                                    $coachee_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                                                    ?>
                                                                                                <?php endif; ?>
                                                                                            <?php } ?>
                                                                                        <?php } ?>
                                                                                        <?php
                                                                                        $users_shown[] = $invitedUsers['User']["id"];
                                                                                    endforeach;
                                                                                }else {
                                                                                    foreach ($huddleUsers as $invitedUsers):
                                                                                        ?>
                                                                                        <?php if (isset($invitedUsers['User']["id"]) && in_array($invitedUsers['User']["id"], $users_shown)) continue; ?>
                                                                                        <?php
                                                                                        $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $invitedUsers['User']['id'] . "/" . $invitedUsers['User']['image'], $invitedUsers['User']['image']);
                                                                                        ?>
                                                                                        <?php if (isset($invitedUsers['User']['image']) && $invitedUsers['User']['image'] != ''): ?>
                                                                                            <?php $coach_html .= $this->Html->image($avatar_path, array('alt' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'class' => 'photo', 'data-original-title' => $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'], 'height' => '34', 'rel' => 'tooltip', 'width' => '34', 'align' => 'left')); ?>
                                                                                            <?php
                                                                                        else:
                                                                                            $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png');
                                                                                            $coach_html .= '<img width="34" height="34" src="' . $chimg . '" rel="tooltip" class="photo" alt="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '" data-original-title="' . $invitedUsers['User']['first_name'] . ' ' . $invitedUsers['User']['last_name'] . '">';
                                                                                            ?>
                                                                                        <?php endif; ?>
                                                                                        <?php
                                                                                        $users_shown[] = $invitedUsers['User']["id"];
                                                                                    endforeach;
                                                                                }
                                                                                echo $coach_html;
                                                                                if ($coachee_html != '') {
                                                                                    $style = "";
                                                                                    echo '</div></div></div>';
                                                                                    if ($row['AccountFolder']['meta_data_value'] == '2') {
                                                                                        echo '<label>Coachee</label>';
                                                                                    } elseif ($row['AccountFolder']['meta_data_value'] == '3') {
                                                                                        if ($this->Custom->check_if_eval_huddle($row['AccountFolder']['account_folder_id'])) {
                                                                                            if ($this->Custom->check_if_evalutor($row['AccountFolder']['account_folder_id'], $user_id)) {
                                                                                                echo '<label>Assessed Participant(s)</label>';
                                                                                                $style = 'display: block';
                                                                                            } else {
                                                                                                $style = 'display: none;';
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                    ?>
                                                                                    <div class="widget-scrollable nopadding-left nopadding-top" style="margin-top: 4px !important;margin-bottom: 4px !important; <?php echo $style; ?>">
                                                                                        <div class="scrollbar disable" style="height: 44px;">
                                                                                            <div class="track" style="height: 44px;">
                                                                                                <div class="thumb" style="top: 0px; height: 15.3651px;">
                                                                                                    <div class="end"></div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="viewport vshort2">
                                                                                            <div class="overview" style="top: 0px;">
                                                                                                <?php
                                                                                                echo $coachee_html;
                                                                                            }
                                                                                            $users_shown = array();
                                                                                            ?>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            <?php else: ?>
                                                                                <div style="height: 60px;"> No People in the Huddle.</div>
                                                                            <?php endif; ?>
                                                                            <?php if (($is_group && $row['AccountFolder']['meta_data_value'] != '3')): // || ($is_group && $row['AccountFolder']['meta_data_value'] == '3' && $this->Custom->check_if_evalutor($row['AccountFolder']['account_folder_id'], $user_id) ) ): ?>
                                                                                <label>Groups in huddle</label>
                                                                                <div class="widget-scrollable nopadding-left nopadding-vertical">
                                                                                    <div class="scrollbar disable" style="height: 50px;">
                                                                                        <div class="track" style="height: 50px;">
                                                                                            <div class="thumb" style="top: 0px; height: 50px;">
                                                                                                <div class="end"></div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="viewport vshort3">
                                                                                        <div class="overview">
                                                                                            <?php
                                                                                            $counter = 0;
                                                                                            $groups_shown = array();
                                                                                            foreach ($huddleGroups as $grp):
                                                                                                ?>
                                                                                                <?php if (isset($grp['huddle_groups']['group_id']) && in_array($grp['huddle_groups']['group_id'], $groups_shown)) continue; ?>
                                                                                                <?php
                                                                                                if ($counter > 0)
                                                                                                    echo ", ";
                                                                                                echo $grp['Group']['name'];
                                                                                                ?>
                                                                                                <?php
                                                                                                $counter +=1;

                                                                                                $groups_shown[] = $grp['huddle_groups']['group_id'];

                                                                                            endforeach;

                                                                                            $groups_shown = array();
                                                                                            ?>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            <?php endif; ?>

                                                                        </div>

                                                                        <?php if ($row['AccountFolder']['meta_data_value'] == '2'): ?>
                                                                            <div style="float: right;font-weight: bold;font-size: 12px;height:0px;    position: relative;bottom: 11px;">Coaching</div>
                                                                        <?php elseif ($row['AccountFolder']['meta_data_value'] == '3'): ?>
                                                                            <div style="float: right;font-weight: bold;font-size: 12px;height:0px;     position: relative;bottom: 14px;">Assessment</div>
                                                                        <?php else: ?>
                                                                            <div style="float: right;font-weight: bold;font-size: 12px;height:0px;    position: relative;bottom: 14px;">Collaboration </div>
                                                                        <?php endif; ?>
                                                                        </a>
                                                                    </div> <!---->
                                                                <?php endforeach; ?>

                                                            <?php endif; ?>

                                                        <?php elseif (isset($view_mode) && $view_mode == 'list'): ?>
                                                            <!----List View---->
                                                            <style>
                                                                .box.style2{
                                                                    margin:0px;
                                                                    border: 1px solid #dddad2;
                                                                    margin-bottom:-1px;
                                                                    border-radius:0px;
                                                                    padding:8px;
                                                                    min-height:70px;
                                                                }
                                                                .huddle_list_left{
                                                                    float:left;
                                                                    width:710px;
                                                                }
                                                                .huddle_list_right{
                                                                    float:right;
                                                                }
                                                                .row{
                                                                    margin-left:0px;
                                                                }
                                                                [data-group] {
                                                                    margin-top: 6px;
                                                                }
                                                                .bridcrm{
                                                                    margin:4px 0px 2px 0px;
                                                                }
                                                                .grid_listHeader{
                                                                    font-size: 15px;
                                                                    color: #424240;
                                                                    padding: 0px 30px;
                                                                    font-weight: 600;
                                                                    margin-bottom:10px;
                                                                }
                                                                .grid_listHeader span{
                                                                    display:inline-block;
                                                                }
                                                                .header_name{
                                                                    float:left !important;
                                                                }
                                                                .header_rightBox{
                                                                    float:right;
                                                                }
                                                                .header_video{
                                                                    margin-left:10px;
                                                                }
                                                                .header_resources{
                                                                    margin-left:10px;
                                                                }
                                                                .header_date{
                                                                    margin-left:10px;
                                                                    width: 100px;
                                                                }
                                                                .header_huddle{
                                                                    margin-left:10px;
                                                                }
                                                                .header_participant{
                                                                    margin-left:10px;
                                                                }
                                                                .header_action{
                                                                    margin-left:10px;
                                                                    width: 58px;
                                                                }
                                                            </style>
                                                            <div class="grid_listHeader">
                                                                <span class="header_name">Name</span>
                                                                <div class="header_rightBox">
                                                                    <span class="header_video" style="margin-right:20px;">Type</span>
                                                                    <span class="header_video">Videos</span>
                                                                    <span class="header_resources">Resources</span>
                                                                    <span class="header_date">Date Created</span>
                                                                    <?php if (isset($folders) && !empty($folders)): ?>
                                                                        <span class="header_huddle">Huddles</span>
                                                                    <?php endif; ?>
                                                                    <span class="header_participant">Participants</span>
                                                                    <span class="header_action">Action</span>
                                                                </div>
                                                                <div class="clear"></div>
                                                            </div>
                                                            <div class="row huddles-list huddles-list--list" style="margin-top: -14px;margin:0 15px;" id="dropdown">
                                                                <?php if (isset($folders) && !empty($folders)): ?>
                                                                    <ol class="huddle-list-view mainfold" style="margin-bottom: 0px;">
                                                                        <?php
                                                                        $grups = array();
                                                                        foreach ($folders as $row):
                                                                            $first_letter = $this->Custom->py_slice(trim($row['AccountFolder']['name']), '0');
                                                                            $grups[$row['AccountFolder']['account_folder_id']] = strtoupper($first_letter);
                                                                            $grups = array_unique($grups);
                                                                            ?>
                                                                        <?php endforeach; ?>

                                                                        <?php foreach ($folders as $row): ?>
                                                                            <?php
                                                                            $invitedUser = '';
                                                                            $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                                                                            $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                                                                            $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $huddleGroups, $user_current_account['User']['id']);
                                                                            ?>

                                                                            <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                                                                <li class="folderlist list_folder stackDrop2" id="huddle_row_<?php echo $row['AccountFolder']['account_folder_id']; ?>" folder_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>">
                                                                                    <div class="huddle_list_detail_folder">
                                                                                        <span class="foldrcl1"><?php echo $this->Html->image('icons_folder.png'); ?></span>
                                                                                        <span class="foldrcl2" style="white-space: nowrap;display: inline-block;text-overflow: ellipsis;overflow: hidden;width: 45%;position: relative;top: 4px;"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Folder/' . $row['AccountFolder']['account_folder_id'] ?>">
                                                                                                <?php echo $row['AccountFolder']['name']; ?>
                                                                                            </a></span>
                                                                                    <?php else: ?>
                                                                                        <li class="huddlist">
                                                                                            <span class="foldrcl1" style="white-space: nowrap;display: inline-block;text-overflow: ellipsis;overflow: hidden;width: 48%;position: relative;"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $row['AccountFolder']['account_folder_id'] ?>">
                                                                                                    <?php echo $row['AccountFolder']['name']; ?>
                                                                                                </a></span>
                                                                                        <?php endif; ?>             <span class="foldrcl5" style="display:inline-block;min-height:5px;">
                                                                                        <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1'))): ?>
                                                                                            <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                                                                                    <a id="editfolder1" huddle_id="<?php echo $row['AccountFolder']['account_folder_id'] ?> " rel="tooltip" data-original-title="Edit"  href="#<?php //echo $this->base . '/Folder/edit/' . $row['AccountFolder']['account_folder_id']                         ?>"> <?php echo $this->Html->image('icons_pen.png'); ?> </a>
                                                                                                <?php else: ?>
                                                                                                    <a href="<?php echo $this->base . '/add_huddle_angular/edit/' . $row['AccountFolder']['account_folder_id']; ?>"> <?php echo $this->Html->image('icons_pen.png'); ?> </a>
                                                                                                <?php endif; ?>
                                                                                            <?php endif; ?>
                                                                                            <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1'))): ?>
                                                                                                <a id="deletefolder1" huddle_id_del="<?php echo $row['AccountFolder']['account_folder_id'] ?>"  rel="tooltip nofollow" data-original-title="Delete" data-method="delete" href="#<?php //echo $this->base . '/Folder/delete/' . $row['AccountFolder']['account_folder_id']                          ?>"> <?php echo $this->Html->image('icons_del.png'); ?> </a>
                                                                                            <?php endif; ?>
                                                                                            <?php if (($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && ($user_permissions['UserAccount']['folders_check'] == '1'))): ?>
                                                                                                <a href="#" class="move_huddle" rel="tooltip" data-original-title="Move" data-toggle="modal" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>" data-target="#movefolderto" fold_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>"> <?php echo $this->Html->image('icons_mov.png'); ?> </a>
                                                                                            <?php endif; ?>
                                                                                        </span>
                                                                                        <span class="foldrcl4" style="width: 85px; text-align: center;">&nbsp;</span>
                                                                                        <span class="foldrcl4" style="text-align: center;  width: 85px;"><?php echo $this->Custom->count_huddles_in_folder($row['AccountFolder']['account_folder_id'], $user_id); ?></span>
                                                                                        <span class="foldrcl3" style="text-align:center;">&nbsp;<?php echo date('M d, Y', strtotime($row['AccountFolder']['created_date'])) ?></span>
                                                                                        <?php if ($row['AccountFolder']['folder_type'] != '5'): ?>
                                                                                            <span class="foldrcl7"><?php echo $this->Html->image('icons_fle.png'); ?> <i><?php echo $row['AccountFolder']['total_docs']; ?></i></span>
                                                                                            <span class="foldrcl6"><?php echo $this->Html->image('icons_vdo.png'); ?><i><?php echo $row['AccountFolder']['total_videos']; ?></i> </span>

                                                                                        <?php endif; ?>
                                                                                        <div class="clear"></div>
                                                                                </div>
                                                                            </li>
                                                                        <?php endforeach; ?>
                                                                    </ol>

                                                                <?php endif; ?>

                                                                <?php if (isset($video_huddles) && !empty($video_huddles)): ?>
                                                                    <ol class="huddle-list-view mainfold launchPad2" style="margin-top: -2px;" >
                                                                        <?php
                                                                        $grups = array();
                                                                        foreach ($video_huddles as $row):
                                                                            $first_letter = $this->Custom->py_slice(trim($row['AccountFolder']['name']), '0');
                                                                            $grups[$row['AccountFolder']['account_folder_id']] = strtoupper($first_letter);
                                                                            $grups = array_unique($grups);
                                                                            ?>
                                                                        <?php endforeach; ?>

                                                                        <?php foreach ($video_huddles as $row): ?>
                                                                            <?php
//                                                                        if ($this->Custom->check_if_eval_huddle_active($account_id) == false) {
//                                                                                if ($row[0]['folderType'] == 'evaluation') {
//                                                                                    continue;
//                                                                                }
//                                                                            }
                                                                            $invitedUser = '';
                                                                            $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                                                                            $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                                                                            $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $huddleGroups, $user_current_account['User']['id']);
                                                                            ?>
                                                                            <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                                                                <li class="folderlist">
                                                                                    <span class="foldrcl1"><?php echo $this->Html->image('icons_folder.png'); ?></span>
                                                                                    <span class="foldrcl2"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Folder/' . $row['AccountFolder']['account_folder_id'] ?>">
                                                                                            <?php echo $row['AccountFolder']['name']; ?>
                                                                                        </a></span>
                                                                                <?php else: ?>
                                                                                <li class="huddlist <?php if (isset($folders) && !empty($folders)): ?><?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>card<?php endif; ?><?php endif; ?>" id="huddle_row_<?php echo $row['AccountFolder']['account_folder_id']; ?>" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>">

                                                                                    <div class="huddle_list_detail handle">

                                                                                        <span class="foldrcl1" style="white-space: nowrap;display: inline-block;text-overflow: ellipsis;overflow: hidden;width: 48%;position: relative;"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Huddles/view/' . $row['AccountFolder']['account_folder_id'] ?>">
                                                                                                <?php echo $row['AccountFolder']['name']; ?>
                                                                                            </a></span>
                                                                                    <?php endif; ?>             <span class="foldrcl5" style="display:inline-block;min-height:5px;">
                                                                                    <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']))): ?>
                                                                                        <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                                                                                <a rel="tooltip" data-original-title="Edit" href="<?php echo $this->base . '/Folder/edit/' . $row['AccountFolder']['account_folder_id']; ?>"> <?php echo $this->Html->image('icons_pen.png'); ?> </a>
                                                                                            <?php else: ?>
                                                                                                <a rel="tooltip" data-original-title="Edit" href="<?php echo $this->base . '/add_huddle_angular/edit/' . $row['AccountFolder']['account_folder_id']; ?>"> <?php echo $this->Html->image('icons_pen.png'); ?> </a>
                                                                                            <?php endif; ?>
                                                                                        <?php endif; ?>
                                                                                        <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']))): ?>
                                                                                            <a rel="tooltip nofollow" data-original-title="Delete" data-method="delete" data-confirm="Are you sure you want to permanently delete this huddle?" href="<?php echo $this->base . '/Huddles/delete/' . $row['AccountFolder']['account_folder_id'] ?>"> <?php echo $this->Html->image('icons_del.png'); ?> </a>
                                                                                        <?php endif; ?>
                                                                                        <?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']))): ?>
                                                                                            <?php //if ($this->custom->check_folders_in_account($account_id)):   ?>
                                                                                            <a href="#" class="move_huddle" rel="tooltip" data-original-title="Move" data-toggle="modal" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>" data-target="#movefolderto" > <?php echo $this->Html->image('icons_mov.png'); ?> </a>
                                                                                            <?php //endif;    ?>
                                                                                        <?php endif; ?>
                                                                                    </span>
                                                                                    <?php
                                                                                    if ($this->Custom->check_if_eval_huddle($row['AccountFolder']['account_folder_id'])) {
                                                                                        if ($this->Custom->check_if_evalutor($row['AccountFolder']['account_folder_id'], $user_id)) {
                                                                                            $this->Custom->check_if_evalutor($row['AccountFolder']['account_folder_id'], $user_id);
                                                                                            ?>
                                                                                            <span class="foldrcl4"style="width: 85px; text-align: center;"><?php echo count($huddleUsers); ?></span>

                                                                                        <?php } else { ?>
                                                                                            <span class="foldrcl4"style="width: 85px; text-align: center;">0</span>
                                                                                            <?php
                                                                                        }
                                                                                    } else {
                                                                                        ?>
                                                                                        <span class="foldrcl4"style="width: 85px; text-align: center;"><?php echo count($huddleUsers); ?></span>
                                                                                    <?php } ?>


                                                                                    <?php if (isset($folders) && !empty($folders)): ?>     <span class="foldrcl4" style="text-align: center;  width: 85px;">&nbsp;</span>  <?php endif ?>
                                                                                    <span class="foldrcl3" style="text-align:center;">&nbsp;<?php echo date('M d, Y', strtotime($row['AccountFolder']['created_date'])) ?></span>
                                                                                    <?php if ($row['AccountFolder']['folder_type'] != '5'): ?>
                                                                                        <span class="foldrcl7" style="text-align:center;width: 80px;"><?php echo $row['AccountFolder']['total_docs']; ?></span>
                                                                                        <span class="foldrcl6" style="text-align:center;"><?php echo $row['AccountFolder']['total_videos']; ?></span>
                                                                                    <?php endif; ?>
                                                                                    <?php if ($row['AccountFolder']['meta_data_value'] == '2'): ?>
                                                                                        <span class="foldrcl6" style="font-weight: bold;font-size: 12px;width: 76px;">Coach</span>
                                                                                    <?php elseif ($row['AccountFolder']['meta_data_value'] == '3'): ?>
                                                                                        <span class="foldrcl6" style="float: right;font-weight: bold;font-size: 12px;width: 76px;">Assess</span>
                                                                                    <?php else: ?>
                                                                                        <span class="foldrcl6" style="float: right;font-weight: bold;font-size: 12px;width: 76px;">Collab </span>
                                                                                    <?php endif; ?>
                                                                                    <div class="clear"></div>

                                                                                </div>
                                                                            </li>
                                                                        <?php endforeach; ?>
                                                                    </ol>

                                                                <?php endif; ?>
                                                            </div>
                                                        <?php endif; ?>
                                                    <?php else: ?>
                                                        <div class="text-center">
                                                            <?php $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/video-brown.png'); ?>
                                                            <p><?php echo $this->Html->image($chimg, array('alt' => 'Video-brown')); ?></p>
                                                            <h2 class="heading--info">No Huddle found in this Folder.</h2>
                                                            <br />
                                                            <a href="<?php echo $this->base; ?>/Huddles" style="padding-left:20px;">&laquo; Back to Huddles</a>

                                                            <p style="display:none;">To view sample Coaching Huddle, <a href="/video_huddles?sample=yes" class="blue-link">click here</a>!</p>
                                                        </div>
                                                        <div class="<?php if ($user_current_account['roles']['role_id'] != '120' || $user_permissions['UserAccount']['permission_maintain_folders'] == '1'): ?>btn-annotation<?php else: ?>btn-annotation2<?php endif; ?>">
                                                            <?php if ($user_current_account['roles']['role_id'] != '120' || $user_permissions['UserAccount']['permission_maintain_folders'] == '1'): ?>
                                                                Click here to create your first Huddle!
                                                            <?php endif; ?>
                                                            <!--            A Huddle is a great way to work with others to improve teaching and learning. Share videos and related resources and participate in discussions with other Huddle participants.-->
                                                        </div>

                                                        <a href="#" id="view_browser_notification" style="display: none;" data-toggle="modal" data-target="BrowserMessageModal">browser message</a>
                                                        <div id="BrowserMessageModal" class="modal" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="header">
                                                                        <h4 class="header-title nomargin-vertical smargin-bottom">Upgade Your Browser</h4>
                                                                        <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                                                                    </div>

                                                                    <p>You're Using an unsupported browser. Please upgrade your browser to view <?php $this->Custom->get_site_settings('site_title') ?> Application</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                    <div class="clear"></div>
                                                </div>


                                                <div id="editfoldermodal1" class="modal in" role="dialog" aria-hidden="true">
                                                    <div class="modal-dialog"  style="width: 400px;">
                                                        <div class="modal-content">
                                                            <div class="header" style="padding-left: 25px;padding-bottom: 0px;margin-bottom: 0px;padding-top: 15px;">
                                                                <h4 class="header-title nomargin-vertical smargin-bottom">Rename Folder</h4>
                                                                <a class="close-reveal-modal btn btn-grey close style2" onclick="$('#editfoldermodal1').hide();">x</a>
                                                            </div>
                                                            <form method="post" id="new_video_huddle3" enctype="multipart/form-data" class="new_video_huddle"
                                                                  action="<?php echo $this->base . '/Folder/edit/' ?>" accept-charset="UTF-8">
                                                                <div id="tabs">
                                                                    <div>
                                                                        <div id="step-1" style="margin-left: -20px;">
                                                                            <div style="margin:0;padding:0;display:inline"><input type="hidden" value="✓" name="utf8"></div>

                                                                            <a class="close-reveal-modal"></a>
                                                                            <div class="span5">
                                                                                <div class="row"  style="margin-bottom: 20px;">
                                                                                    <span class="wiz-step1-radio"><input type="radio" id="collab-huddle" name="type" class="cls_coaching_trig_2" checked="checked" value="1" style="display:none;"></span>
                                                                                    <div style="clear: both;height:10px;"></div>
                                                                                </div>
                                                                                <div class="row" style="margin-bottom: 20px;">
                                                                                    <input name="data[hname]" id="video_folder_name2" class="size-big huddle-name "  placeholder="Folder Name" size="30" type="text" value="<?php //echo $huddles['AccountFolder']['name'];                              ?>" style="width: 350px;"  required/>
                                                                                    <label for="video_folder_name" class="error" style="display:none;background-color: none !important;">This field is required.</label>
                                                                                </div>
                                                                                <div style="clear: both;"></div>
                                                                                <div class="row">
                                                                                    <textarea style="display:none;" rows="4" placeholder="folder Description (Optional)" id="video_huddle_description"  cols="40" class="size-big" name="hdescription"><?php //echo $huddles['AccountFolder']['desc'];                               ?></textarea>
                                                                                </div>
                                                                                <?php echo $this->Form->input('created_by', array('value' => $user_id, 'type' => 'hidden', 'id' => 'video_huddle_created_by')); ?>
                                                                            </div>
                                                                            <div style="clear: both;"></div>
                                                                            <div class="form-actions" style="text-align: left;margin-left: 22px;margin-top:0px;">
                                                                                <input type="hidden" id="hidden_huddle_id">
                                                                                <input type="button" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" id="btnSaveHuddle" value="Edit Folder" name="commit"  class="btn btn-green"  >
                                                                                <a id="cancel1" class="btn btn-white" onclick="$('#editfoldermodal1').hide();">Cancel</a>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </form>



                                                        </div>
                                                    </div>
                                                </div>

                                                <script>

                                                    $(document).ready(function () {
                                                        $('#new_video_huddle3').submit(function (evt) {
                                                            evt.preventDefault();
                                                        });

                                                        $("#video_folder_name2").keyup(function (e) {
                                                            var str_length = $("#video_folder_name2").val().trim();
                                                            if (str_length.length > 0 && e.which == 13) {
                                                                $("#btnSaveHuddle").trigger('click');
                                                            }
                                                        });

                                                        $(document).on("click", "#editfolder1", function () {
                                                            var huddle_id = $(this).attr("huddle_id");
                                                            console.log(huddle_id);
                                                            $.ajax({url: home_url + '/Folder/editajax/' + huddle_id,
                                                                data: {hname: '', hdescription: ''},
                                                                type: 'post',
                                                                dataType: 'json',
                                                                success: function (response) {

                                                                    $("#editfoldermodal1").show();
                                                                    $("#video_folder_name2").val(response.name);
                                                                    $("#hidden_huddle_id").val(response.account_folder_id);
                                                                    //                                                console.log(response.account_folder_id);
                                                                    //                                                console.log(response.name);
                                                                    // $('#editfoldermodal').val(output);

                                                                }
                                                            });



                                                        });
                                                        $(document).on("click", "#btnSaveHuddle", function () {
                                                            var huddle_id = $("#hidden_huddle_id").val();
                                                            var huddle_name = $("#video_folder_name2").val();
                                                            var folder_id = "<?php echo $folder_id_edit; ?>";
                                                            $.ajax({url: home_url + '/Folder/edit/' + huddle_id,
                                                                data: {hname: huddle_name, hdescription: '', type: 1, folder_id: folder_id},
                                                                type: 'post',
                                                                success: function (response) {
                                                                    $('#editfoldermodal1').hide();
                                                                    $("#flashMessage").hide();
                                                                    $("#showflashmessage1").fadeIn();
                                                                    if (response == 'folderexists')
                                                                    {
                                                                        var show_msg = '<div id="flashMessage" class="message error" style="cursor: pointer;">A folder with same name already exist, please try another name. You might not see the folder with name "' + huddle_name + '" because you are not participating in that</div>';
                                                                    }
                                                                    else {
                                                                        var show_msg = '<div id="flashMessage" class="message success" style="cursor: pointer;">' + huddle_name + ' has been updated successfully.</div>';
                                                                    }
                                                                    $("#showflashmessage1").prepend(show_msg);
                                                                    $("#txtSearchHuddles:input").val('');
                                                                    dofolderHuddleSearch();
                                                                    // location.reload();
                                                                }
                                                            });



                                                        });
                                                        $(document).on("click", "#deletefolder1", function () {
                                                            var huddle_id = $(this).attr("huddle_id_del");
                                                            if (confirm('Are you sure you want to permanently delete this folder?')) {

                                                                $.ajax({url: home_url + '/Folder/delete/' + huddle_id,
                                                                    data: {hname: '', hdescription: ''},
                                                                    type: 'post',
                                                                    success: function (response) {
                                                                        $("#flashMessage").hide();
                                                                        $("#showflashmessage1").fadeIn();
                                                                        if (response == 'contentexists')
                                                                        {
                                                                            var show_msg = '<div id="flashMessage" class="message error" style="cursor: pointer;">Folder cannot be deleted until it\'s empty.</div>';
                                                                        }
                                                                        else {
                                                                            var show_msg = '<div id="flashMessage" class="message success" style="cursor: pointer;">Folder has been deleted successfully.</div>';
                                                                        }
                                                                        $("#showflashmessage1").prepend(show_msg);
                                                                        $("#txtSearchHuddles:input").val('');

                                                                        dofolderHuddleSearch();


                                                                    }
                                                                });
                                                            } else {
                                                                alert('Folder Not Deleted');
                                                            }
                                                        });


                                                    });



                                                </script>


                                                <script type="text/javascript">

                                                    $("#txtSearchFolderHuddles").on("keypress", function (e) {
                                                        $("#clearSearchHuddlesfolder").show();
                                                        var code = (e.keyCode ? e.keyCode : e.which);
                                                        if (code == 13) {
                                                            dofolderHuddleSearch_textbox();
                                                            return false;
                                                        }
                                                    });

                                                    $("#btnHuddleSearchfolder").click(function () {
                                                        if ($("#txtSearchFolderHuddles").val().length > 0) {
                                                            dofolderHuddleSearch_textbox();
                                                        }
                                                    });

                                                    $("#clearSearchHuddlesfolder").click(function () {
                                                        $("#txtSearchFolderHuddles").val("");
                                                        $("#clearSearchHuddlesfolder").hide();
                                                        dofolderHuddleSearch();
                                                        return false;
                                                    });

                                                    function dofolderHuddleSearch() {
                                                        var $title = $('#txtSearchFolderHuddles').val();
                                                        var $mode = $('#search-mode').val();
                                                        var $folder_id = $('#txtfolderid').val();
                                                        //$.blockUI({message: $('#domMessage').html()});
                                                        $.ajax({
                                                            url: home_url + '/Folder/search',
                                                            data: {title: $title, mode: $mode, folder_id: $folder_id},
                                                            dataType: 'JSON',
                                                            type: 'POST',
                                                            success: function (response) {
                                                                //$(document).ajaxStop($.unblockUI);
                                                                if (response.status == true) {
                                                                    $('#huddle-listings').html(response.contents);
                                                                    $('.page-title__counter').html($('#search-huddle-count').html());

                                                                } else {
                                                                    $('.page-title__counter').html($('#search-huddle-count').html());
                                                                    $('#huddle-listings').html(response.contents);
                                                                }
                                                                //                $(".btn-move-to").hide();
                                                                //                $(".sel-move-to").hide();
                                                                //                $(".huddle-to-move").hide();
                                                                //                $(".btn-move").show();
                                                            },
                                                            error: function () {

                                                            }
                                                        });
                                                    }

                                                    function dofolderHuddleSearch_textbox() {
                                                        var $title = $('#txtSearchFolderHuddles').val();
                                                        var $mode = $('#search-mode').val();
                                                        var $folder_id = $('#txtfolderid').val();
                                                        //$.blockUI({message: $('#domMessage').html()});
                                                        $.ajax({
                                                            url: home_url + '/Folder/search_textbox',
                                                            data: {title: $title, mode: $mode, folder_id: $folder_id},
                                                            dataType: 'JSON',
                                                            type: 'POST',
                                                            success: function (response) {
                                                                //$(document).ajaxStop($.unblockUI);
                                                                if (response.status == true) {
                                                                    $('#huddle-listings').html(response.contents);
                                                                    $('.page-title__counter').html($('#search-huddle-count').html());

                                                                } else {
                                                                    $('.page-title__counter').html($('#search-huddle-count').html());
                                                                    $('#huddle-listings').html(response.contents);
                                                                }
                                                                //                $(".btn-move-to").hide();
                                                                //                $(".sel-move-to").hide();
                                                                //                $(".huddle-to-move").hide();
                                                                //                $(".btn-move").show();
                                                            },
                                                            error: function () {

                                                            }
                                                        });
                                                    }
                                                    $(document).ready(function (e) {

                                                        var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><a href="/huddles"><?php echo $breadcrumb_language_based_content['huddles_breadcrumb']; ?></span></a><?php echo $breadcrumb; ?> <span><?php echo addslashes($current_folder_name); ?></span>';

                                                        $('.breadCrum').html(bread_crumb_data);

                                                    });


                                                </script>


                                            </div>