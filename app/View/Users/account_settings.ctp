<div id="main" class="container">
    <?php
    if (strpos($_SERVER['HTTP_HOST'], 'hmh') !== false) {
        $site_id = 2;
    } else {
        $site_id = 1;
    }
    $credentials = '';
    if (!empty($account_id) && !empty($auth_token) && !empty($user_id)) {
        $credentials = $account_id . '/' . $auth_token . '/' . $user_id . '/' . $account_folder_id;
    } else {
        $credentials = '';
    }
    ?>
    <?php
    if (!isset($user) || empty($user)) :
        ?>
        <div class="message info" style="position:relative;">Your Invitation is expired. </div>
    <?php else: ?>
        <div class="registration box">
            <?php if ($site_id == 1) { ?>
                <div class="login_logo">
                    <img src="/img/sibme_logo2.png" alt="">
                </div>
            <?php } else { ?>
                <div class="login_logo">
                    <img src="/img/logo_2.svg" alt="">
                </div>
            <?php } ?>
            <header class="registration__header header">
                <h2>Let's get you started</h2>
                <p>Already have an account? <a href="<?php echo $this->base . '/Users/login/' . $credentials ?>">Sign in here</a></p>
            </header>
            <form method="post" id="frmActiviation" action="<?php echo $this->base . '/Users/account_settings/' . $credentials ?>"
                  enctype="multipart/form-data">
                <div class="row">
                    <div class = "span5 upload">
                        <div class = "upload_place">
                            <?php echo $this->Html->image('/img/camera-img.png', array('alt' => 'Logo', 'id' => 'preview_image')) ?>
                        </div>
                        <a href = "#" class = "upload-toggle upload_sibme" <?php if ($site_id != 1) { ?>style="background: #FCB814;"<?php } ?>>Upload a photo</a>
                        <input id= "account_image_logo" name = "myfile" type = "file" />
                    </div>
                </div>
                <!--<div class="input-group">
                    <input name="data[username]" class="text_input_larg" tabindex="0" value="" placeholder="Username"
                           style="margin-top: 10px;margin-bottom: 0px;" type="text" id="username" required
                        />
                </div>-->
                <!-- <div class="input-group">
                <?php
                //echo $this->Form->input('username', array(
                //'label' => FALSE,
                //'div' => FALSE,
                //'tabindex' => '1',
                //'class' => 'text_input_larg',
                //'placeholder' => 'Username',
                //'required' => 'required',
                //'value' => isset($user['User']['username']) ? $user['User']['username'] : ''
                //));
                ?>

                </div> -->
                <div class="input-group">
                    <?php
                    echo $this->Form->input('first_name', array(
                        'label' => FALSE,
                        'div' => FALSE,
                        'class' => 'first_name',
                        'tabindex' => '2',
                        'placeholder' => 'First Name',
                        'required' => 'required',
                        'value' => isset($user['User']['first_name']) ? $user['User']['first_name'] : ''
                    ));
                    ?>
                </div>
                <div class="input-group">
                    <?php
                    echo $this->Form->input('last_name', array(
                        'label' => FALSE,
                        'div' => FALSE,
                        'tabindex' => '3',
                        'class' => 'last_name',
                        'placeholder' => 'Last Name',
                        'required' => 'required',
                        'value' => isset($user['User']['last_name']) ? $user['User']['last_name'] : ''
                    ));
                    ?>

                </div>
                <div class="input-group">
                    <?php
                    echo $this->Form->input('email', array(
                        'label' => FALSE,
                        'class' => 'text_input_larg',
                        'div' => FALSE,
                        'tabindex' => '4',
                        'type' => 'text',
                        'value' => isset($user['User']['email']) ? $user['User']['email'] : '',
                        'placeholder' => 'Email',
                        'readonly' => 'readonly'
                    ));
                    ?>
                </div>
                <div class="input-group">
                    <?php
                    echo $this->Form->input('password', array(
                        'id' => 'password',
                        'label' => FALSE,
                        'class' => 'text_input_larg',
                        'div' => FALSE,
                        'tabindex' => '5',
                        'required' => 'required',
                        'type' => 'password',
                        'placeholder' => 'Choose a password',
                    ));
                    ?>
                </div>
                <div class="text-password"><span>minimum 8 characters</span></div>
                <div class="input-group">
                    <?php
                    echo $this->Form->input('password_confirmation', array(
                        'id' => 'password_confirmation',
                        'label' => FALSE,
                        'div' => FALSE,
                        'class' => 'text_input_larg',
                        'type' => 'password',
                        'tabindex' => '6',
                        'placeholder' => 'Confirm password'
                    ));
                    ?>
                    <input name="username" type="hidden" value="<?php echo isset($user['User']['email']) ? $user['User']['email'] : ''; ?>"/>
                    <input name="inv_account_id" type="hidden" value="<?php echo $account_id; ?>"/>
                    <input name="inv_auth_token" type="hidden" value="<?php echo $auth_token; ?>"/>
                    <input name="inv_user_id" type="hidden" value="<?php echo $user_id; ?>"/>
                </div>
                <hr>
                <button class="btn <?php if ($site_id == 1) { ?>btn-green<?php } else { ?>btn-grey<?php } ?>" type="submit">OK LET'S GO  <span class="arrow-size">&#8250;</span></button>
            </form>
        </div>
    </div>
<?php endif; ?>

<style>
    html, body {
        background: #f3f4f5;
    }


    .registration {
        float: none;
        width: 700px;
        margin: 0 auto;
        margin-top: 0px;
        background: #fff;
        box-shadow: 0px 0px 5px rgba(0,0,0,0.16);         padding: 23px 150px 52px 150px;
    }

    .header{text-align: center;
            background: none;
            border: 0;     margin-bottom: 5px;}
    .registration__header>h2 {    color: #0d3244;
                                  font-family: 'Roboto', sans-serif;
                                  font-size: 18px;

                                  margin: 0px;
                                  font-weight: 600;
                                  padding: 21px 0 6px 0;}
    .registration__header>p {font-family: 'Roboto', sans-serif;     color: #a7a7a7;}

    .registration__header>p>a {color: #524014;}

    .registration .input-group>input {    width: 100%;
                                          -webkit-box-sizing: border-box;
                                          -moz-box-sizing: border-box;
                                          box-sizing: border-box;
                                          color: #757575;
                                          box-sizing: border-box;
                                          border: solid 1px #d9d9d9;
                                          padding: 10px 8px;
                                          font-family: 'Roboto', sans-serif;
                                          font-weight: 500;
                                          font-size: 14px;
                                          outline: none;
                                          color: #acb3b7;
                                          height: 46px;
                                          box-shadow: 0 0px 4px rgba(0,0,0,0.09) inset;
    }

    .registration .btn-grey{background: #808184;
                            border-radius: 3px;
                            border: 0px;
                            color: #fff;
                            padding: 12px 0px;
                            cursor: pointer;
                            outline: none;
                            display: block;
                            width: 100%;
                            font-size: 16px;
                            font-family: 'Roboto', sans-serif;
                            box-shadow: 0 0px 0px rgba(0,0,0,0.13), 0 0px 0 rgba(255,255,255,0.2) inset;
    }


    .registration .btn-green{background: #28A745;
                             border-radius: 3px;
                             border: 0px;
                             color: #fff;
                             padding: 12px 0px;
                             cursor: pointer;
                             outline: none;
                             display: block;
                             width: 100%;
                             font-size: 16px;
                             font-family: 'Roboto', sans-serif;
                             box-shadow: 0 0px 0px rgba(0,0,0,0.13), 0 0px 0 rgba(255,255,255,0.2) inset;
    }



    .upload-preview {        padding: 0;
                             border: 1px solid #dadada;
                             width: 80px;
                             overflow: hidden;
                             height: 80px;
                             border-radius: 50%;
                             float: left;}

    .upload-preview img {
        height: 100px;
        width: auto;

    }

    .upload-toggle {    float: left;
                        background: #FCB814;
                        border-radius: 3px;
                        border: 0px;
                        color: #fff;
                        padding: 10px 8px;
                        cursor: pointer;
                        outline: none;
                        display: block;
                        width: 33%;
                        font-size: 14px;
                        font-family: 'Roboto', sans-serif;
                        margin-left: 15px;
                        position: relative;
                        top: 18px;}
    .upload-toggle:hover {    color: #fff;}
    .upload {    margin-bottom: 30px;}

    .login_logo {    text-align: center;}
    .login_logo img{        max-height: 95px;}
    .upload_place{float: left;}
    .upload_sibme{    background: #007AFF;}
    .text-password{
        text-align: right;
        margin-top: -20px;
        margin-bottom: 8px;
        font-size: 13px;
        opacity: 0.4;
        font-weight: bold;
    }
    .arrow-size{
        font-size: 26px;
        line-height: 14px;
        position: relative;
        top: 1px;
    }

    img#preview_image {
        width: 72px;
        height: 72px;
        border-radius: 42px;
    }

</style>

<script>
    $(".upload_sibme").click(function () {
        $("#account_image_logo").trigger("click");
    });
    var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
    function readURL(input) {

        if (input.files && input.files[0]) {
            var extension = input.files[0].name.split('.').pop().toLowerCase(), //file extension from input file
                    isSuccess = fileExtension.indexOf(extension) > -1;
            if (!isSuccess) {
                alert("Only formats allowed : " + fileExtension.join(', '));
                return;
            }
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview_image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#account_image_logo").change(function () {
        readURL(this);
    });


</script>
