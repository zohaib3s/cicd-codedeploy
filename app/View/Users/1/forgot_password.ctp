<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,700i" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

<style>
    div#hs-beacon {
        display: none;
    }
    .resetPass_container{
        width:700px;
        margin:0 auto;
        margin-top:70px;
    }
    .resetPass_box{
        background:#fff;
        box-shadow:0px 0px 5px rgba(0,0,0,0.16);
        text-align:center;
        padding:30px 0px;
        margin-bottom: 15px;
    }
    .resetPass_logo{
        padding: 15px 0px 10px 0px;
    }
    .resetPass_box h1{
        color: #0d3244;
        font-family: 'Roboto', sans-serif;
        font-size: 18px;

        margin: 0px;
        font-weight: 600;
        padding: 21px 0;
    }
    .resetPass_box p{
        color: #0d3244;
        font-family: 'Roboto', sans-serif;
        font-size: 15px;
        max-width: 365px;
        margin: auto;
        margin-top: 15px;
        margin-bottom: 40px;
        line-height: 22px;

    }
    .resetPass_box p a {text-decoration: none;}
    .resetPass_box h1 img{
        vertical-align:middle;
    }
    .resetPass_field_box{
        max-width: 365px;
        margin: 0 auto;
    }
    .resetPass_field_box input[type="text"], .resetPass_field_box input[type="password"]{
        width:365px;
        box-sizing:border-box;
        border:solid 2px #d9d9d9;
        padding:10px 5px;
        font-family: 'Roboto', sans-serif;
        font-weight:500;
        font-size:14px;
        outline:none;
        color:#acb3b7;     height: 46px;    box-shadow: 0 1px 2px rgba(0,0,0,0.09) inset; padding-left: 40px;
    }
    .resetLink_btn{
        background: #848688;
        font-size: 16px;
        border-radius: 3px;
        border: 0px;
        color: #fff;

        padding: 14px 0px;
        cursor: pointer;
        outline: none;
        display: block;
        width: 100%;
        font-family: 'Roboto', sans-serif;       margin: 28px 0;
    }
    .resetLink_btn:hover{
        background:#636465;
    }

    html, body {
        background: #f3f4f5;
    }
    .for_outer {    position: relative;
                    max-width: 365px;
                    margin: 0 auto;}
    .for_outer .far{color: #dcdbdb;

                    position: absolute;
                    top: 13px;
                    font-size: 22px;
                    left: 10px;}
    #main { margin-top: 0;}
    #flashMessage {
        width: 449px!important;
        text-align: center;
        position: relative;
        top: 46px;
        color: #32613c;
        background: #d3ecda;
        border-color: #d3ecda;
        margin: 0 auto;
        padding: 11px;
        border-radius: 3px;
        font-family: 'Roboto', sans-serif;        font-size: 14px;
    }
    #flashMessage .message .error{
        color: #32613c !important;
        background: #d3ecda !important;
        border-color: #d3ecda !important;
    }
</style>
<?php
$color = $this->Custom->get_site_settings('login_btn_color');
$description = $this->Custom->get_site_settings('rest_password_descritpion',$this->Custom->getPreferredLanguage());
$site_title = $this->Custom->get_site_settings('site_title');
$description = str_replace("{redirect_url}", '<a href="' . $this->base . '/users/login">' . $site_title . '</a>', $description);
?>
<div class="resetPass_container">
    <div class="resetPass_box">
        <div class="resetPass_logo">
            <img src="<?php echo $this->Custom->get_site_settings('login_logo'); ?>" width="195" alt=""/>
        </div>
        <h1><?php echo $this->Custom->get_site_settings('reset_password_text',$this->Custom->getPreferredLanguage()); ?></h1>
        <p>
            <?php echo $description; ?>
        </p>

        <div class="resetPass_field_box">
            <form accept-charset="UTF-8" action="<?php echo $this->base . '/Users/forgot_password' ?>" class="new_user" id="login-form" method="post">
                <div class="for_outer">
                    <i class="far fa-envelope"></i>
                    <input type="text" value="" placeholder="<?php echo $language_based_content['enter_email_fp_placeholder']; ?>" name="email" id="email">
                </div>
                <button class="resetLink_btn" style="background:<?php echo $color; ?>" type="submit"><?php echo $language_based_content['send_reset_link']; ?></button>
            </form>
        </div>
    </div>
</div>

<!--
<div class="forgot_password_box">
    <div class="header header--lite">
        <h4 class="header__title">
            Can't sign in? Forgot Password?
        </h4>
    </div>
    <form accept-charset="UTF-8" action="<?php echo $this->base . '/Users/forgot_password' ?>" class="new_user" id="login-form" method="post">
        <p>
            Enter your email address below and we'll send you password reset instructions.
        </p>
        <p class="input-group">
            <input type="text" value="" placeholder="Enter your email address" name="email" id="email" class="size-large">
            Never mind, <a href="<?php echo $this->base . '/users/login' ?>" class="blue-link">send me back to the sign-in screen</a>
        </p>
        <p class="input-group">
            <button class="btn btn-orange btn-medium full-input" type="submit">Send me reset instructions</button>
        </p>
    </form>
    <div class="text-center islet">
        <h4 style="text-align: left">Information about spam filters and junk mail</h4>
        <p style="text-align: left">If you don't get an email from Sibme within a few minutes, please be sure to check your spam filter or junk mail. The email will be sent from do-not-reply@sibme.com.</p>
    </div>
</div>
-->