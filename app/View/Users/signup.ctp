<?php
$trial_duration = Configure::read('trial_duration');
?>
<form accept-charset="UTF-8" action="<?php echo $this->base . '/users/signup' ?>" class="signup island compact" enctype="multipart/form-data" id="new_user" method="post" style="padding-bottom: 0px;">
    <div style="margin:0;padding:0;display:inline">
        <input name="utf8" type="hidden" value="&#x2713;" />
        <input name="authenticity_token" type="hidden" value="d5KI32DtOGU2o6duxIGhW0VRgTZkj+3UWAQjQHTsGJk=" />
    </div>
    <div class="error">
    </div>
    <h3 class="epsilon semi underline dotted">Free <?php print $trial_duration; ?> day trial.  No credit card required</h3>
    <div class="gw">
        <div class="g one-half">
            <?php echo $this->Form->input('first_name', array('label' => 'First Name*', 'tabindex' => '1', 'required' => 'required', 'title' => 'Please enter your first name')); ?>
            <?php echo $this->Form->error('Users.first_name'); ?>
            <?php echo $this->Form->input('email', array('label' => 'E-mail*', 'tabindex' => '3', 'type' => 'email', 'required', 'title' => 'Please enter valid email address')); ?>
            <?php echo $this->Form->error('Users.email'); ?>

            <?php echo $this->Form->input('company_name', array('label' => 'School/District, Organization, or Institution Name*', 'tabindex' => '5', '', 'required', 'title' => 'Please enter your Organization/Institute name')); ?>
            <?php echo $this->Form->error('Users.company_name'); ?>
            <?php echo $this->Form->input('password', array('label' => 'Password*', 'tabindex' => '7', 'type' => 'password', 'required', 'title' => 'Password must be at least 6 characters', 'minlength' => '6')); ?>
            <?php echo $this->Form->error('Users.password'); ?>
        </div>
        <div class="g one-half">
            <?php echo $this->Form->input('last_name', array('label' => 'Last Name*', 'tabindex' => '2', 'required', 'title' => 'Please enter your last name')); ?>
            <?php echo $this->Form->error('Users.last_name'); ?>
            <?php echo $this->Form->input('username', array('label' => 'Username*', 'tabindex' => '4', '', 'required', 'title' => 'Please enter your username')); ?>
            <?php echo $this->Form->error('Users.username'); ?>
            <?php echo $this->Form->input('phone', array('label' => 'Phone(Optional)', 'tabindex' => '6')); ?>

            <?php echo $this->Form->input('password_confirmation', array('label' => 'Confirm Password*', 'type' => 'password', 'tabindex' => '8', '', 'required', 'title' => 'Please confirm your password', 'minlength' => '6')); ?>
            <?php echo $this->Form->error('Users.password_confirmation'); ?>
        </div>

        <div class='req'>
            <label for='website'>Leave blank</label>
            <input type='text' name='website'>
        </div>


    </div>

    <div class="form-actions full-bleed" style="margin-top: 20px;">
        <input type="submit" class="fr btn btn-green btn-lrg btn-inv" value="Start your free trial">
        <p class="compact semi milli">By clicking “Start your free trial” I agree to Sibme's <a target="_blank" href="http://sibme.com/terms.html">Terms of Use</a>
            and <a target="_blank" href="http://sibme.com/privacy-policy.html">Privacy Policy</a>
        </p>
        <div style="clear:both"></div>

        <div style="float: right;">
            <a class="compact semi milli" href="<?php echo $this->base . '/users/login/'; ?>">Already <?php $this->Custom->get_site_settings('site_title') ?> User? Login here.</a>
        </div>

    </div>
</form>
<style>
    label.error{
        color: #be1616;
        font-size: 12px;
    }
</style>
<script type="text/javascript">

    $(document).ready(function () {
        /*$('#new_usersdfd').validate({
         errorPlacement: function(error, element) {

         }
         });*/
        $("#new_user").validate({
            errorLabelContainer: $("#form1 div.error")
        });


        $(".req").hide();





    });

    /*function PostSignup() {

     if ($('#new_user').valid()) {
     return true;
     }

     return false;

     }*/

</script>
