<style>
    .resetPass_container{
        width:650px;
        margin:0 auto;
        margin-top:70px;
    }
    .resetPass_box{
        background:#fff;
        box-shadow:0px 0px 5px rgba(0,0,0,0.16);
        text-align:center;
        padding:30px 0px;
    }
    .resetPass_logo{
        padding:0px 0px 10px 0px;
    }
    .resetPass_box h1{
        color:#0d3244;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        font-size:35px;
        font-weight:500;
        margin:0px;
    }
    .resetPass_box p{
        color:#0d3244;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        font-size:15px;
        font-weight:500;
    }
    .resetPass_box h1 img{
        vertical-align:middle;
    }

    .resetPass_field_box{
        margin:15px 0px;
    }
    .resetPass_field_box input[type="text"], .resetPass_field_box input[type="password"]{
        width:365px;
        box-sizing:border-box;
        border:solid 2px #d9d9d9;
        padding:10px 5px;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        font-weight:normal;
        font-size:14px;
        outline:none;
        color:#acb3b7;
    }
    .resetLink_btn{
        background:#5daf46;
        width:365px;
        border-radius:3px;
        border:0px;
        border-bottom:solid 2px #dbdbdb;
        color:#fff;
        font-size:20px;
        font-weight:500;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        padding:8px 0px;
        cursor:pointer;
        outline:none;
        margin-top:15px;
        text-shadow:0px 1px 0px #56a141;
    }
    .resetLink_btn:hover{
        background:#74d05b;
    }
    .resetLink_btn:active{
        border-bottom:solid 2px #cdcdcd;
    }


</style>

<div class="resetPass_container">
    <div class="resetPass_box">
        <div class="resetPass_logo">
            <img src="/app/img/sibme_logo.jpg" width="230" alt=""/>
        </div>
        <h1>Password Reset</h1>

        <p>
            To reset your password, enter the email address you use to <br />
            sign in to <a href="<?php echo $this->base . '/users/login' ?>" style="font-weight: 700;color: #333;">Sibme.</a>
        </p>

        <div class="resetPass_field_box">
            <form accept-charset="UTF-8" action="<?php echo $this->base . '/Users/forgot_password' ?>" class="new_user" id="login-form" method="post">
                <input type="text" value="" placeholder="Enter your email address" name="email" id="email" style="text-align:center;">
                <button class="resetLink_btn" type="submit">Send Me Reset Link</button>
            </form>
        </div>
    </div>
</div>

<!--
<div class="forgot_password_box">
    <div class="header header--lite">
        <h4 class="header__title">
            Can't sign in? Forgot Password?
        </h4>
    </div>
    <form accept-charset="UTF-8" action="<?php echo $this->base . '/Users/forgot_password' ?>" class="new_user" id="login-form" method="post">
        <p>
            Enter your email address below and we'll send you password reset instructions.
        </p>
        <p class="input-group">
            <input type="text" value="" placeholder="Enter your email address" name="email" id="email" class="size-large">
            Never mind, <a href="<?php echo $this->base . '/users/login' ?>" class="blue-link">send me back to the sign-in screen</a>
        </p>
        <p class="input-group">
            <button class="btn btn-orange btn-medium full-input" type="submit">Send me reset instructions</button>
        </p>
    </form>
    <div class="text-center islet">
        <h4 style="text-align: left">Information about spam filters and junk mail</h4>
        <p style="text-align: left">If you don't get an email from Sibme within a few minutes, please be sure to check your spam filter or junk mail. The email will be sent from do-not-reply@sibme.com.</p>
    </div>
</div>
-->