<style>
    .resetPass_container{
        width:650px;
        margin:0 auto;
        margin-top:70px;
    }
    .resetPass_box{
        background:#fff;
        box-shadow:0px 0px 5px rgba(0,0,0,0.16);
        text-align:center;
        padding:30px 0px;
    }
    .resetPass_logo{
        padding:0px 0px 10px 0px;
    }
    .resetPass_box h1{
        color:#0d3244;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        font-size:35px;
        font-weight:500;
        margin:0px;
    }
    .resetPass_box p{
        color:#0d3244;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        font-size:15px;
        font-weight:500;
    }
    .resetPass_box h1 img{
        vertical-align:middle;
    }
    .resetPass_field_box{
        margin:15px 0px;
    }
    .resetPass_field_box input[type="text"], .resetPass_field_box input[type="password"]{
        width:365px;
        box-sizing:border-box;
        border:solid 2px #d9d9d9;
        padding:10px 5px;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        font-weight:normal;
        font-size:14px;
        outline:none;
        color:#acb3b7;
    }
    .resetLink_btn{
        background:#5daf46;
        width:365px;
        border-radius:3px;
        border:0px;
        border-bottom:solid 2px #dbdbdb;
        color:#fff;
        font-size:20px;
        font-weight:500;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        padding:8px 0px;
        cursor:pointer;
        outline:none;
        margin-top:15px;
        text-shadow:0px 1px 0px #56a141;
    }
    .resetLink_btn:hover{
        background:#74d05b;
    }
    .resetLink_btn:active{
        border-bottom:solid 2px #cdcdcd;
    }


</style>
<?php
$template_title ='';
$template_contents ='';
if($template_id ==9){
    $template_title ="Sibme's Weekly Summary Report";
    $template_contents  = "You have successfully unsubscribe from Sibme's Weekly Summary Report";
}else{
    $template_title ="Sibme's Monthly Summary Report";
    $template_contents   = "You have successfully unsubscribe from Sibme's Monthly Summary Report";
}

?>

<div class="resetPass_container" style="width: 960px">
    <div class="resetPass_box">
        <div class="resetPass_logo">
            <img src="/app/img/sibme_logo.jpg" width="230" alt=""/>
        </div>
        <h1><?php echo $template_title; ?></h1>

        <p><?php echo $template_contents;?></p>
    </div>
</div>
