<style>
    .cl_invalid{
        border: 1px solid red !important;
    }
    label.error{
        color: #be1616;
        font-size: 12px;
    }
    .gw select{
        border: 1px solid #c5c5c5;
        border-radius: 3px;
        box-shadow: 0 1px 2px rgba(0,0,0,0.09) inset;
        font-size: 13px;
        font-weight: bold;
        color: #b2b2b2;
        padding: 8px 4px;
        width: 309px;
        height:37px;
        background:#fff;
    }

    .gw #amount{
        border: 1px solid #c5c5c5;
        border-radius: 3px;
        box-shadow: 0 1px 2px rgba(0,0,0,0.09) inset;
        font-size: 13px;
        font-weight: bold;
        color: #b2b2b2;
        padding: 8px 4px;
        width: 300px;
    }

    .gw #card_number{
        border: 1px solid #c5c5c5;
        border-radius: 3px;
        box-shadow: 0 1px 2px rgba(0,0,0,0.09) inset;
        font-size: 13px;
        font-weight: bold;
        color: #b2b2b2;
        padding: 8px 4px;
        width: 300px;
    }

</style>


<form accept-charset="UTF-8" action="<?php echo $this->base . '/Users/free_sibme_registration/' . $account_id.'?first_name='.$first_name.'&last_name='.$last_name.'&email='.$email.'&huddle_id='.$huddle_id; ?>" class="signup island compact" enctype="multipart/form-data" id="frmActiviation" method="post" style="padding-bottom: 0px;">
    <div style="margin:0;padding:0;display:inline">
        <input name="utf8" type="hidden" value="&#x2713;" />
        <input name="authenticity_token" type="hidden" value="d5KI32DtOGU2o6duxIGhW0VRgTZkj+3UWAQjQHTsGJk=" />
    </div>
    <div class="error">
    </div>

    <div class="gw">
        <div class="g one-half">
            <?php
            echo $this->Form->input('first_name', array(
                'label' => $language_based_content['first_name_payment'],
                'id' => 'name',
                'div' => FALSE,
                'tabindex' => '1',
                'class' => 'text_input_larg',
                'placeholder' => '',
                'required' => 'required',
                'readonly'=>true,
                'value' => $first_name,
                'title' => $language_based_content['enter_your_first_name']
            ));
            ?>
            <?php
            echo $this->Form->input('email', array(
                'label' => $language_based_content['email_payment'],
                'class' => 'text_input_larg',
                'id' => 'email',
                'div' => FALSE,
                'tabindex' => '3',
                'required' => 'required',
                'type' => 'email',
                // 'readonly'=>true,
                'value' => $email,
                'placeholder' => '',
                'title' => $language_based_content['please_enter_valid_email']
            ));
            ?>

            <?php echo $this->Form->input('password', array('label' => $language_based_content['password_payment'], 'tabindex' => '4', 'type' => 'password', 'required', 'title' => $language_based_content['password_must_atleast_6_characters'], 'minlength' => '6')); ?>
            <?php echo $this->Form->error('Users.password'); ?>        

        </div>
        <div class="g one-half">
            <?php
            echo $this->Form->input('last_name', array(
                'label' => $language_based_content['last_name_payment'],
                'div' => FALSE,
                'required' => 'required',
                'id' => 'last_name',
                'tabindex' => '2',
                'placeholder' => '',
                'class' => 'text_input_larg',
                'readonly'=>true,
                'value' => $last_name,
                'title' => $language_based_content['please_enter_last_name']
            ));
            ?>
           <?php
            echo $this->Form->input('huddle_id', array(
                'label' => 'Huddle ID',
                'id' => 'huddle_id',
                'div' => FALSE,
                'name' => 'huddle_id',
                'tabindex' => '5',
                'class' => 'text_input_larg',
                'placeholder' => '',
                'type' => 'text',
                //'required' => 'required',
                'readonly'=>true,
                'value' => $huddle_id,
                'title' => 'Please enter Huddle Id',
                'type' => 'text'
            ));
            ?>
            <?php echo $this->Form->input('password_confirmation', array('label' => $language_based_content['confirm_password_payment'], 'type' => 'password', 'tabindex' => '6', '', 'required', 'title' => $language_based_content['confirm_your_password'], 'minlength' => '6')); ?>
            <?php echo $this->Form->error('Users.password_confirmation'); ?>
            

        </div>
    </div>

    <div class="form-actions full-bleed" style="margin-top: 20px;background-color:white; ">

        <input onclick="var e=this;setTimeout(function(){e.disabled=true;},0);return true;" id="form_submit" type="submit" class="fr btn btn-green btn-lrg btn-inv" value="<?php echo $language_based_content['free_payment_register']; ?>">
        <p class="compact semi milli"><?php echo $language_based_content['by_clicking_free_register']; ?> <a target="_blank" href="http://sibme.com/terms.html"><?php echo $language_based_content['terms_of_use_payment']; ?></a> <?php echo $language_based_content['and']; ?> 
            <?php //echo $language_based_content['and_payment']; ?> <a target="_blank" href="http://sibme.com/privacy-policy.html"><?php echo $language_based_content['privacy_policy_payment']; ?></a>
        </p>
        <div style="clear:both"></div>


        <!-- <div class="span12"> 
            <div class="span2" style="margin-left: 0px;">
                <img alt="Braintree-gray" class="braintree" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/braintree-gray.png'); ?>" />
            </div>
            <div class="span6 braintree-p">
                <?php echo $language_based_content['our_payment_billing_payment']; ?>
                <br/>
                <?php echo $language_based_content['payment_credit_card_braintree'];  ?>
            </div>
            <div style="text-align:center;margin:20px 0px 0px 0px;"><img alt="Braintree-gray" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/cards.jpg'); ?>" /></div>
        </div> -->

    </div>

</form>


<script type="text/javascript">

    $(document).ready(function () {

        $("#frmActiviation").validate({
            errorLabelContainer: $("#form1 div.error"),
            rules: {
//                amount: {
//                    greaterThanZero: true,
//                    number: true,
//                    max: 500,
//                    min: <?php //echo isset($student_fees) ? $student_fees : '60';   ?>
//                },
                card_number: {
                    digits: true,
                },
                card_cvv: {
                    digits: true,
                }

            }
        });

    });
    jQuery.validator.addMethod("greaterThanZero", function (value, element) {
        return this.optional(element) || (parseFloat(value) > 0);
    }, "* Amount must be greater than zero");

    $(document).on("click", "#flashMessage", function () {
        $("#flashMessage").fadeOut(200);
    });

</script>



<?php

/**
 * States Dropdown
 *
 * @uses check_select
 * @param string $post, the one to make "selected"
 * @param string $type, by default it shows abbreviations. 'abbrev', 'name' or 'mixed'
 * @return string
 */
function StateDropdown($post = null, $type = 'abbrev') {
    $states = array(
        array('AK', 'Alaska'),
        array('AL', 'Alabama'),
        array('AR', 'Arkansas'),
        array('AZ', 'Arizona'),
        array('CA', 'California'),
        array('CO', 'Colorado'),
        array('CT', 'Connecticut'),
        array('DC', 'District of Columbia'),
        array('DE', 'Delaware'),
        array('FL', 'Florida'),
        array('GA', 'Georgia'),
        array('HI', 'Hawaii'),
        array('IA', 'Iowa'),
        array('ID', 'Idaho'),
        array('IL', 'Illinois'),
        array('IN', 'Indiana'),
        array('KS', 'Kansas'),
        array('KY', 'Kentucky'),
        array('LA', 'Louisiana'),
        array('MA', 'Massachusetts'),
        array('MD', 'Maryland'),
        array('ME', 'Maine'),
        array('MI', 'Michigan'),
        array('MN', 'Minnesota'),
        array('MO', 'Missouri'),
        array('MS', 'Mississippi'),
        array('MT', 'Montana'),
        array('NC', 'North Carolina'),
        array('ND', 'North Dakota'),
        array('NE', 'Nebraska'),
        array('NH', 'New Hampshire'),
        array('NJ', 'New Jersey'),
        array('NM', 'New Mexico'),
        array('NV', 'Nevada'),
        array('NY', 'New York'),
        array('OH', 'Ohio'),
        array('OK', 'Oklahoma'),
        array('OR', 'Oregon'),
        array('PA', 'Pennsylvania'),
        array('PR', 'Puerto Rico'),
        array('RI', 'Rhode Island'),
        array('SC', 'South Carolina'),
        array('SD', 'South Dakota'),
        array('TN', 'Tennessee'),
        array('TX', 'Texas'),
        array('UT', 'Utah'),
        array('VA', 'Virginia'),
        array('VT', 'Vermont'),
        array('WA', 'Washington'),
        array('WI', 'Wisconsin'),
        array('WV', 'West Virginia'),
        array('WY', 'Wyoming')
    );

    $options = '<option value=""></option>';

    foreach ($states as $state) {
        if ($type == 'abbrev') {
            $options .= '<option value="' . $state[0] . '" ' . check_select($post, $state[0], false) . ' >' . $state[0] . '</option>' . "\n";
        } elseif ($type == 'name') {
            $options .= '<option value="' . $state[1] . '" ' . check_select($post, $state[1], false) . ' >' . $state[1] . '</option>' . "\n";
        } elseif ($type == 'mixed') {
            $options .= '<option value="' . $state[0] . '" ' . check_select($post, $state[0], false) . ' >' . $state[1] . '</option>' . "\n";
        }
    }

    echo $options;
}

function check_select($i, $m, $e = true) {
    if ($i != null) {
        if ($i == $m) {
            $var = ' selected="selected" ';
        } else {
            $var = '';
        }
    } else {
        $var = '';
    }
    if (!$e) {
        return $var;
    } else {
        echo $var;
    }
}
