<?php $breadcrumb_language_based_content = $this->Custom->get_page_lang_based_content('BreadCrumb'); ?>
<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<div id="image_size_err"></div>

<div class="container box edit-user" style="margin: 0 auto !important;float: none;">
    <style>
        #cboxWrapper {
            /*display:none;*/
        }

        #cboxTopLeft,
        #cboxTopRight,
        #cboxBottomLeft,
        #cboxBottomRight,
        #cboxMiddleLeft,
        #cboxMiddleRight,
        #cboxTopCenter,
        #cboxBottomCenter,
        #cboxClose {
            display: none !important;
        }

        #cboxOverlay {
            background: rgba(0, 0, 0, 0.5);
        }

        #cboxContent {
            background: none;
        }

        #cboxLoadedContent {
            width: 574px;
            height: 633px;
            text-align: center;
            border-radius: 4px;
            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border: 1px solid #5d5d5d;

            -webkit-box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 1);
            box-shadow: 0px 0px 8px 0px rgba(0, 0, 0, 1);
        }

        #cboxLoadedContent a {
            position: relative;
            color: #68a5d6;
            text-align: center;
            top: 580px;
            text-decoration: underline;
        }

        .languagepicker {

            display: inline-block;
            padding: 0;
            height: 40px;
            overflow: hidden;
            transition: all .3s ease;
            vertical-align: top;
            float: left;
            border: 1px solid #b4b9bd;
        }

        .languagepicker:hover {
            /* don't forget the 1px border */
            height: 81px;
        }

        .languagepicker a {
            color: #000;
            text-decoration: none;
            font-weight: 400;
        }

        .languagepicker li {
            display: block;
            padding: 8px 15px 10px 15px;
            border-top: 1px solid #EEE;

        }

        .languagepicker a:first-child li {
            border: none;

        }

        .languagepicker li img {
            margin-right: 5px;
            position: relative;
            top: -1px;
        }

        .roundborders {
            border-radius: 3px;
        }

        .languagepicker li {
            display: block;
            padding: 4px 11px 4px 11px !important;

            border-top: 1px solid #ddd;
        }

        .languagepicker:hover {
            height: 58px !important;
        }

        .languagepicker {
            margin: 17px 0 0 0;
        }

        .languagepicker {
            height: 30px !important;
            background-image: linear-gradient(to bottom, rgba(255, 255, 255, 0.09) 0%, rgba(0, 0, 0, 0.09) 100%);
        }

        .tabsright_cls {
            border: 0 !important;
            border-radius: 0;
        }

        .tabsright_cls .huddle-span4 {
            border: 1px solid #cecece;
            border-radius: 3px;
        }
        .tabsright_cls .select {    width: 100%;
            margin-bottom: 11px;

            font-size: 13px;
            padding: 1px;}
    </style>
    <?php
    $user_current_account = $this->Session->read('user_current_account');

    $disable = '';
    if ($permissions == "not_allow") {
    $disable = 'disabled';
    }
    ?>
    <div class="header_user_s">
        <h1><?php echo $language_based_content['user_settings']; ?></h1>
        <div class="popover right">
            <div class="arrow"></div>
            <div class="popover-inner">
                <h3 class="popover-title"><?php echo $language_based_content['change_your_photo']; ?></h3>

                <div class="popover-content">
                    <p>
                    </p>
                    <form accept-charset="UTF-8"
                          action="<?php echo $this->base; ?>/Users/uploadImage/<?php echo $user['User']['id'] ?>"
                          class="edit_user" enctype="multipart/form-data" id="edit_user" method="post">
                        <div style="margin:0;padding:0;display:inline">
                            <input name="utf8" type="hidden" value="✓">
                            <input name="_method" type="hidden" value="put">
                            <input name="authenticity_token" type="hidden"
                                   value="AzISl4KNSvvnXZRln6mSOBmuQbonH2JDMopE6+l850g=">
                        </div>
                        <input id="user_image" name="data[image]" <?php echo $disable; ?> type="file" >
                        <p class="spadding-top">
                            <input <?php echo $disable; ?> type="submit" name="submit"
                            value="<?php echo $language_based_content['save_us']; ?>" class="btn btn-green btn-small">
                            <a href="#"
                               class="btn btn-transparent btn-small cancel"><?php echo $language_based_content['cancel_us']; ?></a>
                        </p>
                    </form>
                    <p></p>
                </div>
            </div>
        </div>
        <?php if (isset($user['User']['image']) && $user['User']['image'] != ''): ?>
        <?php
            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['User']['id'] . "/" .
        $user['User']['image'], $user['User']['image']);
        ?>
        <?php echo $this->Html->image($avatar_path, array('alt' => $user['User']['first_name'] . " " .
        $user['User']['last_name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '83', 'width' =>
        '83', 'align' => 'left')); ?>
        <?php else: ?>
        <img width="83" height="83"
             src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/home/photo-default.png'); ?>"
             class="photo photo inline" rel="image-uploader" alt="Photo-default">
        <?php endif; ?>
        <div class="inline vertical-top userpro">
            <h2 class="inline nomargin-vertical"><?php echo $user['User']['first_name'] . " " . $user['User']['last_name'] ?></h2>
            <span class="inline user-status">
                <?php
                //TODO : Show User Role here.
                //echo $user['roles']['role']
                ?>
            </span>

            <p class="margin_user">
                <span class="user-job"></span>
            </p>
            <p class="nomargin-top smargin-bottom">
            <div style="width: 20px; float: left;    line-height: 0;"><a rel="tooltip" class="icon-email blue-link"
                                                                         data-toggle="modal"
                                                                         data-target="#messageParticipant" href="#"
                                                                         data-original-title="<?php echo $user['User']['email']; ?>">
                &nbsp;</a></div>
            <div style="float: left; margin-top: 1px;    font-size: 12px;"><?php echo $user['User']['email']; ?></div>
            </p>
        </div>
        <?php
        $selected_lang = $this->Custom->get_selected_lang($user['User']['id']);
        $languages = array(
        0 => 'en',
        1 => 'es'
        );
        $list = array(
        'en' => '<a href="#en" class="lang-settings">
        <li class="lang_settings" lang-attr="en"><img src="http://i64.tinypic.com/fd60km.png"/>English</li>
    </a>',
        'es' => '<a href="#es" class="lang-settings">
        <li class="lang_settings" lang-attr="es"><img src="http://i68.tinypic.com/avo5ky.png"/>Español</li>
    </a>'
        );
        ?>
        <div class="right" style="margin-left: 13px;height: 21px;display: none;">
            <div class="success-alert"
                 style="display: none;color: green;position: absolute;top: 71px;font-size: 13px;"></div>
            <ul class="languagepicker roundborders">
                <?php for ($i = 0; $i <= count($languages); $i++): ?>
                <?php if ($languages[$i] == $selected_lang): ?>
                <?php
                        echo $list[$languages[$i]];
                        unset($languages[$i]);
                        ?>

                <?php endif; ?>
                <?php endfor; ?>
                <?php
                for ($i = 0; $i <= count($languages); $i++) {
                    echo $list[$languages[$i]];
                }
                ?>


            </ul>
        </div>
        <div class="action-buttons right">
            <?php if (($user_current_account['roles']['role_id'] == '100' || $user_current_account['roles']['role_id'] == '110') && false): ?>
            <a href="<?php echo $this->base . '/permissions/assign_user/' . $account_id . '/' . $user['User']['id']; ?>"
               class="btn icon-locked" rel="tooltip" data-original-title="<?php echo $language_based_content['permission_privileges_user_section']; ?>"></a>
            <?php endif; ?>
            <?php if ($user_current_account['roles']['role_id'] == '120' && $permissions == 'allow'): ?>
              <!--Commented this code on the behalf of ticket SW-2641 Commented by Hamid-->
             <!--<a href="<?php //echo $this->base . '/Users/deleteAccount/' . $user['User']['id'] . '/1' ?>"
               class="btn icon2-trash" data-confirm="<?php //echo $alert_messages['are_you_usre_delete_user_account']; ?>"
               data-method="delete" rel="tooltip nofollow" title="Remove"></a>-->
            <?php endif; ?>
            <?php if ($key): ?>
            <a class="btn" href="<?php echo $this->base . '/users/administrators_groups' ?>">Back to all Users</a>
            <?php endif; ?>
        </div>
        <div style="clear:both;"></div>
    </div>
    <?php //echo "<pre>"; print_r($user); echo "</pre>";?>
    <div class="span5">
        <h2><?php echo $language_based_content['personal_info']; ?></h2>
        <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/editUser/' . $user['User']['id'] ?>"
              class="form-horizontal" id="edit_user" method="post">
            <div style="margin:0;padding:0;display:inline">
                <input name="utf8" type="hidden" value="✓">
                <input name="_method" type="hidden" value="put">
                <input name="authenticity_token" type="hidden" value="AzISl4KNSvvnXZRln6mSOBmuQbonH2JDMopE6+l850g=">
            </div>
            <div class="controls">

            </div>

            <div class="input-group label-toggle">
                <input class="size-big" id="user_first_name" required name="user[first_name]" <?php echo $disable; ?>
                placeholder="<?php echo $language_based_content['fname_placeholder']; ?>" size="30" type="text"
                value="<?php echo $user['User']['first_name'] ?>">
                <label><?php echo $language_based_content['whats_your_fname']; ?></label>
            </div>

            <div class="input-group label-toggle">
                <input class="size-big" id="user_last_name" required name="user[last_name]" <?php echo $disable; ?>
                placeholder="<?php echo $language_based_content['lname_placeholder']; ?>" size="30" type="text"
                value="<?php echo $user['User']['last_name'] ?>">
                <label><?php echo $language_based_content['whats_your_lname']; ?></label>
            </div>

            <div class="input-group label-toggle">
                <input class="size-big" id="user_title" name="user[title]" <?php echo $disable; ?>
                placeholder="<?php echo $language_based_content['title_placeholder']; ?>" size="30"
                value="<?php echo $user['User']['title'] ?>" type="text">
                <label><?php echo $language_based_content['whats_your_title']; ?></label>
            </div>

            <div class="input-group label-toggle">
                <input class="size-big" id="user_email" required name="user[email]" <?php echo $disable; ?>
                placeholder="<?php echo $language_based_content['email_address_placeholder']; ?>" size="30" type="email"
                value="<?php echo $user['User']['email'] ?>">
            </div>

            <div class="input-group label-toggle">
                <input class="size-big" id="user_username" required name="user[username]" <?php echo $disable; ?>
                placeholder="<?php echo $language_based_content['username_placeholder']; ?>" size="30" type="text"
                value="<?php echo $user['User']['username'] ?>">
                <label><?php echo $language_based_content['enter_username_us']; ?></label>
            </div>


            <div class="input-group label-toggle">
                <input autocomplete="off" class="size-big" id="user_password"
                       name="user[password]" <?php echo $disable; ?>
                placeholder="<?php echo $language_based_content['new_password_placeholder']; ?>" size="30"
                type="password">
                <label><?php echo $language_based_content['enter_password_us']; ?></label>
            </div>

            <div class="input-group label-toggle">
                <input class="size-big" id="user_password_confirmation"
                       name="user[password_confirmation]" <?php echo $disable; ?>
                placeholder="<?php echo $language_based_content['confirm_password_placeholder']; ?>" size="30"
                type="password">
                <label><?php echo $language_based_content['enter_password_us']; ?></label>
            </div>
             <div class="input-group label-toggle">
                <input class="size-big" id="institution_id"  name="user[institution_id]" <?php echo $disable; ?> placeholder="Institution ID" size="30" type="text" value="<?php echo $user['User']['institution_id'] ?>">
                <label>Institution ID</label>
            </div>
            <?php /* ?>
            <div class="input select size-big">
                <?php echo $this->Form->input('time_zone', array('type' => 'select', 'options' => $this->App->timeZone,
                'label' => false, 'class' => 'size-big', $disable, 'div' => false, 'selected' =>
                htmlentities($user['User']['time_zone'])), array('id' => 'user_time_zone')); ?>
            </div>
            <?php */ ?>
            <div class="form-actions text-left lmargin-left lmargin-top">
                <button type="submit"
                        style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>"
                <?php echo $disable; ?> class="btn
                btn-green"><?php echo $language_based_content['save_changes_us']; ?></button>
                <a href="<?php echo $this->base . '/Users/editUser/' . $user['User']['id']; ?>"
                   class="btn btn-transparent"><?php echo $language_based_content['cancel_us']; ?></a>
            </div>
        </form>
    </div>
    <div class=" groups-table  span6 tabsright_cls">
        <?php if($site_id == '1'){ ?>
        <h2><?php echo $language_based_content['sibme_language_desktop_only']; ?></h2>
        <div class="input-group select ">
            <select class="language_switch" name="userLangugage" required="required">
                <option value="en" <?php if($user['User']['lang'] == 'en'){echo 'selected';} ?>>English</option>
                <option value="es" <?php if($user['User']['lang'] == 'es'){echo 'selected';} ?>>Spanish</option>
            </select>
        </div>
        <?php } ?>


        <?php
        $optOutIds = array();
        if (isset($optInIds) && count($optInIds) > 0) {
        foreach ($optInIds as $optout) {
        $optOutIds[] = $optout['EmailUnsubscribers']['email_format_id'];
        }
        }
        ?>
        <div class="span4 huddle-span4" style="margin-left:0px;">


            <div class="groups-table-header">
                <div style="clear:both"></div>
                <div class="select-all-none" style="float: left; margin-left: -8px; margin-top: 5px;width: 100%;">
                    <?php echo $language_based_content['unsub_from_email']; ?>

                    <?php if ($this->Session->read('totalAccounts') != 0 && $allAccounts && count($allAccounts) > 1 &&
                    $users['accounts']['is_suspended'] != 1): ?>
                    <div class="input select" style="float: right;margin-top: -6px;">
                        <select name="list_accounts" id="list_accounts" style="width: 145px;height: 32px;float: right;"
                                onchange="">
                            <!--<option value="all accounts">All Accounts</option>-->
                            <!--<option >Please Select Account</option>-->
                            <?php
                                $accountsArr = array();
                                foreach ($allAccounts as $accounts) {
                                    $accountsArr[] = $accounts['accounts']['account_id'];
                                    if (strlen($accounts['accounts']['company_name']) > 16) {
                            $title = mb_substr($accounts['accounts']['company_name'], 0, 14) . '...';
                            } else {
                            $title = $accounts['accounts']['company_name'];
                            }
                            ?>
                            <option title="<?php echo $accounts['accounts']['company_name'] ?>"
                                    value="<?php echo $accounts['accounts']['account_id']; ?>"
                            <?php
                                    if ($accounts['accounts']['account_id'] == $current_user['accounts']['account_id']) {
                                        echo selected;
                                    }
                                    ?>><?php echo $title; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <?php endif; ?>
                </div>
                <div style="clear:both"></div>
            </div>

            <div class="groups-table-content">
                <div class="widget-scrollable">
                    <div class="scrollbar" style="height:450px;">
                        <div class="track" style="height:450px;">
                            <div class="thumb" style="top: 0px; height:99px;">
                                <div class="end"></div>
                            </div>
                        </div>
                    </div>

                    <div class="viewport short Unsubscribe_email_cls">
                        <div class="overview" style="top: 0px;">
                            <div id="people-lists">
                                <div id="account_email_setting"></div>
                                <ul id="list-containers">
                                    <?php if ($emailFormat): ?>
                                    <?php
                                        foreach ($emailFormat as $row) :
                                            if ($user_current_account['roles']['role_id'] == '120' || $user_current_account['roles']['role_id'] == '125'):
                                                if ($row['EmailFormats']['template_name'] == 'monthly_report') {
                                                    continue;
                                                }
                                            endif;
                                            ?>
                                    <li>
                                        <label id="email-format-id-<?php echo $row['EmailFormats']['id'] ?>"
                                               for="email-format-<?php echo $row['EmailFormats']['id'] ?>"><input
                                                type="checkbox" <?php echo ((isset($optOutIds) && is_array($optOutIds) && count($optOutIds) >
                                            0 && in_array($row['EmailFormats']['id'], $optOutIds) ) ? '' :
                                            'checked="checked"') ?>
                                            id="email-format-<?php echo $row['EmailFormats']['id'] ?>"
                                            name="super_admin_ids[]" value="<?php echo $row['EmailFormats']['id'] ?>"
                                            class="member-user"> <?php echo $language_based_content['full_name_'.$row['EmailFormats']['id']] ?>
                                        </label>
                                        <div class="permissions">
                                            <label id="template-idz-<?php echo $row['EmailFormats']['id'] ?>"
                                                   template-id="<?php echo $row['EmailFormats']['id'] ?>"
                                                   for="subscrib-template-<?php echo $row['EmailFormats']['id'] ?>">
                                                <?php echo ((isset($optOutIds) && is_array($optOutIds) && count($optOutIds) >
                                                0 && in_array($row['EmailFormats']['id'], $optOutIds) ) ? '<span
                                                    id="template-lable-' . $row['EmailFormats']['id'] . '">'.$language_based_content['unsubscribed_us'].'</span>'
                                                : '<span id="template-lable-' . $row['EmailFormats']['id'] . '">'. $language_based_content['subscribed_us'].' </span>')
                                                ?>
                                            </label>
                                        </div>
                                    </li>
                                    <script type="text/javascript">
                                        $(document).ready(function (e) {
                                            $('#email-format-id-<?php echo $row['EmailFormats']['id']; ?>').click(function (e) {
                                                $template_id = '<?php echo $row['EmailFormats']['id'] ?>';
                                                var list_accounts = $('#list_accounts').val();

                                                var account_ids = '';
                                                var countAccounts = '<?php echo count($allAccounts); ?>';
                                                if (countAccounts > 1) {
                                                    if (list_accounts == 'all accounts') {
                                                        account_ids = '<?php echo json_encode($accountsArr); ?>';
                                                        type = 'all';
                                                    } else {
                                                        account_ids = list_accounts;
                                                        type = 'single';
                                                    }
                                                } else {
                                                    account_ids = '<?php echo $current_user['accounts']['account_id']; ?>';
                                                    type = 'current account';
                                                }
                                                if ($('#email-format-<?php echo $row['EmailFormats']['id'] ?>').is(':checked') == 0)
                                                {
                                                    $.ajax({
                                                        dataType: 'json',
                                                        type: 'POST',
                                                        url: home_url + '/users/subscribe',
                                                        data: {
                                                            template_id: $template_id,
                                                            list_accounts: account_ids,
                                                            type: type
                                                        },
                                                        success: function (data) {
                                                            if (data.status == true) {
                                                                $('#email-format-<?php echo $row['EmailFormats']['id'] ?>').prop('checked', false);
                                                                $('#template-lable-<?php echo $row['EmailFormats']['id'] ?>').html('<?php echo $language_based_content['unsubscribed_us']; ?>');
                                                            } else {
                                                                return false;
                                                            }
                                                        }
                                                    });
                                                }
                                            else
                                                {
                                                    $.ajax({
                                                        dataType: 'json',
                                                        type: 'POST',
                                                        url: home_url + '/users/unsubscribe',
                                                        //data: {template_id: $template_id},
                                                        data: {
                                                            template_id: $template_id,
                                                            list_accounts: account_ids,
                                                            type: type
                                                        },
                                                        success: function (data, status) {
                                                            if (data.status == true) {
                                                                $('#email-format-<?php echo $row['EmailFormats']['id'] ?>').prop('checked', true);
                                                                $('#template-lable-<?php echo $row['EmailFormats']['id'] ?>').html('<?php echo $language_based_content['subscribed_us']; ?>');
                                                            } else {
                                                                return false;
                                                            }
                                                        }
                                                    });
                                                }

                                            })
                                        })
                                    </script>
                                    <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <script type="text/css">

    </script>

</div>
<div id="messageParticipant" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="header">
                <h4 class="header-title nomargin-vertical smargin-bottom">Message
                    To <?php echo $user['User']['first_name'] . " " . $user['User']['last_name'] ?></h4>
                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
            </div>
            <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/send_user_messages' ?>"
                  class="new_message" enctype="multipart/form-data" id="new_message" method="post">
                <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;"/></div>
                <div class="input-group mmargin-top">
                    <div id="editor<?php echo $user['User']['id']; ?>-toolbar" class="editor-toolbar"
                         style="display: none;">
                        <a data-wysihtml5-command="bold">bold</a>
                        <a data-wysihtml5-command="italic">italic</a>
                        <a data-wysihtml5-command="insertOrderedList">ol</a>
                        <a data-wysihtml5-command="insertUnorderedList">ul</a>
                        <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
                    </div>
                    <?php echo $this->Form->textarea('message', array('id' => 'editor-' . $user['User']['id'],
                    'required' => 'required', 'class' => 'editor-textarea', 'cols' => '65', 'placeholder' => 'Message to
                    User(s) participating in Huddle...(Optional)')); ?>
                </div>

                <?php echo $this->Form->input('sender_id', array('type' => 'hidden', 'value' =>
                $user_current_account['User']['id'])); ?>
                <?php echo $this->Form->input('recipient_id', array('type' => 'hidden', 'value' =>
                $user['User']['id'])); ?>
                <?php echo $this->Form->input('message_type', array('type' => 'hidden', 'value' => '4')); ?>
                <p>
                    <button class="btn btn-blue inline" type="submit">Send</button>

                </p>
            </form>
        </div>
    </div>
</div>

<script>
    $("#user_image").change(function () {
        var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("<?php echo $alert_messages['only_format_allowed']; ?> : " + fileExtension.join(', '));
            $(this).val('');

        }
    });
    $('.lang_settings').on('click', function (e) {
        e.preventDefault();
        var lang_settings = $(this).attr('lang-attr');
        $.ajax({
            type: "POST",
            url: "<?php echo $this->base . '/users/addLangSettings' ?>",
            data: {lang_id: lang_settings},
            dataType: 'json',
            success: function (response) {
                $('.success-alert').text('Settings saved!');
                $('.success-alert').fadeIn('slow', function () {
                    $('.success-alert').delay(2000).fadeOut();
                });
            }
        });
    })


    $(document).ready(function () {

        $('form,input,select,textarea').attr("autocomplete", "off");

    });

    $('#user_image').bind('change', function () {

        //this.files[0].size gets the size of your file.
        var bytes = this.files[0].size;
        var file_size = (bytes / 1048576).toFixed(3);
        if (file_size > 2) {
            document.getElementById("user_image").value = "";
            $('#image_size_err').html('<div id="flashMessage" class="message error" style="cursor: pointer;">Please upload file max size 2MB</div>');
        } else {
            $('#image_size_err').html('');
        }

    });

    $('#list_accounts').bind('change', function () {

        var account_id = $('#list_accounts').val();

        var data = {
            account_id: account_id,
        };
        $("#account_email_setting").animate({opacity: 0.4}, 100);

        $.ajax({
            type: "POST",
            url: "<?php echo $this->base . '/users/getAccountSubscribers' ?>",
            data: data,
            success: function (response) {
                console.log(response);
                $("#account_email_setting").html(' ');
                $('#list-containers').remove();
                $("#account_email_setting").html(response);
                $("#account_email_setting").animate({opacity: 1}, 1000);

            }
        });
    });

    function test(obj) {
        var id = obj.id;

        var last = id.split('-');
        var template_id = last[last.length - 1];

        var list_accounts = $('#list_accounts').val();

        var account_ids = '';
        var countAccounts = '<?php echo count($allAccounts); ?>';
        if (countAccounts > 1) {
            if (list_accounts == 'all accounts') {
                account_ids = '<?php echo json_encode($accountsArr); ?>';
                type = 'all';
            } else {
                account_ids = list_accounts;
                type = 'single';
            }
        } else {
            account_ids = '<?php echo $current_user['
            accounts
            ']['
            account_id
            ']; ?>';
            type = 'current account';
        }

        $("#account_email_setting").animate({opacity: 0.4}, 100);

        if ($("#" + id).prop('checked') == false) {
            //alert('false');
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: home_url + '/users/subscribe',
                data: {template_id: template_id, list_accounts: account_ids, type: type},
                success: function (data) {
                    if (data.status == true) {
                        $('#' + id).prop('checked', false);
                        $('#template-lable-' + template_id).html('<?php echo $language_based_content['unsubscribed_us']; ?>');
                        $("#account_email_setting").animate({opacity: 1}, 1000);
                    } else {
                        return false;
                    }
                }
            });
        } else {
            //alert('true');
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: home_url + '/users/unsubscribe',
                //data: {template_id: $template_id},
                data: {template_id: template_id, list_accounts: account_ids, type: type},
                success: function (data, status) {
                    if (data.status == true) {
                        $('#' + id).prop('checked', true);
                        $('#template-lable-' + template_id).html('<?php echo $language_based_content['subscribed_us']; ?>');
                        $("#account_email_setting").animate({opacity: 1}, 1000);
                    } else {
                        return false;
                    }
                }
            });
        }
    }

</script>

<script>
    $(document).ready(function (e) {

        var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><span><?php echo $breadcrumb_language_based_content['user_setting_breadcrumb']; ?></span></div>';

        $('.breadCrum').html(bread_crumb_data);
        $(".languagepicker li").click(function (e) {
            if ($(".languagepicker li")[0].innerText != e.target.innerText) {
                var i = $(".languagepicker li").index(this);
                var selector = $(".languagepicker li");
                if (i > 0) {
                    swapNodes(selector[i], selector[0]);

                }

            }

        });

        function swapNodes(a, b) {
            var aparent = a.parentNode;
            var asibling = a.nextSibling === b ? a : a.nextSibling;
            b.parentNode.insertBefore(a, b);
            aparent.insertBefore(b, asibling);
        }
        
        
        $(".language_switch").change(function () {
       
            var lang = this.value;
            
            $.ajax({url: home_url + '/Users/update_user_email_language/<?php echo $user['User']['id'];?>',
                    data: {lang: lang},
                    type: 'post',
                    dataType: 'json',
                    success: function (response) {
                        window.location.reload(true);
                    }
                });

            });
        
        

    });
    
    

 
    
    
</script>

