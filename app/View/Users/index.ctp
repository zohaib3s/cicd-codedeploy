<?php
$btn_1_color = $this->Custom->get_site_settings('primary_bg_color');
$btn_2_color = $this->Custom->get_site_settings('secondary_bg_color');
?>
<?php if ($this->Session->read('role') && $this->Session->read('role') == 'admin'): ?>
    <div class="message warning">
        <div>You don't have permission</div>
    </div>
<?php else: ?>
    <p style="font-size: 20px;"></p>
    <div class="row relative box administrators_group">

        <h1>All Users</h1>
        <div class="rel" id="owners-container">
            <?php echo $accountOwner; ?>
        </div>
        <div class="rel" id="super-admins-container">
            <?php echo $supperAdmins; ?>
            <div class="clear" style="clear: both;"></div>
        </div>
        <div class="rel" id="admin-container">
            <?php echo $admins; ?>
            <div class="clear" style="clear: both;"></div>
        </div>
        <div class="rel">
            <?php echo $groups; ?>
        </div>
    </div>

    <div id="addSuperAdminModal" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="header">
                    <h4 class="header-title nomargin-vertical smargin-bottom"><img alt="Admin" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/admin.png'); ?>" /> New Super Admin</h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                </div>
                <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/administrators_groups' ?>" enctype="multipart/form-data" method="post" name="super_admin_form" class="people-form"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                    <div class="way-form">
                        <h3><span>Step 1:</span> Add name and email</h3>
                        <ol class="autoadd autoadd-sfont fmargin-list-left">
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name_1" name="users[][name]" required placeholder="Full Name" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email_1" name="users[][email]" required placeholder="Email Address" type="email" value="" /></label>
                                <a href="#" class="close">×</a>

                            </li>
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name_2" name="users[][name]" placeholder="Full Name" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email_2" name="users[][email]" placeholder="Email Address" type="email" value="" /></label>
                                <a href="#" class="close">×</a>

                            </li>
                        </ol>

                        <h3 class="way-form-divider"><span>Step 2:</span> Send message to Super Admin(s)</h3>
                        <textarea class="lmargin-bottom fmargin-left" cols="50"  id="message_1" name="message" placeholder="Message(Optional)" rows="4" style="width:450px;"></textarea>
                        <input id="controller_source_1" name="user_type" type="hidden" value="110" /><br/><br/>
                        <button class="btn btn-green fmargin-left" style="background-color: <?php echo $btn_1_color; ?>; border-color: <?php echo $btn_1_color ?>;" type="submit">Send Invitation</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="addAdminModal"  class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="header">
                    <h4 class="header-title nomargin-vertical smargin-bottom"><img alt="Admin" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/admin.png'); ?>" /> New User</h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                </div>
                <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/administrators_groups' ?>" enctype="multipart/form-data" method="post" name="admin_form" class="people-form"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                    <div class="way-form">
                        <h3><span>Step 1:</span> Add name and email</h3>
                        <ol class="autoadd autoadd-sfont fmargin-list-left">
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name_3" name="users[][name]" required placeholder="Full Name" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email_3" name="users[][email]" required placeholder="Email Address" type="email" value="" /></label>
                                <a href="#" class="close">×</a>
                            </li>
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name_4" name="users[][name]" placeholder="Full Name" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email_4" name="users[][email]" placeholder="Email Address" type="email" value="" /></label>
                                <a href="#" class="close">×</a>

                            </li>
                        </ol>

                        <h3 class="way-form-divider"><span>Step 2:</span> Send message to User(s)</h3>
                        <textarea class="lmargin-bottom fmargin-left" cols="50" id="message_2" name="message" placeholder="Message(Optional)" rows="4" style="width:450px;"></textarea>
                        <input id="controller_source_2" name="user_type" type="hidden" value="120" />
                        <button style="background-color: <?php echo $btn_1_color; ?>; border-color: <?php echo $btn_1_color ?>;" class="btn btn-green fmargin-left" type="submit">Send Invitation</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="addGroupModal"  class="modal" role="dialog">

        <div class="modal-dialog">
            <div class="modal-content">

                <script type="text/javascript">
                    function validateForm_2() {
                        var x = document.forms["new_group"]["name"].value
                        if (x == null || x == "") {
                            alert("Name must be filled");
                            return false;
                        }

                    }
                </script>

                <div class="header">
                    <h4 class="nomargin-top nomargin-bottom">Create group</h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                </div>

                <hr class="full mmargin-top" style="margin-left: 0px;margin-right: 0px;">

                <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/administrators_groups' ?>" class="form-horizontal" enctype="multipart/form-data" id="new_group" method="post" name="new_group"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                    <div class="input-group">
                        <input class="full-input" id="group_name" name="group[name]" required placeholder="Group Name" size="30" type="text" />
                    </div>
                    <p class="small no-margin-bottom">Add People into the Group</p>

                    <hr class="no-margin">

                    <div class="row-fluid">
                        <div class="span4">
                            <h3><input type="checkbox" id="account-owner-toggle"> Account Owner</h3>
                            <div id="account-owner">
                                <?php if ($account_owner): ?>
                                    <p>
                                        <label>
                                            <input id="account_owner_ids_" name="super_admin_ids[]" type="checkbox" value="<?php echo $account_owner['User']['id'] ?>" /><?php echo $account_owner['User']['first_name'] . ' ' . $account_owner['User']['last_name']; ?>
                                        </label>
                                    </p>

                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="span4">
                            <h3><input type="checkbox" id="susers-toggle"> Super Admins</h3>
                            <div id="susers">
                                <?php if ($superAdmins): ?>
                                    <?php foreach ($superAdmins as $row):
                                        ?>
                                        <p>
                                            <label>
                                                <input id="super_admin_ids_<?php echo $row['User']['id'] ?>" name="super_admin_ids[]" type="checkbox" value="<?php echo $row['User']['id'] ?>" /><?php echo $row['User']['first_name'] . ' ' . $row['User']['last_name']; ?>
                                            </label>
                                        </p>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="span4">
                            <h3><input type="checkbox" id="users-toggle"> Users</h3>
                            <div id="users">
                                <?php if ($adminUsers): ?>
                                    <?php foreach ($adminUsers as $row):
                                        ?>
                                        <p>
                                            <label>
                                                <input id="super_admin_ids_<?php echo $row['User']['id'] ?>" name="super_admin_ids[]" type="checkbox" value="<?php echo $row['User']['id'] ?>" /><?php echo $row['User']['first_name'] . ' ' . $row['User']['last_name']; ?>
                                            </label>
                                        </p>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <script>
                            $(document).ready(function () {
                                /*//check or uncheck all*/
                                document.new_group.reset(); /*//prevents FF from restoring values after refresh*/

                                $("#account-owner-toggle").click(function () {
                                    isChecked = $(this).prop("checked");
                                    if (isChecked) {
                                        $("#account-owner input[type='checkbox']").prop("checked", true)
                                    } else {
                                        $("#account-owner input[type='checkbox']").prop("checked", false)
                                    }
                                });
                                $("#susers-toggle").click(function () {
                                    isChecked = $(this).prop("checked");
                                    if (isChecked) {
                                        $("#susers input[type='checkbox']").each(function () {
                                            $(this).prop("checked", true);
                                        })
                                    } else {
                                        $("#susers input[type='checkbox']").each(function () {
                                            $(this).prop("checked", false);
                                        })
                                    }
                                })

                                $("#users-toggle").click(function () {
                                    isChecked = $(this).prop("checked");
                                    if (isChecked) {
                                        $("#users input[type='checkbox']").each(function () {
                                            $(this).prop("checked", true);
                                        })
                                    } else {
                                        $("#users input[type='checkbox']").each(function () {
                                            $(this).prop("checked", false);
                                        })
                                    }
                                })

                            });
                        </script>
                    </div>
                    <hr>
                    <p class="right" style="text-align: right;float: none;">
                        <input id="controller_source_3" name="user_type" type="hidden" value="groups" />
                        <button style="background-color:<?php echo $btn_1_color ?>; border-color:<?php echo $btn_1_color; ?>;" class="btn btn-green" type="submit">Create</button>
                        <a class="btn btn-transparent close-reveal" data-dismiss="modal">Cancel</a>
                    </p>
                </form>

            </div>
        </div>
    </div>
    <a href="#" id="view_browser_notification" style="display: none;" data-toggle="modal" data-target="BrowserMessageModal">browser message</a>
    <div id="BrowserMessageModal" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="header">
                    <h4 class="header-title nomargin-vertical smargin-bottom">Upgade Your Browser</h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                </div>

                <p>You're Using an unsupported browser. Please upgrade your browser to view <?php echo $this->Custom->get_site_settings('site_title') ?>Application</p>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {
            function loadData(page, role) {
                $.ajax({
                    type: "POST",
                    url: "<?php echo $this->base . '/users/getAjaxUsers' ?>",
                    data: {page: page, role: role},
                    success: function (msg) {
                        if (role == 110) {
                            $("#super-admins-container").html(msg);
                        } else if (role == 120) {
                            $("#admin-container").html(msg);
                        }
                    }
                });
            }

            loadData(1, 110);  /*// For first time page load default results*/
            loadData(1, 120);  /*// For first time page load default results*/
            $(document).on('click', '#super-admins-container .pagination li.active', function () {
                var page = $(this).attr('p');
                loadData(page, 110);
            });
            $(document).on('click', '#admin-container .pagination li.active', function () {
                var page = $(this).attr('p');
                loadData(page, 120);
            });

        });
    </script>

<?php endif; ?>
