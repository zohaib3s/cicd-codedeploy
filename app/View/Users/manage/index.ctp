<?php $breadcrumb_language_based_content = $this->Custom->get_page_lang_based_content('BreadCrumb'); ?>
<?php
$users = $this->Session->read('user_current_account');
$btn_1_color = $this->Custom->get_site_settings('primary_bg_color');
$btn_2_color = $this->Custom->get_site_settings('secondary_bg_color');

if ($this->Session->read('role') && $this->Session->read('role') == 'admin'):
    ?>
    <div class="message warning">
        <div>You don't have permission</div>
    </div>
<?php else: ?>
    <?php $user_current_account = $this->Session->read('user_current_account'); ?>
    <style>
        .row-fluid .span4{
            max-height: 60px;
        }
        .groupd-cls .span4{
            max-height: initial !important;
        }
        .glow3 img{
            box-shadow:0 0 5px 1px rgb(218, 9, 58);
        }

    </style>
    <p style="font-size: 20px;"></p>
    <div class="relative box administrators_group">
        <div class="header header-huddle people_topcls">

            <h2 class="title"><?php echo $language_based_content['people_people_section']; ?> <span class="blue_count">0</span></h2>
        </div>
        <div class="rel" id="owners-container">
            <?php echo $accountOwner; ?>
        </div>
        <div class="rel" id="super-admins-container">
            <div rel="super-admins" class="rel">
                <?php if ($users['roles']['role_id'] == '100' || $users['roles']['role_id'] == '110'): ?>
                    <a class="btn btn-green right" style="background-color:<?php echo $btn_1_color ?>; border-color:<?php echo $btn_1_color; ?>;" data-toggle="modal" data-target="#addSuperAdminModal" href="#"><span class="plus">+</span> <?php echo $language_based_content['add_super_admin_people_section']; ?></a>
                <?php endif; ?>
                <h2 class="inline"><?php echo $language_based_content['super_admins_people_section']; ?> <span id="super-user-count"></span></h2>
                <a onMouseOut="hide_sidebar()" class="appendix appendix--info" href="#"></a>

                <div class="appendix-content down">
                    <h3><?php echo $language_based_content['info_people_section']; ?></h3>
                    <p><?php echo $language_based_content['super_admin_info_people_section'].$language_based_content['super_admin_info_people_section_1']; ?></p>
                </div>
                <div style="padding: 0px; float: right; width: 298px; margin-right: 20px;" class="search-box">
                    <div id="super-user-container" class="filterform">
                        <div class="filterform">
                            <input class="permissions-btn-search" id="superuser-filter-btn" type="button" value="">
                            <input id="superuser-filter-input" placeholder="<?php echo $language_based_content['search_super_admins_people_section']; ?>" class="permissions-text-input" type="text" style="width: 233px; float: right;padding: 0px;border-left: 0;">
                            <div id="superuser-filter-cancel" type="text" class="filterinput" style="width: 10px; position: absolute; right: 213px; margin-top: 7px; cursor: pointer;display:none;">X</div>
                        </div>
                    </div>
                </div>

                <div id="super-user-list-containers"></div>

                <div style="clear: both;"></div>
            </div>
            <div class="clear" style="clear: both;"></div>
        </div>

        <div class="rel" id="new-admin-container">
            <div rel="super-admins" class="rel">
                <?php if ($users['roles']['role_id'] == '100' || $users['roles']['role_id'] == '110'): ?>
                    <a class="btn btn-green right" style="background-color:<?php echo $btn_1_color ?>; border-color:<?php echo $btn_1_color; ?>;" data-toggle="modal" data-target="#addNewAdminModal" href="#"><span class="plus">+</span> <?php echo $language_based_content['add_admin_people_section']; ?></a>
                <?php endif; ?>
                <h2 class="inline"><?php echo $language_based_content['admins_people_section']; ?> <span id="new-admin-count"></span></h2>
                <a onMouseOut="hide_sidebar()" class="appendix appendix--info" href="#"></a>

                <div class="appendix-content down">
                    <h3><?php echo $language_based_content['info_people_section']; ?></h3>
                    <p><?php echo $language_based_content['admin_info_people_section']; ?></p>
                </div>
                <div style="padding: 0px; float: right; width: 298px; margin-right: 20px;" class="search-box">
                    <div id="new-admin-filterform-container" class="filterform">
                        <div class="filterform">
                            <input class="permissions-btn-search" id="newadmin-filter-btn" type="button" value="">
                            <input id="newadmin-filter-input" placeholder="<?php echo $language_based_content['search_admins_people_section']; ?>" class="permissions-text-input" type="text" style="width: 233px; float: right;padding: 0px;border-left: 0;">
                            <div id="newadmin-filter-cancel" type="text" class="filterinput" style="width: 10px; position: absolute; right: 213px; margin-top: 7px; cursor: pointer;display:none;">X</div>
                        </div>
                    </div>
                </div>

                <div id="new-admin-list-containers"></div>

                <div style="clear: both;"></div>
            </div>
            <div class="clear" style="clear: both;"></div>
        </div>


        <div class="rel" id="admin-container">
            <div class="rel" rel="admins">
                <?php if ($users['roles']['role_id'] == '100' || $users['roles']['role_id'] == '110' || $users['roles']['role_id'] == '120' || $users['roles']['role_id'] == '115'): ?>
                    <a href="#" data-toggle="modal" style="background-color:<?php echo $btn_1_color ?>; border-color:<?php echo $btn_1_color; ?>;" data-target="#addAdminModal" class="btn btn-green right"><span class="plus">+</span> <?php echo $language_based_content['add_user_people_section']; ?></a>
                <?php endif; ?>
                <h2 class="inline"><?php echo $language_based_content['users_people_section']; ?> <span id="user-count"></span></h2>
                <a onMouseOut="hide_sidebar()" href="#" class="appendix appendix--info"></a>

                <div class="appendix-content down">
                    <h3><?php echo $language_based_content['info_people_section']; ?></h3>
                    <p><?php echo $language_based_content['users_info_people_section']; ?></p>
                </div>

                <div style="padding: 0px; float: right; width: 298px; margin-right: 20px;" class="search-box">
                    <div  id="admin-user-container" class="filterform">
                        <div class="filterform">
                            <input class="permissions-btn-search" id="user-filter-btn" type="button" value="">
                            <input id="user-filter-input" placeholder="<?php echo $language_based_content['search_users_people_section']; ?>" class="permissions-text-input" type="text" style="width: 233px; float: right;height: 26px;padding: 0px;border-left: 0;" />
                            <div id="user-filter-cancel" type="text" class="filterinput" style="width: 10px; position: absolute; right: 151px; margin-top: 7px; cursor: pointer; display: none;">X</div>
                        </div>
                    </div>
                </div>

                <div id="admin-user-list-containers"></div>

                <div style="clear: both;"></div>
            </div>
            <div class="clear" style="clear: both;"></div>
        </div>

        <div class="rel" id="viewer-container">
            <div rel="super-admins" class="rel">
                <?php if ($users['roles']['role_id'] == '100' || $users['roles']['role_id'] == '110' || $users['roles']['role_id'] == '115'): ?>
                    <a class="btn btn-green right" style="background-color:<?php echo $btn_1_color ?>; border-color:<?php echo $btn_1_color; ?>;" data-toggle="modal" data-target="#addViewerModal" href="#"><span class="plus">+</span> <?php echo $language_based_content['add_viewer_people_section']; ?></a>
                <?php endif; ?>
                <h2 class="inline"><?php echo $language_based_content['viewers_people_section']; ?> <span id="viewer-count"></span></h2>
                <a onMouseOut="hide_sidebar()" class="appendix appendix--info" href="#"></a>

                <div class="appendix-content down">
                    <h3><?php echo $language_based_content['info_people_section']; ?></h3>
                    <p><?php echo $language_based_content['viewers_info_people_section']; ?></p>
                </div>
                <div style="padding: 0px; float: right; width: 298px; margin-right: 20px;" class="search-box">
                    <div id="viewer-filterform-container" class="filterform">
                        <div class="filterform">
                            <input class="permissions-btn-search" id="viewer-filter-btn" type="button" value="">
                            <input id="viewer-filter-input" placeholder="<?php echo $language_based_content['search_viewers_people_section']; ?>" class="permissions-text-input" type="text" style="width: 233px; float: right;padding: 0px;border-left: 0;">
                            <div id="viewer-filter-cancel" type="text" class="filterinput" style="width: 10px; position: absolute; right: 213px; margin-top: 7px; cursor: pointer;display:none;">X</div>
                        </div>
                    </div>
                </div>

                <div id="viewer-list-containers"></div>

                <div style="clear: both;"></div>
            </div>
            <div class="clear" style="clear: both;"></div>
        </div>


        <div class="rel">
            <?php echo $groups; ?>
        </div>
    </div>

    <div id="addSuperAdminModal" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="header">
                    <h4 class="header-title nomargin-vertical smargin-bottom"><img alt="Admin" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/admin.png'); ?>" /> <?php echo $language_based_content['new_super_admin_people_section']; ?></h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                </div>
                <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/administrators_groups' ?>" enctype="multipart/form-data"
                      method="post" name="super_admin_form" id="super_admin_form" class="people-form" novalidate onsubmit="return false;">
                    <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                    <div class="way-form">
                        <h3><span><?php echo $language_based_content['step_1_people_section']; ?></span> <?php echo $language_based_content['add_name_email_people_section']; ?></h3>
                        <ol class="autoadd autoadd-sfont fmargin-list-left">
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name_5" name="users[][name]" required placeholder="<?php echo $language_based_content['full_name_people_section']; ?>" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email_5" name="users[][email]" required placeholder="<?php echo $language_based_content['email_address_people_section']; ?>" type="email" value="" /></label>
                                <a href="#" class="close">×</a>
                            </li>
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name_6" name="users[][name]" placeholder="<?php echo $language_based_content['full_name_people_section']; ?>" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email_6" name="users[][email]" placeholder="<?php echo $language_based_content['email_address_people_section']; ?>" type="email" value="" /></label>
                                <a href="#" class="close">×</a>
                            </li>
                        </ol>
                        <h3 class="way-form-divider"><span><?php echo $language_based_content['step_2_people_section']; ?></span> <?php echo $language_based_content['send_message_to_super_admins_people_section']; ?></h3>
                        <textarea class="lmargin-bottom fmargin-left" cols="50"  id="message_1" name="message" placeholder="<?php echo $language_based_content['message_optional_people_section']; ?>" rows="4" style="width:450px;"></textarea>
                        <input id="controller_source_1" name="user_type" type="hidden" value="110" /><br/><br/>
                        <button class="btn btn-green fmargin-left" style="background-color: <?php echo $btn_1_color; ?>; border-color: <?php echo $btn_1_color ?>;" type="submit" id="btnAddToAccount_addSuperuser"><?php echo $language_based_content['send_invitation_people_section']; ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div id="addNewAdminModal" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="header">
                    <h4 class="header-title nomargin-vertical smargin-bottom"><img alt="Admin" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/admin.png'); ?>" /> <?php echo $language_based_content['new_admin_people_section']; ?></h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                </div>
                <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/administrators_groups' ?>" enctype="multipart/form-data"
                      method="post" name="new_admin_form" id="new_admin_form" class="people-form" novalidate onsubmit="return false;">
                    <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                    <div class="way-form">
                        <h3><span><?php echo $language_based_content['step_1_people_section']; ?></span> <?php echo $language_based_content['add_name_email_people_section']; ?></h3>
                        <ol class="autoadd autoadd-sfont fmargin-list-left">
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name_7" name="users[][name]" required placeholder="<?php echo $language_based_content['full_name_people_section']; ?>" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email_7" name="users[][email]" required placeholder="<?php echo $language_based_content['email_address_people_section']; ?>" type="email" value="" /></label>
                                <a href="#" class="close">×</a>
                            </li>
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name_8" name="users[][name]" placeholder="<?php echo $language_based_content['full_name_people_section']; ?>" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email_8" name="users[][email]" placeholder="<?php echo $language_based_content['email_address_people_section']; ?>" type="email" value="" /></label>
                                <a href="#" class="close">×</a>
                            </li>
                        </ol>
                        <h3 class="way-form-divider"><span><?php echo $language_based_content['step_2_people_section']; ?></span> <?php echo $language_based_content['send_message_to_admins_people_section']; ?></h3>
                        <textarea class="lmargin-bottom fmargin-left" cols="50"  id="message_2" name="message" placeholder="<?php echo $language_based_content['message_optional_people_section']; ?>" rows="4" style="width:450px;"></textarea>
                        <input id="controller_source_2" name="user_type" type="hidden" value="115" /><br/><br/>
                        <button class="btn btn-green fmargin-left" style="background-color: <?php echo $btn_1_color; ?>; border-color: <?php echo $btn_1_color ?>;" type="submit" id="btnAddToAccount_addNewAdmin"><?php echo $language_based_content['send_invitation_people_section']; ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div id="addViewerModal" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="header">
                    <h4 class="header-title nomargin-vertical smargin-bottom"><img alt="Admin" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/admin.png'); ?>" /> <?php echo $language_based_content['new_viewer_people_section']; ?></h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                </div>
                <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/administrators_groups' ?>" enctype="multipart/form-data"
                      method="post" name="viewer_form" id="viewer_form" class="people-form" novalidate onsubmit="return false;">
                    <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                    <div class="way-form">
                        <h3><span><?php echo $language_based_content['step_1_people_section']; ?></span> <?php echo $language_based_content['add_name_email_people_section']; ?></h3>
                        <ol class="autoadd autoadd-sfont fmargin-list-left">
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name_9" name="users[][name]" required placeholder="<?php echo $language_based_content['full_name_people_section']; ?>" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email_9" name="users[][email]" required placeholder="<?php echo $language_based_content['email_address_people_section']; ?>" type="email" value="" /></label>
                                <a href="#" class="close">×</a>
                            </li>
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name_10" name="users[][name]" placeholder="<?php echo $language_based_content['full_name_people_section']; ?>" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email_10" name="users[][email]" placeholder="<?php echo $language_based_content['email_address_people_section']; ?>" type="email" value="" /></label>
                                <a href="#" class="close">×</a>
                            </li>
                        </ol>
                        <h3 class="way-form-divider"><span><?php echo $language_based_content['step_2_people_section']; ?></span> <?php echo $language_based_content['send_message_to_viewers_people_section']; ?></h3>
                        <textarea class="lmargin-bottom fmargin-left" cols="50"  id="message_3" name="message" placeholder="<?php echo $language_based_content['message_optional_people_section']; ?>" rows="4" style="width:450px;"></textarea>
                        <input id="controller_source_3" name="user_type" type="hidden" value="125" /><br/><br/>
                        <button style="background-color: <?php echo $btn_1_color; ?>; border-color: <?php echo $btn_1_color ?>;" class="btn btn-green fmargin-left" type="submit" id="btnAddToAccount_addViewer"><?php echo $language_based_content['send_invitation_people_section']; ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>






    <div id="addAdminModal"  class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="header">
                    <h4 class="header-title nomargin-vertical smargin-bottom"><img alt="Admin" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/admin.png'); ?>" /> <?php echo $language_based_content['new_user_people_section']; ?></h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                </div>
                <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/administrators_groups' ?>" enctype="multipart/form-data"
                      method="post" name="admin_form" id="admin_form" class="people-form" novalidate onsubmit="return false;">
                    <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                    <div class="way-form">
                        <h3><span><?php echo $language_based_content['step_1_people_section']; ?></span> <?php echo $language_based_content['add_name_email_people_section']; ?></h3>
                        <ol class="autoadd autoadd-sfont fmargin-list-left">
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name_11" name="users[][name]" required placeholder="<?php echo $language_based_content['full_name_people_section']; ?>" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email_11" name="users[][email]" required placeholder="<?php echo $language_based_content['email_address_people_section']; ?>" type="email" value="" /></label>
                                <a href="#" class="close">×</a>
                            </li>
                            <li>
                                <label class="icon3-user"><input class="input-dashed" id="users__name_12" name="users[][name]" placeholder="<?php echo $language_based_content['full_name_people_section']; ?>" type="text" value="" /></label>
                                <label class="icon3-email"><input class="input-dashed" id="users__email_12" name="users[][email]" placeholder="<?php echo $language_based_content['email_address_people_section']; ?>" type="email" value="" /></label>
                                <a href="#" class="close">×</a>
                            </li>
                        </ol>
                        <h3 class="way-form-divider"><span><?php echo $language_based_content['step_2_people_section']; ?></span> <?php echo $language_based_content['send_message_to_users_people_section']; ?></h3>
                        <textarea class="lmargin-bottom fmargin-left" cols="50" id="message_4" name="message" placeholder="<?php echo $language_based_content['message_optional_people_section']; ?>" rows="4" style="width:450px;"></textarea>
                        <input id="controller_source_4" name="user_type" type="hidden" value="120" />
                        <button style="background-color: <?php echo $btn_1_color; ?>; border-color: <?php echo $btn_1_color ?>;" class="btn btn-green fmargin-left" type="submit" id="btnAddToAccount_addPeople"><?php echo $language_based_content['send_invitation_people_section']; ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="addGroupModal"  class="modal" role="dialog">
        <div class="modal-dialog" style="width: 800px;">
            <div class="modal-content">
                <div class="header">
                    <h4 class="nomargin-top nomargin-bottom"><?php echo $language_based_content['create_group_people_section']; ?></h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                </div>
                <hr class="full mmargin-top" style="margin-left: 0px;margin-right: 0px;">
                <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/administrators_groups' ?>" class="form-horizontal"
                      enctype="multipart/form-data" id="new_group" method="post" name="new_group">
                    <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                    <div class="input-group">
                        <input class="full-input" id="group_name" name="group[name]" required placeholder="<?php echo $language_based_content['group_name_people_section']; ?>" size="30" type="text" />
                    </div>
                    <p class="small no-margin-bottom"><?php echo $language_based_content['add_people_into_group_people_section']; ?></p>
                    <hr class="no-margin">
                    <div style="clear:both;"></div>
                    <div class="row-fluid groupd-cls">
                        <div class="span3">
                            <h3><label><input type="checkbox" id="account-owner-toggle"> <?php echo $language_based_content['account_owner_people_section']; ?></label></h3>
                            <div id="account-owner">
                                <?php if ($account_owner): ?>
                                    <p>
                                        <label>
                                            <input id="account_owner_ids_" name="super_admin_ids[]" type="checkbox" value="<?php echo $account_owner['User']['id'] ?>" />
                                            <span><?php echo $account_owner['User']['first_name'] . ' ' . $account_owner['User']['last_name']; ?></span>
                                        </label>
                                    </p>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="span3">
                            <h3><label><input type="checkbox" id="susers-toggle"> <?php echo $language_based_content['super_admins_people_section']; ?></label></h3>
                            <div class="search-wrapper">
                                <input type="text" class="search-box" id="suser-search" placeholder="<?php echo $language_based_content['search_simple_super_admins_people_section']; ?>" style="width:148px;" />
                                <div id="suser-search-cancel-btn" type="text" class="hidden">X</div>
                            </div>

                            <div id="susers">
                                <?php if ($superAdmins): ?>
                                    <?php foreach ($superAdmins as $row): ?>
                                        <p>
                                            <label>
                                                <input id="super_admin_ids_<?php echo $row['User']['id'] ?>" name="super_admin_ids[]" type="checkbox" value="<?php echo $row['User']['id'] ?>" />
                                                <span><?php echo $row['User']['first_name'] . ' ' . $row['User']['last_name']; ?></span>
                                            </label>
                                        </p>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>

                            <div id="suser-pagination" class="group-pagination">
                                <a href="#" class="previous" data-action="previous">&lsaquo;</a>
                                <input type="text" readonly="readonly" data-max-page="10" />
                                <a href="#" class="next" data-action="next">&rsaquo;</a>
                            </div>
                        </div>
                        <div class="span3">
                            <h3><label><input type="checkbox" id="newAdmin-toggle"> <?php echo $language_based_content['admins_people_section']; ?></label></h3>
                            <div class="search-wrapper">
                                <input type="text" class="search-box" id="newAdmin-search" placeholder="<?php echo $language_based_content['search_simple_admins_people_section']; ?>" />
                                <div id="newAdmin-search-cancel-btn" type="text" class="hidden">X</div>
                            </div>

                            <div id="newAdmin">
                                <?php if ($newAmins): ?>
                                    <?php foreach ($newAmins as $row): ?>
                                        <p>
                                            <label>
                                                <input id="super_admin_ids_<?php echo $row['User']['id'] ?>" name="super_admin_ids[]" type="checkbox" value="<?php echo $row['User']['id'] ?>" />
                                                <?php $name = $row['User']['first_name'] . ' ' . $row['User']['last_name']; ?>
                                                <span><?php echo strlen($name) > 18 ? mb_substr($name, 0, 18) . '...' : $name; ?></span>
                                            </label>
                                        </p>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>

                            <div id="newAdmin-pagination" class="group-pagination">
                                <a href="#" class="previous" data-action="previous">&lsaquo;</a>
                                <input type="text" readonly="readonly" data-max-page="10" />
                                <a href="#" class="next" data-action="next">&rsaquo;</a>
                            </div>
                        </div>
                        <div class="span3">
                            <h3><label><input type="checkbox" id="users-toggle"> <?php echo $language_based_content['users_people_section']; ?></label></h3>
                            <div class="search-wrapper">
                                <input type="text" class="search-box" id="user-search" placeholder="<?php echo $language_based_content['search_simple_users_people_section']; ?>" />
                                <div id="user-search-cancel-btn" type="text" class="hidden">X</div>
                            </div>

                            <div id="users">
                                <?php if ($adminUsers): ?>
                                    <?php foreach ($adminUsers as $row): ?>
                                        <p>
                                            <label>
                                                <input id="super_admin_ids_<?php echo $row['User']['id'] ?>" name="super_admin_ids[]" type="checkbox" value="<?php echo $row['User']['id'] ?>" />
                                                <span><?php echo $row['User']['first_name'] . ' ' . $row['User']['last_name']; ?></span>
                                            </label>
                                        </p>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>

                            <div id="user-pagination" class="group-pagination">
                                <a href="#" class="previous" data-action="previous">&lsaquo;</a>
                                <input type="text" readonly="readonly" data-max-page="10" />
                                <a href="#" class="next" data-action="next">&rsaquo;</a>
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                    </div>
                    <div style="clear:both;"></div>
                    <hr>
                    <p class="right" style="text-align: right;float: none;">
                        <input id="controller_source_5" name="user_type" type="hidden" value="groups" />
                        <button class="btn btn-green" type="submit" style="background-color:<?php echo $btn_1_color ?>; border-color:<?php echo $btn_1_color; ?>;"><?php echo $language_based_content['create_people_section']; ?></button>
                        <a class="btn btn-transparent close-reveal" data-dismiss="modal"><?php echo $language_based_content['cancel_people_section_button']; ?></a>
                    </p>
                </form>

            </div>
        </div>
    </div>
    <a href="#" id="view_browser_notification" style="display: none;" data-toggle="modal" data-target="BrowserMessageModal"><?php echo $language_based_content['browser_message']; ?></a>
    <div id="BrowserMessageModal" class="modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="header">
                    <h4 class="header-title nomargin-vertical smargin-bottom"><?=$language_based_content['Upgade_Your_Browser']; ?></h4>
                    <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
                </div>
                <p><?php echo $this->Custom->parse_translation_params($language_based_content['you_are_using_unsupported'], ['site_title' => $this->Custom->get_site_settings('site_title')]); ?></p>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var userRoleId = <?php echo $user_current_account['users_accounts']['role_id']; ?>;
        $(function () {
            $('#btnAddToAccount_addPeople').click(function () {
                addToInviteList();
            });
            $('#btnAddToAccount_addSuperuser').click(function () {
                addToInviteListSuper();
            });
            $('#btnAddToAccount_addNewAdmin').click(function () {
                addToInviteListNewAdmin();
            });
            $('#btnAddToAccount_addViewer').click(function () {
                addToInviteListViewer();
            });
            if (userRoleId != 100) {
                var storage = 'localStorage' in window && window['localStorage'] !== null ? window['localStorage'] : false;
                /*if (storage) {
                 storage.setItem('show_tour', 0);
                 }*/
            }
            document.new_group.reset(); /* prevents FF from restoring values after refresh */

            loadUserList(1, 110, '', '', '1');
            loadUserList(1, 120, '', '', '1');
            loadUserList(1, 115, '', '', '1');
            loadUserList(1, 125, '', '', '1');

            var searchSuperUserInp = $("#superuser-filter-input");
            var searchSuperUserBtn = $("#superuser-filter-btn");
            var cancelSuperUserBtn = $("#superuser-filter-cancel");

            var searchNewAdminInp = $("#newadmin-filter-input");
            var searchNewAdminBtn = $("#newadmin-filter-btn");
            var cancelNewAdminBtn = $("#newadmin-filter-cancel");

            var searchViewerInp = $("#viewer-filter-input");
            var searchViewerBtn = $("#viewer-filter-btn");
            var cancelViewerBtn = $("#viewer-filter-cancel");

            var searchUserInp = $("#user-filter-input");
            var searchUserBtn = $("#user-filter-btn");
            var cancelUserBtn = $("#user-filter-cancel");

            $('#super-user-list-containers').on('click', '.pagination li.active', function () {
                var page = $(this).attr('p');
                loadUserList(page, 110, searchSuperUserInp.val(), function () {
                    $('html, body').animate({
                        scrollTop: $("#super-admins-container").offset().top
                    }, 200);
                });
            });

            $('#new-admin-list-containers').on('click', '.pagination li.active', function () {
                var page = $(this).attr('p');
                loadUserList(page, 115, searchNewAdminInp.val(), function () {
                    $('html, body').animate({
                        scrollTop: $("#new-admin-container").offset().top
                    }, 200);
                });
            });

            $('#admin-user-list-containers').on('click', '.pagination li.active', function () {
                var page = $(this).attr('p');
                loadUserList(page, 120, searchUserInp.val(), function () {
                    $('html, body').animate({
                        scrollTop: $("#admin-container").offset().top
                    }, 200);
                });
            });

            $('#viewer-list-containers').on('click', '.pagination li.active', function () {
                var page = $(this).attr('p');
                loadUserList(page, 125, searchViewerInp.val(), function () {
                    $('html, body').animate({
                        scrollTop: $("#viewer-container").offset().top
                    }, 200);
                });
            });

            searchSuperUserBtn.click(function (e) {
                var filter = searchSuperUserInp.val();
                loadUserList(1, 110, filter);
                if (filter.length > 0) {
                    cancelSuperUserBtn.show();
                } else {
                    cancelSuperUserBtn.hide();
                }
                e.preventDefault();
                return false;
            });

            searchSuperUserInp.keypress(function (e) {
                if (e.keyCode == 13) {
                    searchSuperUserBtn.trigger('click');
                }
            });

            cancelSuperUserBtn.click(function () {
                cancelSuperUserBtn.hide();
                searchSuperUserInp.val('');
                searchSuperUserBtn.trigger('click');
            });


            searchNewAdminBtn.click(function (e) {
                var filter = searchNewAdminInp.val();
                loadUserList(1, 115, filter);
                if (filter.length > 0) {
                    cancelNewAdminBtn.show();
                } else {
                    cancelNewAdminBtn.hide();
                }
                e.preventDefault();
                return false;
            });

            searchNewAdminInp.keypress(function (e) {
                if (e.keyCode == 13) {
                    searchNewAdminBtn.trigger('click');
                }
            });

            cancelNewAdminBtn.click(function () {
                cancelNewAdminBtn.hide();
                searchNewAdminInp.val('');
                searchNewAdminBtn.trigger('click');
            });


            searchViewerBtn.click(function (e) {
                var filter = searchViewerInp.val();
                loadUserList(1, 125, filter);
                if (filter.length > 0) {
                    cancelViewerBtn.show();
                } else {
                    cancelViewerBtn.hide();
                }
                e.preventDefault();
                return false;
            });

            searchViewerInp.keypress(function (e) {
                if (e.keyCode == 13) {
                    searchViewerBtn.trigger('click');
                }
            });

            cancelViewerBtn.click(function () {
                cancelViewerBtn.hide();
                searchViewerInp.val('');
                searchViewerBtn.trigger('click');
            });


            searchUserBtn.click(function () {
                var filter = searchUserInp.val();
                loadUserList(1, 120, filter);
                if (filter.length > 0) {
                    cancelUserBtn.show();
                } else {
                    cancelUserBtn.hide();
                }
                return false;
            });

            searchUserInp.keypress(function (e) {
                if (e.keyCode == 13) {
                    searchUserBtn.trigger('click');
                }
            });

            cancelUserBtn.click(function () {
                cancelUserBtn.hide();
                searchUserInp.val('');
                searchUserBtn.trigger('click');
            });

            searchSuperUserInp.val('');
            searchNewAdminInp.val('');
            searchUserInp.val('');

            $("#account-owner-toggle").click(function () {
                var isChecked = $(this).prop("checked");
                if (isChecked) {
                    $("#account-owner input[type='checkbox']").prop("checked", true);
                } else {
                    $("#account-owner input[type='checkbox']").prop("checked", false);
                }
            });
            $("#susers-toggle").click(function () {
                var isChecked = $(this).prop("checked");
                if (isChecked) {
                    $("#susers input[type='checkbox']").prop("checked", true);
                } else {
                    $("#susers input[type='checkbox']").prop("checked", false);
                }
            });
            $("#users-toggle").click(function () {
                var isChecked = $(this).prop("checked");
                if (isChecked) {
                    $("#users input[type='checkbox']").prop("checked", true);
                } else {
                    $("#users input[type='checkbox']").prop("checked", false);
                }
            });
            $("#newAdmin-toggle").click(function () {
                var isChecked = $(this).prop("checked");
                if (isChecked) {
                    $("#newAdmin input[type='checkbox']").prop("checked", true);
                } else {
                    $("#newAdmin input[type='checkbox']").prop("checked", false);
                }
            });

            /*        var container = document.querySelector('#grous-list');
             var msnry = new Masonry( container, {
             itemSelector: '.span5',
             columnWidth: 400
             });*/
        });
        function ValidateEmail(mail)
        {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (re.test(mail))
            {
                return (true);
            }
            return (false);
        }
        function addToInviteList() {
            var lposted_data = [];
            var retVal = true;
            var userNames = document.getElementsByName('users[][name]');
            var userEmails = document.getElementsByName('users[][email]');
            var i = 0;
            for (i = 0; i < userNames.length; i++) {
                var userEmail = $(userEmails[i]);
                if (is_email_exists(userEmail.val())) {
                    alert('<?=$language_based_content["A_new_user_with_this_email_is_already_added"]; ?>');
                    retVal = false;
                }
            }

            for (i = 0; i < userNames.length; i++) {
                var userName = $(userNames[i]);
                var userEmail = $(userEmails[i]);
                if (userName.val() != '' || userEmail.val() != '') {
                    if (userName.val() == '') {
                        alert('<?=$language_based_content["Please_enter_a_valid_Full_Name"]; ?>');
                        userName.focus();
                        retVal = false;
                    }
                    if (userEmail.val() == '' || !ValidateEmail(userEmail.val())) {
                        alert('<?=$language_based_content["Please_enter_a_valid_Email"]; ?>');
                        userEmail.focus();
                        retVal = false;
                    }
                    if (is_email_exists(userEmail.val())) {
                        alert('<?=$language_based_content["A_new_user_with_this_email_is_already_added"]; ?>');
                        retVal = false;
                    }
                    var newUser = [];
                    newUser[newUser.length] = '';
                    newUser[newUser.length] = userName.val();
                    newUser[newUser.length] = userEmail.val();
                    lposted_data[lposted_data.length] = newUser;
                }
            }

            if (lposted_data.length == 0) {
                alert('<?=$language_based_content["Please_enter_atleast_one_Full_name_or_email"]; ?>');
                retVal = false;
            }
            if (retVal) {
                document.getElementById("admin_form").submit();
            } else {
                return retVal;
            }
        }
        function addToInviteListSuper() {
            var lposted_data = [];
            var retVal = true;
            var userNames = document.getElementsByName('users[][name]');
            var userEmails = document.getElementsByName('users[][email]');
            var i = 0;
            for (i = 0; i < userNames.length; i++) {
                var userEmail = $(userEmails[i]);
                if (is_email_exists(userEmail.val())) {
                    alert('<?=$language_based_content["A_new_user_with_this_email_is_already_added"]; ?>');
                    retVal = false;
                }
            }

            for (i = 0; i < userNames.length; i++) {
                var userName = $(userNames[i]);
                var userEmail = $(userEmails[i]);
                if (userName.val() != '' || userEmail.val() != '') {
                    if (userName.val() == '') {
                        alert('<?=$language_based_content["Please_enter_a_valid_Full_Name"]; ?>');
                        userName.focus();
                        retVal = false;
                    }
                    if (userEmail.val() == '' || !ValidateEmail(userEmail.val())) {
                        alert('<?=$language_based_content["Please_enter_a_valid_Email"]; ?>');
                        userEmail.focus();
                        retVal = false;
                    }
                    if (is_email_exists(userEmail.val())) {
                        alert('<?=$language_based_content["A_new_user_with_this_email_is_already_added"]; ?>');
                        retVal = false;
                    }
                    var newUser = [];
                    newUser[newUser.length] = '';
                    newUser[newUser.length] = userName.val();
                    newUser[newUser.length] = userEmail.val();
                    lposted_data[lposted_data.length] = newUser;
                }
            }

            if (lposted_data.length == 0) {
                alert('<?=$language_based_content["Please_enter_atleast_one_Full_name_or_email"]; ?>');
                retVal = false;
            }
            if (retVal) {
                document.getElementById("super_admin_form").submit();
            } else {
                return retVal;
            }
        }


        function addToInviteListNewAdmin() {
            var lposted_data = [];
            var retVal = true;
            var userNames = document.getElementsByName('users[][name]');
            var userEmails = document.getElementsByName('users[][email]');
            var i = 0;
            for (i = 0; i < userNames.length; i++) {
                var userEmail = $(userEmails[i]);
                if (is_email_exists(userEmail.val())) {
                    alert('<?=$language_based_content["A_new_user_with_this_email_is_already_added"]; ?>');
                    retVal = false;
                }
            }

            for (i = 0; i < userNames.length; i++) {
                var userName = $(userNames[i]);
                var userEmail = $(userEmails[i]);
                if (userName.val() != '' || userEmail.val() != '') {
                    if (userName.val() == '') {
                        alert('<?=$language_based_content["Please_enter_atleast_one_Full_name_or_email"]; ?>');
                        userName.focus();
                        retVal = false;
                    }
                    if (userEmail.val() == '' || !ValidateEmail(userEmail.val())) {
                        alert('<?=$language_based_content["Please_enter_a_valid_Email"]; ?>');
                        userEmail.focus();
                        retVal = false;
                    }
                    if (is_email_exists(userEmail.val())) {
                        alert('<?=$language_based_content["A_new_user_with_this_email_is_already_added"]; ?>');
                        retVal = false;
                    }
                    var newUser = [];
                    newUser[newUser.length] = '';
                    newUser[newUser.length] = userName.val();
                    newUser[newUser.length] = userEmail.val();
                    lposted_data[lposted_data.length] = newUser;
                }
            }

            if (lposted_data.length == 0) {
                alert('<?=$language_based_content["Please_enter_atleast_one_Full_name_or_email"]; ?>');
                retVal = false;
            }
            if (retVal) {
                document.getElementById("new_admin_form").submit();
            } else {
                return retVal;
            }
        }

        function addToInviteListViewer() {
            var lposted_data = [];
            var retVal = true;
            var userNames = document.getElementsByName('users[][name]');
            var userEmails = document.getElementsByName('users[][email]');
            var i = 0;
            for (i = 0; i < userNames.length; i++) {
                var userEmail = $(userEmails[i]);
                if (is_email_exists(userEmail.val())) {
                    alert('<?=$language_based_content["A_new_user_with_this_email_is_already_added"]; ?>');
                    retVal = false;
                }
            }

            for (i = 0; i < userNames.length; i++) {
                var userName = $(userNames[i]);
                var userEmail = $(userEmails[i]);
                if (userName.val() != '' || userEmail.val() != '') {
                    if (userName.val() == '') {
                        alert('<?=$language_based_content["Please_enter_a_valid_Full_Name"]; ?>');
                        userName.focus();
                        retVal = false;
                    }
                    if (userEmail.val() == '' || !ValidateEmail(userEmail.val())) {
                        alert('<?=$language_based_content["Please_enter_a_valid_Email"]; ?>');
                        userEmail.focus();
                        retVal = false;
                    }
                    if (is_email_exists(userEmail.val())) {
                        alert('<?=$language_based_content["A_new_user_with_this_email_is_already_added"]; ?>');
                        retVal = false;
                    }
                    var newUser = [];
                    newUser[newUser.length] = '';
                    newUser[newUser.length] = userName.val();
                    newUser[newUser.length] = userEmail.val();
                    lposted_data[lposted_data.length] = newUser;
                }
            }

            if (lposted_data.length == 0) {
                alert('<?=$language_based_content["Please_enter_atleast_one_Full_name_or_email"]; ?>');
                retVal = false;
            }
            if (retVal) {
                document.getElementById("viewer_form").submit();
            } else {
                return retVal;
            }
        }



        function loadUserList(page, role, keywords, callback, request_type) {
            var data = {
                page: page,
                role: role,
                keywords: keywords !== undefined ? keywords : '',
                request_type: request_type
            };
            $.ajax({
                type: "POST",
                url: "<?php echo $this->base . '/users/getAjaxUsers' ?>",
                dataType: "json",
                data: data,
                success: function (msg) {
                    if (role == 110) {
                        $("#super-user-list-containers").html(msg.msg);
                    } else if (role == 120) {
                        $("#admin-user-list-containers").html(msg.msg);
                    }
                    else if (role == 115) {
                        $("#new-admin-list-containers").html(msg.msg);
                    }

                    else if (role == 125) {
                        $("#viewer-list-containers").html(msg.msg);
                    }

                    $(".blue_count").html(msg.total_people_count);

                    if (typeof callback == 'function') {
                        callback();
                    }
                }
            });
        }
        function hide_sidebar() {
            $(".appendix-content").delay(500).fadeOut(500);
            // $("#collab_help").delay(500).fadeOut(500);


        }

        function changePassword(obj) {
            var id = obj.id;

            var password = $('#user_password' + id).val();
            var cpassword = $('#user_password_confirmation' + id).val();

            var data = {
                password: password,
                confirm_password: cpassword,
                user_id: id,
            };
            $.ajax({
                type: "POST",
                url: "<?php echo $this->base . '/users/change_password_accounts' ?>",
                data: data,
                success: function (msg) {
                    console.log(msg);

                    if (msg == 'success') {
                        $("#msg_err" + id).hide();
                        $('.pass_success').show();
                        $("#msg_succ" + id).text('<?=$language_based_content["Password_has_been_successfully_changed"]; ?>');
                        setTimeout(function () {
                            window.location = "/users/administrators_groups";
                        }, 1000);

                    } else {
                        $('.pass_success').hide();
                        $("#msg_err" + id).text(msg);
                        $("#msg_err" + id).html(msg);
                    }
                }
            });
        }

        $(document).ready(function (e) {

            var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><span><?php echo $breadcrumb_language_based_content['people_breadcrumb']; ?></span></div>';

            $('.breadCrum').html(bread_crumb_data);


        });

    </script>

<?php endif; ?>
