<div id="main" class="container box">
    <h2 class="nomargin-top nomargin-bottom"><?php echo $language_based_content['Edit_Group']?></h2>
    <hr class="full mmargin-top">
    <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/edit/' . $group_id; ?>" class="edit_group" id="edit_group_36" method="post">
        <div style="margin:0;padding:0;display:inline">
            <input name="utf8" type="hidden" value="✓"><input name="_method" type="hidden" value="put">
        </div>
        <?php
        if ($userGroup['groups']) {
            $selectGroupIds = '';
            foreach ($userGroup['groups'] as $row) {
                $selectGroupIds[$row['UserGroup']['user_id']] = $row['UserGroup']['user_id'];
            }
        }
        ?>
        </pre>
        <div class="input-group">
            <input class="full-input" id="group_name" name="group[name]" required placeholder="<?php echo $language_based_content['group_name_people_section']?>" size="30" type="text" value="<?php echo $userGroup['name']; ?>">
        </div>
        <p class="small no-margin-bottom"><?php echo $language_based_content['add_people_into_group_people_section']?></p>

        <hr class="no-margin">

        <div class="row-fluid">
            <div class="span3">
                <h3><?php echo $language_based_content['account_owner_people_section_single']?></h3>
                <?php if ($accountOwner): ?>
                    <?php
                    ?>
                    <p>
                        <label>
                            <input id="super_admin_ids_<?php echo $row['User']['id'] ?>" type="checkbox" name="super_admin_ids[]" <?php echo ((isset($selectGroupIds) && is_array($selectGroupIds) && count($selectGroupIds) > 0 && in_array($accountOwner['User']['id'], $selectGroupIds) !== FALSE) ? 'checked="checked"' : '') ?> value="<?php echo $accountOwner['User']['id'] ?>">
                            <?php echo $accountOwner['User']['first_name'] . ' ' . $accountOwner['User']['last_name']; ?>

                        </label>
                    </p>
                <?php endif; ?>
            </div>
            <div class="span3">
                <h3><?php echo $language_based_content['super_admin_people_section_single']?></h3>
                <div class="search-wrapper">
                    <input type="text" id="suser-search" placeholder="<?php echo $language_based_content['search_simple_super_admins_people_section']?>" style="width:148px;"/>
                    <div id="suser-search-cancel-btn" type="text" class="hidden" style="right: 22px;">X</div>
                </div>
                <div id="susers">
                    <?php if ($superAdmins): ?>
                        <?php foreach ($superAdmins as $row): ?>
                            <p>
                                <label>
                                    <input id="super_admin_ids_<?php echo $row['User']['id'] ?>" type="checkbox" name="super_admin_ids[]" <?php echo ((isset($selectGroupIds) && is_array($selectGroupIds) && count($selectGroupIds) > 0 && in_array($row['User']['id'], $selectGroupIds) !== FALSE) ? 'checked="checked"' : '') ?> value="<?php echo $row['User']['id'] ?>">
                                    <span><?php echo $row['User']['first_name'] . ' ' . $row['User']['last_name']; ?></span>
                                </label>
                            </p>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div id="suser-pagination" class="group-pagination">
                    <a href="#" class="previous" data-action="previous">&lsaquo;</a>
                    <input type="text" readonly="readonly" data-max-page="10" />
                    <a href="#" class="next" data-action="next">&rsaquo;</a>
                </div>
            </div>
            <div class="span3">
                <h3> <?php echo $language_based_content['admin_people_section_single']?> </h3>
                <div class="search-wrapper">
                    <input type="text" class="search-box" id="newAdmin-search" placeholder="<?php echo $language_based_content['search_simple_admins_people_section']?>" />
                    <div id="newAdmin-search-cancel-btn" type="text" class="hidden">X</div>
                </div>

                <div id="newAdmin">
                    <?php if ($newAdminUsers): ?>
                        <?php foreach ($newAdminUsers as $row): ?>
                            <p>
                                <label>
                                    <input id="super_admin_ids_<?php echo $row['User']['id'] ?>" name="super_admin_ids[]" type="checkbox" <?php echo ((isset($selectGroupIds) && is_array($selectGroupIds) && count($selectGroupIds) > 0 && in_array($row['User']['id'], $selectGroupIds) !== FALSE) ? 'checked="checked"' : '') ?> value="<?php echo $row['User']['id'] ?>" />
                                    <?php $name = $row['User']['first_name'] . ' ' . $row['User']['last_name']; ?>
                                    <span><?php echo strlen($name) > 18 ? mb_substr($name, 0, 18) . '...' : $name; ?></span>
                                </label>
                            </p>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>

                <div id="newAdmin-pagination" class="group-pagination">
                    <a href="#" class="previous" data-action="previous">&lsaquo;</a>
                    <input type="text" readonly="readonly" data-max-page="10" />
                    <a href="#" class="next" data-action="next">&rsaquo;</a>
                </div>
            </div>
            <div class="span3">
                <h3>Users</h3>
                <div class="search-wrapper">
                    <input type="text" id="user-search" placeholder="<?php echo $language_based_content['search_simple_users_people_section']?>" />
                    <div id="user-search-cancel-btn" type="text" class="hidden">X</div>
                </div>
                <div id="users">
                    <?php if ($adminUsers): ?>
                        <?php foreach ($adminUsers as $row): ?>
                            <p>
                                <label>
                                    <input id="super_admin_ids_<?php echo $row['User']['id'] ?>" type="checkbox" name="super_admin_ids[]" <?php echo ((isset($selectGroupIds) && is_array($selectGroupIds) && count($selectGroupIds) > 0 && in_array($row['User']['id'], $selectGroupIds) !== FALSE) ? 'checked="checked"' : '') ?> value="<?php echo $row['User']['id'] ?>">
                                    <span><?php echo $row['User']['first_name'] . ' ' . $row['User']['last_name']; ?></span>
                                </label>
                            </p>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div id="user-pagination" class="group-pagination">
                    <a href="#" class="previous" data-action="previous">&lsaquo;</a>
                    <input type="text" readonly="readonly" data-max-page="10" />
                    <a href="#" class="next" data-action="next">&rsaquo;</a>
                </div>
            </div>
        </div>
        <hr>
        <p class="right">
            <input class="btn btn-green" type="submit" value="<?php echo $language_based_content['Update_Group']?>" style="background-color:<?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>">
            <a href="<?php echo $this->base . '/users/administrators_groups' ?>" class="btn btn-transparent"><?php echo $language_based_content['cancel_people_section_button']?></a>
        </p>

    </form>


</div>
