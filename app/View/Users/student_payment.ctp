<style>
    .one-half label{    
        width: 309px;
        margin: 6px 0 2px 0;
        cursor: auto;
    } 
    .cl_invalid{
        border: 1px solid red !important;
    }
    label.error{
        color: #be1616;
        font-size: 12px;
    }
    .gw select{
        border: 1px solid #c5c5c5;
        border-radius: 3px;
        box-shadow: 0 1px 2px rgba(0,0,0,0.09) inset;
        font-size: 13px;
        font-weight: bold;
        color: #b2b2b2;
        padding: 8px 4px;
        width: 309px;
        height:37px;
        background:#fff;
    }

    .gw #amount{
        border: 1px solid #c5c5c5;
        border-radius: 3px;
        box-shadow: 0 1px 2px rgba(0,0,0,0.09) inset;
        font-size: 13px;
        font-weight: bold;
        color: #b2b2b2;
        padding: 8px 4px;
        width: 300px;
    }

    .gw #card_number{
        border: 1px solid #c5c5c5;
        border-radius: 3px;
        box-shadow: 0 1px 2px rgba(0,0,0,0.09) inset;
        font-size: 13px;
        font-weight: bold;
        color: #b2b2b2;
        padding: 8px 4px;
        width: 300px;
    }

</style>


<form accept-charset="UTF-8" action="<?php echo $this->base . '/Users/student_payment/' . $account_id . '/' . $amount ?>" class="signup island compact" enctype="multipart/form-data" id="frmActiviation" method="post" style="padding-bottom: 0px;">
    <div style="margin:0;padding:0;display:inline">
        <input name="utf8" type="hidden" value="&#x2713;" />
        <input name="authenticity_token" type="hidden" value="d5KI32DtOGU2o6duxIGhW0VRgTZkj+3UWAQjQHTsGJk=" />
    </div>
    <div class="error">
    </div>

    <div class="gw">
        <div class="g one-half">

            <?php
            echo $this->Form->input('first_name', array(
                'label' => $language_based_content['first_name_payment'],
                'id' => 'name',
                'div' => FALSE,
                'tabindex' => '1',
                'class' => 'text_input_larg',
                'placeholder' => '',
                'required' => 'required',
                'value' => '',
                'title' => $language_based_content['enter_your_first_name'],
                'maxlength' => '35'
            ));
            ?>





            <?php
            echo $this->Form->input('email', array(
                'label' => $language_based_content['email_payment'],
                'class' => 'text_input_larg',
                'id' => 'email',
                'div' => FALSE,
                'tabindex' => '3',
                'required' => 'required',
                'type' => 'email',
                'value' => isset($user['User']['email']) ? $user['User']['email'] : '',
                'placeholder' => '',
                'title' => $language_based_content['please_enter_valid_email'],
                'maxlength' => '50'
            ));
            ?>
            <?php echo $this->Form->input('password', array('label' => $language_based_content['password_payment'], 'tabindex' => '4', 'type' => 'password', 'required', 'title' => $language_based_content['password_must_atleast_6_characters'], 'minlength' => '6','maxlength' => '35')); ?>
            <?php echo $this->Form->error('Users.password'); ?>      
            <?php
            echo $this->Form->input('city', array(
                'label' => $language_based_content['city_payment'],
                'div' => FALSE,
                'id' => 'city',
                'tabindex' => '7',
                'class' => 'text_input_larg',
                'placeholder' => '',
                'required' => 'required',
                'value' => '',
                'title' => $language_based_content['enter_your_city'],
                'maxlength' => '35'
            ));
            ?>
            <?php
            echo $this->Form->input('zip_code', array(
                'label' => $language_based_content['zip_code_payment'],
                'div' => FALSE,
                'id' => 'zip_code',
                'tabindex' => '9',
                'class' => 'text_input_larg',
                'placeholder' => '',
                'required' => 'required',
                'value' => '',
                'title' => $language_based_content['please_enter_your_zip_code'],
                'maxlength' => '35'
            ));
            ?>



            <?php
            echo $this->Form->input('card_holder_name', array(
                'label' => $language_based_content['card_holder_name'],
                'id' => 'card_holder_name',
                'div' => true,
                'tabindex' => '11',
                'class' => 'text_input_larg payment_mod',
                'placeholder' => '',
                'type' => 'text',
                'required' => 'required',
                'value' => '',
                'title' => $language_based_content['please_enter_card_holder_name'],
                'maxlength' => '35'
            ));
            ?>
           
            <?php
            echo $this->Form->input('card_cvv', array(
                'label' => $language_based_content['cvv_payment'],
                'id' => 'card_cvv',
                'div' => true,
                'name' => 'card_cvv',
                'tabindex' => '13',
                'class' => 'text_input_larg payment_mod',
                'placeholder' => '',
                'type' => 'text',
                'required' => 'required',
                'value' => '',
                'title' => $language_based_content['please_enter_cvv_number'],
                'maxlength' => '4'
                    //   'style' => 'width:100px;'
            ));
            ?>
           
           

        </div>
        <div class="g one-half">
            <?php
            echo $this->Form->input('last_name', array(
                'label' => $language_based_content['last_name_payment'],
                'div' => FALSE,
                'required' => 'required',
                'id' => 'last_name',
                'tabindex' => '2',
                'placeholder' => '',
                'class' => 'text_input_larg',
                'value' => '',
                'title' => $language_based_content['please_enter_last_name'],
                'maxlength' => '35'
            ));
            ?>
            <?php
            echo $this->Form->input('institution_id', array(
                'label' => 'Institution ID',
                'id' => 'institution_id',
                'div' => FALSE,
                'name' => 'institution_id',
                'tabindex' => '12',
                'class' => 'text_input_larg',
                'placeholder' => '',
                'type' => 'text',
                //'required' => 'required',
                'value' => '',
                'title' => 'Please enter Institution ID',
                'type' => 'text',
                'maxlength' => '35'
            ));
            ?>
            

            <?php echo $this->Form->input('password_confirmation', array('label' => $language_based_content['confirm_password_payment'], 'type' => 'password', 'tabindex' => '5', '', 'required', 'title' => $language_based_content['confirm_your_password'], 'minlength' => '6','maxlength' => '35')); ?>
            <?php echo $this->Form->error('Users.password_confirmation'); ?>


            <?php
            echo $this->Form->input('address', array(
                'label' => $language_based_content['billing_address_payment'],
                'id' => 'address',
                'div' => FALSE,
                'tabindex' => '6',
                'class' => 'text_input_larg',
                'placeholder' => '',
                'required' => 'required',
                'value' => '',
                'title' => $language_based_content['please_enter_address'],
                'maxlength' => '150'
            ));
            ?>
            <?php /* echo $this->Form->input('institution', array(
              'label' => $language_based_content['school_district_payment'],
              'div' => FALSE,
              'id'=>'institution',
              'tabindex' => '4',
              'class' => 'text_input_larg',
              'placeholder' => '',
              //    'required' => 'required',
              'value' => isset($company_name) ? $company_name : '',
              'readonly' ,
              )); */ ?>

            <label class="control-label payment_mod " for="expiry_date"><?php echo $language_based_content['state_payment']; ?></label>
            <select tabindex = "8" name="state" class="payment_mod"><?php echo StateDropdown('Alaska', 'name'); ?></select>
            <label class="control-label payment_mod" for="expiry_date"><?php echo $language_based_content['amount_payment']; ?></label>
            <div style="background: #f3f3f3;width: 309px;font-weight: bold; font-size: 14px;" class="payment_mod">
                <?php if ($student_fees != ''): ?>
                    &nbsp; $<?php echo $student_fees; ?>
                <?php else: ?>
                    <?php echo $language_based_content['there_are_no_payments_please_contact'];  ?>
                <?php endif; ?>
            </div>


            <?php
            echo $this->Form->input('amount', array(
                'label' => $language_based_content['enter_amount_payment'],
                'id' => 'amount',
                'div' => FALSE,
                'tabindex' => '10',
                'class' => 'text_input_larg',
                'placeholder' => '',
                'required' => 'required',
                'value' => $student_fees,
                //  'title' => 'Please enter valid amount',
                'pattern' => '[0-9]+',
                'type' => 'hidden',
                'name' => 'amount'
            ));
            ?>

            <?php
            echo $this->Form->input('card_number', array(
                'label' => $language_based_content['card_number_payment'],
                'id' => 'card_number',
                'div' => true,
                'name' => 'card_number',
                'tabindex' => '12',
                'class' => 'text_input_larg payment_mod',
                'placeholder' => '',
                'type' => 'text',
                'required' => 'required',
                'value' => '',
                'title' => $language_based_content['please_enter_valid_card_number'],
                'type' => 'text',
                'maxlength' => '16'
            ));
            ?>


            <label class="control-label payment_mod" for="expiry_date"><?php echo $language_based_content['card_expiry_date']; ?></label>
            <select tabindex = "14"  name="expiry_month" id="expiry_month" class="payment_mod" style='width:100px;'>

                <option value="01">Jan</option>
                <option value="02">Feb</option>
                <option value="03">Mar</option>
                <option value="04">Apr</option>
                <option value="05">May</option>
                <option value="06">June</option>
                <option value="07">July</option>
                <option value="08">Aug</option>
                <option value="09">Sep</option>
                <option value="10">Oct</option>1
                <option value="11">Nov</option>
                <option value="12">Dec</option>
            </select>
            <select tabindex = "15"  name="expiry_year" class="payment_mod" style='width:100px;'>

                <option value="19">2019</option>
                <option value="20">2020</option>
                <option value="21">2021</option>
                <option value="22">2022</option>
                <option value="23">2023</option>
                <option value="24">2024</option>
                <option value="25">2025</option>
                <option value="26">2026</option>
                <option value="27">2027</option>
                <option value="28">2028</option>
                <option value="29">2029</option>
                <option value="30">2030</option>
            </select>           

        </div>
    </div>

    <div class="form-actions full-bleed" style="margin-top: 20px;background-color:white; ">

        <input id="form_submit" type="submit" class="fr btn btn-green btn-lrg btn-inv" value="<?php echo $language_based_content['process_payment']; ?>">
        <p class="compact semi milli"><?php echo $language_based_content['by_clicking_payment']; ?> <a target="_blank" href="http://sibme.com/terms.html"><?php echo $language_based_content['terms_of_use_payment']; ?></a> <?php echo $language_based_content['and']; ?> 
            <?php echo $language_based_content['and_payment']; ?> <a target="_blank" href="http://sibme.com/privacy-policy.html"><?php echo $language_based_content['privacy_policy_payment']; ?></a>
        </p>
        <div style="clear:both"></div>


        <div class="span12"> 
            <div class="span2" style="margin-left: 0px;">
                <img alt="Braintree-gray" class="braintree" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/braintree-gray.png'); ?>" />
            </div>
            <div class="span6 braintree-p">
                <?php echo $language_based_content['our_payment_billing_payment']; ?>
                <br/>
                <?php echo $language_based_content['payment_credit_card_braintree'];  ?>
            </div>
            <div style="text-align:center;margin:20px 0px 0px 0px;"><img alt="Braintree-gray" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/cards.jpg'); ?>" /></div>
        </div>

    </div>

</form>


<script type="text/javascript">

    $(document).ready(function () {
        $("form").submit(function() { 
            $('#form_submit').attr('disabled','disabled');
        });
        $("#frmActiviation").validate({
            errorLabelContainer: $("#form1 div.error"),
            rules: {
//                amount: {
//                    greaterThanZero: true,
//                    number: true,
//                    max: 500,
//                    min: <?php //echo isset($student_fees) ? $student_fees : '60';   ?>
//                },
                card_number: {
                    digits: true,
                },
                card_cvv: {
                    digits: true,
                }

            }
        });

    });
    jQuery.validator.addMethod("greaterThanZero", function (value, element) {
        return this.optional(element) || (parseFloat(value) > 0);
    }, "* Amount must be greater than zero");

    $(document).on("click", "#flashMessage", function () {
        $("#flashMessage").fadeOut(200);
    });

</script>



<?php

/**
 * States Dropdown
 *
 * @uses check_select
 * @param string $post, the one to make "selected"
 * @param string $type, by default it shows abbreviations. 'abbrev', 'name' or 'mixed'
 * @return string
 */
function StateDropdown($post = null, $type = 'abbrev') {
    $states = array(
        array('AK', 'Alaska'),
        array('AL', 'Alabama'),
        array('AR', 'Arkansas'),
        array('AZ', 'Arizona'),
        array('CA', 'California'),
        array('CO', 'Colorado'),
        array('CT', 'Connecticut'),
        array('DC', 'District of Columbia'),
        array('DE', 'Delaware'),
        array('FL', 'Florida'),
        array('GA', 'Georgia'),
        array('HI', 'Hawaii'),
        array('IA', 'Iowa'),
        array('ID', 'Idaho'),
        array('IL', 'Illinois'),
        array('IN', 'Indiana'),
        array('KS', 'Kansas'),
        array('KY', 'Kentucky'),
        array('LA', 'Louisiana'),
        array('MA', 'Massachusetts'),
        array('MD', 'Maryland'),
        array('ME', 'Maine'),
        array('MI', 'Michigan'),
        array('MN', 'Minnesota'),
        array('MO', 'Missouri'),
        array('MS', 'Mississippi'),
        array('MT', 'Montana'),
        array('NC', 'North Carolina'),
        array('ND', 'North Dakota'),
        array('NE', 'Nebraska'),
        array('NH', 'New Hampshire'),
        array('NJ', 'New Jersey'),
        array('NM', 'New Mexico'),
        array('NV', 'Nevada'),
        array('NY', 'New York'),
        array('OH', 'Ohio'),
        array('OK', 'Oklahoma'),
        array('OR', 'Oregon'),
        array('PA', 'Pennsylvania'),
        array('PR', 'Puerto Rico'),
        array('RI', 'Rhode Island'),
        array('SC', 'South Carolina'),
        array('SD', 'South Dakota'),
        array('TN', 'Tennessee'),
        array('TX', 'Texas'),
        array('UT', 'Utah'),
        array('VA', 'Virginia'),
        array('VT', 'Vermont'),
        array('WA', 'Washington'),
        array('WI', 'Wisconsin'),
        array('WV', 'West Virginia'),
        array('WY', 'Wyoming')
    );

    $options = '<option value=""></option>';

    foreach ($states as $state) {
        if ($type == 'abbrev') {
            $options .= '<option value="' . $state[0] . '" ' . check_select($post, $state[0], false) . ' >' . $state[0] . '</option>' . "\n";
        } elseif ($type == 'name') {
            $options .= '<option value="' . $state[1] . '" ' . check_select($post, $state[1], false) . ' >' . $state[1] . '</option>' . "\n";
        } elseif ($type == 'mixed') {
            $options .= '<option value="' . $state[0] . '" ' . check_select($post, $state[0], false) . ' >' . $state[1] . '</option>' . "\n";
        }
    }

    echo $options;
}

function check_select($i, $m, $e = true) {
    if ($i != null) {
        if ($i == $m) {
            $var = ' selected="selected" ';
        } else {
            $var = '';
        }
    } else {
        $var = '';
    }
    if (!$e) {
        return $var;
    } else {
        echo $var;
    }
}
