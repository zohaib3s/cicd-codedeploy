

<script type="text/javascript">
    $(document).ready(function (e) {

        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        if (iOS) {
            var now = new Date().valueOf();
            setTimeout(function () {
                if (new Date().valueOf() - now > 100)
                    return;
                //window.location = "https://app.sibme.com/forgot_password?app=notinstalled";
                return;
            }, 50);


            window.location = "sibme://activate_user/<?php echo $verification_code; ?>/<?php echo $user_id; ?>";

        }

    });
</script>



<style type="text/css">
    #flashMessage
    {
        text-align: center;
    }
    .box-page .wrapper{width: 650px;}
    .page_contner{  text-align:center; padding: 0 35px;}
    .page_contner p{ color:#606060; font-size:13px; margin:0;}
    .page_contner p.green_cl{ color:#00a651; text-align:center;     font-size: 15px;
                              line-height: 18px;}

    .page_contner input[type=submit]{ background:#6fb828; border:0; border-radius:4px; display:inline-block; width:300px; margin:0 auto; margin-bottom:15px;}

    .page_contner button{background: #6fb828;
                         border: 0;
                         border-radius: 4px;
                         display: inline-block;
                         width: 309px;
                         margin: 0 auto;
                         margin-bottom: 15px;
                         padding: 12px;
                         color: #fff;
                         font-size: 15px;
                         text-transform: uppercase;
                         font-weight: bold;     margin-top: 15px;}

    .page_contner a{ color: #0b3244;
                     width: 300px;
                     display: inline-block;
                     border-radius: 4px;
                     padding: 9px 8px;
                     text-align: center;
                     border: 2px solid #0b3244;
                     margin: 16px 0 34px 0;
                     cursor: pointer;}
    .clr{ clear: both;}

    .page_contner h2{margin: 32px 0 0 0;
                     font-weight: 600;}

    .page_contner input[type=text]{margin-top: 15px; text-align: center;}
</style>

<div class="page_contner">
    <p class="green_cl">You have successfully registered for a <?php $this->Custom->get_site_settings('site_title') ?> account.  Please click the activation link in the email we just sent you to confirm your account.</p>

    <h2><?php echo $first_name . ' ' . $last_name; ?></h2>
    <p><?php echo $email; ?></p>
    <p><?php echo $company_name; ?></p>
<!--     <form accept-charset="UTF-8" action="<?php //echo $this->base . '/users/activate_user_code/' . $account_id . '/'.$user_id  ?>"  enctype="multipart/form-data"  method="post">
         <input required name="verification_code" type="text" placeholder="Enter Verification Code">
    <div class="clr"></div>
    <button type="submit">Submit</button>
    </form>-->
    <p>Please click the button below if you did not receive an email with a activation link.</p>
    <a href="<?php echo $this->base . '/users/resend_activation_code/' . $account_id . '/' . $user_id; ?>">Resend me verification link</a>

</div>


<script>

    $("#flashMessage").click(function () {
        $('#flashMessage').fadeOut();
    });

</script>
