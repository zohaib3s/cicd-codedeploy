<script type="text/javascript">
    $(document).ready(function (e) {
        //var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        if(iOS){
        var now = new Date().valueOf();
        setTimeout(function () { 
            if (new Date().valueOf() - now > 100) return;
            //window.location = "https://app.sibme.com/forgot_password?app=notinstalled";
            return;
        }, 50);
        window.location = "sibme://reset_password/?token=<?php echo $key;?>";
        }
        
        
            var ua = navigator.userAgent.toLowerCase();
            var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
            if(isAndroid) {
              var now = new Date().valueOf();
                    setTimeout(function () { 
                        if (new Date().valueOf() - now > 100) return;
                        //window.location = "https://app.sibme.com/forgot_password?app=notinstalled";
                        return;
                    }, 50);
                    window.location = "sibme://reset_password/?token=<?php echo $key;?>";
            }
        
        
        
    });

</script>
<div class="forgot_password_box">
    <div class="header header--lite">           
        <h4 class="header__title">
            <?php echo $language_based_content['choose_your_password']; ?>
        </h4>
    </div>
    <form accept-charset="UTF-8" action="<?php echo $this->base . '/Users/change_password/'.$key ?>" class="new_user" id="login-form" method="post">
        <input type="hidden" name="key" value="<?php echo $key ?>"/>
        <p class="input-group">
            <label><?php echo $language_based_content['new_password']; ?></label>
            <input type="password" value="" placeholder="<?php echo $language_based_content['new_password']; ?>" name="password" id="new-passowrd" class="size-large">
            <?php echo $language_based_content['minimum_six_char_cp']; ?>
        </p>
        <p class="input-group">
            <label><?php echo $language_based_content['retype_password']; ?></label>
            <input type="password" value="" placeholder="<?php echo $language_based_content['retype_password']; ?>" name="confirm_password" id="confirm-password" class="size-large">
        </p>
        <p class="input-group">           
            <button class="btn btn-orange btn-medium full-input" type="submit"><?php echo $language_based_content['save_changes_cp']; ?></button>
        </p>
    </form> 
</div>

