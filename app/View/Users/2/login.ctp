<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,700i" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">


<script type="text/javascript">
    $(document).ready(function (e) {
        var huddle_workspace = '<?php echo $huddle_workspace ?>';
        var is_goal = <?php echo !empty($is_goal_request)?$is_goal_request:'false';?>;
        //var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        var ua = navigator.userAgent.toLowerCase();
        var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
        if((iOS || isAndroid) && is_goal){
            alert("You have been redirected to the browser version of the Coaching Studio app because the Goals module is not presently available in the mobile app.  The browser version of sibme is not optimized for mobile.  We recommend that you view sibme Goals on the desktop.");
        }
        if (iOS) {
            var now = new Date().valueOf();
            setTimeout(function () {
                if (new Date().valueOf() - now > 100)
                    return;
                //window.location = "https://app.sibme.com/forgot_password?app=notinstalled";
                return;
            }, 50);

            if (huddle_workspace == 1)
            {
                window.location = "hmh://play_video/?huddleID=<?php echo $huddle_id; ?>&videoID=<?php echo $video_id; ?>&isWorkspace=false";
            }
            else if (huddle_workspace == 3)
            {
                window.location = "hmh://play_video/?huddleID=<?php echo $huddle_id; ?>&videoID=<?php echo $video_id; ?>&isWorkspace=true";
            }
            else
            {
                window.location = "hmh://play_video/";
            }
        }

       
        if (isAndroid) {
            var now = new Date().valueOf();
            setTimeout(function () {
                if (new Date().valueOf() - now > 100)
                    return;
                //window.location = "https://app.sibme.com/forgot_password?app=notinstalled";
                return;
            }, 50);

            if (huddle_workspace == 1)
            {
                window.location = "hmh://play_video/?huddleID=<?php echo $huddle_id; ?>&videoID=<?php echo $video_id; ?>&isWorkspace=false";
            }
            else if (huddle_workspace == 3)
            {
                window.location = "hmh://play_video/?huddleID=<?php echo $huddle_id; ?>&videoID=<?php echo $video_id; ?>&isWorkspace=true";
            }
            else
            {
                // window.location = "hmh://play_video/";
            }
        }

        $('.far').on('click', function (e) {
            $classValue = $(this).attr('class');
            // if ($classValue == 'far fa-eye') {
            //     $(this).removeClass('fa-eye');
            //     $(this).addClass('fa-eye-slash');
            //     $('#UserPassword').attr('type', 'password');

            // } else {
            //     $(this).removeClass('fa-eye-slash');
            //     $(this).addClass('fa-eye');
            //     $('#UserPassword').attr('type', 'text');
            // }

            if ($classValue == 'far fa-eye-slash') {
                $(this).removeClass('fa-eye-slash');
                $(this).addClass('fa-eye');
                $('#UserPassword').attr('type', 'password');

            } else {
                $(this).removeClass('fa-eye');
                $(this).addClass('fa-eye-slash');
                $('#UserPassword').attr('type', 'text');
            }
        })


    })
</script>

<?php
function get_user_browser() {
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $ub = '';
    if (preg_match('/MSIE/i', $u_agent)) {
        $ub = "ie";
    } elseif (preg_match('/Firefox/i', $u_agent)) {
        $ub = "firefox";
    } elseif (preg_match('/Safari/i', $u_agent)) {
        $ub = "safari";
    } elseif (preg_match('/Chrome/i', $u_agent)) {
        $ub = "chrome";
    } elseif (preg_match('/Flock/i', $u_agent)) {
        $ub = "flock";
    } elseif (preg_match('/Opera/i', $u_agent)) {
        $ub = "opera";
    }

    return $ub;
}

?>
<script type="text/javascript">
    $(document).ready(function () {
//        $('div.auth-form').css('top', '80px');
        $('#flashMessage').click(function (e) {
            $(this).slideUp('slow');
        })
    });

</script>


<div class="row auth-form">
    <?php if (get_user_browser() == 'ie' && preg_match('/(?i)msie [1-8]/', $_SERVER['HTTP_USER_AGENT'])): ?>
        <div class="message">We do not actively support IE 7 or 8. We strongly recommend that you upgrade to the most recent version of IE. If you are unable to do so, please use the most recent version of Google Chrome, Safari, or FireFox.</div>
    <?php endif; ?>

    <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/login' ?>" class="new_user" id="login-form" method="post">

        <?php
        /*
        if (isset($_SESSION['site_id']) && $_SESSION['site_id'] == 2){
        ?>
        <div id="maintenance" class="message error" style="cursor: pointer;">The HMH Blended Coaching Studio accounts transition planned for Friday, March 15 has been moved to Thursday, March 21. Please continue to use <a href="https://app.sibme.com" class="app_link">app.sibme.com</a> and the Sibme mobile apps on iOS and Android until that date.</div>
        <?php 
        }
        */
        ?>

        <div class="login_container" >
        <?php
            echo $this->Session->flash();
            ?>
            <?php
            if ($this->Session->read('error') != '') {
                echo $this->Session->read('error');
                unset($_SESSION['error']);
            }
            ?>
            
            <?php if( isset($_SESSION['deactivate_user_msg']) && !empty($_SESSION['deactivate_user_msg']) ) { ?>
            <div id="flashMessage" class="message" style="cursor: pointer;"><?php echo $_SESSION['deactivate_user_msg']; ?></div>
          <?php }
            $_SESSION['deactivate_user_msg'] = '' ;
             ?>

            <div class="login_box">
                <div class="login_logo">
                    <img src="<?php echo $this->Custom->get_site_settings('login_logo'); ?>" alt=""/>
                </div>
                <h1><?php echo $this->Custom->get_site_settings('login_text'); ?></h1>
                <?php
                $cerdentials = '';
                if (!isset($account_id))
                    $account_id = '';
                if (!isset($auth_token))
                    $auth_token = '';
                if (!isset($user_id))
                    $user_id = '';
                if (!empty($account_id) && !empty($auth_token) && !empty($user_id)) {
                    $cerdentials = '/' . $account_id . '/' . $auth_token . '/' . $user_id;
                } else {
                    $cerdentials = '';
                }
                $color = $this->Custom->get_site_settings('login_btn_color');
                ?>

                <div style="margin:0;padding:0;display:inline">
                    <input name="utf8" type="hidden" value="✓">
                    <input name="inv_account_id" type="hidden" value="<?php echo $account_id; ?>">
                    <input name="inv_auth_token" type="hidden" value="<?php echo $auth_token; ?>">
                    <input name="inv_user_id" type="hidden" value="<?php echo $user_id; ?>">
                </div>
                <div class="login_field_box">
                    <input type="text" name="data[User][username]" placeholder="Email..." id="UserUsername" class="email_icon">
                    <?php echo $this->Form->error('users.username'); ?>
                    <div class="password_outr">
                        <input type="password" name="data[User][password]" placeholder="Password..." id="UserPassword" class="password_icon">
                        <i class="far fa-eye" style="cursor: pointer;"></i>
                    </div>
                    

                    <div class="signin_btn">
                        <button type="submit" style="background:<?php echo $color; ?>"  type="button"><?php echo $language_based_content['sign_in']; ?></button>
                    </div>
                    <div class="clear"></div>
                    <div class="forget_pass">
                    <div class="remeber_me"><input name="data[User][remember_me]" type="checkbox" value="1"> Remember me</div>
                    <a href="<?php echo $this->base . '/users/forgot_password' ?>">Forgot Password?</a></div>
                </div>



                <div class="or_txt">
                    <span><?php echo $language_based_content['or_login']; ?></span>
                </div>

                <a class="google_signin_btn" href="<?php echo $this->base . '/GoogleApi/google_login' ?>">
                    <img src="<?php echo $this->webroot . 'img/gicon.png' ?>" alt=""/>Sign In with Google</a>
              <!--<button class="google_signin_btn" type="button"><img src="<?php // echo $this->webroot . 'img/google_icon.png' ?>" alt=""/> Sign In with Google</button>-->


              <!-- <div class="or_txt">
                    <span><?php echo $language_based_content['or_login']; ?></span>
                </div> -->

                <a data-toggle="modal" data-target="#sso_login_modal" class="sso_signin_btn" href="#">
                <span class="sso_img">
                    <img src="<?php echo $this->webroot . 'img/sso-icon.svg' ?>" alt=""/>
                    </span><?php echo $language_based_content['sign_in_sso']; ?>
                    
                    <!-- <img src="<?php echo $this->webroot . 'img/sso-icon.png' ?>" alt=""/> Sign In with SSO -->
                </a>

                <div class="terms_policy">
                        <?php echo $language_based_content['by_signing_agreement']; ?> <a target="_blank" class="blue_color" href="http://sibme.com/privacy-policy.html"> <?php echo $language_based_content['privacy_policy_payment']; ?></a> <?php echo $language_based_content['and']; ?> <a target="_blank" class="blue_color"  href="http://sibme.com/terms.html"><?php echo $language_based_content['terms_of_use_payment']; ?></a>.
                </div>

            </div>
            <input name="data[remember_me]" type="hidden" value="0">


        </div>
    </form>
</div>


<!-- SSO Modal -->
<div class="modal fade" id="sso_login_modal" tabindex="-1" role="dialog" aria-labelledby="ssoModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ssoModalLabel"><?php echo $language_based_content['sign_in_sso']; ?></h5>
      </div>
      <div class="modal-body">
        <div class="login_field_box">
            <input type="text" id="sso_email" name="sso_email" placeholder="<?php echo $language_based_content['email_placeholder_login']; ?>" class="email_icon">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $language_based_content['sign_in_model_close']; ?></button>
        <button type="button" id="btn_idp" class="btn btn-primary" style="background:<?php echo $color; ?>;color: white;"><?php echo $language_based_content['sign_in']; ?></button>
      </div>
    </div>
  </div>
</div>






<!--<div class="box">
    <div class="header header--lite header-huddle">
<?php
$chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/keys-login.png');
echo $this->Html->image($chimg, array('width' => '20', 'height' => '32', 'alt' => 'Keys', 'allign' => 'middle'));
?>
        <h4 class="header__title">Log into Sibme</h4>
    </div>
<?php
$cerdentials = '';
if (!isset($account_id))
    $account_id = '';
if (!isset($auth_token))
    $auth_token = '';
if (!isset($user_id))
    $user_id = '';


if (!empty($account_id) && !empty($auth_token) && !empty($user_id)) {
    $cerdentials = '/' . $account_id . '/' . $auth_token . '/' . $user_id;
} else {
    $cerdentials = '';
}
?>

    <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/login' ?>" class="new_user" id="login-form" method="post">
        <div style="margin:0;padding:0;display:inline">
            <input name="utf8" type="hidden" value="✓">
            <input name="inv_account_id" type="hidden" value="<?php echo $account_id; ?>">
            <input name="inv_auth_token" type="hidden" value="<?php echo $auth_token; ?>">
            <input name="inv_user_id" type="hidden" value="<?php echo $user_id; ?>">
        </div>
        <div class="input-group">
            <label for="UserUsername">Username or Email</label>
            <input name="data[User][username]" class="text-input" type="text" id="UserUsername">
<?php echo $this->Form->error('users.username'); ?>
        </div>

        <div class="input-group">
            <label for="UserPassword">Password</label>
            <input name="data[User][password]" class="text-input" type="password" id="UserPassword">
        </div>
        <p>
            <label class="checkbox-label">
                <input name="data[remember_me]" type="hidden" value="0">
                <input class="inline" id="user_remember_me" name="data[User][remember_me]" type="checkbox" value="1"> Remember me
            </label>
        </p>
        <button type="submit" class="btn btn-orange btn-medium">Sign In</button>

<?php
echo $this->Html->image("login-google.jpg", array(
    "alt" => "Signin with Google",
    'url' => array('controller' => 'GoogleApi', 'action' => 'google_login', 'Google')
));
?>
        <span class="text-center islet" style="display:none;">ATTENTION ALL iOS USERS: Please update iOS app to version <a target="_blank" href="https://itunes.apple.com/us/app/sibme/id629939151?mt=8">2.1.3</a>. You are no longer able to use previous iOS versions.</span>
    </form>
</div>
<div class="text-center islet">
<?php echo $this->Html->link("Forgot Password?", array("controller" => "users", "action" => "forgot_password"), array('class' => 'blue-link blue-link-login')); ?>

</div>-->


<style>
    div#hs-beacon {
        display: none;
    }
    html, body{    background: #f3f4f5;}
    .login_container{
        width:700px;
        margin:0 auto;
        margin-top:0px; background:#fff;
        box-shadow:0px 0px 5px rgba(0,0,0,0.16);
    }
    .auth-form { width:700px;}
    .login_box{
        max-width: 368px;    margin: 0 auto;
        text-align:center;
        padding:50px 0px;
    }
    .login_logo{
        padding:0px 0px 20px 0px;
    }
    .login_logo img {max-width:368px;}
    .login_box h1{
        color:#0d3244;
        font-family: 'Roboto', sans-serif;
        font-size:18px;
        font-weight:normal;
        margin:0px; font-weight:600; padding: 21px 0;
    }
    .login_box h1 img{
        vertical-align:middle;
    }
    .login_field_box{
        margin:15px 0px 0 0;
    }
    .login_field_box input[type="text"], .login_field_box input[type="password"]{
        width:365px;
        box-sizing:border-box;
        border:solid 2px #d9d9d9;
        padding:10px 5px;
        font-family: 'Roboto', sans-serif;
        font-weight:500;
        font-size:14px;
        outline:none;
        color:#acb3b7;     height: 46px;
    }
    .email_icon{

        padding-left:12px !important;
        margin-bottom:22px;
    }
    .password_icon{

        padding-left:15px !important;
        margin-bottom:10px;
    }
    .blue_color{
        color:#2b88a9
    }
    .signin_btn button{
        /* background:#848688; */
        background:#039542;
        border-radius:3px;
        border:0px;

        color:#fff;


        padding:12px 0px;
        cursor:pointer;
        outline:none;    display: block;
        width: 100%; font-size: 16px; font-family: 'Roboto', sans-serif;
    }
    .signin_btn button:hover{
        background:#636465;
    }
    .signin_btn button:active{
        border-bottom:solid 2px #cdcdcd;
    }
    .signin_btn { float:none; width:100%; padding-top:10px;}
    .or_txt{
        text-align:center;
        margin:0;
        border-bottom: 2px solid  #ddd;
    }
    .or_txt span {    display: inline-block;
                      background: #fff;
                      position: relative;
                      top: 9px;
                      padding: 0 12px;font-family: 'Roboto', sans-serif; font-weight: 600;
    }
    .google_signin_btn{
        display: block;
        background:#4285f4;
        font-weight: 600;
        border-radius:0px;
        border:0px;

        color:#fff!important;
        font-size:17px;

        padding:10px 0px;
        cursor:pointer;
        outline:none;
        margin: 0 auto;    border-radius: 2px;
        text-decoration: none;margin-top: 30px;
    }
    .google_signin_btn:hover{
        background:#ef594b;
    }
    .google_signin_btn:active{
        background:#ef594b;
        border-bottom:solid 2px #cdcdcd;
    }
    .google_signin_btn img{
        vertical-align: middle;
        width: 40px;
        background: #fff;
        padding: 9px;
        float: left;
        position: relative;
        top: -8.5px;
        left: 1px;
    }
    .remeber_me{
        padding:0px;

        font-size:14px;
        color:#616161;
        float:left;
        width: 50%;
        text-align: left;
    }
    .remeber_me input[type="checkbox"]{
        position:relative;
        top:2px;
        margin-right: 3px;
    }
    .forget_pass{
        padding: 7px 2px 11px 1px;
        font-size:13px;
        text-align: right;

    }
    .forget_pass a{
        color:#616161;
        font-family: 'Roboto', sans-serif;
        font-weight: 400;
        text-decoration: underline;     font-size: 13px;
    }
    .forget_pass a:hover{


    }
    .g-error-msg{
        cursor: pointer;
        text-align: center;
        margin-top: 10px;
        padding: 10px;
        background: lightcoral;
        border-radius: 5px;
        color: #fff;
        /*font-weight: bold;*/
    }

    #flashMessage {
        /*width: 351px!important;*/
        text-align: center;
        position: relative;
        top: -50px;
        color: #32613c;
        background: #d3ecda;
        border-color: #d3ecda;
    }
    .password_outr { position: relative;}
    .password_outr .far{    position: absolute; right: 12px;top: 17px;}
    #main { margin-top: 0;}

    #maintenance {
        background: #f8d7da !important;
        padding: 20px;
        margin-bottom: 30px !important;
        border-radius: 8px !important;
        width: 700px;
        margin: 0 auto;
        color: #721c24 !important;
        border: 1px solid #f5c6cb !important;
        font-size: 16px;    
        font-weight: 600;
    }

    #maintenance a{    
        color: #721c24 !important;
        font-weight: 600;     
        text-decoration: underline;
    }
    .sso_signin_btn{
        display: block;
        background:#8E5FE5!important;
        font-weight: 600;
        border-radius:0px;
        border:0px;

        color:#fff!important;
        font-size:17px;

        padding:10px 0px;
        cursor:pointer;
        outline:none;
        margin: 0 auto;    border-radius: 2px;
        text-decoration: none;margin-top: 20px;
    }
    .sso_signin_btn img{
        height: 36px;
        float: left;
        margin-left: 2px;
        margin-top: -6px;
    }
    .sso_img {
        display: inline-block;
    background: #fff;
    float: left;
    padding: 8px 8px 5px 3px;
    width: 40px;
    height: 41px;
    position: relative;
    top: -9px;
    left: 1px;
    }

 
    .terms_policy {
    /* font-family: Roboto-Regular; */
    padding: 20px 0px;
    font-size: 12px;
    /* margin: 0; */
    text-align: left; 
    }
  

</style>
<script type="text/javascript">

$("#btn_idp").click(function(e){
    e.preventDefault();
    $.ajax({url: api_url + 'get/sso-link',
        data: {
                email: $("#sso_email").val(),
                redirect_to: home_url + 'users/sso_login_callback'
              },
        type: 'post',
        dataType: 'json',
        success: function (response) {
            if(response.success==true){
                window.location.href = response.idp_login_url;
            } else {
                alert(response.message);
            }
        }
    });
});

</script>