<script type="text/javascript">
    $(document).ready(function (e) {
        var huddle_workspace = '<?php echo $huddle_workspace ?>';
        //var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
        var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
        if (iOS) {
            var now = new Date().valueOf();
            setTimeout(function () {
                if (new Date().valueOf() - now > 100)
                    return;
                //window.location = "https://app.sibme.com/forgot_password?app=notinstalled";
                return;
            }, 50);

            if (huddle_workspace == 1)
            {
                window.location = "sibme://play_video/?huddleID=<?php echo $huddle_id; ?>&videoID=<?php echo $video_id; ?>&isWorkspace=false";
            }
            else if (huddle_workspace == 3)
            {
                window.location = "sibme://play_video/?huddleID=<?php echo $huddle_id; ?>&videoID=<?php echo $video_id; ?>&isWorkspace=true";
            }
            else
            {
                window.location = "sibme://play_video/";
            }
        }

        var ua = navigator.userAgent.toLowerCase();
        var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
        if (isAndroid) {
            var now = new Date().valueOf();
            setTimeout(function () {
                if (new Date().valueOf() - now > 100)
                    return;
                //window.location = "https://app.sibme.com/forgot_password?app=notinstalled";
                return;
            }, 50);

            if (huddle_workspace == 1)
            {
                // window.location = "sibme://play_video/?huddleID=<?php echo $huddle_id; ?>&videoID=<?php echo $video_id; ?>&isWorkspace=false";
            }
            else if (huddle_workspace == 3)
            {
                // window.location = "sibme://play_video/?huddleID=<?php echo $huddle_id; ?>&videoID=<?php echo $video_id; ?>&isWorkspace=true";
            }
            else if (huddle_workspace == 4)
            {
                // window.location = "sibme://play_video/?huddleID=<?php echo $huddle_id; ?>&commentID=<?php echo $comment_id; ?>&isWorkspace=false";
            }
            else
            {
                // window.location = "sibme://play_video/";
            }
        }




    })
</script>

<?php

function get_user_browser() {
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $ub = '';
    if (preg_match('/MSIE/i', $u_agent)) {
        $ub = "ie";
    } elseif (preg_match('/Firefox/i', $u_agent)) {
        $ub = "firefox";
    } elseif (preg_match('/Safari/i', $u_agent)) {
        $ub = "safari";
    } elseif (preg_match('/Chrome/i', $u_agent)) {
        $ub = "chrome";
    } elseif (preg_match('/Flock/i', $u_agent)) {
        $ub = "flock";
    } elseif (preg_match('/Opera/i', $u_agent)) {
        $ub = "opera";
    }

    return $ub;
}
?>
<script type="text/javascript">
    $(document).ready(function () {
//        $('div.auth-form').css('top', '80px');
        $('#flashMessage').click(function (e) {
            $(this).slideUp('slow');
        })
    });

</script>


<div class="row auth-form">
    <?php if (get_user_browser() == 'ie' && preg_match('/(?i)msie [1-8]/', $_SERVER['HTTP_USER_AGENT'])): ?>
        <div class="message">We do not actively support IE 7 or 8. We strongly recommend that you upgrade to the most recent version of IE. If you are unable to do so, please use the most recent version of Google Chrome, Safari, or FireFox.</div>
    <?php endif; ?>
    <?php echo $this->Form->create(NULL, ['url' => ['controller' => 'users', 'action' => 'login'], 'style' => 'display: block;']); ?>

<!--<form accept-charset="UTF-8" action="<?php echo $this->base . '/users/login' ?>" class="new_user" id="login-form" method="post">-->
    <div class="login_container" >
        <?php
        echo $this->Session->flash();
        ?>
        <?php
        if ($this->Session->read('error') != '') {
            echo $this->Session->read('error');
            unset($_SESSION['error']);
        }
        ?>
        <div class="login_box">
            <div class="login_logo">
                <img src="<?php echo $this->webroot . 'img/sibme_logo.jpg' ?>" width="275" height="89" alt=""/>
            </div>
            <h1><img src="<?php echo $this->webroot . 'img/keys.png' ?>" width="22" height="33" alt=""/> Login to Sibme</h1>
            <?php
            $cerdentials = '';
            if (!isset($account_id))
                $account_id = '';
            if (!isset($auth_token))
                $auth_token = '';
            if (!isset($user_id))
                $user_id = '';
            if (!empty($account_id) && !empty($auth_token) && !empty($user_id)) {
                $cerdentials = '/' . $account_id . '/' . $auth_token . '/' . $user_id;
            } else {
                $cerdentials = '';
            }
            ?>
            <div class="login_box">
                <div class="login_logo">
                    <img src="<?php echo $this->webroot . 'img/sibme_logo.jpg' ?>" width="275" height="89" alt=""/>
                </div>
                <h1><img src="<?php echo $this->webroot . 'img/keys.png' ?>" width="22" height="33" alt=""/> Login to Sibme</h1>
                <?php
                $cerdentials = '';
                if (!isset($account_id))
                    $account_id = '';
                if (!isset($auth_token))
                    $auth_token = '';
                if (!isset($user_id))
                    $user_id = '';
                if (!empty($account_id) && !empty($auth_token) && !empty($user_id)) {
                    $cerdentials = '/' . $account_id . '/' . $auth_token . '/' . $user_id;
                } else {
                    $cerdentials = '';
                }
                ?>

                <div style="margin:0;padding:0;display:inline">
                    <input name="utf8" type="hidden" value="✓">
                    <input name="inv_account_id" type="hidden" value="<?php echo $account_id; ?>">
                    <input name="inv_auth_token" type="hidden" value="<?php echo $auth_token; ?>">
                    <input name="inv_user_id" type="hidden" value="<?php echo $user_id; ?>">
                </div>
                <div class="login_field_box">
                    <input type="text" name="data[User][username]" placeholder="Email" id="UserUsername" class="email_icon">
                    <?php echo $this->Form->error('users.username'); ?>
                    <input type="password" name="data[User][password]" placeholder="Password" id="UserPassword" class="password_icon">
                    <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="signin_btn" type="button">Sign In</button>
                </div>



                <div class="or_txt"><img src="<?php echo $this->webroot . 'img/or_txt.png' ?>" width="186" height="10" alt=""/></div>
                <a class="google_signin_btn" href="<?php echo $this->base . '/GoogleApi/google_login' ?>"><img src="<?php echo $this->webroot . 'img/google_icon.png' ?>" alt=""/> Sign In with Google</a>
                <!--<button class="google_signin_btn" type="button"><img src="<?php echo $this->webroot . 'img/google_icon.png' ?>" alt=""/> Sign In with Google</button>-->

            <div style="margin:0;padding:0;display:inline">
                <input name="utf8" type="hidden" value="✓">
                <input name="inv_account_id" type="hidden" value="<?php echo $account_id; ?>">
                <input name="inv_auth_token" type="hidden" value="<?php echo $auth_token; ?>">
                <input name="inv_user_id" type="hidden" value="<?php echo $user_id; ?>">
            </div>
            <div class="login_field_box">
                <input type="text" name="data[User][username]" placeholder="Email" id="UserUsername" class="email_icon">
                <?php echo $this->Form->error('users.username'); ?>
                <input type="password" name="data[User][password]" placeholder="Password" id="UserPassword" class="password_icon">
                <button type="submit" class="signin_btn" type="button">Sign In</button>
            </div>



            <div class="or_txt"><img src="<?php echo $this->webroot . 'img/or_txt.png' ?>" width="186" height="10" alt=""/></div>
            <a class="google_signin_btn" href="<?php echo $this->base . '/GoogleApi/google_login' ?>"><img src="<?php echo $this->webroot . 'img/google_icon.png' ?>" alt=""/> Sign In with Google</a>
            <!--<button class="google_signin_btn" type="button"><img src="<?php echo $this->webroot . 'img/google_icon.png' ?>" alt=""/> Sign In with Google</button>-->

        </div>
        <input name="data[remember_me]" type="hidden" value="0">
        <div class="remeber_me"><input name="data[User][remember_me]" type="checkbox" value="1"> Remember me</div>
        <div class="forget_pass"><a href="<?php echo $this->base . '/users/forgot_password' ?>">Forgot Password?</a></div>
    </div>
</form>
</div>







<!--<div class="box">
    <div class="header header--lite header-huddle">
<?php
$chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/keys-login.png');
echo $this->Html->image($chimg, array('width' => '20', 'height' => '32', 'alt' => 'Keys', 'allign' => 'middle'));
?>
        <h4 class="header__title">Log into Sibme</h4>
    </div>
<?php
$cerdentials = '';
if (!isset($account_id))
    $account_id = '';
if (!isset($auth_token))
    $auth_token = '';
if (!isset($user_id))
    $user_id = '';


if (!empty($account_id) && !empty($auth_token) && !empty($user_id)) {
    $cerdentials = '/' . $account_id . '/' . $auth_token . '/' . $user_id;
} else {
    $cerdentials = '';
}
?>

    <form accept-charset="UTF-8" action="<?php echo $this->base . '/users/login' ?>" class="new_user" id="login-form" method="post">
        <div style="margin:0;padding:0;display:inline">
            <input name="utf8" type="hidden" value="✓">
            <input name="inv_account_id" type="hidden" value="<?php echo $account_id; ?>">
            <input name="inv_auth_token" type="hidden" value="<?php echo $auth_token; ?>">
            <input name="inv_user_id" type="hidden" value="<?php echo $user_id; ?>">
        </div>
        <div class="input-group">
            <label for="UserUsername">Username or Email</label>
            <input name="data[User][username]" class="text-input" type="text" id="UserUsername">
<?php echo $this->Form->error('users.username'); ?>
        </div>

        <div class="input-group">
            <label for="UserPassword">Password</label>
            <input name="data[User][password]" class="text-input" type="password" id="UserPassword">
        </div>
        <p>
            <label class="checkbox-label">
                <input name="data[remember_me]" type="hidden" value="0">
                <input class="inline" id="user_remember_me" name="data[User][remember_me]" type="checkbox" value="1"> Remember me
            </label>
        </p>
        <button type="submit" class="btn btn-orange btn-medium">Sign In</button>

<?php
echo $this->Html->image("login-google.jpg", array(
    "alt" => "Signin with Google",
    'url' => array('controller' => 'GoogleApi', 'action' => 'google_login', 'Google')
));
?>
        <span class="text-center islet" style="display:none;">ATTENTION ALL iOS USERS: Please update iOS app to version <a target="_blank" href="https://itunes.apple.com/us/app/sibme/id629939151?mt=8">2.1.3</a>. You are no longer able to use previous iOS versions.</span>
    </form>
</div>
<div class="text-center islet">
<?php echo $this->Html->link("Forgot Password?", array("controller" => "users", "action" => "forgot_password"), array('class' => 'blue-link blue-link-login')); ?>

</div>-->


<style>
    .login_container{
        width:450px;
        margin:0 auto;
        margin-top:0px;
    }
    .login_box{
        background:#fff;
        box-shadow:0px 0px 5px rgba(0,0,0,0.16);
        text-align:center;
        padding:30px 0px;
    }
    .login_logo{
        padding:0px 0px 20px 0px;
    }
    .login_box h1{
        color:#0d3244;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        font-size:18px;
        font-weight:normal;
        margin:0px;
    }
    .login_box h1 img{
        vertical-align:middle;
    }
    .login_field_box{
        margin:15px 0px;
    }
    .login_field_box input[type="text"], .login_field_box input[type="password"]{
        width:365px;
        box-sizing:border-box;
        border:solid 2px #d9d9d9;
        padding:10px 5px;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        font-weight:500;
        font-size:14px;
        outline:none;
        color:#acb3b7;
    }
    .email_icon{
        background: url(/img/login_user.png) center left no-repeat;
        padding-left:28px !important;
        margin-bottom:16px;
    }
    .password_icon{
        background: url(/img/login_lock.png) center left no-repeat;
        padding-left:28px !important;
        margin-bottom:16px;
    }
    .signin_btn{
        background:#5daf46;
        width:365px;
        border-radius:3px;
        border:0px;
        border-bottom:solid 2px #dbdbdb;
        color:#fff;
        font-size:20px;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        padding:12px 0px;
        cursor:pointer;
        outline:none;
    }
    .signin_btn:hover{
        background:#74d05b;
    }
    .signin_btn:active{
        border-bottom:solid 2px #cdcdcd;
    }
    .or_txt{
        text-align:center;
        margin:15px 0px;
    }
    .google_signin_btn{
        display: block;
        background:#dc4e41 !important;
        width:275px;
        border-radius:3px;
        border:0px;
        border-bottom:solid 2px #dbdbdb;
        color:#fff!important;
        font-size:17px;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        padding:12px 0px;
        cursor:pointer;
        outline:none;
        margin: 0 auto;
        text-decoration: none;
    }
    .google_signin_btn:hover{
        background:#ef594b;
    }
    .google_signin_btn:active{
        background:#ef594b;
        border-bottom:solid 2px #cdcdcd;
    }
    .google_signin_btn img{
        vertical-align:middle;
    }
    .remeber_me{
        padding:8px 0px;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        font-size:13px;
        color:#8f9294;
        float:left;
    }
    .remeber_me input[type="checkbox"]{
        position:relative;
        top:2px;
    }
    .forget_pass{
        padding:8px 0px;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        font-size:13px;
        color:#76b863;
        float:right;
    }
    .forget_pass a{
        color:#76b863;
    }
    .forget_pass a:hover{
        color:#76b863;
        text-decoration:none;
    }
    .g-error-msg{
        cursor: pointer;
        text-align: center;
        margin-top: 10px;
        padding: 10px;
        background: lightcoral;
        border-radius: 5px;
        color: #fff;
        font-weight: bold;
    }

    #flashMessage {
        margin-bottom: 10px;
        width: 451px!important;
        text-align: center;
    }

</style>
