<style>

.message {
    color: #878314;
    background: #fefccb;
    border-color: #e5db55;
    position: absolute;
    bottom: 125px;
    background: none;
    color: red;
    border: 0;
    font-size: 20px;
    width: 100%; margin:0;
    text-align: center;
}
.two_auth {    font-family: sans-serif;
    text-align: center; padding-bottom: 80px;}
	
	.two_auth img{     max-width: 150px;
    margin-top: 80px;}
	
	.two_auth h1{ margin: 0;
    padding: 40px 0;
    font-size: 50px;
    color: #fff;
    background: #000;
    font-weight: 100;
    margin-top: 40px;}
	body, html{ margin:0; padding:0;background: #fff;}
	
	.two_auth p{    max-width: 903px;
    margin: 0 auto;
    padding: 40px 0;
    font-size: 24px;}
	
	
	.two_auth form{    padding-bottom: 0px;
    max-width: 900px;
    margin: 0 auto;     text-align: right;}
	
	.two_auth form input[type=text]{display: block;
    width: 100%;
    height: 40px;
    border: 1px solid #ddd;
    margin-bottom: 23px;     text-align: left;
}
.two_auth form input[type=submit], .two_auth form a{ background:none; border:1px solid #333;margin-top: 23px; padding:10px 33px; font-size:18px; color:#333; cursor:pointer; border-radius:4px; text-decoration:none !important; margin-left:10px;height: 45px;float: right;font-weight: 400;}
.two_auth form input[type=submit]:hover, .two_auth form a:hover{    background: #28A745;
    border: 1px solid #28A745;
    color: #fff;}
@media screen and (max-width:768px) {
 .message {
    color: #878314;
    background: #FEFCCB;
    border-color: #E5DB55;
    position: absolute;
    bottom: 127px;
    background: none;
    color: red;
    border: 0;
    font-size: 14px;
    width: 92%;
    margin: 0;
    text-align: center;
    line-height: 17px;
}
}
</style>
<div class="two_auth">
<img src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/new/logo-dark.svg'); ?>" />
    <h1>Two Factor Authentication</h1>
    <p>Please enter the code that was emailed to the given email address. If the email is not in your inbox, please check your spam folder.  </p> 
    <form accept-charset="UTF-8" action="<?php echo $this->base . '/Users/two_factor_auth/' . $account_id . '/' . $user_id . '/' . $type ?>" class="signup island compact" enctype="multipart/form-data" id="frmActiviation" method="post" style="padding-bottom: 0px;">
        <input name = "verification_code" type = "text" required >
        <input type = "submit" value = "Enter"> 
        <a href = "<?php echo $this->base . '/Users/sibme_registration/' . base64_encode($account_id) ?>">Back</a>
               
    </form>
</div>