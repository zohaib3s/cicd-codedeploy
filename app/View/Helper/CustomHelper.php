<?php

use Aws\S3\S3Client;
use Aws\CloudFront\CloudFrontClient;
use FFMpeg\FFMpeg;
use FFMpeg\FFProbe\DataMapping\StreamCollection;
use FFMpeg\FFProbe\DataMapping\Stream;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Filters\Video\ResizeFilter;
require_once dirname(__FILE__).'/../../aws3/autoload.php';
use Aws3\TranscribeService\TranscribeServiceClient;

App::import('Vendor', 'PushNotifications');

/**
 * Html Helper class file.
 *
 * Simplifies the construction of HTML elements.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Helper
 * @since         CakePHP(tm) v 0.9.1
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('AppHelper', 'View/Helper');
App::uses('CakeResponse', 'Network');
App::uses('HttpSocket', 'Network/Http');
App::uses('AppController', 'Controller');

/**
 * Html Helper class for easy use of HTML widgets.
 *
 * HtmlHelper encloses all methods needed while working with HTML pages.
 *
 * @package       Cake.View.Helper
 * @link http://book.cakephp.org/2.0/en/core-libraries/helpers/html.html
 */
class CustomHelper extends AppHelper {

    public $uses = array('AccountFolderMetaData','Document');
    public $site_id = '';
    public $lang_translations = null;

    public function __construct(\View $View, $settings = array()) {
        parent::__construct($View, $settings);

        if (isset($_SESSION['site_id']) && $_SESSION['site_id'] != 1) {
            $this->site_id = $_SESSION['site_id'];
        } else {
            $this->site_id = 1;
        }
    }

    /**
     * Reference to the Response object
     *
     * @var CakeResponse
     */
    public $docs = 'wordpress';
    public $pdf = 'acro';
    public $xlsx = 'x-icon';
    public $default = 'file';
    public $ppt = 'ppt';
    public $image = 'image';

    function getIcons($file) {
        $ext = explode('.', $file);
        if (is_array($ext) && count($ext) > 0) {
            $extension = count($ext) > 0 ? strtolower(end($ext)) : '';
            if ($extension == 'doc' || $extension == 'docx') {
                return $icon_class = $this->docs;
            } elseif ($extension == 'xlsx' || $extension == 'xls') {
                return $icon_class = $this->xlsx;
            } elseif ($extension == 'pdf') {
                return $icon_class = $this->pdf;
            } elseif ($extension == 'pptx' || $extension == 'ppt') {
                return $icon_class = $this->ppt;
            } elseif ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif') {
                return $icon_class = $this->image;
            } else {
                return $this->default;
            }
        }
    }

    function getSecureAmazonUrl($file, $name = null) {
        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
        $aws_bucket = Configure::read('bucket_name');
        $expires = '+240 minutes';

        // Create an Amazon S3 client object
        $client = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));

        $url = $client->getObjectUrl($aws_bucket, $file, $expires, array(
            'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : basename($file)),
        ));

        return $url;
    }

    function getSecureAmazonSibmeUrl($file, $name = null) {
        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
        $aws_bucket = Configure::read('bucket_name_cdn');
        $expires = '+240 minutes';

        // Create an Amazon S3 client object
        $client = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));

        $url = $client->getObjectUrl($aws_bucket, $file, $expires, array(
            'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : basename($file)),
        ));

        return $url;
    }

    function getSecureSibmecdnUrl($file, $name = null) {

        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
        $aws_bucket = Configure::read('bucket_name_cdn');
        $expires = '+240 minutes';

        // Create an Amazon S3 client object
        $client = S3Client::factory(array(
                    'key' => $aws_access_key_id,
                    'secret' => $aws_secret_key
        ));

        $url = $client->getObjectUrl($aws_bucket, $file, $expires, array(
            'ResponseContentDisposition' => 'attachment; filename=' . ($name ? : basename($file)),
        ));

        return $url;
    }

    function getSecureSibmecdnImageUrl($file, $name = null) {

        $aws_access_key_id = Configure::read('access_key_id');
        $aws_secret_key = Configure::read('secret_access_key');
        $aws_bucket = Configure::read('bucket_name_cdn');
        $expires = '+240 minutes';

        $url = "https://s3.amazonaws.com/" . $aws_bucket . "/" . $file;

        return $url;
    }

    function getSecureAmazonCloudFrontUrl($resource, $name = '', $timeout = 600, $useHTTP = false) {


        //This comes from key pair you generated for cloudfront
        $keyPairId = Configure::read('cloudfront_keypair');

        //Read Cloudfront Private Key Pair
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => realpath(dirname(__FILE__) . '/../..') . "/Config/cloudfront_key.pem",
                    'key_pair_id' => $keyPairId,
        ));

        $streamHostUrl = Configure::read('cloudfront_host_url');

        if ($useHTTP == true) {
            $streamHostUrl = str_replace("https://", "http://", $streamHostUrl);
        }
        $resourceKey = $resource;
        $expires = time() + 21600;

        //Construct the URL
        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
        ));

        return $signedUrlCannedPolicy;
    }

    function getSecureSibmeCdnCloudFrontUrl($resource, $name = '', $timeout = 600) {


        //This comes from key pair you generated for cloudfront
        $keyPairId = Configure::read('cloudfront_keypair');

        //Read Cloudfront Private Key Pair
        $cloudFront = CloudFrontClient::factory(array(
                    'private_key' => realpath(dirname(__FILE__) . '/../..') . "/Config/cloudfront_key.pem",
                    'key_pair_id' => $keyPairId,
        ));

        $streamHostUrl = Configure::read('sibmecdn_host_url');
        $resourceKey = $resource;
        $expires = time() + 21600;

        //Construct the URL
        $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
            'url' => $streamHostUrl . '/' . $resourceKey,
            'expires' => $expires,
        ));

        return $signedUrlCannedPolicy;
    }

    function hmacsha1($key, $data) {
        $blocksize = 64;
        $hashfunc = 'sha1';
        if (strlen($key) > $blocksize)
            $key = pack('H*', $hashfunc($key));
        $key = str_pad($key, $blocksize, chr(0x00));
        $ipad = str_repeat(chr(0x36), $blocksize);
        $opad = str_repeat(chr(0x5c), $blocksize);
        $hmac = pack(
                'H*', $hashfunc(
                        ($key ^ $opad) . pack(
                                'H*', $hashfunc(
                                        ($key ^ $ipad) . $data
                                )
                        )
                )
        );
        return bin2hex($hmac);
    }

    /*
     * Used to encode a field for Amazon Auth
     * (taken from the Amazon S3 PHP example library)
     */

    function hex2b64($str) {
        $raw = '';
        for ($i = 0; $i < strlen($str); $i += 2) {
            $raw .= chr(hexdec(substr($str, $i, 2)));
        }
        return base64_encode($raw);
    }

    function docType($file) {
        $ext = explode('.', $file);
        if (is_array($ext) && count($ext) > 0) {
            $extension = count($ext) > 1 ? strtolower(end($ext)) : '';
            if ($extension == 'doc' || $extension == 'docx') {
                return 'DOC';
            } elseif ($extension == 'xlsx' || $extension == 'xls') {
                return 'Excel';
            } elseif ($extension == 'pdf') {
                return 'PDF';
            } elseif ($extension == 'ppt' || $extension == 'pptx') {
                return 'PPT';
            } elseif ($extension == 'jpeg' || $extension == 'jpg' || $extension == 'png' || $extension == 'gif') {
                return 'Image';
            } else {
                return 'Unknown';
            }
        }
    }

    public function getDocTypeIconLink($ext) {
        $ext = strtolower($ext);
        switch ($ext) {
            case 'doc':
            case 'docx':
                $link = 'img/icons/wd48.png';
                break;
            case 'xls':
            case 'xlsx':
                $link = 'img/icons/excel48.png';
                break;
            case 'pdf':
                $link = 'img/icons/pdf48.png';
                break;
            case 'png':
            case 'gif':
            case 'jpg':
            case 'jpeg':
                $link = 'img/icons/jpeg48.png';
                break;
            case 'ppt':
            case 'pptx':
                $link = 'img/icons/ms-powerpoint.png';
                break;
            default:
                $link = 'img/icons/file48.png';
        }
        return $this->webroot . $link;
    }

    public function getThumbnailUrl($row, $form_workspace = false) {

        $s = explode('.', $row['url']);
        $ext = count($s) > 0 ? end($s) : "";
        $ext = strtolower($ext);
        $base = $this->getDomain();
        if ($row['doc_type'] == 1 || ($form_workspace && $row['doc_type'] == 3 && $row['scripted_current_duration'] == null && $row['is_processed'] >= 4)) {
            $data = (new AppController())->get_document_url($row);
            $row['thubnail_url'] = $data['thumbnail'];
            if ($row['doc_type'] == 3) {
                $row['scripted_current_duration_bool'] = 0;
            }
            $row['static_url'] = $data['url'];
        } elseif ($row['doc_type'] == 2) {

            if ($ext == 'xls' || $ext == 'xlsx') {
                $row['thubnail_url'] = $base . 'img/excel_gry_icon.svg';
            } elseif ($ext == 'pdf') {
                $row['thubnail_url'] = $base . 'img/pdf_gry_icon.svg';
            } elseif ($ext == "pptx" || $ext == "ppt") {
                $row['thubnail_url'] = $base . 'img/power_point.png';
            } elseif ($ext == "docx" || $ext == "doc") {
                $row['thubnail_url'] = $base . 'img/word_icon.png';
            } elseif ($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif') {
                $row['thubnail_url'] = $base . 'img/image_icon.png';
            } else {
                $row['thubnail_url'] = $base . 'img/file_gry_icon.svg';
            }
        } elseif ($row['doc_type'] == 3) {
            $row['scripted_current_duration_bool'] = 1;
            $row['thubnail_url'] = $base . 'img/nots_gry_con.svg';
            // $row['file_type'] = $ext;
        } else {
            $row['thubnail_url'] = $base . 'img/file_gry_icon.svg';
            //$row['file_type'] = $ext;
        }
        if (!isset($row["static_url"])) {
            $row["static_url"] = $row["stack_url"];
        }
        return $row;
    }

    function dateDiff($date) {
        $ago = '';
        $datetime1 = new DateTime("now");
        $datetime2 = new DateTime($date);
        $interval = $datetime1->diff($datetime2);
        $years = $interval->format('%y');
        $months = $interval->format('%m');
        $days = $interval->format('%d');
        $hours = $interval->format('%H');
        $minutes = $interval->format('%I');
        $seconds = $interval->format('%S');
        if ($years != 0) {
            $ago = $years . ' year(s) ago';
        } elseif ($months != 0) {
            $ago = $months . ' month(s) ago';
        } elseif ($days != 0) {
            $ago = $days . ' day(s) ago';
        } elseif ($hours != 0) {
            $ago = $hours . ' hour(s) ago';
        } elseif ($minutes != 0) {
            $ago = $minutes . ' minute(s) ago';
        } elseif ($seconds != 0) {
            $ago = $seconds . ' second(s) ago';
        }
        return $ago;
    }

    function formateDate($date) {
        $time_values = $this->get_page_lang_based_content('VideoLibrary');
        if (empty($date)) {
            return "No date provided";
        }
        $periods = array($time_values['second'], $time_values['minute'], $time_values['hour'], $time_values['day'], $time_values['week'], $time_values['month'], $time_values['year'], $time_values['decade']);
        $lengths = array("60", "60", "24", "7", "4.35", "12", "10");
        $now = time();
        $unix_date = strtotime($date);
        // check validity of date
        if (empty($unix_date)) {
            return "Bad date";
        }
        // is it future date or past date
        if ($now > $unix_date) {
            $difference = $now - $unix_date;
            $tense = $time_values['ago'];
        } else {
            $difference = $unix_date - $now;
            $tense = $time_values['from_now'];
        }
        for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths) - 1; $j++) {
            $difference /= $lengths[$j];
        }
        $difference = round($difference);
        if ($difference != 1) {
            $periods[$j].= "s";
        }
        return "$difference $periods[$j] {$tense}";
    }

    function formateDate1($date) {
        $currentDate = date('Y-m-d H:i:s');
        $start_date = new DateTime($date, new DateTimeZone('UTC'));

        $since_start = $start_date->diff(new DateTime($currentDate));

        if ($since_start->y > 0) {
            $formateDate = "About " . $since_start->y . ($since_start->y > 1 ? ' Years Ago' : ' Year Ago');
        } elseif ($since_start->m > 0) {
            $formateDate = "About " . $since_start->m . ($since_start->m > 1 ? ' Months Ago' : ' Month Ago');
        } elseif ($since_start->d > 0) {
            $formateDate = $since_start->d . ($since_start->d > 1 ? ' Days Ago' : ' Day Ago');
        } elseif ($since_start->h > 0) {
            $formateDate = $since_start->h . ($since_start->h > 1 ? ' Hours Ago' : ' Hour Ago');
        } elseif ($since_start->i > 0) {
            $formateDate = $since_start->i . ($since_start->i > 1 ? ' Minutes Ago' : ' Minute Ago');
        } elseif ($since_start->s > 0) {
            $formateDate = 'Less Than A Minute Ago';
        } else {
            $formateDate = 'Just Now';
        }
        return $formateDate;
    }

    function formatDate2($date) {
        $currentDate = new DateTime();
        $objectDate = new DateTime($date);
        if ($currentDate->format('Y-m-d') == $objectDate->format('Y-m-d')) {
            if ($_SESSION['LANG'] == 'es') {
                $formatDate = 'Hoy ' . $this->SpanishDate(strtotime($date), 'discussion_time');
            } else {
                $formatDate = 'Today ' . $objectDate->format('h:i A');
            }
        } elseif ($currentDate->sub(new DateInterval('P1D'))->format('Y-m-d') == $objectDate->format('Y-m-d')) {
            if ($_SESSION['LANG'] == 'es') {
                $formatDate = 'Ayer ' . $this->SpanishDate(strtotime($date), 'discussion_time');
            } else {
                $formatDate = 'Yesterday ' . $objectDate->format('h:i A');
            }
        } else {
            if ($_SESSION['LANG'] == 'es') {
                $formatDate = 'Ayer ' . $this->SpanishDate(strtotime($date), 'discussion_date');
            } else {
                $formatDate = $objectDate->format('M d, Y h:i A');
            }
        }
        return $formatDate;
    }

    function has_admin_access($huddleUsers, $account_folder_groups, $user_id) {
        $huddle_role = FALSE;
        if ($huddleUsers && count($huddleUsers) > 0 || $account_folder_groups && count($account_folder_groups) > 0 && $user_id != '') {
            foreach ($huddleUsers as $huddle_user) {
                if (isset($huddle_user['huddle_users']) && $huddle_user['huddle_users']['user_id'] == $user_id) {
                    $huddle_role = $huddle_user['huddle_users']['role_id'];
                    return $huddle_role;
                } else {
                    $huddle_role = FALSE;
                }
            }
            if ($huddle_role != '') {
                return $huddle_role;
            } else {
                if ($account_folder_groups && count($account_folder_groups) > 0) {
                    foreach ($account_folder_groups as $groups) {

                        if (isset($groups['user_groups']) && $groups['user_groups']['user_id'] == $user_id) {
                            $huddle_role = $groups['AccountFolderGroup']['role_id'];
                            return $huddle_role;
                        } else {
                            $huddle_role = FALSE;
                        }
                    }
                } else {
                    return FALSE;
                }
            }
        } else {

            return FALSE;
        }
    }

    function is_creator($created_by_id, $user_id) {
        if ($created_by_id == $user_id) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function py_slice($input, $slice) {

        $arg = explode(':', $slice);
        $start = intval($arg[0]);
        if ($start < 0) {
            $start += strlen($input);
        }
        if (count($arg) === 1) {
            return substr($input, $start, 1);
        }
        if (trim($arg[1]) === '') {
            return substr($input, $start);
        }
        $end = intval($arg[1]);
        if ($end < 0) {
            $end += strlen($input);
        }
        return substr($input, $start, $end - $start);
    }

    function log_activity($activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '', $video_id = 0, $bool = 1, $huddle_id = 0) {
        if (empty($this->lang_translations)) {
            $language_based_content = $this->get_page_lang_based_content('Dashboard/home');
            $this->lang_translations = $language_based_content;
        } else {
            $language_based_content = $this->lang_translations;
        }
        switch ($activityType) {
            case 1:
                $activityUrl = '/Huddles/view/' . $extra_title;
                return $this->_get_huddle_log($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $bool, false, $language_based_content);
                break;
            case 2:
                return $this->_get_huddle_video($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, false, $language_based_content);
                break;
            case 3:
                return $this->_get_huddle_doc($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $huddle_id, false, $language_based_content);
                break;
            case 4:
                return $this->_get_library_log($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, false, $language_based_content);
                break;
            case 5:
                return $this->_get_video_comments($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, false, $language_based_content);
                break;
            case 6:
                return $this->_get_video_discussion($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $huddle_id, false, $language_based_content);
                break;
            case 7:
                return $this->_get_account_users($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '', false, $language_based_content);
                break;
            case 8:
                return $this->_get_video_discussion_child($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, false, $language_based_content);
                break;
            case 17:
                return $this->_get_account_users_delete($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '', false, $language_based_content);
                break;
            case 18:
                return $this->_get_users_added_Huddle($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '', $huddle_id, false, $language_based_content);
                break;
            case 19:
                return $this->_get_users_removed_Huddle($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '', $huddle_id, false, $language_based_content);
                break;
            case 20:
                return $this->observation_started($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, false, $language_based_content);
                break;
            case 21:
                return $this->observation_stopped($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, false, $language_based_content);
                break;
            case 22:
                return $this->copy_video($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, false, $language_based_content);
                break;
            case 24:
                return $this->live_stream_video_started($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, false, $language_based_content);
            case 25:
                return $this->live_stream_video_stopped($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, false, $language_based_content);
            case 28:
                return $this->assignment_submit_activity($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $bool, $huddle_id);
        }
    }

   
    function log_activity_for_api($activityType, $activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '', $video_id = 0, $bool = 1, $huddle_id = 0, $user_id = '', $current_user_id = '') {
        $lang = isset($this->request->data['lang']) ? $this->request->data['lang'] : 'en';
        if (empty($this->lang_translations)) {
            $language_based_content = $this->get_page_lang_based_content_for_api('Dashboard/home', $lang);
            $this->lang_translations = $language_based_content;
        } else {
            $language_based_content = $this->lang_translations;
        }

        switch ($activityType) {
            case 1:
                $activityUrl = '/Huddles/view/' . $extra_title;
                return $this->_get_huddle_log($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $bool, true, $language_based_content);
                break;
            case 2:
                return $this->_get_huddle_video($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, true, $language_based_content);
                break;
            case 3:
                return $this->_get_huddle_doc($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $huddle_id, true, $language_based_content);
                break;
            case 4:
                return $this->_get_library_log($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, true, $language_based_content);
                break;
            case 5:
                return $this->_get_video_comments($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, true, $language_based_content);
                break;
            case 6:
                return $this->_get_video_discussion($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $huddle_id, true, $language_based_content);
                break;
            case 7:
                return $this->_get_account_users($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '', true, $language_based_content);
                break;
            case 8:
                return $this->_get_video_discussion_child($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, true, $language_based_content);
                break;
            case 17:
                return $this->_get_account_users_delete($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '', true, $language_based_content);
                break;
            case 18:
                return $this->_get_users_added_Huddle($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '', $huddle_id, true, $language_based_content);
                break;
            case 19:
                return $this->_get_users_removed_Huddle($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title = '', $huddle_id, true, $language_based_content);
                break;
            case 20:
                return $this->observation_started($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, true, $language_based_content);
                break;
            case 21:
                return $this->observation_stopped($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, true, $language_based_content);
                break;
            case 22:
                return $this->copy_video($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, true, $language_based_content);
                break;
            case 24:
                return $this->live_recording_started($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, true, $language_based_content);
                break;
            case 25:
                return $this->live_recording_stopped($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, true, $language_based_content);
                break;
            case 29:
                return $this->get_huddle_urls($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id,$huddle_id,false,$user_id,$current_user_id,$language_based_content);
                break;
            case 30:
                return $this->_get_shared_urls($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id,$huddle_id,false,$user_id,$current_user_id,$language_based_content);
                break;

        }
    }
    function _get_shared_urls($activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name,$video_id, $huddle_id, $is_api = false, $user_id,$current_user_id = '',$language_based_content){
        if ($activityLogs_users != '' && $video_title != '') {
            if (strlen($huddle_name) > 50) {
                $huddle_name = substr($huddle_name, 0, 50) . '...';
            }
            $video_name = $video_title;
            if ($this->workspace_video($huddle_id)) {
                if(!empty($video_title)){
                    $video_name = $video_title;
                } else {
                    $video_name = $huddle_name;
                }

                //$video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $video_name . "</a>";
                $text = $language_based_content['shared_url_activity'] ;
                $text .= " in Workspace";
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'title' => $video_name, 'url' => $activityUrl, 'activityDate' => $activityDate, 'workspace'=>true];
            }
            if($this->check_if_eval_huddle($huddle_id))
            {
                $is_avaluator = $this->check_if_evalutor($huddle_id, $user_id);
                if($is_avaluator)
                {
                    $sample_data = $language_based_content['sample_data_global'];
                    // $text = 'added ' . $video_title . ' into ' . $huddle_name ;
                    $huddle_url = '/home/video_huddles/assessment/'.$huddle_id.'/huddle/details';
                }
                else {
                    $sample_data = '';
                    // $text = 'added ' . $video_title . ' into ' . $huddle_name ;
                    $huddle_url = '/home/video_huddles/assessment/'.$huddle_id.'/huddle/details';
                }
                // $huddle_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";

                 if($user_id == $current_user_id)
                {
                   $activityLogs_users = $language_based_content['you_global']; 
                }      
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $language_based_content['shared_url_activity'], 'url' => $activityUrl, 'title' => $video_title, 'activityDate' => $activityDate , 'huddle_name' => $huddle_name , 'sample_data' => $sample_data , 'huddle_url' => $huddle_url, 'activity_type'=>30 ];
             }else{
               // $huddle_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";
                $huddle_url = "/Huddles/view/".$huddle_id;
                $text = $language_based_content['shared_url_activity'];
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_name, 'title' => $video_title, 'huddle_name'=>$huddle_name, 'url' => $activityUrl, 'activityDate' => $activityDate, 'huddle_url' => $huddle_url, 'activity_type'=>30];
             }           

        }else{
            return false;
        }
    }

    function get_huddle_urls($activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name,$video_id, $huddle_id, $is_api = false, $user_id,$current_user_id = '',$language_based_content){
        if ($activityLogs_users != '' && $video_title != '') {
            if (strlen($huddle_name) > 50) {
                $huddle_name = substr($huddle_name, 0, 50) . '...';
            }
            $video_name = $video_title;
            if ($this->workspace_video($huddle_id)) {
                if(!empty($video_title)){
                    $video_name = $video_title;
                } else {
                    $video_name = $huddle_name;
                }
               // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $video_name . "</a>";
                $text = $language_based_content['added_url_workspace_activity'] ;
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'title' => $video_name, 'url' => $activityUrl, 'activityDate' => $activityDate, 'workspace'=>true];
            }
            if($this->check_if_eval_huddle($huddle_id))
            {
                $is_avaluator = $this->check_if_evalutor($huddle_id, $user_id);
                if($is_avaluator)
                {
                    $sample_data = $language_based_content['sample_data_global'];
                    // $text = 'added ' . $video_title . ' into ' . $huddle_name ;
                    $huddle_url = '/home/video_huddles/assessment/'.$huddle_id.'/huddle/details';
                }
                else {
                    $sample_data = '';
                    // $text = 'added ' . $video_title . ' into ' . $huddle_name ;
                    $huddle_url = '/home/video_huddles/assessment/'.$huddle_id.'/huddle/details';
                }
                // $huddle_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";

                 if($user_id == $current_user_id)
                {
                   $activityLogs_users = $language_based_content['you_global']; 
                }      
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $language_based_content['uploaded_global'], 'url' => $activityUrl, 'title' => $video_title, 'activityDate' => $activityDate , 'huddle_name' => $huddle_name , 'sample_data' => $sample_data , 'huddle_url' => $huddle_url, 'activity_type'=>29 ];
             }else{
                //$huddle_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";
                $huddle_url = "/Huddles/view/".$huddle_id;
                $text = $language_based_content['added_url_link_activity'];
                return ['activityLogs_users' => $activityLogs_users, 'resource_name' => $text, 'resource_video_name' => $video_name, 'title' => $video_title, 'huddle_name'=>$huddle_name, 'url' => $activityUrl, 'activityDate' => $activityDate, 'huddle_url' => $huddle_url, 'activity_type'=>29];
             }           

        }else{
            return false;
        }
    }

    function get_show_parent_video_library($account_id) {

        App::import("Model", "AccountMetaData");
        $db = new AccountMetaData();
        $check_data = $db->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "show_parent_video_library"
        )));

        if (!empty($check_data)) {

            return $check_data['AccountMetaData']['meta_data_value'];
        } else {

            return 0;
        }
    }

    function get_account_details($account_id) {

        App::import("Model", "Account");
        $db = new Account();
        $check_data = $db->find("first", array("conditions" => array(
                "id" => $account_id,
        )));

        if (!empty($check_data)) {

            return $check_data['Account']['parent_account_id'];
        } else {

            return 0;
        }
    }

    private function _get_huddle_log($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $huddle_id, $bool, $is_api = false, $language_based_content) {
        if ($activityLogs_users != '' && $huddle_name != '') {
            $collaborcoach = $this->coach_or_collab($huddle_id);
            $evaluationHuddle = $this->check_if_eval_huddle($huddle_id);
            App::import("Model", "AccountFolder");
            $db = new AccountFolder();
            $af_data = $db->find("first", array("conditions" => array(
                    "account_folder_id" => $huddle_id
            )));

            if (!empty($af_data)) {
                $huddle_type = $af_data['AccountFolder']['folder_type'];
            }

            if ($huddle_type == 5)
                $text = $language_based_content['created_folder_activity'];
            else {
                if ($collaborcoach == 1) {
                    $text = $language_based_content['created_coaching_huddle_activity'];
                } else if ($evaluationHuddle == 1) {
                    $text = $language_based_content['created_assessment_huddle_activity'];
                } else {
                    $text = $language_based_content['created_collab_huddle_activity'];
                }

                if ($bool == 2) {
                    if ($collaborcoach == 1) {
                        $text = $language_based_content['invited_coaching_huddle_activity'];
                    } else if ($evaluationHuddle == 1) {
                        $text = $language_based_content['invited_assessment_huddle_activity'];
                    } else {
                        $text = $language_based_content['invited_collab_huddle_activity'];
                    }
                }
            }
            if ($is_api == false) {
                $huddle_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";
            }
            return $activityLogs_users . " " . $text . $huddle_name;
        } else {
            return FALSE;
        }
    }

    private function _get_huddle_video($activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false, $language_based_content) {

        if ($activityLogs_users != '' && $video_title != '') {

            $videoFilePath = pathinfo($activityDescription);
            $videoFileName = $videoFilePath['extension'];
            // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            $text = $language_based_content['uploaded_video_activity'];
            if ($this->coach_or_collab($huddle_id)) {
                $text = $language_based_content['uploaded_video_coaching_activity'];
            } elseif ($this->check_if_eval_huddle($huddle_id)) {
                $text = $language_based_content['uploaded_video_assessment_activity'];
            } else {
                $text = $language_based_content['uploaded_video_collab_activity'];
            }
            if ($this->workspace_video($huddle_id)) {
                $text = $language_based_content['uploaded_video_workspace_activity'];
            }

            if ($videoFileName == 'mp3' || $videoFileName == 'm4a') {
                $text = $language_based_content['uploaded_audio_activity'];

                if ($this->coach_or_collab($huddle_id)) {
                    $text = $language_based_content['uploaded_audio_coaching_activity'];
                } elseif ($this->check_if_eval_huddle($huddle_id)) {
                    $text = $language_based_content['uploaded_audio_assessment_activity'];
                } else {
                    $text = $language_based_content['uploaded_audio_collab_activity'];
                }
                if ($this->workspace_video($huddle_id)) {
                    $text = $language_based_content['uploaded_audio_workspace_activity'];
                }
            }

            App::import("Model", "AccountFolderDocument");
            $db = new AccountFolderDocument();
            $af_data = $db->find("first", array("conditions" => array(
                    "document_id" => $video_id
            )));

            if (!empty($af_data)) {
                $huddle_id = $af_data['AccountFolderDocument']['account_folder_id'];
            }

            App::import("Model", "Document");
            $db = new Document();
            $check_data = $db->find("first", array("conditions" => array(
                    "id" => $video_id
            )));
            /* if (!empty($check_data) && $check_data['Document']['doc_type'] == 3) {
              $text = 'completed an observation in the Coaching Huddle';
              if ($check_data['Document']['current_duration'] > 0 && $check_data['Document']['is_processed'] == 4 && $check_data['Document']['is_associated'] > 0) {
              $url = $this->base . '/Huddles/view/' . $huddle_id . '/' . '1/' . $check_data['Document']['is_associated'];
              } elseif ($check_data['Document']['current_duration'] > 0 && $check_data['Document']['is_processed'] == 4 && $check_data['Document']['is_associated'] == 0) {
              $url = $this->base . '/Huddles/observation_details_3/' . $huddle_id . '/' . $check_data['Document']['id'];
              } elseif ($check_data['Document']['current_duration'] > 0) {
              $url = $this->base . '/Huddles/observation_details_2/' . $huddle_id . '/' . $check_data['Document']['id'];
              } else {
              $url = $this->base . '/Huddles/observation_details_1/' . $huddle_id . '/' . $check_data['Document']['id'];
              }
              } else { */
            $url = $activityUrl;
            //}
            if ($is_api == false) {
                $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            } else {
                $video_name = $video_title;
            }


            return $activityLogs_users . " " . $text . ": " . $video_name;
        } else {
            return FALSE;
        }
    }

    private function copy_video($activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false, $language_based_content) {

        if ($activityLogs_users != '' && $video_title != '') {

            $videoFilePath = pathinfo($activityDescription);
            $videoFileName = $videoFilePath['extension'];
            // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            $text = $language_based_content['copied_video_activity'];
            if ($this->coach_or_collab($huddle_id)) {
                $text = $language_based_content['copied_video_to_coaching_huddle_activity'];
            } elseif ($this->check_if_eval_huddle($huddle_id)) {
                $text = $language_based_content['copied_video_to_assessment_huddle_activity'];
            } else {
                $text = $language_based_content['copied_video_to_collaboration_huddle_activity'];
            }
            if ($this->library_video($huddle_id)) {
                $text = $language_based_content['copied_video_to_library_activity'];
            }


            if ($this->workspace_video($huddle_id)) {
                $text = $language_based_content['copied_video_to_workspace_activity'];
            }

            if ($videoFileName == 'mp3' || $videoFileName == 'm4a') {
                $text = $language_based_content['copied_audio_file_activity'];

                if ($this->coach_or_collab($huddle_id)) {
                    $text = $language_based_content['copied_audio_file_in_coaching_huddle_activity'];
                } elseif ($this->check_if_eval_huddle($huddle_id)) {
                    $text = $language_based_content['copied_audio_file_in_assessment_huddle_activity'];
                } else {
                    $text = $language_based_content['copied_audio_file_in_collaboration_huddle_activity'];
                }
                if ($this->workspace_video($huddle_id)) {
                    $text = $language_based_content['copied_audio_file_to_your_workspace_activity'];
                }
            }

            App::import("Model", "AccountFolderDocument");
            $db = new AccountFolderDocument();
            $af_data = $db->find("first", array("conditions" => array(
                    "document_id" => $video_id
            )));

            if (!empty($af_data)) {
                $huddle_id = $af_data['AccountFolderDocument']['account_folder_id'];
            }

            App::import("Model", "Document");
            $db = new Document();
            $check_data = $db->find("first", array("conditions" => array(
                    "id" => $video_id
            )));

            $url = $activityUrl;
            //}
            if ($is_api == false) {
                $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            } else {
                $video_name = $huddle_name . " - " . $video_title;
            }
            return $activityLogs_users . " " . $text . ": " . $video_name;
        } else {
            return FALSE;
        }
    }

    private function assignment_submit_activity($activityLogs_users, $huddle_name, $activityDescription, $activityUrl, $activityDate, $extra_title, $video_id, $huddle_id, $bool) {

        if ($this->check_if_eval_huddle($huddle_id)) {
            $is_avaluator = $this->check_if_evalutor($huddle_id, $bool);
            if ($is_avaluator) {
                $activityUrl = $activityUrl;
            } else {

                $activityUrl = $activityDescription;
            }
            //$huddle_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";
            $text = 'has submitted assessment';
            return $activityLogs_users . " " . $text . ": " . $huddle_name;
        }
    }

    private function _get_huddle_doc($activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $huddle_id, $is_api = false, $language_based_content) {
        if ($activityLogs_users != '' && $video_title != '') {
            $collaborcoach = $this->coach_or_collab($huddle_id);
            if (strlen($huddle_name) > 50) {
                $huddle_name = mb_substr($huddle_name, 0, 50) . '...';
            }
            if ($is_api == false) {
                $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            } else {
                $video_name = $huddle_name . " - " . $video_title;
            }

            if ($this->workspace_video($huddle_id)) {
                return $activityLogs_users . " " . $language_based_content['uploaded_rescource_workspace_activity'] . $video_name;
            }

            if ($this->library_video($huddle_id)) {
                return $activityLogs_users . " " . $language_based_content['uploaded_rescource_library_activity'] . $video_name;
            }

            if ($collaborcoach == 1) {
                return $activityLogs_users . " " . $language_based_content['uploaded_rescource_coaching_huddle_activity'] . $video_name;
            } elseif ($this->check_if_eval_huddle($huddle_id)) {
                return $activityLogs_users . " " . $language_based_content['uploaded_rescource_assessment_huddle_activity'] . $video_name;
            } else {
                return $activityLogs_users . " " . $language_based_content['uploaded_rescource_collab_huddle_activity'] . $video_name;
            }
        } else {
            return FALSE;
        }
    }

    private function _get_library_log($activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $is_api = false, $language_based_content) {
        if ($activityLogs_users != '' && $video_title != '') {
            if ($is_api == false) {
                $video_title = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $video_title . "</a>";
            }

            return $activityLogs_users . " " . $language_based_content['added_video_library_activity'] . $video_title;
        } else {
            return FALSE;
        }
    }

    private function _get_video_comments($activityLogs_users, $huddle_name, $comments, $activityUrl, $activityDate, $video_title, $video_id, $hudd_id, $is_api = false, $language_based_content) {
        if ($activityLogs_users != '' && $huddle_name != '') {

            App::import("Model", "AccountFolderDocument");
            $db = new AccountFolderDocument();
            $af_data = $db->find("first", array("conditions" => array(
                    "document_id" => $video_id
            )));

            if (!empty($af_data)) {
                $huddle_id = $af_data['AccountFolderDocument']['account_folder_id'];
            }

            App::import("Model", "Document");
            $db = new Document();
            $check_data = $db->find("first", array("conditions" => array(
                    "id" => $video_id
            )));
            if (!empty($check_data) && $check_data['Document']['doc_type'] == 3 && !$this->workspace_video($hudd_id)) {
                if ($check_data['Document']['current_duration'] > 0 && $check_data['Document']['is_processed'] == 4 && $check_data['Document']['is_associated'] > 0) {
                    $url = $this->base . '/video_details/home/' . $huddle_id . '/' . $check_data['Document']['is_associated'];
                } elseif ($check_data['Document']['current_duration'] > 0 && $check_data['Document']['is_processed'] == 4 && $check_data['Document']['is_associated'] == 0) {
                    $url = $this->base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data['Document']['id'];
                } elseif ($check_data['Document']['current_duration'] > 0) {
                    $url = $this->base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data['Document']['id'];
                } else {
                    $url = $this->base . '/video_details/scripted_observations/' . $huddle_id . '/' . $check_data['Document']['id'];
                }
            } else {
                $url = $activityUrl;
            }
            $text = $language_based_content['made_comment_activity'];
            if ($this->coach_or_collab($hudd_id)) {
                $text = $language_based_content['made_coaching_huddle_comment_activity'];
            } elseif ($this->check_if_eval_huddle($hudd_id)) {
                $text = $language_based_content['made_assessment_huddle_comment_activity'];
            } else {
                $text = $language_based_content['made_collab_huddle_comment_activity'];
            }
            if ($this->workspace_video($hudd_id)) {
                $text = $language_based_content['made_video_note_activity'];
            }
            App::import("Model", "Document");
            $db = new Document();
            $filename = $db->find("first", array("conditions" => array(
                    "id" => $video_id
            )));

            if (!empty($filename)) {
                $videoFilePath = pathinfo($filename['Document']['original_file_name']);
                $videoFileName = $videoFilePath['extension'];
                if ($videoFileName == 'mp3' || $videoFileName == 'm4a') {
                    $text = ' made a comment ';

                    if ($this->coach_or_collab($hudd_id)) {
                        $text = $language_based_content['made_coaching_huddle_audio_comment_activity'];
                    } elseif ($this->check_if_eval_huddle($hudd_id)) {
                        $text = $language_based_content['made_assessment_huddle_audio_comment_activity'];
                    } else {
                        $text = $language_based_content['made_collab_huddle_audio_comment_activity'];
                    }
                    if ($this->workspace_video($hudd_id)) {
                        $text = $language_based_content['made_audio_note_activity'];
                    }
                }
            }
            if ($is_api == false) {
                $huddle_video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            } else {
                $huddle_video_name = $huddle_name . " - " . $video_title;
            }

            return $activityLogs_users . " " . $text . $huddle_video_name;
        } else {
            return FALSE;
        }
    }

    private function _get_video_discussion($activityLogs_users, $dicussion_title, $activityDescription, $activityUrl, $activityDate, $extra_title, $huddle_id, $is_api = false, $language_based_content) {
        if ($activityLogs_users != '' && ($dicussion_title != '' || $activityDescription != '')) {
            $collaborcoach = $this->coach_or_collab($huddle_id);
            if ($dicussion_title != '' && $is_api == false) {
                $huddle_discussion_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $extra_title . " - " . $dicussion_title . "</a>";
            } elseif ($activityDescription != '' && $is_api == false) {
                $huddle_discussion_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $extra_title . " - " . $activityDescription . "</a>";
            } elseif ($activityDescription != '' && $is_api == true) {
                $huddle_discussion_name = $extra_title . " - " . $activityDescription;
            } else {
                return FALSE;
            }
            if ($collaborcoach == 1) {
                return $activityLogs_users . " " . $language_based_content['created_new_discussion_coaching_huddle_activity'] . $huddle_discussion_name;
            } elseif ($this->check_if_eval_huddle($huddle_id)) {
                return $activityLogs_users . " " . $language_based_content['created_new_discussion_assessment_huddle_activity'] . $huddle_discussion_name;
            } else {
                return $activityLogs_users . " " . $language_based_content['created_new_discussion_collab_huddle_activity'] . $huddle_discussion_name;
            }
        } else {
            return FALSE;
        }
    }

    private function _get_video_discussion_child($activityLogs_users, $dicussion_title, $activityDescription, $activityUrl, $activityDate, $extra_title, $is_api = false, $language_based_content) {
        if ($activityLogs_users != '' && ($dicussion_title != '' || $activityDescription != '') && $is_api == false) {
            if ($dicussion_title != '') {
                $huddle_discussion_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $extra_title . " - " . $dicussion_title . "</a>";
            } elseif ($activityDescription != '') {
                $huddle_discussion_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $extra_title . " - " . $activityDescription . "</a>";
            } else {
                return FALSE;
            }
            return $activityLogs_users . " " . $language_based_content['posted_comment_in_activity'] . $huddle_discussion_name;
        } elseif ($activityLogs_users != '' && ($dicussion_title != '' || $activityDescription != '') && $is_api == true) {
            if ($dicussion_title != '') {
                $huddle_discussion_name = $extra_title . " - " . $dicussion_title;
            } elseif ($activityDescription != '') {
                $huddle_discussion_name = $extra_title . " - " . $activityDescription;
            } else {
                return FALSE;
            }
            return $activityLogs_users . " " . $language_based_content['posted_comment_in_activity'] . $huddle_discussion_name;
        } else {
            return FALSE;
        }
    }

    private function _get_account_users($activityLogs_users, $huddle_name, $username, $activityUrl, $activityDate, $is_api = false, $language_based_content) {
        if ($activityLogs_users != '' && $username != '' && $is_api == false) {
            if ($huddle_name != '') {
                $username_link = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";

                return $activityLogs_users . $this->parse_translation_params($language_based_content['invited_user_in_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
            } else {
                $username_link = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $username . "</a>";
                return $activityLogs_users . $this->parse_translation_params($language_based_content['added_user_into_account_activity'], ['username_link' => $username_link]);
            }
        } elseif ($activityLogs_users != '' && $username != '' && $is_api == true) {
            if ($huddle_name != '') {
                $username_link = $huddle_name;
                return $activityLogs_users . $this->parse_translation_params($language_based_content['invited_user_in_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
            } else {
                $username_link = $username;
                return $activityLogs_users . $this->parse_translation_params($language_based_content['added_user_into_account_activity'], ['username_link' => $username_link]);
            }
        } else {
            return FALSE;
        }
    }

    private function _get_account_users_delete($activityLogs_users, $huddle_name, $username, $activityUrl, $activityDate, $extra_title = '', $is_api = false, $language_based_content) {
        if ($activityLogs_users != '' && $username != '' && $is_api == false) {
            if ($huddle_name != '') {
                $username_link = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";
                return $activityLogs_users . " invited " . $username . " into the Huddle: " . $username_link;
            } else {
                $username_link = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $username . "</a>";
                return $activityLogs_users . $this->parse_translation_params($language_based_content['removed_user_from_account_activity'], ['username_link' => $username_link]);
            }
        } elseif ($activityLogs_users != '' && $username != '' && $is_api == true) {
            if ($huddle_name != '') {
                $username_link = $huddle_name;
                return $activityLogs_users . $this->parse_translation_params($language_based_content['invited_user_in_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
            } else {
                $username_link = $username;
                return $activityLogs_users . $this->parse_translation_params($language_based_content['removed_user_from_account_activity'], ['username_link' => $username_link]);
            }
        } else {
            return FALSE;
        }
    }

    private function _get_users_added_Huddle($activityLogs_users, $huddle_name, $username, $activityUrl, $activityDate, $name, $huddle_id, $is_api = false, $language_based_content) {
        if ($activityLogs_users != '' && $username != '' && $is_api == false) {
            if ($huddle_name != '') {
                if ($this->coach_or_collab($huddle_id)) {
                    $username_link = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";
                    return $activityLogs_users . $this->parse_translation_params($language_based_content['invited_user_into_coaching_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
                } elseif ($this->check_if_eval_huddle($huddle_id)) {
                    $username_link = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";
                    return $activityLogs_users . $this->parse_translation_params($language_based_content['invited_user_into_assessment_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
                } else {
                    $username_link = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";
                    return $activityLogs_users . $this->parse_translation_params($language_based_content['invited_user_into_collaboration_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
                }
            } else {
                $username_link = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $username . "</a>";
                return $activityLogs_users . $this->parse_translation_params($language_based_content['removed_user_from_account_activity'], ['username_link' => $username_link]);
            }
        } elseif ($activityLogs_users != '' && $username != '' && $is_api == true) {
            if ($huddle_name != '') {
                if ($this->coach_or_collab($huddle_id)) {
                    $username_link = $huddle_name;
                    return $activityLogs_users . $this->parse_translation_params($language_based_content['invited_user_into_coaching_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
                } elseif ($this->check_if_eval_huddle($huddle_id)) {
                    $username_link = $huddle_name;
                    return $activityLogs_users . $this->parse_translation_params($language_based_content['invited_user_into_assessment_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
                } else {
                    $username_link = $huddle_name;
                    return $activityLogs_users . $this->parse_translation_params($language_based_content['invited_user_into_collaboration_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
                }
            } else {
                $username_link = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $username . "</a>";
                return $activityLogs_users . $this->parse_translation_params($language_based_content['removed_user_from_account_activity'], ['username_link' => $username_link]);
            }
        } else {
            return FALSE;
        }
    }

    private function _get_users_removed_Huddle($activityLogs_users, $huddle_name, $username, $activityUrl, $activityDate, $name, $huddle_id, $is_api = false, $language_based_content) {
        if ($activityLogs_users != '' && $username != '' && $is_api == false) {
            if ($huddle_name != '') {
                if ($this->coach_or_collab($huddle_id)) {
                    $username_link = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";
                    return $activityLogs_users . $this->parse_translation_params($language_based_content['removed_user_from_coaching_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
                } elseif ($this->check_if_eval_huddle($huddle_id)) {
                    $username_link = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";
                    return $activityLogs_users . $this->parse_translation_params($language_based_content['removed_user_from_assessment_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
                } else {
                    $username_link = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . "</a>";
                    return $activityLogs_users . $this->parse_translation_params($language_based_content['removed_user_from_collaboration_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
                }
            } else {
                $username_link = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $username . "</a>";
                return $activityLogs_users . $this->parse_translation_params($language_based_content['removed_user_from_account_activity'], ['username_link' => $username_link]);
            }
        } elseif ($activityLogs_users != '' && $username != '' && $is_api == true) {
            if ($huddle_name != '') {
                if ($this->coach_or_collab($huddle_id)) {
                    $username_link = $huddle_name;
                    return $activityLogs_users . $this->parse_translation_params($language_based_content['removed_user_from_coaching_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
                } elseif ($this->check_if_eval_huddle($huddle_id)) {
                    $username_link = $huddle_name;
                    return $activityLogs_users . $this->parse_translation_params($language_based_content['removed_user_from_assessment_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
                } else {
                    $username_link = $huddle_name;
                    return $activityLogs_users . $this->parse_translation_params($language_based_content['removed_user_from_collaboration_huddle_activity'], ['username' => $username, 'username_link' => $username_link]);
                }
            } else {
                $username_link = $username;
                return $activityLogs_users . $this->parse_translation_params($language_based_content['removed_user_from_account_activity'], ['username_link' => $username_link]);
            }
        } else {
            return FALSE;
        }
    }

    private function live_stream_video_started($activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false) {
        if ($activityLogs_users != '' && $video_title != '') {


            // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            $text = 'started a new live stream video';


            App::import("Model", "AccountFolderDocument");
            $db = new AccountFolderDocument();
            $af_data = $db->find("first", array("conditions" => array(
                    "document_id" => $video_id
            )));

            if (!empty($af_data)) {
                $huddle_id = $af_data['AccountFolderDocument']['account_folder_id'];
            }

            App::import("Model", "Document");
            $db = new Document();
            $check_data = $db->find("first", array("conditions" => array(
                    "id" => $video_id
            )));
            if (!empty($check_data) && $check_data['Document']['doc_type'] == 4) {
                $url = $this->base . '/Huddles/view_live/' . $huddle_id . '/1/' . $check_data['Document']['id'];
            } else {
                $url = $activityUrl;
            }

            if ($is_api == false) {
                $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            } else {
                $video_name = $huddle_name . " - " . $video_title;
            }
            return $activityLogs_users . " " . $text . ": " . $video_name;
        } else {
            return FALSE;
        }
    }

    private function live_stream_video_stopped($activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false) {
        if ($activityLogs_users != '' && $video_title != '') {
            // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            $text = 'stopped a live stream video';
            App::import("Model", "AccountFolderDocument");
            $db = new AccountFolderDocument();
            $af_data = $db->find("first", array("conditions" => array(
                    "document_id" => $video_id
            )));

            if (!empty($af_data)) {
                $huddle_id = $af_data['AccountFolderDocument']['account_folder_id'];
            }

            App::import("Model", "Document");
            $db = new Document();
            $check_data = $db->find("first", array("conditions" => array(
                    "id" => $video_id
            )));
            if (!empty($check_data) && $check_data['Document']['doc_type'] == 4) {
                $url = $this->base . '/Huddles/view_live/' . $huddle_id . '/1/' . $check_data['Document']['id'];
            } else {
                $url = $activityUrl;
            }

            if ($is_api == false) {
                $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            } else {
                $video_name = $huddle_name . " - " . $video_title;
            }
            return $activityLogs_users . " " . $text . ": " . $video_name;
        } else {
            return FALSE;
        }
    }

    private function observation_started($activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false, $language_based_content) {

        if ($activityLogs_users != '' && $video_title != '') {

            // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            $text = $language_based_content['started_new_video_observation_activity'];

            App::import("Model", "AccountFolderDocument");
            $db = new AccountFolderDocument();
            $af_data = $db->find("first", array("conditions" => array(
                    "document_id" => $video_id
            )));

            if (!empty($af_data)) {
                $huddle_id = $af_data['AccountFolderDocument']['account_folder_id'];
            }

            App::import("Model", "Document");
            $db = new Document();
            $check_data = $db->find("first", array("conditions" => array(
                    "id" => $video_id
            )));


            if ($this->workspace_video($huddle_id)) {
                $text = $language_based_content['started_synced_video_notes_activity'];
                if (!empty($check_data) && $check_data['Document']['doc_type'] == 3) {
                    $url = $this->base . '/MyFiles/view/1/' . $huddle_id;
                } else {
                    $url = $activityUrl;
                }
            } else {


                if (!empty($check_data) && $check_data['Document']['doc_type'] == 3) {

                    if ($check_data['Document']['current_duration'] > 0 && $check_data['Document']['is_processed'] == 4 && $check_data['Document']['is_associated'] > 0) {
                        $url = $this->base . '/video_details/home/' . $huddle_id . '/' . $check_data['Document']['is_associated'];
                    } elseif ($check_data['Document']['current_duration'] > 0 && $check_data['Document']['is_processed'] == 4 && $check_data['Document']['is_associated'] == 0) {
                        $url = $this->base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data['Document']['id'];
                    } elseif ($check_data['Document']['current_duration'] > 0) {
                        $url = $this->base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data['Document']['id'];
                    } else {
                        $url = $this->base . '/video_details/scripted_observations/' . $huddle_id . '/' . $check_data['Document']['id'];
                    }
                } else {
                    $url = $activityUrl;
                }
            }
            if ($is_api == false) {
                $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            } else {
                $video_name = $huddle_name . " - " . $video_title;
            }


            return $activityLogs_users . " " . $text . ": " . $video_name;
        } else {
            return FALSE;
        }
    }

    private function live_recording_started($activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false, $language_based_content) {

        if ($activityLogs_users != '' && $video_title != '') {


            // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            $text = $language_based_content['started_new_live_recording_activity'];


            App::import("Model", "AccountFolderDocument");
            $db = new AccountFolderDocument();
            $af_data = $db->find("first", array("conditions" => array(
                    "document_id" => $video_id
            )));

            if (!empty($af_data)) {
                $huddle_id = $af_data['AccountFolderDocument']['account_folder_id'];
            }

            App::import("Model", "Document");
            $db = new Document();
            $check_data = $db->find("first", array("conditions" => array(
                    "id" => $video_id
            )));


            if ($this->workspace_video($huddle_id)) {
                $text = $language_based_content['started_synced_video_notes_activity'];
                if (!empty($check_data) && $check_data['Document']['doc_type'] == 3) {
                    $url = $this->base . '/MyFiles/view/1/' . $huddle_id;
                } else {
                    $url = $activityUrl;
                }
            } else {


                if (!empty($check_data) && $check_data['Document']['doc_type'] == 3) {

                    if ($check_data['Document']['current_duration'] > 0 && $check_data['Document']['is_processed'] == 4 && $check_data['Document']['is_associated'] > 0) {
                        $url = $this->base . '/video_details/home/' . $huddle_id . '/' . $check_data['Document']['is_associated'];
                    } elseif ($check_data['Document']['current_duration'] > 0 && $check_data['Document']['is_processed'] == 4 && $check_data['Document']['is_associated'] == 0) {
                        $url = $this->base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data['Document']['id'];
                    } elseif ($check_data['Document']['current_duration'] > 0) {
                        $url = $this->base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data['Document']['id'];
                    } else {
                        $url = $this->base . '/video_details/scripted_observations/' . $huddle_id . '/' . $check_data['Document']['id'];
                    }
                } else {
                    $url = $activityUrl;
                }
            }
            if ($is_api == false) {
                $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            } else {
                $video_name = $huddle_name . " - " . $video_title;
            }


            return $activityLogs_users . " " . $text . ": " . $video_name;
        } else {
            return FALSE;
        }
    }

    private function observation_stopped($activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false, $language_based_content) {

        if ($activityLogs_users != '' && $video_title != '') {

            // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            $text = $language_based_content['stopped_video_observation_activity'];

            App::import("Model", "AccountFolderDocument");
            $db = new AccountFolderDocument();
            $af_data = $db->find("first", array("conditions" => array(
                    "document_id" => $video_id
            )));

            if (!empty($af_data)) {
                $huddle_id = $af_data['AccountFolderDocument']['account_folder_id'];
            }

            App::import("Model", "Document");
            $db = new Document();
            $check_data = $db->find("first", array("conditions" => array(
                    "id" => $video_id
            )));


            if ($this->workspace_video($huddle_id)) {
                $text = $language_based_content['stopped_synced_video_notes_activity'];
                if (!empty($check_data) && $check_data['Document']['doc_type'] == 3) {
                    $url = $this->base . '/MyFiles/view/1/' . $huddle_id;
                } else {
                    $url = $activityUrl;
                }
            } else {


                if (!empty($check_data) && $check_data['Document']['doc_type'] == 3) {

                    if ($check_data['Document']['current_duration'] > 0 && $check_data['Document']['is_processed'] == 4 && $check_data['Document']['is_associated'] > 0) {
                        $url = $this->base . '/video_details/home/' . $huddle_id . '/' . $check_data['Document']['is_associated'];
                    } elseif ($check_data['Document']['current_duration'] > 0 && $check_data['Document']['is_processed'] == 4 && $check_data['Document']['is_associated'] == 0) {
                        $url = $this->base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data['Document']['id'];
                    } elseif ($check_data['Document']['current_duration'] > 0) {
                        $url = $this->base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data['Document']['id'];
                    } else {
                        $url = $this->base . '/video_details/scripted_observations/' . $huddle_id . '/' . $check_data['Document']['id'];
                    }
                } else {
                    $url = $activityUrl;
                }
            }
            if ($is_api == false) {
                $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            } else {
                $video_name = $huddle_name . " - " . $video_title;
            }


            return $activityLogs_users . " " . $text . ": " . $video_name;
        } else {
            return FALSE;
        }
    }

    private function live_recording_stopped($activityLogs_users, $video_title, $activityDescription, $activityUrl, $activityDate, $huddle_name, $video_id, $huddle_id, $is_api = false, $language_based_content) {

        if ($activityLogs_users != '' && $video_title != '') {


            // $video_name = "<a style='color:#6191bb' href='" . $activityUrl . "'>" . $huddle_name . " - " . $video_title . "</a>";
            $text = $language_based_content['stopped_live_recording_activity'];

            App::import("Model", "AccountFolderDocument");
            $db = new AccountFolderDocument();
            $af_data = $db->find("first", array("conditions" => array(
                    "document_id" => $video_id
            )));

            if (!empty($af_data)) {
                $huddle_id = $af_data['AccountFolderDocument']['account_folder_id'];
            }

            App::import("Model", "Document");
            $db = new Document();
            $check_data = $db->find("first", array("conditions" => array(
                    "id" => $video_id
            )));


            if ($this->workspace_video($huddle_id)) {
                $text = $language_based_content['stopped_synced_video_notes_activity'];
                if (!empty($check_data) && $check_data['Document']['doc_type'] == 3) {
                    $url = $this->base . '/MyFiles/view/1/' . $huddle_id;
                } else {
                    $url = $activityUrl;
                }
            } else {


                if (!empty($check_data) && $check_data['Document']['doc_type'] == 3) {

                    if ($check_data['Document']['current_duration'] > 0 && $check_data['Document']['is_processed'] == 4 && $check_data['Document']['is_associated'] > 0) {
                        $url = $this->base . '/video_details/home/' . $huddle_id . '/' . $check_data['Document']['is_associated'];
                    } elseif ($check_data['Document']['current_duration'] > 0 && $check_data['Document']['is_processed'] == 4 && $check_data['Document']['is_associated'] == 0) {
                        $url = $this->base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data['Document']['id'];
                    } elseif ($check_data['Document']['current_duration'] > 0) {
                        $url = $this->base . '/video_details/video_observation/' . $huddle_id . '/' . $check_data['Document']['id'];
                    } else {
                        $url = $this->base . '/video_details/scripted_observations/' . $huddle_id . '/' . $check_data['Document']['id'];
                    }
                } else {
                    $url = $activityUrl;
                }
            }
            if ($is_api == false) {
                $video_name = "<a style='color:#6191bb' href='" . $url . "'>" . $huddle_name . " - " . $video_title . "</a>";
            } else {
                $video_name = $huddle_name . " - " . $video_title;
            }


            return $activityLogs_users . " " . $text . ": " . $video_name;
        } else {
            return FALSE;
        }
    }

    function sendEmail() {
        echo "text";
        exit;
    }

    function is_enabled_framework_and_standards($account_id) {
        App::import("Model", "AccountFolderMetaData");
        $db = new AccountFolderMetaData();
        $check_data = $db->find("first", array("conditions" => array(
                "account_folder_id" => $account_id,
                "meta_data_name" => "enable_framework_standard"
        )));

        if (isset($check_data['AccountFolderMetaData']['meta_data_value']) && $check_data['AccountFolderMetaData']['meta_data_value'] == '1') {
            return true;
        } else {
            return false;
        }
    }

    function is_enable_tags($account_id) {
        App::import("Model", "AccountFolderMetaData");
        $db = new AccountFolderMetaData();
        $check_data = $db->find("first", array("conditions" => array(
                "account_folder_id" => $account_id,
                "meta_data_name" => "enable_tags"
        )));
        if (isset($check_data['AccountFolderMetaData']['meta_data_value']) && $check_data['AccountFolderMetaData']['meta_data_value'] == '1') {
            return true;
        } else {
            return false;
        }
    }

    function is_enable_assessment($account_id) {
        App::import("Model", "AccountMetaData");
        $db = new AccountMetaData();
        $check_data = $db->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "enable_matric"
        )));
        if (isset($check_data['AccountMetaData']['meta_data_value']) && $check_data['AccountMetaData']['meta_data_value'] == '1') {
            return true;
        } else {
            return false;
        }
    }

    function is_enable_huddle_tags($account_folder_id) {
        App::import("Model", "AccountFolderMetaData");
        $db = new AccountFolderMetaData();
        $check_data = $db->find("first", array("conditions" => array(
                "account_folder_id" => $account_folder_id,
                "meta_data_name" => "chk_tags"
        )));
        if (isset($check_data['AccountFolderMetaData']['meta_data_value'])) {
            if ($check_data['AccountFolderMetaData']['meta_data_value'] == '1') {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    function is_enabled_huddle_framework_and_standards($account_folder_id) {
        App::import("Model", "AccountFolderMetaData");
        $db = new AccountFolderMetaData();
        $check_data = $db->find("first", array("conditions" => array(
                "account_folder_id" => $account_folder_id,
                "meta_data_name" => "chk_frameworks"
        )));

        if (isset($check_data['AccountFolderMetaData']['meta_data_value'])) {
            if ($check_data['AccountFolderMetaData']['meta_data_value'] == '1') {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    function is_enabled_coach_feedback($account_folder_id) {
        App::import("Model", "AccountFolderMetaData");
        $db = new AccountFolderMetaData();
        $check_data = $db->find("first", array("conditions" => array(
                "account_folder_id" => $account_folder_id,
                "meta_data_name" => "coach_hud_feedback"
        )));

        if (isset($check_data['AccountFolderMetaData']['meta_data_value'])) {
            if ($check_data['AccountFolderMetaData']['meta_data_value'] == '1') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function get_account_video_permissions($account_id) {
        App::import("Model", "AccountMetaData");
        $db = new AccountMetaData();
        $check_data = $db->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "enable_video_library"
        )));
        if (isset($check_data['AccountMetaData']['meta_data_value'])) {
            if ($check_data['AccountMetaData']['meta_data_value'] == '1') {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function get_file_size($document_id) {
        if (empty($document_id)) {
            return false;
        }
        $file_size = 0;
        App::import("Model", "DocumentFiles");
        $db = new DocumentFiles();
        $result = $db->find("first", array("conditions" => array(
                "document_id" => $document_id
        )));

        if (isset($result['DocumentFiles']['file_size']) && !empty($result['DocumentFiles']['file_size'])) {
            $file_size = $result['DocumentFiles']['file_size'];
        } else {

            App::import("Model", "Document");
            $db = new Document();
            $result = $db->find("first", array("conditions" => array(
                    "id" => $document_id
            )));
            $file_size = 0;

            if ($result) {
                $file_size = $result['Document']['file_size'];
            }
        }



        return $this->formatbytes($file_size, 'MB');
    }

    function get_file_size_in_kb($document_id) {
        if (empty($document_id)) {
            return false;
        }

        $file_size = 0;
        App::import("Model", "DocumentFiles");
        $db = new DocumentFiles();
        $result = $db->find("first", array("conditions" => array(
                "document_id" => $document_id
        )));

        if (isset($result['DocumentFiles']['file_size']) && !empty($result['DocumentFiles']['file_size'])) {
            $file_size = $result['DocumentFiles']['file_size'];
        } else {

            App::import("Model", "Document");
            $db = new Document();
            $result = $db->find("first", array("conditions" => array(
                    "id" => $document_id
            )));
            $file_size = 0;

            if ($result) {
                $file_size = $result['Document']['file_size'];
            }
        }

        return $file_size;
    }

    function formatbytes($file_size, $type) {
        switch ($type) {
            case "KB":
                $filesize = $file_size * .0009765625; // bytes to KB
                break;
            case "MB":
                $filesize = $file_size * 0.000001; // bytes to MB
                break;
            case "GB":
                $filesize = $file_size * .000976562 * .0009765625 * .0009765625; // bytes to GB
                break;
        }
        if ($filesize <= 0) {
            return $filesize = 'unknown file size';
        } else {
            return round($filesize, 2) . ' ' . $type;
        }
    }

    function filesize_formatted($file) {
        $bytes = filesize($file);

        if ($bytes >= 1073741824) {
            return number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            return number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            return number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            return $bytes . ' bytes';
        } elseif ($bytes == 1) {
            return '1 byte';
        } else {
            return '0 bytes';
        }
    }

    function get_document_url($document_video) {
        App::import("Model", "DocumentFiles");
        $db = new DocumentFiles();
        $videoFilePath = pathinfo($document_video['url']);
        $videoFileExtension = $videoFilePath['extension'];
        if ($document_video['published'] == 0) {

            return array(
                'url' => $document_video['url'],
                'thumbnail' => '',
                'encoder_status' => $document_video['encoder_status']
            );
        }

        $check_data = $db->find("first", array("conditions" => array(
                "document_id" => $document_video['id'],
                "default_web" => true
        )));
        $duration = 0;
        if (isset($check_data) && isset($check_data['DocumentFiles']['url'])) {

            $url = $check_data['DocumentFiles']['url'];
            $duration = $check_data['DocumentFiles']['duration'];
            $thumbnail = '';
            $videoFilePath = pathinfo($url);
            $videoFileName = $videoFilePath['filename'];
            //these are old files before JobQueue
            if ($check_data['DocumentFiles']['transcoding_status'] == '-1') {

                $videoFilePathThumb = pathinfo($document_video['url']);
                $videoFileNameThumb = $videoFilePathThumb['filename'];
            } else {
                $videoFileNameThumb = $videoFileName;
            }


            $poster_end_extension = "_thumb.png";
            if ($document_video['encoder_provider'] == '2') {
                $poster_end_extension = empty($document_video['thumbnail_number']) ?
                        "_thumb_00001.png" :
                        '_thumb_' . sprintf('%05d', $document_video['thumbnail_number']) . '.png';
            }

            $thumbnail_image_path = '';
            if (Configure::read('use_cloudfront') == true) {
                if ($check_data['DocumentFiles']['transcoding_status'] == '2') {
                    if ($this->site_id != 1) {
                        $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/video-thumbnail-' . $this->site_id . '.png');
                    } else {
                        $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/video-thumbnail.png');
                    }
                } else {
                    if ($videoFileExtension == 'mp3' || $videoFileExtension == 'm4a' || $videoFileExtension == 'wav') {
                        $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/audio-thumbnail.png');
                    } else {
                        if ($videoFilePath['dirname'] == ".")
                            $thumbnail_image_path = $this->getSecureAmazonUrl(($videoFileNameThumb) . "$poster_end_extension");
                        else
                            $thumbnail_image_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileNameThumb) . "$poster_end_extension");
                    }
                }
            } else {
                if ($videoFileExtension == 'mp3' || $videoFileExtension == 'm4a' || $videoFileExtension == 'wav') {
                    $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/audio-thumbnail.png');
                } else {
                    if ($videoFilePath['dirname'] == ".")
                        $thumbnail_image_path = $this->getSecureAmazonUrl(($videoFileNameThumb) . "$poster_end_extension");
                    else
                        $thumbnail_image_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileNameThumb) . "$poster_end_extension");
                }
            }

            if ($thumbnail_image_path == '') {
                if ($this->site_id != 1) {
                    $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/video-thumbnail-' . $this->site_id . '.png');
                } else {
                    $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/video-thumbnail.png');
                }
            }

            $video_path = '';
            $lvideo_path = '';
            $relative_video_path = '';

            if (Configure::read('use_cloudfront') == true) {

                if ($videoFilePath['dirname'] == ".") {

                    $video_path = $this->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) . ".mp4");
                    $lvideo_path = $this->getSecureAmazonCloudFrontUrl(urlencode($videoFileName) . ".mp4", '', 600, true);
                    $relative_video_path = $videoFileName . ".mp4";
                } else {

                    $video_path = $this->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . ".mp4");
                    $lvideo_path = $this->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileName) . ".mp4", "", 600, true);
                    $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".mp4";
                }
            } else {

                if ($videoFilePath['dirname'] == ".") {

                    $video_path = $this->getSecureAmazonUrl(($videoFileName) . ".mp4");
                    $lvideo_path = $this->getSecureAmazonUrl(($videoFileName) . ".mp4");
                    $relative_video_path = $videoFileName . ".mp4";
                } else {

                    $video_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileName) . ".mp4");
                    $lvideo_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileName) . ".mp4");
                    $relative_video_path = $videoFilePath['dirname'] . "/" . $videoFileName . ".mp4";
                }
            }


            if ($duration == 0) {

                try {

                    $use_ffmpeg_path = Configure::read('use_ffmpeg_path');
                    $ffmpeg_binaries = Configure::read('ffmpeg_binaries');
                    $ffprobe_binaries = Configure::read('ffprobe_binaries');

                    if ($use_ffmpeg_path == 0) {

                        $ffmpeg = FFMpeg::create();
                    } else {

                        $ffmpeg = FFMpeg::create(array(
                                    'ffmpeg.binaries' => $ffmpeg_binaries,
                                    'ffprobe.binaries' => $ffprobe_binaries,
                        ));
                    }

                    $ffmpeg_streams = $ffmpeg->getFFProbe()->streams($lvideo_path);
                    if (isset($ffmpeg_streams)) {

                        $ffmpeg_streams_videos = $ffmpeg_streams->videos();
                        if (isset($ffmpeg_streams_videos)) {

                            $ffmpeg_streams_videos_first = $ffmpeg_streams_videos->first();
                            if (isset($ffmpeg_streams_videos_first)) {
                                $duration = $ffmpeg_streams_videos->first()->get('duration');
                            }
                        }
                    }
                } catch (Exception $e) {
                    $duration = 0;
                }
            }
            //echo $thumbnail_image_path;
            //die;
            return array(
                'url' => $video_path,
                'relative_url' => $relative_video_path,
                'thumbnail' => $thumbnail_image_path,
                'duration' => $duration,
                'encoder_status' => (isset($document_video['encoder_status']) ? $document_video['encoder_status'] : '')
            );
        } else {
            //todo
            //through the exception here
            return array(
                'url' => '',
                'relative_url' => '',
                'thumbnail' => '',
                'duration' => '',
                'encoder_status' => 'Error'
            );
        }
    }

    function getSecureSibmeResouceUrlVideo($url) {
        if (Configure::read('use_local_file_store') == false) {
            //This comes from key pair you generated for cloudfront
            $keyPairId = Configure::read('cloudfront_keypair');

            //Read Cloudfront Private Key Pair
            $cloudFront = CloudFrontClient::factory(array(
                        'private_key' => realpath(dirname(__FILE__) . '/../..') . "/Config/cloudfront_key.pem",
                        'key_pair_id' => $keyPairId,
            ));

            $streamHostUrl = Configure::read('amazon_assets_url');
            $resourceKey = $url;
            $expires = time() + 21600;

            //Construct the URL
            $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
                'url' => $streamHostUrl . '' . $resourceKey,
                'expires' => $expires,
            ));

            return $signedUrlCannedPolicy;
        } else {
            return $url;
        }
    }

    function getSecureSibmeResouceUrl($url) {
        if (Configure::read('use_local_file_store') == false) {
            //This comes from key pair you generated for cloudfront
            $keyPairId = Configure::read('cloudfront_keypair');

            //Read Cloudfront Private Key Pair
            $cloudFront = CloudFrontClient::factory(array(
                        'private_key' => realpath(dirname(__FILE__) . '/../..') . "/Config/cloudfront_key.pem",
                        'key_pair_id' => $keyPairId,
            ));

            $streamHostUrl = Configure::read('amazon_assets_url');
            $resourceKey = $url;
            $expires = time() + 21600;

            //Construct the URL
            $signedUrlCannedPolicy = $cloudFront->getSignedUrl(array(
                'url' => $streamHostUrl . 'app' . $resourceKey,
                'expires' => $expires,
            ));

            return $signedUrlCannedPolicy;
        } else {
            return $url;
        }
    }

    function count_huddles_in_folder($account_folder_id, $user_id) {
        App::import("Model", "AccountFolder");
        App::import("Model", "AccountFolderUser");
        $db1 = new AccountFolderUser();
        $arrays = $db1->find("all", array("conditions" => array(
                "user_id" => $user_id,
        )));
        // print_r($arrays);die();
        $user_ids = array();
        foreach ($arrays as $array) {
            $account_folder_ids[] = $array['AccountFolderUser']['account_folder_id'];
        }
        //print_r($account_folder_ids);die();

        $db = new AccountFolder();
        $check_data = $db->find("all", array("conditions" => array(
                "parent_folder_id" => $account_folder_id,
                "folder_type" => "1",
                "active" => "1",
                "account_folder_id" => $account_folder_ids
        )));

        return count($check_data);
    }

    function count_coaching_colab_huddles_in_folder($account_folder_id, $user_id, $bool) {

        App::import("Model", "AccountFolder");
        App::import("Model", "AccountFolderUser");
        App::import("Model", "AccountFolderMetaData");

        /*

          $db1 = new AccountFolderUser();
          $arrays = $db1->find("all", array("conditions" => array(
          "user_id" => $user_id,
          )));
          // print_r($arrays);die();
          $account_folder_ids = array();
          foreach ($arrays as $array) {
          $account_folder_ids[] = $array['AccountFolderUser']['account_folder_id'];
          }
          //print_r($account_folder_ids);die();

         */

        $account_folder_model = new AccountFolder();

        $account_id_detail = $account_folder_model->find("first", array("conditions" => array(
                "account_folder_id" => $account_folder_id
        )));

        $account_id = $account_id_detail['AccountFolder']['account_id'];



        $db = new AccountFolder();
        $check_data = $db->find("all", array("conditions" => array(
                "parent_folder_id" => $account_folder_id,
                "folder_type" => "1",
                "active" => "1",
                "account_id" => $account_id,
                "((account_folder_id in (select account_folder_id from account_folder_users where user_id=$user_id) ) OR (account_folder_id IN (select  account_folder_id from account_folder_groups afg join user_groups ug on afg.group_id = ug.group_id where ug.user_id = $user_id  ) ) )"
        )));

        $huddle_ids = array();
        foreach ($check_data as $check) {
            $huddle_ids[] = $check['AccountFolder']['account_folder_id'];
        }
        $db2 = new AccountFolderMetaData();
        $count_coaching_huddles = 0;
        $count_colab_huddles = 0;
        $count_eval_huddles = 0;

        foreach ($huddle_ids as $huddle_id) {
            if ($bool == 2) {
                $coaching_huddles = $db2->find("all", array("conditions" => array(
                        "meta_data_value" => "2",
                        "meta_data_name" => "folder_type",
                        "account_folder_id" => $huddle_id
                )));
                if (count($coaching_huddles) > 0) {
                    $count_coaching_huddles++;
                }
            } elseif ($bool == 1) {
//                $collab_huddles = $db2->find("all", array("conditions" => array(
//                        "meta_data_value" => "1",
//                        "meta_data_name" => "folder_type",
//                        "account_folder_id" => $huddle_id
//                )));

                $coaching_huddles = $db2->find("all", array("conditions" => array(
                        "meta_data_value" => "2",
                        "meta_data_name" => "folder_type",
                        "account_folder_id" => $huddle_id
                )));

                $evaluation_huddles = $db2->find("all", array("conditions" => array(
                        "meta_data_value" => "3",
                        "meta_data_name" => "folder_type",
                        "account_folder_id" => $huddle_id
                )));


                if (!count($coaching_huddles) > 0 && !count($evaluation_huddles) > 0) {
                    $count_colab_huddles++;
                }
            } elseif ($bool == 3) {
                $evaluation_huddles = $db2->find("all", array("conditions" => array(
                        "meta_data_value" => "3",
                        "meta_data_name" => "folder_type",
                        "account_folder_id" => $huddle_id
                )));
                if (count($evaluation_huddles) > 0) {
                    $count_eval_huddles++;
                }
            }
        }
        if ($bool == 2) {
            return $count_coaching_huddles;
        } elseif ($bool == 3) {
            return $count_eval_huddles;
        } elseif ($bool == 1) {
            return $count_colab_huddles;
        }
    }

    function count_archive_coaching_colab_huddles_in_folder($account_folder_id, $user_id, $bool) {
        App::import("Model", "AccountFolder");
        App::import("Model", "AccountFolderUser");
        App::import("Model", "AccountFolderMetaData");
        $db1 = new AccountFolderUser();
        $arrays = $db1->find("all", array("conditions" => array(
                "user_id" => $user_id,
        )));
        // print_r($arrays);die();
        $account_folder_ids = array();
        foreach ($arrays as $array) {
            $account_folder_ids[] = $array['AccountFolderUser']['account_folder_id'];
        }
        //print_r($account_folder_ids);die();

        $db = new AccountFolder();
        $check_data = $db->find("all", array("conditions" => array(
                "parent_folder_id" => $account_folder_id,
                "folder_type" => "1",
                "active" => "0",
                "archive" => "1",
                "account_folder_id" => $account_folder_ids
        )));
        $huddle_ids = array();
        foreach ($check_data as $check) {
            $huddle_ids[] = $check['AccountFolder']['account_folder_id'];
        }
        $db2 = new AccountFolderMetaData();
        $count_coaching_huddles = 0;
        $count_colab_huddles = 0;

        foreach ($huddle_ids as $huddle_id) {
            $coaching_huddles = $db2->find("all", array("conditions" => array(
                    "meta_data_value" => "2",
                    "meta_data_name" => "folder_type",
                    "account_folder_id" => $huddle_id
            )));
            if (count($coaching_huddles) > 0) {
                $count_coaching_huddles++;
            } else {
                $count_colab_huddles++;
            }
        }
        if ($bool == 2) {
            return $count_coaching_huddles;
        } else {
            return $count_colab_huddles;
        }
    }

    function folder_has_huddle_participating($folder_id, $user_id, $account_id) {
        App::import("Model", "AccountFolder");
        $db = new AccountFolder();

        $folder_ids = $this->get_folder_childs($folder_id);
        $flag = false;
        foreach ($folder_ids as $fold_id) {
            $has_huddle = $db->hasAccountHuddlesInFolder($account_id, $user_id, $fold_id);
            if ($has_huddle == true) {

                $flag = true;
            }
        }

        if ($flag) {
            return true;
        } else {
            return false;
        }
    }

    function count_folders_in_folder($folder_id, $user_id, $account_id) {
        App::import("Model", "AccountFolder");
        $db = new AccountFolder();
        $check_data = $db->find("all", array("conditions" => array(
                "parent_folder_id" => $folder_id,
                "folder_type" => "5",
                "active" => "1",
        )));

        $count_folder = 0;
        foreach ($check_data as $check) {
            if ($this->folder_has_huddle_participating($check['AccountFolder']['account_folder_id'], $user_id, $account_id) || $check['AccountFolder']['created_by'] == $user_id) {
                $count_folder++;
            }
        }
        return $count_folder;
    }

    function count_archive_folders_in_folder($folder_id, $user_id, $account_id) {
        App::import("Model", "AccountFolder");
        $db = new AccountFolder();
        $check_data = $db->find("all", array("conditions" => array(
                "parent_folder_id" => $folder_id,
                "folder_type" => "5",
                "active" => "0",
                "archive" => "1"
        )));

        $count_folder = 0;
        foreach ($check_data as $check) {
            if ($this->folder_has_huddle_participating($check['AccountFolder']['account_folder_id'], $user_id, $account_id) || $check['AccountFolder']['created_by'] == $user_id) {
                $count_folder++;
            }
        }
        return $count_folder;
    }

    function check_folders_in_account($account_id) {
        App::import("Model", "AccountFolder");
        $db = new AccountFolder();
        $check_data = $db->find("all", array("conditions" => array(
                "folder_type" => "5",
                "active" => "1",
                "account_id" => $account_id
        )));

        if (count($check_data) > 0) {
            return true;
        } else {
            return false;
        }
    }

    function user_name_from_id($user_id) {
        App::import("Model", "User");
        $db = new User();
        $check_data = $db->find('first', array("conditions" => array(
                "id" => $user_id,
        )));

        return $check_data['User']['first_name'] . ' ' . $check_data['User']['last_name'];
    }

    function get_user_detail($user_id) {
        App::import("Model", "User");
        $db = new User();
        $check_data = $db->find('first', array("conditions" => array(
                "id" => $user_id,
        )));

        return $check_data;
    }

    function check_enable_analytics($account_id) {
        App::import("Model", "AccountMetaData");
        $db = new AccountMetaData();
        $check_data = $db->find('first', array("conditions" => array(
                "meta_data_name" => 'enableanalytics',
                "account_id" => $account_id
        )));
        if (!empty($check_data)) {
            return $check_data['AccountMetaData']['meta_data_value'];
        } else {
            return 0;
        }
    }

    function coach_or_collab($huddle_id) {
        App::import("Model", "AccountFolderMetaData");
        $db = new AccountFolderMetaData();
        $coaching_huddles = $db->find("all", array("conditions" => array(
                "meta_data_value" => "2",
                "meta_data_name" => "folder_type",
                "account_folder_id" => $huddle_id
        )));
        if (count($coaching_huddles) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function check_if_eval_huddle($huddle_id) {

        App::import("Model", "AccountFolderMetaData");
        $db = new AccountFolderMetaData();
        $eval_huddles = $db->find("count", array("conditions" => array(
                "meta_data_value" => "3",
                "meta_data_name" => "folder_type",
                "account_folder_id" => $huddle_id
        )));

        if ($eval_huddles > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function get_all_parents($huddle_id, $parents) {
        App::import("Model", "AccountFolder");
        $db = new AccountFolder();
        $result = $db->find("first", array("conditions" => array(
                "account_folder_id" => $huddle_id,
        )));
        if ($result['AccountFolder']['parent_folder_id'] == NULL) {
            return $parents;
        } else {
            $parents[] = $result['AccountFolder']['parent_folder_id'];
            return $this->get_all_parents($result['AccountFolder']['parent_folder_id'], $parents);
        }
    }

    function workspace_video($huddle_id) {
        App::import("Model", "AccountFolder");
        $db = new AccountFolder();
        $result = $db->find("first", array("conditions" => array(
                "account_folder_id" => $huddle_id,
        )));

        if ($result['AccountFolder']['folder_type'] == 3) {
            return 1;
        } else {
            return 0;
        }
    }

    function library_video($huddle_id) {
        App::import("Model", "AccountFolder");
        $db = new AccountFolder();
        $result = $db->find("first", array("conditions" => array(
                "account_folder_id" => $huddle_id,
        )));
        if ($result['AccountFolder']['folder_type'] == 2) {
            return 1;
        } else {
            return 0;
        }
    }

    function transcoding_status($video_id) {
        App::import("Model", "DocumentFiles");
        $db = new DocumentFiles();
        $document_files = $db->find('first', array('conditions' => array('document_id' => $video_id)));

        return $document_files['DocumentFiles']['transcoding_status'];
    }

    function user_present_huddle($user_id, $huddle_id) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $joins = array(
            array(
                'table' => 'account_folders',
                'alias' => 'AccountFolder',
                'type' => 'left',
                'conditions' => 'AccountFolder.account_folder_id = AccountFolderUser.account_folder_id'
            ),
            array(
                'table' => 'account_folder_groups',
                'alias' => 'afg',
                'type' => 'left',
                'conditions' => 'AccountFolder.account_folder_id = afg.account_folder_id'
            ),
            array(
                'table' => 'user_groups as ug',
                'type' => 'left',
                'conditions' => array('ug.group_id = afg.group_id')
            )
        );
        $result = $db->find("all", array('joins' => $joins, "conditions" => array(
                "AccountFolder.account_folder_id" => $huddle_id,
                'OR' => array(
                    'AccountFolderUser.user_id' => $user_id,
                    'ug.user_id' => $user_id
                )
        )));
        if (count($result) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function account_storage_limit($account_id) {

        App::import("Model", "Account");
        $db = new Account();
        $result = $db->find("first", array("conditions" => array(
                "id" => $account_id
        )));

        $plan_id = $result['Account']['plan_id'];

        App::import("Model", "Plans");
        $db = new Plans();

        $result1 = $db->find("first", array("conditions" => array(
                "id" => $plan_id
        )));

        $total_space = $result1['Plans']['storage'];
        $space_consumed = $result['Account']['storage_used'] / (1024 * 1024 * 1024);

        if ($space_consumed > $total_space) {
            return 1;
        } else {
            return 0;
        }
    }

    function get_document_holder_url($document_video) {
        App::import("Model", "DocumentFiles");
        $db = new DocumentFiles();
        $videoFilePath = pathinfo($document_video['url']);
        $videoFileExtension = $videoFilePath['extension'];
        if ($document_video['published'] == 0) {

            return array(
                'url' => $document_video['url'],
                'thumbnail' => '',
                'encoder_status' => $document_video['encoder_status']
            );
        }

        $check_data = $db->find("first", array("conditions" => array(
                "document_id" => $document_video['id'],
                "default_web" => true
        )));

        if (isset($check_data) && isset($check_data['DocumentFiles']['url'])) {

            $url = $check_data['DocumentFiles']['url'];
            $duration = $check_data['DocumentFiles']['duration'];
            $thumbnail = '';
            $videoFilePath = pathinfo($url);
            $videoFileName = $videoFilePath['filename'];
            //these are old files before JobQueue
            if ($check_data['DocumentFiles']['transcoding_status'] == '-1') {

                $videoFilePathThumb = pathinfo($document_video['url']);
                $videoFileNameThumb = $videoFilePathThumb['filename'];
            } else {
                $videoFileNameThumb = $videoFileName;
            }


            $poster_end_extension = "_thumb.png";
            if ($document_video['encoder_provider'] == '2') {
                $poster_end_extension = empty($document_video['thumbnail_number']) ?
                        "_thumb_00001.png" :
                        '_thumb_' . sprintf('%05d', $document_video['thumbnail_number']) . '.png';
            }

            $thumbnail_image_path = '';
            if (Configure::read('use_cloudfront') == true) {
                if ($check_data['DocumentFiles']['transcoding_status'] == '2') {
                    if ($this->site_id != 1) {
                        $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/video-thumbnail-' . $this->site_id . '.png');
                    } else {
                        $thumbnail_image_path = $this->getSecureSibmeResouceUrl('/img/video-thumbnail.png');
                    }
                } else {
                    if ($videoFileExtension == 'mp3' || $videoFileExtension == 'm4a' || $videoFileExtension == 'wav') {
                        $thumbnail_image_path = $this->getSecureSibmeResouceUrl('/img/audio-thumbnail.png');
                    } else {
                        if ($videoFilePath['dirname'] == ".")
                            $thumbnail_image_path = $this->getSecureAmazonCloudFrontUrl(urlencode($videoFileNameThumb) . "$poster_end_extension");
                        else
                            $thumbnail_image_path = $this->getSecureAmazonCloudFrontUrl($videoFilePath['dirname'] . "/" . urlencode($videoFileNameThumb) . "$poster_end_extension");
                    }
                }
            } else {
                if ($videoFileExtension == 'mp3' || $videoFileExtension == 'm4a' || $videoFileExtension == 'wav') {
                    $thumbnail_image_path = $this->getSecureSibmeResouceUrl('/img/audio-thumbnail.png');
                } else {
                    if ($videoFilePath['dirname'] == ".")
                        $thumbnail_image_path = $this->getSecureAmazonUrl(($videoFileNameThumb) . "$poster_end_extension");
                    else
                        $thumbnail_image_path = $this->getSecureAmazonUrl($videoFilePath['dirname'] . "/" . ($videoFileNameThumb) . "$poster_end_extension");
                }
            }

            if ($thumbnail_image_path == '') {
                if ($this->site_id != 1) {
                    $thumbnail_image_path = $this->getSecureAmazonSibmeUrl('app/img/video-thumbnail-' . $this->site_id . '.png');
                } else {
                    $thumbnail_image_path = $this->getSecureSibmeResouceUrl('/img/video-thumbnail.png');
                }
            }
            $video_path = '';
            $relative_video_path = '';


            return array(
                'url' => $video_path,
                'relative_url' => $relative_video_path,
                'thumbnail' => $thumbnail_image_path,
                'duration' => $duration,
                'encoder_status' => (isset($document_video['encoder_status']) ? $document_video['encoder_status'] : '')
            );
        } else {
            //todo
            //through the exception here
            return array(
                'url' => '',
                'relative_url' => '',
                'thumbnail' => '',
                'duration' => '',
                'encoder_status' => 'Error'
            );
        }
    }

    function check_if_coach($account_id, $user_id) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $result = $db->query("SELECT * FROM `account_folder_users` AS afu LEFT JOIN account_folders AS af ON af.account_folder_id = afu.account_folder_id WHERE afu.user_id = " . $user_id . " AND af.account_id = " . $account_id . " AND af.active = 1 AND af.folder_type = 1 AND afu.`is_coach` = 1 AND af.site_id =" . $this->site_id);
        if (count($result) > 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function check_if_recording_stopped($ref_id) {
        App::import("Model", "UserActivityLog");
        $db = new UserActivityLog();
        $check_data = $db->find("first", array("conditions" => array(
                "ref_id" => $ref_id,
                "type" => '21'
        )));

        if (!empty($check_data)) {
            return 1;
        } else {
            return 0;
        }
    }

    function check_if_live_recording_stopped($ref_id) {
        App::import("Model", "UserActivityLog");
        $db = new UserActivityLog();
        $check_data = $db->find("first", array("conditions" => array(
                "ref_id" => $ref_id,
                "type" => '25'
        )));

        if (!empty($check_data)) {
            return 1;
        } else {
            return 0;
        }
    }

    function check_if_eval_huddle_active($account_id) {
        App::import("Model", "Account");
        $db = new Account();
        $result = $db->find("first", array("conditions" => array(
                "id" => $account_id,
        )));
        if ($result['Account']['is_evaluation_activated'] == 1) {
            return true;
        } else {
            return false;
        }
    }

    function check_if_live_rec_active($account_id, $user_id = false,$with_data = false) {
        App::import("Model", "Account");
        $db = new Account();
        $result = $db->find("first", array("conditions" => array(
                "id" => $account_id,
        )));
        $live_permission = false;
        if ($result['Account']['enable_live_rec'] == 1) {

            if($user_id)
            {
                App::import("Model", "UserAccount");
                $db = new UserAccount();
                $check_data = $db->find("first", array("conditions" => array(
                    "account_id" => $account_id,
                    "user_id" => $user_id,
                )));
                if($check_data['UserAccount']['live_recording'] == '1')
                {
                    $live_permission = true;
                }
                else
                {
                    $live_permission = false;
                }
            }
            else
            {
                $live_permission = true;
            }
            if($with_data)
            {
                return ["live_permission" =>$live_permission, "Account" => $result['Account']];
            }
            return $live_permission;
        } else {
            return $live_permission;
        }
    }

    function get_eval_participant_videos($huddle_id, $user_id) {
        App::import("Model", "Document");
        $db = new Document();
        $videos = $db->getVideos($huddle_id, '', '', '', '', $user_id);
        if (count($videos) >= 1) {
            return count($videos);
        } else {
            return 0;
        }
    }

    function check_if_evalutor($huddle_id, $user_id) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $result = $db->find('first', array(
            'conditions' => array(
                'user_id' => $user_id,
                'account_folder_id' => $huddle_id
            )
        ));

        App::import("Model", "AccountFolderGroup");
        $db = new AccountFolderGroup();

        $joins = array(
            array(
                'table' => 'user_groups as ug',
                'type' => 'inner',
                'conditions' => array('ug.group_id = AccountFolderGroup.group_id')
            )
        );


        $result_groups = $db->find('first', array(
            'joins' => $joins,
            'conditions' => array(
                'ug.user_id' => $user_id,
                'AccountFolderGroup.account_folder_id' => $huddle_id,
            )
        ));



        if (($result && $result['AccountFolderUser']['role_id'] == 200) || ($result_groups && $result_groups['AccountFolderGroup']['role_id'] == 200)) {
            return true;
        } else {
            return false;
        }
    }

    function check_if_evaluated_participant($huddle_id, $user_id) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $result = $db->find('first', array(
            'conditions' => array(
                'user_id' => $user_id,
                'account_folder_id' => $huddle_id
            )
        ));

        App::import("Model", "AccountFolderGroup");
        $db = new AccountFolderGroup();

        $joins = array(
            array(
                'table' => 'user_groups as ug',
                'type' => 'inner',
                'conditions' => array('ug.group_id = AccountFolderGroup.group_id')
            )
        );


        $result_groups = $db->find('first', array(
            'joins' => $joins,
            'conditions' => array(
                'ug.user_id' => $user_id,
                'AccountFolderGroup.account_folder_id' => $huddle_id,
            )
        ));

        if ($result && $result['AccountFolderUser']['role_id'] == 200) {
            return false;
        }


        if (($result && $result['AccountFolderUser']['role_id'] == 210) || ( $result_groups && $result_groups['AccountFolderGroup']['role_id'] == 210 )) {
            return true;
        } else {
            return false;
        }
    }

    function get_created_by($user_id) {
        App::import("Model", "User");
        $db = new User();
        $result = $db->find('first', array(
            'conditions' => array(
                'id' => $user_id,
            )
        ));
        return $result['User']['email'];
    }

    function check_plan_permission($account_id) {

        App::import("Model", "Account");
        $db = new Account();
        $result = $db->find("first", array("conditions" => array(
                "id" => $account_id
        )));

        if ($result['Account']['in_trial'] == 1) {
            return 1;
        } else {

            $plan_id = $result['Account']['plan_id'];

            App::import("Model", "Plans");
            $db = new Plans();

            $result1 = $db->find("first", array("conditions" => array(
                    "id" => $plan_id
            )));

            if ($result1['Plans']['plan_permission_id'] == 2) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    function check_plan_deactivate($account_id) {
        App::import("Model", "Account");
        $db = new Account();
        $result = $db->find("first", array("conditions" => array(
                "id" => $account_id
        )));

        if ($result['Account']['deactive_plan'] == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    function get_evaluator_ids($huddle_id) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $result = $db->find('all', array(
            'conditions' => array(
                'role_id' => 200,
                'account_folder_id' => $huddle_id
            )
        ));
        $evaluator = '';
        if ($result) {
            foreach ($result as $row) {
                $evaluator[] = $row['AccountFolderUser']['user_id'];
            }
            return $evaluator;
        } else {
            return FALSE;
        }
    }
    
    function get_evaluator_ids_including_groups($huddle_id) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $result = $db->find('all', array(
            'conditions' => array(
                'role_id' => 200,
                'account_folder_id' => $huddle_id
            )
        ));
        $evaluator = FALSE;
        App::import("Model", "AccountFolderGroup");
        $db = new AccountFolderGroup();

        $joins = array(
            array(
                'table' => 'user_groups as ug',
                'type' => 'inner',
                'conditions' => array('ug.group_id = AccountFolderGroup.group_id')
            )
        );

        $fields = array('ug.*');
        $result_groups = $db->find('all', array(
            'joins' => $joins,
            'conditions' => array(
                'AccountFolderGroup.account_folder_id' => $huddle_id,
                'AccountFolderGroup.role_id' => 200,
            ),
            'fields' => $fields
        ));
        if ($result) {
            foreach ($result as $row) {
                $evaluator[] = $row['AccountFolderUser']['user_id'];
            }
        }
        
        if($result_groups)
        {
             foreach ($result_groups as $row) {
                $evaluator[] = $row['ug']['user_id'];
            }
        }
        
        return $evaluator;
        
    }

    /* function get_all_participant_ids($huddle_id, $user_id) {
      App::import("Model", "AccountFolderUser");
      $db = new AccountFolderUser();
      $result = $db->find('all', array(
      'conditions' => array(
      'role_id' => array(200, 210, 220),
      'account_folder_id' => $huddle_id,
      'user_id NOT IN (' . $user_id . ') '
      )
      ));
      $evaluator = '';
      if ($result) {
      foreach ($result as $row) {
      $evaluator[] = $row['AccountFolderUser']['user_id'];
      }
      return $evaluator;
      } else {
      return FALSE;
      }
      } */

    function get_all_participant_ids($huddle_id, $user_id) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $result = $db->find('all', array(
            'conditions' => array(
                'role_id' => array(200, 210, 220),
                'account_folder_id' => $huddle_id
            )
        ));
        $evaluator = '';
        if ($result) {
            foreach ($result as $row) {
                $evaluator[] = $row['AccountFolderUser']['user_id'];
            }
            return $evaluator;
        } else {
            return [];
        }
    }

    function get_huddle_permissions($huddle_id, $user_id) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $result = $db->find('first', array(
            'conditions' => array(
                'account_folder_id' => $huddle_id,
                'user_id' => $user_id
            )
        ));
        if ($result) {
            return $result['AccountFolderUser']['role_id'];
        } else {
            return FALSE;
        }
    }

    function get_huddle_evaluator_emails($huddle_id) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $joins = array(
            array(
                'table' => 'users as User',
                'type' => 'inner',
                'conditions' => array('User.id=AccountFolderUser.user_id')
            )
        );
        $fields = array('AccountFolderUser.*', 'User.id as user_id', 'User.email');
        $result = $db->find('all', array(
            'conditions' => array(
                'AccountFolderUser.role_id' => 200,
                'AccountFolderUser.account_folder_id' => $huddle_id
            ),
            'fields' => $fields,
            'joins' => $joins
        ));
        $evaluator = '';
        if ($result) {
            foreach ($result as $row) {
                $evaluator[] = $row['User']['email'];
            }
            return $evaluator;
        } else {
            return FALSE;
        }
    }

    function get_participants_ids($huddle_id) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $result = $db->find('all', array(
            'conditions' => array(
                'role_id' => 210,
                'account_folder_id' => $huddle_id
            )
        ));
        $participants = '';
        if ($result) {
            foreach ($result as $row) {
                $participants[] = $row['AccountFolderUser']['user_id'];
            }
            return $participants;
        } else {
            return false;
        }
    }

    function get_all_participants_ids($huddle_id) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $result = $db->find('all', array(
            'conditions' => array(
                //  'role_id' => 210,
                'account_folder_id' => $huddle_id
            )
        ));
        $participants = '';
        if ($result) {
            foreach ($result as $row) {
                $participants[] = $row['AccountFolderUser']['user_id'];
            }
            return $participants;
        } else {
            return false;
        }
    }

    function chck_publish_count($data, $coach_rules) {
        $new_array = array();
        if ($coach_rules == 210) {
            foreach ($data as $row) {
                if (empty($row['Document']['is_associated']) || $row['Document']['is_associated'] == 0) {
                    continue;
                }
                $new_array[] = $row;
            }
            return count($new_array);
        } else {
            return count($data);
        }
    }

    function get_video_details($video_id) {
        App::import("Model", "Document");
        $db = new Document();
        $result = $db->find('first', array(
            'conditions' => array(
                'id' => $video_id
            )
        ));

        return $result;
    }

    function get_video_published($video_id) {
        App::import("Model", "Document");
        $db = new Document();
        $result = $db->find('first', array(
            'conditions' => array(
                'id' => $video_id
            )
        ));

        return $result['Document']['published'];
    }

    function get_submission_date($huddle_id, $return = false) {
        App::import("Model", "AccountFolderMetaData");
        $db = new AccountFolderMetaData();
        $submission_deadline_date = $db->find("first", array("conditions" => array(
                "meta_data_name" => "submission_deadline_date",
                "account_folder_id" => $huddle_id
        )));
        $submission_deadline_time = $db->find("first", array("conditions" => array(
                "meta_data_name" => "submission_deadline_time",
                "account_folder_id" => $huddle_id
        )));
        if ($submission_deadline_date['AccountFolderMetaData']['meta_data_value'] != '') {
            $date_modified = explode('-', $submission_deadline_date['AccountFolderMetaData']['meta_data_value']);
            $date = $date_modified[2] . '-' . $date_modified[0] . '-' . $date_modified[1];
            //$date = date('Y-m-d', strtotime($submission_deadline_date['AccountFolderMetaData']['meta_data_value']));
            // $time = $submission_deadline_time['AccountFolderMetaData']['meta_data_value'] . ':00';
            $time = date("h:i:s a", strtotime($submission_deadline_time['AccountFolderMetaData']['meta_data_value']));
            return $date . ' ' . $time;
        } else {
            return '';
        }
    }

    function get_evaluator_huddles($account_id, $evaluator_id) {
        App::import("Model", "AccountFolder");
        $db = new AccountFolder();
        $account_folder = $db->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_folders_meta_data as afmd',
                    'type' => 'left',
                    'conditions' => 'AccountFolder.`account_folder_id` = afmd.account_folder_id'
                ),
                array(
                    'table' => 'account_folder_users',
                    'alias' => 'huddle_users',
                    'type' => 'left',
                    'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
                ),
                array(
                    'table' => 'users as User',
                    'type' => 'left',
                    'conditions' => array('User.id=huddle_users.user_id')
                )
            ),
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1)',
                'AccountFolder.active' => 1,
                'afmd.meta_data_name' => 'folder_type',
                'afmd.meta_data_value' => 3,
                'User.is_active' => 1,
                'huddle_users.role_id' => 200,
                'huddle_users.user_id' => $evaluator_id,
            ),
            'fields' => array(
                'AccountFolder.*'
            ),
        ));
        return $account_folder;
    }

    function get_users_of_admin_circle($account_id, $user_id) {
        App::import("Model", "AccountFolder");
        $db = new AccountFolder();
        $user_distinct_ids = $db->query("SELECT user_id FROM `account_folder_users` as afu2 WHERE account_folder_id IN
        (SELECT afu.`account_folder_id`
        FROM `account_folders` AS af
        INNER JOIN `account_folder_users` AS afu ON (afu.`account_folder_id` = af.`account_folder_id`)
        WHERE af.`active`=1 AND
        af.`account_id`=" . $account_id . " AND
        af.`folder_type`=1 AND
        afu.`role_id`=200 AND
        af.site_id = " . $this->site_id . " AND
        afu.`user_id`=" . $user_id . ") AND user_id!=" . $user_id . " GROUP BY user_id;");


        $final_users_ids = array();
        foreach ($user_distinct_ids as $user_distinct_id) {
            $final_users_ids[] = $user_distinct_id['afu2']['user_id'];
        }

        if (!empty($final_users_ids)) {
            return $final_users_ids;
        } else {
            return array($user_id);
        }
    }

    /*
      function get_users_of_admin_circle($account_id, $user_id) {
      App::import("Model", "AccountFolder");
      $db = new AccountFolder();
      $user_distinct_ids = $db->find('all', array(
      'joins' => array(
      array(
      'table' => 'account_folders_meta_data as afmd',
      'type' => 'left',
      'conditions' => 'AccountFolder.`account_folder_id` = afmd.account_folder_id'
      ),
      array(
      'table' => 'account_folder_users',
      'alias' => 'huddle_users',
      'type' => 'left',
      'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
      ),
      array(
      'table' => 'account_folder_users as afu',
      'type' => 'left',
      'conditions' => array('afu.account_folder_id = AccountFolder.account_folder_id')
      ),
      array(
      'table' => 'users as User',
      'type' => 'left',
      'conditions' => array('User.id=huddle_users.user_id')
      ),
      ),
      'conditions' => array(
      'AccountFolder.account_id' => $account_id,
      'AccountFolder.folder_type IN(1)',
      'AccountFolder.active' => 1,
      //       'afmd.meta_data_name' => 'folder_type',
      //       'afmd.meta_data_value' => 3,
      'User.is_active' => 1,
      'huddle_users.role_id' => 200,
      'huddle_users.user_id' => $user_id,
      ),
      'fields' => array(
      'DISTINCT afu.user_id as user_id'
      ),
      ));


      $final_users_ids = array();
      foreach ($user_distinct_ids as $user_distinct_id) {
      $final_users_ids[] = $user_distinct_id['afu']['user_id'];
      }

      if (!empty($final_users_ids)) {
      return $final_users_ids;
      } else {
      return array($user_id);
      }
      }
     */

    function get_user_huddle_ids($account_id, $user_id, $bool = 0) {
        App::import("Model", "AccountFolder");
        $check = 'AccountFolder.folder_type IN(1,2,3)';

        if ($bool = 1) {
            $check = 'AccountFolder.folder_type IN(1)';
        }
        $db = new AccountFolder();
        $user_distinct_ids = $db->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_folders_meta_data as afmd',
                    'type' => 'left',
                    'conditions' => 'AccountFolder.`account_folder_id` = afmd.account_folder_id'
                ),
                array(
                    'table' => 'account_folder_users',
                    'alias' => 'huddle_users',
                    'type' => 'left',
                    'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
                ),
                array(
                    'table' => 'account_folder_users as afu',
                    'type' => 'left',
                    'conditions' => array('afu.account_folder_id = AccountFolder.account_folder_id')
                ),
                array(
                    'table' => 'users as User',
                    'type' => 'left',
                    'conditions' => array('User.id=huddle_users.user_id')
                ),
            ),
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                $check,
                'AccountFolder.active' => 1,
                //       'afmd.meta_data_name' => 'folder_type',
                //       'afmd.meta_data_value' => 3,
                'User.is_active' => 1,
                //  'huddle_users.role_id' => 200,
                'huddle_users.user_id' => $user_id,
            ),
            'fields' => array(
                'DISTINCT AccountFolder.account_folder_id as huddle_id'
            ),
        ));


        $final_users_ids = array();
        foreach ($user_distinct_ids as $user_distinct_id) {
            $final_users_ids[] = $user_distinct_id['AccountFolder']['huddle_id'];
        }

        if (!empty($final_users_ids)) {
            return $final_users_ids;
        } else {
            return array(0);
        }
    }

    function get_evaluators_emails($account_id, $evaluator_id) {
        App::import("Model", "AccountFolder");
        $db = new AccountFolder();
        $account_folder = $db->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_folders_meta_data as afmd',
                    'type' => 'left',
                    'conditions' => 'AccountFolder.`account_folder_id` = afmd.account_folder_id'
                ),
                array(
                    'table' => 'account_folder_users',
                    'alias' => 'huddle_users',
                    'type' => 'left',
                    'conditions' => array('huddle_users.account_folder_id = AccountFolder.account_folder_id')
                ),
                array(
                    'table' => 'users as User',
                    'type' => 'left',
                    'conditions' => array('User.id=huddle_users.user_id')
                )
            ),
            'conditions' => array(
                'AccountFolder.account_id' => $account_id,
                'AccountFolder.folder_type IN(1)',
                'AccountFolder.active' => 1,
                'afmd.meta_data_name' => 'folder_type',
                'afmd.meta_data_value' => 3,
                'User.is_active' => 1,
                'huddle_users.role_id' => 200,
                'huddle_users.user_id' => $evaluator_id,
            ),
            'fields' => array(
                'AccountFolder.*'
            ),
        ));
        $emails = '';
        if ($account_folder) {

            foreach ($account_folder as $row) {
                $emails[] = $row['User']['email'];
            }

            $emails = implode(',', $emails);
            return $emails;
        }
    }

    function get_huddle_participants($coach_hud) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $account_coachees = $db->find('all', array(
            'joins' => array(
                array(
                    'table' => 'users as User',
                    'type' => 'left',
                    'conditions' => array('User.id=AccountFolderUser.user_id')
                )
            ),
            'conditions' => array(
                'AccountFolderUser.account_folder_id' => $coach_hud,
                'User.is_active' => 1,
                'AccountFolderUser.role_id' => 210
            ),
            'fields' => array(
                'User.*'
            ),
            'group' => array('AccountFolderUser.user_id'),
        ));
        return $account_coachees;
    }

    function user_role_in_account($account_id, $user_id) {
        App::import("Model", "UserAccount");
        $db = new UserAccount();

        $result = $db->find('first', array(
            'conditions' => array(
                'account_id' => $account_id,
                'user_id' => $user_id
            )
        ));

        if (!empty($result)) {
            return $result['UserAccount']['role_id'];
        } else {
            return 0;
        }
    }

    function get_allowed_users($account_id) {
        $trial_users = 40;
        $unlimited_users = 100000;

        App::import("Model", "Account");
        $db = new Account();

        $result = $db->find('first', array(
            'conditions' => array(
                'id' => $account_id,
            )
        ));

//        print_r($result);die;

        if ($result['Account']['deactive_plan']) {
            if ($result['Account']['custom_users'] == -1) {
                return $unlimited_users;
            } else {
                return $result['Account']['custom_users'];
            }
        } elseif ($result['Account']['in_trial']) {
            return $trial_users;
        } elseif ($result['Account']['plan_id'] < 9) {
            App::import("Model", "Plans");
            $db = new Plans();

            $plans = $db->find('first', array(
                'conditions' => array(
                    'id' => $result['Account']['plan_id'],
                )
            ));
            return $plans['Plans']['users'];
        } else {
            return $result['Account']['plan_qty'];
        }
    }

    function get_allowed_storage($account_id) {

        $trial_storage = 5;
        $unlimited_storage = 100000;

        App::import("Model", "Account");
        $db = new Account();

        $result = $db->find('first', array(
            'conditions' => array(
                'id' => $account_id,
            )
        ));

//        print_r($result);die;

        if ($result['Account']['deactive_plan']) {
            if ($result['Account']['custom_storage'] == -1) {
                return $unlimited_storage;
            } else {
                return $result['Account']['custom_storage'];
            }
        } elseif ($result['Account']['in_trial']) {
            return $trial_storage;
        } elseif ($result['Account']['plan_id'] < 9) {
            App::import("Model", "Plans");
            $db = new Plans();

            $plans = $db->find('first', array(
                'conditions' => array(
                    'id' => $result['Account']['plan_id'],
                )
            ));
            return $plans['Plans']['storage'];
        } else {

            App::import("Model", "Plans");
            $db = new Plans();

            $plans = $db->find('first', array(
                'conditions' => array(
                    'id' => $result['Account']['plan_id'],
                )
            ));

            return $result['Account']['plan_qty'] * $plans['Plans']['storage'];
        }
    }

    function get_submission_allowed($account_folder_id) {

        App::import("Model", "AccountFolderMetaData");
        $db = new AccountFolderMetaData();

        $submission_allowed_count = 1;
        $submission_allowed = $db->find('first', array('conditions' => array('account_folder_id' => $account_folder_id, 'meta_data_name' => 'submission_allowed')));

        if (isset($submission_allowed['AccountFolderMetaData']['meta_data_value'])) {

            $submission_allowed_count = $submission_allowed['AccountFolderMetaData']['meta_data_value'];
        }

        return $submission_allowed_count;
    }

    function get_users_added($account_id) {
        App::import("Model", "User");
        $db = new User();

        $total_users = $db->getTotalUsers($account_id);

        return $total_users;
    }

    function get_consumed_storage($account_id) {

        App::import("Model", "Account");
        $db = new Account();

        $result = $db->find('first', array(
            'conditions' => array(
                'id' => $account_id,
            )
        ));
        return (($result['Account']['storage_used'] / 1024) / 1024) / 1024;
    }

    function get_framework_id($account_id, $huddle_id) {


        $framework_id = '';

        App::import("Model", "AccountFolderMetaData");
        $db = new AccountFolderMetaData();

        $id_framework = $db->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'framework_id')));
        $framework_id = isset($id_framework['AccountFolderMetaData']['meta_data_value']) ? $id_framework['AccountFolderMetaData']['meta_data_value'] : "0";

        App::import("Model", "AccountMetaData");
        $db = new AccountMetaData();

        if ($framework_id == -1 || $framework_id == 0) {
            $id_framework = $db->find('first', array('conditions' => array('account_id' => $account_id, 'meta_data_name' => 'default_framework')));
            if (!empty($id_framework['AccountMetaData']['meta_data_value']) && isset($id_framework['AccountMetaData']['meta_data_value'])) {
                $framework_id = $id_framework['AccountMetaData']['meta_data_value'];
            } else {
                $framework_id = 0;
            }
        }

        return $framework_id;
    }

    function check_if_submission_date_passed($huddleId, $user_id) {
        if ($this->check_if_evaluated_participant($huddleId, $user_id)) {
            $submission_date = $this->get_submission_date($huddleId);
            if ($submission_date != '') {
                $submission_string = strtotime($submission_date);
                $current_time = strtotime(date('Y-m-d h:i:s a', time()));
                //echo $current_time.'  '.$submission_date;die;
                if ($submission_string < $current_time) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    function check_trial($account_id) {

        App::import("Model", "Account");
        $db = new Account();

        $result = $db->find('first', array(
            'conditions' => array(
                'id' => $account_id,
            )
        ));
        return $result['Account']['in_trial'];
    }

    function get_video_comments($video_id) {

        App::import("Model", "Comment");
        $db = new Comment();
        $result = $db->find('all', array(
            'conditions' => array(
                'ref_id' => $video_id,
                'active' => 1
            )
        ));

        return $result;
    }

    function dis_mem_del_video($account_id, $logged_user_role, $huddle_type) {

        App::import("Model", "AccountMetaData");
        $db = new AccountMetaData();
        $check_data = $db->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "dis_mem_del_video"
        )));

        if (!empty($check_data)) {

            if ($check_data['AccountMetaData']['meta_data_value'] && $logged_user_role == 210 && $huddle_type == 1) {
                return 0;
            } else {
                return 1;
            }
        } else {

            return 1;
        }
    }

    function coaching_perfomance_level($account_id, $huddle_type, $logged_user_role) {

        App::import("Model", "AccountMetaData");
        $db = new AccountMetaData();
        $check_data = $db->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "coaching_perfomance_level"
        )));

        if (!empty($check_data)) {

            if ($check_data['AccountMetaData']['meta_data_value'] && ($logged_user_role == 200 || $logged_user_role == 210) && $huddle_type == 2) {
                return 1;
            } else {
                return 0;
            }
        } else {

            return 0;
        }
    }

    function coaching_perfomance_level_without_role($account_id) {

        App::import("Model", "AccountMetaData");
        $db = new AccountMetaData();
        $check_data = $db->find("first", array("conditions" => array(
                "account_id" => $account_id,
                "meta_data_name" => "coaching_perfomance_level"
        )));

        if (!empty($check_data)) {

            return $check_data['AccountMetaData']['meta_data_value'];
        } else {

            return 0;
        }
    }

    function get_video_comment_numbers($video_id, $huddle_id = '', $user_id = '') {
        App::import("Model", "AccountFolderMetaData");
        $db = new AccountFolderMetaData();
        $h_type = $db->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $h_type = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $ref_type = array(2, 3, 6, 7);


        App::import("Model", "Comment");
        $db = new Comment();


        if ((!$this->check_if_evalutor($huddle_id, $user_id)) && (($h_type == '2' && $this->is_enabled_coach_feedback($huddle_id)) || $h_type == '3')) {
            $result = $db->find('all', array(
                'conditions' => array(
                    'ref_id' => $video_id,
                    'active' => 1,
                    'ref_type' => $ref_type
                )
            ));
        } else {
            $result = $db->find('all', array(
                'conditions' => array(
                    'ref_id' => $video_id,
                    // 'active' => 1
                    'ref_type' => $ref_type
                )
            ));
        }

        return count($result);
    }

    function get_video_attachment_numbers_for_lti($video_id, $huddle_id, $user_id) {
        App::import("Model", "AccountFolderMetaData");
        App::import("Model", "Document");
        $dbafd = new AccountFolderMetaData();
        $dbdoc = new Document();
        $h_type = $dbafd->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'meta_data_name' => 'folder_type')));
        $huddle_type = isset($h_type['AccountFolderMetaData']['meta_data_value']) ? $h_type['AccountFolderMetaData']['meta_data_value'] : "1";
        $is_coach_enable = $this->is_enabled_coach_feedback($huddle_id);
        $if_evaluator = $this->check_if_evalutor($huddle_id, $user_id);
        $vidDocuments = $dbdoc->getVideoDocumentsByVideo_mobile($video_id, $huddle_id, $user_id, $huddle_type, $if_evaluator, $is_coach_enable);
        return count($vidDocuments);
    }

    function get_video_attachment_numbers($document_id, $is_published = 0) {
        App::import("Model", "Document");
        $db = new Document();
        $document_id = (int) $document_id;
        if ($is_published == 1) {
            return $this->get_assessor_count($document_id);
        }
        $joins = array(
            array(
                'table' => 'account_folder_documents as afd',
                'type' => 'inner',
                'conditions' => array('Document.id= afd.document_id')
            ),
            array(
                'table' => 'account_folderdocument_attachments as afda',
                'type' => 'inner',
                'conditions' => array(
                    'afda.account_folder_document_id= afd.id and afda.attach_id=' . $document_id
                )
            )
        );

        $fields = array('Document.*', 'afd.title', 'afd.desc', 'afd.account_folder_id');
        return count($db->find('all', array(
                    'joins' => $joins,
                    //'conditions' => array('afd.account_folder_id' => $account_folder_id, 'doc_type' => '2'),
                    'conditions' => array('doc_type' => '2'),
                    'fields' => $fields
        )));
    }

    function get_assessor_count($document_id) {
        App::import("Model", "Document");
        $db = new Document();
        $results = $db->query("(SELECT
                    `Document`.*,
                    `afd`.`title`,
                    `afd`.`desc`,
                    `afd`.`account_folder_id`
                FROM
                    `sibmeproduction`.`documents` AS `Document`
                    INNER JOIN `sibmeproduction`.`account_folder_documents` AS `afd`
                    ON (
                        `Document`.`id` = `afd`.`document_id`
                    )
                    INNER JOIN `sibmeproduction`.`account_folderdocument_attachments` AS `afda`
                    ON (
                        `afda`.`account_folder_document_id` = `afd`.`id`
                        AND `afda`.`attach_id` = $document_id
                    )
                    LEFT JOIN `sibmeproduction`.`comment_attachments` AS `ca`
                    ON Document.`id` = ca.`document_id`
                    LEFT JOIN comments AS cmt
                    ON cmt.`id` = ca.`comment_id`
                WHERE `doc_type` = 2
                    AND Document.site_id= " . $this->site_id . "
                    AND cmt.active = 1)
                UNION
                (SELECT
                    `Document`.*,
                    `afd`.`title`,
                    `afd`.`desc`,
                    `afd`.`account_folder_id`
                FROM
                    `sibmeproduction`.`documents` AS `Document`
                    INNER JOIN `sibmeproduction`.`account_folder_documents` AS `afd`
                    ON (
                        `Document`.`id` = `afd`.`document_id`
                    )
                    INNER JOIN `sibmeproduction`.`account_folderdocument_attachments` AS `afda`
                    ON (
                        `afda`.`account_folder_document_id` = `afd`.`id`
                        AND `afda`.`attach_id` = $document_id
                    )
                    LEFT JOIN `sibmeproduction`.`comment_attachments` AS `ca`
                    ON Document.`id` = ca.`document_id`
                    LEFT JOIN comments AS cmt
                    ON cmt.`id` = ca.`comment_id`
                WHERE `doc_type` = 2
                    AND Document.site_id= " . $this->site_id . "
                    AND (cmt.active IS NULL))");
        return count($results);
    }

    function match_timestamp_count_comment($comment_id, $video_id) {
        App::import("Model", "Comment");
        $db = new Comment();
        $result = $db->find('first', array(
            'conditions' => array(
                'id' => $comment_id,
            //'active' => 1
            )
        ));

        App::import("Model", "Document");
        $db = new Document();

        $commentDate = $result['Comment']['created_date'];
        $currentDate = date('Y-m-d H:i:s');

        $start_date = new DateTime($commentDate);
        $since_start = $start_date->diff(new DateTime($currentDate));

        ;

        if ($start_date < new DateTime('2018-08-10 00:00:00')) {

            $joins = array(
                array(
                    'table' => 'account_folder_documents as afd',
                    'type' => 'inner',
                    'conditions' => array('Document.id= afd.document_id')
                ),
                array(
                    'table' => 'account_folderdocument_attachments as afda',
                    'type' => 'inner',
                    'conditions' => array(
                        'afda.account_folder_document_id= afd.id and Document.scripted_current_duration = ' . $result['Comment']['time'] . '  and afda.attach_id=' . $video_id
                    )
                )
            );

            $fields = array('Document.*', 'afd.title', 'afd.desc', 'afd.account_folder_id');
            if (!empty($result['Comment']['time'])) {
                $results = $db->find('all', array(
                    'joins' => $joins,
                    //'conditions' => array('afd.account_folder_id' => $account_folder_id, 'doc_type' => '2'),
                    'conditions' => array('doc_type' => '2'),
                    'fields' => $fields
                ));
            }

            if (!empty($results)) {
                return count($results);
            } else {
                return 0;
            }
        } else {

            $joins = array(
                array(
                    'table' => 'account_folder_documents as afd',
                    'type' => 'inner',
                    'conditions' => array('Document.id= afd.document_id')
                ),
                array(
                    'table' => 'comment_attachments as ca',
                    'type' => 'inner',
                    'conditions' => array('Document.id= ca.document_id')
                )
            );

            $fields = array('Document.*', 'afd.title', 'afd.desc', 'afd.account_folder_id');

            $results = $db->find('all', array(
                'joins' => $joins,
                //'conditions' => array('afd.account_folder_id' => $account_folder_id, 'doc_type' => '2'),
                'conditions' => array('doc_type' => '2', 'ca.comment_id' => $comment_id),
                'fields' => $fields
            ));

            if (!empty($results)) {
                return count($results);
            } else {
                return 0;
            }
        }
    }

    function get_standard_tagged_count($video_id, $standard_id) {

        App::import("Model", "AccountCommentTag");
        $db = new AccountCommentTag();


        $result = $db->find('all', array(
            'conditions' => array(
                'account_tag_id' => $standard_id,
                'ref_id' => $video_id,
            // 'active' => 1
            )
        ));
        if (count($result) == 0) {
            return 'N/O';
        } else {
            return count($result);
        }
    }

    function get_selected_rating($standard_id, $account_id, $huddle_id, $video_id) {
        App::import("Model", "DocumentStandardRating");

        $db = new DocumentStandardRating();

        $result = $db->find('first', array('conditions' => array('account_folder_id' => $huddle_id, 'document_id' => $video_id, 'account_id' => $account_id, 'standard_id' => $standard_id)));
        return $result;
    }

    function get_average_video_rating($video_id, $account_id, $huddle_id) {
        App::import("Model", "DocumentStandardRating");

        $db = new DocumentStandardRating();

        $results = $db->find('all', array('conditions' => array('document_id' => $video_id, 'account_id' => $account_id, 'account_folder_id' => $huddle_id)));

        if (!empty($results)) {
            $avg = 0;
            foreach ($results as $result) {
                $avg = $avg + (int) $result['DocumentStandardRating']['rating_value'];
            }

            return round($avg / count($results));
        } else {
            return 'N/O';
        }
    }

    function get_eval_participant_video_details($account_folder_id, $user_id) {
        App::import("Model", "Document");
        $db = new Document();
        $result = $db->query("SELECT * FROM `documents` AS d JOIN `account_folder_documents` AS afd ON d.`id` = afd.`document_id` WHERE d.`created_by` = " . $user_id . " AND afd.`account_folder_id` = " . $account_folder_id . " AND d.`doc_type` IN (1,3) AND d.site_id =" . $this->site_id);

        return $result;
    }

    function get_rating_name($average, $account_id) {
        App::import("Model", "AccountMetaData");

        $db = new AccountMetaData();
        $average_rating_name_details = $db->find('first', array("conditions" => array(
                'account_id' => $account_id, 'meta_data_value' => $average, 'meta_data_name like "metric_value_%"'
        )));
        if (!empty($average_rating_name_details)) {
            $average_rating_name = substr($average_rating_name_details['AccountMetaData']['meta_data_name'], 13);
        } else {
            $average_rating_name = 'No Rating';
        }


        return $average_rating_name;
    }

    function _check_evaluator_permissions($huddle_id, $created_by, $user_id, $role_id) {
        App::import("Model", "AccountFolderMetaData");
        $is_avaluator = $this->check_if_evalutor($huddle_id, $user_id);
        $evaluators_ids = $this->get_evaluator_ids($huddle_id);
        $participants_ids = $this->get_participants_ids($huddle_id);
        $isEditable = true;

        $isEditable = (($user_id == $created_by) || ($is_avaluator));


        $db = new AccountFolderMetaData();

        $eval_huddles = $db->find("all", array("conditions" => array(
                "meta_data_value" => "3",
                "meta_data_name" => "folder_type",
                "account_folder_id" => $huddle_id
        )));

        if (empty($eval_huddles)) {
            $isEditable = true;
        }


        return $isEditable;
    }

    function _check_evaluator_permissions_invite($huddle_id, $user_id, $ref_user_id) {
        App::import("Model", "AccountFolderMetaData");
        $is_avaluator = $this->check_if_evalutor($huddle_id, $user_id);

        $isEditable = true;

        $isEditable = ($is_avaluator || ($user_id == $ref_user_id));


        $db = new AccountFolderMetaData();

        $eval_huddles = $db->find("all", array("conditions" => array(
                "meta_data_value" => "3",
                "meta_data_name" => "folder_type",
                "account_folder_id" => $huddle_id
        )));

        if (empty($eval_huddles)) {
            $isEditable = true;
        }


        return $isEditable;
    }

    function check_if_parent($account_id) {
        App::import("Model", "Account");
        $db = new Account();
        $child_accounts = $db->find('all', array(
            'conditions' => array(
                'parent_account_id' => $account_id
            )
                )
        );

        if (count($child_accounts) > 0) {
            return true;
        } else {
            return false;
        }
    }

    function get_folder_childs($folder_id) {
        App::import("Model", "AccountFolder");
        $db = new AccountFolder();
        $results = $db->query("SELECT
         af.`account_folder_id`
          , af1.`account_folder_id`
           , af2.`account_folder_id`
           , af3.`account_folder_id`
            , af4.`account_folder_id`
             , af5.`account_folder_id`
             , af6.`account_folder_id`
        FROM
          `account_folders` af
          LEFT JOIN `account_folders` af1
            ON (af.`account_folder_id` = af1.`parent_folder_id` AND af1.`folder_type` = 5 AND af1.`active` =1  AND af.`account_id` = af1.`account_id`)
            LEFT JOIN `account_folders` af2
            ON (af1.`account_folder_id` = af2.`parent_folder_id`  AND af2.`folder_type` = 5 AND af2.`active` =1  AND af1.`account_id` = af2.`account_id` )
                LEFT JOIN `account_folders` af3
            ON (af2.`account_folder_id` = af3.`parent_folder_id` AND af3.`folder_type` = 5 AND af3.`active` =1  AND af2.`account_id` = af3.`account_id` )
                LEFT JOIN `account_folders` af4
            ON (af3.`account_folder_id` = af4.`parent_folder_id` AND af4.`folder_type` = 5 AND af4.`active` =1   AND af3.`account_id` = af4.`account_id`)
                LEFT JOIN `account_folders` af5
            ON (af4.`account_folder_id` = af5.`parent_folder_id` AND af5.`folder_type` = 5 AND af5.`active` =1  AND af4.`account_id` = af5.`account_id`)
                LEFT JOIN `account_folders` af6
            ON (af5.`account_folder_id` = af6.`parent_folder_id` AND af5.`folder_type` = 5 AND af5.`active` =1  AND af5.`account_id` = af6.`account_id`)

            WHERE af.`account_folder_id` = " . $folder_id . " AND af.`active` = 1 AND af.`folder_type` = 5 AND af.site_id =" . $this->site_id);


        $account_folder_ids = array();

        foreach ($results as $result) {

            if (isset($result['af']['account_folder_id']) && $result['af']['account_folder_id'] != '') {
                $account_folder_ids[$result['af']['account_folder_id']] = $result['af']['account_folder_id'];
            }
            if (isset($result['af1']['account_folder_id']) && $result['af1']['account_folder_id'] != '') {
                $account_folder_ids[$result['af1']['account_folder_id']] = $result['af1']['account_folder_id'];
            }
            if (isset($result['af2']['account_folder_id']) && $result['af2']['account_folder_id'] != '') {
                $account_folder_ids[$result['af2']['account_folder_id']] = $result['af2']['account_folder_id'];
            }
            if (isset($result['af3']['account_folder_id']) && $result['af3']['account_folder_id'] != '') {
                $account_folder_ids[$result['af3']['account_folder_id']] = $result['af3']['account_folder_id'];
            }
            if (isset($result['af4']['account_folder_id']) && $result['af4']['account_folder_id'] != '') {
                $account_folder_ids[$result['af4']['account_folder_id']] = $result['af4']['account_folder_id'];
            }
            if (isset($result['af5']['account_folder_id']) && $result['af5']['account_folder_id'] != '') {
                $account_folder_ids[$result['af5']['account_folder_id']] = $result['af5']['account_folder_id'];
            }
            if (isset($result['af6']['account_folder_id']) && $result['af6']['account_folder_id'] != '') {
                $account_folder_ids[$result['af6']['account_folder_id']] = $result['af6']['account_folder_id'];
            }
        }


        return $account_folder_ids;
    }

    function is_teacher($account_id, $user_id) {
        App::import("Model", "UserAccount");
        $db = new UserAccount();

        $result = $db->find('first', array(
            'conditions' => array(
                'account_id' => $account_id,
                'user_id' => $user_id
            )
        ));

        if (!empty($result) && ($result['UserAccount']['role_id'] == 100 || $result['UserAccount']['role_id'] == 110 || $result['UserAccount']['role_id'] == 115)) {
            return true;
        } else {
            return false;
        }
    }

    function get_huddle_ids_by_coach($user_id, $account_id) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $account_coaches = $db->find('all', array(
            'joins' => array(
                array(
                    'table' => 'account_folders as af',
                    'type' => 'inner',
                    'conditions' => array('af.account_folder_id = AccountFolderUser.account_folder_id')
                )
            ),
            'conditions' => array(
                'AccountFolderUser.user_id' => $user_id,
                'af.account_id' => $account_id,
                'AccountFolderUser.role_id' => 200
            ),
            'fields' => array(
                'af.*',
                'AccountFolderUser.*'
            ),
                //'group' => array('AccountFolderUser.user_id'),
        ));

        $account_folder_ids = array();
        if ($account_coaches) {
            foreach ($account_coaches as $row) {
                $account_folder_ids[] = $row['AccountFolderUser']['account_folder_id'];
            }
        }
        return $account_folder_ids;
    }

    function get_huddle_participants_for_edTPA($coach_hud) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $account_coachees = $db->find('all', array(
            'joins' => array(
                array(
                    'table' => 'users as User',
                    'type' => 'left',
                    'conditions' => array('User.id=AccountFolderUser.user_id')
                )
            ),
            'conditions' => array(
                'AccountFolderUser.account_folder_id' => $coach_hud,
                'AccountFolderUser.role_id' => array(210, 220)
            ),
            'fields' => array(
                'User.*,AccountFolderUser.*'
            ),
                // 'group' => array('AccountFolderUser.user_id'),
        ));
        return $account_coachees;
    }

    function get_all_coaches($user_id, $account_id) {
        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $account_coaches = $db->find('all', array(
            'joins' => array(
                array(
                    'table' => 'users as User',
                    'type' => 'left',
                    'conditions' => array('User.id=AccountFolderUser.user_id')
                ),
                array(
                    'table' => 'users_accounts as ua',
                    'type' => 'inner',
                    'conditions' => array('User.id = ua.user_id')
                )
            ),
            'conditions' => array(
                'AccountFolderUser.user_id' => $user_id,
                'ua.account_id' => $account_id,
                'User.is_active' => 1,
                'AccountFolderUser.role_id' => 200
            ),
            'fields' => array(
                'User.*'
            ),
            'group' => array('AccountFolderUser.user_id'),
        ));
        return $account_coaches;
    }

    function check_to_send_live_push($video_id, $user_id) {
        App::import("Model", "AccountFolderDocument");
        $huddle_id = '';
        $db = new AccountFolderDocument();
        $af_data = $db->find("first", array("conditions" => array(
                "document_id" => $video_id
        )));

        if (!empty($af_data)) {
            $huddle_id = $af_data['AccountFolderDocument']['account_folder_id'];
        }

        App::import("Model", "AccountFolderUser");
        $db = new AccountFolderUser();
        $video_data = '';

        $account_folder_user_data = $db->find("first", array("conditions" => array(
                "account_folder_id" => $huddle_id,
                "user_id" => $user_id
        )));

        App::import("Model", "LiveStreamLog");
        $db = new LiveStreamLog();

        $live_stream_log = $db->find("first", array("conditions" => array(
                "document_id" => $video_id,
        )));

        App::import("Model", "LiveStreamLogDetail");
        $db = new LiveStreamLogDetail();

        $live_stream_log_detail = $db->find("first", array("conditions" => array(
                "user_id" => $user_id,
                "live_stream_log_id" => $live_stream_log['LiveStreamLog']['id']
        )));


        if (!empty($account_folder_user_data) && empty($live_stream_log_detail)) {
            $video_data = array(
                'video_id' => $video_id,
                'huddle_id' => $huddle_id
            );

//            $db->create();
//
//            $data = array(
//            'live_stream_log_id' => $live_stream_log['LiveStreamLog']['id'] ,
//            'user_id' => $user_id,
//            'participant_type' => '2',
//            'joined_at' => date("Y-m-d H:i:s"),
//            );
//            $db->save($data);


            return $video_data;
        } else {
            return $video_data;
        }
    }

    function get_live_stream_logs($document_id) {
        App::import("Model", "LiveStreamLog");
        $db = new LiveStreamLog();

        $result = $db->find('first', array(
            'conditions' => array(
                'document_id' => $document_id,
            )
        ));

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    function get_user_role_name($role_id) {
        $user_roles = $this->get_page_lang_based_content('user_roles');

        $user_role = '';
        if ($role_id == '100') {
            $user_role = $user_roles['account_owner_role_sep'];
        }
        if ($role_id == '110') {
            $user_role = $user_roles['super_admin_role_sep'];
        }
        if ($role_id == '115') {
            $user_role = $user_roles['admin_role_sep'];
        }
        if ($role_id == '120') {
            $user_role = $user_roles['user_role_sep'];
        }

        if ($role_id == '125') {
            $user_role = $user_roles['viewer_role_sep'];
        }

        return $user_role;
    }

    function get_framework_settings($account_tag_id) {
        App::import("Model", "AccountFrameworkSetting");
        $db = new AccountFrameworkSetting();

        $result = $db->find('first', array(
            'conditions' => array(
                'account_tag_id' => $account_tag_id,
            )
        ));

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    function get_comment_attachment_file_names($comment_id, $video_id) {
        App::import("Model", "Comment");
        $db = new Comment();
        $result = $db->find('first', array(
            'conditions' => array(
                'id' => $comment_id,
            //'active' => 1
            )
        ));

        App::import("Model", "Document");
        $db = new Document();

        $commentDate = $result['Comment']['created_date'];
        $currentDate = date('Y-m-d H:i:s');

        $start_date = new DateTime($commentDate);
        $since_start = $start_date->diff(new DateTime($currentDate));

        ;

        if ($start_date < new DateTime('2018-08-10 00:00:00')) {

            $joins = array(
                array(
                    'table' => 'account_folder_documents as afd',
                    'type' => 'inner',
                    'conditions' => array('Document.id= afd.document_id')
                ),
                array(
                    'table' => 'account_folderdocument_attachments as afda',
                    'type' => 'inner',
                    'conditions' => array(
                        'afda.account_folder_document_id= afd.id and Document.scripted_current_duration = ' . $result['Comment']['time'] . '  and afda.attach_id=' . $video_id
                    )
                )
            );

            $fields = array('Document.*', 'afd.title', 'afd.desc', 'afd.account_folder_id');
            if (!empty($result['Comment']['time'])) {
                $results = $db->find('all', array(
                    'joins' => $joins,
                    //'conditions' => array('afd.account_folder_id' => $account_folder_id, 'doc_type' => '2'),
                    'conditions' => array('doc_type' => '2'),
                    'fields' => $fields
                ));
            }
        } else {

            $joins = array(
                array(
                    'table' => 'account_folder_documents as afd',
                    'type' => 'inner',
                    'conditions' => array('Document.id= afd.document_id')
                ),
                array(
                    'table' => 'comment_attachments as ca',
                    'type' => 'inner',
                    'conditions' => array('Document.id= ca.document_id')
                )
            );

            $fields = array('Document.*', 'afd.title', 'afd.desc', 'afd.account_folder_id');

            $results = $db->find('all', array(
                'joins' => $joins,
                //'conditions' => array('afd.account_folder_id' => $account_folder_id, 'doc_type' => '2'),
                'conditions' => array('doc_type' => '2', 'ca.comment_id' => $comment_id),
                'fields' => $fields
            ));
        }


        $file_names = array();
        $final_string = '';
        foreach ($results as $result) {
            $file_names[] = $result['afd']['title'];
        }

        if (!empty($file_names)) {
            $final_string = implode(',', $file_names);
        } else {
            $final_string = '';
        }

        return $final_string;
    }

    function redirectHTTPS($uri) {
        // if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
        $redirect = 'https://' . $_SERVER['HTTP_HOST'] . '/' . $uri;
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: ' . $redirect);
        exit();
        // }
    }

    function getCurrentURL(){
        return 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
    }

    function addParamToURL( $key, $value, $url) {
        $url .= (parse_url($url, PHP_URL_QUERY) ? '&' : '?') . $key . '=' . $value;
        return $url;
    }

    function getParamFromUrl($url,$paramName){
        parse_str(parse_url($url,PHP_URL_QUERY),$op);// fetch querystring parameters from string and convert to associative array
        return array_key_exists($paramName,$op) ? $op[$paramName] : null; // check key is exist in this array
    }

    function check_if_account_in_trial_intercom($account_id) {
        App::import("Model", "Account");
        $db = new Account();
        $result = $db->find('first', array(
            'conditions' => array(
                'id' => $account_id
            )
        ));

        $value = "";

        if ($result['Account']['in_trial'] == '1') {
            $value = "True";
        } else {
            $value = "False";
        }

        return $value;
    }

    function get_site() {
        $server_name = $_SERVER['SERVER_NAME'];
        return $server_name;
        /*
          $result = $this->getinfo();
          if(!empty($result)){
          return $result['Sites']['site_url'];
          }

          return $_SERVER['SERVER_NAME'];
         */
    }

    function get_site_settings($key = 'sibme', $lang = 'en') {
        $result = $this->getinfo();
        $lang = $_SESSION['LANG'];
        $site_metadata = $result['Sites']['site_metadata'];
        $site_metadata = preg_replace('!s:(\d+):"(.*?)";!e', "'s:'.strlen('$2').':\"$2\";'", $site_metadata);
        $site_data = unserialize($site_metadata);
        $site_data = $site_data[$lang];
        if (is_array($site_data) && !empty(array_key_exists($key, $site_data))) {
            return $site_data[$key];
        } else {
            return 'Sibme';
        }
    }

    function get_impersonation_ids() {
        $result = $this->getinfo();
        $site_impersonation_allowed_ids = $result['Sites']['site_impersonation_allowed_ids'];
        return $site_impersonation_allowed_ids;
    }

    function huddle_check_on_permission_page($huddle_id, $user_id, $role_id) {

        App::import("Model", "AccountFolderMetaData");
        $db = new AccountFolderMetaData();
        $result = $db->find('first', array(
            'conditions' => array(
                'meta_data_name' => 'folder_type',
                'account_folder_id' => $huddle_id
            )
        ));

        App::import("Model", "AccountFolder");
        $db = new AccountFolder();
        $huddle_details = $db->find('first', array(
            'conditions' => array(
                'account_folder_id' => $huddle_id,
            )
        ));


        if ($result['AccountFolderMetaData']['meta_data_value'] == '2') {

            App::import("Model", "AccountFolderUser");
            $db = new AccountFolderUser();
            $current_coachee = $db->find('first', array(
                'conditions' => array(
                    'user_id' => $user_id,
                    'account_folder_id' => $huddle_id,
                    'role_id' => '210'
                )
            ));
            $any_other_coachee = $db->find('all', array(
                'conditions' => array(
                    'account_folder_id' => $huddle_id,
                    'role_id' => '210'
                )
            ));

            if (empty($current_coachee) && count($any_other_coachee) > 0 && $role_id == 210) {
                return array(
                    'success' => true,
                    'message' => 'Huddle "' . $huddle_details['AccountFolder']['name'] . '" cannot have more than 1 Coachee'
                );
            }

            if (!empty($current_coachee) && count($any_other_coachee) == 1 && $role_id == 200) {
                return array(
                    'success' => true,
                    'message' => 'Huddle "' . $huddle_details['AccountFolder']['name'] . '" should have atleast one Coachee.'
                );
            }

            $coach_number = $db->find('all', array(
                'conditions' => array(
                    'account_folder_id' => $huddle_id,
                    'role_id' => '200'
                )
            ));

            $current_coach = $db->find('first', array(
                'conditions' => array(
                    'account_folder_id' => $huddle_id,
                    'role_id' => '200',
                    'user_id' => $user_id
                )
            ));


            if (count($coach_number) == 1 && !empty($current_coach) && $role_id == 210) {
                return array(
                    'success' => true,
                    'message' => 'Huddle "' . $huddle_details['AccountFolder']['name'] . '" should have at least one Coach'
                );
            }
        } else {
            return false;
        }
    }

    function getinfo() {
        App::import("Model", "Sites");
        $db = new Sites();
        $result = $db->find('first', array(
            'conditions' => array(
                'site_id' => $this->site_id
            )
        ));
        return $result;
    }

    function getLoginCount($account_id, $user_id) {
        App::import("Model", "UserActivityLog");
        $db = new UserActivityLog();
        $result = $db->find('count', array(
            'conditions' => array(
                'account_id' => $account_id,
                'user_id' => $user_id,
                'environment_type' => '2',
                'type' => '9'
            )
        ));
        return $result;
    }

    function translate($key, $lang = 'en', $replacement_array = []) {
        App::import("Model", "Translation");
        $db = new Translation();
        // Can't use find() here because it adds site_id in where clause which we don't need here. The labels are same for both HMH and WL.

        $data = $db->query("SELECT en, es FROM `translations` WHERE `key` = '" . $key . "'");
        $lang = in_array($lang, ["en", "es"]) ? $lang : "en";
        if (!empty($data)) {

            return $this->parse_translation_params($data[0]['translations'][$lang], $replacement_array);
            /*
              $string_processed = preg_replace_callback(
              '~\{\$(.*?)\}~si', function($match) use ($replacement_array) {
              return str_replace($match[0], isset($replacement_array[$match[1]]) ? $replacement_array[$match[1]] : $match[0], $match[0]);
              }, $data[0]['translations'][$lang]);

              return $string_processed;
             */
        } else {
            return "Empty - " . $lang . " - " . $key;
        }
    }

    function get_page_lang_based_content($url, $user_id = '') {
        App::import("Model", "User");
        $db = new User();
        if (empty($user_id)) {
            $lang = $this->getPreferredLanguage();
        } else {
            $result = $db->find('first', array(
                'conditions' => array(
                    'id' => $user_id
                )
            ));
            $lang = $result['User']['lang'];
        }

        $lang = $_SESSION['LANG'];

        if ($lang != 'es' && $lang != 'en') {
            $lang = 'en';
            $_SESSION['LANG'] = 'en';
        }

        $language_contents = $this->get_page_lang_based_data($url);
        $language_array = array();
        foreach ($language_contents as $row) {
            $language_array[$row['translations']['key']] = $row['translations'][$lang];
        }
        $language_array['current_lang'] = $lang;

        return $language_array;
    }

    function getPreferredLanguage() {

        $langs = array();
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            // break up string into pieces (languages and q factors)
            preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);
            if (count($lang_parse[1])) {
                // create a list like "en" => 0.8
                $langs = array_combine($lang_parse[1], $lang_parse[4]);
                // set default to 1 for any without q factor
                foreach ($langs as $lang => $val) {
                    if ($val === '')
                        $langs[$lang] = 1;
                }
                // sort list based on value
                arsort($langs, SORT_NUMERIC);
            }
        }
        //extract most important (first)
        foreach ($langs as $lang => $val) {
            break;
        }
        //if complex language simplify it
        if (stristr($lang, "-")) {
            $tmp = explode("-", $lang);
            $lang = $tmp[0];
        }
        return $lang;
    }

    function parse_translation_params($string, $replacement_array = []) {
        $processed_string = preg_replace_callback(
                '~\{\$(.*?)\}~si', function($match) use ($replacement_array) {
            return str_replace($match[0], isset($replacement_array[$match[1]]) ? $replacement_array[$match[1]] : $match[0], $match[0]);
        }, $string);

        return $processed_string;
    }

    function get_selected_lang($user_id) {
        App::import("Model", "User");
        $db = new User();
        $result = $db->find("first", array("conditions" => array(
                "id" => $user_id
        )));
        return $result['User']['lang'];
    }

    function get_page_lang_based_content_for_api($url, $lang = 'en') {
        $language_contents = $this->get_page_lang_based_data($url);
        $language_array = array();
        foreach ($language_contents as $row) {
            $language_array[$row['translations']['key']] = $row['translations'][$lang];
        }

        return $language_array;
    }

    function get_page_lang_based_data($url) {
        App::import("Model", "Translation");
        $db = new Translation();
        // Can't use find() here because it adds site_id in where clause which we don't need here. The labels are same for both HMH and WL.

        // $data = $db->query("SELECT * FROM `translations` WHERE `url` = '" . $url . "' OR `url` = 'global'");
        $data = $db->query("SELECT * FROM `translations` WHERE `url` = 'global'");

        return $data;
    }

     public function BroadcastEvent($response_object, $send_push = true,$single_token = [])
     {
         if(strpos($this->base, "//app.sibme.com") || strpos($this->base, "//coachingstudio.hmhco.com")){
             $event_url = "https://echo.sibme.com/";
         } else {
             $event_url = "https://q2echo.sibme.com/";
         }

         $HttpSocket = new HttpSocket();
         $results = $HttpSocket->post($event_url . 'broadcast_event', $response_object);
         // $this->log($results,'sockets'); // check in tmp/logs/sockets.log
         if($send_push) //Saad Zia disabled this because push notifications were slowing down comments
         {
             try
             {
                 $this->sendPushNotification($response_object,$single_token);
             }
             catch (\Exception $e)
             {
                 //var_dump($e->getMessage());die;
             }
         }
         else
         {
             $this->addWebsocketLog($response_object);
         }
     }

    /* check if account level access to allow transcribe function */

    public function CheckAllowTrancribe($document_id,$account_id) {
        try {
            App::import("Model", "Account");
            App::import("Model", "Document");
            $db = new Account();
            $result = $db->query("SELECT d.id, IF(af.`folder_type` = 3 , a.transcribe_workspace_videos, IF (af.`folder_type` = 1, a.transcribe_huddle_videos , a.transcribe_library_videos )) AS allow_transcribe FROM documents d JOIN accounts a ON a.id = d.`account_id` JOIN account_folder_documents afd ON afd.`document_id` = d.`id` JOIN account_folders af ON af.`account_folder_id` = afd.`account_folder_id` WHERE d.id = $document_id AND d.account_id = $account_id");
            $allowed = isset($result[0][0]['allow_transcribe']) ? $result[0][0]['allow_transcribe'] : 0;
            if($allowed){
            $db = new Document();
              $db->updateAll(array('allow_transcribe' => 1),array('id' => $document_id));
            }
            } catch (\Exception $e) {
            echo $e->getMessage();
            //continue;
        }
    }

    public function sendHttpRequest($url, $data = []) {
        $HttpSocket = new HttpSocket();
        $domain = $this->getDomain(true);
        $results = $HttpSocket->post($domain . '/' . $url, $data);
        if (isset($results["body"])) {
            return $results["body"];
        }
        return [];
    }

    public function getDomain($lumen_link = false) {
        $url = Configure::read('sibme_base_url');
        if (!$lumen_link) {
            return $url;
        }
        if (strpos($url, 'q2hmh') !== false) {
            $base_url = 'https://q2hmhapi.sibme.com/';
        } elseif (strpos($url, 'hmh') !== false) {
            $base_url = 'https://hmhapi.sibme.com/';
        } elseif (strpos($url, 'staging') !== false) {
            $base_url = 'https://stagingapi.sibme.com/';
        } elseif (strpos($url, 'qa') !== false) {
            $base_url = 'https://qaapi.sibme.com/';
        } elseif (strpos($url, 'q21') !== false) {
            $base_url = 'https://q21api.sibme.com/';
        } elseif (strpos($url, 'q22') !== false) {
            $base_url = 'https://q22api.sibme.com/';
        } elseif (strpos($url, 'q2') !== false) {
            $base_url = 'https://q2api.sibme.com/';
        } elseif (strpos($url, 'q31') !== false) {
            $base_url = 'https://q31api.sibme.com/';
        } elseif (strpos($url, 'q32') !== false) {
            $base_url = 'https://q32api.sibme.com/';
        } elseif (strpos($url, 'q3') !== false) {
            $base_url = 'https://q3api.sibme.com/';
        } elseif (strpos($url, 'wl') !== false) {
            $base_url = 'https://wlapi.sibme.com/';
        } elseif (strpos($url, 'cls') !== false) {
            $base_url = 'https://clsapi.sibme.com/';
        } elseif (strpos($url, 'coachingstudio') !== false) {
            $base_url = 'https://csapi.hmhco.com/';
        } elseif(strpos($url, 'devteam-app') !== false) {
            $base_url = 'https://devteam-api.sibme.com/';
        } elseif(strpos($url, 'doapp') !== false) {
            $base_url = 'https://doapi.sibme.com/';
        } elseif(strpos($url, 'devops') !== false) {
            $base_url = 'https://devopsapi.sibme.com/';
        } else {
            $base_url = 'https://api.sibme.com/';
        }
        return $base_url;
    }

    public function sendPushNotification($data,$single_token = []) {
        if (isset($data["huddle_id"])) {
            $account_folder_id = $data["huddle_id"];
        }

        if (empty($account_folder_id)) {
            $account_folder_id = isset($this->request->data['huddle_id']) ? $this->request->data['huddle_id'] : (isset($this->request->data['account_folder_id']) ? $this->request->data['account_folder_id'] : null);
        }

        $video_id = isset($data["document_id"]) ? $data["document_id"] : (isset($data["item_id"]) ? $data["item_id"] : 0);
        if ($data && isset($data["data"]->doc_id)) {
            $video_id = $data["data"]->doc_id;
        } else if (isset($data["data"]->id)) {
            $video_id = $data["data"]->id;
        }
        if (empty($account_folder_id)) {
            if (isset($data["data"]->account_folder_id)) {
                $account_folder_id = $data["data"]->account_folder_id;
            } else {
                $account_folder_id = 0;
            }
        }
        $video_file_name = "";
        if ($data && isset($data["data"]->name)) {
            $video_file_name = $data["data"]->name;
        }
        $user_id = isset($this->request->data['user_id']) ? $this->request->data['user_id'] : null;
        if (empty($user_id)) {
            $user_id = isset($data["user_id"]) ? $data["user_id"] : null;
        }
        $token_ids = self::get_device_token_ids($account_folder_id, $user_id);
        /* $user_data = User::find($user_id); */

        $title = '';
        $desc = '';
        $data_1 = array(
            /* 'event_data' => $data, */
            'huddle_id' => $account_folder_id,
            'notification_type' => 10,
            'video_id' => $video_id,
            'video_file_name' => $video_file_name,
                /* 'user' => $user_data, */
        );
        $data_1 = array_merge($data_1, $data);
        if (!isset($data_1["reference_id"])) {
            $data_1["reference_id"] = $video_id;
        }
        $full_data = $data_1;
        if (isset($data_1["data"])) {
            unset($data_1["data"]); //removing data key because it some times get too large data and push give error.
        }

        $data_array = $data_1;

        $data2 = [
            'mtitle' => $title,
            'mdesc' => $desc,
            'badge' => 1,
            'required_data' => $data_array
        ];

        $push_sent = [];
        $current_device_token = $this->request->data["deviceToken"];
        foreach ($token_ids as $token_id) {
            if (!in_array($token_id['one_signal_device_id'], $push_sent) && $current_device_token != $token_id['apns_token']) {
                $push_sent[] = $token_id['one_signal_device_id'];
            }
        }
        if(!empty($single_token))
        {
            $push_sent = $single_token;
        }
        $this->ios_and_android($data2, $push_sent);
        $this->addWebsocketLog($full_data, $token_ids);
    }
    public function addWebsocketLog($full_data, $token_ids = [])
    {
        $HttpSocket = new HttpSocket();
        $results = $HttpSocket->post($this->getDomain(true)."add_websocket_logs", ['data'=>$full_data, 'token_ids'=>$token_ids]);
    }

    public function ios_and_android($data,$one_signal_ids) {
        //$helper = new CustomHelper;
        $this->send_onesignal_notifications($data['mtitle'] , $data['mdesc'] , $data['required_data'] ,$one_signal_ids);
    }
    
    public function send_onesignal_notifications($title, $desc , $data ,$one_signal_ids)
    {
        //$helper = new CustomHelper;
            if($this->site_id == 2)
            {
               $title_on_base_site_id = 'HMH';
               $desc_on_base_site_id = 'HMH Notifications';
               $app_id = "6522eb42-9763-4e96-8da6-ad02d7126378";  
            }
            
            else
            {
               $title_on_base_site_id = 'Sibme';
               $desc_on_base_site_id = 'Sibme Notifications'; 
               $app_id = "695ea104-69f8-4dde-a309-6e3e3906a21b"; 
            }
        
            if(isset($data['notification_type']) && ($data['notification_type'] == '1' || $data['notification_type'] == '2' || $data['notification_type'] == '3' || $data['notification_type'] == '4' || $data['notification_type'] == '5' ) )
            {
                    if(empty($title))
                    {
                        $title = $title_on_base_site_id;
                    }

            if (empty($desc)) {
                $desc = $desc_on_base_site_id;
            }
        }

        $non_empty_ids = [];
        foreach ($one_signal_ids as $id)
        {
            if(!empty($id))
            {
                $non_empty_ids[] = $id;
            }
        }

        $fields = array(
            'app_id' => $app_id,
            'include_player_ids' => $non_empty_ids,
            'headings' => ["en" => $title],
            'contents' => ["en" => $desc],
            'data' => $data ,
            'content_available' => true   
            );

            $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function get_device_token_ids($huddle_id, $user_id) {
        App::import("Model", "UserDeviceLog");
        $db = new UserDeviceLog();
        $token_ids = array();
        $participant_ids = self::get_all_participant_ids($huddle_id, $user_id);
        $count = 0;
        foreach ($participant_ids as $participant_id) {
            if ($user_id != $participant_id) {
                $user_device_logs = $db->find('all', array(
                    'conditions' => array(
                        'user_id' => $participant_id,
                        'push_notifications_on' => '1'
                    ),
                ));
                foreach ($user_device_logs as $user_device_log) {
                    if (!empty($user_device_log)) {
                        $token_ids[$count]['apns_token'] = $user_device_log['UserDeviceLog']['apns_token'];
                        $token_ids[$count]['device_type'] = $user_device_log['UserDeviceLog']['device_type'];
                        $token_ids[$count]['one_signal_device_id'] = $user_device_log['UserDeviceLog']['one_signal_device_id'];
                        $token_ids[$count]['user_id'] = $user_device_log['UserDeviceLog']['user_id'];
                        $count++;
                    }
                }
            }
        }

        $user_device_logs = $db->find('all', array(
            'conditions' => array(
                'user_id' => $user_id,
                'push_notifications_on' => '1'
            ),
        ));
        foreach ($user_device_logs as $user_device_log) {
            if (!empty($user_device_log)) {
                $token_ids[$count]['apns_token'] = $user_device_log['UserDeviceLog']['apns_token'];
                $token_ids[$count]['device_type'] = $user_device_log['UserDeviceLog']['device_type'];
                $token_ids[$count]['one_signal_device_id'] = $user_device_log['UserDeviceLog']['one_signal_device_id'];
                $token_ids[$count]['user_id'] = $user_device_log['UserDeviceLog']['user_id'];
                $count++;
            }
        }
        return $token_ids;
    }

    function SpanishDate($FechaStamp, $location = '') {
        $ano = date('Y', $FechaStamp);
        $mes = date('n', $FechaStamp);
        $dia = date('d', $FechaStamp);
        $time = date('h:i A', $FechaStamp);

        $diasemana = date('w', $FechaStamp);
        $diassemanaN = array("Domingo", "Lunes", "Martes", "Mi�rcoles",
            "Jueves", "Viernes", "S�bado");
        $mesesN = array(1 => "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio",
            "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
        if ($location == 'dashboard') {
            $mesesN = array(1 => "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul",
                "Ago", "Sep", "Oct", "Nov", "Dic");
            return $mesesN[$mes] . ' ' . $dia;
        } elseif ($location == 'trackers') {
            $mesesN = array(1 => "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul",
                "Ago", "Sep", "Oct", "Nov", "Dic");
            return $dia . '-' . $mesesN[$mes];
        } elseif ($location == 'trackers_filter') {
            $mesesN = array(1 => "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul",
                "Ago", "Sep", "Oct", "Nov", "Dic");
            return $mesesN[$mes];
        } elseif ($location == 'archive_module') {
            $mesesN = array(1 => "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul",
                "Ago", "Sep", "Oct", "Nov", "Dic");
            return $mesesN[$mes] . ' ' . $dia . ', ' . $ano;
        } elseif ($location == 'discussion_time') {
            $mesesN = array(1 => "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul",
                "Ago", "Sep", "Oct", "Nov", "Dic");
            return $time;
        } elseif ($location == 'discussion_date') {
            $mesesN = array(1 => "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul",
                "Ago", "Sep", "Oct", "Nov", "Dic");
            return $mesesN[$mes] . ' ' . $dia . ', ' . $ano . ' ' . $time;
        } elseif ($location == 'most_common') {
            $mesesN = array(1 => "Ene", "Feb", "March", "Abr", "May", "Jun", "Jul",
                "Ago", "Sep", "Oct", "Nov", "Dic");
            return $mesesN[$mes] . ' ' . $dia . ', ' . $ano;
        }

        //return $diassemanaN[$diasemana].", $dia de ". $mesesN[$mes] ." de $ano";
    }

    function get_single_user_stat($account_id, $user_id) {
        App::import("Model", "User");
        $db = new User();

        $data = $db->query("SELECT DISTINCT usr.user_id,usr.account_id,usr.username AS username, usr.email,usr.company_name,CONCAT(usr.first_name,' ',usr.last_name) AS account_name,
web_login_counts.login_counts AS web_login_counts,
video_upload_counts.video_upload_counts AS video_upload_counts,
workspace_upload_counts.workspace_upload_counts AS workspace_upload_counts,
library_upload_counts.library_upload_counts AS library_upload_counts,
shared_upload_counts.shared_upload_counts AS shared_upload_counts,
library_shared_upload_counts.library_shared_upload_counts AS library_shared_upload_counts,
huddle_created_count.huddle_created_count AS huddle_created_count,
comments_initiated_count.comments_initiated_count AS comments_initiated_count,
workspace_comments_initiated_count.workspace_comments_initiated_count AS workspace_comments_initiated_count,
replies_initiated_count.replies_initiated_count AS replies_initiated_count,
videos_viewed_count.videos_viewed_count AS videos_viewed_count,
workspace_videos_viewed_count.workspace_videos_viewed_count AS workspace_videos_viewed_count,
library_videos_viewed_count.library_videos_viewed_count AS library_videos_viewed_count,
documents_uploaded_count.documents_uploaded_count AS documents_uploaded_count,
documents_viewed_count.documents_viewed_count AS documents_viewed_count,
scripted_observations.scripted_observations AS scripted_observations,
scripted_video_observations.scripted_video_observations AS scripted_video_observations,
ROUND(document_durations.total_duration/60/60,2) AS total_document_durations,
ROUND(dv_histories.minutes_watched/60/60,2) AS minutes_watched_total,
assessment_huddle.total_assess_huddle,
coaching_huddle.total_coaching_huddle,
collab_huddles.total_collab_huddle,
is_user_coache_details.is_coachee,
user_accounts.role_id,
user_accounts.manage_evaluation_huddles,
user_accounts.manage_coach_huddles,
user_accounts.manage_collab_huddles

FROM (SELECT u.id AS user_id,u.username, u.email,u.first_name,u.last_name,a.company_name AS company_name,a.id AS account_id FROM users u
LEFT JOIN users_accounts ua ON u.id = ua.user_id
LEFT JOIN accounts a ON ua.account_id = a.id
WHERE u.site_id = 1 AND u.`id` = $user_id AND u.is_active=1 AND u.id NOT IN (2423,2422,2427) AND ua.role_id NOT IN(125) AND u.type='Active' AND ua.account_id IN ($account_id) GROUP BY u.id, ua.account_id ORDER BY CONCAT(u.`first_name`,' ',u.`last_name`) ASC) AS usr

LEFT JOIN

(SELECT user_id,user_activity_logs.account_id, COUNT(*) AS login_counts FROM `user_activity_logs` WHERE site_id = 1 AND TYPE=9 AND account_id IN ($account_id) AND user_id = $user_id GROUP BY user_id,user_activity_logs.account_id) AS web_login_counts

ON usr.user_id = web_login_counts.user_id && usr.account_id = web_login_counts.account_id

LEFT JOIN

(SELECT user_id,ua.account_id, COUNT(*) AS video_upload_counts FROM `user_activity_logs` ua LEFT JOIN
account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE af.site_id = 1 AND (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(1) AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN ($account_id) AND ua.user_id = $user_id  GROUP BY ua.user_id ,ua.account_id) AS video_upload_counts
ON usr.user_id = video_upload_counts.user_id && usr.account_id = video_upload_counts.account_id

LEFT JOIN

(SELECT user_id,ua.account_id, COUNT(*) AS workspace_upload_counts FROM `user_activity_logs` ua LEFT JOIN
account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE af.site_id = 1 AND (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(3) AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN ($account_id) AND ua.user_id = $user_id  GROUP BY ua.user_id,ua.account_id) AS workspace_upload_counts
ON usr.user_id = workspace_upload_counts.user_id && usr.account_id = workspace_upload_counts.account_id

LEFT JOIN

(SELECT user_id,ua.account_id, COUNT(*) AS library_upload_counts FROM `user_activity_logs` ua LEFT JOIN
account_folders af ON ua.`account_folder_id` = af.`account_folder_id` WHERE af.site_id = 1 AND (ua.TYPE=2 OR ua.TYPE=4) AND af.`folder_type` IN(2) AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1)) AND ua.account_id IN ($account_id) AND ua.user_id = $user_id  GROUP BY ua.user_id,ua.account_id) AS library_upload_counts
ON usr.user_id = library_upload_counts.user_id && usr.account_id = library_upload_counts.account_id

LEFT JOIN

(SELECT d.created_by AS user_id,d.account_id, COUNT(*) AS shared_upload_counts
FROM documents d
LEFT JOIN account_folder_documents afd
ON d.`id`= afd.`document_id`
LEFT JOIN account_folders af
ON afd.`account_folder_id`= af.`account_folder_id`
LEFT JOIN user_activity_logs ua
ON (d.id = ua.ref_id)
WHERE d.site_id = 1 AND d.account_id IN ($account_id) AND ua.user_id = $user_id  AND af.`folder_type` = 1 AND ua.`type` = 22 AND d.doc_type = 1 GROUP BY d.created_by) AS shared_upload_counts
ON usr.user_id = shared_upload_counts.user_id && usr.account_id = shared_upload_counts.account_id

LEFT JOIN

(SELECT d.created_by AS user_id,d.account_id, COUNT(*) AS library_shared_upload_counts
FROM documents d
LEFT JOIN account_folder_documents afd
ON d.`id`= afd.`document_id`
LEFT JOIN account_folders af
ON afd.`account_folder_id`= af.`account_folder_id`
LEFT JOIN user_activity_logs ua
ON (d.id = ua.ref_id)
WHERE d.site_id = 1 AND d.account_id IN ($account_id) AND af.`folder_type` = 2 AND ua.`type` = 22 AND ua.user_id = $user_id AND d.doc_type = 1 GROUP BY d.created_by) AS library_shared_upload_counts
ON usr.user_id = library_shared_upload_counts.user_id && usr.account_id = library_shared_upload_counts.account_id

LEFT JOIN

(SELECT user_activity_logs.user_id,user_activity_logs.account_id,COUNT(*) AS `huddle_created_count`
FROM `user_activity_logs`
JOIN account_folders af ON af.account_folder_id = user_activity_logs.account_folder_id
JOIN `users_accounts` uact ON uact.user_id=user_activity_logs.user_id AND uact.account_id=user_activity_logs.account_id
WHERE af.site_id = 1 AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
AND `type` = 1
AND user_activity_logs.user_id IN ($user_id)
AND user_activity_logs.account_id IN ($account_id)
GROUP BY user_activity_logs.user_id) AS huddle_created_count
ON usr.user_id = huddle_created_count.`user_id` && usr.account_id = huddle_created_count.account_id

LEFT JOIN

(SELECT user_activity_logs.user_id,user_activity_logs.account_id, COUNT(*) AS comments_initiated_count FROM `user_activity_logs`
JOIN comments ON user_activity_logs.ref_id = comments.id
JOIN account_folders ON user_activity_logs.account_folder_id = account_folders.account_folder_id
WHERE user_activity_logs.site_id = 1 AND TYPE=5 AND user_activity_logs.user_id = $user_id AND account_folders.folder_type = 1 AND user_activity_logs.account_id IN ($account_id)  GROUP BY user_activity_logs.user_id ) AS comments_initiated_count

ON usr.user_id = comments_initiated_count.`user_id` && usr.account_id = comments_initiated_count.account_id

LEFT JOIN

(SELECT user_activity_logs.user_id,user_activity_logs.account_id, COUNT(*) AS workspace_comments_initiated_count FROM `user_activity_logs`
JOIN comments ON user_activity_logs.ref_id = comments.id
JOIN account_folders ON user_activity_logs.account_folder_id = account_folders.account_folder_id
WHERE user_activity_logs.site_id = 1 AND
TYPE=5 AND account_folders.folder_type = 3
AND user_activity_logs.account_id IN ($account_id) 
AND user_activity_logs.user_id = $user_id
GROUP BY user_activity_logs.user_id
) AS workspace_comments_initiated_count

ON usr.user_id = workspace_comments_initiated_count.`user_id` && usr.account_id = workspace_comments_initiated_count.account_id

LEFT JOIN

(SELECT user_activity_logs.user_id,user_activity_logs.account_id, COUNT(*) AS replies_initiated_count FROM `user_activity_logs`
JOIN comments ON user_activity_logs.ref_id = comments.id
WHERE user_activity_logs.site_id = 1 AND TYPE=8 AND user_activity_logs.account_id IN ($account_id) AND user_activity_logs.user_id = $user_id GROUP BY user_activity_logs.user_id ) AS replies_initiated_count

ON usr.user_id = replies_initiated_count.`user_id` && usr.account_id = replies_initiated_count.account_id

LEFT JOIN

(SELECT user_id,user_activity_logs.account_id, COUNT(*) AS documents_uploaded_count FROM `user_activity_logs` WHERE site_id = 1 AND TYPE=3 AND account_id IN ($account_id) AND user_id = $user_id GROUP BY user_id,user_activity_logs.account_id) AS documents_uploaded_count

ON usr.user_id = documents_uploaded_count.`user_id` && usr.account_id = documents_uploaded_count.account_id

LEFT JOIN

(SELECT user_id,user_activity_logs.account_id, COUNT(*) AS documents_viewed_count FROM `user_activity_logs` WHERE site_id = 1 AND TYPE=13 AND account_id IN ($account_id) AND user_id = $user_id  GROUP BY user_id,user_activity_logs.account_id) AS documents_viewed_count

ON usr.user_id = documents_viewed_count.`user_id` && usr.account_id = documents_viewed_count.account_id

LEFT JOIN

(SELECT user_activity_logs.user_id,user_activity_logs.account_id, COUNT(*) AS videos_viewed_count
FROM `user_activity_logs`
JOIN account_folder_documents ON user_activity_logs.ref_id = account_folder_documents.document_id
JOIN account_folders ON account_folders.account_folder_id = account_folder_documents.account_folder_id
JOIN `users_accounts` uact ON uact.user_id=user_activity_logs.user_id AND uact.account_id=user_activity_logs.account_id
WHERE user_activity_logs.site_id = 1 AND TYPE=11 AND account_folders.folder_type = 1 AND user_activity_logs.account_id IN ($account_id) AND user_activity_logs.user_id = $user_id  GROUP BY user_activity_logs.user_id,user_activity_logs.account_id) AS videos_viewed_count
ON usr.user_id = videos_viewed_count.`user_id` && usr.account_id = videos_viewed_count.account_id


LEFT JOIN

(SELECT user_activity_logs.user_id,user_activity_logs.account_id, COUNT(*) AS workspace_videos_viewed_count
FROM `user_activity_logs`
JOIN account_folder_documents ON user_activity_logs.ref_id = account_folder_documents.document_id
JOIN account_folders ON account_folders.account_folder_id = account_folder_documents.account_folder_id
JOIN `users_accounts` uact ON uact.user_id=user_activity_logs.user_id AND uact.account_id=user_activity_logs.account_id
WHERE user_activity_logs.site_id = 1 AND TYPE=11 AND account_folders.folder_type = 3 AND user_activity_logs.account_id IN ($account_id) AND user_activity_logs.user_id = $user_id  GROUP BY user_activity_logs.user_id,user_activity_logs.account_id) AS workspace_videos_viewed_count

ON usr.user_id = workspace_videos_viewed_count.`user_id` && usr.account_id = workspace_videos_viewed_count.account_id

LEFT JOIN

(SELECT user_activity_logs.user_id,user_activity_logs.account_id, COUNT(*) AS library_videos_viewed_count
FROM `user_activity_logs`
JOIN account_folder_documents ON user_activity_logs.ref_id = account_folder_documents.document_id
JOIN account_folders ON account_folders.account_folder_id = account_folder_documents.account_folder_id
JOIN `users_accounts` uact ON uact.user_id=user_activity_logs.user_id AND uact.account_id=user_activity_logs.account_id
WHERE user_activity_logs.site_id = 1 AND TYPE=11 AND account_folders.folder_type = 2 AND user_activity_logs.account_id IN ($account_id) AND user_activity_logs.user_id = $user_id  GROUP BY user_activity_logs.user_id,user_activity_logs.account_id) AS library_videos_viewed_count

ON usr.user_id = library_videos_viewed_count.`user_id` && usr.account_id = library_videos_viewed_count.account_id


LEFT JOIN

(SELECT d.created_by AS user_id,d.account_id, COUNT(*) AS scripted_observations
FROM documents d
LEFT JOIN account_folder_documents afd
ON d.`id`= afd.`document_id`
LEFT JOIN account_folders af
ON afd.`account_folder_id`= af.`account_folder_id`
LEFT JOIN user_activity_logs ua
ON (d.id = ua.ref_id)
WHERE d.site_id = 1 AND d.account_id IN ($account_id) AND ua.user_id = $user_id AND af.`folder_type` = 1 AND ua.`type` = 23 AND d.is_associated = 1 AND d.current_duration IS NULL AND d.published = 1 AND d.doc_type = 3 GROUP BY d.created_by,account_id) AS scripted_observations
ON usr.user_id = scripted_observations.user_id && usr.account_id = scripted_observations.account_id


LEFT JOIN

(SELECT d.created_by AS user_id,d.account_id, COUNT(*) AS scripted_video_observations
FROM documents d
LEFT JOIN account_folder_documents afd
ON d.`id`= afd.`document_id`
LEFT JOIN account_folders af
ON afd.`account_folder_id`= af.`account_folder_id`
LEFT JOIN user_activity_logs ua
ON (d.id = ua.ref_id)
WHERE d.site_id = 1 AND d.account_id IN ($account_id) AND ua.user_id = $user_id AND  af.`folder_type` = 1 AND ua.`type` = 20 AND d.is_associated > 0 AND d.current_duration IS NOT NULL AND d.published = 1 AND d.doc_type = 3 GROUP BY d.created_by,account_id) AS scripted_video_observations
ON usr.user_id = scripted_video_observations.user_id && usr.account_id = scripted_video_observations.account_id

LEFT JOIN
(
SELECT
d.created_by,
d.account_id,
SUM(df.duration) AS total_duration
FROM documents AS d
INNER JOIN document_files AS df
ON df.`document_id` = d.`id`
INNER JOIN user_activity_logs AS ua
ON ua.ref_id = df.document_id
WHERE
d.site_id = 1 AND
d.account_id IN ($account_id)
AND ua.type IN (2,4)
AND ua.user_id = $user_id

GROUP BY d.created_by,account_id
) AS document_durations
ON usr.user_id = document_durations.created_by && usr.account_id = document_durations.account_id

LEFT JOIN
(SELECT
dvh_history.user_id,
dvh_history.account_id,
SUM(minutes_watched) AS minutes_watched
FROM
(SELECT
dvh.`document_id`,
dvh.user_id,
dvh.account_id,
MAX(dvh.minutes_watched) AS minutes_watched
FROM
document_viewer_histories AS dvh
INNER JOIN account_folder_documents AS afd
ON afd.document_id = dvh.document_id
WHERE dvh.site_id = 1 AND
dvh.account_id IN ($account_id)
AND dvh.user_id = $user_id

GROUP BY dvh.`document_id`, dvh.`user_id`) AS dvh_history
GROUP BY dvh_history.user_id,dvh_history.account_id
) AS dv_histories
ON usr.user_id = dv_histories.user_id && usr.account_id = dv_histories.account_id

LEFT JOIN
(
SELECT
afu.`user_id`,
af.account_id,
CASE WHEN COUNT(af.account_folder_id) > 0 THEN 1 ELSE 0 END AS total_assess_huddle
FROM account_folders AS af
JOIN `account_folders_meta_data` AS afmd
ON afmd.`account_folder_id` = af.`account_folder_id`
JOIN `account_folder_users` AS afu
ON afu.`account_folder_id` = af.`account_folder_id`
WHERE
af.site_id = 1 AND afmd.`meta_data_name` = 'folder_type'
AND afmd.`meta_data_value` = 3
AND af.`account_id` IN ($account_id)
AND afu.`user_id` IN($user_id)    
AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
GROUP BY afu.user_id,af.`account_id`

)AS assessment_huddle
ON usr.user_id = assessment_huddle.user_id && usr.account_id = assessment_huddle.account_id

LEFT JOIN
(
SELECT
afu.`user_id`,
af.`account_id`,
CASE WHEN COUNT(af.account_folder_id) > 0 THEN 1 ELSE 0 END AS total_coaching_huddle
FROM account_folders AS af
JOIN `account_folders_meta_data` AS afmd
ON afmd.`account_folder_id` = af.`account_folder_id`
JOIN `account_folder_users` AS afu
ON afu.`account_folder_id` = af.`account_folder_id`
WHERE
af.site_id = 1 AND
afmd.`meta_data_name` = 'folder_type'
AND afmd.`meta_data_value` = 2
AND af.`account_id` IN ($account_id)
AND afu.`user_id` IN($user_id)
AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
GROUP BY afu.user_id,af.`account_id`

)AS coaching_huddle
ON usr.user_id = coaching_huddle.user_id && usr.account_id = coaching_huddle.account_id

LEFT JOIN (
SELECT
afu.`user_id`,
af.`account_id`,
CASE WHEN COUNT(af.account_folder_id) > 0 THEN 1 ELSE 0 END AS total_collab_huddle
FROM account_folders AS af
JOIN `account_folders_meta_data` AS afmd
ON afmd.`account_folder_id` = af.`account_folder_id`
JOIN `account_folder_users` AS afu
ON afu.`account_folder_id` = af.`account_folder_id`
WHERE
af.site_id = 1 AND
afmd.`meta_data_name` = 'folder_type'
AND afmd.`meta_data_value` = 1
AND af.`account_id` IN($account_id)
AND afu.`user_id` IN($user_id)    
AND af.active IN(IF((af.`archive`=1)AND(af.`active`=0),0,1))
GROUP BY afu.user_id,af.`account_id`

) AS collab_huddles
ON usr.user_id = collab_huddles.user_id && usr.account_id = collab_huddles.account_id
LEFT JOIN(
SELECT
afu.`user_id`,
af.`account_id`,
CASE WHEN COUNT(af.account_folder_id) > 0 THEN 1 ELSE 0 END AS is_coachee
FROM
account_folders AS af
INNER JOIN account_folder_users afu
ON af.`account_folder_id` = afu.`account_folder_id`
INNER JOIN `account_folders_meta_data` afmd ON afmd.`account_folder_id` = af.`account_folder_id`
WHERE af.site_id = 1 AND
af.`account_id` IN($account_id)
AND afu.`user_id` IN($user_id) 
AND afu.`role_id` = '210'
AND afmd.`meta_data_name` = 'folder_type'
AND afmd.`meta_data_value` = '2'
AND afu.`is_mentee` = 1
GROUP BY afu.user_id,af.`account_id`
) AS is_user_coache_details
ON usr.user_id = is_user_coache_details.user_id && usr.account_id = is_user_coache_details.account_id

LEFT JOIN (
SELECT
*
FROM
users_accounts
WHERE
users_accounts.`user_id` = $user_id AND users_accounts.`account_id` IN  ($account_id)
GROUP BY account_id, user_id
)AS user_accounts
ON (usr.user_id = user_accounts.user_id AND usr.account_id = user_accounts.account_id)
GROUP BY usr.user_id,account_id
ORDER BY CONCAT(usr.`first_name`,' ',usr.`last_name`) ASC");


        return $data;
    }

    function get_counts_of_comment_tags($account_id, $user_id) {

        App::import("Model", "Account");
        $db = new Account();
        $type_of_account = 'Individual';
        $account_detail = $db->find("first", array("conditions" => array(
                "id" => $account_id
        )));

        $number_of_child_account = $db->find("count", array("conditions" => array(
                "parent_account_id" => $account_id
        )));

        if (!empty($account_detail['Account']['parent_account_id']) && $account_detail['Account']['parent_account_id'] != '0') {
            $type_of_account = 'Child Account';
        } else {
            $type_of_account = 'Individual';
        }


        if ($number_of_child_account > 0) {
            $type_of_account = 'Parent Account';
        } else {
            $type_of_account = 'Individual';
        }









        App::import("Model", "User");
        $db = new User();
        // Can't use find() here because it adds site_id in where clause which we don't need here. The labels are same for both HMH and WL.
//         $avg_rating = $db->query("SELECT ROUND(AVG(rating_value),2) AS avg_rating FROM `document_standard_ratings` WHERE account_id = $account_id AND user_id = $user_id");
//         
//            
//         $custom_markers = $db->query("SELECT 
//                        COUNT(*) AS custom_markers 
//                      FROM
//                        `account_comment_tags` act 
//                        JOIN documents d 
//                          ON act.`ref_id` = d.`id` 
//                      WHERE act.`created_by` = $user_id 
//                        AND act.`ref_type` IN (2)
//                        AND d.`account_id` = $account_id");
// 
//         
//         
//         $tagged_standards = $db->query("SELECT 
//                        COUNT(*) AS tagged_standards 
//                      FROM
//                        `account_comment_tags` act 
//                        JOIN documents d 
//                          ON act.`ref_id` = d.`id` 
//                      WHERE act.`created_by` = $user_id 
//                        AND act.`ref_type` IN (0)
//                        AND d.`account_id` = $account_id");
//         $total_super_admins = $db->query("SELECT COUNT(*) AS total_super_admins FROM `users_accounts` WHERE role_id = 110 AND account_id = $account_id");
//         
//         $total_admins = $db->query("SELECT COUNT(*) AS total_admins FROM `users_accounts` WHERE role_id = 115 AND account_id = $account_id");
//         
//         $total_users = $db->query("SELECT COUNT(*) AS total_users FROM `users_accounts` WHERE role_id = 120 AND account_id = $account_id");
//         
//         $total_viewers = $db->query("SELECT COUNT(*) AS total_viewers FROM `users_accounts` WHERE role_id = 125 AND account_id = $account_id");
//         
        // $number_of_frameworks = $db->query("SELECT COUNT(*) AS number_of_frameworks FROM `account_tags` WHERE account_id = $account_id AND tag_type = 2 ");

        $coaching_perfomance_level = $db->query("SELECT meta_data_value AS coaching_perfomance_level FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'coaching_perfomance_level'");

        $assessment_perfomance_level = $db->query("SELECT meta_data_value AS assessment_perfomance_level FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'assessment_perfomance_level'");

        $account_framework_setting = $db->query("SELECT meta_data_value AS account_framework_setting FROM `account_folders_meta_data` WHERE account_folder_id = $account_id AND meta_data_name = 'enable_framework_standard'");

        $account_custom_marker_setting = $db->query("SELECT meta_data_value AS account_custom_marker_setting FROM `account_folders_meta_data` WHERE account_folder_id = $account_id AND meta_data_name = 'enable_tags'");

        $assessment_tracker = $db->query("SELECT meta_data_value AS assessment_tracker FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'enable_matric'");

        $coaching_tracker = $db->query("SELECT meta_data_value AS coaching_tracker FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'enable_tracker'");

        $student_payment = $db->query("SELECT amount AS payment_amount , `type` AS payment_type FROM `account_student_payments` WHERE account_id = $account_id");

        $user_limit = $this->get_allowed_users($account_id);

        $enable_video_library = $db->query("SELECT meta_data_value AS enable_video_library FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'enable_video_library'");
        $number_of_child_accounts = $db->query("SELECT COUNT(*) AS number_of_child_accounts FROM accounts WHERE parent_account_id = $account_id");
        $tracking_duration = $db->query("SELECT meta_data_value AS tracking_duration FROM `account_meta_data`  WHERE account_id = $account_id AND meta_data_name = 'tracking_duration'");

        App::import("Model", "AccountStudentPayments");
        $db = new AccountStudentPayments();
        $account_payments = $db->find("all", array("conditions" => array(
                "account_id" => $account_id,
        )));

        $payment_type = "";
        $payment_amount = "";

        foreach ($account_payments as $key => $pay) {
            if (count($account_payments) - 1 > $key) {
                $payment_type .= $pay['AccountStudentPayments']['type'] . '|';
                $payment_amount .= $pay['AccountStudentPayments']['amount'] . '|';
            } else {
                $payment_type .= $pay['AccountStudentPayments']['type'];
                $payment_amount .= $pay['AccountStudentPayments']['amount'];
            }
        }

        $data = array(
            //    'custom_markers' => $custom_markers[0][0]['custom_markers'],
            //     'tagged_standards' => $tagged_standards[0][0]['tagged_standards'],
            //     'avg_rating' => $avg_rating[0][0]['avg_rating'],
            // 'total_super_admins' => $total_super_admins[0][0]['total_super_admins'],
            // 'total_admins' => $total_admins[0][0]['total_admins'],
            // 'total_users' =>  $total_users[0][0]['total_users'],
            // 'total_viewers' =>  $total_viewers[0][0]['total_viewers'],
            // 'number_of_frameworks' => $number_of_frameworks[0][0]['number_of_frameworks'],
             'assessment_perfomance_level' => (isset($assessment_perfomance_level[0]['account_meta_data']['assessment_perfomance_level']) ? $assessment_perfomance_level[0]['account_meta_data']['assessment_perfomance_level'] : "0" ) ,
             'coaching_perfomance_level' => (isset($coaching_perfomance_level[0]['account_meta_data']['coaching_perfomance_level']) ? $coaching_perfomance_level[0]['account_meta_data']['coaching_perfomance_level'] : "0"),
             'account_framework_setting' => (isset($account_framework_setting[0]['account_folders_meta_data']['account_framework_setting']) ? $account_framework_setting[0]['account_folders_meta_data']['account_framework_setting'] : "0"),
             'account_custom_marker_setting' => (isset($account_custom_marker_setting[0]['account_folders_meta_data']['account_custom_marker_setting']) ? $account_custom_marker_setting[0]['account_folders_meta_data']['account_custom_marker_setting'] : "0"),
             'account_detail' => $account_detail,
             'assessment_tracker' => (isset($assessment_tracker[0]['account_meta_data']['assessment_tracker']) ? $assessment_tracker[0]['account_meta_data']['assessment_tracker'] : "0") ,
             'coaching_tracker' => (isset($coaching_tracker[0]['account_meta_data']['coaching_tracker']) ? $coaching_tracker[0]['account_meta_data']['coaching_tracker'] : "0"),
             'enable_video_library' => (isset($enable_video_library[0]['account_meta_data']['enable_video_library']) ? $enable_video_library[0]['account_meta_data']['enable_video_library'] : "0"),
             'number_of_child_accounts' => (isset($number_of_child_accounts[0][0]['number_of_child_accounts']) ? $number_of_child_accounts[0][0]['number_of_child_accounts'] : "0"),
             'tracking_duration' => (isset($tracking_duration[0]['account_meta_data']['tracking_duration']) ? $tracking_duration[0]['account_meta_data']['tracking_duration'] : "0"),
             'payment_amount' => $payment_amount,
             'payment_type' => $payment_type,
             'type_of_account' => $type_of_account,
             'user_limit' => $user_limit
             
             
         );
 
 
         return $data;
     }

     function getStreamingPlatformDetails($account_id)
     {
         App::import("Model", "Account");
         $db = new Account();
         $result = $db->query("SELECT * FROM `live_stream_platforms`  WHERE id = (select live_streaming_platform from accounts where id = $account_id)");
         return isset($result[0])?$result[0]['live_stream_platforms']:[];
     }

    public function downloadFile($url, $path, $strategy = 1)
    {
        if($strategy == 2)
        {
            $contents = file_get_contents($url);
            file_put_contents($path, $contents);
        }
        else
        {
            $newfname = $path;
            $file = fopen ($url, 'rb');
            if ($file) {
                $newf = fopen ($newfname, 'wb');
                if ($newf) {
                    while(!feof($file)) {
                        fwrite($newf, fread($file, 1024 * 8), 1024 * 8);
                    }
                }
            }
            if ($file) {
                fclose($file);
            }
            if ($newf) {
                fclose($newf);
            }
        }

    }

    public function sendCurlRequest($url,$authorization_token, $method = false, $is_post = false, $post_fields = [])
    {
        $headers = [];
        if(!empty($authorization_token))
        {
            $headers = array("Authorization: Bearer $authorization_token");
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        if($method && !$is_post)
        {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        }
        if($is_post)
        {
            if(is_array($post_fields))
            {
                $post_fields = json_encode($post_fields);
            }
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        }
        $server_output = curl_exec($ch);
        curl_close($ch);
        return json_decode($server_output);
    }
    
    public function is_push_notification_allowed($account_id)
    {
        App::import("Model", "Account");
        $db = new Account();
        $account_details = $db->find("first", array("conditions" => array(
                "id" => $account_id
        )));
        if($account_details['Account']['enable_push_notifications'] == '1')
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    
     public function is_push_notification_allowed_at_user_level($user_id)
    {
        App::import("Model", "User");
        $db = new User();
        $user_details = $db->find("first", array("conditions" => array(
                "id" => $user_id
        )));
        if($user_details['User']['enable_push_notifications'] == '1')
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
    
    public function is_email_notification_allowed($account_id)
    {
        App::import("Model", "Account");
        $db = new Account();
        $account_details = $db->find("first", array("conditions" => array(
                "id" => $account_id
        )));
        
        if($account_details['Account']['enable_emails'] == '1')
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }

    public function getGoalSettings($account_id, $role_id)
    {
        App::import("Model", "Account");
        $db = new Account();
        $result = $db->query("SELECT * FROM `goal_settings`  WHERE account_id =$account_id");
        return isset($result[0]) ? self::getPreferredAccountSettings($result[0]['goal_settings'], $role_id) : [];
    }

    public static function getPreferredAccountSettings($goal_settings, $role_id)
    {
        if($goal_settings['make_all_goals_public'])
        {
            $goal_settings['group_type_goals_public'] = 1;
            $goal_settings['individual_type_goals_public'] = 1;
            $goal_settings['account_type_goals_public'] = 1;
            return $goal_settings;
        }
        else if($goal_settings['make_all_goals_public_admin_super_owner'] && $role_id <= 115)//Admin,SuperAdmin,AccountOwner
        {
            $goal_settings['group_type_goals_public'] = 1;
            $goal_settings['individual_type_goals_public'] = 1;
            $goal_settings['account_type_goals_public'] = 1;
            return $goal_settings;
        }
        else if($goal_settings['make_all_goals_public_super_owner'] && $role_id <= 110)//SuperAdmin,AccountOwner
        {
            $goal_settings['group_type_goals_public'] = 1;
            $goal_settings['individual_type_goals_public'] = 1;
            $goal_settings['account_type_goals_public'] = 1;
            return $goal_settings;
        }

        if(!$goal_settings['group_type_goals_public'] && (($goal_settings['group_type_goals_public_admin_super_owner'] && $role_id <= 115) || ($goal_settings['group_type_goals_public_super_owner'] && $role_id <= 110)))
        {
            $goal_settings['group_type_goals_public'] = 1;
        }
        if(!$goal_settings['individual_type_goals_public'] && (($goal_settings['individual_type_goals_public_admin_super_owner'] && $role_id <= 115) || ($goal_settings['individual_type_goals_public_super_owner'] && $role_id <= 110)))
        {
            $goal_settings['individual_type_goals_public'] = 1;
        }
        if(!$goal_settings['account_type_goals_public'] && (($goal_settings['account_type_goals_public_admin_super_owner'] && $role_id <= 115) || ($goal_settings['account_type_goals_public_super_owner'] && $role_id <= 110)))
        {
            $goal_settings['account_type_goals_public'] = 1;
        }
        return $goal_settings;
    }
    public function getUserGoalSettings($account_id, $user_id)
    {
        App::import("Model", "Account");
        $db = new Account();
        $result = $db->query("SELECT * FROM `goal_users_settings`  WHERE account_id =$account_id AND user_id=$user_id");
        return isset($result[0])?$result[0]['goal_users_settings']:[];
    }

}
