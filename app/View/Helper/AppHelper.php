<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {

	var $timeZone = array(
        'American Samoa' => '(GMT-11:00) American Samoa',
        'International Date Line West' => '(GMT-11:00) International Date Line West',
        'Midway Island' => '(GMT-11:00) Midway Island',
        'Hawaii' => '(GMT-10:00) Hawaii',
        'Alaska' => '(GMT-09:00) Alaska',
        'Pacific Time (US &amp; Canada)' => '(GMT-08:00) Pacific Time (US &amp; Canada)',
        'Tijuana' => '(GMT-08:00) Tijuana',
        'Arizona' => '(GMT-07:00) Arizona',
        'Chihuahua' => '(GMT-07:00) Chihuahua',
        'Mazatlan' => '(GMT-07:00) Mazatlan',
        'Mountain Time (US &amp; Canada)' => '(GMT-07:00) Mountain Time (US &amp; Canada)',
        'Central America' => '(GMT-07:00) Mountain Time (US &amp; Canada)',
        'Central America' => '(GMT-06:00) Central America',
        'Central Time (US &amp; Canada)' => '(GMT-06:00) Central Time (US & Canada)',
        'Guadalajara' => '(GMT-06:00) Guadalajara',
        'Mexico City' => '(GMT-06:00) Mexico City',
        'Monterrey' => '(GMT-06:00) Monterrey',
        'Saskatchewan' => '(GMT-06:00) Saskatchewan',
        'Bogota' => '(GMT-05:00) Bogota',
        'Eastern Time (US &amp; Canada)' => '(GMT-05:00) Eastern Time (US &amp; Canada)',
        'Indiana (East)' => '(GMT-05:00) Indiana (East)',
        'Lima' => '(GMT-05:00) Lima',
        'Quito' => '(GMT-05:00) Quito',
        'Caracas' => '(GMT-04:30) Caracas',
        'Atlantic Time (Canada)' => '(GMT-04:00) Atlantic Time (Canada)',
        'Georgetown' => '(GMT-04:00) La Paz',
        'La Paz' => '(GMT-04:00) La Paz',
        'Santiago' => '(GMT-04:00) Santiago',
        'Newfoundland' => '(GMT-03:30) Newfoundland',
        'Brasilia' => '(GMT-03:00) Brasilia',
        'Buenos Aires' => '(GMT-03:00) Buenos Aires',
        'Greenland' => '(GMT-03:00) Greenland',
        'Mid-Atlantic' => '(GMT-02:00) Mid-Atlantic',
        'Azores' => '(GMT-01:00) Azores',
        'Cape Verde Is.' => '(GMT-01:00) Cape Verde Is.',
        'Casablanca' => '(GMT+00:00) Casablanca',
        'Dublin' => '(GMT+00:00) Dublin',
        'Edinburgh' => '(GMT+00:00) Edinburgh',
        'Lisbon' => '(GMT+00:00) Lisbon',
        'London' => '(GMT+00:00) London',
        'Monrovia' => '(GMT+00:00) Monrovia',
        'UTC' => '(GMT+00:00) UTC',
        'Amsterdam' => '(GMT+01:00) Belgrade',
        'Belgrade' => '(GMT+01:00) Belgrade',
        'Berlin' => '(GMT+01:00) Berlin',
        'Bern' => '(GMT+01:00) Bern',
        'Bratislava' => '(GMT+01:00) Bratislava',
        'Brussels' => '(GMT+01:00) Brussels',
        'Budapest' => '(GMT+01:00) Budapest',
        'Copenhagen' => '(GMT+01:00) Copenhagen',
        'Ljubljana' => '(GMT+01:00) Ljubljana',
        'Madrid' => '(GMT+01:00) Madrid',
        'Paris' => '(GMT+01:00) Paris',
        'Prague' => '(GMT+01:00) Prague',
        'Rome' => '(GMT+01:00) Rome',
        'Sarajevo' => '(GMT+01:00) Sarajevo',
        'Skopje' => '(GMT+01:00) Skopje',
        'Stockholm' => '(GMT+01:00) Stockholm',
        'Vienna' => '(GMT+01:00) Vienna',
        'Warsaw' => '(GMT+01:00) Warsaw',
        'West Central Africa' => '(GMT+01:00) West Central Africa',
        'Zagreb' => '(GMT+01:00) Zagreb', 'Athens' => '(GMT+02:00) Athens',
        'Bucharest' => '(GMT+02:00) Bucharest',
        'Cairo' => '(GMT+02:00) Cairo',
        'Harare' => '(GMT+02:00) Harare',
        'Helsinki' => '(GMT+02:00) Helsinki',
        'Istanbul' => '(GMT+02:00) Istanbul',
        'Jerusalem' => '(GMT+02:00) Jerusalem',
        'Kyiv' => '(GMT+02:00) Kyiv',
        'Pretoria' => '(GMT+02:00) Pretoria',
        'Riga' => '(GMT+02:00) Riga',
        'Sofia' => '(GMT+02:00) Sofia',
        'Tallinn' => '(GMT+02:00) Tallinn',
        'Vilnius' => '(GMT+02:00) Vilnius',
        'Baghdad' => '(GMT+03:00) Baghdad',
        'Kuwait' => '(GMT+03:00) Kuwait',
        'Minsk' => '(GMT+03:00) Minsk',
        'Nairobi' => '(GMT+03:00) Nairobi',
        'Riyadh' => '(GMT+03:00) Riyadh',
        'Tehran' => '(GMT+03:30) Tehran'
    );
}
