<?php


class BrowserHelper extends AppHelper
{

    public function __construct(\View $View, $settings = array())
    {
        parent::__construct($View, $settings);
    }


    public function isFirefox() {
        $agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
        if (strlen(strstr($agent, 'Firefox')) > 0) {
            return true;
        }
        return false;
    }

}
