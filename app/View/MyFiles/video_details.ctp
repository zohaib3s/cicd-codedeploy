<?php $breadcrumb_language_based_content = $this->Custom->get_page_lang_based_content('BreadCrumb'); ?>
<?php $total_doc_vid_count = $video_count + $documents_count; ?>

<div id="flashMessage2" class="message error" style="display:none;"></div>
<input id="latest_video_created_date" type="hidden" value="<?php echo $latest_video_created_date; ?>">
<input id="video_envo" type="hidden" value="3">
<div class="tab-box" style="overflow: visible;">
    <div id="tab-area">
            <?php echo $video_tab; ?>
        </div>
    </div>

    <input id="txtUploadedFilePath" type="hidden" value="" />
    <input id="txtUploadedFileName" type="hidden" value="" />
    <input id="txtUploadedFileMimeType" type="hidden" value="" />
    <input id="txtUploadedFileSize" type="hidden" value="" />
    <input id="txtUploadedDocType" type="hidden" value="" />
    <input id="txtUploadedUrl" type="hidden" value="" />
    <input id="uploadPath" type="hidden" value="<?php echo "/tempupload/$account_id/" . date('Y/m/d/'); ?>" />
    <input id="myFileTabIndex" type="hidden" value="<?php echo $tab; ?>" />
    <input id="isLiveRecording" type="hidden" value="<?php echo (isset($video_details) && !empty($video_details) && $video_details['doc']['published'] == '0') ? '1' : '0'; ?>" />
</div>
<?php echo $reveal_model; ?>
<script>
    $('.copy').click(function () {
        $.each($('.copyFiles_checkbox'), function (index, value) {

            $(this).closest('label').removeClass('active');
            $(this).prop('checked', false);


        });
    });

    $('#gVideoUpload').click(function () {
        $.ajax({
            url: '<?php echo $this->base; ?>/Huddles/check_storage_full',
            type: 'POST',
            success: function (response) {
                if (response != '') {
                    $('#flashMessage2').css('display', 'block');
                    $('#flashMessage2').html(response);

                } else {
                    OpenFilePicker('MyFilesVideo');
                }
            }
        });

    });


    $('#gDocumentUpload').click(function () {
        $.ajax({
            url: '<?php echo $this->base; ?>/Huddles/check_storage_full',
            type: 'POST',
            success: function (response) {
                if (response != '') {
                    $('#flashMessage2').css('display', 'block');
                    $('#flashMessage2').html(response);

                } else {
                    OpenFilePicker('MyFilesDocs');
                }
            }
        });

    });

    $('#flashMessage2').click(function (e) {
        $('#flashMessage2').fadeOut();
    });

     $(document).ready(function (e) {
        <?php if (isset($main_workspace)) : ?>

        var bread_crumb_data =  '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><span href="#"><?php echo $breadcrumb_language_based_content['my_workspace_breadcrumb']; ?></span></div>';
        $('.breadCrum').html(bread_crumb_data);


       <?php  else: ?>

        var bread_crumb_data =  '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><a href="/myFiles"><?php echo $breadcrumb_language_based_content['my_workspace_breadcrumb']; ?></a><span>Video</span></div>';

        $('.breadCrum').html(bread_crumb_data);



         <?php endif; ?>

       doMyFileDocumentsSearchAjax();

   });

</script>
