<?php
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];
$user_id = $users['User']['id'];
?>
<style>
  [class*="icon-"]::before
    {
        background: none !important;
        
    }   
</style>   
<script src="<?php echo $this->webroot . 'crossdomain.xml' ?>"></script>
<script src="<?php echo $this->webroot . 'cross-domain-policy.dtd' ?>"></script>
<html lang="en">    
    <head>
        <title>3S Live Stream</title>
        <meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="http://fonts.googleapis.com/css?family=Dosis" rel="stylesheet" type="text/css"/>
        <!-- bitdash player -->
        <script src="https://content.jwplatform.com/libraries/gXmBKmOv.js"> </script>
    </head>

    <body>
        <div id="myElement"></div>
        <script type="text/JavaScript">
        var playerInstance = jwplayer("myElement");
        playerInstance.setup({
            file: "https://59ec70ba7b9df.streamlock.net/livecf/<?php echo $account_id . '_' . $user_id; ?>/playlist.m3u8"
        });
        </script>
    </body>
</html>