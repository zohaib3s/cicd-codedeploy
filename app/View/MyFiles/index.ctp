<?php $breadcrumb_language_based_content = $this->Custom->get_page_lang_based_content('BreadCrumb'); ?>
<?php
$total_doc_vid_count = $video_count + $documents_count;
$btn_res_color = $this->Custom->get_site_settings('primary_bg_color');
$btn_vd_color = $this->Custom->get_site_settings('secondary_bg_color');
?>

<div id="flashMessage2" class="message error" style="display:none;"></div>
<input id="latest_video_created_date" type="hidden" value="<?php echo $latest_video_created_date; ?>">
<input id="video_envo" type="hidden" value="3">
<div class="header header-huddle">
    <h2 class="title"><?php echo $language_based_content['my_workspace_workspace']; ?> <?php if ($total_doc_vid_count > -1 && empty($video_id))://isset($main_workspace)) :             ?><span class="blue_count"><?php echo $total_doc_vid_count; ?></span><?php endif; ?></h2>
    <p class="title-desc" style="display:none;"><?php echo $language_based_content['upload_your_videos_desc_workspace']; ?></p>
</div>
<div class="tab-box" style="overflow: visible;">
    <div id="tab-area">
        <?php //if(empty($video_id)):?>
        <?php // echo (isset($video_details) && !empty($video_details) && $video_details['doc']['is_processed'] == '5') ? 'display:none;' : 'display:block;' ?>
        <div class="btn-box" style="margin:none;">
            <a id="gDocumentUpload" class="btn btn-green js-upload-document" style="display: none;background-color: <?php echo $btn_res_color ?>; border-color:<?php echo $btn_res_color ?>;" href="#"><?php echo $language_based_content['upload_resource_view_workspace']; ?></a>
            <?php if (isset($video_details) && !empty($video_details) && $video_details['doc']['is_processed'] == '5') { ?>
                <a id="gVideoUpload" class="btn btn-orange icon2-video tab2 gVideoUpload" style="display:none;background-color: <?php echo $btn_vd_color ?>; border-color:<?php echo $btn_vd_color ?>;" href="#"><?php echo $language_based_content['upload_video_view_workspace']; ?></a>
            <?php } else { ?>
                <a id="gVideoUpload" class="btn btn-orange icon2-video tab2 gVideoUpload" style="display:block;background-color: <?php echo $btn_vd_color ?>; border-color:<?php echo $btn_vd_color ?>;" href="#"><?php echo $language_based_content['upload_video_view_workspace']; ?></a>
            <?php } ?>
        </div>
        <ul class="tabset <?php
        if (( $video_count != '' || $documents_count != '' ) && empty($video_id)) {
            echo "tcount";
        }
        ?> ">
            <li><a class="tab <?php echo $tab == 1 ? 'active' : ''; ?> " href="#tabbox1">Videos <span><?php echo $video_count; ?></span></a> </li>
            <li><a class="tab <?php echo $tab == 2 ? 'active' : ''; ?>"  href="#tabbox2"><?php echo $language_based_content['resources_view_workspace']; ?> <span><?php echo $documents_count; ?></span></a> </li>
        </ul>
        <?php //endif;  ?>
        <div class="table-container">
            <?php echo $video_tab; ?>
            <?php //if(empty($video_id)):?>
            <?php echo $documents_tab; ?>
            <?php //endif;   ?>
        </div>
    </div>

    <input id="txtUploadedFilePath" type="hidden" value="" />
    <input id="txtUploadedFileName" type="hidden" value="" />
    <input id="txtUploadedFileMimeType" type="hidden" value="" />
    <input id="txtUploadedFileSize" type="hidden" value="" />
    <input id="txtUploadedDocType" type="hidden" value="" />
    <input id="txtUploadedUrl" type="hidden" value="" />
    <input id="uploadPath" type="hidden" value="<?php echo "/tempupload/$account_id/" . date('Y/m/d/'); ?>" />
    <input id="myFileTabIndex" type="hidden" value="<?php echo $tab; ?>" />
    <input id="isLiveRecording" type="hidden" value="<?php echo (isset($video_details) && !empty($video_details) && $video_details['doc']['published'] == '0') ? '1' : '0'; ?>" />
</div>
<?php echo $reveal_model; ?>
<script>
    $('.copy').click(function () {
        $.each($('.copyFiles_checkbox'), function (index, value) {

            $(this).closest('label').removeClass('active');
            $(this).prop('checked', false);


        });
    });

    $('#gVideoUpload').click(function () {
        $.ajax({
            url: '<?php echo $this->base; ?>/Huddles/check_storage_full',
            type: 'POST',
            success: function (response) {
                if (response != '') {
                    $('#flashMessage2').css('display', 'block');
                    $('#flashMessage2').html(response);

                } else {
                    OpenFilePicker('MyFilesVideo');
                }
            }
        });

    });


    $('#gDocumentUpload').click(function () {
        $.ajax({
            url: '<?php echo $this->base; ?>/Huddles/check_storage_full',
            type: 'POST',
            success: function (response) {
                if (response != '') {
                    $('#flashMessage2').css('display', 'block');
                    $('#flashMessage2').html(response);

                } else {
                    OpenFilePicker('MyFilesDocs');
                }
            }
        });

    });

    $('#flashMessage2').click(function (e) {
        $('#flashMessage2').fadeOut();
    });

    $(document).ready(function (e) {
<?php if (isset($main_workspace)) : ?>

            var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><span href="#"><?php echo $breadcrumb_language_based_content['my_workspace_breadcrumb']; ?></span></div>';
            $('.breadCrum').html(bread_crumb_data);


<?php else: ?>

            var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><a href="/myFiles"><?php echo $breadcrumb_language_based_content['my_workspace_breadcrumb']; ?></a><span>Video</span></div>';

            $('.breadCrum').html(bread_crumb_data);



<?php endif; ?>

        doMyFileDocumentsSearchAjax();

    });

</script>
