<!DOCTYPE html>
<html>
    <head>
        <meta name="msapplication-config" content="none"/>
        <?php echo $this->Html->charset(); ?>
        <?php echo $this->Html->meta('icon'); ?>
        <title><?php echo $title_for_layout; ?></title>

        <?php  
              
        $controllerName = strtolower($this->params['controller']);
        $actionName = strtolower($this->params['action']);
        $isMyFilePage = $controllerName == 'MyFiles' && $actionName == 'view';
        if ($isMyFilePage) {//video-editor.css,
            $cssFiles = array(
                'app', 'sibme', 'alerts', 'bootstrap.min', 'fancybox/jquery.fancybox.css',
                'video-editor.css','video-js.css', 'jquery.nouislider.css', 'custom.css', 'jquery.timepicker.css'
            );
        } else {
            $cssFiles = array(
                'app', 'sibme', 'alerts', 'bootstrap.min', 'fancybox/jquery.fancybox.css',
                'video-editor.css','video-js.css', 'custom.css', 'jquery.timepicker.css','jquery.datetimepicker.css','newstyle.css','jquery.treeview.css'
            );
        }
        $use_local_store = Configure::read('use_local_file_store');
        $amazon_assets_url = Configure::read('amazon_assets_url');
        if($use_local_store){
            echo $this->Minify->css($cssFiles);
        }else{
            if ($isMyFilePage) {
                echo '<link href="'.$amazon_assets_url.'static/css/min-huddle-myfiles.zip.css" media="screen" rel="stylesheet" type="text/css" />';
            }else{
                echo '<link href="'.$amazon_assets_url.'static/css/min-huddle-general.zip.css" media="screen" rel="stylesheet" type="text/css" />';
            }
        }
        
        ?>
        <link href="https://desk-customers.s3.amazonaws.com/shared/email_widget.css" media="screen" rel="stylesheet" type="text/css" />

        <script type="text/javascript">
            var home_url = '<?php echo $this->base; ?>';
            var supress_app_init = true;
            var VideoEditor = null;
            var bucket_name = "<?php echo Configure::read('bucket_name'); ?>";
            var filepicker_access_key = "<?php echo Configure::read('filepicker_access_key'); ?>";
            var autoPlaySibmePlayer = true;
            <?php if (isset($_REQUEST['open_trim']) && $_REQUEST['open_trim'] == '1'): ?>
                autoPlaySibmePlayer = false;
            <?php endif; ?>
        </script>

        <?php
        if ($controllerName == 'myfiles') {
            $jsFiles = array(
                'jquery-1.11.0.min','advanced','wysihtml5-0.3.0', 'bootstrap.min', 'jquery.validate.min', 'jquery.tinyscrollbar.min','videojs.js',
                'jquery.jcarousel', 'jquery.cookie', 'bootstrap-wysihtml5', 'jquery.loadmask', 'jquery.tagsinput', 'fancybox/jquery.fancybox.js',
                'tabs', 'video-js/video.dev.js', 'common', 'script', 'script.video.js', 'plugins_editor.js', 'video-editor.js', 'myfiles', 'jquery.jqpagination', 'masonry.pkgd.min',
                'jquery.timepicker.js',  'slimtable.min.js','jquery.overlay.min.js', 'jquery.highlight.js'
            );
        } else {
            $jsFiles = array(
                'jquery-1.11.0.min','jquery-ui','advanced','wysihtml5-0.3.0', 'bootstrap.min', 'jquery.validate.min', 'jquery.tinyscrollbar.min','videojs.js',
                'jquery.jcarousel', 'jquery.cookie', 'bootstrap-wysihtml5', 'jquery.loadmask', 'jquery.tagsinput', 'fancybox/jquery.fancybox.js',
                'tabs', 'video-js/video.dev.js', 'common', 'script', 'script.video.js', 'plugins_editor.js', 'video-editor.js', 'huddles', 'jquery.jqpagination', 'masonry.pkgd.min',
                'jquery.timepicker.js',  'slimtable.min.js', 'jquery.overlay.min.js', 'quick_search.js' ,'jquery.highlight.js','jquery.datetimepicker.js','jquery.treeview.js','demo.js'
            );
        }
        
        if($use_local_store){
            echo $this->Minify->script($jsFiles);
        }else{
            if ($controllerName == 'myfiles') {
                echo '<script type="text/javascript" src="'.$amazon_assets_url.'static/js/min-huddle-myfiles.js"></script>';
            }else{
                echo '<script type="text/javascript" src="'.$amazon_assets_url.'static/js/min-huddle-general.js"></script>';
            }
        }
        ?>
    </head>
    <body <?php if($_SESSION['LANG'] == 'es' ) { echo 'class="language_sp"'; }  ?>>
        
        <?php if(IS_PROD): ?>
    
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KQKHC78"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    
    
<?php endif; ?>
        
        
        
        
        <style>
                  .impersonation{
        padding: 5px;
    background: #FF7800;
    color: #fff;
    font-size: 13px;
    font-weight: bold;
    text-align: center;
    z-index: 9;
    border-bottom: solid 1px #4A3A2E;
    position: relative;
    }.impersonation a {
                color:white;
            }
                            .container {
    position: relative;
}
.breadCrum{
    margin-top: 28px;
}
.breadCrum span{
    color: #484848 ;
}
.breadCrum a{
    font-weight: normal;
    color: #838383 ;
}
.breadCrum a:hover{
    color: #484848;
}
.breadCrum a:after{
    content:">";
    margin:0px 12px;
}


  .breadCrum {
                margin-top: 20px;
           
                font-size: 13px;
            }
            .breadCrum a:after {
                content: "";
                padding: 0px 12px;
                background: url(/img/breadcrum_arrow.svg) no-repeat center center;
                top: 1px;
                position: relative;
            }
            .breadCrum .container span {
                color: #4d504d;
            }

        </style>
        <!-- <?php  echo date('M/d/Y h:i:s',strtotime('now')); ?>-->
                <?php        
$lst_login = $this->Session->read('last_logged_in_user');  
if (!empty($lst_login['id'])):
        ?>
<div  class="impersonation"><a href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>">exit impersonation</a></div>
<?php else: ?>
<div style="display:none;" class="impersonation"><a href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>">exit impersonation</a></div>
<?php endif; ?>
        <header id="header">
            <?php echo $this->element('menus/top_menus'); ?>
        </header>
         <div class="breadCrum">
            <div class="container">
<!--            <a href="#">Huddles</a> <a href="#">Coaching Huddles 16-17</a> <a href="#">Andrew Rosenquist</a> <span>Observation 9-1</span>-->
            </div>
        </div>
        <div style="margin: 32px auto 0px;width: 948px;background: red;color: #fff;padding: 10px;display:none;">We are currently experiencing technical difficulties with video processing. If one of your huddles was inactivated after uploading a video, please let us know immediately. Thank you for your patience as we resolve this issue.</div>
        <div id="main" class="container" style="overflow: visible;">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch("content"); ?>
        </div>

        <?php if (isset($video_huddle_model) && $video_huddle_model != '' && $tab == '1'): ?>
            <?php echo $video_huddle_model; ?>
        <?php endif; ?>

        <script src="//static.filestackapi.com/v3/filestack.js"></script>
        <?php echo $this->element('default_footer'); ?>
        <!-- <?php  echo date('M/d/Y h:i:s',strtotime('now')); ?>-->
    </body>
</html>
