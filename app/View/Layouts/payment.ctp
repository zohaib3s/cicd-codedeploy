<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
$cakeDescription = __d('cake_dev', 'Student Payment Registration Form');
$lang_content = $this->Custom->get_page_lang_based_content('student_payment');
?>
<!DOCTYPE html>
<html class="js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths -webkit-">
    <head>
        <meta name="msapplication-config" content="none"/>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $cakeDescription ?>:
            <?php echo $title_for_layout; ?>
        </title>
        <?php
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>

        <?php
            echo $this->Minify->css(array('sibme.min', 'alerts', '/usermgmt/css/umstyle'));
        ?>
        <?php

            echo $this->Minify->script(array('jquery-1.9.0', 'jquery.form', 'jquery.validate.min'));

        ?>

        <!-- Facebook Conversion Tracking Pixel Start -->
        <script type="text/javascript">
        var fb_param = {};
        fb_param.pixel_id = '6009844314164';
        fb_param.value = '0.00';
        fb_param.currency = 'USD';
        (function(){
          var fpw = document.createElement('script');
          fpw.async = true;
          fpw.src = '//connect.facebook.net/en_US/fp.js';
          var ref = document.getElementsByTagName('script')[0];
          ref.parentNode.insertBefore(fpw, ref);
        })();
        </script>
        
        <!-- Facebook Conversion Tracking Pixel End -->
    </head>
    <body class="box-page <?php if($_SESSION['LANG'] == 'es' ) { echo 'language_sp'; }  ?>" data-twttr-rendered="true">
        <div class="wrapper">
        <style>
            .header_logo{       
                width: 150px;
            }
        </style>
            <header class="island compact text-center">
                <h1><a href="#" title="Sibme"><?php 
                $chimg = $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/sibme_logo2.png');
                echo $this->Html->image($chimg,array('class'=>'header_logo')); ?></a></h1>
            </header>
            <!-- / logo -->

            <header class="island subheader pitch-black text-center">
                <h2 class="normal"><b><?php echo $lang_content['student_payment']; ?></b> <?php echo $lang_content['reg_form_payment']; ?></h2>
            </header>

            <!-- / sub header -->
            <div id="content">
                <?php
                echo  $this->Session->flash();
                ?>

                <?php echo $this->fetch('content'); ?>
            </div>
        </div>
        <div id="cboxOverlay" style="display: none;"></div>
        <div id="colorbox" class="" style="display: none;">
            <div id="cboxWrapper">
                <div>
                    <div id="cboxTopLeft" style="float: left;">

                    </div>
                    <div id="cboxTopCenter" style="float: left;"></div>
                    <div id="cboxTopRight" style="float: left;"></div>

                </div>
                <div style="clear: left;">
                    <div id="cboxMiddleLeft" style="float: left;"></div>
                    <div id="cboxContent" style="float: left;">
                        <div id="cboxLoadedContent" style="width: 0px; height: 0px; overflow: hidden; float: left;"></div>
                        <div id="cboxTitle" style="float: left;"></div>
                        <div id="cboxCurrent" style="float: left;"></div>
                        <div id="cboxNext" style="float: left;"></div>
                        <div id="cboxPrevious" style="float: left;"></div>
                        <div id="cboxSlideshow" style="float: left;"></div>
                        <div id="cboxClose" style="float: left;"></div></div>
                    <div id="cboxMiddleRight" style="float: left;"></div></div>
                <div style="clear: left;"><div id="cboxBottomLeft" style="float: left;">
                    </div><div id="cboxBottomCenter" style="float: left;"></div>
                    <div id="cboxBottomRight" style="float: left;"></div>
                </div>
            </div>
            <div style="position: absolute; width: 9999px; visibility: hidden; display: none;"></div>
        </div>
    </body>
</html>
