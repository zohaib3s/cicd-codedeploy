<!DOCTYPE html>
<html>
    <meta name="msapplication-config" content="none"/>
    
    <?php    
    $users = $this->Session->read('user_current_account');

    if ($users && $this->params['controller'] == 'accounts' && $this->params['action'] == 'accountSettings' || $this->params['action'] == 'account_settings') {
        echo $this->element('default_header_old');
    } else {
        echo $this->element('default_header');
    }
    ?>                                                
    <body <?php if($_SESSION['LANG'] == 'es' ) { echo 'class="language_sp"'; }  ?> >
        
        
        <?php if(IS_PROD): ?>
    
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KQKHC78"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    
    
<?php endif; ?>
        
        
        
        <style>
            .dialogAdj{
                overflow: inherit !important;
            }
            .dialogAdj #desk-support-box{
                margin: -98px -425px;
            }
            .error {
                color: #be1616 !important;
                background-color: #fff2f2 !important;
                border-color: #d93f3f !important;
            }
        .impersonation{
        padding: 5px;
    background: #FF7800;
    color: #fff;
    font-size: 13px;
    font-weight: bold;
    text-align: center;
    z-index: 9;
    border-bottom: solid 1px #4A3A2E;
    position: relative;
    }.impersonation a {
                color:white;
            }
        </style>
        
        <?php        
$lst_login = $this->Session->read('last_logged_in_user');  
if (!empty($lst_login['id'])):
        ?>
<div  class="impersonation"><a href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>">exit impersonation</a></div>
<?php else: ?>
<div style="display:none;" class="impersonation"><a href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>">exit impersonation</a></div>
<?php endif; ?>
        
        
        <header id="header">
            <?php
            $action = $this->params['action'];
            if ($users && !in_array($action, array('login', 'forgot_password', 'change_password'))) {
                echo $this->element('menus/top_menus');
            }
            ?>
        </header>
        <div style="margin: 32px auto 0px;width: 948px;background: red;color: #fff;padding: 10px;display:none;">We are currently experiencing technical difficulties with video processing. If one of your huddles was inactivated after uploading a video, please let us know immediately. Thank you for your patience as we resolve this issue.</div>
        <?php
            $mainClass = '';
            if ($users && $this->name == "Dashboard" && ($this->action == 'home' || $this->action == 'coach_tracker' || $this->action == 'assessment_tracker')) {
                $mainClass .= ' box';
            }
            elseif ($this->name == 'Users' && $this->action == 'login') {
                $mainClass .= ' login-main';
            }
            elseif ($this->name == 'Users' && ($this->action == 'forgot_password' || $this->action == 'change_password')) {
                $mainClass .= ' forgot-password';
            }elseif ($this->name == 'Huddles' && ($this->action == 'create' || $this->action == 'edit')) {
                $mainClass .= ' dialogAdj';
            }
        ?>

        <div id="main" class="container<?php echo $mainClass;?>" >
            
            <?php
            if ($users && isset($head) && $head != ''):
             echo $head;
            endif;
            ?>

            <?php echo $this->fetch('content'); ?>
        </div>
        <?php if ($users && isset($video_huddle_model) && $video_huddle_model != ''): ?>
            <?php echo $video_huddle_model; ?>
        <?php endif; ?>

        <script src="//static.filestackapi.com/v3/filestack.js"></script>
        <?php echo $this->element('default_footer'); ?>
    </body>
</html>
