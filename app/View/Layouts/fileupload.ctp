<!DOCTYPE html>
<html>
    <head>
        <meta name="msapplication-config" content="none"/>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>

        <?php
            echo $this->Minify->css(array('fancybox/jquery.fancybox', 'sibme'));
        ?>

        <script type="text/javascript">
            var home_url = '<?php echo $this->base; ?>';
            var bucket_name = "<?php echo Configure::read('bucket_name');?>";
        </script>

        <?php
            echo $this->Minify->script(array('jquery-1.9.0', 'fancybox/jquery.fancybox', 'jquery.form', 'jquery.validate.min',
                'jquery.ui.widget', 'jquery.iframe-transport', 'jquery.fileupload', 'jquery.fileupload-process'));
        ?>
    </head>
    <body <?php if($_SESSION['LANG'] == 'es' ) { echo 'class="language_sp"'; }  ?>>
        <?php echo $this->fetch("content"); ?>
        <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    </body>
</html>
