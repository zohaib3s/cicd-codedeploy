<!DOCTYPE html>
<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<html>
    <head>
        <meta name="msapplication-config" content="none"/>
        <meta name="viewport" content="width=1024">
        <?php echo $this->Html->charset(); ?>
        <?php //echo $this->Html->meta('icon'); ?>
        <title><?php echo $title_for_layout; ?></title>

        <?php
        $controllerName = strtolower($this->params['controller']);
        $actionName = strtolower($this->params['action']);
        $isAnalytics = false;
        $isMyFilePage = $controllerName == 'MyFiles' && $actionName == 'view';
        $cdn_ver = Configure::read('cdn_ver');
        if ($isMyFilePage) {
            //video-editor.css,
            $cssFiles = array(
                'app', 'sibme', 'alerts', 'bootstrap.min', 'fancybox/jquery.fancybox.css',
                'video-editor.css', 'video-js.css', 'jquery.nouislider.css', 'custom.css', 'jquery.timepicker.css'
            );
        } else {

            if ($isAnalytics) {

                $cssFiles = array(
                    'app', 'sibme', 'alerts', 'bootstrap.min', 'fancybox/jquery.fancybox.css',
                    'video-editor.css', 'video-js.css', 'custom.css', 'jquery.timepicker.css', 'jquery.datetimepicker.css', 'virtual_tracker.css', 'amcharts/plugins/export/export.css',
                    'analytics.css'
                );
            } else {

                $cssFiles = array(
                    'app', 'sibme', 'alerts', 'bootstrap.min', 'fancybox/jquery.fancybox.css',
                    'video-editor.css', 'video-js.css', 'custom.css', 'jquery.timepicker.css', 'jquery.datetimepicker.css'
                );
            }
        }

        if ($controllerName == 'huddles') {
            if ('view' == $actionName) {
                $isAnalytics = true;
            }
        }
        $use_local_store = Configure::read('use_local_file_store');
        $amazon_assets_url = Configure::read('amazon_assets_url');
        if ($use_local_store) {
            echo $this->Minify->css($cssFiles);
        } else {
            if ($isMyFilePage) {
                echo '<link href="' . $amazon_assets_url . 'static/css/min-huddle-myfiles.zip.css?v=' . $cdn_ver . '" media="screen" rel="stylesheet" type="text/css" />';
            } else {
                echo '<link href="' . $amazon_assets_url . 'static/css/min-huddle-general.zip.css?v=' . $cdn_ver . '" media="screen" rel="stylesheet" type="text/css" />';
            }
        }
        ?>
        <link href="https://desk-customers.s3.amazonaws.com/shared/email_widget.css" media="screen" rel="stylesheet" type="text/css" />

        <script type="text/javascript">
            var home_url = '<?php echo $this->base; ?>';
            var supress_app_init = true;
            var VideoEditor = null;
            var bucket_name = "<?php echo Configure::read('bucket_name'); ?>";
            var filepicker_access_key = "<?php echo Configure::read('filepicker_access_key'); ?>";
            var autoPlaySibmePlayer = true;
<?php if (isset($_REQUEST['open_trim']) && $_REQUEST['open_trim'] == '1'): ?>
                autoPlaySibmePlayer = false;
<?php endif; ?>
        </script>

        <?php
        if ($controllerName == 'myfiles') {
            $jsFiles = array(
                'jquery-1.11.0.min', 'jquery-ui', 'advanced', 'wysihtml5-0.3.0', 'bootstrap.min', 'jquery.validate.min', 'jquery.tinyscrollbar.min', 'videojs.js',
                'jquery.jcarousel', 'jquery.cookie', 'bootstrap-wysihtml5', 'jquery.loadmask', 'jquery.tagsinput', 'fancybox/jquery.fancybox.js',
                'tabs', 'video-js/video.dev.js', 'common', 'script', 'script.video.js', 'plugins_editor.js', 'video-editor.js', 'myfiles', 'jquery.jqpagination', 'masonry.pkgd.min',
                'jquery.timepicker.js', 'slimtable.min.js', 'jquery.overlay.min.js', 'jquery.highlight.js',
                'audio_src/src/recorder.js', 'audio_src/src/Fr.voice.js', 'audio_src/js/app.js'
            );
        } else {

            if ($isAnalytics) {

                $jsFiles = array(
                    'jquery-1.11.0.min', 'jquery-ui', 'advanced', 'wysihtml5-0.3.0', 'bootstrap.min', 'jquery.validate.min', 'jquery.tinyscrollbar.min', 'videojs.js',
                    'jquery.jcarousel', 'jquery.cookie', 'bootstrap-wysihtml5', 'jquery.loadmask', 'jquery.tagsinput', 'fancybox/jquery.fancybox.js',
                    'tabs', 'video-js/video.dev.js', 'common', 'script', 'script.video.js', 'plugins_editor.js', 'video-editor.js', 'huddles', 'jquery.jqpagination', 'masonry.pkgd.min',
                    'jquery.timepicker.js', 'slimtable.min.js', 'jquery.overlay.min.js', 'quick_search.js', 'jquery.highlight.js', 'jquery.datetimepicker.js', 'amcharts/amcharts.js', 'amcharts/serial.js'
                    , 'amcharts/themes/light.js', 'amcharts/plugins/export/export.min.js'
                );
            } else {

                $jsFiles = array(
                    'jquery-1.11.0.min', 'jquery-ui', 'advanced', 'wysihtml5-0.3.0', 'bootstrap.min', 'jquery.validate.min', 'jquery.tinyscrollbar.min', 'videojs.js',
                    'jquery.jcarousel', 'jquery.cookie', 'bootstrap-wysihtml5', 'jquery.loadmask', 'jquery.tagsinput', 'fancybox/jquery.fancybox.js',
                    'tabs', 'video-js/video.dev.js', 'common', 'script', 'script.video.js', 'plugins_editor.js', 'video-editor.js', 'huddles', 'jquery.jqpagination', 'masonry.pkgd.min',
                    'jquery.timepicker.js', 'slimtable.min.js', 'jquery.overlay.min.js', 'quick_search.js', 'jquery.highlight.js', 'jquery.datetimepicker.js'
                );
            }
        }


        if ($use_local_store) {
            echo $this->Minify->script($jsFiles);
        } else {
            if ($controllerName == 'myfiles') {
                echo '<script type="text/javascript" src="' . $amazon_assets_url . 'static/js/min-huddle-myfiles.js?v=' . $cdn_ver . '"></script>';
            } else {
                echo '<script type="text/javascript" src="' . $amazon_assets_url . 'static/js/min-huddle-general.js?v=' . $cdn_ver . '"></script>';
            }
        }
        ?>
        <?php if ($isAnalytics) { ?>
            <link href="<?php echo $this->webroot; ?>css/virtual_tracker.css" rel="stylesheet" type="text/css">



            <script type="text/javascript" src="<?php echo $this->webroot; ?>amcharts/amcharts.js"></script>
            <script type="text/javascript" src="<?php echo $this->webroot; ?>amcharts/serial.js"></script>
            <script type="text/javascript" src="<?php echo $this->webroot; ?>amcharts/themes/light.js"></script>
            <script type="text/javascript" src="<?php echo $this->webroot; ?>amcharts/plugins/export/export.min.js"></script>
            <!--<script type="text/javascript" src="<?php echo $this->webroot; ?>amcharts/plugins/export/export.css"></script>-->
            <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>amcharts/plugins/export/export.css">
            <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/analytics.css">
        <?php }
        ?>
        <?php if ($controllerName == 'myfiles') : ?>
            <script type="text/javascript" src="<?php echo $this->webroot; ?>js/audio_src/src/recorder.js"></script>
            <script type="text/javascript" src="<?php echo $this->webroot; ?>js/audio_src/src/Fr.voice.js"></script>
            <script type="text/javascript" src="<?php echo $this->webroot; ?>js/audio_src/js/app.js"></script>
            <style type="text/css">

                .videos-list__item {
                    max-height: 254px;
                }
            </style>
            <script>
            var observation_inreview = [];
            var files_mode = '<?php echo $files_mode ?>';

            function create_live_observation_template_for_grid(obj) {

                var video_environment_type = obj.ual.environment_type;
                if (video_environment_type == '1') {

                    var video_text = '<?php echo $alert_messages['you_are_recording_live_from_ios_device_js']; ?>';
                } else if (video_environment_type == '3') {

                    var video_text = '<?php echo $alert_messages['you_are_recording_live_from_android_js']; ?>';
                } else {

                    var video_text = '<?php echo $alert_messages['you_are_recording_live_js']; ?>';

                }
                var html = '<li class="videos-list__item">';
                html += '<div id="spnLiveRecording' + obj.doc.id + '" style="margin-right: 10px;position: absolute;right: 0;left: 10px;top: 13px;color: white;font-weight: bold;font-size: 10px;z-index:999;"><img src="/img/recording_img.png" id="img_recording_live">&nbsp;<?php echo $alert_messages['live_recording_alert']; ?></div>';
                html += '<div class="ac-btn">';
                html += '<a href="/MyFiles/deleteFile/22425/' + obj.doc.id + '" data-confirm="<?php echo $alert_messages['are_you_sure_want_to_del_video']; ?>" data-original-title="Delete" rel="tooltip" data-method="delete" class="btn icon2-trash right smargin-right fl-btn" style="border:none;background: none;border-radius: 0px;padding: 0px;"></a>';

                html += ' </div>';

                html += '<div class="clearfix"></div>';
                html += '<div class="videos-list__item-thumb" style="background: #000;width: 100%;position: relative;">';
                html += '<a id="processing-message-' + obj.doc.id + '" href="/MyFiles/view/1/' + obj.af.account_folder_id + '" title="' + obj.af.name + '"><div class="video-unpublished"><span class="huddles-unpublished ">' + video_text + '</span></div></a>';

                html += '</div>';
                html += '<div class="videos-list__item-aside">';
                html += '<div class="videos-list__item-title">';
                html += '<a class="wrap" id="vide-title-' + obj.doc.id + '" href="/MyFiles/view/1/' + obj.af.account_folder_id + '" title="' + obj.af.name + '">' + obj.af.name + '</a>';
                html += '</div>';
                html += '<div class="videos-list__item-author">By <a href="#" title="' + obj.User.first_name + ' ' + obj.User.last_name + '">' + obj.User.first_name + ' ' + obj.User.last_name + '</a></div>';
                html += '<div class="videos-list__item-added">Uploaded ' + obj.doc.created_date + '<div style="float: right;margin-right: -15px;"></div>';
                html += '<div style="clear: both;"></div></div></div></li>';

                if ($('#processing-message-' + obj.doc.id).length == 0 && obj.doc.doc_type == 3) {
                    observation_inreview.push(obj.doc.id);
                    $('ul.videos-list').prepend(html);
                } else {

                    if (obj.doc.doc_type == 3 && obj.doc.is_processed == 4 && obj.doc.published == 0 && obj.doc.upload_status != 'uploaded' && obj.doc.upload_progress < 100) {
                        $('#spnLiveRecording' + obj.doc.id).html('');
                        if (obj.doc.upload_status == null)
                            obj.doc.upload_status = "initiating";
                        width = obj.doc.upload_progress + '%';
                        title = width + ' ' + obj.doc.upload_status;
                        $('#processing-message-' + obj.doc.id).html('<div id="uploading-box-' + obj.doc.id + '" class="vnb" style="position: absolute;background: #3e7554; z-index: 2;width:' + width + '; height: 146px;text-align: center; color: #fff; padding-top: 65px;opacity:0.5;">&nbsp;</div><span style="width: 110px;color: #fff;position: absolute;left: 58px;top: 40%;text-align: center;text-transform: uppercase;">' + title + '</span>')

                    } else if (obj.doc.doc_type == 3 && obj.doc.is_processed == 4 && obj.doc.published == 0 && (obj.doc.upload_progress == 100 || obj.doc.upload_status == 'uploaded')) {

                        $('#spnLiveRecording' + obj.doc.id).html('');
                        //                         $('#processing-message-' + obj.doc.id).html('<div class="video-unpublished "><span class="huddles-unpublished" id="spnLiveRecording' + obj.doc.id + '" style="">Video is currently uploading. Once your video has successfully uploaded it will take a few minutes to process and sync your notes. You will receive an email when your video is ready to be viewed.</span></div>');
                        $('#processing-message-' + obj.doc.id).html('<div class="video-unpublished "><span class="huddles-unpublished" id="spnLiveRecording' + obj.doc.id + '" style="padding: 15% 17px !important;"><img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;" /><br><?=$alert_messages["Your_video_is_currently_processing"];?>.</span></div>');
                        $('#processing-message-' + obj.doc.id + ' span.huddles-unpublished').addClass('video_custom_message');

                    }

                }
            }

            function create_live_observation_template_for_list(obj) {

                var html = '<tr class="docs-list__item" id="processing-list-container-' + obj.doc.id + '">';
                html += '<td  style="width: 157px;" class="myfiles_video_icon">';
                html += '<label style="float: left;margin-right: 1.5em;height: 5px;"></label>';
                html += '<div class="thumb1" style="float: left;position:relative;">';
                html += '<a id="processing-message-' + obj.doc.id + '" href="/MyFiles/view/1/' + obj.af.account_folder_id + '" title="' + obj.af.name + '" >';
                html += '<div  class="video-unpublished" style="height: 55px;width: 74px;">';
                html += '<span id="spnLiveRecording' + obj.doc.id + '" class="" style="line-height: 14px;font-size: 8px;">';
                html += '<img src="/img/recording_img.png" style="position: absolute;right: 0;left: 2px;top: 8px;z-index:999;">';
                html += '<?php echo $alert_messages['live_recording_short']; ?>';
                html += '</span>';
                html += '</div>';
                html += '</a>';
                html += '</div>';
                html += '</td>';
                html += '<td style="width: 200px;">';
                html += '<div>';
                html += obj.af.name;
                html += '<div style="clear: both;"> </div>';
                html += '</div>';
                html += '<div>';
                html += '<div>';
                html += 'Uploaded By <a href="#" title="' + obj.User.first_name + ' ' + obj.User.last_name + '">' + obj.User.first_name + ' ' + obj.User.last_name + '</a>';
                html += '</div>';
                html += '</td>';
                html += '<td  class="my-workspace-type" style="border: 0;">Video</td>';
                html += '<td  class="my-workspace-date" style="border: 0;">' + obj.doc.created_date + '</td>';
                html += '<td colspan="3" class="copy-table-last-a">&nbsp;&nbsp;</td>';
                html += '</tr>';
                if ($('#processing-message-' + obj.doc.id).length == 0 && obj.doc.doc_type == 3) {
                    console.log('recording....');
                    observation_inreview.push(obj.doc.id);
                    $('table.docs-list__table').prepend(html);
                } else {
                    if (obj.doc.doc_type == 3 && obj.doc.is_processed == 4 && obj.doc.published == 0 && obj.doc.upload_status != 'uploaded' && obj.doc.upload_progress < 100) {
                        width = obj.doc.upload_progress + '%';
                        if (obj.doc.upload_status == null)
                            obj.doc.upload_status = "initiating";
                        $('#spnLiveRecording' + obj.doc.id).css('line-height', '16px');
                        $('#spnLiveRecording' + obj.doc.id).css('background', '#3e7554');
                        $('#spnLiveRecording' + obj.doc.id).css('height', '55px');
                        $('#spnLiveRecording' + obj.doc.id).css('width', width);
                        $('#spnLiveRecording' + obj.doc.id).css('opacity', "0.5");
                        title = width + ' ' + obj.doc.upload_status;
                        $('#spnLiveRecording' + obj.doc.id).html('<div style="font-size: 9px;">&nbsp;</div><span style="width: 74px;color: #fff;position: absolute;left: 0px;top: 32%;text-align: center;text-transform: uppercase;">' + title + '</span>');
                    } else if (obj.doc.doc_type == 3 && obj.doc.is_processed == 4 && obj.doc.published == 0 && obj.doc.upload_status == 'uploaded') {

                        $('#spnLiveRecording' + obj.doc.id).html('');
                        $('#processing-message-' + obj.doc.id).html('<div class="video-unpublished " style="height: 55px;width: 74px;"><span id="spnLiveRecording' + obj.doc.id + '"><img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 18px;height: 18px;min-width: 0px;min-height: 0px;position: absolute;margin: 14px 7px;" />Video is currently processing.</span></div>');
                    }
                }

            }

            (function workspace_live_poll() {

                setTimeout(function () {

                    if ($('#huddle_id').val() == '') {

                        $.ajax({
                            url: home_url + '/MyFiles/getLiveObservations',
                            type: 'post',
                            data: {"observation_inreview": JSON.stringify(observation_inreview)},
                            dataType: 'json'
                        }).success(function (res) {
                            if (res.live_observations != undefined) {
                                for (var i = 0; i < res.live_observations.length; i++) {
                                    var live_observation = res.live_observations[i];
                                    if (files_mode == 'grid') {
                                        create_live_observation_template_for_grid(live_observation);
                                    } else if (files_mode == 'list') {
                                        create_live_observation_template_for_list(live_observation);
                                    } else {
                                        create_live_observation_template_for_grid(live_observation);
                                    }

                                }
                            }
                        }).always(function () {
                            workspace_live_poll();
                        });

                    }

                }, 2000);
            })();

            </script>
        <?php endif; ?>
        <?php if ($controllerName == 'huddles') : ?>
            <script>
                $(document).ready(function () {
                    $('#tab7').click(function ()
                    {
                        $('#tab_hidden').val('7');
                    });
                    $('#tab5').click(function ()
                    {
                        $('#tab_hidden').val('5');
                    });
                    $('#tab1').click(function ()
                    {
                        $('#tab_hidden').val('1');
                    });
                    $('#tab2').click(function ()
                    {
                        $('#tab_hidden').val('2');
                    });
                    $('#tab3').click(function ()
                    {
                        $('#tab_hidden').val('3');
                    });
                    $('#tab4').click(function ()
                    {
                        $('#tab_hidden').val('4');
                    });
                    $('#gObservationStart').click(function ()
                    {
                        $('#tab_hidden').val('10');
                    });
                    setInterval(function ()
                    {
                        var account_id = <?php echo $account_id; ?>;
                        var huddle_id = <?php echo $huddle_id; ?>;
                        var tab = $('#tab_hidden').val();
                        var ob_count = $('#count_ob').val();
                        if (tab == '5')
                        {
                            $.ajax({
                                type: 'post',
                                url: home_url + '/Huddles/observations_load',
                                dataType: 'json',
                                data: {account_id: account_id, huddle_id: huddle_id, ob_count: ob_count},
                                success: function (data)
                                {

                                    if (data.status == '1')
                                    {
                                        doObVideoSearchAjax1();
                                        $('#huddle_ob_video_title_strong').html('Observations (' + data.compare_count + ')');
                                        $('#count_ob_temp').val(data.compare_count);
                                    }
                                }
                            });
                        }

                    }, 2000);//time in milliseconds
                });
            </script>
        <?php endif; ?>
        <?php if (isset($_SESSION['site_id']) && $_SESSION['site_id'] == 2): ?>
            <link rel="shortcut icon" type="image/x-icon" href="/favicon_hmh.ico" />
        <?php else: ?>
            <link rel="shortcut icon" type="image/x-icon" href="/favicon_sibme.ico" />
        <?php endif; ?>
    </head>
    <body <?php if($_SESSION['LANG'] == 'es' ) { echo 'class="language_sp"'; }  ?>>

        <?php if (IS_PROD): ?>

            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KQKHC78"
                              height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>


        <?php endif; ?>




        <!-- <?php echo date('M/d/Y h:i:s', strtotime('now')); ?>-->
        <style>
            .impersonation{
                padding: 5px;
                background: #FF7800;
                color: #fff;
                font-size: 13px;
                font-weight: bold;
                text-align: center;
                z-index: 9;
                border-bottom: solid 1px #4A3A2E;
                position: relative;
            }.impersonation a {
                color:white;
            }

            .container {
                position: relative;
            }
            .breadCrum{
                margin-top: 28px;
            }
            .breadCrum span{
                color: #484848 ;
            }
            .breadCrum a{
                font-weight: normal;
                color: #838383 ;
            }
            .breadCrum a:hover{
                color: #484848;
            }
            .breadCrum a:after{
                content:">";
                margin:0px 12px;
            }

            .breadCrum {
                margin-top: 20px;

                font-size: 13px;
            }
            .breadCrum a:after {
                content: "";
                padding: 0px 12px;
                background: url(/img/breadcrum_arrow.svg) no-repeat center center;
                top: 1px;
                position: relative;
            }
            .breadCrum .container span {
                color: #4d504d;
            }


        </style>
        <?php
        $lst_login = $this->Session->read('last_logged_in_user');
        if (!empty($lst_login['id'])):
            ?>
            <div  class="impersonation"><a href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>">exit impersonation</a></div>
        <?php else: ?>
            <div style="display:none;" class="impersonation"><a href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>">exit impersonation</a></div>
        <?php endif; ?>

        <header id="header">
            <?php echo $this->element('menus/top_menus'); ?>
        </header>
        <div class="breadCrum">
            <div class="container">
<!--            <a href="#">Huddles</a> <a href="#">Coaching Huddles 16-17</a> <a href="#">Andrew Rosenquist</a> <span>Observation 9-1</span>-->
            </div>
        </div>
        <div style="margin: 32px auto 0px;width: 948px;background: red;color: #fff;padding: 10px;display:none;">We are currently experiencing technical difficulties with video processing. If one of your huddles was inactivated after uploading a video, please let us know immediately. Thank you for your patience as we resolve this issue.</div>
        <div id="main" class="box container" style="overflow: visible;">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch("content"); ?>
        </div>

        <?php if (isset($video_huddle_model) && $video_huddle_model != '' && $tab == '1'): ?>
            <?php echo $video_huddle_model; ?>
        <?php endif; ?>

        <script src="//static.filestackapi.com/v3/filestack.js"></script>
        <?php echo $this->element('default_footer'); ?>
        <!-- <?php echo date('M/d/Y h:i:s', strtotime('now')); ?>-->


    </body>
</html>
