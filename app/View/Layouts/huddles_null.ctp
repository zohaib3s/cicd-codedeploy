<!DOCTYPE html>
<html>
    <head>
        <meta name="msapplication-config" content="none"/>
        <meta name="viewport" content="width=1024">
        <?php echo $this->Html->charset(); ?>
        <?php echo $this->Html->meta('icon'); ?>
        <title><?php echo $title_for_layout; ?></title>

        <?php
        $controllerName = strtolower($this->params['controller']);
        $actionName = strtolower($this->params['action']);
        $isAnalytics = false;
        $isMyFilePage = $controllerName == 'MyFiles' && $actionName == 'view';
        $cdn_ver = Configure::read('cdn_ver');
        $cssFiles = array(
            'app', 'sibme', 'alerts', 'bootstrap.min', 'fancybox/jquery.fancybox.css',
            'video-editor.css', 'video-js.css', 'custom.css', 'jquery.timepicker.css', 'jquery.datetimepicker.css'
        );



        if ($controllerName == 'huddles') {
            if ('view' == $actionName) {
                $isAnalytics = true;
            }
        }
        $use_local_store = Configure::read('use_local_file_store');
        $amazon_assets_url = Configure::read('amazon_assets_url');
        if ($use_local_store) {
            echo $this->Minify->css($cssFiles);
        } else {
            if ($isMyFilePage) {
                echo '<link href="' . $amazon_assets_url . 'static/css/min-huddle-myfiles.zip.css?v=' . $cdn_ver . '" media="screen" rel="stylesheet" type="text/css" />';
            } else {
                echo '<link href="' . $amazon_assets_url . 'static/css/min-huddle-general.zip.css?v=' . $cdn_ver . '" media="screen" rel="stylesheet" type="text/css" />';
            }
        }
        ?>
        <link href="https://desk-customers.s3.amazonaws.com/shared/email_widget.css" media="screen" rel="stylesheet" type="text/css" />

        <script type="text/javascript">
            var home_url = '<?php echo $this->base; ?>';
            var supress_app_init = true;
            var VideoEditor = null;
            var bucket_name = "<?php echo Configure::read('bucket_name'); ?>";
            var filepicker_access_key = "<?php echo Configure::read('filepicker_access_key'); ?>";
            var autoPlaySibmePlayer = true;
<?php if (isset($_REQUEST['open_trim']) && $_REQUEST['open_trim'] == '1'): ?>
                autoPlaySibmePlayer = false;
<?php endif; ?>
        </script>

        <?php
        $jsFiles = array(
            'jquery-1.11.0.min', 'jquery-ui', 'advanced', 'wysihtml5-0.3.0', 'bootstrap.min', 'jquery.validate.min', 'jquery.tinyscrollbar.min', 'videojs.js',
            'jquery.jcarousel', 'jquery.cookie', 'bootstrap-wysihtml5', 'jquery.loadmask', 'jquery.tagsinput', 'fancybox/jquery.fancybox.js',
            'tabs', 'video-js/video.dev.js', 'script', 'script.video.js', 'plugins_editor.js', 'video-editor.js', 'huddles', 'jquery.jqpagination', 'masonry.pkgd.min',
            'jquery.timepicker.js', 'slimtable.min.js', 'jquery.overlay.min.js', 'quick_search.js', 'jquery.highlight.js', 'jquery.datetimepicker.js'
        );

        if ($use_local_store) {
            echo $this->Minify->script($jsFiles);
        } else {
            if ($controllerName == 'myfiles') {
                echo '<script type="text/javascript" src="' . $amazon_assets_url . 'static/js/min-huddle-myfiles.js?v=' . $cdn_ver . '"></script>';
            } else {
                echo '<script type="text/javascript" src="' . $amazon_assets_url . 'static/js/min-huddle-general.js?v=' . $cdn_ver . '"></script>';
            }
        }
        ?>
        <?php if ($controllerName == 'myfiles') : ?>
            <script type="text/javascript" src="<?php echo $this->webroot; ?>js/audio_src/src/recorder.js"></script>
            <script type="text/javascript" src="<?php echo $this->webroot; ?>js/audio_src/src/Fr.voice.js"></script>
            <script type="text/javascript" src="<?php echo $this->webroot; ?>js/audio_src/js/app.js"></script>
            <style type="text/css">

                .videos-list__item {
                    max-height: 254px;
                }
            </style>
            <script>
            var observation_inreview = [];
            var files_mode = '<?php echo $files_mode ?>';

            </script>
        <?php endif; ?>

    </head>
    <body <?php if($_SESSION['LANG'] == 'es' ) { echo 'class="language_sp"'; }  ?>>
        <!-- <?php echo date('M/d/Y h:i:s', strtotime('now')); ?>-->
        <style>
            .impersonation{
                padding: 5px;
                background: #FF7800;
                color: #fff;
                font-size: 13px;
                font-weight: bold;
                text-align: center;
                z-index: 9;
                border-bottom: solid 1px #4A3A2E;
                position: relative;
            }.impersonation a {
                color:white;
            }

            .container {
                position: relative;
            }
            .breadCrum{
                margin-top: 28px;
            }
            .breadCrum span{
                color: #484848 ;
            }
            .breadCrum a{
                font-weight: normal;
                color: #838383 ;
            }
            .breadCrum a:hover{
                color: #484848;
            }
            .breadCrum a:after{
                content:">";
                margin:0px 12px;
            }

            .breadCrum {
                margin-top: 20px;

                font-size: 13px;
            }
            .breadCrum a:after {
                content: "";
                padding: 0px 12px;
                background: url(/img/breadcrum_arrow.svg) no-repeat center center;
                top: 1px;
                position: relative;
            }
            .breadCrum .container span {
                color: #4d504d;
            }


        </style>
        <?php
        $lst_login = $this->Session->read('last_logged_in_user');
        if (!empty($lst_login['id'])):
            ?>
        <?php else: ?>
        <?php endif; ?>
        <div id="main" class="box container" style="overflow: visible; padding: 16px;">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch("content"); ?>
        </div>
        <?php if (isset($video_huddle_model) && $video_huddle_model != '' && $tab == '1'): ?>
            <?php echo $video_huddle_model; ?>
        <?php endif; ?>
    </body>
</html>
