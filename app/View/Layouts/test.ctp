<!DOCTYPE html>
<html>
    <head>
        <meta name="msapplication-config" content="none"/>
        <?php echo $this->Html->charset(); ?>
        <?php echo $this->Html->meta('icon'); ?>
        <title><?php echo $title_for_layout; ?></title>

        <?php
        $cssFiles = array(
            'video-js/video-js.css'
        );
        //echo $this->Minify->css($cssFiles);
        echo $this->Html->css('video-js/video-js.css');
        ?>
       

        <?php
        $jsFiles = array(
            'video-js/video.js'
        );

        echo $this->Html->script('video-js/video.js');
        ?>
        <script>
            videojs.options.flash.swf = "js/video-js/video-js.swf";
        </script>
    </head>
    <body <?php if($_SESSION['LANG'] == 'es' ) { echo 'class="language_sp"'; }  ?>>
        
        <div id="main" class="box container" style="overflow: visible;">
            <?php echo $this->fetch("content"); ?>
        </div>

    </body>
</html>
