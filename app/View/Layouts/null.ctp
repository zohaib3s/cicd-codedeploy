<!DOCTYPE html>
<html>
    <meta name="msapplication-config" content="none"/>
    <?php echo $this->element('default_header_old'); ?>

    <body <?php if($_SESSION['LANG'] == 'es' ) { echo 'class="language_sp"'; }  ?>>
        <?php /*if ($this->Session->read('role') && $this->Session->read('role') != '100'): ?>
            <?php if (isset($this->name) && $this->name == "home" && $this->action == 'dashboard'): ?> <div id="tour-overlay1"></div><?php endif; ?>
        <?php endif;*/ ?>

        <div id="main" class="container" >
            <?php
            echo $this->Session->flash();
            ?>
            <?php echo $this->fetch('content'); ?>
        </div>
        <script type = "text/javascript">

            $(document).ready(function() {
                placeholder();

                var form = $('#frmActiviation');
                var DoActivate = function() {
                    if ($("#frmActiviation").valid()) {
                        return true;
                    }
                    return false;
                };

                form.validate({
                    rules: {
                        "data[password]": {
                            required: true,
                            minlength: 6
                        },
                        "data[password_confirmation]": {
                            required: true,
                            minlength: 6,
                            equalTo: "#password"
                        },
                    },
                    messages: {
                        "data[username]" : {
                            required: 'Please add username'
                        },
                        "data[first_name]" : {
                            required: 'Please add first name'
                        },
                        "data[last_name]" : {
                            required: 'Please add last name'
                        },
                        "data[password]" : {
                            required: 'Please create a new password',
                            minlength: "Your password must be at least 6 characters long"
                        },
                        "data[password_confirmation]" : {
                            required: 'Please create a new password',
                            minlength: "Your password must be at least 6 characters long",
                            equalTo: "Password doesn't match"
                        }
                    }
                });

                form.submit(function(){
                    return DoActivate();
                });

                $('.upload-toggle').on('click', function(e) {
                    e.preventDefault();
                    $(this).next().trigger('click');
                }).next().imageLoader({
                    'show': '.upload-preview',
                    'width': '150px'
                });


            });

            function placeholder() {

                if (!('placeholder' in document.createElement('input'))) {

                    $('[placeholder]').focus(function() {
                        var input = $(this);
                        if (input.val() == input.attr('placeholder')) {
                            input.val('');
                            input.removeClass('placeholder');
                        }
                    }).blur(function() {
                        var input = $(this);
                        if (input.val() == '' || input.val() == input.attr('placeholder')) {
                            input.addClass('placeholder');
                            input.val(input.attr('placeholder'));
                        }
                    }).blur();
                    $('[placeholder]').parents('form').submit(function() {
                        $(this).find('[placeholder]').each(function() {
                            var input = $(this);
                            if (input.val() == input.attr('placeholder')) {
                                input.val('');
                            }
                        })
                    });
                }
            }
        </script>
    </body>
</html>
