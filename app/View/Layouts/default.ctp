<!DOCTYPE html>
<html>
<meta name="msapplication-config" content="none"/>
<?php
$users = $this->Session->read('user_current_account');

if ($users && $this->params['controller'] == 'accounts' && $this->params['action'] == 'accountSettings' || $this->params['action'] == 'account_settings' || $this->params['action'] == 'account_settings_main' || $this->params['action'] == 'account_settings_all') {

    echo $this->element('default_header_old');
} else {

    echo $this->element('default_header');
}
?>
<body <?php if($_SESSION['LANG'] == 'es' ) { echo 'class="language_sp"'; }  ?>>
    <?php if(IS_PROD): ?>

    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KQKHC78"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>


    <?php endif; ?>
<style>
    .dialogAdj {
        overflow: inherit !important;
    }

    .dialogAdj #desk-support-box {
        margin: -98px -425px;
    }

    .error {
        color: #be1616 !important;
        background-color: #fff2f2 !important;
        border-color: #d93f3f !important;
    }

    .impersonation {
        padding: 5px;
        background: #FF7800;
        color: #fff;
        font-size: 13px;
        font-weight: bold;
        text-align: center;
        z-index: 9;
        border-bottom: solid 1px #4A3A2E;
        position: relative;
    }

    .impersonation a {
        color: white;
    }

    .container {
        position: relative;
    }


    .breadCrum span {
        color: #484848;
    }

    .breadCrum a {
        font-weight: normal;
        color: #838383;
    }

    .breadCrum a:hover {
        color: #484848;
    }

    .breadCrum a:after {
        content: ">";
        margin: 0px 12px;
    }

    .breadCrum {
        margin-top: 20px;
        font-size: 13px;
    }

    .breadCrum a:after {
        content: "";
        padding: 0px 12px;
        background: url(/img/breadcrum_arrow.svg) no-repeat center center;
        top: 1px;
        position: relative;
    }

    .breadCrum .container span {
        color: #4d504d;
    }

    .settings_body {
        margin-bottom: 10px;
        margin-top: 5px;
    }

    .cd-tabs-navigation a {
        width: 154px;
    }
    .modal-header .close {
        margin: -1rem -2px -1rem auto;
    }
    .modal-footer button{
        box-shadow: none;
        text-shadow: none;
        font-size:14px;
    }
    .modal-title {
        font-weight: 600;
    }
    .modal-footer button a {
        font-weight: normal;
        color:#fff;
        text-decoration: none;
    }
</style>
<?php
$lst_login = $this->Session->read('last_logged_in_user');
if (!empty($lst_login['id'])):
    ?>
    <div class="impersonation"><a href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>">exit
            impersonation</a></div>
<?php else: ?>
    <div style="display:none;" class="impersonation"><a
                href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>">exit impersonation</a>
    </div>
<?php endif; ?>

    <?php
    $action = $this->params['action'];
    if ($users && !in_array($action, array('login', 'forgot_password', 'change_password'))) {
    ?>
    <header id="header">
        <?php echo $this->element('menus/top_menus'); ?>
    </header>
    <?php } ?>
<div class="breadCrum">
    <div class="container">
        <!--            <a href="#">Huddles</a> <a href="#">Coaching Huddles 16-17</a> <a href="#">Andrew Rosenquist</a> <span>Observation 9-1</span>-->
    </div>
</div>
<div style="margin: 32px auto 0px;width: 948px;background: red;color: #fff;padding: 10px;display:none;">We are currently
    experiencing technical difficulties with video processing. If one of your huddles was inactivated after uploading a
    video, please let us know immediately. Thank you for your patience as we resolve this issue.
</div>
<?php
$mainClass = '';
if ($users && $this->name == "Dashboard" && ($this->action == 'home' || $this->action == 'coach_tracker' || $this->action == 'assessment_tracker')) {
    $mainClass .= ' box';
} elseif ($this->name == 'Users' && $this->action == 'login') {
    $mainClass .= ' login-main';
} elseif ($this->name == 'Users' && ($this->action == 'forgot_password' || $this->action == 'change_password')) {
    $mainClass .= ' forgot-password';
} elseif ($this->name == 'Huddles' && ($this->action == 'create' || $this->action == 'edit')) {
    $mainClass .= ' dialogAdj';
}
?>

<div id="main" class="container<?php echo $mainClass; ?>">

    <?php
    echo $this->Session->flash();
    ?>

    <?php
    if ($users && isset($head) && $head != ''):
        echo $head;
    endif;
    ?>

    <?php echo $this->fetch('content'); ?>
</div>
<?php if ($users && isset($video_huddle_model) && $video_huddle_model != ''): ?>
    <?php echo $video_huddle_model; ?>
<?php endif; ?>

<script src="//static.filestackapi.com/v3/filestack.js"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>
<script type="text/javascript">
    $(".dropdown-menu a").click(function () {
        var selText = $(this).text();
        $(this).parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
    });
</script>
<?php echo $this->element('default_footer'); ?>
</body>
</html>
