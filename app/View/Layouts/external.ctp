<!DOCTYPE html>
<html>
    <head>
        <meta name="msapplication-config" content="none"/>
        <?php echo $this->Html->charset(); ?>
        <?php echo $this->Html->meta('icon'); ?>
        <title><?php echo $title_for_layout; ?></title>

        <?php
        $controllerName = strtolower($this->params['controller']);
        $actionName = strtolower($this->params['action']);
        $isMyFilePage = $controllerName == 'MyFiles' && $actionName == 'view';
        if ($isMyFilePage) {//video-editor.css,
            $cssFiles = array(
                'app', 'sibme', 'alerts', 'bootstrap.min', 'fancybox/jquery.fancybox.css',
                'video-editor.css', 'video-js.css', 'jquery.nouislider.css', 'custom.css', 'jquery.timepicker.css'
            );
        } else {
            $cssFiles = array(
                'app', 'sibme', 'alerts', 'bootstrap.min', 'fancybox/jquery.fancybox.css',
                'video-editor.css', 'video-js.css', 'custom.css', 'jquery.timepicker.css', 'jquery.datetimepicker.css',
            );
        }
        $use_local_store = Configure::read('use_local_file_store');
        $amazon_assets_url = Configure::read('amazon_assets_url');
        if ($use_local_store) {
            echo $this->Minify->css($cssFiles);
        } else {
            if ($isMyFilePage) {
                echo '<link href="' . $amazon_assets_url . 'static/css/min-huddle-myfiles.zip.css" media="screen" rel="stylesheet" type="text/css" />';
            } else {
                echo '<link href="' . $amazon_assets_url . 'static/css/min-huddle-general.zip.css" media="screen" rel="stylesheet" type="text/css" />';
            }
        }
        ?>
        <link href="https://desk-customers.s3.amazonaws.com/shared/email_widget.css" media="screen" rel="stylesheet" type="text/css" />
        
        <script type="text/javascript">
            var home_url = '<?php echo $this->base; ?>';
            var supress_app_init = true;
            var VideoEditor = null;
            var bucket_name = "<?php echo Configure::read('bucket_name'); ?>";
            var filepicker_access_key = "<?php echo Configure::read('filepicker_access_key'); ?>";
            var autoPlaySibmePlayer = true;
            <?php if (isset($_REQUEST['open_trim']) && $_REQUEST['open_trim'] == '1'): ?>
                            autoPlaySibmePlayer = false;
            <?php endif; ?>
        </script>

        <?php
//        if ($controllerName == 'myfiles') {
//            $jsFiles = array(
//                'jquery-1.11.0.min', 'advanced', 'wysihtml5-0.3.0', 'bootstrap.min', 'jquery.validate.min', 'jquery.tinyscrollbar.min', 'videojs.js',
//                'jquery.jcarousel', 'jquery.cookie', 'bootstrap-wysihtml5', 'jquery.loadmask', 'jquery.tagsinput', 'fancybox/jquery.fancybox.js',
//                'tabs', 'video-js/video.dev.js', 'common', 'script', 'script.video.js', 'plugins_editor.js', 'video-editor.js', 'myfiles', 'jquery.jqpagination', 'masonry.pkgd.min',
//                'jquery.timepicker.js', 'slimtable.min.js', 'jquery.overlay.min.js', 'jquery.highlight.js'
//            );
//        } else {
            $jsFiles = array(
                'jquery-1.11.0.min', 'advanced', 'wysihtml5-0.3.0', 'bootstrap.min', 'jquery.validate.min', 'jquery.tinyscrollbar.min', 'videojs.js',
                'jquery.jcarousel', 'jquery.cookie', 'bootstrap-wysihtml5', 'jquery.loadmask', 'jquery.tagsinput', 'fancybox/jquery.fancybox.js',
                'tabs', 'video-js/video.dev.js', 'common', 'script', 'script.video.js', 'plugins_editor.js', 'video-editor.js', 'jquery.jqpagination', 'masonry.pkgd.min',
                'jquery.timepicker.js', 'slimtable.min.js', 'jquery.overlay.min.js', 'quick_search.js', 'jquery.highlight.js', 'jquery.datetimepicker.js'
            );
//        }

//        if ($use_local_store) {
            echo $this->Minify->script($jsFiles);
//        } else {
//            if ($controllerName == 'myfiles') {
//                echo '<script type="text/javascript" src="' . $amazon_assets_url . 'static/js/min-huddle-myfiles.js"></script>';
//            } else {
//                echo '<script type="text/javascript" src="' . $amazon_assets_url . 'static/js/min-huddle-general.js"></script>';
//            }
//        }
        ?>
    </head>
    <?php 
    $logo_image = $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/new/logo-3.png');
    $logo_path = $this->Custom->getSecureSibmecdnImageUrl("static/companies/" . $user_current_account['Account']['id'] . "/" . $user_current_account['Account']['image_logo'], $user_current_account['Account']['image_logo']);

if (!empty($user_current_account['Account']['image_logo'])) {
    $logo_image = $logo_path;
}

    ?>
    <body <?php if($_SESSION['LANG'] == 'es' ) { echo 'class="language_sp"'; }  ?>>
        <?php        
$lst_login = $this->Session->read('last_logged_in_user');  
if (!empty($lst_login['id'])):
        ?>
<div  class="impersonation"><a href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>">exit impersonation</a></div>
<?php else: ?>
<div style="display:none;" class="impersonation"><a href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>">exit impersonation</a></div>
<?php endif; ?>
        <!-- <?php //echo date('M/d/Y h:i:s',strtotime('now'));   ?>-->
        <header id="header" style="height: 106px;">
            <style type="text/css">
                
                    .impersonation{
                        padding: 5px;
                    background: #FF7800;
                    color: #fff;
                    font-size: 13px;
                    font-weight: bold;
                    text-align: center;
                    z-index: 9;
                    border-bottom: solid 1px #4A3A2E;
                    position: relative;
                    }.impersonation a {
                color:white;
            }
                
                #logo a {
                    width: 174px;
                    height: 84px;
                    background: url("<?php echo $logo_image; ?>") no-repeat center center;
                    display: block;
                    top: 0;
                    background-size: 135px;
                }

                #header {
                    background-color: #336699;
                    background-image: -moz-linear-gradient(top, rgba(255, 255, 255, 0.07) 0%, rgba(0, 0, 0, 0.07) 100%);
                    background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(255, 255, 255, 0.07)), color-stop(100%, rgba(0, 0, 0, 0.07)));
                    background-image: -webkit-linear-gradient(top, rgba(255, 255, 255, 0.07) 0%, rgba(0, 0, 0, 0.07) 100%);
                    background-image: -o-linear-gradient(top, rgba(255, 255, 255, 0.07) 0%, rgba(0, 0, 0, 0.07) 100%);
                    background-image: -ms-linear-gradient(top, rgba(255, 255, 255, 0.07) 0%, rgba(0, 0, 0, 0.07) 100%);
                    background-image: linear-gradient(to bottom, rgba(255, 255, 255, 0.07) 0%, rgba(0, 0, 0, 0.07) 100%);
                }

                #user .dropdown-content a:hover {
                    background: #4383b4;
                    text-shadow: none;
                    color: #fff;
                }

                .nav-header a {
                    display: block;
                    padding: 0 22px;
                    background: #668dab;
                    color: #fff;
                    text-shadow: 0 1px 0 rgba(0, 0, 0, .2);
                    font-weight: normal;
                    font-size: 15px;
                }
                .span-wrap{
                    width: 107px;
                    text-align: left;
                    margin-left: 0px;
                }
                .ph{
                    float: left;
                }
                .ext_logo {
                    float: left;
                    margin-left: 16px;
                    margin-top: 15px;
                }

            </style>
            <div class="container">              
                <div id="logo">
                    <a href=""></a>
                </div>



            </div>

            <style type="text/css">
                .inactive-tab{
                    background-color: #ccc !important;
                }
            </style>
            <script type="text/javascript">
                //var current_user_login = 'ksaleem';
                //var current_user_id = '621';
            </script>       
        </header>
        <div style="margin: 32px auto 0px;width: 948px;background: red;color: #fff;padding: 10px;display:none;">We are currently experiencing technical difficulties with video processing. If one of your huddles was inactivated after uploading a video, please let us know immediately. Thank you for your patience as we resolve this issue.</div>
        <div id="main" class="box container" style="overflow: visible;">
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch("content"); ?>
        </div>


        <script type="text/javascript" src="//api.filepicker.io/v2/filepicker.js">
        <?php echo $this->element('default_footer'); ?>
        <!-- <?php echo date('M/d/Y h:i:s', strtotime('now')); ?>-->
    </body>
</html>
