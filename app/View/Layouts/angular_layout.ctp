<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Sibme</title>
        <base href="/angular_resources/header_resources/">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/x-icon" href="<?php echo $this->Custom->get_site_settings('faveIcon'); ?>">

    </head>
    <body <?php if($_SESSION['LANG'] == 'es' ) { echo 'language_sp'; }  ?>>
        <script type="text/javascript">


            window['header_data'] = <?php echo json_encode($settings); ?>;
        </script>
    <app-root></app-root>
    <script type="text/javascript" src="inline.bundle.js"></script><script type="text/javascript" src="polyfills.bundle.js"></script><script type="text/javascript" src="styles.bundle.js"></script><script type="text/javascript" src="vendor.bundle.js"></script><script type="text/javascript" src="main.bundle.js"></script></body>
</html>

