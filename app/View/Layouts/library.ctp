<!DOCTYPE html>
<html>
    <meta name="msapplication-config" content="none"/>
    <?php echo $this->element('default_header'); ?>
    <body <?php if($_SESSION['LANG'] == 'es' ) { echo 'class="language_sp"'; }  ?>>
        
        
        <?php if(IS_PROD): ?>
    
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KQKHC78"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    
    
<?php endif; ?>
        
        
        
        <?php /*if ($this->Session->read('role') && $this->Session->read('role') != '100'): ?>
            <?php if (isset($this->name) && $this->name == "home" && $this->action == 'dashboard'): ?> <div id="tour-overlay1"></div><?php endif; ?>
        <?php endif;*/ ?>
           <style>
                  .impersonation{
        padding: 5px;
    background: #FF7800;
    color: #fff;
    font-size: 13px;
    font-weight: bold;
    text-align: center;
    z-index: 9;
    border-bottom: solid 1px #4A3A2E;
    position: relative;
    }.impersonation a {
                color:white;
            }
            
                .container {
    position: relative;
}
.breadCrum{
    margin-top: 28px;
}
.breadCrum span{
    color: #484848 ;
}
.breadCrum a{
    font-weight: normal;
    color: #838383 ;
}
.breadCrum a:hover{
    color: #484848;
}
.breadCrum a:after{
    content:">";
    margin:0px 12px;
}

            .breadCrum {
                margin-top: 20px;
           
                font-size: 13px;
            }
            .breadCrum a:after {
                content: "";
                padding: 0px 12px;
                background: url(/img/breadcrum_arrow.svg) no-repeat center center;
                top: 1px;
                position: relative;
            }
            .breadCrum .container span {
                color: #4d504d;
            }
            
            
        </style>
<?php        
$lst_login = $this->Session->read('last_logged_in_user');  
if (!empty($lst_login['id'])):
        ?>
<div  class="impersonation"><a href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>">exit impersonation</a></div>
<?php else: ?>
<div style="display:none;" class="impersonation"><a href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>">exit impersonation</a></div>
<?php endif; ?>
        <header id="header">
            <?php
            if ($this->params['action'] != 'login') {
                echo $this->element('menus/top_menus');
            }
            ?>
        </header>
        <div class="breadCrum">
            <div class="container">
<!--            <a href="#">Huddles</a> <a href="#">Coaching Huddles 16-17</a> <a href="#">Andrew Rosenquist</a> <span>Observation 9-1</span>-->
            </div>
        </div>
        <div style="margin: 32px auto 0px;width: 948px;background: red;color: #fff;padding: 10px;display:none;">We are currently experiencing technical difficulties with video processing. If one of your huddles was inactivated after uploading a video, please let us know immediately. Thank you for your patience as we resolve this issue.</div>
        <div id="main" class="container" style="overflow: visible;">
            <?php
            if (isset($head) && $head != ''):
                echo $head;
            endif;
            ?>
            <?php
            echo $this->Session->flash();
            ?>

            <?php echo $this->fetch('content'); ?>
        </div>
        <?php if (isset($video_huddle_model) && $video_huddle_model != ''): ?>
            <?php echo $video_huddle_model; ?>
        <?php endif; ?>
        
        <script src="//static.filestackapi.com/v3/filestack.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Droid+Serif' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <?php echo $this->element('default_footer'); ?>
    </body>

</html>
