<!DOCTYPE html>
<html>
    <head>
        <meta name="msapplication-config" content="none"/>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo $title_for_layout; ?>
        </title>

        <script type="text/javascript">
            var home_url = '<?php echo $this->base; ?>';
            var supress_app_init = true;
            var VideoEditor = null;
            var bucket_name = "<?php echo Configure::read('bucket_name');?>";
        </script>

        <?php
            echo $this->Minify->css(array('app', 'sibme', 'alerts', 'bootstrap.min', 'fancybox/jquery.fancybox.css', 'video-editor.css','video-js.css', 'jquery.nouislider.css'));
        ?>
        <?php
            echo $this->Minify->script(array('wysihtml5-0.3.0', 'jquery-1.11.0.min', 'bootstrap.min', 'jquery.validate.min', 'tabs', 'video-js/video.dev.js', 'jquery.tinyscrollbar.min', 'jquery.jcarousel', 'bootstrap-wysihtml5', 'jquery.loadmask', 'jquery.tagsinput', 'fancybox/jquery.fancybox.js', 'common', 'plugins_editor.js' ,'videojs.js','video-editor.js'));
        ?>

        <?php
            echo $this->Html->meta('icon');
        ?>
    </head>
<body <?php if($_SESSION['LANG'] == 'es' ) { echo 'class="language_sp"'; }  ?>>
    <div id="main" class="box container" style="overflow: visible; margin-top:0px;">
        <?php echo $this->Session->flash(); ?>
        <?php echo $this->fetch("content"); ?>
    </div>
</body>
</html>
