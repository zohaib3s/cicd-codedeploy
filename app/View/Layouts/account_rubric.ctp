<!DOCTYPE html>
<html>
<meta name="msapplication-config" content="none"/>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">

<?php
$users = $this->Session->read('user_current_account');
?>
<head>
    <?php echo $this->Html->charset(); ?>
    <?php echo $this->Html->meta('icon'); ?>
    <title> <?php echo $title_for_layout; ?></title>

    <?php
    $use_local_store = Configure::read('use_local_file_store');
    $amazon_assets_url = Configure::read('amazon_assets_url');

        if ($use_local_store) {
            echo $this->Minify->css(array('app', 'sibme', 'style-extention', 'pro_dropdown_2', 'jquery.loadmask.css', 'alerts', 'bootstrap.min'));
        } else {
            echo '<link href="' . $amazon_assets_url . 'static/css/min-accounts-general.zip.css" media="screen" rel="stylesheet" type="text/css" />';
        }

    ?>

    <script type="text/javascript">
        var home_url = '<?php echo $this->base; ?>';
        var supress_app_init = false;
        var bucket_name = "<?php echo Configure::read('bucket_name');?>";
    </script>

    <?php
    $js_scripts = array(
       'v4/jquery-3.2.1.slim.min', 'v4/bootstrap.min','jquery-2.1.1','v4/editor'
    );

    if ($use_local_store) {
        echo $this->Html->script($js_scripts);
    } else {
        echo '<script type="text/javascript" src="' . $amazon_assets_url . 'static/js/min-accounts-general.js"></script>';
    }

    ?>

    <link href="https://desk-customers.s3.amazonaws.com/shared/email_widget.css" media="screen" rel="stylesheet"  type="text/css"/>
</head>
?>
<body <?php if($_SESSION['LANG'] == 'es' ) { echo 'class="language_sp"'; }  ?>>
 
    <?php if(IS_PROD): ?>
    
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KQKHC78"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    
    
<?php endif; ?>
    
    
<style>
    .dialogAdj {
        overflow: inherit !important;
    }

    .dialogAdj #desk-support-box {
        margin: -98px -425px;
    }

    .error {
        color: #be1616 !important;
        background-color: #fff2f2 !important;
        border-color: #d93f3f !important;
    }

    .impersonation {
        padding: 5px;
        background: #FF7800;
        color: #fff;
        font-size: 13px;
        font-weight: bold;
        text-align: center;
        z-index: 9;
        border-bottom: solid 1px #4A3A2E;
        position: relative;
    }

    .impersonation a {
        color: white;
    }
</style>

<?php
$lst_login = $this->Session->read('last_logged_in_user');
if (!empty($lst_login['id'])):
    ?>
    <div class="impersonation"><a href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>">exit
            impersonation</a></div>
<?php else: ?>
    <div style="display:none;" class="impersonation"><a
                href="<?php echo $this->webroot . 'app/impersonate_back/' . $lst_login['id']; ?>">exit impersonation</a>
    </div>
<?php endif; ?>


<header id="header">
    <?php
    $action = $this->params['action'];
    if ($users && !in_array($action, array('login', 'forgot_password', 'change_password'))) {
        echo $this->element('menus/top_menus');
    }
    ?>
</header>
<div style="margin: 32px auto 0px;width: 948px;background: red;color: #fff;padding: 10px;display:none;">We are currently
    experiencing technical difficulties with video processing. If one of your huddles was inactivated after uploading a
    video, please let us know immediately. Thank you for your patience as we resolve this issue.
</div>

<div id="main" class="container">

    <?php
    if ($users && isset($head) && $head != ''):
        echo $head;
    endif;
    ?>

    <?php echo $this->fetch('content'); ?>
</div>
<?php if ($users && isset($video_huddle_model) && $video_huddle_model != ''): ?>
    <?php echo $video_huddle_model; ?>
<?php endif; ?>
<?php echo $this->element('default_footer'); ?>
</body>
</html>
