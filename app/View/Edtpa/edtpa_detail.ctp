<!doctype html>
<?php
$user = $this->Session->read('user_current_account');
$user_id = $user['User']['id'];
$account_id = $user['accounts']['account_id'];
$edtpa_lang = $this->Session->read('edtpa_lang');

function formate_date_lang($date_str) {
    if ($_SESSION['LANG'] == 'es') {
        if (strpos($date_str, 'Jan')) {
            return str_replace('Jan', 'Ene', $date_str);
        } elseif (strpos($date_str, 'Apr')) {
            return str_replace('Apr', 'Abr', $date_str);
        } elseif (strpos($date_str, 'Aug')) {
            return str_replace('Aug', 'Ago', $date_str);
        } elseif (strpos($date_str, 'Dec')) {
            return str_replace('Dec', 'Dic', $date_str);
        } else {
            return $date_str;
        }
    } else {
        return $date_str;
    }
}

//echo "<pre>";
//print_r($edtpa_lang);
//echo "</pre>";
?>
<input id="account_id" type="hidden" value="<?php echo $account_id ?>">
<input id="user_id" type="hidden" value="<?php echo $user_id ?>">
<input id="huddle_id" type="hidden" value="">
<input id="video_id" type="hidden" value="0">
<input id="edtpa_assessment_id" type="hidden" value="<?php echo $edtpa_assessment_id ?>"/>
<input id="txtUploadedFilePath" type="hidden" value=""/>
<input id="txtUploadedFileName" type="hidden" value=""/>
<input id="txtUploadedFileMimeType" type="hidden" value=""/>
<input id="txtUploadedFileSize" type="hidden" value=""/>
<input id="txtUploadedDocType" type="hidden" value="video"/>
<input id="txtUploadedUrl" type="hidden" value=""/>
<input id="uploadPath" type="hidden" value="<?php echo "/tempupload/$account_id/" . date('Y/m/d/'); ?>"/>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

<script type="text/javascript">
    var nodes_data =<?php echo json_encode($assessment_portfolio_tasks); ?>;
    $(document).ready(function () {
        $('.arrow-upe').hide();
        $('.arrow-downe').show();
        $("#huddlesOuter").find("div.arrow-upe").hide();
        $(document).on('click', '.edtp-heading', function () {
            $(this).next(".edtp-content").slideToggle(500);
            $(this).find(".arrow-upe, .arrow-downe").toggle();
        });

    });

    function loadNodeData(index) {
        $('.edt_tb_placeholder').hide();
        $('#current_node').val(nodes_data[index].node_id);
        $('#current_node_index').val(index);
        $("#spnTaskName").html(nodes_data[index].short_name);
        $("#spnTaskDesc").html(nodes_data[index].long_name);
        $("#spminvideos").html(nodes_data[index].min_number_of_files);
        $("#spmaxvideos").html(nodes_data[index].max_number_of_files);

        //Workspace Area Content
        $("#dvWorkSpeace").html('<p><?php $edtpa_lang['edtpa_videos_documents'] ?></p>');
        $('#dvHuddle').html('');

        var workspace_html = "";
        var huddles_html = "";
        var documents_array = nodes_data[index].documents;
        //console.log(documents_array);

        if (documents_array.length > 0) {
            $("#dvHuddle").html('');
            $('#dvWorkSpeace').html('');
            var workspace_videos = "";
            var workspace_docs = "";
            var huddles_videos = "";
            var huddles_docs = "";
            $.each(documents_array, function (index, value) {
                if (value.document_title && value.document_title != null) {
                    var title = value.document_title;
                    var chars = title.length;
                    var limited_title;
                    if (chars > 25) {
                        limited_title = title.substring(0, 30) + "...";
                    } else {
                        limited_title = value.document_title;
                    }
                    if (value.folder_type == '3' || value.folder_type == 3) {
//                    alert(value.folder_type + ' ' + value.doc_type);
                        if (value.doc_type == '2' || value.doc_type == 2) {
                            workspace_docs += '<div class="adt_thumb2 1" style="position: relative;">';
                            workspace_docs += '<div style="position: absolute;right: -10px;top: -14px;"><img src="/app/img/edtpa/cross_img.png" data-index="' + index + '" class="removeItem" style="width: auto;"></div>';
                            workspace_docs += '<div class=" adt_video2"><a href=' + value.redirect_url + '  target="_blank"><span class="' + value.thumbnail + '">&nbsp;&nbsp;&nbsp;&nbsp;</span></a></div>';
                            workspace_docs += '<div class="adt_desc2"><a href=' + value.redirect_url + '  target="_blank"><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?> <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p></div>';
                            workspace_docs += '<div class="clear"></div></div>';
                        } else if (value.doc_type == '1' || value.doc_type == 1) {
                            workspace_videos += '<div class="adt_thumb2 21" style="position: relative;">';
                            workspace_videos += '<div style="position: absolute;right: -10px;top: -14px;"><img src="/app/img/edtpa/cross_img.png" data-index="' + index + '" class="removeItem" style="width: auto;"></div>';
                            workspace_videos += '<div class="adt_video3"><a href=' + value.redirect_url + '  target="_blank"><img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                            workspace_videos += '<div class="adt_desc2"><a href=' + value.redirect_url + '  target="_blank"><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?> <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p> </div>';
                            workspace_videos += '<div class="clear"></div></div>';

                        } else if (value.doc_type == '3' || value.doc_type == 3) {
                            workspace_videos += '<div class="adt_thumb2 3" style="position: relative;">';
                            workspace_videos += '<div style="position: absolute;right: -10px;top: -14px;"><img src="/app/img/edtpa/cross_img.png" data-index="' + index + '" class="removeItem" style="width: auto;"></div>';
                            workspace_videos += '<div class="adt_video3"><a href=' + value.redirect_url + '  target="_blank"><img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                            workspace_videos += '<div class="adt_desc2"><a href=' + value.redirect_url + '  target="_blank"><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?> <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?>' + value.document_date + '</span></p></div>';
                            workspace_videos += '<div class="clear"></div></div>';
                        }
                    } else if (value.folder_type == '1' || value.folder_type == 1) {
//                    alert(value.folder_type + '' + value.doc_type);
                        if (value.doc_type == '2' || value.doc_type == 2) {
                            huddles_docs += '<div class="adt_thumb2 4" style="position: relative;">';
                            huddles_docs += '<div style="position: absolute;right: -10px;top: -14px;"><img src="/app/img/edtpa/cross_img.png" data-index="' + index + '" class="removeItem" style="width: auto;"></div>';
                            huddles_docs += '<div class="adt_video2"><a href=' + value.redirect_url + '  target="_blank"><span class="' + value.thumbnail + '">&nbsp;&nbsp;&nbsp;&nbsp;</span></a></div>';
                            huddles_docs += '<div class="adt_desc2"><a href=' + value.redirect_url + '  target="_blank"><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?> <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p></div>';
                            huddles_docs += '<div class="clear"></div></div>';
                        } else if (value.doc_type == '1' || value.doc_type == 1) {
                            huddles_videos += '<div class="adt_thumb2 5" style="position: relative;">\n\
                                           <div style="position: absolute;right: -10px;top: -14px;">\n\
                                           <img src="/app/img/edtpa/cross_img.png" data-index="' + index + '" class="removeItem" style="width: auto;">\n\
                                           </div>';
                            huddles_videos += '<div class="adt_video3 6"><a  target="_blank" href=' + value.redirect_url + '><img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                            huddles_videos += '<div class="adt_desc2"><a  target="_blank" href=' + value.redirect_url + '><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?> <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p></div>';
                            huddles_videos += '<div class="clear"></div></div>';
                        } else if (value.doc_type == '3' || value.doc_type == 3) {
                            huddles_videos += '<a target="_blank"  href=' + value.redirect_url + '><div class="adt_thumb3 7" style="position: relative;">';
                            huddles_videos += '<div style="position: absolute;right: -10px;top: -14px;"><img src="/app/img/edtpa/cross_img.png" data-index="' + index + '" class="removeItem" style="width: auto;"></div><div class="adt_video2">';
                            huddles_videos += '<a  target="_blank" href=' + value.redirect_url + '><img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                            huddles_videos += '<div class="adt_desc2"><a  target="_blank" href=' + value.redirect_url + '><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?> <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p></div>';
                            huddles_videos += '<div class="clear"></div></div>';
                        }
                    }

                }
            });
            workspace_html = workspace_videos + workspace_docs + huddles_videos + huddles_docs;
            // huddles_html = huddles_videos + huddles_docs;
            if (workspace_html !== "") {
                $("#dvWorkSpeace").html(workspace_html);
            }
            // if (huddles_html !== "") {
            // $("#dvWorkSpeace").html(huddles_html);
            //}
        }

        $("#btnVideo").hide();
        $(".resources_dropDown").hide();
//        $('.resources_dropDown').prop('onclick', null).off('click');
        $('#btnVideo').prop('onclick', null).off('click');
        var doc_type = nodes_data[index].evidence_type_code;
        var minfile = nodes_data[index].min_number_of_files;
        var maxfile = nodes_data[index].max_number_of_files;
        //console.log(nodes_data[index]);
        $('#required-attachment').html('<h6><?php echo $edtpa_lang['edtpa_required_attachment'] ?>: ' + maxfile + '</h6>');
        //alert(doc_type);
        if (doc_type == 'DOCUMENT') {
            html_des = '<ol>';
            html_des += '<li> <span style="color:red;"><?php echo $edtpa_lang['edtpa_recommended_file_type'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_resource_click'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_add_resources'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_the_resources_like'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_the_resources_selected_will'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_upload_computer'] ?></li>';
            html_des += '</ol>';
            $('#edtpa_instructions').html(html_des);
            $(".resources_dropDown").show();
        } else if (doc_type == 'VIDEO') {
            html_des = '<ol>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_click_on_the_video'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_dialog_box'] ?></li>';
            //html_des += '<li>You can select a minimum of ' + minfile + ' video(s) and a maximum of ' + maxfile + ' video(s) from your Workspace and Huddles.</li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_video'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_video_you_selected'] ?></li>';
            html_des += '</ol>';
            $('#edtpa_instructions').html(html_des);
            $("#btnVideo").show();
        } else {
            html_des = '<div><?php echo $edtpa_lang['edtpa_video_you_selected'] ?></div>';
            html_des += '<ol>';
            html_des += '<li><span style="color:red;"><?php echo $edtpa_lang['edtpa_recommended_file_type'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_resource_click'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_add_resources'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_the_resources_like'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_the_resources_selected_will'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_upload_computer'] ?></li>';
            html_des += '</ol>';
            html_des += '<div><?php echo $edtpa_lang['edtpa_uploading_videos'] ?></div>';
            html_des += '<ol>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_click_on_the_video'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_dialog_box'] ?></li>';
            // html_des += '<li>You can select a minimum of minimum of ' + minfile + ' video(s) and a maximum of ' + maxfile + ' video(s) from your Workspace and Huddles.</li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_video'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_video_you_selected'] ?></li>';
            html_des += '</ol>';
            $('#edtpa_instructions').html(html_des);
            $("#btnVideo").show();
            $(".resources_dropDown").show();
        }
        var i, x, tablinks;
        x = document.getElementsByClassName("tab_edt");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < x.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
        }
        document.getElementById('tskDetail').style.display = "block";
        var selected_elements_arr = nodes_data[index].documents_arr;
        $('.select_checkbox').each(function (ind, val) {
            $(this).prop('checked', false);
        });
        if (typeof (selected_elements_arr) != 'undefined' && selected_elements_arr != null && selected_elements_arr.length > 0) {
            $('.select_checkbox').each(function (ind, val) {
                if ($.inArray($(this).val(), selected_elements_arr) !== -1) {
                    $(this).prop('checked', true);
                }
            });
        }
    }

    $(document).ready(function () {
        $(document).on('click', '.select_checkbox', function () {
            var doc_type = $(this).data('doc_type');
            var doc_extension = $(this).data('document_extension');
            var allowed_extensions;
            var error_msg;
            if (doc_type == '2' || doc_type == 2) {
                allowed_extensions = ['doc', 'docx', 'odt', 'pdf'];
                error_msg = '<?php echo  $edtpa_lang['edtpa_invalid_file_type_for_resources']?>';
            } else if (doc_type == '1' || doc_type == 1) {
                allowed_extensions = ['flv', 'asf', 'qt', 'mov', 'mpg', 'mpeg', 'avi', 'wmv', 'mp4', 'm4v'];
                error_msg = '<?php echo $edtpa_lang['edtpa_invalid_file_type_for_videos']?>';
            } else if (doc_type == '3' || doc_type == 3) {

                allowed_extensions = ['flv', 'asf', 'qt', 'mov', 'mpg', 'mpeg', 'avi', 'wmv', 'mp4', 'm4v'];
                error_msg = '<?php echo  $edtpa_lang['edtpa_invalid_file_type_for_videos']?>';
            }
            if ($(this).is(':checked')) {
                var check_its_extension = validateFileType(doc_extension, allowed_extensions);
                if (check_its_extension === false) {
                    alert(error_msg);
                    $(this).prop("checked", false);
                }
            }
            var values = $('input:checkbox:checked.select_checkbox').map(function () {
                return this.value;
            }).get();
            var current_node_index = $('#current_node_index').val();
            var current_node_details = nodes_data[current_node_index];
            var maxfile = current_node_details.max_number_of_files;
            if (values.length > maxfile) {
                alert("<?php echo $edtpa_lang['edtpa_you_have_already_selected'] ?>");
                $(this).prop("checked", false);
                values.splice(-1, 1);
            }
        });
        $(document).on('click', '.saveChangesBtn', function () {
//            var nodes_data = <?php // echo json_encode($assessment_portfolio_tasks);                                                                                                                                                                                                                                                                                                                                                                              ?>;
            var current_node_index = $('#current_node_index').val();
            var current_node_details = nodes_data[current_node_index];
            var selected_elements_arr = nodes_data[current_node_index].documents_arr;
            var documents_data_arr = nodes_data[current_node_index].documents;
            var checked_checkboxes = $('input:checkbox:checked.select_checkbox');

            checked_checkboxes.each(function (ind, val) {
                var document_id = String($(this).data('document_id'));
                var document_title = $(this).data('document_title');
                var folder_type = String($(this).data('folder_type'));
                var document_by = $(this).data('document_by');
                var doc_type = String($(this).data('doc_type'));
                var thumbnail = $(this).data('thumbnail');
                var document_date = $(this).data('document_date');
                if (selected_elements_arr.length < current_node_details.max_number_of_files) {
                    if ($.inArray($(this).val(), selected_elements_arr) === -1) {
                        var new_file = {
                            doc_type: doc_type,
                            document_by: document_by,
                            document_date: document_date,
                            document_id: document_id,
                            document_title: document_title,
                            folder_type: folder_type,
                            thumbnail: thumbnail
                        };
                        documents_data_arr.push(new_file);
                        selected_elements_arr.push(document_id);
                    }
                    loadNodeData(current_node_index);
                }
            });
            //            return false;
            $('.saveChangesBtn').attr('disabled', 'disabled');
            var values = $('input:checkbox:checked.select_checkbox').map(function () {
                return this.value;
            }).get();
            fn(values);
        });
        $(document).on('click', '.tablink', function () {
            $('.tablink').removeClass('currentTablink');
            $(this).addClass('currentTablink');
            $.ajax({
                type: 'POST',
                url: home_url + '/Edtpa/refresh_tab',
                dataType: 'json',
                data: {node_id: $('#current_node').val()},
                success: function (data) {

                }
            });
        });
    });
    $(document).on('click', '.removeItem', function () {
        var r = confirm('<?php echo trim($edtpa_lang['edtpa_you_sure_you_want']) ?>');
        if (r == true) {
            var index_to_remove = $(this).data('index');
            var current_node_index = $('#current_node_index').val();
            var current_node_details = nodes_data[current_node_index];
            var selected_elements_arr = nodes_data[current_node_index].documents_arr;
            var documents_data_arr = nodes_data[current_node_index].documents;
            selected_elements_arr.splice(index_to_remove, 1);
            documents_data_arr.splice(index_to_remove, 1);
            $(this).closest("div.adt_thumb2").remove();
            loadNodeData(current_node_index);
            var values = $('input:checkbox:checked.select_checkbox').map(function () {
                return this.value;
            }).get();
            fn(values);
        } else {

        }
    });
    var xhr;
    var fn = function (filter) {

        if (xhr && xhr.readyState != 4) {
            xhr.abort();
        }
        xhr = $.ajax({
            type: 'POST',
            url: home_url + '/Edtpa/save_selected_files',
            dataType: 'json',
            data: {
                selected_files: filter,
                assessment_account_id: <?php echo $edtpa_assessment_account_id; ?>,
                node_id: $('#current_node').val()
            },
            success: function (data) {
                if (data.status == true) {
                    loadAjaxFilter();
                    $('.saveChangesBtn').removeAttr("disabled");
                    $(".successCls").fadeIn().fadeOut("slow");
                    //Remove items from nodes_data which are uncheched
                    var current_node_index = $('#current_node_index').val();
                    var current_node_details = nodes_data[current_node_index];
                    var selected_elements_arr = nodes_data[current_node_index].documents_arr;
                    var documents_data_arr = nodes_data[current_node_index].documents;
                    //console.log(selected_elements_arr);
                    var i = 0;
                    $.each(selected_elements_arr, function (index, value) {
                        if ($.inArray(value, filter) === -1) {
                            selected_elements_arr.splice(i, 1);
                            documents_data_arr.splice(i, 1);
                        }
                        i++;
                    });
                    loadNodeData(current_node_index);

                    setTimeout(function () {
                        $('#edtpa_modal').trigger('click');
                    }, 1000);
                    setTimeout(function () {
                        $('#edtpaResources_modal').trigger('click');
                    }, 1000);
                    if (typeof (data.total_submissions) != 'undefined' && data.total_submissions == 1) {
                        location.reload();
                    }
                } else {
                    $(".errorCls").fadeIn().fadeOut("slow");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }

    function sendToEdtpa(url) {
        var errors = "";
        $.each(nodes_data, function (index, value) {
            if (value.min_number_of_files > value.documents_arr.length) {
                errors += value.short_name + ' : <?php echo $edtpa_lang['edtpa_number_of_required_files'] ?>, ' + value.min_number_of_files + "<br/>";
            }
        });
        //        errors = "";
        if (errors == '') {
            window.location.href = url;
        } else {
            $('#error-msg').html('<div class="message error">' + errors + '</div>');
            $("html, body").animate({scrollTop: 0}, "slow");

        }
    }

    function validateFileType(file, valid_types) {
        //        var extension = file.name.replace(/^.*\./, '');
        var extension = file;

        extension = extension.toLowerCase();
        if ($.inArray(extension, valid_types) == -1) {
            return false;
        } else {
            return true;
        }
    }
</script>

<script>
    var baseurl = '<?php echo $this->base; ?>' + '/';
</script>
<script>
    $(document).ready(function () {

        $(".resources_dropDown").click(function () {
            $(".resources_dropDownInner").toggle();
        });

    });

    function save_selected_files(selected_files) {

        // console.log(selected_files);
        $.ajax({
            type: 'POST',
            url: home_url + '/Edtpa/save_selected_files',
            dataType: 'json',
            data: {selected_files: selected_files},
            success: function (response) {
                console.log(response);
                if (response.status == true) {
                    alert('done');
                } else {
                    alert('failed');
                }
            }
        });
    }
</script>
<style>
    .adt_video3{
        float: left;
        width: 117px;
        margin-right: 10px;
        height: 86px;
        overflow: hidden;
    }
    .adt_ajdacent .adt_video3 {
        width: 230px !important;
    }

    .adt_ajdacent .adt_desc3 {
        width: 35% !important;
    }

    .adt_ajdacent .adt_video3 a {
        white-space: nowrap;
        max-width: 168px;
        display: inline-block;
        overflow: hidden;
        text-overflow: ellipsis;
        padding-top: 7px;
    }

    .videosOuterCls .adt_thumb3 {
        position: relative;
    }

    .videosOuterCls .adt_thumb3 .play-icon {
        top: -26px !important;
    }

    p.noDataCls {
        padding-left: 20px;
    }

    .adt_thumb3 .image {
        background: url(/app/img/icons/jpeg96.png) no-repeat left top;
        width: 32px;
        height: 85px;
        background-size: 100%;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
        background-size: 100%;
    }

    .adt_thumb2 .image {
        background: url(/app/img/icons/jpeg96.png) no-repeat left top;
        width: 32px;
        height: 85px;
        background-size: 100%;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
    }

    .adt_thumb3 .acro {
        background: url(/app/img/icon-img-1.gif) no-repeat left top;
        width: 32px;
        height: 85px;
        background-size: 100%;
        margin: 0 9px 0 0;
        overflow: hidden;
        text-indent: -90000px;

        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
    }

    .adt_thumb2 .acro {
        background: url(/app/img/icon-img-1.gif) no-repeat left top;
        width: 32px;
        height: 55px;
        background-size: 100%;
        margin: 0 9px 0 0;
        overflow: hidden;
        text-indent: -90000px;
        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
    }

    .adt_thumb3 span.wordpress {
        background: url(/app/img/icon-img-2.gif) no-repeat left top;
        width: 32px;
        height: 55px;
        background-size: 100%;

        float: left;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
    }

    .adt_thumb2 span.wordpress {
        background: url(/app/img/icon-img-2.gif) no-repeat left top;
        width: 32px;
        height: 55px;
        background-size: 100%;
        float: left;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
    }

    .adt_thumb3 span.x-icon {
        background: url(/app/img/icon-img-4.gif) no-repeat left top;
        width: 32px;
        height: 55px;
        background-size: 100%;
        float: left;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
    }

    .adt_thumb2 span.x-icon {
        background: url(/app/img/icon-img-4.gif) no-repeat left top;
        width: 32px;
        height: 55px;
        background-size: 100%;
        float: left;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
    }

    #edtpaDocs_modal .adt_desc3 p {
        margin-bottom: 2px;
    }

    #dvHuddle .play-icon {
        left: 17% !important;
        top: 75px !important;
    }

    #dvHuddle .play-icon {
        left: 17% !important;
        top: 75px !important;
    }

    #dvWorkSpeace .play-icon {
        left: 17% !important;
        top: 75px !important;
    }

    .modal-content .header {
        margin-bottom: 0px;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
        padding: 12px 20px;
    }

    .modal-footer {
        border-bottom-left-radius: 6px;
        border-top-bottom-radius: 6px;
        padding: 12px 20px !important;
        margin: 12px 0 0 0;
    }

    .modal-content {
        border-bottom-left-radius: 6px;
        border-bottom-radius: 6px;
    }

    .tab-content {
        padding: 20px 13px 11px 13px !important;
    }

    .adt_desc3 {
        width: 56% !important;
    }

    .tablink {
        position: relative;
        outline: none;
    }

    .tablink i {
        display: none;
        width: 0;
        height: 0;
        border-top: 10px solid transparent;
        border-bottom: 10px solid transparent;
        border-left: 10px solid #fff;
        position: absolute;
        right: -11px;
        top: 17%;
    }

    .edt_tab_name button {
        width: 100%;
        margin: 10px 0%;
        padding-bottom: 10px;
        padding: 12px;
    }

    .edt_tab_name button.currentTablink {
        background: #ffffff;
    }

    .currentTablink i {
        display: inline-block;
    }

    .edtpa_container h1 a {
        font-size: 13px;
    }

    .resources_dropDown {
        background: #5daf46;
        display: inline-block;
        padding: 3px 15px 7px 13px;
        color: #fff !important;
        border-radius: 22px;
        font-weight: normal;
        text-decoration: none;
        margin-left: 5px;
        position: relative;
        cursor: pointer;
    }

    .whiteArrow {
        border-left: solid 1px #7dbf6b;
        padding: 13px 0px 12px 12px;
        margin: -8px 0px;
        margin-left: 10px;
    }

    .resources_dropDownInner {
        position: absolute;
        border-radius: 4px;
        box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.1);
        bottom: -64px;
        background: #fff;
        width: 190px;
        padding: 6px;
        left: 0px;
        display: none;
    }

    .tab_content_bar .resources_dropDownInner a {
        display: block;
        color: #616161 !important;
        padding: 3px 0px;
        background: none;
        font-size: 13px
    }

    .bread_crum {
        margin-bottom: 20px;
        font-size: 13px;
    }

    .bread_crum a {
        font-weight: normal;
        color: #668dab;
    }

    .bread_crum a:after {
        content: '>';
        margin: 0px 15px;
        color: #757575
    }

    .adt_video2 {
        float: left;
        width: 35px;
        margin: 5px;
    }

    .adt_thumb2 img {
        width: inherit;
        border-radius: 10px;
    }

    .adt_thumb2 {
        position: relative;
    }

    .adt_desc2 {
        margin-left: 10px;
        width: 50% !important;
    }

    .adt_thumb2 h4 {
        margin-top: 7px;

    }

    .adt_thumb2 p {
        width: 97% !important;
        white-space: unset !important;
    }

    .student_srcv p {
        margin: 0 0 1px 0 !important;
    }
</style>
<div class="edtpa_container">
    <div class="bread_crum">
        <a href="<?php echo $this->base . '/Dashboard/home' ?>"><?php echo $edtpa_lang['edtpa_brd_dashboard'] ?></a> <a
            href="<?php echo $this->base . '/Edtpa/edtpa' ?>">edTPA <?php echo $edtpa_lang['edtpa_brd_home'] ?> </a> <?php echo $edtpa_lang['edtipa_details'] ?>
    </div>
    <div id="error-msg" style="cursor: pointer;"></div>


    <script>
        function open_edTPATransferTracker_Popup(form) {
            var controlinfo;
            var popName = "edTPA_TransferTracker";
            var popStyle =
                    "width=700,height=768,status=yes,resizable=yes,scrollbars=yes";
            form.target = popName;
            controlinfo = window.open('', popName, popStyle);
            controlinfo.focus();
            return;
        }
    </script>

    <?php if ($_SESSION['LANG'] == 'es'): ?>
        <h1><?php echo $assessment_details['EdtpaAssessments']['assessment_name_es'] ?> </h1>
    <?php else: ?>
        <h1><?php echo $assessment_details['EdtpaAssessments']['assessment_name'] ?> </h1>
    <?php endif; ?>
    <?php
    if (!empty($tracker_url)) {
        ?>

        <form action="<?php echo $tracker_url; ?>" method="post"
              style="width: 100%;display: block;margin-bottom: 10px;position: relative;height: 24px;">
            <a style="border-radius: 5px;background-color: #5daf46;color:#FFFFFF;border:0px;cursor:pointer;font-family:Arial;font-size: 14px;font-weight:bold;text-align:center;margin-right:8px;padding:5px 7px;position:relative;box-sizing:border-box;float: right;margin-right: 0px;height: 25px;margin-left: 6px;"
               href="<?php echo $this->base . '/Edtpa/send_to_edtpa/' ?><?php echo $assessment_details['EdtpaAssessments']['id']; ?>"
               onclick='sendToEdtpa("<?php echo $this->base . '/Edtpa/send_to_edtpa/' . $assessment_details['EdtpaAssessments']['id']; ?>");
                       return false;'><?php echo $edtpa_lang['edtpa_pearson'] ?></a>
            <input type="submit" value="<?php echo $edtpa_lang['edtpa_track_edtpa_transfer'] ?>" onclick="open_edTPATransferTracker_Popup(this.form);"
                   style="border-radius:5px;background-color:#910000;color:#FFFFFF;border:0px;cursor:pointer;font-family:Arial;font-size: 14px;font-weight:bold;text-align:center;margin-right:8px;padding:5px 7px;position:relative;box-sizing:border-box;float: right;margin-right: 0px;"/>
        </form>

        <?php
    }
    ?>
    <div class="edt_tab_main">
        <div class="edt_tab_name">
            <input type="hidden" name="current_node" id="current_node" value=""/>
            <input type="hidden" name="current_node_index" id="current_node_index" value=""/>
            <div id="my_computer_resources" style="display: none;"></div>
            <h5><?php echo $edtpa_lang['edtpa_task_list']; ?></h5>
            <?php
            if (!empty($assessment_portfolio_tasks)):
                $i = 0;
                foreach ($assessment_portfolio_tasks

                as $assessment_portfolio_task) {
                    ?>
                    <button class="tablink" id="edTPA_node_<?php echo $assessment_portfolio_task['node_id']; ?>"
                            data-attr-index="<?php echo $i; ?>" data-node="<?php echo $assessment_portfolio_task['node_id']; ?>"
                            onclick="loadNodeData('<?php echo $i; ?>')">
                        <b><?php echo $assessment_portfolio_task['short_name'] ?></b>
                        <?php echo $assessment_portfolio_task['long_name'] ?>
                        <i></i>
                        <?php
                        $i++;
                    }
                else:
                    ?><p><?php echo $edtpa_lang['edtpa_these_are_no_nodes'] ?></p><?php
                endif;
                ?>

        </div>
        <div class="edt_tab_content">
            <div class="tab_content_bar">
                <?php echo $edtpa_lang['edtpa_add'] ?> <a data-toggle="modal" id="btnVideo" data-target="#edtpa_modal" href="#" style="display:none;"><img
                        src="/app/img/edtpa/video_edtpa.png"> <?php echo $edtpa_lang['edtpa_video'] ?></a>
            <!--<a data-toggle="modal" id="btnDocument" data-target="#edtpaDoc_modal" href="#" style="display:none;"><img src="/app/img/edtpa/video_edtpa.png"> Video</a>-->
                <span class="resources_dropDown" style="display:none;"><img src="/app/img/edtpa/resources.png"/> <?php echo $edtpa_lang['edtpa_resources'] ?> <img
                        src="/app/img/edtpa/whiteArrowDown.png" class="whiteArrow"/>
                    <div class="resources_dropDownInner">
                        <a data-toggle="modal" id="btnResources" data-target="#edtpaResources_modal"
                           href="javascript:void(0)" style=""><img src="/app/img/edtpa/resources_black.png"/><?php echo $edtpa_lang['edtpa_add_from_resources'] ?></a>
                        <a id="btnUploadDocument" href="javascript:void(0);"><img src="/app/img/edtpa/upload.png"/><?php echo $edtpa_lang['edtpa_upload_from_computer'] ?></a>
                    </div>
                </span>
                <div id="required-attachment" style="float: right;"></div>
            </div>
            <div id="tskDetail" class="tab_edt " style="display:none">
                <h4><span id="spnTaskName"></span></h4>
                <h6><span id="spnTaskDesc"></span></h6>
                <span id="edtpa_instructions"></span>


                <!-- <ol>
                     <li>Click on the Video icon above</li>
                     <li>A dialog box will appear with all the videos you have stored in your private Workspace and
                         Huddles you are
                         participating in.
                     </li>
                <!--                    <li>You can select 1 or more videos from your Workspace and Huddles.</li>-->
                <!--  <li>You can select a minimum of <span id="spminvideos"></span> Video(s) and a maximum of <span
                              id="spmaxvideos"></span> video(s) from your Workspace and Huddles.
                  </li>
                  <li>The videos you select will appear at the bottom of the page.</li>
              </ol>-->
                <div class="assessment_handbook" style="border: 1px solid #cecece;">
                    <div style="background: #e1e1e1;padding: 10px; cursor: pointer;" id="handbook_title">
                        <?php echo $edtpa_lang['edtpa_assessment_handbook'] ?>
                        <div class="arrow-upe" style="display: none;"></div>
                        <div class="arrow-downe"></div>
                    </div>
                    <ul style="list-style: none;padding-left: 5px; display: none;" data-attr="hidden"
                        id="handbook-header">
                            <?php
                            if ($edtpa_assessment_handbooks) {
                                foreach ($edtpa_assessment_handbooks as $row) {
                                    if ($_SESSION['LANG'] == 'es') {
                                        echo '<li style="background: #ebebeb; padding: 10px; width:98%;"><a style="color:#66a5e2" download href=' . $this->base . '/edtpa_handbooks/' . $row['EdtpaAssessmentsHandbooks']['handbook_url'] . '>' . $row['EdtpaAssessmentsHandbooks']['title_es'] . '</a></li>';
                                    } else {
                                        echo '<li style="background: #ebebeb; padding: 10px; width:98%;"><a style="color:#66a5e2" download href=' . $this->base . '/edtpa_handbooks/' . $row['EdtpaAssessmentsHandbooks']['handbook_url'] . '>' . $row['EdtpaAssessmentsHandbooks']['title'] . '</a></li>';
                                    }
                                }
                            } else {
                                echo '<li>' . $edtpa_lang['edtpa_no_assessment_handbook'] . '.</li>';
                            }
                            ?>
                    </ul>
                </div>
                <div style="clear: both;"></div>
                <div class="workplace_edtp">
                    <h5><?php echo $edtpa_lang['edtpa_attachments'] ?></h5>
                    <div id="dvWorkSpeace"></div>
                    <!--<h5>Huddle</h5>-->
                    <div id="dvHuddle">
                    </div>
                    <div id="dvhuddlesettings" style="display:none;">
                        <div class="edtp-heading"><?php echo $edtpa_lang['edtpa_settings_and_Assistance'] ?>
                            <div class="arrow-upe"></div>
                            <div class="arrow-downe"></div>
                        </div>
                        <div class="edtp-content" style="display:none">
                            <div class="edtp_col">


                            </div>

                        </div>


                        <div class="edtp-heading"> <?php echo $edtpa_lang['edtpa_settings_and_Assistance'] ?>
                            <div class="arrow-upe"></div>
                            <div class="arrow-downe"></div>
                        </div>
                        <div class="edtp-content" style="display:none">
                            <div class="edtp_col">


                            </div>

                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>


            <div class="edt_tb_placeholder">
                <img src="/app/img/edtpa/edt_placeholder.png">
                <h6><?php echo $edtpa_lang['edtpa_please_select_tasks'] ?> </h6>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>
<script type="text/javascript">
    $(document).ready(function (e) {
        $('#handbook_title').on('click', function (e) {
            var status = $('#handbook-header').attr('data-attr');
            if (status == 'hidden') {
                $('#handbook-header').show(500);
                $('#handbook-header').attr('data-attr', 'show');
                $('.arrow-downe').hide()
                $('.arrow-upe').show();

            } else {
                $('#handbook-header').hide(500);
                $('#handbook-header').attr('data-attr', 'hidden');
                $('.arrow-upe').hide();
                $('.arrow-downe').show();
            }
        })
    })
</script>

<div id="edtpa_modal" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:600px;">
        <div class="modal-content">
            <div class="header" style="margin-bottom:0px;">
                <h4 class="header-title nomargin-vertical"><?php echo $edtpa_lang['edtpa_select_video'] ?></h4>

                <a id="cross_contact" class="close-reveal-modal btn btn-grey close style2"
                   data-dismiss="modal">&times;</a>
            </div>
            <div id="tab-area">
                <ul class="tabset" style="max-width: 100%;">
                    <li class="">
                        <a id="tab1" class="tab active" href="#tabbox2tp"><?php echo $edtpa_lang['edtpa_workspace'] ?></a>
                    </li>
                    <li class="">
                        <a id="tab2" class="tab" href="#tabbox3pt"><?php echo $edtpa_lang['edtpa_huddles'] ?></a>
                    </li>
                </ul>
                <div class="tab-content tab-active videosOuterCls" id="tabbox2tp"
                     style="visibility: visible; max-height: 400px;overflow: auto;">
                    <div class="edtp_col">
                        <div id="wsVideosOuter">
                            <?php
                            $publisehd_videos_count = 0;
                            if (isset($MyFilesVideos) && !empty($MyFilesVideos)) {

                                foreach ($MyFilesVideos as $MyFilesVideo):
                                    $document_files_array = $this->Custom->get_document_url($MyFilesVideo['Document']);

                                    if (empty($document_files_array['url'])) {
                                        $MyFilesVideo['Document']['published'] = 0;
                                        $document_files_array['url'] = $MyFilesVideo['Document']['original_file_name'];
                                        $MyFilesVideo['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                    } else {
                                        $MyFilesVideo['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                        @$MyFilesVideo['Document']['duration'] = $document_files_array['duration'];
                                    }
                                    $thumbnail_image_path = $document_files_array['thumbnail'];
                                    ?>
                                    <div class="adt_thumb3">
                                        <div class="adt_video3"
                                             onclick="window.open('<?php echo $this->base . '/MyFiles/view/1/' . $MyFilesVideo['AccountFolder']['account_folder_id']; ?>'


                                                                     );">
                                            <img src="<?php echo $thumbnail_image_path; ?>">
                                            <div class="play-icon"
                                                 style="background-size: 50px;height: 50px;margin: 48px 0 0 -30px;position: absolute;left: 11%;width: 50px;"></div>
                                        </div>
                                        <div class="adt_desc3 student_srcv"
                                             onclick="window.open('<?php echo $this->base . '/MyFiles/view/1/' . $MyFilesVideo['AccountFolder']['account_folder_id']; ?>'


                                                             );">
                                            <h4><?php echo(strlen($MyFilesVideo['afd']['title']) > 25 ? mb_substr($MyFilesVideo['afd']['title'], 0, 25) . "..." : $MyFilesVideo['afd']['title']); ?></h4>
                                            <p><?php echo $edtpa_lang['edtpa_by'] ?>
                                                <a href="javascript:void(0);"><?php echo $MyFilesVideo['User']['first_name'] . " " . $MyFilesVideo['User']['last_name']; ?></a>
                                            </p>
                                            <p><?php echo formate_date_lang(date('M d, Y', strtotime($MyFilesVideo['Document']['created_date']))); ?></p>
                                        </div>
                                        <div class="edt_select">
                                            <?php
                                            $file_path_info = pathinfo($MyFilesVideo['Document']['url']);
                                            $document_extension = $file_path_info['extension'];
                                            ?>
                                            <span> <input type="checkbox"
                                                          data-document_extension="<?php echo $document_extension; ?>"
                                                          data-thumbnail="<?php echo $thumbnail_image_path; ?>"
                                                          data-doc_type="<?php echo $MyFilesVideo['Document']['doc_type']; ?>"
                                                          data-document_by="<?php echo $MyFilesVideo['User']['first_name'] . " " . $MyFilesVideo['User']['last_name']; ?>"
                                                          data-document_date="<?php echo formate_date_lang(date('M d, Y', strtotime($MyFilesVideo['Document']['created_date']))); ?>"
                                                          data-document_id="<?php echo $MyFilesVideo['Document']['id']; ?>"
                                                          data-document_title="<?php echo(strlen($MyFilesVideo['afd']['title']) > 25 ? mb_substr($MyFilesVideo['afd']['title'], 0, 25) . "..." : $MyFilesVideo['afd']['title']); ?>"
                                                          data-folder_type="<?php echo $MyFilesVideo['AccountFolder']['folder_type']; ?>"
                                                          class="select_checkbox"
                                                          id="select_<?php echo $MyFilesVideo['Document']['id']; ?>"
                                                          name="selected_videos[]"
                                                          value="<?php echo $MyFilesVideo['Document']['id']; ?>"> <label
                                                          for="select_<?php echo $MyFilesVideo['Document']['id']; ?>"><?php echo $edtpa_lang['edtpa_select'] ?></label> </span>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <?php
                                    $publisehd_videos_count++;
                                endforeach;
                                ?>

                                <?php
                            }
                            if ($publisehd_videos_count <= 0) {
                                ?><p class="noDataCls"><?php echo $edtpa_lang['edtpa_no_videos'] ?>.</p><?php
                            }
                            ?>
                        </div>
                        <input type="hidden" name="selected_node" id="selected_node" value=""/>
                        <input type="hidden" name="page_num" id="page_num" value="1"/>
                        <div style="text-align:center;">
                            <img id="loading_gif" style="margin-bottom: -32px; display: none;"
                                 src="<?php echo $this->webroot . 'img/loading.gif' ?>">
                            <a class="btn btn-green" id="video-load-more" style="display:none;"><?php echo $edtpa_lang['edtpa_load_more'] ?></a>
                        </div>
                    </div>

                </div>
            </div>
            <?php if ($MyFilesVideosCount > 10) : ?>
                <script type="text/javascript">
                    $('#video-load-more').show();
                </script>
            <?php endif; ?>
            <div class="tab-content tab-hidden videosOuterCls" id="tabbox3pt"
                 style="visibility: visible; max-height: 400px;overflow: auto;">
                <div id="huddlesOuter">
                    <?php
                    if (isset($HuddlesVideos) && !empty($HuddlesVideos)) {
                        foreach ($HuddlesVideos as $HuddlesVideo):
                            ?>
                            <div class="edtp-heading"> <?php echo $HuddlesVideo['AccountFolder_name']; ?>
                                <div class="arrow-upe"></div>
                                <div class="arrow-downe"></div>
                            </div>
                            <div class="edtp-content" style="display:none">
                                <div class="edtp_col">

                                    <?php
                                    if (!empty($HuddlesVideo['videos'])) {
                                        foreach ($HuddlesVideo['videos'] as $video_details):
                                            if ($HuddlesVideo['AccountFolder_folder_type'] == 1) {
                                                $url_type_part = 1;
                                            } else {
                                                $url_type_part = '';
                                            }
                                            $videoID = $video_details['video_id'];
                                            $huddleID = $HuddlesVideo['AccountFolder_id'];
                                            ?>
                                            <div class="adt_thumb3">
                                                <div class="adt_video3"
                                                     onclick="window.open('<?php echo $this->base . '/Huddles/view/' . "$huddleID" . '/' . "$url_type_part/$videoID"; ?>');">
                                                    <img src="<?php echo $video_details['video_thumbnail']; ?>">
                                                    <div class="play-icon"
                                                         style="background-size: 50px;height: 50px;margin: 48px 0 0 -30px;position: absolute;left: 11%;width: 50px;"></div>
                                                </div>
                                                <div class="adt_desc3 student_srcv"
                                                     onclick="window.open('<?php echo $this->base . '/Huddles/view/' . "/$url_type_part/$videoID"; ?>');">
                                                    <h4><?php echo(strlen($video_details['video_title']) > 25 ? mb_substr($video_details['video_title'], 0, 25) . "..." : $video_details['video_title']); ?></h4>
                                                    <p><?php echo $edtpa_lang['edtpa_by'] ?>
                                                        <a href="javascript:void(0);"><?php echo $HuddlesVideo['user_first_name'] . " " . $HuddlesVideo['user_last_name']; ?></a>
                                                    </p>
                                                    <p><?php echo $edtpa_lang['edtpa_upload'] ?> <?php echo $video_details['video_uploaded_date']; ?></p>
                                                </div>

                                                <div class="edt_select">
                                                    <span> <input type="checkbox"
                                                                  data-document_extension="<?php echo $video_details['document_extension']; ?>"
                                                                  data-thumbnail="<?php echo $video_details['video_thumbnail']; ?>"
                                                                  data-doc_type="<?php echo $video_details['doc_type']; ?>"
                                                                  data-document_by="<?php echo $HuddlesVideo['user_first_name'] . " " . $HuddlesVideo['user_last_name']; ?>"
                                                                  data-document_date="<?php echo $video_details['video_uploaded_date']; ?>"
                                                                  data-document_id="<?php echo $video_details['video_id']; ?>"
                                                                  data-document_title="<?php echo $video_details['video_title']; ?>"
                                                                  data-folder_type="<?php echo $HuddlesVideo['AccountFolder_folder_type']; ?>"
                                                                  class="select_checkbox"
                                                                  id="select_<?php echo $video_details['video_id']; ?>"
                                                                  name="selected_videos[]"
                                                                  value="<?php echo $video_details['video_id']; ?>"> <label
                                                                  for="select_<?php echo $video_details['video_id']; ?>"><?php echo $edtpa_lang['edtpa_select'] ?></label> </span>
                                                </div>
                                                <div class="clear"></div>

                                            </div>
                                            <?php
                                        endforeach;
                                    } else {
                                        ?>
                                        <p class="noDataCls"><?php echo $edtpa_lang['edtpa_no_videos_in_huddle'] ?>.</p>
                                    <?php } ?>

                                </div>
                            </div>
                            <?php
                        endforeach;
                    } else {
                        ?>
                        <p class="noDataCls"><?php echo $edtpa_lang['edtpa_no_huddles_account'] ?>.</p>
                    <?php } ?>
                </div>
                <input type="hidden" name="page_num_huddle" id="page_num_huddle" value="1"/>
                <div style="text-align:center;">
                    <br>
                    <img id="loading_gif_huddle" style="margin-bottom: -32px; display: none;"
                         src="<?php echo $this->webroot . 'img/loading.gif' ?>">
                    <a class="btn btn-green" id="video-load-more-huddles" style="display:none;"><?php echo $edtpa_lang['edtpa_load_more_huddles'] ?>...</a>
                </div>
            </div>
            <?php if ($HuddlesCount > 10) : ?>
                <script type="text/javascript">
                    $('#video-load-more-huddles').show();
                </script>
            <?php endif; ?>
            <div class="clear"></div>
            <div class="modal-footer">
                <div class="form-actions" style="text-align: right;margin-top:0px;">
                    <p class="successCls" style="display:none;color:green;"><?php echo $edtpa_lang['edtpa_changes_saved'] ?>.</p>
                    <p class="errorCls" style="display:none;color:red;"><?php echo $edtpa_lang['edtpa_unkwown_error'] ?>. </p>
                    <!--<a href="javascript:void(0);" class="btn btn-primary saveChangesBtn" id="saveChangesBtnVid">Save Changes</a>-->
                    <input id="saveChangesBtnVid" type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" value="<?php echo $edtpa_lang['edtpa_save_changes'] ?>" name="commit"
                           class="btn btn-green saveChangesBtn">
                    <input id="videoModelCancel" class="btn btn-white cancelCls" data-dismiss="modal" type="reset"
                           value="<?php echo $edtpa_lang['edtpa_cancel'] ?>" style="font-size: 14px;">
                </div>
            </div>
        </div>
        <div class="clear"></div>

    </div>
</div>
</div>
<div id="edtpaResources_modal" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:600px;">
        <div class="modal-content">
            <div class="header" style="margin-bottom:0px;">
                <h4 class="header-title nomargin-vertical"><?php echo $edtpa_lang['edtpa_select_resources_btn'] ?></h4>
                <a id="cross_contact" class="close-reveal-modal btn btn-grey close style2"
                   data-dismiss="modal">&times;</a>
            </div>
            <div id="tab-area">
                <ul class="tabset" style="max-width: 100%;">
                    <li class="">
                        <a id="tab1" class="tab active" href="#workspace-edTPA"><?php echo $edtpa_lang['edtpa_workspace'] ?></a>
                    </li>
                    <li class="">
                        <a id="tab2" class="tab" href="#huddle-edTPA"><?php echo $edtpa_lang['edtpa_huddles'] ?></a>
                    </li>

                </ul>
                <div class="tab-content tab-active videosOuterCls" id="workspace-edTPA"
                     style="visibility: visible;  max-height: 400px;overflow: auto;">
                    <div class="edtp_col">
                        <?php
                        $publisehd_docs_count = 0;
                        if (isset($MyFilesDocuments) && !empty($MyFilesDocuments)) {

                            foreach ($MyFilesDocuments as $MyFilesDocument):
                                ?>
                                <div class="adt_thumb3 adt_ajdacent">
                                    <div class="adt_video3">
                                        <span class="<?php echo $this->Custom->getIcons($MyFilesDocument['Document']['url']); ?>">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <?php if (isset($MyFilesDocument['Document']['stack_url']) && $MyFilesDocument['Document']['stack_url'] != '') {
                                            ?>
                                            <?php
                                            $url_code = explode('/', $MyFilesDocument['Document']['stack_url']);
                                            $url_code = $url_code[3];
                                            ?>
                                                                                                                                                                                                                    <!--<a  id="view_resource_myfiles_<?php // echo $MyFilesDocument['Document']['id'];                                                                                                                                                                                                                                                                                                                                                                                                                                                             ?>" target="_blank" href="<?php // echo $this->base . '/MyFiles/view_document/' . $url_code;                                                                                                                                                                                                                                                                                                                                                                                                                                                             ?>" >-->
                                            <a id="view_resource_myfiles_<?php echo $MyFilesDocument['Document']['id']; ?>"
                                               target="_blank" href="javascript:void(0);">
                                                <span id="doc-title-<?php echo $MyFilesDocument['afd']['id']; ?>"><?php echo $MyFilesDocument['afd']['title']; ?></span>
                                            </a>


                                        <?php } else { ?>
                                            <a href="javascript:void(0);"
                                               title="<?php echo $MyFilesDocument['afd']['title'] ?>">
                                                <span id="doc-title-<?php echo $MyFilesDocument['afd']['id']; ?>"><?php echo $MyFilesDocument['afd']['title']; ?></span>
                                            </a>
                                        <?php } ?>
                                    </div>

                                    <div class="adt_desc3 student_srcv">
                                        <h4><?php echo(strlen($MyFilesDocument['afd']['title']) > 10 ? mb_substr($MyFilesDocument['afd']['title'], 0, 10) . "..." : $MyFilesDocument['afd']['title']); ?></h4>
                                        <p><?php echo $edtpa_lang['edtpa_by'] ?>
                                            <a href="javascript:void(0);"><?php echo $MyFilesDocument['User']['first_name'] . " " . $MyFilesDocument['User']['last_name']; ?></a>
                                        </p>
                                        <p><?php echo $edtpa_lang['edtpa_type'] ?> : <a
                                                href="javascript:void(0);"><?php echo $this->Custom->docType($MyFilesDocument['Document']['url']); ?></a>
                                        </p>
                                        <p><?php echo formate_date_lang(date('M d, Y', strtotime($MyFilesDocument['Document']['created_date']))); ?></p>
                                    </div>
                                    <div class="edt_select">
                                        <?php
                                        $file_path_info = pathinfo($MyFilesDocument['Document']['url']);
                                        $document_extension = $file_path_info['extension'];
                                        ?>
                                        <span> <input type="checkbox"
                                                      data-document_extension="<?php echo $document_extension; ?>"
                                                      data-thumbnail="<?php echo $this->Custom->getIcons($MyFilesDocument['Document']['url']); ?>"
                                                      data-doc_type="<?php echo $MyFilesDocument['Document']['doc_type']; ?>"
                                                      data-document_by="<?php echo $MyFilesDocument['User']['first_name'] . " " . $MyFilesDocument['User']['last_name']; ?>"
                                                      data-document_date="<?php echo formate_date_lang(date('M d, Y', strtotime($MyFilesDocument['Document']['created_date']))); ?>"
                                                      data-document_id="<?php echo $MyFilesDocument['Document']['id']; ?>"
                                                      data-document_title="<?php echo(strlen($MyFilesDocument['afd']['title']) > 25 ? mb_substr($MyFilesDocument['afd']['title'], 0, 25) . "..." : $MyFilesDocument['afd']['title']); ?>"
                                                      data-folder_type="<?php echo $MyFilesDocument['AccountFolder']['folder_type']; ?>"
                                                      class="select_checkbox"
                                                      id="select_<?php echo $MyFilesDocument['Document']['id']; ?>"
                                                      name="selected_videos[]"
                                                      value="<?php echo $MyFilesDocument['Document']['id']; ?>"> <label
                                                      for="select_<?php echo $MyFilesDocument['Document']['id']; ?>"><?php echo $edtpa_lang['edtpa_select'] ?></label> </span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <?php
                                $publisehd_docs_count++;
                            endforeach;
                        }
                        if ($publisehd_docs_count <= 0) {
                            ?><p class="noDataCls"><?php echo $edtpa_lang['edtpa_no_document_in_workspace'] ?>.</p><?php
                        }
                        ?>
                    </div>
                </div>
                <div class="tab-content tab-hidden videosOuterCls" id="huddle-edTPA"
                     style="visibility: visible;     max-height: 400px; overflow: auto;">
                         <?php                         
                         if (isset($HuddlesDocuments) && !empty($HuddlesDocuments)) {
                             foreach ($HuddlesDocuments as $HuddlesVideo):
                                 ?>
                            <div class="edtp-heading"> <?php echo $HuddlesVideo['AccountFolder_name']; ?>
                                <div class="arrow-upe"></div>
                                <div class="arrow-downe"></div>
                            </div>
                            <div class="edtp-content" style="display:none">
                                <div class="edtp_col">
                                    <?php
                                    if (!empty($HuddlesVideo['documents'])) {
                                        foreach ($HuddlesVideo['documents'] as $document_details):
                                            if ($HuddlesVideo['AccountFolder_folder_type'] == 1) {
                                                $url_type_part = 1;
                                            } else {
                                                $url_type_part = '';
                                            }
                                            $videoID = $document_details['video_id'];
                                            $huddleID = $HuddlesVideo['AccountFolder_id'];
                                            ?>
                                            <div class="adt_thumb3 adt_ajdacent">
                                                <div class="adt_video3">
                                                    <span class="<?php echo $document_details['document_icon']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    <?php if (isset($document_details['stack_url']) && $document_details['stack_url'] != '') {
                                                        ?>
                                                        <?php
                                                        $url_code = explode('/', $MyFilesDocument['stack_url']);
                                                        $url_code = $url_code[3];
                                                        ?>
                                                        <a id="view_resource_myfiles_<?php echo $document_details['document_id']; ?>"
                                                           target="_blank" href="javascript:void(0);">
                                                            <span id="doc-title-<?php echo $document_details['afd_title']; ?>"><?php echo $document_details['afd_title']; ?></span>
                                                        </a>


                                                    <?php } else { ?>
                                                        <a href="javascript:void(0);"
                                                           title="<?php echo $document_details['afd_title'] ?>">
                                                            <span id="doc-title-<?php echo $document_details['afd_title']; ?>"><?php echo $document_details['afd_title']; ?></span>
                                                        </a>
                                                    <?php } ?>
                                                </div>

                                                <div class="adt_desc3 student_srcv">
                                                    <h4><?php echo(strlen($document_details['afd_title']) > 10 ? mb_substr($document_details['afd_title'], 0, 10) . "..." : $document_details['afd_title']); ?></h4>
                                                    <p><?php echo $edtpa_lang['edtpa_by'] ?>
                                                        <a href="javascript:void(0);"><?php echo $document_details['document_by']; ?></a>
                                                    </p>
                                                    <p><?php echo $edtpa_lang['edtpa_type'] ?> : <a
                                                            href="javascript:void(0);"><?php echo $document_details['document_url']; ?></a>
                                                    </p>
                                                    <p><?php echo $document_details['created_date']; ?></p>
                                                </div>
                                                <div class="edt_select">
                                                    <span> <input type="checkbox"
                                                                  data-document_extension='<?php echo $document_details['document_extension']; ?>'
                                                                  data-thumbnail="<?php echo $document_details['document_icon']; ?>"
                                                                  data-doc_type="<?php echo $document_details['doc_type']; ?>"
                                                                  data-document_by="<?php echo $HuddlesVideo['user_first_name'] . " " . $HuddlesVideo['user_last_name']; ?>"
                                                                  data-document_date="<?php echo $document_details['created_date']; ?>"
                                                                  data-document_id="<?php echo $document_details['document_id']; ?>"
                                                                  data-document_title="<?php echo $document_details['afd_title']; ?>"
                                                                  data-folder_type="<?php echo $HuddlesVideo['AccountFolder_folder_type']; ?>"
                                                                  class="select_checkbox"
                                                                  id="select_<?php echo $document_details['document_id']; ?>"
                                                                  name="selected_videos[]"
                                                                  value="<?php echo $document_details['document_id']; ?>"> <label
                                                                  for="select_<?php echo $document_details['document_id']; ?>"><?php echo $edtpa_lang['edtpa_select'] ?></label> </span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <?php
                                        endforeach;
                                    } else {
                                        ?>
                                        <p class="noDataCls"><?php echo $edtpa_lang['edtpa_no_document_in_huddles'] ?>.</p>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php
                        endforeach;
                    } else {
                        ?>
                        <p class="noDataCls"><?php echo $edtpa_lang['edtpa_no_huddles_account'] ?>.</p>
                    <?php } ?>
                </div>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>
            <div class="modal-footer">

                <div class="form-actions" style="text-align: right;margin-top:0px;">
                    <p class="successCls" style="display:none;color:green;"><?php echo $edtpa_lang['edtpa_changes_saved'] ?></p>
                    <p class="errorCls" style="display:none;color:red;"><?php echo $edtpa_lang['edtpa_unkwown_error'] ?>. </p>
                    <input id="saveChangesBtnDoc" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button" value="<?php echo $edtpa_lang['edtpa_save_changes'] ?>" name="commit"
                           class="btn btn-green saveChangesBtn">
                    <input id="docModelCancel" class="btn btn-white cancelCls" type="reset" value="<?php echo $edtpa_lang['edtpa_cancel'] ?>"
                           style="font-size: 14px;" data-dismiss="modal">
                </div>
            </div>
        </div>

    </div>
</div>

<div class="edt_select">
    <span style="display: none;">
        <input type="checkbox" data-document_extension="" data-thumbnail="" data-doc_type=""
               data-document_by="<?php echo $user['User']['first_name'] . " " . $user['User']['last_name']; ?>"
               data-document_date="" data-document_id="" data-document_title="" data-folder_type="3"
               class="select_checkbox my_computer_resurce" id="" name="selected_videos[]" value="">
        <input type="hidden" id="video_id" value="0"/>
    </span>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '.tablink', function () {
            var node_id = $(this).data('node');
            $('#selected_node').val(node_id);
        });
        $(document).on('click', '#btnUploadDocument', function () {
            <?php if($check_users_permissions['UserAccount']['parmission_access_my_workspace'] == 1 && $check_users_permissions['UserAccount']['role_id'] != '125'):?>
                OpenFilePicker('MyFilesDoc');
        <?php else:?>
            alert('You don\'t have access to upload file into workspace.');
            return false;
        <?php endif?>    
        });

        function OpenFilePicker(docType) {
            //console.log(docType);
            var total_attached_file_count = $('input:checkbox:checked.select_checkbox').length;

            current_node_index = $('#current_node').val();
            current_node_id = $('#edTPA_node_' + current_node_index).attr('data-attr-index');
            var current_node_details = nodes_data[current_node_id];
            var maxfile = current_node_details.max_number_of_files;
            if ($('input:checkbox:checked.select_checkbox').length >= maxfile) {
                alert('<?php echo $edtpa_lang['edtpa_you_have_already_submitted'] ?>');
                return false;
            }
            var allow_limit = maxfile - total_attached_file_count;

            // filepicker.setKey(filepicker_access_key);
            var client = filestack.init('<?php echo Configure::read('filepicker_access_key'); ?>');

            var uploadPath = $("#uploadPath").val();
            $('#txtUploadedFilePath').val("");
            $('#txtUploadedFileName').val("");
            $('#txtUploadedFileMimeType').val("");
            $('#txtUploadedFileSize').val("");
            $('#txtUploadedDocType').val(docType);

            var filepickerOptions;
            if (docType == 'MyFilesVideo') {
                filepickerOptions = {
                    multiple: true,
                    fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive', 'video', 'webcam'],
                    extensions: ['flv', 'asf', 'qt', 'mov', 'mpg', 'mpeg', 'avi', 'wmv', 'mp4', 'm4v']
                }
            } else {
                filepickerOptions = {
                    multiple: true,
                    extensions: ['doc', 'docx', 'odt', 'pdf'],
                    fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive', 'url'],
                }
            }

            // fix for iOS 7
            if (isIOS()) {
                filepickerOptions.multiple = false;
            }

            // new window for iPhone
            if (!!navigator.userAgent.match(/iPhone/i)) {
                filepickerOptions.container = 'window';
            }


            if (docType == 'MyFilesVideo') {

                client.pick({
                    maxFiles: allow_limit,
                    fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive', 'video'],
                    accept: ['flv', 'asf', 'qt', 'mov', 'mpg', 'mpeg', 'avi', 'wmv', 'mp4', 'm4v'],
                    lang: '<?php echo $_SESSION['LANG']; ?>',
                    storeTo: {
                        location: "s3",
                        path: uploadPath,
                        access: 'public',
                        container: bucket_name,
                        region: 'us-east-1'
                    }
                }).then(
                        function (inkBlob) {
                            inkBlob = inkBlob.filesUploaded;
                            if (inkBlob && inkBlob.length > 0) {
                                for (var i = 0; i < inkBlob.length; i++) {
                                    var blob = inkBlob[i];
                                    var fileExt = getFileExtension(blob.filename).toLowerCase();

                                    $('#txtUploadedFilePath').val(blob.key);
                                    $('#txtUploadedFileName').val(blob.filename);
                                    $('#txtUploadedFileMimeType').val(blob.mimetype);
                                    $('#txtUploadedFileSize').val(blob.size);
                                    $('#txtUploadedUrl').val(blob.url);
                                    if (docType == 'MyFilesVideo') {
                                        PostMyFilesVideo(docType, (fileExt == 'mp4' ? true : false));
                                    } else {
                                        PostMyFilesDocument(maxfile);
                                    }
                                }
                            }
                        }),
                        function (FPError) {
                            var error_desc = '<?php echo $edtpa_lang['edtpa_unkn_error'] ?>';
                            //as per filepicker documentation these are possible two errors
                            if (FPError.code == 101) {
                                error_desc = '<?php echo $edtpa_lang['edtpa_user_closed_dialog'] ?>';
                            } else if (FPError.code = 151) {
                                error_desc = '<?php echo $edtpa_lang['edtpa_the_file_store'] ?>';
                            }

                            $.ajax({
                                type: 'POST',
                                data: {
                                    type: 'Workspace',
                                    id: '',
                                    error_id: FPError.code,
                                    error_desc: error_desc,
                                    docType: docType,
                                    current_user_id: current_user_id
                                },
                                url: home_url + '/Huddles/logFilePickerError/',
                                success: function (response) {
                                    //Do nothing.
                                },
                                errors: function (response) {
                                    alert('<?php echo $edtpa_lang['edtpa_error_occured'] ?>' + FPError.code);
                                }

                            });
                        }

                //console.log(client);
            }

            else {

                client.pick({
                    maxFiles: allow_limit,
                    accept: ['doc', 'docx', 'odt', 'pdf'],
                    fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive', 'webcam'],
                    lang: '<?php echo $_SESSION['LANG']; ?>',
                    storeTo: {
                        location: "s3",
                        path: uploadPath,
                        access: 'public',
                        container: bucket_name,
                        region: 'us-east-1'
                    }
                }).then(
                        function (inkBlob) {
                            inkBlob = inkBlob.filesUploaded;
                            if (inkBlob && inkBlob.length > 0) {
                                for (var i = 0; i < inkBlob.length; i++) {
                                    var blob = inkBlob[i];
                                    var fileExt = getFileExtension(blob.filename).toLowerCase();

                                    $('#txtUploadedFilePath').val(blob.key);
                                    $('#txtUploadedFileName').val(blob.filename);
                                    $('#txtUploadedFileMimeType').val(blob.mimetype);
                                    $('#txtUploadedFileSize').val(blob.size);
                                    $('#txtUploadedUrl').val(blob.url);


                                    if (docType == 'MyFilesVideo') {
                                        PostMyFilesVideo(docType, (fileExt == 'mp4' ? true : false));
                                    } else {
                                        var attachment_no = $("#attachment").text();
                                        var attach_new = attachment_no.substring(13, 14);
                                        attachment_no = parseInt(attach_new) + 1;
                                        $("#attachment").text('<?php echo $edtpa_lang['edtpa_attachments'] ?> (' + attachment_no + ')');
                                        PostMyFilesDocument();
                                    }
                                }
                            }
                        }),
                        function (FPError) {
                            var error_desc = '<?php echo $edtpa_lang['edtpa_unkn_error'] ?>';
                            //as per filepicker documentation these are possible two errors
                            if (FPError.code == 101) {
                                error_desc = '<?php echo $edtpa_lang['edtpa_the_user_closed'] ?>';
                            } else if (FPError.code = 151) {
                                error_desc = '<?php echo $edtpa_lang['edtpa_the_file_store'] ?>';
                            }

                            $.ajax({
                                type: 'POST',
                                data: {
                                    type: 'Workspace',
                                    id: '',
                                    error_id: FPError.code,
                                    error_desc: error_desc,
                                    docType: docType,
                                    current_user_id: current_user_id
                                },
                                url: home_url + '/Huddles/logFilePickerError/',
                                success: function (response) {
                                    //Do nothing.
                                },
                                errors: function (response) {
                                    alert('<?php echo $edtpa_lang['edtpa_error_occured'] ?>' + FPError.code);
                                }

                            });
                        }

            }

        }

        function getFileExtension(filename) {

            return filename.substr(filename.lastIndexOf('.') + 1)

        }


        function PostMyFilesDocument(maxfile) {

            showProcessOverlay($('#tskDetail'));
            video_id = $('#video_id').val();
            $.ajax({
                url: home_url + '/MyFiles/addResources',
                method: 'POST',
                dataType: 'json',
                success: function (data) {
                    if(data.success ==false){     
                        alert("You don\'t have access to uploaded file into workspace.");
                        return false;
                    }
                    var dynamic_checklist = '<input type="checkbox" checked="checked" class="select_checkbox" name="selected_videos[]" value="' + data.result.document_id + '"/>';
                    $("#my_computer_resources").append(dynamic_checklist);
                    if (parseInt(data) == 0) {
                        alert('<?php echo $edtpa_lang['edtpa_unable_to_save'] ?>');
                    } else {
                        $.ajax({
                            url: home_url + "/huddles/generate_url/" + data.result.document_id,
                            type: 'POST',
                            // dataType: 'json',
                            success: function (response) {

                                $.ajax({
                                    type: 'POST',
                                    url: home_url + '/Huddles/update_stack_url/' + data.result.document_id + '/' + video_id,
                                    data: {stack_url: $('#txtUploadedUrl').val()},
                                    success: function (response) {
                                        loadAjaxFilter();
                                        if (typeof (data.total_submissions) != 'undefined' && data.total_submissions == 1) {
                                            location.reload();
                                        }
                                    }


                                });


                            },
                            error: function () {
                                alert("<?php $edtpa_lang['edtpa_network_error_occured'] ?>");
                            }
                        });


                    }
                },
                data: {
                    video_title: $('#txtUploadedFileName').val(),
                    video_desc: "",
                    assessment_account_id: <?php echo $edtpa_assessment_account_id; ?>,
                    node_id: $('#current_node').val(),
                    video_url: $('#txtUploadedFilePath').val(),
                    video_file_name: $('#txtUploadedFileName').val(),
                    video_mime_type: $('#txtUploadedFileMimeType').val(),
                    video_file_size: $('#txtUploadedFileSize').val(),
                    video_id: $('#txtCurrentVideoID').val(),
                    url: $('#txtUploadedUrl').val(),
                    rand_folder: "0",
                    req_type :'edtpa'
                }
            });
        }


        function PostMyFilesVideo(doc_type, direct_publish) {
            $.ajax({
                url: home_url + (doc_type == 'MyFilesVideo' ? '/MyFiles/add/1' : '/MyFiles/add/2'),
                method: 'POST',
                success: function (data) {
                    if(data.success ==false){     
                        alert("You don\'t have access to uploaded file into workspace.");
                        return false;
                    }
                    CloseVideoUpload(doc_type);
                    if ($('#flashMessage').length == 0) {
                        $('#main').prepend('<div class="success" id="flashMessage" style="cursor: pointer;"><?php $edtpa_lang['edtpa_sit_tight'] ?>.</div>');
                        $('#flashMessage').click(function () {
                            $(this).css('display', 'none')
                        })
                    } else {
                        var html = $('#flashMessage').html();
                        $('#flashMessage').html(html + '<br/><?php $edtpa_lang['edtpa_sit_tight'] ?>.');
                    }
                },
                data: {
                    video_title: $('#txtUploadedFileName').val(),
                    video_desc: '',
                    assessment_account_id: <?php echo $edtpa_assessment_account_id; ?>,
                    node_id: $('#current_node').val(),
                    video_url: $('#txtUploadedFilePath').val(),
                    video_file_name: $('#txtUploadedFileName').val(),
                    video_mime_type: $('#txtUploadedFileMimeType').val(),
                    video_file_size: $('#txtUploadedFileSize').val(),
                    rand_folder: $('#txtVideoPopupRandomNumber').val(),
                    req_type :'edtpa',
                    direct_publish: (direct_publish == true ? 1 : 0)
                }
            });
        }
    });
    $(document).ready(function (e) {
        loadNodeData(0);
        var current_id = '#edTPA_node_' + $('#current_node').val();
        $(current_id).trigger("click");
        $('#error-msg').on('click', function (e) {
            $(this).html('');
        });

    })

    function loadAjaxFilter() {
        var edtpa_assessment_id = $('#edtpa_assessment_id').val();
        $.ajax({
            url: home_url + '/Edtpa/ajax_assessment_load/' + edtpa_assessment_id,
            method: 'POST',
            dataType: "json",
            success: function (data) {
                $("#dvWorkSpeace").html('');
                $("#dvHuddle").html('');
                var current_index = $('#current_node_index').val();
                nodes_data = data.result;

                loadNodeData(current_index);
                hideProcessOverlay($('#tskDetail'));


            }

        });
    }

    $(document).ready(function (e) {
        loadNodeData(0);
        var current_id = '#edTPA_node_' + $('#current_node').val();
        $(current_id).trigger("click");
        $('#error-msg').on('click', function (e) {
            $(this).html('');
        });

    })


</script>
