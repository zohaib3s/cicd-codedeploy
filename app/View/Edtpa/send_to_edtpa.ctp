<style>
    .adt_thumb .image {
        background: url(/app/img/icons/png.png) no-repeat left top;
        width: 13px;
        height: 15px;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 5px;
        left: 5px;
        background-size: 100%;
    }
    .adt_thumb .adt_video{
        background: url(/app/img/icons/mp4.png) no-repeat left top;
        width: 13px;
        height: 15px;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 5px;
        background-size: 100%;
        left: 5px;
    }

    .adt_thumb .image {
        background: url(/app/img/icons/images.png) no-repeat left top;
        width: 13px;
        height: 15px;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top:5px;
        background-size: 100%;
        left: 5px;
    }

    .adt_thumb .acro {
        background: url(/app/img/icons/pdf.png) no-repeat scroll left top transparent;
        height: 15px;
        margin: 0 9px 0 0;
        overflow: hidden;
        text-indent: -90000px;
        width: 13px;
        float: left;
        padding: 15px;
        position: relative;
        top: 5px;
        background-size: 100%;
        left: 5px;
    }



    .adt_thumb span.wordpress {
        background: url(/app/img/icons/doc.png) no-repeat left top;
        width: 13px;
        height: 15px;
        float: left;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        background-size: 100%;
        float: left;
        padding: 15px;
        position: relative;
        top: 5px;
        left: 5px;
    }

    .adt_thumb span.wordpress {
        background: url(/app/img/icons/doc.png) no-repeat left top;
        width: 13px;
        height: 15px;
        float: left;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 5px;
        left: 5px;
        background-size: 100%;
    }

    .adt_thumb span.x-icon {
        background: url(/app/img/icons/xls.png) no-repeat left top;
        width: 13px;
        height: 15px;
        float: left;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 5px;
        left: 5px;
        background-size: 100%;
    }

    .adt_thumb span.x-icon {
        background: url(/app/img/icons/xls.png) no-repeat left top;
        width: 13px;
        height: 15px;
        float: left;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 5px;
        background-size: 100%;
        left: 5px;
    }

    .modal-content .header {
        margin-bottom: 0px;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
        padding: 12px 20px;
    }

    .modal-footer {
        border-bottom-left-radius: 6px;
        border-top-bottom-radius: 6px;
        padding: 12px 20px !important;
        margin: 12px 0 0 0;
        border: 0;
    }

    .modal-content {
        border-bottom-left-radius: 6px;
        border-bottom-radius: 6px;
    }

    .modal-title {
        font-size: 22px;
        font-family: "Droid Serif";
    }

    .modal-header {
        min-height: 16.42857143px;
        padding: 15px;
        border-bottom: 1px solid #e5e5e5;
        background-color: #f9f9f9;
        padding: 9px 20px;
        border-bottom: 1px solid #e1e2e2;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
        padding: 12px 20px;
    }

    .modal-header .close {
        margin-top: -2px;
        font-size: 25px;
        line-height: 0.6;
        text-shadow: 0 1px 0 rgba(255, 255, 255, 0.3);
        width: 24px;
        height: 24px;
        padding: 0;
        cursor: pointer;
    }

    #auth_key {
        width: 83%;
        margin: 0 auto;
        height: 42px;
        text-align: center;
    }

    .modal-content form {
        text-align: center;
    }

    p.submittedReq {
        margin-top: 15px;
        color: green;
        font-size: 20px;
        font-weight: bold;
    }

    .atch_left {
        padding-top: 0px !important;
    }

    .requestErrorCls ul li {
        list-style: none;
        text-align: left;

    }

    .bread_crum {
        margin-bottom: 20px;
        font-size: 13px;
    }

    .bread_crum a {
        font-weight: normal;
        color: #668dab;
    }

    .bread_crum a:after {
        content: '>';
        margin: 0px 15px;
        color: #757575
    }
    .edtp_submitcls span a{    margin-top: 0px;}
</style>

<?php
$edtpa_lang = $this->Session->read('edtpa_lang');
$total_file_used = round(($total_file_size * .0009765625 * .0009765625), 2);


$file_size_formated = $this->Custom->formatbytes($total_file_size, 'MB');
?>

<div class="edtpa_container">

    <div class="bread_crum">
        <a href="<?php echo $this->base . '/Dashboard/home' ?>"><?php echo $edtpa_lang['edtpa_brd_dashboard']?></a> <a
            href="<?php echo $this->base . '/Edtpa/edtpa' ?>">edTPA <?php echo $edtpa_lang['edtpa_brd_home']?> </a>
        <a href="<?php echo $this->base . '/Edtpa/edtpa_detail/' . $edtpa_assessment_id ?>"><?php echo $edtpa_lang['edtipa_details']?> </a> <?php

          if($_SESSION['LANG'] =='es'){
                echo $assessment_details['EdtpaAssessments']['assessment_name_es'];
            }else{
                 echo $assessment_details['EdtpaAssessments']['assessment_name'];
            }
   
         ?>
    </div>
    
    <h1 style="text-align:center"><?php 
    if($_SESSION['LANG'] =='es'){
         echo $assessment_details['EdtpaAssessments']['assessment_name_es'];
    }else{
         echo $assessment_details['EdtpaAssessments']['assessment_name'];
    }
   

    ?> </h1>
    <?php
     if ($total_file_used > 500): ?>
        <div class="message error"><?php echo $edtpa_lang['edtpa_the_pearson_total_file_size']?></div>
    <?php endif; ?>
    <div class="edtp_submitcls">
        <?php if (isset($candidate_id) && !empty($candidate_id)) { ?>
            <?php
            if (!empty($candidate_details['EdtpaAssessmentAccountCandidates']['edtpa_candidate_auth_key']) && !empty($candidate_details['EdtpaAssessmentAccountCandidates']['status'])) {
                $authDoneCls_style = "block";
                $authNotDoneCls_style = "none";
                $request_submitted_style = "none";
                if ($candidate_details['EdtpaAssessmentAccountCandidates']['status'] == 1) {
                    $requestSubmit_style = 'none';
                    $authNotDoneCls_style = 'inline-block';
                    $request_submitted_style = "none";
                } elseif ($candidate_details['EdtpaAssessmentAccountCandidates']['status'] == 2) {
                    $requestSubmit_style = 'inline-block';
                    $authNotDoneCls_style = 'none';
                    $request_submitted_style = "none";
                } elseif ($candidate_details['EdtpaAssessmentAccountCandidates']['status'] >= 3) {
                    $request_submitted_style = 'inline-block';
                    $authNotDoneCls_style = 'none';
                    $requestSubmit_style = 'none';
                    $request_submitted_style = "none";
                } else {
                    $requestSubmit_style = 'none';
                    $request_submitted_style = "none";
                }
            } else {
                $authDoneCls_style = "none";
                $authNotDoneCls_style = "block";
                $requestSubmit_style = 'inline-block';
                $request_submitted_style = "none";
            }
            ?>

            <div class="authDoneCls" style="display: <?php echo $authDoneCls_style; ?>">
                <div class="requestErrorCls" style="color:red;display: none; clear: both;"></div>
    <!--                <span style="color: #5daf46; float: left;position: relative; padding-top: 15px;padding-bottom: 5px;">Max <Fil></Fil>e Size 500MB</span>-->
                <?php if ($total_file_used < 500): ?>
                    <a href="javascript:void(0);" id="requestSubmit" style="display: <?php echo $requestSubmit_style; ?>; float: right;
                       margin-bottom: 10px;    margin-top: 0;"><?php echo $edtpa_lang['edtpa_transfer_edtpa']?></a>
                   <?php endif; ?>
                <img id="loading_gif" style="margin-top: 20px;display: none;" src="/img/loading.gif">

                <span class="requestSuccessCls" style="margin-top: 30px;color:green;display: none;font-weight: bold;"><?php echo $edtpa_lang['edtpa_assessment_is_schedualed']?></span>
                <span class="requestSubmitted" style="margin-top: 15px;color:green;font-weight: bold;font-size: 18px;display: <?php echo $request_submitted_style; ?>"><?php echo $edtpa_lang['edtpa_submission_requested']?> </span>

            </div>
            <div class="authNotDoneCls" style="display: <?php echo $authNotDoneCls_style; ?>; float: right; ">
                <?php if ($total_file_used < 500): ?>
                    <a href="#" data-toggle="modal" style="float: right; margin-bottom: 10px;" data-target="#authKeyPopup"><?php echo $edtpa_lang['edtpa_verify_key']?></a>
                <?php endif; ?>
            </div>
        <?php } else { ?>
            <p style="color:red;margin-top:10px;"><?php echo $edtpa_lang['edtpa_selected_any_attachments']?></p>
        <?php } ?>
        <?php if ($candidate_details['EdtpaAssessmentAccountCandidates']['status'] == 3) { ?><p class="submittedReq"><?php echo $edtpa_lang['edtpa_submissions']?>
                </p><?php } ?>
    </div>

    <div style="clear:both;"></div>
    <div class="edt_tab_main">


        <?php
        if (!empty($assessment_portfolio_tasks)):
            $i = 0;
            $total_units_sum = 0;
            $calss = '';
            foreach ($assessment_portfolio_tasks as $assessment_portfolio_task) {
                ?>
                <div class="edtp-heading"><b><?php echo $assessment_portfolio_task['short_name'] ?></b>
                    <span><?php echo $assessment_portfolio_task['long_name'] ?></span>
                    <a class="edtp_heading_s" href="<?php echo $assessment_portfolio_task['node_id'] ?>"><img
                            src="/app/img/edtpa/video_edtpa.png"><?php echo $edtpa_lang['edtpa_view_attachments']?> </a>
                </div>
                <div class="edtp-content" style="display:none">
                    <div class="edt_attchment view_ach_cl">
                        <div id="dvattachment">
                            <?php
                            if (!empty($assessment_portfolio_task['documents'])):
                                $attchments_margin = '';
                            else:
                                $attchments_margin = '';
                            endif;
                            ?>
                            <div class="atch_left"><?php echo $edtpa_lang['edtpa_attachments']?></div>
                            <style>
                                .exceeded{
                                    border: 2px solid #e3adad !important;
                                }
                                .notExceeded{
                                    border:none;
                                }
                            </style>
                            <div class="atch_right"
                                 id="dvatt_right_<?php echo $assessment_portfolio_task['node_id'] ?>">
                                     <?php
                                     if (!empty($assessment_portfolio_task['documents'])):

                                         foreach ($assessment_portfolio_task['documents'] as $attachment):
                                             if ($attachment['folder_type'] == '3' || $attachment['folder_type'] == 3) {
                                                 $from_where = 'Workspace';
                                             } else if ($attachment['folder_type'] == '1' || $attachment['folder_type'] == 1) {
                                                 $from_where = 'Huddle';
                                             }

                                             $get_file_size = $this->Custom->get_file_size($attachment['document_id']);

                                             if ($get_file_size == 0) {
                                                 $get_file_size = $this->Custom->formatbytes($attachment['document_id'], 'KB');
                                                 $total_units = filter_var($get_file_size, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                                             } else {

                                                 $total_units = (filter_var($get_file_size, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) / 1024);
                                             }

                                             $total_units_in_kb = $this->Custom->get_file_size_in_kb($attachment['document_id']);
                                             $total_units_sum = $total_units_sum + $total_units_in_kb;

                                             if (filter_var($this->Custom->formatbytes($total_units_sum, 'MB'), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION) >= 500) {
                                                 $class = 'exceeded';
                                             } else {
                                                 $class = 'notExceeded';
                                             }

                                             if ($attachment['doc_type'] == '2' || $attachment['doc_type'] == 2) {
                                                 ?>
                                            <div class="adt_thumb <?php echo $class ?>">

                                                <span class="<?php echo $attachment['thumbnail']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;</span>

                                                <div class="adt_desc" >
                                                    <h4><?php echo(strlen($attachment['document_title']) > 25 ? mb_substr($attachment['document_title'], 0, 25) . "..." : $attachment['document_title']); ?></h4>
                                                    <p><?php echo $get_file_size ?></p>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <?php
                                        } else if ($attachment['doc_type'] == '1' || $attachment['doc_type'] == 1) {
                                            ?>
                                            <div class="adt_thumb <?php echo $class ?>" >
                                                <span class="adt_video">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                <div class="adt_desc">
                                                    <h4><?php echo(strlen($attachment['document_title']) > 25 ? mb_substr($attachment['document_title'], 0, 25) . "..." : $attachment['document_title']); ?></h4>
                                                    <p><?php echo $get_file_size ?></p>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <?php
                                        }
                                    endforeach;
                                else:
                                    ?><p><?php echo $edtpa_lang['edtpa_there_are_no_attachments']?></p><?php
                                endif;
                                ?>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>

                </div>
                <?php
                $i++;
            }
        else:
            ?>
           <?php echo $edtpa_lang['edtpa_no_records_found']?> 
        <?php
        endif;
        ?>

        <div class="clear"></div>
    </div>



    <div class="clear"></div>
</div>
<!-- Modal -->
<script>
    function open_edTPATransferTracker_Popup(form) {

        var controlinfo;
        var popName = "edTPA_TransferTracker";
        var popStyle =
                "width=700,height=768,status=yes,resizable=yes,scrollbars=yes";
        form.target = popName;
        controlinfo = window.open('', popName, popStyle);
        controlinfo.focus();
        return;
    }
</script>
<div class="modal fade" id="authKeyPopup" role="dialog">
    <div class="modal-dialog" style="width:400px;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $edtpa_lang['edtpa_verify_key']?></h4>
            </div>
            <div class="modal-body">
                <div style="display: inline-block;padding: 10px;width: 100%;margin: 0 auto;">
                    <input type="text" name="auth_key" id="auth_key" placeholder="<?php echo $edtpa_lang['edtpa_key']?>" style="width:100%"/><br>

                    <form action="<?php echo $tracker_url; ?>" method="post" style="width: 100%;display: block;margin-bottom: 10px;position: relative;height: 24px;">

                        <input type="submit" value="<?php echo $edtpa_lang['edtpa_edtpa_authorization']?>" onclick="open_edTPATransferTracker_Popup(this.form);" style="color: blue;text-decoration: underline;cursor: pointer;"/>
                    </form>

                    <p class="successCls" style="display:none;color:green;"><?php echo $edtpa_lang['edtpa_authorization_successfully']?>.</p>
                    <p class="errorCls" style="display:none;color:red;"><?php echo $edtpa_lang['edtpa_error_in_transmittings']?>. </p>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-actions" style="text-align: right;margin-top:0px;">
                    <input id="varifyBtn" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button" value="<?php echo $edtpa_lang['edtpa_verify']?>" name="commit"
                           class="btn btn-green saveChangesBtn">
                    <input id="cancelBtn" class="btn btn-white cancelCls" data-dismiss="modal" type="reset"
                           value="<?php echo $edtpa_lang['edtpa_cancel']?>" style="font-size: 14px;">
                </div>
            </div>
        </div>

    </div>
</div>

<script>
    var baseurl = '<?php echo $this->base; ?>' + '/';
    $(document).ready(function () {

        $(".edtp_heading_s").click(function () {

            $(this).parent().next(".edtp-content").slideToggle(500);

            return false;
        });
        $(document).on('click', '#varifyBtn', function () {
            var total_used_size = '<?php echo $total_file_used; ?>';
            if (total_used_size > 500) {
                alert('<?php echo trim($edtpa_lang['edtpa_the_pearson_total_file_size'])?>');
                return false;
            }

            $("#auth_key").css({'border': '1px solid #c5c5c5', 'background': '#ffffff'});
            if ($('#auth_key').val() == '') {
                $("#auth_key").css({'border': '1px solid red', 'background': '#fff2f2'});
                return false;
            } else {
                var auth_key = $('#auth_key').val();
                $.ajax({
                    type: 'post',
                    url: baseurl + "Huddles/edtpa_reserve_authorization_key/" + auth_key + "/<?php echo $assessment_code; ?>/<?php echo $assessment_version; ?>",
                    //                    data: {},
                    dataType: 'json',
                    success: function (result) {
                        console.log(result);
                        if (result.success == 'true') {
                            updateCandidateTable(<?php echo $candidate_id; ?>, auth_key);
                        } else {
                            alert('<?php echo trim($edtpa_lang['edtpa_error_in_transmittings'])?>');
                            //$(".errorCls").fadeIn().fadeOut(2000);
                        }
                    },
                    error: function () {
                        //                        alert('error');
                    }
                });
            }
        });
        $(document).on('click', '#requestSubmit', function () {
            var total_used_size = '<?php echo $total_file_used; ?>';
            if (total_used_size > 500) {
                alert('<?php echo trim($edtpa_lang['edtpa_your_total_file_size'])?>');
                return false;
            }
            $('#requestSubmit').css('display', 'none');
            $('#loading_gif').css('display', 'inline-block');
            $(".requestErrorCls").css('display', 'none');
            $('.requestSuccessCls').css('display', 'none');
            $.ajax({
                type: 'post',
                url: baseurl + "Huddles/edtpa_validate_xml/<?php echo $assessment_code; ?>/<?php echo $assessment_version; ?>",
                dataType: 'json',
                success: function (res) {
                    console.log(res);
                    if (res.success == "true") {
                        completeProcess(res.result);
                    } else {
                        if (typeof (res.result.Errors) != 'undefined' && res.result.Errors.constructor === Array) {
                            var errors = "<ul>";
                            $.each(res.result.Errors, function (index, value) {
                                errors += "<li>" + value.Error + "</li>";
                            });
                            errors += "</ul>";
                        } else {
                            var errors = '<p>' + res.result + '</p>';
                        }
                        $(".requestErrorCls").css('display', 'block');
                        $(".requestErrorCls").html(errors);
                        $('#requestSubmit').css('display', 'inline-block');
                        $('#loading_gif').css('display', 'none');
                    }
                },
                error: function () {
                    //$(".requestErrorCls").html('<p>Error in transmitting to edTPA: invalid Authorization Key.</p>');
                    $('#loading_gif').css('display', 'none');
                    $('#requestSubmit').css('display', 'inline-block');
                    alert('<?php echo trim($edtpa_lang['edtpa_error_in_transmittings'])?>');
                }
            });

        });
    });

    function completeProcess(resp) {
        var total_used_size = '<?php echo $total_file_used; ?>';
        if (total_used_size > 500) {
            alert('<?php echo trim($edtpa_lang['edtpa_your_total_file_size'])?>');
            return false;
        }
        $.ajax({
            type: 'post',
            url: baseurl + "Edtpa/request_edtpa_submission/",
            data: {candidate_id: <?php echo $candidate_id; ?>},
            dataType: 'json',
            success: function (result) {
                if (result.status == true) {
                    $('#loading_gif').css('display', 'none');
                    $(".requestSuccessCls").show();
                    setTimeout(function () {
                        location.href = "<?php echo $this->base . '/Edtpa/edtpa'; ?>"
                    }, 4000);
                } else {

                    //$(".requestErrorCls").html('<p>Error in transmitting to edTPA: invalid Authorization Key.</p>');
                    $('#requestSubmit').css('display', 'inline-block');
                    $('#loading_gif').css('display', 'none');
                    alert('<?php echo trim($edtpa_lang['edtpa_error_in_transmittings'])?>');
                }
            },
            error: function () {
                //$(".requestErrorCls").html('<p>Error in transmitting to edTPA: invalid Authorization Key.</p>');
                $('#requestSubmit').css('display', 'inline-block');
                $('#loading_gif').css('display', 'none');
                alert('<?php echo trim($edtpa_lang['edtpa_error_in_transmittings'])?>');
            }
        });
    }

    function updateCandidateTable(candidate_id, auth_key) {
        if (candidate_id != '' && auth_key != '') {
            $.ajax({
                type: 'post',
                url: baseurl + "Edtpa/update_candidate_record/",
                data: {candidate_id: candidate_id, auth_key: auth_key},
                dataType: 'json',
                success: function (result) {
                    if (result.status == true) {
                        $(".successCls").fadeIn().fadeOut(2000);
                        setTimeout(function () {
                            $('#cancelBtn').trigger('click');
                        }, 2000);
                        $('.authNotDoneCls').css('display', 'none');
                        $('.authDoneCls').css('display', 'block');
                    } else {
                        // $(".errorCls").fadeIn().fadeOut(2000);
                        $('.authNotDoneCls').css('display', 'block');
                        $('.authDoneCls').css('display', 'none');
                        alert('<?php echo trim($edtpa_lang['edtpa_error_in_transmittings'])?>');
                    }
                },
                error: function () {
                    //$(".errorCls").fadeIn().fadeOut(2000);
                    alert('<?php echo trim($edtpa_lang['edtpa_error_in_transmittings'])?>');
                }
            });
        }
    }
</script>

