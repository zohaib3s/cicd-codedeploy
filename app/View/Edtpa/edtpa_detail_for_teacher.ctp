<!doctype html>
<?php
$user = $this->Session->read('user_current_account');
$user_id = $user['User']['id'];
$account_id = $user['accounts']['account_id'];
$edtpa_lang = $this->Session->read('edtpa_lang');
?>
<input id="account_id" type="hidden" value="<?php echo $account_id ?>">
<input id="user_id" type="hidden" value="<?php echo $user_id ?>">
<input id="huddle_id" type="hidden" value="">
<input id="video_id" type="hidden" value="0">
<input id="edtpa_assessment_id" type="hidden" value="<?php echo $edtpa_assessment_id ?>"/>
<input id="txtUploadedFilePath" type="hidden" value=""/>
<input id="txtUploadedFileName" type="hidden" value=""/>
<input id="txtUploadedFileMimeType" type="hidden" value=""/>
<input id="txtUploadedFileSize" type="hidden" value=""/>
<input id="txtUploadedDocType" type="hidden" value="video"/>
<input id="uploadPath" type="hidden" value="<?php echo "/tempupload/$account_id/" . date('Y/m/d/'); ?>"/>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function (e) {
        loadNodeData(0);
        var current_id = '#edTPA_node_' + $('#current_node').val();
        $(current_id).trigger("click");
        $('#error-msg').on('click', function (e) {
            $(this).html('');
        });

    })
    var nodes_data =<?php echo json_encode($assessment_portfolio_tasks); ?>;
    $(document).ready(function () {
        $('.arrow-upe').hide();
        $('.arrow-downe').show();
        $("#huddlesOuter").find("div.arrow-upe").hide();
        $(document).on('click', '.edtp-heading', function () {
            $(this).next(".edtp-content").slideToggle(500);
            $(this).find(".arrow-upe, .arrow-downe").toggle();
        });

    });
    function loadNodeData(index) {
        $('.edt_tb_placeholder').hide();
        $('#current_node').val(nodes_data[index].node_id);
        $('#current_node_index').val(index);
        $("#spnTaskName").html(nodes_data[index].short_name);
        $("#spnTaskDesc").html(nodes_data[index].long_name);
        $("#spminvideos").html(nodes_data[index].min_number_of_files);
        $("#spmaxvideos").html(nodes_data[index].max_number_of_files);

        //Workspace Area Content
        $("#dvWorkSpeace").html('<p><?php $edtpa_lang['edtpa_videos_documents'] ?></p>');
        $('#dvHuddle').html('');

        var workspace_html = "";
        var huddles_html = "";
        var documents_array = nodes_data[index].documents;
        if (documents_array.length > 0) {
            $("#dvHuddle").html('');
            $('#dvWorkSpeace').html('');
            var workspace_videos = "";
            var workspace_docs = "";
            var huddles_videos = "";
            var huddles_docs = "";
            $.each(documents_array, function (index, value) {
             if (value.document_title && value.document_title != null) {
                console.log(value);
                var title = value.document_title;
                var chars = title.length;
                var limited_title;
                if (chars > 25) {
                    limited_title = title.substring(0, 30) + "...";
                } else {
                    limited_title = value.document_title;
                }
                if (value.folder_type == '3' || value.folder_type == 3) {
//                    alert(value.folder_type + ' ' + value.doc_type);
                    if (value.doc_type == '2' || value.doc_type == 2) {
                        workspace_docs += '<div class="adt_thumb2 1" style="position: relative;">';
                        if (value.redirect_url == false) {
                            workspace_docs += '<div class="adt_video2"><a href="javascript:void(0);"><span class="' + value.thumbnail + '">&nbsp;&nbsp;&nbsp;&nbsp;</span></a></div>';
                            workspace_docs += '<div class="adt_desc2"><a href="javascript:void(0);"><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?>  ' + value.document_date + '</span></p></div>';
                        } else {
                            workspace_docs += '<div class=" adt_video2"><a href=' + value.redirect_url + '  target="_blank"><span class="' + value.thumbnail + '">&nbsp;&nbsp;&nbsp;&nbsp;</span></a></div>';
                            workspace_docs += '<div class="adt_desc2"><a href=' + value.redirect_url + '  target="_blank"><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p></div>';
                        }

                        workspace_docs += '<div class="clear"></div></div>';
                    } else if (value.doc_type == '1' || value.doc_type == 1) {
                        video_url = value.video_url;
                        workspace_videos += '<div class="adt_thumb2 21" style="position: relative;">';
                        if (video_url == false) {
                            workspace_videos += '<div class="adt_video3"><a class="video-view-box1" video-url-attr="' + video_url + '" video_title="' + title + '" href="javascript:void(0);" ><img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                            workspace_videos += '<div class="adt_desc2"><a  class="video-view-box1" video-url-attr="' + video_url + '" video_title="' + title + '" href="javascript:void(0);"><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p> </div>';
                        } else {
                            workspace_videos += '<div class="adt_video3"><a class="video-view-box" video-url-attr="' + video_url + '" video_title="' + title + '" href="#"  target="_blank"><img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                            workspace_videos += '<div class="adt_desc2"><a  class="video-view-box" video-url-attr="' + video_url + '" video_title="' + title + '" href="#"   target="_blank"><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p> </div>';
                        }

                        workspace_videos += '<div class="clear"></div></div>';

                    } else if (value.doc_type == '3' || value.doc_type == 3) {
                        video_url = value.video_url;
                        workspace_videos += '<div class="adt_thumb2 3" style="position: relative;">';
                        if (video_url == false) {
                            workspace_videos += '<div class="adt_video3"><a class="video-view-box1" video-url-attr="' + video_url + '" video_title="' + title + '" href="javascript:void(0);" target="_blank"><img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                            workspace_videos += '<div class="adt_desc2"><a class="video-view-box1" video-url-attr="' + video_url + '" video_title="' + title + '" href="javascript:void(0);" ><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?>' + value.document_date + '</span></p></div>';
                        } else {
                            workspace_videos += '<div class="adt_video3"><a class="video-view-box" video-url-attr="' + video_url + '" video_title="' + title + '" href="#" target="_blank"><img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                            workspace_videos += '<div class="adt_desc2"><a class="video-view-box" video-url-attr="' + video_url + '" video_title="' + title + '" href="#" ><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?>' + value.document_date + '</span></p></div>';
                        }
                        workspace_videos += '<div class="clear"></div></div>';
                    }
                } else if (value.folder_type == '1' || value.folder_type == 1) {
//                    alert(value.folder_type + '' + value.doc_type);
                    if (value.doc_type == '2' || value.doc_type == 2) {
                        huddles_docs += '<div class="adt_thumb2 4" style="position: relative;">';
                        if (redirect_url == false) {
                            huddles_docs += '<div class="adt_video2"><a href="javascript:void(0);"><span class="' + value.thumbnail + '">&nbsp;&nbsp;&nbsp;&nbsp;</span></a></div>';
                            huddles_docs += '<div class="adt_desc2"><a href="javascript:void(0);"><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p></div>';
                        } else {
                            huddles_docs += '<div class="adt_video2"><a href=' + value.redirect_url + '  target="_blank"><span class="' + value.thumbnail + '">&nbsp;&nbsp;&nbsp;&nbsp;</span></a></div>';
                            huddles_docs += '<div class="adt_desc2"><a href=' + value.redirect_url + '  target="_blank"><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p></div>';
                        }

                        huddles_docs += '<div class="clear"></div></div>';
                    } else if (value.doc_type == '1' || value.doc_type == 1) {
                        video_url = value.video_url;
                        huddles_videos += '<div class="adt_thumb2 5" style="position: relative;">';
                        if (video_url == false) {
                            huddles_videos += '<div class="adt_video3 6"><a  class="video-view-box1" video-url-attr="' + video_url + '" video_title="' + title + '" href="javascript:void(0);"><img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                            huddles_videos += '<div class="adt_desc2"><a class="video-view-box1" video-url-attr="' + video_url + '" video_title="' + title + '" href="javascript:void(0);" ><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p></div>';
                        } else {
                            huddles_videos += '<div class="adt_video3 6"><a  class="video-view-box" video-url-attr="' + video_url + '" video_title="' + title + '" href="#"><img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                            huddles_videos += '<div class="adt_desc2"><a class="video-view-box" video-url-attr="' + video_url + '" video_title="' + title + '" href="#" ><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p></div>';
                        }

                        huddles_videos += '<div class="clear"></div></div>';
                    } else if (value.doc_type == '3' || value.doc_type == 3) {
                        video_url = value.video_url;
                        if (value.redirect_url == false) {
                            huddles_videos += '<a href="#"><div class="adt_thumb3 7" style="position: relative;">';
                        } else {
                            huddles_videos += '<a target="_blank"  href=' + value.redirect_url + '><div class="adt_thumb3 7" style="position: relative;">';
                        }

                        if (video_url == false) {
                            huddles_videos += '<div class="adt_video2"><a class="video-view-box1" video-url-attr="' + video_url + '" video_title="' + title + '" href="javascript:void(0);" ><img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                            huddles_videos += '<div class="adt_desc2"><a class="video-view-box2" video-url-attr="' + video_url + '" video_title="' + title + '" href="javascript:void(0);" ><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p></div>';
                        } else {
                            huddles_videos += '<div class="adt_video2"><a class="video-view-box" video-url-attr="' + video_url + '" video_title="' + title + '" href="#" ><img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                            huddles_videos += '<div class="adt_desc2"><a class="video-view-box" video-url-attr="' + video_url + '" video_title="' + title + '" href="#" ><h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p></div>';
                        }



                        huddles_videos += '<div class="clear"></div></div>';
                    }
                }
              }
            });
            workspace_html = workspace_videos + workspace_docs + huddles_videos + huddles_docs;
            // huddles_html = huddles_videos + huddles_docs;
            if (workspace_html !== "") {
                $("#dvWorkSpeace").html(workspace_html);
            }
            // if (huddles_html !== "") {
            // $("#dvWorkSpeace").html(huddles_html);
            //}
        }

        $("#btnVideo").hide();
        $(".resources_dropDown").hide();
//        $('.resources_dropDown').prop('onclick', null).off('click');
        $('#btnVideo').prop('onclick', null).off('click');
        var doc_type = nodes_data[index].evidence_type_code;
        var minfile = nodes_data[index].min_number_of_files;
        var maxfile = nodes_data[index].max_number_of_files;
        console.log(nodes_data[index]);
        $('#required-attachment').html('<h6><?php echo $edtpa_lang['edtpa_required_attachment'] ?>: ' + maxfile + '</h6>');
        //alert(doc_type);
        if (doc_type == 'DOCUMENT') {     
            html_des = '<div><?php echo $edtpa_lang['edtpa_details_for_teacher_msg'] ?></div>';       
            html_des += '<ol>';            
            html_des += '<li> <span style="color:red;"><?php echo $edtpa_lang['edtpa_recommended_file_type'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_resource_click'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_add_resources'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_the_resources_like'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_the_resources_selected_will'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_upload_computer'] ?></li>';
            html_des += '</ol>';
            $('#edtpa_instructions').html(html_des);
            $(".resources_dropDown").show();
        } else if (doc_type == 'VIDEO') {
            html_des = '<ol>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_click_on_the_video'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_dialog_box'] ?></li>';
            //html_des += '<li>You can select a minimum of ' + minfile + ' video(s) and a maximum of ' + maxfile + ' video(s) from your Workspace and Huddles.</li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_video'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_video_you_selected'] ?></li>';
            html_des += '</ol>';
            $('#edtpa_instructions').html(html_des);
            $("#btnVideo").show();
        } else {
            html_des = '<div><?php echo $edtpa_lang['edtpa_video_you_selected'] ?></div>';
            html_des = '<div><?php echo $edtpa_lang['edtpa_details_for_teacher_msg'] ?></div>'; 
            html_des += '<ol>';
            html_des += '<li><span style="color:red;"><?php echo $edtpa_lang['edtpa_recommended_file_type'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_resource_click'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_add_resources'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_the_resources_like'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_the_resources_selected_will'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_upload_computer'] ?></li>';
            html_des += '</ol>';
            html_des += '<div><?php echo $edtpa_lang['edtpa_uploading_videos'] ?></div>';
            html_des += '<ol>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_click_on_the_video'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_dialog_box'] ?></li>';
            // html_des += '<li>You can select a minimum of minimum of ' + minfile + ' video(s) and a maximum of ' + maxfile + ' video(s) from your Workspace and Huddles.</li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_selected_video'] ?></li>';
            html_des += '<li><?php echo $edtpa_lang['edtpa_video_you_selected'] ?></li>';
            html_des += '</ol>';
            $('#edtpa_instructions').html(html_des);
            $("#btnVideo").show();
            $(".resources_dropDown").show();
        }
        var i, x, tablinks;
        x = document.getElementsByClassName("tab_edt");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < x.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
        }
        document.getElementById('tskDetail').style.display = "block";
        var selected_elements_arr = nodes_data[index].documents_arr;
        $('.select_checkbox').each(function (ind, val) {
            $(this).prop('checked', false);
        });
        if (typeof (selected_elements_arr) != 'undefined' && selected_elements_arr != null && selected_elements_arr.length > 0) {
            $('.select_checkbox').each(function (ind, val) {
                if ($.inArray($(this).val(), selected_elements_arr) !== -1) {
                    $(this).prop('checked', true);
                }
            });
        }
    }
    function loadNodeData_old(index) {
        $('.edt_tb_placeholder').hide();
        $('#current_node').val(nodes_data[index].node_id);
        $('#current_node_index').val(index);
        $("#spnTaskName").html(nodes_data[index].short_name);
        $("#spnTaskDesc").html(nodes_data[index].long_name);
        $("#spminvideos").html(nodes_data[index].min_number_of_files);
        $("#spmaxvideos").html(nodes_data[index].max_number_of_files);

        //Workspace Area Content
        $("#dvWorkSpeace").html('<p>There are no videos or documents.</p>');
        $('#dvHuddle').html('');

        var workspace_html = "";
        var huddles_html = "";
        var documents_array = nodes_data[index].documents;
        if (documents_array.length > 0) {
            $("#dvHuddle").html('');
            $('#dvWorkSpeace').html('');
            var workspace_videos = "";
            var workspace_docs = "";
            var huddles_videos = "";
            var huddles_docs = "";
            $.each(documents_array, function (index, value) {
                if (value.document_title && value.document_title != null) {
                console.log(value);
                var title = value.document_title;
                var chars = title.length;
                var limited_title;
                if (chars > 25) {
                    limited_title = title.substring(0, 30) + "...";
                } else {
                    limited_title = value.document_title;
                }
                if (value.folder_type == '3' || value.folder_type == 3) {
//                    alert(value.folder_type + ' ' + value.doc_type);
                    if (value.doc_type == '2' || value.doc_type == 2) {
                        workspace_docs += '<div class="adt_thumb2 1" style="position: relative;">';
                        // workspace_docs += '<div style="position: absolute;right: -10px;top: -14px;"></div>';
                        workspace_docs += '<div class=" adt_video2">';
                        workspace_docs += '<a href=' + value.redirect_url + '  target="_blank">';
                        workspace_docs += '<span class="' + value.thumbnail + '">&nbsp;&nbsp;&nbsp;&nbsp;</span></a></div>';
                        workspace_docs += '<div class="adt_desc2">';
                        workspace_docs += '<a href=' + value.redirect_url + '  target="_blank">';
                        workspace_docs += '<h4>' + limited_title + '</h4>';
                        workspace_docs += '</a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p></div>';
                        workspace_docs += '<div class="clear"></div></div>';
                    } else if (value.doc_type == '1' || value.doc_type == 1) {
                        workspace_videos += '<div class="adt_thumb2 21" style="position: relative;">';
                        //workspace_videos += '<div style="position: absolute;right: -10px;top: -14px;"></div>';
                        workspace_videos += '<div class="adt_video3">';
                        workspace_videos += '<a href=' + value.redirect_url + '  target="_blank">';
                        workspace_videos += '<img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                        workspace_videos += '<div class="adt_desc2">';
                        workspace_videos += '<a href=' + value.redirect_url + '  target="_blank">';
                        workspace_videos += '<h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p> </div>';
                        workspace_videos += '<div class="clear"></div></div>';

                    } else if (value.doc_type == '3' || value.doc_type == 3) {
                        workspace_videos += '<div class="adt_thumb2 3" style="position: relative;">';
                        workspace_videos += '<div class="adt_video3">';
                        workspace_videos += '<a href=' + value.redirect_url + '  target="_blank">';
                        workspace_videos += '<img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a>';
                        workspace_videos += '<div class="adt_desc2">';
                        workspace_videos += '<a href=' + value.redirect_url + '  target="_blank">';
                        workspace_videos += '<h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?>' + value.document_date + '</span></p></div>';
                        workspace_videos += '<div class="clear"></div></div>';
                    }
                } else if (value.folder_type == '1' || value.folder_type == 1) {
//                    alert(value.folder_type + '' + value.doc_type);
                    if (value.doc_type == '2' || value.doc_type == 2) {
                        huddles_docs += '<div class="adt_thumb2 4" style="position: relative;">';
                        //huddles_docs += '<div style="position: absolute;right: -10px;top: -14px;"> </div>';
                        huddles_docs += '<div class="adt_video2">';
                        if (value.redirect_url == false) {
                            huddles_docs += '<a href="#">';
                        } else {
                            huddles_docs += '<a href=' + value.redirect_url + '  target="_blank">';
                        }

                        huddles_docs += '<span class="' + value.thumbnail + '">&nbsp;&nbsp;&nbsp;&nbsp;</span></a></div>';
                        huddles_docs += '<div class="adt_desc2">';
                        huddles_docs += '<div class="adt_desc2">';
                        if (value.redirect_url == false) {
                            huddles_docs += '<a href="#">';
                        } else {
                            huddles_docs += '<a href=' + value.redirect_url + '  target="_blank">';
                        }

                        huddles_docs += '<h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p></div>';
                        huddles_docs += '<div class="clear"></div></div>';
                    } else if (value.doc_type == '1' || value.doc_type == 1) {
                        huddles_videos += '<div class="adt_thumb2 5" style="position: relative;">';
                        huddles_videos += '<div class="adt_video3 6">';
                        if (value.redirect_url == false) {
                            huddles_docs += '<a href="#">';
                        } else {
                            huddles_videos += '<a  target="_blank" href=' + value.redirect_url + '>';
                        }

                        huddles_videos += '<img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                        huddles_videos += '<div class="adt_desc2">';
                        if (value.redirect_url == false) {
                            huddles_docs += '<a href="#">';
                        } else {
                            huddles_videos += '<a  target="_blank" href=' + value.redirect_url + '>';
                        }

                        huddles_videos += '<h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> ' + value.document_date + '</span></p></div>';
                        huddles_videos += '<div class="clear"></div></div>';
                    } else if (value.doc_type == '3' || value.doc_type == 3) {
                        if (value.redirect_url == false) {
                            huddles_docs += '<a href="#">';
                        } else {
                            huddles_videos += '<a target="_blank"  href=' + value.redirect_url + '>';
                        }

                        huddles_videos += '<div class="adt_thumb3 7" style="position: relative;"><div class="adt_video2">';
                        if (value.redirect_url == false) {
                            huddles_docs += '<a href="#">';
                        } else {
                            huddles_videos += '<a  target="_blank" href=' + value.redirect_url + '>';
                        }

                        huddles_videos += '<img src="' + value.thumbnail + '"><div class="play-icon" style="background-size: 50px;height: 50px;position: absolute;left: 11%;width: 50px;"></div></a></div>';
                        huddles_videos += '<div class="adt_desc2">';
                        if (value.redirect_url == false) {
                            huddles_docs += '<a href="#">';
                        } else {
                            huddles_videos += '<a  target="_blank" href=' + value.redirect_url + '>';
                        }

                        huddles_videos += '<h4>' + limited_title + '</h4></a><p><?php echo $edtpa_lang['edtpa_by'] ?>  <a href="javascript:void(0);">' + value.document_by + '</a>&nbsp;<span><?php echo $edtpa_lang['edtpa_upload'] ?> \' + value.document_date + \'</span></p></div>';
                        huddles_videos += '<div class="clear"></div></div>';
                    }
                 }
                }
            });
            workspace_html = workspace_videos + workspace_docs;
            huddles_html = huddles_videos + huddles_docs;
            if (workspace_html !== "") {
                $("#dvWorkSpeace").html(workspace_html);
            }
            if (huddles_html !== "") {
                $("#dvHuddle").html(huddles_html);
            }
        }

        $("#btnVideo").hide();
        $(".resources_dropDown").hide();
//        $('.resources_dropDown').prop('onclick', null).off('click');
        $('#btnVideo').prop('onclick', null).off('click');
        var doc_type = nodes_data[index].evidence_type_code;
        var minfile = nodes_data[index].min_number_of_files;
        var maxfile = nodes_data[index].max_number_of_files;
        $('#required-attachment').html('<h6>Required attachments: ' + maxfile + '</h6>');
        //alert(doc_type);
        if (doc_type == 'DOCUMENT') {
            html_des = '<ol>';
            html_des += '<li>Click on the Resources icon above.</li>';
            html_des += '<li>If you select Add From Resources, a dialog box will appear with all resources available in your Workspace and Huddles.</li>';
            html_des += '<li>Select the resources you\'d like to include and click the Save Changes button.</li>';
            html_des += '<li>The resources you\'ve selected will appear at the bottom of the page.</li>';
            html_des += '<li>If you selected Upload From Computer, follow the on-screen instructions that appear in the popup window.</li>';
            html_des += '</ol>';
            $('#edtpa_instructions').html(html_des);
            $(".resources_dropDown").show();
        } else if (doc_type == 'VIDEO') {
            html_des = '<ol>';
            html_des += '<li>Click on the Video icon above.</li>';
            html_des += '<li>A dialog box will appear with all the videos you\'ve stored in your Workspace and Huddles.</li>';
            //html_des += '<li>You can select a minimum of ' + minfile + ' video(s) and a maximum of ' + maxfile + ' video(s) from your Workspace and Huddles.</li>';
            html_des += '<li>Select the video(s) you\'d like to include and click the Save Changes button.</li>';
            html_des += '<li>The videos you\'ve selected will appear at the bottom of the page.</li>';
            html_des += '</ol>';
            $('#edtpa_instructions').html(html_des);
            $("#btnVideo").show();
        } else {
            html_des = '<div>Uploading Resources</div>';
            html_des += '<ol>';
            html_des += '<li>Click on the Resources icon above.</li>';
            html_des += '<li>If you select Add From Resources, a dialog box will appear with all resources available in your Workspace and Huddles.</li>';
            html_des += '<li>Select the resources you\'d like to include and click the Save Changes button.</li>';
            html_des += '<li>The resources you\'ve selected will appear at the bottom of the page.</li>';
            html_des += '<li>If you selected Upload From Computer, follow the on-screen instructions that appear in the popup window.</li>';
            html_des += '</ol>';
            html_des += '<div>Uploading Videos</div>';
            html_des += '<ol>';
            html_des += '<li>Click on the Video icon above.</li>';
            html_des += '<li>A dialog box will appear with all the videos you\'ve stored in your Workspace and Huddles.</li>';
            // html_des += '<li>You can select a minimum of minimum of ' + minfile + ' video(s) and a maximum of ' + maxfile + ' video(s) from your Workspace and Huddles.</li>';
            html_des += '<li>Select the video(s) you\'d like to include and click the Save Changes button.</li>';
            html_des += '<li>The videos you\'ve selected will appear at the bottom of the page.</li>';
            html_des += '</ol>';
            $('#edtpa_instructions').html(html_des);
            $("#btnVideo").show();
            $(".resources_dropDown").show();
        }
        var i, x, tablinks;
        x = document.getElementsByClassName("tab_edt");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < x.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" w3-red", "");
        }
        document.getElementById('tskDetail').style.display = "block";
        var selected_elements_arr = nodes_data[index].documents_arr;
        $('.select_checkbox').each(function (ind, val) {
            $(this).prop('checked', false);
        });
        if (selected_elements_arr.length > 0) {
            $('.select_checkbox').each(function (ind, val) {
                if ($.inArray($(this).val(), selected_elements_arr) !== -1) {
                    $(this).prop('checked', true);
                }
            });
        }
    }

    $(document).ready(function () {
        $(document).on('click', '.select_checkbox', function () {
            var doc_type = $(this).data('doc_type');
            var doc_extension = $(this).data('document_extension');
            var allowed_extensions;
            var error_msg;
            if (doc_type == '2' || doc_type == 2) {
                allowed_extensions = ['doc', 'docx', 'odt', 'pdf'];
                error_msg = 'Invalid file type, only following file types are allowed :' + " \n" + 'doc, docx, odt, pdf';
            } else if (doc_type == '1' || doc_type == 1) {
                allowed_extensions = ['flv', 'asf', 'qt', 'mov', 'mpg', 'mpeg', 'avi', 'wmv', 'mp4', 'm4v'];
                error_msg = 'Invalid file type, only following file types are allowed :' + " \n" + 'flv, asf, qt, mov, mpg, mpeg, avi, wmv, mp4, m4v';
            } else if (doc_type == '3' || doc_type == 3) {
                allowed_extensions = ['flv', 'asf', 'qt', 'mov', 'mpg', 'mpeg', 'avi', 'wmv', 'mp4', 'm4v'];
                error_msg = 'Invalid file type, only following file types are allowed :' + " \n" + 'flv, asf, qt, mov, mpg, mpeg, avi, wmv, mp4, m4v';
            }
            if ($(this).is(':checked')) {
                var check_its_extension = validateFileType(doc_extension, allowed_extensions);
                if (check_its_extension === false) {
                    alert(error_msg);
                    $(this).prop("checked", false);
                }
            }
            var values = $('input:checkbox:checked.select_checkbox').map(function () {
                return this.value;
            }).get();
            var current_node_index = $('#current_node_index').val();
            var current_node_details = nodes_data[current_node_index];
            var maxfile = current_node_details.max_number_of_files;
            if (values.length > maxfile) {
                alert('You have already seleted max number of allowed files.');
                $(this).prop("checked", false);
                values.splice(-1, 1);
            }
        });
        $(document).on('click', '.saveChangesBtn', function () {
//            var nodes_data = <?php // echo json_encode($assessment_portfolio_tasks);                                                                                                                                                                                                                                                                                                             ?>;
            var current_node_index = $('#current_node_index').val();
            var current_node_details = nodes_data[current_node_index];
            var selected_elements_arr = nodes_data[current_node_index].documents_arr;
            var documents_data_arr = nodes_data[current_node_index].documents;
            var checked_checkboxes = $('input:checkbox:checked.select_checkbox');
            checked_checkboxes.each(function (ind, val) {
                var document_id = String($(this).data('document_id'));
                var document_title = $(this).data('document_title');
                var folder_type = String($(this).data('folder_type'));
                var document_by = $(this).data('document_by');
                var doc_type = String($(this).data('doc_type'));
                var thumbnail = $(this).data('thumbnail');
                var document_date = $(this).data('document_date');
                if (selected_elements_arr.length < current_node_details.max_number_of_files) {
                    if ($.inArray($(this).val(), selected_elements_arr) === -1) {
                        var new_file = {
                            doc_type: doc_type,
                            document_by: document_by,
                            document_date: document_date,
                            document_id: document_id,
                            document_title: document_title,
                            folder_type: folder_type,
                            thumbnail: thumbnail
                        };
                        documents_data_arr.push(new_file);
                        selected_elements_arr.push(document_id);
                    }
                    loadNodeData(current_node_index);
                }
            });
            //            return false;
            $('.saveChangesBtn').attr('disabled', 'disabled');
            var values = $('input:checkbox:checked.select_checkbox').map(function () {
                return this.value;
            }).get();
            fn(values);
        });
        $(document).on('click', '.tablink', function () {
            $('.tablink').removeClass('currentTablink');
            $(this).addClass('currentTablink');
            $.ajax({
                type: 'POST',
                url: home_url + '/Edtpa/refresh_tab',
                dataType: 'json',
                data: {node_id: $('#current_node').val()},
                success: function (data) {

                }
            });
        });
    });
    $(document).on('click', '.close', function (e) {
        e.preventDefault();
        $('#video-modal').html('<div>Your browser doesn`t support HTML5 video tag.</div>');
        jQuery.noConflict();
        $('#myModal').modal('hide');
    });

    $(document).on('click', '.video-view-box', function (e) {
        e.preventDefault();
        var video_url = $(this).attr('video-url-attr');
        var video_title = $(this).attr('video_title');
        $('#myModalLabel').html(video_title);
        var video_html = '';
        video_html = '<video controls id="video1" style="width: 100%; height: auto; margin:0 auto; frameborder:0;" ><source src="' + video_url + '" type="video/mp4">';
        video_html += 'Your browser doesn`t support HTML5 video tag.';
        video_html += '</video>';
        $('#video-modal').html(video_html);
         jQuery.noConflict();
        $('#myModal').modal('show');
    });
    $(document).on('click', '.removeItem', function () {
        var r = confirm("Are you sure! you want to delete this attachment?");
        if (r == true) {
            var index_to_remove = $(this).data('index');
            var current_node_index = $('#current_node_index').val();
            var current_node_details = nodes_data[current_node_index];
            var selected_elements_arr = nodes_data[current_node_index].documents_arr;
            var documents_data_arr = nodes_data[current_node_index].documents;
            selected_elements_arr.splice(index_to_remove, 1);
            documents_data_arr.splice(index_to_remove, 1);
            $(this).closest("div.adt_thumb2").remove();
            loadNodeData(current_node_index);
            var values = $('input:checkbox:checked.select_checkbox').map(function () {
                return this.value;
            }).get();
            fn(values);
        } else {

        }
    });
    var xhr;
    var fn = function (filter) {
        if (xhr && xhr.readyState != 4) {
            xhr.abort();
        }
        xhr = $.ajax({
            type: 'POST',
            url: home_url + '/Edtpa/save_selected_files',
            dataType: 'json',
            data: {
                selected_files: filter,
                assessment_account_id: <?php echo $edtpa_assessment_account_id; ?>,
                node_id: $('#current_node').val()
            },
            success: function (data) {
                if (data.status == true) {
                    $('.saveChangesBtn').removeAttr("disabled");
                    $(".successCls").fadeIn().fadeOut("slow");
                    //Remove items from nodes_data which are uncheched
                    var current_node_index = $('#current_node_index').val();
                    var current_node_details = nodes_data[current_node_index];
                    var selected_elements_arr = nodes_data[current_node_index].documents_arr;
                    var documents_data_arr = nodes_data[current_node_index].documents;
                    console.log(selected_elements_arr);
                    var i = 0;
                    $.each(selected_elements_arr, function (index, value) {
                        if ($.inArray(value, filter) === -1) {
                            selected_elements_arr.splice(i, 1);
                            documents_data_arr.splice(i, 1);
                        }
                        i++;
                    });
                    loadNodeData(current_node_index);

                    setTimeout(function () {
                        $('#edtpa_modal').trigger('click');
                    }, 1000);
                    setTimeout(function () {
                        $('#edtpaResources_modal').trigger('click');
                    }, 1000);
                } else {
                    $(".errorCls").fadeIn().fadeOut("slow");
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }

    function sendToEdtpa(url) {
        var errors = "";
        $.each(nodes_data, function (index, value) {
            if (value.min_number_of_files > value.documents_arr.length) {
                errors += value.short_name + ' : Number of required files, ' + value.min_number_of_files + "<br/>";
            }
        });
        //        errors = "";
        if (errors == '') {
            window.location.href = url;
        } else {
            $('#error-msg').html('<div class="message error">' + errors + '</div>');

        }
    }

    function validateFileType(file, valid_types) {
        //        var extension = file.name.replace(/^.*\./, '');
        var extension = file;

        extension = extension.toLowerCase();
        if ($.inArray(extension, valid_types) == -1) {
            return false;
        } else {
            return true;
        }
    }
</script>

<script>
    var baseurl = '<?php echo $this->base; ?>' + '/';
</script>
<script>
    $(document).ready(function () {

        $(".resources_dropDown").click(function () {
            $(".resources_dropDownInner").toggle();
        });

    });

    function save_selected_files(selected_files) {

        console.log(selected_files);
        $.ajax({
            type: 'POST',
            url: home_url + '/Edtpa/save_selected_files',
            dataType: 'json',
            data: {selected_files: selected_files},
            success: function (response) {
                console.log(response);
                if (response.status == true) {
                    alert('done');
                } else {
                    alert('failed');
                }
            }
        });
    }
</script>
<style>
.adt_video3{
    float: left;
    width: 117px;
    margin-right: 10px;
    height: 68px;
    overflow: hidden;
}
.play-icon{    width: 40px!important;     height: 40px!important; 
    background-size: 100%!important; }
    #myModal .close {
        vertical-align: top;
        display: inline-block;
        text-align: center;
        font-size: 25px;
        width: 28px;
        height: 31px;
        float: right;
        margin-right: -5px;}
        .adt_video3 .video-view-box {    display: block;
    height: 68px;
    overflow: hidden;
    border-radius: 10px;}
    .adt_ajdacent .adt_video3 { width: 230px !important; }
    .adt_ajdacent .adt_desc3 {width: 35% !important;}
    .adt_ajdacent .adt_video3  a {    white-space: nowrap;
                                      max-width: 168px;
                                      display: inline-block;
                                      overflow: hidden;
                                      text-overflow: ellipsis;
                                      padding-top: 7px;}
    .videosOuterCls .adt_thumb3 {
        position: relative;
    }

    .videosOuterCls .adt_thumb3 .play-icon {
        top: -26px !important;
    }

    p.noDataCls {
        padding-left: 20px;
    }

    .adt_thumb3 .image {
        background: url(/app/img/icons/jpeg96.png) no-repeat left top;
        width: 32px;
        height: 85px;
        background-size: 100%;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
        background-size: 100%;
    }

    .adt_thumb2 .image {
        background: url(/app/img/icons/jpeg96.png) no-repeat left top;
        width: 32px;
        background-size: 100%;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
    }

    .adt_thumb3 .acro {
        background: url(/app/img/icon-img-1.gif) no-repeat left top;
        width: 32px;
        height: 85px;
        background-size: 100%;
        margin: 0 9px 0 0;
        overflow: hidden;
        text-indent: -90000px;

        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
    }

    .adt_thumb2 .acro {
        background: url(/app/img/icon-img-1.gif) no-repeat left top;
        width: 32px;
        height: 55px;
        background-size: 100%;
        margin: 0 9px 0 0;
        overflow: hidden;
        text-indent: -90000px;

        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
    }

    .adt_thumb3 span.wordpress {
        background: url(/app/img/icon-img-2.gif) no-repeat left top;
        width: 32px;
        height: 55px;
        background-size: 100%;

        float: left;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
    }

    .adt_thumb2 span.wordpress {
        background: url(/app/img/icon-img-2.gif) no-repeat left top;
        width: 32px;
        height: 55px;
        background-size: 100%;
        float: left;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
    }

    .adt_thumb3 span.x-icon {
        background: url(/app/img/icon-img-4.gif) no-repeat left top;
        width: 32px;
        height: 55px;
        background-size: 100%;
        float: left;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
    }

    .adt_thumb2 span.x-icon {
        background: url(/app/img/icon-img-4.gif) no-repeat left top;
        width: 32px;
        height: 55px;
        background-size: 100%;
        float: left;
        text-indent: -90000px;
        overflow: hidden;
        margin: 0 9px 0 0;
        float: left;
        padding: 15px;
        position: relative;
        top: 8px;
        left: 5px;
    }

    #edtpaDocs_modal .adt_desc3 p {
        margin-bottom: 2px;
    }

    #dvHuddle .play-icon {
        left: 17% !important;
        top: 75px !important;
    }

    #dvHuddle .play-icon {
        left: 17% !important;
        top: 75px !important;
    }

    #dvWorkSpeace .play-icon {
        left: 17% !important;
        top: 75px !important;
    }

    .modal-content .header {
        margin-bottom: 0px;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
        padding: 12px 20px;
    }

    .modal-footer {
        border-bottom-left-radius: 6px;
        border-top-bottom-radius: 6px;
        padding: 12px 20px !important;
        margin: 12px 0 0 0;
    }

    .modal-content {
        border-bottom-left-radius: 6px;
        border-bottom-radius: 6px;
    }

    .tab-content {
        padding: 20px 13px 11px 13px !important;
    }

    .adt_desc3 {
        width: 56% !important;
    }

    .tablink {
        position: relative;
        outline: none;
    }

    .tablink i {
        display: none;
        width: 0;
        height: 0;
        border-top: 10px solid transparent;
        border-bottom: 10px solid transparent;
        border-left: 10px solid #fff;
        position: absolute;
        right: -11px;
        top: 17%;
    }
    .edt_tab_name button{
        width: 100%;
        margin: 10px 0%;
        padding-bottom: 10px;
        padding: 12px;
    }

    .edt_tab_name button.currentTablink{
        background: #ffffff;
    }

    .currentTablink i {
        display: inline-block;
    }

    .edtpa_container h1 a {
        font-size: 13px;
    }

    .resources_dropDown {
        background: #5daf46;
        display: inline-block;
        padding: 3px 15px 7px 13px;
        color: #fff !important;
        border-radius: 22px;
        font-weight: normal;
        text-decoration: none;
        margin-left: 5px;
        position: relative;
        cursor: pointer;
    }

    .whiteArrow {
        border-left: solid 1px #7dbf6b;
        padding: 13px 0px 12px 12px;
        margin: -8px 0px;
        margin-left: 10px;
    }

    .resources_dropDownInner {
        position: absolute;
        border-radius: 4px;
        box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.1);
        bottom: -64px;
        background: #fff;
        width: 190px;
        padding: 6px;
        left: 0px;
        display: none;
    }

    .tab_content_bar .resources_dropDownInner a {
        display: block;
        color: #616161 !important;
        padding: 3px 0px;
        background: none;
        font-size: 13px
    }

    .bread_crum {
        margin-bottom: 20px;
        font-size: 13px;
    }

    .bread_crum a {
        font-weight: normal;
        color: #668dab;
    }

    .bread_crum a:after {
        content: '>';
        margin: 0px 15px;
        color: #757575
    }

    .adt_video2 {
        float: left;
        width: 35px;
        margin: 5px;
    }

    .adt_thumb2 img {
        width: inherit;
        border-radius: 10px;
    }

    .adt_thumb2 {
        position: relative;
    }

    .adt_thumb2 h4 {
        margin-top: 7px;

    }

    .adt_thumb2 p {
        width: 97% !important;
        white-space: unset !important;
    }

    .student_srcv p {
        margin: 0 0 1px 0 !important;
    }
</style>
<div class="edtpa_container">
     <div class="bread_crum">
        <a href="<?php echo $this->base . '/Dashboard/home' ?>"><?php echo $edtpa_lang['edtpa_brd_dashboard'] ?></a> <a
            href="<?php echo $this->base . '/Edtpa/edtpa' ?>">edTPA <?php echo $edtpa_lang['edtpa_brd_home'] ?> </a> <?php echo $edtpa_lang['edtipa_details'] ?>
    </div>
    <div id="error-msg" style="cursor: pointer;"></div>
    <div class="edt_tab_main">
        <div class="edt_tab_name">
            <input type="hidden" name="current_node" id="current_node" value=""/>
            <input type="hidden" name="current_node_index" id="current_node_index" value=""/>
            <h5><?php echo $edtpa_lang['edtpa_task_list']; ?></h5>
            <?php
            if (!empty($assessment_portfolio_tasks)):
                $i = 0;
                foreach ($assessment_portfolio_tasks

                as $assessment_portfolio_task) {
                    ?>
                    <button class="tablink" id="edTPA_node_<?php echo $assessment_portfolio_task['node_id']; ?>"
                            data-attr-index="<?php echo $i; ?>" data-node="<?php echo $assessment_portfolio_task['node_id']; ?>"
                            onclick="loadNodeData('<?php echo $i; ?>')">
                        <b><?php echo $assessment_portfolio_task['short_name'] ?></b>
                        <?php echo $assessment_portfolio_task['long_name'] ?>
                        <i></i>
                        <?php
                        $i++;
                    }
                else:
                    ?><p><?php echo $edtpa_lang['edtpa_these_are_no_nodes'] ?></p><?php
                endif;
                ?>

        </div>
        <div class="edt_tab_content">
            <div class="tab_content_bar" style="display: none;">
                <div id="required-attachment" style="float: right;"></div>
            </div>
            <div id="tskDetail" class="tab_edt " style="display:none">
                <h4><span id="spnTaskName"></span></h4>
                <h6><span id="spnTaskDesc"></span></h6>
                <span id="edtpa_instructions"></span>


                <!-- <ol>
                     <li>Click on the Video icon above</li>
                     <li>A dialog box will appear with all the videos you have stored in your private Workspace and
                         Huddles you are
                         participating in.
                     </li>
                <!--                    <li>You can select 1 or more videos from your Workspace and Huddles.</li>-->
           <!--  <li>You can select a minimum of <span id="spminvideos"></span> Video(s) and a maximum of <span
                         id="spmaxvideos"></span> video(s) from your Workspace and Huddles.
             </li>
             <li>The videos you select will appear at the bottom of the page.</li>
         </ol>-->
                <div class="assessment_handbook" style="border: 1px solid #cecece;">
                    <div style="background: #e1e1e1;padding: 10px; cursor: pointer;" id="handbook_title">
                         <?php echo $edtpa_lang['edtpa_assessment_handbook'] ?>
                        <div class="arrow-upe" style="display: none;"></div>
                        <div class="arrow-downe"></div>
                    </div>
                    <ul style="list-style: none;padding-left: 5px; display: none;" data-attr="hidden" id="handbook-header">
                        <?php
                        if ($edtpa_assessment_handbooks) {
                            foreach ($edtpa_assessment_handbooks as $row) {
                                echo '<li style="background: #ebebeb; padding: 10px; width:98%;"><a download style="color:#66a5e2" href=' . $this->base . '/edtpa_handbooks/' . $row['EdtpaAssessmentsHandbooks']['handbook_url'] . '>' . $row['EdtpaAssessmentsHandbooks']['title'] . '</a></li>';
                            }
                        } else {
                            echo '<li>'.$edtpa_lang['edtpa_no_assessment_handbook'].'</li>';
                        }
                        ?>
                    </ul>
                </div>
                <div style="clear: both;"></div>
                <div class="workplace_edtp">
                    <h5><?php echo $edtpa_lang['edtpa_attachments'] ?></h5>
                    <div id="dvWorkSpeace"></div>
                    <!--<h5>Huddle</h5>-->
                    <div id="dvHuddle">
                    </div>
                    <div id="dvhuddlesettings" style="display:none;">
                        <div class="edtp-heading"> <?php echo $edtpa_lang['edtpa_settings_and_Assistance'] ?>
                            <div class="arrow-upe"></div>
                            <div class="arrow-downe"></div>
                        </div>
                        <div class="edtp-content" style="display:none">
                            <div class="edtp_col">


                            </div>

                        </div>


                        <div class="edtp-heading"> <?php echo $edtpa_lang['edtpa_settings_and_Assistance'] ?>
                            <div class="arrow-upe"></div>
                            <div class="arrow-downe"></div>
                        </div>
                        <div class="edtp-content" style="display:none">
                            <div class="edtp_col">


                            </div>

                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
            </div>


            <div class="edt_tb_placeholder">
                <img src="/app/img/edtpa/edt_placeholder.png">
                <h6><?php echo $edtpa_lang['edtpa_please_select_tasks'] ?></h6>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>
<script type="text/javascript">
    $(document).ready(function (e) {
        $('#handbook_title').on('click', function (e) {
            var status = $('#handbook-header').attr('data-attr');
            if (status == 'hidden') {
                $('#handbook-header').show(500);
                $('#handbook-header').attr('data-attr', 'show');
                $('.arrow-downe').hide()
                $('.arrow-upe').show();

            } else {
                $('#handbook-header').hide(500);
                $('#handbook-header').attr('data-attr', 'hidden');
                $('.arrow-upe').hide();
                $('.arrow-downe').show();
            }
        })
    })
</script>

<div id="edtpa_modal" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:600px;">
        <div class="modal-content">
            <div class="header" style="margin-bottom:0px;">
                <h4 class="header-title nomargin-vertical">Select Video</h4>

                <a id="cross_contact" class="close-reveal-modal btn btn-grey close style2"
                   data-dismiss="modal">&times;</a>
            </div>
            <div id="tab-area">
                <ul class="tabset" style="max-width: 100%;">
                    <li class="">
                        <a id="tab1" class="tab active" href="#tabbox2tp">Workspace</a>
                    </li>
                    <li class="">
                        <a id="tab2" class="tab" href="#tabbox3pt">Huddles</a>
                    </li>
                </ul>
                <div class="tab-content tab-active videosOuterCls" id="tabbox2tp"
                     style="visibility: visible; max-height: 400px;overflow: auto;">
                    <div class="edtp_col">
                        <div id="wsVideosOuter">
                            <?php
                            $publisehd_videos_count = 0;
                            if (isset($MyFilesVideos) && !empty($MyFilesVideos)) {

                                foreach ($MyFilesVideos as $MyFilesVideo):
                                    $document_files_array = $this->Custom->get_document_url($MyFilesVideo['Document']);

                                    if (empty($document_files_array['url'])) {
                                        $MyFilesVideo['Document']['published'] = 0;
                                        $document_files_array['url'] = $MyFilesVideo['Document']['original_file_name'];
                                        $MyFilesVideo['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                    } else {
                                        $MyFilesVideo['Document']['encoder_status'] = $document_files_array['encoder_status'];
                                        @$MyFilesVideo['Document']['duration'] = $document_files_array['duration'];
                                    }
                                    $thumbnail_image_path = $document_files_array['thumbnail'];
                                    ?>
                                    <div class="adt_thumb3">
                                        <div class="adt_video3"
                                             onclick="window.open('<?php echo $this->base . '/MyFiles/view/1/' . $MyFilesVideo['AccountFolder']['account_folder_id']; ?>'


                                                                             );">
                                            <img src="<?php echo $thumbnail_image_path; ?>">
                                            <div class="play-icon"
                                                 style="background-size: 50px;height: 50px;margin: 48px 0 0 -30px;position: absolute;left: 11%;width: 50px;"></div>
                                        </div>
                                        <div class="adt_desc3 student_srcv"
                                             onclick="window.open('<?php echo $this->base . '/MyFiles/view/1/' . $MyFilesVideo['AccountFolder']['account_folder_id']; ?>'


                                                                     );">
                                            <h4><?php echo(strlen($MyFilesVideo['afd']['title']) > 25 ? mb_substr($MyFilesVideo['afd']['title'], 0, 25) . "..." : $MyFilesVideo['afd']['title']); ?></h4>
                                            <p>By
                                                <a href="javascript:void(0);"><?php echo $MyFilesVideo['User']['first_name'] . " " . $MyFilesVideo['User']['last_name']; ?></a>
                                            </p>
                                            <p><?php echo date('M d, Y', strtotime($MyFilesVideo['Document']['created_date'])); ?></p>
                                        </div>
                                        <div class="edt_select">
                                            <?php
                                            $file_path_info = pathinfo($MyFilesVideo['Document']['url']);
                                            $document_extension = $file_path_info['extension'];
                                            ?>
                                            <span> <input type="checkbox"
                                                          data-document_extension="<?php echo $document_extension; ?>"
                                                          data-thumbnail="<?php echo $thumbnail_image_path; ?>"
                                                          data-doc_type="<?php echo $MyFilesVideo['Document']['doc_type']; ?>"
                                                          data-document_by="<?php echo $MyFilesVideo['User']['first_name'] . " " . $MyFilesVideo['User']['last_name']; ?>"
                                                          data-document_date="<?php echo date('M d, Y', strtotime($MyFilesVideo['Document']['created_date'])); ?>"
                                                          data-document_id="<?php echo $MyFilesVideo['Document']['id']; ?>"
                                                          data-document_title="<?php echo(strlen($MyFilesVideo['afd']['title']) > 25 ? mb_substr($MyFilesVideo['afd']['title'], 0, 25) . "..." : $MyFilesVideo['afd']['title']); ?>"
                                                          data-folder_type="<?php echo $MyFilesVideo['AccountFolder']['folder_type']; ?>"
                                                          class="select_checkbox"
                                                          id="select_<?php echo $MyFilesVideo['Document']['id']; ?>"
                                                          name="selected_videos[]"
                                                          value="<?php echo $MyFilesVideo['Document']['id']; ?>"> <label
                                                          for="select_<?php echo $MyFilesVideo['Document']['id']; ?>">Select</label> </span>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <?php
                                    $publisehd_videos_count++;
                                endforeach;
                                ?>

                                <?php
                            }
                            if ($publisehd_videos_count <= 0) {
                                ?><p class="noDataCls">There are no videos in workspace.</p><?php
                            }
                            ?>
                        </div>
                        <input type="hidden" name="selected_node" id="selected_node" value=""/>
                        <input type="hidden" name="page_num" id="page_num" value="1"/>
                        <div style="text-align:center;">
                            <img id="loading_gif" style="margin-bottom: -32px; display: none;"
                                 src="<?php echo $this->webroot . 'img/loading.gif' ?>">
                            <a class="btn btn-green" id="video-load-more" style="display:none;">Load more videos...</a>
                        </div>
                    </div>

                </div>
            </div>
            <?php if ($MyFilesVideosCount > 10) : ?>
                <script type="text/javascript">
                    $('#video-load-more').show();
                </script>
            <?php endif; ?>
            <div class="tab-content tab-hidden videosOuterCls" id="tabbox3pt"
                 style="visibility: visible; max-height: 400px;overflow: auto;">
                <div id="huddlesOuter">
                    <?php
                    if (isset($HuddlesVideos) && !empty($HuddlesVideos)) {
                        foreach ($HuddlesVideos as $HuddlesVideo):
                            ?>
                            <div class="edtp-heading"> <?php echo $HuddlesVideo['AccountFolder_name']; ?>
                                <div class="arrow-upe"></div>
                                <div class="arrow-downe"></div>
                            </div>
                            <div class="edtp-content" style="display:none">
                                <div class="edtp_col">

                                    <?php
                                    if (!empty($HuddlesVideo['videos'])) {
                                        foreach ($HuddlesVideo['videos'] as $video_details):
                                            if ($HuddlesVideo['AccountFolder_folder_type'] == 1) {
                                                $url_type_part = 1;
                                            } else {
                                                $url_type_part = '';
                                            }
                                            $videoID = $video_details['video_id'];
                                            $huddleID = $HuddlesVideo['AccountFolder_id'];
                                            ?>
                                            <div class="adt_thumb3">
                                                <div class="adt_video3"
                                                     onclick="window.open('<?php echo $this->base . '/Huddles/view/' . "$huddleID" . '/' . "$url_type_part/$videoID"; ?>');">
                                                    <img src="<?php echo $video_details['video_thumbnail']; ?>">
                                                    <div class="play-icon"
                                                         style="background-size: 50px;height: 50px;margin: 48px 0 0 -30px;position: absolute;left: 11%;width: 50px;"></div>
                                                </div>
                                                <div class="adt_desc3 student_srcv"
                                                     onclick="window.open('<?php echo $this->base . '/Huddles/view/' . "/$url_type_part/$videoID"; ?>');">
                                                    <h4><?php echo(strlen($video_details['video_title']) > 25 ? mb_substr($video_details['video_title'], 0, 25) . "..." : $video_details['video_title']); ?></h4>
                                                    <p>By
                                                        <a href="javascript:void(0);"><?php echo $HuddlesVideo['user_first_name'] . " " . $HuddlesVideo['user_last_name']; ?></a>
                                                    </p>
                                                    <p><?php echo $edtpa_lang['edtpa_upload'] ?> <?php echo $video_details['video_uploaded_date']; ?></p>
                                                </div>

                                                <div class="edt_select">
                                                    <span> <input type="checkbox"
                                                                  data-document_extension="<?php echo $video_details['document_extension']; ?>"
                                                                  data-thumbnail="<?php echo $video_details['video_thumbnail']; ?>"
                                                                  data-doc_type="<?php echo $video_details['doc_type']; ?>"
                                                                  data-document_by="<?php echo $HuddlesVideo['user_first_name'] . " " . $HuddlesVideo['user_last_name']; ?>"
                                                                  data-document_date="<?php echo $video_details['video_uploaded_date']; ?>"
                                                                  data-document_id="<?php echo $video_details['video_id']; ?>"
                                                                  data-document_title="<?php echo $video_details['video_title']; ?>"
                                                                  data-folder_type="<?php echo $HuddlesVideo['AccountFolder_folder_type']; ?>"
                                                                  class="select_checkbox"
                                                                  id="select_<?php echo $video_details['video_id']; ?>"
                                                                  name="selected_videos[]"
                                                                  value="<?php echo $video_details['video_id']; ?>"> <label
                                                                  for="select_<?php echo $video_details['video_id']; ?>">Select</label> </span>
                                                </div>
                                                <div class="clear"></div>

                                            </div>
                                            <?php
                                        endforeach;
                                    } else {
                                        ?>
                                        <p class="noDataCls">There are no videos in this huddle.</p>
                                    <?php } ?>

                                </div>
                            </div>
                            <?php
                        endforeach;
                    } else {
                        ?>
                        <p class="noDataCls">There are no huddles in this account.</p>
                    <?php } ?>
                </div>
                <input type="hidden" name="page_num_huddle" id="page_num_huddle" value="1"/>
                <div style="text-align:center;">
                    <br>
                    <img id="loading_gif_huddle" style="margin-bottom: -32px; display: none;"
                         src="<?php echo $this->webroot . 'img/loading.gif' ?>">
                    <a class="btn btn-green" id="video-load-more-huddles" style="display:none;">Load more huddles...</a>
                </div>
            </div>
            <?php if ($HuddlesCount > 10) : ?>
                <script type="text/javascript">
                    $('#video-load-more-huddles').show();
                </script>
            <?php endif; ?>
            <div class="clear"></div>
            <div class="modal-footer">
                <div class="form-actions" style="text-align: right;margin-top:0px;">
                    <p class="successCls" style="display:none;color:green;">Changes saved successfully.</p>
                    <p class="errorCls" style="display:none;color:red;">Oooops! an unknown error occurred.Please try
                        later. </p>
                    <!--<a href="javascript:void(0);" class="btn btn-primary saveChangesBtn" id="saveChangesBtnVid">Save Changes</a>-->
                    <input id="saveChangesBtnVid" type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" value="Save Changes" name="commit"
                           class="btn btn-green saveChangesBtn">
                    <input id="videoModelCancel" class="btn btn-white cancelCls" data-dismiss="modal" type="reset"
                           value="Cancel" style="font-size: 14px;">
                </div>
            </div>
        </div>
        <div class="clear"></div>

    </div>
</div>
</div>


<div id="edtpaDocs_modal" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:600px;">
        <div class="modal-content">
            <div class="header" style="margin-bottom:0px;">
                <h4 class="header-title nomargin-vertical">Select Resources</h4>
                <a id="cross_contact" class="close-reveal-modal btn btn-grey close style2"
                   data-dismiss="modal">&times;</a>
            </div>
            <div id="tab-area">
                <ul class="tabset" style="max-width: 100%;">
                    <li class="">
                        <a id="tab1" class="tab active" href="#tabbox2tpDocs">Workspace</a>
                    </li>
                    <li class="">
                        <a id="tab2" class="tab" href="#tabbox3ptDocs">Huddles</a>
                    </li>

                </ul>
                <div class="tab-content tab-active videosOuterCls" id="tabbox2tpDocs"
                     style="visibility: visible;  max-height: 400px;overflow: auto;">
                    <div class="edtp_col">
                        <?php
                        $publisehd_docs_count = 0;
                        if (isset($MyFilesDocuments) && !empty($MyFilesDocuments)) {

                            foreach ($MyFilesDocuments as $MyFilesDocument):
                                ?>
                                <div class="adt_thumb3 adt_ajdacent">
                                    <div class="adt_video3">
                                        <span class="<?php echo $this->Custom->getIcons($MyFilesDocument['Document']['url']); ?>">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <?php if (isset($MyFilesDocument['Document']['stack_url']) && $MyFilesDocument['Document']['stack_url'] != '') {
                                            ?>
                                            <?php
                                            $url_code = explode('/', $MyFilesDocument['Document']['stack_url']);
                                            $url_code = $url_code[3];
                                            ?>
                                                                                        <!--<a  id="view_resource_myfiles_<?php // echo $MyFilesDocument['Document']['id'];                                                                                                                                                                                                                                                                                                                                                                                               ?>" target="_blank" href="<?php // echo $this->base . '/MyFiles/view_document/' . $url_code;                                                                                                                                                                                                                                                                                                                                                                                               ?>" >-->
                                            <a id="view_resource_myfiles_<?php echo $MyFilesDocument['Document']['id']; ?>"
                                               target="_blank" href="javascript:void(0);">
                                                <span id="doc-title-<?php echo $MyFilesDocument['afd']['id']; ?>"><?php echo strlen($MyFilesDocument['afd']['title']) > 5 ? mb_substr($MyFilesDocument['afd']['title'], 0, 5) . "..." : $MyFilesDocument['afd']['title'] ?></span>
                                            </a>


                                        <?php } else { ?>
                                            <a href="javascript:void(0);"
                                               title="<?php echo $MyFilesDocument['afd']['title'] ?>">
                                                <span id="doc-title-<?php echo $MyFilesDocument['afd']['id']; ?>"><?php echo strlen($MyFilesDocument['afd']['title']) > 5 ? mb_substr($MyFilesDocument['afd']['title'], 0, 5) . "..." : $MyFilesDocument['afd']['title'] ?></span>
                                            </a>
                                        <?php } ?>
                                    </div>

                                    <div class="adt_desc3 student_srcv">
                                        <h4><?php echo(strlen($MyFilesDocument['afd']['title']) > 10 ? mb_substr($MyFilesDocument['afd']['title'], 0, 10) . "..." : $MyFilesDocument['afd']['title']); ?></h4>
                                        <p>By
                                            <a href="javascript:void(0);"><?php echo $MyFilesDocument['User']['first_name'] . " " . $MyFilesDocument['User']['last_name']; ?></a>
                                        </p>
                                        <p>Type : <a
                                                href="javascript:void(0);"><?php echo $this->Custom->docType($MyFilesDocument['Document']['url']); ?></a>
                                        </p>
                                        <p><?php echo date('M d, Y', strtotime($MyFilesDocument['Document']['created_date'])); ?></p>
                                    </div>
                                    <div class="edt_select">
                                        <?php
                                        $file_path_info = pathinfo($MyFilesDocument['Document']['url']);
                                        $document_extension = $file_path_info['extension'];
                                        ?>
                                        <span> <input type="checkbox"
                                                      data-document_extension="<?php echo $document_extension; ?>"
                                                      data-thumbnail="<?php echo $this->Custom->getIcons($MyFilesDocument['Document']['url']); ?>"
                                                      data-doc_type="<?php echo $MyFilesDocument['Document']['doc_type']; ?>"
                                                      data-document_by="<?php echo $MyFilesDocument['User']['first_name'] . " " . $MyFilesDocument['User']['last_name']; ?>"
                                                      data-document_date="<?php echo date('M d, Y', strtotime($MyFilesDocument['Document']['created_date'])); ?>"
                                                      data-document_id="<?php echo $MyFilesDocument['Document']['id']; ?>"
                                                      data-document_title="<?php echo(strlen($MyFilesDocument['afd']['title']) > 25 ? mb_substr($MyFilesDocument['afd']['title'], 0, 25) . "..." : $MyFilesDocument['afd']['title']); ?>"
                                                      data-folder_type="<?php echo $MyFilesDocument['AccountFolder']['folder_type']; ?>"
                                                      class="select_checkbox"
                                                      id="select_<?php echo $MyFilesDocument['Document']['id']; ?>"
                                                      name="selected_videos[]"
                                                      value="<?php echo $MyFilesDocument['Document']['id']; ?>"> <label
                                                      for="select_<?php echo $MyFilesDocument['Document']['id']; ?>">Select</label> </span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <?php
                                $publisehd_docs_count++;
                            endforeach;
                        }
                        if ($publisehd_docs_count <= 0) {
                            ?><p class="noDataCls">There are no documents in workspace.</p><?php
                        }
                        ?>
                    </div>
                </div>
                <div class="tab-content tab-hidden videosOuterCls" id="tabbox3ptDocs"
                     style="visibility: visible;     max-height: 400px; overflow: auto;">
                         <?php
                         if (isset($HuddlesDocuments) && !empty($HuddlesDocuments)) {
                             foreach ($HuddlesDocuments as $HuddlesVideo):
                                 ?>
                            <div class="edtp-heading"> <?php echo $HuddlesVideo['AccountFolder_name']; ?>
                                <div class="arrow-upe"></div>
                                <div class="arrow-downe"></div>
                            </div>
                            <div class="edtp-content" style="display:none">
                                <div class="edtp_col">
                                    <?php
                                    if (!empty($HuddlesVideo['documents'])) {
                                        foreach ($HuddlesVideo['documents'] as $document_details):
                                            if ($HuddlesVideo['AccountFolder_folder_type'] == 1) {
                                                $url_type_part = 1;
                                            } else {
                                                $url_type_part = '';
                                            }
                                            $videoID = $document_details['video_id'];
                                            $huddleID = $HuddlesVideo['AccountFolder_id'];
                                            ?>
                                            <div class="adt_thumb3 adt_ajdacent">
                                                <div class="adt_video3">
                                                    <span class="<?php echo $document_details['document_icon']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    <?php if (isset($document_details['stack_url']) && $document_details['stack_url'] != '') {
                                                        ?>
                                                        <?php
                                                        $url_code = explode('/', $MyFilesDocument['stack_url']);
                                                        $url_code = $url_code[3];
                                                        ?>
                                                        <a id="view_resource_myfiles_<?php echo $document_details['document_id']; ?>"
                                                           target="_blank" href="javascript:void(0);">
                                                            <span id="doc-title-<?php echo $document_details['afd_title']; ?>"><?php echo strlen($document_details['afd_title']) > 5 ? mb_substr($document_details['afd_title'], 0, 5) . "..." : $document_details['afd_title'] ?></span>
                                                        </a>


                                                    <?php } else { ?>
                                                        <a href="javascript:void(0);"
                                                           title="<?php echo $document_details['afd_title'] ?>">
                                                            <span id="doc-title-<?php echo $document_details['afd_title']; ?>"><?php echo strlen($document_details['afd_title']) > 5 ? mb_substr($document_details['afd_title'], 0, 5) . "..." : $document_details['afd_title'] ?></span>
                                                        </a>
                                                    <?php } ?>
                                                </div>

                                                <div class="adt_desc3 student_srcv">
                                                    <h4><?php echo(strlen($document_details['afd_title']) > 10 ? mb_substr($document_details['afd_title'], 0, 10) . "..." : $document_details['afd_title']); ?></h4>
                                                    <p>By
                                                        <a href="javascript:void(0);"><?php echo $document_details['document_by']; ?></a>
                                                    </p>
                                                    <p>Type : <a
                                                            href="javascript:void(0);"><?php echo $document_details['document_url']; ?></a>
                                                    </p>
                                                    <p><?php echo $document_details['created_date']; ?></p>
                                                </div>
                                                <div class="edt_select">
                                                    <span> <input type="checkbox"
                                                                  data-document_extension='<?php echo $document_details['document_extension']; ?>'
                                                                  data-thumbnail="<?php echo $document_details['document_icon']; ?>"
                                                                  data-doc_type="<?php echo $document_details['doc_type']; ?>"
                                                                  data-document_by="<?php echo $HuddlesVideo['user_first_name'] . " " . $HuddlesVideo['user_last_name']; ?>"
                                                                  data-document_date="<?php echo $document_details['created_date']; ?>"
                                                                  data-document_id="<?php echo $document_details['document_id']; ?>"
                                                                  data-document_title="<?php echo $document_details['afd_title']; ?>"
                                                                  data-folder_type="<?php echo $HuddlesVideo['AccountFolder_folder_type']; ?>"
                                                                  class="select_checkbox"
                                                                  id="select_<?php echo $document_details['document_id']; ?>"
                                                                  name="selected_videos[]"
                                                                  value="<?php echo $document_details['document_id']; ?>"> <label
                                                                  for="select_<?php echo $document_details['document_id']; ?>">Select</label> </span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <?php
                                        endforeach;
                                    } else {
                                        ?>
                                        <p class="noDataCls">There are no documents in this huddle.</p>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php
                        endforeach;
                    } else {
                        ?>
                        <p class="noDataCls">There are no huddles in this account.</p>
                    <?php } ?>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="modal-footer">
                <div class="form-actions" style="text-align: right;margin-top:0px;">
                    <p class="successCls" style="display:none;color:green;">Changes saved successfully.</p>
                    <p class="errorCls" style="display:none;color:red;">Oooops! an unknown error occurred.Please try
                        later. </p>
                    <input id="saveChangesBtnDoc" type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" value="Save Changes" name="commit"
                           class="btn btn-green saveChangesBtn">
                    <input id="docModelCancel" class="btn btn-white cancelCls" type="reset" value="Cancel"
                           style="font-size: 14px;" data-dismiss="modal">
                </div>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div id="video-modal"></div>
            </div>
        </div>
    </div>
</div>

<div id="edtpaResources_modal" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:600px;">
        <div class="modal-content">
            <div class="header" style="margin-bottom:0px;">
                <h4 class="header-title nomargin-vertical">Select Resources</h4>
                <a id="cross_contact" class="close-reveal-modal btn btn-grey close style2"
                   data-dismiss="modal">&times;</a>
            </div>
            <div id="tab-area">
                <ul class="tabset" style="max-width: 100%;">
                    <li class="">
                        <a id="tab1" class="tab active" href="#workspace-edTPA">Workspace</a>
                    </li>
                    <li class="">
                        <a id="tab2" class="tab" href="#huddle-edTPA">Huddles</a>
                    </li>

                </ul>
                <div class="tab-content tab-active videosOuterCls" id="workspace-edTPA"
                     style="visibility: visible;  max-height: 400px;overflow: auto;">
                    <div class="edtp_col">
                        <?php
                        $publisehd_docs_count = 0;
                        if (isset($MyFilesDocuments) && !empty($MyFilesDocuments)) {

                            foreach ($MyFilesDocuments as $MyFilesDocument):
                                ?>
                                <div class="adt_thumb3 adt_ajdacent">
                                    <div class="adt_video3">
                                        <span class="<?php echo $this->Custom->getIcons($MyFilesDocument['Document']['url']); ?>">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <?php if (isset($MyFilesDocument['Document']['stack_url']) && $MyFilesDocument['Document']['stack_url'] != '') {
                                            ?>
                                            <?php
                                            $url_code = explode('/', $MyFilesDocument['Document']['stack_url']);
                                            $url_code = $url_code[3];
                                            ?>
                                                                                        <!--<a  id="view_resource_myfiles_<?php // echo $MyFilesDocument['Document']['id'];                                                                                                                                                                                                                                                                                                                                                                                               ?>" target="_blank" href="<?php // echo $this->base . '/MyFiles/view_document/' . $url_code;                                                                                                                                                                                                                                                                                                                                                                                               ?>" >-->
                                            <a id="view_resource_myfiles_<?php echo $MyFilesDocument['Document']['id']; ?>"
                                               target="_blank" href="javascript:void(0);">
                                                <span id="doc-title-<?php echo $MyFilesDocument['afd']['id']; ?>"><?php echo strlen($MyFilesDocument['afd']['title']) > 5 ? mb_substr($MyFilesDocument['afd']['title'], 0, 5) . "..." : $MyFilesDocument['afd']['title'] ?></span>
                                            </a>


                                        <?php } else { ?>
                                            <a href="javascript:void(0);"
                                               title="<?php echo $MyFilesDocument['afd']['title'] ?>">
                                                <span id="doc-title-<?php echo $MyFilesDocument['afd']['id']; ?>"><?php echo strlen($MyFilesDocument['afd']['title']) > 5 ? mb_substr($MyFilesDocument['afd']['title'], 0, 5) . "..." : $MyFilesDocument['afd']['title'] ?></span>
                                            </a>
                                        <?php } ?>
                                    </div>

                                    <div class="adt_desc3 student_srcv">
                                        <h4><?php echo(strlen($MyFilesDocument['afd']['title']) > 10 ? mb_substr($MyFilesDocument['afd']['title'], 0, 10) . "..." : $MyFilesDocument['afd']['title']); ?></h4>
                                        <p>By
                                            <a href="javascript:void(0);"><?php echo $MyFilesDocument['User']['first_name'] . " " . $MyFilesDocument['User']['last_name']; ?></a>
                                        </p>
                                        <p>Type : <a
                                                href="javascript:void(0);"><?php echo $this->Custom->docType($MyFilesDocument['Document']['url']); ?></a>
                                        </p>
                                        <p><?php echo date('M d, Y', strtotime($MyFilesDocument['Document']['created_date'])); ?></p>
                                    </div>
                                    <div class="edt_select">
                                        <?php
                                        $file_path_info = pathinfo($MyFilesDocument['Document']['url']);
                                        $document_extension = $file_path_info['extension'];
                                        ?>
                                        <span> <input type="checkbox"
                                                      data-document_extension="<?php echo $document_extension; ?>"
                                                      data-thumbnail="<?php echo $this->Custom->getIcons($MyFilesDocument['Document']['url']); ?>"
                                                      data-doc_type="<?php echo $MyFilesDocument['Document']['doc_type']; ?>"
                                                      data-document_by="<?php echo $MyFilesDocument['User']['first_name'] . " " . $MyFilesDocument['User']['last_name']; ?>"
                                                      data-document_date="<?php echo date('M d, Y', strtotime($MyFilesDocument['Document']['created_date'])); ?>"
                                                      data-document_id="<?php echo $MyFilesDocument['Document']['id']; ?>"
                                                      data-document_title="<?php echo(strlen($MyFilesDocument['afd']['title']) > 25 ? mb_substr($MyFilesDocument['afd']['title'], 0, 25) . "..." : $MyFilesDocument['afd']['title']); ?>"
                                                      data-folder_type="<?php echo $MyFilesDocument['AccountFolder']['folder_type']; ?>"
                                                      class="select_checkbox"
                                                      id="select_<?php echo $MyFilesDocument['Document']['id']; ?>"
                                                      name="selected_videos[]"
                                                      value="<?php echo $MyFilesDocument['Document']['id']; ?>"> <label
                                                      for="select_<?php echo $MyFilesDocument['Document']['id']; ?>">Select</label> </span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <?php
                                $publisehd_docs_count++;
                            endforeach;
                        }
                        if ($publisehd_docs_count <= 0) {
                            ?><p class="noDataCls">There are no documents in workspace.</p><?php
                        }
                        ?>
                    </div>
                </div>
                <div class="tab-content tab-hidden videosOuterCls" id="huddle-edTPA"
                     style="visibility: visible;     max-height: 400px; overflow: auto;">
                         <?php
                         if (isset($HuddlesDocuments) && !empty($HuddlesDocuments)) {
                             foreach ($HuddlesDocuments as $HuddlesVideo):
                                 ?>
                            <div class="edtp-heading"> <?php echo $HuddlesVideo['AccountFolder_name']; ?>
                                <div class="arrow-upe"></div>
                                <div class="arrow-downe"></div>
                            </div>
                            <div class="edtp-content" style="display:none">
                                <div class="edtp_col">
                                    <?php
                                    if (!empty($HuddlesVideo['documents'])) {
                                        foreach ($HuddlesVideo['documents'] as $document_details):
                                            if ($HuddlesVideo['AccountFolder_folder_type'] == 1) {
                                                $url_type_part = 1;
                                            } else {
                                                $url_type_part = '';
                                            }
                                            $videoID = $document_details['video_id'];
                                            $huddleID = $HuddlesVideo['AccountFolder_id'];
                                            ?>
                                            <div class="adt_thumb3 adt_ajdacent">
                                                <div class="adt_video3">
                                                    <span class="<?php echo $document_details['document_icon']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    <?php if (isset($document_details['stack_url']) && $document_details['stack_url'] != '') {
                                                        ?>
                                                        <?php
                                                        $url_code = explode('/', $MyFilesDocument['stack_url']);
                                                        $url_code = $url_code[3];
                                                        ?>
                                                        <a id="view_resource_myfiles_<?php echo $document_details['document_id']; ?>"
                                                           target="_blank" href="javascript:void(0);">
                                                            <span id="doc-title-<?php echo $document_details['afd_title']; ?>"><?php echo strlen($document_details['afd_title']) > 5 ? mb_substr($document_details['afd_title'], 0, 5) . "..." : $document_details['afd_title'] ?></span>
                                                        </a>


                                                    <?php } else { ?>
                                                        <a href="javascript:void(0);"
                                                           title="<?php echo $document_details['afd_title'] ?>">
                                                            <span id="doc-title-<?php echo $document_details['afd_title']; ?>"><?php echo strlen($document_details['afd_title']) > 5 ? mb_substr($document_details['afd_title'], 0, 5) . "..." : $document_details['afd_title'] ?></span>
                                                        </a>
                                                    <?php } ?>
                                                </div>

                                                <div class="adt_desc3 student_srcv">
                                                    <h4><?php echo(strlen($document_details['afd_title']) > 10 ? mb_substr($document_details['afd_title'], 0, 10) . "..." : $document_details['afd_title']); ?></h4>
                                                    <p>By
                                                        <a href="javascript:void(0);"><?php echo $document_details['document_by']; ?></a>
                                                    </p>
                                                    <p>Type : <a
                                                            href="javascript:void(0);"><?php echo $document_details['document_url']; ?></a>
                                                    </p>
                                                    <p><?php echo $document_details['created_date']; ?></p>
                                                </div>
                                                <div class="edt_select">
                                                    <span> <input type="checkbox"
                                                                  data-document_extension='<?php echo $document_details['document_extension']; ?>'
                                                                  data-thumbnail="<?php echo $document_details['document_icon']; ?>"
                                                                  data-doc_type="<?php echo $document_details['doc_type']; ?>"
                                                                  data-document_by="<?php echo $HuddlesVideo['user_first_name'] . " " . $HuddlesVideo['user_last_name']; ?>"
                                                                  data-document_date="<?php echo $document_details['created_date']; ?>"
                                                                  data-document_id="<?php echo $document_details['document_id']; ?>"
                                                                  data-document_title="<?php echo $document_details['afd_title']; ?>"
                                                                  data-folder_type="<?php echo $HuddlesVideo['AccountFolder_folder_type']; ?>"
                                                                  class="select_checkbox"
                                                                  id="select_<?php echo $document_details['document_id']; ?>"
                                                                  name="selected_videos[]"
                                                                  value="<?php echo $document_details['document_id']; ?>"> <label
                                                                  for="select_<?php echo $document_details['document_id']; ?>">Select</label> </span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <?php
                                        endforeach;
                                    } else {
                                        ?>
                                        <p class="noDataCls">You have uploaded no document in huddles.</p>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php
                        endforeach;
                    } else {
                        ?>
                        <p class="noDataCls">There are no huddles in this account.</p>
                    <?php } ?>
                </div>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>
            <div class="modal-footer">

                <div class="form-actions" style="text-align: right;margin-top:0px;">
                    <p class="successCls" style="display:none;color:green;">Changes saved successfully.</p>
                    <p class="errorCls" style="display:none;color:red;">Oooops! an unknown error occurred.Please try
                        later. </p>
                    <input id="saveChangesBtnDoc" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button" value="Save Changes" name="commit"
                           class="btn btn-green saveChangesBtn">
                    <input id="docModelCancel" class="btn btn-white cancelCls" type="reset" value="Cancel"
                           style="font-size: 14px;" data-dismiss="modal">
                </div>
            </div>
        </div>

    </div>
</div>

<div class="edt_select">
    <span style="display: none;">
        <input type="checkbox" data-document_extension="" data-thumbnail="" data-doc_type=""
               data-document_by="<?php echo $user['User']['first_name'] . " " . $user['User']['last_name']; ?>"
               data-document_date="" data-document_id="" data-document_title="" data-folder_type="3"
               class="select_checkbox my_computer_resurce" id="" name="selected_videos[]" value="">
        <input type="hidden" id="video_id" value="0"/>
    </span>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click', '.tablink', function () {
            $('.tablink').removeClass('currentTablink');
            $(this).addClass('currentTablink');
            var node_id = $(this).data('node');
            $('#selected_node').val(node_id);
        });
        $(document).on('click', '#btnUploadDocument', function () {
            OpenFilePicker('MyFilesDoc');
        });

        function OpenFilePicker(docType) {
            current_node_index = $('#current_node').val();
            current_node_id = $('#edTPA_node_' + current_node_index).attr('data-attr-index');
            var current_node_details = nodes_data[current_node_id];
            var maxfile = current_node_details.max_number_of_files;
            if ($('input:checkbox:checked.select_checkbox').length >= maxfile) {
                alert('You have already seleted max number of allowed files.');
                return false;
            }
            // filepicker.setKey(filepicker_access_key);
            var client = filestack.init('<?php echo Configure::read('filepicker_access_key'); ?>');

            var uploadPath = $("#uploadPath").val();
            $('#txtUploadedFilePath').val("");
            $('#txtUploadedFileName').val("");
            $('#txtUploadedFileMimeType').val("");
            $('#txtUploadedFileSize').val("");
            $('#txtUploadedDocType').val(docType);

            var filepickerOptions;
            if (docType == 'MyFilesVideo') {
                filepickerOptions = {
                    multiple: true,
                    fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive', 'video', 'webcam'],
                    extensions: ['3gp', '3gpp', 'avi', 'divx', 'dv', 'flv', 'm4v', 'mjpeg', 'mkv', 'mod', 'mov', 'mp4', 'mpeg', 'mpg', 'm2ts', 'mts', 'mxf', 'ogv', 'wmv', 'aif', 'mp3', 'm4a', 'ogg', 'wav', 'wma']
                }
            } else {
                filepickerOptions = {
                    multiple: true,
                    extensions: ['bmp', 'gif', 'jpeg', 'jpg', 'png', 'tif', 'tiff', 'swf', 'pdf', 'txt', 'docx', 'ppt', 'pptx', 'potx', 'xls', 'xlsx', 'xlsm', 'rtf', 'odt', 'doc', 'mp3', 'm4a'],
                    fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive', 'url'],
                }
            }

            // fix for iOS 7
            if (isIOS()) {
                filepickerOptions.multiple = false;
            }

            // new window for iPhone
            if (!!navigator.userAgent.match(/iPhone/i)) {
                filepickerOptions.container = 'window';
            }


            if (docType == 'MyFilesVideo') {

                client.pick({
                    maxFiles: 1000,
                    fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive', 'video'],
                    accept: ['video/*', '3gp', '3gpp', 'avi', 'divx', 'dv', 'flv', 'm4v', 'mjpeg', 'mkv', 'mod', 'mov', 'mp4', 'mpeg', 'mpg', 'm2ts', 'mts', 'mxf', 'ogv', 'wmv', 'aif', 'mp3', 'm4a', 'ogg', 'wav', 'wma'],
                    lang : <?php echo $_SESSION['LANG']; ?>,
                    storeTo: {
                        location: "s3",
                        path: uploadPath,
                        access: 'public',
                        container: bucket_name,
                        region: 'us-east-1'
                    }
                }).then(
                        function (inkBlob) {
                            inkBlob = inkBlob.filesUploaded;
                            if (inkBlob && inkBlob.length > 0) {
                                for (var i = 0; i < inkBlob.length; i++) {
                                    var blob = inkBlob[i];
                                    var fileExt = getFileExtension(blob.filename).toLowerCase();

                                    $('#txtUploadedFilePath').val(blob.key);
                                    $('#txtUploadedFileName').val(blob.filename);
                                    $('#txtUploadedFileMimeType').val(blob.mimetype);
                                    $('#txtUploadedFileSize').val(blob.size);
                                    $('#txtUploadedUrl').val(blob.url);


                                    if (docType == 'MyFilesVideo') {
                                        PostMyFilesVideo(docType, (fileExt == 'mp4' ? true : false));
                                    } else {
                                        PostMyFilesDocument();
                                    }
                                }
                            }
                        }),
                        function (FPError) {
                            var error_desc = 'Unkown Error';
                            //as per filepicker documentation these are possible two errors
                            if (FPError.code == 101) {
                                error_desc = 'The user closed the dialog without picking a file';
                            } else if (FPError.code = 151) {
                                error_desc = 'The file store couldnt be reached';
                            }

                            $.ajax({
                                type: 'POST',
                                data: {
                                    type: 'Workspace',
                                    id: '',
                                    error_id: FPError.code,
                                    error_desc: error_desc,
                                    docType: docType,
                                    current_user_id: current_user_id
                                },
                                url: home_url + '/Huddles/logFilePickerError/',
                                success: function (response) {
                                    //Do nothing.
                                },
                                errors: function (response) {
                                    alert('Error occured while logging, please report the error code to the system administrator: ' + FPError.code);
                                }

                            });
                        }

            }

            else {


                client.pick({
                    maxFiles: 1000,
                    accept: ['application/*', '.bmp', '.gif', '.jpeg', '.jpg', '.png', '.tif', '.tiff', '.swf', '.pdf', '.txt', '.docx', '.ppt', '.pptx', '.potx', '.xls', '.xlsx', '.xlsm', '.rtf', '.odt', '.doc'],
                    fromSources: ['local_file_system', 'dropbox', 'googledrive', 'box', 'onedrive'],
                    lang : <?php echo $_SESSION['LANG']; ?>,
                    storeTo: {
                        location: "s3",
                        path: uploadPath,
                        access: 'public',
                        container: bucket_name,
                        region: 'us-east-1'
                    }
                }).then(
                        function (inkBlob) {
                            inkBlob = inkBlob.filesUploaded;
                            if (inkBlob && inkBlob.length > 0) {
                                for (var i = 0; i < inkBlob.length; i++) {
                                    var blob = inkBlob[i];
                                    var fileExt = getFileExtension(blob.filename).toLowerCase();

                                    $('#txtUploadedFilePath').val(blob.key);
                                    $('#txtUploadedFileName').val(blob.filename);
                                    $('#txtUploadedFileMimeType').val(blob.mimetype);
                                    $('#txtUploadedFileSize').val(blob.size);
                                    $('#txtUploadedUrl').val(blob.url);


                                    if (docType == 'MyFilesVideo') {
                                        PostMyFilesVideo(docType, (fileExt == 'mp4' ? true : false));
                                    } else {
                                        var attachment_no = $("#attachment").text();
                                        var attach_new = attachment_no.substring(13, 14);
                                        attachment_no = parseInt(attach_new) + 1;
                                        $("#attachment").text('Attachments (' + attachment_no + ')');
                                        PostMyFilesDocument();
                                    }
                                }
                            }
                        }),
                        function (FPError) {
                            var error_desc = 'Unkown Error';
                            //as per filepicker documentation these are possible two errors
                            if (FPError.code == 101) {
                                error_desc = 'The user closed the dialog without picking a file';
                            } else if (FPError.code = 151) {
                                error_desc = 'The file store couldnt be reached';
                            }

                            $.ajax({
                                type: 'POST',
                                data: {
                                    type: 'Workspace',
                                    id: '',
                                    error_id: FPError.code,
                                    error_desc: error_desc,
                                    docType: docType,
                                    current_user_id: current_user_id
                                },
                                url: home_url + '/Huddles/logFilePickerError/',
                                success: function (response) {
                                    //Do nothing.
                                },
                                errors: function (response) {
                                    alert('Error occured while logging, please report the error code to the system administrator: ' + FPError.code);
                                }

                            });
                        }

            }

        }

        function getFileExtension(filename) {

            return filename.substr(filename.lastIndexOf('.') + 1)

        }

        function PostMyFilesDocument() {
            showProcessOverlay($('#tskDetail'));
            video_id = $('#video_id').val();
            $.ajax({
                url: home_url + '/MyFiles/addResources',
                method: 'POST',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    if (parseInt(data) == 0) {
                        alert('Unable to save data, please try again');
                    } else {
                        $.ajax({
                            url: home_url + "/huddles/generate_url/" + data.result.document_id,
                            type: 'POST',
                            // dataType: 'json',
                            success: function (response) {

                                filepicker.setKey('A3w6JbXR2RJmKr3kfnbZtz');

                                filepicker.storeUrl(
                                        response,
                                        {filename: data.file_name},
                                function (blob) {
                                    // var result = blob.url.split('/');
                                    $.ajax({
                                        type: 'POST',
                                        url: home_url + '/Huddles/update_stack_url/' + data.result.document_id + '/' + video_id,
                                        data: {stack_url: blob.url},
                                        success: function (response) {
                                            loadAjaxFilter();
                                        }
                                    });


                                }
                                );


                            },
                            error: function () {
                                alert("Network Error Occured");
                            }
                        });


                    }
                },
                data: {
                    video_title: $('#txtUploadedFileName').val(),
                    video_desc: "",
                    assessment_account_id: <?php echo $edtpa_assessment_account_id; ?>,
                    node_id: $('#current_node').val(),
                    video_url: $('#txtUploadedFilePath').val(),
                    video_file_name: $('#txtUploadedFileName').val(),
                    video_mime_type: $('#txtUploadedFileMimeType').val(),
                    video_file_size: $('#txtUploadedFileSize').val(),
                    video_id: $('#txtCurrentVideoID').val(),
                    url: $('#txtUploadedUrl').val(),
                    rand_folder: "0"
                }
            });
        }

        function loadAjaxFilter() {
            var edtpa_assessment_id = $('#edtpa_assessment_id').val();
            $.ajax({
                url: home_url + '/Edtpa/ajax_assessment_load/' + edtpa_assessment_id,
                method: 'POST',
                dataType: "json",
                success: function (data) {
                    $("#dvWorkSpeace").html('');
                    $("#dvHuddle").html('');
                    var current_index = $('#current_node_index').val();
                    nodes_data = data.result;
                    loadNodeData(current_index);
                    hideProcessOverlay($('#tskDetail'));


                }

            });
        }

        function PostMyFilesVideo(doc_type, direct_publish) {
            $.ajax({
                url: home_url + (doc_type == 'MyFilesVideo' ? '/MyFiles/add/1' : '/MyFiles/add/2'),
                method: 'POST',
                success: function (data) {
                    CloseVideoUpload(doc_type);
                    if ($('#flashMessage').length == 0) {
                        $('#main').prepend('<div class="success" id="flashMessage" style="cursor: pointer;">Sit tight, your video is currently processing.  You will receive an email when your video is ready to be viewed.</div>');
                        $('#flashMessage').click(function () {
                            $(this).css('display', 'none')
                        })
                    } else {
                        var html = $('#flashMessage').html();
                        $('#flashMessage').html(html + '<br/>Sit tight, your video is currently processing.  You will receive an email when your video is ready to be viewed.');
                    }
                },
                data: {
                    video_title: $('#txtUploadedFileName').val(),
                    video_desc: '',
                    assessment_account_id: <?php echo $edtpa_assessment_account_id; ?>,
                    node_id: $('#current_node').val(),
                    video_url: $('#txtUploadedFilePath').val(),
                    video_file_name: $('#txtUploadedFileName').val(),
                    video_mime_type: $('#txtUploadedFileMimeType').val(),
                    video_file_size: $('#txtUploadedFileSize').val(),
                    rand_folder: $('#txtVideoPopupRandomNumber').val(),
                    direct_publish: (direct_publish == true ? 1 : 0)
                }
            });
        }
    });



</script>