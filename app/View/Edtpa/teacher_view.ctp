<link rel="stylesheet" type="text/css" href="edtpa.css">
<link rel="stylesheet" type="text/css" media="screen" href="/css/analytics/lib/js/jqgrid/css/ui.jqgrid.css"></link>
<link rel="stylesheet" type="text/css" media="screen" href="/css/analytics/lib/js/themes/redmond/jquery-ui.custom.css"></link>
<link rel="stylesheet" type="text/css" media="screen" href="/css/analytics/lib/js/jqgrid/css/ui.jqgrid.css"></link>
<link rel="stylesheet" href="/css/analytics/lib/js/themes/redmond/green_theme.css" type="text/css" />


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<?php if($_SESSION['LANG'] =='es'):?>
            <script src="<?php echo $this->webroot; ?>css/analytics/lib/js/jqgrid/js/i18n/grid.locale-es.js" type="text/javascript"></script>
        <?php else:?>
            <script src="<?php echo $this->webroot; ?>css/analytics/lib/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
        <?php endif;?> 
<script src="/css/analytics/lib/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>
<script src="/css/analytics/lib/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
<style>
    th:first-child{
        width: 140px!important;
    }
    td{
        padding: 10px 10px;
    }
    th.ui-th-column div{
        white-space:normal !important;
        height:auto !important;
        padding:2px;
    }
    .ui-jqgrid .ui-jqgrid-resize {height:100% !important;}
    .data_grid_adjs {
        height: auto !important;
    }
    .sel_dur_grph{
        width:200px;
        float:left;
    }
    .cl_token_field{
        width:200px;
        float:left;
        margin-left:5px;
        margin-right:5px;
    }
    .filter_box select{
        -moz-appearance: tab-scroll-arrow-forward;
        vertical-align: top;
    }
    .filter_box .tokenfield{
        display:inline-block;
    }
    .token .close{
        margin-top:0px;
        margin-right:0px;
    }
    .token .token-label{
        max-width: 60px !important;
    }
    .tokenfield{
        min-height: 37px !important;
    }
    .tokenfield .token-input{
        height: 20px !important;
    }
    .colCell {position: sticky;
              left: 0;
              width: 165px !important;
              z-index: 9;
              background: #fff;}
    #grid_account_name {position: sticky;
                        left: 0;
                        width: 165px !important;
                        z-index: 9;
                        background: #e0e0e0;}

    .search_mod_class
    {
        position: sticky;
        left: 0;
        width: 165px !important;
        z-index: 9;
        background: #e0e0e0;

    }

    #ui-datepicker-div
    {
        z-index:9 !important;
    }
    .edtpa_leftBox .ui-jqgrid-view{width: 100%!important;}
    .edtpa_leftBox .ui-jqgrid {width: 100%!important;}
    .edtpa_leftBox .ui-jqgrid-hdiv {width: 100%!important;}
    .edtpa_leftBox .ui-jqgrid-bdiv {width: 100%!important;height: 400px!important;}
    .edtpa_leftBox .table_footer {width: 100%!important;}
    .edtpa_leftBox #accUsersAnalytics .ui-jqgrid-hdiv {width: 100%!important;}
    .edtpa_leftBox .ui-jqgrid .ui-jqgrid-btable{table-layout: unset;}
    .edtpa_leftBox #jqGrid{width: 100%!important;}
    .language_sp .edtpa_leftBox .ui-jqgrid .ui-jqgrid-htable th div {    font-size: 13px !important}

</style>
<?php
$edtpa_lang = $this->Session->read('edtpa_lang');
?>
<script>
    $(document).ready(function () {
        $(".section-content").show();
        $(".arrow-up").hide();
        $(".section-heading").click(function () {
            $(this).next(".section-content").slideToggle(500);
            $(this).find(".arrow-up, .arrow-down").toggle();
        });

        $(document).on('click', '.edt_toggle', function () {
            $('.edtcel3dropdown').hide();
            $(this).parent().find('.edtcel3dropdown').stop().toggle('fast');
        });

    });

    function cancelRequest(transfer_id) {
        if (transfer_id == '') {
            alert('Invalid Transfer ID.');
        } else {
            $.ajax({
                type: 'POST',
                url: baseurl + "Huddles/Portfolio_Package_Transfer_Cancel/" + transfer_id,
                dataType: 'json',
                success: function (response) {
                    console.log(response);
                    if (response.success == true) {
                        location.reload();
                    } else {
                        alert('<?php echo trim($edtpa_lang['edtpa_failed_to_submit_request']) ?>');
                    }
                }
            });
        }
    }
</script>
<style>
    .edtpa_container {
        min-height: 750px;
    }

    .edtcel3dropdown {
        position: absolute;
        top: 36px;
        z-index: 4;
        background: #fff;
        padding: 9px 9px 7px 9px;
        -webkit-box-shadow: 0px 2px 9px -1px rgba(221, 221, 221, 1);
        -moz-box-shadow: 0px 2px 9px -1px rgba(221, 221, 221, 1);
        box-shadow: 0px 2px 9px -1px rgba(221, 221, 221, 1);
        text-align: left;
        display: none;
        border: 1px solid #ddd;
        width: 80px;
        right: -18px;
    }

    .edtcel3dropdown:before {
        pointer-events: none;
        position: absolute;
        z-index: -1;
        content: '';
        border-style: solid;
        -webkit-transition-duration: 0.3s;
        transition-duration: 0.3s;
        -webkit-transition-property: transform;
        transition-property: transform;
        left: calc(50% - 10px);
        top: 0;
        border-width: 0 7px 7px 7px;
        border-color: transparent transparent #e1e1e1 transparent;
        right: 43px;
        left: inherit;
        -webkit-transform: translateY(-10px);
        transform: translateY(-10px);
        top: 3px;
    }

    .edtcel3dropdown ul {
        margin: 0;
        padding: 0;
        list-style: none;
    }

    .edtcel3dropdown ul li {
        margin-bottom: 2px;
    }

    .edtcel3dropdown ul li a {
        font-weight: normal;
    }

    img.edt_toggle {
        cursor: pointer;
    }

    .edtpa_leftBox {
        float: left;
        width: 100%;
        margin-right: 0%;
    }

    .edtpa_rightBox {
        float: right;
        display: inline-block;
        width: 27%;
    }

    .edtpa_leftBox h1 {
        margin: 0px 0 15px 0;
        font-size: 26px;
    }

    .edtpa_leftBox table tr th {
        border-bottom: solid 2px #f2f2f2;
    }

    .edtpa_leftBox table td a {
        color: #00aeef;
        font-weight: normal;
        text-decoration: underline;
    }

    .edtpa_leftBox table td a:hover {
        color: #f6701b;
    }

    .bread_crum {
        margin-bottom: 20px;
        font-size: 13px;
    }

    .bread_crum a {
        font-weight: normal;
        color: #668dab;
    }

    .bread_crum a:after {
        content: '>';
        margin: 0px 15px;
        color: #757575
    }

    .table_footer {
        font-size: 13px;
        color: #606060;
        margin-top: 16px;
    }

    .table_footer select {
        padding: 8px;
        margin-right: 10px;
        width: 70px;
        border: solid 1px #f1f1f1;
        box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.08);
    }

    .table_footer ul {
        float: right;
        margin: 0px;
        padding: 0px;
        list-style: none;
    }

    .table_footer li {
        display: inline-block;
    }

    .table_footer li a {
        padding: 5px 9px;
        color: #606060;
        font-weight: normal;
    }

    .table_footer li a:hover {
        color: #f6701b;
    }

    .table_footer .active a {
        background: #5daf46;
        border-radius: 3px;
        color: #fff;
    }

    .table_footer .active a:hover {
        color: #fff;
    }

    .edtpa_rightBox h1 {
        margin: 0px;
        font-size: 22px;
        margin-top: 36px;
        margin-bottom: 10px;
    }

    .edtpa_rightBox ul {
        border: solid 2px #e0e0e0;
        margin: 0px;
        padding: 0px;
        list-style: none;
    }

    .edtpa_rightBox ul li {
        padding: 10px 6px;
        border-bottom: solid 1px #e0e0e0;
        position: relative;
    }

    .edtpa_rightBox ul li h2 {
        font-weight: normal;
        color: #616161;
        font-size: 15px;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, "sans-serif";
    }

    .submitted_label {
        font-size: 12px;
        color: #3cb878;
        font-weight: bold;
    }

    .submittedApprval_label {
        font-size: 12px;
        color: #8560a8;
        font-weight: bold;
    }

    .submitedEdtpa {
        font-size: 12px;
        color: #f26522;
        font-weight: bold;
    }

    .approved_label {
        position: absolute;
        right: 0px;
        top: 0px;
        background: #8dc63f;
        color: #fff;
        font-size: 9px;
        padding: 3px 12px;
    }

    .submission_box li span {
        font-size: 11px;
    }

    .submission_box li span img {
        width: 18px;
        height: auto;
        margin-left: 12px;
    }

    .submission_box li:last-child {
        border-bottom: 0px;
    }
    .ui-jqgrid .ui-jqgrid-hbox {padding-right: 0px !important;}
    .ui-jqgrid .ui-jqgrid-htable{width:100% !important}
    .edtpa_leftBox .data_grid_green .ui-jqgrid-hbox table th {background:none !important; border: 0 !important;
                                                              padding: 7px 5px; width: 150px !important;}
    .edtpa_leftBox thead tr{background:none !important;}
    .edtpa_leftBox .ui-jqgrid .ui-jqgrid-htable th div {font-size: 15px !important;font-weight: 800;text-align: left;font-family: "Segoe UI";}

    .data_grid_green .ui-jqgrid tr.jqgrow td { border-right:0!important;}
    .edtpa_leftBox #jqGridPager {background:none !important;}
    .edtpa_leftBox #jqGridPager_left {display:none;}
    .edtpa_leftBox #jqGridPager  .ui-pg-input {    height: 28px!important;border-radius: 0!important;}
    .edtpa_leftBox .ui-pager-control {background:none !important;}
    .edtpa_leftBox .ui-pg-table td{ border:none!important;}
    .edtpa_leftBox .ui-pager-control {background:none !important;}
    .edtpa_leftBox .edtpa_secls{ background:#f4f4f4; padding:20px 15px; margin-bottom: 15px;}
    .edtpa_leftBox .ui-jqgrid {border: 0;}
    .edtpa_leftBox .ui-jqgrid-hdiv {border-right:0 !important;}

    .edtpa_secls input[type=text]{border: 1px solid #c5c5c5;
                                  border-radius: 3px; box-shadow: 0 1px 2px rgba(0,0,0,0.09) inset;font-size: 13px;font-weight: bold;color: #b2b2b2;padding: 8px 4px;float: left; margin-right: 18px;}

    .edtpa_secls span {    float: left; margin-right: 18px;}
    .edtpa_secls span select{    height: 32px !important;}
    .data_grid_green .ui-jqgrid tr.jqgrow td{    word-wrap: break-word !important;
                                                 white-space: initial!important;width: 144px !important;}


</style>
<style>
    #export-btn{
        display: block;
        // z-index: 999999999999;
        text-align: right;
        padding: 10px;
        width: 124px;
        border-radius: 5px;
        font-weight: normal;
        position: relative;
        float: right;
    }
</style>
<div class="edtpa_container">
    <div class="bread_crum">
        <a href="<?php echo $this->base . '/Dashboard/home' ?>"><?php echo trim($edtpa_lang['edtpa_brd_dashboard']) ?></a>edTPA <?php echo trim($edtpa_lang['edtpa_brd_home']) ?>
    </div>
    <div class="edtpa_leftBox" style="widht:100%">
        <h1><?php echo trim($edtpa_lang['edtpa_assessment']) ?></h1>
        <a href="<?php echo $this->base ?>/Edtpa/export_to_excel" id="export-btn"><?php echo $edtpa_lang['edtpa_export_in_excel'] ?></a>
        <div id="accUsersAnalytics" class="data_grid_green data_grid_adjs">
            <div class="edtpa_secls">
                <input type="text" placeholder="<?php echo $edtpa_lang['edtpa_placehoder_name'] ?>" id="txtFName">
                <input type="text" placeholder="<?php echo $edtpa_lang['edtpa_attachments'] ?>" id="txtLName">
       <!--  <span class="input select">
   <!--  <select class="">
           <option>1</option>
         </select>
                </span>-->
                <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" id="do_search" class="btn btn-green"><?php echo trim($edtpa_lang['edtpa_filter']) ?></button>
            </div>


            <table id="jqGrid"></table>
            <div id="jqGridPager" CLASS="table_footer"></div>


        </div>



    </div>

    <div class="clear"></div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#jqGrid").jqGrid({
                url: '<?php echo $this->base . '/Edtpa/fetach_teacher_assessments' ?>',
                mtype: "GET",
                datatype: "json",
                colModel: [
                    {label: '<?php echo trim($edtpa_lang['edtpa_placehoder_name']) ?>', name: 'student_name', width: 150, search: false},
                    {label: '<?php echo trim($edtpa_lang['edtpa_assessment_cap']) ?>', name: 'assessment_name', width: 150, search: false},
                    {label: '<?php echo trim($edtpa_lang['edtpa_date_of_transfer']) ?>', name: 'date_of_transfer', width: 150, search: false, formatter: 'date', formatoptions: {srcformat: 'Y-m-d', newformat: 'ShortDate'}},
                    {label: '<?php echo trim($edtpa_lang['edtpa_time_of_transfer']) ?>', name: 'time_of_transfer', width: 150, search: false},
                    {label: '<?php echo trim($edtpa_lang['edtpa_status']) ?>', name: 'status', width: 100, search: false},
                    {label: '<?php echo trim($edtpa_lang['edtpa_action']) ?>', name: 'action', width: 100, search: false}
                ],
                viewrecords: true,
                rowNum: 10000,
                // pager: "#jqGridPager"
            });
            $('#do_search').on('click', function (e) {
                doSearch();
            })
            function doSearch() {
                var strFName = $('#txtFName').val();
                var strLName = $('#txtLName').val();
                jQuery("#jqGrid").jqGrid('setGridParam', {
                    url: "<?php echo $this->base . '/Edtpa/fetach_teacher_assessments' ?>?student_name=" + strFName + "&assessment_name=" + strLName,
                    page: 1
                }).trigger("reloadGrid");
            }

        });

    </script>

