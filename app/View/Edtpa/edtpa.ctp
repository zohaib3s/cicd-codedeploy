<!doctype html>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $(".section-content").show();
        $(".arrow-up").hide();
        $(".section-heading").click(function () {
            $(this).next(".section-content").slideToggle(500);
            $(this).find(".arrow-up, .arrow-down").toggle();
        });

        $(document).on('click', '.edt_toggle', function () {
            $('.edtcel3dropdown').hide();
            $(this).parent().find('.edtcel3dropdown').stop().toggle('fast');
        });
        $(document).on('click', '.cantEditCls', function () {
            alert('You cannot edit this assessment.Please first cancel your tranfer request.');
        });
    });
    function cancelRequest(transfer_id) {
        if (transfer_id == '') {
            alert('Invalid Transfer ID.');
        } else {
            $.ajax({
                type: 'POST',
                url: baseurl + "Huddles/Portfolio_Package_Transfer_Cancel/" + transfer_id,
                dataType: 'json',
                success: function (response) {
                    console.log(response);
                    if (response.success == true) {
                        location.reload();
                    } else {
                        alert('Failed to submit your request.Please try later.');
                    }
                }
            });
        }
    }
</script>

</head>
<style>
    .edtpa_container{
        min-height: 750px;
    }
    .edtcel3dropdown {
        position: absolute;
        top: 36px;
        z-index: 4;
        background: #fff;
        padding: 9px 9px 7px 9px;
        -webkit-box-shadow: 0px 2px 9px -1px rgba(221,221,221,1);
        -moz-box-shadow: 0px 2px 9px -1px rgba(221,221,221,1);
        box-shadow: 0px 2px 9px -1px rgba(221,221,221,1);
        text-align: left;
        display: none;
        border: 1px solid #ddd;
        width: 80px;
        right: -18px;
    }

    .edtcel3dropdown:before {
        pointer-events: none;
        position: absolute;
        z-index: -1;
        content: '';
        border-style: solid;
        -webkit-transition-duration: 0.3s;
        transition-duration: 0.3s;
        -webkit-transition-property: transform;
        transition-property: transform;
        left: calc(50% - 10px);
        top: 0;
        border-width: 0 7px 7px 7px;
        border-color: transparent transparent #e1e1e1 transparent;
        right: 43px;
        left: inherit;
        -webkit-transform: translateY(-10px);
        transform: translateY(-10px);
        top: 3px;
    }
    .edtcel3dropdown ul{
        margin: 0;
        padding: 0;
        list-style: none;
    }
    .edtcel3dropdown ul li{    margin-bottom: 2px;}
    .edtcel3dropdown ul li a{font-weight: normal;}
    img.edt_toggle {
        cursor: pointer;
    }



    .edtpa_leftBox{
        float: left;
        width: 68%;
        margin-right: 3%;
    }
    .edtpa_rightBox{
        float: right;
        display: inline-block;
        width: 27%;
    }
    .edtpa_leftBox h1{
        margin:0px;
        font-size:26px;
    }
    .edtpa_leftBox table tr th{
        border-bottom:solid 2px #f2f2f2;
    }
    .edtpa_leftBox table td a{
        color: #00aeef;
        font-weight: normal;
        text-decoration: underline;
    }
    .edtpa_leftBox table td a:hover {
        color: #f6701b;
    }
    .bread_crum{
        margin-bottom:20px;
        font-size:13px;
    }
    .bread_crum a{
        font-weight:normal;
        color:#668dab;
    }
    .bread_crum a:after{
        content:'>';
        margin:0px 15px;
        color: #757575
    }
    .table_footer{
        font-size: 13px;
        color: #606060;
        margin-top: 16px;
    }
    .table_footer select{
        padding: 8px;
        margin-right: 10px;
        width: 70px;
        border: solid 1px #f1f1f1;
        box-shadow: 0px 1px 2px rgba(0,0,0,0.08);
    }
    .table_footer ul{
        float: right;
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    .table_footer li{
        display: inline-block;
    }
    .table_footer li a{
        padding: 5px 9px;
        color: #606060;
        font-weight: normal;
    }
    .table_footer li a:hover{
        color: #f6701b;
    }
    .table_footer .active a{
        background: #5daf46;
        border-radius: 3px;
        color: #fff;
    }
    .table_footer .active a:hover{
        color: #fff;
    }
    .edtpa_rightBox h1{
        margin: 0px;
        font-size: 22px;
        margin-top: 36px;
        margin-bottom: 10px;
    }
    .edtpa_rightBox ul{
        border: solid 2px #e0e0e0;
        margin: 0px;
        padding: 0px;
        list-style: none;
    }
    .edtpa_rightBox ul li{
        padding: 10px 6px;
        border-bottom: solid 1px #e0e0e0;
        position: relative;
    }
    .edtpa_rightBox ul li h2{
        font-weight: normal;
        color: #616161;
        font-size: 15px;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, "sans-serif";
    }
    .submitted_label{
        font-size: 12px;
        color: #3cb878;
        font-weight: bold;
    }
    .submittedApprval_label{
        font-size: 12px;
        color: #8560a8;
        font-weight: bold;
    }
    .submitedEdtpa{
        font-size: 12px;
        color: #f26522;
        font-weight: bold;
    }
    .approved_label{
        position: absolute;
        right: 0px;
        top: 0px;
        background: #8dc63f;
        color: #fff;
        font-size: 9px;
        padding: 3px 12px;
    }
    .submission_box li span{
        font-size: 11px;
    }
    .submission_box li span img{
        width:18px;
        height: auto;
        margin-left: 12px;
    }
    .submission_box li:last-child{
        border-bottom: 0px;
    }

</style>
<body>
    <div class="edtpa_container">
        <div class="bread_crum">
            <a href="#">Dashboard</a> edTPA Home
        </div>

        <div class="edtpa_leftBox">
            <h1>edTPA Assessmants</h1>
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <th style="text-align: left;">Name</th>
                    <th style="width: 150px;">Version</th>
                    <th style="width:110px;">Year</th>
                    <th style="width: 90px;">Action </th>
                </tr>
                <?php
                if (!empty($assessment_accounts)): {
                        $cancel_btn = false;
                        foreach ($assessment_accounts as $assessment_account) {
                            if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 0) {
                                $submit_status = 'Not Started';
                                $cancel_btn = false;
                            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 1) {
                                $submit_status = 'Auth Key Verified';
                                $cancel_btn = false;
                            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 2) {
                                $submit_status = ' Submission Verified';
                                $cancel_btn = false;
                            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 3) {
                                $submit_status = 'Schedule Request Submitted';
                                $cancel_btn = true;
                            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 4) {
                                $submit_status = 'Portfolio Retrieved';
                                $cancel_btn = true;
                            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 5) {
                                $submit_status = 'Request Cancelled';
                                $cancel_btn = false;
                            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 6) {
                                $submit_status = 'In Process';
                                $cancel_btn = true;
                            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 7) {
                                $submit_status = 'Portfolio Failed';
                                $cancel_btn = false;
                            } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 8) {
                                $submit_status = 'Success';
                                $cancel_btn = false;
                            } else {
                                $submit_status = 'Not Started';
                                $cancel_btn = false;
                            }
                            ?>
                            <tr>
                                <td><a class="<?php echo ($cancel_btn) ? 'cantEditCls' : ''; ?>" href="<?php echo ($cancel_btn) ? 'javascript:void(0);' : $this->base . '/Edtpa/edtpa_detail/' . $assessment_account['EdtpaAssessments']['id']; ?>"><?php echo $assessment_account['EdtpaAssessments']['assessment_name'] ?></a></td>
                                <td style="text-align: center;"><?php echo $submit_status; ?></td>
                                <td style="text-align: center;"><?php echo $assessment_account['EdtpaAssessments']['assessment_version'] ?></td>
                                <td style="text-align: center;"><a href="<?php echo ($cancel_btn) ? 'javascript:void(0);' : $this->base . '/Edtpa/edtpa_detail/' . $assessment_account['EdtpaAssessments']['id']; ?>">select</a></td>
                            </tr>
            <!--                            <tr>
                                            <td class="edt_cell1"><a class="<?php echo ($cancel_btn) ? 'cantEditCls' : ''; ?>" href="<?php echo ($cancel_btn) ? 'javascript:void(0);' : $this->base . '/Edtpa/edtpa_detail/' . $assessment_account['EdtpaAssessments']['id']; ?>"><?php echo $assessment_account['EdtpaAssessments']['assessment_name'] ?></a></td>
                                            <td class="edt_cell2"><span><?php echo $submit_status; ?></span></td>
                                            <td class="edt_cell3"><?php echo $assessment_account['EdtpaAssessments']['assessment_version'] ?></td>
                                            <td class="edt_cell3" style="position:relative;">
                                                <img class="edt_toggle" src="/app/img/edtpa/edt_dot3.png">
                                                <div class="edtcel3dropdown">
                                                    <ul>
            <?php if (!$cancel_btn) { ?><li><a href="<?php echo ($cancel_btn) ? 'javascript:void(0);' : $this->base . '/Edtpa/edtpa_detail/' . $assessment_account['EdtpaAssessments']['id']; ?>">Edit</a></li><?php } ?>
            <?php if ($cancel_btn) { ?><li><a href="javascript:void(0);" class="cancleRequest" onlclick="cancelRequest(<?php echo $assessment_account['transfer_id'] ?>)">Cancel</a></li><?php } ?>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>-->
                            <?php
                        }
                    }
                else:
                    ?>
                    <tr>
                        <td colspan="3" style="text-align:center;"> No records found!</td>
                    </tr>
<?php endif;
?>



            </table>
            <div class="table_footer">
                <select><option>3</option></select> Showing 1 to 3 of 3 entries
                <ul>
                    <li><a href="#">Previous</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">Next</a></li>
                </ul>
            </div>
        </div>
        <div class="edtpa_rightBox">
            <h1>My edTPA Submission</h1>
            <ul class="submission_box">
                <li>
                    <h2>World Language</h2>
                    <span class="submitted_label">Submitted</span> <span><img src="/app/webroot/img/icons/calendar_icon.png"> 2016-2017</span>
                </li>
                <li>
                    <div class="approved_label">Approved</div>
                    <h2>Elementary Mathematics</h2>
                    <a href="#" class="submitedEdtpa">Submit to edTPA</a> <span><img src="/app/webroot/img/icons/calendar_icon.png"> 2016-2017</span>
                </li>
                <li>
                    <h2>Secondary Language</h2>
                    <span class="submitted_label">Submitted</span> <span><img src="/app/webroot/img/icons/calendar_icon.png"> 2016-2017</span>
                </li>
                <li>
                    <h2>Secondary English-Language Arts</h2>
                    <span class="submittedApprval_label">Submitted for Approval</span> <span><img src="/app/webroot/img/icons/calendar_icon.png"> 2016-2017</span>
                </li>
            </ul>
        </div>
        <div class="clear"></div>
        <!--<h1>Available edTPA Submission</h1>-->
        <!--        <div class="edtpa_sub">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <th class="edt_cell1">Assessment Name</th>
                            <th class="edt_cell2">Submit to EdTPA</th>
                            <th class="edt_cell3">Version</th>
                            <th class="edt_cell3"> </th>
                        </tr>
        <?php
        if (!empty($assessment_accounts)): {
                $cancel_btn = false;
                foreach ($assessment_accounts as $assessment_account) {
                    if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 0) {
                        $submit_status = 'Not Started';
                        $cancel_btn = false;
                    } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 1) {
                        $submit_status = 'Auth Key Verified';
                        $cancel_btn = false;
                    } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 2) {
                        $submit_status = ' Submission Verified';
                        $cancel_btn = false;
                    } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 3) {
                        $submit_status = 'Schedule Request Submitted';
                        $cancel_btn = true;
                    } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 4) {
                        $submit_status = 'Portfolio Retrieved';
                        $cancel_btn = true;
                    } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 5) {
                        $submit_status = 'Request Cancelled';
                        $cancel_btn = false;
                    } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 6) {
                        $submit_status = 'In Process';
                        $cancel_btn = true;
                    } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 7) {
                        $submit_status = 'Portfolio Failed';
                        $cancel_btn = false;
                    } else if (isset($assessment_account['EdtpaAssessmentAccountCandidates']['id']) && $assessment_account['EdtpaAssessmentAccountCandidates']['status'] == 8) {
                        $submit_status = 'Success';
                        $cancel_btn = false;
                    } else {
                        $submit_status = 'Not Started';
                        $cancel_btn = false;
                    }
                    ?>

                                                                                                                                                                        <tr>
                                                                                                                                                                            <td class="edt_cell1"><a class="<?php echo ($cancel_btn) ? 'cantEditCls' : ''; ?>" href="<?php echo ($cancel_btn) ? 'javascript:void(0);' : $this->base . '/Edtpa/edtpa_detail/' . $assessment_account['EdtpaAssessments']['id']; ?>"><?php echo $assessment_account['EdtpaAssessments']['assessment_name'] ?></a></td>
                                                                                                                                                                            <td class="edt_cell2"><span><?php echo $submit_status; ?></span></td>
                                                                                                                                                                            <td class="edt_cell3"><?php echo $assessment_account['EdtpaAssessments']['assessment_version'] ?></td>
                                                                                                                                                                            <td class="edt_cell3" style="position:relative;">
                                                                                                                                                                                <img class="edt_toggle" src="/app/img/edtpa/edt_dot3.png">
                                                                                                                                                                                <div class="edtcel3dropdown">
                                                                                                                                                                                    <ul>
            <?php if (!$cancel_btn) { ?><li><a href="<?php echo ($cancel_btn) ? 'javascript:void(0);' : $this->base . '/Edtpa/edtpa_detail/' . $assessment_account['EdtpaAssessments']['id']; ?>">Edit</a></li><?php } ?>
            <?php if ($cancel_btn) { ?><li><a href="javascript:void(0);" class="cancleRequest" onlclick="cancelRequest(<?php echo $assessment_account['transfer_id'] ?>)">Cancel</a></li><?php } ?>
                                                                                                                                                                                    </ul>
                                                                                                                                                                                </div>
                                                                                                                                                                            </td>
                                                                                                                                                                        </tr>
                    <?php
                }
            }
        else:
            ?>
                                                                        <tr>
                                                                            <td colspan="3" style="text-align:center;"> No records found!</td>
                                                                        </tr>
<?php endif;
?>


                    </table>

                    <div class="edt_attchment" style="display:none;">
                        <div class="atch_left">Attachments</div>
                        <div class="atch_right">
                            <div class="adt_thumb">
                                <div class="adt_video">
                                    <img src="/app/img/edtpa/video_thm.png"></div>

                                <div class="adt_desc">
                                    <h4>Video Name will</h4>
                                    <p>Huddle</p>
                                </div>
                                <div class="clear"></div>

                            </div>
                            <div class="adt_thumb">
                                <div class="adt_video">
                                    <img src="/app/img/edtpa/video_thm.png"></div>

                                <div class="adt_desc">
                                    <h4>Video Name will</h4>
                                    <p>Huddle</p>
                                </div>
                                <div class="clear"></div>

                            </div>
                            <div class="adt_thumb">
                                <div class="adt_video">
                                    <img src="/app/img/edtpa/video_thm.png"></div>

                                <div class="adt_desc">
                                    <h4>Video Name will</h4>
                                    <p>Huddle</p>
                                </div>
                                <div class="clear"></div>

                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>-->

        <div class="clear"></div>
    </div>
</body>
</html>

