<!--<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>-->
<style type="text/css">
    .current_doc {
        width: 100%;
        border: 0;
        height: 600px;
    }
 
    .upload_video_text_box{
        position: absolute;
        z-index: 1000;
        bottom: 10px;
        width: 100%;
        text-align: center;
    }
    .upload_video_text_box h1{
        font-weight:normal;
    }
    .tab2{
        width: 100%;
        clear: both;
        display: block;
        background-color: #EDEDED;
        padding-top: 78px;
        text-align: center;
        padding-bottom: 76px;    
    }
    .tab3{
        display: none;
    }
    .cl_invalid{
        border: 1px solid red !important;
    }
    .tab4 {
        width: 100%;
        clear: both;
        display: none;
        background-color: #8EC63F;
        padding-top: 78px;
        text-align: center;
        padding-bottom: 76px;
        color: #fff;
    }
    .tab4 h1{
        color: #fff;
    }
    ::-webkit-input-placeholder {
       color: #ADABAB;
    }

    :-moz-placeholder { /* Firefox 18- */
       color: #ADABAB;  
    }

    ::-moz-placeholder {  /* Firefox 19+ */
       color: #ADABAB;  
    }

    :-ms-input-placeholder {  
       color: #ADABAB;
    }
 
    .videoWrapper {
    position: relative;
    padding-bottom: 44.90%; /* 16:9 */
    padding-top: 25px;
    height: 0;
}
.videoWrapper iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}
.video_box2{
    width:302px;
    margin:0 auto;
}
.top_video_img {
    z-index: 100;
    position: relative;
    margin-bottom: -42px;
    background:url(/app/img/upload_video_bg.png) no-repeat;
    height:335px;
}
.expain_video_box{
    position: absolute;
    width: 100%;
    height:310px;
    bottom: 0px;
    text-align: center;
    background:url(/app/img/video_bg.png) no-repeat center bottom;
 
}
.vimeo_video{
    width:302px;
    height:167px;
    margin:0 auto;
    margin-top:25px;
}
</style>
<div class="top_video_img" id="tab1">
    <div class="upload_video_text_box">
        <h1 id="vdo-txt">Please enter your personal information below</h1><p>&nbsp;</p>
  </div>
  <div class="expain_video_box">

        <div class="vimeo_video">
        <?php if($is_embeded_enable == 1){?>
            <?php echo stripslashes($external_embeded_link); ?>
        <?php }?>
        </div>

  </div>
</div>

<div id="tab2" class="tab2">
    <label id="video_name">&nbsp;</label>
    <br>
    <br>
    <input type="text" id="txt_first" name="txt_first" value="" style="width: 385px;"  placeholder="Please Enter Your First Name" required/>
    <br>
    <br>
    <input type="text" id="txt_last" name="txt_last" value="" style="width: 385px;"  placeholder="Please Enter Your Last Name" required/>
    <br>
    <br>
    <input type="email" id="txt_confirm" name="txt_confirm" value="" style="width: 385px;"  placeholder="Please Enter Your Email Address" required/>
    <br>
    <br>
    <input type="button"  id="btn_confirm" name="btn_confirm" style="background-color: #24BA01;width: 113px;line-height: 25px;border: none;border-radius: 15px;color: #fff;font-weight: bold;" value="Confirm" />
</div>

<div class="tab3" id="tab3">
    <div id="go_back" style="float: right;margin-right: 130px;">
    <a style="position: absolute;z-index: 100;font-size: 20px;cursor: pointer;width: 157px;" onclick="$('#tab3').hide();$('#tab2').show();"><< Go Back</a>
    </div>
    <input type="hidden" id="account_id" name="account_id" value="<?php echo $account_id; ?>"/>
    <!--<input type="hidden" id="ob_account_folder_id" name="ob_account_folder_id" value="181"/>-->
    <input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id; ?>"/>
    <input id="txtUploadedFilePath" type="hidden" value="" />
    <input id="txtUploadedFileName" type="hidden" value="" />
    <input id="txtUploadedFileMimeType" type="hidden" value="" />
    <input id="txtUploadedFileSize" type="hidden" value="" />
    <input id="txtUploadedDocType" type="hidden" value="video" />
    <input id="uploadPath" type="hidden" value="<?php echo "/tempupload/$account_id/" . date('Y/m/d/'); ?>" />


    <iframe id="current_doc" class="current_doc" ></iframe>
</div>

<div id="tab4" class="tab4">
    <h1>THANK YOU</h1>
    <p>Your video has been submitted successfully.</p>

</div>

<script type="text/javascript" >
    $(document).ready(function () {
        var current_user_id = '<?php echo $user_id;?>';
        $(document).on("click", "#btn_confirm", function () {

            var check_email = isEmail($("#txt_confirm").val());
            var check_first = $("#txt_first").val();
            var check_last = $("#txt_last").val();
            var validate_all = true;
            if (check_email) {
                $("#txt_confirm").removeClass("cl_invalid");
            } else {
                $("#txt_confirm").addClass("cl_invalid");
                validate_all = false;
            }
            if (check_first.length > 0) {
                
                $("#txt_first").removeClass("cl_invalid");
            } else {
                $("#txt_first").addClass("cl_invalid");
                validate_all = false;
            }
            if (check_last.length > 0) {
                $("#txt_last").removeClass("cl_invalid");
            } else {
                $("#txt_last").addClass("cl_invalid");
                validate_all = false;
            }
            if(validate_all){
                $("#tab2").hide();
                $("#vdo-txt").html('Please Upload your Video');
                $("#tab3").show();   
            }
        });
        ob_OpenFilePicker_external(docType = 'video');
    });
    function ob_OpenFilePicker_external(docType) {
        if (docType == 'doc' && window.isCurrentVideoPlaying == true) {
            if (!confirm('Uploading a document while your video is playing will restart the video.  Are you sure you want to upload a document?'))
                return;
        }
        filepicker.setKey(filepicker_access_key);
        var uploadPath = $("#uploadPath").val();
        $('#txtUploadedFilePath').val("");
        $('#txtUploadedFileName').val("");
        $('#txtUploadedFileMimeType').val("");
        $('#txtUploadedFileSize').val("");
        $('#txtUploadedDocType').val(docType);

        var filepickerOptions;

        if (docType == 'video') {
            filepickerOptions = {
                multiple: false,
                services: ['COMPUTER', 'DROPBOX', 'GOOGLE_DRIVE', 'SKYDRIVE', 'BOX', 'VIDEO'],
                extensions: ['3gp', '3gpp', 'avi', 'divx', 'dv', 'flv', 'm4v', 'mjpeg', 'mkv', 'mod', 'mov', 'mp4', 'mpeg', 'mpg', 'm2ts', 'mts', 'mxf', 'ogv', 'wmv', 'aif', 'mp3', 'm4a', 'ogg', 'wav', 'wma']
            }
        } else {
            filepickerOptions = {
                multiple: true,
                extensions: ['bmp', 'gif', 'jpeg', 'jpg', 'png', 'tif', 'tiff', 'swf', 'pdf', 'txt', 'docx', 'ppt', 'pptx', 'potx', 'xls', 'xlsx', 'xlsm', 'rtf', 'odt', 'doc', 'mp3', 'm4a'],
                services: ['COMPUTER', 'DROPBOX', 'GOOGLE_DRIVE', 'SKYDRIVE', 'BOX']
            }
        }

        // fix for iOS 7
        if (isIOS()) {
            filepickerOptions.multiple = false;
        }

        // new window for iPhone
        if (!!navigator.userAgent.match(/iPhone/i)) {
            filepickerOptions.container = 'window';
        }
        filepickerOptions.container = 'current_doc';
        filepicker.pickAndStore(
                filepickerOptions, {
                    location: "S3",
                    path: uploadPath,
                    container: bucket_name
                },
        function (inkBlob) {
            if (inkBlob && inkBlob.length > 0) {
                for (var i = 0; i < inkBlob.length; i++) {
                    var blob = inkBlob[i];
                    var fileExt = getFileExtensionexternal(blob.filename).toLowerCase();

                    $('#txtUploadedFilePath').val(blob.key);
                    $('#txtUploadedFileName').val(blob.filename);
                    $('#txtUploadedFileMimeType').val(blob.mimetype);
                    $('#txtUploadedFileSize').val(blob.size);
                    if (docType == 'video') {
                        $('#temp-video-title').html(blob.filename);
                        $('ul.videos-list').prepend($('#temp-list').html());
                        ob_PostHuddleVideo_external(docType, (fileExt == 'mp4' ? true : false));
                    } else {
                        $('#doc-title').html(blob.filename);
                        $('#doc-type').html(blob.mimetype);
                        $('#add-document-row').html($('#extra-row-li').html());
                        //ob_PostHuddleDocument();
                    }
                }
            }

        },
                function (FPError) {
                    var error_desc = 'Unkown Error';
                    //as per filepicker documentation these are possible two errors
                    if (FPError.code == 101) {
                        error_desc = 'The user closed the dialog without picking a file';
                    } else if (FPError.code = 151) {
                        error_desc = 'The file store couldnt be reached';
                    }

                    $.ajax({
                        type: 'POST',
                        data: {
                            type: 'Huddles',
                            id: $('#ob_account_folder_id').val(),
                            error_id: FPError.code,
                            error_desc: error_desc,
                            docType: docType,
                            current_user_id: current_user_id
                        },
                        url: home_url + '/Huddles/logFilePickerError/',
                        success: function (response) {
                            //Do nothing.
                        },
                        errors: function (response) {
                            alert('Error occured while logging, please report the error code to the system administrator: ' + FPError.code);
                        }
                    });
                }

        );
    }
    function ob_PostHuddleVideo_external(doc_type, direct_publish) {
        var video_id_url = '';
        if (doc_type == 'doc' && $('ul.tabset li a.active').length > 0 && $('ul.tabset li a.active').html() == 'Videos' && $('#txtCurrentVideoID').length > 0) {
            video_id_url = "/" + $('#txtCurrentVideoID').val();
        }

        $.ajax({
            url: home_url + (doc_type == 'video' ? '/Huddles/externaluploadVideos/' : '/Huddles/externaluploadVideos/') +
                    +$('#account_id').val() + '/' + $('#user_id').val() + video_id_url + '/' + $('#txt_confirm').val()+ '/' + $('#txt_first').val()+ '/' + $('#txt_last').val(),
            method: 'POST',
            dataType: 'json',
            success: function (data) {
                console.log(data);
//                    if (data['user_session_expired']) {
//                        location.href = home_url + '/users/login';
//                        return;
//                    }
                $("#current_doc").hide();
                $("#video_name").text($("#txtUploadedFileName").val());
                $("#vdo-txt").html('');
                $("#tab4").show();
                $("#go_back").hide();
                // doVideoSearchAjax();
//                    if ($('#flashMessage').length == 0) {
//                        $('#main').prepend('<div class="success" id="flashMessage" style="cursor: pointer;">Sit tight, your video is currently processing.  You will receive an email when your video is ready to be viewed.</div>');
//                        $('#flashMessage').click(function () {
//                            $(this).css('display', 'none')
//                        })
//                    } else {
//                        var html = $('#flashMessage').html();
//                        $('#flashMessage').html(html + '<br/>Sit tight, your video is currently processing.  You will receive an email when your video is ready to be viewed.');
//                    }
            },
            data: {
                video_title: $('#txtUploadedFileName').val(),
                video_desc: '',
                video_url: $('#txtUploadedFilePath').val(),
                video_file_name: $('#txtUploadedFileName').val(),
                video_mime_type: $('#txtUploadedFileMimeType').val(),
                video_file_size: $('#txtUploadedFileSize').val(),
                rand_folder: $('#txtVideoPopupRandomNumber').val(),
                direct_publish: (direct_publish == true ? 1 : 0)
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (textStatus == 'error') {
                alert('An error just occurred. The site will be refreshed.');
                location.reload(true);
            }
        });
    }
    function getFileExtensionexternal(filename) {

        return filename.substr(filename.lastIndexOf('.') + 1)

    }
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
</script>