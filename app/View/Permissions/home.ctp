<?php $breadcrumb_language_based_content = $this->Custom->get_page_lang_based_content('BreadCrumb'); ?>
<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<?php
$priviliges_disables = '';
$disabled_class = '';
//if ($permission_set == 'not_allow' || $permotion_set == 'not_allow') {
//    $priviliges_disables = 'disabled';
//    $disabled_class = 'disables-box';
//}
$disabled_class2 = '';
//if ($permission_set == 'not_allow') {
//    $disabled_class2 = 'disables-box';
//}

$disabled_role_change_class = $disabled_class;
$priviliges_role_disables = $priviliges_disables;
$permission_role_set = $permission_set;

if ($current_user['users_accounts']['role_id'] == '100' && $users['UserAccount']['role_id'] == '100') {
    $disabled_role_change_class = 'disables-box';
    $priviliges_role_disables = 'disabled';
    $permission_role_set = 'not_allow';
}
$account_owner_disable = '';
$super_user_disable = '';
if ($current_user['roles']['role_id'] == '110') {
    if ($users['roles']['role_id'] == '120') {
        $account_owner_disable = 'disabled';
        $super_user_disable = '';
    } elseif ($users['roles']['role_id'] == '110') {
        $account_owner_disable = 'disabled';
    } else {
        $account_owner_disable = '';
        $super_user_disable = '';
    }
}
if ($current_user['roles']['role_id'] == '120') {
    if ($users['roles']['role_id'] == '120') {
        $account_owner_disable = 'disabled';
        $super_user_disable = 'disabled';
    } elseif ($users['roles']['role_id'] == '110') {
        $account_owner_disable = 'disabled';
    } else {
        $account_owner_disable = '';
        $super_user_disable = '';
    }
}


$btn_1_color = $this->Custom->get_site_settings('primary_bg_color');
$btn_2_color = $this->Custom->get_site_settings('secondary_bg_color');
?>
<style type="text/css">
    #select-all-label{
        position: relative;
        top: -4px;
    }
    .disables-box{
        display: none;
    }
    [type="search"]{
        border: 1px solid #c5c5c5;
        border-radius: 3px;
        box-shadow: 0 1px 2px rgba(0,0,0,0.09) inset;
        font-size: 13px;
        font-weight: bold;
        color: #222;
        padding: 8px 4px;
    }
</style>
<script>
    function hide_sidebar() {
        $(".appendix-content").delay(500).fadeOut(500);
    }
</script>
<div class="permission-header-box" style="background:none repeat scroll 0 0 #F9F9F9">
    <div class="header style3">
        <?php if (isset($users['users']['image']) && $users['users']['image'] != ''): ?>
            <?php
            $avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $users['users']['id'] . "/" . $users['users']['image'], $users['users']['image']);
            ?>
            <?php echo $this->Html->image($avatar_path, array('alt' => $users['users']['first_name'] . " " . $users['users']['last_name'], 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '73', 'width' => '73', 'align' => 'left')); ?>
        <?php else: ?>
            <a href="#">
                <img width="73" height="73" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo" alt="Photo-default">
            </a>
        <?php endif; ?>
        <div class="inline vertical-top">
            <h2 class="inline nomargin-vertical"><?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?></h2>
            <span class="user-status inline"><?php echo $this->Custom->get_user_role_name($users['roles']['role_id']); ?></span>
            <p class="nomargin-top smargin-bottom">
                <span class="user-job"></span>
            </p>
            <p class="nomargin-top smargin-bottom">

                <a href="#" class="icon-email blue-link" style="float: left;">&nbsp;</a>
                <span style="float: left; display: block; margin-top: 3px;"><?php echo $users['users']['email']; ?></span>
            </p>
        </div>
        <div class="action-buttons right">
            <?php if (strpos($referer, '/Users/editUser/') !== false): ?>
                <a href="<?php echo $this->base . $referer ?>" class="btn"><?=$language_based_content['Back_to_User_Settings_people_section'];?></a>
            <?php else: ?>
                <a href="<?php echo $this->base . '/users/administrators_groups' ?>" class="btn"><?php echo $language_based_content['back_to_all_users_people_section']; ?></a>

            <?php endif; ?>

            <?php if ($permission_set == 'allow' && $users['roles']['role_id'] != '100'): ?>
                <?php if ($users['users']['is_active'] == '1'): ?>
                    <a data-confirm="<?php echo $language_based_content['user_will_be_switched_to_viewer_people_section']; ?>" href="<?php echo $this->base . '/users/activte/' . $users['accounts']['id'] . '/' . $users['users']['id'] ?>" class="btn"><?php echo $language_based_content['deactivate_people_section']; ?></a>
                <?php else: ?>
<!--                    <a href="<?php //echo $this->base . '/users/activte/' . $users['accounts']['id'] . '/' . $users['users']['id'] ?>" class="btn">Activate</a>-->
                <?php endif; ?>
                <a href="<?php echo $this->base . '/Users/deleteAccount/' . $users['users']['id'] . '/' . $users['accounts']['id'] ?>" class="btn icon2-trash" data-confirm="<?php echo $language_based_content['are_you_sure_want_to_remove_person_people_section']; ?>" data-method="delete" rel="tooltip nofollow" title="<?=$language_based_content['delete_people_section'];?> <?php print $this->Custom->get_user_role_name($users['roles']['role_id']); ?>"></a>
            <?php endif; ?>
        </div>

        <div class="clear"></div>
    </div>
</div>
<?php if ($users['users']['type'] != 'Active'): ?>
    <div class="text-center permission-header-box style5 mmargin-top">
        <h1><?php echo $this->Custom->parse_translation_params($language_based_content['user_name_has_not_yet_logged_in'], ['first_name'=>$users['users']['first_name'], 'last_name' => $users['users']['last_name'],site_title => $this->Custom->get_site_settings('site_title')]) ?></h1>
        <p><?php echo $this->Custom->parse_translation_params($language_based_content['click_below_to_resend'], ['user_email'=>$users['users']['email']]) ?></p>
        <p>
            <a href="<?php echo $this->base . '/users/resendInvitation/' . $users['accounts']['id'] . '/' . $users['users']['id'] ?>" class="btn btn-green btn-flat btn-huge" style="background-color: <?php echo $btn_1_color; ?>; border-color: <?php echo $btn_1_color ?>;  box-shadow: 0 2px 0 <?php echo $btn_1_color ?>; "><?=$language_based_content['Resend_Invitation'];?></a>
        </p>
    </div>
<?php endif; ?>
<?php //if ($users['users']['type'] == 'Active'): ?>
<div class="permission-header-box style5 mmargin-top">
    <h2><?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?>’s <?=$language_based_content['role']?> </h2>
    <form accept-charset="UTF-8" action="<?php echo $this->base . '/permissions/change_permission/' . $users['accounts']['id'] . '/' . $users['users']['id'] ?>" enctype="multipart/form-data" method="post" style="">
        <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓"></div>
        <?php if ($users['roles']['role_id'] == '100'): ?>
            <h2 class="inline"><?php echo $language_based_content['account_owner_people_section_single']; ?></h2>
            <a onMouseOut="hide_sidebar()" href="#" class="appendix appendix--info"></a>
            <div class="appendix-content down">
                <h3 style="margin-bottom: 0"><?=$language_based_content['info']?></h3>
                <p>
                    <?php echo $language_based_content['account_owner_people_section_detail'].$language_based_content['account_owner_people_section_detail_1']; ?>
                    <!--The account owner is responsible for managing the account. They are the only member of the account who can manage account settings. They can change the colors and logos of the account, manage plans and billing, and cancel the account.-->
                </p>
            </div>
        <?php endif; ?>
        <div class="input-group styled-input-group <?php echo $disabled_role_change_class ?> ">
            <p>
                <label>
                    <span class="radio-substitute checked">
                        <input  <?php echo $priviliges_role_disables; ?> <?php echo $super_user_disable; ?> <?php echo ($users['roles']['role_id'] == '110') ? 'checked="checked"' : '' ?>  class="radio" id="role_super_admin" name="role" type="radio" value="110">
                    </span>
                    <b><?php echo $language_based_content['super_admin_people_section_single']; ?></b> -
                    <span class="label-desc"><?php echo $this->Custom->parse_translation_params($language_based_content['allow_to_create_mod_people_section'], ['first_name'=>$users['users']['first_name'], 'last_name' => $users['users']['last_name']]);?></span>
                </label>
            </p>
            <p>
                <label>
                    <span class="radio-substitute checked">
                        <input  <?php echo $priviliges_role_disables; ?> <?php echo $super_user_disable; ?> <?php echo ($users['UserAccount']['role_id'] == '115') ? 'checked="checked"' : '' ?>  class="radio" id="role_super_admin" name="role" type="radio" value="115">
                    </span>
                    <b><?php echo $language_based_content['admin_people_section_single']; ?></b> -
                    <span class="label-desc"><?php echo $this->Custom->parse_translation_params($language_based_content['admin_allow_to_create_mod_people_section'], ['first_name'=>$users['users']['first_name'], 'last_name' => $users['users']['last_name']]);?> </span>
                </label>
            </p>
            <p>
                <label>
                    <span class="radio-substitute">
                        <input <?php echo $priviliges_role_disables; ?>  <?php echo ($users['roles']['role_id'] == '120') ? 'checked="checked"' : '' ?> class="radio" id="role_admin" name="role" type="radio" value="120"></span> <b><?php echo $language_based_content['user_people_section_single']; ?></b> -
                    <span class="label-desc"><?php echo $this->Custom->parse_translation_params($language_based_content['user_allow_to_create_mod_people_section'], ['first_name'=>$users['users']['first_name'], 'last_name' => $users['users']['last_name']]);?></span>
                </label>
            </p>
            <p>
                <label>
                    <span class="radio-substitute checked">
                        <input  <?php echo $priviliges_role_disables; ?> <?php echo $super_user_disable; ?> <?php echo ($users['UserAccount']['role_id'] == '125') ? 'checked="checked"' : '' ?>  class="radio" id="role_super_admin" name="role" type="radio" value="125">
                    </span>
                    <b><?php echo $language_based_content['viewer_people_section_single']; ?></b> -
                    <span class="label-desc"><?php echo $this->Custom->parse_translation_params($language_based_content['user_allow_to_create_mod_people_section'], ['first_name'=>$users['users']['first_name'], 'last_name' => $users['users']['last_name']]);?> </span>
                </label>
            </p>
        </div>
        <input id="user_id" name="user_id" type="hidden" value="<?php echo $users['users']['id']; ?>">
        <?php if ($users['roles']['role_id'] != '100'): ?>
            <button style="background-color: <?php echo $btn_1_color; ?>; border-color: <?php echo $btn_1_color ?>; id="change-role" <?php echo $super_user_disable; ?> <?php echo ($permission_role_set == "not_allow") ? 'disabled' : ''; ?>  class="btn btn-green" type="submit" <?php if ($users['users']['id'] == $current_user['User']['id']): ?> onclick="return confirm('Are you sure you want to change your role?')" <?php endif; ?>><?php echo $language_based_content['change_role_people_section']; ?></button>
        <?php endif; ?>
    </form>
    <p id="role_msg"></p>
    <hr class="full dashed">
    <?php if ($users['roles']['role_id'] == '120'): ?>

        <h2>Extra Privileges</h2>
        <form accept-charset="UTF-8" action="<?php echo $this->base . '/permissions/associate_video_huddle_user/' . $users['accounts']['id'] . '/' . $users['users']['id'] ?>" enctype="multipart/form-data" method="post" style="margin-left: 15px;">
            <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓"></div>
            <div class="input-group styled-input-group <?php echo $disabled_class2; ?>">
                <label class=""><span class="checkbox-substitute"><input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?> <?php echo ($users['UserAccount']['folders_check'] == 1) ? 'checked="checked"' : '' ?> class="checkbox" id="video_huddle_ids_" name="folders_check" type="checkbox" value="1"></span> <?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?> <?php echo $language_based_content['can_create_manage_huddles_people_section']; ?> </label>
                <label class=""><span class="checkbox-substitute"><input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?> <?php echo ($users['UserAccount']['manage_collab_huddles'] == 1) ? 'checked="checked"' : '' ?> class="checkbox" id="video_huddle_ids_" name="manage_collab_huddles" type="checkbox" value="1"></span> <?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?> <?php echo $language_based_content['can_create_manage_collab_people_section']; ?> </label>
                <?php if ($this->Custom->get_account_video_permissions($current_user['users_accounts']['account_id'])): ?>
                    <label class=""><span class="checkbox-substitute"><input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?>  <?php echo ($users['UserAccount']['permission_access_video_library'] == 1) ? 'checked="checked"' : '' ?> class="checkbox" id="video_huddle_ids_" name="permission_access_video_library" type="checkbox" value="1"></span> <?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?> <?php echo $language_based_content['can_access_video_library_people_section']; ?> </label>
                <?php endif; ?>
                <label class=""><span class="checkbox-substitute"><input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?>  <?php echo ($users['UserAccount']['parmission_access_my_workspace'] == 1) ? 'checked="checked"' : '' ?>class="checkbox" id="video_huddle_ids_" name="parmission_access_my_workspace" type="checkbox" value="1"></span> <?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?> <?php echo $language_based_content['can_access_workspace_people_section']; ?> </label>

                <?php if ($this->Custom->get_account_video_permissions($current_user['users_accounts']['account_id'])): ?>
                    <label class=""><span class="checkbox-substitute"><input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?> <?php echo ($users['UserAccount']['permission_video_library_upload'] == 1) ? 'checked="checked"' : '' ?>class="checkbox" id="video_huddle_ids_" name="permission_video_library_upload" type="checkbox" value="1"></span> <?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?> <?php echo $language_based_content['can_upload_videos_resources_people_section']; ?> </label>
                <?php endif; ?>
                <?php if ($this->Custom->check_enable_analytics($current_user['users_accounts']['account_id'])): ?>
                    <label class=""><span class="checkbox-substitute"><input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?> <?php echo ($users['UserAccount']['permission_view_analytics'] == 1) ? 'checked="checked"' : '' ?>class="checkbox" id="video_huddle_ids_" name="permission_view_analytics" type="checkbox" value="1"></span> <?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?> <?php echo $language_based_content['can_view_personal_analytics_people_section']; ?> </label>
                <?php endif; ?>
                <?php if ($this->Custom->check_if_live_rec_active($current_user['users_accounts']['account_id'])): ?>
                    <label class=""><span class="checkbox-substitute"><input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?>  <?php echo ($users['UserAccount']['live_recording'] == 1) ? 'checked="checked"' : '' ?>class="checkbox" id="video_huddle_ids_" name="live_recording" type="checkbox" value="1"></span> <?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?> <?php echo $language_based_content['can_access_live_recordings_people_section']; ?> </label>
                <?php endif; ?>

                <label class=""><span class="checkbox-substitute"><input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?>  <?php echo ($users['UserAccount']['permission_start_synced_scripted_notes'] == 1) ? 'checked="checked"' : '' ?>class="checkbox" id="video_huddle_ids_" name="permission_start_synced_scripted_notes" type="checkbox" value="1"></span> <?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?> <?php echo $language_based_content['can_start_synced_scripted_notes_people_section']; ?> </label>


            </div>
            <input id="user_id" name="user_account_id" type="hidden" value="<?php echo $users['UserAccount']['id']; ?>">
            <button <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?> style="background-color: <?php echo $btn_1_color; ?>; border-color: <?php echo $btn_1_color ?>;" class="btn btn-green" type="submit"><?php echo $language_based_content['save_changes_people_section']; ?></button>
        </form>
    <?php elseif ($users['roles']['role_id'] == '115'): ?>
        <h2>Extra Privileges</h2>
        <form accept-charset="UTF-8" action="<?php echo $this->base . '/permissions/associate_video_huddle_user/' . $users['accounts']['id'] . '/' . $users['users']['id'] ?>" enctype="multipart/form-data" method="post" style="margin-left: 15px;">
            <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓"></div>
            <div class="input-group styled-input-group <?php echo $disabled_class2; ?>">

                <label class=""><span class="checkbox-substitute"><input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?> <?php echo ($users['UserAccount']['folders_check'] == 1) ? 'checked="checked"' : '' ?> class="checkbox" id="video_huddle_ids_" name="folders_check" type="checkbox" value="1"></span> <?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?> <?php echo $language_based_content['can_create_manage_huddles_people_section']; ?> </label>

                <?php if ($this->Custom->get_account_video_permissions($current_user['users_accounts']['account_id'])): ?>
                    <label class=""><span class="checkbox-substitute"><input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?> <?php echo ($users['UserAccount']['permission_video_library_upload'] == 1) ? 'checked="checked"' : '' ?>class="checkbox" id="video_huddle_ids_" name="permission_video_library_upload" type="checkbox" value="1"></span> <?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?> <?php echo $language_based_content['can_upload_videos_resources_people_section']; ?> </label>
                <?php endif; ?>
                <?php if ($this->Custom->check_enable_analytics($current_user['users_accounts']['account_id'])): ?>
                    <label class=""><span class="checkbox-substitute"><input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?> <?php echo ($users['UserAccount']['permission_view_analytics'] == 1) ? 'checked="checked"' : '' ?>class="checkbox" id="video_huddle_ids_" name="permission_view_analytics" type="checkbox" value="1"></span> <?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?> <?php echo $language_based_content['can_view_account_wide_analytics_people_section']; ?> </label>
                <?php endif; ?>
                <label class=""><span class="checkbox-substitute"><input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?>  <?php echo ($users['UserAccount']['permission_administrator_user_new_role'] == 1) ? 'checked="checked"' : '' ?>class="checkbox" id="video_huddle_ids_" name="permission_administrator_user_new_role" type="checkbox" value="1"></span> <?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?> <?php echo $language_based_content['can_manage_users_in_account_people_section']; ?> </label>
                <?php if ($this->Custom->check_if_live_rec_active($current_user['users_accounts']['account_id'])): ?>
                    <label class=""><span class="checkbox-substitute"><input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?>  <?php echo ($users['UserAccount']['live_recording'] == 1) ? 'checked="checked"' : '' ?>class="checkbox" id="video_huddle_ids_" name="live_recording" type="checkbox" value="1"></span> <?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?> <?php echo $language_based_content['can_access_live_recordings_people_section']; ?> </label>
                <?php endif; ?>

            </div>
            <input id="user_id" name="user_account_id" type="hidden" value="<?php echo $users['UserAccount']['id']; ?>">
            <button <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?> style="background-color: <?php echo $btn_1_color; ?>; border-color: <?php echo $btn_1_color ?>;" class="btn btn-green" type="submit"><?php echo $language_based_content['save_changes_people_section']; ?></button>
        </form>
    <?php endif; ?>
    <p id="vh_msg"></p>
    <?php
    if ($huddles && count($huddles) > 0):

        $user_ids = array();
        $role_admin = array();
        $role_user = array();
        $role_viewer = array();
        $is_coach = array();
        $is_mentor = array();
        $is_coaching_huddle = false;
        $is_evaluation_huddle = false;
        if ($huddleRoles && count($huddleRoles) > 0) {
            foreach ($huddleRoles as $row) {
                $account_folder_ids[] = $row['AccountFolderUser']['account_folder_id'];
                if ($row['AccountFolderUser']['role_id'] == '200') {
                    $role_admin[] = $row['AccountFolderUser']['account_folder_id'];
                }
                if ($row['AccountFolderUser']['role_id'] == '210') {
                    $role_user[] = $row['AccountFolderUser']['account_folder_id'];
                }
                if ($row['AccountFolderUser']['role_id'] == '220') {
                    $role_viewer[] = $row['AccountFolderUser']['account_folder_id'];
                }
                if ($row['AccountFolderUser']['is_coach'] == '1') {
                    $is_coach[] = $row['AccountFolderUser']['account_folder_id'];
                }
                if ($row['AccountFolderUser']['is_mentee'] == '1') {
                    $is_mentor[] = $row['AccountFolderUser']['account_folder_id'];
                }
            }
        }

        foreach ($huddles as $row):
            if ($row['AccountFolderMetaData']['meta_data_value'] == '2') {
                $is_coaching_huddle = true;
            } elseif ($row['AccountFolderMetaData']['meta_data_value'] == '3') {
                $is_evaluation_huddle = true;
            }
        endforeach;
        ?>

        <div class="row-fluid">
            <?php
            $string = $this->Custom->parse_translation_params($language_based_content['collab_huddles_has_permission_people_section'], ['first_name'=>$users['users']['first_name'], 'last_name' => $users['users']['last_name']]);
            $coachee_string = $this->Custom->parse_translation_params($language_based_content['coaching_huddles_has_permission_people_section'], ['first_name'=>$users['users']['first_name'], 'last_name' => $users['users']['last_name']]);
            $evaluation_string = $this->Custom->parse_translation_params($language_based_content['assessment_huddles_has_permission_people_section'], ['first_name'=>$users['users']['first_name'], 'last_name' => $users['users']['last_name']]);
            ?>
            <h2><?php echo $string; ?></h2>
            <form onkeypress="return event.keyCode != 13;" accept-charset="UTF-8" action="<?php echo $this->base . '/permissions/associate_huddle_user/' . $users['accounts']['id'] . '/' . $users['users']['id'] ?>" enctype="multipart/form-data" method="post" style="margin-left: 15px;">
                <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓"></div>


                <div class="groups-table span9 prem1" style="margin-left:0px; margin-top:0px;">
                    <div style="margin-left:0px;" class="span4 huddle-span4">
                        <div class="groups-table-header">
                            <div style="clear: both;"></div>
                            <div id="searchdiv" class="filterform">

                                <fieldset>
                                    <input type="search" name="search" value="" id="id_search" placeholder='<?php echo $language_based_content['search_huddles_people_section']; ?>' class="filterinput" style="margin-right: 44px;width: 233px; float: right;">
                                    <div id='cancel-srch' style='width:10px;position:absolute; right:330px; margin-top:7px;cursor:pointer;display:none;'>X</div>
                                </fieldset>

                            </div>
                            <div style="float: left;width: 300px; margin-left: -7px; ">
                                <div  class="select-all-none" style="float: left;  margin-top: 5px;">
                                    <label style="width: 300px;" id="select-all-none"  class="checkbox-substitute" for="select-all">
                                        <span class="checkbox-substitute select-all">
                                            <input type="checkbox" name="select-all" id="select-all"/>
                                            <input type="hidden" name="current_role" value="<?php echo $users['roles']['role_id']; ?>" id="current_role"/>
                                        </span>
                                        <span id="select-all-label"><?php echo $language_based_content['add_all_to_huddles_people_section']; ?> </span>
                                    </label>
                                </div>
                            </div>
                            <div class="huddles-select-all-block" style="margin-top:10px !important;">
                                <span class="admin-radio"><input type="radio" style=" margin-left: 7px; " id="admin-radio" name="select-all"> <?php echo $language_based_content['admin_people_section']; ?></span>
                                <span class="member-radio"><input type="radio" id="member-radio" name="select-all"> <?php echo $language_based_content['member_people_section']; ?></span>
                                <span class="viewer-radio"><input type="radio" id="viewers-radio" name="select-all"> <?php echo $language_based_content['viewer_people_section']; ?></span>
                            </div>
                            <div style="clear: both;"></div>
                        </div>
                        <div style="clear: both;"></div>
                        <div class="groups-table-content">
                            <div class="scrolable_box">
                                <style>
                                    .my_check_box input[type="checkbox"] {
                                        display:none;
                                    }
                                    .my_check_box input[type="checkbox"] + label span {
                                        display:inline-block;
                                        width:19px;
                                        height:18px;
                                        margin:1px 4px 0 0;
                                        vertical-align:middle;
                                        background:url(/app/img/new/checkbox.png) no-repeat 0 0;
                                        cursor:pointer;
                                    }
                                    .my_check_box input[type="checkbox"]:checked + label span {
                                        background:url(/app/img/new/checkbox.png) no-repeat 0 -18px;
                                    }
                                    .scrolable_box {
                                        height:200px;
                                        overflow-y:auto;
                                        padding-left: 12px;
                                    }

                                </style>
                                <div>
                                    <div style="top: 0px;">
                                        <div id="huddles-lists">
                                            <ul id="permission-list-containers">
                                                <?php
                                                $count = 0;
                                                $roles_id = '';
                                                foreach ($huddles as $row):
                                                    if ($row['AccountFolderMetaData']['meta_data_value'] == '1') {
                                                        if (!in_array($row['AccountFolder']['account_folder_id'], $role_admin) && !in_array($row['AccountFolder']['account_folder_id'], $role_user) && !in_array($row['AccountFolder']['account_folder_id'], $role_viewer)) {

                                                            $role_viewer[] = $row['AccountFolder']['account_folder_id'];
                                                        }
                                                        ?>
                                                        <li>

                                                            <label for="account_folder_ids_<?php echo $row['AccountFolder']['account_folder_id'] ?>">
                                                                <input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?>   class="checkbox all-huddles" id="account_folder_ids_<?php echo $row['AccountFolder']['account_folder_id'] ?>"    <?php echo (isset($associate_huddle_users) && is_array($associate_huddle_users) && count($associate_huddle_users) > 0 && in_array($row['AccountFolder']['account_folder_id'], $associate_huddle_users) !== FALSE) ? 'checked="checked"' : ''; ?> name="account_folder_ids[]" type="checkbox" value="<?php echo $row['AccountFolder']['account_folder_id'] ?>">
                                                                <a style="color: #757575; font-weight: normal;"> <?php echo $row['AccountFolder']['name'] ?></a>
                                                            </label>
                                                            <div class="permissions">
                                                                <label for="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>_200">
                                                                    <input  type="radio"  <?php echo (((in_array($row['AccountFolder']['account_folder_id'], $role_admin) !== FALSE)) ? 'checked="checked"' : '') ?>  class="admin-btn"  value="200" id="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>_200 ?>" name="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>"><?php echo $language_based_content['admin_people_section']; ?>
                                                                </label>
                                                                <label for="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>_210">
                                                                    <input type="radio"   <?php echo (((in_array($row['AccountFolder']['account_folder_id'], $role_user) !== FALSE)) ? 'checked="checked"' : '') ?> value="210" class="member-btn" id="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>_210>" name="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>"><?php echo $language_based_content['member_people_section']; ?>
                                                                </label>
                                                                <?php ?>
                                                                <label for="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>_220">
                                                                    <input class="viewer-btn" type="radio"   <?php echo (((in_array($row['AccountFolder']['account_folder_id'], $role_viewer) !== FALSE)) ? 'checked="checked"' : '') ?> value="220" id="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>_220" name="user_role_<?php echo $row['AccountFolder']['account_folder_id'] ?>"><?php echo $language_based_content['viewer_people_section']; ?>
                                                                </label>
                                                            </div>
                                                        </li>
                                                        <?php $count++; ?>
                                                    <?php } endforeach; ?>
                                                <li id="noresults"><?=$language_based_content['No_Huddles_match_this_search_people_section'];?></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear" style="clear: both;  "></div>

                <?php if ($is_coaching_huddle && $users['roles']['role_id'] != '125'): ?>
                    <h2 style="margin-left: -14px;"><?php echo $coachee_string; ?></h2>

                    <div class="groups-table span9 prem2" style="margin-left:0px; margin-top:0px;">
                        <div style="margin-left:0px;" class="span4 huddle-span4">
                            <div class="groups-table-header">
                                <div style="clear: both;"></div>
                                <div id="searchdiv1" class="filterform">

                                    <fieldset>
                                        <input type="search" name="search" value="" id="id_search1" placeholder='<?php echo $language_based_content['search_huddles_people_section']; ?>' class="filterinput" style="margin-right: 44px;width: 233px; float: right;">
                                        <div id='cancel-srch1' style='width:10px;position:absolute; right:330px; margin-top:7px;cursor:pointer;display:none;'>X</div>
                                    </fieldset>

                                </div>
                                <div style="float: left;width: 300px; margin-left: -7px; ">
                                    <div  class="select-all-none2" style="float: left;  margin-top: 5px;">
                                        <label style="width: 300px;" id="select-all-none2"  class="checkbox-substitute2" for="select-all-none2">
                                            <span class="checkbox-substitute select-all2">
                                                <input type="checkbox" name="select-all" id="select-all2"/>
                                            </span>
                                            <span id="select-all-label2"><?php echo $language_based_content['add_all_to_huddles_people_section']; ?> </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="huddles-select-all-block" style="width: 209px;margin-top:9px; margin: 5px 0px 0px 0px;">
                                    <span class="admin-radio" style="width: 79px;"><input type="radio" style="" id="coach-radio" name="select-all">&nbsp;<label for="coach-radio"><?php echo $language_based_content['coach_people_section']; ?></label></span>
                                    <span class="member-radio" style="width: 89px;text-align: left;"><input type="radio" id="coachee-radio" name="select-all">&nbsp;<label for="coachee-radio"><?php echo $language_based_content['coachee_people_section']; ?></label></span>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="groups-table-content">
                                <div class="scrolable_box">
                                    <div>
                                        <div style="top: 0px;">
                                            <div id="huddles-lists">
                                                <ul id="permission-list-containers2">
                                                    <?php
                                                    $count = 0;
                                                    $roles_id = '';
                                                    foreach ($huddles as $row):
                                                        if ($row['AccountFolderMetaData']['meta_data_value'] == '2') {
                                                            if (!in_array($row['AccountFolder']['account_folder_id'], $role_admin) && !in_array($row['AccountFolder']['account_folder_id'], $role_user) && !in_array($row['AccountFolder']['account_folder_id'], $role_viewer)) {
                                                                $role_viewer[] = $row['AccountFolder']['account_folder_id'];
                                                            }
                                                            ?>
                                                            <li>

                                                                <label class="my_check_box" for="account_folder_ids_<?php echo $row['AccountFolder']['account_folder_id'] ?>">
                                                                    <a style="color: #757575; font-weight: normal;">
                                                                        <input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?> id="account_folder_ids_<?php echo $row['AccountFolder']['account_folder_id'] ?>" <?php echo ((isset($associate_huddle_users) && is_array($associate_huddle_users) && count($associate_huddle_users) > 0 && in_array($row['AccountFolder']['account_folder_id'], $associate_huddle_users) !== FALSE && (in_array($row['AccountFolder']['account_folder_id'], $is_coach) !== FALSE || in_array($row['AccountFolder']['account_folder_id'], $is_mentor) !== FALSE) ) ? 'checked="checked"' : '') ?> name="coach_account_folder_id[]" type="checkbox" value="<?php echo $row['AccountFolder']['account_folder_id'] ?>">



                                                                        <label for="account_folder_ids_<?php echo $row['AccountFolder']['account_folder_id'] ?>" <?php echo ((isset($associate_huddle_users) && is_array($associate_huddle_users) && count($associate_huddle_users) > 0 && in_array($row['AccountFolder']['account_folder_id'], $associate_huddle_users) !== FALSE && (in_array($row['AccountFolder']['account_folder_id'], $is_coach) !== FALSE || in_array($row['AccountFolder']['account_folder_id'], $is_mentor) !== FALSE) ) ? 'checked="checked"' : '') ?> ><span></span></label> <?php echo $row['AccountFolder']['name'] ?></a>
                                                                </label>
                                                                <div class="permissions">
                                                                    <label for="user_coach_<?php echo $row['AccountFolder']['account_folder_id'] ?>_200">
                                                                        <input  type="radio"  <?php echo (((in_array($row['AccountFolder']['account_folder_id'], $is_coach) !== FALSE)) ? 'checked="checked"' : '') ?>  class="coach-btn"  value="200" id="user_coach_<?php echo $row['AccountFolder']['account_folder_id'] ?>_200" name="user_coach_<?php echo $row['AccountFolder']['account_folder_id'] ?>"><?php echo $language_based_content['coach_people_section']; ?>
                                                                    </label>
                                                                    <label for="user_coachee_<?php echo $row['AccountFolder']['account_folder_id'] ?>_210">
                                                                        <input type="radio"   <?php echo (((in_array($row['AccountFolder']['account_folder_id'], $is_mentor) !== FALSE) || (in_array($row['AccountFolder']['account_folder_id'], $is_coach) == FALSE && in_array($row['AccountFolder']['account_folder_id'], $is_mentor) == FALSE)) ? 'checked="checked"' : '') ?> value="210" class="coachee-btn" id="user_coachee_<?php echo $row['AccountFolder']['account_folder_id'] ?>_210" name="user_coach_<?php echo $row['AccountFolder']['account_folder_id'] ?>"><?php echo $language_based_content['coachee_people_section']; ?>
                                                                    </label>
                                                                </div>
                                                            </li>
                                                            <?php $count++; ?>
                                                            <?php
                                                        }
                                                    endforeach;
                                                    ?>
                                                    <li id="noresults1"><?=$language_based_content['No_Huddles_match_this_search_people_section'];?></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear" style="clear: both;  "></div>

                <?php endif; ?>
                <?php if ($is_evaluation_huddle && $this->Custom->check_if_eval_huddle_active($users['accounts']['id']) && $users['roles']['role_id'] != '125'): ?>

                    <h2 style="margin-left: -14px;"><?php echo $evaluation_string; ?></h2>
                    <div class="groups-table span9 prem3 " style="margin-left:0px; margin-top:0px;">
                        <div style="margin-left:0px;" class="span4 huddle-span4">
                            <div class="groups-table-header">
                                <div style="clear: both;"></div>
                                <div id="searchdiv2" class="filterform">

                                    <fieldset>
                                        <input type="search" name="search" value="" id="id_search2" placeholder='<?php echo $language_based_content['search_huddles_people_section']; ?>' class="filterinput" style="margin-right: 44px;width: 233px; float: right;">
                                        <div id='cancel-srch2' style='width:10px;position:absolute; right:330px; margin-top:7px;cursor:pointer;display:none;'>X</div>
                                    </fieldset>

                                </div>
                                <div style="float: left;width: 381px;margin-left: -7px; ">
                                    <div  class="select-all-none3" style="float: left;  margin-top: 5px;">
                                        <label style="width: 300px;" id="select-all-none3"  class="checkbox-substitute2" for="select-all3">
                                            <span class="checkbox-substitute select-all3">
                                                <input type="checkbox" name="select-all" id="select-all3"/>
                                            </span>
                                            <span id="select-all-label3"><?php echo $language_based_content['add_all_to_huddles_people_section']; ?> </span>
                                        </label>
                                    </div>
                                </div>
                                <div class="huddles-select-all-block" style="margin-right:41px !important;">
                                    <span class="admin-radio" style="width: 97px;margin-left: 3px;position: relative;left: 7px;top: 5px;"><input type="radio" style=" margin-left: 7px; " id="eval-coach-radio" name="select-all"><label for="eval-coach-radio">&nbsp;<?php echo $language_based_content['assessor_people_section']; ?></label></span>
                                    <span class="member-radio" style="width: 175px;position: relative;top: 5px;"><input type="radio" id="eval-coachee-radio" name="select-all"><label for="eval-coachee-radio">&nbsp;<?php echo $language_based_content['assessed_participant_people_section']; ?></label></span>
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                            <div style="clear: both;"></div>
                            <div class="groups-table-content">
                                <div class="scrolable_box">
                                    <div>
                                        <div style="top: 0px;">
                                            <div id="huddles-lists">
                                                <ul id="permission-list-containers3">
                                                    <?php
                                                    $count = 0;
                                                    $roles_id = '';
                                                    foreach ($huddles as $row):
                                                        if ($row['AccountFolderMetaData']['meta_data_value'] == '3') {
                                                            if (!in_array($row['AccountFolder']['account_folder_id'], $role_admin) && !in_array($row['AccountFolder']['account_folder_id'], $role_user) && !in_array($row['AccountFolder']['account_folder_id'], $role_viewer)) {
                                                                $role_viewer[] = $row['AccountFolder']['account_folder_id'];
                                                            }
                                                            ?>
                                                            <li>

                                                                <label class="my_check_box" for="account_folder_ids_<?php echo $row['AccountFolder']['account_folder_id'] ?>">
                                                                    <a style="color: #757575; font-weight: normal;">
                                                                        <input <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?> id="account_folder_ids_<?php echo $row['AccountFolder']['account_folder_id'] ?>" <?php echo ((isset($associate_huddle_users) && is_array($associate_huddle_users) && count($associate_huddle_users) > 0 && in_array($row['AccountFolder']['account_folder_id'], $associate_huddle_users) !== FALSE && (in_array($row['AccountFolder']['account_folder_id'], $is_coach) !== FALSE || in_array($row['AccountFolder']['account_folder_id'], $is_mentor) !== FALSE) ) ? 'checked="checked"' : '') ?> name="coach_account_folder_id[]" type="checkbox" value="<?php echo $row['AccountFolder']['account_folder_id'] ?>">
                                                                        <label for="account_folder_ids_<?php echo $row['AccountFolder']['account_folder_id'] ?>" <?php echo ((isset($associate_huddle_users) && is_array($associate_huddle_users) && count($associate_huddle_users) > 0 && in_array($row['AccountFolder']['account_folder_id'], $associate_huddle_users) !== FALSE && (in_array($row['AccountFolder']['account_folder_id'], $is_coach) !== FALSE || in_array($row['AccountFolder']['account_folder_id'], $is_mentor) !== FALSE) ) ? 'checked="checked"' : '') ?> ><span></span></label> <?php echo $row['AccountFolder']['name'] ?></a>
                                                                </label>
                                                                <div class="permissions">
                                                                    <label for="user_coach_<?php echo $row['AccountFolder']['account_folder_id'] ?>_200">
                                                                        <input  type="radio"  <?php echo (((in_array($row['AccountFolder']['account_folder_id'], $is_coach) !== FALSE)) ? 'checked="checked"' : '') ?>  class="eval-coach-btn"  value="200" id="user_coach_<?php echo $row['AccountFolder']['account_folder_id'] ?>_200" name="user_coach_<?php echo $row['AccountFolder']['account_folder_id'] ?>"><?php echo $language_based_content['assessor_people_section']; ?>
                                                                    </label>
                                                                    <label for="user_coachee_<?php echo $row['AccountFolder']['account_folder_id'] ?>_210">
                                                                        <input type="radio"   <?php echo (((in_array($row['AccountFolder']['account_folder_id'], $is_mentor) !== FALSE) || (in_array($row['AccountFolder']['account_folder_id'], $is_coach) == FALSE && in_array($row['AccountFolder']['account_folder_id'], $is_mentor) == FALSE)) ? 'checked="checked"' : '') ?> value="210" class="eval-coachee-btn" id="user_coachee_<?php echo $row['AccountFolder']['account_folder_id'] ?>_210" name="user_coach_<?php echo $row['AccountFolder']['account_folder_id'] ?>"><?php echo $language_based_content['assessed_participant_people_section']; ?>
                                                                    </label>
                                                                </div>
                                                            </li>
                                                            <?php $count++; ?>
                                                            <?php
                                                        }
                                                    endforeach;
                                                    ?>
                                                    <li id="noresults2"><?=$language_based_content['No_Huddles_match_this_search_people_section'];?></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clear" style="clear: both;  "></div>
                <?php endif; ?>

                <p class="huddle-permission-save-container">
                    <input id="user_id" name="user_id" type="hidden" value="<?php echo $users['users']['id']; ?>">
                    <button <?php echo ($permission_set == "not_allow") ? 'disabled' : ''; ?>  style="background-color: <?php echo $btn_1_color; ?>; border-color: <?php echo $btn_1_color ?>;" class="btn btn-green" type="submit"><?php echo $language_based_content['save_changes_people_section']; ?></button>
                </p>
            </form>
        </div>
    <?php endif; ?>
    <p id="pc_msg"></p>
</div>


<?php //else:    ?>
<!--    <div class="text-center permission-header-box style5 lmargin-top">
        <p class="hmargin-top hmargin-bottom">
<?php echo $this->Html->image('new/sad-face.png'); ?>
        </p>
        <h1><?php echo $users['users']['first_name'] . ' ' . $users['users']['last_name']; ?> has not logged into Sibme yet</h1>
        <p>Click below to resend <?php echo $users['users']['email']; ?> an invitation notification to join your account.</p>
        <p>
            <a href="<?php echo $this->base . '/users/resendInvitation/' . $users['accounts']['id'] . '/' . $users['users']['id'] ?>" class="btn btn-green btn-flat btn-huge mmargin-top hmargin-bottom">Resend Invitation</a>
        </p>
    </div>-->

<?php //endif;    ?>
<script>
    $(document).ready(function () {
        $("#id_search").quicksearch("#permission-list-containers li", {
            noResults: '#noresults',
            stripeRows: ['odd', 'even'],
            loader: 'span.loading',
            minValLength: 2
        });
        $('#cancel-srch').click(function (e) {
            $("#searchdiv").triggerHandler("focus");
            $("#id_search").val('').trigger('keyup');
        });
        $("#id_search").keyup(function () {
            if ($('#id_search').val().length > 0)
                $('#cancel-srch').css('display', 'block');
            else
                $('#cancel-srch').css('display', 'none');
        });
        $("#id_search1").quicksearch("#permission-list-containers2 li", {
            noResults: '#noresults1',
            stripeRows: ['odd', 'even'],
            loader: 'span.loading',
            minValLength: 2
        });
        $("#id_search2").quicksearch("#permission-list-containers3 li", {
            noResults: '#noresults2',
            stripeRows: ['odd', 'even'],
            loader: 'span.loading',
            minValLength: 2
        });
        $('#cancel-srch1').click(function (e) {
            $("#searchdiv1").triggerHandler("focus");
            $("#id_search1").val('').trigger('keyup');
        });
        $('#cancel-srch2').click(function (e) {
            $("#searchdiv2").triggerHandler("focus");
            $("#id_search2").val('').trigger('keyup');
        });
        $("#id_search1").keyup(function () {
            if ($('#id_search1').val().length > 0)
                $('#cancel-srch1').css('display', 'block');
            else
                $('#cancel-srch1').css('display', 'none');
        });
        $("#id_search2").keyup(function () {
            if ($('#id_search2').val().length > 0)
                $('#cancel-srch2').css('display', 'block');
            else
                $('#cancel-srch2').css('display', 'none');
        });


        var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><a href="/users/administrators_groups">People</a><span><?php echo $breadcrumb_language_based_content['user_permissions_breadcrumb']; ?></span></div>';

        $('.breadCrum').html(bread_crumb_data);

    });
</script>
<script type="text/javascript">
    $(document).ready(function (e) {
        $('#select-all-none').click(function (e) {

            if ($('#select-all').is(':checked')) {
                $('#select-all-label').html('<?php echo $language_based_content['remove_from_all_huddles_people_section']; ?>');
                $('.select-all-none label span .checkbox-substitute').addClass('checked');
                $('#permission-list-containers li label .checkbox-substitute').addClass('checked');
                $('.select-all').addClass('checked');
                $('#select-all').prop('checked', true);
                $(".all-huddles").prop('checked', true);


            } else {
                $('#select-all-label').html('<?php echo $language_based_content['add_all_to_huddles_people_section']; ?>');
                $('#permission-list-containers li label .checkbox-substitute').removeClass('checked');
                $('.checkbox-substitute label input .all-huddles').removeClass('checked');
                $('.select-all').removeClass('checked');
                $('#select-all').prop('checked', false);
                $(".all-huddles").prop('checked', false);
            }

        });
        $('#select-all-none2').click(function (e) {

            if ($('#select-all2').is(':checked')) {
                $('#select-all-label2').html('<?php echo $language_based_content['remove_from_all_huddles_people_section']; ?>');
                $('.select-all-none2 label span.checkbox-substitute').addClass('checked');
                $('#permission-list-containers2 li label .checkbox-substitute').addClass('checked');
                $('#permission-list-containers2 li label input[type="checkbox"]').prop("checked", true);
                $('.select-all2').addClass('checked');
                $('#select-all2').prop('checked', true);
                $(".all-huddles2").prop('checked', true);
            } else {
                $('#select-all-label2').html('<?php echo $language_based_content['add_all_to_huddles_people_section']; ?>');
                $('#permission-list-containers2 li label .checkbox-substitute').removeClass('checked');
                $('.checkbox-substitute label input .all-huddles2').removeClass('checked');
                $('#permission-list-containers2 li label input[type="checkbox"]').prop("checked", false);
                $('.select-all2').removeClass('checked');
                $('#select-all2').prop('checked', false);
                $(".all-huddles2").prop('checked', false);
            }

        });
        $('#select-all-none3').click(function (e) {
            if ($('#select-all3').is(':checked')) {
                $('#select-all-label3').html('<?php echo $language_based_content['remove_from_all_huddles_people_section']; ?>');
                $('.select-all-none3 label span.checkbox-substitute').addClass('checked');
                $('#permission-list-containers3 li label .checkbox-substitute').addClass('checked');
                $('#permission-list-containers3 li label input[type="checkbox"]').prop("checked", true);
                $('.select-all3').addClass('checked');
                $('#select-all3').prop('checked', true);
                $(".all-huddles3").prop('checked', true);
            } else {
                $('#select-all-label3').html('<?php echo $language_based_content['add_all_to_huddles_people_section']; ?>');
                $('#permission-list-containers3 li label .checkbox-substitute').removeClass('checked');
                $('.checkbox-substitute label input .all-huddles3').removeClass('checked');
                $('#permission-list-containers3 li label input[type="checkbox"]').prop("checked", false);
                $('.select-all3').removeClass('checked');
                $('#select-all3').prop('checked', false);
                $(".all-huddles3").prop('checked', false);
            }
        });
        $(document).on('click', '.admin-btn', function (e) {
//            e.preventDefault();
            var current_role = $('#current_role').val();
            if (typeof (current_role) != 'undefined' && current_role == 125) {
                alert("<?php echo $alert_messages['viewers_are_unable_to_participate']; ?>");
                return false;
            }

        });

        $(document).on('click', '.member-btn', function (e) {
//            e.preventDefault();
            var current_role = $('#current_role').val();
            if (typeof (current_role) != 'undefined' && current_role == 125) {
                alert("<?php echo $alert_messages['viewers_are_unable_to_participate']; ?>");
                return false;
            }
        });
        $("#admin-radio").click(function () {
            var current_role = $('#current_role').val();
            if (typeof (current_role) != 'undefined' && current_role == 125) {
                alert("<?php echo $alert_messages['viewers_are_unable_to_participate']; ?>");
                return false;
            }
            if ($(this).is(':checked')) {
                $(".member-btn").removeAttr("checked");
                $(".viewer-btn").removeAttr("checked");
                $(".admin-btn").prop('checked', true);
            }
        });
        $("#coach-radio").click(function () {
            if ($(this).is(':checked')) {
                $(".coachee-btn").removeAttr("checked");
                $(".coach-btn").prop('checked', true);
            }
        });
        $("#eval-coach-radio").click(function () {
            if ($(this).is(':checked')) {
                $(".eval-coachee-btn").removeAttr("checked");
                $(".eval-coach-btn").prop('checked', true);
            }
        });
        $("#coachee-radio").click(function () {
            if ($(this).is(':checked')) {
                $(".coach-btn").removeAttr("checked");
                $(".coachee-btn").prop('checked', true);
            }
        });
        $("#eval-coachee-radio").click(function () {
            if ($(this).is(':checked')) {
                $(".eval-coach-btn").removeAttr("checked");
                $(".eval-coachee-btn").prop('checked', true);
            }
        });
        $("#member-radio").click(function () {
            var current_role = $('#current_role').val();
            if (typeof (current_role) != 'undefined' && current_role == 125) {
                alert("<?php echo $alert_messages['viewers_are_unable_to_participate']; ?>");
                return false;
            }
            if ($(this).is(':checked')) {
                $(".admin-btn").removeAttr("checked");
                $(".viewer-btn").removeAttr("checked");
                $(".member-btn").prop('checked', true);
            }
        })
        $("#viewers-radio").click(function () {
            if ($(this).is(':checked')) {
                $(".admin-btn").removeAttr("checked");
                $(".member-btn").removeAttr("checked");
                $(".viewer-btn").prop('checked', true);
            }

        })
    });
    (function ($) {
        jQuery.expr[':'].Contains = function (a, i, m) {
            return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
        };
        function listFilter(header, list) {
            var form = $("<div>").attr({"class": "filterform"}),
                    input = $("<input id='input-filter' placeholder='<?php echo $language_based_content['search_huddles_people_section']; ?>'>  <div  id='cancel-btn' type='text' style=' width: 10px;  position: absolute; right: 330px; margin-top: 7px; display:none;cursor: pointer; '>X</div>").attr({"class": "filterinput", "type": "text"});
            $(form).append(input).appendTo(header);
            $('.filterform input').css({'width': '233px', 'float': 'right'});
            $(input).change(function (e) {
                e.preventDefault();
                var filter = $(this).val();
                if (filter) {
                    $search_count = $(list).find("a:Contains(" + filter + ")").length;
                    if ($search_count > 4) {
                        $('.widget-scrollable .scrollbar .thumb').css('top', '0px');
                        $('.widget-scrollable .overview').css('top', '0px');
                        $('.scrollbar').css('display', 'block');
                    } else {
                        $('.widget-scrollable .scrollbar .thumb').css('top', '0px');
                        $('.widget-scrollable .overview').css('top', '0px');
                        $('.scrollbar').css('display', 'none');
                    }
                    $(list).find("a:not(:Contains(" + filter + "))").parent().parent().slideUp(400, function () {
                        $('.widget-scrollable').tinyscrollbar_update();
                    });
                    $(list).find("a:Contains(" + filter + ")").parent().parent().slideDown(400, function () {
                        $('.widget-scrollable').tinyscrollbar_update();
                    });
                    $('#cancel-btn').css('display', 'block');
                    if ($(list).find("a:Contains(" + filter + ")").length == 0) {
                        $('#liNodataFound').remove();
                        $("#permission-list-containers").append('<li id="liNodataFound"><?=$language_based_content["No_Huddles_match_this_search_people_section"];?></li>');
                    } else {

                        $('#liNodataFound').remove();
                    }

                } else {
                    $('#cancel-btn').css('display', 'none');
                    $('.scrollbar').css('display', 'block');
                    $(list).find("li").slideDown(400, function () {
                        $('.widget-scrollable').tinyscrollbar_update();
                    });
                }
                return false;
            })
                    .keyup(function () {
                        $(this).change();
                    });

            $('#cancel-btn').click(function (e) {
                $('.widget-scrollable .scrollbar .thumb').css('top', '0px');
                $('.widget-scrollable .overview').css('top', '0px');
                $('#cancel-btn').css('display', 'none');
                $('#input-filter').val('');
                $('.scrollbar').css('display', 'block');
                $(list).find("li").slideDown(400, function () {
                    $('.widget-scrollable').tinyscrollbar_update();
                });
            })
        }
        function listFilter2(header, list) {
            var form = $("<div>").attr({"class": "filterform"}),
                    input = $("<input id='input-filter1' placeholder='<?php echo $language_based_content['search_huddles_people_section']; ?>'>  <div  id='cancel-btn1' type='text' style=' width: 10px;  position: absolute; right: 330px; margin-top: 7px; display:none;cursor: pointer; '>X</div>").attr({"class": "filterinput", "type": "text"});
            $(form).append(input).appendTo(header);
            $('.filterform input').css({'width': '233px', 'float': 'right'});
            $(input).change(function (e) {
                e.preventDefault();
                var filter = $(this).val();
                if (filter) {
                    $search_count = $(list).find("a:Contains(" + filter + ")").length;
                    if ($search_count > 4) {
                        $('.widget-scrollable .scrollbar .thumb').css('top', '0px');
                        $('.widget-scrollable .overview').css('top', '0px');
                        $('.scrollbar').css('display', 'block');
                    } else {
                        $('.widget-scrollable .scrollbar .thumb').css('top', '0px');
                        $('.widget-scrollable .overview').css('top', '0px');
                        $('.scrollbar').css('display', 'none');
                    }
                    $(list).find("a:not(:Contains(" + filter + "))").parent().parent().slideUp(400, function () {
                        $('.widget-scrollable').tinyscrollbar_update();
                    });
                    $(list).find("a:Contains(" + filter + ")").parent().parent().slideDown(400, function () {
                        $('.widget-scrollable').tinyscrollbar_update();
                    });
                    $('#cancel-btn1').css('display', 'block');
                    if ($(list).find("a:Contains(" + filter + ")").length == 0) {
                        $('#liNodataFound').remove();
                        $("#permission-list-containers2").append('<li id="liNodataFound"><?=$language_based_content["No_Huddles_match_this_search_people_section"];?></li>');
                    } else {

                        $('#liNodataFound').remove();
                    }

                } else {
                    $('#cancel-btn1').css('display', 'none');
                    $('.scrollbar').css('display', 'block');
                    $(list).find("li").slideDown(400, function () {
                        $('.widget-scrollable').tinyscrollbar_update();
                    });
                }
                return false;
            })
                    .keyup(function () {
                        $(this).change();
                    });

            $('#cancel-btn1').click(function (e) {
                $('.widget-scrollable .scrollbar .thumb').css('top', '0px');
                $('.widget-scrollable .overview').css('top', '0px');
                $('#cancel-btn1').css('display', 'none');
                $('#input-filter1').val('');
                $('.scrollbar').css('display', 'block');
                $(list).find("li").slideDown(400, function () {
                    $('.widget-scrollable').tinyscrollbar_update();
                });
            })
        }
        /*$(function () {
         listFilter($("#permission-header-container"), $("#permission-list-containers"));
         });
         $(function () {
         listFilter2($("#permission-header-container2"), $("#permission-list-containers2"));
         });*/
    }(jQuery));

</script>
