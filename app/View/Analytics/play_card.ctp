<?php
//$user = $this->Session->read('Auth');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');

$permission_maintain_folders = $user_current_account['users_accounts']['permission_maintain_folders'];
$user_role_id = $user_current_account['roles']['role_id'];
$account_id = $user_current_account['accounts']['account_id'];

$collab_permission = $user_permissions['UserAccount']['manage_collab_huddles'];
$coach_permission = $user_permissions['UserAccount']['manage_coach_huddles'];
$eval_permission = $user_permissions['UserAccount']['manage_evaluation_huddles'];
$section_under_overview = 1;
$dropdown_permission = 1;

//if ($collab_permission == 0 && $coach_permission == 0 && $eval_permission == 0 && $this->Session->read('role') == '120') {
//    $section_under_overview = 0;
//    $dropdown_permission = 0;
//}
//
//if ($collab_permission == 1 && $coach_permission == 0 && $eval_permission == 0 && $this->Session->read('role') == '120') {
//    $dropdown_permission = 0;
//}
//
//if ($collab_permission == 0 && $coach_permission == 1 && $eval_permission == 0 && $this->Session->read('role') == '120') {
//    $dropdown_permission = 0;
//}
//
//if ($collab_permission == 0 && $coach_permission == 0 && $eval_permission == 1 && $this->Session->read('role') == '120') {
//    $dropdown_permission = 0;
//}

$month_arr = array(
    '01' => 'Jan',
    '02' => 'Feb',
    '03' => 'Mar',
    '04' => 'Apr',
    '05' => 'May',
    '06' => 'Jun',
    '07' => 'Jul',
    '08' => 'Aug',
    '09' => 'Sep',
    '10' => 'Oct',
    '11' => 'Nov',
    '12' => 'Dec'
);
$frequency = array(
    '1' => '3 Months',
    '2' => '6 Months',
    '3' => '9 Months',
    '4' => '12 Months',
);

$avatar_path = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $user['User']['id'] . "/" . $user['User']['image'], $user['User']['image']);
$created_date = date_create($user['User']['created_date']);
?>
<style>
    .flter_rightcls input[type=text]{    position: absolute;
                                         left: 33px;
                                         top: -1px;
                                         background: #fff;
                                         border: 1px solid #ddd;
                                         border-radius: 0;
                                         width:  442px !important;}
    .ui-autocomplete {    top: 675px !important;}
    .arrow-up {
        width: 13px;
        height: 9px;
        background: url(/app/img/dbimages/arrow_up.png) no-repeat;
        float:right;
        margin-top: 8px;
        margin-right: 5px;
    }
    .arrow-down {
        width: 13px;
        height: 9px;
        background: url(/app/img/dbimages/arrow_down.png) no-repeat;
        float:right;
        margin-top: 8px;
        margin-right: 5px;
    }
    .toggle_arrow{font-size: 24px;font-weight: 600;   cursor: pointer;    border-bottom: solid 1px #3c3c3c; padding-bottom: 7px;}
    .toggle_arrow span{    position: relative;  left: 1px;top: 7px;}
    #coach_views{
        width: 179px;
        height: 38px;
        border-radius: 0px;
        padding: 5px 9px 6px 4px;
        border: 1px solid #ddd;
        background: #fff;
    }
    .over_select1{
        position: absolute;
        top: 25px;
        right: 198px;
    }
</style>
<script>
    function formatValue(value, formattedValue, valueAxis) {
//            console.log(valueAxis);
        var assessment_array = new Array();
<?php foreach ($assessment_array as $key => $val) { ?>
            assessment_array.push("<?php echo $val; ?>");
<?php } ?>
        if (value == 0) {
            return typeof (assessment_array[0]) != 'undefined' ? assessment_array[0] : "No Assesment";
        }
        else if (value == 1) {
            return typeof (assessment_array[1]) != 'undefined' ? assessment_array[1] : "";
        }
        else if (value == 2) {
            return typeof (assessment_array[2]) != 'undefined' ? assessment_array[2] : "";
        }
        else if (value == 3) {
            return typeof (assessment_array[3]) != 'undefined' ? assessment_array[3] : "";
        }
        else if (value == 4) {
            return typeof (assessment_array[4]) != 'undefined' ? assessment_array[4] : "";
        }
        else if (value == 5) {
            return typeof (assessment_array[5]) != 'undefined' ? assessment_array[5] : "";
        } else {
            return '';
        }
    }

</script>
<div style="display:none;" class = "ana_spinner">
    <img src="<?php echo $this->webroot . 'img/loading.gif' ?>">
</div>
<div class="container analytics_new">
    <div class="analytics_top">
        <div class="analytic_img_cls">
            <?php if (isset($user['User']['image']) && $user['User']['image'] != ''): ?>
                <?php echo $this->Html->image($avatar_path, array('alt' => $user['User']['first_name'] . ' ' . $user['User']['last_name'], 'class' => 'photo ph', 'height' => '96', 'rel' => 'tooltip', 'width' => '96')); ?>
            <?php else: ?>
                <img width="96" height="96" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo" alt="<?php echo $user['User']['first_name'] . ' ' . $user['User']['last_name']; ?>" style="float: left !important;">
            <?php endif; ?>

        </div>
        <div class="analytic_detail_cls">
            <h3><?php echo $user['User']['first_name'] . ' ' . $user['User']['last_name'] ?></h3>
            <h4><?php echo $user['User']['email']; ?></h4>
            <p><label>User Role:</label> <span><?php echo $user['roles']['name']; ?></span>      <label>User Type:</label> <span><?php echo $user_type; ?></span>  <label>Last Login:</label> <span><?php echo date_format($last_login, 'F d, Y'); ?></span></p>
        </div>
        <div class="analytic_topnav">
            <?php if ($this->Session->read('role') == '100' || $this->Session->read('role') == '110'): ?>
                <a href="<?php echo $this->base . '/Dashboard/home' ?>">Dashboard</a> <img src="<?php echo $this->webroot . 'img/analytic_arrow.png' ?>" /> <a href="<?php echo $this->base . '/analytics' ?>">Analytics</a> <img src="<?php echo $this->webroot . 'img/analytic_arrow.png' ?>"> <?php echo $user['User']['first_name'] . ' ' . $user['User']['last_name']; ?>
            <?php endif; ?>
            <?php if ($section_under_overview == 1): ?>
                <div onclick="exportCharts();" class="down_pdcls"> <img src="<?php echo $this->webroot . 'img/downloadicon.png' ?>" /> Export charts to PDF</div>
            <?php endif; ?>
        </div>
        <div class="clear"></div>
    </div>

    <!--Account Overviews Section-->
    <div id="account_overview"><?php echo $account_overview; ?></div>


    <!--Account Overviews Section-->

    <?php if ($section_under_overview == 1): ?>

        <div class="analytic_overview analytic_overview2">
            <?php if ($folder_type == 1): ?>
                <h3 id='total_huddles_box_title'><?php echo $total_huddles; ?> Collaboration Huddle Video Sessions</h3>
            <?php elseif ($folder_type == 2): ?>
                <h3 id='total_huddles_box_title'><?php echo $total_huddles; ?> Coaching Huddle Video Sessions</h3>
            <?php elseif ($folder_type == 3): ?>
                <h3 id='total_huddles_box_title'><?php echo $total_huddles; ?> Assessment Huddle Video Sessions</h3>
            <?php endif; ?>

            <div class="over_datecls">
                <p><span class="filter_date"></span></p>
            </div>
            <?php if (count($view_dropdown) > 0): ?>
                <?php // var_dump($view_dropdown);   ?>
                <div id="coache_view-box" class="over_select1" style="display:<?php echo ($folder_type == 2) ? 'block;' : 'none;' ?> ">
                    <select name="coach_views" id="coach_views" onchange="filters();">
                        <?php if (isset($view_dropdown[1]) && $view_dropdown[1] != ''): ?>
                            <option <?php echo isset($folder_type) && $folder_type == '2' ? 'selected="selected"' : '' ?> value="1"><?php echo $view_dropdown[1]; ?></option>
                        <?php endif; ?>
                        <?php if (isset($view_dropdown[2]) && $view_dropdown[2] != ''): ?>
                            <option <?php echo isset($folder_type) && $folder_type == '1' ? 'selected="selected"' : '' ?> value="2" ><?php echo $view_dropdown[2]; ?></option>
                        <?php endif; ?>
                    </select>
                </div>
            <?php endif; ?>
            <?php if ($dropdown_permission == 1 && count($filter_drodown) > 0): ?>
                <div class="over_select">
                    <select name="folder_type" id="folder_type" onchange="filters();">
                        <?php if (isset($filter_drodown[1]) && $filter_drodown[1] != ''): ?>
                            <?php if ($collab_permission == 1 || $this->Session->read('role') == '110' || $this->Session->read('role') == '100' || $this->Session->read('role') == '120'): ?> <option <?php echo isset($folder_type) && $folder_type == '1' ? 'selected="selected"' : '' ?> value="1">Collaboration Huddles</option> <?php endif; ?>
                        <?php endif; ?>
                        <?php if (isset($filter_drodown[2]) && $filter_drodown[2] != ''): ?>
                            <?php if ($coach_permission == 1 || $this->Session->read('role') == '110' || $this->Session->read('role') == '100' || $this->Session->read('role') == '120'): ?><option <?php echo isset($folder_type) && $folder_type == '2' ? 'selected="selected"' : '' ?> value="2" >Coaching Huddles</option><?php endif; ?>
                        <?php endif; ?>
                        <?php if ($this->Custom->check_if_eval_huddle_active($account_id)): ?>
                            <?php if (isset($filter_drodown[3]) && $filter_drodown[3] != ''): ?>
                                <?php if ($eval_permission == 1 || $this->Session->read('role') == '110' || $this->Session->read('role') == '100' || $this->Session->read('role') == '120'): ?><option <?php echo isset($folder_type) && $folder_type == '3' ? 'selected="selected"' : '' ?> value="3">Assessment Huddles</option><?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </select>
                </div>
            <?php endif; ?>
        </div>
        <?php if ($this->Custom->is_enabled_framework_and_standards($current_logged_in_account_id)) { ?>
            <div class="framework_filter">
                <div class="flter_leftcls">
                    <?php if (!empty($frameworks) && count($frameworks) > 0): ?>
                        <label>Framework:</label>
                        <select name="frameworks" id="frameworks">
                            <?php foreach ($frameworks as $framework) { ?>
                                <option value="<?php echo $framework['AccountTag']['account_tag_id'] ?>" <?php if (isset($framework_id) && $framework['AccountTag']['account_tag_id'] == $framework_id) echo ' selected="selected"'; ?>><?php echo $framework['AccountTag']['tag_title'] ?></option>
                            <?php }
                            ?>
                        </select>
                    <?php endif; ?>
                </div>
                <div class="flter_rightcls" style="float: right;">
                    <label style="height:6px;float: left;margin-right:15px;"><img src="<?php echo $this->webroot . 'img/analytics_filtericon.png' ?>" /></label>
                    <div style="width: 398px; float:left;     padding-top: 36px;">
                        <input type="text" class="form-control token_field_outer" id="tokenfield" name="search_by_standards" placeholder="Filter Standards" value style="width: 100%;" />
                    </div>
                    <button style="float: right;" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button" class="btn btn-green" onclick="filters();">Filter</button>
                </div>
                <div class="clear"></div>
            </div>
        <?php } ?>
        <div id="frequency_of_tagged_standars_chart"><?php echo $frequency_tagged_standards['frequency_of_tagged_standars_chart']; ?></div>
        <div id="custom_markers_chart"><?php echo $frequency_tagged_standards['custom_markers_chart']; ?></div>
        <?php
        if (!empty($frequency_tagged_standards['serial_charts'])):
            ?>
            <div style="padding: 12px; font-size: 15px; clear: both;">
                <?php if ($this->Custom->coaching_perfomance_level_without_role($current_logged_in_account_id) == '1'): ?>
                    <h4  class="toggle_arrow" id="serial_charts_title" >
                        Performance Level
                        <span  class=" arrow-down arrow-up"></span>
                    </h4>
                <?php else: ?>
                    <h4  class="toggle_arrow" id="serial_charts_title" >
                        Frequency of Tagged Standards
                        <span  class=" arrow-down arrow-up"></span>
                    </h4>
                <?php endif; ?>
                <div style="float:right;margin-top: 9px;font-size: 14px;font-weight: 800; <?php
                if ($this->Custom->coaching_perfomance_level_without_role($current_logged_in_account_id) == '1') {
                    echo "display: block;";
                } else {
                    echo "display: none;";
                }
                ?> color: #000;" id="ratting-title" > <?php //echo implode('&nbsp;&nbsp;&nbsp;&nbsp;', $ratings);     ?></div>
                <div style="clear: both;"></div>
            </div>
        <?php endif; ?>
        <div id="serial_charts">
            <?php
            if ($frequency_tagged_standards['serial_charts']) {
                foreach ($frequency_tagged_standards['serial_charts'] as $row) {
                    echo $row;
                }
            }
            ?>
        </div>
    <?php endif; ?>

</div>

<script>

    $(document).ready(function (e) {
        $('.toggle_arrow').click(function () {
            $(this).find('span').toggleClass('arrow-down');
            $('#serial_charts').slideToggle();

        });
        setTimeout(function () {
            $('.form-control').removeClass('tokenfield');
        }, 3000);
        $('#tokenfield').tokenfield({
            autocomplete: {
                //source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
                source: <?php echo $standards; ?>,
                delay: 100,
                minWidth: 10,
                createTokensOnBlur: true,
            },
            showAutocompleteOnFocus: true
        });
        $('#tokenfield').on('tokenfield_1:createdtoken', function (e) {
            $("#tokenfield_1-tokenfield_1").blur();
        }).tokenfield();
        $('#tokenfield').on('tokenfield:createtoken', function (event) {
            var existingTokens = $(this).tokenfield('getTokens');
            $.each(existingTokens, function (index, token) {
                if (token.value === event.attrs.value)
                    event.preventDefault();
            });
            setTimeout(function () {
                $('#tokenfield-tokenfield').blur();
                $('#tokenfield-tokenfield').focusout();
            }, 0)
        });

        $('.switch_filter1').click(function (e) {
            $('.analytic_custom1').hide();
            $('.analytic_custom2').show();
            $('#filter_type').val('custom');
        });
        $('.switch_filter2').click(function (e) {
            $('.analytic_custom2').hide();
            $('.analytic_custom1').show();
            $('#filter_type').val('default');
        });
        $(function () {
            $(".dateTimePicker").datetimepicker({
                format: 'M d, Y  H:i:s', maxDate: 0
            });
        });
        $(document).on('change', '#frameworks', function (e) {

            $.ajax({
                type: 'POST',
                url: home_url + '/analytics/getFramework/' + '<?php echo $current_logged_in_account_id ?>',
                dataType: 'json',
                data: {framework_id: $(this).val()},
                success: function (res) {
                    //console.log(res);
                    $('#tokenfield').data('bs.tokenfield').$input.autocomplete({source: res})
                    $('#tokenfield').tokenfield('setTokens', []);
                    $('#tokenfield').data('bs.tokenfield').$input.autocomplete({source: res})
                    $('#tokenfield').val('');
                    filters();
                }
            });
        });
    });


    function filters() {
        ajaxMasterAccountGraph();
    }

    function ajaxMasterAccountGraph() {
        $('.ana_spinner').show();
        if ($('#frameworks').val() > 0) {
            var framework = $('#frameworks').val();
        }

        var coaching_performance_level = '<?php echo $this->Custom->coaching_perfomance_level_without_role($current_logged_in_account_id); ?>';
        var folder_type;
        if ($('#folder_type').val() != '') {
            folder_type = $('#folder_type').val();
        }
        if (folder_type == 2) {
            $('#coache_view-box').show();
        } else {
            $('#coache_view-box').hide();
        }
        if ($('#folder_type').val() == 3) {
            $('#ratting-title').show();
            $('#serial_charts_title').html('Performance Level');
        }

        else if ($('#folder_type').val() == 2 && coaching_performance_level == '1')
        {
            $('#ratting-title').show();
            $('#serial_charts_title').html('Performance Level');
        }

        else {
            $('#ratting-title').hide();
            $('#serial_charts_title').html('Frequency of tags');
        }
        var coachee_view = $('#coach_views').val();
        $.ajax({
            type: 'POST',
            url: home_url + '/analytics/ajax_single_filter/<?php echo $current_logged_in_account_id . '/' . $user_id . '/' . $start_date . '/' . $end_date . '/' ?>' + folder_type,
            dataType: 'json',
            data: {
                search_by_standards: $('#tokenfield').val(),
                framework_id: framework,
                folder_type: folder_type,
                coachee_view: coachee_view
            },
            success: function (res) {
                if (typeof (AmCharts.charts.length) != 'undefined' && AmCharts.charts.length > 0) {
                    $total_length = AmCharts.charts.length;
                    for ($i = 0; $i < $total_length; $i++) {
                        AmCharts.charts[$i].chartData = null;
                    }
                    AmCharts.charts.length = 0;
                }
                $('#frequency_of_tagged_standars_chart').html(res.frequency_of_tagged_standars_chart);
                $('#custom_markers_chart').html(res.custom_markers_chart);
                $('#serial_charts').html(res.serial_charts);
                if (folder_type == 1) {
                    $('#total_huddles_box_title').html(res.total_huddles + ' Collaboration Huddle Video Sessions');
                } else if (folder_type == 2) {
                    $('#total_huddles_box_title').html(res.total_huddles + ' Coaching Huddle Video Sessions');
                } else if (folder_type == 3) {
                    $('#total_huddles_box_title').html(res.total_huddles + ' Assessment Huddle Video Sessions');
                }

                $('.ana_spinner').hide();

            }
        });

    }
    /**
     * Function that triggers export of all charts to PDF
     */
    function exportCharts() {
        // iterate through all of the charts and prepare their images for export
        var images = [];
        var pending = AmCharts.charts.length;
        console.log(AmCharts.charts.length);
        for (var i = 0; i < AmCharts.charts.length; i++) {
            var chart = AmCharts.charts[i];
            if (chart != 'undefined') {
                chart.export.capture({}, function () {
                    this.toJPG({}, function (data) {
                        images.push({
                            "image": data,
                            "fit": [523.28, 769.89]
                        });
                        pending--;
                        if (pending === 0) {
                            // all done - construct PDF
                            chart.export.toPDF({
                                content: images
                            }, function (data) {
                                this.download(data, "application/pdf", "amCharts.pdf");
                            });
                        }
                    });
                });
            }

        }
    }
    function doMyFileVideoSearchAjax() {
    }

</script>
