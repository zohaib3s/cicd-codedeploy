<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Sibme</title>
  <base href="/angular_resources/analytics/">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="/assets/img/favicon.ico">
  
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />

  <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
  
</head>
<body>
  <script type="text/javascript">
    
    
    window.header_data = {
  "user_current_account": {
    "User": {
      "id": "621",
      "username": "ksaleem",
      "email": "khurrams@sibme.com",
      "password": "12647933a0432836640e8086baa3f07aca8d4897",
      "reset_password_token": "69d89a804cfd93e9c1b9132f3a7cb7c836f1f737f28bf2669871277378e51b2498f8f116b913ce4d8051b1421d8ed3b3f1640587a289751835c751213fcaa7e5",
      "reset_password_sent_at": null,
      "remember_created_at": null,
      "sign_in_count": "0",
      "current_sign_in_at": null,
      "last_sign_in_at": null,
      "current_sign_in_ip": null,
      "last_sign_in_ip": null,
      "first_name": "Khurram",
      "last_name": "Saleem",
      "title": "",
      "phone": null,
      "time_zone": "American Samoa",
      "image": "1562345454thumb.jpeg",
      "file_size": "11333",
      "authentication_token": "85fc37b18c57097425b52fc7afbb6969",
      "email_notification": true,
      "is_active": true,
      "type": "Active",
      "created_by": "0",
      "created_date": "2013-10-17 02:52:05",
      "last_edit_by": "621",
      "last_edit_date": "2018-02-02 09:36:33",
      "is_deleted": "0",
      "hide_welcome": "1",
      "welcome_email_sent": "0",
      "recent_activity": "0",
      "settings_assistance": "1",
      "application_insight": "1",
      "go_mobile": "0",
      "verification_code": null,
      "survey_check": "1",
      "setting_assistance_edTPA": "1"
    },
    "users_accounts": {
      "id": "633",
      "account_id": "181",
      "user_id": "621",
      "role_id": "100",
      "is_default": "1",
      "permission_maintain_folders": "1",
      "permission_access_video_library": "1",
      "permission_video_library_upload": "1",
      "permission_view_analytics": "0",
      "created_by": "621",
      "created_date": "2013-10-17 02:52:05",
      "last_edit_by": "621",
      "last_edit_date": "2016-11-09 04:27:56",
      "permission_administrator_user_new_role": "1",
      "parmission_access_my_workspace": "1",
      "manage_collab_huddles": "1",
      "manage_evaluation_huddles": true,
      "manage_coach_huddles": "1",
      "folders_check": "1",
      "type_pause": "1",
      "press_enter_to_send": "1",
      "autoscroll_switch": "0",
      "huddle_to_workspace": "1",
      "live_recording": "1"
    },
    "roles": {
      "name": "Account Owner",
      "role_id": "100"
    },
    "accounts": {
      "in_trial": false,
      "is_suspended": false,
      "storage_used": "6526211622",
      "allow_launchpad": "0",
      "account_id": "181",
      "company_name": "3S",
      "image_logo": "",
      "header_background_color": "0d5d85",
      "nav_bg_color": "668dab",
      "created_at": "2016-01-11 11:53:30"
    }
  },
  "user_permissions": {
    "UserAccount": {
      "id": "633",
      "account_id": "181",
      "user_id": "621",
      "role_id": "100",
      "is_default": "1",
      "permission_maintain_folders": "1",
      "permission_access_video_library": "1",
      "permission_video_library_upload": "1",
      "permission_view_analytics": "0",
      "created_by": "621",
      "created_date": "2013-10-17 02:52:05",
      "last_edit_by": "621",
      "last_edit_date": "2016-11-09 04:27:56",
      "permission_administrator_user_new_role": "1",
      "parmission_access_my_workspace": "1",
      "manage_collab_huddles": "1",
      "manage_evaluation_huddles": true,
      "manage_coach_huddles": "1",
      "folders_check": "1",
      "type_pause": "1",
      "press_enter_to_send": "1",
      "autoscroll_switch": "0",
      "huddle_to_workspace": "1",
      "live_recording": "1"
    },
    "accounts": {
      "id": "181",
      "parent_account_id": "0",
      "user_id": "0",
      "company_name": "3S",
      "custom_url": null,
      "image_logo": "",
      "header_background_color": "0d5d85",
      "text_link_color": null,
      "plan_id": "1",
      "plan_qty": null,
      "enable_edtpa": "1",
      "storage": "0",
      "storage_used": "6526211622",
      "card_type": null,
      "last_4digits": null,
      "created_at": "2016-01-11 11:53:30",
      "updated_at": "2018-04-03 13:52:42",
      "subdomain": null,
      "is_active": true,
      "braintree_customer_id": "75401818",
      "braintree_subscription_id": "b2s6sr",
      "nav_bg_color": "668dab",
      "usernav_bg_color": null,
      "in_trial": false,
      "has_credit_card": false,
      "suspended_at": null,
      "is_suspended": false,
      "allow_launchpad": "0",
      "deactive_plan": true,
      "hide_plan_links": false,
      "is_evaluation_activated": true,
      "mailchimp_list_id": "ecb6cba51b",
      "custom_users": "2000",
      "custom_storage": "100",
      "student_fees": "60",
      "enable_live_rec": "1"
    },
    "users": {
      "id": "621",
      "username": "ksaleem",
      "email": "khurrams@sibme.com",
      "password": "12647933a0432836640e8086baa3f07aca8d4897",
      "reset_password_token": "69d89a804cfd93e9c1b9132f3a7cb7c836f1f737f28bf2669871277378e51b2498f8f116b913ce4d8051b1421d8ed3b3f1640587a289751835c751213fcaa7e5",
      "reset_password_sent_at": null,
      "remember_created_at": null,
      "sign_in_count": "0",
      "current_sign_in_at": null,
      "last_sign_in_at": null,
      "current_sign_in_ip": null,
      "last_sign_in_ip": null,
      "first_name": "Khurram",
      "last_name": "Saleem",
      "title": "",
      "phone": null,
      "time_zone": "American Samoa",
      "image": "1562345454thumb.jpeg",
      "file_size": "11333",
      "authentication_token": "85fc37b18c57097425b52fc7afbb6969",
      "email_notification": true,
      "is_active": true,
      "type": "Active",
      "created_by": "0",
      "created_date": "2013-10-17 02:52:05",
      "last_edit_by": "621",
      "last_edit_date": "2018-02-02 09:36:33",
      "is_deleted": "0",
      "hide_welcome": "1",
      "welcome_email_sent": "0",
      "recent_activity": "0",
      "settings_assistance": "1",
      "application_insight": "1",
      "go_mobile": "0",
      "verification_code": null,
      "survey_check": "1",
      "setting_assistance_edTPA": "1"
    },
    "roles": {
      "role_id": "100",
      "role_type": "1",
      "name": "Account Owner",
      "desc": null
    }
  },
  "logo_image": "/img/new/logo-3.png",
  "header_background_color": "#0d5d85",
  "nav_bg_color": "#668dab",
  "logo_background_color": "#fff",
  "is_change_account_enable": 1,
  "base_url": "http://www.sibme.test",
  "expiry_date": "02/10/2016",
  "status": true,
  "is_impersonate": 0,
  "avatar_path": "https://s3.amazonaws.com/sibme.com/static/users/621/1562345454thumb.jpeg",
  "library_permission": 1
};
  </script>
  <app-root></app-root>
<script type="text/javascript" src="inline.bundle.js"></script><script type="text/javascript" src="polyfills.bundle.js"></script><script type="text/javascript" src="styles.bundle.js"></script><script type="text/javascript" src="vendor.bundle.js"></script><script type="text/javascript" src="main.bundle.js"></script></body>
</html>
