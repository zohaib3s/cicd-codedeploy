<?php $breadcrumb_language_based_content = $this->Custom->get_page_lang_based_content('BreadCrumb'); ?>
<?php
$filters_data = $this->Session->read('filters_data');

$user = $this->Session->read('Auth');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$user_id = $user_current_account['User']['id'];
$permission_maintain_folders = $user_current_account['users_accounts']['permission_maintain_folders'];
$user_role_id = $user_current_account['roles']['role_id'];
$account_id = $user_current_account['accounts']['account_id'];
$month_arr = array(
    '01' => 'Jan',
    '02' => 'Feb',
    '03' => 'Mar',
    '04' => 'Apr',
    '05' => 'May',
    '06' => 'Jun',
    '07' => 'Jul',
    '08' => 'Aug',
    '09' => 'Sep',
    '10' => 'Oct',
    '11' => 'Nov',
    '12' => 'Dec'
);
$frequency = array(
    '1' => '3 Months',
    '2' => '6 Months',
    '3' => '9 Months',
    '4' => '12 Months',
);
$standers_title = '';

if ($filters_data['search_by_standards'] != '') {
    $standards_1 = json_decode($standards);
    $selected = explode(',', $filters_data['search_by_standards']);

    foreach ($standards_1 as $row) {
        if (isset($selected) && in_array($row->value, $selected)) {
            $standers_title[] = $row->label;
        }
    }
}


if (isset($filters_data) && count($filters_data) > 0) {
    if ($filters_data['filter_type'] == 'custom') {
        $start_month = '';
        $month = '';
        $selected_year = '';
        $selected_frequency = '';
        $selected_account = $filters_data['subAccount'];
        $folder_type = $filters_data['folder_type'];
        $filter_type = $filters_data['filter_type'];
        $start_date = $filters_data['start_date'];
        $end_date = $filters_data['end_date'];
    } elseif ($filters_data['filter_type'] == 'default') {
        $start_month = explode('-', $filters_data['startDate']);
        $month = $start_month[1];
        $selected_year = $start_month[0];
        $selected_frequency = $filters_data['duration'];
        $selected_account = $filters_data['subAccount'];
        $filter_type = $filters_data['filter_type'];
        $folder_type = $filters_data['folder_type'];
        $start_date = $filters_data['start_date'];
        $end_date = $filters_data['end_date'];
    }
} else {
    $selected_account = '';
}
?>
<style type="text/css">
    .flter_rightcls input[type=text]{    position: absolute;
                                         left: 33px;
                                         top: -1px;
                                         background: #fff;
                                         border: 1px solid #ddd;
                                         border-radius: 0;
                                         width: 442px !important;}
    .ui-autocomplete {    top: 689px !important;}
    .arrow-up {
        width: 13px;
        height: 9px;
        background: url(/app/img/dbimages/arrow_up.png) no-repeat;
        float:right;
        margin-top: 8px;
        margin-right: 5px;
    }
    .arrow-down {
        width: 13px;
        height: 9px;
        background: url(/app/img/dbimages/arrow_down.png) no-repeat;
        float:right;
        margin-top: 8px;
        margin-right: 5px;
    }
    .toggle_arrow{font-size: 24px;font-weight: 600;   cursor: pointer;    border-bottom: solid 1px #3c3c3c; padding-bottom: 7px;}
    .toggle_arrow span{    position: relative;  left: 1px;top: 7px;}
    .flter_rightcls .close{right: 0px !important;top: 0px!important;}
    #account_name_info{color: #339357;}
</style>
<script type="text/javascript">
    function formatValue(value, formattedValue, valueAxis) {
//            console.log(valueAxis);
        var assessment_array = new Array();
<?php foreach ($assessment_array as $key => $val) { ?>
            assessment_array.push("<?php echo $val; ?>");
<?php } ?>
        if (value == 0) {
            return typeof (assessment_array[0]) != 'undefined' ? assessment_array[0] : "No Assesment";
        }
        else if (value == 1) {
            return typeof (assessment_array[1]) != 'undefined' ? assessment_array[1] : "";
        }
        else if (value == 2) {
            return typeof (assessment_array[2]) != 'undefined' ? assessment_array[2] : "";
        }
        else if (value == 3) {
            return typeof (assessment_array[3]) != 'undefined' ? assessment_array[3] : "";
        }
        else if (value == 4) {
            return typeof (assessment_array[4]) != 'undefined' ? assessment_array[4] : "";
        }
        else if (value == 5) {
            return typeof (assessment_array[5]) != 'undefined' ? assessment_array[5] : "";
        } else {
            return '';
        }
    }

</script>
<div style="display:none;" class = "ana_spinner">
    <img src="<?php echo $this->webroot . 'img/loading.gif' ?>">
</div>
<div class="container analytics_new">
    <div class="analytics_top">

        <div class="analytic_filter" >
            <div class="header header-huddle" style="    margin: -8px -25px 20px -28px;">
                <h2 id="account-analytics-title">Account Analytics</h2>
                <a href="<?php echo $this->base . '/Dashboard/home'; ?>" class="back_to_das"><img src="<?php echo $this->webroot . 'img/arrow_back.png' ?>" /></a>
                <div style="cursor: pointer;" onclick="exportCharts();" class="pdf_exp"><img src="<?php echo $this->webroot . 'img/pdf_arrow_icon.png' ?>" /></div>
            </div>
            <div class="clear"></div>
            <div style = "display:none;" class="analytic_topnav">
                <a href="<?php echo $this->base . '/Dashboard/home'; ?>"><img src="<?php echo $this->webroot . 'img/analytic_arrow2.png' ?>" /> Back to Dashboard</a>
                <br>
                <div onclick="exportCharts();" class="down_pdcls"> <img src="<?php echo $this->webroot . 'img/downloadicon.png' ?>" /> Export charts to PDF</div>
            </div>
            <div class="clear"></div>
            <form action="<?php echo $this->base . '/analytics/index' ?>" method="post" class='analytic_custom1' style="display: <?php echo $filter_type == 'default' ? 'block' : 'none;' ?>" >
                <div class="analytic_custom11">

                    <label>Start month/year:</label>
                    <select id="monthFrom" name="month" style="width: 75px;">
                        <?php foreach ($month_arr as $key => $val): ?>
                            <option <?php echo $month == $key ? 'selected="selected"' : ""; ?> value="<?php echo $key ?>"><?php echo $val; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <select id="yearFrom" name="year" style="width: 65px;">
                        <?php
                        $curr_year = date('Y');
                        for ($y = 2012; $y <= $curr_year; $y++) {
                            $year = "<option value=$y";
                            if ($selected_year == $y)
                                $year .= " selected=selected";
                            $year .=">$y</option>";
                            echo $year;
                        }
                        ?>
                    </select>
                    <label>Frequency:</label>
                    <select id="quarterFrom" name="frequency">
                        <?php foreach ($frequency as $key => $val): ?>
                            <option <?php echo ($selected_frequency == $key) ? 'selected="selected"' : '' ?> value="<?php echo $key ?>"> <?php echo $val ?></option>
                        <?php endforeach; ?>
                    </select>
                    <label>Select Account:</label>
                    <select class="subAccountGrph_class filter-frameworks" id="subAccountGrph" name="account_id" style="width:156px">
                        <option option_company_name = "Overview" value="">All Accounts</option>
                        <option option_company_name = "<?php echo $user_current_account['accounts']['company_name'] . ': Overview'; ?>" <?php echo $selected_account == $user_current_account['accounts']['account_id'] ? 'selected="selected"' : '' ?>  value="<?php echo $user_current_account['accounts']['account_id']; ?>"><?php echo $user_current_account['accounts']['company_name']; ?>
                        </option>
                        <?php if (is_array($account_sub_users) && count($account_sub_users) > 0): ?>
                            <?php foreach ($account_sub_users as $sub_user): ?>
                                <option <?php echo $selected_account == $sub_user['a']['id'] ? 'selected="selected"' : '' ?> option_company_name = "<?php echo $sub_user['a']['company_name'] . ': Overview'; ?>" value="<?php echo $sub_user['a']['id']; ?>"><?php echo $sub_user['a']['company_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <button class="btn btn-green" onclick="top_filter('1');
                            return false;" />Update</button>

                    <div class="analytic_custombtn switch_filter1">
                        <img src="<?php echo $this->webroot . 'img/custom_switch_icon.png' ?>" /> Custom
                    </div>
                    <div class="clear"></div>
                </div>
            </form>
            <form action="<?php echo $this->base . '/analytics/index' ?>" method="post" class='analytic_custom2' style="display: <?php echo $filter_type == 'custom' ? 'block' : 'none;' ?>">
                <div class="analytic_custom12">
                    <label>Date:</label>
                    <span class="calander_date_st"><input type="text"  value="<?php echo $start_date; ?>" name="start_date" id="start_date" placeholder="Start" style="width: 150px;"/></span>
                    <span class="calander_date_st"><input type="text"  value="<?php echo $end_date; ?>" name="end_date" id="end_date" placeholder="End" style="width: 150px;" /></span>
                    <label>Select Account:</label>
                    <select class="subAccountGrph_class_1 filter-frameworks" id="subAccountGrph_custom" name="account_id" style="width:156px">
                        <?php if (is_array($account_sub_users) && count($account_sub_users) > 0): ?>
                            <option option_company_name = "Overview" value="" >All Accounts</option>start_date
                        <?php endif; ?>
                        <option <?php echo $selected_account == $user_current_account['accounts']['account_id'] ? 'selected="selected"' : '' ?> option_company_name = "<?php echo $user_current_account['accounts']['company_name'] . ': Overview'; ?>" value="<?php echo $user_current_account['accounts']['account_id']; ?>"><?php echo $user_current_account['accounts']['company_name']; ?>
                        </option>
                        <?php if (is_array($account_sub_users) && count($account_sub_users) > 0): ?>
                            <?php foreach ($account_sub_users as $sub_user): ?>
                                <option <?php echo $selected_account == $sub_user['a']['id'] ? 'selected="selected"' : '' ?> option_company_name = "<?php echo $sub_user['a']['company_name'] . ': Overview'; ?>"  value="<?php echo $sub_user['a']['id']; ?>"><?php echo $sub_user['a']['company_name']; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <button class="btn btn-green" onclick="top_filter('2');
                            return false" />Update</button>
                    <!--<input class="btn btn-green" onclick="filters()" type="submit" value="Update" />-->
                    <input id="filter_type" name="filter_type" value="<?php echo isset($filter_type) && $filter_type != '' ? $filter_type : 'default' ?>" type="hidden" />

                    <div class="analytic_custombtn switch_filter2">
                        <img src="<?php echo $this->webroot . 'img/custom_switch_icon.png' ?>" /> Default
                    </div>
                    <div class="clear"></div>
                </div>
            </form>

        </div>



        <div class="clear"></div>
    </div>

    <!--Account Overviews Section-->
    <div id="account_overview"><?php echo $account_overview; ?></div>
    <!--Account Overviews Section-->
    <div class="analytic_overview analytic_overview2">
        <h3 id="waring-message" style="display: none;">
            <div style="font-size: 21px;">Huddle Video Sessions</div>
            <div style=" text-align: center; font-size: 12px; margin-top: 30px; font-style: italic;padding-bottom: 19px;">Please select an account at the top of this page to see huddle analytics for that specific account.</div>
        </h3>
        <h3 id='total_huddles_box_title'><?php echo trim($user_current_account['accounts']['company_name']); ?>: <?php echo $total_huddles; ?> Coaching Huddle Video Sessions</h3>
        <div class="over_datecls over_view_date">
            <p><span class="filter_date"></span></p>
        </div>
        <div class="over_select">
            <select name="folder_type" id="folder_type" onchange="filters()">
                <option <?php echo isset($folder_type) && $folder_type == '1' ? 'selected="selected"' : '' ?> value="1">Collaboration Huddles</option>
                <option <?php echo isset($folder_type) && $folder_type == '2' ? 'selected="selected"' : '' ?> value="2" >Coaching Huddles</option>
                <?php if ($this->Custom->check_if_eval_huddle_active($account_id)): ?>
                    <option <?php echo isset($folder_type) && $folder_type == '3' ? 'selected="selected"' : '' ?> value="3">Assessment Huddles</option>
                <?php endif; ?>
            </select>
        </div>
    </div>


    <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)) { ?>
        <div class="framework_filter">
            <div class="flter_leftcls">
                <?php if (!empty($frameworks) && count($frameworks) > 0): ?>
                    <label>Framework:</label>
                    <select name="frameworks" id="frameworks" style="width:315px">
                        <?php foreach ($frameworks as $framework) { ?>
                            <option value="<?php echo $framework['AccountTag']['account_tag_id'] ?>" <?php if (isset($framework_id) && $framework['AccountTag']['account_tag_id'] == $framework_id) echo ' selected="selected"'; ?>><?php echo $framework['AccountTag']['tag_title'] ?></option>
                        <?php }
                        ?>
                    </select>
                <?php else: ?>
                    <select name="frameworks" id="frameworks" style="width:315px; display: none;"></select>
                    <span class="no-framework-msg" style="width:250px;float: left;">
                        No Framework Available
                    </span>
                <?php endif; ?>
            </div>
            <div class="no-framework-msg" style="display: none;width:250px;float: left;"> No Framework Available</div>
            <?php
            $standers_title = implode(',', $standers_title);
            ?>
            <div class="flter_rightcls" style="float: right;">
                <label style="height:6px;float: left;margin-right:15px;"><img src="<?php echo $this->webroot . 'img/analytics_filtericon.png' ?>" /></label>
                <div style="width: 398px; float:left;     padding-top: 36px;">
                    <input type="text" class="form-control token_field_outer" id="tokenfield" name="search_by_standards" placeholder="Filter Standards" value="" style="width: 100%;" />
                </div>
                <button style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>; float: right;" type="button" class="btn btn-green" onclick="filters()

                                    ;">Filter</button>
            </div>
            <div class="clear"></div>
        </div>
    <?php } ?>





    <div id="frequency_of_tagged_standars_chart"><?php //echo $frequency_tagged_standards['frequency_of_tagged_standars_chart'];    ?></div>
    <div id="custom_markers_chart"><?php //echo $frequency_tagged_standards['custom_markers_chart'];    ?></div>
    <div style="clear: both;"></div>
    <?php if (!empty($frequency_tagged_standards['serial_charts'])): ?>
        <div style="padding: 12px; font-size: 15px; clear: both;" class="performance-level-box">
            <?php if ($this->Custom->coaching_perfomance_level_without_role($account_id) == '1'): ?>
                <h4 class="toggle_arrow" id="serial_charts_title" status="opened"><?php echo trim($user_current_account['accounts']['company_name']); ?>: Performance Level
                    <span  class=" arrow-down arrow-up"></span>
                </h4>
            <?php else: ?>
                <h4 class="toggle_arrow" id="serial_charts_title" status="opened"><?php echo trim($user_current_account['accounts']['company_name']); ?>: Frequency of Tagged Standards
                    <span  class=" arrow-down arrow-up"></span>
                </h4>
            <?php endif; ?>


            <div style="float:right;margin-top: 11px;font-size: 14px;font-weight: 800; <?php
            if ($this->Custom->coaching_perfomance_level_without_role($account_id) == '1') {
                echo "display: block;";
            } else {
                echo "display: none;";
            }
            ?>color: #000;" id="ratting-title" > <?php //echo implode('&nbsp;&nbsp;&nbsp;&nbsp;', $ratings);      ?></div>
            <div style="clear: both;"></div>
        </div>
        <div style="clear: both;"></div>
    <?php endif; ?>
    <div id="serial_charts">
        <?php
        if ($frequency_tagged_standards['serial_charts']) {
            foreach ($frequency_tagged_standards['serial_charts'] as $row) {
                echo $row;
            }
        }
        ?>
    </div>
    <div class="analytic_overview analytic_overview2" style="border:0;">
        <h3 id='total_huddles_box_title' class="user-summary-text" ><?php echo $user_current_account['accounts']['company_name']; ?>: User Summary</h3>
        <div class="over_datecls">
            <p><span class="filter_date"></span></p>
        </div>
    </div>
    <div id="accUsersAnalytics" class="data_grid_green data_grid_adjs">
        <?php echo $usersHtml; ?>
    </div>


</div>
<input type="hidden" name="start_date" value="" id="f_start_date" />
<input type="hidden" name="end_dates" value="" id="f_end_dates" />
<script type="text/javascript">
    $(document).ready(function (e) {
        var is_filterd = '<?php echo count($filters_data) > 0 ? json_encode($filters_data) : ''; ?>'
        if (is_filterd != '') {
            if ($('.subAccountGrph_class').val() == '') {
                $('#total_huddles_box_title').hide();
                $('.over_view_date').hide();
                $('.over_select').hide();
                $('.framework_filter').hide();
                $('#frequency_of_tagged_standars_chart').hide();
                $('#custom_markers_chart').hide();
                $('.performance-level-box').hide();
                $('#serial_charts').hide();
                $('#waring-message').show();
            }
            top_filter('1');
        } else {
            if ($('.subAccountGrph_class').val() == '') {
                $('#total_huddles_box_title').hide();
                $('.over_view_date').hide();
                $('.over_select').hide();
                $('.framework_filter').hide();
                $('#frequency_of_tagged_standars_chart').hide();
                $('#custom_markers_chart').hide();
                $('.performance-level-box').hide();
                $('#serial_charts').hide();
                $('#waring-message').show();
            }
            top_filter('1');
        }

        $('.filter-frameworks').on('change', function (e) {

            var account_id;
            if ($('#filter_type').val() == 'custom') {
                account_id = $('#subAccountGrph_custom').val();
            } else {
                account_id = $('#subAccountGrph').val();
            }
            if (account_id == '') {
                $('#total_huddles_box_title').hide();
                $('.over_view_date').hide();
                $('.over_select').hide();
                $('.framework_filter').hide();
                $('#frequency_of_tagged_standars_chart').hide();
                $('#custom_markers_chart').hide();
                $('.performance-level-box').hide();
                $('#serial_charts').hide();
                $('#waring-message').show();
                top_filter('1');
                return false;
            }


            if (account_id == '') {
                $('#frequency_of_tagged_standars_chart').hide();
                $('#custom_markers_chart').hide();
                $('.performance-level-box').hide();
                $('#serial_charts').hide();
                $('#waring-message').show();
                $('.user-summary-text').text('User Summary');
//                $('#company_name_overview').text('Overview');
                if ($('#folder_type').val() == 1) {
                    $('#total_huddles_box_title').html('Collaboration Huddle Video Sessions');
                } else if ($('#folder_type').val() == 2) {
                    $('#total_huddles_box_title').html('Coaching Huddle Video Sessions');
                } else if ($('#folder_type').val() == 3) {
                    $('#total_huddles_box_title').html('Assessment Huddle Video Sessions');
                }
            } else {
                $('#frequency_of_tagged_standars_chart').show();
                $('#custom_markers_chart').show();
                $('.performance-level-box').show();
                $('#serial_charts').show();
                $('.user-summary-text').text('User Summary');
            }
            $.ajax({
                type: 'POST',
                url: home_url + '/analytics/getAccountFramework',
                dataType: 'html',
                data: {
                    account_id: $(this).val()
                },
                success: function (res) {
                    if (res != '') {
                        $('.flter_leftcls').show();
                        $('.no-framework-msg').hide();
                        $('#frameworks').show();
                        $('#frameworks').html(res);
                        $('#frameworks').trigger('change');
                        var company_name = $('.subAccountGrph_class').find('option:selected').attr('option_company_name');
                        company_title = company_name.split(":");
                        $('#company_name_overview').text(company_name);
                        $('#account_name_info').text(company_title[0]);
                        $('#waring-message').hide();
                        $('#total_huddles_box_title').show();
                        $('.over_view_date').show();
                        $('.over_select').show();
                        $('.framework_filter').show();
                        top_filter('1');
                    }
                    else {
                        $('#frameworks').hide();
                        $('.no-framework-msg').show();
                        $('.flter_leftcls').hide();
                        var company_name = $('.subAccountGrph_class').find('option:selected').attr('option_company_name');
                        company_title = company_name.split(":");
                        $('#company_name_overview').text(company_name);
                        $('#account_name_info').text(company_title[0]);
                        $('#waring-message').hide();
                        $('#total_huddles_box_title').show();
                        $('.over_view_date').show();
                        $('.over_select').show();
                        $('.framework_filter').show();
                        top_filter('1');
                    }

                }
            });
        })

        $('.toggle_arrow').click(function () {
            $(this).find('span').toggleClass('arrow-down');
            $('#serial_charts').slideToggle();
        });
        setTimeout(function () {
            $('.form-control').removeClass('tokenfield');
        }, 3000);
        $('#tokenfield').tokenfield({
            autocomplete: {
                //source: ['red', 'blue', 'green', 'yellow', 'violet', 'brown', 'purple', 'black', 'white'],
                source: <?php echo $standards; ?>,
                delay: 100,
                minWidth: 10,
                createTokensOnBlur: true,
            },
            showAutocompleteOnFocus: true
        });
        $('#tokenfield').on('tokenfield_1:createdtoken', function (e) {
            $("#tokenfield_1-tokenfield_1").blur();
        }).tokenfield();
        $('#tokenfield').on('tokenfield:createtoken', function (event) {
            var existingTokens = $(this).tokenfield('getTokens');
            $.each(existingTokens, function (index, token) {
                if (token.value === event.attrs.value)
                    event.preventDefault();
            });
            setTimeout(function () {
                $('#tokenfield-tokenfield').blur();
                $('#tokenfield-tokenfield').focusout();
            }, 0)
        });
        $('.switch_filter1').click(function (e) {
            $('.analytic_custom1').hide();
            $('.analytic_custom2').show();
            $('#filter_type').val('custom');
        });
        $('.switch_filter2').click(function (e) {
            $('.analytic_custom2').hide();
            $('.analytic_custom1').show();
            $('#filter_type').val('default');
        });
        $(function () {
            $(".dateTimePicker").datetimepicker({
                format: 'M d, Y  H:i:s',
                maxDate: 0
            });
        });
        $(document).on('change', '#frameworks', function (e) {
            $.ajax({
                type: 'POST',
                url: home_url + '/analytics/getFramework',
                dataType: 'json',
                data: {
                    account_id: $('.filter-frameworks').val(),
                    framework_id: $(this).val()
                },
                success: function (res) {
                    $('#tokenfield').tokenfield('setTokens', []);
                    $('#tokenfield').data('bs.tokenfield').$input.autocomplete({
                        source: res
                    })
                    $('#tokenfield').val('');
                }
            });
        });
    });
    function top_filter(filter_number) {
        var filter_type = $('#filter_type').val();
        var filter_location = '1';
        ajaxMasterAccountGraph(filter_type, filter_location, filter_number);
    }

    $('.analytic_custombtn').click(function (e) {
        $.ajax({
            type: 'POST',
            url: home_url + '/analytics/flush_filter',
            dataType: 'json',
            success: function (response) {


            }
        });
    });

    function filters() {
        var coaching_performance_level = '<?php echo $this->Custom->coaching_perfomance_level_without_role($account_id); ?>';
        var filter_type = $('#filter_type').val();
        var filter_location = '2';
        if ($('#folder_type').val() == 3) {
            $('#ratting-title').show();
            $('#serial_charts_title').text('Performance Level');
        } else if ($('#folder_type').val() == 2 && coaching_performance_level == '1') {
            $('#ratting-title').show();
            $('#serial_charts_title').text('Performance Level');
        } else {
            $('#ratting-title').hide();
            $('#serial_charts_title').text('Frequency of tags');
        }
        ajaxMasterAccountGraph(filter_type, filter_location);
    }

    function ajaxMasterAccountGraph(filter_type, filter_location, filter_number) {
        $('.ana_spinner').show();
        var subAccount;

        if (filter_type == 'custom') {
            var start_date = $('#start_date').val();
            var end_date = $('#end_date').val();
            subAccount = $('#subAccountGrph_custom').val();
        } else {
            var durationFrom = $('#yearFrom').val() + '-' + $('#monthFrom').val();
            var frequency = $('#quarterFrom').val();
            subAccount = $('#subAccountGrph').val();
        }
        //        console.log($('select#subAccountGrph option:selected').val());
        if ($('#frameworks').val() > 0) {
            var framework = $('#frameworks').val();
        }
        $.ajax({
            type: 'POST',
            url: home_url + '/analytics/ajax_filter/2',
            dataType: 'json',
            data: {
                subAccount: subAccount,
                duration: $('#quarterFrom').val(),
                startDate: durationFrom,
                search_by_standards: $('#tokenfield').val(),
                framework_id: framework,
                start_date: start_date,
                end_date: end_date,
                bool: filter_location,
                filter_type: filter_type,
                folder_type: $('#folder_type').val()
            },
            success: function (res) {
                if (typeof (AmCharts.charts.length) != 'undefined' && AmCharts.charts.length > 0) {
                    $total_length = AmCharts.charts.length;
                    for ($i = 0; $i < $total_length; $i++) {
                        AmCharts.charts[$i].chartData = null;
                    }
                    AmCharts.charts.length = 0;
                }
                if (subAccount == '') {
                    $('#frequency_of_tagged_standars_chart').hide();
                    $('#custom_markers_chart').hide();
                    $('.performance-level-box').hide();
                    $('#serial_charts').hide();
                    $('#waring-message').show();
                    $('.user-summary-text').text('User Summary');
                    $('#f_start_date').val(res.start_date);
                    $('#f_end_dates').val(res.end_dates);
                    $('#account-analytics-title').html('Account Analytics');

                } else {
                    $('#frequency_of_tagged_standars_chart').show();
                    $('#custom_markers_chart').show();
                    $('.performance-level-box').show();
                    $('#serial_charts').show();
                    $('#f_start_date').val(res.start_date);
                    $('#f_end_dates').val(res.end_dates);
                    var coaching_performance_level = '<?php echo $this->Custom->coaching_perfomance_level_without_role($account_id); ?>';
//                    $('#company_name_overview').text($.trim($('.subAccountGrph_class').find('option:selected').text()) + ':' + ' Overview');
                    var performance_level = '';
                    if ($('#folder_type').val() == 3) {
                        performance_level = 'Performance Level';
                    } else if ($('#folder_type').val() == 2 && coaching_performance_level == '1') {
                        performance_level = 'Performance Level';
                    } else {
                        performance_level = 'Frequency of tags';
                    }

                    if ($('#filter_type').val() == 'custom') {
                        $('#serial_charts_title').html($.trim($('.subAccountGrph_class_1 ').find('option:selected').text()) + ': ' + performance_level);
                        $('.user-summary-text').html($.trim($('.subAccountGrph_class_1').find('option:selected').text()) + ':' + ' User Summary');
                        $('#account-analytics-title').html($.trim($('.subAccountGrph_class_1').find('option:selected').text()) + ': Account Analytics');
                    } else {
                        $('#serial_charts_title').html($.trim($('.subAccountGrph_class').find('option:selected').text()) + ': ' + performance_level);
                        $('.user-summary-text').html($.trim($('.subAccountGrph_class').find('option:selected').text()) + ':' + ' User Summary');
                        $('#account-analytics-title').html($.trim($('.subAccountGrph_class').find('option:selected').text()) + ': Account Analytics');
                    }

                    if ($('#folder_type').val() == 1) {
                        $('#total_huddles_box_title').html($.trim($('.subAccountGrph_class').find('option:selected').text()) + ': ' + res.total_huddles + ' Collaboration Huddle Video Sessions');
                    } else if ($('#folder_type').val() == 2) {
                        $('#total_huddles_box_title').html($.trim($('.subAccountGrph_class').find('option:selected').text()) + ': ' + res.total_huddles + ' Coaching Huddle Video Sessions');
                    } else if ($('#folder_type').val() == 3) {
                        $('#total_huddles_box_title').html($.trim($('.subAccountGrph_class').find('option:selected').text()) + ': ' + res.total_huddles + ' Assessment Huddle Video Sessions');
                    }

                    $('#frequency_of_tagged_standars_chart').html(res.frequency_of_tagged_standars_chart);
                    $('#custom_markers_chart').html(res.custom_markers_chart);
                    $('#serial_charts').html(res.serial_charts);
                }

                $('#accUsersAnalytics').html(res.account_user_analytics);
                if (filter_location == 1) {
                    $('#account_overview').html(res.account_overview.account_overview_elem);
                }
                $('.filter_date').html(res.filter_date);
                $('#f_start_date').val(res.start_date);
                $('#f_end_date').val(res.f_end_dates);

                $('.ana_spinner').hide();
                if (filter_number == 1) {
                    var company_name = $('.subAccountGrph_class').find('option:selected').attr('option_company_name');
                    $('#company_name_overview').text(company_name);

                } else {
                    var company_name = $('.subAccountGrph_class_1').find('option:selected').attr('option_company_name');
                    $('#company_name_overview').text(company_name);
                }

            }
        });
    }



    $("#start_date").datepicker({
        changeMonth: true,
        changeYear: true,
        showOn: "button",
        buttonImage: '<?php echo $this->webroot . "img/icons/calendar_icon.png"; ?>',
        onClose: function (selectedDate) {
            $("#toUsr").datepicker("option", "minDate", selectedDate);
        },
        yearRange: '2012:2026'
    });
    $("#end_date").datepicker({
        changeMonth: true,
        changeYear: true,
        showOn: "button",
        buttonImage: '<?php echo $this->webroot . "img/icons/calendar_icon.png"; ?>',
        onClose: function (selectedDate) {
            $("#toUsr").datepicker("option", "minDate", selectedDate);
        },
        yearRange: '2012:2026'
    });
    /**
     * Function that triggers export of all charts to PDF
     */
    function exportCharts() {         // iterate through all of the charts and prepare their images for export
        var images = [];
        var pending = AmCharts.charts.length;
        for (var i = 0; i < AmCharts.charts.length; i++) {
            var chart = typeof (AmCharts.charts[i]) != 'undefined' ? AmCharts.charts[i] : '';
            if (chart != '') {
                chart.export.capture({}, function () {
                    this.toJPG({}, function (data) {
                        images.push({
                            "image": data,
                            "fit": [523.28, 769.89]
                        });
                        pending--;
                        if (pending === 0) {                             // all done - construct PDF
                            chart.export.toPDF({
                                content: images
                            }, function (data) {
                                this.download(data, "application/pdf", "amCharts.pdf");
                            });
                        }
                    });
                });
            }

        }
    }

    $(document).ready(function (e) {

        var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><span><?php echo $breadcrumb_language_based_content['analytics_breadcrumb']; ?></span></div>';

        $('.breadCrum').html(bread_crumb_data);


    });

</script>