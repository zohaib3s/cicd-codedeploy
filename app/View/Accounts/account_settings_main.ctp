<?php

$trial_storage = 5;
$trial_users = 40;
?>
<?php $breadcrumb_language_based_content = $this->Custom->get_page_lang_based_content('BreadCrumb'); ?>
<?php
$storageUsed = $myAccount['Account']['storage_used'];

$user_current_account = $this->Session->read('user_current_account');

if ($myAccount['Account']['in_trial'] == '1') {
    $totalStorage = 20;
} else {
     if(!empty($myAccount['plans']['category']))
    {
    $totalStorage = $myAccount['plans']['storage'] * $myAccount['Account']['plan_qty'];
    }
    else
    {
    $totalStorage = $myAccount['plans']['storage'];
    }
}
$storageUsed = $storageUsed / 1024;
$storageUsed = $storageUsed / 1024;
$storageUsed = $storageUsed / 1024;
$total_used = $storageUsed;

$storageUsed = ($this->Custom->get_consumed_storage($account_id) * 100) / $this->Custom->get_allowed_storage($account_id);
if($storageUsed > 100)
{
    $storageUsed = 100;
}

$trial_duration = Configure::read('trial_duration');
?>
<style type="text/css">
    .settings_iconsBox .col-md-3 .setting_icon_container{margin: 0 auto;     float: none;}
    .settings_iconsBox .col-md-3 .setting_icon_container p{    margin-top: 15px;}
    .settings_iconsBox .col-md-3  a:hover { text-decoration: none;}
    .storange_cls a, .storange_cls strong { color: #000;}
    .setting_header_left {
    float: left;
    margin-top: -9px;
    margin-bottom: -7px;
}

@media (min-width: 1200px) {
  .container {
    max-width: 1053px;
  }
}

</style>
<div class="">
  <div class="settings_header">
      <?php if ($sample_huddle_not_exists && $site_id != '2'): ?>
    	<div class="setting_header_left">
        	<!-- <a href="<?php //echo $this->base . '/huddles/restore'; ?>" data-confirm="<?php echo $language_based_content['restore_sample_huddles_ques_main']; ?>" data-method="delete" rel="nofollow"><img src="<?php echo $this->webroot.'app/img/restore.png'?>" width="15" height="15" alt=""/> <?php echo $language_based_content['restore_sample_huddle']; ?></a> -->
      </div>
      <?php endif; ?>
      <?php if($myAccount['Account']['deactive_plan'] == 1 && $myAccount['Account']['custom_storage'] == -1 ): ?>
       <p style="margin-left: 860px;"><b>Unlimited Storage</b></p>
      <?php else: ?>
     
        <div class="setting_header_right" style="display: none;">
        	<div class="progress_bar_box">
            	<?php if(!empty($myAccount['plans']['category'])) : ?>
<!--                <div class="total_space">Total <?php //echo (isset($myAccount['plans']['storage']) ? $myAccount['plans']['storage'] * $myAccount['Account']['plan_qty'] : $trial_storage); ?> GB</div>-->
                <div class="total_space">Total <?php echo $this->Custom->get_allowed_storage($account_id); ?> GB</div>
                
                <?php else : ?>
<!--                <div class="total_space">Total <?php //echo (isset($myAccount['plans']['storage']) ? $myAccount['plans']['storage']  : $trial_storage); ?> GB</div>-->
                <div class="total_space">Total <?php echo $this->Custom->get_allowed_storage($account_id); ?> GB</div>
               
                <?php endif; ?>
            	<div class="progress_bar_bg" style="width:<?php echo $storageUsed != '' ? round($storageUsed) . "%" : '0%'; ?>"><?php echo $storageUsed != '' ? round($storageUsed) . "%" : '0%'; ?></div>
            </div>
        </div>
      <?php endif;?>
        <div class="clear"></div>
  </div>
  <div class="settings_body">
  	<div class="package_detail storange_cls">
        <h1><?php echo $company_name;?></h1>
        <p><?php echo $language_based_content['currently_main']; ?> <strong><a href="<?php echo $this->base . "/users/administrators_groups" ?>"><?php echo $user_count  .'/'. $this->Custom->get_allowed_users($account_id) . ($language_based_content['users_main']); ?></a></strong><?php echo $language_based_content['joined_account_main']; ?><strong> <?php echo round($total_used) ?> GB/<?php echo $this->Custom->get_allowed_storage($account_id); ?> <?php echo $language_based_content['gb_storage_main'] ?> </strong> <?php echo $language_based_content['storage_used_main']; ?> <?php if (!$deactive_plan): ?> <?php echo $language_based_content['add_more_users_main']; ?> <a href="<?php echo $this->base . "/accounts/account_settings_all/" . $account_id . "/4" ?>"><?php echo $language_based_content['upgrade_your_account_main']; ?></a>.  <?php endif; ?></p>
    </div>
    <div class="settings_iconsBox">
        <div class="row">
            <div class="col-md-3">
   	 <a href="<?php echo $this->base . "/accounts/account_settings_all/" . $account_id . "/1" ?>"> <div class="setting_icon_container" >
        	<div class="settings_grey_icon">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td height="144" align="center" valign="middle"><img src="<?php echo $this->webroot.'app/img/account_settings.png'?>" width="69" height="66" alt=""/></td>
                </tr>
              </tbody>
            </table>

            </div>
            <p><?php echo $language_based_content['account_settings_main']; ?></p>
      </div> </a>
            </div>
            <div class="col-md-3">
      <a href="<?php echo $this->base . "/accounts/account_settings_all/" . $account_id . "/2" ?>"><div class="setting_icon_container" >
        	<div class="settings_grey_icon">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td height="144" align="center" valign="middle"><img src="<?php echo  $this->webroot.'app/img/color_logo.png'?>" width="70" height="70" alt=""/></td>
                </tr>
              </tbody>
            </table>
            </div>
            <p><?php echo $language_based_content['colors_logo_main']; ?></p>
      </div></a>
            </div>

        <?php if($myAccount['Account']['in_trial'] != '1'): ?>
            <div class="col-md-3">
      <a href="<?php echo $this->base . "/accounts/account_settings_all/" . $account_id . "/3" ?>"><div class="setting_icon_container">
        	<div class="settings_grey_icon">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td height="144" align="center" valign="middle"><img src="<?php echo  $this->webroot.'app/img/archiving.png'?>" width="75" height="75" alt=""/></td>
                </tr>
              </tbody>
            </table>
            </div>
            <p><?php echo $language_based_content['archiving_main']; ?></p>
      </div></a>
            </div>
        <?php endif; ?>
        <?php if( ($deactive_plan && $myAccount['Account']['in_trial'] != '1' && ($this->Custom->is_enabled_framework_and_standards($account_id))) || (($this->Custom->is_enabled_framework_and_standards($account_id)) && $myAccount['Account']['in_trial'] != '1' && !in_array(intval($myAccount['Account']['plan_id']), $emitplans, true))): ?>
            <div class="col-md-3">
            <a href="<?php echo $this->base . "/accounts/account_settings_all/" . $account_id . "/7/1" ?>"><div class="setting_icon_container">
                    <div class="settings_grey_icon">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td height="144" align="center" valign="middle"><img src="<?php echo  $this->webroot.'app/img/rubric.svg'?>" width="75" height="75" alt=""/></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <p><?php echo $language_based_content['rubric_main']; ?></p>
                </div></a>
            </div>
        <?php endif; ?>
        <?php if (!$deactive_plan): ?>
            <div class="col-md-3">
     <a href="<?php echo $this->base . "/accounts/account_settings_all/" . $account_id . "/4" ?>"> <div class="setting_icon_container" >
        	<div class="settings_grey_icon">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td height="144" align="center" valign="middle"><img src="<?php echo  $this->webroot.'app/img/plans.png'?>" width="84" height="65" alt=""/></td>
                </tr>
              </tbody>
            </table>
            </div>
            <p><?php echo $language_based_content['plans_main']; ?></p>
      </div></a>
            </div>
                <div class="col-md-3">
     <a href="<?php echo $this->base . "/accounts/account_settings_all/" . $account_id . "/5" ?>"> <div class="setting_icon_container" >
        	<div class="settings_grey_icon">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td height="144" align="center" valign="middle"><img src="<?php echo  $this->webroot.'app/img/billing.png'?>" width="71" height="74" alt=""/></td>
                </tr>
              </tbody>
            </table>
            </div>
            <p><?php echo $language_based_content['billing_info_main']; ?></p>
      </div></a>
                </div>
            <div class="col-md-3">
      <a href="<?php echo $this->base . "/accounts/account_settings_all/" . $account_id . "/6" ?>"><div class="setting_icon_container" >
        	<div class="settings_grey_icon">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tbody>
                <tr>
                  <td height="144" align="center" valign="middle"><img src="<?php echo  $this->webroot.'app/img/cancel_account.png'?>" width="75" height="77" alt=""/></td>
                </tr>
              </tbody>
            </table>
            </div>
            <p><?php echo $language_based_content['cancel_account_main']; ?></p>
      </div></a>
            </div>
            
        <?php endif; ?>
        <div class="clear"></div>
    </div>

  </div>



  <div class="settings_iconsBox">
    <div class="row">    
    <?php if($myAccount['Account']['in_trial'] != '1'):?>
      <div class="col-md-3">
          <a href="<?php echo $this->base . '/accounts/account_settings_all/' . $account_id . '/8'?>"><div class="setting_icon_container">
              <div class="settings_grey_icon">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tbody>
                      <tr>
                          <td height="144" align="center" valign="middle"><img src="/app/img/rubric.svg" width="75" height="75" alt=""></td>
                      </tr>
                      </tbody>
                  </table>
              </div>
              <p><?php echo $language_based_content['global_export_main_global'];?></p>
          </div></a>
      </div>
    <?php endif;?>

    </div>
  </div>



  
  </div>
</div>

<script>
    $(document).ready(function (e) {
         
                        var bread_crumb_data =  '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><span><?php echo $breadcrumb_language_based_content['main_settings_breadcrumb']; ?></span></div>';

                        $('.breadCrum').html(bread_crumb_data);
         
                        });
</script>
