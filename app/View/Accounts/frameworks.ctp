<?php
    $language_based_framework = $this->Custom->get_page_lang_based_content('accounts/account_settings_all');
?>
<div id="main" class="container box">

    <div class="header">
        <h2 class="header-title"><?=$language_based_framework['Edit_Framework_Names'];?></h2>
    </div>

    <form accept-charset="UTF-8" action="<?php echo $this->base . '/accounts/change_framework_names/' . $account_id ?>" enctype="multipart/form-data" method="POST">
        <?php foreach ($frameworks as $framework) { ?>
            <input type="text" name="names[]"  value="<?php echo $framework['AccountTag']['tag_title'] ?>"><br><br>
            <input type="hidden" name = "ids[]"  value="<?php echo $framework['AccountTag']['account_tag_id'] ?>">

        <?php }
        ?>
        <button class="btn btn-green" type = "submit" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>; border-color:<?php echo $this->Custom->get_site_settings('primary_border_color') ?>"><?=$language_based_framework['save_changes_as_all'];?></button>
        <a href = "<?php echo $this->base . '/' ?>" class="btn btn-transparent"><?=$language_based_framework['cancel_all'];?></a>
    </form>

</div>