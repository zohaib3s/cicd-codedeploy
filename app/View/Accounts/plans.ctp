<?php
$trial_storage = 20;
$trial_users = 40;
?>
<?php
$storageUsed = $myAccount['Account']['storage_used'];

$user_current_account = $this->Session->read('user_current_account');

if ($myAccount['Account']['in_trial'] == '1') {
    $totalStorage = 20;
} else {
    $totalStorage = $myAccount['plans']['storage'];
}
$storageUsed = $storageUsed / 1024;
$storageUsed = $storageUsed / 1024;
$storageUsed = $storageUsed / 1024;
$storageUsed = ($storageUsed * 100) / $totalStorage;

$trial_duration = Configure::read('trial_duration');
?>
<style>
    .nav-tabs a{
        padding: 4px 17px;
    }
</style>
<div class="container">
    <nav class="nav nav-tabs" style="margin-top: 0px;">
        <ul>
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?>
                <li <?php if (isset($tab) && $tab == 1): ?> class="active"  <?php endif; ?> >
                    <a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/1" ?>">School/District/Institution Settings</a>
                </li>
            <?php endif; ?>
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?>
                <li <?php if (isset($tab) && $tab == 2): ?> class="active"  <?php endif; ?>>
                    <a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/2" ?>">Colors &amp; Logo</a>
                </li>
            <?php endif; ?>
            <li <?php if (isset($tab) && $tab == 7): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/7" ?>">Archiving</a></li>
            <li <?php if (isset($tab) && $tab == 3): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/3" ?>">Plans</a></li>
            <li <?php if (isset($tab) && $tab == 4): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/customer/updateBillingInfo/" . $account_id . "/4" ?>">Billing Information</a></li>
            <?php if ($sample_huddle_not_exists): ?>
                <li <?php if (isset($tab) && $tab == 6): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/6" ?>">Restore sample huddle</a></li>
            <?php endif; ?>
            <li <?php if (isset($tab) && $tab == 5): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/5" ?>">Cancel Account</a></li>
        </ul>
    </nav>
</div>
<div id="main" class="container box">
    <div class="header">
        <h2 class="header-title">Plans</h2>
    </div>
    <div class="row">
        <div class="span8">
            <p><b>Storage</b></p>
            <?php //echo $myAccount['Account']['storage_used'] / (1024 * 1024 * 1024);          ?>
            <span class="progress progress-striped progress-success"><span class="bar" style="width:<?php echo $storageUsed; ?>%;"></span></span>
            <span class="progress-status">
                <span><?php echo $storageUsed != '' ? round($storageUsed) . "%" : '0%'; ?></span> (of <?php echo (isset($myAccount['plans']['storage']) ? $myAccount['plans']['storage'] : $trial_storage); ?> GB)
            </span>
            <hr>
            <p><b>Users</b></p>
            <p>Currently <b><?php echo $totalUsers . ($totalUsers > 1 ? ' Users' : ' User'); ?></b> have joined this account.
                Max <b><?php echo ( (isset($myAccount['plans']) && isset($myAccount['plans']['users']) ) ? $myAccount['plans']['users'] : $trial_users); ?> Users</b> can be added to your current plan.
                To add more users, upgrade your account using the dropdown menu above.</p>
            <hr>
            <?php if ($myAccount['plans']['id'] != ''): ?>
                <p><b>Account Type: <?php echo ($myAccount['plans']['plan_type']); ?></b></p>
                <p>Do you want to change your plan, Upgrade to a plan below! </p>
            <?php else: ?>
                <p><b>Account Type: Trial</b></p>
                <p>
                    You are currently on a trial subscription. Your trial runs until
                    <b><?php echo date('F d, Y', strtotime("+{$trial_duration} day", strtotime($myAccount['Account']['created_at']))); ?></b>.
                </p>
                <p>Don't let your trial account expire! Upgrade to a plan below!</p>
            <?php endif; ?>


            <form accept-charset="UTF-8" action="<?php echo $this->base . '/customer/create_subscription/' . $account_id . '/' . '3' ?>" enctype="multipart/form-data" method="post">
                <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                <div class="input-group select size-extra-big">
                    <?php echo $this->Form->input('plan_id', array('type' => 'select', 'options' => $plans, 'label' => false, 'class' => 'size-big', 'div' => false, 'selected' => $myAccount['Account']['plan_id']), array('id' => 'plan_plan_id')); ?>
                </div>
                <?php if ($myAccount['plans']['id'] == ''): ?>
                    <p>
                        <i>
                            <span class="green">**</span>
                            Since you are running on a trial account you need to add a valid credit card (Visa, MasterCard, Discover, or American Express) in order to subscribe to a paid plan.
                        </i>
                    </p>
                <?php endif; ?>
                <hr>
                </div>
                </div>
                <div class="form-actions lmargin-left lmargin-top">
                    <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="btn btn-green">Update Plan</button>
                    <a href="<?php echo $this->base . '/dashboard' ?>" class="btn btn-transparent">Cancel</a>
                </div>
            </form>
        </div>
