<script src="<?php echo $this->webroot . 'app/js/jquery-2.1.1.js' ?>"></script>
<script src="<?php echo $this->webroot . 'app/js/main.js' ?>"></script>
<script type="text/javascript" src="/js/application.js"></script>
<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<?php $breadcrumb_language_based_content = $this->Custom->get_page_lang_based_content('BreadCrumb'); ?>
<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages');

$jsdata = $this->Custom->get_page_lang_based_content('JSDATA');
?>
<script>

    window.language_based_content_jsData = JSON.parse('<?php echo addslashes(json_encode($jsdata)) ?>');
    //console.log(language_based_content);
</script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<style>
    body {
        background-color: none;
    }
        .gicons { text-align:right; padding-top: 11px;}
        .gicons a {color:#8B8B8B; text-decoration:none !important;font-weight:600; }
        .gicons img {max-width: 23px;margin-right: 4px;margin-top: -3px; }
        .hide_li{
      display:none;
        }
    .cd-tabs-navigation a.selected{
        box-shadow: inset 0 2px 0 <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?> !important;
    }
	select::-ms-expand {
    display: none;
}
select.simple-control:disabled{
         /*For FireFox*/
        -webkit-appearance: none;
        /*For Chrome*/
        -moz-appearance: none;
}
/*For IE10+*/
select:disabled.simple-control::-ms-expand {
        display: none;
}
    .input-group label{width: 100%!important ; }
    .red_browse_plan {
        background: #f26c4f;
        color: #fff !important;
        text-align: center !important;
        font-size: 13px !important;
        border-radius: 40px !important;
        padding: 10px 30px !important;
        border: 0px !important;
        outline: none;
        cursor: pointer;
        width: inherit !important;
        margin-top: 70px;
        margin-left: 103px;
    }

    .reset_settingsLink {
        margin-top: 0px;
    }

    .colorpicker_tables td {
        text-align: left;
        border: 0;
        padding: 10px 7px;
    }

    .colorpicker_tables .color-preview {
        border-right: 0;
        height: 34px;
    }

    .colorpicker_tables .color-picker-wrap {
        width: 45px;
    }

    .tab-content {
        visibility: visible;
    }

    .size-full {
        width: 100%;
    }

    .size_full_adjs select {
        width: 90%;
    }

    .reb_step3_div textarea {
        font-size: 14px;
    }

    .reb_step3_div .f-group {
        padding: 3px 10px;
    }

    .input-group {
        margin: 0 0 3px 0;
        padding: 3px 3px;
    }

    .infobox-container {
        position: relative;
        display: inline-block;
        margin: 0;
        padding: 0;
        width: auto;
    }

    .infobox {
        width: 250px;
        padding: 10px 5px 5px 5px;
        margin: 10px;
        position: relative;
        z-index: 90;
        -webkit-border-radius: 2px;
        -moz-border-radius: 2px;
        border-radius: 2px;
        -webkit-box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.55);
        -moz-box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.55);
        box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.55);
        background: #424242;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#6a6b6b), to(#424242));
        background-image: -moz-linear-gradient(top, #6a6a6a, #424242);
        color: #fff;
        font-size: 90%;
    }

    .infobox h3 {
        position: relative;
        width: 270px;
        color: #fff;
        padding: 10px 5px;
        margin: 0;
        left: -15px;
        z-index: 100;
        -webkit-box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.55);
        -moz-box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.55);
        box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.55);
        background: #3198dd;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#33acfc), to(#3198dd));
        background-image: -moz-linear-gradient(top, #33acfc, #3198dd);
        font-size: 160%;
        text-align: center;
        text-shadow: #2187c8 0 -1px 1px;
        font-weight: bold;
    }

    .infobox-container .triangle-l {
        border-color: transparent #2083c2 transparent transparent;
        border-style: solid;
        border-width: 13px;
        height: 0;
        width: 0;
        position: absolute;
        left: -12px;
        top: 45px;
        z-index: 0; /* displayed under infobox */
    }

    .infobox-container .triangle-r {
        border-color: transparent transparent transparent #2083c2;
        border-style: solid;
        border-width: 13px;
        height: 0;
        width: 0;
        position: absolute;
        left: 266px;
        top: 45px;
        z-index: 0; /* displayed under infobox */
    }

    .infobox a {
        color: #35b0ff;
        text-decoration: none;
        border-bottom: 1px dotted transparent;
    }

    .infobox a:hover, .infobox a:focus {
        text-decoration: none;
        border-bottom: 1px dotted #35b0ff;
    }

    .lock_box {
        margin-top: -10px;
    }

    .btn-group > .btn-group:not(:last-child) > .btn, .btn-group > .btn:not(:last-child):not(.dropdown-toggle) {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        border-color: #ccc;
    }

    .btn-group > .btn-group:not(:first-child) > .btn, .btn-group > .btn:not(:first-child) {
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
        border-color: #ccc;
    }

    .cd-tabs-content li {
        padding: 20px 0px 4px 0px;
    }

    .blueFinishedBtn {
        padding: 7px 18px;
        text-align: center;
        display: inline-block;
        padding: 7px 18px;
        min-width: 101px;
        text-decoration: none;
        border-radius: 4px;
        margin-left: 6px;
        font-weight: 400;
        border: 0;
        font-size: 14px;
        background: <?php echo $this->Custom->get_site_settings('rubric_previus') ?>;
        color: #fff;
    }

    .btn-circle {
        box-shadow: none;
    }

    .reb_step3_div .f-group {
        font-size: 14px;
        color: #636363;
        padding: 5px;
    }

    .settings_body {
        padding-bottom: 46px;
    }

    .settings_body input[type="text"] {
        font-weight: normal;
        box-shadow: none;
    }

    .onoffswitch-label {
        display: block;
        overflow: hidden;
        cursor: pointer;
        border: 0px;
        border-radius: 20px;
        height: 20px;
        width: 55px;
    }

    .onoffswitch-inner:after {
        content: "OFF";
        padding-right: 10px;
        background-color: #a4a4a4;
        color: #fff;
        text-align: right;
    }

    .onoffswitch-inner:before, .onoffswitch-inner:after {
        display: block;
        float: left;
        width: 50%;
        height: 20px;
        padding: 0 11px 0 0;
        line-height: 20px;
        font-size: 10px;
        color: white;
        font-family: Trebuchet, Arial, sans-serif;
        font-weight: bold;
        box-sizing: border-box;
    }

    .onoffswitch-switch {
        display: block;
        width: 14px;
        height: 14px;
        margin: 6px;
        background: #fff;
        position: absolute;
        top: -3px;
        bottom: 0;
        right: 38px;
        border-radius: 20px;
        transition: all 0.3s ease-in 0s;
    }

    .onoffswitch-inner:before {
        content: "ON";
        padding-left: 10px;
        background-color: <?php echo $this->Custom->get_site_settings('rubric_next'); ?>;
        color: #FFF;
    }

    .onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
        right: 3px;
    }

    .btn-success {
        color: #fff;
        background-color: #5ab75c;
        border-color: #5ab75c;
    }

    .btn-primary {
        color: #fff;
        background-color: #5092ce;
        border-color: #5092ce;
    }

    .sortable {
        border-left: solid 1px #e2e2e2;
    }

    /*    .datagrid_trans table {
            border-collapse: collapse;
            text-align: left;
            width: 100%;
    }
    .datagrid_trans {
            font-size:14px;
            background: #fff;
            overflow: hidden;
            border: 1px solid #F2F3F6;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
    }
    .datagrid_trans table td, .datagrid table th {
            padding: 5px 10px;
    }
    .datagrid_trans table thead th {
            background-color: #F2F3F6;
            color: #353535;
            font-size: 14px;
            font-weight: normal;
            border-left: 1px solid #E6E7EA;
            text-align:left;
    }
    .datagrid_trans table thead th:first-child {
            border: none;
    }
    .datagrid_trans table tbody td {
            color: #00496B;
            border-left: 1px solid #F2F3F6;
            font-size: 13px;
            border-bottom: 1px solid #F2F3F6;
            font-weight: normal;
            text-align:left;
    }
    .datagrid_trans table tbody td:first-child {
            border-left: none;
    }
    .datagrid_trans table tbody tr:last-child td {
            border-bottom: none;
    }*/

    .send_message_dialog {
        padding: 20px;
    }

    .send_message_dialog input[type="text"], .send_message_dialog input[type="email"], .send_message_dialog textarea {
        width: 100%;
        margin-bottom: 8px;
        margin-top: 3px;
    }

    .account_checkbox {
        margin-top: 25px;
        margin-left: 12px;
    }

    .plan_box {
        background: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;
        color: #fff;
        padding: 10px;
    }
    .progress_bill{
        background: <?php echo $this->Custom->get_site_settings('primary_bg_color') ?>;
    }


    .plan_box h2 {
        color: #fff;
        font-family: sans-serif;
    }

    .reset_btn {
        text-align: center;
    }

    .reset_btn a {
        background: #f1c40f;
        color: #fff;
        text-align: center;
        font-size: 13px;
        border-radius: 40px;
        padding: 10px 38px;
        border: 0px;
        outline: none;
        cursor: pointer;
        font-weight: normal;
    }

    .rubric_main h3 {
        font-size: 26px;
        font-weight: 300;
        color: #414141;
        margin: 0px;
    }

    .rub_adnew {
        text-align: right;
    }

    .rub_adnew a {
        background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>;
        display: inline-block;
        padding: 6px 30px;
        border-radius: 4px;
        color: #fff;
        text-decoration: none;
        font-weight: 400;
        font-size: 14px;
    }

    .rubric_table {
        font-size: 14px;
    }

    .rubric_table td {
        text-align: center;
        padding: 10px 5px;
    }

    .rubric_table th {
        text-align: center;
        padding: 10px 5px;
        color: #6A6A6A;
    }

    .rubric_table .reb_cell1 {
        text-align: left !important;
        width: 60%;
    }

    .rubric_th {
        border-bottom: 2px solid #c7c7c7 !important;
    }

    .rub_lasrcell {
        position: relative;
    }

    .rub_dots {
        cursor: pointer;
    }

    .rub_options {
        background: #fff;
        position: absolute;
        padding: 5px 10px;
        right: 26px;
        top: 42px;
        border-radius: 4px;
        border: 1px solid #ddd;
        display: none;
        z-index: 9;
        width: 106px;
    }

    .rub_options li:hover {
        background: #f7f7f7;
    }

    .rub_options ul {
        padding: 0;
        list-style: none;
        margin: 0;
    }

    .rub_options ul li {
        display: block;
        text-align: left;
        padding: 3px 5px;
        border: 0;
        width: auto;
        font-size: 13px;
    }

    .rub_options ul li a {
        color: #616161 !important;
        text-decoration: none !important;
        display: block;
    }

    .rubric_table {
        padding-bottom: 20px;
    }

    .rub_maincls .selected {
        background: none !important;
    }

    .selectedRB {
        background-color: #F1F1F1;
    }

    .stepscls {
        max-width: 650px;
        margin: 0 auto;
        margin-top: 30px;
    }

    .step1cls {
        float: left;
        width: 45%;
    }

    .step2cls {
        float: left;
        width: 45%;
    }

    .step3cls {
        float: left;
        width: 4%;
        border-bottom: 0px !important;
    }

    .stepscls .fa-dot-circle {
        color: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>;
        position: relative;
        display: inline-block;
        font-size: 20px;
        background: #fff;
        top: -11px;
    }

    .stepscls .fa-circle {
        color: #ececec;
        position: relative;
        display: inline-block;
        font-size: 20px;
        background: #fff;
        top: -11px;
    }

    .stepborder {
        border-bottom: 1px solid #ececec;
        width: 90%;
        margin-left: 24px;
    }

    .stepscls label {
        color: #626262;
        font-size: 15px;
        display: block;
        font-family: 'roboto';
    }

    .step1cls label {
        position: relative;
        left: -31px;
        top: -10px;
    }

    .step2cls label {
        position: relative;
        left: -49px;
        top: -10px;
    }

    .step3cls label {
        position: relative;
        left: -19px;
        top: -10px;
    }

    .activestp {
        border-bottom: 1px solid <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>;
    }

    .reb_step1_div {
        border-top: 1px solid #ececec;
        padding-top: 20px;
        width: 100%;
        font-family: 'roboto';
    }

    .reb_step1_div input[type=text] {
        height: 36px !important;
        padding: 6px !important;
        margin-top: 4px;
    }

    .reb_step1_div label {
        margin-bottom: 0;
        font-size: 14px;
        color: #626262;
    }

    .reb_step2_div {
        border-top: 1px solid #ececec;
        padding-top: 20px;
        width: 100%;
        font-family: 'roboto';
        margin-bottom: 10px;
    }

    .reb_step2_div input[type=text] {
        height: 36px !important;
        padding: 6px !important;
        margin-top: 4px;
        margin-bottom: 15px;
        box-shadow: none;
    }

    .reb_step2_div textarea {
        padding: 10px;
        border: solid 1px #c5c5c5;
        border-radius: 3px;
        outline: none;
        width: 100%;
        box-sizing: border-box;
        font-weight: normal;
        font-size: 13px;
        box-shadow: none;
    }

    .reb_step2_div label {
        margin-bottom: 0;
        font-size: 14px;
    }

    .tires_cls label {
        display: block;
        width: 100%;
        font-weight: 400;
        font-size: 14px;
        color: #414141;
        margin-bottom: 7px;
    }

    .tires_cls {
        margin-top: 30px
    }

    .tires_cls .btn-group {
        margin: 1px 0 0 0 !important;
    }

    .tires_cls .btn-outline-dark:not(:disabled):not(.disabled).active, .tires_cls .btn-outline-dark:not(:disabled):not(.disabled):active, .tires_cls .show > .btn-outline-dark.dropdown-toggle {
        background-color: #e6e6e6;
        color: #4c4c4c;
        border-color: #cccccc;
    }

    .tires_cls .btn-outline-dark {
        padding: 6px 18px;
        box-shadow: none;
    }

    .tires_cls .btn-outline-dark:hover {
        background-color: #e6e6e6;
        color: #4c4c4c;
        border-color: #cccccc;
    }

    .tires_cls .btn-outline-dark {
        padding: 6px 18px;
    }

    .reb_step1_div h6 {
        font-size: 18px;
        margin-top: 40px;
        font-weight: 400;
        font-family: 'roboto';
        color: #4c4c4c;
    }

    .tires_cls .btn-outline-dark:focus {
        box-shadow: 0 0 0 0.0rem rgba(52, 58, 64, 0) !important;
    }

    .rubric_op {
    }

    .L4 {
        margin-left: 105px;
    }

    .reb_step1_div .f-group {
        margin-bottom: 0;
        padding: 0 10px;
        color: #636363;
        font-size: 15px;
    }

    .fram_sample {
        margin-top: 30px;
    }

    .buttons_reb {
        text-align: right;
        width: 100%;
        border-top: 1px solid #ececec;
        padding-top: 20px;
        margin-top: 40px;
    }

    .buttons_reb a {
        text-align: center;
        display: inline-block;
        padding: 7px 18px;
        min-width: 101px;
        text-decoration: none;
        border-radius: 4px;
        margin-left: 6px;
        font-weight: 400;
        border: 0;
        font-size: 14px;
    }

    .buttons_reb input[type=submit] {
        text-align: center;
        display: inline-block;
        padding: 7px 18px;
        min-width: 101px;
        text-decoration: none;
        border-radius: 4px;
        margin-left: 6px;
        font-weight: 400;
        border: 0;
        font-size: 14px;
    }

    .buttons_reb .reb_cancel {
        background: #fff;
        color: #636363;
        min-width: 50px;
    }

    .buttons_reb .reb_save {
        background: <?php echo $this->Custom->get_site_settings('g_color_4'); ?>;
        color: #fff;
    }

    .buttons_reb .reb_next {
        background: <?php echo $this->Custom->get_site_settings('rubric_next'); ?>;
        color: #fff;
    }

    .rubric_op h6 {
        margin-bottom: 20px;
    }

    .rubric_op .label_txt {
        font-size: 14px;
        color: #636363;
    }

    .border_leftrb {
        border-left: 1px solid #e2e2e2;
        margin-left: -6px;
        padding: 0 0 50px 35px;
    }

    .rubric_op .onoffswitch {
        float: left;
        margin-right: 10px;
    }

    .rub_onoff {
        float: left;
        width: 25%;
    }

    .rub_onoff .onoffswitch {
        float: left;
        margin-right: 10px;
    }

    .onoff_main {
        padding: 10px 15px 50px 15px;
    }

    .rub_g_btn {
        border: 0;
        border-radius: 50%;
        background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>;
        color: #fff;
        margin: 0 0 3px 4px !important;
    }

    .rub_b_btn {
        border: 0;
        border-radius: 50%;
        background: #5491ce;
        color: #fff;
        margin: 0 0 3px 4px !important;
    }

    .topaddminus {
        text-align: right;
    }

    .rub_used {
        color: #8f8f8f;
        text-align: right;
        padding-bottom: 15px;
        font-size: 13px;
    }

    .rub_box_dark {
        /*background: #f5f5f5;*/
        margin-bottom: 15px;
        padding: 7px 12px 12px 10px;
        display: block;
        position: relative;
        margin-left: 103px;
    }

    .rub_box_light {
        background: #fff;
        margin-bottom: 25px;
        padding: 7px 12px 12px 10px;
    }

    .rub_line {
        display: block;
        border-bottom: 1px solid #cccccc;
        position: relative;
        top: -11px;
        position: relative;
        width: 55%;
        right: -50px;
    }

    .rub_row label {
        font-size: 18px;
    }

    .rub_row p {
        font-size: 13px;
        margin-bottom: 0;
    }

    .rub_row {
    }

    .rub_rowleft span {
        font-size: 13px;
        display: inline-block;
        /*background: #fff;*/
        padding: 2px 2px 2px 0;
        position: relative;
        z-index: 1;
    }

    .rub_higest span {
        color: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>;
    }

    .rub_lowest span {
        color: #f4836d;
    }

    .rub_rowleft {
        padding-top: 10px;
        position: absolute;
        left: -81px;
        top: 1px;
        width: 55px;

    }

    .rub_box_dark .f-item.content {
        font-size: 14px;
    }

    .rub_box_dark .f-item label {
        font-size: 19px;
        font-weight: 400;
    }

    .buttons_reb a {
        text-align: center;
        display: inline-block;
        padding: 7px 18px;
        min-width: 101px;
        text-decoration: none;
        border-radius: 4px;
        margin-left: 6px;
        font-weight: 400;
        border: 0;
        font-size: 14px;
        background: <?php echo $this->Custom->get_site_settings('rubric_previus') ?>;
        color: #fff;
    }

    .reb_step3_div {
        border-top: 1px solid #ececec;
        padding-top: 20px;
        width: 100%;
        font-family: 'roboto';
        margin-bottom: 10px;
    }

    .reb_step3_div input[type=text] {
        height: 36px !important;
        padding: 6px !important;
        margin-top: 4px;
        margin-bottom: 8px;
    }

    .reb_step3_div textarea {
        height: 105px !important;
        padding: 6px !important;
        margin-top: 4px;
        display: block;
        width: 100%;
        margin-bottom: 8px;
        box-shadow: none;
    }

    .reb_step3_div label {
        margin-bottom: 0;
        font-size: 14px;
    }

    .reb_step3_div .f-group {
        margin-bottom: 0;
        padding: 0 10px;
    }

    .reb_step3_div .border_leftrb {

    }

    .error_msg_text {
        color: red;
    }

    .error_message_tier {
        color: red;
    }

    .error_message_check_tier {
        color: red;
    }

    .table-striped tbody tr:nth-of-type(odd) {
        background: #fff !important;
    }

    #rubrics-table_length {
        display: none;
    }

    #rubrics-table_filter {
        text-align: right;
    }

    .rubric_th {
        background: #fff !important;
    }

    #rubrics-table td {
        border: 0;
        border-bottom: 1px solid #f2f2f2;
    }

    #rubrics-table th {
        border: 0;
        border-bottom: 1px solid #f2f2f2;
    }

    #rubrics-table.dataTable.no-footer {
        padding-bottom: 0;
        border-bottom: 0px solid #f2f2f2;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button {
        border-radius: 0 !important;
        padding: 0 !important;
        border: 0 !important;
    }

    div.dataTables_wrapper .dataTables_paginate .paginate_button.current, div.dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        background: none !important;
        border: 0 !important;
        color: #0056b3 !important;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button:active {
        box-shadow: inset 0 0 0px #111 !important;
        background: none !important;
        color: #000 !important;
        border: 0 !important;
    }

    .dataTables_wrapper .dataTables_paginate .paginate_button:hover {
        box-shadow: inset 0 0 0px #111 !important;
        background: none !important;
        color: #000 !important;
        border: 0 !important;
    }

    div.dataTables_wrapper .dataTables_paginate .paginate_button:hover {
        color: #000 !important;
        border: 0 !important;
    }

    div.dataTables_wrapper div.dataTables_paginate div.paginate_button.disabled, div.dataTables_wrapper div.dataTables_paginate div.paginate_button.disabled:hover, div.dataTables_wrapper .dataTables_paginate .paginate_button.disabled:active {
        border: 0 !important;
    }

    .fscroll {
        max-height: 400px;
        overflow: auto;
        overflow-x: hidden;
    }
    .account_user_privileges {
        float: left;
        width: 48%;
    }
    .account_admin_privileges {
        float: right;
        width: 48%;
    }
    
    
    .performance_level_alert {
    background: #fff3cd;
    color: #94751c;
    padding: 15px 50px;
    text-align: center;
    border-radius: 5px;
    margin: 15px 0;
    position :relative;
        width: 70%;
    margin: 0 auto;
    font-size: 13px;
    margin-bottom: 25px;
    margin-top: -18px;
    }
    
    .performance_level_alert_wo_cross {
    background: #fff3cd;
    color: #94751c;
    padding: 15px 50px;
    text-align: center;
    border-radius: 5px;
    margin: 15px 0;
    position :relative;
        width: 70%;
    margin: 0 auto;
    font-size: 13px;
    }
    
    .language_sp .cd-tabs-navigation li a{ width:auto !important;     padding: 0px 13px !important;}

    .cd-tabs-content .field_box p{ margin-bottom:0;}
    .language_sp .field_box label {    font-size: 12px;}
    .cd-tabs-content li {
   padding: 20px 0px 20px 0px !important;     border-right: 0 !important;
}
    
    
    
</style>
<div id="showflashmessage1">
</div>
<?php
$check_plan_deactivate = $this->Custom->check_plan_deactivate($account_id);
$trial_storage = 5;
$trial_users = 40;
?>

<?php
$storageUsed = $myAccount['Account']['storage_used'];

$user_current_account = $this->Session->read('user_current_account');

if ($myAccount['Account']['in_trial'] == '1') {
    $totalStorage = 20;
} else {
    if (!empty($myAccount['plans']['category'])) {
        $totalStorage = $myAccount['plans']['storage'] * $myAccount['Account']['plan_qty'];
    } else {
        $totalStorage = $myAccount['plans']['storage'];
    }
}
$storageUsed = $storageUsed / 1024;
$storageUsed = $storageUsed / 1024;
$storageUsed = $storageUsed / 1024;
$storageUsed = ($this->Custom->get_consumed_storage($account_id) * 100) / $this->Custom->get_allowed_storage($account_id);
if ($storageUsed > 100) {
    $storageUsed = 100;
}

$trial_duration = Configure::read('trial_duration');
?>
<script>
    $(document).ready(function () {
        $(".tabs-menu a").click(function (event) {
            event.preventDefault();
            $(this).parent().addClass("current");
            $(this).parent().siblings().removeClass("current");
            var tab = $(this).attr("href");
            $(".tab-content").not(tab).css("display", "none");
            $(tab).fadeIn();
        });

        $(".up").on('click', function () {
            id = '#' + $(this).attr('id-data');
            min_limit = $(this).attr('min-limit');
            max_limit = $(this).attr('max-limit');
            value = parseInt($(this).parent("div").parent('div').find('input').val()) + 1;


            if (value < max_limit && value > min_limit || (value == max_limit)) {
                $(this).parent("div").parent('div').find('input').val(parseInt($(this).parent("div").parent('div').find('input').val()) + 1);
                // $(".quantity").val(parseInt($(this).parent("div").parent('div').find('input').val()));

                $(id).val(parseInt($(this).parent("div").parent('div').find('input').val()));
            }


        });

        $(".down").on('click', function () {
            id = '#' + $(this).attr('id-data');
            min_limit = $(this).attr('min-limit');
            max_limit = $(this).attr('max-limit');
            value = parseInt($(this).parent("div").parent('div').find('input').val()) - 1;
            value_check = parseInt($(this).parent("div").parent('div').find('input').val());
            in_trial = '<?php echo $myAccount['Account']['in_trial']; ?>';
            if (value_check == min_limit && in_trial && value_check != 1 && value_check != 21) {
                alert('<?php echo $alert_messages['to_purchase_fewer_licenses']; ?>');
            }


            if ((value < max_limit && value > min_limit) || (value == min_limit)) {
                $(this).parent("div").parent('div').find('input').val(parseInt($(this).parent("div").parent('div').find('input').val()) - 1);
                // $(".quantity").val(parseInt($(this).parent("div").parent('div').find('input').val()));
                $(id).val(parseInt($(this).parent("div").parent('div').find('input').val()));
            }
        });
        $(".up2").on('click', function () {
            $(".incdec2 input").val(parseInt($(".incdec2 input").val()) + 1);
        });

        $(".down2").on('click', function () {
            $(".incdec2 input").val(parseInt($(".incdec2 input").val()) - 1);
        });

        $("#school_annually").on('click', function () {
            $("#school_annually").addClass("btn-special-active");
            $("#school_monthly").removeClass("btn-special-active");
            $(".school_planBox_yearly").show();
            $(".school_planBox_monthly").hide();
        });
        $("#school_monthly").on('click', function () {
            $("#school_monthly").addClass("btn-special-active");
            $("#school_annually").removeClass("btn-special-active");
            $(".school_planBox_yearly").hide();
            $(".school_planBox_monthly").show();
        });
        $("#ED_annually").on('click', function () {
            $("#ED_annually").addClass("btn-special-active");
            $("#ED_monthly").removeClass("btn-special-active");
            $(".ED_planBox_yearly").show();
            $(".ED_planBox_monthly").hide();
        });
        $("#ED_monthly").on('click', function () {
            $("#ED_monthly").addClass("btn-special-active");
            $("#ED_annually").removeClass("btn-special-active");
            $(".ED_planBox_yearly").hide();
            $(".ED_planBox_monthly").show();
        });
        $("#business_annually").on('click', function () {
            $("#business_annually").addClass("btn-special-active");
            $("#business_monthly").removeClass("btn-special-active");
            $(".business_planBox_yearly").show();
            $(".business_planBox_monthly").hide();
        });
        $("#business_monthly").on('click', function () {
            $("#business_monthly").addClass("btn-special-active");
            $("#business_annually").removeClass("btn-special-active");
            $(".business_planBox_yearly").hide();
            $(".business_planBox_monthly").show();
        });

        $(document).on("click", ".login-button", function () {
            var form = $(this).closest("form");
            //console.log(form);
            form.submit();
        });


        $("#change_plan").on('click', function () {
            $("#billing").removeClass("selected");
            $("#plan").addClass("selected");
            $("#billing_anchor").removeClass("selected");
            $("#plan_anchor").addClass("selected");
            $('.cd-tabs-content').removeAttr('style');
        });

        $("#billing_anchor").on('click', function () {
            var in_trial = '<?php echo $myAccount['Account']['in_trial']; ?>';
            var account_id = '<?php echo $account_id; ?>';
            var selected_plan = '<?php echo empty($selected_plan); ?>';
            url = home_url + '/accounts/account_settings_all/' + account_id + '/5';
            if (in_trial == 1 && selected_plan) {
                window.location.href = url;
            }

        });


    });

</script>
<div class="gicons">
 <a href ="<?php echo $this->base . '/home/account-settings' ?>"><img src="<?php echo $this->webroot . 'app/img/g_setting.png' ?>"  >
                    <?php echo $language_based_content['back_to_account_settings_global_2'];?></a>
</div>
<br>
<div class="rub_maincls">
    <div class="settings_header" style="display: none;">
        <?php if ($sample_huddle_not_exists): ?>
            <div class="setting_header_left">
                <a href="<?php echo $this->base . '/huddles/restore'; ?>"
                   data-confirm="<?php echo $language_based_content['restore_sample_huddles_ques_all']; ?>" data-method="delete"
                   rel="nofollow"><img src="<?php echo $this->webroot . 'app/img/restore.png' ?>" width="15" height="15"
                                    alt=""/><?=$language_based_content['restore-sample-huddle'];?> </a>
            </div>
        <?php endif; ?>
        <?php if ($myAccount['Account']['deactive_plan'] == 1 && $myAccount['Account']['custom_storage'] == -1): ?>
            <p style="margin-left: 860px;"><b><?=$language_based_content['unlimited-storage'];?></b></p>
        <?php else: ?>

            <div class="setting_header_right">
                <div class="progress_bar_box">
                    <?php if (!empty($myAccount['plans']['category'])) : ?>
                                                                                                                                                                                                                <!--                    <div class="total_space">Total <?php //echo (isset($myAccount['plans']['storage']) ? $myAccount['plans']['storage'] * $myAccount['Account']['plan_qty'] : $trial_storage);                                                                                ?> GB</div>-->
                        <div class="total_space"><?=$language_based_content['total'];?> <?php echo $this->Custom->get_allowed_storage($account_id); ?>
                            GB
                        </div>
                    <?php else : ?>
                        <!--                    <div class="total_space">Total <?php //echo (isset($myAccount['plans']['storage']) ? $myAccount['plans']['storage'] : $trial_storage);                                                                                ?> GB</div>-->
                        <div class="total_space"><?=$language_based_content['total'];?> <?php echo $this->Custom->get_allowed_storage($account_id); ?>
                            GB
                        </div>
                    <?php endif; ?>
                    <div class="progress_bar_bg"
                         style="width:<?php echo $storageUsed != '' ? round($storageUsed) . "%" : '0%'; ?>"><?php echo $storageUsed != '' ? round($storageUsed) . "%" : '0%'; ?></div>
                </div>
            </div>
        <?php endif; ?>
        <div class="clear"></div>
    </div>
    <div class="settings_body">

        <div class="cd-tabs">
            <nav>
                <ul class="cd-tabs-navigation">
                    <li class = "hide_li"><a data-content="account" <?php
                        if (isset($tab) && $tab == 1) {
                            echo 'class="selected"';
                        }
                        ?> href="#0"><img src="<?php echo $this->webroot . 'app/img/account_settings.png' ?>" alt=""/><?php echo $language_based_content['account_settings_all']; ?></a></li>
                    <li class = "hide_li"><a data-content="color" <?php
                        if (isset($tab) && $tab == 2) {
                            echo 'class="selected"';
                        }
                        ?> href="#0"><img src="<?php echo $this->webroot . 'app/img/color_logo.png' ?>" alt=""/><?php echo $language_based_content['colors_logo_all']; ?></a></li>
                    <?php if ($myAccount['Account']['in_trial'] != '1'): ?>
                        <li class = "hide_li"><a data-content="archiving" <?php
                            if (isset($tab) && $tab == 3) {
                                echo 'class="selected"';
                            }
                            ?> href="#0"><img src="<?php echo $this->webroot . 'app/img/archiving.png' ?>" alt=""/><?php echo $language_based_content['archiving_all']; ?></a>
                        </li> <?php endif; ?>
                    <?php if (!$deactive_plan): ?>
                        <li class = "hide_li"><a id="plan_anchor" data-content="plan" <?php
                            if (isset($tab) && $tab == 4) {
                                echo 'class="selected"';
                            }
                            ?> href="#0"><img src="<?php echo $this->webroot . 'app/img/plans.png' ?>" alt=""/><?php echo $language_based_content['plans_all']; ?></a></li>
                        <li class = "hide_li"><a id="billing_anchor" data-content="billing" <?php
                            if (isset($tab) && $tab == 5) {
                                echo 'class="selected"';
                            }
                            ?> href="#0"><img src="<?php echo $this->webroot . 'app/img/billing.png' ?>" alt=""/><?php echo $language_based_content['billing_information_all']; ?></a></li>
                        <li class = "hide_li"><a data-content="cancel" <?php
                            if (isset($tab) && $tab == 6) {
                                echo 'class="selected"';
                            }
                            ?> href="#0"><img src="<?php echo $this->webroot . 'app/img/cancel_account.png' ?>" alt=""/><?php echo $language_based_content['cancel_account_all']; ?></a></li>

                    <?php endif; ?>
                    <?php if ( ($deactive_plan && $myAccount['Account']['in_trial'] != '1' && ($this->Custom->is_enabled_framework_and_standards($account_id)) ) || ($myAccount['Account']['in_trial'] != '1' && ($this->Custom->is_enabled_framework_and_standards($account_id)) && !in_array(intval($myAccount['Account']['plan_id']), $emitplans, true)) ): ?>
                        <?php if (!empty($sub_tab) && $sub_tab != 1): ?>
                            <li>
                                <a data-content="rubric"
                                <?php
                                if (isset($tab) && $tab == 7) {
                                    echo 'class="selected"';
                                }
                                ?>
                                   data-toggle="modal" data-target="#rubric_cancel" data-backdrop="static" data-keyboard="false" href="#">
                                    <img src="<?php echo $this->webroot . 'app/img/rubric.svg' ?>" alt=""/><?php echo $language_based_content['rubric_all']; ?></a>
                            </li>
                        <?php else: ?>
                            <li><a data-content="rubric" <?php
                                if (isset($tab) && $tab == 7) {
                                    echo 'class="selected"';
                                }
                                ?> href="#0"><img src="<?php echo $this->webroot . 'app/img/rubric.svg' ?>" alt=""/><?php echo $language_based_content['rubric_all']; ?></a>
                            </li>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?php if ($myAccount['Account']['in_trial'] != '1'):?>
                    <li class = "hide_li"><a data-content="global_export" <?php
                        if (isset($tab) && $tab == 8) {
                            echo 'class="selected"';
                        }
                        ?> href="#0"><img src="<?php echo $this->webroot . 'app/img/account_settings.png' ?>" alt=""/>
                            <?php echo $language_based_content['global_export_all_global'];?></a>
                    </li>
                    <?php endif;?>
                </ul>
            </nav>
            <ul class="cd-tabs-content">
                <li  data-content="account" <?php
                if (isset($tab) && $tab == 1) {
                    echo 'class="selected"';
                }
                ?>>

                    <form accept-charset="UTF-8"
                          action="<?php echo $this->base . '/accounts/tab_1_edit/' . $account_id . '/1' ?>"
                          class="edit_account" enctype="multipart/form-data" id="edit_account_116" method="post">

                        <div class="field_box">
                            <label><?php echo $language_based_content['account_name_all'];?></label>
                            <input id="account_company_name" name="account[company_name]" size="30" type="text"
                                   value="<?php echo $myAccount['Account']['company_name'] ?>"/>
                        </div>

                        <input id="tab" name="tab" type="hidden" value="1"/>
                        <input type="hidden" id="txtchkframework" name="txtchkframework"
                               value="<?php echo $framwork_value; ?>"/>
                        <input type="hidden" id="txtchkenablewsframework" name="txtchkenablewsframework" value="<?php echo $chkenablewsframework; ?>" />
                        <input type="hidden" id="txtchkenablewstags" name="txtchkenablewstags" value="<?php echo $chkenablewstags; ?>" />
                        <input type="hidden" id="defaultframework" name="defaultframework"
                               value="<?php echo $default_framework; ?>"/>
                        <input type="hidden" id="txtchktags" name="txtchktags" value="<?php echo $tag_value; ?>"/>
                        <input type="hidden" id="enabletracking1" name="enabletracking"
                               value="<?php echo $enabletracking; ?>"/>
                        <input type="hidden" id="enableanalytics" name="enableanalytics"
                               value="<?php echo $enableanalytics; ?>"/>
                        <input type="hidden" id="auto_delete" name="auto_delete" value="<?php echo $auto_delete; ?>"/>
                        <input type="hidden" id="txtchktracker" name="txtchktracker"
                               value="<?php echo $ch_tracker_value; ?>"/>
                        <input type="hidden" id="txtchkmatric" name="txtchkmatric"
                               value="<?php echo $matric_value; ?>"/>
                        <input type="hidden" id="txtchkvideos" name="txtchkvideos" value="<?php echo $video_value; ?>"/>
                        <input type="hidden" id="txtanalyticsduration" name="txtanalyticsduration"
                               value="<?php echo $duration_value; ?>"/>
                        <input type="hidden" id="txttrackingduration" name="txttrackingduration"
                               value="<?php echo $tracking_value; ?>"/>
                        <input type="hidden" id="txtembedlink" name="txtembedlink"
                               value='<?php echo stripslashes($external_embeded_link); ?>'/>
                        <button id="company_info_submit_button" type="submit" style="display: none;"></button>
                    </form>


                    <form accept-charset="UTF-8"
                          action="<?php echo $this->base . '/accounts/changeAccountOwner/' . $account_id . '/1' ?>"
                          enctype="multipart/form-data" method="POST">

                        <div class="field_box">
                            <label><?php echo $language_based_content['change_account_owner_all']  ; ?></label>
                            <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden"
                                                                                  value="&#x2713;"/></div>
                            <div class="input-group select size-full size_full_adjs inline">
                                <?php echo $this->Form->input('user_id', array('id' => 'user_user_id', 'options' => $allAccountOwner, 'selected' => $user_id, 'type' => 'select', 'label' => FALSE, 'div' => FALSE, 'required' => 'required')); ?>
                            </div>
                            <input id="account_owner_user_id" name="account_owner_user_id" type="hidden"
                                   value="<?php echo $user_id; ?>"/>
                            <input type="submit"
                                   style="background-color: transparent;text-decoration: underline;border: none;cursor: pointer;"
                                   class="blue-link underline inline" value="<?php echo $language_based_content['select_as_new_owner_all']; ?>">

                        </div>
                    </form>
                    <?php if ($myAccount['Account']['in_trial'] != '1') { ?>
                        <div class="field_box" style = "display:none;">
                            <p id="panalytics_duration" style="display:none;">
                                <label>Change Analytics Duration</label></p>
                            <div style="display:none;" id="divanalytics_duration"
                                 class="input-group select size-full inline">
                                <select name="analytics_duration" id="analytics_duration" required="required">
                                    <option value="01" <?php echo $duration_value == '01' ? 'selected=selected' : ''; ?>>
                                        Jan
                                    </option>
                                    <option value="02" <?php echo $duration_value == '02' ? 'selected=selected' : ''; ?>>
                                        Feb
                                    </option>
                                    <option value="03" <?php echo $duration_value == '03' ? 'selected=selected' : ''; ?>>
                                        Mar
                                    </option>
                                    <option value="04" <?php echo $duration_value == '04' ? 'selected=selected' : ''; ?>>
                                        Apr
                                    </option>
                                    <option value="05" <?php echo $duration_value == '05' ? 'selected=selected' : ''; ?>>
                                        May
                                    </option>
                                    <option value="06" <?php echo $duration_value == '06' ? 'selected=selected' : ''; ?>>
                                        Jun
                                    </option>
                                    <option value="07" <?php echo $duration_value == '07' ? 'selected=selected' : ''; ?>>
                                        Jul
                                    </option>
                                    <option value="08" <?php echo $duration_value == '08' ? 'selected=selected' : ''; ?>>
                                        Aug
                                    </option>
                                    <option value="09" <?php echo $duration_value == '09' ? 'selected=selected' : ''; ?>>
                                        Sep
                                    </option>
                                    <option value="10" <?php echo $duration_value == '10' ? 'selected=selected' : ''; ?>>
                                        Oct
                                    </option>
                                    <option value="11" <?php echo $duration_value == '11' ? 'selected=selected' : ''; ?>>
                                        Nov
                                    </option>
                                    <option value="12" <?php echo $duration_value == '12' ? 'selected=selected' : ''; ?>>
                                        Dec
                                    </option>
                                </select>
                            </div>

                        </div>
                    <?php } ?>

                    <div class="field_box">
                        <p id="cpduration" style="display:none;">
                            <label id="bduration">
                                <?php echo $language_based_content['tracker_feedback_duration_all']; ?>
                            </label></p>
                        <p id="apduration" style="display:none;">
                            <label id="b1duration"><?php echo $language_based_content['tracker_feedback_duration_all']; ?></label></p>
                        <div id="duration" style="display:none;" class="input-group select size-full inline">
                            <select name="tracking_duration" id="tracking_duration" required="required">
                                <option value="12" <?php echo $tracking_value == '12' ? 'selected=selected' : ''; ?>>12
                                    <?php echo $language_based_content['hours_all']; ?>
                                </option>
                                <option value="24" <?php echo $tracking_value == '24' ? 'selected=selected' : ''; ?>>24
                                    <?php echo $language_based_content['hours_all']; ?>
                                </option>
                                <option value="36" <?php echo $tracking_value == '36' ? 'selected=selected' : ''; ?>>36
                                    <?php echo $language_based_content['hours_all']; ?>
                                </option>
                                <option value="48" <?php echo $tracking_value == '48' ? 'selected=selected' : ''; ?>>48
                                    <?php echo $language_based_content['hours_all']; ?>
                                </option>
                                <option value="60" <?php echo $tracking_value == '60' ? 'selected=selected' : ''; ?>>60
                                    <?php echo $language_based_content['hours_all']; ?>
                                </option>
                                <option value="72" <?php echo $tracking_value == '72' ? 'selected=selected' : ''; ?>>72
                                    <?php echo $language_based_content['hours_all']; ?>
                                </option>
                                <option value="120" <?php echo $tracking_value == '120' ? 'selected=selected' : ''; ?>>5
                                    <?php echo $language_based_content['days_all']; ?>
                                </option>
                                <option value="168" <?php echo $tracking_value == '168' ? 'selected=selected' : ''; ?>>1
                                <?php echo $language_based_content['week_all']; ?>
                                </option>
                                <option value="336" <?php echo $tracking_value == '336' ? 'selected=selected' : ''; ?>>2
                                <?php echo $language_based_content['week_all']; ?>
                                </option>


                            </select>
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="field_box">
                        <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                            <?php if (!empty($frameworks) && count($frameworks) > 0): ?>

                                <label>
                                    <?php echo $language_based_content['default_framework_rubric_all']; ?>
                                </label>

                                <div class="input-group select size-full inline">
                                    <select name="frameworks" id="frameworks">
                                        <option value=""><?php echo $language_based_content['select_framework_all']; ?></option>
                                        <?php foreach ($frameworks as $framework) { ?>
                                            <option value="<?php echo $framework['AccountTag']['account_tag_id'] ?>" <?php echo $default_framework == $framework['AccountTag']['account_tag_id'] ? 'selected=selected' : ''; ?>><?php echo $framework['AccountTag']['tag_title'] ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                                <a style="color: #5a80a0 !important;margin-left:-3px; "
                                   class="blue-link underline inline"
                                   href="<?php echo $this->base . '/accounts/frameworks/' . $account_id ?>"><?php echo $language_based_content['change_framework_names_all']; ?></a>
                                <hr>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <div class="clear"></div>


                    <?php if ($is_embeded_enable == 1) { ?>

                        <div class="field_box" style="width:50%;">
                            <label>Region 4 Tutorial Embedded link</label>
                            <div class="input-group size-medium inline">
                                <textarea name="external_video_link" id="external_video_link" cols="50"
                                          rows="5"><?php echo stripslashes($external_embeded_link); ?></textarea>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <hr>
                    <?php } ?>
                    <?php if ($myAccount['Account']['in_trial'] != '1') { ?>
                        <div class="account_checkbox">
                            <?php if ($this->Custom->check_plan_permission($account_id) || $check_plan_deactivate == 1): ?>
                                <input type="checkbox" id="chkenablevideos"
                                       name="chkenablevideos" <?php echo $video_value == '1' ? 'checked' : ''; ?>><label
                                       for="chkenablevideos" style="padding-left: 7px;"><?php echo $language_based_content['enable_best_practices_lib_all']; ?></label>
                                <br><br>
                                <input type="checkbox" id="chkenableanalytics"
                                       name="enableanalytics" <?php echo $enableanalytics == '1' ? 'checked' : ''; ?>>
                                <label for="chkenableanalytics" style="padding-left: 7px;"><?php echo $language_based_content['enable_analytics_all']; ?></label>
                                <br><br>
                            <?php endif; ?>
                            <?php if (IS_QA): ?>
                                <input type="checkbox" id="chkauto_delete"
                                       name="auto_delete" <?php echo $auto_delete == '1' ? 'checked' : ''; ?>><label
                                       for="chkauto_delete" style="padding-left: 7px;"><?php echo $language_based_content['enable_auto_delete_all']; ?></label>
                            <?php endif; ?>
                            <br><br>
                            <input type="checkbox" id="chkenablewstags" name="chkenablewstags" <?php echo $chkenablewstags == '1' ? 'checked' : ''; ?>><label for="chkenablewstags" style="padding-left: 7px;">Enable custom Video Markers in Workspace</label>
                            <br><br>
                            <input type="checkbox" id="chkenabletags"
                                   name="chkenabletags" <?php echo $tag_value == '1' ? 'checked' : ''; ?>><label
                                   for="chkenabletags" style="padding-left: 7px;"><?php echo $language_based_content['create_custom_video_markers_all']; ?></label>
                                   <br><br>
                            <div class="clear"></div>
                            <div class="account_boxes span6" style="margin-left:0px;">
                                <div class="row-fluid row-marker" <?php echo $tag_value == '1' ? 'style="display:block;"' : 'style="display:none;"'; ?>>
                                    <div class="groups-table span12">

                                        <div class="span4 huddle-span4" style="margin-left:0px;">
                                            <div class="groups-table-header">
                                                <h2><?php echo $language_based_content['create_custom_markers_huddles_all']; ?></h2>
                                                <div style="clear:both"></div>
                                            </div>
                                            <div class="groups-table-content">
                                                <div class="widget-scrollable">
                                                    <div class="scrollbar" style="height: 155px;">
                                                        <div class="track" style="height: 155px;">
                                                            <div class="thumb" style="top: 0px; height: 90.3195px;">
                                                                <div class="end"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="viewport short">
                                                        <div class="overview" style="top: 0px;">
                                                            <div id="people-lists">
                                                                <ul id="list-containers">
                                                                    <?php
                                                                    for ($i = 0; $i < 4; $i++) {
                                                                        if (isset($tags[$i])) {
                                                                            ?>
                                                                            <li class="cledittags">
                                                                                <label class="huddle_permission_editor_row"
                                                                                       for="super_admin_ids_">
                                                                                    <span><?php echo $i + 1; ?>. </span>
                                                                                    <input class="super-user"
                                                                                           type="checkbox" value=""
                                                                                           name="tags_id[]"
                                                                                           id="super_admin_ids_"
                                                                                           style="display:none;">
                                                                                    <a style="color: #757575; font-weight: normal;"><?php echo $tags[$i]['AccountTag']['tag_title']; ?></a>
                                                                                    <input type="text" id="txtedittag"
                                                                                           name="txtedittag"
                                                                                           value="<?php echo $tags[$i]['AccountTag']['tag_title']; ?>"
                                                                                           style="display:none;"
                                                                                           class="txtedittag"/>
                                                                                    <input type="hidden" id="txttagid"
                                                                                           name="txttagid"
                                                                                           value="<?php echo $tags[$i]['AccountTag']['account_tag_id']; ?>"
                                                                                           class="txttagid"/>
                                                                                </label>
                                                                                <span style="float: right;margin-right: 0px;cursor: pointer"
                                                                                      class="clearTag"><img
                                                                                        src="<?php echo $this->webroot . 'app/img/remove_icon.png' ?>"/></span>
                                                                            </li>

                                                                            <?php
                                                                        } else {
                                                                            ?>
                                                                            <li class="cledittags"><label
                                                                                    class="huddle_permission_editor_row"
                                                                                    for="super_admin_ids_">
                                                                                    <span><?php echo $i + 1; ?>. </span>
                                                                                    <input class="super-user"
                                                                                           type="checkbox" value=""
                                                                                           name="tags_id[]"
                                                                                           id="super_admin_ids_"
                                                                                           style="display:none;">
                                                                                    <a style="color: #757575; font-weight: normal;"><?php echo $language_based_content['add_tag_all']; ?></a>
                                                                                    <input type="text" id="txtedittag"
                                                                                           name="txtedittag" value=""
                                                                                           style="display:none;"
                                                                                           class="txtedittag"/>
                                                                                    <input type="hidden" id="txttagid"
                                                                                           name="txttagid"
                                                                                           value="nw_<?php echo $i; ?>"
                                                                                           class="txttagid"/>
                                                                                </label>
                                                                                <span style="float: right;margin-right: 0px;cursor: pointer"
                                                                                      class="clearTag"><img
                                                                                        src="<?php echo $this->webroot . 'app/img/remove_icon.png' ?>"/></span>
                                                                            </li>

                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="clear"></div>
                            <br>
                            <?php if ($this->Custom->check_plan_permission($account_id) || $check_plan_deactivate == 1): ?>
                                <?php //if (isset($standards) && count($standards) > 0) { ?>
                                <input type="checkbox" id="chkenableframework"
                                       name="chkenableframework" <?php echo $framwork_value == '1' ? 'checked' : ''; ?>>
                                <label for="chkenableframework" style="padding-left: 7px;"><?php echo $language_based_content['enable_rubric_in_account_all']; ?></label>
                                <br><br>
                                <input type="checkbox" id="chkenablewsframework" name="chkenablewsframework" <?php echo $chkenablewsframework == '1' ? 'checked' : ''; ?>><label for="chkenablewsframework" style="padding-left: 7px;">Enable Frameworks in Workspace</label>
                                <br><br>
                                <?php //} else { ?>
                                <!-- Are you interested in adding a custom framework to your account?  If so, contact <a
                                    href="mailto:info@sibme.com">info@sibme.com</a><br><br> -->
                                <?php //} ?>

                                <input style ="display:none;" type="checkbox" id="enabletracking" name="enabletracking1" checked>
                                <input type="checkbox" id="chkenabletracker"
                                                     name="chkenabletracker" <?php echo $ch_tracker_value == '1' ? 'checked' : ''; ?>>
                                <label id="labelchkenabletracker" for="chkenabletracker" style="padding-left: 7px;"><?php echo $language_based_content['enable_coaching_all']; ?></label>
                                <br><br>
                                <?php if ($this->Custom->check_if_eval_huddle_active($account_id)): ?>
                                 <input type="checkbox" id="chkenablematric"
                                                         name="chkenabletracker" <?php echo $matric_value == '1' ? 'checked' : ''; ?>>
                                    <label id="labelchkenablematric" for="chkenablematric" style="padding-left: 7px;"><?php echo $language_based_content['enable_assessment_all']; ?></label>
                                <?php endif; ?>
                                <div class="clear"></div>
                                <?php if (isset($metrics) && !empty($metrics) && $tagged_standards_count > 0) { ?>
                                    <div class="account_boxes span6" style="margin-left:0px;">
                                        <div class="row-fluid row-metric" <?php echo $matric_value == '1' ? 'style="display:block;"' : 'style="display:none;"'; ?>>
                                            <div class="groups-table span12">

                                                <div class="span4 huddle-span4" style="margin-left:0px;">
                                                    <div class="groups-table-header">
                                                        <h2><?php echo $language_based_content['create_assessment_ratings_all']; ?></h2>
                                                        <div style="clear:both"></div>
                                                    </div>
                                                    <div class="groups-table-content">
                                                        <div class="widget-scrollable">
                                                            <div class="scrollbar" style="height: 155px;">
                                                                <div class="track" style="height: 155px;">
                                                                    <div class="thumb" style="top: 0px; height: 90.3195px;">
                                                                        <div class="end"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="viewport short">
                                                                <div class="overview" style="top: 0px;">
                                                                    <div id="people-lists">
                                                                        <ul id="list-containers">
                                                                            <?php
                                                                            for ($i = 0; $i < 5; $i++) {
                                                                                if (isset($metrics[$i])) {
                                                                                    $str = substr($metrics[$i]['AccountMetaData']['meta_data_name'], 13);
                                                                                    ?>
                                                                                    <li class="cleditmetrics">
                                                                                        <label class="huddle_permission_editor_row"
                                                                                               for="super_admin_ids_">
                                                                                            <span><?php echo $i + 1; ?>
                                                                                                . </span>
                                                                                            <input class="super-user"
                                                                                                   type="checkbox" value=""
                                                                                                   name="metric_id[]"
                                                                                                   id="super_admin_ids_"
                                                                                                   style="display:none;">
                                                                                            <a style="color: #757575; font-weight: normal;"><?php echo $str; ?></a>
                                                                                            <input type="text"
                                                                                                   id="txteditmetric"
                                                                                                   data-attr-val='<?php echo $metrics[$i]['AccountMetaData']['account_meta_data_id'] ?>'
                                                                                                   data-attr='old'
                                                                                                   name="metrics[][nw_<?php echo $i + 1; ?>]"
                                                                                                   value="<?php echo $str; ?>"
                                                                                                   style="display:none;"
                                                                                                   class="txteditmetric"/>
                                                                                            <input type="hidden"
                                                                                                   id="txtmetricid"
                                                                                                   name="metrics1[][nw_<?php echo $i + 1; ?>]"
                                                                                                   value="nw_<?php echo $i + 1; ?>"
                                                                                                   key="<?php echo $str; ?>"
                                                                                                   class="txtmetricid"/>
                                                                                        </label>
                                                                                        <span style="float: right;margin-right: 0px;cursor: pointer"
                                                                                              class="clearMetric"><img
                                                                                                src="<?php echo $this->webroot . 'app/img/remove_icon.png' ?>"/></span>
                                                                                    </li>

                                                                                    <?php
                                                                                } else {
                                                                                    ?>
                                                                                    <li class="cleditmetrics">

                                                                                        <label class="huddle_permission_editor_row"
                                                                                               for="super_admin_ids_">
                                                                                            <span><?php echo $i + 1; ?>
                                                                                                . </span>
                                                                                            <input class="super-user"
                                                                                                   type="checkbox" value=""
                                                                                                   name="metric_id[]"
                                                                                                   id="super_admin_ids_"
                                                                                                   style="display:none;">
                                                                                            <a style="color: #757575; font-weight: normal;"><?php echo $language_based_content['add_rating_all']; ?></a>
                                                                                            <input type="text"
                                                                                                   id="txteditmetric"
                                                                                                   data-attr='new'
                                                                                                   name="txteditmetric"
                                                                                                   value=""
                                                                                                   style="display:none;"
                                                                                                   class="txteditmetric"/>
                                                                                            <input type="hidden"
                                                                                                   id="txtmetricid"
                                                                                                   name="txtmetricid"
                                                                                                   value="new_<?php echo $i + 1; ?>"
                                                                                                   class="txtmetricid"/>
                                                                                        </label>
                                                                                        <span style="float: right;margin-right: 0px;cursor: pointer"
                                                                                              class="clearMetric"><img
                                                                                                src="<?php echo $this->webroot . 'app/img/remove_icon.png' ?>"/></span>
                                                                                    </li>

                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php endif; ?>

                        </div>
                    <?php } ?>
                    <div class="clear"></div>
                    <?php if ($this->Custom->check_plan_permission($account_id) || $check_plan_deactivate == 1): ?>
                        <?php if ($myAccount['Account']['in_trial'] != '1') { ?>
                            <div class="account_user_privileges">
                                <br>
                                <h2><?php echo $language_based_content['account_user_privileges_all']; ?></h2>

                                <?php if (count($account_users) > 0): ?>

                                    <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓">
                                    </div>
                                    <div class="input-group styled-input-group <?php //echo $disabled_class2;                                                                                       ?>"
                                         style="margin: 0px 0px 4px 5px;" id="save-user-permissions">
                                        <label><span
                                                class="checkbox-substitute"><input <?php echo ($folders_check == 1) ? 'checked="checked"' : '' ?>
                                                    class="checkbox" id="folders_check" name="folders_check" type="checkbox"
                                                    value="1"></span><?php echo $language_based_content['users_can_create_huddle_folders_all']; ?></label>
                                        <label><span
                                                class="checkbox-substitute"><input <?php echo ($manage_collab_huddles == 1) ? 'checked="checked"' : '' ?>
                                                    class="checkbox" id="manage_collab_huddles" name="manage_collab_huddles"
                                                    type="checkbox" value="1"></span><?php echo $language_based_content['users_can_create_mange_collab_all']; ?></label>
                                        <label class=""><span
                                                class="checkbox-substitute"><input <?php echo ($permission_access_video_library == 1) ? 'checked="checked"' : '' ?>
                                                    class="checkbox" id="permission_access_video_library"
                                                    name="permission_access_video_library" type="checkbox" value="1"></span><?php echo $language_based_content['users_can_access_video_lib_all']; ?></label>
                                        <label class=""><span class="checkbox-substitute"><input
                                                    <?php echo ($parmission_access_my_workspace == 1) ? 'checked="checked"' : '' ?>class="checkbox"
                                                    id="parmission_access_my_workspace" name="parmission_access_my_workspace"
                                                    type="checkbox" value="1"></span><?php echo $language_based_content['users_can_access_private_workspace_all']; ?></label>
                                        <label class=""><span class="checkbox-substitute"><input
                                                    <?php echo ($permission_video_library_upload == 1) ? 'checked="checked"' : '' ?>class="checkbox"
                                                    id="permission_video_library_upload" name="permission_video_library_upload"
                                                    type="checkbox" value="1"></span><?php echo $language_based_content['users_can_upload_vid_res_all']; ?></label>
                                        <label class=""><span class="checkbox-substitute"><input
                                                    <?php echo ($view_analytics == 1) ? 'checked="checked"' : '' ?>class="checkbox"
                                                    id="view_analytics" name="view_analytics" type="checkbox" value="1"></span><?php echo $language_based_content['users_can_access_analytics_dash_all']; ?></label>

                                        <label class=""><span class="checkbox-substitute"><input
                                                     <?php echo ($start_synced_scripted_notes == 1) ? 'checked="checked"' : '' ?>class="checkbox"
                                                     id="start_synced_scripted_notes" name="start_synced_scripted_notes" type="checkbox" value="1"></span><?php echo $language_based_content['users_can_start_synced_scripted_notes']; ?></label>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <!--<label class="" style="display: none;"><span class="checkbox-substitute"><input  <?php //echo ($permission_administrator_user_new_role == 1) ? 'checked="checked"' : ''                                                                                       ?>class="checkbox" id="permission_administrator_user_new_role" name="permission_administrator_user_new_role" type="checkbox" value="1"></span> Users can manage users in the account </label>-->
                                    </div>
                                    <input id="user_id" name="user_account_id" type="hidden"
                                           value="<?php //echo $users['UserAccount']['id'];                                                                                              ?>">


                                    <script type="text/javascript">
                                        $(document).ready(function (e) {
                                            $('#save-user-permissions').click(function (e) {
                                                var folders_check;
                                                var manage_coach_huddle;
                                                var manage_collab_huddles;
                                                var manage_evaluation_huddles;
                                                var permission_access_video_library;
                                                var parmission_access_my_workspace;
                                                var permission_video_library_upload;
                                                var permission_administrator_user_new_role;
                                                var view_analytics;
                                                var start_synced_scripted_notes;
                                                if ($("#folders_check").is(':checked')) {
                                                    folders_check = 1;
                                                } else {
                                                    folders_check = 0;
                                                }
                                                if ($("#manage_coach_huddles").is(':checked')) {
                                                    manage_coach_huddle = 1;
                                                } else {
                                                    manage_coach_huddle = 0;
                                                }
                                                if ($("#manage_collab_huddles").is(':checked')) {
                                                    manage_collab_huddles = 1;
                                                } else {
                                                    manage_collab_huddles = 0;
                                                }
                                                if ($("#manage_evaluation_huddles").is(':checked')) {
                                                    manage_evaluation_huddles = 1;
                                                } else {
                                                    manage_evaluation_huddles = 0;
                                                }
                                                if ($("#permission_access_video_library").is(':checked')) {
                                                    permission_access_video_library = 1;
                                                } else {
                                                    permission_access_video_library = 0;
                                                }
                                                if ($("#parmission_access_my_workspace").is(':checked')) {
                                                    parmission_access_my_workspace = 1;
                                                } else {
                                                    parmission_access_my_workspace = 0;
                                                }
                                                if ($("#permission_video_library_upload").is(':checked')) {
                                                    permission_video_library_upload = 1;
                                                } else {
                                                    permission_video_library_upload = 0;
                                                }
                                                if ($("#permission_administrator_user_new_role").is(':checked')) {
                                                    permission_administrator_user_new_role = 1;
                                                } else {
                                                    permission_administrator_user_new_role = 0;
                                                }

                                                if ($("#view_analytics").is(':checked')) {
                                                    view_analytics = 1;
                                                } else {
                                                    view_analytics = 0;
                                                }

                                                if ($("#start_synced_scripted_notes").is(':checked')) {
                                                    start_synced_scripted_notes = 1;
                                                } else {
                                                    start_synced_scripted_notes = 0;
                                                }


                                                $.ajax({
                                                    url: '<?php echo $this->base . '/permissions/users_extra_privileges/' . $account_id ?>',
                                                    data: {
                                                        folders_check: folders_check,
                                                        manage_coach_huddles: manage_coach_huddle,
                                                        manage_collab_huddles: manage_collab_huddles,
                                                        manage_evaluation_huddles: manage_evaluation_huddles,
                                                        permission_access_video_library: permission_access_video_library,
                                                        parmission_access_my_workspace: parmission_access_my_workspace,
                                                        permission_video_library_upload: permission_video_library_upload,
                                                        permission_administrator_user_new_role: permission_administrator_user_new_role,
                                                        view_analytics: view_analytics,
                                                        start_synced_scripted_notes: start_synced_scripted_notes,
                                                    },
                                                    type: "POST",
                                                    dataType: 'json',
                                                    success: function (e) {

                                                    }
                                                })


                                            })
                                        });
                                    </script>
                                <?php else: ?>
                                    No Users in Account
                                <?php endif; ?>
                            </div><!-- End Div of Users Privileges -->





                            <div class="account_admin_privileges">
                                <br>
                                <h2><?php echo $language_based_content['account_admin_privileges_all']; ?></h2>

                                <?php if (count($account_admins) > 0): ?>

                                    <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓">
                                    </div>
                                    <div class="input-group styled-input-group <?php //echo $disabled_class2;                                                                                       ?>"
                                         style="margin: 0px 0px 4px 5px;" id="save-admin-permissions">
                                        <label class=""><span class="checkbox-substitute"><input
                                                    <?php echo ($admin_permission_video_library_upload == 1) ? ' checked="checked" ' : '' ?> class="checkbox"
                                                    id="admin_permission_video_library_upload" name="admin_permission_video_library_upload"
                                                    type="checkbox" value="1"></span><?php echo $language_based_content['admins_can_upload_vid_res_all']; ?></label>
                                        <label class=""><span class="checkbox-substitute"><input
                                                    <?php echo ($admin_view_analytics == 1) ? ' checked="checked" ' : '' ?> class="checkbox"
                                                    id="admin_view_analytics" name="admin_view_analytics" type="checkbox" value="1"></span><?php echo $language_based_content['admins_can_view_wide_analytics_all']; ?></label>

                                        <label class=""><span class="checkbox-substitute"><input
                                                    <?php echo ($admin_permission_administrator_user_new_role == 1) ? 'checked="checked"' : '' ?>class="checkbox" id="admin_permission_administrator_user_new_role" name="admin_permission_administrator_user_new_role" type="checkbox" value="1"></span><?php echo $language_based_content['admins_can_manage_users_in_account_all']; ?></label>

                                    </div>
                                    <input id="user_id" name="user_account_id" type="hidden"
                                           value="<?php //echo $users['UserAccount']['id'];                                                                                              ?>">


                                    <script type="text/javascript">
                                        $(document).ready(function (e) {
                                            $('#save-admin-permissions input[type="checkbox"]').click(function (e) {

                                                var admin_permission_video_library_upload;
                                                var admin_permission_administrator_user_new_role;
                                                var admin_view_analytics;

                                                if ($("#admin_permission_video_library_upload").is(':checked')) {
                                                    admin_permission_video_library_upload = 1;
                                                } else {
                                                    admin_permission_video_library_upload = 0;
                                                }
                                                if ($("#admin_permission_administrator_user_new_role").is(':checked')) {
                                                    admin_permission_administrator_user_new_role = 1;
                                                } else {
                                                    admin_permission_administrator_user_new_role = 0;
                                                }
                                                if ($("#admin_view_analytics").is(':checked')) {
                                                    admin_view_analytics = 1;
                                                } else {
                                                    admin_view_analytics = 0;
                                                }


                                                $.ajax({
                                                    url: '<?php echo $this->base . '/permissions/users_extra_privileges/' . $account_id ?>',
                                                    data: {
                                                        admin_post: 1,
                                                        admin_permission_video_library_upload: admin_permission_video_library_upload,
                                                        admin_permission_administrator_user_new_role: admin_permission_administrator_user_new_role,
                                                        admin_view_analytics: admin_view_analytics
                                                    },
                                                    type: "POST",
                                                    dataType: 'json',
                                                    success: function (e) {

                                                    }
                                                })


                                            })
                                        });
                                    </script>
                                <?php else: ?>
                                    <?php echo $language_based_content['no_admins_in_account']; ?>
                                <?php endif; ?>
                            </div><!-- End Div of Admin Privileges -->


                            <?php
                        }
                    endif;
                    ?>
                    </form>
                    <div class="clear"></div>
                    <div style="margin:0;padding:0;display:inline">
                        <input name="utf8" type="hidden" value="&#x2713;"/>
                        <input name="_method" type="hidden" value="put"/>
                    </div>
                    <div class="clear"></div>
                    <?php if ($myAccount['Account']['in_trial'] == '1'): ?>
                        <div class="schedule_demoBox">
                            <div class="lock_box"><img src="<?php echo $this->webroot . 'app/img/lock.png' ?>"
                                                       width="37" height="50" alt=""/></div>
                            <div>
                                <p>Interested in learning about <?php echo $this->Custom->get_site_settings('site_title') ?> account analytics, frameworks/rubrics, coaching
                                    and assessment trackers, and the best practices video library. To upgrade, please
                                    <a data-toggle="modal" data-target="#email_ob" style="cursor:pointer;">contact
                                        us</a> to schedule a demo!</p>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($myAccount['Account']['in_trial'] != '1') { ?>
                        <div class="settings_bottom_btn">
                            <button id="account_tab1_button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button" class="setting_greenBtn"><?php echo $language_based_content['save_changes_as_all']; ?></button>
                        </div>
                    <?php } ?>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('#btnAddToAccount_addHuddle').click(function () {
                                addToInviteList();
                            });
                            $("#account_tab1_button").click(function () {
                                var count = 0;
                                var addcount = 0;
                                var edittag = 0;
                                var count12 = 0;
                                $('#txtembedlink').val($('#external_video_link').val());
                                if ($("#chkenabletags").is(':checked')) {
                                    $('.txttagid').each(function (value) {
                                        var input_value = $(this).val();
                                        if (input_value.search('nw') >= 0) {
                                            count++;
                                            var new_tag_val = $('#txtedittag_' + input_value).val();
                                            if (typeof new_tag_val !== "undefined" && typeof new_tag_val !== null && new_tag_val.length > 0) {
                                                addcount++;
                                            }
                                        }
                                        else {
                                            if (input_value.length > 0 && $(this).parent().find("#txtedittag").val().length == 0) {
                                                edittag++;
                                            }
                                        }

                                    });

                                }
                                if ($("#chkenablematric").is(':checked')) {
                                    cnt = 1;
                                    $('.txteditmetric').each(function (value) {
                                        tag_name1 = $(this).val();
                                        tag_attr = $(this).attr('data-attr');
                                        if (tag_attr == 'new') {
                                            add_html_form = '<input type="hidden" name="metrics[][new_' + cnt + ']" value="' + tag_name1 + '" id="txteditmetric_new_' + cnt + '" />';
                                        } else {
                                            var account_meta_data_id = $(this).attr('data-attr-val');
                                            add_html_form = '<input type="hidden" name="metrics[][' + account_meta_data_id + ']" value="' + tag_name1 + '" id="txteditmetric_nw_' + cnt + '" />';
                                        }

                                        $('#edit_account_116').append(add_html_form);
                                        cnt++;
                                    });
                                    $('.txtmetricid').each(function (value) {
                                        var input_value = $(this).val();
                                        if (input_value.search('nw') >= 0) {
                                            count++;
                                            var new_tag_val = $('#txteditmetric_' + input_value).val();
                                            if (typeof new_tag_val !== "undefined" && typeof new_tag_val !== null && new_tag_val.length > 0) {
                                                addcount++;
                                            }
                                        }
                                        else {
                                            if (input_value.length > 0 && $(this).parent().find("#txteditmetric").val().length == 0) {
                                                edittag++;
                                            }
                                        }

                                    });

                                }
                                $("#company_info_submit_button").click();

                            });
                            $('#enabletracking').on('change', function () {
                                if ($(this).is(':checked')) {
                                    $('#enabletracking1').val('1');
                                    $('#chkenabletracker').show();
                                    $('#chkenablematric').show();
                                    $('#labelchkenabletracker').show();
                                    $('#labelchkenablematric').show();
                                    $('#cpduration').show();
                                    $('#duration').show();
                                    $('#hrduration').show();
                                    //  $('#apduration').show();

                                    if ($('#chkenabletracker').is(':checked')) {
                                        //  $('#txtchkmatric').val('0');
                                        $('#txtchktracker').val('1');
                                        //  $('.row-metric').hide();
                                        $("#hrduration").show();
                                        $("#apduration").hide();
                                        $("#cpduration").show();
                                        $("#duration").show();

                                    } else {
                                        $('#txtchktracker').val('0');
                                    }

                                    if ($('#chkenablematric').is(':checked')) {
                                        $('.row-metric').show();
                                        // $('#txtchktracker').val('0');
                                        $('#txtchkmatric').val('1');
                                        $("#hrduration").show();
                                        $("#cpduration").hide();
                                        $("#apduration").show();
                                        $("#duration").show();
                                        //$('#chkenabletags').attr('checked', false);
                                        //$('.row-marker').hide();
                                        //$('#txtchktags').val('0');
                                        $('.widget-scrollable').tinyscrollbar();
                                    } else {
                                        $('#txtchkmatric').val('0');
                                        $('.row-metric').hide();
                                    }


                                } else {
                                    $('#enabletracking1').val('0');
                                    $('#txtchkmatric').val('0');
                                    $('.row-metric').hide();
                                    $('#txtchktracker').val('0');
                                    $('#chkenabletracker').hide();
                                    $('#chkenablematric').hide();
                                    $('#labelchkenabletracker').hide();
                                    $('#labelchkenablematric').hide();
                                    $('#cpduration').hide();
                                    $('#duration').hide();
                                    $('#hrduration').hide();
                                    $('#apduration').hide();
                                }
                            });
                            if ($("#enabletracking").is(':checked')) {
                                $('#enabletracking1').val('1');
                                $('#chkenabletracker').show();
                                $('#chkenablematric').show();
                                $('#labelchkenabletracker').show();
                                $('#labelchkenablematric').show();
                                $('#cpduration').show();
                                $('#duration').show();
                                $('#hrduration').show();
                                //     $('#apduration').show();

                                if ($('#chkenabletracker').is(':checked')) {
                                    //   $('#txtchkmatric').val('0');
                                    $('#txtchktracker').val('1');
                                    //   $('.row-metric').hide();
                                    $("#hrduration").show();
                                    $("#apduration").hide();
                                    $("#cpduration").show();
                                    $("#duration").show();

                                } else {
                                    $('#txtchktracker').val('0');
                                }

                                if ($('#chkenablematric').is(':checked')) {
                                    $('.row-metric').show();
                                    //   $('#txtchktracker').val('0');
                                    $('#txtchkmatric').val('1');
                                    $("#hrduration").show();
                                    $("#cpduration").hide();
                                    $("#apduration").show();
                                    $("#duration").show();
                                    // $('#chkenabletags').attr('checked', false);
                                    //$('.row-marker').hide();
                                    //$('#txtchktags').val('0');
                                    $('.widget-scrollable').tinyscrollbar();
                                } else {
                                    $('#txtchkmatric').val('0');
                                    $('.row-metric').hide();
                                }


                            } else {
                                $('#enabletracking1').val('0');
                                $('#txtchkmatric').val('0');
                                $('.row-metric').hide();
                                $('#txtchktracker').val('0');
                                $('#chkenabletracker').hide();
                                $('#chkenablematric').hide();
                                $('#labelchkenabletracker').hide();
                                $('#labelchkenablematric').hide();
                                $('#cpduration').hide();
                                $('#duration').hide();
                                $('#hrduration').hide();
                                $('#apduration').hide();
                            }
                            $('#chkenablevideos').on('change', function () {
                                var status_value = '0';
                                if ($(this).is(':checked')) {
                                    $('#txtchkvideos').val('1');
                                    $('.schedule_demoBox').hide();
                                } else {
                                    $('#txtchkvideos').val('0');
                                    $('.schedule_demoBox').show();
                                }
                            });
                            $('#enabletracking').on('change', function () {
                                var status_value = '0';
                                if ($(this).is(':checked')) {
                                    $('#txtchkvideos').val('1');
                                    $('.schedule_demoBox').hide();
                                } else {
                                    $('#txtchkvideos').val('0');
                                    $('.schedule_demoBox').show();
                                }
                            });


                            $('#chkenableframework').on('change', function () {
                                var status_value = '0';
                                if ($(this).is(':checked')) {
                                    $('#txtchkframework').val('1');
                                } else {
                                    $('#txtchkframework').val('0');
                                }
                            });
                            $('#chkenablewstags').on('change', function () {
                                var status_value = '0';
                                if ($(this).is(':checked')) {
                                    $('#txtchkenablewstags').val('1');
                                } else {
                                    $('#txtchkenablewstags').val('0');
                                }
                            });
                            $('#chkenablewsframework').on('change', function () {
                                var status_value = '0';
                                if ($(this).is(':checked')) {
                                    $('#txtchkenablewsframework').val('1');
                                } else {
                                    $('#txtchkenablewsframework').val('0');
                                }
                            });
                            $('#analytics_duration').on('change', function () {
                                $('#txtanalyticsduration').val($('#analytics_duration').val());
                            });
                            $('#tracking_duration').on('change', function () {
                                $('#txttrackingduration').val($('#tracking_duration').val());
                            });
                            $('#frameworks').on('change', function () {
                                $('#defaultframework').val($('#frameworks').val());
                            });
                            $('#chkenabletags').on('change', function () {
                                if ($(this).is(':checked')) {
                                    $('#txtchktags').val('1');
                                    $('.row-marker').show();
                                    $('.widget-scrollable').tinyscrollbar();
                                } else {
                                    $('#txtchktags').val('0');
                                    $('.row-marker').hide();
                                }
                            });
                            $('#chkenablematric').click(function () {
                                if ($(this).is(':checked')) {
                                    $('.row-metric').show();
                                    //   $('#txtchktracker').val('0');
                                    $('#txtchkmatric').val('1');
                                    $("#hrduration").show();
                                    $("#cpduration").hide();
                                    $("#apduration").show();
                                    $("#duration").show();
                                    // $('#chkenabletags').attr('checked', false);
                                    //$('.row-marker').hide();
                                    //$('#txtchktags').val('0');
                                    $('.widget-scrollable').tinyscrollbar();
                                } else {
                                    $('#txtchkmatric').val('0');
                                    $('.row-metric').hide();
                                }
                            });
                            $('#chkenabletracker').click(function () {
                                var status_value = '0';
                                if ($(this).is(':checked')) {
                                    //   $('#txtchkmatric').val('0');
                                    $('#txtchktracker').val('1');
                                    //   $('.row-metric').hide();
                                    $("#hrduration").show();
                                    $("#apduration").hide();
                                    $("#cpduration").show();
                                    $("#duration").show();


                                } else {
                                    $('#txtchktracker').val('0');
                                }
                            });

                            if ($('#chkenabletracker').is(':checked')) {
                                //   $('#txtchkmatric').val('0');
                                $('#txtchktracker').val('1');
                                // $('.row-metric').hide();
                                $("#hrduration").show();
                                $("#apduration").hide();
                                if ($("#enabletracking").is(':checked')) {
                                    $("#cpduration").show();
                                    $("#duration").show();
                                }

                            } else {
                                $('#txtchktracker').val('0');
                            }

                            if ($('#chkenablematric').is(':checked')) {
                                $('.row-metric').show();
                                //    $('#txtchktracker').val('0');
                                $('#txtchkmatric').val('1');
                                $("#hrduration").show();
                                $("#cpduration").hide();
                                $("#apduration").show();
                                $("#duration").show();
                                // $('#chkenabletags').attr('checked', false);
                                //$('.row-marker').hide();
                                //$('#txtchktags').val('0');
                                $('.widget-scrollable').tinyscrollbar();
                            } else {
                                $('#txtchkmatric').val('0');
                                $('.row-metric').hide();
                            }


                            $('#pop-up-btn').click(function () {
                                $('.fmargin-left').attr('disabled', false);
                            });

                            $('.cledittags').on('click', function (e) {
                                e.preventDefault();
                                $(this).find('a').hide();
                                $(this).find('.txtedittag').show();
                                $(this).find('.txtedittag').focus();
                            });
                            $('.cleditmetrics').on('click', function (e) {
                                e.preventDefault();
                                $(this).find('a').hide();
                                $(this).find('.txteditmetric').show();
                                $(this).find('.txteditmetric').focus();
                            });
                            $('.clearTag').on('click', function (e) {
                                e.preventDefault();
                                $(this).parent().find('a').hide();
                                $(this).parent().find('.txtedittag').show();
                                $(this).parent().find('.txtedittag').val('');
                                $(this).parent().find('.txtedittag').focus();

                            });
                            $('.clearMetric').on('click', function (e) {
                                e.preventDefault();
                                $(this).parent().find('a').hide();
                                $(this).parent().find('.txteditmetric').show();
                                $(this).parent().find('.txteditmetric').val('');
                                $(this).parent().find('.txteditmetric').focus();

                            });
                            $('.txtedittag').on('keyup', function (e) {
                                var code = e.which;
                                var add_html_form = '';
                                var tag_name = '';
                                var key = '';
                                if (code == 13) {
                                    tag_name = $(this).val();
                                    $(this).hide();
                                    $(this).parent().find('a').text(tag_name);
                                    $(this).parent().find('a').show();
                                    key = $(this).parent().find('.txttagid').val();
                                    if (!$('#txtedittag_' + key).is('*')) {
                                        add_html_form = '<input type="hidden" name="tags[][' + key + ']" value="' + tag_name + '" id="txtedittag_' + key + '" />';
                                        $('#edit_account_116').append(add_html_form);
                                    }
                                    else {
                                        $('#txtedittag_' + key).val(tag_name);
                                    }
                                }
                            });
                            $('.txteditmetric').on('keyup', function (e) {
                                var code = e.which;
                                var add_html_form = '';
                                var tag_name = '';
                                var key = '';
                                if (code == 13) {
                                    tag_name = $(this).val();
                                    $(this).hide();
                                    $(this).parent().find('a').text(tag_name);
                                    $(this).parent().find('a').show();
                                    key = $(this).parent().find('.txtmetricid').val();
                                    if (!$('#txteditmetric_' + key).is('*')) {
                                        //add_html_form = '<input type="hidden" name="metrics[][' + key + ']" value="' + tag_name + '" id="txteditmetric_' + key + '" />';
                                        //$('#edit_account_116').append(add_html_form);
                                    }
                                    else {
                                        $('#txteditmetric_' + key).val(tag_name);
                                    }
                                }
                            });
                            $('.txtedittag').on('focusout', function (e) {
                                var code = e.which;
                                var add_html_form = '';
                                var tag_name = '';
                                var key = '';
                                tag_name = $(this).val();
                                $(this).hide();
                                $(this).parent().find('a').text(tag_name);
                                $(this).parent().find('a').show();
                                key = $(this).parent().find('.txttagid').val();

                                if (!$('#txtedittag_' + key).is('*')) {
                                    add_html_form = '<input type="hidden" name="tags[][' + key + ']" value="' + tag_name + '" id="txtedittag_' + key + '" />';
                                    $('#edit_account_116').append(add_html_form);
                                }
                                else {
                                    $('#txtedittag_' + key).val(tag_name);
                                }
                            });
                            $('.txteditmetric').on('focusout', function (e) {
                                var code = e.which;
                                var add_html_form = '';
                                var tag_name = '';
                                var key = '';
                                tag_name = $(this).val();
                                $(this).hide();
                                $(this).parent().find('a').text(tag_name);
                                $(this).parent().find('a').show();
                                key = $(this).parent().find('.txtmetricid').val();
                                if (!$('#txteditmetric_' + key).is('*')) {
                                    //add_html_form = '<input type="hidden" name="metrics[][' + key + ']" value="' + tag_name + '" id="txtmetrictag_' + key + '" />';
                                    //$('#edit_account_116').append(add_html_form);
                                }
                                else {
                                    $('#txteditmetric_' + key).val(tag_name);
                                }
                            });

                            $('#chkenableanalytics').on('change', function () {
                                if ($(this).is(':checked')) {
                                    $('#enableanalytics').val('1');
                                    $('#panalytics_duration').show();
                                    $('#divanalytics_duration').show();
                                    $('.schedule_demoBox').hide();


                                }
                                else {

                                    $('#enableanalytics').val('0');
                                    $('#panalytics_duration').hide();
                                    $('#divanalytics_duration').hide();
                                    $('.schedule_demoBox').show();

                                }
                            });


                            $('#chkauto_delete').on('change', function () {
                                if ($(this).is(':checked')) {
                                    $('#auto_delete').val('1');
                                }
                                else {

                                    $('#auto_delete').val('0');
                                }
                            });

                            if ($('#chkenableanalytics').is(':checked')) {
                                $('#enableanalytics').val('1');
                                $('#panalytics_duration').show();
                                $('#divanalytics_duration').show();
                                $('.schedule_demoBox').hide();

                            }
                            else {

                                $('#enableanalytics').val('0');
                                $('#panalytics_duration').hide();
                                $('#divanalytics_duration').hide();
                                $('.schedule_demoBox').show();

                            }

                            if ($('#chkenablevideos').is(':checked')) {
                                $('#txtchkvideos').val('1');
                                $('.schedule_demoBox').hide();
                            } else {
                                $('#txtchkvideos').val('0');
                                $('.schedule_demoBox').show();
                            }


                        });
                        $(function () {
                            $("#trial_created_at").datepicker();
                        });

                        function addToInviteList() {
                            var lposted_data = [];
                            var userNames = document.getElementsByName('users[][name]');

                            for (i = 0; i < userNames.length; i++) {
                                var userName = $(userNames[i]);
                                if (userName.val() != '' != '') {
                                    if (userName.val() == '') {
                                        alert('<?php echo $alert_messages['please_enter_valid_full_name']; ?>');
                                        userName.focus();
                                        return false;
                                    }

                                    var newUser = [];
                                    newUser[newUser.length] = '';
                                    newUser[newUser.length] = userName.val();
                                    lposted_data[lposted_data.length] = newUser;
                                }
                            }

                            if (lposted_data.length == 0) {
                                alert('<?php echo $alert_messages['please_enter_atleast_full_name']; ?>');
                                return;
                            }
                            $('#flashMessage2').css('display', 'none');

                            new_ids = 1;

                            for (var i = 0; i < lposted_data.length; i++) {

                                var data = lposted_data[i];
                                var html = '';

                                html += '<li>';
                                html += '<label class="huddle_permission_editor_row" for="super_admin_ids_' + new_ids + '"><input type="checkbox" style="display:none;" value="' + new_ids + '" name="super_admin_ids[]" id="super_admin_ids_' + new_ids + '"> ' + data[1] + ' </label>';

                                html += '</li>';
                                $('.groups-table .huddle-span4 .groups-table-content ul').append(html);
                                var add_html_form = '';
                                add_html_form += '<input type="hidden" name="tags[]" value="' + data[1] + '"/>';
                                $('#edit_account_116').append(add_html_form);


                                new_ids -= 1;
                                var newUser = [];
                                newUser[newUser.length] = data[0];
                                newUser[newUser.length] = data[1];
                                var $overview = $('.widget-scrollable.horizontal .overview');
                                $.each($overview, function () {

                                    var width = $.map($(this).children(), function (child) {
                                        return $(child).outerWidth() +
                                                $(child).pixels('margin-left') + $(child).pixels('margin-right');
                                    });
                                    $(this).width($.sum(width));

                                });

                                $('.widget-scrollable').tinyscrollbar();
                                $('#addSuperAdminModal').modal('hide');
                                $('#tagnofound').remove();

                            }
                        }
                    </script>

                    <!--                    <form accept-charset="UTF-8" action="<?php echo $this->base . '/accounts/changeAccountOwner/' . $account_id . '/1' ?>" enctype="multipart/form-data" method="POST">
                        <div class="field_box">
                <label>Account Name</label>
                <input type="text" value="<?php echo $myAccount['Account']['company_name'] ?>" />
            </div>
            <div class="field_box">
                <label>Change account owner</label>
                    <?php echo $this->Form->input('user_id', array('id' => 'user_user_id', 'options' => $allAccountOwner, 'selected' => $user_id, 'type' => 'select', 'label' => FALSE, 'div' => FALSE, 'required' => 'required')); ?>
                <a class="login-button" href="javascript:void(0)">Select as new account owner</a>
                 <input id="account_owner_user_id" name="account_owner_user_id" type="hidden" value="<?php echo $user_id; ?>" />

            </div>
            <div class="clear"></div>-->


                    <!--
                                <div class="clear"></div>
                                </form>-->


                </li>

                <li data-content="color" <?php
                if (isset($tab) && $tab == 2) {
                    echo 'class="selected"';
                }
                ?>>
                    <form accept-charset="UTF-8"
                          action="<?php echo $this->base . '/accounts/tab_2_edit/' . $account_id . '/2' ?>"
                          class="edit_account" enctype="multipart/form-data" id="edit_account_116" method="post">
                        <div class="logo_Detailbox">
                            <div class="logo_detail_greyBox">
                                <div class="logoPReview_box" style="background:url(<?php echo $this->Custom->get_site_settings('header_logo_bg') ?>) no-repeat">
                                    <div class="inside_preview_box"></div>
                                </div>
                                <p><?php echo $language_based_content['change_logo_all']; ?></p>

                                <button type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="upload-toggle red_browse_plan"><?php echo $language_based_content['cl_browse_all']; ?></button>
                                <input id="account_image_logo" name="account[image_logo]" type="file"/>


                            </div>
                            <p><?php echo $language_based_content['recommend_size_logo_all']; ?></p>

                        </div>
                        <?php
                        $globel_background_color = '#808184';
                        if (isset($_SESSION['site_id']) && $_SESSION['site_id'] == 1) {
                            $globel_background_color = '#336699';
                        }
                        ?>
                        <div class="color_platebox">
                            <table class="colorpicker_tables">
                                <tr>
                                    <td style="width:30px">
                                        <div class="color-picker-wrap">

                                            <input id="color_picker" type="text" value="" class="color-preview"
                                                   style="background-color:<?php echo isset($myAccount['Account']['header_background_color']) ? '#' . $myAccount['Account']['header_background_color'] : $globel_background_color ?>">
                                        </div>
                                    </td>
                                    <td><p><?php echo $language_based_content['custom_header_background_all']; ?></p></td>
                                </tr>
                            </table>
                            <table class="colorpicker_tables">
                                <tr>
                                    <td style="width:30px">
                                        <div class="color-picker-wrap">
                                            <input id="color_picker_2" type="text" value="" class="color-preview-2"
                                                   style="background-color:<?php echo isset($myAccount['Account']['usernav_bg_color']) ? '#' . $myAccount['Account']['usernav_bg_color'] : '#fff' ?>">
                                        </div>
                                    </td>
                                    <td><p><?php echo $language_based_content['custom_logo_background_all']; ?></p></td>
                                </tr>
                            </table>
                            <br>
                            <span><?php echo $language_based_content['brand_pages_with_colors_all']; ?></span>
                        </div>
                        <div class="clear"></div>
                        <div class="settings_bottom_btn">
                            <a href="<?php echo $this->base . '/accounts/reset_custom_theme/' . $account_id . '/2' ?>"
                               class="reset_settingsLink"><?php echo $language_based_content['reset_to_default_logo_color_all']; ?></a>
                            <button type="submit"  style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="setting_greenBtn"><?php echo $language_based_content['save_changes_cl_all']; ?></button>
                        </div>
                        <input id="account_header_background_color" name="account[header_background_color]" type="hidden"/>
                        <input id="account_nav_bg_color" name="account[nav_bg_color]" type="hidden"/>
                        <input id="account_usernav_bg_color" name="account[usernav_bg_color]" type="hidden"/>

                    </form>
                </li>
                <?php if ($myAccount['Account']['in_trial'] != '1'): ?>
                    <li data-content="archiving" <?php
                    if (isset($tab) && $tab == 3) {
                        echo 'class="selected"';
                    }
                    ?>>
                        <div class="archiving_detailBox">
                            <div class="archiving archiving_icon">
                                <h1><?php echo $language_based_content['archive_all']; ?></h1>
                                <a href="<?php echo $this->base . '/Huddles/archive_contents/1' ?>"><img
                                        src="<?php echo $this->webroot . 'app/img/archive_huddle.png' ?>" width="16"
                                        height="13" alt=""/><?php echo $language_based_content['archive_huddles_all']; ?></a>
                                <a href="<?php echo $this->base . '/Huddles/archive_contents/2' ?>"><img
                                        src="<?php echo $this->webroot . 'app/img/archive_folder.png' ?>" width="16"
                                        height="13" alt=""/><?php echo $language_based_content['archive_folders_all']; ?></a>
                            </div>
                        </div>
                        <div class="archiving_detailBox">
                            <div class="archiving unarchiving_icon">
                                <h1><?php echo $language_based_content['unarchive_all']; ?></h1>
                                <a href="<?php echo $this->base . '/Huddles/unarchive_contents/1' ?>"><img
                                        src="<?php echo $this->webroot . 'app/img/archive_huddle.png' ?>" width="16"
                                        height="13" alt=""/><?php echo $language_based_content['archive_huddles_all']; ?></a>
                                <a href="<?php echo $this->base . '/Huddles/unarchive_contents/2' ?>"><img
                                        src="<?php echo $this->webroot . 'app/img/archive_folder.png' ?>" width="16"
                                        height="13" alt=""/><?php echo $language_based_content['archive_folders_all']; ?></a>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </li>
                <?php endif; ?>
                <?php if (!$deactive_plan): ?>
                    <li id="plan" data-content="plan" <?php
                    if (isset($tab) && $tab == 4) {
                        echo 'class="selected"';
                    }
                    ?>>
                        <div class="settings_plans">
                            <h1><?php echo $language_based_content['plans_pricing_all']; ?></h1>
                            <div id="tabs-container">
                                <ul class="tabs-menu">
                                    <li class="current"><a href="#tab-1">K12 Schools</a></li>
                                    <li><a href="#tab-2">Higher ED</a></li>
                                    <li><a href="#tab-3">Business</a></li>
                                    <div class="clear"></div>
                                </ul>
                                <div class="tab">
                                    <div id="tab-1" class="tab-content">
                                        <div class="tabs_sub_links">
                                            <a href="javascript:void(0);" id="school_annually"
                                               class="btn-special btn-special-active"
                                               style="border-right:0px;"><?php echo $language_based_content['annually_all']; ?></a><a href="javascript:void(0);"
                                               class="btn-special"
                                               id="school_monthly"
                                               style="border-left:0px;border-radius: 0px 10px 10px 0px;"><?php echo $language_based_content['monthly_all']; ?></a>
                                        </div>
                                        <div class="plans_box school_planBox_yearly">
                                            <div class="datagrid">
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th style="width:25%;"></th>
                                                            <th style="width:25%;">Team/PLC</th>
                                                            <th style="width:25%;">School</th>
                                                            <th style="width:25%;">District/ESC</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td>
                                                                <div>
                                                                    <div class="price"><span id="team_plc_price_yearly"
                                                                                             class="dollar">7</span></div>
                                                                        <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 9): ?>
                                                                        <div class="incdec">
                                                                            <?php
                                                                            if ($myAccount['Account']['in_trial'] == 1) {

                                                                                $minimum_users = $UserObject->getTotalUsers($account_id);
                                                                            } else {
                                                                                $minimum_users = $myAccount['Account']['plan_qty'];
                                                                            }
                                                                            ?>
                                                                            <input readonly="readonly" type="text"
                                                                                   value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"/>
                                                                            <div class="value_btn">


                                                                                <img id-data="quantity-9" max-limit="20"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/plus_value.png' ?>"
                                                                                     class="up"/>
                                                                                <img id-data="quantity-9" max-limit="20"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/minus_value.png' ?>"
                                                                                     class="down"
                                                                                     style="top: -3px;position: relative"/>
                                                                            </div>

                                                                        </div>
                                                                    <?php endif; ?>

                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <form accept-charset="UTF-8"
                                                                          action="<?php echo $this->base . '/customer/create_subscription/' . $account_id . '/' . '3' ?>"
                                                                          enctype="multipart/form-data" method="post">
                                                                              <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 9): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['purchase_btn_all']; ?>
                                                                            </button><input readonly="readonly" class="quantity" id="quantity-9"
                                                                                            type="hidden" name="quantity"
                                                                                            value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"> <?php endif; ?>
                                                                                        <?php if (($myAccount['plans']['id'] < 9 || $myAccount['plans']['per_year'] != 1) && $myAccount['Account']['in_trial'] == 0 && $myAccount['plans']['category'] != NULL && $myAccount['plans']['id'] != 9 && $myAccount['Account']['plan_qty'] < 21): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['change_tier_all']; ?>
                                                                            </button><input class="quantity" id="quantity-9"
                                                                                            type="hidden" name="quantity"
                                                                                            value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $myAccount['Account']['plan_qty'] : 1; ?>">  <?php endif; ?>
                                                                        <input type="hidden" name="plan_id" value="9">
                                                                        <input type="hidden" name="monthly_yearly"
                                                                               value="0">

                                                                    </form>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div>
                                                                    <div class="price"><span class="dollar">9</span></div>
                                                                    <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 11): ?>
                                                                        <div class="incdec">

                                                                            <?php
                                                                            $minimum_users_check = $UserObject->getTotalUsers($account_id);
                                                                            if ($myAccount['Account']['in_trial'] == 1 && $minimum_users_check > 21) {

                                                                                $minimum_users = $UserObject->getTotalUsers($account_id);
                                                                            } else {
                                                                                $minimum_users = 21;
                                                                            }
                                                                            ?>

                                                                            <input readonly="readonly" type="text"
                                                                                   value="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"/>
                                                                            <div class="value_btn">

                                                                                <img id-data="quantity-11" max-limit="100"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/plus_value.png' ?>"
                                                                                     class="up"/>
                                                                                <img id-data="quantity-11" max-limit="100"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/minus_value.png' ?>"
                                                                                     class="down"
                                                                                     style="top: -3px;position: relative"/>
                                                                            </div>

                                                                        </div>
                                                                    <?php endif; ?>

                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <form accept-charset="UTF-8"
                                                                          action="<?php echo $this->base . '/customer/create_subscription/' . $account_id . '/' . '3' ?>"
                                                                          enctype="multipart/form-data" method="post">
                                                                              <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 11): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['purchase_btn_all']; ?>
                                                                            </button><input readonly="readonly" class="quantity"
                                                                                            id="quantity-11" type="hidden"
                                                                                            name="quantity"
                                                                                            value="21"> <?php endif; ?>
                                                                                        <?php if (($myAccount['plans']['id'] < 11 || $myAccount['plans']['per_year'] != 1) && $myAccount['Account']['in_trial'] == 0 && $myAccount['plans']['category'] != NULL && $myAccount['plans']['id'] != 11): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['change_tier_all']; ?>
                                                                            </button><input class="quantity"
                                                                                            id="quantity-11" type="hidden"
                                                                                            name="quantity"
                                                                                            value="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['plan_qty'] > 20) ? $myAccount['Account']['plan_qty'] : 21; ?>">  <?php endif; ?>
                                                                        <input type="hidden" name="plan_id" value="11">
                                                                        <input type="hidden" name="monthly_yearly"
                                                                               value="0">

                                                                    </form>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="grid_contact">
                                                                    <h2>Contact Us</h2>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <button data-toggle="modal" data-target="#email_ob"
                                                                            type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn"><?php echo $language_based_content['contact_us_btn_all']; ?>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['user_licenses_all']; ?></td>
                                                            <td><?php echo $language_based_content['up_to_20_all']; ?></td>
                                                            <td>21-100</td>
                                                            <td>100+</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['shared_file_storage_all']; ?></td>
                                                            <td>25 <?php echo $language_based_content['gb_users_year_all']; ?></td>
                                                            <td>50 <?php echo $language_based_content['gb_users_year_all']; ?></td>
                                                            <td><?php echo $language_based_content['custom_box_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_uploads_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['resource_file_sharing_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['private_user_workspace_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['coaching_collab_huddles_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['huddle_discussion_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['time_stamped_video_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['time_stamped_audio_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_tags_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_video_comment_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_editing_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['iOS_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['android_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['dropbox_googledrive_etc_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_branding_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['visible_ob_notes_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['administrative_management_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['google_sign_in_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['best_practices_lib_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_framework_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['performance_analytics_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['coaching_tracker_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['assessment_tracker_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['lms_intergration_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th colspan="4" style="text-align:left;"><?php $language_based_content['support_training_all']; ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align:left;"><?php $language_based_content['support_all']; ?></td>
                                                            <td><?php echo $language_based_content['help_desk_email_all']; ?></td>
                                                            <td><?php echo $language_based_content['priority_email_all']; ?></td>
                                                            <td><?php echo $language_based_content['account_manager_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['online_workshop_all']; ?></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['onsite_professional_dev_all']; ?>
                                                            </td>
                                                            <td></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;">Payment</td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_transfer_all']; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="plans_box school_planBox_monthly" style="display:none;">
                                            <div class="datagrid">
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th style="width:25%;"></th>
                                                            <th style="width:25%;">Team/PLC</th>
                                                            <th style="width:25%;">School</th>
                                                            <th style="width:25%;">District/ESC</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td>
                                                                <div>
                                                                    <div class="price"><span id="team_plc_price_monthly"
                                                                                             class="dollar">8</span></div>
                                                                        <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 10): ?>
                                                                        <div class="incdec">
                                                                            <?php
                                                                            if ($myAccount['Account']['in_trial'] == 1) {

                                                                                $minimum_users = $UserObject->getTotalUsers($account_id);
                                                                            } else {
                                                                                $minimum_users = $myAccount['Account']['plan_qty'];
                                                                            }
                                                                            ?>
                                                                            <input readonly="readonly" type="text"
                                                                                   value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"/>
                                                                            <div class="value_btn">

                                                                                <img id-data="quantity-10" max-limit="20"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/plus_value.png' ?>"
                                                                                     class="up"/>
                                                                                <img id-data="quantity-10" max-limit="20"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/minus_value.png' ?>"
                                                                                     class="down"
                                                                                     style="top: -3px;position: relative"/>
                                                                            </div>

                                                                        </div>
                                                                    <?php endif; ?>

                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <form accept-charset="UTF-8"
                                                                          action="<?php echo $this->base . '/customer/create_subscription/' . $account_id . '/' . '3' ?>"
                                                                          enctype="multipart/form-data" method="post">
                                                                              <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 10): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['purchase_btn_all']; ?>
                                                                            </button><input readonly="readonly" class="quantity"
                                                                                            id="quantity-10" type="hidden"
                                                                                            name="quantity"
                                                                                            value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"> <?php endif; ?>
                                                                                        <?php if ($myAccount['plans']['id'] < 10 && $myAccount['plans']['per_year'] != 1 && $myAccount['Account']['in_trial'] == 0 && $myAccount['plans']['category'] != NULL && $myAccount['plans']['id'] != 10 && $myAccount['Account']['plan_qty'] < 21): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['change_tier_all']; ?>
                                                                            </button><input class="quantity"
                                                                                            id="quantity-10" type="hidden"
                                                                                            name="quantity"
                                                                                            value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $myAccount['Account']['plan_qty'] : 1; ?>">  <?php endif; ?>
                                                                        <input type="hidden" name="plan_id" value="10">
                                                                        <input type="hidden" name="monthly_yearly"
                                                                               value="1">

                                                                    </form>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div>
                                                                    <div class="price"><span class="dollar">10</span></div>
                                                                    <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 12): ?>
                                                                        <div class="incdec">
                                                                            <?php
                                                                            $minimum_users_check = $UserObject->getTotalUsers($account_id);
                                                                            if ($myAccount['Account']['in_trial'] == 1 && $minimum_users_check > 21) {

                                                                                $minimum_users = $UserObject->getTotalUsers($account_id);
                                                                            } else {
                                                                                $minimum_users = 21;
                                                                            }
                                                                            ?>
                                                                            <input readonly="readonly" type="text"
                                                                                   value="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"/>
                                                                            <div class="value_btn">

                                                                                <img id-data="quantity-12" max-limit="100"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/plus_value.png' ?>"
                                                                                     class="up"/>
                                                                                <img id-data="quantity-12" max-limit="100"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/minus_value.png' ?>"
                                                                                     class="down"
                                                                                     style="top: -3px;position: relative"/>
                                                                            </div>

                                                                        </div>
                                                                    <?php endif; ?>


                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <form accept-charset="UTF-8"
                                                                          action="<?php echo $this->base . '/customer/create_subscription/' . $account_id . '/' . '3' ?>"
                                                                          enctype="multipart/form-data" method="post">
                                                                              <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 12): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['purchase_btn_all']; ?>
                                                                            </button><input readonly="readonly" class="quantity"
                                                                                            id="quantity-12" type="hidden"
                                                                                            name="quantity"
                                                                                            value="21"> <?php endif; ?>
                                                                                        <?php if ($myAccount['plans']['id'] < 12 && $myAccount['plans']['per_year'] != 1 && $myAccount['Account']['in_trial'] == 0 && $myAccount['plans']['category'] != NULL && $myAccount['plans']['id'] != 12): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['change_tier_all']; ?>
                                                                            </button><input class="quantity"id="quantity-12"
                                                                                            type="hidden"name="quantity"
                                                                                            value="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['plan_qty'] > 20) ? $myAccount['Account']['plan_qty'] : 21; ?>" ><?php endif; ?>
                                                                        <input type="hidden" name="plan_id" value="12">
                                                                        <input type="hidden" name="monthly_yearly"
                                                                               value="1">

                                                                    </form>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="grid_contact">
                                                                    <h2>Contact Us</h2>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <button data-toggle="modal" data-target="#email_ob"
                                                                            type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn"><?php echo $language_based_content['contact_us_btn_all']; ?>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['user_licenses_all']; ?></td>
                                                            <td><?php echo $language_based_content['up_to_20_all']; ?></td>
                                                            <td>21-100</td>
                                                            <td>100+</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['shared_file_storage_all']; ?></td>
                                                            <td>25 <?php echo $language_based_content['gb_users_year_all']; ?></td>
                                                            <td>50 <?php echo $language_based_content['gb_users_year_all']; ?></td>
                                                            <td><?php echo $language_based_content['custom_box_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_uploads_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['resource_file_sharing_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['private_user_workspace_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['coaching_collab_huddles_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['huddle_discussion_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['time_stamped_video_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['time_stamped_audio_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_tags_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_video_comment_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_editing_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['iOS_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['android_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['dropbox_googledrive_etc_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_branding_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['visible_ob_notes_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['administrative_management_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['google_sign_in_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['best_practices_lib_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_framework_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['performance_analytics_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['coaching_tracker_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['assessment_tracker_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['lms_intergration_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th colspan="4" style="text-align:left;"><?php $language_based_content['support_training_all']; ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align:left;"><?php $language_based_content['support_all']; ?></td>
                                                            <td><?php echo $language_based_content['help_desk_email_all']; ?></td>
                                                            <td><?php echo $language_based_content['priority_email_all']; ?></td>
                                                            <td><?php echo $language_based_content['account_manager_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['online_workshop_all']; ?></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['onsite_professional_dev_all']; ?>
                                                            </td>
                                                            <td></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['payment_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_transfer_all']; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab-2" class="tab-content">
                                        <div class="tabs_sub_links">
                                            <a href="javascript:void(0);" id="ED_annually"
                                               class="btn-special btn-special-active"
                                               style="border-right:0px;"><?php echo $language_based_content['annually_all']; ?></a><a href="javascript:void(0);"
                                               class="btn-special"
                                               id="ED_monthly"
                                               style="border-left:0px;border-radius: 0px 10px 10px 0px;"><?php echo $language_based_content['monthly_all']; ?></a>
                                        </div>
                                        <div class="plans_box ED_planBox_yearly">
                                            <div class="datagrid">
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th style="width:25%;"></th>
                                                            <th style="width:25%;">Team</th>
                                                            <th style="width:25%;">Department</th>
                                                            <th style="width:25%;">Institution</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td>
                                                                <div>
                                                                    <div class="price"><span class="dollar">7</span></div>
                                                                    <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 13): ?>
                                                                        <div class="incdec">
                                                                            <?php
                                                                            if ($myAccount['Account']['in_trial'] == 1) {

                                                                                $minimum_users = $UserObject->getTotalUsers($account_id);
                                                                            } else {
                                                                                $minimum_users = $myAccount['Account']['plan_qty'];
                                                                            }
                                                                            ?>
                                                                            <input readonly="readonly" type="text"
                                                                                   value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"/>
                                                                            <div class="value_btn">

                                                                                <img id-data="quantity-13" max-limit="20"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/plus_value.png' ?>"
                                                                                     class="up"/>
                                                                                <img id-data="quantity-13" max-limit="20"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/minus_value.png' ?>"
                                                                                     class="down"
                                                                                     style="top: -3px;position: relative"/>
                                                                            </div>

                                                                        </div>
                                                                    <?php endif; ?>

                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <form accept-charset="UTF-8"
                                                                          action="<?php echo $this->base . '/customer/create_subscription/' . $account_id . '/' . '3' ?>"
                                                                          enctype="multipart/form-data" method="post">
                                                                              <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 13): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['purchase_btn_all']; ?>
                                                                            </button><input readonly="readonly" class="quantity"
                                                                                            id="quantity-13" type="hidden"
                                                                                            name="quantity"
                                                                                            value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"> <?php endif; ?>
                                                                                        <?php if (($myAccount['plans']['id'] < 13 || $myAccount['plans']['per_year'] != 1) && $myAccount['Account']['in_trial'] == 0 && $myAccount['plans']['category'] != NULL && $myAccount['plans']['id'] != 13 && $myAccount['Account']['plan_qty'] < 21): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['change_tier_all']; ?>
                                                                            </button> <input class="quantity"
                                                                                             id="quantity-13" type="hidden"
                                                                                             name="quantity"
                                                                                             value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $myAccount['Account']['plan_qty'] : 1; ?>"> <?php endif; ?>
                                                                        <input type="hidden" name="plan_id" value="13">
                                                                        <input type="hidden" name="monthly_yearly"
                                                                               value="0">

                                                                    </form>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div>
                                                                    <div class="price"><span class="dollar">9</span></div>
                                                                    <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 15): ?>
                                                                        <div class="incdec">
                                                                            <?php
                                                                            $minimum_users_check = $UserObject->getTotalUsers($account_id);
                                                                            if ($myAccount['Account']['in_trial'] == 1 && $minimum_users_check > 21) {

                                                                                $minimum_users = $UserObject->getTotalUsers($account_id);
                                                                            } else {
                                                                                $minimum_users = 21;
                                                                            }
                                                                            ?>
                                                                            <input readonly="readonly" type="text"
                                                                                   value="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"/>
                                                                            <div class="value_btn">

                                                                                <img id-data="quantity-15" max-limit="100"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/plus_value.png' ?>"
                                                                                     class="up"/>
                                                                                <img id-data="quantity-15" max-limit="100"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/minus_value.png' ?>"
                                                                                     class="down"
                                                                                     style="top: -3px;position: relative"/>
                                                                            </div>

                                                                        </div>
                                                                    <?php endif; ?>

                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <form accept-charset="UTF-8"
                                                                          action="<?php echo $this->base . '/customer/create_subscription/' . $account_id . '/' . '3' ?>"
                                                                          enctype="multipart/form-data" method="post">
                                                                              <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 15): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['purchase_btn_all']; ?>
                                                                            </button> <input readonly="readonly" class="quantity"
                                                                                             id="quantity-15" type="hidden"
                                                                                             name="quantity"
                                                                                             value="21"><?php endif; ?>
                                                                                         <?php if (($myAccount['plans']['id'] < 15 || $myAccount['plans']['per_year'] != 1) && $myAccount['Account']['in_trial'] == 0 && $myAccount['plans']['category'] != NULL && $myAccount['plans']['id'] != 15): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['change_tier_all']; ?>
                                                                            </button><input class="quantity"
                                                                                            id="quantity-15" type="hidden"
                                                                                            name="quantity"
                                                                                            value="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['plan_qty'] > 20) ? $myAccount['Account']['plan_qty'] : 21; ?>">  <?php endif; ?>
                                                                        <input type="hidden" name="plan_id" value="15">
                                                                        <input type="hidden" name="monthly_yearly"
                                                                               value="0">

                                                                    </form>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="grid_contact">
                                                                    <h2>Contact Us</h2>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <button data-toggle="modal" data-target="#email_ob"
                                                                            type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn"><?php echo $language_based_content['contact_us_btn_all']; ?>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['user_licenses_all']; ?></td>
                                                            <td><?php echo $language_based_content['up_to_20_all']; ?></td>
                                                            <td>21-100</td>
                                                            <td>100+</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['shared_file_storage_all']; ?></td>
                                                            <td>50 <?php echo $language_based_content['gb_users_year_all']; ?></td>
                                                            <td>100 <?php echo $language_based_content['gb_users_year_all']; ?></td>
                                                            <td><?php echo $language_based_content['custom_box_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_uploads_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['resource_file_sharing_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['private_user_workspace_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['coaching_collab_huddles_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['huddle_discussion_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['time_stamped_video_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['time_stamped_audio_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_tags_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_video_comment_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_editing_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['iOS_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['android_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['dropbox_googledrive_etc_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_branding_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['visible_ob_notes_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['administrative_management_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>

                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['google_sign_in_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['best_practices_lib_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_framework_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['performance_analytics_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['coaching_tracker_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['assessment_tracker_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['lms_intergration_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th colspan="4" style="text-align:left;"><?php $language_based_content['support_training_all']; ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align:left;"><?php $language_based_content['support_all']; ?></td>
                                                            <td><?php echo $language_based_content['help_desk_email_all']; ?></td>
                                                            <td><?php echo $language_based_content['priority_email_all']; ?></td>
                                                            <td><?php echo $language_based_content['account_manager_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['online_workshop_all']; ?></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['onsite_professional_dev_all']; ?>
                                                            </td>
                                                            <td></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['payment_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_transfer_all']; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="plans_box ED_planBox_monthly" style="display:none;">
                                            <div class="datagrid">
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th style="width:25%;"></th>
                                                            <th style="width:25%;">Team</th>
                                                            <th style="width:25%;">Department</th>
                                                            <th style="width:25%;">Institution</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td>
                                                                <div>
                                                                    <div class="price"><span class="dollar">8</span></div>
                                                                    <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 14): ?>
                                                                        <div class="incdec">
                                                                            <?php
                                                                            if ($myAccount['Account']['in_trial'] == 1) {

                                                                                $minimum_users = $UserObject->getTotalUsers($account_id);
                                                                            } else {
                                                                                $minimum_users = $myAccount['Account']['plan_qty'];
                                                                            }
                                                                            ?>
                                                                            <input readonly="readonly" type="text"
                                                                                   value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"/>
                                                                            <div class="value_btn">

                                                                                <img id-data="quantity-14" max-limit="20"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/plus_value.png' ?>"
                                                                                     class="up"/>
                                                                                <img id-data="quantity-14" max-limit="20"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/minus_value.png' ?>"
                                                                                     class="down"
                                                                                     style="top: -3px;position: relative"/>
                                                                            </div>

                                                                        </div>
                                                                    <?php endif; ?>

                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <form accept-charset="UTF-8"
                                                                          action="<?php echo $this->base . '/customer/create_subscription/' . $account_id . '/' . '3' ?>"
                                                                          enctype="multipart/form-data" method="post">
                                                                              <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 14): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['purchase_btn_all']; ?>
                                                                            </button> <input readonly="readonly" class="quantity" type="hidden"
                                                                                             id="quantity-14"
                                                                                             name="quantity"
                                                                                             value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"> <?php endif; ?>
                                                                                         <?php if ($myAccount['plans']['id'] < 14 && $myAccount['plans']['per_year'] != 1 && $myAccount['Account']['in_trial'] == 0 && $myAccount['plans']['category'] != NULL && $myAccount['plans']['id'] != 14 && $myAccount['Account']['plan_qty'] < 21): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['change_tier_all']; ?>
                                                                            </button> <input class="quantity"
                                                                                             id="quantity-14" type="hidden"
                                                                                             name="quantity"
                                                                                             value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $myAccount['Account']['plan_qty'] : 1; ?>">  <?php endif; ?>
                                                                        <input type="hidden" name="plan_id" value="14">
                                                                        <input type="hidden" name="monthly_yearly"
                                                                               value="1">

                                                                    </form>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div>
                                                                    <div class="price"><span class="dollar">10</span></div>
                                                                    <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 16): ?>
                                                                        <div class="incdec">
                                                                            <?php
                                                                            $minimum_users_check = $UserObject->getTotalUsers($account_id);
                                                                            if ($myAccount['Account']['in_trial'] == 1 && $minimum_users_check > 21) {

                                                                                $minimum_users = $UserObject->getTotalUsers($account_id);
                                                                            } else {
                                                                                $minimum_users = 21;
                                                                            }
                                                                            ?>
                                                                            <input readonly="readonly" type="text"
                                                                                   value="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"/>
                                                                            <div class="value_btn">

                                                                                <img id-data="quantity-16" max-limit="100"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/plus_value.png' ?>"
                                                                                     class="up"/>
                                                                                <img id-data="quantity-16" max-limit="100"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/minus_value.png' ?>"
                                                                                     class="down"
                                                                                     style="top: -3px;position: relative"/>
                                                                            </div>

                                                                        </div>
                                                                    <?php endif; ?>

                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <form accept-charset="UTF-8"
                                                                          action="<?php echo $this->base . '/customer/create_subscription/' . $account_id . '/' . '3' ?>"
                                                                          enctype="multipart/form-data" method="post">
                                                                              <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 16): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['purchase_btn_all']; ?>
                                                                            </button><input readonly="readonly" class="quantity"
                                                                                            id="quantity-16" type="hidden"
                                                                                            name="quantity"
                                                                                            value="21"> <?php endif; ?>
                                                                                        <?php if ($myAccount['plans']['id'] < 16 && $myAccount['plans']['per_year'] != 1 && $myAccount['Account']['in_trial'] == 0 && $myAccount['plans']['category'] != NULL && $myAccount['plans']['id'] != 16): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['change_tier_all']; ?>
                                                                            </button> <input class="quantity"
                                                                                             id="quantity-16" type="hidden"
                                                                                             name="quantity"
                                                                                             value="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['plan_qty'] > 20) ? $myAccount['Account']['plan_qty'] : 21; ?>"> <?php endif; ?>
                                                                        <input type="hidden" name="plan_id" value="16">
                                                                        <input type="hidden" name="monthly_yearly"
                                                                               value="1">

                                                                    </form>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="grid_contact">
                                                                    <h2>Contact Us</h2>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <button data-toggle="modal" data-target="#email_ob"
                                                                            type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn"><?php echo $language_based_content['contact_us_btn_all']; ?>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['user_licenses_all']; ?></td>
                                                            <td><?php echo $language_based_content['up_to_20_all']; ?></td>
                                                            <td>21-100</td>
                                                            <td>100+</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['shared_file_storage_all']; ?></td>
                                                            <td>50 <?php echo $language_based_content['gb_users_year_all']; ?></td>
                                                            <td>100 <?php echo $language_based_content['gb_users_year_all']; ?></td>
                                                            <td><?php echo $language_based_content['custom_box_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_uploads_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['resource_file_sharing_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['private_user_workspace_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['coaching_collab_huddles_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['huddle_discussion_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['time_stamped_video_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['time_stamped_audio_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_tags_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_video_comment_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_editing_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['iOS_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['android_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['dropbox_googledrive_etc_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_branding_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['visible_ob_notes_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['administrative_management_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['google_sign_in_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['best_practices_lib_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_framework_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['performance_analytics_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['coaching_tracker_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['assessment_tracker_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['lms_intergration_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th colspan="4" style="text-align:left;"><?php $language_based_content['support_training_all']; ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align:left;"><?php $language_based_content['support_all']; ?></td>
                                                            <td><?php echo $language_based_content['help_desk_email_all']; ?></td>
                                                            <td><?php echo $language_based_content['priority_email_all']; ?></td>
                                                            <td><?php echo $language_based_content['account_manager_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['online_workshop_all']; ?></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['onsite_professional_dev_all']; ?>
                                                            </td>
                                                            <td></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['payment_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_transfer_all']; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="tab-3" class="tab-content">
                                        <div class="tabs_sub_links">
                                            <a href="javascript:void(0);" id="business_annually"
                                               class="btn-special btn-special-active"
                                               style="border-right:0px;"><?php echo $language_based_content['annually_all']; ?></a><a href="javascript:void(0);"
                                               class="btn-special"
                                               id="business_monthly"
                                               style="border-left:0px;border-radius: 0px 10px 10px 0px;"><?php echo $language_based_content['monthly_all']; ?></a>
                                        </div>
                                        <div class="plans_box business_planBox_yearly">
                                            <div class="datagrid">
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th style="width:25%;"></th>
                                                            <th style="width:25%;">Team</th>
                                                            <th style="width:25%;">Business</th>
                                                            <th style="width:25%;">Enterprise</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td>
                                                                <div>
                                                                    <div class="price"><span class="dollar">15</span></div>
                                                                    <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 17): ?>
                                                                        <div class="incdec">
                                                                            <?php
                                                                            if ($myAccount['Account']['in_trial'] == 1) {

                                                                                $minimum_users = $UserObject->getTotalUsers($account_id);
                                                                            } else {
                                                                                $minimum_users = $myAccount['Account']['plan_qty'];
                                                                            }
                                                                            ?>
                                                                            <input readonly="readonly" type="text"
                                                                                   value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"/>
                                                                            <div class="value_btn">

                                                                                <img id-data="quantity-17" max-limit="20"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/plus_value.png' ?>"
                                                                                     class="up"/>
                                                                                <img id-data="quantity-17" max-limit="20"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/minus_value.png' ?>"
                                                                                     class="down"
                                                                                     style="top: -3px;position: relative"/>
                                                                            </div>

                                                                        </div>
                                                                    <?php endif; ?>

                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <form accept-charset="UTF-8"
                                                                          action="<?php echo $this->base . '/customer/create_subscription/' . $account_id . '/' . '3' ?>"
                                                                          enctype="multipart/form-data" method="post">
                                                                              <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 17): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['purchase_btn_all']; ?>
                                                                            </button><input readonly="readonly" class="quantity"
                                                                                            id="quantity-17" type="hidden"
                                                                                            name="quantity"
                                                                                            value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"> <?php endif; ?>
                                                                                        <?php if (($myAccount['plans']['id'] < 17 || $myAccount['plans']['per_year'] != 1) && $myAccount['Account']['in_trial'] == 0 && $myAccount['plans']['category'] != NULL && $myAccount['plans']['id'] != 17 && $myAccount['Account']['plan_qty'] < 21): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['change_tier_all']; ?>
                                                                            </button> <input class="quantity"
                                                                                             id="quantity-17" type="hidden"
                                                                                             name="quantity"
                                                                                             value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $myAccount['Account']['plan_qty'] : 1; ?>">  <?php endif; ?>
                                                                        <input type="hidden" name="plan_id" value="17">
                                                                        <input type="hidden" name="monthly_yearly"
                                                                               value="0">

                                                                    </form>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div>
                                                                    <div class="price"><span class="dollar">25</span></div>
                                                                    <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 19): ?>
                                                                        <div class="incdec">
                                                                            <?php
                                                                            $minimum_users_check = $UserObject->getTotalUsers($account_id);
                                                                            if ($myAccount['Account']['in_trial'] == 1 && $minimum_users_check > 21) {

                                                                                $minimum_users = $UserObject->getTotalUsers($account_id);
                                                                            } else {
                                                                                $minimum_users = 21;
                                                                            }
                                                                            ?>
                                                                            <input readonly="readonly" type="text"
                                                                                   value="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"/>
                                                                            <div class="value_btn">

                                                                                <img id-data="quantity-19" max-limit="100"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/plus_value.png' ?>"
                                                                                     class="up"/>
                                                                                <img id-data="quantity-19" max-limit="100"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/minus_value.png' ?>"
                                                                                     class="down"
                                                                                     style="top: -3px;position: relative"/>
                                                                            </div>

                                                                        </div>
                                                                    <?php endif; ?>

                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <form accept-charset="UTF-8"
                                                                          action="<?php echo $this->base . '/customer/create_subscription/' . $account_id . '/' . '3' ?>"
                                                                          enctype="multipart/form-data" method="post">
                                                                              <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 19): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['purchase_btn_all']; ?>
                                                                            </button><input readonly="readonly" class="quantity"
                                                                                            id="quantity-19" type="hidden"
                                                                                            name="quantity"
                                                                                            value="21"> <?php endif; ?>
                                                                                        <?php if (($myAccount['plans']['id'] < 19 || $myAccount['plans']['per_year'] != 1) && $myAccount['Account']['in_trial'] == 0 && $myAccount['plans']['category'] != NULL && $myAccount['plans']['id'] != 19): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['change_tier_all']; ?>
                                                                            </button><input class="quantity"
                                                                                            id="quantity-19" type="hidden"
                                                                                            name="quantity"
                                                                                            value="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['plan_qty'] > 20) ? $myAccount['Account']['plan_qty'] : 21; ?>">  <?php endif; ?>
                                                                        <input type="hidden" name="plan_id" value="19">
                                                                        <input type="hidden" name="monthly_yearly"
                                                                               value="0">

                                                                    </form>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="grid_contact">
                                                                    <h2>Contact Us</h2>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <button data-toggle="modal" data-target="#email_ob"
                                                                            type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn"><?php echo $language_based_content['contact_us_btn_all']; ?>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['user_licenses_all']; ?></td>
                                                            <td><?php echo $language_based_content['up_to_20_all']; ?></td>
                                                            <td>21-100</td>
                                                            <td>100+</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['shared_file_storage_all']; ?></td>
                                                            <td>100 <?php echo $language_based_content['gb_users_year_all']; ?></td>
                                                            <td>200 <?php echo $language_based_content['gb_users_year_all']; ?></td>
                                                            <td><?php echo $language_based_content['custom_box_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_uploads_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['resource_file_sharing_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['private_user_workspace_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['coaching_collab_huddles_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['huddle_discussion_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['time_stamped_video_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['time_stamped_audio_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_tags_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_video_comment_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_editing_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['iOS_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['android_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['dropbox_googledrive_etc_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_branding_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['visible_ob_notes_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['administrative_management_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>

                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['google_sign_in_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['best_practices_lib_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_framework_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['performance_analytics_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['coaching_tracker_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['assessment_tracker_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['lms_intergration_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th colspan="4" style="text-align:left;"><?php $language_based_content['support_training_all']; ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align:left;"><?php $language_based_content['support_all']; ?></td>
                                                            <td><?php echo $language_based_content['help_desk_email_all']; ?></td>
                                                            <td><?php echo $language_based_content['priority_email_all']; ?></td>
                                                            <td><?php echo $language_based_content['account_manager_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['online_workshop_all']; ?></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['onsite_professional_dev_all']; ?>
                                                            </td>
                                                            <td></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['payment_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_transfer_all']; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="plans_box business_planBox_monthly" style="display:none;">
                                            <div class="datagrid">
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <th style="width:25%;"></th>
                                                            <th style="width:25%;">Team</th>
                                                            <th style="width:25%;">Business</th>
                                                            <th style="width:25%;">Enterprise</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td>
                                                                <div>
                                                                    <div class="price"><span class="dollar">17</span></div>
                                                                    <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 18): ?>
                                                                        <div class="incdec">
                                                                            <?php
                                                                            if ($myAccount['Account']['in_trial'] == 1) {

                                                                                $minimum_users = $UserObject->getTotalUsers($account_id);
                                                                            } else {
                                                                                $minimum_users = $myAccount['Account']['plan_qty'];
                                                                            }
                                                                            ?>
                                                                            <input readonly="readonly" type="text"
                                                                                   value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"/>
                                                                            <div class="value_btn">

                                                                                <img id-data="quantity-18" max-limit="20"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/plus_value.png' ?>"
                                                                                     class="up"/>
                                                                                <img id-data="quantity-18" max-limit="20"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/minus_value.png' ?>"
                                                                                     class="down"
                                                                                     style="top: -3px;position: relative"/>
                                                                            </div>

                                                                        </div>
                                                                    <?php endif; ?>

                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <form accept-charset="UTF-8"
                                                                          action="<?php echo $this->base . '/customer/create_subscription/' . $account_id . '/' . '3' ?>"
                                                                          enctype="multipart/form-data" method="post">
                                                                              <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 18): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['purchase_btn_all']; ?>
                                                                            </button> <input readonly="readonly" class="quantity"
                                                                                             id="quantity-18" type="hidden"
                                                                                             name="quantity"
                                                                                             value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $minimum_users : 1; ?>" ><?php endif; ?>
                                                                                         <?php if ($myAccount['plans']['id'] < 18 && $myAccount['plans']['per_year'] != 1 && $myAccount['Account']['in_trial'] == 0 && $myAccount['plans']['category'] != NULL && $myAccount['plans']['id'] != 18 && $myAccount['Account']['plan_qty'] < 21): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['change_tier_all']; ?>
                                                                            </button> <input class="quantity"
                                                                                             id="quantity-18" type="hidden"
                                                                                             name="quantity"
                                                                                             value="<?php echo (!empty($myAccount['Account']['plan_qty'])) ? $myAccount['Account']['plan_qty'] : 1; ?>">   <?php endif; ?>
                                                                        <input type="hidden" name="plan_id" value="18">
                                                                        <input type="hidden" name="monthly_yearly"
                                                                               value="1">

                                                                    </form>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div>
                                                                    <div class="price"><span class="dollar">27</span></div>
                                                                    <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 20): ?>
                                                                        <div class="incdec">
                                                                            <?php
                                                                            $minimum_users_check = $UserObject->getTotalUsers($account_id);
                                                                            if ($myAccount['Account']['in_trial'] == 1 && $minimum_users_check > 21) {

                                                                                $minimum_users = $UserObject->getTotalUsers($account_id);
                                                                            } else {
                                                                                $minimum_users = 21;
                                                                            }
                                                                            ?>
                                                                            <input readonly="readonly" type="text"
                                                                                   value="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"/>
                                                                            <div class="value_btn">

                                                                                <img id-data="quantity-20" max-limit="100"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/plus_value.png' ?>"
                                                                                     class="up"/>
                                                                                <img id-data="quantity-20" max-limit="100"
                                                                                     min-limit="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['in_trial'] != 1 && $myAccount['plans']['category'] != NULL) ? $myAccount['Account']['plan_qty'] : $minimum_users; ?>"
                                                                                     src="<?php echo $this->webroot . 'app/img/minus_value.png' ?>"
                                                                                     class="down"
                                                                                     style="top: -3px;position: relative"/>
                                                                            </div>

                                                                        </div>
                                                                    <?php endif; ?>

                                                                    <div class="clear"></div>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <form accept-charset="UTF-8"
                                                                          action="<?php echo $this->base . '/customer/create_subscription/' . $account_id . '/' . '3' ?>"
                                                                          enctype="multipart/form-data" method="post">
                                                                              <?php if ($myAccount['Account']['in_trial'] == 1 || $myAccount['plans']['category'] == NULL || $myAccount['plans']['id'] == 20): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['purchase_btn_all']; ?>
                                                                            </button><input class="quantity"
                                                                                            id="quantity-20" type="hidden"
                                                                                            name="quantity"
                                                                                            value="21" readonly="readonly"> <?php endif; ?>
                                                                                        <?php if ($myAccount['plans']['id'] < 20 && $myAccount['plans']['per_year'] != 1 && $myAccount['Account']['in_trial'] == 0 && $myAccount['plans']['category'] != NULL && $myAccount['plans']['id'] != 20): ?>
                                                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn">
                                                                                <?php echo $language_based_content['change_tier_all']; ?>
                                                                            </button><input class="quantity"
                                                                                            id="quantity-20" type="hidden"
                                                                                            name="quantity"
                                                                                            value="<?php echo (!empty($myAccount['Account']['plan_qty']) && $myAccount['Account']['plan_qty'] > 20) ? $myAccount['Account']['plan_qty'] : 21; ?>">  <?php endif; ?>
                                                                        <input type="hidden" name="plan_id" value="20">
                                                                        <input type="hidden" name="monthly_yearly"
                                                                               value="1">

                                                                    </form>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="grid_contact">
                                                                    <h2>Contact Us</h2>
                                                                </div>
                                                                <div class="grid_btn_box">
                                                                    <button data-toggle="modal" data-target="#email_ob"
                                                                            type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="green_grid_btn"><?php echo $language_based_content['contact_us_btn_all']; ?>
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['user_licenses_all']; ?></td>
                                                            <td><?php echo $language_based_content['up_to_20_all']; ?></td>
                                                            <td>21-100</td>
                                                            <td>100+</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['shared_file_storage_all']; ?></td>
                                                            <td>100 <?php echo $language_based_content['gb_users_year_all']; ?></td>
                                                            <td>200 <?php echo $language_based_content['gb_users_year_all']; ?></td>
                                                            <td><?php echo $language_based_content['custom_box_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_uploads_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['unlimited_box_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['resource_file_sharing_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['private_user_workspace_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['coaching_collab_huddles_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['huddle_discussion_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['time_stamped_video_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['time_stamped_audio_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_tags_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_video_comment_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['video_editing_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['iOS_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['android_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>

                                                            <td style="text-align:left;"><?php echo $language_based_content['dropbox_googledrive_etc_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_branding_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['visible_ob_notes_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['administrative_management_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['google_sign_in_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['best_practices_lib_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['custom_framework_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['performance_analytics_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['coaching_tracker_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['assessment_tracker_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['lms_intergration_all']; ?>
                                                            </td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/dot.png' ?>"
                                                                     width="8" height="8" alt=""/></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                    </tbody>
                                                    <thead>
                                                        <tr>
                                                            <th colspan="4" style="text-align:left;"><?php $language_based_content['support_training_all']; ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td style="text-align:left;"><?php $language_based_content['support_all']; ?></td>
                                                            <td><?php echo $language_based_content['help_desk_email_all']; ?></td>
                                                            <td><?php echo $language_based_content['priority_email_all']; ?></td>
                                                            <td><?php echo $language_based_content['account_manager_all']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['online_workshop_all']; ?></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['onsite_professional_dev_all']; ?>
                                                            </td>
                                                            <td></td>
                                                            <td><?php echo $language_based_content['contact_us_box_all']; ?></td>
                                                            <td><img src="<?php echo $this->webroot . 'app/img/tick.png' ?>"
                                                                     width="18" height="14" alt=""/></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;"><?php echo $language_based_content['payment_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_all']; ?></td>
                                                            <td><?php echo $language_based_content['credit_card_po_check_transfer_all']; ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <p style="text-align:center; font-size:14px;"><?php echo $language_based_content['email_us_at_all']; ?><?php echo $this->custom->get_site_settings('static_emails')['sales']; ?><?php echo $language_based_content['call_us_at_all']; ?><strong>888-601-6786</strong></p>
                        </div>
                    </li>

                    <li id="billing" data-content="billing" <?php
                    if (isset($tab) && $tab == 5) {
                        echo 'class="selected"';
                    }
                    ?>>

                        <div class="billing_container">
                            <h1 style="margin-bottom: 20px;"><?php echo $language_based_content['account_billing_all']; ?></h1>
                            <div class="billing_left">
                                <div class="plan_box">
                                    <h2><?php echo $language_based_content['current_plan_all']; ?></h2>
                                    <?php if (!empty($myAccount['plans']['category'])): ?>
                                        <b><?php echo((isset($myAccount['plans']) && isset($myAccount['plans']['category'])) ? $myAccount['plans']['category'] : 'Trial'); ?>
                                            |</b> <?php endif; ?>
                                    <span><?php echo((isset($myAccount['plans']) && isset($myAccount['plans']['plan_type'])) ? $myAccount['plans']['plan_type'] : 'Trial Mode'); ?></span>
                                </div>
                                <div class="user_storage_box">
                                    <div class="UserStorage_left"><?php echo $language_based_content['total_users_all']; ?></div>
                                    <?php if (!empty($myAccount['plans']['category'])): ?>
                                        <div class="UserStorage_right"><?php echo((isset($myAccount['plans']) && isset($myAccount['plans']['users'])) ? $myAccount['Account']['plan_qty'] : $trial_users); ?></div>
                                    <?php else : ?>
                                        <div class="UserStorage_right"><?php echo((isset($myAccount['plans']) && isset($myAccount['plans']['users'])) ? $myAccount['plans']['users'] : $trial_users); ?></div>
                                    <?php endif; ?>

                                    <div class="clear"></div>
                                    <?php
                                    if (!empty($myAccount['plans']['category'])) {
                                        $max_users = ((isset($myAccount['plans']) && isset($myAccount['plans']['users'])) ? $myAccount['Account']['plan_qty'] : $trial_users);
                                    } else {
                                        $max_users = ((isset($myAccount['plans']) && isset($myAccount['plans']['users'])) ? $myAccount['plans']['users'] : $trial_users);
                                    }
                                    ?>
                                    <?php
                                    $progress_bill_width = round(($totalUsers / $max_users) * 100);
                                    if ($progress_bill_width > 100) {
                                        $progress_bill_width = 100;
                                    }
                                    ?>
                                    <div class="progress_bill"
                                         style="width:<?php echo $progress_bill_width; ?>%;"></div>
                                </div>
                                <div class="user_storage_box" style="margin-top:-1px;">
                                    <div class="UserStorage_left"><?php echo $language_based_content['total_storage_all']; ?></div>
                                    <?php if (!empty($myAccount['plans']['category'])): ?>
                                        <div class="UserStorage_right"><?php echo(isset($myAccount['plans']['storage']) ? $myAccount['plans']['storage'] * $myAccount['Account']['plan_qty'] : $trial_storage); ?>
                                            <span class="gb_box">GB</span></div>
                                    <?php else : ?>
                                        <div class="UserStorage_right"><?php echo(isset($myAccount['plans']['storage']) ? $myAccount['plans']['storage'] : $trial_storage); ?>
                                            <span class="gb_box">GB</span></div>
                                    <?php endif; ?>
                                    <div class="clear"></div>
                                    <div class="progress_bill"
                                         style="width:<?php echo $storageUsed != '' ? round($storageUsed) . "%" : '0%'; ?>"></div>
                                </div>
                                <?php if (isset($selected_plan) && count($selected_plan) > 0): ?>
                                    <div class="datagrid" style="margin-top:20px;">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <thead>
                                                <tr>
                                                    <th colspan="2" style="padding:10px;">
                                            <div style="font-size:12px;"><?php echo $language_based_content['purchase_small_all']; ?></div>
                                            <span style="font-size:13px;"><?php echo $selected_plan['Plans']['category']; ?>
                                                |</span> <?php echo $selected_plan['Plans']['plan_type']; ?>
                                            </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td colspan="2" class="price_box">$<?php
                                                        if ($subscription_data['monthly'] == 1) {
                                                            echo $selected_plan['Plans']['price'];
                                                        } else {
                                                            echo $selected_plan['Plans']['price_yearly'];
                                                        }
                                                        ?><br/> <span>mo/user</span></td>
                                                </tr>
                                                <tr <?php
                                                if ($subscription_data['monthly'] == 0) {
                                                    echo 'style="display:none;"';
                                                }
                                                ?>>
                                                    <td style="text-align:left;"><?php echo $language_based_content['monthly_all']; ?></td>
                                                    <td class="big_text" style="text-align:left;border-left:0px;">
                                                        $<?php echo $selected_plan['Plans']['price'] * $subscription_data['quantity']; ?></td>
                                                </tr>
                                                <tr <?php
                                                if ($subscription_data['monthly'] == 1) {
                                                    echo 'style="display:none;"';
                                                }
                                                ?>>
                                                    <td style="text-align:left;">Billed Annually
                                                        <br/><span
                                                            class="red_text_settings">You Save <?php echo 100 - round(((12 * $selected_plan['Plans']['price_yearly'] * $subscription_data['quantity']) / (12 * $selected_plan['Plans']['price'] * $subscription_data['quantity'])) * 100); ?>
                                                            %</span>
                                                    </td>
                                                    <td class="big_text" style="text-align:left;border-left:0px;">
                                                        $<?php echo(12 * $selected_plan['Plans']['price_yearly'] * $subscription_data['quantity']); ?>
                                                        <span>/year</span></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align:left;"><?php echo $language_based_content['total_users_all']; ?>

                                                    </td>
                                                    <td class="big_text"
                                                        style="text-align:left;border-left:0px;"><?php echo($subscription_data['quantity']); ?></td>
                                                </tr>

                                                <tr>
                                                    <td style="text-align:left;"><?php echo $language_based_content['total_storage_all']; ?>

                                                    </td>
                                                    <td class="big_text" style="text-align:left;border-left:0px;">
                                                        <?php echo($selected_plan['Plans']['storage'] * $subscription_data['quantity']); ?>
                                                        GB
                                                    </td>

                                                </tr>


                                            </tbody>
                                        </table>
                                    </div>
                                <?php endif; ?>
                                <div class="change_plan_btn">
                                    <button id="change_plan" type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="red_browse"><?php echo $language_based_content['change_plan_all']; ?></button>
                                </div>
                                <?php if (!empty($subscription_data)): ?>
                                    <div class="reset_btn"><a id="clear_cart"
                                                              href="<?php echo $this->base . '/customer/clear_cart/' . $account_id ?>"
                                                              class="red_browse"><?php echo $language_based_content['clear_cart_all']; ?></a></div>
                                    <?php endif; ?>
                            </div>

                            <?php
                            //                               $new_checked = '';
                            //                               $prev_checked = '';
                            //                               if (isset($customer_info['plan_info']['Plans']) && count($customer_info['plan_info']['Plans']) > 0 && isset($customer_info['card_info']) && count($customer_info['card_info']) > 0):
                            //                                   $prev_checked = 'checked="checked"';
                            //                               else:
                            //                                   $new_checked = 'checked="checked"';
                            //                               endif;
                            ?>


                            <div class="billing_right">

                                <!--                <div class="settings_12">
                                                        <input type="radio" id='prevous' <?php //if (empty($customer_info['plan_info'])) echo "disabled='disabled'";                                                                                                ?> <?php //echo $prev_checked;                                                                                               ?> class="radio" value="old" name="subscription_type"> Use Previous Card Information &nbsp;&nbsp;&nbsp;
                                                    <input type="radio" id='new' <?php //echo $new_checked;                                                                                                ?> class="radio" value="new" name="subscription_type"> Use New Card Information
                                                </div>-->
                                <?php if (!empty($subscription_data_table)): ?>
                                    <div class="settings_12"><b><?php echo $language_based_content['subscription_billing_date_all']; ?> <?php echo date('M d, Y', strtotime($subscription_data_table['AccountBraintreeSubscription']['Subscription_start_date'])); ?>
                                            . <?php echo $language_based_content['it_will_renew_all']; ?><?php echo date('M d, Y', strtotime($subscription_data_table['AccountBraintreeSubscription']['Subscription_end_date'])); ?>
                                            .
                                            <!--and your subscription's ending date is <?php //echo date('M d,Y', strtotime("-1 day", strtotime($subscription_data_table['AccountBraintreeSubscription']['Subscription_end_date'])))                                                                                 ?>-->
                                        </b></div>
                                <?php endif; ?>

                                <?php if ($customer_info && count($customer_info) > 0 && !empty($subscription_data)): ?>
                                    <?php
                                    if (!empty($customer_info['plan_info'])) :
                                        ?>
                                        <div id="use-previous-info">
                                            <form accept-charset="UTF-8"
                                                  action="<?php echo $this->base . '/customer/genrateSubscription/' . $account_id . '/4' ?>"
                                                  method="post">
                                                <input type="hidden" name="check_out_type" value="old"
                                                       class="check-out-type"/>
                                                       <?php if (isset($selected_plan) && count($selected_plan) > 0): ?>

                                                    <?php if ($subscription_data['monthly'] == 0): ?>
                                                        <input type="hidden" name="plan_price"
                                                               value="<?php echo 12 * $selected_plan['Plans']['price_yearly'] * $subscription_data['quantity']; ?>"/>
                                                           <?php endif; ?>

                                                    <?php if ($subscription_data['monthly'] == 1): ?>
                                                        <input type="hidden" name="plan_price"
                                                               value="<?php echo $selected_plan['Plans']['price'] * $subscription_data['quantity']; ?>"/>
                                                           <?php endif; ?>

                                                    <input type="hidden" name="plan_quantity"
                                                           value="<?php echo $subscription_data['quantity']; ?>"/>

                                                <?php endif; ?>
                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['first_name_all']; ?></label>
                                                    <input class="size-medium" type="text" name="first_name"
                                                           value="<?php echo($customer_info['customer_info']['first_name'] != '' ? $customer_info['customer_info']['first_name'] : ''); ?>"/>

                                                </div>

                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['last_name_all']; ?></label>
                                                    <input class="size-medium" type="text" name="last_name"
                                                           value="<?php echo($customer_info['customer_info']['last_name'] != '' ? $customer_info['customer_info']['last_name'] : ''); ?>"/>

                                                </div>

                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['email_all']; ?></label>
                                                    <input class="size-medium" type="text" name="email"
                                                           value="<?php echo($customer_info['customer_info']['email'] != '' ? $customer_info['customer_info']['email'] : ''); ?>"/>

                                                </div>

                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['account_name_all']; ?></label>
                                                    <input class="size-medium" type="text" name="company"
                                                           value="<?php echo($customer_info['customer_info']['company'] != '' ? $customer_info['customer_info']['company'] : ''); ?>"/>

                                                </div>


                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['billing_address_all']; ?>*</label>
                                                    <input name="address" id="address" placeholder=""
                                                           required="required"
                                                           value="<?php echo($customer_info['customer_info']['address'] != '' ? $customer_info['customer_info']['address'] : ''); ?>"
                                                           type="text">
                                                </div>
                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['state_all']; ?>*</label>
                                                    <div class="input-group select size-full inline">
                                                        <select name="state"><?php echo StateDropdown($customer_info['customer_info']['state'], 'name'); ?></select>
                                                    </div>
                                                </div>
                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['city_all']; ?>*</label>
                                                    <input name="city" required="required"
                                                           value="<?php echo($customer_info['customer_info']['city'] != '' ? $customer_info['customer_info']['city'] : ''); ?>"
                                                           type="text">
                                                </div>
                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['zip_code_all']; ?>*</label>
                                                    <input name="zip_code" required="required"
                                                           value="<?php echo($customer_info['customer_info']['zip_code'] != '' ? $customer_info['customer_info']['zip_code'] : ''); ?>"
                                                           type="text">
                                                </div>


                                                <input type="hidden" name="customer_id"
                                                       value="<?php echo($customer_info['customer_info']['id'] != '' ? $customer_info['customer_info']['id'] : ''); ?>"/>

                                                <h1 style="text-align:left;"><?php echo $language_based_content['payment_details_all']; ?></h1>

                                                <div class="input-group">
                                                    <label>Current Plan Title</label>
                                                    <input class="size-medium" type="text" name="plan"
                                                           readonly="readonly"
                                                           value=" <?php echo ($customer_info['plan_info']['Plans']['plan_type'] != '') ? $customer_info['plan_info']['Plans']['plan_type'] : ''; ?>"/>
                                                    <input class="size-medium" type="hidden" name="planId"
                                                           readonly="readonly"
                                                           value=" <?php echo ($new_plan['plan_id'] != '') ? $new_plan['plan_id'] : ''; ?>"/>
                                                </div>

                                                <div class="input-group">
                                                    <label><?php echo $language_based_content['card_number_all']; ?></label>
                                                    <input class="size-medium" type="text" name="card_mask"
                                                           readonly="readonly"
                                                           value="<?php echo($customer_info['card_info']->maskedNumber != '' ? $customer_info['card_info']->maskedNumber : ''); ?>"/><?php
                                                           echo $this->Html->image($customer_info['card_info']->imageUrl, array('style ="border:0px;margin-left:5px;margin-top:5px;"'));
                                                           ?>
                                                    <input class="size-medium" type="hidden" name="token"
                                                           readonly="readonly"
                                                           value=" <?php echo ($customer_info['card_info']->token != '') ? $customer_info['card_info']->token : ''; ?>"/>
                                                </div>


                                                <div class="clear"></div>
                                                <h1 style="text-align:left;"><?php echo $language_based_content['enter_payment_details_all']; ?></h1>
                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['card_number_all']; ?></label>
                                                    <input id="credit_card_number" name="credit_card[number]"
                                                           type="text">
                                                </div>
                                                <div class="field_box6">
                                                    <div class="field_box6">
                                                        <label><?php echo $language_based_content['expiration_date_all']; ?></label>
                                                        <input type="text" placeholder="MM/YY" name="card_expiration">
                                                    </div>
                                                    <div class="field_box6">
                                                        <label>CVV</label>
                                                        <input type="text" id="credit_card_cvv" name="credit_card[cvv]">
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="settings_12"><?php echo $language_based_content['for_10_or_more_all']; ?><a data-toggle="modal"
                                                                                                   data-target="#email_ob"
                                                                                                   style="cursor: pointer;"><?php echo $language_based_content['sales_all']; ?></a>
                                                    <?php echo $language_based_content['for_an_invoice']; ?>
                                                </div>
                                                <div class="settings_12"><img
                                                        src="<?php echo $this->webroot . 'app/img/braintree.png' ?>"
                                                        width="132" height="33"
                                                        style="float: left;margin-right: 15px;margin-top: 8px;"
                                                        alt=""/><span style="top: 9px;position: relative;"><?php echo $language_based_content['our_payment_billing_all']; ?></span></div>
                                                <div class="clear"></div>


                                                <div class="input-group">
                                                    <input type="submit" value="<?php echo $language_based_content['update_plan_all']; ?>" class="setting_greenBtn"/>

                                                </div>
                                            </form>
                                        </div>
                                        <?php
                                    endif;
                                    ?>
                                <?php endif; ?>


                                <?php if ($customer_info && count($customer_info) > 0 && empty($subscription_data)): ?>
                                    <?php
                                    if (!empty($customer_info['plan_info'])) :
                                        ?>
                                        <div id="use-previous-info">
                                            <form accept-charset="UTF-8"
                                                  action="<?php echo $this->base . '/customer/genrateSubscription/' . $account_id . '/4' ?>"
                                                  method="post">
                                                <input type="hidden" name="check_out_type" value="update"
                                                       class="check-out-type"/>

                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['first_name_all']; ?></label>
                                                    <input class="size-medium" type="text" name="first_name"
                                                           value="<?php echo($customer_info['customer_info']['first_name'] != '' ? $customer_info['customer_info']['first_name'] : ''); ?>"/>

                                                </div>

                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['last_name_all']; ?></label>
                                                    <input class="size-medium" type="text" name="last_name"
                                                           value="<?php echo($customer_info['customer_info']['last_name'] != '' ? $customer_info['customer_info']['last_name'] : ''); ?>"/>

                                                </div>

                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['email_all']; ?></label>
                                                    <input class="size-medium" type="text" name="email"
                                                           value="<?php echo($customer_info['customer_info']['email'] != '' ? $customer_info['customer_info']['email'] : ''); ?>"/>

                                                </div>

                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['account_name_all']; ?></label>
                                                    <input class="size-medium" type="text" name="company"
                                                           value="<?php echo($customer_info['customer_info']['company'] != '' ? $customer_info['customer_info']['company'] : ''); ?>"/>

                                                </div>

                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['billing_address_all']; ?>*</label>
                                                    <input name="address" id="address" placeholder=""
                                                           required="required"
                                                           value="<?php echo($customer_info['customer_info']['address'] != '' ? $customer_info['customer_info']['address'] : ''); ?>"
                                                           type="text">
                                                </div>
                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['state_all']; ?>*</label>
                                                    <div class="input-group select size-full inline">
                                                        <select name="state"><?php echo StateDropdown($customer_info['customer_info']['state'], 'name'); ?></select>
                                                    </div>
                                                </div>
                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['city_all']; ?>*</label>
                                                    <input name="city" required="required"
                                                           value="<?php echo($customer_info['customer_info']['city'] != '' ? $customer_info['customer_info']['city'] : ''); ?>"
                                                           type="text">
                                                </div>
                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['zip_code_all']; ?>*</label>
                                                    <input name="zip_code" required="required"
                                                           value="<?php echo($customer_info['customer_info']['zip_code'] != '' ? $customer_info['customer_info']['zip_code'] : ''); ?>"
                                                           type="text">
                                                </div>


                                                <input type="hidden" name="customer_id"
                                                       value="<?php echo($customer_info['customer_info']['id'] != '' ? $customer_info['customer_info']['id'] : ''); ?>"/>
                                                <input class="size-medium" type="hidden" name="token"
                                                       readonly="readonly"
                                                       value=" <?php echo ($customer_info['card_info']->token != '') ? $customer_info['card_info']->token : ''; ?>"/>
                                                <div class="clear"></div>
                                                <h1 style="text-align:left;"><?php echo $language_based_content['enter_payment_details_all']; ?></h1>
                                                <div class="field_box6">
                                                    <label><?php echo $language_based_content['card_number_all']; ?></label>
                                                    <input id="credit_card_number" name="credit_card[number]"
                                                           type="text" required>
                                                </div>
                                                <div class="field_box6">
                                                    <div class="field_box6">
                                                        <label><?php echo $language_based_content['expiration_date_all']; ?></label>
                                                        <input type="text" placeholder="MM/YY" required
                                                               name="card_expiration">
                                                    </div>
                                                    <div class="field_box6">
                                                        <label>CVV</label>
                                                        <input type="text" id="credit_card_cvv" required
                                                               name="credit_card[cvv]">
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                                <div class="settings_12"><?php echo $language_based_content['for_10_or_more_all']; ?><a data-toggle="modal"
                                                                                                   data-target="#email_ob"
                                                                                                   style="cursor: pointer;"><?php echo $language_based_content['sales_all']; ?></a>
                                                    <?php echo $language_based_content['for_an_invoice']; ?>
                                                </div>
                                                <div class="settings_12"><img
                                                        src="<?php echo $this->webroot . 'app/img/braintree.png' ?>"
                                                        width="132" height="33"
                                                        style="float: left;margin-right: 15px;margin-top: 8px;"
                                                        alt=""/><span style="top: 9px;position: relative;"><?php echo $language_based_content['our_payment_billing_all']; ?></span></div>
                                                <div class="clear"></div>


                                                <div class="input-group">
                                                    <input type="submit" value="<?php echo $language_based_content['update_all']; ?>" class="setting_greenBtn"/>

                                                </div>
                                            </form>
                                        </div>
                                        <?php
                                    endif;
                                    ?>
                                <?php endif; ?>

                                <div id="use-new-info"
                                     <?php if (isset($customer_info['plan_info']['Plans']) && count($customer_info['plan_info']['Plans']) > 0 && isset($customer_info['card_info']) && count($customer_info['card_info']) > 0): ?>style="display: none"<?php endif; ?>>
                                    <form accept-charset="UTF-8"
                                          action="<?php echo $this->base . '/customer/genrateSubscription/' . $account_id . '/4' ?>"
                                          method="post">
                                        <input type="hidden" name="check_out_type" value="new" class="check-out-type"/>
                                        <?php if (isset($selected_plan) && count($selected_plan) > 0): ?>

                                            <?php if ($subscription_data['monthly'] == 0): ?>
                                                <input type="hidden" name="plan_price"
                                                       value="<?php echo 12 * $selected_plan['Plans']['price_yearly'] * $subscription_data['quantity']; ?>"/>
                                                   <?php endif; ?>

                                            <?php if ($subscription_data['monthly'] == 1): ?>
                                                <input type="hidden" name="plan_price"
                                                       value="<?php echo $selected_plan['Plans']['price'] * $subscription_data['quantity']; ?>"/>
                                                   <?php endif; ?>

                                            <input type="hidden" name="plan_quantity"
                                                   value="<?php echo $subscription_data['quantity']; ?>"/>

                                        <?php endif; ?>
                                        <input class="size-medium" type="hidden" name="planId" readonly="readonly"
                                               value=" <?php echo ($new_plan['plan_id'] != '') ? $new_plan['plan_id'] : ''; ?>"/>
                                        <div class="field_box6">
                                            <label><?php echo $language_based_content['first_name_all']; ?>*</label>
                                            <input name="first_name" type="text" required>
                                        </div>
                                        <div class="field_box6">
                                            <label><?php echo $language_based_content['last_name_all']; ?>*</label>
                                            <input name="last_name" type="text" required>
                                        </div>
                                        <div class="field_box6">
                                            <label>Email*</label>
                                            <input name="email" type="text" required>
                                        </div>

                                        <div class="field_box6">
                                            <label><?php echo $language_based_content['account_name_all']; ?>*</label>
                                            <input name="company_name" type="text" required>
                                        </div>
                                        <div class="field_box6">
                                            <label><?php echo $language_based_content['billing_address_all']; ?>*</label>
                                            <input name="address" id="address" placeholder="" required="required"
                                                   value="" type="text">
                                        </div>
                                        <div class="field_box6">
                                            <label><?php echo $language_based_content['state_all']; ?>*</label>
                                            <div class="input-group select size-full inline">
                                                <select name="state"><?php echo StateDropdown('Alaska', 'name'); ?></select>
                                            </div>
                                        </div>
                                        <div class="field_box6">
                                            <label><?php echo $language_based_content['city_all']; ?>*</label>
                                            <input name="city" required="required" value="" type="text">
                                        </div>
                                        <div class="field_box6">
                                            <label><?php echo $language_based_content['zip_code_all']; ?>*</label>
                                            <input name="zip_code" required="required" value="" type="text">
                                        </div>

                                        <div class="clear"></div>
                                        <h1 style="text-align:left;"><?php echo $language_based_content['enter_payment_details_all']; ?></h1>
                                        <div class="field_box6">
                                            <label><?php echo $language_based_content['card_number_all']; ?></label>
                                            <input id="credit_card_number" name="credit_card[number]" type="text"
                                                   required>
                                        </div>
                                        <div class="field_box6">
                                            <div class="field_box6">
                                                <label><?php echo $language_based_content['expiration_date_all']; ?></label>
                                                <input type="text" placeholder="MM/YY" required name="card_expiration">
                                            </div>
                                            <div class="field_box6">
                                                <label>CVV</label>
                                                <input type="text" id="credit_card_cvv" required
                                                       name="credit_card[cvv]">
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                        <div class="settings_12"><?php echo $language_based_content['for_10_or_more_all']; ?><a data-toggle="modal"
                                                                                  data-target="#email_ob"
                                                                                  style="cursor: pointer;"><?php echo $language_based_content['enter_payment_details_all']; ?></a> <?php echo $language_based_content['for_an_invoice']; ?>
                                        </div>
                                        <div class="settings_12"><img
                                                src="<?php echo $this->webroot . 'app/img/braintree.png' ?>"
                                                width="132" height="33"
                                                style="float: left;margin-right: 15px;margin-top: 8px;"
                                                alt=""/><span style="top: 9px;position: relative;"><?php echo $language_based_content['our_payment_billing_all']; ?></span></div>
                                        <div class="clear"></div>
                                        <div class="buy_box">
                                            <button type="submit" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="setting_greenBtn" style="width:175px;"><?php echo $language_based_content['buy_all']; ?>
                                            </button>
                                            <img src="<?php echo $this->webroot . 'app/img/card.jpg' ?>" width="167"
                                                 height="34" alt=""/></div>
                                    </form>


                                </div>
                            </div>
                            <div class="clear"></div>
                            <h1 style="margin-top:25px;"><?php echo $language_based_content['billing_history_all']; ?></h1>

                            <?php if (!empty($transactions)) : ?>
                                <div class="datagrid">
                                    <table>
                                        <thead>
                                            <tr>
                                                <th><?php echo $language_based_content['invoice_number_all']; ?></th>
                                                <th><?php echo $language_based_content['due_date_all']; ?></th>
                                                <th><?php echo $language_based_content['status_all']; ?></th>
                                                <th><?php echo $language_based_content['amount_all']; ?></th>
                                                <th><?php echo $language_based_content['invoice_all']; ?></th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($transactions as $transaction): ?>
                                                                                                                                                                                                                    <!--                    	<li><a href="#"><?php //echo $transaction['date'];                                                                                               ?> - $<?php //echo $transaction['amount'];                                                                                               ?></a></li>-->

                                                <tr>
                                                    <td><?php echo $transaction['id']; ?></td>
                                                    <td><?php echo $transaction['date']; ?></td>
                                                    <td><?php echo $transaction['status']; ?></td>
                                                    <td>$<?php echo $transaction['amount']; ?></td>
                                                    <td>
                                                        <a href="<?php echo $this->base . '/accounts/find_transaction/' . $transaction['id']; ?>">Download</a>
                                                    </td>

                                                </tr>


                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php else: ?>
                                <div class="billing_history">
                                    <ul>
                                        <li><a href="#">No Billing History</a></li>
                                    </ul>
                                </div>
                            <?php endif; ?>


                        </div>
                    </li>

                    <li data-content="cancel" <?php
                    if (isset($tab) && $tab == 6) {
                        echo 'class="selected"';
                    }
                    ?>>
                        <div class="Settings_cancel_account">
                            <h1><?php echo $language_based_content['sure_to_cancel_account_all']; ?></h1>
                            <p><?php echo $language_based_content['thanks_for_trying_sibme_all']; ?></p>
                            <div><img src="<?php echo $this->webroot . 'app/img/cancel_accountbig.png' ?>" width="184"
                                      height="158" style="margin:20px 0px;" alt=""/></div>
                            <p>
                                <?php echo $language_based_content['cancelling_your_account_all']; ?><br/>
                                <?php echo $language_based_content['do_you_really_want_all']; ?>
                            </p>
                            <div class="settings_bottom_btn">
                                <a href="<?php echo $this->base . '/accounts/cancel_subscription/' . $account_id; ?>"
                                   data-confirm="<?php $language_based_content['sure_to_cancel_account_all']; ?>" data-method="delete"
                                   rel="nofollow" class="setting_redBtn"><?php echo $language_based_content['cancel_your_account_all']; ?></a><br/>
                                <a href="<?php echo $this->base . '/Dashboard/home'; ?>" class="never_mind"><?php echo $language_based_content['nevermind_let_me_all']; ?></a>
                            </div>
                        </div>
                    </li>

                <?php endif; ?>

                <?php // Start of Global Export ?>
                <?php if ($myAccount['Account']['in_trial'] != '1'): ?>
                    <li data-content="global_export" <?php
                    if (isset($tab) && $tab == 8) {
                        echo 'class="selected"';
                    }
                    ?>>
                        <div class="global_export_detailBox">
                            <h3><?php echo $language_based_content['global_export_all_global'];?></h3>
                            <div class="global_export_content_container">
                                <div class="general_export">
                                    <div class="ge_header"><?php echo $language_based_content['general_export_all_global'];?></div>
                                    <form id="frm_general_export" action="<?php echo $this->base . '/accounts/global_export/' . $account_id . '/8' ?>" method="post">
                                        <input type="hidden" id="file_type" name="file_type" value="">
                                        <input type="hidden" id="export_type" name="export_type" value="general">
                                        <ul class="fields_cls">
                                            <li class="checkmark2">
                                                <p><?php echo $language_based_content['account_name_all_global'];?></p>
                                            </li>
                                            <li class="checkmark2">
                                                <p><?php echo $language_based_content['account_guid_all_global'];?></p>
                                            </li>
                                            <li class="checkmark2">
                                                <p><?php echo $language_based_content['huddle_name_all_global'];?></p>
                                            </li>
                                            <li class="checkmark2">
                                                <p><?php echo $language_based_content['huddle_guid_all_global'];?></p>
                                            </li>
                                            <li class="checkmark2">
                                                <p><?php echo $language_based_content['video_name_all_global'];?></p>
                                            </li>
                                            <li class="checkmark2">
                                                <p><?php echo $language_based_content['video_guid_all_global'];?></p>
                                            </li>
                                            <li class="checkmark2">
                                                <p><?php echo $language_based_content['user_guid_all_global'];?></p>
                                            </li>
                                            <li class="checkmark2">
                                                <p><?php echo $language_based_content['intitution_id_all_global'];?></p>
                                            </li>
                                            <li class="checkmark2">
                                                <p><?php echo $language_based_content['username_all_global'];?></p>
                                            </li>
                                            <li class="checkmark2">
                                                <p><?php echo $language_based_content['first_name_all_global'];?></p>
                                            </li>
                                            <li class="checkmark2">
                                                <p><?php echo $language_based_content['last_name_all_global'];?></p>
                                            </li>
                                            <li class="checkmark2">
                                                <p><?php echo $language_based_content['user_type_all_global'];?></p>
                                            </li>
                                            <li>
                                                <label class="checkboxcls"><?php echo $language_based_content['comment_all_global'];?>
                                                    <input name="export_fields[]" value="comment" type="checkbox" >
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkboxcls"><?php echo $language_based_content['time_stamp_all_global'];?>
                                                    <input name="export_fields[]" value="time_stamp" type="checkbox" >
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkboxcls"><?php echo $language_based_content['date_stamp_all_global'];?>
                                                    <input name="export_fields[]" value="date_stamp" type="checkbox" >
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkboxcls"><?php echo $language_based_content['framework_name_all_global'];?>
                                                    <input name="export_fields[]" value="framework_name" type="checkbox" >
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkboxcls"><?php echo $language_based_content['tagged_standards_all_global'];?>
                                                    <input name="export_fields[]" value="tagged_standards" type="checkbox" >
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkboxcls"><?php echo $language_based_content['custom_marker_tags_all_global'];?>
                                                    <input name="export_fields[]" value="custom_marker_tags" type="checkbox" >
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                            <li>
                                                <label class="checkboxcls"><?php echo $language_based_content['attachment_file_name_all_global'];?>
                                                    <input name="export_fields[]" value="attachment_file_names" type="checkbox" >
                                                    <span class="checkmark"></span>
                                                </label>
                                            </li>
                                        </ul>
                                    </form>
                                </div>
                                <div class="performance_level_export">
                                    <div class="pl_header"><?php echo $language_based_content['performance_level_data_export_global'];?></div>
                                    <ul class="fields_cls">
                                        <li class="checkmark2">
                                            <p><?php echo $language_based_content['account_name_all_global'];?></p>
                                        </li>
                                        <li class="checkmark2">
                                            <p><?php echo $language_based_content['account_guid_all_global'];?></p>
                                        </li>
                                        <li class="checkmark2">
                                            <p><?php echo $language_based_content['huddle_name_all_global'];?></p>
                                        </li>
                                        <li class="checkmark2">
                                            <p><?php echo $language_based_content['huddle_guid_all_global'];?></p>
                                        </li>
                                        <li class="checkmark2">
                                            <p><?php echo $language_based_content['video_name_all_global'];?></p>
                                        </li>
                                        <li class="checkmark2">
                                            <p><?php echo $language_based_content['video_guid_all_global'];?></p>
                                        </li>
                                        <li class="checkmark2">
                                            <p><?php echo $language_based_content['coachee_user_guid_global'];?></p>
                                        </li>
                                        <li class="checkmark2">
                                            <p><?php echo $language_based_content['coaches_export_global'];?></p>
                                        </li>
                                        <li class="checkmark2">
                                            <p><?php echo $language_based_content['standard_export_global'];?></p>
                                        </li>
                                        <li class="checkmark2">
                                            <p><?php echo $language_based_content['rating_export_global'];?></p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clear"></div>

                            </div>
                            <div class="clear"></div>
                            <div class="global_export_buttons_container">
                                <div class="general_export">
                                    <div style="display: none;">
                                        <input type="radio" name="ge_file_type" value="excel" checked>
                                        <label>Excel</label>
                                        <input type="radio" name="ge_file_type" value="csv">
                                        <label>CSV</label>
                                    </div>
                                    <button id="btn_ge_export" class="setting_greenBtn"><?php echo $language_based_content['export_global'];?></button>
                                </div>
                                <div class="performance_level_export">
                                    <form id="frm_performance_level_export" action="<?php echo $this->base . '/accounts/global_export/' . $account_id . '/8' ?>" method="post">
                                        <input type="hidden" id="export_type" name="export_type" value="performance_level">
                                        <div style="display: none;">
                                            <input type="radio" name="file_type" value="excel" checked>
                                            <label>Excel</label>
                                            <input type="radio" name="file_type" value="csv">
                                            <label>CSV</label>
                                        </div>
                                        <button id="btn_pl_export" class="setting_greenBtn"><?php echo $language_based_content['export_global'];?></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </li>


<style>
    .general_export { 
        float: left;    
        border-right: 1px solid #ddd;
        margin-right: 55px;
        -webkit-box-flex: 0;
        -ms-flex: 0 0 40%;
        flex: 0 0 40%;
        max-width: 40%; padding-right:15px;
    }
    .global_export_buttons_container .general_export{ border:0; width:40% !important; margin-right:55px !important; text-align: right; padding-right: 45px;}
    .global_export_buttons_container .performance_level_export{ padding-left:15px;}
    .performance_level_export {  
        float: left;
        -webkit-box-flex: 0;
        -ms-flex: 0 0 40%;
        flex: 0 0 40%;
        max-width: 40%;
    }
    .global_export_content_container ul li { display:block !important}
    .global_export_content_container ul { padding: 0 0 0 15px !important;}
    .global_export_content_container ul li { 
        padding:8px 0 0 0 !important;     
        font-size: 14px;
        margin-bottom: 8px;
        color: #000;
        font-weight: 600;
    }
    .global_export_content_container ul li p { margin:0;}

    .global_export_content_container ul li.checkmark2{ 
        background:url(https://demosite.firstdegree.golf/wp-content/uploads/sites/13/2019/03/checkmark.png) no-repeat left top;
        padding-left: 24px !important;
        background-position: 1px 11px !important;
        background-size: 12px !important;
    }
    
    /* The container */
    .checkboxcls {
        display: block;
        position: relative;
        padding-left: 25px;
        margin: 0;
        cursor: pointer;
        /* font-size: 13px; */
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        /* color: #616161; */
        font-size: 14px;
        margin-bottom: 0;
        color: #000;
        font-weight: 600;
        line-height: 14px;
    }

    /* Hide the browser's default checkbox */
    .checkboxcls input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
    }

    /* Create a custom checkbox */
    .checkmark {
    position: absolute;
        top: 0px;
        left: 0;
        height: 16px;
        width: 16px;
        background-color: #fff;
        border: 1px solid #7c7c7c;
        border-radius: 3px;
    }

    /* On mouse-over, add a grey background color */
    .checkboxcls:hover input ~ .checkmark {
    background-color: #fff;
    }

    /* When the checkbox is checked, add a blue background */
    .checkboxcls input:checked ~ .checkmark {
        background-color: #fff;
    
    }

    /* Create the checkmark/indicator (hidden when not checked) */
    .checkmark:after {
    content: "";
    position: absolute;
    display: none;
    }

    /* Show the checkmark when checked */
    .checkboxcls input:checked ~ .checkmark:after {
    display: block;
    }

    /* Style the checkmark/indicator */
    .checkboxcls .checkmark:after {
        left: 5px;
        top: 2px;
        width: 4px;
        height: 9px;
        border: solid #7c7c7c;
        border-width: 0px 2px 2px 0;
        -webkit-transform: rotate(40deg);
        -ms-transform: rotate(40deg);
        transform: rotate(40deg);
    }

    .ge_header, .pl_header{    
        color: #7c7c7c;
        font-weight: 600;
        margin: 0 0 20px 0;
    }
    
    .global_export_content_container { 
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
    }
    .global_export_buttons_container {margin-top: 14px;}
</style>


                <?php endif; ?>
<?php // End of Global Export ?>
                
                <li data-content="rubric" <?php
                if (isset($tab) && $tab == 7) {
                    echo 'class="selected"';
                }
                ?>>
                        <?php if ($sub_tab == 1): ?>
                        <style>
                            th:first-child {
                                width: 140px !important;
                            }

                            td {
                                padding: 10px 10px;
                            }

                            th.ui-th-column div {
                                white-space: normal !important;
                                height: auto !important;
                                padding: 2px;
                            }

                            .ui-jqgrid .ui-jqgrid-resize {
                                height: 100% !important;
                            }

                            .data_grid_adjs {
                                height: auto !important;
                            }

                            .sel_dur_grph {
                                width: 200px;
                                float: left;
                            }

                            .cl_token_field {
                                width: 200px;
                                float: left;
                                margin-left: 5px;
                                margin-right: 5px;
                            }

                            .filter_box select {
                                -moz-appearance: tab-scroll-arrow-forward;
                                vertical-align: top;
                            }

                            .filter_box .tokenfield {
                                display: inline-block;
                            }

                            .token .close {
                                margin-top: 0px;
                                margin-right: 0px;
                            }

                            .token .token-label {
                                max-width: 60px !important;
                            }

                            .tokenfield {
                                min-height: 37px !important;
                            }

                            .tokenfield .token-input {
                                height: 20px !important;
                            }

                            .colCell {
                                position: sticky;
                                left: 0;
                                width: 165px !important;
                                z-index: 9;
                                background: #fff;
                            }

                            #grid_account_name {
                                position: sticky;
                                left: 0;
                                width: 165px !important;
                                z-index: 9;
                                background: #e0e0e0;
                            }

                            .search_mod_class {
                                position: sticky;
                                left: 0;
                                width: 165px !important;
                                z-index: 9;
                                background: #e0e0e0;

                            }

                            #ui-datepicker-div {
                                z-index: 9 !important;
                            }

                            .edtpa_leftBox .ui-jqgrid-view {
                                width: 100% !important;
                            }

                            .edtpa_leftBox .ui-jqgrid {
                                width: 100% !important;
                            }

                            .edtpa_leftBox .ui-jqgrid-hdiv {
                                width: 100% !important;
                            }

                            .edtpa_leftBox .ui-jqgrid-bdiv {
                                width: 100% !important;
                                height: 400px !important;
                            }

                            .edtpa_leftBox .table_footer {
                                width: 100% !important;
                            }

                            .edtpa_leftBox #accUsersAnalytics .ui-jqgrid-hdiv {
                                width: 100% !important;
                            }

                            .edtpa_leftBox .ui-jqgrid .ui-jqgrid-btable {
                                table-layout: unset;
                            }

                            .edtpa_leftBox #jqGrid {
                                width: 100% !important;
                            }

                            .edtpa_leftBox #jqGridPager .ui-pg-input {
                                height: 28px !important;
                                border-radius: 0 !important;
                                width: 35px;
                                padding: 5px;
                                text-align: center;
                            }

                            .ui-jqgrid .ui-pg-selbox {
                                height: 30px;
                            }

                            .ui-jqgrid .ui-pg-table td {
                                padding-bottom: 3px !important;
                            }

                            .edtpa_leftBox {
                                margin-top: 0px;
                            }

                        </style>
                        <style>
                            .edtpa_container {
                                min-height: 750px;
                            }

                            .edtcel3dropdown {
                                position: absolute;
                                top: 36px;
                                z-index: 4;
                                background: #fff;
                                padding: 9px 9px 7px 9px;
                                -webkit-box-shadow: 0px 2px 9px -1px rgba(221, 221, 221, 1);
                                -moz-box-shadow: 0px 2px 9px -1px rgba(221, 221, 221, 1);
                                box-shadow: 0px 2px 9px -1px rgba(221, 221, 221, 1);
                                text-align: left;
                                display: none;
                                border: 1px solid #ddd;
                                width: 80px;
                                right: -18px;
                            }

                            .edtcel3dropdown:before {
                                pointer-events: none;
                                position: absolute;
                                z-index: -1;
                                content: '';
                                border-style: solid;
                                -webkit-transition-duration: 0.3s;
                                transition-duration: 0.3s;
                                -webkit-transition-property: transform;
                                transition-property: transform;
                                left: calc(50% - 10px);
                                top: 0;
                                border-width: 0 7px 7px 7px;
                                border-color: transparent transparent #e1e1e1 transparent;
                                right: 43px;
                                left: inherit;
                                -webkit-transform: translateY(-10px);
                                transform: translateY(-10px);
                                top: 3px;
                            }

                            .edtcel3dropdown ul {
                                margin: 0;
                                padding: 0;
                                list-style: none;
                            }

                            .edtcel3dropdown ul li {
                                margin-bottom: 2px;
                            }

                            .edtcel3dropdown ul li a {
                                font-weight: normal;
                            }

                            img.edt_toggle {
                                cursor: pointer;
                            }

                            .edtpa_leftBox {
                                /*float: left;*/
                                width: 100%;
                                margin-right: 0%;
                            }

                            .edtpa_rightBox {
                                float: right;
                                display: inline-block;
                                width: 27%;
                            }

                            .edtpa_leftBox h1 {
                                margin: 0px 0 15px 0;
                                font-size: 26px;
                            }

                            .edtpa_leftBox table tr th {
                                border-bottom: solid 2px #f2f2f2;
                            }

                            .edtpa_leftBox table td a {
                                color: #00aeef;
                                font-weight: normal;
                                text-decoration: underline;
                            }

                            .edtpa_leftBox table td a:hover {
                                color: #f6701b;
                            }

                            .bread_crum {
                                margin-bottom: 20px;
                                font-size: 13px;
                            }

                            .bread_crum a {
                                font-weight: normal;
                                color: #668dab;
                            }

                            .bread_crum a:after {
                                content: '>';
                                margin: 0px 15px;
                                color: #757575
                            }

                            .table_footer {
                                font-size: 13px;
                                color: #606060;
                                margin-top: 16px;
                            }

                            .table_footer select {
                                padding: 8px;
                                margin-right: 10px;
                                width: 70px;
                                border: solid 1px #f1f1f1;
                                box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.08);
                            }

                            .table_footer ul {
                                float: right;
                                margin: 0px;
                                padding: 0px;
                                list-style: none;
                            }

                            .table_footer li {
                                display: inline-block;
                            }

                            .table_footer li a {
                                padding: 5px 9px;
                                color: #606060;
                                font-weight: normal;
                            }

                            .table_footer li a:hover {
                                color: #f6701b;
                            }

                            .table_footer .active a {
                                background: #5daf46;
                                border-radius: 3px;
                                color: #fff;
                            }

                            .table_footer .active a:hover {
                                color: #fff;
                            }

                            .edtpa_rightBox h1 {
                                margin: 0px;
                                font-size: 22px;
                                margin-top: 36px;
                                margin-bottom: 10px;
                            }

                            .edtpa_rightBox ul {
                                border: solid 2px #e0e0e0;
                                margin: 0px;
                                padding: 0px;
                                list-style: none;
                            }

                            .edtpa_rightBox ul li {
                                padding: 10px 6px;
                                border-bottom: solid 1px #e0e0e0;
                                position: relative;
                            }

                            .edtpa_rightBox ul li h2 {
                                font-weight: normal;
                                color: #616161;
                                font-size: 15px;
                                font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, "sans-serif";
                            }

                            .submitted_label {
                                font-size: 12px;
                                color: #3cb878;
                                font-weight: bold;
                            }

                            .submittedApprval_label {
                                font-size: 12px;
                                color: #8560a8;
                                font-weight: bold;
                            }

                            .submitedEdtpa {
                                font-size: 12px;
                                color: #f26522;
                                font-weight: bold;
                            }

                            .approved_label {
                                position: absolute;
                                right: 0px;
                                top: 0px;
                                background: #8dc63f;
                                color: #fff;
                                font-size: 9px;
                                padding: 3px 12px;
                            }

                            .submission_box li span {
                                font-size: 11px;
                            }

                            .submission_box li span img {
                                width: 18px;
                                height: auto;
                                margin-left: 12px;
                            }

                            .submission_box li:last-child {
                                border-bottom: 0px;
                            }

                            .ui-jqgrid .ui-jqgrid-hbox {
                                padding-right: 0px !important;
                            }

                            .ui-jqgrid .ui-jqgrid-htable {
                                width: 100% !important
                            }

                            .edtpa_leftBox .data_grid_green .ui-jqgrid-hbox table th {
                                background: none !important;
                                border: 0 !important;
                                padding: 7px 5px;
                                width: 150px !important;
                            }

                            .edtpa_leftBox thead tr {
                                background: none !important;
                            }

                            .edtpa_leftBox .ui-jqgrid .ui-jqgrid-htable th div {
                                font-size: 15px !important;
                                font-weight: 800;
                                text-align: left;
                                font-family: "Segoe UI";
                            }

                            .data_grid_green .ui-jqgrid tr.jqgrow td {
                                border-right: 0 !important;
                            }

                            .edtpa_leftBox #jqGridPager {
                                background: none !important;
                            }

                            .edtpa_leftBox #jqGridPager_left {
                                display: none;
                            }

                            .edtpa_leftBox #jqGridPager .ui-pg-input {
                                height: 28px !important;
                                border-radius: 0 !important;
                            }

                            .edtpa_leftBox .ui-pager-control {
                                background: none !important;
                            }

                            .edtpa_leftBox .ui-pg-table td {
                                border: none !important;
                            }

                            .edtpa_leftBox .ui-pager-control {
                                background: none !important;
                            }

                            .edtpa_leftBox .edtpa_secls {
                                background: #f4f4f4;
                                padding: 20px 15px;
                                margin-bottom: 15px;
                            }

                            .edtpa_leftBox .ui-jqgrid {
                                border: 0;
                            }

                            .edtpa_leftBox .ui-jqgrid-hdiv {
                                border-right: 0 !important;
                            }

                            .edtpa_secls input[type=text] {
                                border: 1px solid #c5c5c5;
                                border-radius: 3px;
                                box-shadow: 0 1px 2px rgba(0, 0, 0, 0.09) inset;
                                font-size: 13px;
                                font-weight: bold;
                                color: #b2b2b2;
                                padding: 8px 4px;
                                float: left;
                                margin-right: 18px;
                            }

                            .edtpa_secls span {
                                float: left;
                                margin-right: 18px;
                            }

                            .edtpa_secls span select {
                                height: 32px !important;
                            }

                            .data_grid_green .ui-jqgrid tr.jqgrow td {
                                word-wrap: break-word !important;
                                white-space: initial !important;
                                width: 144px !important;
                            }

                            .cd-tabs-navigation a {
                                height: 60px;
                                line-height: 60px;
                                padding: 0px;
                                font-size: 14px;
                                width: 154px;
                                text-align: center;
                                font-weight: normal;
                                color: #5b5b5b;
                                text-decoration: none;
                            }

                        </style>

                        <!--                        <link rel="stylesheet" href="/css/analytics/lib/js/themes/redmond/green_theme.css"-->
                        <!--                              type="text/css"/>-->


                        <!--                       <link rel="stylesheet" type="text/css"-->
                        <!--                             href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css"/>-->
                        <!--                        <link rel="stylesheet" href="/css/analytics/lib/js/themes/redmond/green_theme.css" type="text/css" />-->
                        <!--                        <script type="text/javascript"-->
                        <!--                                src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>-->

                        <div class="rubric_main edtpa_leftBox">
                            <h3><?php echo $language_based_content['rubrics_all']; ?></h3>
                            <div class="rub_adnew" style="margin-bottom:10px;">
                                <a href="<?php echo $this->base . '/accounts/account_settings_all/' . $account_id . '/7/2' ?>"><?php echo $language_based_content['new_all']; ?></a>
                            </div>
                            <div class="rubric_table data_grid_green data_grid_adjs">
                                <table id="rubrics-table" class="" style="width:100%">
                                    <thead>
                                        <tr class="rubric_th">
                                            <th class="reb_cell1"><?php echo $language_based_content['name_all']; ?></th>
                                            <th class="no-sor"><?php echo $language_based_content['updated_all']; ?></th>
                                            <th class="no-sor"><?php echo $language_based_content['published_all']; ?></th>
                                            <th class="no-sor rub_lasrcell"><?php echo $language_based_content['action_all']; ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (count($framework_list) > 0): ?>
                                            <?php
                                            foreach ($framework_list as $row):
                                                $account_framework_settings = $this->Custom->get_framework_settings($row['AccountTag']['account_tag_id']);
                                                ?>
                                                <tr>
                                                    <td class="reb_cell1"><?php echo $row['AccountTag']['tag_title'] ?></td>
                                                    <td><?php echo!empty($account_framework_settings['AccountFrameworkSetting']['updated_at']) ? $account_framework_settings['AccountFrameworkSetting']['updated_at'] : 'N/A'; ?></td>
                                                    <td><?php echo (!empty($account_framework_settings['AccountFrameworkSetting']['published']) && $account_framework_settings['AccountFrameworkSetting']['published'] == '1') ? $language_based_content['published_all'] : 'N/A'; ?></td>
                                                    <td class="rub_lasrcell"><img class="rub_dots" data-status="false"
                                                                                  src="<?php echo $this->webroot . 'app/img/list_icons.svg' ?>"
                                                                                  alt=""/>
                                                        <div class="rub_options">
                                                            <ul>
                                                                <li>
                                                                    <a href="<?php echo $this->base . '/accounts/account_settings_all/' . $account_id . '/7/2/0/' . $row['AccountTag']['account_tag_id']; ?>"><?php echo $language_based_content['edit_all']; ?></a>
                                                                </li>
                                                                <li>
                                                                    <a data-toggle="modal"
                                                                       data-target="#delete_rubric_<?php echo $row['AccountTag']['account_tag_id'] ?>"
                                                                       data-backdrop="static" data-keyboard="false"
                                                                       href="javascript:void(0);"><?php echo $language_based_content['delete_all']; ?></a>
                                                                </li>

                                                                <?php if (empty($account_framework_settings['AccountFrameworkSetting']['published']) && $account_framework_settings['AccountFrameworkSetting']['published'] != '1'): ?>
                                                                    <li>
                                                                        <a data-toggle="modal"
                                                                           data-target="#rubric_publishWarning_<?php echo $row['AccountTag']['account_tag_id'] ?>"
                                                                           data-backdrop="static" data-keyboard="false"
                                                                           href="javascript:void(0);"><?php echo $language_based_content['publish_all']; ?></a>
                                                                    </li>
                                                                <?php endif; ?>


                                                                <?php if (!empty($account_framework_settings['AccountFrameworkSetting']['published']) && $account_framework_settings['AccountFrameworkSetting']['published'] == '1'): ?>
                                                                    <li>
                                                                        <a data-toggle="modal"
                                                                           data-target="#rubric_unpublishWarning_<?php echo $row['AccountTag']['account_tag_id'] ?>"
                                                                           data-backdrop="static" data-keyboard="false"
                                                                           href="javascript:void(0);"><?php echo $language_based_content['unpublish_all']; ?></a>
                                                                    </li>
                                                                <?php endif; ?>

                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <!-- Modal -->
                                            <div class="modal fade"
                                                 id="delete_rubric_<?php echo $row['AccountTag']['account_tag_id'] ?>"
                                                 tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $language_based_content['rubric_delete_all']; ?></h5>
                                                            <button type="button"  class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><?php echo $language_based_content['deleting_rubric_all']; ?></p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $language_based_content['cancel_all']; ?>
                                                            </button>
                                                            <button type="button" style="background-color: <?php echo $this->Custom->get_site_settings('primary_border_color'); ?>;" class="btn btn-success"><a
                                                                    href="<?php echo $this->base . '/accounts/delete_framework/' . $row['AccountTag']['account_tag_id'] . '/' . $account_id; ?>"><?php echo $language_based_content['delete_all']; ?></a>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal fade"
                                                 id="rubric_publishWarning_<?php echo $row['AccountTag']['account_tag_id'] ?>"
                                                 tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $language_based_content['rubric_publish_all']; ?></h5>
                                                            <button type="button"  class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><?php echo $language_based_content['after_choose_publish_all']; ?></p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $language_based_content['cancel_all']; ?></button>
                                                            <button type="button" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>;border-color: <?php echo $this->Custom->get_site_settings('primary_border_color'); ?>" class="btn btn-success"><a
                                                                    href="<?php echo $this->base . '/accounts/publish_framework/' . $row['AccountTag']['account_tag_id'] . '/' . $account_id; ?>"><?php echo $language_based_content['publish_all']; ?></a>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="modal fade"
                                                 id="rubric_unpublishWarning_<?php echo $row['AccountTag']['account_tag_id'] ?>"
                                                 tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
                                                 aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $language_based_content['rubric_unpublish_all']; ?></h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                    aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p><?php echo $language_based_content['unpublish_rubric_msg_all']; ?></p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $language_based_content['cancel_all']; ?></button>
                                                            <button type="button" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>;border-color: <?php echo $this->Custom->get_site_settings('primary_border_color'); ?>" class="btn btn-success"><a
                                                                    href="<?php echo $this->base . '/accounts/unpublish_framework/' . $row['AccountTag']['account_tag_id'] . '/' . $account_id; ?>"><?php echo $language_based_content['unpublish_all']; ?></a>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>

                        </div>


                    <?php elseif ($sub_tab == 2 || $sub_tab == 3 || $sub_tab == 4): ?>
                        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
                              integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp"
                              crossorigin="anonymous">
                        <main role="main" class="container" style="overflow: hidden;">
                            <div class="rubric_main">
                                <h3><?php echo $language_based_content['rubric_editor_all']; ?></h3>

                                <?php if ($sub_tab == 2): ?>
                                    <div class="row">
                                        <div class="stepscls col-md-8">
                                            <div class="step1cls">
                                                <div class="stepborder"></div>
                                                <i class="far fa-dot-circle"></i>
                                                <label><?php echo $language_based_content['general_settings_all']; ?></label>

                                            </div>
                                            <div class="step2cls">
                                                <div class="stepborder"></div>
                                                <i class="fas fa-circle"></i>
                                                <label><?php echo $language_based_content['performance_levels_all'];  ?></label>
                                            </div>
                                            <div class="step3cls">

                                                <i class="fas fa-circle"></i>
                                                <label><?php echo $language_based_content['framework_all'];  ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="reb_step1_div">
                                        <div class="row">

                                            <div class="col-sm-12">
                                                <label><?php echo $language_based_content['rubric_name']; ?></label>
                                                <?php if ($framework_id != ''): ?>
                                                    <input id="rubric_name" max_length="65" class="input_limit"
                                                           value="<?php echo $framework_settings_details['AccountFrameworkSetting']['framework_name']; ?>"
                                                           type="text" required/>
                                                    <span class="error_msg_text"></span>
                                                <?php else: ?>
                                                    <input id="rubric_name" max_length="65 " class="input_limit"
                                                           type="text"/>
                                                    <span class="error_msg_text"></span>
                                                <?php endif; ?>
                                            </div>

                                            <div class="col-md-6">
                                                <h6><?php echo $language_based_content['framework_tiers_all']; ?></h6>
                                                <div class="col--md-12 tires_cls">
                                                    <label for="Check"><?php echo $language_based_content['number_of_tiers_all']; ?></label>
                                                    <!-- tagging level -->

                                                    <?php if ($framework_id != ''): ?>

                                                        <div id="check-level"
                                                             class="btn-group btn-group-sm btn-group-toggle ml-2"
                                                             data-toggle="buttons">
                                                            <label class="btn  btn-outline-dark chk-ptn-tier <?php
                                                            if ($framework_settings_details['AccountFrameworkSetting']['tier_level'] == '1') {
                                                                echo 'active';
                                                            }
                                                            ?>" data-attr="1">
                                                                <input type="radio" name="check_level_tier" class="check-tier-level" id="option1"
                                                                       autocomplete="off" value="1" <?php
                                                                       if ($framework_settings_details['AccountFrameworkSetting']['tier_level'] == '1') {
                                                                           echo 'checked';
                                                                       }
                                                                       ?>> 1
                                                            </label>
                                                            <label class="btn  btn-outline-dark chk-ptn-tier <?php
                                                            if ($framework_settings_details['AccountFrameworkSetting']['tier_level'] == '2') {
                                                                echo 'active';
                                                            }
                                                            ?>" data-attr="2">
                                                                <input type="radio" name="check_level_tier" class="check-tier-level" id="option2"
                                                                       autocomplete="off" value="2" <?php
                                                                       if ($framework_settings_details['AccountFrameworkSetting']['tier_level'] == '2') {
                                                                           echo 'checked';
                                                                       }
                                                                       ?>> 2
                                                            </label>
                                                            <label class="btn  btn-outline-dark chk-ptn-tier <?php
                                                            if ($framework_settings_details['AccountFrameworkSetting']['tier_level'] == '3') {
                                                                echo 'active';
                                                            }
                                                            ?>"
                                                                   data-attr="3">
                                                                <input type="radio" name="check_level_tier" class="check-tier-level" id="option3"
                                                                       autocomplete="off" value="3" <?php
                                                                       if ($framework_settings_details['AccountFrameworkSetting']['tier_level'] == '3') {
                                                                           echo 'checked';
                                                                       }
                                                                       ?>>3
                                                            </label>
                                                            <label class="btn  btn-outline-dark chk-ptn-tier <?php
                                                            if ($framework_settings_details['AccountFrameworkSetting']['tier_level'] == '4') {
                                                                echo 'active';
                                                            }
                                                            ?>" data-attr="4">
                                                                <input type="radio" name="check_level_tier" class="check-tier-level" id="option4"
                                                                       autocomplete="off" value="4" <?php
                                                                       if ($framework_settings_details['AccountFrameworkSetting']['tier_level'] == '4') {
                                                                           echo 'checked';
                                                                       }
                                                                       ?>>4
                                                            </label>
                                                        </div>
                                                        <div class="error_message_tier"></div>

                                                    <?php else: ?>

                                                        <div id="check-level"
                                                             class="btn-group btn-group-sm btn-group-toggle ml-2"
                                                             data-toggle="buttons">
                                                            <label class="btn  btn-outline-dark chk-ptn-tier "
                                                                   data-attr="1">
                                                                <input type="radio" name="check_level_tier" id="option1"
                                                                       autocomplete="off" value="1"> 1
                                                            </label>
                                                            <label class="btn  btn-outline-dark chk-ptn-tier" data-attr="2">
                                                                <input type="radio" name="check_level_tier" id="option2"
                                                                       autocomplete="off" value="2"> 2
                                                            </label>
                                                            <label class="btn  btn-outline-dark chk-ptn-tier active"
                                                                   data-attr="3">
                                                                <input type="radio" name="check_level_tier" id="option3"
                                                                       autocomplete="off" value="3" checked>3
                                                            </label>
                                                            <label class="btn  btn-outline-dark chk-ptn-tier "
                                                                   data-attr="4">
                                                                <input type="radio" name="check_level_tier" id="option4"
                                                                       autocomplete="off" value="4">4
                                                            </label>
                                                        </div>
                                                        <div class="error_message_check_tier"></div>
                                                    <?php endif; ?>
                                                </div>

                                                <div class="col--md-12 tires_cls">
                                                    <label for="Check"><?php echo $language_based_content['checkbox_tier_all']; ?></label>
                                                    <!-- tagging level -->

                                                    <?php if ($framework_id != ''): ?>
                                                        <div id="check-level"
                                                             class="btn-group btn-group-sm btn-group-toggle ml-2 checkobx-tier"
                                                             data-toggle="buttons">
                                                            <label class="btn  btn-outline-dark chk-tire-box <?php
                                                            if ($framework_settings_details['AccountFrameworkSetting']['checkbox_level'] == '1') {
                                                                echo 'active';
                                                            }
                                                            ?>"
                                                                   id="chk-label-1" data-label="chk-optn-1">
                                                                <input type="radio" name="check-level" id="chk-optn-1"
                                                                       autocomplete="off" value="1" <?php
                                                                       if ($framework_settings_details['AccountFrameworkSetting']['checkbox_level'] == '1') {
                                                                           echo 'checked';
                                                                       }
                                                                       ?>>1
                                                            </label>

                                                            <label class="btn  btn-outline-dark chk-tire-box <?php
                                                            if ($framework_settings_details['AccountFrameworkSetting']['checkbox_level'] == '2') {
                                                                echo 'active';
                                                            }
                                                            ?>" id="chk-label-2" data-label="chk-optn-2">
                                                                <input type="radio" name="check-level" id="chk-optn-2"
                                                                       autocomplete="off" value="2" <?php
                                                                       if ($framework_settings_details['AccountFrameworkSetting']['checkbox_level'] == '2') {
                                                                           echo 'checked';
                                                                       }
                                                                       ?>>2
                                                            </label>
                                                            <label class="btn  btn-outline-dark chk-tire-box <?php
                                                            if ($framework_settings_details['AccountFrameworkSetting']['checkbox_level'] == '3') {
                                                                echo 'active';
                                                            }
                                                            ?>" id="chk-label-3" data-label="chk-optn-3">
                                                                <input type="radio" name="check-level" id="chk-optn-3"
                                                                       autocomplete="off" value="3" <?php
                                                                       if ($framework_settings_details['AccountFrameworkSetting']['checkbox_level'] == '3') {
                                                                           echo 'checked';
                                                                       }
                                                                       ?>>3
                                                            </label>
                                                            <label class="btn  btn-outline-dark chk-tire-box <?php
                                                            if ($framework_settings_details['AccountFrameworkSetting']['checkbox_level'] == '4') {
                                                                echo 'active';
                                                            }
                                                            ?>"
                                                                   id="chk-label-4" data-label="chk-optn-4">
                                                                <input type="radio" name="check-level" id="chk-optn-4"
                                                                       autocomplete="off" value="4" <?php
                                                                       if ($framework_settings_details['AccountFrameworkSetting']['checkbox_level'] == '4') {
                                                                           echo 'checked';
                                                                       }
                                                                       ?>>4
                                                            </label>
                                                        </div>
                                                        <div class="error_message_check_tier"></div>

                                                    <?php else: ?>


                                                        <div id="check-level"
                                                             class="btn-group btn-group-sm btn-group-toggle ml-2 checkobx-tier"
                                                             data-toggle="buttons">
                                                            <label class="btn  btn-outline-dark active chk-tire-box"
                                                                   id="chk-label-1">
                                                                <input type="radio" name="check-level" id="chk-optn-1"
                                                                       autocomplete="off" value="1" checked>1
                                                            </label>
                                                            <label class="btn  btn-outline-dark chk-tire-box"
                                                                   id="chk-label-2">
                                                                <input type="radio" name="check-level" id="chk-optn-2"
                                                                       autocomplete="off" value="2">2
                                                            </label>
                                                            <label class="btn  btn-outline-dark chk-tire-box"
                                                                   id="chk-label-3">
                                                                <input type="radio" name="check-level" id="chk-optn-3"
                                                                       autocomplete="off" value="3">3
                                                            </label>
                                                            <label class="btn  btn-outline-dark chk-tire-box disabled"
                                                                   id="chk-label-4">
                                                                <input type="radio" name="check-level" id="chk-optn-4"
                                                                       autocomplete="off" value="4" disabled>4
                                                            </label>
                                                        </div>
                                                        <div class="error_message_tier"></div>

                                                    <?php endif; ?>

                                                </div>

                                                <?php if (!empty($child_accounts)): ?>

                                                    <div class="rubric_op">
                                                        <h6><?php echo $language_based_content['rubric_parent_sharing_all']; ?></h6>
                                                        <div class="onoffswitch">
                                                            <input type="checkbox" name="auto_scroll_switch"
                                                                   class="onoffswitch-checkbox" id="auto_scroll_switch"
                                                                   mysis="0">

                                                            <label class="onoffswitch-label" for="auto_scroll_switch">
                                                                <span class="onoffswitch-inner"></span>
                                                                <span class="onoffswitch-switch"></span>
                                                            </label>

                                                        </div>
                                                        <div class="label_txt"><?=$language_based_content['share-with-child-acount'];?></div>
                                                    </div>

                                                <?php endif; ?>


                                            </div>

                                            <div class="col-md-6">
                                                <div class="border_leftrb">
                                                    <h6><?php echo $language_based_content['framework_sample_all']; ?></h6>
                                                    <div class="fscroll">
                                                        <div class="fram_sample">
                                                            <?php
                                                            if ($framework_id != ''):

                                                                echo html_entity_decode($framework_settings_details['AccountFrameworkSetting']['framework_sample']);
                                                                ?>

                                                            <?php else: ?>
                                                                <div class="f-group L1" data-level="1"><input
                                                                        type="checkbox"
                                                                        class="form-check-input cbox">
                                                                    <div class="f-item pre">1.1</div>
                                                                    <div class="f-item content"><?=$language_based_content['new_item']; ?></div>
                                                                </div>
                                                                <div class="f-group L2" data-level="2"><input
                                                                        type="checkbox"
                                                                        class="form-check-input cbox d-none">
                                                                    <div class="f-item pre">1.2</div>
                                                                    <div class="f-item content"><?=$language_based_content['new_item']; ?></div>
                                                                </div>
                                                                <div class="f-group L3" data-level="3"><input
                                                                        type="checkbox"
                                                                        class="form-check-input cbox d-none">
                                                                    <div class="f-item pre">1.3</div>
                                                                    <div class="f-item content"><?=$language_based_content['new_item']; ?></div>
                                                                </div>

                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                        </div>

                                        <div class="buttons_reb">
                                            <a class="reb_cancel" data-toggle="modal" data-target="#rubric_cancel"
                                               data-backdrop="static" data-keyboard="false"
                                               href="#"><?php echo $language_based_content['cancel_all']; ?></a>
                                            <a id="save_first_level" style="background-color:<?php echo $this->Custom->get_site_settings('rubric_previus') ?>;border-color:<?php echo $this->Custom->get_site_settings('rubric_previus') ?>"  class="reb_save" href="javascript:void(0)"><?php echo $language_based_content['save_exit_all']; ?></a>
                                            <a id="save_first_level_next" href="javascript:void(0)" class="reb_next"
                                               ><?php echo $language_based_content['next_all']; ?></a>
                                        </div>
                                    </div>


                                    <script>

                                        $(document).ready(function () {

                                            var parent_sharing = '<?php echo $framework_settings_details['AccountFrameworkSetting']['parent_child_share'] ?>';


                                            if (parent_sharing == '1') {
                                                $('#auto_scroll_switch').trigger('click');
                                            }


                                            $("#save_first_level").click(function () {
                                                if ($('.chk-ptn-tier input').is(':checked') == false) {
                                                    $('.error_message_tier').text("please select tire level.");
                                                    return false;
                                                } else {
                                                    $('.error_message_tier').text('');
                                                }
                                                if ($('.chk-tire-box input').is(':checked') == false) {
                                                    $('.error_message_check_tier').text("please select checkbox tire.");
                                                    return false;
                                                } else {
                                                    $('.error_message_check_tier').text('');
                                                }

                                                if ($('#rubric_name').val() == '') {
                                                    $('#rubric_name').css('border', '1px red solid');
                                                    return false;
                                                } else {
                                                    $('#rubric_name').css('border', 'solid 1px #c5c5c5');
                                                }
                                                var tier_level = $("input[name='check_level_tier']:checked").val();
                                                var checkbox_level = $("input[name='check-level']:checked").val();
                                                var rubric_name = $('#rubric_name').val();
                                                var framework_sample = $('.fram_sample').html();
                                                var account_id = '<?php echo $account_id; ?>';
                                                var parent_sharing = '0';
                                                if ($('#auto_scroll_switch').is(":checked")) {
                                                    parent_sharing = '1';
                                                }


                                                var postData = {
                                                    tier_level: tier_level,
                                                    checkbox_level: checkbox_level,
                                                    rubric_name: rubric_name,
                                                    framework_sample: framework_sample,
                                                    account_id: '<?php echo $account_id; ?>',
                                                    user_id: '<?php echo $user_id; ?>',
                                                    parent_sharing: parent_sharing

                                                };
                                                var framework_id = '<?php echo $framework_id; ?>';
                                                $.ajax({
                                                    type: 'POST',
                                                    url: home_url + '/accounts/save_frame_work_first_level/' + framework_id,
                                                    data: postData,
                                                    dataType: "json",
                                                    success: function (response) {
                                                        var redirect_url = home_url + '/accounts/account_settings_all/' + account_id + '/7/1/';
                                                        window.location.href = redirect_url;


                                                    }
                                                });
                                            });


                                            $("#save_first_level_next").click(function () {
                                                if ($('.chk-ptn-tier input').is(':checked') == false) {
                                                    $('.error_message_tier').text("please select tire level.");
                                                    return false;
                                                } else {
                                                    $('.error_message_tier').text('');
                                                }
                                                if ($('.chk-tire-box input').is(':checked') == false) {
                                                    $('.error_message_check_tier').text("please select checkbox tire.");
                                                    return false;
                                                } else {
                                                    $('.error_message_check_tier').text("");
                                                }
                                                if ($('#rubric_name').val() == '') {
                                                    $('#rubric_name').css('border', '1px red solid');
                                                    return false;
                                                } else {
                                                    $('#rubric_name').css('border', 'solid 1px #c5c5c5');
                                                }
                                                var tier_level = $("input[name='check_level_tier']:checked").val();
                                                var checkbox_level = $("input[name='check-level']:checked").val();
                                                var rubric_name = $('#rubric_name').val();
                                                var framework_sample = $('.fram_sample').html();
                                                var account_id = '<?php echo $account_id; ?>';
                                                var parent_sharing = '0';
                                                if ($('#auto_scroll_switch').is(":checked")) {
                                                    parent_sharing = '1';
                                                }

                                                var framework_id = '<?php echo $framework_id; ?>';
                                                var postData = {
                                                    tier_level: tier_level,
                                                    checkbox_level: checkbox_level,
                                                    rubric_name: rubric_name,
                                                    framework_sample: framework_sample,
                                                    account_id: '<?php echo $account_id; ?>',
                                                    user_id: '<?php echo $user_id; ?>',
                                                    parent_sharing: parent_sharing

                                                };
                                                $.ajax({
                                                    type: 'POST',
                                                    url: home_url + '/accounts/save_frame_work_first_level/' + framework_id,
                                                    data: postData,
                                                    dataType: "json",
                                                    success: function (response) {
                                                        var redirect_url = home_url + '/accounts/account_settings_all/' + account_id + '/7/3/0/' + response.framework_id + '/';
                                                        window.location.href = redirect_url;


                                                    }
                                                });
                                            });


                                        });

                                    </script>

                                <?php endif; ?>

                                <?php if ($sub_tab == 3): ?>
                                    <div class="row">
                                        <div class="stepscls col-md-8">
                                            <div class="step1cls">
                                                <div class="stepborder activestp"></div>
                                                <i class="far fa-dot-circle"></i>
                                                <label><?php echo $language_based_content['general_settings_all']; ?></label>

                                            </div>
                                            <div class="step2cls">
                                                <div class="stepborder"></div>
                                                <i class="far fa-dot-circle "></i>
                                                <label><?php echo $language_based_content['performance_levels_all']; ?></label>
                                            </div>
                                            <div class="step3cls">

                                                <i class="fas fa-circle"></i>
                                                <label><?php echo $language_based_content['framework_all'];  ?></label>
                                            </div>
                                        </div>
                                    </div>
                                    <form id="second_level_framework" accept-charset="UTF-8"
                                          action="<?php echo $this->base . '/accounts/save_frame_work_second_level/' ?>"
                                          enctype="multipart/form-data" method="post">
                                        <div class="reb_step2_div">
                                            
                                      <?php if($coaching_perfomance_level_value == '0' && $assessment_perfomance_level_value == '0' ): ?>      
                                      <div class="performance_level_alert_wo_cross"><?php echo $language_based_content['performance_level_are_not_enabled_in_account']; ?></div>      
                                        <?php endif; ?>
                                      
                                     
                                      
                                      <?php if($coaching_perfomance_level_value == '1' || $assessment_perfomance_level_value == '1'): ?>  
                                      
                                            <div class="row">

                                                <div class="col-sm-12 onoff_main">
                                                    <div class="rub_onoff">
                                                        <div class="onoffswitch">
                                                            <input type="checkbox" name="auto_scroll_switch1"
                                                                   class="onoffswitch-checkbox" id="auto_scroll_switch1"
                                                                   mysis="0" value="1">

                                                            <label class="onoffswitch-label" for="auto_scroll_switch1">
                                                                <span class="onoffswitch-inner"></span>
                                                                <span class="onoffswitch-switch"></span>
                                                            </label>

                                                        </div>
                                                        <div class="label_txt"><?php echo $language_based_content['performance_levels_C_all']; ?></div>
                                                    </div>
                                                    
                                                    

                                                    
                                                    
                                                    <div <?php
                                                    if (!$framework_settings_details['AccountFrameworkSetting']['enable_performance_level']) {
                                                        echo "style = 'display:none;' ";
                                                    }
                                                    ?>  class="rub_onoff" id="pl_unique_description">
                                                        <div class="onoffswitch">
                                                            <input type="checkbox" name="auto_scroll_switch2"
                                                                   class="onoffswitch-checkbox" id="auto_scroll_switch2"
                                                                   mysis="0" value="1">

                                                            <label class="onoffswitch-label" for="auto_scroll_switch2">
                                                                <span class="onoffswitch-inner"></span>
                                                                <span class="onoffswitch-switch"></span>
                                                            </label>

                                                        </div>
                                                        <div class="label_txt"><?php echo $language_based_content['unique_descriptions_all']; ?></div>
                                                    </div>

                                                    <!--                                                <div class="rub_onoff" id="pl_assending_order" style="display: none;">-->
                                                    <!--                                                    <div class="onoffswitch">-->
                                                    <!--                                                        <input type="checkbox" name="auto_scroll_switch3"-->
                                                    <!--                                                               class="onoffswitch-checkbox" id="auto_scroll_switch3"-->
                                                    <!--                                                               mysis="0">-->
                                                    <!---->
                                                    <!--                                                        <label class="onoffswitch-label" for="auto_scroll_switch3">-->
                                                    <!--                                                            <span class="onoffswitch-inner"></span>-->
                                                    <!--                                                            <span class="onoffswitch-switch"></span>-->
                                                    <!--                                                        </label>-->
                                                    <!---->
                                                    <!--                                                    </div>-->
                                                    <!--                                                    <div class="label_txt">Ascending Order</div>-->
                                                    <!--                                                </div>-->
                                                    <div class="clearfix"></div>
                                                </div>
                                                
                                                                                    <?php if($coaching_perfomance_level_value == '1' && $assessment_perfomance_level_value == '0' ): ?>
                                          <br>          
                                      <div class="performance_level_alert"><?php echo $language_based_content['performance_level_enabled_for_coaching']; ?>
                                      
                                        <span id ="cross_pl_message" style="
                                          font-weight: bold;
                                          position: absolute;
                                          right: 10px;
                                          top: 6px;
                                          cursor: pointer;
                                                "><svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 8 8">
                                            <path style="fill:#94751c;opacity: 0.6;" d="M1.41 0l-1.41 1.41.72.72 1.78 1.81-1.78 1.78-.72.69 1.41 1.44.72-.72 1.81-1.81 1.78 1.81.69.72 1.44-1.44-.72-.69-1.81-1.78 1.81-1.81.72-.72-1.44-1.41-.69.72-1.78 1.78-1.81-1.78-.72-.72z"/>
                                          </svg></span>
                                      
                                      </div>  
                                         <div class="clearfix"></div>
                                        <?php endif; ?>
                                      
                                      <?php if($coaching_perfomance_level_value == '0' && $assessment_perfomance_level_value == '1' ): ?>
                                          <br>
                                      <div class="performance_level_alert"><?php echo $language_based_content['performance_level_enabled_for_assessment']; ?>
                                      
                                      <span id ="cross_pl_message" style="
                                              font-weight: bold;
                                                position: absolute;
                                                right: 10px;
                                                top: 6px;
                                                cursor: pointer;
                                      "><svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 8 8">
                                            <path style="fill:#94751c;    opacity: 0.6;" d="M1.41 0l-1.41 1.41.72.72 1.78 1.81-1.78 1.78-.72.69 1.41 1.44.72-.72 1.81-1.81 1.78 1.81.69.72 1.44-1.44-.72-.69-1.81-1.78 1.81-1.81.72-.72-1.44-1.41-.69.72-1.78 1.78-1.81-1.78-.72-.72z"/>
                                          </svg></span>
                                      
                                      
                                      </div>
                                          <div class="clearfix"></div>
                                    <?php endif; ?>

                                                <div <?php
                                                if (!$framework_settings_details['AccountFrameworkSetting']['enable_performance_level']) {
                                                    echo "style = 'display:none;' ";
                                                }
                                                ?>  class="col-md-6 performance_level">


                                                    <div class="clearfix topaddminus">
                                                        <!-- controls -->
                                                        <div class="clearfix">
                                                            <button style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button"
                                                                    class="btn btn-success btn-circle btn-lg float-right pl-add-entry">
                                                                <i class="fa fa-plus"></i></button>
                                                            <button style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button"
                                                                    class="btn btn-success btn-circle btn-lg float-right pl-remove-entry">
                                                                <i class="fa fa-minus"></i></button>
                                                            <button style="background: <?php echo $this->Custom->get_site_settings('g_color_4'); ?>" type="button"
                                                                    class="btn btn-primary btn-circle btn-lg float-right f-up-pl">
                                                                <i class="fa fa-arrow-up"></i></button>
                                                            <button style="background: <?php echo $this->Custom->get_site_settings('g_color_4'); ?>" type="button"
                                                                    class="btn btn-primary btn-circle btn-lg float-right f-down-pl">
                                                                <i class="fa fa-arrow-down"></i></button>
                                                        </div>
                                                    </div>

                                                    <div class="fscroll">
                                                        <label><?php echo $language_based_content['name_all']; ?></label>
                                                        <input type="text" class="input_limit" max_length="35"
                                                               id="itemPrePl"/>
                                                        <span class="error_msg_text"></span>
                                                        <div id="itemContentBoxPl">
                                                            <label><?php echo $language_based_content['global_description_all']; ?></label>
                                                            <textarea id="itemContentPl"></textarea>
                                                        </div>

                                                    </div>


                                                </div>
                                                <div <?php
                                                if (!$framework_settings_details['AccountFrameworkSetting']['enable_performance_level']) {
                                                    echo "style = 'display:none;' ";
                                                }
                                                ?> class="col-md-6 performance_level">
                                                    <div class="rub_used"><?php
                                                        if (count($framework_settings_performance_level) > 1) {
                                                            echo count($framework_settings_performance_level);
                                                        } else {
                                                            echo '1';
                                                        }
                                                        ?>/10 <?=$jsdata['used']?>
                                                    </div>
                                                    <span id="total-used-count" data-count="<?php
                                                    if (count($framework_settings_performance_level) > 1) {
                                                        echo count($framework_settings_performance_level) + 1;
                                                    } else {
                                                        echo 2;
                                                    }
                                                    ?>"></span>
                                                    <div id="pl-f-preview" class="sortable">

                                                        <?php if (!empty($framework_settings_performance_level)): ?>
                                                            <?php foreach ($framework_settings_performance_level as $fspl): ?>
                                                                <div <?php
                                                                if ($framework_settings_details['AccountFrameworkSetting']['published']) {
                                                                    echo 'published = "true" ';
                                                                }
                                                                ?> class="rub_box_dark f-group L1 selectedRB pl-levels"
                                                                    data-level="<?php echo $fspl['AccountFrameworkSettingPerformanceLevel']['performance_level_rating']; ?>"
                                                                    data-index="<?php echo $fspl['AccountFrameworkSettingPerformanceLevel']['performance_level_rating']; ?>">
                                                                    <div class="titlecls">
                                                                        <div class="rub_rowleft rub_higest">
                                                                            <span>Highest</span>
                                                                            <div class="rub_line">

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="f-item pre">
                                                                        <label class="plTitleText"><?php echo $fspl['AccountFrameworkSettingPerformanceLevel']['performance_level']; ?></label>
                                                                        <input type="hidden" name="title[]"
                                                                               value="<?php echo $fspl['AccountFrameworkSettingPerformanceLevel']['performance_level']; ?>"
                                                                               class="f-item plTitleInput">
                                                                        <input type="hidden" name="performance_level_ids[]"
                                                                               class="performance_level_ids"
                                                                               value="<?php echo $fspl['AccountFrameworkSettingPerformanceLevel']['id']; ?>">
                                                                        <input type="hidden" name="ratting_standards[]"
                                                                               value="<?php echo $fspl['AccountFrameworkSettingPerformanceLevel']['performance_level_rating']; ?>"
                                                                               class="standards_rattings">
                                                                    </div>

                                                                    <div class="f-item content pl_contents_show" <?php
                                                                    if (!$framework_settings_details['AccountFrameworkSetting']['enable_unique_desc']) {
                                                                        echo 'style = "display:none;"';
                                                                    }
                                                                    ?> >
                                                                        <p class="pl_contents_text"><?php echo $fspl['AccountFrameworkSettingPerformanceLevel']['description']; ?></p>
                                                                        <span id="pl-textarea-1"><textarea
                                                                                class="f-item pl_contents_input"
                                                                                style="display: none;"
                                                                                name="description[]"><?php echo $fspl['AccountFrameworkSettingPerformanceLevel']['description']; ?></textarea></span>
                                                                    </div>


                                                                </div>
                                                            <?php endforeach; ?>

                                                        <?php else: ?>

                                                            <div class="rub_box_dark f-group L1 selectedRB pl-levels"
                                                                 data-level="1"
                                                                 data-index="1">
                                                                <div class="titlecls">
                                                                    <div class="rub_rowleft rub_higest">
                                                                        <span>Highest</span>
                                                                        <div class="rub_line">

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="f-item pre">
                                                                    <label class="plTitleText">PL title 1</label>
                                                                    <input type="hidden" name="title[]" value="PL title 1"
                                                                           class="f-item plTitleInput">
                                                                    <input type="hidden" name="ratting_standards[]"
                                                                           value="1" class="standards_rattings">


                                                                    <input type="hidden" name="performance_level_ids[]"
                                                                           value="" class="performance_level_ids">
                                                                </div>
                                                                <div class="f-item content pl_contents_show">
                                                                    <p class="pl_contents_text"> Lorem Ipsum is simply dummy
                                                                        text of the printing and
                                                                        typesetting industry. Lorem Ipsum has been the
                                                                        industry's
                                                                        standard dummy text</p>
                                                                    <span id="pl-textarea-1"><textarea
                                                                            class="f-item pl_contents_input"
                                                                            style="display: none;"
                                                                            name="description[]"></textarea></span>
                                                                </div>

                                                            </div>

                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <script type="text/javascript">
                                                    $(document).ready(function (e) {
                                                        if ($('#auto_scroll_switch2').is(':checked') == true) {
                                                            $('#pl_contents_1').hide();
                                                            $('#itemContentBoxPl').hide();

                                                        } else {
                                                            //   $('#pl-textarea-1').html('<textarea class="f-item pl_contents_input" style="display: none;" name="description[]"></textarea>');
                                                            $('#pl_contents_1').show();
                                                            $('#itemContentBoxPl').show();

                                                            //   $('.pl_contents_input').val($.trim('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text'));
                                                        }

                                                        var enable_performance_level = '<?php echo $framework_settings_details['AccountFrameworkSetting']['enable_performance_level'] ?>';
                                                        var enable_unique_desc = '<?php echo $framework_settings_details['AccountFrameworkSetting']['enable_unique_desc'] ?>';
                                                        var enable_ascending_order = '<?php echo $framework_settings_details['AccountFrameworkSetting']['enable_ascending_order'] ?>';

                                                        if (enable_performance_level == '1') {
                                                            $('#auto_scroll_switch1').trigger('click');
                                                        }
                                                        if (enable_unique_desc == '1') {
                                                            $('#auto_scroll_switch2').trigger('click');
                                                        }
                                                        if (enable_ascending_order == '1') {
                                                            $('#auto_scroll_switch3').trigger('click');
                                                        }

                                                        var published = '<?php echo $framework_settings_details['AccountFrameworkSetting']['published']; ?>';

                                                        if (published == '1') {
                                                            $("#auto_scroll_switch1").prop('disabled', true);
                                                            $("#auto_scroll_switch2").prop('disabled', true);
                                                            $("#auto_scroll_switch3").prop('disabled', true);
                                                        }


                                                    });
                                                </script>


                                            </div>
                                          <?php endif; ?>
                                            <div class="buttons_reb">
                                                <a class="reb_cancel" data-toggle="modal" data-target="#rubric_cancel"
                                                   data-backdrop="static" data-keyboard="false" href="#"><?php echo $language_based_content['cancel_all']; ?></a>
                                                <input <?php
                                                if (!$framework_settings_details['AccountFrameworkSetting']['enable_performance_level']) {
                                                    echo "style = 'display:none;' ";
                                                }
                                                ?>  type="submit" class="reb_save performance_leve_save"
                                                    name="save_button" value="<?php echo $language_based_content['save_exit_all']; ?>">
                                                <a style="background:<?php echo $this->Custom->get_site_settings('rubric_previus') ?>" class="reb_save"
                                                   href="<?php echo $this->base . '/accounts/account_settings_all/' . $account_id . '/7/2/0/' . $framework_id ?>"><?php echo $language_based_content['previous_all']; ?></a>
                                                <input type="submit" name="next_button" class="reb_next step_2" value="<?php echo $language_based_content['next_all']; ?>">
                                                <input type="hidden" name="framework_id"
                                                       value="<?php echo $framework_id; ?>">
                                                <input type="hidden" name="account_id" value="<?php echo $account_id; ?>">
                                                <input type="hidden" id="submit_type" name="submit_type" value="">
                                            </div>
                                        </div>
                                    </form>
                                    <script type="text/javascript">
                                        $(document).ready(function (e) {
                                            performance_level();
                                            $('#auto_scroll_switch1').click(function (e) {
                                                performance_level();
                                            });

                                            function performance_level() {
                                                if ($('#auto_scroll_switch1').is(':checked') == true) {
                                                    $('#pl_unique_description').show();
                                                    $('#pl_assending_order').show();
                                                    $('.performance_level').show();
                                                    $('.performance_leve_save').show();


                                                } else {
                                                    $('#pl_unique_description').hide();
                                                    $('#pl_assending_order').hide();
                                                    $('.performance_level').hide();
                                                    $('.performance_leve_save').hide();

                                                }
                                            }


                                        })
                                    </script>
                                <?php endif; ?>

                                <?php if ($sub_tab == 4): ?>
                                    <form accept-charset="UTF-8"
                                          action="<?php echo $this->base . '/accounts/save_rubrics_last_level/' ?>"
                                          enctype="multipart/form-data" method="post" id="third_level_framework">
                                        <input type="hidden" name="current_level_tier" id="current_level_tier"
                                               autocomplete="off"
                                               value="<?php echo $current_level_tier ?>">
                                        <input type="hidden" name="check_level" id="current_level_checkbox"
                                               autocomplete="off"
                                               value="<?php echo $current_level_checkbox ?>">
                                               <?php $account_tags_with_unqie_desc1 = count($account_tags_with_unqie_desc); ?>
                                        <input type="hidden" id="loopCounter"
                                               value="<?php echo (!empty($account_tags_with_unqie_desc1) && $account_tags_with_unqie_desc1 > 0) ? count($account_tags_with_unqie_desc) + 1 : $current_level_tier + 1 ?>"/>

                                        <input type="hidden" name="account_id"
                                               value="<?php echo $account_id; ?>">
                                        <input type="hidden" name="framework_id"
                                               value="<?php echo $framework_id; ?>">

                                        <div class="row">
                                            <div class="stepscls col-md-8">
                                                <div class="step1cls">
                                                    <div class="stepborder activestp"></div>
                                                    <i class="far fa-dot-circle"></i>
                                                    <label><?php echo $language_based_content['general_settings_all']; ?></label>

                                                </div>
                                                <div class="step2cls">
                                                    <div class="stepborder activestp"></div>
                                                    <i class="far fa-dot-circle "></i>
                                                    <label><?php echo $language_based_content['performance_levels_all']; ?></label>
                                                </div>
                                                <div class="step3cls activestp">
                                                    <i class="far fa-dot-circle "></i>
                                                    <label><?php echo $language_based_content['framework_all'];  ?></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="reb_step3_div">

                                            <div class="row">


                                                <div class="col-md-6">

                                                    <div class="clearfix topaddminus">

                                                        <div class="clearfix">
                                                            <button style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button"
                                                                    class="btn btn-success btn-circle btn-lg float-right add-entry">
                                                                <i class="fa fa-plus"></i></button>
                                                            <button style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button"
                                                                    class="btn btn-success btn-circle btn-lg float-right  std-remove-entry">
                                                                <i class="fa fa-minus"></i></button>
                                                            <button style="background: <?php echo $this->Custom->get_site_settings('g_color_4'); ?>" type="button"
                                                                    class="btn btn-primary btn-circle btn-lg float-right f-indent">
                                                                <i class="fa fa-arrow-right"></i></button>
                                                            <button style="background: <?php echo $this->Custom->get_site_settings('g_color_4'); ?>" type="button"
                                                                    class="btn btn-primary btn-circle btn-lg float-right f-outdent">
                                                                <i class="fa fa-arrow-left"></i></button>
                                                            <button style="background: <?php echo $this->Custom->get_site_settings('g_color_4'); ?>" type="button"
                                                                    class="btn btn-primary btn-circle btn-lg float-right f-up">
                                                                <i class="fa fa-arrow-up"></i></button>
                                                            <button style="background: <?php echo $this->Custom->get_site_settings('g_color_4'); ?>" type="button"
                                                                    class="btn btn-primary btn-circle btn-lg float-right f-down">
                                                                <i class="fa fa-arrow-down"></i></button>
                                                        </div>

                                                    </div>

                                                    <div class="fscroll">

                                                        <label for="itemPre"><?php echo $language_based_content['prefix_all']; ?> &nbsp;<span style="color:red;font-size:11px;font-weight: bold;"><?php echo $language_based_content['10_max_char_all']; ?></span> </label>
                                                        <input type="text" max_length="10"
                                                               class="form-control form-control-sm input_limit" id="itemPre"
                                                               placeholder="" value="1.1" placeholder="1.1">
                                                        <div class="error_msg_text"></div>


                                                        <label for="itemContent"><?php echo $language_based_content['standard_text_all']; ?>&nbsp;<span style="color:red;font-size:11px;font-weight: bold;"><?php $language_based_content['1000_max_char_all']; ?></span> </label>
                                                        <textarea class="form-control input_limit" max_length="1000"
                                                                  id="itemContent" rows="2">new entry</textarea>
                                                        <div class="error_msg_text"></div>


                                                        <div class="performance_levels_with_desc" style="display:none;">
                                                            <label for="standardLabel"><?php echo $language_based_content['standard_analytics_label_all']; ?>&nbsp;<span style="color:red;font-size:11px;font-weight: bold;"><?php echo $language_based_content['24_max_char_all']; ?></span> </label>
                                                            <input max_length="24" type="text"
                                                                   class="form-control form-control-sm input_limit"
                                                                   id="standardLabel" placeholder="" value="">
                                                            <div class="error_msg_text"></div>


                                                            <?php if ($framework_settings_details['AccountFrameworkSetting']['enable_unique_desc'] && $framework_settings_details['AccountFrameworkSetting']['enable_performance_level'] == 1): ?>

                                                                <?php
                                                                $count = 1;
                                                                $first_index = $framework_settings_performance_level[0];
                                                                $last_index = end($framework_settings_performance_level);
                                                                $first_pl_ratting = $first_index['AccountFrameworkSettingPerformanceLevel']['performance_level_rating'];
                                                                $last_pl_ratting = $last_index['AccountFrameworkSettingPerformanceLevel']['performance_level_rating'];
                                                                $sorting_order = $framework_settings_details['AccountFrameworkSetting']['enable_ascending_order'];

                                                                foreach ($framework_settings_performance_level as $key => $fspl):
                                                                    $pl_ratting_title = '';
                                                                    if ($first_pl_ratting == $fspl['AccountFrameworkSettingPerformanceLevel']['performance_level_rating']) {
                                                                        if ($sorting_order == 1) {
                                                                            $pl_ratting_title = '<span style="color: #f4836d' . $this->Custom->get_site_settings('primary_bg_color') . '">(Worst)</span>';
                                                                        } else {
                                                                            $pl_ratting_title = '<span style="color:' . $this->Custom->get_site_settings('primary_bg_color') . ' ">(Best)</span>';
                                                                        }
                                                                    } elseif ($last_pl_ratting == $fspl['AccountFrameworkSettingPerformanceLevel']['performance_level_rating']) {
                                                                        // $pl_ratting_title = '<span style="color: #5bb75d;">(Best)</span>';
                                                                        if ($sorting_order == 1) {
                                                                            $pl_ratting_title = '<span style="color: ' . $this->Custom->get_site_settings('primary_bg_color') . '">(Best)</span>';
                                                                        } else {
                                                                            $pl_ratting_title = '<span style="color: ' . $this->Custom->get_site_settings('primary_bg_color') . '">(Worst)</span>';
                                                                        }
                                                                    } else {
                                                                        $pl_ratting_title = '';
                                                                    }
                                                                    ?>

                                                                    <label><?php echo $fspl['AccountFrameworkSettingPerformanceLevel']['performance_level']; ?>
                                                                        PL
                                                                        Description <?php echo $pl_ratting_title; ?></label>
                                                                    <textarea data_counter="<?php echo $count; ?>"
                                                                              id="pl_desc_<?php echo $fspl['AccountFrameworkSettingPerformanceLevel']['id']; ?>"
                                                                              class="pl_desc"
                                                                              pl_data="<?php echo $fspl['AccountFrameworkSettingPerformanceLevel']['id']; ?>"></textarea>


                                                                    <?php
                                                                    $count++;
                                                                endforeach;
                                                                ?>

                                                            <?php endif; ?>

                                                        </div>


                                                    </div>


                                                </div>
                                                <div class="col-md-6 border_leftrb">

                                                    <h6><?php echo $framework_settings_details['AccountFrameworkSetting']['framework_name']; ?></h6>
                                                    <div class="fscroll">
                                                        <div id="f-preview">
                                                            <?php $chuck_counter = 1; ?>
                                                            <?php if (!empty($account_tags_with_unqie_desc) && count($account_tags_with_unqie_desc) > 0): ?>
                                                                <?php $count = 1; ?>
                                                                <?php $framework_counter = 1; ?>
                                                                <?php foreach ($account_tags_with_unqie_desc as $atwud): ?>
                                                                    <?php
                                                                    $selected = '';
                                                                    if ($count == $current_level_tier + 1) {
                                                                        $count = 1;
                                                                        $chuck_counter++;
                                                                    }
                                                                    if ($count == $current_level_tier) {
                                                                        $selected = 'selectedRB';
                                                                    } else {
                                                                        $selected = '';
                                                                    }
                                                                    ?>
                                                                    <div class="f-group L<?php echo $atwud['AccountTagDetail']['AccountTag']['standard_level'] . ' ' . $selected ?> "
                                                                         data-level="<?php echo $atwud['AccountTagDetail']['AccountTag']['standard_level']; ?>">
                                                                        <input class="account_tag_id" type="hidden"
                                                                               name="result[<?php echo $framework_counter ?>][account_tag_id]"
                                                                               value="<?php echo $atwud['AccountTagDetail']['AccountTag']['account_tag_id']; ?>">

                                                                        <?php if ($atwud['AccountTagDetail']['AccountTag']['standard_level'] == $current_level_checkbox): ?>
                                                                            <input type="checkbox" name="check_standard"
                                                                                   class="form-check-input cbox pl_standard_level">
                                                                               <?php else: ?>
                                                                            <input type="checkbox" name="check_standard"
                                                                                   class="form-check-input cbox d-none">
                                                                               <?php endif; ?>
                                                                        <input type="hidden" class="pre_input_box"
                                                                               name="result[<?php echo $framework_counter ?>][prefix_level]"
                                                                               value="<?php echo $atwud['AccountTagDetail']['AccountTag']['tag_code']; ?>"/>

                                                                        <input type="hidden" class="pl_standard_label"
                                                                               name="result[<?php echo $framework_counter ?>][pl_standard_label]"
                                                                               value="<?php echo substr($atwud['AccountTagDetail']['AccountTag']['standard_analytics_label'], 0, 24); ?>">

                                                                        <div class="f-item pre"><?php echo $atwud['AccountTagDetail']['AccountTag']['tag_code']; ?></div>
                                                                        <?php if ($atwud['AccountTagDetail']['AccountTag']['tag_title'] != ''): ?>

                                                                            <div class="f-item content">
                                                                                <?php
                                                                                echo html_entity_decode($atwud['AccountTagDetail']['AccountTag']['tag_html']);
                                                                                ?>
                                                                            </div>
                                                                            <div class="f-item content_1" style="display: none">
                                                                                <?php
                                                                                echo $atwud['AccountTagDetail']['AccountTag']['tag_html'];
                                                                                ?>
                                                                            </div>
                                                                        <?php else: ?>

                                                                            <div class="f-item content"><?php echo html_entity_decode($atwud['AccountTagDetail']['AccountTag']['tag_html']); ?></div>
                                                                            <div class="f-item content_1" style="display: none"><?php echo $atwud['AccountTagDetail']['AccountTag']['tag_html']; ?></div>
                                                                        <?php endif; ?>
                                                                        <?php
                                                                        $account_tag = explode(':', $atwud['AccountTagDetail']['AccountTag']['tag_html']);
                                                                        //echo "<pre>";
                                                                        //print_r($account_tag);
                                                                        // echo "</pre>";
                                                                        ?>
                                                                        <input type="hidden" class="content_input_box" name="result[<?php echo $framework_counter ?>][contents]" value="<?php echo trim($atwud['AccountTagDetail']['AccountTag']['tag_html']); ?>"/>
                                                                        <input type="hidden" class="standard_level"
                                                                               name="result[<?php echo $framework_counter ?>][standard_level]"
                                                                               value="<?php echo $atwud['AccountTagDetail']['AccountTag']['standard_level']; ?>"/>
                                                                               <?php for ($counter = 0; $counter < count($framework_settings_performance_level); $counter++): ?>
                                                                            <input type="hidden" value="<?php
                                                                            if (!empty(isset($atwud['performace_level_descriptions']))) {
                                                                                echo $atwud['performace_level_descriptions'][$counter]['PerformanceLevelDescription']['description'];
                                                                            } else {
                                                                                echo '';
                                                                            }
                                                                            ?>"

                                                                                   pl_id_with_value="<?php
                                                                                   if (!empty(isset($atwud['performace_level_descriptions']))) {
                                                                                       echo $atwud['performace_level_descriptions'][$counter]['PerformanceLevelDescription']['performance_level_id'];
                                                                                   } else {
                                                                                       echo '';
                                                                                   }
                                                                                   ?>"
                                                                                   class="pl_desc_data pl_desc_text_<?php echo $counter + 1 ?>"
                                                                                   name="result[<?php echo $framework_counter ?>][pl_desc][<?php echo $counter + 1 ?>]"/>
                                                                            <input type="hidden" value="<?php
                                                                            if (!empty(isset($atwud['performace_level_descriptions']))) {
                                                                                echo $atwud['performace_level_descriptions'][$counter]['PerformanceLevelDescription']['performance_level_id'];
                                                                            } else {
                                                                                echo '';
                                                                            }
                                                                            ?>"
                                                                                   class="pl_desc_text_id_<?php echo $counter + 1 ?>"
                                                                                   name="result[<?php echo $framework_counter ?>][pl_desc_id][<?php echo $counter + 1 ?>]"/>
                                                                               <?php endfor; ?>
                                                                    </div>
                                                                    <?php
                                                                    $count++;
                                                                    $framework_counter++;
                                                                endforeach;
                                                                ?>


                                                            <?php else: ?>

                                                                <?php
                                                                $current_level_tier = 1;
                                                                for ($count = 1; $count <= $current_level_tier; $count++):
                                                                    ?>
                                                                    <?php
                                                                    $selected = '';
                                                                    if ($count == $current_level_tier) {
                                                                        $selected = 'selectedRB';
                                                                    } else {
                                                                        $selected = '';
                                                                    }
                                                                    ?>
                                                                    <div class="f-group L<?php echo $count . ' ' . $selected ?> "
                                                                         data-level="<?php echo $count; ?>">

                                                                        <?php if ($count == $current_level_checkbox): ?>
                                                                            <input type="hidden" class="pl_standard_label"
                                                                                   name="result[1][pl_standard_label]">
                                                                            <input type="checkbox" name="check_standard"
                                                                                   class="form-check-input cbox pl_standard_level">
                                                                               <?php else: ?>
                                                                            <input type="checkbox" name="check_standard"
                                                                                   class="form-check-input cbox d-none">
                                                                               <?php endif; ?>
                                                                        <input type="hidden" class="pre_input_box"
                                                                               name="result[1][prefix_level]" value="1.0"/>
                                                                        <input type="hidden" class="pl_standard_label" name="result[1][pl_standard_label]">
                                                                        <div class="f-item pre">1.0</div>
                                                                        <div class="f-item content"><?=$language_based_content['new_item']; ?></div>
                                                                        <input type="hidden" class="content_input_box"
                                                                               name="result[1][contents]"
                                                                               value="new item"/>
                                                                        <input type="hidden" class="standard_level"
                                                                               name="result[1][standard_level]"
                                                                               value="1"/>
                                                                               <?php for ($counter = 1; $counter <= count($framework_settings_performance_level); $counter++): ?>
                                                                            <input type="hidden"
                                                                                   class="pl_desc_data pl_desc_text_<?php echo $counter ?>"
                                                                                   name="result[1][pl_desc][<?php echo $counter ?>]"/>
                                                                            <input type="hidden"
                                                                                   class="pl_desc_text_id_<?php echo $counter ?>"
                                                                                   name="result[1][pl_desc_id][<?php echo $counter ?>]"/>
                                                                               <?php endfor; ?>
                                                                    </div>
                                                                <?php endfor; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php if (!empty($account_tags_with_unqie_desc)): ?>
                                                <input type="hidden" id="chunks_level"
                                                       value="<?php echo $chuck_counter + 1; ?>"/>
                                                   <?php else: ?>
                                                <input type="hidden" id="chunks_level" value="2"/>
                                            <?php endif; ?>
                                            <input id="total_performance_levels" type="hidden"
                                                   value="<?php echo count($framework_settings_performance_level); ?>"
                                                   name="total_performance_levels">

                                            <div class="buttons_reb">
                                                <a class="reb_cancel" data-toggle="modal" data-target="#rubric_cancel"
                                                   data-backdrop="static" data-keyboard="false"
                                                   href="#"><?php echo $language_based_content['cancel_all']; ?></a>
                                                <a class="blueFinishedBtn" href="<?php echo $this->base . '/accounts/account_settings_all/' . $account_id . '/7/3/0/' . $framework_id ?>"><?php echo $language_based_content['previous_all']; ?></a>
                                                <input type="submit" value="<?php echo $language_based_content['save_exit_all']; ?>" name="btn"  class="reb_next"/>
                                                <!---<input type="submit" value="Publish" name="btn" class="blueFinishedBtn"
                                                       style="background: #218838; color:#fff;"/>-->
                                            </div>
                                        </div>
                                    </form>
                                <?php endif; ?>

                            </div>

                            </div>


                        </main>
                    <?php endif; ?>

                </li>

            </ul>


        </div>


    </div>
</div>


<div id="email_ob" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:400px;">
        <div class="modal-content">


            <div class="header" style="margin-bottom:0px;">
                <h4 class="header-title nomargin-vertical smargin-bottom">Contact Us</h4>
                <a id="cross_contact" class="close-reveal-modal btn btn-grey close style2"
                   data-dismiss="modal">&times;</a>
            </div>

            <div class="send_message_dialog">


                <label class="label" for="subject"><?php echo $language_based_content['subject_all']; ?></label><br>
                <input id="subject" type="text" name="subject" class="input-xlarge"><span style="color:red;"
                                                                                          id="span_subject"></span><br>
                <label class="label" for="email"><?php echo $language_based_content['to_all']; ?></label><br>
                <input id="email" value="<?php echo $this->custom->get_site_settings('static_emails')['sales']; ?>" type="email" name="email" class="input-xlarge"><br>


                <label class="label" for="message"><?php echo $language_based_content['enter_message_all']; ?></label><br>
                <textarea id="message" name="message" class="input-xlarge" style="height: 80px;"></textarea><span
                    style="color:red;" id="span_message"></span><br>
                <input id="account_id" type="hidden" value="<?php echo $account_id; ?>">
                <input id="filename_email" type="hidden" value="">
                <input id="send_email" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" type="button" value="<?php echo $language_based_content['send_email_all']; ?>" data-dismiss="modal"
                       class="btn btn-green">

            </div>


        </div>
    </div>
</div>
<div class="modal fade" id="rubric_cancel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $language_based_content['cancel_all']; ?></h5>
                <button type="button"  class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p><?php echo $language_based_content['warning-settings-made-on-this-step-will-not-be-saved']; ?></p>
            </div>
            <div class="modal-footer">
                <button type="button" style="background-color: <?php echo $this->Custom->get_site_settings('cancel_btn_color'); ?>; border-color:<?php echo $this->Custom->get_site_settings('cancel_btn_color'); ?>" class="btn btn-secondary" data-dismiss="modal"><?php echo $language_based_content['cancel_all']; ?></button>
                <button type="button" style="background-color: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>;border-color:<?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="btn btn-success"><a
                        href="<?php echo $this->base . '/accounts/account_settings_all/' . $account_id . '/7/1' ?>">Ok</a>
                </button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
<?php
$is_published = '';
if ($framework_settings_details['AccountFrameworkSetting']['published'] == '1') {
    $is_published = 'yes';
}
?>
    $(document).ready(function () {
        var is_published = '<?php echo $is_published; ?>';
        $('#check-level .chk-tire-box').each(function (e) {
            var row_tier_val = $('.tires_cls input[name=check_level_tier]:checked').val();
            if (row_tier_val <= 1) {
                $('#chk-label-2').addClass('disabled');
                $('#chk-label-3').addClass('disabled');
                $('#chk-label-4').addClass('disabled');
            } else if (row_tier_val <= 2) {
                $('#chk-label-1').removeClass('disabled');
                $('#chk-label-2').removeClass('disabled');
                $('#chk-label-3').addClass('disabled');
                $('#chk-label-4').addClass('disabled');
            }
            else if (row_tier_val <= 3) {
                $('#chk-label-1').removeClass('disabled');
                $('#chk-label-2').removeClass('disabled');
                $('#chk-label-3').removeClass('disabled');
                $('#chk-label-4').addClass('disabled');
            } else if (row_tier_val <= 4) {
                $('#chk-label-1').removeClass('disabled');
                $('#chk-label-2').removeClass('disabled');
                $('#chk-label-3').removeClass('disabled');
                $('#chk-label-4').removeClass('disabled');
            }

        })
        $('.chk-ptn-tier').on('click', function (e) {
            if (is_published == 'yes') {
                alert('<?php echo $alert_messages['number_of_tiers_are_not_editable']; ?>');
                return false;
            }
            if ($(this).attr('data-attr') == 1) {
                $('#chk-label-2').removeClass('active');
                $('#chk-label-3').removeClass('active');
                $('#chk-label-4').removeClass('active');
                $('#chk-label-1').addClass('active');
                $("#chk-optn-1").prop("checked", true);
                $('#chk-label-2').addClass('disabled');
                $('#chk-label-3').addClass('disabled');
                $('#chk-label-4').addClass('disabled');
                var html = '';
                for (var i = 1; i <= 1; i++) {
                    class_txt = 'class="f-group L' + i + '"';
                    html += '<div ' + class_txt + ' data-level=' + i + '>';
                    if ($('#chk-optn-' + i).is(':checked') == true) {
                        html += '<input type="checkbox" class="form-check-input cbox">';
                    }
                    html += '<div class="f-item pre">1.' + i + '</div>';
                    html += '<div class="f-item content"><?=$language_based_content["new_item"]; ?></div>';
                    html += '</div>';
                }
                $('.fram_sample').html(html);
            } else if ($(this).attr('data-attr') == 2) {
                $('#chk-label-1').removeClass('active');
                $('#chk-label-3').removeClass('active');
                $('#chk-label-4').removeClass('active');
                $('#chk-label-2').removeClass('disabled');
                $('#chk-label-3').removeClass('active');
                $('#chk-label-3').addClass('disabled');
                $('#chk-label-4').removeClass('active');
                $('#chk-label-4').addClass('disabled');

                $('#chk-label-2').addClass('active');
                $("#chk-optn-2").prop("checked", true);
                var html = '';
                for (var i = 1; i <= 2; i++) {
                    class_txt = 'class="f-group L' + i + '"';
                    html += '<div ' + class_txt + ' data-level=' + i + '>';
                    if ($('#chk-optn-' + i).is(':checked') == true) {
                        html += '<input type="checkbox" class="form-check-input cbox">';
                    }
                    html += '<div class="f-item pre">1.' + i + '</div>';
                    html += '<div class="f-item content"><?=$language_based_content["new_item"]; ?></div>';
                    html += '</div>';
                }
                $('.fram_sample').html(html);
            } else if ($(this).attr('data-attr') == 3) {
                $('#chk-label-1').removeClass('active');
                $('#chk-label-2').removeClass('active');
                $('#chk-label-4').removeClass('active');

                $('#chk-label-2').removeClass('disabled');
                $('#chk-label-3').removeClass('disabled');
                $('#chk-label-4').addClass('disabled');
                $('#chk-label-3').addClass('active');
                $("#chk-optn-3").prop("checked", true);
                var html = '';
                for (var i = 1; i <= 3; i++) {
                    class_txt = 'class="f-group L' + i + '"';
                    html += '<div ' + class_txt + ' data-level=' + i + '>';
                    if ($('#chk-optn-' + i).is(':checked') == true) {
                        html += '<input type="checkbox" class="form-check-input cbox">';
                    }
                    html += '<div class="f-item pre">1.' + i + '</div>';
                    html += '<div class="f-item content"><?=$language_based_content["new_item"]; ?></div>';
                    html += '</div>';
                }
                $('.fram_sample').html(html);

            } else if ($(this).attr('data-attr') == 4) {
                $('#chk-label-1').removeClass('active');
                $('#chk-label-2').removeClass('active');
                $('#chk-label-3').removeClass('active');
                $('#chk-label-2').removeClass('disabled');
                $('#chk-label-3').removeClass('disabled');
                $('#chk-label-4').removeClass('disabled');
                $('#chk-label-4').addClass('active');
                $("#chk-optn-4").prop("checked", true);
                var html = '';
                for (var i = 1; i <= 4; i++) {
                    class_txt = 'class="f-group L' + i + '"';
                    html += '<div ' + class_txt + ' data-level=' + i + '>';
                    if ($('#chk-optn-' + i).is(':checked') == true) {
                        html += '<input type="checkbox" class="form-check-input cbox">';
                    }
                    html += '<div class="f-item pre">1.' + i + '</div>';
                    html += '<div class="f-item content"><?=$language_based_content["new_item"]; ?></div>';
                    html += '</div>';
                }
                $('.fram_sample').html(html);
            }
        })
        $('.chk-tire-box').click(function (e) {
            $(this).find('input:radio').removeAttrs('disabled');
            if (is_published == 'yes') {
                alert('<?php echo $alert_messages['checkbox_tier_is_not_available']; ?>');
                return false;
            }
            if ($(this).hasClass('disabled') == true) {
                return false;
            }
            var selectedVal = $('input[name=check_level_tier]:checked').val();
            // if ($(this).attr('id') == 'data-attr') {
            //     selectedVal = 1;
            // } else if ($(this).attr('id') == 'chk-label-2') {
            //     selectedVal = 2;
            // } else if ($(this).attr('id') == 'chk-label-3') {
            //     selectedVal = 3;
            // } else if ($(this).attr('id') == 'chk-label-4') {
            //     selectedVal = 4;
            // }

            html = '';
            for (var i = 1; i <= selectedVal; i++) {
                value_tex = $(this).find('input:radio').val();
                class_txt = 'class="f-group L' + i + '"';
                html += '<div ' + class_txt + ' data-level=' + i + '>';
                if (value_tex == i) {
                    html += '<input type="checkbox" class="form-check-input cbox">';
                }
                html += '<div class="f-item pre">1.' + i + '</div>';
                html += '<div class="f-item content"><?=$language_based_content["new_item"]; ?></div>';
                html += '</div>';
            }
            $('.fram_sample').html(html);
        });

        function ColorLuminance(hex, lum) {
            // validate hex string
            hex = String(hex).replace(/[^0-9a-f]/gi, '');
            if (hex.length < 6) {
                hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
            }
            lum = lum || 0;
            // convert to decimal and change luminosity
            var rgb = "#", c, i;
            for (i = 0;
                    i < 3;
                    i++) {
                c = parseInt(hex.substr(i * 2, 2), 16);
                c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
                rgb += ("00" + c).substr(c.length);
            }
            return rgb;
        }

        $('.upload-toggle').on('click', function (e) {
            e.preventDefault();
            $('.inside_preview_box').show();
            $('.inside_preview_box').children().remove();

            $(this).next().trigger('click');
        });

        $('.upload-toggle').next().imageLoader({
            'show': '.inside_preview_box',
            'width': '150px',
        });


        $("#account_image_logo").change(function () {
            var fileExtension = ['jpeg', 'jpg', 'png', 'gif', 'bmp'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                alert("<?php echo $alert_messages['only_format_allowed']; ?> : " + fileExtension.join(', '));
                $('.inside_preview_box').hide();
                $(this).val('');


            }
        });


        var RGBtoHEX = function (color) {
            return "#" + $.map(color.match(/\b(\d+)\b/g), function (digit) {
                return ('0' + parseInt(digit).toString(16)).slice(-2)
            }).join('');
        },
                $el = $('.color-preview');

        $el.ColorPicker({
            color: RGBtoHEX($el.css('backgroundColor')),
            onChange: function (hsb, hex) {
                $el.val(hex);
                console.log(hex);
                $el.val(hex).css('background-color', '#' + hex);
                $("#account_header_background_color").val(hex);

            },
            onSubmit: function (hsb, hex, rgb, el) {
                $el.val(hex).css('background-color', '#' + hex).ColorPickerHide();
                $("#account_header_background_color").val(hex);

                var darker = ColorLuminance(hex, -0.2);

                $("#account_usernav_bg_color").val(darker);


            },
            onBeforeShow: function () {

            }
        });

        $el2 = $('.color-preview-2');

        $el2.ColorPicker({
            color: RGBtoHEX($el2.css('backgroundColor')),
            onChange: function (hsb, hex) {
                $el2.val(hex);
                console.log(hex);
                $el2.val(hex).css('background-color', '#' + hex);
                $("#account_nav_bg_color").val(hex);

            },
            onSubmit: function (hsb, hex, rgb, el) {
                $el2.val(hex).css('background-color', '#' + hex).ColorPickerHide();
                $("#account_nav_bg_color").val(hex);

            },
            onBeforeShow: function () {

            }
        });


        if ($('#prevous').is(':checked')) {
            $('.check-out-type').val($('#prevous').val());
            $('#use-previous-info').css('display', 'block');
            $('#use-new-info').css('display', 'none');

        }
        if ($('#new').is(':checked')) {
            $('.check-out-type').val($('#new').val());
            $('#use-previous-info').css('display', 'none');
            $('#use-new-info').css('display', 'block');

        }
        $('#prevous').click(function (e) {
            $('.check-out-type').val($('#prevous').val());
            $('#use-previous-info').css('display', 'block');
            $('#use-new-info').css('display', 'none');
        })
        $('#new').click(function (e) {
            $('.check-out-type').val($('#new').val());
            $('#use-previous-info').css('display', 'none');
            $('#use-new-info').css('display', 'block');
        })
        $('.step_2').on('click', function () {
            submit_function();
            return false;
        });
        $('.performance_leve_save').on('click', function () {
            $('#submit_type').val($this.val());
            submit_function();
            return false;
        });
        function obj2Arr(obj) {
            var out = [];
            Object.keys(obj).forEach(function (k) {
                out.push(obj[k])
            })
            return out;
        }
        function submit_function() {
            $.ajax({
                type: 'POST',
                url: home_url + '/accounts/check_validation',
                data: $('#second_level_framework').serialize(),
                dataType: 'json',
                success: function (res) {
                    if (res.status == false) {
                        html_contents = '';
                        if (res.title != '') {
                            html_contents += res.title;
                        }
                        if (res.description != '') {
                            html_contents += res.description;
                        }

                        var res_indexes = $.parseJSON(res);
                        $("#pl-f-preview .f-group").css('border', 'none');
                        // console.log(res_indexes);
                        var arr = obj2Arr(res.result_array);

                        for (var i = 0; i <= arr.length; i++) {
                            indexs = arr[i];
                            $("#pl-f-preview .f-group:eq(" + indexs + ")").css('border', '1px solid red');
                        }
                        if (html_contents != '') {
                            $('#showflashmessage1').html('<div id="flashMessage" class="message error" style="cursor: pointer;"> <?=$language_based_content["all-name-global-description-fields-are-rquired"];?>*</div>');
                            $('html, body').animate({
                                scrollTop: 0
                            }, 800);
                        }

                        return false;

                    } else {
                        $('#second_level_framework').submit();
                    }

                }
            });
            return false;

        }

        $('#third_level_framework').on('submit', function (e) {
            error_message = '';
            // if (typeof($('#itemPre').val()) == 'undefined' || $('#itemPre').val() == '') {
            //     // $('#itemPre').css('border','1px solid red');
            //     error_message += 'Prefix field required<br/>';
            //
            // }
            if (typeof ($('#itemContent').val()) == 'undefined' || $('#itemContent').val() == '') {
                //$('#itemContent').css('border','1px solid red');
                error_message += 'Standard Text field required<br/>';

            }

            // if(typeof($('.pl_standard_label').val()) =='undefined' || $('#pl_standard_label').val()==''){
            //     error_message+= 'Standard Analytics Label field required<br>';
            // }
            if (error_message != '') {
                $('#showflashmessage1').html('<div id="flashMessage" class="message error" style="cursor: pointer;">' + error_message + '</div>');
                return false;
            }
        })


    });


    $("#send_email").click(function () {
        // $('#email_ob').modal('hide');

        var subject = $('#subject').val();
        var message = $('#message').val();

        if (subject == '') {
            document.getElementById('span_subject').innerHTML = "<?=$language_based_content['the-input-field-is-required']; ?><br>";
        }
        else {
            document.getElementById('span_subject').innerHTML = "";
        }

        if (message == '') {
            document.getElementById('span_message').innerHTML = "<?=$language_based_content['the-input-field-is-required']; ?><br>";
        }
        else {
            document.getElementById('span_message').innerHTML = "";
        }

        if (message == '' || subject == '') {

            return false;
        }


        var postData = {
//                name: $('#name').val(),
            email: $('#email').val(),
            message: $('#message').val(),
            account_id: $('#account_id').val(),
            subject: $('#subject').val(),
//                filename:$('#filename_email').val(),
//                video_id:  $('#video_id').val(),
//                huddle_id: $('#huddle_id').val()
        };


        $.ajax({
            type: 'POST',
            url: home_url + '/accounts/send_email/',
            data: postData,
            success: function (response) {
                var show_msg = '<div id="flashMessage" class="message success" style="cursor: pointer;"><?=$language_based_content["email-sent-successfully"]; ?></div>';
                $("#showflashmessage1").prepend(show_msg);
                $('#message').val('');
                $('#subject').val('');
                $('#email').val('<?php echo $this->custom->get_site_settings('static_emails')['sales']; ?>');

                document.getElementById('span_message').innerHTML = "";
                document.getElementById('span_subject').innerHTML = "";

            }
        });
    });

    $("#cross_contact").click(function () {
        $('#message').val('');
        $('#subject').val('');
        document.getElementById('span_message').innerHTML = "";
        document.getElementById('span_subject').innerHTML = "";


    });

    $("#showflashmessage1").click(function () {
        $('#flashMessage').fadeOut();
    });


</script>
</div>


<?php

/**
 * States Dropdown
 *
 * @uses check_select
 * @param string $post , the one to make "selected"
 * @param string $type , by default it shows abbreviations. 'abbrev', 'name' or 'mixed'
 * @return string
 */
function StateDropdown($post = null, $type = 'abbrev') {
    $states = array(
        array('AK', 'Alaska'),
        array('AL', 'Alabama'),
        array('AR', 'Arkansas'),
        array('AZ', 'Arizona'),
        array('CA', 'California'),
        array('CO', 'Colorado'),
        array('CT', 'Connecticut'),
        array('DC', 'District of Columbia'),
        array('DE', 'Delaware'),
        array('FL', 'Florida'),
        array('GA', 'Georgia'),
        array('HI', 'Hawaii'),
        array('IA', 'Iowa'),
        array('ID', 'Idaho'),
        array('IL', 'Illinois'),
        array('IN', 'Indiana'),
        array('KS', 'Kansas'),
        array('KY', 'Kentucky'),
        array('LA', 'Louisiana'),
        array('MA', 'Massachusetts'),
        array('MD', 'Maryland'),
        array('ME', 'Maine'),
        array('MI', 'Michigan'),
        array('MN', 'Minnesota'),
        array('MO', 'Missouri'),
        array('MS', 'Mississippi'),
        array('MT', 'Montana'),
        array('NC', 'North Carolina'),
        array('ND', 'North Dakota'),
        array('NE', 'Nebraska'),
        array('NH', 'New Hampshire'),
        array('NJ', 'New Jersey'),
        array('NM', 'New Mexico'),
        array('NV', 'Nevada'),
        array('NY', 'New York'),
        array('OH', 'Ohio'),
        array('OK', 'Oklahoma'),
        array('OR', 'Oregon'),
        array('PA', 'Pennsylvania'),
        array('PR', 'Puerto Rico'),
        array('RI', 'Rhode Island'),
        array('SC', 'South Carolina'),
        array('SD', 'South Dakota'),
        array('TN', 'Tennessee'),
        array('TX', 'Texas'),
        array('UT', 'Utah'),
        array('VA', 'Virginia'),
        array('VT', 'Vermont'),
        array('WA', 'Washington'),
        array('WI', 'Wisconsin'),
        array('WV', 'West Virginia'),
        array('WY', 'Wyoming')
    );

    $options = '<option value=""></option>';

    foreach ($states as $state) {
        if ($type == 'abbrev') {
            $options .= '<option value="' . $state[0] . '" ' . check_select($post, $state[0], false) . ' >' . $state[0] . '</option>' . "\n";
        } elseif ($type == 'name') {
            $options .= '<option value="' . $state[1] . '" ' . check_select($post, $state[1], false) . ' >' . $state[1] . '</option>' . "\n";
        } elseif ($type == 'mixed') {
            $options .= '<option value="' . $state[0] . '" ' . check_select($post, $state[0], false) . ' >' . $state[1] . '</option>' . "\n";
        }
    }

    echo $options;
}

function check_select($i, $m, $e = true) {
    if ($i != null) {
        if ($i == $m) {
            $var = ' selected="selected" ';
        } else {
            $var = '';
        }
    } else {
        $var = '';
    }
    if (!$e) {
        return $var;
    } else {
        echo $var;
    }
}
?>

<script>
    $(document).ready(function (e) {
        $(document).on('click', 'body', function (event) {
            if (event.target.classList.contains('rub_dots') == false) {
                $('.rub_options').hide();
                $('.rub_dots').attr('data-status', false);
            }
        });
        $(document).on('click', '.rub_dots', function (e) {
            $('.rub_options').hide();
            if ($(this).attr('data-status') == 'false') {
                $(this).parent().find('.rub_options').stop().show()
                $(this).attr('data-status', true);
            } else {
                $(this).parent().find('.rub_options').stop().hide();
                $(this).attr('data-status', false);
            }
            // $(this).parent().find('.rub_options').stop().slideToggle()
        });

        // Global Export JS starts here 
        $("#btn_ge_export").click(function(){
            $("#file_type").val( $("input[name='ge_file_type']:checked").val() );
            $("#frm_general_export").submit();
        });
        $("#btn_pl_export").click(function(){
            $("#performance_level_export").submit();
        });
        // End of Global Export JS starts here 
        
        var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><a href="/accounts/account_settings_main/<?php echo $account_id; ?>"><?php echo $breadcrumb_language_based_content['main_settings_breadcrumb']; ?></a><span><?php echo $breadcrumb_language_based_content['account_settings_breadcrumb']; ?></span></div>';

        $('.breadCrum').html(bread_crumb_data);
    });
    
    
        $("#cross_pl_message").click(function (event) {

            $('.performance_level_alert').hide();

        });
    
    
    
    
</script>
