<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<nav class="navbar navbar-expand-md navbar-dark bg-navy fixed-top no-padding">
    <div class="container">
        <div class="logo-block"></div>
        <!-- <a class="navbar-brand" href="#">Sibme</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button> -->
    </div>
</nav>

<main role="main" class="container" style="overflow: hidden;">
    <div class="row mt-100p pb-3 border-bottom">

        <!-- framework editing area -->
        <div class="col-12">

            <div class="row">
                <h3>Rubric Editor Prototype</h3>
            </div>


            <div class="row">
                <div class="col-md-2 mb-3">
                    <label for="Check">Checkbox Tier</label>
                    <!-- tagging level -->
                    <div id="check-level" class="btn-group btn-group-sm btn-group-toggle ml-2" data-toggle="buttons">
                        <label class="btn  btn-outline-dark active">
                            <input type="radio" name="check-level" id="option1" autocomplete="off" value="1" checked> Tier-1
                        </label>
                        <label class="btn  btn-outline-dark">
                            <input type="radio" name="check-level" id="option2" autocomplete="off" value="2"> Tier-2
                        </label>
                        <label class="btn  btn-outline-dark">
                            <input type="radio" name="check-level" id="option3" autocomplete="off" value="3"> Tier-3
                        </label>
                    </div>

                    <div class="invalid-feedback">
                        Feedback goes here.
                    </div>
                </div>
                <div class="col-md-10 mb-3">
                    <!-- two -->
                </div>
            </div>



        </div>
    </div>

    <div class="row mt-4">

        <!-- left side: form elements -->
        <div class="col-6">


            <!-- controls -->
            <div class="clearfix">
                <button type="button"  class="btn btn-success btn-circle btn-lg float-right add-entry"><i class="fa fa-plus"></i></button>
                <button type="button" class="btn btn-secondary btn-circle btn-lg float-right f-indent"><i class="fa fa-arrow-right"></i></button>
                <button type="button" class="btn btn-secondary btn-circle btn-lg float-right f-outdent"><i class="fa fa-arrow-left"></i></button>

                <button type="button" class="btn btn-secondary btn-circle btn-lg float-right f-up"><i class="fa fa-arrow-up"></i></button>
                <button type="button" class="btn btn-secondary btn-circle btn-lg float-right f-down"><i class="fa fa-arrow-down"></i></button>
            </div>

            <!-- form elements -->
            <label for="firstName">Prefix</label>
            <input type="text" class="form-control form-control-sm" id="itemPre" placeholder="" value="1.0" required="" placeholder="1.0">
            <div class="invalid-feedback">
                Feedback goes here.
            </div>

            <div>&nbsp</div>
            <label for="lastName">Text</label>
            <textarea class="form-control" id="itemContent" rows="2">new entry</textarea>
            <div class="invalid-feedback">
                Feedback goes here.
            </div>


            <div>&nbsp</div>
            <label for="firstName">Analytics Label</label>
            <input type="text" class="form-control form-control-sm" id="firstName" placeholder="" value="" required="">
            <div class="invalid-feedback">
                Feedback goes here.
            </div>

            <div>&nbsp</div>
            <label for="lastName">Distinguished PL Description (best)</label>
            <textarea class="form-control" id="itemContent" rows="2">new entry</textarea>
            <div class="invalid-feedback">
                Feedback goes here.
            </div>

            <div>&nbsp</div>
            <label for="lastName">Proficient PL Description</label>
            <textarea class="form-control" id="itemContent" rows="2">new entry</textarea>
            <div class="invalid-feedback">
                Feedback goes here.
            </div>

            <div>&nbsp</div>
            <label for="lastName">Apprentice PL Description</label>
            <textarea class="form-control" id="itemContent" rows="2">new entry</textarea>
            <div class="invalid-feedback">
                Feedback goes here.
            </div>

            <div>&nbsp</div>
            <label for="lastName">Novice PL Description (worst)</label>
            <textarea class="form-control" id="itemContent" rows="2">new entry</textarea>
            <div class="invalid-feedback">
                Feedback goes here.
            </div>


            <!-- controls -->
            <div class="clearfix mt-4">
                <button type="button" class="btn btn-success btn-circle btn-lg float-right add-entry"><i class="fa fa-plus"></i></button>
                <button type="button" class="btn btn-secondary btn-circle btn-lg float-right f-indent"><i class="fa fa-arrow-right"></i></button>
                <button type="button" class="btn btn-secondary btn-circle btn-lg float-right f-outdent"><i class="fa fa-arrow-left"></i></button>

                <button type="button" class="btn btn-secondary btn-circle btn-lg float-right f-up"><i class="fa fa-arrow-up"></i></button>
                <button type="button" class="btn btn-secondary btn-circle btn-lg float-right f-down"><i class="fa fa-arrow-down"></i></button>
            </div>

        </div>

        <!-- right side: form framework preview -->
        <div class="col-6" id="f-preview">

            <div class="f-group L1 selected" data-level="1">
                <input type="checkbox" class="form-check-input cbox">
                <div class="f-item pre">1.0</div>
                <div class="f-item content">new item</div>
            </div>

        </div>
    </div>

    <!-- <div class="starter-template">
      <h1>Bootstrap starter template</h1>
      <p class="lead">Use this document as a way to quickly start any new project.<br> All you get is this text and a mostly barebones HTML document.</p>
    </div> -->

</main>
