<?php
$user_current_account = $this->Session->read('user_current_account');
?>
<style>
.nav-tabs a{
    padding: 4px 17px;
}
</style>
<div class="container">
    <nav class="nav nav-tabs" style="margin-top: 0px;">
        <ul>
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><li <?php if (isset($tab) && $tab == 1): ?> class="active"  <?php endif; ?> ><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/1" ?>">School/District/Institution Settings</a></li><?php endif; ?>
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><li <?php if (isset($tab) && $tab == 2): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/2" ?>">Colors &amp; Logo</a></li><?php endif; ?>
            <li <?php if (isset($tab) && $tab == 7): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/7" ?>">Archiving</a></li>
            <li <?php if (isset($tab) && $tab == 3): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/3" ?>">Plans</a></li>
            <li <?php if (isset($tab) && $tab == 4): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/customer/updateBillingInfo/" . $account_id . "/4" ?>">Billing Information</a></li>
            <?php if ($sample_huddle_not_exists): ?>
            <li <?php if (isset($tab) && $tab == 6): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/6" ?>">Restore sample huddle</a></li>
            <?php endif; ?>
            <li <?php if (isset($tab) && $tab == 5): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/5" ?>">Cancel Account</a></li>
        </ul>
    </nav>
</div>
<div id="main" class="container box">
    <?php if ($braintree_subscription_id == 'canceled'): ?>
        <div class="message error" id="flashMessage">Subscription has already been canceled.</div>
    <?php else: ?>
        <div class="header">
            <h2 class="header-title">Cancel Account</h2>
        </div>
        <h2>Are you sure you want to cancel your account?</h2>
        <p>Thanks for trying Sibme. If you're sure you'd like to cancel your account, we'll be sorry to see you go!</p>

        <?php
        $users = $this->Session->read('user_current_account');

        if (!$users['accounts']['in_trial']) {
            ?>
            <p>Once you've canceled you won’t be charged again, but you are responsible for any charges already incurred. Please review our <a href="http://www.sibme.com/terms.html" target="_blank" style="font-weight:bold;">Terms of Use policy.</a></p>

            <?php
        }
        ?>

        <p  style="font-weight:bold;">Do you really want to cancel your account?</p>
        <p  style="font-weight:bold;">We hate to lose you. Your account is now in Trial Mode. Canceling your account will permanently delete all the data.</p>

        <a href="<?php echo $this->base . '/accounts/cancel_subscription/' . $account_id; ?>" class="btn btn-transparent" data-confirm="Are you sure you want to cancel your Account?" data-method="delete" rel="nofollow">Cancel your account</a>
        <a href="<?php echo $this->base . '/Dashboard/home'; ?>" style="text-decoration:underline;color:#777;font-size: 12px;font-weight: normal;">Nevermind, let me keep my account</a>

        <span style="float: right;width: 100%;margin-top: 28px;text-align: right;">Have questions? contact us at <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['info']; ?>"><?php echo $this->custom->get_site_settings('static_emails')['info']; ?></a></span>

    <?php endif; ?>
</div>
