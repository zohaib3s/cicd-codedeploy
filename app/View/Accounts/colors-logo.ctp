<?php
$user_current_account = $this->Session->read('user_current_account');
?>
<style>
    .nav-tabs a{
        padding: 4px 17px;
    }
</style>
<div class="container">
    <nav class="nav nav-tabs" style="margin-top: 0px;">
        <ul>
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><li <?php if (isset($tab) && $tab == 1): ?> class="active"  <?php endif; ?> ><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/1" ?>">School/District/Institution Settings</a></li><?php endif; ?>
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><li <?php if (isset($tab) && $tab == 2): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/2" ?>">Colors &amp; Logo</a></li><?php endif; ?>
            <li <?php if (isset($tab) && $tab == 7): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/7" ?>">Archiving</a></li>
            <?php if (!$deactive_plan): ?>
                <li <?php if (isset($tab) && $tab == 3): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/3" ?>">Plans</a></li>
                <li <?php if (isset($tab) && $tab == 4): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/customer/updateBillingInfo/" . $account_id . "/4" ?>">Billing Information</a></li>
            <?php endif; ?>
            <?php if ($sample_huddle_not_exists): ?>
                <li <?php if (isset($tab) && $tab == 6): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/6" ?>">Restore sample huddle</a></li>
            <?php endif; ?>
            <?php if (!$deactive_plan): ?>
                <li <?php if (isset($tab) && $tab == 5): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/5" ?>">Cancel Account</a></li>
            <?php endif; ?>
        </ul>
    </nav>
</div>

<div id="main" class="container box">
    <div class="header">
        <h2 class="header-title">Colors & Logo</h2>
    </div>

    <form accept-charset="UTF-8" action="<?php echo $this->base . '/accounts/tab_2_edit/' . $account_id . '/2' ?>" class="edit_account" enctype="multipart/form-data" id="edit_account_116" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="_method" type="hidden" value="put" /></div>
        <div id="first-picker" class="picker">
            <p>
                <label>
                    <b>Custom Header Background</b>
                </label>
            </p>
            <div class="color-picker-wrap">

                <input id="color_picker" type="text" value="" class="color-preview" style="background-color:<?php echo isset($myAccount['Account']['header_background_color']) ? '#' . $myAccount['Account']['header_background_color'] : $this->Custom->get_site_settings('site_header_color') ?>">
            </div>
        </div>

        <div id="second-picker" class="picker">
            <p>
                <label>
                    <b>Custom Tabs Color</b>
                </label>
            </p>
            <div class="color-picker-wrap">
                <input id="color_picker_2" type="text" value="" class="color-preview-2" style="background-color:<?php echo isset($myAccount['Account']['nav_bg_color']) ? '#' . $myAccount['Account']['nav_bg_color'] : $this->Custom->get_site_settings('site_header_color') ?>">
            </div>
        </div>

        <input id="account_header_background_color" name="account[header_background_color]" type="hidden" />
        <input id="account_nav_bg_color" name="account[nav_bg_color]" type="hidden" />
        <input id="account_usernav_bg_color" name="account[usernav_bg_color]" type="hidden" />
        <div class="row">
            <p class="span8">Brand the pages with your School/District/Institution color.</p>
        </div>

        <hr>

        <p>
            <label>
                <b>Current Logo</b>
            </label>
        </p>
        <div class="row">
            <p class="span6">We recommend a logo size of 150 x 100 pixels. Logos wider than 150 pixels will be resized to
                scale.</p>
            <div class = "span5 upload">
                <div class = "upload-preview">
                    <?php
                    $logo_path = $this->Custom->getSecureSibmecdnImageUrl("static/companies/" . $account_id . "/" . $myAccount['Account']['image_logo'], $myAccount['Account']['image_logo']);
                    ?>
                    <?php echo (isset($myAccount['Account']['image_logo']) && !empty($myAccount['Account']['image_logo'])) ? $this->Html->image($logo_path, array('alt' => 'Logo')) : $this->Html->image('company_logos/sample-logo.png', array('alt' => 'Logo')) ?>
                </div>
                <a href = "#" class = "upload-toggle">Change logo</a>
                <input id = "account_image_logo" name = "account[image_logo]" type = "file" />
            </div>
        </div>
        <a href = "<?php echo $this->base . '/accounts/reset_custom_theme/' . $account_id . '/2' ?>" class = "blue-link underline inline">Reset to default logo and color</a>
        <hr>

        <input id = "tab" name = "tab" type = "hidden" value = "2" />
        <div class = "form-actions lmargin-left lmargin-top">
            <button type = "submit" class = "btn btn-green">Save Changes</button>

            <a href = "<?php echo $this->base . '/' ?>" class = "btn btn-transparent">Cancel</a>
        </div>

    </form>
    <script type = "text/javascript">

        $(document).ready(function () {


            function ColorLuminance(hex, lum) {
                // validate hex string
                hex = String(hex).replace(/[^0-9a-f]/gi, '');
                if (hex.length < 6) {
                    hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
                }
                lum = lum || 0;
                // convert to decimal and change luminosity
                var rgb = "#", c, i;
                for (i = 0;
                        i < 3;
                        i++) {
                    c = parseInt(hex.substr(i * 2, 2), 16);
                    c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
                    rgb += ("00" + c).substr(c.length);
                }
                return rgb;
            }


            $('.upload-toggle').on('click', function (e) {
                e.preventDefault();
                $(this).next().trigger('click');
            });

            $('.upload-toggle').next().imageLoader({
                'show': '.upload-preview',
                'width': '150px'
            });


            var RGBtoHEX = function (color) {
                return "#" + $.map(color.match(/\b(\d+)\b/g), function (digit) {
                    return ('0' + parseInt(digit).toString(16)).slice(-2)
                }).join('');
            },
                    $el = $('.color-preview');

            $el.ColorPicker({
                color: RGBtoHEX($el.css('backgroundColor')),
                onChange: function (hsb, hex) {
                    $el.val(hex);
                    console.log(hex);
                    $el.val(hex).css('background-color', '#' + hex);
                    $("#account_header_background_color").val(hex);

                },
                onSubmit: function (hsb, hex, rgb, el) {
                    $el.val(hex).css('background-color', '#' + hex).ColorPickerHide();
                    $("#account_header_background_color").val(hex);

                    var darker = ColorLuminance(hex, -0.2);

                    $("#account_usernav_bg_color").val(darker);


                },
                onBeforeShow: function () {

                }
            });

            $el2 = $('.color-preview-2');

            $el2.ColorPicker({
                color: RGBtoHEX($el2.css('backgroundColor')),
                onChange: function (hsb, hex) {
                    $el2.val(hex);
                    console.log(hex);
                    $el2.val(hex).css('background-color', '#' + hex);
                    $("#account_nav_bg_color").val(hex);

                },
                onSubmit: function (hsb, hex, rgb, el) {
                    $el2.val(hex).css('background-color', '#' + hex).ColorPickerHide();
                    $("#account_nav_bg_color").val(hex);

                },
                onBeforeShow: function () {

                }
            });

        });</script>
</div>
