<?php
$user_current_account = $this->Session->read('user_current_account');
?>
<style>
.nav-tabs a{
    padding: 4px 17px;
}
</style>
<style type="text/css">
    .archiving{
        padding-left:110px;
        margin-bottom:28px;
    }
    .archiving_icon{
        background:url(/app/img/archive.png) no-repeat left center;        
    }
    .unarchiving_icon{
        background:url(/app/img/unarchive.png) no-repeat left center;        
    }
    .archiving h1{
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        font-size:26px;
        color:#525252;
        font-weight:normal;
        margin:0px 0px 15px 0px;
    }
    .archiving a{
        width:133px;
        padding:10px 0px;
        font-size:14px;
        color:#9d9d9d;
        border:solid 1px #d4d4d5;
        background:#f4f4f4;
        margin-right:10px;
        display:inline-block;
        text-align:center;
        text-decoration:none;
        border-radius:4px;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .archiving a:hover{
        border:solid 1px #a6a6a6;
    }
    .archiving a img{
        position:relative;
        top:2px;
        margin-right:6px;
    }

</style>
<div class="container">
    <nav class="nav nav-tabs" style="margin-top: 0px;">
        <ul>
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><li <?php if (isset($tab) && $tab == 1): ?> class="active"  <?php endif; ?> ><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/1" ?>">School/District/Institution Settings</a></li><?php endif; ?>
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><li <?php if (isset($tab) && $tab == 2): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/2" ?>">Colors &amp; Logo</a></li><?php endif; ?>
            <li <?php if (isset($tab) && $tab == 7): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/7" ?>">Archiving</a></li>
            <?php if (!$deactive_plan): ?>
            <li <?php if (isset($tab) && $tab == 3): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/3" ?>">Plans</a></li>
            <li <?php if (isset($tab) && $tab == 4): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/customer/updateBillingInfo/" . $account_id . "/4" ?>">Billing Information</a></li>
            <?php endif; ?>
            <?php if ($sample_huddle_not_exists): ?>
            <li <?php if (isset($tab) && $tab == 6): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/6" ?>">Restore sample huddle</a></li>
            <?php endif; ?>
            <?php if (!$deactive_plan): ?>
            <li <?php if (isset($tab) && $tab == 5): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/5" ?>">Cancel Account</a></li>
            <?php endif; ?>
        </ul>
    </nav>
</div>

<div id="main" class="container box">
    <div class="header">
        <h2 class="header-title">Archiving</h2>
    </div>
    <div class="archiving archiving_icon">
    <h1>Archive</h1>
    <a href="<?php echo $this->base . '/Huddles/archive_contents/1' ?>"><img src="/app/img/archive_huddle.png" width="16" height="13" alt=""/> Huddles </a>
    <a href="<?php echo $this->base . '/Huddles/archive_contents/2' ?>"><img src="/app/img/archive_folder.png" width="16" height="13" alt=""/> Folders</a>
</div>
<div class="archiving unarchiving_icon">
    <h1>Unarchive</h1>
    <a href="<?php echo $this->base . '/Huddles/unarchive_contents/1' ?>"><img src="/app/img/archive_huddle.png" width="16" height="13" alt=""/> Huddles </a>
    <a href="<?php echo $this->base . '/Huddles/unarchive_contents/2' ?>"><img src="/app/img/archive_folder.png" width="16" height="13" alt=""/> Folders</a>
</div>
</div>
