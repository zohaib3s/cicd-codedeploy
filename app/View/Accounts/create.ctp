<style>
.nav-tabs a{
    padding: 4px 17px;
}
</style>
<div id="main" class="container box create-account">
    <p style="font-size: 20px;"></p>
    <form accept-charset="UTF-8" action="<?php echo $this->base . '/Accounts/create/' . $user_id . '/' . $account_id ?>" class="edit_account" enctype="multipart/form-data" id="add-account" method="POST">
        <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="_method" type="hidden" value="put" /></div>
        <div class="header">
            <h2 class="header-title">School/District/Institution Settings</h2>
        </div>
        <p>
            <label>
                <b>School/District/Institution Name</b>
            </label>
        </p>
        <div class="input-group" style="float:left;width:100%;">
            <?php echo $this->Form->input('company_name', array('class' => 'size-medium', 'id' => 'account_company_name', 'size' => '30', 'type' => 'text', 'label' => FALSE, 'required' => 'required')); ?>
            <?php echo $this->Form->input('user_id', array('class' => 'size-medium', 'type' => 'hidden', 'value' => $user_id)); ?>
            <?php echo $this->Form->input('account_id', array('class' => 'size-medium', 'type' => 'hidden', 'value' => $account_id)); ?>
        </div>
        <div class="input-group">
            <button id="account_tab1_button" class="btn btn-green">Save Account</button>
            <a href="<?php echo $this->base . '/launchpad' ?>" class="btn btn-transparent">Cancel</a>
        </div>
    </form>
</div>
