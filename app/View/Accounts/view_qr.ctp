<script type="text/javascript" src="<?php echo $this->webroot; ?>js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="<?php echo $this->webroot; ?>js/qrcode.min.js"></script>
<div class="container_inner">
	<span class="page_title">Student Registration Code</span><br />
	<div class="code_details">
		<div id="qrcode" style="width:150px; height:150px; margin: 0 auto;"></div><br />
		<span class="details">
			Code: <?php echo $code;?> <br />
			Registration Link: <a href="<?php echo $registration_link?>"><?php echo $registration_link?></a>
		</span>
	</div>


</div>


<style>
	#header,.breadCrum
	{
		display: none;
	}
    #main
    {
        background: white;
    }
	.container
	{
		width: 100%;
		height: 100%;
		margin: 0;
		padding: 115px 10px;
	}
	#content
	{
		margin-top: 40px;
	}
	.page_title
	{
		width: 100%;
		text-align: center;
		font-size: 20px;
		float: left;
	}
	.code_details
	{
		text-align: center;
		margin-top: 40px;
	}
	.details
	{
		font-size: 16px;
		font-weight: bold;
	}
</style>
<script>
    (function($) {
        $(document).ready(function() {
			var qrcode = new QRCode(document.getElementById("qrcode"), {
				width : 150,
				height : 150
			});

			function makeCode () {
				qrcode.makeCode("<?php echo $registration_link;?>");
			}

			makeCode();
        });

    })(jQuery);
</script>
