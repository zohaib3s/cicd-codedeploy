<?php
$user_current_account = $this->Session->read('user_current_account');
?>
<style>
.nav-tabs a{
    padding: 4px 17px;
}
</style>
<div class="container">
    <nav class="nav nav-tabs" style="margin-top: 0px;">
        <ul>
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><li <?php if (isset($tab) && $tab == 1): ?> class="active"  <?php endif; ?> ><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/1" ?>">School/District/Institution Settings</a></li><?php endif; ?>
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><li <?php if (isset($tab) && $tab == 2): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/2" ?>">Colors &amp; Logo</a></li><?php endif; ?>
            <li <?php if (isset($tab) && $tab == 7): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/7" ?>">Archiving</a></li>
            <?php if (!$deactive_plan): ?>
            <li <?php if (isset($tab) && $tab == 3): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/3" ?>">Plans</a></li>
            <li <?php if (isset($tab) && $tab == 4): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/customer/updateBillingInfo/" . $account_id . "/4" ?>">Billing Information</a></li>
            <?php endif; ?>
            <li <?php if (isset($tab) && $tab == 6): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/6" ?>">Restore sample huddle</a></li>
            <?php if (!$deactive_plan): ?>
            <li <?php if (isset($tab) && $tab == 5): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/5" ?>">Cancel Account</a></li>
            <?php endif; ?>
        </ul>
    </nav>
</div>

<div id="main" class="container box">
    <div class="header">
        <h2 class="header-title">Restore Sample Huddles</h2>
    </div>
    <h2>Are you sure you want to restore sample huddles and users?</h2>

    <a href="<?php echo $this->base . '/huddles/restore'; ?>" class="btn btn-transparent" data-confirm="Are you sure you want to restore sample huddles?" data-method="delete" rel="nofollow">Restore sample huddles</a>
    
    <span style="float: left;width: 100%;margin-top: 28px;text-align: left;">Have questions? contact us at <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['info']; ?>"><?php echo $this->custom->get_site_settings('static_emails')['info']; ?></a></span>

</div>
