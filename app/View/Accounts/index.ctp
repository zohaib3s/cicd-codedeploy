<?php
$user_current_account = $this->Session->read('user_current_account');
//echo $deactive_plan;die;
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<style>
.nav-tabs a{
    padding: 4px 17px;
}
</style>
<div class="container">
    <nav class="nav nav-tabs" style="margin-top: 0px;">
        <ul>
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><li <?php if (isset($tab) && $tab == 1): ?> class="active"  <?php endif; ?> ><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/1" ?>">School/District/Institution Settings</a></li><?php endif; ?>
            <?php if (isset($user_current_account['accounts']['is_suspended']) && $user_current_account['accounts']['is_suspended'] != 1): ?><li <?php if (isset($tab) && $tab == 2): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/2" ?>">Colors &amp; Logo</a></li><?php endif; ?>
            <li <?php if (isset($tab) && $tab == 7): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/7" ?>">Archiving</a></li>
            <?php if (!$deactive_plan): ?>
            <li <?php if (isset($tab) && $tab == 3): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/3" ?>">Plans</a></li>
            <li <?php if (isset($tab) && $tab == 4): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/customer/updateBillingInfo/" . $account_id . "/4" ?>">Billing Information</a></li>
            <?php endif; ?>
            <?php if ($sample_huddle_not_exists): ?>
                <li <?php if (isset($tab) && $tab == 6): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/6" ?>">Restore sample huddle</a></li>
            <?php endif; ?>
                <?php if (!$deactive_plan): ?>
            <li <?php if (isset($tab) && $tab == 5): ?> class="active"  <?php endif; ?>><a href="<?php echo $this->base . "/accounts/accountSettings/" . $account_id . "/5" ?>">Cancel Account</a></li>
            <?php endif; ?>
        </ul>
    </nav>
</div>
<div id="main" class="container box">
    <form accept-charset="UTF-8" action="<?php echo $this->base . '/accounts/tab_1_edit/' . $account_id . '/1' ?>" class="edit_account" enctype="multipart/form-data" id="edit_account_116" method="post">
        <div style="margin:0;padding:0;display:inline">
            <input name="utf8" type="hidden" value="&#x2713;" />
            <input name="_method" type="hidden" value="put" />
        </div>
        <div class="header">
            <h2 class="header-title">School/District/Institution Information</h2>
        </div>
        <p>
            <label>
                <b>School/District/Institution Name</b>
            </label>
        </p>
        <div class="input-group">
            <input class="size-medium" id="account_company_name" name="account[company_name]" size="30" type="text" value="<?php echo $myAccount['Account']['company_name'] ?>" />
        </div>
<!--        <p>
            <label>
                <b>Trial Date Picker</b>
            </label>
        </p>
        <div class="input-group">
            <input class="size-medium" id="trial_created_at" name="account[create_at]" size="30" type="text" value="<?php echo date('Y-m-d', strtotime($myAccount['Account']['created_at'])) ?>" />
        </div>-->
        <input id="tab" name="tab" type="hidden" value="1" />
        <input type="hidden" id="txtchkframework" name="txtchkframework" value="<?php echo $framwork_value; ?>" />
        <input type="hidden" id="txtchkenablewstags" name="txtchkenablewstags" value="<?php echo $chkenablewstags; ?>" />
        <input type="hidden" id="txtchkenablewsframework" name="txtchkenablewsframework" value="<?php echo $chkenablewsframework; ?>" />
        <input type="hidden" id="defaultframework" name="defaultframework" value="<?php echo $default_framework; ?>" />
        <input type="hidden" id="txtchktags" name="txtchktags" value="<?php echo $tag_value; ?>" />
        <input type="hidden" id="enabletracking1" name="enabletracking" value="<?php echo $enabletracking; ?>" />
        <input type="hidden" id="enableanalytics" name="enableanalytics" value="<?php echo $enableanalytics; ?>" />
        <input type="hidden" id="txtchktracker" name="txtchktracker" value="<?php echo $ch_tracker_value; ?>" />
        <input type="hidden" id="txtchkmatric" name="txtchkmatric" value="<?php echo $matric_value; ?>" />
        <input type="hidden" id="txtchkvideos" name="txtchkvideos" value="<?php echo $video_value; ?>" />
        <input type="hidden" id="txtanalyticsduration" name="txtanalyticsduration" value="<?php echo $duration_value; ?>" />
        <input type="hidden" id="txttrackingduration" name="txttrackingduration" value="<?php echo $tracking_value; ?>" />
        <input type="hidden" id="txtembedlink" name="txtembedlink" value='<?php echo stripslashes($external_embeded_link); ?>' />
        <button id="company_info_submit_button" type="submit" style="display: none;"></button>
    </form>
    <hr>
    <p>
        <label>
            <b>Change account owner</b>
        </label>
    </p>
    <form accept-charset="UTF-8" action="<?php echo $this->base . '/accounts/changeAccountOwner/' . $account_id . '/1' ?>" enctype="multipart/form-data" method="POST">
        <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
        <div class="input-group select size-medium inline">
            <?php echo $this->Form->input('user_id', array('id' => 'user_user_id', 'options' => $allAccountOwner, 'selected' => $user_id, 'type' => 'select', 'label' => FALSE, 'div' => FALSE, 'required' => 'required')); ?>
        </div>
        <input id="account_owner_user_id" name="account_owner_user_id" type="hidden" value="<?php echo $user_id; ?>" />
        <input type="submit" style="background-color: transparent;text-decoration: underline;border: none;cursor: pointer;" class="blue-link underline inline" value="Select as new account owner">
        <hr id="hrduration" style="display:none;">
    <p id="panalytics_duration" style="display:none;">
        <label>
            <b>Change Analytics Duration</b>
        </label>
    </p>
        <div style="display:none;" id="divanalytics_duration" class="input-group select size-medium inline">
            <select name="analytics_duration" id="analytics_duration" required="required">
                <option value="01" <?php echo $duration_value == '01' ? 'selected=selected' : ''; ?>>Jan</option>
                <option value="02" <?php echo $duration_value == '02' ? 'selected=selected' : ''; ?>>Feb</option>
                <option value="03" <?php echo $duration_value == '03' ? 'selected=selected' : ''; ?>>Mar</option>
                <option value="04" <?php echo $duration_value == '04' ? 'selected=selected' : ''; ?>>Apr</option>
                <option value="05" <?php echo $duration_value == '05' ? 'selected=selected' : ''; ?>>May</option>
                <option value="06" <?php echo $duration_value == '06' ? 'selected=selected' : ''; ?>>Jun</option>
                <option value="07" <?php echo $duration_value == '07' ? 'selected=selected' : ''; ?>>Jul</option>
                <option value="08" <?php echo $duration_value == '08' ? 'selected=selected' : ''; ?>>Aug</option>
                <option value="09" <?php echo $duration_value == '09' ? 'selected=selected' : ''; ?>>Sep</option>
                <option value="10" <?php echo $duration_value == '10' ? 'selected=selected' : ''; ?>>Oct</option>
                <option value="11" <?php echo $duration_value == '11' ? 'selected=selected' : ''; ?>>Nov</option>
                <option value="12" <?php echo $duration_value == '12' ? 'selected=selected' : ''; ?>>Dec</option>
            </select>
        </div>
        <p id="cpduration" style="display:none;">
        <label>
            <b id="bduration">Coaching Tracker Feedback Duration</b>
        </label>
    </p>
    <p id="apduration" style="display:none;">
        <label>
            <b id="b1duration">Assessment Tracker Feedback Duration</b>
        </label>
    </p>
        <div id="duration" style="display:none;" class="input-group select size-medium inline">
            <select name="tracking_duration" id="tracking_duration" required="required">
                <option value="12" <?php echo $tracking_value == '12' ? 'selected=selected' : ''; ?>>12 Hours</option>
                <option value="24" <?php echo $tracking_value == '24' ? 'selected=selected' : ''; ?>>24 Hours</option>
                <option value="36" <?php echo $tracking_value == '36' ? 'selected=selected' : ''; ?>>36 Hours</option>
                <option value="48" <?php echo $tracking_value == '48' ? 'selected=selected' : ''; ?>>48 Hours</option>
                <option value="60" <?php echo $tracking_value == '60' ? 'selected=selected' : ''; ?>>60 Hours</option>
                <option value="72" <?php echo $tracking_value == '72' ? 'selected=selected' : ''; ?>>72 Hours</option>
                <option value="120" <?php echo $tracking_value == '120' ? 'selected=selected' : ''; ?>>5 Days</option>
                <option value="168" <?php echo $tracking_value == '168' ? 'selected=selected' : ''; ?>>1 Week</option>
                <option value="336" <?php echo $tracking_value == '336' ? 'selected=selected' : ''; ?>>2 Week</option>
                
                
            </select>
        </div>
        <hr>
        <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
            <?php if(!empty($frameworks) && count($frameworks)>0):?>
            <p>
            <label>
                <b>Default Framework/Rubric</b>
            </label>
            </p>
            <div class="input-group select size-medium inline">
            <select name="frameworks" id="frameworks">
                <option value="">Select Framework</option>
                <?php
                foreach($frameworks as $framework){?>
                    <option value="<?php echo $framework['AccountTag']['account_tag_id'] ?>" <?php echo $default_framework == $framework['AccountTag']['account_tag_id'] ? 'selected=selected' : ''; ?>><?php echo $framework['AccountTag']['tag_title'] ?></option>
               <?php }
                ?>
            </select>
            </div>
            <a class="blue-link underline inline" href = "<?php echo $this->base . '/accounts/frameworks/' . $account_id ?>">Change Framework Names</a>
            <hr>
            <?php endif;?>
        <?php endif; ?>
        <?php if($is_embeded_enable == 1){?>
        <p>
        <label>
            <b>Region 4 Tutorial Embedded link</b>
        </label>
    </p>
        <div class="input-group size-medium inline">
            <textarea name="external_video_link" id="external_video_link" cols="50" rows="5"><?php echo stripslashes($external_embeded_link); ?></textarea>
        </div>
        <hr>
        <?php }?>
        <input type="checkbox" id="chkenablevideos" name="chkenablevideos" <?php echo $video_value == '1' ? 'checked' : ''; ?>><label for="chkenablevideos" style="padding-left: 7px;">Enable best practices account Video Library in this account</label>
        <br><br>
        <input type="checkbox" id="chkenabletags" name="chkenabletags" <?php echo $tag_value == '1' ? 'checked' : ''; ?>><label for="chkenabletags" style="padding-left: 7px;">Enable custom Video Markers in this account</label>
        <br><br>
        <input type="checkbox" id="chkenablewstags" name="chkenablewstags" <?php echo $chkenablewstags == '1' ? 'checked' : ''; ?>><label for="chkenablewstags" style="padding-left: 7px;">Enable custom Video Markers in Workspace</label>
        <br><br>
        <?php if (isset($standards) && count($standards)>0) {?>
        <input type="checkbox" id="chkenableframework" name="chkenableframework" <?php echo $framwork_value == '1' ? 'checked' : ''; ?>><label for="chkenableframework" style="padding-left: 7px;">Enable Framework/Rubric in this account</label>
        <br><br>
        <?php }else{?>Are you interested in adding a custom framework to your account?  If so, contact <a href="mailto:<?php echo $this->custom->get_site_settings('static_emails')['info']; ?>"><?php echo $this->custom->get_site_settings('static_emails')['info']; ?></a><br><br><?php } ?>
        <input type="checkbox" id="chkenablewsframework" name="chkenablewsframework" <?php echo $chkenablewsframework == '1' ? 'checked' : ''; ?>><label for="chkenablewsframework" style="padding-left: 7px;">Enable Frameworks in Workspace</label>
        <br><br>
        
        <input type="checkbox" id="chkenableanalytics" name="enableanalytics" <?php echo $enableanalytics == '1' ? 'checked' : ''; ?>><label for="chkenableanalytics" style="padding-left: 7px;">Enable Analytics</label>
        <br><br>
        <input type="checkbox" id="enabletracking" name="enabletracking1" <?php echo $enabletracking == '1' ? 'checked' : ''; ?>><label for="enabletracking" style="padding-left: 7px;">Enable Tracking</label>
        <br><br>
       &nbsp; &nbsp; <input type="radio" id="chkenabletracker" name="chkenabletracker" <?php echo $ch_tracker_value == '1' ? 'checked' : ''; ?>><label id="labelchkenabletracker" for="chkenabletracker" style="padding-left: 7px;">Enable Coaching Tracker in this account</label>
        <br><br>
       &nbsp; &nbsp; <input type="radio" id="chkenablematric" name="chkenabletracker" <?php echo $matric_value == '1' ? 'checked' : ''; ?>><label id="labelchkenablematric" for="chkenablematric" style="padding-left: 7px;">Enable Assessment Tracker in this account</label>
    </form>
    <hr>
    <div class="row-fluid row-marker" <?php echo $tag_value == '1' ? 'style="display:block;"' : 'style="display:none;"'; ?>>
        <div class="groups-table span12">

            <div class="span4 huddle-span4" style="margin-left:0px;">
                <div class="groups-table-header">
                    <h2>Create custom Video Markers for Huddles in Account</h2>                 
                    <div style="clear:both"></div>                                                               
                </div>
                <div class="groups-table-content">
                    <div class="widget-scrollable">
                        <div class="scrollbar" style="height: 155px;">
                            <div class="track" style="height: 155px;">
                                <div class="thumb" style="top: 0px; height: 90.3195px;">
                                    <div class="end"></div>
                                </div>
                            </div>
                        </div>
                        <div class="viewport short">
                            <div class="overview" style="top: 0px;">
                                <div  id="people-lists">
                                    <ul id="list-containers">
                                        <?php
                                        for ($i = 0; $i < 4; $i++) {
                                            if (isset($tags[$i])) {
                                                ?>
                                                <li class="cledittags">
                                                    <label  class="huddle_permission_editor_row" for="super_admin_ids_">
                                                        <span><?php echo $i + 1; ?>. </span>
                                                        <input class="super-user" type="checkbox" value="" name="tags_id[]" id="super_admin_ids_" style="display:none;"> 
                                                        <a style="color: #757575; font-weight: normal;"><?php echo $tags[$i]['AccountTag']['tag_title']; ?></a>
                                                        <input type="text" id="txtedittag" name="txtedittag" value="<?php echo $tags[$i]['AccountTag']['tag_title']; ?>" style="display:none;" class="txtedittag" />
                                                        <input type="hidden" id="txttagid" name="txttagid" value="<?php echo $tags[$i]['AccountTag']['account_tag_id']; ?>" class="txttagid" />
                                                    </label>     
                                                    <span style="float: right;margin-right: 19px;cursor: pointer" class="clearTag">X</span>
                                                </li>

                                                <?php
                                            } else {
                                                ?>                                                
                                                <li class="cledittags">

                                                    <label  class="huddle_permission_editor_row" for="super_admin_ids_">
                                                        <span><?php echo $i + 1; ?>. </span>
                                                        <input class="super-user" type="checkbox" value="" name="tags_id[]" id="super_admin_ids_" style="display:none;"> 
                                                        <a style="color: #757575; font-weight: normal;">Add Tag</a>
                                                        <input type="text" id="txtedittag" name="txtedittag" value="" style="display:none;" class="txtedittag" />
                                                        <input type="hidden" id="txttagid" name="txttagid" value="nw_<?php echo $i; ?>" class="txttagid" />
                                                    </label>         
                                                    <span style="float: right;margin-right: 19px;cursor: pointer" class="clearTag">X</span>
                                                </li>

                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row-fluid row-metric" <?php echo $matric_value == '1' ? 'style="display:block;"' : 'style="display:none;"'; ?>>
        <div class="groups-table span12">

            <div class="span4 huddle-span4" style="margin-left:0px;">
                <div class="groups-table-header">
                    <h2>Create Assessment Ratings in this account</h2>                 
                    <div style="clear:both"></div>                                                               
                </div>
                <div class="groups-table-content">
                    <div class="widget-scrollable">
                        <div class="scrollbar" style="height: 155px;">
                            <div class="track" style="height: 155px;">
                                <div class="thumb" style="top: 0px; height: 90.3195px;">
                                    <div class="end"></div>
                                </div>
                            </div>
                        </div>
                        <div class="viewport short">
                            <div class="overview" style="top: 0px;">
                                <div  id="people-lists">
                                    <ul id="list-containers">
                                        <?php
                                        for ($i = 0; $i < 5; $i++) {
                                            if (isset($metrics[$i])) {
                                                $str = substr($metrics[$i]['AccountMetaData']['meta_data_name'],13);
                                                ?>
                                                <li class="cleditmetrics">
                                                    <label  class="huddle_permission_editor_row" for="super_admin_ids_">
                                                        <span><?php echo $i+1; ?>. </span>
                                                        <input class="super-user" type="checkbox" value="" name="metric_id[]" id="super_admin_ids_" style="display:none;"> 
                                                        <a style="color: #757575; font-weight: normal;"><?php echo $str; ?></a>
                                                        <input type="text" id="txteditmetric" name="metrics[][nw_<?php echo $i+1; ?>]" value="<?php echo $str; ?>" style="display:none;" class="txteditmetric" />
                                                        <input type="hidden" id="txtmetricid" name="metrics1[][nw_<?php echo $i+1; ?>]" value="nw_<?php echo $i+1; ?>" key="<?php echo $str; ?>" class="txtmetricid" />
                                                    </label>     
                                                    <span style="float: right;margin-right: 19px;cursor: pointer" class="clearMetric">X</span>
                                                </li>

                                                <?php
                                            } else {
                                                ?>                                                
                                                <li class="cleditmetrics">

                                                    <label  class="huddle_permission_editor_row" for="super_admin_ids_">
                                                        <span><?php echo $i+1; ?>. </span>
                                                        <input class="super-user" type="checkbox" value="" name="metric_id[]" id="super_admin_ids_" style="display:none;"> 
                                                        <a style="color: #757575; font-weight: normal;">Add Rating</a>
                                                        <input type="text" id="txteditmetric" name="txteditmetric" value="" style="display:none;" class="txteditmetric" />
                                                        <input type="hidden" id="txtmetricid" name="txtmetricid" value="nw_<?php echo $i+1; ?>" class="txtmetricid" />
                                                    </label>         
                                                    <span style="float: right;margin-right: 19px;cursor: pointer" class="clearMetric">X</span>
                                                </li>

                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="form-actions lmargin-left lmargin-top">
        <button id="account_tab1_button" class="btn btn-green">Save Changes</button>
        <a href = "<?php echo $this->base . '/' ?>" class="btn btn-transparent">Cancel</a>
    </div>    
    <script type="text/javascript">
        $(document).ready(function () {
            $('#btnAddToAccount_addHuddle').click(function () {
                addToInviteList();
            });
            $("#account_tab1_button").click(function () {
                var count = 0;
                var addcount = 0;
                var edittag = 0;
                var count12 = 0;
                $('#txtembedlink').val($('#external_video_link').val());
                if ($("#chkenabletags").is(':checked')) {
                    $('.txttagid').each(function (value) {
                        var input_value = $(this).val();
                        if (input_value.search('nw') >= 0) {
                            count++;
                            var new_tag_val = $('#txtedittag_' + input_value).val();
                            if (typeof new_tag_val !== "undefined" && typeof new_tag_val !== null && new_tag_val.length > 0) {
                                addcount++;
                            }
                        }
                        else {
                            if (input_value.length > 0 && $(this).parent().find("#txtedittag").val().length == 0) {
                                edittag++;
                            }
                        }

                    });

                }
                if ($("#chkenablematric").is(':checked')) {
                    cnt = 1;
                    $('.txteditmetric').each(function (value) {
                        tag_name1 = $(this).val();
                        add_html_form = '<input type="hidden" name="metrics[][nw_' + cnt + ']" value="' + tag_name1 + '" id="txteditmetric_nw_' + cnt + '" />';
                        $('#edit_account_116').append(add_html_form);
                        cnt++;
                    });
                    $('.txtmetricid').each(function (value) {
                        var input_value = $(this).val();
                        if (input_value.search('nw') >= 0) {
                            count++;
                            var new_tag_val = $('#txteditmetric_' + input_value).val();
                            if (typeof new_tag_val !== "undefined" && typeof new_tag_val !== null && new_tag_val.length > 0) {
                                addcount++;
                            }
                        }
                        else {
                            if (input_value.length > 0 && $(this).parent().find("#txteditmetric").val().length == 0) {
                                edittag++;
                            }
                        }

                    });

                }
                $("#company_info_submit_button").click();
                
            });
            $('#enabletracking').on('change', function () {
             if ($(this).is(':checked')) {
                   $('#enabletracking1').val('1');
                   $('#chkenabletracker').show();
                   $('#chkenablematric').show();
                   $('#labelchkenabletracker').show();
                   $('#labelchkenablematric').show();
                   $('#cpduration').show();
                   $('#duration').show();
                   $('#hrduration').show();
                   $('#apduration').show();
                   
                   if ($('#chkenabletracker').is(':checked')) {
                    $('#txtchkmatric').val('0');
                    $('#txtchktracker').val('1');
                    $('.row-metric').hide();
                    $("#hrduration").show();
                    $("#apduration").hide();
                    $("#cpduration").show();
                    $("#duration").show();
                    
                } else {
                    $('#txtchktracker').val('0');
                }
                
                if ($('#chkenablematric').is(':checked')) {
                    $('.row-metric').show();
                    $('#txtchktracker').val('0');
                    $('#txtchkmatric').val('1');
                    $("#hrduration").show();
                    $("#cpduration").hide();
                    $("#apduration").show();
                    $("#duration").show();
                    //$('#chkenabletags').attr('checked', false);
                    //$('.row-marker').hide();
                    //$('#txtchktags').val('0');
                    $('.widget-scrollable').tinyscrollbar();
                } else {
                    $('#txtchkmatric').val('0');
                    $('.row-metric').hide();
                }
                   
                   
                } else {
                    $('#enabletracking1').val('0');
                    $('#txtchkmatric').val('0');
                    $('.row-metric').hide();
                    $('#txtchktracker').val('0');
                    $('#chkenabletracker').hide();
                    $('#chkenablematric').hide();
                    $('#labelchkenabletracker').hide();
                    $('#labelchkenablematric').hide();
                    $('#cpduration').hide();
                    $('#duration').hide();
                    $('#hrduration').hide();
                    $('#apduration').hide();
                }
            });
            if ($("#enabletracking").is(':checked')) {
                   $('#enabletracking1').val('1');
                   $('#chkenabletracker').show();
                   $('#chkenablematric').show();
                   $('#labelchkenabletracker').show();
                   $('#labelchkenablematric').show();
                   $('#cpduration').show();
                   $('#duration').show();
                   $('#hrduration').show();
                   $('#apduration').show();
                   
                   if ($('#chkenabletracker').is(':checked')) {
                    $('#txtchkmatric').val('0');
                    $('#txtchktracker').val('1');
                    $('.row-metric').hide();
                    $("#hrduration").show();
                    $("#apduration").hide();
                    $("#cpduration").show();
                    $("#duration").show();
                    
                } else {
                    $('#txtchktracker').val('0');
                }
                
                if ($('#chkenablematric').is(':checked')) {
                    $('.row-metric').show();
                    $('#txtchktracker').val('0');
                    $('#txtchkmatric').val('1');
                    $("#hrduration").show();
                    $("#cpduration").hide();
                    $("#apduration").show();
                    $("#duration").show();
                   // $('#chkenabletags').attr('checked', false);
                    //$('.row-marker').hide();
                    //$('#txtchktags').val('0');
                    $('.widget-scrollable').tinyscrollbar();
                } else {
                    $('#txtchkmatric').val('0');
                    $('.row-metric').hide();
                }
                   
                   
                } else {
                    $('#enabletracking1').val('0');
                    $('#txtchkmatric').val('0');
                    $('.row-metric').hide();
                    $('#txtchktracker').val('0');
                    $('#chkenabletracker').hide();
                    $('#chkenablematric').hide();
                    $('#labelchkenabletracker').hide();
                    $('#labelchkenablematric').hide();
                    $('#cpduration').hide();
                    $('#duration').hide();
                    $('#hrduration').hide();
                    $('#apduration').hide();
                }
            $('#chkenablevideos').on('change', function () {
                var status_value = '0';
                if ($(this).is(':checked')) {
                    $('#txtchkvideos').val('1');
                } else {
                    $('#txtchkvideos').val('0');
                }
            });
            $('#chkenableframework').on('change', function () {
                var status_value = '0';
                if ($(this).is(':checked')) {
                    $('#txtchkframework').val('1');
                } else {
                    $('#txtchkframework').val('0');
                }
            });
            $('#chkenablewstags').on('change', function () {
                var status_value = '0';
                if ($(this).is(':checked')) {
                    $('#txtchkenablewstags').val('1');
                } else {
                    $('#txtchkenablewstags').val('0');
                }
            });
            $('#chkenablewsframework').on('change', function () {
                var status_value = '0';
                if ($(this).is(':checked')) {
                    $('#txtchkenablewsframework').val('1');
                } else {
                    $('#txtchkenablewsframework').val('0');
                }
            });
            
            $('#analytics_duration').on('change', function () {
                $('#txtanalyticsduration').val($('#analytics_duration').val());
            });
            $('#tracking_duration').on('change', function () {
                $('#txttrackingduration').val($('#tracking_duration').val());
            });
            $('#frameworks').on('change', function () {
                $('#defaultframework').val($('#frameworks').val());
            });
            $('#chkenabletags').on('change', function () {
                if ($(this).is(':checked')) {
                    $('#txtchktags').val('1');
                    $('.row-marker').show();
                    $('.widget-scrollable').tinyscrollbar();
                } else {
                    $('#txtchktags').val('0');
                    $('.row-marker').hide();
                }
            });
            $('#chkenablematric').click(function () {
                if ($(this).is(':checked')) {
                    $('.row-metric').show();
                    $('#txtchktracker').val('0');
                    $('#txtchkmatric').val('1');
                    $("#hrduration").show();
                    $("#cpduration").hide();
                    $("#apduration").show();
                    $("#duration").show();
                   // $('#chkenabletags').attr('checked', false);
                    //$('.row-marker').hide();
                    //$('#txtchktags').val('0');
                    $('.widget-scrollable').tinyscrollbar();
                } else {
                    $('#txtchkmatric').val('0');
                    $('.row-metric').hide();
                }
            });
            $('#chkenabletracker').click(function () {
                var status_value = '0';
                if ($(this).is(':checked')) {
                    $('#txtchkmatric').val('0');
                    $('#txtchktracker').val('1');
                    $('.row-metric').hide();
                    $("#hrduration").show();
                    $("#apduration").hide();
                    $("#cpduration").show();
                    $("#duration").show();
                   
                    
                } else {
                    $('#txtchktracker').val('0');
                }
            });
            
                if ($('#chkenabletracker').is(':checked')) {
                    $('#txtchkmatric').val('0');
                    $('#txtchktracker').val('1');
                    $('.row-metric').hide();
                    $("#hrduration").show();
                    $("#apduration").hide();
                    if ($("#enabletracking").is(':checked')) {
                    $("#cpduration").show();
                    $("#duration").show();
                }
                    
                } else {
                    $('#txtchktracker').val('0');
                }
                
                if ($('#chkenablematric').is(':checked')) {
                    $('.row-metric').show();
                    $('#txtchktracker').val('0');
                    $('#txtchkmatric').val('1');
                    $("#hrduration").show();
                    $("#cpduration").hide();
                    $("#apduration").show();
                    $("#duration").show();
                   // $('#chkenabletags').attr('checked', false);
                    //$('.row-marker').hide();
                    //$('#txtchktags').val('0');
                    $('.widget-scrollable').tinyscrollbar();
                } else {
                    $('#txtchkmatric').val('0');
                    $('.row-metric').hide();
                }
            
            
            
            
            $('#pop-up-btn').click(function () {
                $('.fmargin-left').attr('disabled', false);
            });

            $('.cledittags').on('click', function (e) {
                e.preventDefault();
                $(this).find('a').hide();
                $(this).find('.txtedittag').show();
                $(this).find('.txtedittag').focus();
            });
            $('.cleditmetrics').on('click', function (e) {
                e.preventDefault();
                $(this).find('a').hide();
                $(this).find('.txteditmetric').show();
                $(this).find('.txteditmetric').focus();
            });
            $('.clearTag').on('click', function (e) {
                e.preventDefault();
                $(this).parent().find('a').hide();
                $(this).parent().find('.txtedittag').show();
                $(this).parent().find('.txtedittag').val('');
                $(this).parent().find('.txtedittag').focus();

            });
            $('.clearMetric').on('click', function (e) {
                e.preventDefault();
                $(this).parent().find('a').hide();
                $(this).parent().find('.txteditmetric').show();
                $(this).parent().find('.txteditmetric').val('');
                $(this).parent().find('.txteditmetric').focus();

            });
            $('.txtedittag').on('keyup', function (e) {
                var code = e.which;
                var add_html_form = '';
                var tag_name = '';
                var key = '';
                if (code == 13) {
                    tag_name = $(this).val();
                    $(this).hide();
                    $(this).parent().find('a').text(tag_name);
                    $(this).parent().find('a').show();
                    key = $(this).parent().find('.txttagid').val();
                    if (!$('#txtedittag_' + key).is('*')) {
                        add_html_form = '<input type="hidden" name="tags[][' + key + ']" value="' + tag_name + '" id="txtedittag_' + key + '" />';
                        $('#edit_account_116').append(add_html_form);
                    }
                    else {
                        $('#txtedittag_' + key).val(tag_name);
                    }
                }
            });
            $('.txteditmetric').on('keyup', function (e) {
                var code = e.which;
                var add_html_form = '';
                var tag_name = '';
                var key = '';
                if (code == 13) {
                    tag_name = $(this).val();
                    $(this).hide();
                    $(this).parent().find('a').text(tag_name);
                    $(this).parent().find('a').show();
                    key = $(this).parent().find('.txtmetricid').val();
                    if (!$('#txteditmetric_' + key).is('*')) {
                        //add_html_form = '<input type="hidden" name="metrics[][' + key + ']" value="' + tag_name + '" id="txteditmetric_' + key + '" />';
                        //$('#edit_account_116').append(add_html_form);
                    }
                    else {
                        $('#txteditmetric_' + key).val(tag_name);
                    }
                }
            });
            $('.txtedittag').on('focusout', function (e) {
                var code = e.which;
                var add_html_form = '';
                var tag_name = '';
                var key = '';
                tag_name = $(this).val();
                $(this).hide();
                $(this).parent().find('a').text(tag_name);
                $(this).parent().find('a').show();
                key = $(this).parent().find('.txttagid').val();

                if (!$('#txtedittag_' + key).is('*')) {
                    add_html_form = '<input type="hidden" name="tags[][' + key + ']" value="' + tag_name + '" id="txtedittag_' + key + '" />';
                    $('#edit_account_116').append(add_html_form);
                }
                else {
                    $('#txtedittag_' + key).val(tag_name);
                }
            });
            $('.txteditmetric').on('focusout', function (e) {
                var code = e.which;
                var add_html_form = '';
                var tag_name = '';
                var key = '';
                tag_name = $(this).val();
                $(this).hide();
                $(this).parent().find('a').text(tag_name);
                $(this).parent().find('a').show();
                key = $(this).parent().find('.txtmetricid').val();
                if (!$('#txteditmetric_' + key).is('*')) {
                    //add_html_form = '<input type="hidden" name="metrics[][' + key + ']" value="' + tag_name + '" id="txtmetrictag_' + key + '" />';
                    //$('#edit_account_116').append(add_html_form);
                }
                else {
                    $('#txteditmetric_' + key).val(tag_name);
                }
            });
            
            $('#chkenableanalytics').on('change', function () {
             if ($(this).is(':checked')) {
                   $('#enableanalytics').val('1');
                   $('#panalytics_duration').show();
                   $('#divanalytics_duration').show();
                   
               }
               else{
                   
                   $('#enableanalytics').val('0');
                    $('#panalytics_duration').hide();
                   $('#divanalytics_duration').hide();
                   
               }
           });
           
           if ($('#chkenableanalytics').is(':checked')) {
                   $('#enableanalytics').val('1');
                   $('#panalytics_duration').show();
                   $('#divanalytics_duration').show();
                   
               }
               else{
                   
                   $('#enableanalytics').val('0');
                    $('#panalytics_duration').hide();
                   $('#divanalytics_duration').hide();
                   
               }
            
            
            
        });
        $(function () {
            $("#trial_created_at").datepicker();
        });
        function addToInviteList() {
            var lposted_data = [];
            var userNames = document.getElementsByName('users[][name]');

            for (i = 0; i < userNames.length; i++) {
                var userName = $(userNames[i]);
                if (userName.val() != '' != '') {
                    if (userName.val() == '') {
                        alert('Please enter a valid Full Name.');
                        userName.focus();
                        return false;
                    }

                    var newUser = [];
                    newUser[newUser.length] = '';
                    newUser[newUser.length] = userName.val();
                    lposted_data[lposted_data.length] = newUser;
                }
            }

            if (lposted_data.length == 0) {
                alert('Please enter atleast one Full name or email.');
                return;
            }
            $('#flashMessage2').css('display', 'none');

            new_ids = 1;

            for (var i = 0; i < lposted_data.length; i++) {

                var data = lposted_data[i];
                var html = '';

                html += '<li>';
                html += '<label class="huddle_permission_editor_row" for="super_admin_ids_' + new_ids + '"><input type="checkbox" style="display:none;" value="' + new_ids + '" name="super_admin_ids[]" id="super_admin_ids_' + new_ids + '"> ' + data[1] + ' </label>';

                html += '</li>';
                $('.groups-table .huddle-span4 .groups-table-content ul').append(html);
                var add_html_form = '';
                add_html_form += '<input type="hidden" name="tags[]" value="' + data[1] + '"/>';
                $('#edit_account_116').append(add_html_form);


                new_ids -= 1;
                var newUser = [];
                newUser[newUser.length] = data[0];
                newUser[newUser.length] = data[1];
                var $overview = $('.widget-scrollable.horizontal .overview');
                $.each($overview, function () {

                    var width = $.map($(this).children(), function (child) {
                        return $(child).outerWidth() +
                                $(child).pixels('margin-left') + $(child).pixels('margin-right');
                    });
                    $(this).width($.sum(width));

                });

                $('.widget-scrollable').tinyscrollbar();
                $('#addSuperAdminModal').modal('hide');
                $('#tagnofound').remove();

            }
        }
    </script>
</div>
