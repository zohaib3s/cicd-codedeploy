<?php  $sibme_base_url = Configure::read('sibme_base_url');?>
<div class="box videos-library">
    <div class="header header--large header-huddle">        
        <h1 class="header__title">Turn off email notifications. </h1>
    </div>
    <div class="videos-library__main" style="padding: 20px; font-weight: bold;">
        <?php echo $message . '.'; ?> <span> <a href="<?php echo $sibme_base_url ?>dashboard/home" class="btn green"> Back to Dashboard</a></span>
    </div>

</div>
