<?php $breadcrumb_language_based_content = $this->Custom->get_page_lang_based_content('BreadCrumb'); ?>
<?php
//$huddle_permission = $this->Session->read('user_huddle_level_permissions');
//$user_current_account = $this->Session->read('user_current_account');
$user_role = $this->Custom->get_user_role_name($user_current_account['users_accounts']['role_id']);

$user_permissions = $this->Session->read('user_permissions');
$account_id = $user_current_account['accounts']['account_id'];
$user_id = $user_current_account['User']['id'];
$video_data["huddle"] = $video_data["huddle"][0];

foreach ($video_data['huddle_users'] as $hudd_user) {
    if ($hudd_user['huddle_users']['role_id'] == 210) {
        $video_detail_coachee = $hudd_user['huddle_users']['user_id'];
    }
}
//print_r($video_data['huddle_type']);
//print_r($enable_matric);
?>
<div id="flashMessage2" class="message error" style="display:none;"></div>
<input id="tab_hidden" type="hidden" value="<?php echo $tab; ?>">
<input id="latest_video_created_date" type="hidden" value="<?php echo $latest_video_created_date; ?>">
<input id="video_envo" type="hidden" value="1">

<style>
    .action-buttons a{
        position:relative;
    }
    .share_div{
        position: absolute;
        z-index: 20;
        background: #fff;
        padding: 10px;
        width: 308px;
        left: -255px;
        top: 40px;
        border-radius:5px;
        box-shadow: 0px 0px 12px rgba(0,0,0,0.1);
    }
    .share_div:after {
        bottom: 100%;
        left: 88%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
        border-color: rgba(255, 255, 255, 0);
        border-bottom-color: #fff;
        border-width: 10px;
        margin-left: -10px;
    }
    .share_border{
        border-radius:5px;
        border:dashed 1px #bfbfbf;
    }
    .share_border input[type="text"]{
        background: transparent;
        border:0px;
        box-shadow:none;
        border-radius:0px;
        font-weight:normal;
        width: 239px;
        border-radius:5px;
    }
    .share_copy_btn{
        background: url(/app/img/tab-bg.gif) repeat-x left top;
        background-size: contain;
        border:0px;
        padding: 8px 8px;
        color:#333;
        font-family: "Segoe UI",Frutiger,"Frutiger Linotype";
        font-size: 13px;
        font-weight: 600;
        outline:none;
    }
    /*    .header.header-huddle .action-buttons.btn-group a {
            padding: 8px 15px;
            line-height: 1.3;
        }*/

    .observation_mainContainer  .tab-content{
        margin-top:0px!important;
    }
</style>
<div class="tab-box" style="overflow: visible;">
    <div id="tab-area">
        <?php
        $isCreator = $this->Custom->is_creator($user_current_account['User']['id'], $video_data["huddle"]['AccountFolder']['created_by']) &&
                $user_permissions['UserAccount']['permission_maintain_folders'] == '1';
        $canUploadDocument = in_array($user_huddle_level_permissions, array(200, 210)) || $isCreator;
        $canViewDiscussion = in_array($user_huddle_level_permissions, array(200, 210)) || $isCreator;
        $canViewPeople = in_array($user_huddle_level_permissions, array(200, 210, 220)) || $isCreator;
        $canAddObservation = $user_current_account['roles']['role_id'] != '120' || $isCreator || @$user_permissions['UserAccount']['permission_administrator_observation_new_role'];
        ?>


        <div class="table-container">
            <?php echo $videosTab; ?>

        </div>
        <input id="txtUploadedFilePath" type="hidden" value="" />
        <input id="txtUploadedFileName" type="hidden" value="" />
        <input id="txtUploadedFileMimeType" type="hidden" value="" />
        <input id="txtUploadedUrl" type="hidden" value="" />
        <input id="txtUploadedFileSize" type="hidden" value="" />
        <input id="txtUploadedDocType" type="hidden" value="" />
        <input id="uploadPath" type="hidden" value="<?php echo "/tempupload/$account_id/" . date('Y/m/d/'); ?>" />
        <input id="tabIndex" type="hidden" value="<?php echo $tab; ?>" />
        <input id="account_id" type="hidden" value="<?php echo $account_id; ?>" />
        <input id="user_id" type="hidden" value="<?php echo $user_id; ?>" />
    </div>
</div>
<script>
    $("#assessment_modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });</script>
<script>
    $("#coach_modal").on("show.bs.modal", function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    $(document).on("click", ".remove_tracker_button", function () {
        var r = confirm('Are you sure want to Delete this video from tracker?');
        if (r === true)
        {
            $('#remove_tracking_form').submit();
        }
    });</script>
<script type="text/javascript">
    $('#publish-feeback').click(function (e) {
        e.preventDefault();
        $video_id = $('#video_id').val();
        $.ajax({
            url: '<?php echo $this->base . '/huddles/feedback_publish/' . $huddle_id ?>/' + $video_id,
            type: 'POST',
            dataType: 'json',
            success: function (response) {
                if (response.status == true) {
                    $('#publish-feeback').css('display', 'none');

<?php if (IS_QA): ?>
                        var metadata = {
                            comment_published: 'Comments Published',
                            user_role: '<?php echo $user_role; ?>'

                        };
                        Intercom('trackEvent', 'comments-published', metadata);
<?php endif; ?>
                    alert(response.message);
                } else {
                    alert(response.message);
                }
            }
        });
    });

<?php if ($video_data['huddle_type'] == 3) { ?>
        $('#gVideoUpload').click(function (e) {
            e.preventDefault();
            $.ajax({
                url: '<?php echo $this->base . '/Huddles/check_videos_count' ?>',
                type: 'POST',
                dataType: 'json',
                data: {
                    huddle_id: '<?php echo $huddle_id ?>',
                    user_id: '<?php echo $user_id; ?>'
                },
                success: function (response) {
                    if (response.can_upload == 'yes') {
                        $.ajax({
                            url: '<?php echo $this->base; ?>/Huddles/check_storage_full',
                            type: 'POST',
                            success: function (response) {
                                if (response != '') {
                                    $('#flashMessage2').css('display', 'block');
                                    $('#flashMessage2').html(response);

                                } else {
                                    OpenFilePicker('video');
                                }
                            }
                        });
                    } else {
                        alert(response.message);
                    }
                }
            });
        });
<?php } else { ?>
        $('#gVideoUpload').click(function (e) {
            $.ajax({
                url: '<?php echo $this->base; ?>/Huddles/check_storage_full',
                type: 'POST',
                success: function (response) {
                    if (response != '') {
                        $('#flashMessage2').css('display', 'block');
                        $('#flashMessage2').html(response);

                    } else {
                        OpenFilePicker('video');
                    }
                }
            });
        });
<?php } ?>


    $('#gDocumentUpload').click(function (e) {
        $.ajax({
            url: '<?php echo $this->base; ?>/Huddles/check_storage_full',
            type: 'POST',
            success: function (response) {
                if (response != '') {
                    $('#flashMessage2').css('display', 'block');
                    $('#flashMessage2').html(response);

                } else {
                    OpenFilePicker('doc');
                }
            }
        });
    });

    $('#flashMessage2').click(function (e) {

        $('#flashMessage2').fadeOut();
    });

</script>

<style type="text/css">
    .analytics_new{    background-color: #FFF; border: 1px solid #DAD6CE;border-radius: 3px 3px 3px 3px; box-shadow: 0 0 5px rgba(0, 0, 0, 0.1); padding-bottom:25px; font-family: "Segoe UI",Helvetica,Arial,sans-serif;}
    .analytics_new img{ max-width:100%;}
    .analytics_top{ background:#fff; border-bottom:2px solid #e8e8e8; padding:8px 10px 15px 8px;}
    .analytic_img_cls { float:left;    width: 98px;}
    .analytic_detail_cls{ float:left;padding: 7px 17px 9px 16px;    width: 532px;}
    .analytic_detail_cls h3{    font-family: "Segoe UI",Helvetica,Arial,sans-serif;font-weight: 400;font-size: 23px;}
    .analytic_detail_cls h4{    font-family: "Segoe UI",Helvetica,Arial,sans-serif;font-weight: 400;font-size:15px;}
    .analytic_detail_cls label{ color:#668dab;}
    .analytic_detail_cls span{ display:inline-block; margin-right:7px;}
    .analytic_topnav { float:right;    font-family: "Segoe UI",Helvetica,Arial,sans-serif;    font-weight: 600;     width:365px;    text-align: right;}
    .analytic_topnav a{ color:#668dab;    font-family: "Segoe UI",Helvetica,Arial,sans-serif;    font-weight: 600;}
    .analytic_topnav  img{ margin:0 4px;}
    .analytic_overview { background:#fcfcfc; padding:12px;     border-bottom: 2px solid #e8e8e8;}
    .analytic_overview  h3{    font-family: "Segoe UI",Helvetica,Arial,sans-serif;font-weight: 400;font-size: 23px;}
    .over_datecls {color:#668dab;    font-size: 14px;padding: 4px 0 5px 0;}
    .over_datecls img{ margin-right:5px; position:relative; top:-1px;}
    .analytic_countsbox {    border: 1px solid #DAD6CE; border-radius: 2px;box-shadow: 0 0 5px rgba(0, 0, 0, 0.1); float:left; width:23%; background:#fff;     margin: 12px 1%;}
    .analytic_countsbox .anbox1{ background:#fcfcfc; text-align:center;    padding: 21px 14px;width:25%;    float: left;}
    .analytic_countsbox .anbox2{    padding: 10px 8px; width:75%; float:right;}
    .analytic_countsbox .anbox2 h4{  font-family: "Segoe UI",Helvetica,Arial,sans-serif;    font-weight: 600; font-size:18px; color:#000000;}
    .analytic_countsbox .anbox2 h5{  font-family: "Segoe UI",Helvetica,Arial,sans-serif;    font-weight: 400; font-size:13px; color:#878787;}
    .analytic_overview2 { background:#fff; padding:12px;     border-bottom: 2px solid #e8e8e8; position:relative;}
    .over_select { position:absolute; top:12px; right:15px;}
    .over_select select{ background:#668dab; width:170px; height: 38px;border-radius: 36px; color: #fff;padding: 6px 6px 6px 10px;}
    .am_chart1 { border:2px solid #e8e8e8; padding:10px; float:left; width:47%; margin:20px 1%;}
    .am_chart2 { padding:20px 0;}
    .analytic_filter { float:left; width:100%;     padding: 0 15px 15px 20px;}
    .analytic_filter h3{font-family: "Segoe UI",Helvetica,Arial,sans-serif;font-weight: 400;font-size: 23px; margin:-6px 0 5px 0;}
    .analytic_filter select { width:100px; border:0; border-bottom:2px solid #ddd; margin:0 6px 0 4px;}
    .analytic_filter input[type=text] { width:70px; border:1px solid #ddd; margin:0 6px 0 4px;}
    .analytic_filter .btn{ margin-left:10px;}
    .analytic_custombtn { float: right;color:#668dab; cursor:pointer;}
    .analytic_custombtn a{color:#668dab; text-decoration:none;}
    .analytic_custom2 { display:none;}
    .framework_filter{background: #fcfcfc; padding: 12px; border-bottom: 2px solid #e8e8e8;}
    .flter_leftcls { float:left; width:45%;}
    .flter_leftcls select { width:150px; border:0; border-bottom:2px solid #ddd; margin:0 6px 0 4px;background: #fcfcfc;;}
    .flter_rightcls { float:left; width:55%;}
    .flter_rightcls input[type=text]{ border:0; outline:0 !important;     box-shadow: 0 0px 0px rgba(0,0,0,0.09) inset;     background: #fcfcfc; color:#668dab;     width: 119px;}

    .flter_rightcls ::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #668dab;
    }
    .flter_rightcls ::-moz-placeholder { /* Firefox 19+ */
        color: #668dab;
    }
    .flter_rightcls :-ms-input-placeholder { /* IE 10+ */
        color: #668dab;
    }
    .flter_rightcls :-moz-placeholder { /* Firefox 18- */
        color: #668dab;
    }

    .flter_rightcls .token_filter{    -webkit-box-sizing: border-box;
                                      -moz-box-sizing: border-box;
                                      box-sizing: border-box;
                                      -webkit-border-radius: 3px;
                                      -moz-border-radius: 3px;
                                      border-radius: 3px;
                                      display: inline-block;
                                      border: 1px solid #b9b9b9;
                                      background-color: #fff;
                                      white-space: nowrap;

                                      vertical-align: top;
                                      cursor: default; border-radius:5px;    }
    .flter_rightcls .token-label{display: inline-block;
                                 overflow: hidden;
                                 text-overflow: ellipsis;
                                 padding-left: 4px;
                                 vertical-align: top; max-width:100px;     font-size: 12px;}
    .flter_rightcls .close {
        font-family: Arial;
        display: inline-block;
        line-height: 100%;
        font-size: 1.1em;
        line-height: 1.49em;
        margin-left: 5px;
        float: none;
        height: 100%;
        vertical-align: top;
        padding-right: 4px;
        position: absolute;
        right: 15px;
        top: 6px;
        font-weight: 400;
        color: #998c8c;
    }

    .flter_rightcls .token {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        display: inline-block;
        border: 1px solid #d9d9d9;
        background-color: #fff;
        white-space: nowrap;
        margin: 4px 5px 5px 0;

        vertical-align: top;
        cursor: default; position: relative;
        border-radius: 34px;
        padding: 3px 20px 0px 4px;
    }

    .performance_left { float:left; width:49%; border-right:2px solid #ddd; padding:2%;    max-height: 350px;
                        overflow: auto;}
    .performance_left h3{    font-family: "Segoe UI",Helvetica,Arial,sans-serif; font-size:17px;}
    .performance_left h5{    font-family: "Segoe UI",Helvetica,Arial,sans-serif;font-size:13px;     margin: 16px 0 13px 0;
    }
    .performance_right { float:left; width:49%;  padding:2%;     max-height: 350px;
                         overflow: auto;}
    .performance_outer { padding:20px;}
    .per_r1 { width:30%; float:left;}
    .per_r2 { width:11%; float:left;     text-align: center;}
    .per_r3 { width:59%; float:left;}
    .per_row { margin:5px 0;}

    .per_r1 select{ width:100%; border-radius:4px; border:1px solid #ddd;    height: 28px;}
    .per_r2 span{ width:28px; height:28px; border-radius:50%;  display:inline-block;     line-height: 26px; text-align:center; color:#fff; font-size:12px;}
    .per_green{background:#2cb742;}
    .per_blu{ background:#3498db;}
    .per_r3 {}



    .performance_before_select h3{    font-family: "Segoe UI",Helvetica,Arial,sans-serif; margin:15px 0; font-size:15px; color:#4f4f4f;}
    .performance_before_select { text-align:center; padding-top: 73px;}
    .performance_right h4{ font-family: "Segoe UI",Helvetica,Arial,sans-serif; margin:10px 0; font-size:14px; color:#4f4f4f;}
    #performance_poup .modal-content form{padding:0;}


</style>
<script>

    $("#performance_btn").click(function () {

        $.ajax({
            type: 'POST',
            data: {
                huddle_id: <?php echo $huddle_id; ?>,
                account_id: <?php echo $account_id; ?>,
                video_id: '<?php echo $video_id; ?>'
            },
            url: home_url + '/huddles/performace_level_update',
            success: function (response) {

                $('.performance_outer').html(response);

            },
            errors: function (response) {

            }

        });

    });
<?php if ($is_video_page): ?>

        $(document).ready(function (e) {
    <?php if (isset($parent_folder_name)): ?>
                var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><a href="/huddles"><?php echo $breadcrumb_language_based_content['huddles_breadcrumb']; ?></a> <?php echo $breadcrumb; ?><a href="/Folder/<?php echo addslashes($parent_folder_name['AccountFolder']['account_folder_id']); ?>"><?php echo addslashes($parent_folder_name['AccountFolder']['name']); ?> </a><a href="/huddles/view/<?php echo $current_huddle_info['AccountFolder']['account_folder_id']; ?>"><?php echo addslashes($current_huddle_info['AccountFolder']['name']); ?></a> <span><?php echo addslashes($video_total_info['afd']['title']); ?></span></div>';

                $('.breadCrum').html(bread_crumb_data);

    <?php else : ?>
                var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><a href="/huddles"><?php echo $breadcrumb_language_based_content['huddles_breadcrumb']; ?></a> <a href="/huddles/view/<?php echo $current_huddle_info['AccountFolder']['account_folder_id']; ?>"><?php echo addslashes($current_huddle_info['AccountFolder']['name']); ?></a> <span><?php echo addslashes($video_total_info['afd']['title']); ?></span></div>';

                $('.breadCrum').html(bread_crumb_data);

    <?php endif; ?>

        });



<?php else: ?>
        $(document).ready(function (e) {
    <?php if (isset($parent_folder_name)): ?>
                var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><a href="/huddles"><?php echo $breadcrumb_language_based_content['huddles_breadcrumb']; ?></a><?php echo $breadcrumb; ?> <a href="/Folder/<?php echo addslashes($parent_folder_name['AccountFolder']['account_folder_id']); ?>"><?php echo addslashes($parent_folder_name['AccountFolder']['name']); ?> </a><span><?php echo addslashes($current_huddle_info['AccountFolder']['name']); ?></span></div>';

                $('.breadCrum').html(bread_crumb_data);

    <?php else : ?>
                var bread_crumb_data = '<div class="container"><a href = "/dashboard/home"><?php echo $breadcrumb_language_based_content['home_breadcrumb']; ?></a><a href="/huddles"><?php echo $breadcrumb_language_based_content['huddles_breadcrumb']; ?></a> <span><?php echo addslashes($current_huddle_info['AccountFolder']['name']); ?></span></div>';

                $('.breadCrum').html(bread_crumb_data);

    <?php endif; ?>

        });

<?php endif; ?>




</script>

