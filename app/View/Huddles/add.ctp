<?php
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];
?>
<div class="container box newCoachingHuddle">
    <h1>New Huddle</h1>
    <script type="text/javascript">

        (function ($) {
            jQuery.expr[':'].Contains = function (a, i, m) {
                return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
            };

            function listFilter(header, list) {
                var form = $("<div>").attr({"class": "filterform"}),
                        input = $("<input id='input-filter' placeholder='Search people or group'><div  id='cancel-btn' type='text' style=' width: 10px;  position: absolute; right: 93px; margin-top: 7px; display:none;cursor: pointer; '>X</div>").attr({"class": "filterinput", "type": "text"});
                $(form).append(input).appendTo(header);
                $('.filterform input').css({'width': '233px', 'float': 'right'});
                $(input).change(function () {
                    var filter = $(this).val();
                    if (filter) {
                        $search_count = $(list).find("a:Contains(" + filter + ")").length;
                        if ($search_count > 4) {
                            $('div.thumb').css('top', '0px');
                            $('div.overview').css('top', '0px');
                            $('.scrollbar').css('display', 'block');
                        } else {
                            $('div.thumb').css('top', '0px');
                            $('div.overview').css('top', '0px');
                            $('.scrollbar').css('display', 'none');
                        }
                        $(list).find("a:not(:Contains(" + filter + "))").parent().parent().slideUp(400, function () {
                            $('.widget-scrollable').tinyscrollbar_update();
                        });
                        $(list).find("a:Contains(" + filter + ")").parent().parent().slideDown(400, function () {
                            $('.widget-scrollable').tinyscrollbar_update();
                        });
                        jQuery('#cancel-btn').css('display', 'block');
                        if ($(list).find("a:Contains(" + filter + ")").length == 0) {
                            $('#liNodataFound').remove();
                            $("#list-containers").append('<li id="liNodataFound">No people or groups match this search. Please try again.</li>');
                        } else {
                            $('#liNodataFound').remove();
                        }

                    } else {
                        jQuery('#cancel-btn').css('display', 'none');
                        $('.scrollbar').css('display', 'block');
                        $(list).find("li").slideDown(400, function () {
                            $('.widget-scrollable').tinyscrollbar_update();
                        });

                    }
                    return false;
                }).keyup(function () {
                    $(this).change();
                });

                $('#cancel-btn').click(function (e) {
                    jQuery('#cancel-btn').css('display', 'none');
                    $('div.thumb').css('top', '0px');
                    $('div.overview').css('top', '0px');
                    $('.scrollbar').css('display', 'block');
                    jQuery('#input-filter').val('');
                    $(list).find("li").slideDown(400, function () {
                        $('.widget-scrollable').tinyscrollbar_update();
                    });
                })
            }

            $(function () {
                listFilter($("#header-container"), $("#list-containers"));
            });
        }(jQuery));
        $(document).ready(function (e) {
            $('#caoch-checkbox').click(function (e) {
                if ($(this).is(':checked') == true) {
                    $(".caoch-checkbox input[type='checkbox']").prop('checked', true);
                } else {
                    $(".caoch-checkbox input[type='checkbox']").prop('checked', false);
                }
            })
            $('#mentee-checkbox').click(function (e) {
                if ($(this).is(':checked') == true) {
                    $(".mentee-checkbox input[type='checkbox']").prop('checked', true);
                } else {
                    $(".mentee-checkbox input[type='checkbox']").prop('checked', false);
                }
            })
            $('#select-all-none').click(function (e) {
                if ($('#select-all').is(':checked')) {
                    $('#select-all-label').html('Select None');
                    $('#select-all').prop('checked', true);
                    $(".member-user").prop('checked', true);
                    $(".super-user").prop('checked', true);
                    $(".viewer-user").prop('checked', true);

                } else {
                    $('#select-all-label').html('Select All');
                    $('#select-all').prop('checked', false);
                    $(".member-user").prop('checked', false);
                    $(".super-user").prop('checked', false);
                    $(".viewer-user").prop('checked', false);
                }

            });
        })

    </script>
    <form method="post" id="new_video_huddle" enctype="multipart/form-data" class="new_video_huddle"
          action="<?php echo $this->base . '/Huddles/add'; ?>" accept-charset="UTF-8">
        <div style="margin:0;padding:0;display:inline"><input type="hidden" value="✓" name="utf8"></div>

        <hr class="full">
        <a class="close-reveal-modal"></a>
        <div class="span5">
            <div class="row" style="margin-bottom: 20px;">
                <input name="data[name]" id="video_huddle_name" class="size-big huddle-name" placeholder="Huddle Name" size="30" type="text" required/>
                <label for="video_huddle_name" class="error" style="display:none;background-color: none !important;">This field is required.</label>
            </div>
            <div style="clear: both;"></div>
            <div class="row">
                <?php echo $this->Form->textarea('description', array('rows' => '4', 'placeholder' => 'Huddle Description (Optional)', 'id' => 'video_huddle_description', 'cols' => '40', 'class' => 'size-big')); ?>
            </div>
            <?php echo $this->Form->input('created_by', array('value' => $user_id, 'type' => 'hidden', 'id' => 'video_huddle_created_by')); ?>
        </div>
        <div class="examples">
            <h4>Example Huddle names and descriptions</h4>

            <p>Peter Rosenquist | English</p>
            <span>Peter's classroom for 2015-16</span>

            <p>Amelia Earhart 3rd Grade PLC</p>
            <span>Professional Learning Community for 2015-16</span>

            <p>Beth Johnson and Sarah Campbell</p>
            <span>Mentoring Huddle for 2015-16</span>
        </div>
        <hr class="full dashed">
        <h3 style="float: left;margin-top: 5px;margin-bottom: 10px;">Invite other people in your account to join you in the Huddle - you can always do this later.</h3>

        <div class="row-fluid">
            <div class="groups-table span12">

                <div class="span4 huddle-span4" style="margin-left:0px;">
                    <div class="groups-table-header">
                        <div style=" width: 200px; float: left; ">
                            <?php if ($user_permissions['UserAccount']['permission_maintain_folders'] == 1 && $user_permissions['UserAccount']['permission_administrator_user_new_role'] == 1): ?>
                                <a style="margin-left: -8px;" id="pop-up-btn" data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="btn btn-small btn-green" href="#"><span class="plus">+</span>
                                </a>
                            <?php else: ?>
                                <a style="margin-left: -8px;"><span class="">&nbsp;&nbsp;</span>
                                </a>
                            <?php endif; ?>
                        </div>
                        <div style="width: 302px; padding: 0px;float: left;margin-left: 394px;" class="search-box">
                            <div  id="header-container" class="filterform">
                            </div>
                        </div>
                        <a class="appendix right" href="#">?</a>
                        <div class="appendix-content appendix-narrow down" style="font-weight: normal; display: none;">
                            <h3>Info</h3>
                            <p>Admin: Can add/remove users; upload/download/copy/delete all videos and documents; clip all videos; add/edit/delete all video annotations and comments; create/participate/edit all Huddle discussions; delete Huddle.</p>
                            <p>Members: Can upload/download/copy/delete videos and documents they add to the Huddle; clip their videos; add/edit/delete their video annotations and comments; create/participate in Huddle discussions.</p>
                            <p>Viewers: View videos and documents only.</p>
                        </div>
                        <div style="clear:both"></div>
                        <?php if (count($super_users) > 1 || (isset($huddleUsers) && count($huddleUsers) > 1) || (isset($users_groups) && count($users_groups) > 0)): ?>
                            <div class="select-all-none"  style="float: left; margin-left: -8px; margin-top: 5px;min-width: 200px;max-width: 200px;">
                                <label id="select-all-none" for="select-all"><input type="checkbox" name="select-all" id="select-all"/> <span id="select-all-label">Select All</span></label>
                            </div>
                        <?php endif; ?>
                        <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                            <div class="huddles-select-all-block coach_mentee">
                                <span class="caoch-checkbox" style="margin-right: 5px;">
                                    <input type="checkbox" id="caoch-checkbox" name="is_coach"> Coach</span>
                                <span class="mentee-checkbox"><input type="checkbox" id="mentee-checkbox" name="is_mentor"> Coachee</span>
                            </div>
                        <?php endif; ?>

                        <div class="huddles-select-all-block">
                            <span class="admin-radio"><input type="radio" id="admin-radio" name="select-all"> Admin</span>
                            <span class="member-radio"><input type="radio" id="member-radio" name="select-all" style="margin-left: -8px;"> Member</span>
                            <span class="viewer-radio"><input type="radio" id="viewers-radio" name="select-all"> Viewer</span>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <div class="groups-table-content">
                        <div class="widget-scrollable">
                            <div class="scrollbar" style="height: 155px;">
                                <div class="track" style="height: 155px;">
                                    <div class="thumb" style="top: 0px; height: 90.3195px;">
                                        <div class="end"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="viewport short">
                                <div class="overview" style="top: 0px;">
                                    <div  id="people-lists">
                                        <ul id="list-containers">
                                            <?php
                                            if (count($users_record) > 0):
                                                ?>
                                                <?php if ($users_record): ?>
                                                    <?php foreach ($users_record as $row): ?>
                                                        <?php
                                                        if ($row['id'] == $user_id)
                                                            continue;
                                                        ?>
                                                        <?php if ($row['is_user'] == 'admin'): ?>
                                                            <li>
                                                                <label  class="huddle_permission_editor_row" for="super_admin_ids_<?php echo $row['id'] ?>"><input class="super-user" type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>"> <a style="color: #757575; font-weight: normal;"><?php echo $row['first_name'] . " " . $row['last_name'] ?> </a></label>
                                                                <input type="hidden" value="<?php echo $row['email'] ?>" name="super_admin_email_<?php echo $row['id'] ?>" id="super_admin_email_<?php echo $row['id'] ?>">
                                                                <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                                                                    <div class="permissions coach_mentee">
                                                                        <span class="caoch-checkbox" style="margin-right: 5px;">
                                                                            <input type="checkbox" value="1" id="caoch-checkbox" name="is_coach_<?php echo $row['id'] ?>"> Coach</span>
                                                                        <span class="mentee-checkbox"><input value="1" type="checkbox" id="mentee-checkbox" name="is_mentor_<?php echo $row['id'] ?>"> Coachee</span>
                                                                    </div>
                                                                <?php endif; ?>
                                                                <div class="permissions">
                                                                    <label for="user_role_<?php echo $row['id'] ?>_200"><input style="margin-left: -20px;" class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200" checked="checked"/>Admin
                                                                    </label>
                                                                    <label for="user_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn"  type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210"/>Member
                                                                    </label>
                                                                    <label for="user_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220"/>Viewer
                                                                    </label>
                                                                </div>
                                                            </li>

                                                        <?php elseif ($row['is_user'] == 'member'): ?>
                                                            <li>
                                                                <label  class="huddle_permission_editor_row" for="super_admin_ids_<?php echo $row['id'] ?>"><input class="member-user"  type="checkbox" value="<?php echo $row['id'] ?>" name="super_admin_ids[]" id="super_admin_ids_<?php echo $row['id'] ?>"> <a style="color: #757575; font-weight: normal;"> <?php echo $row['first_name'] . " " . $row['last_name'] ?></a></label>
                                                                <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                                                                    <div class="permissions coach_mentee">
                                                                        <span class="caoch-checkbox" style="margin-right: 5px;">
                                                                            <input type="checkbox" id="caoch-checkbox" value="1"  name="is_coach_<?php echo $row['id'] ?>"> Coach</span>
                                                                        <span class="mentee-checkbox"><input type="checkbox" value="1"   id="mentee-checkbox" name="is_mentor_<?php echo $row['id'] ?>"> Coachee</span>
                                                                    </div>
                                                                <?php endif; ?>
                                                                <div class="permissions">
                                                                    <label for="user_role_<?php echo $row['id'] ?>_200"><input style="margin-left: -20px;" class="admin-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_200" value="200"/>Admin
                                                                    </label>
                                                                    <label for="user_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_210" value="210" checked="checked"/>Member
                                                                    </label>
                                                                    <label for="user_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="user_role_<?php echo $row['id'] ?>" id="user_role_<?php echo $row['id'] ?>_220" value="220"/>Viewer
                                                                    </label>
                                                                </div>
                                                            </li>

                                                        <?php elseif ($row['is_user'] == 'group'): ?>
                                                            <li>
                                                                <label  class="huddle_permission_editor_row"><input class="viewer-user" type="checkbox" value="<?php echo $row['id']; ?>" name="group_ids[]" id="group_ids_<?php echo $row['id'] ?>"> <a style="color: #757575; font-weight: normal;"><?php echo $row['name']; ?></a></label>
                                                                <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                                                                    <div class="permissions coach_mentee">
                                                                        <span class="caoch-checkbox" style="margin-right: 5px;">
                                                                            <input type="checkbox" id="caoch-checkbox" value="1"  name="is_coach_<?php echo $row['id'] ?>"> Coach</span>
                                                                        <span class="mentee-checkbox"><input type="checkbox" value="1" id="mentee-checkbox" name="is_mentor_<?php echo $row['id'] ?>"> Coachee</span>
                                                                    </div>
                                                                <?php endif; ?>
                                                                <div class="permissions">
                                                                    <label for="group_role_<?php echo $row['id'] ?>_200"><input style="margin-left: -20px;" class="admin-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_200" value="200"/>Admin
                                                                    </label>
                                                                    <label for="group_role_<?php echo $row['id'] ?>_210"><input style="margin-right: 5px;" class="member-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_210" value="210"/>Member
                                                                    </label>
                                                                    <label for="group_role_<?php echo $row['id'] ?>_220"><input class="viewer-btn" type="radio" name="group_role_<?php echo $row['id'] ?>" id="group_role_<?php echo $row['id'] ?>_220" value="220" checked="checked"/>Viewer
                                                                    </label>
                                                                </div>
                                                            </li>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            <?php else: ?>
                                                <li>
                                                    To Invite users into the account <a data-original-title="Add new User" rel="tooltip" data-toggle="modal" data-target="#addSuperAdminModal" class="" href="#"><span class="">click here</span></a>
                                                </li>
                                            <?php endif; ?>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div><br/><br/>
        <div class="input-group mmargin-top">
            <div id="editor2-toolbar" class="editor-toolbar" style="display: none;">
                <a data-wysihtml5-command="bold">bold</a>
                <a data-wysihtml5-command="italic">italic</a>
                <a data-wysihtml5-command="insertOrderedList">ol</a>
                <a data-wysihtml5-command="insertUnorderedList">ul</a>
                <a data-wysihtml5-command="formatBlock" data-wysihtml5-command-value="blockquote">quote</a>
            </div>
            <?php
            echo $this->Form->textarea('message', array('id' => 'editor-2', 'class' => 'editor-textarea',
                'style' => 'width:100%;text-indent:5px;padding-right:0px;padding-left:0px;',
                'placeholder' => 'Message to User(s) participating in Huddle...(Optional)'));
            ?>
            <?php echo $this->Form->error('videoHuddle.message'); ?>
        </div>
        <hr class="full">
        <div class="form-actions">
            <input type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" id="btnSaveHuddle" value="Create Huddle" name="commit" onclick="this.disabled = 'disabled';
                            beforeHuddleAdd();" class="btn btn-green"  >
            <a class="btn btn-transparent" href="<?php echo $this->base . '/Huddles' ?>">Cancel</a>
        </div>
    </form>
</div>
<div id="addSuperAdminModal" class="modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="header">
                <h4 class="header-title nomargin-vertical smargin-bottom"><img alt="Admin" src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->base . '/img/add-user.png'); ?>" /> New User</h4>
                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
            </div>
            <form accept-charset="UTF-8" action="<?php echo $this->base . '/Huddles/addUsers' ?>" enctype="multipart/form-data" method="post" name="admin_form" onsubmit="return false;" style="padding-top:0px;">
                <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
                <div id="flashMessage2" class="message error" style="display:none;"></div>
                <div class="way-form">
                    <h3>Add name and email</h3>
                    <ol class="autoadd autoadd-sfont fmargin-list-left">
                        <li>
                            <label class="icon3-user"><input class="input-dashed" id="users__name" name="users[][name]" required placeholder="Full Name" type="text" value="" /></label>
                            <label class="icon3-email"><input class="input-dashed" id="users__email" name="users[][email]" required placeholder="Email Address" type="email" value="" /></label>
                            <a href="#" class="close">x</a>
                        </li>
                        <li>
                            <label class="icon3-user"><input class="input-dashed" id="users__name" name="users[][name]" placeholder="Full Name" type="text" value="" /></label>
                            <label class="icon3-email"><input class="input-dashed" id="users__email" name="users[][email]" placeholder="Email Address" type="email" value="" /></label>
                            <a href="#" class="close">x</a>
                        </li>
                    </ol>
                    <input id="controller_source" name="controller_source" type="hidden" value="video_huddles" />
                    <input id="action_source" name="action" type="hidden" value="add" />
                    <input id="action_source" name="user_type" type="hidden" value="110" />
                    <button id="btnAddToAccount_addHuddle" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" class="btn btn-green fmargin-left" type="button">Add to Account</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function (e) {

        $("#admin-radio").click(function () {
            if ($(this).is(':checked')) {
                $(".member-btn").removeAttr("checked");
                $(".viewer-btn").removeAttr("checked");
                $(".admin-btn").prop('checked', true);
            }

        });
        $("#member-radio").click(function () {
            if ($(this).is(':checked')) {
                $(".admin-btn").removeAttr("checked");
                $(".viewer-btn").removeAttr("checked");
                $(".member-btn").prop('checked', true);

            }
        });
        $("#viewers-radio").click(function () {
            if ($(this).is(':checked')) {
                $(".admin-btn").removeAttr("checked");
                $(".member-btn").removeAttr("checked");
                $(".viewer-btn").prop('checked', true);
            }
        });
    });

    var posted_data = [];
    var new_ids = 0;

    $(document).ready(function () {
        $('#btnAddToAccount_addHuddle').click(function () {
            addToInviteList();
        });

        $('#pop-up-btn').click(function () {
            $('.fmargin-left').attr('disabled', false);
        });

        $('#video_huddle_name').keypress(function (e) {
            if (e.keyCode == 13) {
                $(this).closest('form').find('textarea').eq(0).focus();
                return false;
            }
            $(this).removeClass('error').next().hide();
        });

    });

    function beforeHuddleAdd() {
//        console.log('before huddle add ' + (new Date()).getTime());
        var video_huddle_name = $("#video_huddle_name");
        if (video_huddle_name.val().trim().length > 0) {
            $('#btnSaveHuddle').attr('disabled', true);
            $('#new_video_huddle').submit();
        } else {
            video_huddle_name.addClass('error');
            $('#btnSaveHuddle').attr('disabled', false);
//            video_huddle_name.next().css('display', 'block');
        }
//        console.log('before huddle add ' + (new Date()).getTime());
        return false;
    }

    function is_email_exists(email) {
        for (var i = 0; i < posted_data.length; i++) {
            var posted_user = posted_data[i];
            if (posted_user[1] == email)
                return true;
        }
        return false;
    }

    function addToInviteList() {
        var lposted_data = [];
        var userNames = document.getElementsByName('users[][name]');
        var userEmails = document.getElementsByName('users[][email]');
        var i = 0;
        for (i = 0; i < userNames.length; i++) {
            var userEmail = $(userEmails[i]);
            if (is_email_exists(userEmail.val())) {
                alert('A new user with this email is already added.');
                return false;
            }
        }

        for (i = 0; i < userNames.length; i++) {
            var userName = $(userNames[i]);
            var userEmail = $(userEmails[i]);
            if (userName.val() != '' || userEmail.val() != '') {
                if (userName.val() == '') {
                    alert('Please enter a valid Full Name.');
                    userName.focus();
                    return false;
                }
                if (userEmail.val() == '') {
                    alert('Please enter a valid Email.');
                    userEmail.focus();
                    return false;
                }
                if (is_email_exists(userEmail.val())) {
                    alert('A new user with this email is already added.');
                    return false;
                }
                var newUser = [];
                newUser[newUser.length] = '';
                newUser[newUser.length] = userName.val();
                newUser[newUser.length] = userEmail.val();
                lposted_data[lposted_data.length] = newUser;
            }
        }

        if (lposted_data.length == 0) {
            alert('Please enter atleast one Full name or email.');
            return;
        }
        $.ajax({
            type: 'POST',
            data: {
                user_data: lposted_data,
            },
            url: '<?php echo $this->base; ?>/Huddles/verifyNewUsers',
            success: function (response) {
                if (response != '') {
                    $('#flashMessage2').css('display', 'block');
                    $('#flashMessage2').html(response);
                } else {

                    $('#flashMessage2').css('display', 'none');

                    new_ids -= 1;

                    for (var i = 0; i < lposted_data.length; i++) {

                        var data = lposted_data[i];
                        var html = '';

                        html += '<li>';
                        html += '<label class="huddle_permission_editor_row" for="super_admin_ids_' + new_ids + '"><input type="checkbox" value="' + new_ids + '" name="super_admin_ids[]" id="super_admin_ids_' + new_ids + '"> ' + data[1] + ' </label>';
                        html += '<input type="hidden" value="' + data[1] + '" name="super_admin_fullname_' + new_ids + '" id="super_admin_fullname_' + new_ids + '">';
                        html += '<input type="hidden" value="' + data[2] + '" name="super_admin_email_' + new_ids + '" id="super_admin_email_' + new_ids + '">';

                        html += '<div class="permissions coach_mentee">';
                        html += '<span class="caoch-checkbox" style="margin-right: 5px;">';
                        html += '<input type="checkbox" id="caoch-checkbox" value="1" name="is_coach_' + new_ids + '"> Coach</span>';
                        html += '<span class="mentee-checkbox"><input type="checkbox" value="1" id="mentee-checkbox" name="is_mentor_' + new_ids + '"> Coachee</span>';
                        html += '</div>';

                        html += '<div class="permissions">';
                        html += '<label for="user_role_' + new_ids + '_200"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_200" value="200">Admin</label>';
                        html += '<label for="user_role_' + new_ids + '_210"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_210" value="210" checked="checked">Member</label>';
                        html += '<label for="user_role_' + new_ids + '_220"><input type="radio" name="user_role_' + new_ids + '" id="user_role_' + new_ids + '_220" value="220">Viewer</label>';
                        html += '</div>';
                        html += '</li>';
                        $('.groups-table .huddle-span4 .groups-table-content ul').append(html);
                        new_ids -= 1;
                        var newUser = [];
                        newUser[newUser.length] = data[0];
                        newUser[newUser.length] = data[1];
                        posted_data[posted_data.length] = newUser;
                    }

                    var $overview = $('.widget-scrollable.horizontal .overview');
                    $.each($overview, function () {

                        var width = $.map($(this).children(), function (child) {
                            return $(child).outerWidth() +
                                    $(child).pixels('margin-left') + $(child).pixels('margin-right');
                        });
                        $(this).width($.sum(width));

                    });

                    $('.widget-scrollable').tinyscrollbar();
                    $('#addSuperAdminModal').modal('hide');


                }
            },
            errors: function (response) {
                alert(response.contents);
            }

        });

    }

</script>