<?php
    $timeS = array();
    $videoID = $videoDetail['Document']['id'];
    $videoFilePath = pathinfo($videoDetail['Document']['url']);
    $videoFileName = $videoFilePath['filename'];

    if ($videoComments) {
        foreach ($videoComments as $cmt) {
            if (!empty($cmt['Comment']['time']))
                $timeS[] = $cmt['Comment']['time'];
        }
    }
?>

<?php 
    
    $document_files_array = $this->Custom->get_document_url($videoDetail['Document']);
    if (empty($document_files_array['url'])) {
        $videoDetail['Document']['published'] =0;
        $document_files_array['url'] = $videoDetail['Document']['original_file_name'];
        $videoDetail['Document']['encoder_status'] = $document_files_array['encoder_status'];
    } else {
        $videoDetail['Document']['encoder_status'] = $document_files_array['encoder_status'];
    }

    if (isset($videoDetail['Document']['published']) && $videoDetail['Document']['published'] == 1): ?>

    <?php
        
        $video_url = $document_files_array['url'];
        $thumbnail_image_path = $document_files_array['thumbnail'];

    ?>
<div id="crop-panel" class="video-editor video-editor--loading js-video-editor" style="padding:20px;">
    <video id="video-hidden" class="video-editor__video video-js vjs-default-skin" controls preload="metadata" width="500" height="321"
           poster="<?php echo $thumbnail_image_path; ?>" data-markers="[]">
        <source src="<?php echo $video_url; ?>" type='video/mp4'>
    </video>
    <input type="hidden" id="video_hidden_id" value="<?php echo $videoDetail['Document']['id'] ?>"/>
    <input type="hidden" id="huddle_hidden_id" value="<?php echo $huddle_id ?>"/>
    <div class="video-editor__trimmer" style="position:relative;">
        <span class="video-editor__trimmer-time video-editor__trimmer-time--start-user" style="float: none;text-align: left;position: absolute;top: -24px;left: 0px;"></span>
        <span class="video-editor__trimmer-time video-editor__trimmer-time--end-user" style="float: none;text-align: left;position: absolute;top: -24px;left: 96%;"></span>
        <div class="video-editor__trimmer-slider"></div>
        <span class="video-editor__trimmer-time video-editor__trimmer-time--start"></span>
        <span class="video-editor__trimmer-time video-editor__trimmer-time--end"></span>
    </div>
    <footer class="video-editor__footer">
        <input class="ve-btn js-close-video" type="button" value="Cancel">
        <input class="ve-btn js-done-video" type="button" value="Trim">
        <input class="js-start-time" type="hidden" name="start-time">
        <input class="js-end-time" type="hidden" name="start-time">
        <button class="ve-btn js-preview-video" type="button">Preview Video</button>
    </footer>
</div>
<script type="text/javascript">

    $(document).ready(function() {

        $('.js-close-video').click(function(){
            parent.$.fancybox.close();
        });

        var VE = new VideoEditor('.js-video-editor');

        setInterval(function(){
            parent.$('#example_video_<?php echo $videoDetail['Document']['id'] ?>').find('video')[0].pause();
        }, 1000);

    });

</script>
<?php endif; ?>
