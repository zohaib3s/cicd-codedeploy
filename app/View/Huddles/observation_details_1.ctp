<style type="text/css">
    .observation_left{
        float:left;
        width:50%;
        padding:10px;
        box-sizing:border-box;
        border-right:solid 1px #dadada;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .observation_left p{
        margin:0px;
        font-size:13px;
    }
    .observation_left h2{
        text-align:center;
        font-size:20px;
        font-weight:normal;
        margin: 12px 0px;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .observation_left h3{
        color:#707070;
        font-size:14px;
        text-align:center;
        font-weight:normal;
        margin:0px;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .observation_right{
        float:right;
        width:50%;
        padding:10px;
        box-sizing:border-box;
        margin-top: -20px;
    }
    .observation_btn_box{
        text-align:center;
        margin:15px 0px;
    }
    .observation_btn{
        width:235px;
        background:url(/img/observation_bg.png) repeat-x;
        font-weight:bold;
        font-size:18px;
        color:#fff;
        text-shadow:1px 1px 0px #205f15;
        border:solid 2px #2a8f17;
        border-radius:5px;
        outline:none;
        text-align:center;
        padding:15px 0px;
        cursor:pointer;
    }
    .timer_box{
        padding:0px 0px;
        text-align:center;
        color:#5daf46;
        font-weight:300 !important;
        font-size:4em ;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
    }
    .timer_box span{
        font-weight:300 !important;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
    }
    .stop_watch_sec{
        color:#707070;
        font-size:20px;
        font-weight:400;
    }
    .start_btn_box{
        text-align:center;
    }
    .start_time_btn{
        width:235px;
        background:url(/img/observation_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:0px 1px 0px #8e631f;
        border:solid 1px #2a8f17;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .start_time_sml_btn{
        width:85px;
        background:url(/img/observation_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:0px 1px 0px #8e631f;
        border:solid 1px #2a8f17;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .pause_time_btn{
        width:85px;
        background: url(/img/pause_btn_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:0px 1px 0px #832920;
        border:solid 1px #f5ab35;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .stop_time_btn{
        width:85px;
        background: url(/img/stop_btn_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:1px 1px 0px #205f15;
        border:solid 1px #d23727;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .note_box{
        border:solid 1px #cccccc;
        margin:20px 0px;
    }
    .note_box textarea{
        border:0px;
        padding:10px;
        resize:none;
        outline:none;
        height:95px;
        color:#707070;
    }
    .tags_standard_box{
        border-top:solid 1px #cccccc;
        border-bottom:solid 1px #cccccc;
        padding:11px 15px;
        color:#898686;
        font-size:13px;
        height:42px;
        box-sizing:border-box;
    }
    .tags_box{
        height:42px;
        padding:9px 15px;
        box-sizing:border-box;
    }
    .tags_box img{
        vertical-align:middle;
    }
    .tags_box input[type="text"]{
        border:0px;
        outline:none;
    }
    .call_notes_data_box{
        margin:5px 0px;
    }
    .call_notes_data_body{
        font-size:13px;
        padding:10px;
        background:#f2f2f2;
        color:#454545;
    }
    .call_notes_data_header{
        padding:10px;
        color:#858585;
    }
    .call_notes_data_header img{
        vertical-align:middle;
    }
    .callNotes_actionBox{
        float:right;
        margin-top: 3px;
    }
    .optioncls a {
        width: 20px;
        height: 20px;
        border-radius: 50%;
        border: solid 2px #1297e0;
        display: inline-block;
        position: relative;
        text-align: center;
        line-height: 12px;
        text-align:center;
    }
    .call_notes_data_box .tags_standard_box{
        border-top:0px;
    }
    .call_notes_data_box .note_box{
        margin:0px;
    }
    .associate_video{
        width:482px;
        height:190px;
        background:#000;
        margin:20px auto;
        text-align:center;
        position: relative;
    }
    .associate_video .videos-list__item-thumb{
        width: 100%;
        margin: 0 auto;
        position: relative;
    }
    .associate_video .videos-list__item-thumb video{
        width: 95%;
    }
    .associate_video .videos-list__item-thumb .play-icon {
        top: 135px !important;
    }
    .associate_video img{
        margin-top:22px;
        cursor:pointer;
    }
    .video_upload_dialog{
        padding:40px;
        text-align:center;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        /*width:600px;*/
        padding-top: 20px;
    }
    .Associate_Video_btn{
        display:inline-block;
        margin:0px 10px;
        border:solid 1px #add_fromHuddle;
        padding:15px;
        color:#b4b1aa;
        border:solid 1px #cccccc;
    }
    .Associate_Video_btn a{
        color:#b4b1aa;
        text-decoration:none;
        font-weight: normal;
    }
    .Associate_Video_btn img{
        vertical-align:middle;
        position:relative;
        margin-right:5px;
    }
    .video_box_workSpace{
        padding:10px;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        width:600px;
    }
    .video_box_workSpace img{
        vertical-align:middle;
        position:relative;
    }
    .workspace_header{
        color:#979797;
    }
    .back_btn_dialog{
        float:right;
        font-size:13px;
        margin-top:7px;
        cursor: pointer;
    }
    .find_video{
        width:100%;
        box-sizing:border-box;
        padding:10px;
        border:solid 1px #ececec;
        color:#b4b1aa;
        margin:6px 0px;
        -webkit-box-shadow: inset 0px 2px 22px -9px rgba(0,0,0,0.75);
        -moz-box-shadow: inset 0px 2px 22px -9px rgba(0,0,0,0.75);
        box-shadow: inset 0px 2px 22px -9px rgba(0,0,0,0.75);
    }
    .videos_list_box{
        height:270px;
        overflow-y:scroll;
    }
    .video_detail{
        padding:6px;
    }
    .video_detail_left{
        float:left;
        clear: both;
    }
    .video_detail_left img{
        vertical-align: top;
        width: 74px !important;
        height: 55px !important;
        min-width: 74px !important;
        min-height: 60px !important;
    }
    .video_detail_text{
        float:left;
        color:#616160;
        padding:0px 0px 0px 20px;
        font-size:14px;
    }
    .video_detail_text a{
        color:#5a80a0;
        text-decoration:none;
    }
    #observations-1 .observation_left{
        float: left;
        width: 100% !important;
        padding: 10px;
        box-sizing: border-box;
        border-right: solid 0px #dadada;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .videos-list__item {
        margin: 0 0 40px 30px !important;
    }
    .video_viewercls{
        width:auto;
    }
    .observation_mainContainer{
        margin: -10px -20px;
    }
    .coach_cochee_container{
        width: 100%;
        margin: 10px 0px;
    }
    .doted_container{
        padding: 0px;
        color: #959595;
        font-size: 15px;
        margin-bottom: 0px;
    }
    .timer_box2{
        background: #ed1c24;
        font-size: 9px;
        color: #fff;
        padding: 2px 5px;
        position: absolute;
        right: 0px;
        top: -18px;
    }
    .note_box{
        position: relative;
    }
    #tab-area .tab-content{
        padding-top:-10px;
        margin-top: -10px;
    }
    .standard-search{
        position: relative;
        float: none;
        margin: 0 auto;
        width: 390px;
    }
    .standard-search .text-input{
        width: 340px !important;
    }
    .observation_search_bg{
        width: 94% !important;
        background:  url(/img/observation_search_bg.jpg) no-repeat right;
    }
    .printable_icons{
        text-align: right;
        padding-right: 10px;
    }
    #observatons-added-notes{
        //position: relative;
    }
    .videos_list_box .videos-list__item-thumb{
        min-height: 56px;
        margin-left: 23px;
    }
    .videos_list_box .video-unpublished{
        overflow:hidden;
        position: relative;
    }
    .videos_list_box .video-unpublished img{
        min-height: 55px !important;
    }
    .videos_list_box .video-unpublished .play-icon {
        top: 74px !important;
        width: 25px;
        height: 25px;
        background-size: 25px;
        left: 48px;
    }
    #scrollbar1 {
        width: 100%;
    }
    .observationCls { padding-left: 0; margin-left: 0px;}
    .observationCls li{ margin-bottom: 15px;    display: inline-block;}
    .observationCls li .tab-3-box-left{    margin-bottom: 15px;}
    #txtVideostandard_tagsinput {
        width: 469px !important;
        border-bottom: 0 !important;
        border-radius: 0!important;
        border-left: 0 !important;
    }
    #txtVideoTags_tagsinput {
        border-radius: 0px;
        border-left: none;
        width: 469px !important;
        border-bottom: none;
    }
    .comments ul li {
        margin-top: 10px;
        margin-left: 0px;
    }
    .comments ul {
        padding: 0px;
    }
    a.publish_obser{
        background:#2dcc70; padding:4px 12px; color:#fff; text-decoration:none;
        position:relative;

        margin-left: 8px;
        float:right;
        top: -2px;
    }
    .scripted_tick
    {
        position:relative;
        top:20px;
        margin-left: 8px;
        float:right;
        display: block;
        padding: 6px 4px;

    }
</style>
<?php
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];
$user_id = $users['User']['id'];
//$result['s'] = '11586';
//$date = gmdate("H:i:s", (int)$result['s']);
//print_r($date);
//$date = explode(':',$date);
//print_r($date);
//die();
//echo $this->Session->flash('good');
?>
<input id="account_id" type="hidden" value="<?php echo $account_id ?>">
<input id="user_id" type="hidden" value="<?php echo $user_id ?>">
<input id="huddle_id" type="hidden" value="<?php echo $huddle_id ?>">
<input id="video_id" type="hidden" value="<?php echo $video_id ?>">
<input id="video-id" type="hidden" value="<?php echo $video_id ?>">
<div id="showflashmessage1"></div>

<style>
    .stop_observation_container{
        margin: 10px;
    }
    .printable_icons {
        border-bottom: none;
        margin-bottom: 0px;
        padding: 0px 0px 0px 0px;
    }
    .send_message_dialog{
        padding: 20px;
    }
    .send_message_dialog input[type="text"], .send_message_dialog input[type="email"], .send_message_dialog textarea{
        width:100%;
        margin-bottom:8px;
        margin-top: 3px;
    }
    .attachment_box{
        margin: 5px 0px;
    }

    .document_outer { background:#fff; padding:10px;    position: absolute;
                      z-index: 99;
                      border: 1px solid #ddd;
                      float: right;
                      right: 28px;
                      top: 82px; display:none;}
    .document_outer:before {
        pointer-events: none;
        position: absolute;
        z-index: -1;
        content: '';
        border-style: solid;
        -webkit-transition-duration: 0.3s;
        transition-duration: 0.3s;
        -webkit-transition-property: transform;
        transition-property: transform;
        left: calc(50% - 10px);
        top: 0;
        border-width: 0 10px 10px 10px;
        border-color: transparent transparent #e1e1e1 transparent;
        right: 1px;
        left: inherit;
        -webkit-transform: translateY(-10px);
        transform: translateY(-10px);
    }
    .document_outer a{
        height: inherit !important;
        margin: 0 5px !important;}
    .document_click { float:right;    cursor: pointer; width: 22px;}


</style>


<script type="text/javascript">


    $(document).ready(function () {
        $('.document_click').click(function (e) {
            $('.document_outer').slideToggle();
        });

    });


</script>


<div class="stop_observation_container">
    <div class="coach_cochee_container" >
        <a href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/5' ?>" class="back huddle_videos_all">Back to all observations</a>
        <div class="clear"></div>
    </div>



    <div class="printable_icons">
        <img src="/img/amchart-download.png" class="document_click">
        <div class="document_outer">
            <?php if ($huddle_permission == 200): ?>
                <a title="Send Email" id="email_send" data-toggle="modal" data-target="#email_ob"  href="#"><img src="/img/email.png"></a>
            <?php endif; ?>
            <a title="Export comments as PDF" target="_blank"  href="<?php echo $this->base . '/Huddles/print_pdf_comments/' . $video_id . '/2/2.pdf' ?>"><img src="/img/icons/pdf48.png" style="width:28px;" /></a>
            <a title="Export comments as Excel" href="<?php echo $this->base . '/Huddles/print_excel_comments/' . $video_id . '/2' ?>"><img src="/img/icons/excel48.png" style="width:28px;"  /></a>
        </div>
    </div>
    <?php if ($videos['Document']['published'] == 1 && $videos['Document']['is_associated'] == 1): ?>
        <?php if (($h_type == 3 && ($huddle_permission == 200 || $huddle_permission == 210 ) ) || ($this->Custom->coaching_perfomance_level($account_id, $h_type, $huddle_permission))): ?>
            <?php if (isset($video_id) && !empty($video_id)): ?>
                <a rel="tooltip" data-original-title = "<?php echo $this->Custom->get_rating_name($this->Custom->get_average_video_rating($video_id, $account_id, $huddle_id), $account_id); ?>" data-toggle="modal" id="performance_btn"  href="#performance_poup" class="btn performance_cl"><?php
                    if ($huddle_permission == 200) {
                        echo 'Performance Levels';
                    } else {
                        echo 'View Performance Levels';
                    }
                    ?>
                    <span <?php
                    if ($video_details['Document']['not_observed'] == 0) {
                        echo 'style = "display : none;"';
                    }
                    ?> id="performance_level_span_number"> <?php echo $this->Custom->get_average_video_rating($video_id, $account_id, $huddle_id); ?> </span>
                </a>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>



    <?php if ($videos['Document']['published'] == 1 && $videos['Document']['is_associated'] == 1): ?>
        <span class="scripted_tick"><img src="/app/img/scripted_tick.png"> Observation is Published</span>
    <?php else : ?>
        <a class="publish_obser" href="<?php echo $this->base . '/Huddles/publish_scripted_observation/' . $huddle_id . '/' . $video_id ?>" >Publish Observation for Coachee</a>
    <?php endif; ?>
    <div class="clear"></div>
    <div id="commentsTab1" style="visibility: visible;">

        <div class="doted_container">
            <div id="ObservationTitle" name="click to edit title" style="font-weight: bold;cursor:pointer;"><?php echo $video_name['AccountFolderDocument']['title']; ?></div>
            <div class="editArea" style="width:300px;">
                <input id="ajaxInput" value="Observation_note 12-12-1234" type="text" style="width:100%">
            </div>
        </div>
        <div class="call_notes_container call" id="stop-call-notes">
            <?php echo $comments_html ?>
        </div>
    </div>
</div>
<div class="clear"></div>

<div id="email_ob" class="modal in" role="dialog" aria-hidden="true">
    <div class="modal-dialog" style="width:400px;">
        <div class="modal-content">


            <div class="header" style="margin-bottom:0px;">
                <h4 class="header-title nomargin-vertical smargin-bottom">Send Email</h4>
                <a class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">&times;</a>
            </div>

            <div class="send_message_dialog">
                <label class="label" for="subject">Subject</label><br>
                <input id="subject" type="text" name="subject" class="input-xlarge"><br>
                <label class="label" for="email">To:</label><br>
                <input id="email" value="<?php echo $coachee_email ?>" type="email" name="email" class="input-xlarge"><br>
                <div class="attachment_box">
                    <img src="/app/img/attachment.png" />
                    <span id="filename_email_span" ></span>
                </div>

                <label class="label" for="message">Enter a Message</label><br>
                <textarea id="message" name="message" class="input-xlarge" style="height: 80px;"></textarea>
                <input id="account_id" type="hidden" value="<?php echo $account_id ?>">
                <input id="filename_email" type="hidden" value="">
                <input id="send_email" type="button" style="background: <?php echo $this->Custom->get_site_settings('primary_bg_color'); ?>" value="Send Email" class="btn btn-green"></input>
            </div>
        </div>
    </div>
</div>


<div id="performance_poup" class="modal" role="dialog" aria-hidden="false">
    <div class="modal-dialog" style="width:900px;">
        <div class="modal-content">
            <div class="header" style="padding-left: 25px;padding-bottom: 0px;margin-bottom: 0px;padding-top: 15px;">
                <h4 class="header-title nomargin-vertical smargin-bottom">Performance Level</h4>
                <a id="cross" class="close-reveal-modal btn btn-grey close style2" data-dismiss="modal">x</a>
            </div>
            <div class="performance_outer">

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Cancel</button>
                <?php if ($huddle_permission == 200): ?>
                    <button style="position: relative; top: 1px;" class="btn btn-green" id="add_rating_btn">Save</button>
                <?php endif; ?>
            </div>





        </div>
    </div>
</div>



<script type="text/javascript">



    $(document).ready(function () {
        $('#subject').val($('#ObservationTitle').html());
        $('#message').html('Hi <?php echo $coachee_name ?>,');

    });

    $("#showflashmessage1").click(function () {
        $("#flashMessage").fadeOut();

    });
<?php if ($huddle_permission == '200'): ?>
        $("#ObservationTitle").mouseover(function () {
            $(this).css("backgroundColor", "#FAFABE")
        });
        $("#ObservationTitle").mouseout(function () {
            $(this).css("backgroundColor", "#ffffff");
        });
        $("#ObservationTitle").click(function () {
            $(this).css("display", "none");
            $(".editArea").css("display", "block");
            $("#ajaxInput").val($(this).text().trim());
            $("#ajaxInput").focus();
        });
        $("#ajaxInput").blur(function () {

            $(".editArea").css("display", "none");
            $("#ObservationTitle").css("display", "block");
            $("#ObservationTitle").text($(this).val());
            $.ajax({
                type: "POST",
                url: home_url + "/huddles/update_note",
                data: {
                    huddle_id: $('#huddle_id').val(),
                    video_id: $('#video_id').val(),
                    title: $("#ObservationTitle").text()
                },
                success: function (response) {
                    $("#ObservationTitle1").html($("#ObservationTitle").text());
                    $('#subject').val($("#ObservationTitle").text());
                },
                error: function (e) {

                }
            });


        });
<?php endif; ?>
    $("#email_send").click(function () {
        var video_id = $('#video_id').val();
        $.ajax({
            type: 'POST',
            url: home_url + '/Huddles/print_pdf_comments_1/' + video_id,
            success: function (response) {
                $('#filename_email').val(response);
                //alert($('#filename_email').val());
                $('#filename_email_span').html('PdfReport' + response);
            }
        });


    });




    $("#send_email").click(function () {
        $('#email_ob').modal('hide');
        var postData = {
            name: $('#name').val(),
            email: $('#email').val(),
            message: $('#message').val(),
            account_id: $('#account_id').val(),
            subject: $('#subject').val(),
            filename: $('#filename_email').val(),
            video_id: $('#video_id').val(),
            huddle_id: $('#huddle_id').val()
        };


        $.ajax({
            type: 'POST',
            url: home_url + '/Huddles/send_email/',
            data: postData,
            success: function (response) {
                var show_msg = '<div id="flashMessage" class="message success" style="cursor: pointer;">Email Sent Successfully</div>';
                $("#showflashmessage1").prepend(show_msg);

            }
        });
    });


    $("#performance_btn").click(function () {

        $.ajax({
            type: 'POST',
            data: {
                huddle_id: <?php echo $huddle_id; ?>,
                account_id: <?php echo $account_id; ?>,
                video_id: '<?php echo $video_id; ?>'
            },
            url: home_url + '/huddles/performace_level_update',
            success: function (response) {

                $('.performance_outer').html(response);

            },
            errors: function (response) {

            }

        });

    });



</script>
