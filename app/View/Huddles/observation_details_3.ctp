<style type="text/css">

    #txtVideoTags1_tagsinput {
        border-radius: 0px;
        border-left: none;
        width: 484px !important;
        border-bottom: none;
    }
    #ObservationTitle1 {
        font-weight: bold;
        width: 410px;
        margin-bottom: 10px;
    }

    #txtVideostandard1_tagsinput{
        width: 484px !important;
        border-bottom: 0 !important;
        border-radius: 0!important;
        border-left: 0 !important;
    }
    #observations-2 #txtVideoTags_tagsinput {
        border-radius: 0px;
        border-left: none;
        width: 484px !important;
        border-bottom: none;
    }
    #txtVideostandard1_tag{
        width: 100%  !important;
        cursor: pointer;
    }

    .observation_left{
        float: left;
        width: 50%;
        padding: 10px;
        box-sizing: border-box;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        padding-right: 0px;
        padding-left: 15px;
    }
    .observation_left p{
        margin:0px;
        font-size:13px;
    }
    .observation_left h2{
        text-align:center;
        font-size:20px;
        font-weight:normal;
        margin: 12px 0px;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .observation_left h3{
        color:#707070;
        font-size:14px;
        text-align:center;
        font-weight:normal;
        margin:0px;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .observation_right{
        float:right;
        width:50%;
        padding:10px;
        box-sizing:border-box;
        margin-top: -20px;
    }
    .observation_btn_box{
        text-align:center;
        margin:15px 0px;
    }
    .observation_btn{
        width:120px;
        background:url(/img/observation_bg.png) repeat-x;
        font-weight:bold;
        font-size:18px;
        color:#fff;
        /*text-shadow:1px 1px 0px #205f15;*/
        border:solid 2px #2a8f17;
        border-radius:5px;
        outline:none;
        text-align:center;
        padding:10px 0px;
        cursor:pointer;
    }
    .timer_box{
        padding:0px 0px;
        text-align:center;
        color:#5daf46;
        font-weight:300 !important;
        font-size:4em ;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
    }
    .timer_box span{
        font-weight:300 !important;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
    }
    .stop_watch_sec{
        color:#707070;
        font-size:20px;
        font-weight:400;
    }
    .start_btn_box{
        text-align:center;
    }
    .start_time_btn{
        width:235px;
        background:url(/img/observation_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:0px 1px 0px #8e631f;
        border:solid 1px #2a8f17;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .start_time_sml_btn{
        width:85px;
        background:url(/img/observation_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:0px 1px 0px #8e631f;
        border:solid 1px #2a8f17;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .pause_time_btn{
        width:85px;
        background: url(/img/pause_btn_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:0px 1px 0px #832920;
        border:solid 1px #f5ab35;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .stop_time_btn{
        width:85px;
        background: url(/img/stop_btn_bg.png) repeat-x;
        font-size:14px;
        color:#fff;
        text-shadow:1px 1px 0px #205f15;
        border:solid 1px #d23727;
        border-radius:20px;
        outline:none;
        text-align:center;
        padding:6px 0px;
        cursor:pointer;
    }
    .note_box{
        border:solid 1px #cccccc;
        margin:20px 0px;
    }
    .note_box textarea{
        border:0px;
        padding:10px;
        resize:none;
        outline:none;
        height:95px;
        color:#707070;
    }
    #observations-comments::-webkit-input-placeholder
    {
        color:    #999;
    }

    .tags_standard_box{
        border-top:solid 1px #cccccc;
        border-bottom:solid 1px #cccccc;
        padding:11px 15px;
        color:#898686;
        font-size:13px;
        height:42px;
        box-sizing:border-box;
    }
    .tags_box{
        height:42px;
        padding:9px 15px;
        box-sizing:border-box;
    }
    .tags_box img{
        vertical-align:middle;
    }
    .tags_box input[type="text"]{
        border:0px;
        outline:none;
    }
    .call_notes_data_box{
        margin:5px 0px;
    }
    .call_notes_data_body{
        font-size:13px;
        padding:10px;
        background:#f2f2f2;
        color:#454545;
    }
    .call_notes_data_header{
        padding:10px;
        color:#858585;
    }
    .call_notes_data_header img{
        vertical-align:middle;
    }
    .callNotes_actionBox{
        float:right;
        margin-top: 3px;
    }
    .optioncls a {
        width: 20px;
        height: 20px;
        border-radius: 50%;
        border: solid 2px #1297e0;
        display: inline-block;
        position: relative;
        text-align: center;
        line-height: 12px;
        text-align:center;
    }
    .call_notes_data_box .tags_standard_box{
        border-top:0px;
    }
    .call_notes_data_box .note_box{
        margin:0px;
    }
    .associate_video{
        width:482px;
        height:190px;
        background:#000;
        margin:20px auto;
        text-align:center;
        position: relative;
    }
    .associate_video .videos-list__item-thumb{
        width: 100%;
        margin: 0 auto;
        position: relative;
    }
    .associate_video .videos-list__item-thumb video{
        width: 95%;
    }
    .associate_video .videos-list__item-thumb .play-icon {
        top: 135px !important;
    }
    .associate_video img{
        margin-top:22px;
        cursor:pointer;
    }
    .video_upload_dialog{
        padding:40px;
        text-align:center;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        /*width:600px;*/
        padding-top: 20px;
    }
    .Associate_Video_btn{
        display:inline-block;
        margin:0px 10px;
        border:solid 1px #add_fromHuddle;
        padding:15px;
        color:#b4b1aa;
        border:solid 1px #cccccc;
    }
    .Associate_Video_btn a{
        color:#b4b1aa;
        text-decoration:none;
        font-weight: normal;
    }
    .Associate_Video_btn img{
        vertical-align:middle;
        position:relative;
        margin-right:5px;	
    }
    .video_box_workSpace{
        padding:10px;
        font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
        width:600px;
    }
    .video_box_workSpace img{
        vertical-align:middle;
        position:relative;
    }
    .workspace_header{
        color:#979797;
    }
    .back_btn_dialog{
        float:right;
        font-size:13px;
        margin-top:7px;
        cursor: pointer;
    }
    .find_video{
        width:100%;
        box-sizing:border-box;
        padding:10px;
        border:solid 1px #ececec;
        color:#b4b1aa;
        margin:6px 0px;
        -webkit-box-shadow: inset 0px 2px 22px -9px rgba(0,0,0,0.75);
        -moz-box-shadow: inset 0px 2px 22px -9px rgba(0,0,0,0.75);
        box-shadow: inset 0px 2px 22px -9px rgba(0,0,0,0.75);
    }
    .videos_list_box{
        height:270px;
        overflow-y:scroll;
    }
    .video_detail{
        padding:6px;
    }
    .video_detail_left{
        float:left;
        clear: both;
    }
    .video_detail_left img{
        vertical-align: top;
        width: 74px !important;
        height: 55px !important;
        min-width: 74px !important;
        min-height: 60px !important;
    }
    .video_detail_text{
        float:left;
        color:#616160;
        padding:0px 0px 0px 20px;
        font-size:14px;
    }
    .video_detail_text a{
        color:#5a80a0;
        text-decoration:none;
    }
    #observations-1 .observation_left{
        float: left;
        width: 100% !important;
        padding: 10px;
        box-sizing: border-box;
        border-right: solid 0px #dadada;
        font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
    }
    .videos-list__item {
        margin: 0 0 40px 30px !important;
    }
    .video_viewercls{
        width:auto;
    }
    .observation_mainContainer{
        margin: -10px -20px;
    } 
    .coach_cochee_container{
        width: 100%;
        margin: 10px 0px;
    }
    .doted_container{
        padding:0px;
        color: #959595;
        font-size: 15px;
        margin-bottom: 10px;
    }
    .timer_box2{
        background: #ed1c24;
        font-size: 9px;
        color: #fff;
        padding: 2px 5px;
        position: absolute;
        right: 0px;
        top: -18px;
    }
    .note_box{
        position: relative;
    }
    #tab-area .tab-content{
        padding-top:-10px;
        margin-top: 0px !important;
    }
    .standard-search{
        position: relative;
        float: none;
        margin: 0 auto;
        width: 390px;
    }
    .standard-search .text-input{
        width: 340px !important;
    }
    .observation_search_bg{
        width: 94% !important;
        background:  url(/img/observation_search_bg.jpg) no-repeat right;
    }
    .printable_icons{
        text-align: right;
        padding-right: 10px;
    }
    #observatons-added-notes{
        //position: relative;
    }
    .videos_list_box .videos-list__item-thumb{
        min-height: 56px;
        margin-left: 23px;
    }
    .videos_list_box .video-unpublished{
        overflow:hidden;
        position: relative;
    }
    .videos_list_box .video-unpublished img{
        min-height: 55px !important;
    }
    .videos_list_box .video-unpublished .play-icon {
        top: 74px !important;
        width: 25px;
        height: 25px;
        background-size: 25px;
        left: 48px;
    }
    #scrollbar1 {
        width: 100%;
    }
    .observationCls { padding-left: 0; margin-left: 0px;}
    .observationCls li{ margin-bottom: 15px;    display: inline-block;}
    .observationCls li .tab-3-box-left{    margin-bottom: 15px;}
    #txtVideostandard_tagsinput {
        width: 469px !important;
        border-bottom: 0 !important;
        border-radius: 0!important;
        border-left: 0 !important;
    }
    #txtVideoTags_tagsinput {
        border-radius: 0px;
        border-left: none;
        width: 469px !important;
        border-bottom: none;
    }

    #tab-area .tabset{
        width:100% !important;
    }
    .call_notes_container{
        height:576px;
        overflow-y:auto;
    }
    #txtVideostandard_tagsinput {
        width: 495px !important;
    }
    #txtVideoTags_tagsinput {
        width: 495px !important;
    }
    #tagFilterContainer .btn{
        box-shadow:none !important;
        cursor:default !important;
        text-shadow: none !important;
        background: #f4f4f4;
    }
    #tagFilterContainer .btn:active{
        box-shadow:none;
        cursor:default !important;
    }

    #tagFilterContainer .btn:hover{
        cursor:default !important;
    }

    #tagFilterContainer .btn:focus{
        outline:none;
        cursor:default !important;
    }
    #observations-comments:focus{
        z-index: 20;
        /*        box-shadow: inset 0 1px 1px rgba(0,0,0,0.075),0 0 8px rgba(82,168,236,0.6) !important;
                border-color: rgba(82,168,236,0.8) !important;*/
        border-color: #fff !important;
        box-shadow: none !important;
    }
    .petsCls {
        float: right;
        margin-right: 10px;
        /*margin-top: -27px;*/
        right: 0;
        position: relative;
        margin-bottom: 3px;
    }
    .petsCls label {
        font-weight: normal !important;
        font-size: 13px;
        float: left;
    }
    input#press_enter_to_send {
        margin-top: 3px;
        float: left;
        margin-right: 4px;
    }
    .publish_obser1{ 
        background: #2dcc70;
        padding: 4px 12px;
        color: #fff !important;
        text-decoration: none;
        position: relative;
        top: -10px;
        margin-left: 8px;
    }
    #expList .L1 {
        margin-left: 0px;
        list-style: none;
    }
    #expList .L2 {
        margin-left: 35px;
        list-style: none;
    }
    #expList .L3 {
        margin-left: 70px;
        list-style: none;
    }
    #expList .L4 {
        margin-left: 105px;
        list-style: none;
    }
    #expList-edit .L1 {
        margin-left: 0px;
        list-style: none;
    }
    #expList-edit .L2 {
        margin-left: 35px;
        list-style: none;
    }
    #expList-edit .L3 {
        margin-left: 70px;
        list-style: none;
    }
    #expList-edit .L4 {
        margin-left: 105px;
        list-style: none;
    }
    .expList-edit li.standard-cls{
        margin-top: 10px !important
    }
</style>


<?php
$huddle_permission = $this->Session->read('user_huddle_level_permissions');
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];
$user_id = $users['User']['id'];
$date = gmdate("H:i:s", (int) $video['Document']['current_duration']);
$date = explode(':', $date);

function defaulttagsclasses4($posid) {
    $class_name = 'tags_qucls';
    if ($posid == 1) {
        $class_name = 'tags_qucls';
    } else if ($posid == 2) {
        $class_name = 'tags_sugcls';
    } else if ($posid == 3) {
        $class_name = 'tags_notescls';
    } else if ($posid == 4) {
        $class_name = 'tags_strangthcls';
    } else if ($posid == 5) {
        $class_name = 'tags_strangthcls';
    }
    return $class_name;
}
?>
<input id="account_id" type="hidden" value="<?php echo $account_id ?>">
<input id="user_id" type="hidden" value="<?php echo $user_id ?>">
<input id="huddle_id" type="hidden" value="<?php echo $huddle_id ?>">
<input id="video_id" type="hidden" value="<?php echo $video_id ?>">
<input id="count_comments" type="hidden" value="<?php echo $count_comments ?>">



<style>
    .stop_observation_container{
        margin: 10px;
    }
    .printable_icons {
        border-bottom: solid 1px #E0E0E0;
        margin-bottom: 15px;
        padding:0px 0px 10px 0px;
    }
    .video_recording_container{
        background:#000 url('/app/webroot/img/camera.png') no-repeat center center;
        padding:20px;
        position:relative;
        height:235px;
        margin-top:10px;

    }
    .video_recording_container .timer_box{
        color:#fff;
        font-size:13px;
        position: absolute;
        right: 10px;
        top: 10px;
    }
    .live_record{
        position:absolute;
        left: 10px;
        top: 10px;
        color:#fff;
        font-size:13px;
    }
    .rec_border{
        border-bottom:solid 1px #D3D2D6;
        height:1px;
        margin:10px 0px;
    }
    input.publish_obser{
        background: #2dcc70;
        padding: 8px 25px;
        color: #fff;
        text-decoration: none;
        position: relative;
        top: 20px;
        margin-left: 8px;
        float: right;
        border: none;
        font-weight: bold;
    }


</style>

<script type="text/javascript">

    var observation_notes_array = [];

</script>
<?php $alert_messages = $this->Custom->get_page_lang_based_content('alert_messages'); ?>
<div class="header header-huddle">


    <h2 class="title" style="width: 445px;"><?php echo $video_data['AccountFolder']['name']; ?></h2>
    <p class="title-desc" style="width: 720px;"> </p>

    <?php if ($videos['Document']['published'] == '1' && $videos['Document']['is_processed'] == 4 && $videos['Document']['is_associated'] == 0): ?>
        <div id="publish-button-container" style="text-align: right;float: right;position: relative;top: -48px;">  

            <input type="submit" style="margin-right: 8px;" id="publish-observations" data-document-id="<?php echo $videos['Document']['id']; ?>" class="publish_obser" value="Publish Observation" />
        </div>
    <?php endif; ?>

</div>

<div class="observation_mainContainer">
    <div class="observation_left">



        <div class="coach_cochee_container" >
            <a style="margin-bottom:10px;" href="<?php echo $this->base . '/Huddles/view/' . $huddle_id . '/5' ?>" class="back huddle_videos_all">Back to all observations</a>            
            <div class="clear"></div>           
        </div>

        <div id="ObservationTitle1" name="click to edit title">
          <?php //echo date('m-d-Y'); ?>
            <?php echo $video_name['AccountFolderDocument']['title']; ?>
        </div>


        <?php if (is_array($videos) && count($videos) > 0) { ?>

            <?php
            $timeS = array();
            $timecls = array();
            $videoID = $videos['Document']['id'];
            $document_files_array = $this->Custom->get_document_url($videos['Document']);

            if (empty($document_files_array['url'])) {
                $videos['Document']['published'] = 0;
                $document_files_array['url'] = $videos['Document']['original_file_name'];
                $videos['Document']['encoder_status'] = $document_files_array['encoder_status'];
            } else {
                $videos['Document']['encoder_status'] = $document_files_array['encoder_status'];
            }
//                        echo "<pre>";
//                        print_r($videos);
//                        echo "</pre>";
            $videoFilePath = pathinfo($document_files_array['url']);
            $videoFileName = $videoFilePath['filename'];


            $videoID = $videos['Document']['id'];
            $document_files_array = $this->Custom->get_document_url($videos['Document']);
            if (empty($document_files_array['url'])) {
                $videos['Document']['published'] = 0;
                $document_files_array['url'] = $videos['Document']['original_file_name'];
                $videos['Document']['encoder_status'] = $document_files_array['encoder_status'];
            } else {
                $videos['Document']['encoder_status'] = $document_files_array['encoder_status'];
                @$videos['Document']['duration'] = $document_files_array['duration'];
            }

            if ($videoCommentsArray) {
                foreach ($videoCommentsArray as $cmt) {
                    if (!empty($cmt['Comment']['time'])) {
                        $timeS[] = $cmt['Comment']['time'];
                    }
                }
            }

            $videoFilePath = pathinfo($document_files_array['url']);
            $videoFileName = $videoFilePath['filename'];
            ?>
            <div>

                <input id="txtHuddleID" type="hidden" value="<?php echo $huddle_id ?>"/>
                <div>
                    <div id="video_span" style="width:100%;">

                        <div class="associate_video" id="open-dialog" style="height: 322px;">

                            <div class="videos-list__item-thumb">
                                <style>
                                    .associate_video .videos-list__item-thumb video {
                                        width: 100%;
                                    }
                                    .hide_add_comemnts{
                                        display: none;
                                    }
                                    .show_add_comemnts{
                                        display: block;
                                    }
                                </style>
                                <?php
                                $hide_add_comments = '';
                                if ($videos['Document']['published'] == '1' && $videos['Document']['is_processed'] == '4') {
                                    $thumbnail_image_path = $document_files_array['thumbnail'];
                                    $video_path = $document_files_array['url'];
                                    $hide_add_comments = 'show_add_comemnts';
                                    ?> 
                                    <input type="hidden" id="txtCurrentVideoID" value="<?php echo $video_id; ?>" />
                                    <input type="hidden" id="txtCurrentVideoUrl" value="<?php echo $this->base . '/Huddles/view/' . $huddle_id . "/1/$video_id" ?>" />
                                    <video id="example_video_<?php echo $videos['Document']['id'] ?>" class="video-js vjs-default-skin" controls preload="metadata" width="100%" height="320" poster="<?php echo $thumbnail_image_path; ?>" data-markers="[<?php echo implode(',', array_reverse($timeS)); ?>]" data-cls="[<?php echo implode(',', array_reverse($timecls)); ?>]">
                                        <source src="<?php echo $video_path; ?>" type='video/mp4'/>
                                    </video>

                                <?php } elseif ($videos['Document']['published'] == '0' && $videos['Document']['is_processed'] == '4') { ?>
                                    <?php
                                    $thumbnail_image_path = $document_files_array['thumbnail'];
                                    $video_path = $document_files_array['url'];
                                    $hide_add_comments = 'hide_add_comemnts';
                                    ?> 
                                    <div style="width: 480px; height: 321px; background: #000; float: left; text-align: center; color: #fff; position:relative;" id="observation-details-box" >
                                        <div id="progress-title" style="padding-top: 150px;z-index: 99;position: relative; text-transform: uppercase;">0% initiating</div>
                                        <div id="message-box">

                                        </div>

                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <div style="clear: both;"></div>
            </div>



        <?php } ?>
        <div style="clear: both;"></div>


        <?php if ($this->Custom->is_enable_tags($account_id)): ?>
            <?php if ($this->Custom->is_enable_huddle_tags($account_id)): ?>

                <div class="divblockwidth <?php echo $hide_add_comments; ?>" style="margin-top: 15px;" id="tag_div">
                    <?php
                    if (count($tags) > 0) {
                        $count = 1;
                        foreach ($tags as $tag) {
                            ?>
                            <a class="default_tag123 <?php echo defaulttagsclasses4($count) ?>" href="javascript:#" position_id="<?php echo $count; ?>" status_flag="0"># <?php echo $tag['AccountTag']['tag_title']; ?></a>
                            <?php
                            $count++;
                        }
                    }
                    ?>
                </div>   
            <?php endif; ?>
        <?php endif; ?>
        <div class="clear"></div>
        <div class="note_box <?php echo $hide_add_comments; ?>">
            <div class="commentsOueterCls" style="margin-right: 1px;">
                <textarea cols="50" onkeyup="textAreaAdjust(this)" style="overflow:hidden;resize: none;border-radius:0px;margin-bottom: 5px;border: none;" id="observations-comments"  name="comment[comment]" placeholder="Add a comment..." rows="4"></textarea>
                <div class="petsCls">
                    <input style="" type="checkbox" id="press_enter_to_send" name="press_enter_to_send" myssss="<?php echo $press_enter_to_send; ?>" <?php echo $press_enter_to_send == '1' ? 'checked' : ''; ?>>
                    <label style="" for="press_enter_to_send">Press Enter to post</label>
                </div>
                <div class="clearfix"></div>
            </div>
            <!--<textarea  cols="50" id="observations-comments" required name="comment[comment]" placeholder="Add a comment..." rows="4" style="position: relative; outline: 0px; background: transparent;border: none;box-shadow: 0 0px 0px rgba(0,0,0,0.09) inset;" ></textarea>-->
            <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                    <div class="tags divblockwidth">
                        <div class="video-tags-row row">
                            <input type="text" name="txtVideostandard" data-default="Tag Standard..." id="txtVideostandard" value="" placeholder="" style="width:464px !important"  disabled="disabled"  required/>
                        </div>
                        <div class="clear" style="clear: both;"></div>
                    </div>
                    <input type="text" name="txtVideostandard_vid_acc_tag_ids"
                           data-default="" id="txtVideostandard_vid_acc_tag_ids"
                           value="" placeholder="" style="display: none;" />
                <?php endif; ?>
            <?php endif; ?>
            <div class="tags divblockwidth">
                <div class="video-tags-row row">
                    <input type="text" name="txtVideoTags" data-default="Tags..." id="txtVideoTags" value="" placeholder=""  style="width:112px !important;" readonly="readonly" disabled="disabled"/>
                </div>
                <div class="clear" style="clear: both;"></div>
            </div>
            <div class="clear" style="clear: both;"></div>
        </div>
        <div class="input-group  <?php echo $hide_add_comments; ?>" style="text-align:right;">
            <input type="hidden" name="assessment_value" id="synchro_time_class_tags1">

            <?php if ($this->Custom->is_enable_assessment($account_id) && $this->Custom->is_enabled_framework_and_standards($account_id) && $this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                <style>
                    .default_rating123{
                        max-width: 95px;
                        overflow: hidden;
                        text-overflow: ellipsis;
                        white-space: nowrap;
                    }
                </style>
                <?php    if($huddle_type == 3 ): ?>
                <div class="divblockwidth2" style="float: left;margin-bottom: 20px;" id="tag_div">
                    <?php
                    if (count($ratings) > 0) {
                        $count = 1;
                        foreach ($ratings as $tag) {
                            ?>
                            <a rel="tooltip" data-original-title="<?php echo substr($tag['AccountMetaData']['meta_data_name'], 13); ?>" class="default_rating123 <?php echo defaulttagsclasses4($count) ?>" href="javascript:#" position_id="<?php echo $count; ?>" status_flag="0" value="<?php echo substr($tag['AccountMetaData']['meta_data_name'], 13); ?>"># <?php echo substr($tag['AccountMetaData']['meta_data_name'], 13); ?></a>
                            <?php
                            $count++;
                        }
                    }
                    ?>
                </div>   
                <?php endif; ?>
                <div class="clear" style="clear: both;"></div> 
            <?php endif; ?>
            <input id="synchro_time" name="synchro_time" type="hidden" value="{:value=&gt;&quot;&quot;}" />
            <?php if ($videos['Document']['published'] == 1): ?>
                <div id="comment-for" class="input-group" style="margin-right: 168px;">
                    For:
                    <label for="for_synchro_time">
                        <input id="for_synchro_time"  name="for" type="radio" value="synchro_time" /> Time-specific (<span id="right-now-time">0:00</span>)
                    </label>

                    <label for="for_entire_video">
                        <input id="for_entire_video"  name="for" type="radio" value="entire_video" checked/> Whole video
                    </label>
                    <input style="margin-right: 4px;" type="checkbox" id="pause_while_type" name="pause_while_type" <?php echo $type_pause == '1' ? 'checked' : ''; ?>><label style="float:right;margin-right: -168px;"  for="pause_while_type" >Pause video while typing</label>
                </div>
            <?php endif; ?>
            <input id="add-notes" type="submit" name="submit" value="Add Note" class="btn btn-green" style="height:35px;">
            <div class="clear" style="clear: both;"></div>
        </div> 
        <div class="clear" style="clear: both;"></div>        


    </div>





    <style>
        .observation_detail li{
            margin-bottom: 5px;
            position: relative;
        }
        .observation_detail li .tab-3-box{
            box-shadow: none;
            padding: 10px;
        }
        .observation_detail li .tab-3-box .tab-3-box-left{
            margin-bottom: 0px;
        }
        .observation_detail li .tab-3-box .tab-3-box-left h2{
            margin-bottom: 0px;
        }
        .observation_detail li .tab-3-box .tab-3-box-left h2 a{
            color: #2678C0;
            text-decoration: none;
        }
        .observation_detail li .tab-3-box .tab-3-box-left .frame{
            background: none;
        }
        .observation_detail li .tab-3-box .tab-3-box-left .text{
            padding-top: 5px;
        }
        .video_icon{
            height: 36px;
            position: absolute;
            right: 15px;
            top: 40px;
        }
        .timer_container{
            background: #F5F7F6;
            padding:15px  10px;
        }
        .right-box {
            width: 100% !important;
            margin-top: 1px;
            margin-right: -2px;
        }
        .no_notes{
            text-align: center;
            margin:20px;
            font-size: 15px;
            color:#AFAFAF;
        }
    </style>

    <?php
    if ($count_comments == 0):
        ?>
        <div id="print_icons_container">
            <div  class="printable_icons" style="display:none;">
                <a href="#"><img src="/img/email.png"></a>

                <a target="_blank" href="<?php echo $this->base . '/Huddles/print_pdf_comments/' . $video_id . '/2/2.pdf' ?>"><img src="/img/icons/pdf48.png" style="width:28px;" /></a>

                <a href="<?php echo $this->base . '/Huddles/print_excel_comments/' . $video_id . '/2' ?>"><img src="/img/icons/excel48.png" style="width:28px;"  /></a>
            </div>
        </div>

        <div id="observatons-added-notes" style="display: none;">
            <style>
                .call_notes_data_header {
                    padding: 6px 7px;
                    color: #858585;
                }
                .callNotes_actionBox {
                    margin-top: 10px;
                }
                .call_notes_data_header .comments {
                    list-style-type: none;
                    padding: inherit;
                    margin: inherit;
                    font-size: inherit;
                    color: inherit; 
                    float:left;
                }
                .call_notes_data_header .synchro {
                    border-top:0px;
                }
                .call_notes_data_header .synchro-inner{
                    position: inherit;
                    top:inherit;
                    left: inherit;
                    padding: inherit;
                    background-color:inherit;
                    border-radius: inherit;
                }
                .bubble_adjs .btnwraper{
                    margin-right:10px;
                }
                #tagFilterList{
                    float: left;
                }
                .bubble_adjs{
                    width:100% !important;
                }

            </style>
            <style>
                .vjs-tooltip{display: none !important; }
            </style>
            <div class="call_notes_container" id="vidComments">
            </div>
        </div>
    <?php endif; ?>

    <div id="comments_user_meta_data" style="display:none;">
        <a href="<?php echo $this->base . '/users/editUser/' . $users['User']['id']; ?>">
            <?php if (isset($users['User']['image']) && $users['User']['image'] != ''): ?>
                <?php
                $chimg = $this->Custom->getSecureSibmecdnImageUrl("static/users/" . $users['User']['id'] . "/" . $users['User']['image']);
                echo $this->Html->image($chimg, array('alt' => 'Reply', 'class' => 'photo inline', 'rel' => 'image-uploader', 'height' => '21', 'width' => '21', 'align' => 'left'));
                ?>
            <?php else: ?>
                <img width="21" height="21"  src="<?php echo $this->Custom->getSecureSibmeResouceUrl($this->webroot . 'img/home/photo-default.png'); ?>" class="photo photo inline" rel="image-uploader" alt="Photo-default">
            <?php endif; ?>
        </a>
        <div class="comment-header"><?php echo $users['User']['first_name'] . " " . $users['User']['last_name']; ?></div>
    </div>

    <div class="observation_right">
        <div class="" id="tab-area">
            <div class="right-box">
                <ul class="tabset commentstabs">
                    <li class="">
                        <a id="comments" class="tab active" href="#commentsTab">
                            Notes (<?php echo $count_comments; ?>)
                        </a>
                    </li>
                    <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                        <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                            <li class="">
                                <a id="frameWorkss" class="tab" href="#frameWorksTab">
                                    Framework
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endif; ?>

                </ul>
                <div class="clear"></div>
                <div id="commentsTab" style="visibility: visible;padding:10px 0px;"> 
                    <?php if ($count_comments == 0) { ?>
                        <div id="no_notes_added1" class="no_notes">No Notes Added Yet</div>
                    <?php } ?>

                    <div id="observation-comments"></div> 
                    <?php echo $comments_html ?>
                </div>
                <?php if ($this->Custom->is_enabled_framework_and_standards($account_id)): ?>
                    <?php if ($this->Custom->is_enabled_huddle_framework_and_standards($account_folder_id)): ?>
                        <div class="tab-content tab" id="frameWorksTab">
                            <div class="search-box standard-search" style="position: relative;width:100%;">
                                <input type="button" id="btnSearchTags" class="btn-search" value="">
                                <input class="text-input observation_search_bg" id="txtSearchTags" type="text" value="" placeholder="Search Standards..." style="margin-right: 0px;width: 94% !important;">
                                <span id="clearTagsButton" class="clear-video-input-box" style="display:none;right: 42px;top: 20px;">X</span>
                            </div>
                            <div id="scrollbar1" style="float: left;">
                                <div class="viewport short" style="overflow:scroll;overflow-x: hidden;">
                                    <div class="overview p-left0" style="top: 0px;padding: 0px;">
                                        <div id="listContainer">
                                            <style type="text/css">
                                                .standardRed {
                                                    color: red;display: inline;
                                                }
                                                .standardBlue {
                                                    color: blue;display: inline;
                                                }
                                                .standardBlack {
                                                    color: #000;display: inline;
                                                }
                                                .standardOrange {
                                                    color: orange;display: inline;
                                                }
                                                .standardGreen {
                                                    color: green;display: inline;
                                                }
                                                .frame_work_heading{
                                                    font-family:Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif !important;
                                                    font-weight: 300 !important;
                                                    font-size: 20px !important;
                                                    margin:10px 0px !important;
                                                    cursor:auto !important;
                                                }
                                            </style>
                                            <ul id="expList" class="expList" style="padding-left:0px">
                                                <?php

                                                if (!empty($framework_data)) {

                                                    $checkbox_settings = $framework_data['account_framework_settings']['AccountFrameworkSetting']['checkbox_level'];
                                                    foreach ($framework_data['account_tag_type_0'] as $row) {


                                                        ?>
                                                        <?php if($checkbox_settings ==$row['AccountTag']['standard_level']):?>
                                                            <li class="standard standard-cls L<?php echo $row['AccountTag']['standard_level']?>">
                                                                <?php if ($huddle_type == 3): ?>
                                                                    <?php if ($this->Custom->check_if_evalutor($huddle_id, $user_id)): ?>
                                                                        <input class="check_class" type="checkbox" name="name1" account_tag_id="<?php echo $row['AccountTag']['account_tag_id']; ?>" st_code="<?php echo $row['AccountTag']['tag_code']; ?>" st_name="<?php echo $row['AccountTag']['tag_title']; ?>"/>
                                                                    <?php endif ?>
                                                                <?php else: ?>
                                                                    <input class="check_class" type="checkbox" name="name1" account_tag_id="<?php echo $row['AccountTag']['account_tag_id']; ?>" st_code="<?php echo $row['AccountTag']['tag_code']; ?>" st_name="<?php echo $row['AccountTag']['tag_title']; ?>"/>
                                                                <?php endif ?>

                                                                <?php
                                                                $account_tag = explode(':', $row['AccountTag']['tag_html']);
                                                                echo '<span style="color: #7c7c69;font-weight: bold;">' . $row['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ': </span>' . $account_tag[1];
                                                                ?>
                                                            </li>
                                                        <?php else:?>
                                                            <li class="standard standard-cls L<?php echo $row['AccountTag']['standard_level']?>">
                                                                <?php echo $row['AccountTag']['tag_code'] . ' - ' . $row['AccountTag']['tag_html']; ?>
                                                            </li>
                                                        <?php endif;?>

                                                        <?php
                                                    }
                                                }?>
                                                <li id="noresults" style="width: 280px;">No standards match your search
                                                    criteria.
                                                </li>
                                            </ul>
                                            <ul id="expList1" class="expList1" style="padding-left:0px; display: none;">

                                                <?php
                                                if (!empty($standardsL2)) {
                                                    for ($i = 0; $i < count($standardsL2); $i++) {
                                                        ?>
                                                        <li class="frame_work_heading" ><?php echo $standardsL2[$i]['AccountTag']['tag_code'] . ' - ' . $standardsL2[$i]['AccountTag']['tag_title']; ?></li>
                                                        <?php
                                                        foreach ($standards as $standard) {
                                                            if ($standardsL2[$i]['AccountTag']['account_tag_id'] == $standard['AccountTag']['parent_account_tag_id'] || ($standardsL2[$i]['AccountTag']['account_tag_id'] == $standard['AccountTag']['framework_id'])) {
                                                                ?>
                                                                <li class="standard standard-cls" >
                                                                    <input class="check_class" type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" />
                                                                    <?php
                                                                    $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                                    echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                                    ?>                                        
                                                                </li>

                                                                <?php
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    if (!empty($standards)) {
                                                        foreach ($standards as $standard) {
                                                            ?>
                                                            <li class="standard standard-cls">
                                                                <input class="check_class" type="checkbox" name="name1" st_code="<?php echo $standard['AccountTag']['tag_code']; ?>" st_name="<?php echo $standard['AccountTag']['tag_title']; ?>" />
                                                                <?php
                                                                $account_tag = explode(':', $standard['AccountTag']['tag_html']);
                                                                echo '<span style="color: #7c7c69;font-weight: bold;">' . $standard['AccountTag']['tag_code'] . ' - ' . $account_tag[0] . ':</span>' . $account_tag[1];
                                                                ?>                                        
                                                            </li>

                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?> 
                                                <li id="noresults" style="width: 280px;">No standards match your search criteria.</li>                             
                                            </ul>
                                            <script type="text/javascript">

        //                                    $(document).ready(function () {
        //                                        $('.standard input').on('change', function () {
        //                                            var tag_code = $(this).attr('st_code');
        //                                            var tag_value = '';
        //                                            var tag_array = {};
        //                                            var tag_name = $(this).attr('st_name');
        //                                            tag_array = tag_name.split(" ");
        //                                            if ($(this).is(':checked')) {
        //                                                tag_array = tag_name.split(" ");
        //                                                tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
        //                                                $('#txtVideostandard').addTag(tag_value);
        //                                                if ($('input[name="name1"]:checked').length > 0) {
        //                                                    $('#txtVideostandard_tag').hide();
        //                                                }
        //                                            } else {
        //                                                tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
        //                                                $('#txtVideostandard').removeTag(tag_value);
        //                                            }
        //                                        });
        //                                    });
                                            </script>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    <?php endif; ?>
                <?php endif; ?> 
            </div> 
        </div>


    </div>
</div>
<style type="">
    .your-class::-webkit-input-placeholder {
        color: #b2b2b2;
    }
    .your-class:-moz-placeholder {
        /* FF 4-18 */
        color: #b2b2b2;
    }
    .your-class::-moz-placeholder {
        /* FF 19+ */
        color: #b2b2b2;
    }
    .your-class:-ms-input-placeholder {
        /* IE 10+ */
        color: #b2b2b2;
    }
</style>
<script>
    $(document).ready(function() {
        $(".printable_icons").hide();


        $(document).on('click', '#add-notes', function(e) {

            e.preventDefault();
            var tags_chk_values = [];
            $('input.check_class').each(function () {
                if ($(this).is(':checked')) {
                    tags_chk_values.push($(this).attr('account_tag_id'));
                }
            });

            if(tags_chk_values!==''){
                $('#txtVideostandard_vid_acc_tag_ids').val(tags_chk_values);
                var standardswhenpresent_acc_tag_ids = $('#txtVideostandard_vid_acc_tag_ids').val();
            }else{
                standardswhenpresent_acc_tag_ids='';
            }

            if ($('#pause_while_type').length > 0)
            {
                playPlayer();
            }
            if ($('#observations-comments').val() == '' && ($('#txtVideostandard').val() == '' || $('#txtVideostandard').length == 0) && $('#txtVideoTags').val() == '') {
                $('#observations-comments').css('border', '1px solid red');
                $('#observations-comments').css('border-radius', '0');
                return true;
            }
            if ($('.timer_box2').text() == '00:00:00') {
                var ob_time = $('#sw_h').text() + ':' + $('#sw_m').text() + ':' + $('#sw_s').text();
            } else {
                var ob_time = $('.timer_box2').text();
            }
            $('#observations-comments').css('border', 'none');
            if ($('#txtVideostandard').length > 0)
            {
                var standardswhenpresent = $('#txtVideostandard').val();
            }
            else {
                standardswhenpresent = '';
            }

            var result = '00:00:00';
            if ($('#for_synchro_time').is(':checked')) {
                var totalSec = $('#synchro_time').val();
                var hours = Math.floor(totalSec / 3600);
                var minutes = Math.floor((totalSec - (hours * 3600)) / 60);
                var seconds = totalSec - (hours * 3600) - (minutes * 60);

                result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
            }

            var postData = {
                observation_title: $('#ObservationTitle').text(),
                observations_comments: $('#observations-comments').val(),
                observations_standards: standardswhenpresent,
                observations_tags: $('#txtVideoTags').val(),
                observations_time: result,
                account_id: $('#account_id').val(),
                user_id: $('#user_id').val(),
                huddle_id: $('#huddle_id').val(),
                video_id: $('#video_id').val(),
                internal_comment_id: observation_notes_array.length + 1,
                comments_optimized: 1,
                observations_standard_acc_tag_id: standardswhenpresent_acc_tag_ids,
                assessment_value: $('#synchro_time_class_tags1').val()
            };
            console.log(postData);

            $("ul#expList").find('input:checkbox').removeAttr('checked');
            $('#observations-comments').val('');
            $('#txtVideoTags1_tagsinput span').remove();
            $('#txtVideostandard1_tagsinput span').remove();
            $('#txtVideostandard1_tag').show();
            $('.timer_box2').text('00:00:00');
            $('.timer_box2').hide();

            var tagid = '';
            var quick_tag_counter = 0;
            $(".divblockwidth a").each(function(value) {
                $(this).attr('status_flag', '0');
                $(this).removeAttr('class');

                if (quick_tag_counter == 0) {
                    $(this).attr('class', 'default_tag123 tags_qucls');
                }

                if (quick_tag_counter == 1) {
                    $(this).attr('class', 'default_tag123 tags_sugcls');
                }

                if (quick_tag_counter == 2) {
                    $(this).attr('class', 'default_tag123 tags_notescls');
                }

                if (quick_tag_counter == 3) {
                    $(this).attr('class', 'default_tag123 tags_strangthcls');
                }

                quick_tag_counter++;
            });
            var tagid = '';
            var quick_tag_counter = 0;
            $(".divblockwidth2 a").each(function(value) {
                $(this).attr('status_flag', '0');
                $(this).removeAttr('class');

                if (quick_tag_counter == 0) {
                    $(this).attr('class', 'default_rating123 tags_qucls');
                }

                if (quick_tag_counter == 1) {
                    $(this).attr('class', 'default_rating123 tags_sugcls');
                }

                if (quick_tag_counter == 2) {
                    $(this).attr('class', 'default_rating123 tags_notescls');
                }

                if (quick_tag_counter == 3) {
                    $(this).attr('class', 'default_rating123 tags_strangthcls');
                }

                if (quick_tag_counter == 4) {
                    $(this).attr('class', 'default_rating123 tags_strangthcls');
                }

                quick_tag_counter++;
            });
            $('#synchro_time_class_tags1').val('');

            /*$('#observation-comments').html(response.comments);
             $('.call').html(response.comments);
             $('#video_id').val(response.document_id);*/

            $('#txtVideoTags').removeAllTag();
            $('#txtVideostandard').removeAllTag();
            $("#expList").find('input:checkbox').removeAttr('checked');
            $('#observations-comments').attr("placeholder", "Add a comment...");
            $('#observations-comments').addClass("your-class");
            $(".printable_icons").hide();
            $("#no_notes_added1").hide();

            observation_notes_array.push(postData);
            var count_comments = observation_notes_array.length;

            if (count_comments == 1 && ($('#count_comments').val() == 0 || $('#count_comments').val() == '')) {

                $('#observation-comments').append($('#observatons-added-notes'));
                $('#observatons-added-notes').show();
            }

            count_comments = parseInt($('#count_comments').val()) + 1;
            //$("#comments").html('Notes ('+ count_comments + ')');
            $('#comments').trigger('click');
            $('#count_comments').val(count_comments);

            generate_comment_html(postData);
            $('#observations-comments').css('height', '100px');
            $.ajax({
                type: "POST",
                url: home_url + "/Huddles/add_call_notes",
                dataType: 'json',
                data: postData,
                success: function(response) {

                    var internal_id = response.internal_comment_id;
                    $('#normal_comment_' + internal_id).html(response.comments);
                    $('#comments').html('Notes (' + response.total_comments + ')');
                },
                error: function(e) {

                }
            });

        });

        function generate_comment_html(postData) {

            var html = '<ul class="comments comments_huddles" id="normal_comment_' + postData.internal_comment_id + '" style="overflow-x: hidden;">';
            html += '<div rel="' + postData.internal_comment_id + '" id="comment_box' + postData.internal_comment_id + '" style="margin-top: 12px;">';

            html += '<li class="synchro"> ';
            html += '<div class="synchro-inner"> ';
            html += '<i></i><span data-time="' + postData.observations_time + '" class="synchro-time">' + postData.observations_time + '</span>';
            html += '</div></li>';

            html += '<li class="comment thread" style="display: block;float: left;">';
            html += $('#comments_user_meta_data').html();

            html += '<div class="comment-body comment more">' + nl2br(postData.observations_comments) + '</div>'
            html += '<div class="comment-footer">';
            html += '<span class="comment-date">Saving comment....</span> <div class="comment-actions" style="width: 150px;">&nbsp;</div>'
            html += ' </div>';


            html += '<div id="docs-standards" class="docs-standards" style="margin-bottom: 20px;display: block;float: left;">';

            if (postData.observations_standards != '') {

                html += '<div id="tagFilterList"><div id="tagFilterContainer" class="btnwraper">';

                var standards_selected = postData.observations_standards.split(',');

                for (var i = 0; i < standards_selected.length; i++) {

                    html += '<button class="btn btn-lilghtgray" style="margin-right: 2px;">' + standards_selected[i] + '</button>';
                }

                html += '</div></div>';

            }

            if (postData.observations_tags != '') {

                html += '<div id="tagFilterList"><div id="tagFilterContainer" class="btnwraper">';

                var standards_selected = postData.observations_tags.split(',');

                for (var i = 0; i < standards_selected.length; i++) {

                    html += '<button class="btn btn-lilghtgray" style="margin-right: 2px;">' + standards_selected[i] + '</button>';
                }

                html += '</div></div>';

            }

            html += '</div>';


            html += '</li>';

            html += '</div></ul>';
            if ($('#vidComments').length > 0)
            {
                $('#vidComments').prepend(html);
            }
            else {
                $("#observatons-added-notes").prepend(html);
            }

        }

        $('#observations-comments').keypress(function(e) {
            $hours = $('#sw_h').text();
            $min = $('#sw_m').text();
            $sec = $('#sw_s').text();
            $time = $hours + ':' + $min + ':' + $sec;
            if ($('.timer_box2').text() == '00:00:00') {
                $('.timer_box2').text($time);
                $('.timer_box2').show();

            }
            $(this).css('border', '0');
            $(this).css('border-radius', '0');
        });
        $('.default_tag123').on('click', function(e) {
            e.preventDefault();
            var posid = '';
            tag_name = $(this).text();
            tag_name.replace(/\s/g, "");

            posid = $(this).attr('position_id');
            //$( ".default_tag" ).addClass(defaulttagsclasses(0));
            //var numItems = $('.yourclass').length;
            $('.default_tag123').each(function(index) {
                var tag_name1 = $(this).text();
                tag_name1.replace(/\s/g, "");
                $('#txtVideoTags').removeTag(tag_name1);
                if ($(this).hasClass(defaulttagsclasses(index))) {
                    $('#txtVideoTags').removeTag(tag_name1);
                    $(this).removeClass(defaulttagsclasses(index) + 'bg');
                }
            });
            $('.default_tag123').each(function(index) {
                if ($(this).attr('position_id') != posid) {
                    $(this).removeClass(defaulttagsclasses($(this).attr('position_id')));
                    $(this).addClass(defaulttagsclasses(0));

                }
            });

            if ($(this).hasClass(defaulttagsclasses(posid))) {
                $('#txtVideoTags').removeTag(tag_name);
                $(this).addClass(defaulttagsclasses(0));
                $(this).removeClass(defaulttagsclasses(posid));
                $(this).attr('status_flag', '0');
                $('#comment_comment').attr("placeholder", "Add a comment");
                $('#synchro_time_class').val('');
            }
            else {
                $('#txtVideoTags').addTag(tag_name);
                $(this).addClass(defaulttagsclasses(posid));
                $(this).removeClass(defaulttagsclasses(0));
                $(this).attr('status_flag', '1')
                $('#comment_comment').focus();
                //var spt=$(this).text().split(" ");
                var spt = $(this).text().slice(2);
                $('#comment_comment').attr("placeholder", "Add " + spt);
                $('#synchro_time_class').val('short_tag_' + posid);
            }
        });

        $('.default_rating123').on('click', function(e) {
            e.preventDefault();
            var posid = '';
            tag_name = $(this).text();
            tag_name.replace(/\s/g, "");
            tag_name_mod = tag_name.replace('# ', '');

            posid = $(this).attr('position_id');
            $('.default_rating123').each(function(index) {
                var tag_name1 = $(this).text();
                tag_name1.replace(/\s/g, "");
                $('#txtVideoTags').removeTag(tag_name1);
                if ($(this).hasClass(defaulttagsclasses(index))) {
                    $('#txtVideoTags').removeTag(tag_name1);
                    $(this).removeClass(defaulttagsclasses(index) + 'bg');
                }
            });
            $('.default_rating123').each(function(index) {
                if ($(this).attr('position_id') != posid) {
                    $(this).removeClass(defaulttagsclasses($(this).attr('position_id')));
                    $(this).addClass(defaulttagsclasses(0));

                }
            });

            if ($(this).hasClass(defaulttagsclasses(posid))) {
                $('#txtVideoTags').removeTag(tag_name);
                $(this).addClass(defaulttagsclasses(0));
                $(this).removeClass(defaulttagsclasses(posid));
                $(this).attr('status_flag', '0');
                $('#comment_comment').attr("placeholder", "Add a comment");
                $('#synchro_time_class').val('');
                $('#synchro_time_class_tags1').val('');
            }
            else {
                //$('#txtVideoTags').addTag(tag_name);
                $(this).addClass(defaulttagsclasses(posid));
                $(this).removeClass(defaulttagsclasses(0));
                $(this).attr('status_flag', '1');
                //$('#comment_comment').focus();
                //var spt=$(this).text().split(" ");
                //var spt = $(this).text().slice(2);
                //$('#comment_comment').attr("placeholder", "Add " + spt);
                $('#synchro_time_class_tags1').val(tag_name_mod);
                $('#synchro_time_class').val('short_tag_' + posid);
            }
        });

        $(document).on('click', '#publish-observations', function(e) {
            e.preventDefault();
            video_id = $('#video_id').val();
            huddle_id = $('#huddle_id').val();
            $.ajax({
                type: "POST",
                url: home_url + "/huddles/publish_observation",
                dataType: 'json',
                data: {
                    account_id: $('#account_id').val(),
                    user_id: $('#user_id').val(),
                    huddle_id: huddle_id,
                    video_id: video_id
                },
                success: function(response) {
                    // console.log(response.document_id);
                    //json_decode(response);
                    var new_id = response.document_id;
                    // alert(new_id);
                    window.location.href = home_url + '/Huddles/view/' + huddle_id + '/' + '1/' + new_id;
                },
                error: function(e) {

                }
            });
        });
        video_id = $('#video_id').val();
        $.ajax({
            type: 'post',
            url: home_url + '/Huddles/mobile_video/' + video_id,
            data: {video_id: $("#video_id").val()},
            dataType: 'json',
            success: function(data)
            {

                if (data.Document.published === '0')
                {
                    var intrvl = setInterval(function()
                    {

                        $('.divblockwidth').hide();
                        $('.note_box').hide();
                        $('.input-group').hide();
                        video_id = $('#video_id').val();
                        huddle_id = $('#huddle_id').val();
                        $.ajax({
                            type: 'post',
                            url: home_url + '/Huddles/mobile_video/' + video_id,
                            data: {video_id: $("#video_id").val()},
                            dataType: 'json',
                            success: function(data)
                            {
                                width = data.Document.upload_progress + '%';
                                if (data.Document.upload_status == null)
                                    data.Document.upload_status = "initiating";
                                if (data.Document.upload_status == 'uploading') {
                                    title = width + ' ' + data.Document.upload_status + '...';
                                } else {
                                    title = width + ' ' + data.Document.upload_status;
                                }

                                if (data.Document.doc_type == '3' && data.Document.is_processed == 4 && data.Document.published == 0 && data.Document.upload_status != 'uploaded' && data.Document.upload_progress < 100) {
                                    $('#progress-title').html(title);
                                    $('#message-box').html('<div style="font-size: 9px;">&nbsp;</div><span style="width:' + width + ' ;color: #fff;position: absolute;left: 0px;top: 32%;text-align: center;text-transform: uppercase;background: #3e7554;top: 0;bottom: -1px;">&nbsp;</span>');
                                } else if (data.Document.doc_type == '3' && data.Document.is_processed == 4 && data.Document.published == 0 && (data.Document.upload_progress == 100 || data.Document.upload_status == 'uploaded')) {

                                    $('#progress-title').html("");
                                    $('#progress-title').removeAttr("style");
                                    $('#message-box').html('<div class="empty_video_box" style="padding-top:105px !important; padding-left: 2px !important;width: 500px; "><img src="<?php echo $this->webroot . 'img/loading.gif' ?>" style="margin-bottom:10px;width: 20px;height: 20px;min-width: 0px;min-height: 0px;" /><p><?=$alert_messages["Your_video_is_currently_processing"]; ?></p></div>');
                                }
                                else if (data.Document.published === '1')
                                {
                                    window.location = home_url + '/Huddles/observation_details_3/' + huddle_id + '/' + video_id;
                                    clearInterval(intrvl);
                                }
                            }
                        });



                    }, 1000);//time in milliseconds

                }


            }
        });

    });
    function defaulttagsclasses(posid) {
        var class_name = 'tags_qucls';
        if (posid == 1) {
            class_name = 'tags_quclsbg';
        }
        else if (posid == 2) {
            class_name = 'tags_sugclsbg';
        }
        else if (posid == 3) {
            class_name = 'tags_notesclsbg';
        }
        else if (posid == 4) {
            class_name = 'tags_strangthclsbg';
        }
        else if (posid == 5) {
            class_name = 'tags_strangthclsbg';
        }
        return class_name;
    }
    function defaulttagsclasses1(posid) {
        alert(posid);
        var class_name = 'tags_qucls';
        if (posid == 1) {
            class_name = 'tags_qucls';
        }
        else if (posid == 2) {
            class_name = 'tags_sugcls';
        }
        else if (posid == 3) {
            class_name = 'tags_notescls';
        }
        else if (posid == 4) {
            class_name = 'tags_strangthcls';
        }
        else if (posid == 5) {
            class_name = 'tags_strangthcls';
        }
        return class_name;
    }

</script>   
<script type="text/javascript">
    $(document).ready(function(e) {
        function sbtfrm() {
            $("ul#expList").find('input:checkbox').removeAttr('checked');
        }
        $('#txtVideoTags').tagsInput({
            defaultText: 'Tags...',
            width: '200px',
            placeholderColor: '#898686'
        });
        $('.video-tags-row label').css('display', 'none');
        $('#txtVideostandard').tagsInput({
            defaultText: 'Tag Standards...',
            width: '350px',
            readonly_input: true,
            placeholderColor: '#898686',
            onRemoveTag: function(val) {
                if ($("div[id=txtVideostandard_tagsinput]").children('.tag').length < 1) {
                    $('#txtVideostandard_tag').show();
                }

                var removed_tag_val = val;
                var checkboxes = $('#expList li input[type="checkbox"]');
                for (var i = 0; i < checkboxes.length; i++) {

                    var chkStandard = $(checkboxes[i]);
                    var tag_code = chkStandard.attr('st_code');
                    var tag_value = '';
                    var tag_array = {};
                    var tag_name = chkStandard.attr('st_name');
                    tag_array = tag_name.split(" ");
                    if (tag_array && tag_array.length > 0) {

                        var tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                        if (removed_tag_val == tag_value) {
                            chkStandard.prop('checked', false);
                        }
                    }

                }

            },
        });

    });
    $(function() {

        $('input#txtSearchTags').quicksearch('ul#expList li', {
            noResults: 'li#noresults',
            'onAfter': function() {
                if (typeof $('#txtSearchTags2').val() !== "undefined" && $('#txtSearchTags2').val().length > 0) {
                    $('#clearTagsButton1').css('display', 'block');
                }
            }
        });
    });

</script>
<script>
    $(document).ready(function() {
        /*document.getElementById("txtVideostandard_tagsinput").addEventListener("click", ActivateFramework);
         
         function ActivateFramework() {
         $('#frameWorkss').trigger('click');
         }*/

        $("#ObservationTitle").mouseover(function() {
            $(this).css("backgroundColor", "#FAFABE")
        });
        $("#ObservationTitle").mouseout(function() {
            $(this).css("backgroundColor", "#ffffff");
        });
        $("#ObservationTitle").click(function() {
            $(this).css("display", "none");
            $(".editArea").css("display", "block");
            $("#ajaxInput").val($(this).text().trim());
            $("#ajaxInput").focus();
        });
        $("#ajaxInput").blur(function() {

            $(".editArea").css("display", "none");
            $("#ObservationTitle").css("display", "block");
            $("#ObservationTitle").text($(this).val());
            $.ajax({
                type: "POST",
                url: home_url + "/huddles/update_note",
                data: {
                    huddle_id: $('#huddle_id').val(),
                    video_id: $('#video_id').val(),
                    title: $("#ObservationTitle").text()
                },
                success: function(response) {
                    //  $("#ObservationTitle1").html($("#ObservationTitle").text());
                    // $('#subject').val($("#ObservationTitle").text());
                },
                error: function(e) {

                }
            });


        });


        $("#txtSearchTags").on("keypress", function(e) {
            $("#clearTagsButton").show();

        });

        $(document).on('click', '#clearTagsButton', function(e) {
            $("#txtSearchTags").val('');
            $("#clearTagsButton").hide();

            $('input#txtSearchTags').quicksearch('ul#expList li', {
                noResults: 'li#noresults',
                'onAfter': function() {
                    if (typeof $('#txtSearchTags2').val() !== "undefined" && $('#txtSearchTags2').val().length > 0) {
                        $('#clearTagsButton1').css('display', 'block');
                    }
                }
            });

        });








        $("#observations-comments").keyup(function(e) {
            var str_length = $("#observations-comments").val().trim();
            if (e.which == 13 && $("input#press_enter_to_send").prop('checked') == true) {
                $("#add-notes").trigger('click');
            }
        });
    });

    $('#pause_while_type').click(function() {
        var value = $("#pause_while_type").is(':checked') ? 1 : 0;
        var user_id = "<?php echo $user_id; ?>";
        var account_id = "<?php echo $account_id; ?>";

        $.ajax({
            type: 'POST',
            url: home_url + '/Huddles/type_pause/',
            data: {value: value, user_id: user_id, account_id: account_id},
            success: function(res) {
            },
            errors: function(response) {

            }
        });


    });


    if ($('#observations-comments').length > 0) {

        $('#observations-comments').keyup(function(e) {


            var value = $("#pause_while_type").is(':checked') ? 1 : 0;
            if (value && e.which != 13)
            {
                pausePlayer();
            }

        });
    }

    $('input.check_class').on('change', function() {


    //    $('input.check_class').not(this).prop('checked', false);

        $(this).each(function() {

            var tag_code = $(this).attr('st_code');
            var tag_value = '';
            var tag_array = {};
            var tag_name = $(this).attr('st_name');
            tag_array = tag_name.split(" ");
            if ($(this).is(':checked')) {
                tag_array = tag_name.split(" ");
                tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                $('#txtVideostandard').addTag(tag_value);
                if ($('input[name="name1"]:checked').length > 0) {
                    $('#txtVideostandard_tag').hide();
                }
            } else {
                tag_value = '# ' + tag_code + '-' + tag_array[0] + '...';
                $('#txtVideostandard').removeTag(tag_value);
            }

        });




    });
    $(document).ready(function() {
        $('#press_enter_to_send').click(function() {
            var value = $("#press_enter_to_send").is(':checked') ? 1 : 0;
            var user_id = "<?php echo $user_id; ?>";
            var account_id = "<?php echo $account_id; ?>";
            $.ajax({
                type: 'POST',
                url: home_url + '/Huddles/press_enter_to_send/',
                data: {
                    value: value,
                    user_id: user_id,
                    account_id: account_id
                },
                success: function(res) {
                },
                errors: function(response) {

                }
            });
        });
    });
    function textAreaAdjust(o) {
        o.style.height = "1px";
        o.style.minHeight = "75px";
        o.style.height = (25 + o.scrollHeight) + "px";
        o.style.maxHeight = (5 + o.scrollHeight) + "px";
//                            alert(o.parentElement.style.minHeight);
//                            o.parentElement.style.height = (5 + parseInt(o.parentElement.style.minHeight)) + "px";
    }
    function nl2br(str, is_xhtml) {
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
    }
</script>








