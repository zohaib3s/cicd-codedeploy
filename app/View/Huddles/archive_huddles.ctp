<?php
$user_current_account = $this->Session->read('user_current_account');
$user_permissions = $this->Session->read('user_permissions');
$loggedInUserRole = '';
$users = $this->Session->read('user_current_account');
$account_id = $users['accounts']['account_id'];

$users_shown = array();
$groups_shown = array();
$is_group = '';
?> 

<div id="archive_search">
<style>
                                    .box.style2{
                                        margin:0px;
                                        border: 1px solid #dddad2;
                                        margin-bottom:-1px;
                                        border-radius:0px;
                                        padding:8px;
                                        min-height:70px;
                                    }
                                    .huddle_list_left{
                                        float:left; 
                                        width:710px;              
                                    }
                                    .huddle_list_right{
                                        float:right; 
                                    }
                                    .row{
                                        margin-left:0px;
                                    }
                                    [data-group] {
                                        margin-top: 6px;
                                    }
                                    .grid_listHeader{
                                            font-size: 15px;
                                            color: #424240;
                                            padding: 0px 30px;
                                            font-weight: 600;
                                            margin-bottom:10px;
                                    }
                                    .grid_listHeader span{
                                        display:inline-block;
                                    }
                                    .header_name{
                                        float:left !important;
                                    }
                                    .header_rightBox{
                                        float:right;
                                    }
                                    .header_video{
                                        margin-left:10px;
                                    }
                                    .header_resources{
                                        margin-left:10px;
                                    }
                                    .header_date{
                                        margin-left:10px;
                                            width: 100px;
                                    }
                                    .header_huddle{
                                        margin-left:10px;
                                    }
                                    .header_participant{
                                        margin-left:10px;
                                    }
                                    .header_action{
                                        margin-left:10px;
                                        width: 58px;
                                    }
                                    
                                </style>
                                <div class="grid_listHeader">
                                    <a style="margin-top: -32px;margin-left: -36px;" href="<?php echo $this->base . '/accounts/account_settings_all/'.$account_id.'/3' ?>" class="back"><?php echo $language_based_content['back_to_account_settings']; ?></a>
                                    <div class="clear"></div>
                                    <span class="header_name archive_cl"><input type="checkbox" id="select_all"/> <?php echo $language_based_content['select_all']; ?></span>
                                    <span class="header_name archive_cl"></span>
                                    <div class="header_rightBox">
                                    <span class="header_video"><?php echo $language_based_content['videos']; ?></span>
                                    <span class="header_resources"><?php echo $language_based_content['resources']; ?></span>
                                    <span class="header_date"><?php echo $language_based_content['date_created']; ?></span>
                                    
                                    
                                    </div>
                                    <div class="clear"></div>
                                </div>
        

            <?php if (isset($video_huddles) && !empty($video_huddles)):?>
                                    <ol class="huddle-list-view mainfold launchPad2" style="margin-top: -2px;" >
                    <?php
                    $grups = array();
                    foreach ($video_huddles as $row):
                        $first_letter = $this->Custom->py_slice(trim($row['AccountFolder']['name']), '0');
                        $grups[$row['AccountFolder']['account_folder_id']] = strtoupper($first_letter);
                        $grups = array_unique($grups);
                        ?>
                    <?php endforeach; ?>
                                        
                     <form id="archive_form" method="post" enctype="multipart/form-data" action="<?php echo $this->base . '/Huddles/archive/1'; ?>" accept-charset="UTF-8"> 
                      <div id="ajax_attachment">   
                    <?php foreach ($video_huddles as $row): ?>   
                        <?php
                        $invitedUser = '';
                        $huddleUsers = $AccountFolder->getHuddleUsers($row['AccountFolder']['account_folder_id']);
                        $huddleGroups = $AccountFolder->getHuddleGroups($row['AccountFolder']['account_folder_id']);
                        $loggedInUserRole = $this->Custom->has_admin_access($huddleUsers, $huddleGroups, $user_current_account['User']['id']);
                        ?>
                        <?php if ($row['AccountFolder']['folder_type'] == '5'): ?>
                                        <li class="folderlist" data-group="<?php echo ($sort == 'name') ? isset($grups[$row['AccountFolder']['account_folder_id']]) ? $grups[$row['AccountFolder']['account_folder_id']] : '' : ''; ?>">
                                           
                                            <span class="foldrcl1"><?php echo $this->Html->image('icons_folder.png'); ?></span>
                                            <span class="foldrcl2"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="<?php echo $this->base . '/Folder/' . $row['AccountFolder']['account_folder_id'] ?>">
                                            <?php echo $row['AccountFolder']['name']; ?>
                                                </a></span>
                                            <?php else: ?>
                                        <li class="huddlist <?php if (isset($folders) && !empty($folders)): ?><?php if ($loggedInUserRole == 200 || ($this->Custom->is_creator($user_current_account['User']['id'], $row['AccountFolder']['created_by']) && $user_permissions['UserAccount']['permission_maintain_folders'] == '1')): ?>card<?php endif;?><?php endif;?>" id="huddle_row_<?php echo $row['AccountFolder']['account_folder_id']; ?>" huddle_id="<?php echo $row['AccountFolder']['account_folder_id']; ?>">

                                            <div class="huddle_list_detail handle">
                                                 <span><input name="selector[]" id="ad_Checkbox_<?php echo $row['AccountFolder']['account_folder_id']; ?>" class="ads_Checkbox" type="checkbox" value="<?php echo $row['AccountFolder']['account_folder_id']; ?>" /></span>
                                                <span class="foldrcl1" style="white-space: nowrap;display: inline-block;text-overflow: ellipsis;overflow: hidden;width: 48%;position: relative;"><a title="<?php echo $row['AccountFolder']['name']; ?>" href="javascript:void(0)">
                                            <?php echo $row['AccountFolder']['name']; ?>
                                                    </a></span>
                <?php endif; ?>               
                                            
                                                    
                                                    
                                                    <span class="foldrcl3" style="text-align:center;margin-right:25px;">&nbsp;<?php if($_SESSION['LANG'] == 'es'){ echo $this->Custom->SpanishDate(strtotime($row['AccountFolder']['created_date']),'archive_module');  } else{ echo date('M d, Y', strtotime($row['AccountFolder']['created_date']));} ?></span>
                                          <?php if ($row['AccountFolder']['folder_type'] != '5'): ?>
                                                    <span class="foldrcl7" style="text-align:center;width: 80px;"><?php echo $row['AccountFolder']['total_docs'];?></span>
                                                    <span class="foldrcl6" style="text-align:center;"><?php echo $row['AccountFolder']['total_videos'];?></span>
                                          <?php endif; ?>
                                                    <div class="clear"></div>

                                            </div>
                                        </li>
           <?php endforeach; ?></div> </form>
                                    </ol> 

            <?php endif; ?>
     </div>

                                <script>
 $(document).ready(function () {
var select_all = document.getElementById("select_all"); //select all checkbox
var checkboxes = document.getElementsByClassName("ads_Checkbox"); //checkbox items

//select all checkboxes
select_all.addEventListener("change", function(e){
    for (i = 0; i < checkboxes.length; i++) { 
        checkboxes[i].checked = select_all.checked;
    }
});

//uncheck "select all", if one of the listed checkbox item is unchecked
for (var i = 0; i < checkboxes.length; i++) {
    checkboxes[i].addEventListener('change', function(e){
        if(this.checked == false){
            select_all.checked = false;
        }
    });
}


function doArchiveSearch() {
    var $title = $('#txtSearchHuddles_archive').val();
    var $mode = $('#search-mode_archive').val();
    //$.blockUI({message: $('#domMessage').html()});
    $.ajax({
        url: home_url + '/Huddles/search_archive/1',
        data: {title: $title, mode: $mode},
        dataType: 'JSON',
        type: 'POST',
        success: function (response) {
            //$(document).ajaxStop($.unblockUI);
            if (response.status == true) {
                $('#archive_search').html(response.contents);
                $('.page-title__counter').html('( ' + response.huddles_count + ' )');
            } else {
                $('.page-title__counter').html($('#search-huddle-count').html());
                $('#archive_search').html(response.contents);
            }
             dragndrop();
        },
        error: function () {

        }
    });
}

 $('#btnHuddleSearch_archive').click(function () {
             $('.load_more').hide();
            if ($('#txtSearchHuddles_archive').val().length > 0) {
                doArchiveSearch();
            }
        });
        
         $('#txtSearchHuddles_archive').on("keypress", function (e) {
             $('.load_more').hide();
            $("#clearSearchHuddles").show();
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                doArchiveSearch();
                return false;
            }
        });
        
        $("#clearSearchHuddles").click(function () {
            $('.load_more').hide();
            $('#txtSearchHuddles_archive').val("");
            $("#clearSearchHuddles").hide();
            doArchiveSearch();
            return false;
        });





 });
    
    </script>
    
     <script type="text/javascript">
                var page = 2;
                function loadMoreObservations() {
                    $.ajax({
                        type: 'POST',
                        data: {
                            page: page,
                            load_more: 1
                        },
                        dataType:'json',
                        url: home_url + '/Huddles/archive_contents/1/1',
                        success: function (response) {
                            if (page == 0) {
                                $('#ajax_attachment').html(response);
                                $('#huddle_video_title_strong').html('Videos (' + $('#videos-list li.videos-list__item').length + ')');
                            } else {
                                var els = $(response);
                                els.appendTo($('#ajax_attachment'));//.hide().fadeIn('slow');
                                var top = els.eq(0).offset().top;
                                if (top > 0) {
                                    $('body').animate({scrollTop: top}, 500);
                                }
                                $('#huddle_video_title_strong').html('Videos (' + $('#videos-list li.videos-list__item').length + ')');
                            }
                            page++;
                        },
                        errors: function (response) {
                            alert(response.contents);
                        }

                    });
                }
                function hideLoadMore() {
                    $('.load_more').hide();
                }
            </script>
            
            <?php
            print $this->element('load_more', array(
                        'total_items' => $total_huddles,
                        'current_page' => $current_page,
                        'number_per_page' => 20,
                        'load_more_what'=>'huddles'
            ));
            ?>
<script>
    $(document).ready(function () {
    $(document).on("click", "#load_more_button", function () {
        $('.load_more').hide();
        $('#load_more_div').remove();
        
        
    });
    });
    
</script>    